<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh para modificar by Chale</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<script language=javascript> 
	
		function firstFocus(){
			var paso = "false";			
			for (i = 0; i < document.forms.length; i++){
				for (j = 0; j < document.forms[i].elements.length; j++){					
					if (document.forms[i].elements[j].type =="text" || document.forms[i].elements[j].type =="select-one"){					
						paso = "true";
						document.forms[i].elements[j].focus();					
						break;
					}
				}				
				if (paso =="true"){
					break;
				}
			}
			
		}
		
    </script>
</head>

<body onload="firstFocus();">
<f:view>

	<jsp:include page="/inc/topnomenu.jsp" />

	<table width="800"  border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top" align="center">
					<tr>	
						<!-- Login -->
						<td width="800" valign="top">
						<h:form>
							<f:verbatim><table class="datatable" width="260" align="center">
								<tr>
									<td colspan="2">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
								<tr>
            						<td>
            							Usuario
            						</td>
            						<td></f:verbatim>
    	                        		<h:inputText
                							size="20"
                							maxlength="20"
            								value="#{loginForm.usuario}" />
            						<f:verbatim></td>
            					</tr>
								<tr>
            						<td>
            							Contrase�a
            						</td>
            						<td></f:verbatim>
    	                        		<h:inputSecret
                							size="20"
                							maxlength="20"
            								value="#{loginForm.password}" />
            						<f:verbatim></td>
            					</tr>
            					<tr>
            						<td>
										Organismo
            						</td>
            						<td></f:verbatim>
                                        <h:selectOneListbox value="#{loginForm.selectOrganismo}"
                                        	immediate="false">
                                            <f:selectItems value="#{loginForm.colOrganismo}" />
                                        </h:selectOneListbox>
            						<f:verbatim></td>
            					</tr>
            					<tr>
            						<td colpsan="2" align="center"></f:verbatim>
										<h:commandButton value="Iniciar Sesi�n" 
                                    		action="#{loginForm.send}" />
									<f:verbatim></td>
            					</tr>
            					<tr>
            						<td colspan="2" align="center"></f:verbatim>
										<h:graphicImage url="/images/home/de-todos-65x40.gif" />
            						<f:verbatim></td>
            					</tr>
            					<tr>
            						<td colspan="2" align="center">     
            						  <jsp:include page="version.inc" />     
            						</td>
            					</tr>
            				</table></f:verbatim>
						</h:form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>