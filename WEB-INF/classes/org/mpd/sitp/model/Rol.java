package org.mpd.sitp.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.TreeSet;

public class Rol
  implements Serializable, Comparable
{
  public static final int CORDINADOR_REMESAS = 1;
  public static final int CORDINADOR_ANALISTAS = 2;
  public static final int ANALISTA = 3;
  private int idRol;
  private String nombre;
  private Collection opciones = new TreeSet();

  public int compareTo(Object object) {
    return toString().compareTo(object.toString());
  }

  public Collection getOpciones() {
    return this.opciones;
  }
  public void setOpciones(Collection opciones) {
    this.opciones = opciones;
  }
  public int getIdRol() {
    return this.idRol;
  }
  public void setIdRol(int idRol) {
    this.idRol = idRol;
  }

  public String getNombre() {
    return this.nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
}