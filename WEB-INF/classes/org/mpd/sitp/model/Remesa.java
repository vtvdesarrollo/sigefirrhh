package org.mpd.sitp.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.TreeSet;

public class Remesa
  implements Serializable
{
  private RemesaPK remesaPK;
  private int anio;
  private long numero;
  private Date fechaCreacion;
  private Date fechaEnvio;
  private Date fechaCierre;
  private String observaciones;
  private Collection movimientosSitp = new TreeSet();
  private Collection accionesRemesa = new TreeSet();
  private Collection trayectorias = new TreeSet();
  private Organismo organismo;
  private Analista analista;
  private long idAnalista;
  private String estatus;
  private String etapa;
  private boolean ingreso;
  private boolean asignacion;
  private boolean recepcion;
  private boolean cierreA;
  private boolean cierreCA;
  private boolean cierreCR;
  private boolean egreso;
  private boolean seleccionado;
  private Date fechaAsignacion;
  private Date fechaRecepcion;
  private Date fechaEntrega;
  private Date fechaRechazo;
  private Date fechaAprobacionAnalista;
  private Date fechaAprobacionCAnalistas;
  private Date fechaAprobacionCRemesas;

  public Date getFechaAprobacionAnalista()
  {
    return this.fechaAprobacionAnalista;
  }
  public void setFechaAprobacionAnalista(Date fechaAprobacionAnalista) {
    this.fechaAprobacionAnalista = fechaAprobacionAnalista;
  }
  public Date getFechaAprobacionCAnalistas() {
    return this.fechaAprobacionCAnalistas;
  }
  public void setFechaAprobacionCAnalistas(Date fechaAprobacionCAnalistas) {
    this.fechaAprobacionCAnalistas = fechaAprobacionCAnalistas;
  }
  public Date getFechaAprobacionCRemesas() {
    return this.fechaAprobacionCRemesas;
  }
  public void setFechaAprobacionCRemesas(Date fechaAprobacionCRemesas) {
    this.fechaAprobacionCRemesas = fechaAprobacionCRemesas;
  }
  public Date getFechaAsignacion() {
    return this.fechaAsignacion;
  }
  public void setFechaAsignacion(Date fechaAsignacion) {
    this.fechaAsignacion = fechaAsignacion;
  }
  public Date getFechaEntrega() {
    return this.fechaEntrega;
  }
  public void setFechaEntrega(Date fechaEntrega) {
    this.fechaEntrega = fechaEntrega;
  }
  public Date getFechaRecepcion() {
    return this.fechaRecepcion;
  }
  public void setFechaRecepcion(Date fechaRecepcion) {
    this.fechaRecepcion = fechaRecepcion;
  }
  public Date getFechaRechazo() {
    return this.fechaRechazo;
  }
  public void setFechaRechazo(Date fechaRechazo) {
    this.fechaRechazo = fechaRechazo;
  }
  public String getEtapa() {
    return this.etapa;
  }
  public void setEtapa(String etapa) {
    this.etapa = etapa;
  }
  public String getEstatus() {
    return this.estatus;
  }
  public void setEstatus(String estatus) {
    this.estatus = estatus;
  }
  public long getIdAnalista() {
    return this.idAnalista;
  }
  public void setIdAnalista(long idAnalista) {
    this.idAnalista = idAnalista;
  }
  public boolean isSeleccionado() {
    return this.seleccionado;
  }
  public void setSeleccionado(boolean seleccionado) {
    this.seleccionado = seleccionado;
  }
  public RemesaPK getRemesaPK() {
    return this.remesaPK;
  }
  public void setRemesaPK(RemesaPK remesaPK) {
    this.remesaPK = remesaPK;
  }

  public Organismo getOrganismo() {
    return this.organismo;
  }
  public void setOrganismo(Organismo organismo) {
    this.organismo = organismo;
  }

  public Analista getAnalista() {
    return this.analista;
  }
  public void setAnalista(Analista analista) {
    this.analista = analista;
  }
  public Collection getMovimientosSitp() {
    return this.movimientosSitp;
  }
  public void setMovimientosSitp(Collection movimientosSitp) {
    this.movimientosSitp = movimientosSitp;
  }
  public int getCountMovimientosSitp() {
    return this.movimientosSitp != null ? this.movimientosSitp.size() : 0;
  }
  public int getAnio() {
    return this.anio;
  }
  public Date getFechaCierre() {
    return this.fechaCierre;
  }
  public Date getFechaCreacion() {
    return this.fechaCreacion;
  }
  public Date getFechaEnvio() {
    return this.fechaEnvio;
  }
  public String getObservaciones() {
    return this.observaciones;
  }

  public long getNumero() {
    return this.numero;
  }
  public void setNumero(long numero) {
    this.numero = numero;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public void setFechaCierre(Date fechaCierre) {
    this.fechaCierre = fechaCierre;
  }
  public void setFechaCreacion(Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }
  public void setFechaEnvio(Date fechaEnvio) {
    this.fechaEnvio = fechaEnvio;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public Collection getAccionesRemesa() {
    return this.accionesRemesa;
  }
  public void setAccionesRemesa(Collection accionesRemesa) {
    this.accionesRemesa = accionesRemesa;
  }
  public boolean isAsignacion() {
    return this.asignacion;
  }

  public void setAsignacion(boolean asignacion) {
    this.asignacion = asignacion;
  }
  public boolean isCierreA() {
    return this.cierreA;
  }
  public void setCierreA(boolean cierreA) {
    this.cierreA = cierreA;
  }
  public boolean isCierreCA() {
    return this.cierreCA;
  }
  public void setCierreCA(boolean cierreCA) {
    this.cierreCA = cierreCA;
  }
  public boolean isCierreCR() {
    return this.cierreCR;
  }
  public void setCierreCR(boolean cierreCR) {
    this.cierreCR = cierreCR;
  }
  public void setEgreso(boolean egreso) {
    this.egreso = egreso;
  }
  public void setIngreso(boolean ingreso) {
    this.ingreso = ingreso;
  }
  public void setRecepcion(boolean recepcion) {
    this.recepcion = recepcion;
  }
  public boolean isEgreso() {
    return this.egreso;
  }
  public boolean isIngreso() {
    return this.ingreso;
  }
  public boolean isRecepcion() {
    return this.recepcion;
  }
  public int getCountTrayectorias() {
    return this.trayectorias != null ? this.trayectorias.size() : 0;
  }
  public Collection getTrayectorias() {
    return this.trayectorias;
  }
  public void setTrayectorias(Collection trayectorias) {
    this.trayectorias = trayectorias;
  }
}