package org.mpd.sitp.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.TreeSet;

public class Analista
  implements Serializable
{
  private int idAnalista;
  private String usuario;
  private String password;
  private String nombres;
  private String apellidos;
  private Collection roles = new TreeSet();

  public String getNombre() {
    return this.nombres + " " + this.apellidos;
  }

  public String getPassword() {
    return this.password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public String getApellidos() {
    return this.apellidos;
  }
  public int getIdAnalista() {
    return this.idAnalista;
  }
  public String getNombres() {
    return this.nombres;
  }
  public Collection getRoles() {
    return this.roles;
  }
  public String getUsuario() {
    return this.usuario;
  }
  public void setApellidos(String apellidos) {
    this.apellidos = apellidos;
  }
  public void setIdAnalista(int idAnalista) {
    this.idAnalista = idAnalista;
  }
  public void setNombres(String nombres) {
    this.nombres = nombres;
  }
  public void setRoles(Collection roles) {
    this.roles = roles;
  }
  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }
}