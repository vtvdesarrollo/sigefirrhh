package org.mpd.sitp.model;

import java.io.Serializable;

public class TrayectoriaPK
  implements Serializable
{
  public long idOrganismo;
  public long idTrayectoria;

  public TrayectoriaPK()
  {
  }

  public TrayectoriaPK(long idOrganismo, long idTrayectoria)
  {
    this.idOrganismo = idOrganismo;
    this.idTrayectoria = idTrayectoria;
  }
  public void setIdTrayectoria(long i) {
    this.idTrayectoria = i;
  }
  public long getIdTrayectoria() {
    return this.idTrayectoria;
  }
  public void setIdOrganismo(long i) {
    this.idOrganismo = i;
  }
  public long getIdOrganismo() {
    return this.idOrganismo;
  }
}