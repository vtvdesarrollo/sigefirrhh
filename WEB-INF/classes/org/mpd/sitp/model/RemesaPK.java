package org.mpd.sitp.model;

import java.io.Serializable;

public class RemesaPK
  implements Serializable
{
  public long idOrganismo;
  public long idRemesa;

  public RemesaPK()
  {
  }

  public RemesaPK(long idOrganismo, long idRemesa)
  {
    this.idOrganismo = idOrganismo;
    this.idRemesa = idRemesa;
  }
  public void setIdRemesa(long i) {
    this.idRemesa = i;
  }
  public long getIdRemesa() {
    return this.idRemesa;
  }
  public void setIdOrganismo(long i) {
    this.idOrganismo = i;
  }
  public long getIdOrganismo() {
    return this.idOrganismo;
  }
}