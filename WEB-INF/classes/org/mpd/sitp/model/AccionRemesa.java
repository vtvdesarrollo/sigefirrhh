package org.mpd.sitp.model;

import java.io.Serializable;
import java.util.Date;

public class AccionRemesa
  implements Serializable
{
  private long idAccionRemesa;
  private String tipoAccion;
  private Date fecha;
  private Analista analista;
  public static final String INGRESO = "IN";
  public static final String ASIGNACION = "AS";
  public static final String RECEPCION = "RE";
  public static final String CIERRE_A = "C_A";
  public static final String CIERRE_CA = "C_CA";
  public static final String CIERRE_CR = "C_CR";
  public static final String EGRESO = "EG";

  public String getDescripcion()
  {
    String nombre = null;

    if (this.analista != null) {
      nombre = this.analista.getNombre();
    }
    if (this.tipoAccion.equals("IN"))
      return "Ingreso al sistema";
    if (this.tipoAccion.equals("AS"))
      return "Asignado al analista por el coordinador de remesas " + nombre;
    if (this.tipoAccion.equals("RE"))
      return "Se recibio el físico por el coordinador de remesas " + nombre;
    if (this.tipoAccion.equals("C_A"))
      return "Fué cerrado por el analista " + nombre;
    if (this.tipoAccion.equals("C_CA"))
      return "Fué cerrado por el coordinador de analistas " + nombre;
    if (this.tipoAccion.equals("C_CR"))
      return "Fué cerrado por el coordinador de remesas " + nombre;
    if (this.tipoAccion.equals("EG")) {
      return "Egresó del sistema";
    }
    return "Error esta acción no se le ha asignado descripción";
  }

  public Analista getAnalista()
  {
    return this.analista;
  }
  public Date getFecha() {
    return this.fecha;
  }

  public long getIdAccionRemesa() {
    return this.idAccionRemesa;
  }
  public void setIdAccionRemesa(long idAccionRemesa) {
    this.idAccionRemesa = idAccionRemesa;
  }

  public void setAnalista(Analista analista) {
    this.analista = analista;
  }
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  public String getTipoAccion() {
    return this.tipoAccion;
  }
  public void setTipoAccion(String etapa) {
    this.tipoAccion = etapa;
  }
}