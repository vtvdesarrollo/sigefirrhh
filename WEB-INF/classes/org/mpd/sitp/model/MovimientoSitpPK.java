package org.mpd.sitp.model;

import java.io.Serializable;

public class MovimientoSitpPK
  implements Serializable
{
  private long idOrganismo;
  private long idMovimientoSitp;

  public MovimientoSitpPK()
  {
  }

  public MovimientoSitpPK(long idOrganismo, long idMovimientoSitp)
  {
    this.idOrganismo = idOrganismo;
    this.idMovimientoSitp = idMovimientoSitp;
  }

  public long getIdMovimientoSitp() {
    return this.idMovimientoSitp;
  }
  public long getIdOrganismo() {
    return this.idOrganismo;
  }
  public void setIdMovimientoSitp(long idMovimientoSitp) {
    this.idMovimientoSitp = idMovimientoSitp;
  }
  public void setIdOrganismo(long idOrganismo) {
    this.idOrganismo = idOrganismo;
  }
}