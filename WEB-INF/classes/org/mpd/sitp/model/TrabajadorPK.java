package org.mpd.sitp.model;

import java.io.Serializable;

public class TrabajadorPK
  implements Serializable
{
  private long idOrganismo;
  private long idTrabajador;

  public long getIdOrganismo()
  {
    return this.idOrganismo;
  }
  public void setIdOrganismo(long idOrganismo) {
    this.idOrganismo = idOrganismo;
  }
  public long getIdTrabajador() {
    return this.idTrabajador;
  }
  public void setIdTrabajador(long idTrabajador) {
    this.idTrabajador = idTrabajador;
  }
}