package org.mpd.sitp.model;

import java.io.Serializable;

public class Opcion
  implements Comparable, Serializable
{
  private int idOpcion;
  private String nombre;
  private String comando;
  private int numero;

  public int compareTo(Object object)
  {
    return toString().compareTo(object.toString());
  }

  public int getNumero()
  {
    return this.numero;
  }
  public void setNumero(int numero) {
    this.numero = numero;
  }
  public int getIdOpcion() {
    return this.idOpcion;
  }
  public void setIdOpcion(int idOpcion) {
    this.idOpcion = idOpcion;
  }
  public String getComando() {
    return this.comando;
  }
  public void setComando(String comando) {
    this.comando = comando;
  }
  public String getNombre() {
    return this.nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
}