package org.mpd.sitp.model;

import java.io.Serializable;

public class Organismo
  implements Serializable
{
  private long idOrganismo;
  private String codigo;
  private String nombre;

  public String toString()
  {
    return this.codigo + " " + this.nombre;
  }
  public String getCodigo() {
    return this.codigo;
  }
  public long getIdOrganismo() {
    return this.idOrganismo;
  }
  public String getNombre() {
    return this.nombre;
  }
  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }
  public void setIdOrganismo(long idOrganismo) {
    this.idOrganismo = idOrganismo;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
}