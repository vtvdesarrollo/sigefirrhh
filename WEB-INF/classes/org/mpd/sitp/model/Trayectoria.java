package org.mpd.sitp.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Trayectoria
  implements Serializable
{
  public static String PREAPROBADO = "P";
  public static String APROBADO = "A";
  public static String RECHAZADO_SISTEMA = "S";
  public static String RECHAZADO_ANALISTA = "R";
  private TrayectoriaPK trayectoriaPK;
  private int cedula = 0;
  private String primerApellido;
  private String segundoApellido;
  private String primerNombre;
  private String segundoNombre;
  private int anioPreparacion;
  private Date fechaPreparacion;
  private int numeroMovimiento = 0;
  private String codOrganismo;
  private String nombreCorto;
  private String nombreOrganismo;
  private String estatus;
  private String estatusTrayectoria;
  private Date fechaEstatus;
  private Date fechaVigencia;
  private String codUbiGeografico;
  private String estado;
  private String codRegion;
  private String nombreRegion;
  private String codDependencia;
  private String nombreDependencia;
  private String codCausaMovimiento;
  private String descripcionMovimiento;
  private String codGrupoOrganismo;
  private String nombreCortoGrupo;
  private String nombreLargoGrupo;
  private String codManualCargo;
  private String codCargo;
  private String descripcionCargo;
  private int grado;
  private int paso = 1;
  private int codigoNomina;
  private double montoJubilacion = 0.0D;
  private double sueldoPromedio = 0.0D;
  private double sueldoBasico = 0.0D;
  private double compensacion = 0.0D;
  private double primaJerarquia = 0.0D;
  private double primaServicio = 0.0D;
  private double ajusteSueldo = 0.0D;
  private double otrosPagos = 0.0D;
  private double primasCargo = 0.0D;
  private double primasTrabajador = 0.0D;
  private String puntoCuenta;
  private Date fechaPuntoCuenta;
  private String codConcurso;
  private String observaciones;
  private String usuario;
  private double horas = 0.0D;
  private String numeroRemesa;

  public String getDescripcionEstatus()
  {
    if (this.estatusTrayectoria != null)
    {
      if (this.estatusTrayectoria.equals(PREAPROBADO))
        return "Preaprobado";
      if (this.estatusTrayectoria.equals(APROBADO))
        return "Aprobado";
      if (this.estatusTrayectoria.equals(RECHAZADO_ANALISTA))
        return "Rechazado por el analista";
      if (this.estatusTrayectoria.equals(RECHAZADO_SISTEMA)) {
        return "Rechazado por el sistema";
      }
    }
    return null;
  }

  public String getEstatusTrayectoria() {
    return this.estatusTrayectoria;
  }
  public void setEstatusTrayectoria(String estatusTrayectoria) {
    this.estatusTrayectoria = estatusTrayectoria;
  }
  public String getNumeroRemesa() {
    return this.numeroRemesa;
  }
  public void setNumeroRemesa(String numeroRemesa) {
    this.numeroRemesa = numeroRemesa;
  }
  public TrayectoriaPK getTrayectoriaPK() {
    return this.trayectoriaPK;
  }
  public void setTrayectoriaPK(TrayectoriaPK trayectoriaPK) {
    this.trayectoriaPK = trayectoriaPK;
  }
  public String toString() {
    return 
      new SimpleDateFormat("dd/MM/yyyy").format(this.fechaVigencia) + " - " + 
      this.descripcionMovimiento + " - " + this.descripcionCargo + " - " + this.nombreCorto;
  }
  public double getHoras() {
    return this.horas;
  }
  public void setHoras(double horas) {
    this.horas = horas;
  }

  public double getAjusteSueldo() {
    return this.ajusteSueldo;
  }
  public void setAjusteSueldo(double ajusteSueldo) {
    this.ajusteSueldo = ajusteSueldo;
  }
  public int getAnioPreparacion() {
    return this.anioPreparacion;
  }
  public void setAnioPreparacion(int anioPreparacion) {
    this.anioPreparacion = anioPreparacion;
  }

  public int getCedula() {
    return this.cedula;
  }
  public void setCedula(int cedula) {
    this.cedula = cedula;
  }

  public String getCodCargo() {
    return this.codCargo;
  }
  public void setCodCargo(String codCargo) {
    this.codCargo = codCargo;
  }
  public String getCodCausaMovimiento() {
    return this.codCausaMovimiento;
  }
  public void setCodCausaMovimiento(String codCausaMovimiento) {
    this.codCausaMovimiento = codCausaMovimiento;
  }
  public String getCodDependencia() {
    return this.codDependencia;
  }
  public void setCodDependencia(String codDependencia) {
    this.codDependencia = codDependencia;
  }

  public int getCodigoNomina() {
    return this.codigoNomina;
  }
  public void setCodigoNomina(int codigoNomina) {
    this.codigoNomina = codigoNomina;
  }
  public String getCodManualCargo() {
    return this.codManualCargo;
  }
  public void setCodManualCargo(String codManualCargo) {
    this.codManualCargo = codManualCargo;
  }
  public String getCodUbiGeografico() {
    return this.codUbiGeografico;
  }
  public void setCodUbiGeografico(String codUbiGeografico) {
    this.codUbiGeografico = codUbiGeografico;
  }
  public double getCompensacion() {
    return this.compensacion;
  }
  public void setCompensacion(double compensacion) {
    this.compensacion = compensacion;
  }

  public String getDescripcionCargo() {
    return this.descripcionCargo;
  }
  public void setDescripcionCargo(String descripcionCargo) {
    this.descripcionCargo = descripcionCargo;
  }
  public String getDescripcionMovimiento() {
    return this.descripcionMovimiento;
  }
  public void setDescripcionMovimiento(String descripcionMovimiento) {
    this.descripcionMovimiento = descripcionMovimiento;
  }
  public String getEstado() {
    return this.estado;
  }
  public void setEstado(String estado) {
    this.estado = estado;
  }
  public String getEstatus() {
    return this.estatus;
  }
  public void setEstatus(String estatus) {
    this.estatus = estatus;
  }
  public Date getFechaEstatus() {
    return this.fechaEstatus;
  }
  public void setFechaEstatus(Date fechaEstatus) {
    this.fechaEstatus = fechaEstatus;
  }
  public Date getFechaVigencia() {
    return this.fechaVigencia;
  }
  public void setFechaVigencia(Date fechaVigencia) {
    this.fechaVigencia = fechaVigencia;
  }
  public int getGrado() {
    return this.grado;
  }
  public void setGrado(int grado) {
    this.grado = grado;
  }

  public double getMontoJubilacion() {
    return this.montoJubilacion;
  }
  public void setMontoJubilacion(double montoJubilacion) {
    this.montoJubilacion = montoJubilacion;
  }
  public String getNombreCorto() {
    return this.nombreCorto;
  }
  public void setNombreCorto(String nombreCorto) {
    this.nombreCorto = nombreCorto;
  }
  public String getNombreCortoGrupo() {
    return this.nombreCortoGrupo;
  }
  public void setNombreCortoGrupo(String nombreCortoGrupo) {
    this.nombreCortoGrupo = nombreCortoGrupo;
  }
  public String getNombreDependencia() {
    return this.nombreDependencia;
  }
  public void setNombreDependencia(String nombreDependencia) {
    this.nombreDependencia = nombreDependencia;
  }
  public String getNombreLargoGrupo() {
    return this.nombreLargoGrupo;
  }
  public void setNombreLargoGrupo(String nombreLargoGrupo) {
    this.nombreLargoGrupo = nombreLargoGrupo;
  }
  public String getNombreOrganismo() {
    return this.nombreOrganismo;
  }
  public void setNombreOrganismo(String nombreOrganismo) {
    this.nombreOrganismo = nombreOrganismo;
  }
  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }

  public double getOtrosPagos() {
    return this.otrosPagos;
  }
  public void setOtrosPagos(double otrosPagos) {
    this.otrosPagos = otrosPagos;
  }

  public double getPrimaJerarquia()
  {
    return this.primaJerarquia;
  }
  public void setPrimaJerarquia(double primaJerarquia) {
    this.primaJerarquia = primaJerarquia;
  }
  public double getPrimasCargo() {
    return this.primasCargo;
  }
  public void setPrimasCargo(double primasCargo) {
    this.primasCargo = primasCargo;
  }
  public double getPrimaServicio() {
    return this.primaServicio;
  }
  public void setPrimaServicio(double primaServicio) {
    this.primaServicio = primaServicio;
  }
  public double getPrimasTrabajador() {
    return this.primasTrabajador;
  }
  public void setPrimasTrabajador(double primasTrabajador) {
    this.primasTrabajador = primasTrabajador;
  }
  public String getPrimerApellido() {
    return this.primerApellido;
  }
  public void setPrimerApellido(String primerApellido) {
    this.primerApellido = primerApellido;
  }
  public String getPrimerNombre() {
    return this.primerNombre;
  }
  public void setPrimerNombre(String primerNombre) {
    this.primerNombre = primerNombre;
  }
  public String getSegundoApellido() {
    return this.segundoApellido;
  }
  public void setSegundoApellido(String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }
  public String getSegundoNombre() {
    return this.segundoNombre;
  }
  public void setSegundoNombre(String segundoNombre) {
    this.segundoNombre = segundoNombre;
  }
  public double getSueldoBasico() {
    return this.sueldoBasico;
  }
  public void setSueldoBasico(double sueldoBasico) {
    this.sueldoBasico = sueldoBasico;
  }
  public double getSueldoPromedio() {
    return this.sueldoPromedio;
  }
  public void setSueldoPromedio(double sueldoPromedio) {
    this.sueldoPromedio = sueldoPromedio;
  }
  public String getCodConcurso() {
    return this.codConcurso;
  }
  public void setCodConcurso(String codConcurso) {
    this.codConcurso = codConcurso;
  }
  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public int getPaso() {
    return this.paso;
  }
  public void setPaso(int paso) {
    this.paso = paso;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }
  public String getUsuario() {
    return this.usuario;
  }
  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }
  public String getCodRegion() {
    return this.codRegion;
  }
  public void setCodRegion(String codRegion) {
    this.codRegion = codRegion;
  }
  public String getNombreRegion() {
    return this.nombreRegion;
  }
  public void setNombreRegion(String nombreRegion) {
    this.nombreRegion = nombreRegion;
  }

  public String getCodGrupoOrganismo() {
    return this.codGrupoOrganismo;
  }
  public void setCodGrupoOrganismo(String codGrupoOrganismo) {
    this.codGrupoOrganismo = codGrupoOrganismo;
  }
  public Date getFechaPreparacion() {
    return this.fechaPreparacion;
  }
  public void setFechaPreparacion(Date fechaPreparacion) {
    this.fechaPreparacion = fechaPreparacion;
  }

  public String getCodOrganismo() {
    return this.codOrganismo;
  }

  public void setCodOrganismo(String codOrganismo) {
    this.codOrganismo = codOrganismo;
  }
}