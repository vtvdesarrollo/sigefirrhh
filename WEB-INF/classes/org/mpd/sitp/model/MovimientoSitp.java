package org.mpd.sitp.model;

import java.io.Serializable;
import java.util.Date;

public class MovimientoSitp
  implements Serializable, Comparable
{
  public static String PREAPROBADO = "P";
  public static String APROBADO = "A";
  public static String RECHAZADO_SISTEMA = "S";
  public static String RECHAZADO_ANALISTA = "R";
  private MovimientoSitpPK movimientoSitpPK;
  private int cedula = 0;
  private String apellidosNombres;
  private String tipoPersonal = "1";
  private String nombreTipoPersonal;
  private String codCausaMovimiento;
  private String nombreCausaMovimiento;
  private Date fechaMovimiento;
  private Date fechaRegistro;
  private int codManualCargo;
  private String codCargo;
  private String codTabulador;
  private String descripcionCargo;
  private int codigoNomina = 0;
  private String codSede;
  private String nombreSede;
  private String codDependencia;
  private String nombreDependencia;
  private double sueldo = 0.0D;
  private double compensacion = 0.0D;
  private double primasCargo = 0.0D;
  private double primasTrabajador = 0.0D;
  private int grado = 1;
  private int paso = 1;
  private int numeroMovimiento;
  private String afectaSueldo = "S";
  private String localidad = "C";
  private String estatus = "0";
  private String documentoSoporte;
  private String codRegion;
  private String nombreRegion;
  private String observaciones;
  private String estatusMpd;
  private String codigoDevolucion;
  private String puntoCuenta;
  private Date fechaPuntoCuenta;
  private String codConcurso;
  private String analistaMpd;
  private Date fechaInicioMpd;
  private Date fechaFinMpd;
  private String observacionesMpd;
  private String codOrganismo;
  private Date fechaAccionAnalista;
  private String estatusMovimiento = "P";
  private boolean seleccionado;
  private String codigoRechazo;
  private String descripcionRechazo;
  private Date fechaCulminacion;
  private int anio;

  public Date getFechaCulminacion()
  {
    return this.fechaCulminacion;
  }
  public void setFechaCulminacion(Date fechaCulminacion) {
    this.fechaCulminacion = fechaCulminacion;
  }

  public String getCodigoRechazo() {
    return this.codigoRechazo;
  }
  public void setCodigoRechazo(String codigoRechazo) {
    this.codigoRechazo = codigoRechazo;
  }
  public String getDescripcionRechazo() {
    return this.descripcionRechazo;
  }
  public void setDescripcionRechazo(String descripcionRechazo) {
    this.descripcionRechazo = descripcionRechazo;
  }
  public boolean isSeleccionado() {
    return this.seleccionado;
  }
  public void setSeleccionado(boolean seleccionado) {
    this.seleccionado = seleccionado;
  }
  public Date getFechaAccionAnalista() {
    return this.fechaAccionAnalista;
  }
  public void setFechaAccionAnalista(Date fechaAccionAnalista) {
    this.fechaAccionAnalista = fechaAccionAnalista;
  }

  public String getEstatusMovimiento() {
    return this.estatusMovimiento;
  }
  public void setEstatusMovimiento(String estatusMovimiento) {
    this.estatusMovimiento = estatusMovimiento;
  }
  public int compareTo(Object object) {
    return toString().compareTo(object.toString());
  }
  public MovimientoSitpPK getMovimientoSitpPK() {
    return this.movimientoSitpPK;
  }
  public void setMovimientoSitpPK(MovimientoSitpPK movimientoSitpPK) {
    this.movimientoSitpPK = movimientoSitpPK;
  }
  public String getAfectaSueldo() {
    return this.afectaSueldo;
  }
  public String getAnalistaMpd() {
    return this.analistaMpd;
  }
  public String getApellidosNombres() {
    return this.apellidosNombres;
  }
  public int getCedula() {
    return this.cedula;
  }
  public String getCodCargo() {
    return this.codCargo;
  }
  public String getCodCausaMovimiento() {
    return this.codCausaMovimiento;
  }
  public String getCodConcurso() {
    return this.codConcurso;
  }
  public String getCodDependencia() {
    return this.codDependencia;
  }
  public String getCodigoDevolucion() {
    return this.codigoDevolucion;
  }
  public int getCodigoNomina() {
    return this.codigoNomina;
  }
  public int getCodManualCargo() {
    return this.codManualCargo;
  }
  public String getCodRegion() {
    return this.codRegion;
  }
  public String getCodSede() {
    return this.codSede;
  }
  public String getCodTabulador() {
    return this.codTabulador;
  }
  public double getCompensacion() {
    return this.compensacion;
  }
  public String getDescripcionCargo() {
    return this.descripcionCargo;
  }
  public String getDocumentoSoporte() {
    return this.documentoSoporte;
  }
  public String getEstatus() {
    return this.estatus;
  }
  public String getEstatusMpd() {
    return this.estatusMpd;
  }
  public Date getFechaFinMpd() {
    return this.fechaFinMpd;
  }
  public Date getFechaInicioMpd() {
    return this.fechaInicioMpd;
  }
  public Date getFechaMovimiento() {
    return this.fechaMovimiento;
  }
  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }
  public Date getFechaRegistro() {
    return this.fechaRegistro;
  }
  public int getGrado() {
    return this.grado;
  }
  public String getLocalidad() {
    return this.localidad;
  }

  public String getNombreCausaMovimiento() {
    return this.nombreCausaMovimiento;
  }
  public String getNombreDependencia() {
    return this.nombreDependencia;
  }
  public String getNombreRegion() {
    return this.nombreRegion;
  }
  public String getNombreSede() {
    return this.nombreSede;
  }
  public String getNombreTipoPersonal() {
    return this.nombreTipoPersonal;
  }
  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public String getObservacionesMpd() {
    return this.observacionesMpd;
  }
  public int getPaso() {
    return this.paso;
  }
  public double getPrimasCargo() {
    return this.primasCargo;
  }
  public double getPrimasTrabajador() {
    return this.primasTrabajador;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public double getSueldo() {
    return this.sueldo;
  }
  public String getTipoPersonal() {
    return this.tipoPersonal;
  }
  public void setAfectaSueldo(String afectaSueldo) {
    this.afectaSueldo = afectaSueldo;
  }
  public void setAnalistaMpd(String analistaMpd) {
    this.analistaMpd = analistaMpd;
  }
  public void setApellidosNombres(String apellidosNombres) {
    this.apellidosNombres = apellidosNombres;
  }
  public void setCedula(int cedula) {
    this.cedula = cedula;
  }
  public void setCodCargo(String codCargo) {
    this.codCargo = codCargo;
  }
  public void setCodCausaMovimiento(String codCausaMovimiento) {
    this.codCausaMovimiento = codCausaMovimiento;
  }
  public void setCodConcurso(String codConcurso) {
    this.codConcurso = codConcurso;
  }
  public void setCodDependencia(String codDependencia) {
    this.codDependencia = codDependencia;
  }
  public void setCodigoDevolucion(String codigoDevolucion) {
    this.codigoDevolucion = codigoDevolucion;
  }
  public void setCodigoNomina(int codigoNomina) {
    this.codigoNomina = codigoNomina;
  }
  public void setCodManualCargo(int codManualCargo) {
    this.codManualCargo = codManualCargo;
  }
  public void setCodRegion(String codRegion) {
    this.codRegion = codRegion;
  }
  public void setCodSede(String codSede) {
    this.codSede = codSede;
  }
  public void setCodTabulador(String codTabulador) {
    this.codTabulador = codTabulador;
  }
  public void setCompensacion(double compensacion) {
    this.compensacion = compensacion;
  }
  public void setDescripcionCargo(String descripcionCargo) {
    this.descripcionCargo = descripcionCargo;
  }
  public void setDocumentoSoporte(String documentoSoporte) {
    this.documentoSoporte = documentoSoporte;
  }
  public void setEstatus(String estatus) {
    this.estatus = estatus;
  }
  public void setEstatusMpd(String estatusMpd) {
    this.estatusMpd = estatusMpd;
  }
  public void setFechaFinMpd(Date fechaFinMpd) {
    this.fechaFinMpd = fechaFinMpd;
  }
  public void setFechaInicioMpd(Date fechaInicioMpd) {
    this.fechaInicioMpd = fechaInicioMpd;
  }
  public void setFechaMovimiento(Date fechaMovimiento) {
    this.fechaMovimiento = fechaMovimiento;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public void setFechaRegistro(Date fechaRegistro) {
    this.fechaRegistro = fechaRegistro;
  }
  public void setGrado(int grado) {
    this.grado = grado;
  }
  public void setLocalidad(String localidad) {
    this.localidad = localidad;
  }

  public void setNombreCausaMovimiento(String nombreCausaMovimiento) {
    this.nombreCausaMovimiento = nombreCausaMovimiento;
  }
  public void setNombreDependencia(String nombreDependencia) {
    this.nombreDependencia = nombreDependencia;
  }
  public void setNombreRegion(String nombreRegion) {
    this.nombreRegion = nombreRegion;
  }
  public void setNombreSede(String nombreSede) {
    this.nombreSede = nombreSede;
  }
  public void setNombreTipoPersonal(String nombreTipoPersonal) {
    this.nombreTipoPersonal = nombreTipoPersonal;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public void setObservacionesMpd(String observacionesMpd) {
    this.observacionesMpd = observacionesMpd;
  }
  public void setPaso(int paso) {
    this.paso = paso;
  }
  public void setPrimasCargo(double primasCargo) {
    this.primasCargo = primasCargo;
  }
  public void setPrimasTrabajador(double primasTrabajador) {
    this.primasTrabajador = primasTrabajador;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }
  public void setSueldo(double sueldo) {
    this.sueldo = sueldo;
  }
  public void setTipoPersonal(String tipoPersonal) {
    this.tipoPersonal = tipoPersonal;
  }
  public String getCodOrganismo() {
    return this.codOrganismo;
  }
  public void setCodOrganismo(String codOrganismo) {
    this.codOrganismo = codOrganismo;
  }
  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
}