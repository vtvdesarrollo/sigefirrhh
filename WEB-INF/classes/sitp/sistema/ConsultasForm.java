package sitp.sistema;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

public class ConsultasForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConsultasForm.class.getName());

  private int consulta = 0;
  private int edadMinima;
  private int edadMaxima;
  private int servicioMinimo;
  private int servicioMaximo;
  private double sueldoDesde;
  private double sueldoHasta;
  private Calendar fechaInicialEdad;
  private Calendar fechaFinalEdad;
  private Calendar fechaInicialServicio;
  private Calendar fechaFinalServicio;
  private String sexo = "T";
  private int concepto = 1;
  private java.util.Date fechaTope;
  private int anio = 1;
  private int cedula;
  private int mes = 0;
  private String cargo;
  private Collection resultado;
  private Collection total;
  private String[] arreglo;
  private int totalOrganismos = 100;

  public boolean isShowResultado() {
    return (this.resultado != null) && (!this.resultado.isEmpty());
  }
  public boolean isShowTotal() {
    return (this.total != null) && (!this.total.isEmpty());
  }
  public ConsultasForm() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.resultado = new ArrayList();
    this.total = new ArrayList();
    this.arreglo = new String[this.totalOrganismos];
    this.anio = Calendar.getInstance().get(1);
    this.mes = Calendar.getInstance().get(2);

    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      sql.append("select nspname from pg_namespace where nspname not like 'pg_%' and nspname not in ('information_schema','public') ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      int contador = 0;
      while (rsRegistros.next()) {
        this.arreglo[contador] = (rsRegistros.getString(1) + ".");
        contador++;
      }

      refresh();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException1) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException2) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException3)
        {
        }
    }
  }

  public void refresh()
  {
  }

  public String ejecutar()
    throws Exception
  {
    if (this.consulta == 0)
      consultaTrayectoria();
    else if (this.consulta == 1)
      consultaCantidadTrabajadores();
    else if (this.consulta == 2)
      consultaCantidadTrabajadoresPorCargo();
    else if (this.consulta == 3)
      consultaEdadServicioSexo();
    else if (this.consulta == 4)
      consultaCostoNomina();
    else if (this.consulta == 5)
      consultaCantidadTrabajadoresPorSueldo();
    else if (this.consulta == 6)
      consultaCargosVacantes99();
    else if (this.consulta == 7)
      consultaCargosVacantesCarrera();
    else if (this.consulta == 8)
      consultaCantidadTrabajadoresPorSueldoIntegral();
    else if (this.consulta == 9)
      consultaGastosConcepto();
    else if (this.consulta == 10) {
      consultaEstructuraCargos();
    }
    return null;
  }

  private String consultaCantidadTrabajadores()
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      sql.append("select id, organismo, cantidad, categoria, relacion from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select  cla.id_clasificacion_personal as id, max(org.nombre_organismo) as organismo, count(t.id_trabajador) as cantidad, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "trabajador t, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "clasificacionpersonal cla, ");
          sql.append(this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel, " + this.arreglo[contador] + "organismo org ");
          sql.append("where t.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_organismo = org.id_organismo ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and t.estatus = 'A' ");
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());
      sql.append(") as registros order by organismo,id");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      this.resultado = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(2));
        consulta.setCategoria(rsRegistros.getString(4));
        consulta.setRelacion(rsRegistros.getString(5));
        consulta.setCantidad(rsRegistros.getInt(3));
        this.resultado.add(consulta);
      }

      sql = new StringBuffer();

      sql.append("select id, sum(cantidad), max(categoria), max(relacion) from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select  cla.id_clasificacion_personal as id, count(t.id_trabajador) as cantidad, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "trabajador t, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "clasificacionpersonal cla, ");
          sql.append(this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel ");
          sql.append("where t.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and t.estatus = 'A' ");
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());
      sql.append(") as registros  group by id order by id");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();
      this.total = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setCategoria(rsRegistros.getString(3));
        consulta.setRelacion(rsRegistros.getString(4));
        consulta.setCantidad(rsRegistros.getInt(2));
        this.total.add(consulta);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6) {  }
 
    }
    return null;
  }

  private String consultaCantidadTrabajadoresPorSueldo()
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      sql.append("select id, organismo, cantidad, categoria, relacion from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select  cla.id_clasificacion_personal as id, max(org.nombre_organismo) as organismo, count(t.id_trabajador) as cantidad, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "trabajador t, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "clasificacionpersonal cla, ");
          sql.append(this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel, " + this.arreglo[contador] + "organismo org, " + this.arreglo[contador] + "sueldopromedio sp ");
          sql.append("where t.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_organismo = org.id_organismo ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and t.estatus = 'A' ");
          sql.append("and t.id_trabajador = sp.id_trabajador ");
          sql.append("and sp.promedio_sueldo between ? and ? ");
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(") as registros ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      int contador2 = 1;
      while (contador2 <= this.arreglo.length * 2) {
        if (this.arreglo[(contador2 - 1)] != null) {
          stRegistros.setDouble(contador2, this.sueldoDesde);
          stRegistros.setDouble(contador2 + 1, this.sueldoHasta);
          contador2++;
          contador2++;
        }
      }
      rsRegistros = stRegistros.executeQuery();

      this.resultado = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(2));
        consulta.setCategoria(rsRegistros.getString(4));
        consulta.setRelacion(rsRegistros.getString(5));
        consulta.setCantidad(rsRegistros.getInt(3));
        this.resultado.add(consulta);
      }

      sql = new StringBuffer();

      sql.append("select id, sum(cantidad), max(categoria), max(relacion) from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select  cla.id_clasificacion_personal as id,  count(t.id_trabajador) as cantidad, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "trabajador t, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "clasificacionpersonal cla, ");
          sql.append(this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel,  " + this.arreglo[contador] + "sueldopromedio sp ");
          sql.append("where t.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and t.estatus = 'A' ");
          sql.append("and t.id_trabajador = sp.id_trabajador ");
          sql.append("and sp.promedio_sueldo between ? and ? ");
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(") as registros  group by id order by id");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      contador2 = 1;
      while (contador2 <= this.arreglo.length * 2) {
        if (this.arreglo[(contador2 - 1)] != null) {
          stRegistros.setDouble(contador2, this.sueldoDesde);
          stRegistros.setDouble(contador2 + 1, this.sueldoHasta);
          contador2++;
          contador2++;
        }
      }

      rsRegistros = stRegistros.executeQuery();
      this.total = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setCategoria(rsRegistros.getString(3));
        consulta.setRelacion(rsRegistros.getString(4));
        consulta.setCantidad(rsRegistros.getInt(2));
        this.total.add(consulta);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6) {  }
 
    }
    return null;
  }

  private String consultaCantidadTrabajadoresPorSueldoIntegral()
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      sql.append("select id, organismo, cantidad, categoria, relacion from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select  cla.id_clasificacion_personal as id, max(org.nombre_organismo) as organismo, count(t.id_trabajador) as cantidad, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "trabajador t, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "clasificacionpersonal cla, ");
          sql.append(this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel, " + this.arreglo[contador] + "organismo org, " + this.arreglo[contador] + "sueldopromedio sp ");
          sql.append("where t.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_organismo = org.id_organismo ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and t.estatus = 'A' ");
          sql.append("and t.id_trabajador = sp.id_trabajador ");
          sql.append("and sp.promedio_integral between ? and ? ");
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(") as registros ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      int contador2 = 1;
      while (contador2 <= this.arreglo.length * 2) {
        if (this.arreglo[(contador2 - 1)] != null) {
          stRegistros.setDouble(contador2, this.sueldoDesde);
          stRegistros.setDouble(contador2 + 1, this.sueldoHasta);
          contador2++;
          contador2++;
        }

      }

      rsRegistros = stRegistros.executeQuery();

      this.resultado = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(2));
        consulta.setCategoria(rsRegistros.getString(4));
        consulta.setRelacion(rsRegistros.getString(5));
        consulta.setCantidad(rsRegistros.getInt(3));
        this.resultado.add(consulta);
      }

      sql = new StringBuffer();

      sql.append("select id, sum(cantidad), max(categoria), max(relacion) from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select  cla.id_clasificacion_personal as id,  count(t.id_trabajador) as cantidad, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "trabajador t, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "clasificacionpersonal cla, ");
          sql.append(this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel,  " + this.arreglo[contador] + "sueldopromedio sp ");
          sql.append("where t.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and t.estatus = 'A' ");
          sql.append("and t.id_trabajador = sp.id_trabajador ");
          sql.append("and sp.promedio_integral between ? and ? ");
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(") as registros  group by id order by id");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      contador2 = 1;
      while (contador2 <= this.arreglo.length * 2) {
        if (this.arreglo[(contador2 - 1)] != null) {
          stRegistros.setDouble(contador2, this.sueldoDesde);
          stRegistros.setDouble(contador2 + 1, this.sueldoHasta);
          contador2++;
          contador2++;
        }
      }

      rsRegistros = stRegistros.executeQuery();
      this.total = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setCategoria(rsRegistros.getString(3));
        consulta.setRelacion(rsRegistros.getString(4));
        consulta.setCantidad(rsRegistros.getInt(2));
        this.total.add(consulta);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6) {  }
 
    }
    return null;
  }

  private String consultaCantidadTrabajadoresPorCargo()
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      sql.append("select organismo, cargo, cantidad from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select max(org.nombre_organismo) as organismo, c.descripcion_cargo as cargo,count(t.id_trabajador) as cantidad ");
          sql.append("from " + this.arreglo[contador] + "trabajador t, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "cargo c, " + this.arreglo[contador] + "organismo org ");
          sql.append("where t.id_cargo = c.id_cargo ");
          sql.append("and c.cod_cargo = ? ");
          sql.append("and t.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = 1 ");
          sql.append("and tp.id_organismo = org.id_organismo ");
          sql.append("and t.estatus = 'A' ");
          sql.append("group by org.nombre_organismo, c.descripcion_cargo ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());
      sql.append(") as registros ");
      sql.append(" group by organismo, cargo, cantidad order by organismo, cargo ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      for (int contador = 1; contador <= this.totalOrganismos; contador++) {
        if (this.arreglo[(contador - 1)] != null) {
          stRegistros.setString(contador, this.cargo);
        }
      }

      rsRegistros = stRegistros.executeQuery();

      this.resultado = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(1));
        consulta.setDescripcionCargo(rsRegistros.getString(2));
        consulta.setCantidad(rsRegistros.getInt(3));
        this.resultado.add(consulta);
      }

      sql = new StringBuffer();

      sql.append("select cargo, sum(cantidad) from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select c.descripcion_cargo as cargo, count(t.id_trabajador) as cantidad ");
          sql.append("from " + this.arreglo[contador] + "trabajador t, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "cargo c ");
          sql.append("where t.id_cargo = c.id_cargo ");
          sql.append("and c.cod_cargo = ? ");
          sql.append("and t.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and t.estatus = 'A' ");
          sql.append("group by c.descripcion_cargo ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(") as registros ");
      sql.append(" group by cargo order by cargo ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      for (int contador = 1; contador <= this.totalOrganismos; contador++) {
        if (this.arreglo[(contador - 1)] != null) {
          stRegistros.setString(contador, this.cargo);
        }
      }

      rsRegistros = stRegistros.executeQuery();

      this.total = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setDescripcionCargo(rsRegistros.getString(1));
        consulta.setCantidad(rsRegistros.getInt(2));
        this.total.add(consulta);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6) {  }
 
    }
    return null;
  }

  private String consultaEdadServicioSexo()
  {
    this.fechaInicialEdad = Calendar.getInstance();
    this.fechaInicialEdad.setTime(this.fechaTope);
    this.fechaInicialEdad.add(1, -this.edadMaxima);
    this.fechaInicialEdad.add(5, 1);
    this.fechaFinalEdad = Calendar.getInstance();
    this.fechaFinalEdad.setTime(this.fechaTope);
    this.fechaFinalEdad.add(1, -this.edadMinima);
    this.fechaFinalEdad.add(5, 1);

    this.fechaInicialServicio = Calendar.getInstance();
    this.fechaInicialServicio.setTime(this.fechaTope);
    this.fechaInicialServicio.add(1, -this.servicioMaximo);
    this.fechaInicialServicio.add(5, 1);
    this.fechaFinalServicio = Calendar.getInstance();
    this.fechaFinalServicio.setTime(this.fechaTope);
    this.fechaFinalServicio.add(1, -this.servicioMinimo);
    this.fechaFinalServicio.add(5, 1);

    java.sql.Date fechaInicialEdadSql = new java.sql.Date(this.fechaInicialEdad.getTime().getYear(), this.fechaInicialEdad.getTime().getMonth(), this.fechaInicialEdad.getTime().getDate());
    java.sql.Date fechaFinalEdadSql = new java.sql.Date(this.fechaFinalEdad.getTime().getYear(), this.fechaFinalEdad.getTime().getMonth(), this.fechaFinalEdad.getTime().getDate());

    java.sql.Date fechaInicialServicioSql = new java.sql.Date(this.fechaInicialServicio.getTime().getYear(), this.fechaInicialServicio.getTime().getMonth(), this.fechaInicialServicio.getTime().getDate());
    java.sql.Date fechaFinalServicioSql = new java.sql.Date(this.fechaFinalServicio.getTime().getYear(), this.fechaFinalServicio.getTime().getMonth(), this.fechaFinalServicio.getTime().getDate());

    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      sql.append("select id, organismo, cantidad, categoria, relacion from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select  cla.id_clasificacion_personal as id, max(org.nombre_organismo) as organismo, count(t.id_trabajador) as cantidad, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "trabajador t, " + this.arreglo[contador] + "personal p, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "organismo org, ");
          sql.append(this.arreglo[contador] + "clasificacionpersonal cla, " + this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel ");
          sql.append("where t.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_relacion_personal not in (4,5)  ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and tp.id_organismo = org.id_organismo ");
          sql.append("and t.estatus = 'A' ");
          sql.append("and p.id_personal = t.id_personal ");
          sql.append("and p.fecha_nacimiento between ? and ? ");
          sql.append("and t.fecha_ingreso between ? and ? ");
          sql.append("and p.sexo = ? ");
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());
      sql.append(") as registros");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      int contador2 = 1;
      while (contador2 <= this.arreglo.length * 5) {
        if (this.arreglo[(contador2 - 1)] != null) {
          stRegistros.setDate(contador2, fechaInicialEdadSql);
          stRegistros.setDate(contador2 + 1, fechaFinalEdadSql);
          stRegistros.setDate(contador2 + 2, fechaInicialServicioSql);
          stRegistros.setDate(contador2 + 3, fechaFinalServicioSql);
          stRegistros.setString(contador2 + 4, this.sexo);
          contador2++;
          contador2++;
          contador2++;
          contador2++;
          contador2++;
        }

      }

      rsRegistros = stRegistros.executeQuery();

      this.resultado = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(2));
        consulta.setCantidad(rsRegistros.getInt(3));
        consulta.setCategoria(rsRegistros.getString(4));
        consulta.setRelacion(rsRegistros.getString(5));

        this.resultado.add(consulta);
      }

      sql = new StringBuffer();

      sql.append("select id, sum(cantidad) as cantidad, max(categoria) as categoria, max(relacion) as relacion from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select  cla.id_clasificacion_personal as id, count(t.id_trabajador) as cantidad, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "trabajador t, " + this.arreglo[contador] + "personal p, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "clasificacionpersonal cla, ");
          sql.append(this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel ");
          sql.append("where t.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_relacion_personal not in (4,5)  ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and t.estatus = 'A' ");
          sql.append("and p.id_personal = t.id_personal ");
          sql.append("and p.fecha_nacimiento between ? and ? ");
          sql.append("and t.fecha_ingreso between ? and ? ");
          sql.append("and p.sexo = ? ");
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(") as registros ");
      sql.append("group by id ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      contador2 = 1;
      while (contador2 <= this.arreglo.length * 5) {
        if (this.arreglo[(contador2 - 1)] != null) {
          stRegistros.setDate(contador2, fechaInicialEdadSql);
          stRegistros.setDate(contador2 + 1, fechaFinalEdadSql);
          stRegistros.setDate(contador2 + 2, fechaInicialServicioSql);
          stRegistros.setDate(contador2 + 3, fechaFinalServicioSql);
          stRegistros.setString(contador2 + 4, this.sexo);
          contador2++;
          contador2++;
          contador2++;
          contador2++;
          contador2++;
        }
      }
      rsRegistros = stRegistros.executeQuery();

      this.total = new ArrayList();

      while (rsRegistros.next())
      {
        Consulta consulta = new Consulta();
        consulta.setCantidad(rsRegistros.getInt(2));
        consulta.setCategoria(rsRegistros.getString(3));
        consulta.setRelacion(rsRegistros.getString(4));

        this.total.add(consulta);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6) {  }
 
    }
    return null;
  }

  private String consultaCargosVacantes99()
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      sql.append("select organismo,  sum(cantidad) from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select max(org.nombre_organismo) as organismo, count(rc.id_registro_cargos) as cantidad ");
          sql.append("from " + this.arreglo[contador] + "registrocargos rc, " + this.arreglo[contador] + "registro r, " + this.arreglo[contador] + "cargo c, " + this.arreglo[contador] + "organismo org ," + this.arreglo[contador] + "registropersonal rp, " + this.arreglo[contador] + "tipopersonal tp ");
          sql.append("where rc.id_cargo = c.id_cargo ");
          sql.append("and rc.id_registro = r.id_registro ");
          sql.append("and rp.id_registro = r.id_registro ");
          sql.append("and tp.id_tipo_personal = rp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = 1 ");
          sql.append("and r.id_organismo = org.id_organismo ");
          sql.append("and rc.situacion = 'V' and c.grado = 99 ");
          sql.append("group by org.nombre_organismo ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());
      sql.append(") as registros ");
      sql.append(" group by organismo order by organismo ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      this.total = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(1));
        consulta.setCantidad(rsRegistros.getInt(2));
        this.total.add(consulta);
      }

      sql = new StringBuffer();

      sql.append("select organismo, cargo, sum(cantidad) from ( ");
      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select max(org.nombre_organismo) as organismo, c.descripcion_cargo as cargo,count(rc.id_registro_cargos) as cantidad ");
          sql.append("from " + this.arreglo[contador] + "registrocargos rc, " + this.arreglo[contador] + "registro r, " + this.arreglo[contador] + "cargo c, " + this.arreglo[contador] + "organismo org ," + this.arreglo[contador] + "registropersonal rp, " + this.arreglo[contador] + "tipopersonal tp ");
          sql.append("where rc.id_cargo = c.id_cargo ");
          sql.append("and rc.id_registro = r.id_registro ");
          sql.append("and rp.id_registro = r.id_registro ");
          sql.append("and tp.id_tipo_personal = rp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = 1 ");
          sql.append("and r.id_organismo = org.id_organismo ");
          sql.append("and rc.situacion = 'V' and c.grado = 99 ");
          sql.append("group by org.nombre_organismo, c.descripcion_cargo ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(" ) as registros ");
      sql.append(" group by organismo, cargo order by organismo, cargo ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      this.resultado = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(1));
        consulta.setDescripcionCargo(rsRegistros.getString(2));
        consulta.setCantidad(rsRegistros.getInt(3));
        this.resultado.add(consulta);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6) {  }
 
    }
    return null;
  }

  private String consultaCargosVacantesCarrera()
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      sql.append("select organismo,  sum(cantidad) from ( ");
      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select max(org.nombre_organismo) as organismo, count(rc.id_registro_cargos) as cantidad ");
          sql.append("from " + this.arreglo[contador] + "registrocargos rc, " + this.arreglo[contador] + "registro r, " + this.arreglo[contador] + "cargo c, " + this.arreglo[contador] + "organismo org ," + this.arreglo[contador] + "registropersonal rp, " + this.arreglo[contador] + "tipopersonal tp ");
          sql.append("where rc.id_cargo = c.id_cargo ");
          sql.append("and rc.id_registro = r.id_registro ");
          sql.append("and rp.id_registro = r.id_registro ");
          sql.append("and tp.id_tipo_personal = rp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = 1 ");
          sql.append("and r.id_organismo = org.id_organismo ");
          sql.append("and rc.situacion = 'V' and c.grado <> 99 ");
          sql.append("group by org.nombre_organismo ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(") as registros ");
      sql.append(" group by organismo order by organismo ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      this.total = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(1));
        consulta.setCantidad(rsRegistros.getInt(2));
        this.total.add(consulta);
      }

      sql = new StringBuffer();

      sql.append("select organismo, cargo, sum(cantidad) from ( ");
      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select max(org.nombre_organismo) as organismo, c.descripcion_cargo as cargo,count(rc.id_registro_cargos) as cantidad ");
          sql.append("from " + this.arreglo[contador] + "registrocargos rc, " + this.arreglo[contador] + "registro r, " + this.arreglo[contador] + "cargo c, " + this.arreglo[contador] + "organismo org ," + this.arreglo[contador] + "registropersonal rp, " + this.arreglo[contador] + "tipopersonal tp ");
          sql.append("where rc.id_cargo = c.id_cargo ");
          sql.append("and rc.id_registro = r.id_registro ");
          sql.append("and rp.id_registro = r.id_registro ");
          sql.append("and tp.id_tipo_personal = rp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = 1 ");
          sql.append("and r.id_organismo = org.id_organismo ");
          sql.append("and rc.situacion = 'V' and c.grado <> 99 ");
          sql.append("group by org.nombre_organismo, c.descripcion_cargo ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());
      sql.append(") as registros ");
      sql.append(" group by organismo, cargo order by organismo, cargo ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      this.resultado = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(1));
        consulta.setDescripcionCargo(rsRegistros.getString(2));
        consulta.setCantidad(rsRegistros.getInt(3));
        this.resultado.add(consulta);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6) {  }
 
    }
    return null;
  }

  private String consultaEstructuraCargos()
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      sql.append("select organismo,  sum(cantidad) from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select max(org.nombre_organismo) as organismo, count(rc.id_registro_cargos) as cantidad ");
          sql.append("from " + this.arreglo[contador] + "registrocargos rc, " + this.arreglo[contador] + "registro r, " + this.arreglo[contador] + "cargo c, " + this.arreglo[contador] + "organismo org ," + this.arreglo[contador] + "registropersonal rp, " + this.arreglo[contador] + "tipopersonal tp ");
          sql.append("where rc.id_cargo = c.id_cargo ");
          sql.append("and rc.id_registro = r.id_registro ");
          sql.append("and rp.id_registro = r.id_registro ");
          sql.append("and tp.id_tipo_personal = rp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = 1 ");
          sql.append("and r.id_organismo = org.id_organismo ");
          sql.append("group by org.nombre_organismo ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());
      sql.append(" ) as registros ");
      sql.append(" group by organismo order by organismo ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      this.total = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(1));
        consulta.setCantidad(rsRegistros.getInt(2));
        this.total.add(consulta);
      }

      sql = new StringBuffer();

      sql.append("select organismo,  situacion, sum(cantidad) from ( ");
      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select max(org.nombre_organismo) as organismo, rc.situacion as situacion,count(rc.id_registro_cargos) as cantidad ");
          sql.append("from " + this.arreglo[contador] + "registrocargos rc, " + this.arreglo[contador] + "registro r, " + this.arreglo[contador] + "cargo c, " + this.arreglo[contador] + "organismo org ," + this.arreglo[contador] + "registropersonal rp," + this.arreglo[contador] + "tipopersonal tp ");
          sql.append("where rc.id_cargo = c.id_cargo ");
          sql.append("and rc.id_registro = r.id_registro ");
          sql.append("and rp.id_registro = r.id_registro ");
          sql.append("and tp.id_tipo_personal = rp.id_tipo_personal ");
          sql.append("and tp.id_clasificacion_personal = 1 ");
          sql.append("and r.id_organismo = org.id_organismo ");
          sql.append("group by org.nombre_organismo, rc.situacion ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());
      sql.append(" ) as registros ");
      sql.append(" group by organismo, situacion order by organismo, situacion ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      this.resultado = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(1));
        consulta.setMovimiento(rsRegistros.getString(2));
        consulta.setCantidad(rsRegistros.getInt(3));
        this.resultado.add(consulta);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6) {  }
 
    }
    return null;
  }

  private String consultaTrayectoria()
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();
      sql.append("select nombre,organismo from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select max(primer_apellido||' '||segundo_apellido||', '||primer_nombre||' '||segundo_nombre)  as nombre,max(o.nombre_organismo) as organismo from " + this.arreglo[contador] + "personal  p, " + this.arreglo[contador] + "personalorganismo po, " + this.arreglo[contador] + "organismo o ");
          sql.append("where p.cedula = ?  and p.id_personal = po.id_personal and po.id_organismo = o.id_organismo ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());
      sql.append(") as registros ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          stRegistros.setInt(contador + 1, this.cedula);
        }
      }
      rsRegistros = stRegistros.executeQuery();

      this.total = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setNombre(rsRegistros.getString(1));
        consulta.setOrganismo(rsRegistros.getString(2));

        this.total.add(consulta);
      }

      sql = new StringBuffer();
      sql.append("select organismo,movimiento,cargo,fecha from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select nombre_organismo as organismo, descripcion_movimiento as movimiento, descripcion_cargo as cargo, fecha_vigencia as fecha  from " + this.arreglo[contador] + "trayectoria t  ");
          sql.append("where t.cedula = ? ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());
      sql.append(") as registros order by fecha");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          stRegistros.setInt(contador + 1, this.cedula);
        }

      }

      rsRegistros = stRegistros.executeQuery();

      this.resultado = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(1));
        consulta.setMovimiento(rsRegistros.getString(2));
        consulta.setDescripcionCargo(rsRegistros.getString(3));
        consulta.setFecha(rsRegistros.getDate(4));
        this.resultado.add(consulta);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6) {  }
 
    }
    return null;
  }

  private String consultaCostoNomina()
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      sql.append("select id, organismo, monto, categoria, relacion from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select cla.id_clasificacion_personal as id, max(org.nombre_organismo) as organismo, sum(h.monto_asigna) as monto, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "v_historico h, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "clasificacionpersonal cla, ");
          sql.append(this.arreglo[contador] + "conceptotipopersonal ctp, " + this.arreglo[contador] + "concepto co, ");
          sql.append(this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel, " + this.arreglo[contador] + "organismo org ");
          sql.append("where h.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_organismo = org.id_organismo ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and h.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal ");
          sql.append("and ctp.id_concepto = co.id_concepto ");
          sql.append("and co.cod_concepto < '5000' ");
          sql.append("and h.anio = ? ");
          if (this.mes != 0)
            sql.append("and h.mes = ? ");
          else {
            sql.append("and h.mes > ? ");
          }
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(") as registros order by organismo,id");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      int contador2 = 1;
      while (contador2 <= this.arreglo.length * 2) {
        if (this.arreglo[(contador2 - 1)] != null) {
          stRegistros.setInt(contador2, this.anio);
          stRegistros.setInt(contador2 + 1, this.mes);
          contador2++;
          contador2++;
        }

      }

      rsRegistros = stRegistros.executeQuery();

      this.resultado = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setOrganismo(rsRegistros.getString(2));
        consulta.setMonto(rsRegistros.getDouble(3));
        consulta.setCategoria(rsRegistros.getString(4));
        consulta.setRelacion(rsRegistros.getString(5));
        this.resultado.add(consulta);
      }

      sql = new StringBuffer();

      sql.append("select id, sum(monto), max(categoria), max(relacion) from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select cla.id_clasificacion_personal as id,  sum(h.monto_asigna) as monto, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "v_historico h, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "clasificacionpersonal cla, ");
          sql.append(this.arreglo[contador] + "conceptotipopersonal ctp, " + this.arreglo[contador] + "concepto co, ");
          sql.append(this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel, " + this.arreglo[contador] + "organismo org ");
          sql.append("where h.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_organismo = org.id_organismo ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and h.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal ");
          sql.append("and ctp.id_concepto = co.id_concepto ");
          sql.append("and co.cod_concepto <'5000' ");
          sql.append("and h.anio = ? ");
          if (this.mes != 0)
            sql.append("and h.mes = ? ");
          else {
            sql.append("and h.mes > ? ");
          }
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(") as registros group by id  order by id");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      contador2 = 1;
      while (contador2 <= this.arreglo.length * 2) {
        if (this.arreglo[(contador2 - 1)] != null) {
          stRegistros.setInt(contador2, this.anio);
          stRegistros.setInt(contador2 + 1, this.mes);
          contador2++;
          contador2++;
        }
      }

      rsRegistros = stRegistros.executeQuery();

      this.total = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setCategoria(rsRegistros.getString(3));
        consulta.setRelacion(rsRegistros.getString(4));
        consulta.setMonto(rsRegistros.getDouble(2));
        this.total.add(consulta);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6) {  }
 
    }
    return null;
  }

  private String consultaGastosConcepto()
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();
      sql.append("select id, concepto, sum(monto), max(categoria), max(relacion) from ( ");

      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select cla.id_clasificacion_personal as id, max(co.descripcion) as concepto, sum(h.monto_asigna+h.monto_deduce) as monto, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "v_historico h, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "clasificacionpersonal cla, ");
          sql.append(this.arreglo[contador] + "conceptotipopersonal ctp, " + this.arreglo[contador] + "concepto co, ");
          sql.append(this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel, " + this.arreglo[contador] + "organismo org ");
          sql.append("where h.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_organismo = org.id_organismo ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and h.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal ");
          sql.append("and ctp.id_concepto = co.id_concepto ");
          sql.append("and co.cod_concepto <> '5000' ");
          if (this.concepto == 1)
            sql.append("and (co.sueldo_basico ='S' or co.ajuste_sueldo ='S')  and co.cod_concepto <> '1500'  and co.cod_concepto <> '1600' and co.cod_concepto < '5000' ");
          else if (this.concepto == 2)
            sql.append("and co.compensacion ='S'  and co.cod_concepto <> '1500'  and co.cod_concepto <> '1600' and co.cod_concepto < '5000' ");
          else if (this.concepto == 3)
            sql.append("and co.primas_cargo ='S'   and co.cod_concepto <> '1500'  and co.cod_concepto <> '1600' and co.cod_concepto < '5000' ");
          else if (this.concepto == 4)
            sql.append("and  co.primas_trabajador ='S'  and co.cod_concepto <> '1500'  and co.cod_concepto <> '1600'  and co.cod_concepto < '5000' ");
          else if (this.concepto == 5)
            sql.append("and (co.sueldo_basico <>'S' and co.ajuste_sueldo <>'S' and co.compensacion <>'S' and co.primas_cargo <>'S' and co.primas_trabajador <>'S' and co.cod_concepto <> '1500'  and co.cod_concepto <> '1600' and co.cod_concepto < '5000') ");
          else if (this.concepto == 6)
            sql.append("and co.cod_concepto = '1500' ");
          else if (this.concepto == 7)
            sql.append("and co.cod_concepto = '1600' ");
          else if (this.concepto == 8) {
            sql.append("and co.cod_concepto > '4999' ");
          }
          sql.append("and h.anio = ? ");
          if (this.mes != 0)
            sql.append("and h.mes = ? ");
          else {
            sql.append("and h.mes > ? ");
          }
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(") as registros group by id,concepto order by concepto,id");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      int contador2 = 1;
      while (contador2 <= this.arreglo.length * 2) {
        if (this.arreglo[(contador2 - 1)] != null) {
          stRegistros.setInt(contador2, this.anio);
          stRegistros.setInt(contador2 + 1, this.mes);
          contador2++;
          contador2++;
        }

      }

      rsRegistros = stRegistros.executeQuery();

      this.total = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setDescripcionConcepto(rsRegistros.getString(2));
        consulta.setMonto(rsRegistros.getDouble(3));
        consulta.setCategoria(rsRegistros.getString(4));
        consulta.setRelacion(rsRegistros.getString(5));
        this.total.add(consulta);
      }

      sql = new StringBuffer();
      sql.append("select id, concepto,organismo, sum(monto), max(categoria), max(relacion) from ( ");
      for (int contador = 0; contador < this.totalOrganismos; contador++) {
        if (this.arreglo[contador] != null) {
          sql.append("select cla.id_clasificacion_personal as id, max(org.nombre_organismo) as organismo, max(co.descripcion) as concepto, sum(h.monto_asigna+h.monto_deduce) as monto, ");
          sql.append("max(cat.desc_categoria) as categoria, max(rel.desc_relacion) as relacion ");
          sql.append("from " + this.arreglo[contador] + "v_historico h, " + this.arreglo[contador] + "tipopersonal tp, " + this.arreglo[contador] + "clasificacionpersonal cla, ");
          sql.append(this.arreglo[contador] + "conceptotipopersonal ctp, " + this.arreglo[contador] + "concepto co, ");
          sql.append(this.arreglo[contador] + "categoriapersonal cat, " + this.arreglo[contador] + "relacionpersonal rel, " + this.arreglo[contador] + "organismo org ");
          sql.append("where h.id_tipo_personal = tp.id_tipo_personal ");
          sql.append("and tp.id_organismo = org.id_organismo ");
          sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
          sql.append("and cla.id_categoria_personal = cat.id_categoria_personal ");
          sql.append("and cla.id_relacion_personal = rel.id_relacion_personal ");
          sql.append("and h.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal ");
          sql.append("and ctp.id_concepto = co.id_concepto ");
          sql.append("and co.cod_concepto <> '5000' ");
          if (this.concepto == 1)
            sql.append("and (co.sueldo_basico ='S' or co.ajuste_sueldo ='S')  and co.cod_concepto <> '1500'  and co.cod_concepto <> '1600' and co.cod_concepto < '5000' ");
          else if (this.concepto == 2)
            sql.append("and co.compensacion ='S'  and co.cod_concepto <> '1500'  and co.cod_concepto <> '1600' and co.cod_concepto < '5000' ");
          else if (this.concepto == 3)
            sql.append("and co.primas_cargo ='S'   and co.cod_concepto <> '1500'  and co.cod_concepto <> '1600' and co.cod_concepto < '5000' ");
          else if (this.concepto == 4)
            sql.append("and  co.primas_trabajador ='S'  and co.cod_concepto <> '1500'  and co.cod_concepto <> '1600'  and co.cod_concepto < '5000' ");
          else if (this.concepto == 5)
            sql.append("and (co.sueldo_basico <>'S' and co.ajuste_sueldo <>'S' and co.compensacion <>'S' and co.primas_cargo <>'S' and co.primas_trabajador <>'S' and co.cod_concepto <> '1500'  and co.cod_concepto <> '1600' and co.cod_concepto < '5000') ");
          else if (this.concepto == 6)
            sql.append("and co.cod_concepto = '1500' ");
          else if (this.concepto == 7)
            sql.append("and co.cod_concepto = '1600' ");
          else if (this.concepto == 8) {
            sql.append("and co.cod_concepto > '4999' ");
          }
          sql.append("and h.anio = ? ");
          if (this.mes != 0)
            sql.append("and h.mes = ? ");
          else {
            sql.append("and h.mes > ? ");
          }
          sql.append("group by cla.id_clasificacion_personal ");
          sql.append(" union ");
        }
      }
      sql.delete(sql.length() - 6, sql.length());

      sql.append(") as registros group by id,organismo,concepto order by organismo,id,concepto");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      contador2 = 1;
      while (contador2 <= this.arreglo.length * 2) {
        if (this.arreglo[(contador2 - 1)] != null) {
          stRegistros.setInt(contador2, this.anio);
          stRegistros.setInt(contador2 + 1, this.mes);
          contador2++;
          contador2++;
        }

      }

      rsRegistros = stRegistros.executeQuery();

      this.resultado = new ArrayList();
      while (rsRegistros.next()) {
        Consulta consulta = new Consulta();
        consulta.setDescripcionConcepto(rsRegistros.getString(2));
        consulta.setOrganismo(rsRegistros.getString(3));
        consulta.setCategoria(rsRegistros.getString(5));
        consulta.setRelacion(rsRegistros.getString(6));
        consulta.setMonto(rsRegistros.getDouble(4));
        this.resultado.add(consulta);
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6) {  }
 
    }
    return null;
  }

  public int getConsulta()
  {
    return this.consulta;
  }

  public void setConsulta(int consulta)
  {
    this.consulta = consulta;
  }

  public int getEdadMaxima()
  {
    return this.edadMaxima;
  }

  public void setEdadMaxima(int edadMaxima)
  {
    this.edadMaxima = edadMaxima;
  }

  public int getEdadMinima()
  {
    return this.edadMinima;
  }

  public void setEdadMinima(int edadMinima)
  {
    this.edadMinima = edadMinima;
  }

  public Calendar getFechaFinalEdad()
  {
    return this.fechaFinalEdad;
  }

  public void setFechaFinalEdad(Calendar fechaFinalEdad)
  {
    this.fechaFinalEdad = fechaFinalEdad;
  }

  public Calendar getFechaFinalServicio()
  {
    return this.fechaFinalServicio;
  }

  public void setFechaFinalServicio(Calendar fechaFinalServicio)
  {
    this.fechaFinalServicio = fechaFinalServicio;
  }

  public Calendar getFechaInicialEdad()
  {
    return this.fechaInicialEdad;
  }

  public void setFechaInicialEdad(Calendar fechaInicialEdad)
  {
    this.fechaInicialEdad = fechaInicialEdad;
  }

  public Calendar getFechaInicialServicio()
  {
    return this.fechaInicialServicio;
  }

  public void setFechaInicialServicio(Calendar fechaInicialServicio)
  {
    this.fechaInicialServicio = fechaInicialServicio;
  }

  public java.util.Date getFechaTope()
  {
    return this.fechaTope;
  }

  public void setFechaTope(java.util.Date fechaTope)
  {
    this.fechaTope = fechaTope;
  }

  public Collection getResultado()
  {
    return this.resultado;
  }

  public void setResultado(Collection resultado)
  {
    this.resultado = resultado;
  }

  public int getServicioMaximo()
  {
    return this.servicioMaximo;
  }

  public void setServicioMaximo(int servicioMaximo)
  {
    this.servicioMaximo = servicioMaximo;
  }

  public int getServicioMinimo()
  {
    return this.servicioMinimo;
  }

  public void setServicioMinimo(int servicioMinimo)
  {
    this.servicioMinimo = servicioMinimo;
  }

  public String getSexo()
  {
    return this.sexo;
  }

  public void setSexo(String sexo)
  {
    this.sexo = sexo;
  }

  public Collection getTotal() {
    return this.total;
  }

  public void setTotal(Collection total) {
    this.total = total;
  }
  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public String getCargo() {
    return this.cargo;
  }
  public void setCargo(String cargo) {
    this.cargo = cargo;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }

  public double getSueldoDesde() {
    return this.sueldoDesde;
  }
  public void setSueldoDesde(double sueldoDesde) {
    this.sueldoDesde = sueldoDesde;
  }
  public double getSueldoHasta() {
    return this.sueldoHasta;
  }
  public void setSueldoHasta(double sueldoHasta) {
    this.sueldoHasta = sueldoHasta;
  }
  public int getCedula() {
    return this.cedula;
  }
  public void setCedula(int cedula) {
    this.cedula = cedula;
  }

  public int getConcepto() {
    return this.concepto;
  }
  public void setConcepto(int concepto) {
    this.concepto = concepto;
  }
}