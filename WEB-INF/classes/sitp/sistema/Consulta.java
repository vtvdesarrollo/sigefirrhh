package sitp.sistema;

import java.util.Date;

public class Consulta
{
  private String organismo;
  private String relacion;
  private String categoria;
  private String descripcionCargo;
  private String descripcionConcepto;
  private int cantidad;
  private double monto;
  private String movimiento;
  private Date fecha;
  private String nombre;

  public int getCantidad()
  {
    return this.cantidad;
  }
  public void setCantidad(int cantidad) {
    this.cantidad = cantidad;
  }
  public String getCategoria() {
    return this.categoria;
  }
  public void setCategoria(String categoria) {
    this.categoria = categoria;
  }
  public double getMonto() {
    return this.monto;
  }
  public void setMonto(double monto) {
    this.monto = monto;
  }
  public String getOrganismo() {
    return this.organismo;
  }
  public void setOrganismo(String organismo) {
    this.organismo = organismo;
  }
  public String getRelacion() {
    return this.relacion;
  }
  public void setRelacion(String relacion) {
    this.relacion = relacion;
  }

  public String getDescripcionCargo() {
    return this.descripcionCargo;
  }
  public void setDescripcionCargo(String descripcionCargo) {
    this.descripcionCargo = descripcionCargo;
  }
  public Date getFecha() {
    return this.fecha;
  }
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }
  public String getMovimiento() {
    return this.movimiento;
  }
  public void setMovimiento(String movimiento) {
    this.movimiento = movimiento;
  }
  public String getNombre() {
    return this.nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  public String getDescripcionConcepto() {
    return this.descripcionConcepto;
  }
  public void setDescripcionConcepto(String descripcionConcepto) {
    this.descripcionConcepto = descripcionConcepto;
  }
}