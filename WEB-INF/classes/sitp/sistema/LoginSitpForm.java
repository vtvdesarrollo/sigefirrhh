package sitp.sistema;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

public class LoginSitpForm
{
  static Logger log = Logger.getLogger(LoginSitpForm.class.getName());
  private String schema;

  public String getSchema()
  {
    return this.schema;
  }

  public void setSchema(String schema) {
    this.schema = schema;
  }

  public String organismo() {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    ExternalContext externalContext = facesContext.getExternalContext();
    try
    {
      ((HttpSession)externalContext.getSession(true)).removeAttribute("descriptor");
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return "organismo";
  }

  public String consolidado()
  {
    return "consolidado";
  }

  public LoginSitpForm() {
    refresh();
  }

  public void refresh() {
    setSchema(null);
  }
}