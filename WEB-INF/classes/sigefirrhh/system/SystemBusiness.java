package sigefirrhh.system;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class SystemBusiness extends AbstractBusiness
  implements Serializable
{
  private SystemBeanBusiness systemBeanBusiness = new SystemBeanBusiness();

  public void addSystem(System system)
    throws Exception
  {
    this.systemBeanBusiness.addSystem(system);
  }

  public void updateSystem(System system) throws Exception {
    this.systemBeanBusiness.updateSystem(system);
  }

  public void deleteSystem(System system) throws Exception {
    this.systemBeanBusiness.deleteSystem(system);
  }

  public System findSystemById(long systemId) throws Exception {
    return this.systemBeanBusiness.findSystemById(systemId);
  }

  public Collection findAllSystem() throws Exception {
    return this.systemBeanBusiness.findSystemAll();
  }
}