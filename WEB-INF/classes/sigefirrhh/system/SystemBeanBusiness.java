package sigefirrhh.system;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SystemBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSystem(System system)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    System systemNew = 
      (System)BeanUtils.cloneBean(
      system);

    pm.makePersistent(systemNew);
  }

  public void updateSystem(System system) throws Exception
  {
    System systemModify = 
      findSystemById(system.getIdSystem());

    BeanUtils.copyProperties(systemModify, system);
  }

  public void deleteSystem(System system) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    System systemDelete = 
      findSystemById(system.getIdSystem());
    pm.deletePersistent(systemDelete);
  }

  public System findSystemById(long idSystem) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSystem == pIdSystem";
    Query query = pm.newQuery(System.class, filter);

    query.declareParameters("long pIdSystem");

    parameters.put("pIdSystem", new Long(idSystem));

    Collection colSystem = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSystem.iterator();
    return (System)iterator.next();
  }

  public Collection findSystemAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent systemExtent = pm.getExtent(
      System.class, true);
    Query query = pm.newQuery(systemExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}