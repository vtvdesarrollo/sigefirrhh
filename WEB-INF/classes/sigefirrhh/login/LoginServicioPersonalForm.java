package sigefirrhh.login;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;

public class LoginServicioPersonalForm
{
  static Logger log = Logger.getLogger(LoginServicioPersonalForm.class.getName());
  private int cedula;
  private String password;
  private String passwordConfirm;
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private TrabajadorNoGenFacade trabajadorNoGenFacade = new TrabajadorNoGenFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private int anioInicio;
  private Date fechaNacimiento;

  public String send()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Collection colTrabajador = null;
    try {
      colTrabajador = this.trabajadorNoGenFacade.findTrabajadorByPassword(
        this.cedula, this.password);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al accesar la base de datos", ""));
      return null;
    }

    Trabajador trabajador = null;

    if ((colTrabajador != null) && (!colTrabajador.isEmpty())) {
      trabajador = (Trabajador)colTrabajador.iterator().next();
      LoginSession session = 
        (LoginSession)context.getApplication().getVariableResolver().resolveVariable(
        context, 
        "loginSession");
      session.setOrganismo(trabajador.getOrganismo());
      session.setUsuario(String.valueOf(trabajador.getPersonal().getCedula()));
      session.setServicioPersonal(true);
      session.setAdministrador("N");
      session.setAgregar(true);
      session.setModificar(true);
      session.setEliminar(true);
      session.setConsultar(true);
      session.setEjecutar(true);
      ExternalContext externalContext = context.getExternalContext();
      HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
      try {
        externalContext.redirect("home.jsf");
      } catch (Exception localException1) {
      }
      context.responseComplete();
      return "success";
    }
    context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario y/o la contraseña no son validos", ""));

    return null;
  }

  public String sendRegistro() {
    FacesContext context = FacesContext.getCurrentInstance();
    Collection colTrabajador = null;
    try {
      colTrabajador = this.trabajadorNoGenFacade.findTrabajadorForRegistro(
        this.cedula, this.anioInicio, this.fechaNacimiento);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al accesar la base de datos", ""));
      return null;
    }

    Trabajador trabajador = null;

    if ((colTrabajador != null) && (!colTrabajador.isEmpty())) {
      trabajador = (Trabajador)colTrabajador.iterator().next();
      if ((trabajador.getPersonal().getPassword() != null) && (!trabajador.getPersonal().getPassword().equals(""))) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error. Este trabajador ya está registrado", ""));
      } else if (!this.password.equals(this.passwordConfirm)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error. El password no es igual al password de confirmación", ""));
      }
      else {
        try {
          Personal personal = this.expedienteFacade.findPersonalById(trabajador.getPersonal().getIdPersonal());
          personal.setPassword(this.password);
          this.expedienteFacade.updatePersonal(personal);
        } catch (Exception e) {
          log.error("Excepcion controlada:", e);
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error con la base de datos", ""));
          return null;
        }

        LoginSession session = 
          (LoginSession)context.getApplication().getVariableResolver().resolveVariable(
          context, 
          "loginSession");
        session.setOrganismo(trabajador.getOrganismo());
        session.setUsuario(String.valueOf(trabajador.getPersonal().getCedula()));
        session.setServicioPersonal(true);

        session.setAdministrador("N");
        session.setAgregar(true);
        session.setModificar(true);
        session.setEliminar(true);
        session.setConsultar(true);
        session.setEjecutar(true);

        ExternalContext externalContext = context.getExternalContext();
        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
        try {
          externalContext.redirect("home.jsf");
        } catch (Exception localException1) {
        }
        context.responseComplete();
        return "success";
      }
    }
    else {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La información que suministró no es válida", ""));
    }
    return null;
  }

  public int getCedula() {
    return this.cedula;
  }

  public String getPassword() {
    return this.password;
  }

  public String getPasswordConfirm() {
    return this.passwordConfirm;
  }

  public void setCedula(int i) {
    this.cedula = i;
  }

  public void setPassword(String string) {
    this.password = string;
  }

  public void setPasswordConfirm(String string) {
    this.passwordConfirm = string;
  }

  public int getAnioInicio() {
    return this.anioInicio;
  }

  public Date getFechaNacimiento() {
    return this.fechaNacimiento;
  }

  public void setAnioInicio(int i) {
    this.anioInicio = i;
  }

  public void setFechaNacimiento(Date date) {
    this.fechaNacimiento = date;
  }
}