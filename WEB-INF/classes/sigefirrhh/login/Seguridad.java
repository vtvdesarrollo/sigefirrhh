package sigefirrhh.login;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.log4j.Logger;
import sigefirrhh.sistema.Rol;
import sigefirrhh.sistema.RolOpcion;
import sigefirrhh.sistema.SistemaFacade;
import sigefirrhh.sistema.UsuarioRol;

public class Seguridad extends TagSupport
{
  static Logger log = Logger.getLogger(Seguridad.class.getName());
  private String opcion;
  private SistemaFacade sistemaFacade = new SistemaFacade();

  public int doStartTag() {
    return 0;
  }

  public int doEndTag()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletRequest httpServletRequest = (HttpServletRequest)this.pageContext.getRequest();
    HttpServletResponse httpServletResponse = (HttpServletResponse)this.pageContext.getResponse();
    LoginSession session = 
      (LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession");
    try
    {
      session.setAutenticado(false);
      session.setConsultar(false);
      session.setAgregar(false);
      session.setModificar(false);
      session.setEliminar(false);
      session.setEjecutar(false);

      if ((session.getAdministrador().equals("S")) || (session.isServicioPersonal())) {
        session.setAutenticado(false);
        session.setConsultar(true);
        session.setAgregar(true);
        session.setModificar(true);
        session.setEliminar(true);
        session.setEjecutar(true);
        return 6;
      }
      String uri = httpServletRequest.getRequestURI().substring(12);
      uri = uri.substring(0, uri.length() - 4);

      Collection colUsuarioRol = new ArrayList();
      colUsuarioRol = (ArrayList)session.getColUsuarioRol();

      Iterator iterator = colUsuarioRol.iterator();

      while (iterator.hasNext())
      {
        UsuarioRol usuarioRol = (UsuarioRol)iterator.next();

        Collection colRolOpcion = new ArrayList();
        colRolOpcion = (ArrayList)this.sistemaFacade.findRolOpcionByRolAndOpcion(usuarioRol.getRol().getIdRol(), uri);

        if (colRolOpcion.iterator().hasNext()) {
          RolOpcion rolOpcion = new RolOpcion();
          rolOpcion = (RolOpcion)colRolOpcion.iterator().next();
          session.setAutenticado(true);
          session.setConsultar(rolOpcion.getConsultar().equals("S"));
          session.setAgregar(rolOpcion.getAgregar().equals("S"));
          session.setModificar(rolOpcion.getModificar().equals("S"));
          session.setEliminar(rolOpcion.getEliminar().equals("S"));
          session.setEjecutar(rolOpcion.getEjecutar().equals("S"));
          break;
        }
      }

      if (!session.isAutenticado())
      {
        httpServletRequest.getRequestDispatcher("/sinpermiso.jsp").forward(
          httpServletRequest, httpServletResponse);
      }
    }
    catch (Exception e)
    {
      log.debug("Error en el TLD de Seguridad", e);
    }

    return 6;
  }
}