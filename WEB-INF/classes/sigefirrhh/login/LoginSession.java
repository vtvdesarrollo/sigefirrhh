package sigefirrhh.login;

import java.util.Collection;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.sistema.Usuario;

public class LoginSession
{
  private String usuario;
  private Organismo organismo;
  private boolean servicioPersonal;
  private Collection colUsuarioRol;
  private long idUsuario;
  private boolean autenticado;
  private boolean consultar;
  private boolean agregar;
  private boolean modificar;
  private boolean eliminar;
  private boolean ejecutar;
  private boolean passwordVencido;
  private String administrador;
  private Usuario usuarioObject;

  public Organismo getOrganismo()
  {
    return this.organismo;
  }

  public String getUsuario() {
    return this.usuario;
  }

  public void setOrganismo(Organismo organismo) {
    this.organismo = organismo;
  }

  public void setUsuario(String string) {
    this.usuario = string;
  }

  public long getIdOrganismo() {
    return this.organismo.getIdOrganismo();
  }

  public String getURLLogo() {
    if (this.organismo != null) {
      return "/images/logo/" + this.organismo.getCodOrganismo() + ".gif";
    }
    return "/images/logo/blank.gif";
  }

  public String getURLNombre()
  {
    if (this.organismo != null) {
      return "/images/logo/" + this.organismo.getCodOrganismo() + "-n.gif";
    }
    return "/images/logo/blank-n.gif";
  }

  public boolean isSupervisor()
  {
    return this.usuario.equals("admin");
  }

  public boolean isValid() {
    return this.usuario != null;
  }

  public boolean isServicioPersonal() {
    return this.servicioPersonal;
  }

  public void setServicioPersonal(boolean b) {
    this.servicioPersonal = b;
  }

  public Collection getColUsuarioRol() {
    return this.colUsuarioRol;
  }

  public void setColUsuarioRol(Collection collection) {
    this.colUsuarioRol = collection;
  }

  public long getIdUsuario() {
    return this.idUsuario;
  }

  public void setIdUsuario(long l) {
    this.idUsuario = l;
  }

  public void setPasswordVencido(boolean b) {
    this.passwordVencido = b;
  }

  public boolean isAutenticado()
  {
    return this.autenticado;
  }

  public void setAutenticado(boolean b) {
    this.autenticado = b;
  }

  public String getAdministrador()
  {
    return this.administrador;
  }

  public boolean isAgregar() {
    return this.agregar;
  }

  public boolean isConsultar() {
    return this.consultar;
  }

  public boolean isEjecutar() {
    return this.ejecutar;
  }

  public boolean isEliminar() {
    return this.eliminar;
  }

  public boolean isModificar() {
    return this.modificar;
  }

  public void setAdministrador(String string) {
    this.administrador = string;
  }

  public void setAgregar(boolean b) {
    this.agregar = b;
  }

  public void setConsultar(boolean b) {
    this.consultar = b;
  }

  public void setEjecutar(boolean b) {
    this.ejecutar = b;
  }

  public void setEliminar(boolean b) {
    this.eliminar = b;
  }

  public void setModificar(boolean b) {
    this.modificar = b;
  }

  public Usuario getUsuarioObject()
  {
    return this.usuarioObject;
  }

  public void setUsuarioObject(Usuario usuario)
  {
    this.usuarioObject = usuario;
  }

  public boolean isPasswordVencido() {
    return this.passwordVencido;
  }
}