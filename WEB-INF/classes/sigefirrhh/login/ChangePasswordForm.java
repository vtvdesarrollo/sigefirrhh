package sigefirrhh.login;

import eforserver.presentation.Form;
import eforserver.tools.PasswordTools;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.SistemaFacade;
import sigefirrhh.sistema.Usuario;

public class ChangePasswordForm extends Form
{
  private LoginSession login;
  private String usuario;
  private String password;
  private String newPassword;
  private String repeatNewPassword;
  static Logger log = Logger.getLogger(ChangePasswordForm.class.getName());
  private Collection colOrganismo;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private String selectOrganismo;
  private Organismo organismo;
  private SistemaFacade sistemaFacade = new SistemaFacade();

  public ChangePasswordForm() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    this.usuario = this.login.getUsuario();

    context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Su password ha vencido debe realizar el cambio de inmediato", ""));
  }

  public String getSelectOrganismo()
  {
    return this.selectOrganismo;
  }
  public boolean isAdministrador() {
    return this.login.getAdministrador().equals("S");
  }

  public void init()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error en el constructor", ""));
  }

  public String cancel()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
    try
    {
      if (this.login.isPasswordVencido()) {
        externalContext.redirect("/sigefirrhh/login.jsf");
        context.responseComplete();
      }
      else {
        externalContext.redirect("/sigefirrhh/home.jsf");
        context.responseComplete();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al accesar la base de datos", ""));
      return null;
    }
    return "success";
  }

  public String update()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Collection colUsuario = null;
    PasswordTools passwordTools = PasswordTools.getInstance();
    try
    {
      colUsuario = this.sistemaFacade.findUsuarioByPassword(
        this.usuario, this.password);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);

      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al accesar la base de datos", ""));
      return null;
    }

    Usuario usuario = (Usuario)colUsuario.iterator().next();
    if ((colUsuario != null) && (!colUsuario.isEmpty()) && (passwordTools.chequear(this.password, usuario.getPassword())))
    {
      int minLen = 6;
      try {
        minLen = this.sistemaFacade.getParametroMinLongitudClave();
      } catch (Exception ex) {
        minLen = 6;
      }
      try
      {
        if (this.newPassword.length() < minLen) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La nueva contraseña debe ser de " + String.valueOf(minLen) + " o mas caracteres ", ""));
          return "success";
        }

        if (this.newPassword.equals(this.password)) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La contraseña nueva no puede ser identica a la actual ", ""));
          return "success";
        }

        if (!this.newPassword.equals(this.repeatNewPassword)) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La contraseña de corfirmación es diferente a la contraeña nueva", ""));
          return "success";
        }

        this.newPassword = passwordTools.cifrar(this.newPassword);

        usuario.setPassword(this.newPassword);
        usuario.setCambioPassword(new Date());
        this.sistemaFacade.updateUsuario(usuario);
        this.login.setPasswordVencido(false);

        RegistrarAuditoria.registrar(context, usuario, 'M', "Usuario cambió la contraseña");

        ExternalContext externalContext = context.getExternalContext();
        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();

        externalContext.redirect("/sigefirrhh/home.jsf");
      }
      catch (Exception e) {
        log.error("Excepcion controlada:", e);
      }
      context.responseComplete();

      return "success";
    }

    context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario y/o la contraseña actual no son validos", ""));

    return null;
  }

  public String getPassword()
  {
    return this.password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public String getRepeatNewPassword() {
    return this.repeatNewPassword;
  }
  public void setRepeatNewPassword(String repeatNewPassword) {
    this.repeatNewPassword = repeatNewPassword;
  }
  public String getUsuario() {
    return this.usuario;
  }
  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }
  public String getNewPassword() {
    return this.newPassword;
  }
  public void setNewPassword(String newPassword) {
    this.newPassword = newPassword;
  }
}