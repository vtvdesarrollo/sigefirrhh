package sigefirrhh.login;

import eforserver.presentation.Form;
import eforserver.tools.PasswordTools;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.SistemaFacade;
import sigefirrhh.sistema.Usuario;

public class LoginForm extends Form
{
  static Logger log = Logger.getLogger(LoginForm.class.getName());
  private String usuario;
  private String password;
  private Collection colOrganismo;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private String selectOrganismo;
  private Organismo organismo;
  private SistemaFacade sistemaFacade = new SistemaFacade();

  public String getSelectOrganismo() {
    return this.selectOrganismo;
  }

  public LoginForm() throws Exception {
    refresh();
  }

  public void setSelectOrganismo(String valOrganismo) {
    Iterator iterator = this.colOrganismo.iterator();
    Organismo organismo = null;
    while (iterator.hasNext()) {
      organismo = (Organismo)iterator.next();
      if (String.valueOf(organismo.getIdOrganismo()).equals(
        valOrganismo)) {
        this.organismo = organismo;
      }
    }
    this.selectOrganismo = valOrganismo;
  }

  public Collection getColOrganismo() {
    Collection col = new ArrayList();
    if (this.colOrganismo != null) {
      Iterator iterator = this.colOrganismo.iterator();
      Organismo organismo = null;
      while (iterator.hasNext()) {
        organismo = (Organismo)iterator.next();
        col.add(new SelectItem(
          String.valueOf(organismo.getIdOrganismo()), 
          organismo.toString()));
      }
    }
    return col;
  }

  public void refresh() {
    try {
      this.colOrganismo = 
        this.estructuraFacade.findAllOrganismo();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String send() {
    FacesContext context = FacesContext.getCurrentInstance();
    Collection colUsuario = null;
    PasswordTools passwordTools = PasswordTools.getInstance();
    log.error("@@ Antes de verificar Usuario");
    try {
      colUsuario = this.sistemaFacade.findUsuarioByPassword(this.usuario, this.password);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al accesar la base de datos", ""));
      return null;
    }

    int maxIntentos = 4;
    try {
      maxIntentos = this.sistemaFacade.getParametroMaxIntentos();
    } catch (Exception e) {
      maxIntentos = 4;
    }

    if ((colUsuario != null) && (!colUsuario.isEmpty())) {
      Usuario usuario = (Usuario)colUsuario.iterator().next();

      if (usuario.getIntentos() >= maxIntentos) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario se bloqueará por cantidad de intentos fallidos", ""));

        usuario.setActivo("N");
        try {
          this.sistemaFacade.updateUsuario(usuario);

          RegistrarAuditoria.registrar(context, usuario, 'M', "Se inactivó el usuario luego de " + String.valueOf(usuario.getIntentos()) + " fallidos");
        }
        catch (Exception e) {
          return null;
        }
        return null;
      }

      if (!passwordTools.chequear(this.password, usuario.getPassword())) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario y/o la contraseña no son validos", ""));

        usuario.setIntentos(usuario.getIntentos() + 1);
        try
        {
          this.sistemaFacade.updateUsuario(usuario);
        } catch (Exception e) {
          return null;
        }
        return null;
      }

      LoginSession session = 
        (LoginSession)context.getApplication().getVariableResolver().resolveVariable(
        context, 
        "loginSession");
      try
      {
        if (!usuario.getActivo().equals("S")) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario " + usuario.getUsuario() + " no está activo", "Consulte con el administrador del sistema si su cuenta de usuario está activa"));

          RegistrarAuditoria.registrar(context, usuario, 'S', "Usuario inactivo intentó ingresar al sistema");
          return null;
        }

        Calendar c = Calendar.getInstance();
        Date hoy = c.getTime();
        if ((usuario.getFechaVence() != null) && (hoy.after(usuario.getFechaVence()))) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario " + usuario.getUsuario() + " está vencido", "Consulte con el administrador del sistema si la fecha de vencimiento de usuario está vigente"));

          usuario.setActivo("N");
          try {
            this.sistemaFacade.updateUsuario(usuario);

            RegistrarAuditoria.registrar(context, usuario, 'M', "Inactivado el usuario, fecha de vencimiento: " + usuario.getFechaVence().toString());
          }
          catch (Exception e) {
            return null;
          }
          return null;
        }

        if (usuario.getAdministrador().equals("N")) {
          Collection colUsuarioOrganismo = this.sistemaFacade.findUsuarioOrganismoByUsuario(usuario.getIdUsuario(), this.organismo.getIdOrganismo());
          if (colUsuarioOrganismo.size() == 0) {
            context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "EL usuario no posee permiso sobre este organismo", ""));
            return null;
          }

        }

        usuario.setIntentos(0);
        try {
          this.sistemaFacade.updateUsuario(usuario);
        } catch (Exception e) {
          return null;
        }

        int diasDuracionClave = this.sistemaFacade.getParametroDiasDuracionClave();

        session.setPasswordVencido(usuario.cambiaPasswordDias(diasDuracionClave));
        if (session.isPasswordVencido()) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "EL usuario tiene que cambiar su clave", ""));
          ExternalContext externalContext = context.getExternalContext();
          externalContext.redirect("sigefirrhh/login/ChangePassword.jsf");
        }
      }
      catch (Exception e) {
        log.error("Excepcion controlada:", e);
        return null;
      }

      session.setServicioPersonal(false);
      session.setOrganismo(this.organismo);
      session.setUsuario(this.usuario);

      Collection colUsuarioRol = new ArrayList();
      usuario = new Usuario();
      usuario = (Usuario)colUsuario.iterator().next();
      usuario.setIdOrganismo(this.organismo.getIdOrganismo());
      session.setIdUsuario(usuario.getIdUsuario());
      session.setAdministrador(usuario.getAdministrador());
      session.setUsuarioObject(usuario);
      try
      {
        colUsuarioRol = this.sistemaFacade.findUsuarioRolByUsuario(usuario.getIdUsuario());
        session.setColUsuarioRol(colUsuarioRol);

        RegistrarAuditoria.registrar(context, usuario, 'I', "Ingresó al sistema");

        ExternalContext externalContext = context.getExternalContext();
        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
        externalContext.redirect("home.jsf");
      }
      catch (Exception e) {
        log.error("Excepcion controlada:", e);
      }

      context.responseComplete();

      return "success";
    }

    context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario y/o la contraseña no son validos", ""));

    return null;
  }

  public String getUsuario() {
    return this.usuario;
  }

  public void setPassword(String string) {
    this.password = string;
  }

  public void setUsuario(String string) {
    this.usuario = string;
  }

  public String getPassword() {
    return this.password;
  }
}