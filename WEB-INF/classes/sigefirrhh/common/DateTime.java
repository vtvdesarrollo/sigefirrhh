package sigefirrhh.common;

import java.util.TimeZone;

public class DateTime
{
  public TimeZone getTimeZone()
  {
    return TimeZone.getDefault();
  }
}