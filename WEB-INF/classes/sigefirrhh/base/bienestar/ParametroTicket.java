package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;

public class ParametroTicket
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_DISTRIBUCION;
  protected static final Map LISTA_SINO;
  protected static final Map LISTA_BASICO_INTEGRAL;
  private long idParametroTicket;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private String diasHabiles;
  private int diasFijos;
  private String incluyeSabados;
  private String deducirAusencias;
  private String cierreAusencias;
  private String pagoNomina;
  private String pagoTarjeta;
  private String sueldoBasicoIntegral;
  private String distribucion;
  private double sueldoMaximo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cierreAusencias", "conceptoTipoPersonal", "deducirAusencias", "diasFijos", "diasHabiles", "distribucion", "idParametroTicket", "incluyeSabados", "pagoNomina", "pagoTarjeta", "sueldoBasicoIntegral", "sueldoMaximo", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 26, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.ParametroTicket"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroTicket());

    LISTA_DISTRIBUCION = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_BASICO_INTEGRAL = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
    LISTA_BASICO_INTEGRAL.put("B", "BASICO");
    LISTA_BASICO_INTEGRAL.put("I", "INTEGRAL");
    LISTA_BASICO_INTEGRAL.put("N", "NO APLICA");
    LISTA_DISTRIBUCION.put("D", "DEPENDENCIA");
    LISTA_DISTRIBUCION.put("S", "SEDE");
    LISTA_DISTRIBUCION.put("L", "LUGAR DE PAGO");
  }

  public ParametroTicket()
  {
    jdoSetdiasHabiles(this, "S");

    jdoSetdiasFijos(this, 0);

    jdoSetincluyeSabados(this, "N");

    jdoSetdeducirAusencias(this, "N");

    jdoSetcierreAusencias(this, "N");

    jdoSetpagoNomina(this, "N");

    jdoSetpagoTarjeta(this, "N");

    jdoSetsueldoBasicoIntegral(this, "B");

    jdoSetdistribucion(this, "N");

    jdoSetsueldoMaximo(this, 0.0D);
  }

  public String toString() {
    return jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion();
  }

  public String getCierreAusencias()
  {
    return jdoGetcierreAusencias(this);
  }
  public void setCierreAusencias(String cierreAusencias) {
    jdoSetcierreAusencias(this, cierreAusencias);
  }
  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public String getDeducirAusencias() {
    return jdoGetdeducirAusencias(this);
  }
  public void setDeducirAusencias(String deducirAusencias) {
    jdoSetdeducirAusencias(this, deducirAusencias);
  }
  public int getDiasFijos() {
    return jdoGetdiasFijos(this);
  }
  public void setDiasFijos(int diasFijos) {
    jdoSetdiasFijos(this, diasFijos);
  }
  public String getDiasHabiles() {
    return jdoGetdiasHabiles(this);
  }
  public void setDiasHabiles(String diasHabiles) {
    jdoSetdiasHabiles(this, diasHabiles);
  }
  public long getIdParametroTicket() {
    return jdoGetidParametroTicket(this);
  }
  public void setIdParametroTicket(long idParametroTicket) {
    jdoSetidParametroTicket(this, idParametroTicket);
  }
  public String getIncluyeSabados() {
    return jdoGetincluyeSabados(this);
  }
  public void setIncluyeSabados(String incluyeSabados) {
    jdoSetincluyeSabados(this, incluyeSabados);
  }

  public String getPagoNomina() {
    return jdoGetpagoNomina(this);
  }
  public void setPagoNomina(String pagoNomina) {
    jdoSetpagoNomina(this, pagoNomina);
  }
  public String getPagoTarjeta() {
    return jdoGetpagoTarjeta(this);
  }
  public void setPagoTarjeta(String pagoTarjeta) {
    jdoSetpagoTarjeta(this, pagoTarjeta);
  }
  public double getSueldoMaximo() {
    return jdoGetsueldoMaximo(this);
  }
  public void setSueldoMaximo(double sueldoMaximo) {
    jdoSetsueldoMaximo(this, sueldoMaximo);
  }

  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public String getSueldoBasicoIntegral() {
    return jdoGetsueldoBasicoIntegral(this);
  }
  public void setSueldoBasicoIntegral(String sueldoBasicoIntegral) {
    jdoSetsueldoBasicoIntegral(this, sueldoBasicoIntegral);
  }

  public String getDistribucion()
  {
    return jdoGetdistribucion(this);
  }

  public void setDistribucion(String distribucion)
  {
    jdoSetdistribucion(this, distribucion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroTicket localParametroTicket = new ParametroTicket();
    localParametroTicket.jdoFlags = 1;
    localParametroTicket.jdoStateManager = paramStateManager;
    return localParametroTicket;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroTicket localParametroTicket = new ParametroTicket();
    localParametroTicket.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroTicket.jdoFlags = 1;
    localParametroTicket.jdoStateManager = paramStateManager;
    return localParametroTicket;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cierreAusencias);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.deducirAusencias);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasFijos);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.diasHabiles);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.distribucion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroTicket);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.incluyeSabados);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.pagoNomina);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.pagoTarjeta);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sueldoBasicoIntegral);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoMaximo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cierreAusencias = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.deducirAusencias = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasFijos = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasHabiles = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.distribucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroTicket = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.incluyeSabados = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pagoNomina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pagoTarjeta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoBasicoIntegral = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoMaximo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroTicket paramParametroTicket, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.cierreAusencias = paramParametroTicket.cierreAusencias;
      return;
    case 1:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramParametroTicket.conceptoTipoPersonal;
      return;
    case 2:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.deducirAusencias = paramParametroTicket.deducirAusencias;
      return;
    case 3:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.diasFijos = paramParametroTicket.diasFijos;
      return;
    case 4:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.diasHabiles = paramParametroTicket.diasHabiles;
      return;
    case 5:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.distribucion = paramParametroTicket.distribucion;
      return;
    case 6:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroTicket = paramParametroTicket.idParametroTicket;
      return;
    case 7:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.incluyeSabados = paramParametroTicket.incluyeSabados;
      return;
    case 8:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.pagoNomina = paramParametroTicket.pagoNomina;
      return;
    case 9:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.pagoTarjeta = paramParametroTicket.pagoTarjeta;
      return;
    case 10:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoBasicoIntegral = paramParametroTicket.sueldoBasicoIntegral;
      return;
    case 11:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoMaximo = paramParametroTicket.sueldoMaximo;
      return;
    case 12:
      if (paramParametroTicket == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramParametroTicket.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroTicket))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroTicket localParametroTicket = (ParametroTicket)paramObject;
    if (localParametroTicket.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroTicket, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroTicketPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroTicketPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroTicketPK))
      throw new IllegalArgumentException("arg1");
    ParametroTicketPK localParametroTicketPK = (ParametroTicketPK)paramObject;
    localParametroTicketPK.idParametroTicket = this.idParametroTicket;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroTicketPK))
      throw new IllegalArgumentException("arg1");
    ParametroTicketPK localParametroTicketPK = (ParametroTicketPK)paramObject;
    this.idParametroTicket = localParametroTicketPK.idParametroTicket;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroTicketPK))
      throw new IllegalArgumentException("arg2");
    ParametroTicketPK localParametroTicketPK = (ParametroTicketPK)paramObject;
    localParametroTicketPK.idParametroTicket = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroTicketPK))
      throw new IllegalArgumentException("arg2");
    ParametroTicketPK localParametroTicketPK = (ParametroTicketPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localParametroTicketPK.idParametroTicket);
  }

  private static final String jdoGetcierreAusencias(ParametroTicket paramParametroTicket)
  {
    if (paramParametroTicket.jdoFlags <= 0)
      return paramParametroTicket.cierreAusencias;
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.cierreAusencias;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 0))
      return paramParametroTicket.cierreAusencias;
    return localStateManager.getStringField(paramParametroTicket, jdoInheritedFieldCount + 0, paramParametroTicket.cierreAusencias);
  }

  private static final void jdoSetcierreAusencias(ParametroTicket paramParametroTicket, String paramString)
  {
    if (paramParametroTicket.jdoFlags == 0)
    {
      paramParametroTicket.cierreAusencias = paramString;
      return;
    }
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.cierreAusencias = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroTicket, jdoInheritedFieldCount + 0, paramParametroTicket.cierreAusencias, paramString);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ParametroTicket paramParametroTicket)
  {
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 1))
      return paramParametroTicket.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramParametroTicket, jdoInheritedFieldCount + 1, paramParametroTicket.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ParametroTicket paramParametroTicket, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramParametroTicket, jdoInheritedFieldCount + 1, paramParametroTicket.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGetdeducirAusencias(ParametroTicket paramParametroTicket)
  {
    if (paramParametroTicket.jdoFlags <= 0)
      return paramParametroTicket.deducirAusencias;
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.deducirAusencias;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 2))
      return paramParametroTicket.deducirAusencias;
    return localStateManager.getStringField(paramParametroTicket, jdoInheritedFieldCount + 2, paramParametroTicket.deducirAusencias);
  }

  private static final void jdoSetdeducirAusencias(ParametroTicket paramParametroTicket, String paramString)
  {
    if (paramParametroTicket.jdoFlags == 0)
    {
      paramParametroTicket.deducirAusencias = paramString;
      return;
    }
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.deducirAusencias = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroTicket, jdoInheritedFieldCount + 2, paramParametroTicket.deducirAusencias, paramString);
  }

  private static final int jdoGetdiasFijos(ParametroTicket paramParametroTicket)
  {
    if (paramParametroTicket.jdoFlags <= 0)
      return paramParametroTicket.diasFijos;
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.diasFijos;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 3))
      return paramParametroTicket.diasFijos;
    return localStateManager.getIntField(paramParametroTicket, jdoInheritedFieldCount + 3, paramParametroTicket.diasFijos);
  }

  private static final void jdoSetdiasFijos(ParametroTicket paramParametroTicket, int paramInt)
  {
    if (paramParametroTicket.jdoFlags == 0)
    {
      paramParametroTicket.diasFijos = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.diasFijos = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroTicket, jdoInheritedFieldCount + 3, paramParametroTicket.diasFijos, paramInt);
  }

  private static final String jdoGetdiasHabiles(ParametroTicket paramParametroTicket)
  {
    if (paramParametroTicket.jdoFlags <= 0)
      return paramParametroTicket.diasHabiles;
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.diasHabiles;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 4))
      return paramParametroTicket.diasHabiles;
    return localStateManager.getStringField(paramParametroTicket, jdoInheritedFieldCount + 4, paramParametroTicket.diasHabiles);
  }

  private static final void jdoSetdiasHabiles(ParametroTicket paramParametroTicket, String paramString)
  {
    if (paramParametroTicket.jdoFlags == 0)
    {
      paramParametroTicket.diasHabiles = paramString;
      return;
    }
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.diasHabiles = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroTicket, jdoInheritedFieldCount + 4, paramParametroTicket.diasHabiles, paramString);
  }

  private static final String jdoGetdistribucion(ParametroTicket paramParametroTicket)
  {
    if (paramParametroTicket.jdoFlags <= 0)
      return paramParametroTicket.distribucion;
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.distribucion;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 5))
      return paramParametroTicket.distribucion;
    return localStateManager.getStringField(paramParametroTicket, jdoInheritedFieldCount + 5, paramParametroTicket.distribucion);
  }

  private static final void jdoSetdistribucion(ParametroTicket paramParametroTicket, String paramString)
  {
    if (paramParametroTicket.jdoFlags == 0)
    {
      paramParametroTicket.distribucion = paramString;
      return;
    }
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.distribucion = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroTicket, jdoInheritedFieldCount + 5, paramParametroTicket.distribucion, paramString);
  }

  private static final long jdoGetidParametroTicket(ParametroTicket paramParametroTicket)
  {
    return paramParametroTicket.idParametroTicket;
  }

  private static final void jdoSetidParametroTicket(ParametroTicket paramParametroTicket, long paramLong)
  {
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.idParametroTicket = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroTicket, jdoInheritedFieldCount + 6, paramParametroTicket.idParametroTicket, paramLong);
  }

  private static final String jdoGetincluyeSabados(ParametroTicket paramParametroTicket)
  {
    if (paramParametroTicket.jdoFlags <= 0)
      return paramParametroTicket.incluyeSabados;
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.incluyeSabados;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 7))
      return paramParametroTicket.incluyeSabados;
    return localStateManager.getStringField(paramParametroTicket, jdoInheritedFieldCount + 7, paramParametroTicket.incluyeSabados);
  }

  private static final void jdoSetincluyeSabados(ParametroTicket paramParametroTicket, String paramString)
  {
    if (paramParametroTicket.jdoFlags == 0)
    {
      paramParametroTicket.incluyeSabados = paramString;
      return;
    }
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.incluyeSabados = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroTicket, jdoInheritedFieldCount + 7, paramParametroTicket.incluyeSabados, paramString);
  }

  private static final String jdoGetpagoNomina(ParametroTicket paramParametroTicket)
  {
    if (paramParametroTicket.jdoFlags <= 0)
      return paramParametroTicket.pagoNomina;
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.pagoNomina;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 8))
      return paramParametroTicket.pagoNomina;
    return localStateManager.getStringField(paramParametroTicket, jdoInheritedFieldCount + 8, paramParametroTicket.pagoNomina);
  }

  private static final void jdoSetpagoNomina(ParametroTicket paramParametroTicket, String paramString)
  {
    if (paramParametroTicket.jdoFlags == 0)
    {
      paramParametroTicket.pagoNomina = paramString;
      return;
    }
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.pagoNomina = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroTicket, jdoInheritedFieldCount + 8, paramParametroTicket.pagoNomina, paramString);
  }

  private static final String jdoGetpagoTarjeta(ParametroTicket paramParametroTicket)
  {
    if (paramParametroTicket.jdoFlags <= 0)
      return paramParametroTicket.pagoTarjeta;
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.pagoTarjeta;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 9))
      return paramParametroTicket.pagoTarjeta;
    return localStateManager.getStringField(paramParametroTicket, jdoInheritedFieldCount + 9, paramParametroTicket.pagoTarjeta);
  }

  private static final void jdoSetpagoTarjeta(ParametroTicket paramParametroTicket, String paramString)
  {
    if (paramParametroTicket.jdoFlags == 0)
    {
      paramParametroTicket.pagoTarjeta = paramString;
      return;
    }
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.pagoTarjeta = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroTicket, jdoInheritedFieldCount + 9, paramParametroTicket.pagoTarjeta, paramString);
  }

  private static final String jdoGetsueldoBasicoIntegral(ParametroTicket paramParametroTicket)
  {
    if (paramParametroTicket.jdoFlags <= 0)
      return paramParametroTicket.sueldoBasicoIntegral;
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.sueldoBasicoIntegral;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 10))
      return paramParametroTicket.sueldoBasicoIntegral;
    return localStateManager.getStringField(paramParametroTicket, jdoInheritedFieldCount + 10, paramParametroTicket.sueldoBasicoIntegral);
  }

  private static final void jdoSetsueldoBasicoIntegral(ParametroTicket paramParametroTicket, String paramString)
  {
    if (paramParametroTicket.jdoFlags == 0)
    {
      paramParametroTicket.sueldoBasicoIntegral = paramString;
      return;
    }
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.sueldoBasicoIntegral = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroTicket, jdoInheritedFieldCount + 10, paramParametroTicket.sueldoBasicoIntegral, paramString);
  }

  private static final double jdoGetsueldoMaximo(ParametroTicket paramParametroTicket)
  {
    if (paramParametroTicket.jdoFlags <= 0)
      return paramParametroTicket.sueldoMaximo;
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.sueldoMaximo;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 11))
      return paramParametroTicket.sueldoMaximo;
    return localStateManager.getDoubleField(paramParametroTicket, jdoInheritedFieldCount + 11, paramParametroTicket.sueldoMaximo);
  }

  private static final void jdoSetsueldoMaximo(ParametroTicket paramParametroTicket, double paramDouble)
  {
    if (paramParametroTicket.jdoFlags == 0)
    {
      paramParametroTicket.sueldoMaximo = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.sueldoMaximo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroTicket, jdoInheritedFieldCount + 11, paramParametroTicket.sueldoMaximo, paramDouble);
  }

  private static final TipoPersonal jdoGettipoPersonal(ParametroTicket paramParametroTicket)
  {
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
      return paramParametroTicket.tipoPersonal;
    if (localStateManager.isLoaded(paramParametroTicket, jdoInheritedFieldCount + 12))
      return paramParametroTicket.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramParametroTicket, jdoInheritedFieldCount + 12, paramParametroTicket.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ParametroTicket paramParametroTicket, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramParametroTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroTicket.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramParametroTicket, jdoInheritedFieldCount + 12, paramParametroTicket.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}