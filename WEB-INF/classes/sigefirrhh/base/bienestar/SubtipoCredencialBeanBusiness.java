package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SubtipoCredencialBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSubtipoCredencial(SubtipoCredencial subtipoCredencial)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SubtipoCredencial subtipoCredencialNew = 
      (SubtipoCredencial)BeanUtils.cloneBean(
      subtipoCredencial);

    TipoCredencialBeanBusiness tipoCredencialBeanBusiness = new TipoCredencialBeanBusiness();

    if (subtipoCredencialNew.getTipoCredencial() != null) {
      subtipoCredencialNew.setTipoCredencial(
        tipoCredencialBeanBusiness.findTipoCredencialById(
        subtipoCredencialNew.getTipoCredencial().getIdTipoCredencial()));
    }
    pm.makePersistent(subtipoCredencialNew);
  }

  public void updateSubtipoCredencial(SubtipoCredencial subtipoCredencial) throws Exception
  {
    SubtipoCredencial subtipoCredencialModify = 
      findSubtipoCredencialById(subtipoCredencial.getIdSubtipoCredencial());

    TipoCredencialBeanBusiness tipoCredencialBeanBusiness = new TipoCredencialBeanBusiness();

    if (subtipoCredencial.getTipoCredencial() != null) {
      subtipoCredencial.setTipoCredencial(
        tipoCredencialBeanBusiness.findTipoCredencialById(
        subtipoCredencial.getTipoCredencial().getIdTipoCredencial()));
    }

    BeanUtils.copyProperties(subtipoCredencialModify, subtipoCredencial);
  }

  public void deleteSubtipoCredencial(SubtipoCredencial subtipoCredencial) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SubtipoCredencial subtipoCredencialDelete = 
      findSubtipoCredencialById(subtipoCredencial.getIdSubtipoCredencial());
    pm.deletePersistent(subtipoCredencialDelete);
  }

  public SubtipoCredencial findSubtipoCredencialById(long idSubtipoCredencial) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSubtipoCredencial == pIdSubtipoCredencial";
    Query query = pm.newQuery(SubtipoCredencial.class, filter);

    query.declareParameters("long pIdSubtipoCredencial");

    parameters.put("pIdSubtipoCredencial", new Long(idSubtipoCredencial));

    Collection colSubtipoCredencial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSubtipoCredencial.iterator();
    return (SubtipoCredencial)iterator.next();
  }

  public Collection findSubtipoCredencialAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent subtipoCredencialExtent = pm.getExtent(
      SubtipoCredencial.class, true);
    Query query = pm.newQuery(subtipoCredencialExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoCredencial(long idTipoCredencial)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoCredencial.idTipoCredencial == pIdTipoCredencial";

    Query query = pm.newQuery(SubtipoCredencial.class, filter);

    query.declareParameters("long pIdTipoCredencial");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoCredencial", new Long(idTipoCredencial));

    query.setOrdering("nombre ascending");

    Collection colSubtipoCredencial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSubtipoCredencial);

    return colSubtipoCredencial;
  }
}