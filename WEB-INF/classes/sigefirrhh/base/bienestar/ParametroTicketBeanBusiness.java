package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ParametroTicketBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParametroTicket(ParametroTicket parametroTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ParametroTicket parametroTicketNew = 
      (ParametroTicket)BeanUtils.cloneBean(
      parametroTicket);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroTicketNew.getTipoPersonal() != null) {
      parametroTicketNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroTicketNew.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (parametroTicketNew.getConceptoTipoPersonal() != null) {
      parametroTicketNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        parametroTicketNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }
    pm.makePersistent(parametroTicketNew);
  }

  public void updateParametroTicket(ParametroTicket parametroTicket) throws Exception
  {
    ParametroTicket parametroTicketModify = 
      findParametroTicketById(parametroTicket.getIdParametroTicket());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroTicket.getTipoPersonal() != null) {
      parametroTicket.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroTicket.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (parametroTicket.getConceptoTipoPersonal() != null) {
      parametroTicket.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        parametroTicket.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    BeanUtils.copyProperties(parametroTicketModify, parametroTicket);
  }

  public void deleteParametroTicket(ParametroTicket parametroTicket) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ParametroTicket parametroTicketDelete = 
      findParametroTicketById(parametroTicket.getIdParametroTicket());
    pm.deletePersistent(parametroTicketDelete);
  }

  public ParametroTicket findParametroTicketById(long idParametroTicket) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParametroTicket == pIdParametroTicket";
    Query query = pm.newQuery(ParametroTicket.class, filter);

    query.declareParameters("long pIdParametroTicket");

    parameters.put("pIdParametroTicket", new Long(idParametroTicket));

    Collection colParametroTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParametroTicket.iterator();
    return (ParametroTicket)iterator.next();
  }

  public Collection findParametroTicketAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parametroTicketExtent = pm.getExtent(
      ParametroTicket.class, true);
    Query query = pm.newQuery(parametroTicketExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ParametroTicket.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colParametroTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParametroTicket);

    return colParametroTicket;
  }
}