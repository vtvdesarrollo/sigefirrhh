package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class ParametroPlanVacacionalPK
  implements Serializable
{
  public long idParametroVacacional;

  public ParametroPlanVacacionalPK()
  {
  }

  public ParametroPlanVacacionalPK(long idParametroVacacional)
  {
    this.idParametroVacacional = idParametroVacacional;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroPlanVacacionalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroPlanVacacionalPK thatPK)
  {
    return 
      this.idParametroVacacional == thatPK.idParametroVacacional;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroVacacional)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroVacacional);
  }
}