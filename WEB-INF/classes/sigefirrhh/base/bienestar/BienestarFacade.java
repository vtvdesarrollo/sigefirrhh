package sigefirrhh.base.bienestar;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class BienestarFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private BienestarBusiness bienestarBusiness = new BienestarBusiness();

  public void addDenominacionTicket(DenominacionTicket denominacionTicket)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addDenominacionTicket(denominacionTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDenominacionTicket(DenominacionTicket denominacionTicket) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateDenominacionTicket(denominacionTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDenominacionTicket(DenominacionTicket denominacionTicket) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteDenominacionTicket(denominacionTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DenominacionTicket findDenominacionTicketById(long denominacionTicketId) throws Exception
  {
    try { this.txn.open();
      DenominacionTicket denominacionTicket = 
        this.bienestarBusiness.findDenominacionTicketById(denominacionTicketId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(denominacionTicket);
      return denominacionTicket;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDenominacionTicket() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllDenominacionTicket();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDenominacionTicketByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findDenominacionTicketByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEstablecimientoSalud(EstablecimientoSalud establecimientoSalud)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addEstablecimientoSalud(establecimientoSalud);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEstablecimientoSalud(EstablecimientoSalud establecimientoSalud) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateEstablecimientoSalud(establecimientoSalud);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEstablecimientoSalud(EstablecimientoSalud establecimientoSalud) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteEstablecimientoSalud(establecimientoSalud);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public EstablecimientoSalud findEstablecimientoSaludById(long establecimientoSaludId) throws Exception
  {
    try { this.txn.open();
      EstablecimientoSalud establecimientoSalud = 
        this.bienestarBusiness.findEstablecimientoSaludById(establecimientoSaludId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(establecimientoSalud);
      return establecimientoSalud;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEstablecimientoSalud() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllEstablecimientoSalud();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEstablecimientoSaludByCodEstablecimientoSalud(String codEstablecimientoSalud)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findEstablecimientoSaludByCodEstablecimientoSalud(codEstablecimientoSalud);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEstablecimientoSaludByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findEstablecimientoSaludByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addNivelBeca(NivelBeca nivelBeca)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addNivelBeca(nivelBeca);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateNivelBeca(NivelBeca nivelBeca) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateNivelBeca(nivelBeca);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteNivelBeca(NivelBeca nivelBeca) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteNivelBeca(nivelBeca);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public NivelBeca findNivelBecaById(long nivelBecaId) throws Exception
  {
    try { this.txn.open();
      NivelBeca nivelBeca = 
        this.bienestarBusiness.findNivelBecaById(nivelBecaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(nivelBeca);
      return nivelBeca;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllNivelBeca() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllNivelBeca();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNivelBecaByNivelEducativo(String nivelEducativo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findNivelBecaByNivelEducativo(nivelEducativo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParametroBeca(ParametroBeca parametroBeca)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addParametroBeca(parametroBeca);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParametroBeca(ParametroBeca parametroBeca) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateParametroBeca(parametroBeca);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParametroBeca(ParametroBeca parametroBeca) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteParametroBeca(parametroBeca);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ParametroBeca findParametroBecaById(long parametroBecaId) throws Exception
  {
    try { this.txn.open();
      ParametroBeca parametroBeca = 
        this.bienestarBusiness.findParametroBecaById(parametroBecaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parametroBeca);
      return parametroBeca;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParametroBeca() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllParametroBeca();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParametroBecaByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findParametroBecaByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParametroBecaGeneral(ParametroBecaGeneral parametroBecaGeneral)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addParametroBecaGeneral(parametroBecaGeneral);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParametroBecaGeneral(ParametroBecaGeneral parametroBecaGeneral) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateParametroBecaGeneral(parametroBecaGeneral);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParametroBecaGeneral(ParametroBecaGeneral parametroBecaGeneral) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteParametroBecaGeneral(parametroBecaGeneral);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ParametroBecaGeneral findParametroBecaGeneralById(long parametroBecaGeneralId) throws Exception
  {
    try { this.txn.open();
      ParametroBecaGeneral parametroBecaGeneral = 
        this.bienestarBusiness.findParametroBecaGeneralById(parametroBecaGeneralId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parametroBecaGeneral);
      return parametroBecaGeneral;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParametroBecaGeneral() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllParametroBecaGeneral();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParametroBecaGeneralByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findParametroBecaGeneralByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParametroGuarderia(ParametroGuarderia parametroGuarderia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addParametroGuarderia(parametroGuarderia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParametroGuarderia(ParametroGuarderia parametroGuarderia) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateParametroGuarderia(parametroGuarderia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParametroGuarderia(ParametroGuarderia parametroGuarderia) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteParametroGuarderia(parametroGuarderia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ParametroGuarderia findParametroGuarderiaById(long parametroGuarderiaId) throws Exception
  {
    try { this.txn.open();
      ParametroGuarderia parametroGuarderia = 
        this.bienestarBusiness.findParametroGuarderiaById(parametroGuarderiaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parametroGuarderia);
      return parametroGuarderia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParametroGuarderia() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllParametroGuarderia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParametroGuarderiaByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findParametroGuarderiaByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParametroJuguete(ParametroJuguete parametroJuguete)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addParametroJuguete(parametroJuguete);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParametroJuguete(ParametroJuguete parametroJuguete) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateParametroJuguete(parametroJuguete);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParametroJuguete(ParametroJuguete parametroJuguete) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteParametroJuguete(parametroJuguete);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ParametroJuguete findParametroJugueteById(long parametroJugueteId) throws Exception
  {
    try { this.txn.open();
      ParametroJuguete parametroJuguete = 
        this.bienestarBusiness.findParametroJugueteById(parametroJugueteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parametroJuguete);
      return parametroJuguete;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParametroJuguete() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllParametroJuguete();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParametroJugueteByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findParametroJugueteByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParametroTicket(ParametroTicket parametroTicket)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addParametroTicket(parametroTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParametroTicket(ParametroTicket parametroTicket) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateParametroTicket(parametroTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParametroTicket(ParametroTicket parametroTicket) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteParametroTicket(parametroTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ParametroTicket findParametroTicketById(long parametroTicketId) throws Exception
  {
    try { this.txn.open();
      ParametroTicket parametroTicket = 
        this.bienestarBusiness.findParametroTicketById(parametroTicketId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parametroTicket);
      return parametroTicket;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParametroTicket() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllParametroTicket();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParametroTicketByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findParametroTicketByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParametroUtiles(ParametroUtiles parametroUtiles)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addParametroUtiles(parametroUtiles);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParametroUtiles(ParametroUtiles parametroUtiles) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateParametroUtiles(parametroUtiles);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParametroUtiles(ParametroUtiles parametroUtiles) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteParametroUtiles(parametroUtiles);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ParametroUtiles findParametroUtilesById(long parametroUtilesId) throws Exception
  {
    try { this.txn.open();
      ParametroUtiles parametroUtiles = 
        this.bienestarBusiness.findParametroUtilesById(parametroUtilesId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parametroUtiles);
      return parametroUtiles;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParametroUtiles() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllParametroUtiles();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParametroUtilesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findParametroUtilesByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPlanPoliza(PlanPoliza planPoliza)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addPlanPoliza(planPoliza);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePlanPoliza(PlanPoliza planPoliza) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updatePlanPoliza(planPoliza);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePlanPoliza(PlanPoliza planPoliza) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deletePlanPoliza(planPoliza);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PlanPoliza findPlanPolizaById(long planPolizaId) throws Exception
  {
    try { this.txn.open();
      PlanPoliza planPoliza = 
        this.bienestarBusiness.findPlanPolizaById(planPolizaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(planPoliza);
      return planPoliza;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPlanPoliza() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllPlanPoliza();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPlanPolizaByPoliza(long idPoliza)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findPlanPolizaByPoliza(idPoliza);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPlanPolizaByCodPlanPoliza(String codPlanPoliza)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findPlanPolizaByCodPlanPoliza(codPlanPoliza);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPlanPolizaByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findPlanPolizaByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPoliza(Poliza poliza)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addPoliza(poliza);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePoliza(Poliza poliza) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updatePoliza(poliza);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePoliza(Poliza poliza) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deletePoliza(poliza);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Poliza findPolizaById(long polizaId) throws Exception
  {
    try { this.txn.open();
      Poliza poliza = 
        this.bienestarBusiness.findPolizaById(polizaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(poliza);
      return poliza;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPoliza() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllPoliza();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPolizaByCodPoliza(String codPoliza, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findPolizaByCodPoliza(codPoliza, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPolizaByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findPolizaByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPolizaByVigente(String vigente, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findPolizaByVigente(vigente, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPolizaByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findPolizaByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPrimasPlan(PrimasPlan primasPlan)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addPrimasPlan(primasPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePrimasPlan(PrimasPlan primasPlan) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updatePrimasPlan(primasPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePrimasPlan(PrimasPlan primasPlan) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deletePrimasPlan(primasPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PrimasPlan findPrimasPlanById(long primasPlanId) throws Exception
  {
    try { this.txn.open();
      PrimasPlan primasPlan = 
        this.bienestarBusiness.findPrimasPlanById(primasPlanId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(primasPlan);
      return primasPlan;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPrimasPlan() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllPrimasPlan();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPrimasPlanByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findPrimasPlanByPlanPoliza(idPlanPoliza);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPrimasPlanByParentesco(String parentesco)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findPrimasPlanByParentesco(parentesco);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addProveedorTicket(ProveedorTicket proveedorTicket)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addProveedorTicket(proveedorTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateProveedorTicket(ProveedorTicket proveedorTicket) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateProveedorTicket(proveedorTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteProveedorTicket(ProveedorTicket proveedorTicket) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteProveedorTicket(proveedorTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ProveedorTicket findProveedorTicketById(long proveedorTicketId) throws Exception
  {
    try { this.txn.open();
      ProveedorTicket proveedorTicket = 
        this.bienestarBusiness.findProveedorTicketById(proveedorTicketId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(proveedorTicket);
      return proveedorTicket;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllProveedorTicket() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllProveedorTicket();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProveedorTicketByCodProveedorTicket(String codProveedorTicket)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findProveedorTicketByCodProveedorTicket(codProveedorTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProveedorTicketByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findProveedorTicketByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSubtipoDotacion(SubtipoDotacion subtipoDotacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addSubtipoDotacion(subtipoDotacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSubtipoDotacion(SubtipoDotacion subtipoDotacion) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateSubtipoDotacion(subtipoDotacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSubtipoDotacion(SubtipoDotacion subtipoDotacion) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteSubtipoDotacion(subtipoDotacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SubtipoDotacion findSubtipoDotacionById(long subtipoDotacionId) throws Exception
  {
    try { this.txn.open();
      SubtipoDotacion subtipoDotacion = 
        this.bienestarBusiness.findSubtipoDotacionById(subtipoDotacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(subtipoDotacion);
      return subtipoDotacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSubtipoDotacion() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllSubtipoDotacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSubtipoDotacionByTipoDotacion(long idTipoDotacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findSubtipoDotacionByTipoDotacion(idTipoDotacion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoDotacion(TipoDotacion tipoDotacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addTipoDotacion(tipoDotacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoDotacion(TipoDotacion tipoDotacion) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateTipoDotacion(tipoDotacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoDotacion(TipoDotacion tipoDotacion) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteTipoDotacion(tipoDotacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoDotacion findTipoDotacionById(long tipoDotacionId) throws Exception
  {
    try { this.txn.open();
      TipoDotacion tipoDotacion = 
        this.bienestarBusiness.findTipoDotacionById(tipoDotacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoDotacion);
      return tipoDotacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoDotacion() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllTipoDotacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoDotacionByCodTipoDotacion(String codTipoDotacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findTipoDotacionByCodTipoDotacion(codTipoDotacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoDotacionByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findTipoDotacionByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoSiniestro(TipoSiniestro tipoSiniestro)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.bienestarBusiness.addTipoSiniestro(tipoSiniestro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoSiniestro(TipoSiniestro tipoSiniestro) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateTipoSiniestro(tipoSiniestro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoSiniestro(TipoSiniestro tipoSiniestro) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteTipoSiniestro(tipoSiniestro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoSiniestro findTipoSiniestroById(long tipoSiniestroId) throws Exception
  {
    try { this.txn.open();
      TipoSiniestro tipoSiniestro = 
        this.bienestarBusiness.findTipoSiniestroById(tipoSiniestroId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoSiniestro);
      return tipoSiniestro;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoSiniestro() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllTipoSiniestro();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoSiniestroByCodTipoSiniestro(String codTipoSiniestro)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findTipoSiniestroByCodTipoSiniestro(codTipoSiniestro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoSiniestroByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findTipoSiniestroByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void addTipoCredencial(TipoCredencial tipoCredencial) throws Exception
  {
    try {
      this.txn.open();
      this.bienestarBusiness.addTipoCredencial(tipoCredencial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoCredencial(TipoCredencial tipoCredencial) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateTipoCredencial(tipoCredencial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoCredencial(TipoCredencial tipoCredencial) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteTipoCredencial(tipoCredencial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoCredencial findTipoCredencialById(long tipoCredencialId) throws Exception
  {
    try { this.txn.open();
      TipoCredencial tipoCredencial = 
        this.bienestarBusiness.findTipoCredencialById(tipoCredencialId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoCredencial);
      return tipoCredencial;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoCredencial() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllTipoCredencial();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoCredencialByCodTipoCredencial(String codTipoCredencial)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findTipoCredencialByCodTipoCredencial(codTipoCredencial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoCredencialByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findTipoCredencialByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void addSubtipoCredencial(SubtipoCredencial subtipoCredencial) throws Exception
  {
    try {
      this.txn.open();
      this.bienestarBusiness.addSubtipoCredencial(subtipoCredencial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSubtipoCredencial(SubtipoCredencial subtipoCredencial) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.updateSubtipoCredencial(subtipoCredencial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSubtipoCredencial(SubtipoCredencial subtipoCredencial) throws Exception
  {
    try { this.txn.open();
      this.bienestarBusiness.deleteSubtipoCredencial(subtipoCredencial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SubtipoCredencial findSubtipoCredencialById(long subtipoCredencialId) throws Exception
  {
    try { this.txn.open();
      SubtipoCredencial subtipoCredencial = 
        this.bienestarBusiness.findSubtipoCredencialById(subtipoCredencialId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(subtipoCredencial);
      return subtipoCredencial;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSubtipoCredencial() throws Exception
  {
    try { this.txn.open();
      return this.bienestarBusiness.findAllSubtipoCredencial();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSubtipoCredencialByTipoCredencial(long idTipoCredencial)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.bienestarBusiness.findSubtipoCredencialByTipoCredencial(idTipoCredencial);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}