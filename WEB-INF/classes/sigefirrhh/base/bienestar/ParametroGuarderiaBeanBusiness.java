package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ParametroGuarderiaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParametroGuarderia(ParametroGuarderia parametroGuarderia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ParametroGuarderia parametroGuarderiaNew = 
      (ParametroGuarderia)BeanUtils.cloneBean(
      parametroGuarderia);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroGuarderiaNew.getTipoPersonal() != null) {
      parametroGuarderiaNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroGuarderiaNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(parametroGuarderiaNew);
  }

  public void updateParametroGuarderia(ParametroGuarderia parametroGuarderia) throws Exception
  {
    ParametroGuarderia parametroGuarderiaModify = 
      findParametroGuarderiaById(parametroGuarderia.getIdParametroGuarderia());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroGuarderia.getTipoPersonal() != null) {
      parametroGuarderia.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroGuarderia.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(parametroGuarderiaModify, parametroGuarderia);
  }

  public void deleteParametroGuarderia(ParametroGuarderia parametroGuarderia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ParametroGuarderia parametroGuarderiaDelete = 
      findParametroGuarderiaById(parametroGuarderia.getIdParametroGuarderia());
    pm.deletePersistent(parametroGuarderiaDelete);
  }

  public ParametroGuarderia findParametroGuarderiaById(long idParametroGuarderia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParametroGuarderia == pIdParametroGuarderia";
    Query query = pm.newQuery(ParametroGuarderia.class, filter);

    query.declareParameters("long pIdParametroGuarderia");

    parameters.put("pIdParametroGuarderia", new Long(idParametroGuarderia));

    Collection colParametroGuarderia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParametroGuarderia.iterator();
    return (ParametroGuarderia)iterator.next();
  }

  public Collection findParametroGuarderiaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parametroGuarderiaExtent = pm.getExtent(
      ParametroGuarderia.class, true);
    Query query = pm.newQuery(parametroGuarderiaExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ParametroGuarderia.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colParametroGuarderia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParametroGuarderia);

    return colParametroGuarderia;
  }
}