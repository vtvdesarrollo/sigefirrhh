package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class ParametroGuarderia
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_QUIENCOBRA;
  protected static final Map LISTA_NOMINA;
  private long idParametroGuarderia;
  private TipoPersonal tipoPersonal;
  private double sueldoMaximo;
  private int edadMaxima;
  private double montoPatron;
  private String pagoNomina;
  private String quienCobra;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "edadMaxima", "idParametroGuarderia", "montoPatron", "pagoNomina", "quienCobra", "sueldoMaximo", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.ParametroGuarderia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroGuarderia());

    LISTA_QUIENCOBRA = 
      new LinkedHashMap();
    LISTA_NOMINA = 
      new LinkedHashMap();

    LISTA_QUIENCOBRA.put("M", "MADRE");
    LISTA_QUIENCOBRA.put("P", "PADRE");
    LISTA_QUIENCOBRA.put("A", "AMBOS");
    LISTA_NOMINA.put("S", "SI");
    LISTA_NOMINA.put("N", "NO");
  }

  public ParametroGuarderia()
  {
    jdoSetsueldoMaximo(this, 0.0D);

    jdoSetedadMaxima(this, 5);

    jdoSetmontoPatron(this, 0.0D);

    jdoSetpagoNomina(this, "N");

    jdoSetquienCobra(this, "M");
  }
  public String toString() {
    return jdoGettipoPersonal(this).getNombre();
  }

  public int getEdadMaxima()
  {
    return jdoGetedadMaxima(this);
  }

  public long getIdParametroGuarderia()
  {
    return jdoGetidParametroGuarderia(this);
  }

  public String getPagoNomina()
  {
    return jdoGetpagoNomina(this);
  }

  public String getQuienCobra()
  {
    return jdoGetquienCobra(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setEdadMaxima(int i)
  {
    jdoSetedadMaxima(this, i);
  }

  public void setIdParametroGuarderia(long l)
  {
    jdoSetidParametroGuarderia(this, l);
  }

  public void setPagoNomina(String string)
  {
    jdoSetpagoNomina(this, string);
  }

  public void setQuienCobra(String string)
  {
    jdoSetquienCobra(this, string);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }
  public double getMontoPatron() {
    return jdoGetmontoPatron(this);
  }
  public void setMontoPatron(double montoPatron) {
    jdoSetmontoPatron(this, montoPatron);
  }
  public double getSueldoMaximo() {
    return jdoGetsueldoMaximo(this);
  }
  public void setSueldoMaximo(double sueldoMaximo) {
    jdoSetsueldoMaximo(this, sueldoMaximo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroGuarderia localParametroGuarderia = new ParametroGuarderia();
    localParametroGuarderia.jdoFlags = 1;
    localParametroGuarderia.jdoStateManager = paramStateManager;
    return localParametroGuarderia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroGuarderia localParametroGuarderia = new ParametroGuarderia();
    localParametroGuarderia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroGuarderia.jdoFlags = 1;
    localParametroGuarderia.jdoStateManager = paramStateManager;
    return localParametroGuarderia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadMaxima);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroGuarderia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPatron);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.pagoNomina);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.quienCobra);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoMaximo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadMaxima = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroGuarderia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPatron = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pagoNomina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.quienCobra = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoMaximo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroGuarderia paramParametroGuarderia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.edadMaxima = paramParametroGuarderia.edadMaxima;
      return;
    case 1:
      if (paramParametroGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroGuarderia = paramParametroGuarderia.idParametroGuarderia;
      return;
    case 2:
      if (paramParametroGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.montoPatron = paramParametroGuarderia.montoPatron;
      return;
    case 3:
      if (paramParametroGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.pagoNomina = paramParametroGuarderia.pagoNomina;
      return;
    case 4:
      if (paramParametroGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.quienCobra = paramParametroGuarderia.quienCobra;
      return;
    case 5:
      if (paramParametroGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoMaximo = paramParametroGuarderia.sueldoMaximo;
      return;
    case 6:
      if (paramParametroGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramParametroGuarderia.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroGuarderia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroGuarderia localParametroGuarderia = (ParametroGuarderia)paramObject;
    if (localParametroGuarderia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroGuarderia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroGuarderiaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroGuarderiaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroGuarderiaPK))
      throw new IllegalArgumentException("arg1");
    ParametroGuarderiaPK localParametroGuarderiaPK = (ParametroGuarderiaPK)paramObject;
    localParametroGuarderiaPK.idParametroGuarderia = this.idParametroGuarderia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroGuarderiaPK))
      throw new IllegalArgumentException("arg1");
    ParametroGuarderiaPK localParametroGuarderiaPK = (ParametroGuarderiaPK)paramObject;
    this.idParametroGuarderia = localParametroGuarderiaPK.idParametroGuarderia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroGuarderiaPK))
      throw new IllegalArgumentException("arg2");
    ParametroGuarderiaPK localParametroGuarderiaPK = (ParametroGuarderiaPK)paramObject;
    localParametroGuarderiaPK.idParametroGuarderia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroGuarderiaPK))
      throw new IllegalArgumentException("arg2");
    ParametroGuarderiaPK localParametroGuarderiaPK = (ParametroGuarderiaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localParametroGuarderiaPK.idParametroGuarderia);
  }

  private static final int jdoGetedadMaxima(ParametroGuarderia paramParametroGuarderia)
  {
    if (paramParametroGuarderia.jdoFlags <= 0)
      return paramParametroGuarderia.edadMaxima;
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGuarderia.edadMaxima;
    if (localStateManager.isLoaded(paramParametroGuarderia, jdoInheritedFieldCount + 0))
      return paramParametroGuarderia.edadMaxima;
    return localStateManager.getIntField(paramParametroGuarderia, jdoInheritedFieldCount + 0, paramParametroGuarderia.edadMaxima);
  }

  private static final void jdoSetedadMaxima(ParametroGuarderia paramParametroGuarderia, int paramInt)
  {
    if (paramParametroGuarderia.jdoFlags == 0)
    {
      paramParametroGuarderia.edadMaxima = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGuarderia.edadMaxima = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroGuarderia, jdoInheritedFieldCount + 0, paramParametroGuarderia.edadMaxima, paramInt);
  }

  private static final long jdoGetidParametroGuarderia(ParametroGuarderia paramParametroGuarderia)
  {
    return paramParametroGuarderia.idParametroGuarderia;
  }

  private static final void jdoSetidParametroGuarderia(ParametroGuarderia paramParametroGuarderia, long paramLong)
  {
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGuarderia.idParametroGuarderia = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroGuarderia, jdoInheritedFieldCount + 1, paramParametroGuarderia.idParametroGuarderia, paramLong);
  }

  private static final double jdoGetmontoPatron(ParametroGuarderia paramParametroGuarderia)
  {
    if (paramParametroGuarderia.jdoFlags <= 0)
      return paramParametroGuarderia.montoPatron;
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGuarderia.montoPatron;
    if (localStateManager.isLoaded(paramParametroGuarderia, jdoInheritedFieldCount + 2))
      return paramParametroGuarderia.montoPatron;
    return localStateManager.getDoubleField(paramParametroGuarderia, jdoInheritedFieldCount + 2, paramParametroGuarderia.montoPatron);
  }

  private static final void jdoSetmontoPatron(ParametroGuarderia paramParametroGuarderia, double paramDouble)
  {
    if (paramParametroGuarderia.jdoFlags == 0)
    {
      paramParametroGuarderia.montoPatron = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGuarderia.montoPatron = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGuarderia, jdoInheritedFieldCount + 2, paramParametroGuarderia.montoPatron, paramDouble);
  }

  private static final String jdoGetpagoNomina(ParametroGuarderia paramParametroGuarderia)
  {
    if (paramParametroGuarderia.jdoFlags <= 0)
      return paramParametroGuarderia.pagoNomina;
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGuarderia.pagoNomina;
    if (localStateManager.isLoaded(paramParametroGuarderia, jdoInheritedFieldCount + 3))
      return paramParametroGuarderia.pagoNomina;
    return localStateManager.getStringField(paramParametroGuarderia, jdoInheritedFieldCount + 3, paramParametroGuarderia.pagoNomina);
  }

  private static final void jdoSetpagoNomina(ParametroGuarderia paramParametroGuarderia, String paramString)
  {
    if (paramParametroGuarderia.jdoFlags == 0)
    {
      paramParametroGuarderia.pagoNomina = paramString;
      return;
    }
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGuarderia.pagoNomina = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroGuarderia, jdoInheritedFieldCount + 3, paramParametroGuarderia.pagoNomina, paramString);
  }

  private static final String jdoGetquienCobra(ParametroGuarderia paramParametroGuarderia)
  {
    if (paramParametroGuarderia.jdoFlags <= 0)
      return paramParametroGuarderia.quienCobra;
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGuarderia.quienCobra;
    if (localStateManager.isLoaded(paramParametroGuarderia, jdoInheritedFieldCount + 4))
      return paramParametroGuarderia.quienCobra;
    return localStateManager.getStringField(paramParametroGuarderia, jdoInheritedFieldCount + 4, paramParametroGuarderia.quienCobra);
  }

  private static final void jdoSetquienCobra(ParametroGuarderia paramParametroGuarderia, String paramString)
  {
    if (paramParametroGuarderia.jdoFlags == 0)
    {
      paramParametroGuarderia.quienCobra = paramString;
      return;
    }
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGuarderia.quienCobra = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroGuarderia, jdoInheritedFieldCount + 4, paramParametroGuarderia.quienCobra, paramString);
  }

  private static final double jdoGetsueldoMaximo(ParametroGuarderia paramParametroGuarderia)
  {
    if (paramParametroGuarderia.jdoFlags <= 0)
      return paramParametroGuarderia.sueldoMaximo;
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGuarderia.sueldoMaximo;
    if (localStateManager.isLoaded(paramParametroGuarderia, jdoInheritedFieldCount + 5))
      return paramParametroGuarderia.sueldoMaximo;
    return localStateManager.getDoubleField(paramParametroGuarderia, jdoInheritedFieldCount + 5, paramParametroGuarderia.sueldoMaximo);
  }

  private static final void jdoSetsueldoMaximo(ParametroGuarderia paramParametroGuarderia, double paramDouble)
  {
    if (paramParametroGuarderia.jdoFlags == 0)
    {
      paramParametroGuarderia.sueldoMaximo = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGuarderia.sueldoMaximo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGuarderia, jdoInheritedFieldCount + 5, paramParametroGuarderia.sueldoMaximo, paramDouble);
  }

  private static final TipoPersonal jdoGettipoPersonal(ParametroGuarderia paramParametroGuarderia)
  {
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGuarderia.tipoPersonal;
    if (localStateManager.isLoaded(paramParametroGuarderia, jdoInheritedFieldCount + 6))
      return paramParametroGuarderia.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramParametroGuarderia, jdoInheritedFieldCount + 6, paramParametroGuarderia.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ParametroGuarderia paramParametroGuarderia, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramParametroGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGuarderia.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramParametroGuarderia, jdoInheritedFieldCount + 6, paramParametroGuarderia.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}