package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class SubtipoCredencialPK
  implements Serializable
{
  public long idSubtipoCredencial;

  public SubtipoCredencialPK()
  {
  }

  public SubtipoCredencialPK(long idSubtipoCredencial)
  {
    this.idSubtipoCredencial = idSubtipoCredencial;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SubtipoCredencialPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SubtipoCredencialPK thatPK)
  {
    return 
      this.idSubtipoCredencial == thatPK.idSubtipoCredencial;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSubtipoCredencial)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSubtipoCredencial);
  }
}