package sigefirrhh.base.bienestar;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class PlanPolizaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PlanPolizaForm.class.getName());
  private PlanPoliza planPoliza;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private boolean showPlanPolizaByPoliza;
  private boolean showPlanPolizaByCodPlanPoliza;
  private boolean showPlanPolizaByNombre;
  private String findSelectPoliza;
  private String findCodPlanPoliza;
  private String findNombre;
  private Collection findColPoliza;
  private Collection colPoliza;
  private String selectPoliza;
  private Object stateResultPlanPolizaByPoliza = null;

  private Object stateResultPlanPolizaByCodPlanPoliza = null;

  private Object stateResultPlanPolizaByNombre = null;

  public String getFindSelectPoliza()
  {
    return this.findSelectPoliza;
  }
  public void setFindSelectPoliza(String valPoliza) {
    this.findSelectPoliza = valPoliza;
  }

  public Collection getFindColPoliza() {
    Collection col = new ArrayList();
    if (this.findColPoliza != null) {
      Iterator iterator = this.findColPoliza.iterator();
      Poliza poliza = null;
      while (iterator.hasNext()) {
        poliza = (Poliza)iterator.next();
        col.add(new SelectItem(
          String.valueOf(poliza.getIdPoliza()), 
          poliza.toString()));
      }
    }
    return col;
  }
  public String getFindCodPlanPoliza() {
    return this.findCodPlanPoliza;
  }
  public void setFindCodPlanPoliza(String findCodPlanPoliza) {
    this.findCodPlanPoliza = findCodPlanPoliza;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectPoliza()
  {
    return this.selectPoliza;
  }
  public void setSelectPoliza(String valPoliza) {
    Iterator iterator = this.colPoliza.iterator();
    Poliza poliza = null;
    this.planPoliza.setPoliza(null);
    while (iterator.hasNext()) {
      poliza = (Poliza)iterator.next();
      if (String.valueOf(poliza.getIdPoliza()).equals(
        valPoliza)) {
        this.planPoliza.setPoliza(
          poliza);
        break;
      }
    }
    this.selectPoliza = valPoliza;
  }
  public Collection getResult() {
    return this.result;
  }

  public PlanPoliza getPlanPoliza() {
    if (this.planPoliza == null) {
      this.planPoliza = new PlanPoliza();
    }
    return this.planPoliza;
  }

  public PlanPolizaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPoliza()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPoliza.iterator();
    Poliza poliza = null;
    while (iterator.hasNext()) {
      poliza = (Poliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(poliza.getIdPoliza()), 
        poliza.toString()));
    }
    return col;
  }

  public Collection getListBeneficiarios()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanPoliza.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListPrimaUnica() {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanPoliza.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListExceso()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanPoliza.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColPoliza = 
        this.bienestarFacade.findPolizaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colPoliza = 
        this.bienestarFacade.findPolizaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPlanPolizaByPoliza()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findPlanPolizaByPoliza(Long.valueOf(this.findSelectPoliza).longValue());
      this.showPlanPolizaByPoliza = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPlanPolizaByPoliza)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPoliza = null;
    this.findCodPlanPoliza = null;
    this.findNombre = null;

    return null;
  }

  public String findPlanPolizaByCodPlanPoliza()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findPlanPolizaByCodPlanPoliza(this.findCodPlanPoliza);
      this.showPlanPolizaByCodPlanPoliza = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPlanPolizaByCodPlanPoliza)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPoliza = null;
    this.findCodPlanPoliza = null;
    this.findNombre = null;

    return null;
  }

  public String findPlanPolizaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findPlanPolizaByNombre(this.findNombre);
      this.showPlanPolizaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPlanPolizaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPoliza = null;
    this.findCodPlanPoliza = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowPlanPolizaByPoliza() {
    return this.showPlanPolizaByPoliza;
  }
  public boolean isShowPlanPolizaByCodPlanPoliza() {
    return this.showPlanPolizaByCodPlanPoliza;
  }
  public boolean isShowPlanPolizaByNombre() {
    return this.showPlanPolizaByNombre;
  }

  public String selectPlanPoliza()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPoliza = null;

    long idPlanPoliza = 
      Long.parseLong((String)requestParameterMap.get("idPlanPoliza"));
    try
    {
      this.planPoliza = 
        this.bienestarFacade.findPlanPolizaById(
        idPlanPoliza);
      if (this.planPoliza.getPoliza() != null) {
        this.selectPoliza = 
          String.valueOf(this.planPoliza.getPoliza().getIdPoliza());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.planPoliza = null;
    this.showPlanPolizaByPoliza = false;
    this.showPlanPolizaByCodPlanPoliza = false;
    this.showPlanPolizaByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.bienestarFacade.addPlanPoliza(
          this.planPoliza);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.bienestarFacade.updatePlanPoliza(
          this.planPoliza);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.bienestarFacade.deletePlanPoliza(
        this.planPoliza);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.planPoliza = new PlanPoliza();

    this.selectPoliza = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.planPoliza.setIdPlanPoliza(identityGenerator.getNextSequenceNumber("sigefirrhh.base.bienestar.PlanPoliza"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.planPoliza = new PlanPoliza();
    return "cancel";
  }

  public boolean isShowMontoPrimaUnicaAux()
  {
    try
    {
      return this.planPoliza.getPrimaUnica().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowCoberturaUnicaAux()
  {
    try {
      return this.planPoliza.getPrimaUnica().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}