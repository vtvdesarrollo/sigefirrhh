package sigefirrhh.base.bienestar;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class SubtipoDotacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SubtipoDotacionForm.class.getName());
  private SubtipoDotacion subtipoDotacion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private boolean showSubtipoDotacionByTipoDotacion;
  private String findSelectTipoDotacion;
  private Collection findColTipoDotacion;
  private Collection colTipoDotacion;
  private String selectTipoDotacion;
  private Object stateResultSubtipoDotacionByTipoDotacion = null;

  public String getFindSelectTipoDotacion()
  {
    return this.findSelectTipoDotacion;
  }
  public void setFindSelectTipoDotacion(String valTipoDotacion) {
    this.findSelectTipoDotacion = valTipoDotacion;
  }

  public Collection getFindColTipoDotacion() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoDotacion.iterator();
    TipoDotacion tipoDotacion = null;
    while (iterator.hasNext()) {
      tipoDotacion = (TipoDotacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoDotacion.getIdTipoDotacion()), 
        tipoDotacion.toString()));
    }
    return col;
  }

  public String getSelectTipoDotacion()
  {
    return this.selectTipoDotacion;
  }
  public void setSelectTipoDotacion(String valTipoDotacion) {
    Iterator iterator = this.colTipoDotacion.iterator();
    TipoDotacion tipoDotacion = null;
    this.subtipoDotacion.setTipoDotacion(null);
    while (iterator.hasNext()) {
      tipoDotacion = (TipoDotacion)iterator.next();
      if (String.valueOf(tipoDotacion.getIdTipoDotacion()).equals(
        valTipoDotacion)) {
        this.subtipoDotacion.setTipoDotacion(
          tipoDotacion);
        break;
      }
    }
    this.selectTipoDotacion = valTipoDotacion;
  }
  public Collection getResult() {
    return this.result;
  }

  public SubtipoDotacion getSubtipoDotacion() {
    if (this.subtipoDotacion == null) {
      this.subtipoDotacion = new SubtipoDotacion();
    }
    return this.subtipoDotacion;
  }

  public SubtipoDotacionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoDotacion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoDotacion.iterator();
    TipoDotacion tipoDotacion = null;
    while (iterator.hasNext()) {
      tipoDotacion = (TipoDotacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoDotacion.getIdTipoDotacion()), 
        tipoDotacion.toString()));
    }
    return col;
  }

  public Collection getListSexo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = SubtipoDotacion.LISTA_SEXO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoDotacion = 
        this.bienestarFacade.findAllTipoDotacion();

      this.colTipoDotacion = 
        this.bienestarFacade.findAllTipoDotacion();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSubtipoDotacionByTipoDotacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findSubtipoDotacionByTipoDotacion(Long.valueOf(this.findSelectTipoDotacion).longValue());
      this.showSubtipoDotacionByTipoDotacion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSubtipoDotacionByTipoDotacion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoDotacion = null;

    return null;
  }

  public boolean isShowSubtipoDotacionByTipoDotacion() {
    return this.showSubtipoDotacionByTipoDotacion;
  }

  public String selectSubtipoDotacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoDotacion = null;

    long idSubtipoDotacion = 
      Long.parseLong((String)requestParameterMap.get("idSubtipoDotacion"));
    try
    {
      this.subtipoDotacion = 
        this.bienestarFacade.findSubtipoDotacionById(
        idSubtipoDotacion);
      if (this.subtipoDotacion.getTipoDotacion() != null) {
        this.selectTipoDotacion = 
          String.valueOf(this.subtipoDotacion.getTipoDotacion().getIdTipoDotacion());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.subtipoDotacion = null;
    this.showSubtipoDotacionByTipoDotacion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.bienestarFacade.addSubtipoDotacion(
          this.subtipoDotacion);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.bienestarFacade.updateSubtipoDotacion(
          this.subtipoDotacion);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.bienestarFacade.deleteSubtipoDotacion(
        this.subtipoDotacion);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.subtipoDotacion = new SubtipoDotacion();

    this.selectTipoDotacion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.subtipoDotacion.setIdSubtipoDotacion(identityGenerator.getNextSequenceNumber("sigefirrhh.base.bienestar.SubtipoDotacion"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.subtipoDotacion = new SubtipoDotacion();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}