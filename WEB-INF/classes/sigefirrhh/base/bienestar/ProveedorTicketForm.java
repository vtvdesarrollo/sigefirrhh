package sigefirrhh.base.bienestar;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class ProveedorTicketForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ProveedorTicketForm.class.getName());
  private ProveedorTicket proveedorTicket;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private boolean showProveedorTicketByCodProveedorTicket;
  private boolean showProveedorTicketByNombre;
  private String findCodProveedorTicket;
  private String findNombre;
  private Object stateResultProveedorTicketByCodProveedorTicket = null;

  private Object stateResultProveedorTicketByNombre = null;

  public String getFindCodProveedorTicket()
  {
    return this.findCodProveedorTicket;
  }
  public void setFindCodProveedorTicket(String findCodProveedorTicket) {
    this.findCodProveedorTicket = findCodProveedorTicket;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public ProveedorTicket getProveedorTicket() {
    if (this.proveedorTicket == null) {
      this.proveedorTicket = new ProveedorTicket();
    }
    return this.proveedorTicket;
  }

  public ProveedorTicketForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findProveedorTicketByCodProveedorTicket()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findProveedorTicketByCodProveedorTicket(this.findCodProveedorTicket);
      this.showProveedorTicketByCodProveedorTicket = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showProveedorTicketByCodProveedorTicket)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodProveedorTicket = null;
    this.findNombre = null;

    return null;
  }

  public String findProveedorTicketByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findProveedorTicketByNombre(this.findNombre);
      this.showProveedorTicketByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showProveedorTicketByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodProveedorTicket = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowProveedorTicketByCodProveedorTicket() {
    return this.showProveedorTicketByCodProveedorTicket;
  }
  public boolean isShowProveedorTicketByNombre() {
    return this.showProveedorTicketByNombre;
  }

  public String selectProveedorTicket()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idProveedorTicket = 
      Long.parseLong((String)requestParameterMap.get("idProveedorTicket"));
    try
    {
      this.proveedorTicket = 
        this.bienestarFacade.findProveedorTicketById(
        idProveedorTicket);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.proveedorTicket = null;
    this.showProveedorTicketByCodProveedorTicket = false;
    this.showProveedorTicketByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.bienestarFacade.addProveedorTicket(
          this.proveedorTicket);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.bienestarFacade.updateProveedorTicket(
          this.proveedorTicket);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.bienestarFacade.deleteProveedorTicket(
        this.proveedorTicket);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.proveedorTicket = new ProveedorTicket();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.proveedorTicket.setIdProveedorTicket(identityGenerator.getNextSequenceNumber("sigefirrhh.base.bienestar.ProveedorTicket"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.proveedorTicket = new ProveedorTicket();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}