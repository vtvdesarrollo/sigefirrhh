package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class ParametroTicketPK
  implements Serializable
{
  public long idParametroTicket;

  public ParametroTicketPK()
  {
  }

  public ParametroTicketPK(long idParametroTicket)
  {
    this.idParametroTicket = idParametroTicket;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroTicketPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroTicketPK thatPK)
  {
    return 
      this.idParametroTicket == thatPK.idParametroTicket;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroTicket)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroTicket);
  }
}