package sigefirrhh.base.bienestar;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class DenominacionTicketForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(DenominacionTicketForm.class.getName());
  private DenominacionTicket denominacionTicket;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private boolean showDenominacionTicketByTipoPersonal;
  private String findSelectTipoPersonal;
  private Collection findColTipoPersonal;
  private Collection colTipoPersonal;
  private Collection colProveedorTicket;
  private String selectTipoPersonal;
  private String selectProveedorTicket;
  private Object stateResultDenominacionTicketByTipoPersonal = null;

  public String getFindSelectTipoPersonal()
  {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.denominacionTicket.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.denominacionTicket.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectProveedorTicket() {
    return this.selectProveedorTicket;
  }
  public void setSelectProveedorTicket(String valProveedorTicket) {
    Iterator iterator = this.colProveedorTicket.iterator();
    ProveedorTicket proveedorTicket = null;
    this.denominacionTicket.setProveedorTicket(null);
    while (iterator.hasNext()) {
      proveedorTicket = (ProveedorTicket)iterator.next();
      if (String.valueOf(proveedorTicket.getIdProveedorTicket()).equals(
        valProveedorTicket)) {
        this.denominacionTicket.setProveedorTicket(
          proveedorTicket);
        break;
      }
    }
    this.selectProveedorTicket = valProveedorTicket;
  }
  public Collection getResult() {
    return this.result;
  }

  public DenominacionTicket getDenominacionTicket() {
    if (this.denominacionTicket == null) {
      this.denominacionTicket = new DenominacionTicket();
    }
    return this.denominacionTicket;
  }

  public DenominacionTicketForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColProveedorTicket()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colProveedorTicket.iterator();
    ProveedorTicket proveedorTicket = null;
    while (iterator.hasNext()) {
      proveedorTicket = (ProveedorTicket)iterator.next();
      col.add(new SelectItem(
        String.valueOf(proveedorTicket.getIdProveedorTicket()), 
        proveedorTicket.toString()));
    }
    return col;
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = DenominacionTicket.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colProveedorTicket = 
        this.bienestarFacade.findAllProveedorTicket();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findDenominacionTicketByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findDenominacionTicketByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showDenominacionTicketByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDenominacionTicketByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;

    return null;
  }

  public boolean isShowDenominacionTicketByTipoPersonal() {
    return this.showDenominacionTicketByTipoPersonal;
  }

  public String selectDenominacionTicket()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectProveedorTicket = null;

    long idDenominacionTicket = 
      Long.parseLong((String)requestParameterMap.get("idDenominacionTicket"));
    try
    {
      this.denominacionTicket = 
        this.bienestarFacade.findDenominacionTicketById(
        idDenominacionTicket);
      if (this.denominacionTicket.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.denominacionTicket.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.denominacionTicket.getProveedorTicket() != null) {
        this.selectProveedorTicket = 
          String.valueOf(this.denominacionTicket.getProveedorTicket().getIdProveedorTicket());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.denominacionTicket = null;
    this.showDenominacionTicketByTipoPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.bienestarFacade.addDenominacionTicket(
          this.denominacionTicket);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.bienestarFacade.updateDenominacionTicket(
          this.denominacionTicket);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.bienestarFacade.deleteDenominacionTicket(
        this.denominacionTicket);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.denominacionTicket = new DenominacionTicket();

    this.selectTipoPersonal = null;

    this.selectProveedorTicket = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.denominacionTicket.setIdDenominacionTicket(identityGenerator.getNextSequenceNumber("sigefirrhh.base.bienestar.DenominacionTicket"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.denominacionTicket = new DenominacionTicket();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}