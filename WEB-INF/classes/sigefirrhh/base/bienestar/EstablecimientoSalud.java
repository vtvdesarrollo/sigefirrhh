package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.ubicacion.Ciudad;

public class EstablecimientoSalud
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SECTOR;
  protected static final Map LISTA_SSO;
  private long idEstablecimientoSalud;
  private String codEstablecimientoSalud;
  private String nombre;
  private String direccion;
  private String sector;
  private String sso;
  private String telefono;
  private Ciudad ciudad;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "ciudad", "codEstablecimientoSalud", "direccion", "idEstablecimientoSalud", "nombre", "sector", "sso", "telefono" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 26, 21, 21, 24, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.EstablecimientoSalud"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new EstablecimientoSalud());

    LISTA_SECTOR = 
      new LinkedHashMap();
    LISTA_SSO = 
      new LinkedHashMap();

    LISTA_SECTOR.put("P", "PRIVADO");
    LISTA_SECTOR.put("U", "PUBLICO");
    LISTA_SSO.put("S", "SI");
    LISTA_SSO.put("N", "NO");
  }

  public String toString()
  {
    return jdoGetnombre(this) + " " + 
      jdoGetcodEstablecimientoSalud(this);
  }
  public Ciudad getCiudad() {
    return jdoGetciudad(this);
  }
  public void setCiudad(Ciudad ciudad) {
    jdoSetciudad(this, ciudad);
  }
  public String getCodEstablecimientoSalud() {
    return jdoGetcodEstablecimientoSalud(this);
  }
  public void setCodEstablecimientoSalud(String codEstablecimientoSalud) {
    jdoSetcodEstablecimientoSalud(this, codEstablecimientoSalud);
  }
  public String getDireccion() {
    return jdoGetdireccion(this);
  }
  public void setDireccion(String direccion) {
    jdoSetdireccion(this, direccion);
  }
  public long getIdEstablecimientoSalud() {
    return jdoGetidEstablecimientoSalud(this);
  }
  public void setIdEstablecimientoSalud(long idEstablecimientoSalud) {
    jdoSetidEstablecimientoSalud(this, idEstablecimientoSalud);
  }
  public String getNombre() {
    return jdoGetnombre(this);
  }
  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }
  public String getSector() {
    return jdoGetsector(this);
  }
  public void setSector(String sector) {
    jdoSetsector(this, sector);
  }
  public String getSso() {
    return jdoGetsso(this);
  }
  public void setSso(String sso) {
    jdoSetsso(this, sso);
  }
  public String getTelefono() {
    return jdoGettelefono(this);
  }
  public void setTelefono(String telefono) {
    jdoSettelefono(this, telefono);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    EstablecimientoSalud localEstablecimientoSalud = new EstablecimientoSalud();
    localEstablecimientoSalud.jdoFlags = 1;
    localEstablecimientoSalud.jdoStateManager = paramStateManager;
    return localEstablecimientoSalud;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    EstablecimientoSalud localEstablecimientoSalud = new EstablecimientoSalud();
    localEstablecimientoSalud.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEstablecimientoSalud.jdoFlags = 1;
    localEstablecimientoSalud.jdoStateManager = paramStateManager;
    return localEstablecimientoSalud;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codEstablecimientoSalud);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.direccion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEstablecimientoSalud);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sector);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sso);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codEstablecimientoSalud = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.direccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEstablecimientoSalud = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sector = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(EstablecimientoSalud paramEstablecimientoSalud, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEstablecimientoSalud == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramEstablecimientoSalud.ciudad;
      return;
    case 1:
      if (paramEstablecimientoSalud == null)
        throw new IllegalArgumentException("arg1");
      this.codEstablecimientoSalud = paramEstablecimientoSalud.codEstablecimientoSalud;
      return;
    case 2:
      if (paramEstablecimientoSalud == null)
        throw new IllegalArgumentException("arg1");
      this.direccion = paramEstablecimientoSalud.direccion;
      return;
    case 3:
      if (paramEstablecimientoSalud == null)
        throw new IllegalArgumentException("arg1");
      this.idEstablecimientoSalud = paramEstablecimientoSalud.idEstablecimientoSalud;
      return;
    case 4:
      if (paramEstablecimientoSalud == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramEstablecimientoSalud.nombre;
      return;
    case 5:
      if (paramEstablecimientoSalud == null)
        throw new IllegalArgumentException("arg1");
      this.sector = paramEstablecimientoSalud.sector;
      return;
    case 6:
      if (paramEstablecimientoSalud == null)
        throw new IllegalArgumentException("arg1");
      this.sso = paramEstablecimientoSalud.sso;
      return;
    case 7:
      if (paramEstablecimientoSalud == null)
        throw new IllegalArgumentException("arg1");
      this.telefono = paramEstablecimientoSalud.telefono;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof EstablecimientoSalud))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    EstablecimientoSalud localEstablecimientoSalud = (EstablecimientoSalud)paramObject;
    if (localEstablecimientoSalud.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEstablecimientoSalud, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EstablecimientoSaludPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EstablecimientoSaludPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EstablecimientoSaludPK))
      throw new IllegalArgumentException("arg1");
    EstablecimientoSaludPK localEstablecimientoSaludPK = (EstablecimientoSaludPK)paramObject;
    localEstablecimientoSaludPK.idEstablecimientoSalud = this.idEstablecimientoSalud;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EstablecimientoSaludPK))
      throw new IllegalArgumentException("arg1");
    EstablecimientoSaludPK localEstablecimientoSaludPK = (EstablecimientoSaludPK)paramObject;
    this.idEstablecimientoSalud = localEstablecimientoSaludPK.idEstablecimientoSalud;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EstablecimientoSaludPK))
      throw new IllegalArgumentException("arg2");
    EstablecimientoSaludPK localEstablecimientoSaludPK = (EstablecimientoSaludPK)paramObject;
    localEstablecimientoSaludPK.idEstablecimientoSalud = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EstablecimientoSaludPK))
      throw new IllegalArgumentException("arg2");
    EstablecimientoSaludPK localEstablecimientoSaludPK = (EstablecimientoSaludPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localEstablecimientoSaludPK.idEstablecimientoSalud);
  }

  private static final Ciudad jdoGetciudad(EstablecimientoSalud paramEstablecimientoSalud)
  {
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
      return paramEstablecimientoSalud.ciudad;
    if (localStateManager.isLoaded(paramEstablecimientoSalud, jdoInheritedFieldCount + 0))
      return paramEstablecimientoSalud.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramEstablecimientoSalud, jdoInheritedFieldCount + 0, paramEstablecimientoSalud.ciudad);
  }

  private static final void jdoSetciudad(EstablecimientoSalud paramEstablecimientoSalud, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstablecimientoSalud.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramEstablecimientoSalud, jdoInheritedFieldCount + 0, paramEstablecimientoSalud.ciudad, paramCiudad);
  }

  private static final String jdoGetcodEstablecimientoSalud(EstablecimientoSalud paramEstablecimientoSalud)
  {
    if (paramEstablecimientoSalud.jdoFlags <= 0)
      return paramEstablecimientoSalud.codEstablecimientoSalud;
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
      return paramEstablecimientoSalud.codEstablecimientoSalud;
    if (localStateManager.isLoaded(paramEstablecimientoSalud, jdoInheritedFieldCount + 1))
      return paramEstablecimientoSalud.codEstablecimientoSalud;
    return localStateManager.getStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 1, paramEstablecimientoSalud.codEstablecimientoSalud);
  }

  private static final void jdoSetcodEstablecimientoSalud(EstablecimientoSalud paramEstablecimientoSalud, String paramString)
  {
    if (paramEstablecimientoSalud.jdoFlags == 0)
    {
      paramEstablecimientoSalud.codEstablecimientoSalud = paramString;
      return;
    }
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstablecimientoSalud.codEstablecimientoSalud = paramString;
      return;
    }
    localStateManager.setStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 1, paramEstablecimientoSalud.codEstablecimientoSalud, paramString);
  }

  private static final String jdoGetdireccion(EstablecimientoSalud paramEstablecimientoSalud)
  {
    if (paramEstablecimientoSalud.jdoFlags <= 0)
      return paramEstablecimientoSalud.direccion;
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
      return paramEstablecimientoSalud.direccion;
    if (localStateManager.isLoaded(paramEstablecimientoSalud, jdoInheritedFieldCount + 2))
      return paramEstablecimientoSalud.direccion;
    return localStateManager.getStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 2, paramEstablecimientoSalud.direccion);
  }

  private static final void jdoSetdireccion(EstablecimientoSalud paramEstablecimientoSalud, String paramString)
  {
    if (paramEstablecimientoSalud.jdoFlags == 0)
    {
      paramEstablecimientoSalud.direccion = paramString;
      return;
    }
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstablecimientoSalud.direccion = paramString;
      return;
    }
    localStateManager.setStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 2, paramEstablecimientoSalud.direccion, paramString);
  }

  private static final long jdoGetidEstablecimientoSalud(EstablecimientoSalud paramEstablecimientoSalud)
  {
    return paramEstablecimientoSalud.idEstablecimientoSalud;
  }

  private static final void jdoSetidEstablecimientoSalud(EstablecimientoSalud paramEstablecimientoSalud, long paramLong)
  {
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstablecimientoSalud.idEstablecimientoSalud = paramLong;
      return;
    }
    localStateManager.setLongField(paramEstablecimientoSalud, jdoInheritedFieldCount + 3, paramEstablecimientoSalud.idEstablecimientoSalud, paramLong);
  }

  private static final String jdoGetnombre(EstablecimientoSalud paramEstablecimientoSalud)
  {
    if (paramEstablecimientoSalud.jdoFlags <= 0)
      return paramEstablecimientoSalud.nombre;
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
      return paramEstablecimientoSalud.nombre;
    if (localStateManager.isLoaded(paramEstablecimientoSalud, jdoInheritedFieldCount + 4))
      return paramEstablecimientoSalud.nombre;
    return localStateManager.getStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 4, paramEstablecimientoSalud.nombre);
  }

  private static final void jdoSetnombre(EstablecimientoSalud paramEstablecimientoSalud, String paramString)
  {
    if (paramEstablecimientoSalud.jdoFlags == 0)
    {
      paramEstablecimientoSalud.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstablecimientoSalud.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 4, paramEstablecimientoSalud.nombre, paramString);
  }

  private static final String jdoGetsector(EstablecimientoSalud paramEstablecimientoSalud)
  {
    if (paramEstablecimientoSalud.jdoFlags <= 0)
      return paramEstablecimientoSalud.sector;
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
      return paramEstablecimientoSalud.sector;
    if (localStateManager.isLoaded(paramEstablecimientoSalud, jdoInheritedFieldCount + 5))
      return paramEstablecimientoSalud.sector;
    return localStateManager.getStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 5, paramEstablecimientoSalud.sector);
  }

  private static final void jdoSetsector(EstablecimientoSalud paramEstablecimientoSalud, String paramString)
  {
    if (paramEstablecimientoSalud.jdoFlags == 0)
    {
      paramEstablecimientoSalud.sector = paramString;
      return;
    }
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstablecimientoSalud.sector = paramString;
      return;
    }
    localStateManager.setStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 5, paramEstablecimientoSalud.sector, paramString);
  }

  private static final String jdoGetsso(EstablecimientoSalud paramEstablecimientoSalud)
  {
    if (paramEstablecimientoSalud.jdoFlags <= 0)
      return paramEstablecimientoSalud.sso;
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
      return paramEstablecimientoSalud.sso;
    if (localStateManager.isLoaded(paramEstablecimientoSalud, jdoInheritedFieldCount + 6))
      return paramEstablecimientoSalud.sso;
    return localStateManager.getStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 6, paramEstablecimientoSalud.sso);
  }

  private static final void jdoSetsso(EstablecimientoSalud paramEstablecimientoSalud, String paramString)
  {
    if (paramEstablecimientoSalud.jdoFlags == 0)
    {
      paramEstablecimientoSalud.sso = paramString;
      return;
    }
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstablecimientoSalud.sso = paramString;
      return;
    }
    localStateManager.setStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 6, paramEstablecimientoSalud.sso, paramString);
  }

  private static final String jdoGettelefono(EstablecimientoSalud paramEstablecimientoSalud)
  {
    if (paramEstablecimientoSalud.jdoFlags <= 0)
      return paramEstablecimientoSalud.telefono;
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
      return paramEstablecimientoSalud.telefono;
    if (localStateManager.isLoaded(paramEstablecimientoSalud, jdoInheritedFieldCount + 7))
      return paramEstablecimientoSalud.telefono;
    return localStateManager.getStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 7, paramEstablecimientoSalud.telefono);
  }

  private static final void jdoSettelefono(EstablecimientoSalud paramEstablecimientoSalud, String paramString)
  {
    if (paramEstablecimientoSalud.jdoFlags == 0)
    {
      paramEstablecimientoSalud.telefono = paramString;
      return;
    }
    StateManager localStateManager = paramEstablecimientoSalud.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstablecimientoSalud.telefono = paramString;
      return;
    }
    localStateManager.setStringField(paramEstablecimientoSalud, jdoInheritedFieldCount + 7, paramEstablecimientoSalud.telefono, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}