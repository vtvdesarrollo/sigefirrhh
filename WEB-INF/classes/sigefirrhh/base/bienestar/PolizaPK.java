package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class PolizaPK
  implements Serializable
{
  public long idPoliza;

  public PolizaPK()
  {
  }

  public PolizaPK(long idPoliza)
  {
    this.idPoliza = idPoliza;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PolizaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PolizaPK thatPK)
  {
    return 
      this.idPoliza == thatPK.idPoliza;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPoliza)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPoliza);
  }
}