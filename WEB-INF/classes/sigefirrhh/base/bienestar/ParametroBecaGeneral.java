package sigefirrhh.base.bienestar;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class ParametroBecaGeneral
  implements PersistenceCapable
{
  protected static final Map LISTA_QUIENCOBRA;
  protected static final Map LISTA_SINO;
  private long idParametroBecaGeneral;
  private TipoPersonal tipoPersonal;
  private int maximoTipoPersonal;
  private int maximoTrabajador;
  private String incluyeTrabajador;
  private String aprobacionSolicitud;
  private String quienCobra;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aprobacionSolicitud", "idParametroBecaGeneral", "incluyeTrabajador", "maximoTipoPersonal", "maximoTrabajador", "quienCobra", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.ParametroBecaGeneral"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroBecaGeneral());

    LISTA_QUIENCOBRA = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_QUIENCOBRA.put("M", "MADRE");
    LISTA_QUIENCOBRA.put("P", "PADRE");
    LISTA_QUIENCOBRA.put("A", "AMBOS");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public ParametroBecaGeneral()
  {
    jdoSetmaximoTipoPersonal(this, 0);

    jdoSetmaximoTrabajador(this, 0);

    jdoSetincluyeTrabajador(this, "N");

    jdoSetaprobacionSolicitud(this, "N");

    jdoSetquienCobra(this, "M");
  }
  public String toString() {
    return jdoGettipoPersonal(this).getNombre();
  }

  public int getMaximoTipoPersonal()
  {
    return jdoGetmaximoTipoPersonal(this);
  }
  public void setMaximoTipoPersonal(int maximoTipoPersonal) {
    jdoSetmaximoTipoPersonal(this, maximoTipoPersonal);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }
  public String getIncluyeTrabajador() {
    return jdoGetincluyeTrabajador(this);
  }
  public int getMaximoTrabajador() {
    return jdoGetmaximoTrabajador(this);
  }

  public String getQuienCobra() {
    return jdoGetquienCobra(this);
  }
  public void setIncluyeTrabajador(String incluyeTrabajador) {
    jdoSetincluyeTrabajador(this, incluyeTrabajador);
  }
  public void setMaximoTrabajador(int maximoTrabajador) {
    jdoSetmaximoTrabajador(this, maximoTrabajador);
  }

  public void setQuienCobra(String quienCobra) {
    jdoSetquienCobra(this, quienCobra);
  }

  public String getAprobacionSolicitud() {
    return jdoGetaprobacionSolicitud(this);
  }
  public void setAprobacionSolicitud(String aprobacionSolicitud) {
    jdoSetaprobacionSolicitud(this, aprobacionSolicitud);
  }
  public long getIdParametroBecaGeneral() {
    return jdoGetidParametroBecaGeneral(this);
  }
  public void setIdParametroBecaGeneral(long idParametroBecaGeneral) {
    jdoSetidParametroBecaGeneral(this, idParametroBecaGeneral);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroBecaGeneral localParametroBecaGeneral = new ParametroBecaGeneral();
    localParametroBecaGeneral.jdoFlags = 1;
    localParametroBecaGeneral.jdoStateManager = paramStateManager;
    return localParametroBecaGeneral;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroBecaGeneral localParametroBecaGeneral = new ParametroBecaGeneral();
    localParametroBecaGeneral.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroBecaGeneral.jdoFlags = 1;
    localParametroBecaGeneral.jdoStateManager = paramStateManager;
    return localParametroBecaGeneral;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionSolicitud);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroBecaGeneral);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.incluyeTrabajador);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.maximoTipoPersonal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.maximoTrabajador);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.quienCobra);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionSolicitud = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroBecaGeneral = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.incluyeTrabajador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.maximoTipoPersonal = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.maximoTrabajador = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.quienCobra = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroBecaGeneral paramParametroBecaGeneral, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroBecaGeneral == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionSolicitud = paramParametroBecaGeneral.aprobacionSolicitud;
      return;
    case 1:
      if (paramParametroBecaGeneral == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroBecaGeneral = paramParametroBecaGeneral.idParametroBecaGeneral;
      return;
    case 2:
      if (paramParametroBecaGeneral == null)
        throw new IllegalArgumentException("arg1");
      this.incluyeTrabajador = paramParametroBecaGeneral.incluyeTrabajador;
      return;
    case 3:
      if (paramParametroBecaGeneral == null)
        throw new IllegalArgumentException("arg1");
      this.maximoTipoPersonal = paramParametroBecaGeneral.maximoTipoPersonal;
      return;
    case 4:
      if (paramParametroBecaGeneral == null)
        throw new IllegalArgumentException("arg1");
      this.maximoTrabajador = paramParametroBecaGeneral.maximoTrabajador;
      return;
    case 5:
      if (paramParametroBecaGeneral == null)
        throw new IllegalArgumentException("arg1");
      this.quienCobra = paramParametroBecaGeneral.quienCobra;
      return;
    case 6:
      if (paramParametroBecaGeneral == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramParametroBecaGeneral.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroBecaGeneral))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroBecaGeneral localParametroBecaGeneral = (ParametroBecaGeneral)paramObject;
    if (localParametroBecaGeneral.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroBecaGeneral, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroBecaGeneralPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroBecaGeneralPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroBecaGeneralPK))
      throw new IllegalArgumentException("arg1");
    ParametroBecaGeneralPK localParametroBecaGeneralPK = (ParametroBecaGeneralPK)paramObject;
    localParametroBecaGeneralPK.idParametroBecaGeneral = this.idParametroBecaGeneral;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroBecaGeneralPK))
      throw new IllegalArgumentException("arg1");
    ParametroBecaGeneralPK localParametroBecaGeneralPK = (ParametroBecaGeneralPK)paramObject;
    this.idParametroBecaGeneral = localParametroBecaGeneralPK.idParametroBecaGeneral;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroBecaGeneralPK))
      throw new IllegalArgumentException("arg2");
    ParametroBecaGeneralPK localParametroBecaGeneralPK = (ParametroBecaGeneralPK)paramObject;
    localParametroBecaGeneralPK.idParametroBecaGeneral = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroBecaGeneralPK))
      throw new IllegalArgumentException("arg2");
    ParametroBecaGeneralPK localParametroBecaGeneralPK = (ParametroBecaGeneralPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localParametroBecaGeneralPK.idParametroBecaGeneral);
  }

  private static final String jdoGetaprobacionSolicitud(ParametroBecaGeneral paramParametroBecaGeneral)
  {
    if (paramParametroBecaGeneral.jdoFlags <= 0)
      return paramParametroBecaGeneral.aprobacionSolicitud;
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBecaGeneral.aprobacionSolicitud;
    if (localStateManager.isLoaded(paramParametroBecaGeneral, jdoInheritedFieldCount + 0))
      return paramParametroBecaGeneral.aprobacionSolicitud;
    return localStateManager.getStringField(paramParametroBecaGeneral, jdoInheritedFieldCount + 0, paramParametroBecaGeneral.aprobacionSolicitud);
  }

  private static final void jdoSetaprobacionSolicitud(ParametroBecaGeneral paramParametroBecaGeneral, String paramString)
  {
    if (paramParametroBecaGeneral.jdoFlags == 0)
    {
      paramParametroBecaGeneral.aprobacionSolicitud = paramString;
      return;
    }
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBecaGeneral.aprobacionSolicitud = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroBecaGeneral, jdoInheritedFieldCount + 0, paramParametroBecaGeneral.aprobacionSolicitud, paramString);
  }

  private static final long jdoGetidParametroBecaGeneral(ParametroBecaGeneral paramParametroBecaGeneral)
  {
    return paramParametroBecaGeneral.idParametroBecaGeneral;
  }

  private static final void jdoSetidParametroBecaGeneral(ParametroBecaGeneral paramParametroBecaGeneral, long paramLong)
  {
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBecaGeneral.idParametroBecaGeneral = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroBecaGeneral, jdoInheritedFieldCount + 1, paramParametroBecaGeneral.idParametroBecaGeneral, paramLong);
  }

  private static final String jdoGetincluyeTrabajador(ParametroBecaGeneral paramParametroBecaGeneral)
  {
    if (paramParametroBecaGeneral.jdoFlags <= 0)
      return paramParametroBecaGeneral.incluyeTrabajador;
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBecaGeneral.incluyeTrabajador;
    if (localStateManager.isLoaded(paramParametroBecaGeneral, jdoInheritedFieldCount + 2))
      return paramParametroBecaGeneral.incluyeTrabajador;
    return localStateManager.getStringField(paramParametroBecaGeneral, jdoInheritedFieldCount + 2, paramParametroBecaGeneral.incluyeTrabajador);
  }

  private static final void jdoSetincluyeTrabajador(ParametroBecaGeneral paramParametroBecaGeneral, String paramString)
  {
    if (paramParametroBecaGeneral.jdoFlags == 0)
    {
      paramParametroBecaGeneral.incluyeTrabajador = paramString;
      return;
    }
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBecaGeneral.incluyeTrabajador = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroBecaGeneral, jdoInheritedFieldCount + 2, paramParametroBecaGeneral.incluyeTrabajador, paramString);
  }

  private static final int jdoGetmaximoTipoPersonal(ParametroBecaGeneral paramParametroBecaGeneral)
  {
    if (paramParametroBecaGeneral.jdoFlags <= 0)
      return paramParametroBecaGeneral.maximoTipoPersonal;
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBecaGeneral.maximoTipoPersonal;
    if (localStateManager.isLoaded(paramParametroBecaGeneral, jdoInheritedFieldCount + 3))
      return paramParametroBecaGeneral.maximoTipoPersonal;
    return localStateManager.getIntField(paramParametroBecaGeneral, jdoInheritedFieldCount + 3, paramParametroBecaGeneral.maximoTipoPersonal);
  }

  private static final void jdoSetmaximoTipoPersonal(ParametroBecaGeneral paramParametroBecaGeneral, int paramInt)
  {
    if (paramParametroBecaGeneral.jdoFlags == 0)
    {
      paramParametroBecaGeneral.maximoTipoPersonal = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBecaGeneral.maximoTipoPersonal = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroBecaGeneral, jdoInheritedFieldCount + 3, paramParametroBecaGeneral.maximoTipoPersonal, paramInt);
  }

  private static final int jdoGetmaximoTrabajador(ParametroBecaGeneral paramParametroBecaGeneral)
  {
    if (paramParametroBecaGeneral.jdoFlags <= 0)
      return paramParametroBecaGeneral.maximoTrabajador;
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBecaGeneral.maximoTrabajador;
    if (localStateManager.isLoaded(paramParametroBecaGeneral, jdoInheritedFieldCount + 4))
      return paramParametroBecaGeneral.maximoTrabajador;
    return localStateManager.getIntField(paramParametroBecaGeneral, jdoInheritedFieldCount + 4, paramParametroBecaGeneral.maximoTrabajador);
  }

  private static final void jdoSetmaximoTrabajador(ParametroBecaGeneral paramParametroBecaGeneral, int paramInt)
  {
    if (paramParametroBecaGeneral.jdoFlags == 0)
    {
      paramParametroBecaGeneral.maximoTrabajador = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBecaGeneral.maximoTrabajador = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroBecaGeneral, jdoInheritedFieldCount + 4, paramParametroBecaGeneral.maximoTrabajador, paramInt);
  }

  private static final String jdoGetquienCobra(ParametroBecaGeneral paramParametroBecaGeneral)
  {
    if (paramParametroBecaGeneral.jdoFlags <= 0)
      return paramParametroBecaGeneral.quienCobra;
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBecaGeneral.quienCobra;
    if (localStateManager.isLoaded(paramParametroBecaGeneral, jdoInheritedFieldCount + 5))
      return paramParametroBecaGeneral.quienCobra;
    return localStateManager.getStringField(paramParametroBecaGeneral, jdoInheritedFieldCount + 5, paramParametroBecaGeneral.quienCobra);
  }

  private static final void jdoSetquienCobra(ParametroBecaGeneral paramParametroBecaGeneral, String paramString)
  {
    if (paramParametroBecaGeneral.jdoFlags == 0)
    {
      paramParametroBecaGeneral.quienCobra = paramString;
      return;
    }
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBecaGeneral.quienCobra = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroBecaGeneral, jdoInheritedFieldCount + 5, paramParametroBecaGeneral.quienCobra, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(ParametroBecaGeneral paramParametroBecaGeneral)
  {
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBecaGeneral.tipoPersonal;
    if (localStateManager.isLoaded(paramParametroBecaGeneral, jdoInheritedFieldCount + 6))
      return paramParametroBecaGeneral.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramParametroBecaGeneral, jdoInheritedFieldCount + 6, paramParametroBecaGeneral.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ParametroBecaGeneral paramParametroBecaGeneral, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramParametroBecaGeneral.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBecaGeneral.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramParametroBecaGeneral, jdoInheritedFieldCount + 6, paramParametroBecaGeneral.tipoPersonal, paramTipoPersonal);
  }
}