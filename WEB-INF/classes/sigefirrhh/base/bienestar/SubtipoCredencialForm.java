package sigefirrhh.base.bienestar;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class SubtipoCredencialForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SubtipoCredencialForm.class.getName());
  private SubtipoCredencial subtipoCredencial;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private boolean showSubtipoCredencialByTipoCredencial;
  private String findSelectTipoCredencial;
  private Collection findColTipoCredencial;
  private Collection colTipoCredencial;
  private String selectTipoCredencial;
  private Object stateResultSubtipoCredencialByTipoCredencial = null;

  public String getFindSelectTipoCredencial()
  {
    return this.findSelectTipoCredencial;
  }
  public void setFindSelectTipoCredencial(String valTipoCredencial) {
    this.findSelectTipoCredencial = valTipoCredencial;
  }

  public Collection getFindColTipoCredencial() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoCredencial.iterator();
    TipoCredencial tipoCredencial = null;
    while (iterator.hasNext()) {
      tipoCredencial = (TipoCredencial)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCredencial.getIdTipoCredencial()), 
        tipoCredencial.toString()));
    }
    return col;
  }

  public String getSelectTipoCredencial()
  {
    return this.selectTipoCredencial;
  }
  public void setSelectTipoCredencial(String valTipoCredencial) {
    Iterator iterator = this.colTipoCredencial.iterator();
    TipoCredencial tipoCredencial = null;
    this.subtipoCredencial.setTipoCredencial(null);
    while (iterator.hasNext()) {
      tipoCredencial = (TipoCredencial)iterator.next();
      if (String.valueOf(tipoCredencial.getIdTipoCredencial()).equals(
        valTipoCredencial)) {
        this.subtipoCredencial.setTipoCredencial(
          tipoCredencial);
        break;
      }
    }
    this.selectTipoCredencial = valTipoCredencial;
  }
  public Collection getResult() {
    return this.result;
  }

  public SubtipoCredencial getSubtipoCredencial() {
    if (this.subtipoCredencial == null) {
      this.subtipoCredencial = new SubtipoCredencial();
    }
    return this.subtipoCredencial;
  }

  public SubtipoCredencialForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoCredencial()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoCredencial.iterator();
    TipoCredencial tipoCredencial = null;
    while (iterator.hasNext()) {
      tipoCredencial = (TipoCredencial)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCredencial.getIdTipoCredencial()), 
        tipoCredencial.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoCredencial = 
        this.bienestarFacade.findAllTipoCredencial();

      this.colTipoCredencial = 
        this.bienestarFacade.findAllTipoCredencial();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSubtipoCredencialByTipoCredencial()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findSubtipoCredencialByTipoCredencial(Long.valueOf(this.findSelectTipoCredencial).longValue());
      this.showSubtipoCredencialByTipoCredencial = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSubtipoCredencialByTipoCredencial)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoCredencial = null;

    return null;
  }

  public boolean isShowSubtipoCredencialByTipoCredencial() {
    return this.showSubtipoCredencialByTipoCredencial;
  }

  public String selectSubtipoCredencial()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoCredencial = null;

    long idSubtipoCredencial = 
      Long.parseLong((String)requestParameterMap.get("idSubtipoCredencial"));
    try
    {
      this.subtipoCredencial = 
        this.bienestarFacade.findSubtipoCredencialById(
        idSubtipoCredencial);
      if (this.subtipoCredencial.getTipoCredencial() != null) {
        this.selectTipoCredencial = 
          String.valueOf(this.subtipoCredencial.getTipoCredencial().getIdTipoCredencial());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.subtipoCredencial = null;
    this.showSubtipoCredencialByTipoCredencial = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.bienestarFacade.addSubtipoCredencial(
          this.subtipoCredencial);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.bienestarFacade.updateSubtipoCredencial(
          this.subtipoCredencial);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.bienestarFacade.deleteSubtipoCredencial(
        this.subtipoCredencial);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.subtipoCredencial = new SubtipoCredencial();

    this.selectTipoCredencial = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.subtipoCredencial.setIdSubtipoCredencial(identityGenerator.getNextSequenceNumber("sigefirrhh.base.bienestar.SubtipoCredencial"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.subtipoCredencial = new SubtipoCredencial();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}