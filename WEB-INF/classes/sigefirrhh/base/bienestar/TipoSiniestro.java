package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoSiniestro
  implements Serializable, PersistenceCapable
{
  private long idTipoSiniestro;
  private String codTipoSiniestro;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoSiniestro", "idTipoSiniestro", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodTipoSiniestro(this);
  }

  public String getCodTipoSiniestro()
  {
    return jdoGetcodTipoSiniestro(this);
  }

  public void setCodTipoSiniestro(String codTipoSiniestro)
  {
    jdoSetcodTipoSiniestro(this, codTipoSiniestro);
  }

  public long getIdTipoSiniestro()
  {
    return jdoGetidTipoSiniestro(this);
  }

  public void setIdTipoSiniestro(long idTipoSiniestro)
  {
    jdoSetidTipoSiniestro(this, idTipoSiniestro);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.TipoSiniestro"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoSiniestro());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoSiniestro localTipoSiniestro = new TipoSiniestro();
    localTipoSiniestro.jdoFlags = 1;
    localTipoSiniestro.jdoStateManager = paramStateManager;
    return localTipoSiniestro;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoSiniestro localTipoSiniestro = new TipoSiniestro();
    localTipoSiniestro.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoSiniestro.jdoFlags = 1;
    localTipoSiniestro.jdoStateManager = paramStateManager;
    return localTipoSiniestro;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoSiniestro);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoSiniestro);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoSiniestro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoSiniestro = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoSiniestro paramTipoSiniestro, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoSiniestro = paramTipoSiniestro.codTipoSiniestro;
      return;
    case 1:
      if (paramTipoSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoSiniestro = paramTipoSiniestro.idTipoSiniestro;
      return;
    case 2:
      if (paramTipoSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramTipoSiniestro.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoSiniestro))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoSiniestro localTipoSiniestro = (TipoSiniestro)paramObject;
    if (localTipoSiniestro.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoSiniestro, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoSiniestroPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoSiniestroPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoSiniestroPK))
      throw new IllegalArgumentException("arg1");
    TipoSiniestroPK localTipoSiniestroPK = (TipoSiniestroPK)paramObject;
    localTipoSiniestroPK.idTipoSiniestro = this.idTipoSiniestro;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoSiniestroPK))
      throw new IllegalArgumentException("arg1");
    TipoSiniestroPK localTipoSiniestroPK = (TipoSiniestroPK)paramObject;
    this.idTipoSiniestro = localTipoSiniestroPK.idTipoSiniestro;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoSiniestroPK))
      throw new IllegalArgumentException("arg2");
    TipoSiniestroPK localTipoSiniestroPK = (TipoSiniestroPK)paramObject;
    localTipoSiniestroPK.idTipoSiniestro = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoSiniestroPK))
      throw new IllegalArgumentException("arg2");
    TipoSiniestroPK localTipoSiniestroPK = (TipoSiniestroPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localTipoSiniestroPK.idTipoSiniestro);
  }

  private static final String jdoGetcodTipoSiniestro(TipoSiniestro paramTipoSiniestro)
  {
    if (paramTipoSiniestro.jdoFlags <= 0)
      return paramTipoSiniestro.codTipoSiniestro;
    StateManager localStateManager = paramTipoSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramTipoSiniestro.codTipoSiniestro;
    if (localStateManager.isLoaded(paramTipoSiniestro, jdoInheritedFieldCount + 0))
      return paramTipoSiniestro.codTipoSiniestro;
    return localStateManager.getStringField(paramTipoSiniestro, jdoInheritedFieldCount + 0, paramTipoSiniestro.codTipoSiniestro);
  }

  private static final void jdoSetcodTipoSiniestro(TipoSiniestro paramTipoSiniestro, String paramString)
  {
    if (paramTipoSiniestro.jdoFlags == 0)
    {
      paramTipoSiniestro.codTipoSiniestro = paramString;
      return;
    }
    StateManager localStateManager = paramTipoSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoSiniestro.codTipoSiniestro = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoSiniestro, jdoInheritedFieldCount + 0, paramTipoSiniestro.codTipoSiniestro, paramString);
  }

  private static final long jdoGetidTipoSiniestro(TipoSiniestro paramTipoSiniestro)
  {
    return paramTipoSiniestro.idTipoSiniestro;
  }

  private static final void jdoSetidTipoSiniestro(TipoSiniestro paramTipoSiniestro, long paramLong)
  {
    StateManager localStateManager = paramTipoSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoSiniestro.idTipoSiniestro = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoSiniestro, jdoInheritedFieldCount + 1, paramTipoSiniestro.idTipoSiniestro, paramLong);
  }

  private static final String jdoGetnombre(TipoSiniestro paramTipoSiniestro)
  {
    if (paramTipoSiniestro.jdoFlags <= 0)
      return paramTipoSiniestro.nombre;
    StateManager localStateManager = paramTipoSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramTipoSiniestro.nombre;
    if (localStateManager.isLoaded(paramTipoSiniestro, jdoInheritedFieldCount + 2))
      return paramTipoSiniestro.nombre;
    return localStateManager.getStringField(paramTipoSiniestro, jdoInheritedFieldCount + 2, paramTipoSiniestro.nombre);
  }

  private static final void jdoSetnombre(TipoSiniestro paramTipoSiniestro, String paramString)
  {
    if (paramTipoSiniestro.jdoFlags == 0)
    {
      paramTipoSiniestro.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramTipoSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoSiniestro.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoSiniestro, jdoInheritedFieldCount + 2, paramTipoSiniestro.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}