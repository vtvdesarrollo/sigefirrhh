package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class TipoDotacionPK
  implements Serializable
{
  public long idTipoDotacion;

  public TipoDotacionPK()
  {
  }

  public TipoDotacionPK(long idTipoDotacion)
  {
    this.idTipoDotacion = idTipoDotacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoDotacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoDotacionPK thatPK)
  {
    return 
      this.idTipoDotacion == thatPK.idTipoDotacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoDotacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoDotacion);
  }
}