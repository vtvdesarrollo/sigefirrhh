package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class ParametroJuguete
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_QUIENCOBRA;
  protected static final Map LISTA_MODALIDAD;
  private long idParametroJuguete;
  private TipoPersonal tipoPersonal;
  private int edadMinima;
  private int edadMaxima;
  private int cantidadBeneficiarios;
  private double montoJuguetes;
  private String quienCobra;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cantidadBeneficiarios", "edadMaxima", "edadMinima", "idParametroJuguete", "montoJuguetes", "quienCobra", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, Long.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.ParametroJuguete"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroJuguete());

    LISTA_QUIENCOBRA = 
      new LinkedHashMap();
    LISTA_MODALIDAD = 
      new LinkedHashMap();

    LISTA_QUIENCOBRA.put("M", "MADRE");
    LISTA_QUIENCOBRA.put("P", "PADRE");
    LISTA_QUIENCOBRA.put("A", "AMBOS");
    LISTA_MODALIDAD.put("M", "MONTO DINERO");
    LISTA_MODALIDAD.put("T", "TICKET JUGUETE");
    LISTA_MODALIDAD.put("J", "JUGUETES");
  }

  public ParametroJuguete()
  {
    jdoSetedadMinima(this, 0);

    jdoSetedadMaxima(this, 12);

    jdoSetcantidadBeneficiarios(this, 1);

    jdoSetmontoJuguetes(this, 0.0D);

    jdoSetquienCobra(this, "M");
  }
  public String toString() {
    return jdoGettipoPersonal(this).getNombre();
  }

  public int getCantidadBeneficiarios() {
    return jdoGetcantidadBeneficiarios(this);
  }

  public void setCantidadBeneficiarios(int cantidadBeneficiarios) {
    jdoSetcantidadBeneficiarios(this, cantidadBeneficiarios);
  }

  public int getEdadMaxima() {
    return jdoGetedadMaxima(this);
  }

  public void setEdadMaxima(int edadMaxima) {
    jdoSetedadMaxima(this, edadMaxima);
  }

  public int getEdadMinima() {
    return jdoGetedadMinima(this);
  }

  public void setEdadMinima(int edadMinima) {
    jdoSetedadMinima(this, edadMinima);
  }

  public long getIdParametroJuguete() {
    return jdoGetidParametroJuguete(this);
  }

  public void setIdParametroJuguete(long idParametroJuguete) {
    jdoSetidParametroJuguete(this, idParametroJuguete);
  }

  public double getMontoJuguetes() {
    return jdoGetmontoJuguetes(this);
  }

  public void setMontoJuguetes(double montoJuguetes) {
    jdoSetmontoJuguetes(this, montoJuguetes);
  }

  public String getQuienCobra() {
    return jdoGetquienCobra(this);
  }

  public void setQuienCobra(String quienCobra) {
    jdoSetquienCobra(this, quienCobra);
  }

  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroJuguete localParametroJuguete = new ParametroJuguete();
    localParametroJuguete.jdoFlags = 1;
    localParametroJuguete.jdoStateManager = paramStateManager;
    return localParametroJuguete;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroJuguete localParametroJuguete = new ParametroJuguete();
    localParametroJuguete.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroJuguete.jdoFlags = 1;
    localParametroJuguete.jdoStateManager = paramStateManager;
    return localParametroJuguete;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadBeneficiarios);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadMaxima);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadMinima);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroJuguete);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoJuguetes);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.quienCobra);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadBeneficiarios = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadMaxima = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadMinima = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroJuguete = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoJuguetes = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.quienCobra = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroJuguete paramParametroJuguete, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroJuguete == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadBeneficiarios = paramParametroJuguete.cantidadBeneficiarios;
      return;
    case 1:
      if (paramParametroJuguete == null)
        throw new IllegalArgumentException("arg1");
      this.edadMaxima = paramParametroJuguete.edadMaxima;
      return;
    case 2:
      if (paramParametroJuguete == null)
        throw new IllegalArgumentException("arg1");
      this.edadMinima = paramParametroJuguete.edadMinima;
      return;
    case 3:
      if (paramParametroJuguete == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroJuguete = paramParametroJuguete.idParametroJuguete;
      return;
    case 4:
      if (paramParametroJuguete == null)
        throw new IllegalArgumentException("arg1");
      this.montoJuguetes = paramParametroJuguete.montoJuguetes;
      return;
    case 5:
      if (paramParametroJuguete == null)
        throw new IllegalArgumentException("arg1");
      this.quienCobra = paramParametroJuguete.quienCobra;
      return;
    case 6:
      if (paramParametroJuguete == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramParametroJuguete.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroJuguete))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroJuguete localParametroJuguete = (ParametroJuguete)paramObject;
    if (localParametroJuguete.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroJuguete, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroJuguetePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroJuguetePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroJuguetePK))
      throw new IllegalArgumentException("arg1");
    ParametroJuguetePK localParametroJuguetePK = (ParametroJuguetePK)paramObject;
    localParametroJuguetePK.idParametroJuguete = this.idParametroJuguete;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroJuguetePK))
      throw new IllegalArgumentException("arg1");
    ParametroJuguetePK localParametroJuguetePK = (ParametroJuguetePK)paramObject;
    this.idParametroJuguete = localParametroJuguetePK.idParametroJuguete;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroJuguetePK))
      throw new IllegalArgumentException("arg2");
    ParametroJuguetePK localParametroJuguetePK = (ParametroJuguetePK)paramObject;
    localParametroJuguetePK.idParametroJuguete = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroJuguetePK))
      throw new IllegalArgumentException("arg2");
    ParametroJuguetePK localParametroJuguetePK = (ParametroJuguetePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localParametroJuguetePK.idParametroJuguete);
  }

  private static final int jdoGetcantidadBeneficiarios(ParametroJuguete paramParametroJuguete)
  {
    if (paramParametroJuguete.jdoFlags <= 0)
      return paramParametroJuguete.cantidadBeneficiarios;
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJuguete.cantidadBeneficiarios;
    if (localStateManager.isLoaded(paramParametroJuguete, jdoInheritedFieldCount + 0))
      return paramParametroJuguete.cantidadBeneficiarios;
    return localStateManager.getIntField(paramParametroJuguete, jdoInheritedFieldCount + 0, paramParametroJuguete.cantidadBeneficiarios);
  }

  private static final void jdoSetcantidadBeneficiarios(ParametroJuguete paramParametroJuguete, int paramInt)
  {
    if (paramParametroJuguete.jdoFlags == 0)
    {
      paramParametroJuguete.cantidadBeneficiarios = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJuguete.cantidadBeneficiarios = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroJuguete, jdoInheritedFieldCount + 0, paramParametroJuguete.cantidadBeneficiarios, paramInt);
  }

  private static final int jdoGetedadMaxima(ParametroJuguete paramParametroJuguete)
  {
    if (paramParametroJuguete.jdoFlags <= 0)
      return paramParametroJuguete.edadMaxima;
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJuguete.edadMaxima;
    if (localStateManager.isLoaded(paramParametroJuguete, jdoInheritedFieldCount + 1))
      return paramParametroJuguete.edadMaxima;
    return localStateManager.getIntField(paramParametroJuguete, jdoInheritedFieldCount + 1, paramParametroJuguete.edadMaxima);
  }

  private static final void jdoSetedadMaxima(ParametroJuguete paramParametroJuguete, int paramInt)
  {
    if (paramParametroJuguete.jdoFlags == 0)
    {
      paramParametroJuguete.edadMaxima = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJuguete.edadMaxima = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroJuguete, jdoInheritedFieldCount + 1, paramParametroJuguete.edadMaxima, paramInt);
  }

  private static final int jdoGetedadMinima(ParametroJuguete paramParametroJuguete)
  {
    if (paramParametroJuguete.jdoFlags <= 0)
      return paramParametroJuguete.edadMinima;
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJuguete.edadMinima;
    if (localStateManager.isLoaded(paramParametroJuguete, jdoInheritedFieldCount + 2))
      return paramParametroJuguete.edadMinima;
    return localStateManager.getIntField(paramParametroJuguete, jdoInheritedFieldCount + 2, paramParametroJuguete.edadMinima);
  }

  private static final void jdoSetedadMinima(ParametroJuguete paramParametroJuguete, int paramInt)
  {
    if (paramParametroJuguete.jdoFlags == 0)
    {
      paramParametroJuguete.edadMinima = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJuguete.edadMinima = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroJuguete, jdoInheritedFieldCount + 2, paramParametroJuguete.edadMinima, paramInt);
  }

  private static final long jdoGetidParametroJuguete(ParametroJuguete paramParametroJuguete)
  {
    return paramParametroJuguete.idParametroJuguete;
  }

  private static final void jdoSetidParametroJuguete(ParametroJuguete paramParametroJuguete, long paramLong)
  {
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJuguete.idParametroJuguete = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroJuguete, jdoInheritedFieldCount + 3, paramParametroJuguete.idParametroJuguete, paramLong);
  }

  private static final double jdoGetmontoJuguetes(ParametroJuguete paramParametroJuguete)
  {
    if (paramParametroJuguete.jdoFlags <= 0)
      return paramParametroJuguete.montoJuguetes;
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJuguete.montoJuguetes;
    if (localStateManager.isLoaded(paramParametroJuguete, jdoInheritedFieldCount + 4))
      return paramParametroJuguete.montoJuguetes;
    return localStateManager.getDoubleField(paramParametroJuguete, jdoInheritedFieldCount + 4, paramParametroJuguete.montoJuguetes);
  }

  private static final void jdoSetmontoJuguetes(ParametroJuguete paramParametroJuguete, double paramDouble)
  {
    if (paramParametroJuguete.jdoFlags == 0)
    {
      paramParametroJuguete.montoJuguetes = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJuguete.montoJuguetes = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroJuguete, jdoInheritedFieldCount + 4, paramParametroJuguete.montoJuguetes, paramDouble);
  }

  private static final String jdoGetquienCobra(ParametroJuguete paramParametroJuguete)
  {
    if (paramParametroJuguete.jdoFlags <= 0)
      return paramParametroJuguete.quienCobra;
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJuguete.quienCobra;
    if (localStateManager.isLoaded(paramParametroJuguete, jdoInheritedFieldCount + 5))
      return paramParametroJuguete.quienCobra;
    return localStateManager.getStringField(paramParametroJuguete, jdoInheritedFieldCount + 5, paramParametroJuguete.quienCobra);
  }

  private static final void jdoSetquienCobra(ParametroJuguete paramParametroJuguete, String paramString)
  {
    if (paramParametroJuguete.jdoFlags == 0)
    {
      paramParametroJuguete.quienCobra = paramString;
      return;
    }
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJuguete.quienCobra = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroJuguete, jdoInheritedFieldCount + 5, paramParametroJuguete.quienCobra, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(ParametroJuguete paramParametroJuguete)
  {
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJuguete.tipoPersonal;
    if (localStateManager.isLoaded(paramParametroJuguete, jdoInheritedFieldCount + 6))
      return paramParametroJuguete.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramParametroJuguete, jdoInheritedFieldCount + 6, paramParametroJuguete.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ParametroJuguete paramParametroJuguete, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramParametroJuguete.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJuguete.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramParametroJuguete, jdoInheritedFieldCount + 6, paramParametroJuguete.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}