package sigefirrhh.base.bienestar;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

public class BienestarNoGenBusiness extends BienestarBusiness
  implements Serializable
{
  public Collection findPlanPolizaByPersonal(long idPersonal)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select distinct id_plan_poliza, descripcion from (");
      sql.append(" select pla.id_plan_poliza, (pla.cod_plan_poliza || ' - ' || pla.nombre) as descripcion");
      sql.append(" from planpoliza pla, primasplan pri, titular tit");
      sql.append(" where pla.id_plan_poliza = pri.id_plan_poliza");
      sql.append(" and pri.id_primas_plan = tit.id_primas_plan");
      sql.append(" and tit.id_personal = ?");
      sql.append(" union");
      sql.append(" select pla.id_plan_poliza, (pla.cod_plan_poliza || ' - ' || pla.nombre) as descripcion");
      sql.append(" from planpoliza pla, primasplan pri, beneficiario ben");
      sql.append(" where pla.id_plan_poliza = pri.id_plan_poliza");
      sql.append(" and pri.id_primas_plan = ben.id_primas_plan");
      sql.append(" and ben.id_personal = ?)");
      sql.append(" as registros order by descripcion");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idPersonal);
      stRegistros.setLong(2, idPersonal);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_plan_poliza"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findPlanPolizaByPersonal(long idPersonal, long idPoliza) throws Exception { Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select distinct id_plan_poliza, descripcion from (");
      sql.append(" select pla.id_plan_poliza, (pla.cod_plan_poliza || ' - ' || pla.nombre) as descripcion");
      sql.append(" from planpoliza pla, primasplan pri, titular tit");
      sql.append(" where pla.id_plan_poliza = pri.id_plan_poliza");
      sql.append(" and pri.id_primas_plan = tit.id_primas_plan");
      sql.append(" and tit.id_personal = ?");
      sql.append(" and pla.id_poliza = ?");
      sql.append(" union");
      sql.append(" select pla.id_plan_poliza, (pla.cod_plan_poliza || ' - ' || pla.nombre) as descripcion");
      sql.append(" from planpoliza pla, primasplan pri, beneficiario ben");
      sql.append(" where pla.id_plan_poliza = pri.id_plan_poliza");
      sql.append(" and pri.id_primas_plan = ben.id_primas_plan");
      sql.append(" and ben.id_personal = ?");
      sql.append(" and pla.id_poliza = ?)");
      sql.append(" as registros order by descripcion");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idPersonal);
      stRegistros.setLong(2, idPoliza);
      stRegistros.setLong(3, idPersonal);
      stRegistros.setLong(4, idPoliza);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_plan_poliza"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    } } 
  public long findIdParametroGuarderiaByPersonal(long idPersonal) throws Exception
  {
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select pg.id_parametro_guarderia  ");
      sql.append(" from parametroguarderia pg, tipopersonal tp, trabajador t, personal p ");
      sql.append(" where pg.id_tipo_personal = tp.id_tipo_personal ");
      sql.append(" and tp.id_tipo_personal = t.id_tipo_personal ");
      sql.append(" and t.id_personal = p.id_personal ");
      sql.append(" and t.estatus = 'A' ");
      sql.append(" and p.id_personal = ? ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idPersonal);
      rsRegistros = stRegistros.executeQuery();

      if (rsRegistros.next())
      {
        return rsRegistros.getLong("id_parametro_guarderia");
      }

      return 0L;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException6) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException7) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException8)
        {
        }
    }
  }
}