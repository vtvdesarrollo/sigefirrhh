package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class ParametroUtiles
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_NIVEL_EDUCATIVO;
  protected static final Map LISTA_QUIENCOBRA;
  protected static final Map LISTA_MODALIDAD;
  private long idParametroUtiles;
  private TipoPersonal tipoPersonal;
  private String nivelEducativo;
  private int cantidadBeneficiarios;
  private double montoUtiles;
  private String quienCobra;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cantidadBeneficiarios", "idParametroUtiles", "montoUtiles", "nivelEducativo", "quienCobra", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.ParametroUtiles"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroUtiles());

    LISTA_NIVEL_EDUCATIVO = 
      new LinkedHashMap();
    LISTA_QUIENCOBRA = 
      new LinkedHashMap();
    LISTA_MODALIDAD = 
      new LinkedHashMap();

    LISTA_NIVEL_EDUCATIVO.put("P", "PRESCOLAR");
    LISTA_NIVEL_EDUCATIVO.put("B", "BASICA");
    LISTA_NIVEL_EDUCATIVO.put("I", "PRIMARIA");
    LISTA_NIVEL_EDUCATIVO.put("D", "DIVERSIFICADO");
    LISTA_NIVEL_EDUCATIVO.put("H", "BACHILLERATO");
    LISTA_NIVEL_EDUCATIVO.put("T", "TECNICO MEDIO");
    LISTA_NIVEL_EDUCATIVO.put("S", "TECNICO SUPERIOR");
    LISTA_NIVEL_EDUCATIVO.put("U", "UNIVERSITARIO");
    LISTA_NIVEL_EDUCATIVO.put("E", "ESPECIALIZACION");
    LISTA_NIVEL_EDUCATIVO.put("M", "MAESTRIA");
    LISTA_NIVEL_EDUCATIVO.put("C", "DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("R", "POST DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("G", "POSTGRADO");
    LISTA_NIVEL_EDUCATIVO.put("L", "DIPLOMADO");
    LISTA_NIVEL_EDUCATIVO.put("O", "OTRO");
    LISTA_QUIENCOBRA.put("M", "MADRE");
    LISTA_QUIENCOBRA.put("P", "PADRE");
    LISTA_QUIENCOBRA.put("A", "AMBOS");
    LISTA_MODALIDAD.put("M", "MONTO DINERO");
    LISTA_MODALIDAD.put("T", "TICKET UTILES");
    LISTA_MODALIDAD.put("U", "UTILES");
  }

  public ParametroUtiles()
  {
    jdoSetcantidadBeneficiarios(this, 0);

    jdoSetmontoUtiles(this, 0.0D);

    jdoSetquienCobra(this, "M");
  }
  public String toString() {
    return jdoGettipoPersonal(this).getNombre() + " - " + 
      LISTA_NIVEL_EDUCATIVO.get(jdoGetnivelEducativo(this));
  }

  public int getCantidadBeneficiarios() {
    return jdoGetcantidadBeneficiarios(this);
  }

  public void setCantidadBeneficiarios(int cantidadBeneficiarios) {
    jdoSetcantidadBeneficiarios(this, cantidadBeneficiarios);
  }

  public long getIdParametroUtiles() {
    return jdoGetidParametroUtiles(this);
  }

  public void setIdParametroUtiles(long idParametroUtiles) {
    jdoSetidParametroUtiles(this, idParametroUtiles);
  }

  public double getMontoUtiles() {
    return jdoGetmontoUtiles(this);
  }

  public void setMontoUtiles(double montoUtiles) {
    jdoSetmontoUtiles(this, montoUtiles);
  }

  public String getNivelEducativo() {
    return jdoGetnivelEducativo(this);
  }

  public void setNivelEducativo(String nivelEducativo) {
    jdoSetnivelEducativo(this, nivelEducativo);
  }

  public String getQuienCobra() {
    return jdoGetquienCobra(this);
  }

  public void setQuienCobra(String quienCobra) {
    jdoSetquienCobra(this, quienCobra);
  }

  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroUtiles localParametroUtiles = new ParametroUtiles();
    localParametroUtiles.jdoFlags = 1;
    localParametroUtiles.jdoStateManager = paramStateManager;
    return localParametroUtiles;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroUtiles localParametroUtiles = new ParametroUtiles();
    localParametroUtiles.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroUtiles.jdoFlags = 1;
    localParametroUtiles.jdoStateManager = paramStateManager;
    return localParametroUtiles;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadBeneficiarios);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroUtiles);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoUtiles);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivelEducativo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.quienCobra);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadBeneficiarios = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroUtiles = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoUtiles = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.quienCobra = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroUtiles paramParametroUtiles, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroUtiles == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadBeneficiarios = paramParametroUtiles.cantidadBeneficiarios;
      return;
    case 1:
      if (paramParametroUtiles == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroUtiles = paramParametroUtiles.idParametroUtiles;
      return;
    case 2:
      if (paramParametroUtiles == null)
        throw new IllegalArgumentException("arg1");
      this.montoUtiles = paramParametroUtiles.montoUtiles;
      return;
    case 3:
      if (paramParametroUtiles == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramParametroUtiles.nivelEducativo;
      return;
    case 4:
      if (paramParametroUtiles == null)
        throw new IllegalArgumentException("arg1");
      this.quienCobra = paramParametroUtiles.quienCobra;
      return;
    case 5:
      if (paramParametroUtiles == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramParametroUtiles.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroUtiles))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroUtiles localParametroUtiles = (ParametroUtiles)paramObject;
    if (localParametroUtiles.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroUtiles, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroUtilesPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroUtilesPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroUtilesPK))
      throw new IllegalArgumentException("arg1");
    ParametroUtilesPK localParametroUtilesPK = (ParametroUtilesPK)paramObject;
    localParametroUtilesPK.idParametroUtiles = this.idParametroUtiles;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroUtilesPK))
      throw new IllegalArgumentException("arg1");
    ParametroUtilesPK localParametroUtilesPK = (ParametroUtilesPK)paramObject;
    this.idParametroUtiles = localParametroUtilesPK.idParametroUtiles;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroUtilesPK))
      throw new IllegalArgumentException("arg2");
    ParametroUtilesPK localParametroUtilesPK = (ParametroUtilesPK)paramObject;
    localParametroUtilesPK.idParametroUtiles = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroUtilesPK))
      throw new IllegalArgumentException("arg2");
    ParametroUtilesPK localParametroUtilesPK = (ParametroUtilesPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localParametroUtilesPK.idParametroUtiles);
  }

  private static final int jdoGetcantidadBeneficiarios(ParametroUtiles paramParametroUtiles)
  {
    if (paramParametroUtiles.jdoFlags <= 0)
      return paramParametroUtiles.cantidadBeneficiarios;
    StateManager localStateManager = paramParametroUtiles.jdoStateManager;
    if (localStateManager == null)
      return paramParametroUtiles.cantidadBeneficiarios;
    if (localStateManager.isLoaded(paramParametroUtiles, jdoInheritedFieldCount + 0))
      return paramParametroUtiles.cantidadBeneficiarios;
    return localStateManager.getIntField(paramParametroUtiles, jdoInheritedFieldCount + 0, paramParametroUtiles.cantidadBeneficiarios);
  }

  private static final void jdoSetcantidadBeneficiarios(ParametroUtiles paramParametroUtiles, int paramInt)
  {
    if (paramParametroUtiles.jdoFlags == 0)
    {
      paramParametroUtiles.cantidadBeneficiarios = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroUtiles.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroUtiles.cantidadBeneficiarios = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroUtiles, jdoInheritedFieldCount + 0, paramParametroUtiles.cantidadBeneficiarios, paramInt);
  }

  private static final long jdoGetidParametroUtiles(ParametroUtiles paramParametroUtiles)
  {
    return paramParametroUtiles.idParametroUtiles;
  }

  private static final void jdoSetidParametroUtiles(ParametroUtiles paramParametroUtiles, long paramLong)
  {
    StateManager localStateManager = paramParametroUtiles.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroUtiles.idParametroUtiles = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroUtiles, jdoInheritedFieldCount + 1, paramParametroUtiles.idParametroUtiles, paramLong);
  }

  private static final double jdoGetmontoUtiles(ParametroUtiles paramParametroUtiles)
  {
    if (paramParametroUtiles.jdoFlags <= 0)
      return paramParametroUtiles.montoUtiles;
    StateManager localStateManager = paramParametroUtiles.jdoStateManager;
    if (localStateManager == null)
      return paramParametroUtiles.montoUtiles;
    if (localStateManager.isLoaded(paramParametroUtiles, jdoInheritedFieldCount + 2))
      return paramParametroUtiles.montoUtiles;
    return localStateManager.getDoubleField(paramParametroUtiles, jdoInheritedFieldCount + 2, paramParametroUtiles.montoUtiles);
  }

  private static final void jdoSetmontoUtiles(ParametroUtiles paramParametroUtiles, double paramDouble)
  {
    if (paramParametroUtiles.jdoFlags == 0)
    {
      paramParametroUtiles.montoUtiles = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroUtiles.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroUtiles.montoUtiles = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroUtiles, jdoInheritedFieldCount + 2, paramParametroUtiles.montoUtiles, paramDouble);
  }

  private static final String jdoGetnivelEducativo(ParametroUtiles paramParametroUtiles)
  {
    if (paramParametroUtiles.jdoFlags <= 0)
      return paramParametroUtiles.nivelEducativo;
    StateManager localStateManager = paramParametroUtiles.jdoStateManager;
    if (localStateManager == null)
      return paramParametroUtiles.nivelEducativo;
    if (localStateManager.isLoaded(paramParametroUtiles, jdoInheritedFieldCount + 3))
      return paramParametroUtiles.nivelEducativo;
    return localStateManager.getStringField(paramParametroUtiles, jdoInheritedFieldCount + 3, paramParametroUtiles.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(ParametroUtiles paramParametroUtiles, String paramString)
  {
    if (paramParametroUtiles.jdoFlags == 0)
    {
      paramParametroUtiles.nivelEducativo = paramString;
      return;
    }
    StateManager localStateManager = paramParametroUtiles.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroUtiles.nivelEducativo = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroUtiles, jdoInheritedFieldCount + 3, paramParametroUtiles.nivelEducativo, paramString);
  }

  private static final String jdoGetquienCobra(ParametroUtiles paramParametroUtiles)
  {
    if (paramParametroUtiles.jdoFlags <= 0)
      return paramParametroUtiles.quienCobra;
    StateManager localStateManager = paramParametroUtiles.jdoStateManager;
    if (localStateManager == null)
      return paramParametroUtiles.quienCobra;
    if (localStateManager.isLoaded(paramParametroUtiles, jdoInheritedFieldCount + 4))
      return paramParametroUtiles.quienCobra;
    return localStateManager.getStringField(paramParametroUtiles, jdoInheritedFieldCount + 4, paramParametroUtiles.quienCobra);
  }

  private static final void jdoSetquienCobra(ParametroUtiles paramParametroUtiles, String paramString)
  {
    if (paramParametroUtiles.jdoFlags == 0)
    {
      paramParametroUtiles.quienCobra = paramString;
      return;
    }
    StateManager localStateManager = paramParametroUtiles.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroUtiles.quienCobra = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroUtiles, jdoInheritedFieldCount + 4, paramParametroUtiles.quienCobra, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(ParametroUtiles paramParametroUtiles)
  {
    StateManager localStateManager = paramParametroUtiles.jdoStateManager;
    if (localStateManager == null)
      return paramParametroUtiles.tipoPersonal;
    if (localStateManager.isLoaded(paramParametroUtiles, jdoInheritedFieldCount + 5))
      return paramParametroUtiles.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramParametroUtiles, jdoInheritedFieldCount + 5, paramParametroUtiles.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ParametroUtiles paramParametroUtiles, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramParametroUtiles.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroUtiles.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramParametroUtiles, jdoInheritedFieldCount + 5, paramParametroUtiles.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}