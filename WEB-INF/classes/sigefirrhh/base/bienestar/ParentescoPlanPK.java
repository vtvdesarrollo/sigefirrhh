package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class ParentescoPlanPK
  implements Serializable
{
  public long idParentescoPlan;

  public ParentescoPlanPK()
  {
  }

  public ParentescoPlanPK(long idParentescoPlan)
  {
    this.idParentescoPlan = idParentescoPlan;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParentescoPlanPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParentescoPlanPK thatPK)
  {
    return 
      this.idParentescoPlan == thatPK.idParentescoPlan;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParentescoPlan)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParentescoPlan);
  }
}