package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ParametroUtilesBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParametroUtiles(ParametroUtiles parametroUtiles)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ParametroUtiles parametroUtilesNew = 
      (ParametroUtiles)BeanUtils.cloneBean(
      parametroUtiles);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroUtilesNew.getTipoPersonal() != null) {
      parametroUtilesNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroUtilesNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(parametroUtilesNew);
  }

  public void updateParametroUtiles(ParametroUtiles parametroUtiles) throws Exception
  {
    ParametroUtiles parametroUtilesModify = 
      findParametroUtilesById(parametroUtiles.getIdParametroUtiles());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroUtiles.getTipoPersonal() != null) {
      parametroUtiles.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroUtiles.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(parametroUtilesModify, parametroUtiles);
  }

  public void deleteParametroUtiles(ParametroUtiles parametroUtiles) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ParametroUtiles parametroUtilesDelete = 
      findParametroUtilesById(parametroUtiles.getIdParametroUtiles());
    pm.deletePersistent(parametroUtilesDelete);
  }

  public ParametroUtiles findParametroUtilesById(long idParametroUtiles) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParametroUtiles == pIdParametroUtiles";
    Query query = pm.newQuery(ParametroUtiles.class, filter);

    query.declareParameters("long pIdParametroUtiles");

    parameters.put("pIdParametroUtiles", new Long(idParametroUtiles));

    Collection colParametroUtiles = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParametroUtiles.iterator();
    return (ParametroUtiles)iterator.next();
  }

  public Collection findParametroUtilesAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parametroUtilesExtent = pm.getExtent(
      ParametroUtiles.class, true);
    Query query = pm.newQuery(parametroUtilesExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ParametroUtiles.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colParametroUtiles = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParametroUtiles);

    return colParametroUtiles;
  }
}