package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class DenominacionTicket
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idDenominacionTicket;
  private TipoPersonal tipoPersonal;
  private ProveedorTicket proveedorTicket;
  private double valor;
  private double costo;
  private int ticketsPorDia;
  private String estatus;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "costo", "estatus", "idDenominacionTicket", "proveedorTicket", "ticketsPorDia", "tipoPersonal", "valor" }; private static final Class[] jdoFieldTypes = { Double.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.bienestar.ProveedorTicket"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 26, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.DenominacionTicket"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DenominacionTicket());

    LISTA_ESTATUS = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("A", "ACTIVO");
    LISTA_ESTATUS.put("S", "SUSPENDIDO");
  }

  public DenominacionTicket()
  {
    jdoSetvalor(this, 0.0D);

    jdoSetcosto(this, 0.0D);

    jdoSetticketsPorDia(this, 0);
  }

  public String toString()
  {
    return jdoGettipoPersonal(this).toString() + ", " + " - " + jdoGetproveedorTicket(this).toString() + " " + jdoGetvalor(this);
  }

  public long getIdDenominacionTicket() {
    return jdoGetidDenominacionTicket(this);
  }
  public void setIdDenominacionTicket(long idDenominacionTicket) {
    jdoSetidDenominacionTicket(this, idDenominacionTicket);
  }
  public int getTicketsPorDia() {
    return jdoGetticketsPorDia(this);
  }
  public void setTicketsPorDia(int ticketsPorDia) {
    jdoSetticketsPorDia(this, ticketsPorDia);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }
  public double getValor() {
    return jdoGetvalor(this);
  }
  public void setValor(double valor) {
    jdoSetvalor(this, valor);
  }
  public String getEstatus() {
    return jdoGetestatus(this);
  }
  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }
  public double getCosto() {
    return jdoGetcosto(this);
  }
  public void setCosto(double costo) {
    jdoSetcosto(this, costo);
  }
  public ProveedorTicket getProveedorTicket() {
    return jdoGetproveedorTicket(this);
  }
  public void setProveedorTicket(ProveedorTicket proveedorTicket) {
    jdoSetproveedorTicket(this, proveedorTicket);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DenominacionTicket localDenominacionTicket = new DenominacionTicket();
    localDenominacionTicket.jdoFlags = 1;
    localDenominacionTicket.jdoStateManager = paramStateManager;
    return localDenominacionTicket;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DenominacionTicket localDenominacionTicket = new DenominacionTicket();
    localDenominacionTicket.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDenominacionTicket.jdoFlags = 1;
    localDenominacionTicket.jdoStateManager = paramStateManager;
    return localDenominacionTicket;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDenominacionTicket);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.proveedorTicket);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.ticketsPorDia);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.valor);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDenominacionTicket = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.proveedorTicket = ((ProveedorTicket)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ticketsPorDia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.valor = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DenominacionTicket paramDenominacionTicket, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDenominacionTicket == null)
        throw new IllegalArgumentException("arg1");
      this.costo = paramDenominacionTicket.costo;
      return;
    case 1:
      if (paramDenominacionTicket == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramDenominacionTicket.estatus;
      return;
    case 2:
      if (paramDenominacionTicket == null)
        throw new IllegalArgumentException("arg1");
      this.idDenominacionTicket = paramDenominacionTicket.idDenominacionTicket;
      return;
    case 3:
      if (paramDenominacionTicket == null)
        throw new IllegalArgumentException("arg1");
      this.proveedorTicket = paramDenominacionTicket.proveedorTicket;
      return;
    case 4:
      if (paramDenominacionTicket == null)
        throw new IllegalArgumentException("arg1");
      this.ticketsPorDia = paramDenominacionTicket.ticketsPorDia;
      return;
    case 5:
      if (paramDenominacionTicket == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramDenominacionTicket.tipoPersonal;
      return;
    case 6:
      if (paramDenominacionTicket == null)
        throw new IllegalArgumentException("arg1");
      this.valor = paramDenominacionTicket.valor;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DenominacionTicket))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DenominacionTicket localDenominacionTicket = (DenominacionTicket)paramObject;
    if (localDenominacionTicket.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDenominacionTicket, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DenominacionTicketPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DenominacionTicketPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DenominacionTicketPK))
      throw new IllegalArgumentException("arg1");
    DenominacionTicketPK localDenominacionTicketPK = (DenominacionTicketPK)paramObject;
    localDenominacionTicketPK.idDenominacionTicket = this.idDenominacionTicket;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DenominacionTicketPK))
      throw new IllegalArgumentException("arg1");
    DenominacionTicketPK localDenominacionTicketPK = (DenominacionTicketPK)paramObject;
    this.idDenominacionTicket = localDenominacionTicketPK.idDenominacionTicket;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DenominacionTicketPK))
      throw new IllegalArgumentException("arg2");
    DenominacionTicketPK localDenominacionTicketPK = (DenominacionTicketPK)paramObject;
    localDenominacionTicketPK.idDenominacionTicket = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DenominacionTicketPK))
      throw new IllegalArgumentException("arg2");
    DenominacionTicketPK localDenominacionTicketPK = (DenominacionTicketPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localDenominacionTicketPK.idDenominacionTicket);
  }

  private static final double jdoGetcosto(DenominacionTicket paramDenominacionTicket)
  {
    if (paramDenominacionTicket.jdoFlags <= 0)
      return paramDenominacionTicket.costo;
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDenominacionTicket.costo;
    if (localStateManager.isLoaded(paramDenominacionTicket, jdoInheritedFieldCount + 0))
      return paramDenominacionTicket.costo;
    return localStateManager.getDoubleField(paramDenominacionTicket, jdoInheritedFieldCount + 0, paramDenominacionTicket.costo);
  }

  private static final void jdoSetcosto(DenominacionTicket paramDenominacionTicket, double paramDouble)
  {
    if (paramDenominacionTicket.jdoFlags == 0)
    {
      paramDenominacionTicket.costo = paramDouble;
      return;
    }
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDenominacionTicket.costo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramDenominacionTicket, jdoInheritedFieldCount + 0, paramDenominacionTicket.costo, paramDouble);
  }

  private static final String jdoGetestatus(DenominacionTicket paramDenominacionTicket)
  {
    if (paramDenominacionTicket.jdoFlags <= 0)
      return paramDenominacionTicket.estatus;
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDenominacionTicket.estatus;
    if (localStateManager.isLoaded(paramDenominacionTicket, jdoInheritedFieldCount + 1))
      return paramDenominacionTicket.estatus;
    return localStateManager.getStringField(paramDenominacionTicket, jdoInheritedFieldCount + 1, paramDenominacionTicket.estatus);
  }

  private static final void jdoSetestatus(DenominacionTicket paramDenominacionTicket, String paramString)
  {
    if (paramDenominacionTicket.jdoFlags == 0)
    {
      paramDenominacionTicket.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDenominacionTicket.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramDenominacionTicket, jdoInheritedFieldCount + 1, paramDenominacionTicket.estatus, paramString);
  }

  private static final long jdoGetidDenominacionTicket(DenominacionTicket paramDenominacionTicket)
  {
    return paramDenominacionTicket.idDenominacionTicket;
  }

  private static final void jdoSetidDenominacionTicket(DenominacionTicket paramDenominacionTicket, long paramLong)
  {
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDenominacionTicket.idDenominacionTicket = paramLong;
      return;
    }
    localStateManager.setLongField(paramDenominacionTicket, jdoInheritedFieldCount + 2, paramDenominacionTicket.idDenominacionTicket, paramLong);
  }

  private static final ProveedorTicket jdoGetproveedorTicket(DenominacionTicket paramDenominacionTicket)
  {
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDenominacionTicket.proveedorTicket;
    if (localStateManager.isLoaded(paramDenominacionTicket, jdoInheritedFieldCount + 3))
      return paramDenominacionTicket.proveedorTicket;
    return (ProveedorTicket)localStateManager.getObjectField(paramDenominacionTicket, jdoInheritedFieldCount + 3, paramDenominacionTicket.proveedorTicket);
  }

  private static final void jdoSetproveedorTicket(DenominacionTicket paramDenominacionTicket, ProveedorTicket paramProveedorTicket)
  {
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDenominacionTicket.proveedorTicket = paramProveedorTicket;
      return;
    }
    localStateManager.setObjectField(paramDenominacionTicket, jdoInheritedFieldCount + 3, paramDenominacionTicket.proveedorTicket, paramProveedorTicket);
  }

  private static final int jdoGetticketsPorDia(DenominacionTicket paramDenominacionTicket)
  {
    if (paramDenominacionTicket.jdoFlags <= 0)
      return paramDenominacionTicket.ticketsPorDia;
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDenominacionTicket.ticketsPorDia;
    if (localStateManager.isLoaded(paramDenominacionTicket, jdoInheritedFieldCount + 4))
      return paramDenominacionTicket.ticketsPorDia;
    return localStateManager.getIntField(paramDenominacionTicket, jdoInheritedFieldCount + 4, paramDenominacionTicket.ticketsPorDia);
  }

  private static final void jdoSetticketsPorDia(DenominacionTicket paramDenominacionTicket, int paramInt)
  {
    if (paramDenominacionTicket.jdoFlags == 0)
    {
      paramDenominacionTicket.ticketsPorDia = paramInt;
      return;
    }
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDenominacionTicket.ticketsPorDia = paramInt;
      return;
    }
    localStateManager.setIntField(paramDenominacionTicket, jdoInheritedFieldCount + 4, paramDenominacionTicket.ticketsPorDia, paramInt);
  }

  private static final TipoPersonal jdoGettipoPersonal(DenominacionTicket paramDenominacionTicket)
  {
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDenominacionTicket.tipoPersonal;
    if (localStateManager.isLoaded(paramDenominacionTicket, jdoInheritedFieldCount + 5))
      return paramDenominacionTicket.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramDenominacionTicket, jdoInheritedFieldCount + 5, paramDenominacionTicket.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(DenominacionTicket paramDenominacionTicket, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDenominacionTicket.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramDenominacionTicket, jdoInheritedFieldCount + 5, paramDenominacionTicket.tipoPersonal, paramTipoPersonal);
  }

  private static final double jdoGetvalor(DenominacionTicket paramDenominacionTicket)
  {
    if (paramDenominacionTicket.jdoFlags <= 0)
      return paramDenominacionTicket.valor;
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDenominacionTicket.valor;
    if (localStateManager.isLoaded(paramDenominacionTicket, jdoInheritedFieldCount + 6))
      return paramDenominacionTicket.valor;
    return localStateManager.getDoubleField(paramDenominacionTicket, jdoInheritedFieldCount + 6, paramDenominacionTicket.valor);
  }

  private static final void jdoSetvalor(DenominacionTicket paramDenominacionTicket, double paramDouble)
  {
    if (paramDenominacionTicket.jdoFlags == 0)
    {
      paramDenominacionTicket.valor = paramDouble;
      return;
    }
    StateManager localStateManager = paramDenominacionTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDenominacionTicket.valor = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramDenominacionTicket, jdoInheritedFieldCount + 6, paramDenominacionTicket.valor, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}