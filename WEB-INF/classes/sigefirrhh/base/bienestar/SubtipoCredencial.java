package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class SubtipoCredencial
  implements Serializable, PersistenceCapable
{
  private long idSubtipoCredencial;
  private TipoCredencial tipoCredencial;
  private String codSubtipo;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codSubtipo", "idSubtipoCredencial", "nombre", "tipoCredencial" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.bienestar.TipoCredencial") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this);
  }

  public String getCodSubtipo()
  {
    return jdoGetcodSubtipo(this);
  }

  public void setCodSubtipo(String codSubtipo)
  {
    jdoSetcodSubtipo(this, codSubtipo);
  }

  public long getIdSubtipoCredencial()
  {
    return jdoGetidSubtipoCredencial(this);
  }

  public void setIdSubtipoCredencial(long idSubtipoCredencial)
  {
    jdoSetidSubtipoCredencial(this, idSubtipoCredencial);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public TipoCredencial getTipoCredencial()
  {
    return jdoGettipoCredencial(this);
  }

  public void setTipoCredencial(TipoCredencial tipoCredencial)
  {
    jdoSettipoCredencial(this, tipoCredencial);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.SubtipoCredencial"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SubtipoCredencial());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SubtipoCredencial localSubtipoCredencial = new SubtipoCredencial();
    localSubtipoCredencial.jdoFlags = 1;
    localSubtipoCredencial.jdoStateManager = paramStateManager;
    return localSubtipoCredencial;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SubtipoCredencial localSubtipoCredencial = new SubtipoCredencial();
    localSubtipoCredencial.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSubtipoCredencial.jdoFlags = 1;
    localSubtipoCredencial.jdoStateManager = paramStateManager;
    return localSubtipoCredencial;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSubtipo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSubtipoCredencial);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoCredencial);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSubtipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSubtipoCredencial = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCredencial = ((TipoCredencial)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SubtipoCredencial paramSubtipoCredencial, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSubtipoCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.codSubtipo = paramSubtipoCredencial.codSubtipo;
      return;
    case 1:
      if (paramSubtipoCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.idSubtipoCredencial = paramSubtipoCredencial.idSubtipoCredencial;
      return;
    case 2:
      if (paramSubtipoCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramSubtipoCredencial.nombre;
      return;
    case 3:
      if (paramSubtipoCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCredencial = paramSubtipoCredencial.tipoCredencial;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SubtipoCredencial))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SubtipoCredencial localSubtipoCredencial = (SubtipoCredencial)paramObject;
    if (localSubtipoCredencial.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSubtipoCredencial, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SubtipoCredencialPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SubtipoCredencialPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SubtipoCredencialPK))
      throw new IllegalArgumentException("arg1");
    SubtipoCredencialPK localSubtipoCredencialPK = (SubtipoCredencialPK)paramObject;
    localSubtipoCredencialPK.idSubtipoCredencial = this.idSubtipoCredencial;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SubtipoCredencialPK))
      throw new IllegalArgumentException("arg1");
    SubtipoCredencialPK localSubtipoCredencialPK = (SubtipoCredencialPK)paramObject;
    this.idSubtipoCredencial = localSubtipoCredencialPK.idSubtipoCredencial;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SubtipoCredencialPK))
      throw new IllegalArgumentException("arg2");
    SubtipoCredencialPK localSubtipoCredencialPK = (SubtipoCredencialPK)paramObject;
    localSubtipoCredencialPK.idSubtipoCredencial = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SubtipoCredencialPK))
      throw new IllegalArgumentException("arg2");
    SubtipoCredencialPK localSubtipoCredencialPK = (SubtipoCredencialPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localSubtipoCredencialPK.idSubtipoCredencial);
  }

  private static final String jdoGetcodSubtipo(SubtipoCredencial paramSubtipoCredencial)
  {
    if (paramSubtipoCredencial.jdoFlags <= 0)
      return paramSubtipoCredencial.codSubtipo;
    StateManager localStateManager = paramSubtipoCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramSubtipoCredencial.codSubtipo;
    if (localStateManager.isLoaded(paramSubtipoCredencial, jdoInheritedFieldCount + 0))
      return paramSubtipoCredencial.codSubtipo;
    return localStateManager.getStringField(paramSubtipoCredencial, jdoInheritedFieldCount + 0, paramSubtipoCredencial.codSubtipo);
  }

  private static final void jdoSetcodSubtipo(SubtipoCredencial paramSubtipoCredencial, String paramString)
  {
    if (paramSubtipoCredencial.jdoFlags == 0)
    {
      paramSubtipoCredencial.codSubtipo = paramString;
      return;
    }
    StateManager localStateManager = paramSubtipoCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubtipoCredencial.codSubtipo = paramString;
      return;
    }
    localStateManager.setStringField(paramSubtipoCredencial, jdoInheritedFieldCount + 0, paramSubtipoCredencial.codSubtipo, paramString);
  }

  private static final long jdoGetidSubtipoCredencial(SubtipoCredencial paramSubtipoCredencial)
  {
    return paramSubtipoCredencial.idSubtipoCredencial;
  }

  private static final void jdoSetidSubtipoCredencial(SubtipoCredencial paramSubtipoCredencial, long paramLong)
  {
    StateManager localStateManager = paramSubtipoCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubtipoCredencial.idSubtipoCredencial = paramLong;
      return;
    }
    localStateManager.setLongField(paramSubtipoCredencial, jdoInheritedFieldCount + 1, paramSubtipoCredencial.idSubtipoCredencial, paramLong);
  }

  private static final String jdoGetnombre(SubtipoCredencial paramSubtipoCredencial)
  {
    if (paramSubtipoCredencial.jdoFlags <= 0)
      return paramSubtipoCredencial.nombre;
    StateManager localStateManager = paramSubtipoCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramSubtipoCredencial.nombre;
    if (localStateManager.isLoaded(paramSubtipoCredencial, jdoInheritedFieldCount + 2))
      return paramSubtipoCredencial.nombre;
    return localStateManager.getStringField(paramSubtipoCredencial, jdoInheritedFieldCount + 2, paramSubtipoCredencial.nombre);
  }

  private static final void jdoSetnombre(SubtipoCredencial paramSubtipoCredencial, String paramString)
  {
    if (paramSubtipoCredencial.jdoFlags == 0)
    {
      paramSubtipoCredencial.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramSubtipoCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubtipoCredencial.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramSubtipoCredencial, jdoInheritedFieldCount + 2, paramSubtipoCredencial.nombre, paramString);
  }

  private static final TipoCredencial jdoGettipoCredencial(SubtipoCredencial paramSubtipoCredencial)
  {
    StateManager localStateManager = paramSubtipoCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramSubtipoCredencial.tipoCredencial;
    if (localStateManager.isLoaded(paramSubtipoCredencial, jdoInheritedFieldCount + 3))
      return paramSubtipoCredencial.tipoCredencial;
    return (TipoCredencial)localStateManager.getObjectField(paramSubtipoCredencial, jdoInheritedFieldCount + 3, paramSubtipoCredencial.tipoCredencial);
  }

  private static final void jdoSettipoCredencial(SubtipoCredencial paramSubtipoCredencial, TipoCredencial paramTipoCredencial)
  {
    StateManager localStateManager = paramSubtipoCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubtipoCredencial.tipoCredencial = paramTipoCredencial;
      return;
    }
    localStateManager.setObjectField(paramSubtipoCredencial, jdoInheritedFieldCount + 3, paramSubtipoCredencial.tipoCredencial, paramTipoCredencial);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}