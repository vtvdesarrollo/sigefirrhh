package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class ParametroBecaGeneralPK
  implements Serializable
{
  public long idParametroBecaGeneral;

  public ParametroBecaGeneralPK()
  {
  }

  public ParametroBecaGeneralPK(long idParametroBecaGeneral)
  {
    this.idParametroBecaGeneral = idParametroBecaGeneral;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroBecaGeneralPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroBecaGeneralPK thatPK)
  {
    return 
      this.idParametroBecaGeneral == thatPK.idParametroBecaGeneral;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroBecaGeneral)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroBecaGeneral);
  }
}