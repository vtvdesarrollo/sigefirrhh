package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class ParametroUtilesPK
  implements Serializable
{
  public long idParametroUtiles;

  public ParametroUtilesPK()
  {
  }

  public ParametroUtilesPK(long idParametroUtiles)
  {
    this.idParametroUtiles = idParametroUtiles;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroUtilesPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroUtilesPK thatPK)
  {
    return 
      this.idParametroUtiles == thatPK.idParametroUtiles;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroUtiles)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroUtiles);
  }
}