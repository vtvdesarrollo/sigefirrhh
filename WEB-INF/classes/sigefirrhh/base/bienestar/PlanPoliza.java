package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PlanPoliza
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idPlanPoliza;
  private Poliza poliza;
  private String codPlanPoliza;
  private String nombre;
  private String beneficiarios;
  private String primaUnica;
  private double montoPrimaUnica;
  private double coberturaUnica;
  private String exceso;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "beneficiarios", "coberturaUnica", "codPlanPoliza", "exceso", "idPlanPoliza", "montoPrimaUnica", "nombre", "poliza", "primaUnica" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.bienestar.Poliza"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.PlanPoliza"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PlanPoliza());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public PlanPoliza()
  {
    jdoSetbeneficiarios(this, "S");

    jdoSetprimaUnica(this, "N");
  }

  public String toString()
  {
    return jdoGetcodPlanPoliza(this) + "-" + 
      jdoGetnombre(this);
  }

  public String getBeneficiarios()
  {
    return jdoGetbeneficiarios(this);
  }

  public void setBeneficiarios(String beneficiarios)
  {
    jdoSetbeneficiarios(this, beneficiarios);
  }

  public double getCoberturaUnica()
  {
    return jdoGetcoberturaUnica(this);
  }

  public void setCoberturaUnica(double coberturaUnica)
  {
    jdoSetcoberturaUnica(this, coberturaUnica);
  }

  public String getCodPlanPoliza()
  {
    return jdoGetcodPlanPoliza(this);
  }

  public void setCodPlanPoliza(String codPlanPoliza)
  {
    jdoSetcodPlanPoliza(this, codPlanPoliza);
  }

  public String getExceso()
  {
    return jdoGetexceso(this);
  }

  public void setExceso(String exceso)
  {
    jdoSetexceso(this, exceso);
  }

  public long getIdPlanPoliza()
  {
    return jdoGetidPlanPoliza(this);
  }

  public void setIdPlanPoliza(long idPlanPoliza)
  {
    jdoSetidPlanPoliza(this, idPlanPoliza);
  }

  public double getMontoPrimaUnica()
  {
    return jdoGetmontoPrimaUnica(this);
  }

  public void setMontoPrimaUnica(double montoPrimaUnica)
  {
    jdoSetmontoPrimaUnica(this, montoPrimaUnica);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public Poliza getPoliza()
  {
    return jdoGetpoliza(this);
  }

  public void setPoliza(Poliza poliza)
  {
    jdoSetpoliza(this, poliza);
  }

  public String getPrimaUnica()
  {
    return jdoGetprimaUnica(this);
  }

  public void setPrimaUnica(String primaUnica)
  {
    jdoSetprimaUnica(this, primaUnica);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PlanPoliza localPlanPoliza = new PlanPoliza();
    localPlanPoliza.jdoFlags = 1;
    localPlanPoliza.jdoStateManager = paramStateManager;
    return localPlanPoliza;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PlanPoliza localPlanPoliza = new PlanPoliza();
    localPlanPoliza.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPlanPoliza.jdoFlags = 1;
    localPlanPoliza.jdoStateManager = paramStateManager;
    return localPlanPoliza;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.beneficiarios);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.coberturaUnica);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codPlanPoliza);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.exceso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPlanPoliza);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPrimaUnica);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.poliza);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primaUnica);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.beneficiarios = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.coberturaUnica = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codPlanPoliza = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.exceso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPlanPoliza = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPrimaUnica = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.poliza = ((Poliza)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primaUnica = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PlanPoliza paramPlanPoliza, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPlanPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.beneficiarios = paramPlanPoliza.beneficiarios;
      return;
    case 1:
      if (paramPlanPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.coberturaUnica = paramPlanPoliza.coberturaUnica;
      return;
    case 2:
      if (paramPlanPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.codPlanPoliza = paramPlanPoliza.codPlanPoliza;
      return;
    case 3:
      if (paramPlanPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.exceso = paramPlanPoliza.exceso;
      return;
    case 4:
      if (paramPlanPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.idPlanPoliza = paramPlanPoliza.idPlanPoliza;
      return;
    case 5:
      if (paramPlanPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.montoPrimaUnica = paramPlanPoliza.montoPrimaUnica;
      return;
    case 6:
      if (paramPlanPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramPlanPoliza.nombre;
      return;
    case 7:
      if (paramPlanPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.poliza = paramPlanPoliza.poliza;
      return;
    case 8:
      if (paramPlanPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.primaUnica = paramPlanPoliza.primaUnica;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PlanPoliza))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PlanPoliza localPlanPoliza = (PlanPoliza)paramObject;
    if (localPlanPoliza.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPlanPoliza, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PlanPolizaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PlanPolizaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanPolizaPK))
      throw new IllegalArgumentException("arg1");
    PlanPolizaPK localPlanPolizaPK = (PlanPolizaPK)paramObject;
    localPlanPolizaPK.idPlanPoliza = this.idPlanPoliza;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanPolizaPK))
      throw new IllegalArgumentException("arg1");
    PlanPolizaPK localPlanPolizaPK = (PlanPolizaPK)paramObject;
    this.idPlanPoliza = localPlanPolizaPK.idPlanPoliza;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanPolizaPK))
      throw new IllegalArgumentException("arg2");
    PlanPolizaPK localPlanPolizaPK = (PlanPolizaPK)paramObject;
    localPlanPolizaPK.idPlanPoliza = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanPolizaPK))
      throw new IllegalArgumentException("arg2");
    PlanPolizaPK localPlanPolizaPK = (PlanPolizaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localPlanPolizaPK.idPlanPoliza);
  }

  private static final String jdoGetbeneficiarios(PlanPoliza paramPlanPoliza)
  {
    if (paramPlanPoliza.jdoFlags <= 0)
      return paramPlanPoliza.beneficiarios;
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPoliza.beneficiarios;
    if (localStateManager.isLoaded(paramPlanPoliza, jdoInheritedFieldCount + 0))
      return paramPlanPoliza.beneficiarios;
    return localStateManager.getStringField(paramPlanPoliza, jdoInheritedFieldCount + 0, paramPlanPoliza.beneficiarios);
  }

  private static final void jdoSetbeneficiarios(PlanPoliza paramPlanPoliza, String paramString)
  {
    if (paramPlanPoliza.jdoFlags == 0)
    {
      paramPlanPoliza.beneficiarios = paramString;
      return;
    }
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPoliza.beneficiarios = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanPoliza, jdoInheritedFieldCount + 0, paramPlanPoliza.beneficiarios, paramString);
  }

  private static final double jdoGetcoberturaUnica(PlanPoliza paramPlanPoliza)
  {
    if (paramPlanPoliza.jdoFlags <= 0)
      return paramPlanPoliza.coberturaUnica;
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPoliza.coberturaUnica;
    if (localStateManager.isLoaded(paramPlanPoliza, jdoInheritedFieldCount + 1))
      return paramPlanPoliza.coberturaUnica;
    return localStateManager.getDoubleField(paramPlanPoliza, jdoInheritedFieldCount + 1, paramPlanPoliza.coberturaUnica);
  }

  private static final void jdoSetcoberturaUnica(PlanPoliza paramPlanPoliza, double paramDouble)
  {
    if (paramPlanPoliza.jdoFlags == 0)
    {
      paramPlanPoliza.coberturaUnica = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPoliza.coberturaUnica = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanPoliza, jdoInheritedFieldCount + 1, paramPlanPoliza.coberturaUnica, paramDouble);
  }

  private static final String jdoGetcodPlanPoliza(PlanPoliza paramPlanPoliza)
  {
    if (paramPlanPoliza.jdoFlags <= 0)
      return paramPlanPoliza.codPlanPoliza;
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPoliza.codPlanPoliza;
    if (localStateManager.isLoaded(paramPlanPoliza, jdoInheritedFieldCount + 2))
      return paramPlanPoliza.codPlanPoliza;
    return localStateManager.getStringField(paramPlanPoliza, jdoInheritedFieldCount + 2, paramPlanPoliza.codPlanPoliza);
  }

  private static final void jdoSetcodPlanPoliza(PlanPoliza paramPlanPoliza, String paramString)
  {
    if (paramPlanPoliza.jdoFlags == 0)
    {
      paramPlanPoliza.codPlanPoliza = paramString;
      return;
    }
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPoliza.codPlanPoliza = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanPoliza, jdoInheritedFieldCount + 2, paramPlanPoliza.codPlanPoliza, paramString);
  }

  private static final String jdoGetexceso(PlanPoliza paramPlanPoliza)
  {
    if (paramPlanPoliza.jdoFlags <= 0)
      return paramPlanPoliza.exceso;
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPoliza.exceso;
    if (localStateManager.isLoaded(paramPlanPoliza, jdoInheritedFieldCount + 3))
      return paramPlanPoliza.exceso;
    return localStateManager.getStringField(paramPlanPoliza, jdoInheritedFieldCount + 3, paramPlanPoliza.exceso);
  }

  private static final void jdoSetexceso(PlanPoliza paramPlanPoliza, String paramString)
  {
    if (paramPlanPoliza.jdoFlags == 0)
    {
      paramPlanPoliza.exceso = paramString;
      return;
    }
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPoliza.exceso = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanPoliza, jdoInheritedFieldCount + 3, paramPlanPoliza.exceso, paramString);
  }

  private static final long jdoGetidPlanPoliza(PlanPoliza paramPlanPoliza)
  {
    return paramPlanPoliza.idPlanPoliza;
  }

  private static final void jdoSetidPlanPoliza(PlanPoliza paramPlanPoliza, long paramLong)
  {
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPoliza.idPlanPoliza = paramLong;
      return;
    }
    localStateManager.setLongField(paramPlanPoliza, jdoInheritedFieldCount + 4, paramPlanPoliza.idPlanPoliza, paramLong);
  }

  private static final double jdoGetmontoPrimaUnica(PlanPoliza paramPlanPoliza)
  {
    if (paramPlanPoliza.jdoFlags <= 0)
      return paramPlanPoliza.montoPrimaUnica;
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPoliza.montoPrimaUnica;
    if (localStateManager.isLoaded(paramPlanPoliza, jdoInheritedFieldCount + 5))
      return paramPlanPoliza.montoPrimaUnica;
    return localStateManager.getDoubleField(paramPlanPoliza, jdoInheritedFieldCount + 5, paramPlanPoliza.montoPrimaUnica);
  }

  private static final void jdoSetmontoPrimaUnica(PlanPoliza paramPlanPoliza, double paramDouble)
  {
    if (paramPlanPoliza.jdoFlags == 0)
    {
      paramPlanPoliza.montoPrimaUnica = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPoliza.montoPrimaUnica = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanPoliza, jdoInheritedFieldCount + 5, paramPlanPoliza.montoPrimaUnica, paramDouble);
  }

  private static final String jdoGetnombre(PlanPoliza paramPlanPoliza)
  {
    if (paramPlanPoliza.jdoFlags <= 0)
      return paramPlanPoliza.nombre;
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPoliza.nombre;
    if (localStateManager.isLoaded(paramPlanPoliza, jdoInheritedFieldCount + 6))
      return paramPlanPoliza.nombre;
    return localStateManager.getStringField(paramPlanPoliza, jdoInheritedFieldCount + 6, paramPlanPoliza.nombre);
  }

  private static final void jdoSetnombre(PlanPoliza paramPlanPoliza, String paramString)
  {
    if (paramPlanPoliza.jdoFlags == 0)
    {
      paramPlanPoliza.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPoliza.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanPoliza, jdoInheritedFieldCount + 6, paramPlanPoliza.nombre, paramString);
  }

  private static final Poliza jdoGetpoliza(PlanPoliza paramPlanPoliza)
  {
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPoliza.poliza;
    if (localStateManager.isLoaded(paramPlanPoliza, jdoInheritedFieldCount + 7))
      return paramPlanPoliza.poliza;
    return (Poliza)localStateManager.getObjectField(paramPlanPoliza, jdoInheritedFieldCount + 7, paramPlanPoliza.poliza);
  }

  private static final void jdoSetpoliza(PlanPoliza paramPlanPoliza, Poliza paramPoliza)
  {
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPoliza.poliza = paramPoliza;
      return;
    }
    localStateManager.setObjectField(paramPlanPoliza, jdoInheritedFieldCount + 7, paramPlanPoliza.poliza, paramPoliza);
  }

  private static final String jdoGetprimaUnica(PlanPoliza paramPlanPoliza)
  {
    if (paramPlanPoliza.jdoFlags <= 0)
      return paramPlanPoliza.primaUnica;
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPoliza.primaUnica;
    if (localStateManager.isLoaded(paramPlanPoliza, jdoInheritedFieldCount + 8))
      return paramPlanPoliza.primaUnica;
    return localStateManager.getStringField(paramPlanPoliza, jdoInheritedFieldCount + 8, paramPlanPoliza.primaUnica);
  }

  private static final void jdoSetprimaUnica(PlanPoliza paramPlanPoliza, String paramString)
  {
    if (paramPlanPoliza.jdoFlags == 0)
    {
      paramPlanPoliza.primaUnica = paramString;
      return;
    }
    StateManager localStateManager = paramPlanPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPoliza.primaUnica = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanPoliza, jdoInheritedFieldCount + 8, paramPlanPoliza.primaUnica, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}