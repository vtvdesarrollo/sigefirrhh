package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PrimasPlan
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_PARENTESCO;
  protected static final Map LISTA_SINO;
  private long idPrimasPlan;
  private PlanPoliza planPoliza;
  private String parentesco;
  private int edadMinima;
  private int edadMaxima;
  private double primaFemenino;
  private double primaMasculino;
  private double porcentajePatron;
  private double porcentajeTrabajador;
  private double coberturaParentesco;
  private int maximoBeneficiarios;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "coberturaParentesco", "edadMaxima", "edadMinima", "idPrimasPlan", "maximoBeneficiarios", "parentesco", "planPoliza", "porcentajePatron", "porcentajeTrabajador", "primaFemenino", "primaMasculino" }; private static final Class[] jdoFieldTypes = { Double.TYPE, Integer.TYPE, Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.bienestar.PlanPoliza"), Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 26, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.PrimasPlan"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PrimasPlan());

    LISTA_PARENTESCO = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_PARENTESCO.put("T", "TITULAR");
    LISTA_PARENTESCO.put("C", "CONYUGE");
    LISTA_PARENTESCO.put("M", "MADRE");
    LISTA_PARENTESCO.put("P", "PADRE");
    LISTA_PARENTESCO.put("H", "HIJO(A)");
    LISTA_PARENTESCO.put("E", "HERMANO(A)");
    LISTA_PARENTESCO.put("S", "SUEGRO(A)");
    LISTA_PARENTESCO.put("A", "ABUELO(A)");
    LISTA_PARENTESCO.put("B", "SOBRINO(A)");
    LISTA_PARENTESCO.put("I", "TIO(A)");
    LISTA_PARENTESCO.put("U", "TUTELADO(A)");
    LISTA_PARENTESCO.put("O", "OTRO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public PrimasPlan()
  {
    jdoSetparentesco(this, "T");

    jdoSetedadMinima(this, 0);

    jdoSetedadMaxima(this, 0);

    jdoSetporcentajePatron(this, 0.0D);

    jdoSetporcentajeTrabajador(this, 0.0D);

    jdoSetcoberturaParentesco(this, 0.0D);

    jdoSetmaximoBeneficiarios(this, 0);
  }

  public String toString() {
    return jdoGetplanPoliza(this).getNombre() + " - " + 
      LISTA_PARENTESCO.get(jdoGetparentesco(this));
  }

  public double getCoberturaParentesco()
  {
    return jdoGetcoberturaParentesco(this);
  }

  public void setCoberturaParentesco(double coberturaParentesco)
  {
    jdoSetcoberturaParentesco(this, coberturaParentesco);
  }

  public int getEdadMaxima()
  {
    return jdoGetedadMaxima(this);
  }

  public void setEdadMaxima(int edadMaxima)
  {
    jdoSetedadMaxima(this, edadMaxima);
  }

  public int getEdadMinima()
  {
    return jdoGetedadMinima(this);
  }

  public void setEdadMinima(int edadMinima)
  {
    jdoSetedadMinima(this, edadMinima);
  }

  public long getIdPrimasPlan()
  {
    return jdoGetidPrimasPlan(this);
  }

  public void setIdPrimasPlan(long idPrimasPlan)
  {
    jdoSetidPrimasPlan(this, idPrimasPlan);
  }

  public int getMaximoBeneficiarios()
  {
    return jdoGetmaximoBeneficiarios(this);
  }

  public void setMaximoBeneficiarios(int maximoBeneficiarios)
  {
    jdoSetmaximoBeneficiarios(this, maximoBeneficiarios);
  }

  public String getParentesco()
  {
    return jdoGetparentesco(this);
  }

  public void setParentesco(String parentesco)
  {
    jdoSetparentesco(this, parentesco);
  }

  public PlanPoliza getPlanPoliza()
  {
    return jdoGetplanPoliza(this);
  }

  public void setPlanPoliza(PlanPoliza planPoliza)
  {
    jdoSetplanPoliza(this, planPoliza);
  }

  public double getPorcentajePatron()
  {
    return jdoGetporcentajePatron(this);
  }

  public void setPorcentajePatron(double porcentajePatron)
  {
    jdoSetporcentajePatron(this, porcentajePatron);
  }

  public double getPorcentajeTrabajador()
  {
    return jdoGetporcentajeTrabajador(this);
  }

  public void setPorcentajeTrabajador(double porcentajeTrabajador)
  {
    jdoSetporcentajeTrabajador(this, porcentajeTrabajador);
  }

  public double getPrimaFemenino()
  {
    return jdoGetprimaFemenino(this);
  }

  public void setPrimaFemenino(double primaFemenino)
  {
    jdoSetprimaFemenino(this, primaFemenino);
  }

  public double getPrimaMasculino()
  {
    return jdoGetprimaMasculino(this);
  }

  public void setPrimaMasculino(double primaMasculino)
  {
    jdoSetprimaMasculino(this, primaMasculino);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PrimasPlan localPrimasPlan = new PrimasPlan();
    localPrimasPlan.jdoFlags = 1;
    localPrimasPlan.jdoStateManager = paramStateManager;
    return localPrimasPlan;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PrimasPlan localPrimasPlan = new PrimasPlan();
    localPrimasPlan.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPrimasPlan.jdoFlags = 1;
    localPrimasPlan.jdoStateManager = paramStateManager;
    return localPrimasPlan;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.coberturaParentesco);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadMaxima);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadMinima);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPrimasPlan);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.maximoBeneficiarios);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.parentesco);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPoliza);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajePatron);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeTrabajador);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primaFemenino);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primaMasculino);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.coberturaParentesco = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadMaxima = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadMinima = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPrimasPlan = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.maximoBeneficiarios = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.parentesco = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPoliza = ((PlanPoliza)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajePatron = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primaFemenino = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primaMasculino = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PrimasPlan paramPrimasPlan, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPrimasPlan == null)
        throw new IllegalArgumentException("arg1");
      this.coberturaParentesco = paramPrimasPlan.coberturaParentesco;
      return;
    case 1:
      if (paramPrimasPlan == null)
        throw new IllegalArgumentException("arg1");
      this.edadMaxima = paramPrimasPlan.edadMaxima;
      return;
    case 2:
      if (paramPrimasPlan == null)
        throw new IllegalArgumentException("arg1");
      this.edadMinima = paramPrimasPlan.edadMinima;
      return;
    case 3:
      if (paramPrimasPlan == null)
        throw new IllegalArgumentException("arg1");
      this.idPrimasPlan = paramPrimasPlan.idPrimasPlan;
      return;
    case 4:
      if (paramPrimasPlan == null)
        throw new IllegalArgumentException("arg1");
      this.maximoBeneficiarios = paramPrimasPlan.maximoBeneficiarios;
      return;
    case 5:
      if (paramPrimasPlan == null)
        throw new IllegalArgumentException("arg1");
      this.parentesco = paramPrimasPlan.parentesco;
      return;
    case 6:
      if (paramPrimasPlan == null)
        throw new IllegalArgumentException("arg1");
      this.planPoliza = paramPrimasPlan.planPoliza;
      return;
    case 7:
      if (paramPrimasPlan == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajePatron = paramPrimasPlan.porcentajePatron;
      return;
    case 8:
      if (paramPrimasPlan == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeTrabajador = paramPrimasPlan.porcentajeTrabajador;
      return;
    case 9:
      if (paramPrimasPlan == null)
        throw new IllegalArgumentException("arg1");
      this.primaFemenino = paramPrimasPlan.primaFemenino;
      return;
    case 10:
      if (paramPrimasPlan == null)
        throw new IllegalArgumentException("arg1");
      this.primaMasculino = paramPrimasPlan.primaMasculino;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PrimasPlan))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PrimasPlan localPrimasPlan = (PrimasPlan)paramObject;
    if (localPrimasPlan.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPrimasPlan, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PrimasPlanPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PrimasPlanPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrimasPlanPK))
      throw new IllegalArgumentException("arg1");
    PrimasPlanPK localPrimasPlanPK = (PrimasPlanPK)paramObject;
    localPrimasPlanPK.idPrimasPlan = this.idPrimasPlan;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrimasPlanPK))
      throw new IllegalArgumentException("arg1");
    PrimasPlanPK localPrimasPlanPK = (PrimasPlanPK)paramObject;
    this.idPrimasPlan = localPrimasPlanPK.idPrimasPlan;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrimasPlanPK))
      throw new IllegalArgumentException("arg2");
    PrimasPlanPK localPrimasPlanPK = (PrimasPlanPK)paramObject;
    localPrimasPlanPK.idPrimasPlan = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrimasPlanPK))
      throw new IllegalArgumentException("arg2");
    PrimasPlanPK localPrimasPlanPK = (PrimasPlanPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localPrimasPlanPK.idPrimasPlan);
  }

  private static final double jdoGetcoberturaParentesco(PrimasPlan paramPrimasPlan)
  {
    if (paramPrimasPlan.jdoFlags <= 0)
      return paramPrimasPlan.coberturaParentesco;
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasPlan.coberturaParentesco;
    if (localStateManager.isLoaded(paramPrimasPlan, jdoInheritedFieldCount + 0))
      return paramPrimasPlan.coberturaParentesco;
    return localStateManager.getDoubleField(paramPrimasPlan, jdoInheritedFieldCount + 0, paramPrimasPlan.coberturaParentesco);
  }

  private static final void jdoSetcoberturaParentesco(PrimasPlan paramPrimasPlan, double paramDouble)
  {
    if (paramPrimasPlan.jdoFlags == 0)
    {
      paramPrimasPlan.coberturaParentesco = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasPlan.coberturaParentesco = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrimasPlan, jdoInheritedFieldCount + 0, paramPrimasPlan.coberturaParentesco, paramDouble);
  }

  private static final int jdoGetedadMaxima(PrimasPlan paramPrimasPlan)
  {
    if (paramPrimasPlan.jdoFlags <= 0)
      return paramPrimasPlan.edadMaxima;
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasPlan.edadMaxima;
    if (localStateManager.isLoaded(paramPrimasPlan, jdoInheritedFieldCount + 1))
      return paramPrimasPlan.edadMaxima;
    return localStateManager.getIntField(paramPrimasPlan, jdoInheritedFieldCount + 1, paramPrimasPlan.edadMaxima);
  }

  private static final void jdoSetedadMaxima(PrimasPlan paramPrimasPlan, int paramInt)
  {
    if (paramPrimasPlan.jdoFlags == 0)
    {
      paramPrimasPlan.edadMaxima = paramInt;
      return;
    }
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasPlan.edadMaxima = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrimasPlan, jdoInheritedFieldCount + 1, paramPrimasPlan.edadMaxima, paramInt);
  }

  private static final int jdoGetedadMinima(PrimasPlan paramPrimasPlan)
  {
    if (paramPrimasPlan.jdoFlags <= 0)
      return paramPrimasPlan.edadMinima;
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasPlan.edadMinima;
    if (localStateManager.isLoaded(paramPrimasPlan, jdoInheritedFieldCount + 2))
      return paramPrimasPlan.edadMinima;
    return localStateManager.getIntField(paramPrimasPlan, jdoInheritedFieldCount + 2, paramPrimasPlan.edadMinima);
  }

  private static final void jdoSetedadMinima(PrimasPlan paramPrimasPlan, int paramInt)
  {
    if (paramPrimasPlan.jdoFlags == 0)
    {
      paramPrimasPlan.edadMinima = paramInt;
      return;
    }
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasPlan.edadMinima = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrimasPlan, jdoInheritedFieldCount + 2, paramPrimasPlan.edadMinima, paramInt);
  }

  private static final long jdoGetidPrimasPlan(PrimasPlan paramPrimasPlan)
  {
    return paramPrimasPlan.idPrimasPlan;
  }

  private static final void jdoSetidPrimasPlan(PrimasPlan paramPrimasPlan, long paramLong)
  {
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasPlan.idPrimasPlan = paramLong;
      return;
    }
    localStateManager.setLongField(paramPrimasPlan, jdoInheritedFieldCount + 3, paramPrimasPlan.idPrimasPlan, paramLong);
  }

  private static final int jdoGetmaximoBeneficiarios(PrimasPlan paramPrimasPlan)
  {
    if (paramPrimasPlan.jdoFlags <= 0)
      return paramPrimasPlan.maximoBeneficiarios;
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasPlan.maximoBeneficiarios;
    if (localStateManager.isLoaded(paramPrimasPlan, jdoInheritedFieldCount + 4))
      return paramPrimasPlan.maximoBeneficiarios;
    return localStateManager.getIntField(paramPrimasPlan, jdoInheritedFieldCount + 4, paramPrimasPlan.maximoBeneficiarios);
  }

  private static final void jdoSetmaximoBeneficiarios(PrimasPlan paramPrimasPlan, int paramInt)
  {
    if (paramPrimasPlan.jdoFlags == 0)
    {
      paramPrimasPlan.maximoBeneficiarios = paramInt;
      return;
    }
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasPlan.maximoBeneficiarios = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrimasPlan, jdoInheritedFieldCount + 4, paramPrimasPlan.maximoBeneficiarios, paramInt);
  }

  private static final String jdoGetparentesco(PrimasPlan paramPrimasPlan)
  {
    if (paramPrimasPlan.jdoFlags <= 0)
      return paramPrimasPlan.parentesco;
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasPlan.parentesco;
    if (localStateManager.isLoaded(paramPrimasPlan, jdoInheritedFieldCount + 5))
      return paramPrimasPlan.parentesco;
    return localStateManager.getStringField(paramPrimasPlan, jdoInheritedFieldCount + 5, paramPrimasPlan.parentesco);
  }

  private static final void jdoSetparentesco(PrimasPlan paramPrimasPlan, String paramString)
  {
    if (paramPrimasPlan.jdoFlags == 0)
    {
      paramPrimasPlan.parentesco = paramString;
      return;
    }
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasPlan.parentesco = paramString;
      return;
    }
    localStateManager.setStringField(paramPrimasPlan, jdoInheritedFieldCount + 5, paramPrimasPlan.parentesco, paramString);
  }

  private static final PlanPoliza jdoGetplanPoliza(PrimasPlan paramPrimasPlan)
  {
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasPlan.planPoliza;
    if (localStateManager.isLoaded(paramPrimasPlan, jdoInheritedFieldCount + 6))
      return paramPrimasPlan.planPoliza;
    return (PlanPoliza)localStateManager.getObjectField(paramPrimasPlan, jdoInheritedFieldCount + 6, paramPrimasPlan.planPoliza);
  }

  private static final void jdoSetplanPoliza(PrimasPlan paramPrimasPlan, PlanPoliza paramPlanPoliza)
  {
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasPlan.planPoliza = paramPlanPoliza;
      return;
    }
    localStateManager.setObjectField(paramPrimasPlan, jdoInheritedFieldCount + 6, paramPrimasPlan.planPoliza, paramPlanPoliza);
  }

  private static final double jdoGetporcentajePatron(PrimasPlan paramPrimasPlan)
  {
    if (paramPrimasPlan.jdoFlags <= 0)
      return paramPrimasPlan.porcentajePatron;
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasPlan.porcentajePatron;
    if (localStateManager.isLoaded(paramPrimasPlan, jdoInheritedFieldCount + 7))
      return paramPrimasPlan.porcentajePatron;
    return localStateManager.getDoubleField(paramPrimasPlan, jdoInheritedFieldCount + 7, paramPrimasPlan.porcentajePatron);
  }

  private static final void jdoSetporcentajePatron(PrimasPlan paramPrimasPlan, double paramDouble)
  {
    if (paramPrimasPlan.jdoFlags == 0)
    {
      paramPrimasPlan.porcentajePatron = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasPlan.porcentajePatron = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrimasPlan, jdoInheritedFieldCount + 7, paramPrimasPlan.porcentajePatron, paramDouble);
  }

  private static final double jdoGetporcentajeTrabajador(PrimasPlan paramPrimasPlan)
  {
    if (paramPrimasPlan.jdoFlags <= 0)
      return paramPrimasPlan.porcentajeTrabajador;
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasPlan.porcentajeTrabajador;
    if (localStateManager.isLoaded(paramPrimasPlan, jdoInheritedFieldCount + 8))
      return paramPrimasPlan.porcentajeTrabajador;
    return localStateManager.getDoubleField(paramPrimasPlan, jdoInheritedFieldCount + 8, paramPrimasPlan.porcentajeTrabajador);
  }

  private static final void jdoSetporcentajeTrabajador(PrimasPlan paramPrimasPlan, double paramDouble)
  {
    if (paramPrimasPlan.jdoFlags == 0)
    {
      paramPrimasPlan.porcentajeTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasPlan.porcentajeTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrimasPlan, jdoInheritedFieldCount + 8, paramPrimasPlan.porcentajeTrabajador, paramDouble);
  }

  private static final double jdoGetprimaFemenino(PrimasPlan paramPrimasPlan)
  {
    if (paramPrimasPlan.jdoFlags <= 0)
      return paramPrimasPlan.primaFemenino;
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasPlan.primaFemenino;
    if (localStateManager.isLoaded(paramPrimasPlan, jdoInheritedFieldCount + 9))
      return paramPrimasPlan.primaFemenino;
    return localStateManager.getDoubleField(paramPrimasPlan, jdoInheritedFieldCount + 9, paramPrimasPlan.primaFemenino);
  }

  private static final void jdoSetprimaFemenino(PrimasPlan paramPrimasPlan, double paramDouble)
  {
    if (paramPrimasPlan.jdoFlags == 0)
    {
      paramPrimasPlan.primaFemenino = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasPlan.primaFemenino = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrimasPlan, jdoInheritedFieldCount + 9, paramPrimasPlan.primaFemenino, paramDouble);
  }

  private static final double jdoGetprimaMasculino(PrimasPlan paramPrimasPlan)
  {
    if (paramPrimasPlan.jdoFlags <= 0)
      return paramPrimasPlan.primaMasculino;
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasPlan.primaMasculino;
    if (localStateManager.isLoaded(paramPrimasPlan, jdoInheritedFieldCount + 10))
      return paramPrimasPlan.primaMasculino;
    return localStateManager.getDoubleField(paramPrimasPlan, jdoInheritedFieldCount + 10, paramPrimasPlan.primaMasculino);
  }

  private static final void jdoSetprimaMasculino(PrimasPlan paramPrimasPlan, double paramDouble)
  {
    if (paramPrimasPlan.jdoFlags == 0)
    {
      paramPrimasPlan.primaMasculino = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrimasPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasPlan.primaMasculino = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrimasPlan, jdoInheritedFieldCount + 10, paramPrimasPlan.primaMasculino, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}