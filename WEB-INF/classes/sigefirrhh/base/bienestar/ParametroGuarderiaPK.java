package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class ParametroGuarderiaPK
  implements Serializable
{
  public long idParametroGuarderia;

  public ParametroGuarderiaPK()
  {
  }

  public ParametroGuarderiaPK(long idParametroGuarderia)
  {
    this.idParametroGuarderia = idParametroGuarderia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroGuarderiaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroGuarderiaPK thatPK)
  {
    return 
      this.idParametroGuarderia == thatPK.idParametroGuarderia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroGuarderia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroGuarderia);
  }
}