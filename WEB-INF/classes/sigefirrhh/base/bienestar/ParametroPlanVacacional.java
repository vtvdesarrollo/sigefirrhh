package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class ParametroPlanVacacional
  implements Serializable, PersistenceCapable
{
  private long idParametroVacacional;
  private int anioPlan;
  private int numeroPlanVacacional;
  private String estatusPaln;
  private String descripcion;
  private int edadMinima;
  private int edadMaxima;
  private String internoExterno;
  private int maximoParticipantes;
  private int numeroParticipantes;
  private double costoEstimado;
  private double totalCosto;
  private TipoPersonal tipoPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anioPlan", "costoEstimado", "descripcion", "edadMaxima", "edadMinima", "estatusPaln", "idParametroVacacional", "internoExterno", "maximoParticipantes", "numeroParticipantes", "numeroPlanVacacional", "tipoPersonal", "totalCosto" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Double.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAnioPlan()
  {
    return jdoGetanioPlan(this);
  }

  public double getCostoEstimado()
  {
    return jdoGetcostoEstimado(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public int getEdadMaxima()
  {
    return jdoGetedadMaxima(this);
  }

  public int getEdadMinima()
  {
    return jdoGetedadMinima(this);
  }

  public String getEstatusPaln()
  {
    return jdoGetestatusPaln(this);
  }

  public long getIdParametroVacacional()
  {
    return jdoGetidParametroVacacional(this);
  }

  public String getInternoExterno()
  {
    return jdoGetinternoExterno(this);
  }

  public int getMaximoParticipantes()
  {
    return jdoGetmaximoParticipantes(this);
  }

  public int getNumeroParticipantes()
  {
    return jdoGetnumeroParticipantes(this);
  }

  public int getNumeroPlanVacacional()
  {
    return jdoGetnumeroPlanVacacional(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public double getTotalCosto()
  {
    return jdoGettotalCosto(this);
  }

  public void setAnioPlan(int i)
  {
    jdoSetanioPlan(this, i);
  }

  public void setCostoEstimado(double d)
  {
    jdoSetcostoEstimado(this, d);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setEdadMaxima(int i)
  {
    jdoSetedadMaxima(this, i);
  }

  public void setEdadMinima(int i)
  {
    jdoSetedadMinima(this, i);
  }

  public void setEstatusPaln(String string)
  {
    jdoSetestatusPaln(this, string);
  }

  public void setIdParametroVacacional(long l)
  {
    jdoSetidParametroVacacional(this, l);
  }

  public void setInternoExterno(String string)
  {
    jdoSetinternoExterno(this, string);
  }

  public void setMaximoParticipantes(int i)
  {
    jdoSetmaximoParticipantes(this, i);
  }

  public void setNumeroParticipantes(int i)
  {
    jdoSetnumeroParticipantes(this, i);
  }

  public void setNumeroPlanVacacional(int i)
  {
    jdoSetnumeroPlanVacacional(this, i);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public void setTotalCosto(double d)
  {
    jdoSettotalCosto(this, d);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.ParametroPlanVacacional"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroPlanVacacional());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroPlanVacacional localParametroPlanVacacional = new ParametroPlanVacacional();
    localParametroPlanVacacional.jdoFlags = 1;
    localParametroPlanVacacional.jdoStateManager = paramStateManager;
    return localParametroPlanVacacional;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroPlanVacacional localParametroPlanVacacional = new ParametroPlanVacacional();
    localParametroPlanVacacional.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroPlanVacacional.jdoFlags = 1;
    localParametroPlanVacacional.jdoStateManager = paramStateManager;
    return localParametroPlanVacacional;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioPlan);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costoEstimado);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadMaxima);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadMinima);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatusPaln);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroVacacional);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.internoExterno);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.maximoParticipantes);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroParticipantes);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroPlanVacacional);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalCosto);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioPlan = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costoEstimado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadMaxima = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadMinima = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatusPaln = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroVacacional = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.internoExterno = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.maximoParticipantes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroParticipantes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroPlanVacacional = localStateManager.replacingIntField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalCosto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroPlanVacacional paramParametroPlanVacacional, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.anioPlan = paramParametroPlanVacacional.anioPlan;
      return;
    case 1:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.costoEstimado = paramParametroPlanVacacional.costoEstimado;
      return;
    case 2:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramParametroPlanVacacional.descripcion;
      return;
    case 3:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.edadMaxima = paramParametroPlanVacacional.edadMaxima;
      return;
    case 4:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.edadMinima = paramParametroPlanVacacional.edadMinima;
      return;
    case 5:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.estatusPaln = paramParametroPlanVacacional.estatusPaln;
      return;
    case 6:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroVacacional = paramParametroPlanVacacional.idParametroVacacional;
      return;
    case 7:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.internoExterno = paramParametroPlanVacacional.internoExterno;
      return;
    case 8:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.maximoParticipantes = paramParametroPlanVacacional.maximoParticipantes;
      return;
    case 9:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.numeroParticipantes = paramParametroPlanVacacional.numeroParticipantes;
      return;
    case 10:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.numeroPlanVacacional = paramParametroPlanVacacional.numeroPlanVacacional;
      return;
    case 11:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramParametroPlanVacacional.tipoPersonal;
      return;
    case 12:
      if (paramParametroPlanVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.totalCosto = paramParametroPlanVacacional.totalCosto;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroPlanVacacional))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroPlanVacacional localParametroPlanVacacional = (ParametroPlanVacacional)paramObject;
    if (localParametroPlanVacacional.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroPlanVacacional, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroPlanVacacionalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroPlanVacacionalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroPlanVacacionalPK))
      throw new IllegalArgumentException("arg1");
    ParametroPlanVacacionalPK localParametroPlanVacacionalPK = (ParametroPlanVacacionalPK)paramObject;
    localParametroPlanVacacionalPK.idParametroVacacional = this.idParametroVacacional;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroPlanVacacionalPK))
      throw new IllegalArgumentException("arg1");
    ParametroPlanVacacionalPK localParametroPlanVacacionalPK = (ParametroPlanVacacionalPK)paramObject;
    this.idParametroVacacional = localParametroPlanVacacionalPK.idParametroVacacional;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroPlanVacacionalPK))
      throw new IllegalArgumentException("arg2");
    ParametroPlanVacacionalPK localParametroPlanVacacionalPK = (ParametroPlanVacacionalPK)paramObject;
    localParametroPlanVacacionalPK.idParametroVacacional = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroPlanVacacionalPK))
      throw new IllegalArgumentException("arg2");
    ParametroPlanVacacionalPK localParametroPlanVacacionalPK = (ParametroPlanVacacionalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localParametroPlanVacacionalPK.idParametroVacacional);
  }

  private static final int jdoGetanioPlan(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    if (paramParametroPlanVacacional.jdoFlags <= 0)
      return paramParametroPlanVacacional.anioPlan;
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.anioPlan;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 0))
      return paramParametroPlanVacacional.anioPlan;
    return localStateManager.getIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 0, paramParametroPlanVacacional.anioPlan);
  }

  private static final void jdoSetanioPlan(ParametroPlanVacacional paramParametroPlanVacacional, int paramInt)
  {
    if (paramParametroPlanVacacional.jdoFlags == 0)
    {
      paramParametroPlanVacacional.anioPlan = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.anioPlan = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 0, paramParametroPlanVacacional.anioPlan, paramInt);
  }

  private static final double jdoGetcostoEstimado(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    if (paramParametroPlanVacacional.jdoFlags <= 0)
      return paramParametroPlanVacacional.costoEstimado;
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.costoEstimado;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 1))
      return paramParametroPlanVacacional.costoEstimado;
    return localStateManager.getDoubleField(paramParametroPlanVacacional, jdoInheritedFieldCount + 1, paramParametroPlanVacacional.costoEstimado);
  }

  private static final void jdoSetcostoEstimado(ParametroPlanVacacional paramParametroPlanVacacional, double paramDouble)
  {
    if (paramParametroPlanVacacional.jdoFlags == 0)
    {
      paramParametroPlanVacacional.costoEstimado = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.costoEstimado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroPlanVacacional, jdoInheritedFieldCount + 1, paramParametroPlanVacacional.costoEstimado, paramDouble);
  }

  private static final String jdoGetdescripcion(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    if (paramParametroPlanVacacional.jdoFlags <= 0)
      return paramParametroPlanVacacional.descripcion;
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.descripcion;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 2))
      return paramParametroPlanVacacional.descripcion;
    return localStateManager.getStringField(paramParametroPlanVacacional, jdoInheritedFieldCount + 2, paramParametroPlanVacacional.descripcion);
  }

  private static final void jdoSetdescripcion(ParametroPlanVacacional paramParametroPlanVacacional, String paramString)
  {
    if (paramParametroPlanVacacional.jdoFlags == 0)
    {
      paramParametroPlanVacacional.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroPlanVacacional, jdoInheritedFieldCount + 2, paramParametroPlanVacacional.descripcion, paramString);
  }

  private static final int jdoGetedadMaxima(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    if (paramParametroPlanVacacional.jdoFlags <= 0)
      return paramParametroPlanVacacional.edadMaxima;
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.edadMaxima;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 3))
      return paramParametroPlanVacacional.edadMaxima;
    return localStateManager.getIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 3, paramParametroPlanVacacional.edadMaxima);
  }

  private static final void jdoSetedadMaxima(ParametroPlanVacacional paramParametroPlanVacacional, int paramInt)
  {
    if (paramParametroPlanVacacional.jdoFlags == 0)
    {
      paramParametroPlanVacacional.edadMaxima = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.edadMaxima = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 3, paramParametroPlanVacacional.edadMaxima, paramInt);
  }

  private static final int jdoGetedadMinima(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    if (paramParametroPlanVacacional.jdoFlags <= 0)
      return paramParametroPlanVacacional.edadMinima;
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.edadMinima;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 4))
      return paramParametroPlanVacacional.edadMinima;
    return localStateManager.getIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 4, paramParametroPlanVacacional.edadMinima);
  }

  private static final void jdoSetedadMinima(ParametroPlanVacacional paramParametroPlanVacacional, int paramInt)
  {
    if (paramParametroPlanVacacional.jdoFlags == 0)
    {
      paramParametroPlanVacacional.edadMinima = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.edadMinima = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 4, paramParametroPlanVacacional.edadMinima, paramInt);
  }

  private static final String jdoGetestatusPaln(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    if (paramParametroPlanVacacional.jdoFlags <= 0)
      return paramParametroPlanVacacional.estatusPaln;
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.estatusPaln;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 5))
      return paramParametroPlanVacacional.estatusPaln;
    return localStateManager.getStringField(paramParametroPlanVacacional, jdoInheritedFieldCount + 5, paramParametroPlanVacacional.estatusPaln);
  }

  private static final void jdoSetestatusPaln(ParametroPlanVacacional paramParametroPlanVacacional, String paramString)
  {
    if (paramParametroPlanVacacional.jdoFlags == 0)
    {
      paramParametroPlanVacacional.estatusPaln = paramString;
      return;
    }
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.estatusPaln = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroPlanVacacional, jdoInheritedFieldCount + 5, paramParametroPlanVacacional.estatusPaln, paramString);
  }

  private static final long jdoGetidParametroVacacional(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    return paramParametroPlanVacacional.idParametroVacacional;
  }

  private static final void jdoSetidParametroVacacional(ParametroPlanVacacional paramParametroPlanVacacional, long paramLong)
  {
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.idParametroVacacional = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroPlanVacacional, jdoInheritedFieldCount + 6, paramParametroPlanVacacional.idParametroVacacional, paramLong);
  }

  private static final String jdoGetinternoExterno(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    if (paramParametroPlanVacacional.jdoFlags <= 0)
      return paramParametroPlanVacacional.internoExterno;
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.internoExterno;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 7))
      return paramParametroPlanVacacional.internoExterno;
    return localStateManager.getStringField(paramParametroPlanVacacional, jdoInheritedFieldCount + 7, paramParametroPlanVacacional.internoExterno);
  }

  private static final void jdoSetinternoExterno(ParametroPlanVacacional paramParametroPlanVacacional, String paramString)
  {
    if (paramParametroPlanVacacional.jdoFlags == 0)
    {
      paramParametroPlanVacacional.internoExterno = paramString;
      return;
    }
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.internoExterno = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroPlanVacacional, jdoInheritedFieldCount + 7, paramParametroPlanVacacional.internoExterno, paramString);
  }

  private static final int jdoGetmaximoParticipantes(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    if (paramParametroPlanVacacional.jdoFlags <= 0)
      return paramParametroPlanVacacional.maximoParticipantes;
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.maximoParticipantes;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 8))
      return paramParametroPlanVacacional.maximoParticipantes;
    return localStateManager.getIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 8, paramParametroPlanVacacional.maximoParticipantes);
  }

  private static final void jdoSetmaximoParticipantes(ParametroPlanVacacional paramParametroPlanVacacional, int paramInt)
  {
    if (paramParametroPlanVacacional.jdoFlags == 0)
    {
      paramParametroPlanVacacional.maximoParticipantes = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.maximoParticipantes = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 8, paramParametroPlanVacacional.maximoParticipantes, paramInt);
  }

  private static final int jdoGetnumeroParticipantes(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    if (paramParametroPlanVacacional.jdoFlags <= 0)
      return paramParametroPlanVacacional.numeroParticipantes;
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.numeroParticipantes;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 9))
      return paramParametroPlanVacacional.numeroParticipantes;
    return localStateManager.getIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 9, paramParametroPlanVacacional.numeroParticipantes);
  }

  private static final void jdoSetnumeroParticipantes(ParametroPlanVacacional paramParametroPlanVacacional, int paramInt)
  {
    if (paramParametroPlanVacacional.jdoFlags == 0)
    {
      paramParametroPlanVacacional.numeroParticipantes = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.numeroParticipantes = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 9, paramParametroPlanVacacional.numeroParticipantes, paramInt);
  }

  private static final int jdoGetnumeroPlanVacacional(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    if (paramParametroPlanVacacional.jdoFlags <= 0)
      return paramParametroPlanVacacional.numeroPlanVacacional;
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.numeroPlanVacacional;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 10))
      return paramParametroPlanVacacional.numeroPlanVacacional;
    return localStateManager.getIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 10, paramParametroPlanVacacional.numeroPlanVacacional);
  }

  private static final void jdoSetnumeroPlanVacacional(ParametroPlanVacacional paramParametroPlanVacacional, int paramInt)
  {
    if (paramParametroPlanVacacional.jdoFlags == 0)
    {
      paramParametroPlanVacacional.numeroPlanVacacional = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.numeroPlanVacacional = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroPlanVacacional, jdoInheritedFieldCount + 10, paramParametroPlanVacacional.numeroPlanVacacional, paramInt);
  }

  private static final TipoPersonal jdoGettipoPersonal(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.tipoPersonal;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 11))
      return paramParametroPlanVacacional.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramParametroPlanVacacional, jdoInheritedFieldCount + 11, paramParametroPlanVacacional.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ParametroPlanVacacional paramParametroPlanVacacional, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramParametroPlanVacacional, jdoInheritedFieldCount + 11, paramParametroPlanVacacional.tipoPersonal, paramTipoPersonal);
  }

  private static final double jdoGettotalCosto(ParametroPlanVacacional paramParametroPlanVacacional)
  {
    if (paramParametroPlanVacacional.jdoFlags <= 0)
      return paramParametroPlanVacacional.totalCosto;
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramParametroPlanVacacional.totalCosto;
    if (localStateManager.isLoaded(paramParametroPlanVacacional, jdoInheritedFieldCount + 12))
      return paramParametroPlanVacacional.totalCosto;
    return localStateManager.getDoubleField(paramParametroPlanVacacional, jdoInheritedFieldCount + 12, paramParametroPlanVacacional.totalCosto);
  }

  private static final void jdoSettotalCosto(ParametroPlanVacacional paramParametroPlanVacacional, double paramDouble)
  {
    if (paramParametroPlanVacacional.jdoFlags == 0)
    {
      paramParametroPlanVacacional.totalCosto = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroPlanVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroPlanVacacional.totalCosto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroPlanVacacional, jdoInheritedFieldCount + 12, paramParametroPlanVacacional.totalCosto, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}