package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoAyuda
  implements Serializable, PersistenceCapable
{
  private long idTipoAyuda;
  private String codigoTipoAyuda;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codigoTipoAyuda", "idTipoAyuda", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String getCodigoTipoAyuda()
  {
    return jdoGetcodigoTipoAyuda(this);
  }

  public long getIdTipoAyuda()
  {
    return jdoGetidTipoAyuda(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodigoTipoAyuda(String string)
  {
    jdoSetcodigoTipoAyuda(this, string);
  }

  public void setIdTipoAyuda(long l)
  {
    jdoSetidTipoAyuda(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.TipoAyuda"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoAyuda());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoAyuda localTipoAyuda = new TipoAyuda();
    localTipoAyuda.jdoFlags = 1;
    localTipoAyuda.jdoStateManager = paramStateManager;
    return localTipoAyuda;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoAyuda localTipoAyuda = new TipoAyuda();
    localTipoAyuda.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoAyuda.jdoFlags = 1;
    localTipoAyuda.jdoStateManager = paramStateManager;
    return localTipoAyuda;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoTipoAyuda);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoAyuda);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoTipoAyuda = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoAyuda = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoAyuda paramTipoAyuda, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoAyuda == null)
        throw new IllegalArgumentException("arg1");
      this.codigoTipoAyuda = paramTipoAyuda.codigoTipoAyuda;
      return;
    case 1:
      if (paramTipoAyuda == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoAyuda = paramTipoAyuda.idTipoAyuda;
      return;
    case 2:
      if (paramTipoAyuda == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramTipoAyuda.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoAyuda))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoAyuda localTipoAyuda = (TipoAyuda)paramObject;
    if (localTipoAyuda.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoAyuda, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoAyudaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoAyudaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoAyudaPK))
      throw new IllegalArgumentException("arg1");
    TipoAyudaPK localTipoAyudaPK = (TipoAyudaPK)paramObject;
    localTipoAyudaPK.idTipoAyuda = this.idTipoAyuda;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoAyudaPK))
      throw new IllegalArgumentException("arg1");
    TipoAyudaPK localTipoAyudaPK = (TipoAyudaPK)paramObject;
    this.idTipoAyuda = localTipoAyudaPK.idTipoAyuda;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoAyudaPK))
      throw new IllegalArgumentException("arg2");
    TipoAyudaPK localTipoAyudaPK = (TipoAyudaPK)paramObject;
    localTipoAyudaPK.idTipoAyuda = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoAyudaPK))
      throw new IllegalArgumentException("arg2");
    TipoAyudaPK localTipoAyudaPK = (TipoAyudaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localTipoAyudaPK.idTipoAyuda);
  }

  private static final String jdoGetcodigoTipoAyuda(TipoAyuda paramTipoAyuda)
  {
    if (paramTipoAyuda.jdoFlags <= 0)
      return paramTipoAyuda.codigoTipoAyuda;
    StateManager localStateManager = paramTipoAyuda.jdoStateManager;
    if (localStateManager == null)
      return paramTipoAyuda.codigoTipoAyuda;
    if (localStateManager.isLoaded(paramTipoAyuda, jdoInheritedFieldCount + 0))
      return paramTipoAyuda.codigoTipoAyuda;
    return localStateManager.getStringField(paramTipoAyuda, jdoInheritedFieldCount + 0, paramTipoAyuda.codigoTipoAyuda);
  }

  private static final void jdoSetcodigoTipoAyuda(TipoAyuda paramTipoAyuda, String paramString)
  {
    if (paramTipoAyuda.jdoFlags == 0)
    {
      paramTipoAyuda.codigoTipoAyuda = paramString;
      return;
    }
    StateManager localStateManager = paramTipoAyuda.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAyuda.codigoTipoAyuda = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoAyuda, jdoInheritedFieldCount + 0, paramTipoAyuda.codigoTipoAyuda, paramString);
  }

  private static final long jdoGetidTipoAyuda(TipoAyuda paramTipoAyuda)
  {
    return paramTipoAyuda.idTipoAyuda;
  }

  private static final void jdoSetidTipoAyuda(TipoAyuda paramTipoAyuda, long paramLong)
  {
    StateManager localStateManager = paramTipoAyuda.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAyuda.idTipoAyuda = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoAyuda, jdoInheritedFieldCount + 1, paramTipoAyuda.idTipoAyuda, paramLong);
  }

  private static final String jdoGetnombre(TipoAyuda paramTipoAyuda)
  {
    if (paramTipoAyuda.jdoFlags <= 0)
      return paramTipoAyuda.nombre;
    StateManager localStateManager = paramTipoAyuda.jdoStateManager;
    if (localStateManager == null)
      return paramTipoAyuda.nombre;
    if (localStateManager.isLoaded(paramTipoAyuda, jdoInheritedFieldCount + 2))
      return paramTipoAyuda.nombre;
    return localStateManager.getStringField(paramTipoAyuda, jdoInheritedFieldCount + 2, paramTipoAyuda.nombre);
  }

  private static final void jdoSetnombre(TipoAyuda paramTipoAyuda, String paramString)
  {
    if (paramTipoAyuda.jdoFlags == 0)
    {
      paramTipoAyuda.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramTipoAyuda.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAyuda.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoAyuda, jdoInheritedFieldCount + 2, paramTipoAyuda.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}