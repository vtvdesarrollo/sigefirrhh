package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class ParametroBecaPK
  implements Serializable
{
  public long idParametroBeca;

  public ParametroBecaPK()
  {
  }

  public ParametroBecaPK(long idParametroBeca)
  {
    this.idParametroBeca = idParametroBeca;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroBecaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroBecaPK thatPK)
  {
    return 
      this.idParametroBeca == thatPK.idParametroBeca;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroBeca)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroBeca);
  }
}