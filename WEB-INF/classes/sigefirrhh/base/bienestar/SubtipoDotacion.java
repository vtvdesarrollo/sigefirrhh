package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class SubtipoDotacion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SEXO;
  private long idSubtipoDotacion;
  private TipoDotacion tipoDotacion;
  private String codSubtipo;
  private String nombre;
  private String sexo;
  private double costo;
  private String color;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codSubtipo", "color", "costo", "idSubtipoDotacion", "nombre", "sexo", "tipoDotacion" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.bienestar.TipoDotacion") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.SubtipoDotacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SubtipoDotacion());

    LISTA_SEXO = 
      new LinkedHashMap();

    LISTA_SEXO.put("F", "FEMENINO");
    LISTA_SEXO.put("M", "MASCULINO");
    LISTA_SEXO.put("A", "AMBOS");
  }

  public SubtipoDotacion()
  {
    jdoSetsexo(this, "M");

    jdoSetcosto(this, 0.0D);
  }

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetsexo(this);
  }

  public String getCodSubtipo()
  {
    return jdoGetcodSubtipo(this);
  }

  public void setCodSubtipo(String codSubtipo)
  {
    jdoSetcodSubtipo(this, codSubtipo);
  }

  public double getCosto()
  {
    return jdoGetcosto(this);
  }

  public void setCosto(double costo)
  {
    jdoSetcosto(this, costo);
  }

  public long getIdSubtipoDotacion()
  {
    return jdoGetidSubtipoDotacion(this);
  }

  public void setIdSubtipoDotacion(long idSubtipoDotacion)
  {
    jdoSetidSubtipoDotacion(this, idSubtipoDotacion);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public String getSexo()
  {
    return jdoGetsexo(this);
  }

  public void setSexo(String sexo)
  {
    jdoSetsexo(this, sexo);
  }

  public TipoDotacion getTipoDotacion()
  {
    return jdoGettipoDotacion(this);
  }

  public void setTipoDotacion(TipoDotacion tipoDotacion)
  {
    jdoSettipoDotacion(this, tipoDotacion);
  }

  public String getColor()
  {
    return jdoGetcolor(this);
  }

  public void setColor(String color)
  {
    jdoSetcolor(this, color);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SubtipoDotacion localSubtipoDotacion = new SubtipoDotacion();
    localSubtipoDotacion.jdoFlags = 1;
    localSubtipoDotacion.jdoStateManager = paramStateManager;
    return localSubtipoDotacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SubtipoDotacion localSubtipoDotacion = new SubtipoDotacion();
    localSubtipoDotacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSubtipoDotacion.jdoFlags = 1;
    localSubtipoDotacion.jdoStateManager = paramStateManager;
    return localSubtipoDotacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSubtipo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.color);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSubtipoDotacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sexo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoDotacion);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSubtipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.color = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSubtipoDotacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sexo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoDotacion = ((TipoDotacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SubtipoDotacion paramSubtipoDotacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSubtipoDotacion == null)
        throw new IllegalArgumentException("arg1");
      this.codSubtipo = paramSubtipoDotacion.codSubtipo;
      return;
    case 1:
      if (paramSubtipoDotacion == null)
        throw new IllegalArgumentException("arg1");
      this.color = paramSubtipoDotacion.color;
      return;
    case 2:
      if (paramSubtipoDotacion == null)
        throw new IllegalArgumentException("arg1");
      this.costo = paramSubtipoDotacion.costo;
      return;
    case 3:
      if (paramSubtipoDotacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSubtipoDotacion = paramSubtipoDotacion.idSubtipoDotacion;
      return;
    case 4:
      if (paramSubtipoDotacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramSubtipoDotacion.nombre;
      return;
    case 5:
      if (paramSubtipoDotacion == null)
        throw new IllegalArgumentException("arg1");
      this.sexo = paramSubtipoDotacion.sexo;
      return;
    case 6:
      if (paramSubtipoDotacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoDotacion = paramSubtipoDotacion.tipoDotacion;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SubtipoDotacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SubtipoDotacion localSubtipoDotacion = (SubtipoDotacion)paramObject;
    if (localSubtipoDotacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSubtipoDotacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SubtipoDotacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SubtipoDotacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SubtipoDotacionPK))
      throw new IllegalArgumentException("arg1");
    SubtipoDotacionPK localSubtipoDotacionPK = (SubtipoDotacionPK)paramObject;
    localSubtipoDotacionPK.idSubtipoDotacion = this.idSubtipoDotacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SubtipoDotacionPK))
      throw new IllegalArgumentException("arg1");
    SubtipoDotacionPK localSubtipoDotacionPK = (SubtipoDotacionPK)paramObject;
    this.idSubtipoDotacion = localSubtipoDotacionPK.idSubtipoDotacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SubtipoDotacionPK))
      throw new IllegalArgumentException("arg2");
    SubtipoDotacionPK localSubtipoDotacionPK = (SubtipoDotacionPK)paramObject;
    localSubtipoDotacionPK.idSubtipoDotacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SubtipoDotacionPK))
      throw new IllegalArgumentException("arg2");
    SubtipoDotacionPK localSubtipoDotacionPK = (SubtipoDotacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localSubtipoDotacionPK.idSubtipoDotacion);
  }

  private static final String jdoGetcodSubtipo(SubtipoDotacion paramSubtipoDotacion)
  {
    if (paramSubtipoDotacion.jdoFlags <= 0)
      return paramSubtipoDotacion.codSubtipo;
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
      return paramSubtipoDotacion.codSubtipo;
    if (localStateManager.isLoaded(paramSubtipoDotacion, jdoInheritedFieldCount + 0))
      return paramSubtipoDotacion.codSubtipo;
    return localStateManager.getStringField(paramSubtipoDotacion, jdoInheritedFieldCount + 0, paramSubtipoDotacion.codSubtipo);
  }

  private static final void jdoSetcodSubtipo(SubtipoDotacion paramSubtipoDotacion, String paramString)
  {
    if (paramSubtipoDotacion.jdoFlags == 0)
    {
      paramSubtipoDotacion.codSubtipo = paramString;
      return;
    }
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubtipoDotacion.codSubtipo = paramString;
      return;
    }
    localStateManager.setStringField(paramSubtipoDotacion, jdoInheritedFieldCount + 0, paramSubtipoDotacion.codSubtipo, paramString);
  }

  private static final String jdoGetcolor(SubtipoDotacion paramSubtipoDotacion)
  {
    if (paramSubtipoDotacion.jdoFlags <= 0)
      return paramSubtipoDotacion.color;
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
      return paramSubtipoDotacion.color;
    if (localStateManager.isLoaded(paramSubtipoDotacion, jdoInheritedFieldCount + 1))
      return paramSubtipoDotacion.color;
    return localStateManager.getStringField(paramSubtipoDotacion, jdoInheritedFieldCount + 1, paramSubtipoDotacion.color);
  }

  private static final void jdoSetcolor(SubtipoDotacion paramSubtipoDotacion, String paramString)
  {
    if (paramSubtipoDotacion.jdoFlags == 0)
    {
      paramSubtipoDotacion.color = paramString;
      return;
    }
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubtipoDotacion.color = paramString;
      return;
    }
    localStateManager.setStringField(paramSubtipoDotacion, jdoInheritedFieldCount + 1, paramSubtipoDotacion.color, paramString);
  }

  private static final double jdoGetcosto(SubtipoDotacion paramSubtipoDotacion)
  {
    if (paramSubtipoDotacion.jdoFlags <= 0)
      return paramSubtipoDotacion.costo;
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
      return paramSubtipoDotacion.costo;
    if (localStateManager.isLoaded(paramSubtipoDotacion, jdoInheritedFieldCount + 2))
      return paramSubtipoDotacion.costo;
    return localStateManager.getDoubleField(paramSubtipoDotacion, jdoInheritedFieldCount + 2, paramSubtipoDotacion.costo);
  }

  private static final void jdoSetcosto(SubtipoDotacion paramSubtipoDotacion, double paramDouble)
  {
    if (paramSubtipoDotacion.jdoFlags == 0)
    {
      paramSubtipoDotacion.costo = paramDouble;
      return;
    }
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubtipoDotacion.costo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSubtipoDotacion, jdoInheritedFieldCount + 2, paramSubtipoDotacion.costo, paramDouble);
  }

  private static final long jdoGetidSubtipoDotacion(SubtipoDotacion paramSubtipoDotacion)
  {
    return paramSubtipoDotacion.idSubtipoDotacion;
  }

  private static final void jdoSetidSubtipoDotacion(SubtipoDotacion paramSubtipoDotacion, long paramLong)
  {
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubtipoDotacion.idSubtipoDotacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramSubtipoDotacion, jdoInheritedFieldCount + 3, paramSubtipoDotacion.idSubtipoDotacion, paramLong);
  }

  private static final String jdoGetnombre(SubtipoDotacion paramSubtipoDotacion)
  {
    if (paramSubtipoDotacion.jdoFlags <= 0)
      return paramSubtipoDotacion.nombre;
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
      return paramSubtipoDotacion.nombre;
    if (localStateManager.isLoaded(paramSubtipoDotacion, jdoInheritedFieldCount + 4))
      return paramSubtipoDotacion.nombre;
    return localStateManager.getStringField(paramSubtipoDotacion, jdoInheritedFieldCount + 4, paramSubtipoDotacion.nombre);
  }

  private static final void jdoSetnombre(SubtipoDotacion paramSubtipoDotacion, String paramString)
  {
    if (paramSubtipoDotacion.jdoFlags == 0)
    {
      paramSubtipoDotacion.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubtipoDotacion.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramSubtipoDotacion, jdoInheritedFieldCount + 4, paramSubtipoDotacion.nombre, paramString);
  }

  private static final String jdoGetsexo(SubtipoDotacion paramSubtipoDotacion)
  {
    if (paramSubtipoDotacion.jdoFlags <= 0)
      return paramSubtipoDotacion.sexo;
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
      return paramSubtipoDotacion.sexo;
    if (localStateManager.isLoaded(paramSubtipoDotacion, jdoInheritedFieldCount + 5))
      return paramSubtipoDotacion.sexo;
    return localStateManager.getStringField(paramSubtipoDotacion, jdoInheritedFieldCount + 5, paramSubtipoDotacion.sexo);
  }

  private static final void jdoSetsexo(SubtipoDotacion paramSubtipoDotacion, String paramString)
  {
    if (paramSubtipoDotacion.jdoFlags == 0)
    {
      paramSubtipoDotacion.sexo = paramString;
      return;
    }
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubtipoDotacion.sexo = paramString;
      return;
    }
    localStateManager.setStringField(paramSubtipoDotacion, jdoInheritedFieldCount + 5, paramSubtipoDotacion.sexo, paramString);
  }

  private static final TipoDotacion jdoGettipoDotacion(SubtipoDotacion paramSubtipoDotacion)
  {
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
      return paramSubtipoDotacion.tipoDotacion;
    if (localStateManager.isLoaded(paramSubtipoDotacion, jdoInheritedFieldCount + 6))
      return paramSubtipoDotacion.tipoDotacion;
    return (TipoDotacion)localStateManager.getObjectField(paramSubtipoDotacion, jdoInheritedFieldCount + 6, paramSubtipoDotacion.tipoDotacion);
  }

  private static final void jdoSettipoDotacion(SubtipoDotacion paramSubtipoDotacion, TipoDotacion paramTipoDotacion)
  {
    StateManager localStateManager = paramSubtipoDotacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubtipoDotacion.tipoDotacion = paramTipoDotacion;
      return;
    }
    localStateManager.setObjectField(paramSubtipoDotacion, jdoInheritedFieldCount + 6, paramSubtipoDotacion.tipoDotacion, paramTipoDotacion);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}