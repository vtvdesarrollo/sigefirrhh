package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PlanPolizaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPlanPoliza(PlanPoliza planPoliza)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PlanPoliza planPolizaNew = 
      (PlanPoliza)BeanUtils.cloneBean(
      planPoliza);

    PolizaBeanBusiness polizaBeanBusiness = new PolizaBeanBusiness();

    if (planPolizaNew.getPoliza() != null) {
      planPolizaNew.setPoliza(
        polizaBeanBusiness.findPolizaById(
        planPolizaNew.getPoliza().getIdPoliza()));
    }
    pm.makePersistent(planPolizaNew);
  }

  public void updatePlanPoliza(PlanPoliza planPoliza) throws Exception
  {
    PlanPoliza planPolizaModify = 
      findPlanPolizaById(planPoliza.getIdPlanPoliza());

    PolizaBeanBusiness polizaBeanBusiness = new PolizaBeanBusiness();

    if (planPoliza.getPoliza() != null) {
      planPoliza.setPoliza(
        polizaBeanBusiness.findPolizaById(
        planPoliza.getPoliza().getIdPoliza()));
    }

    BeanUtils.copyProperties(planPolizaModify, planPoliza);
  }

  public void deletePlanPoliza(PlanPoliza planPoliza) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PlanPoliza planPolizaDelete = 
      findPlanPolizaById(planPoliza.getIdPlanPoliza());
    pm.deletePersistent(planPolizaDelete);
  }

  public PlanPoliza findPlanPolizaById(long idPlanPoliza) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPlanPoliza == pIdPlanPoliza";
    Query query = pm.newQuery(PlanPoliza.class, filter);

    query.declareParameters("long pIdPlanPoliza");

    parameters.put("pIdPlanPoliza", new Long(idPlanPoliza));

    Collection colPlanPoliza = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPlanPoliza.iterator();
    return (PlanPoliza)iterator.next();
  }

  public Collection findPlanPolizaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent planPolizaExtent = pm.getExtent(
      PlanPoliza.class, true);
    Query query = pm.newQuery(planPolizaExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPoliza(long idPoliza)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "poliza.idPoliza == pIdPoliza";

    Query query = pm.newQuery(PlanPoliza.class, filter);

    query.declareParameters("long pIdPoliza");
    HashMap parameters = new HashMap();

    parameters.put("pIdPoliza", new Long(idPoliza));

    Collection colPlanPoliza = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPlanPoliza);

    return colPlanPoliza;
  }

  public Collection findByCodPlanPoliza(String codPlanPoliza)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codPlanPoliza == pCodPlanPoliza";

    Query query = pm.newQuery(PlanPoliza.class, filter);

    query.declareParameters("java.lang.String pCodPlanPoliza");
    HashMap parameters = new HashMap();

    parameters.put("pCodPlanPoliza", new String(codPlanPoliza));

    Collection colPlanPoliza = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPlanPoliza);

    return colPlanPoliza;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(PlanPoliza.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    Collection colPlanPoliza = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPlanPoliza);

    return colPlanPoliza;
  }
}