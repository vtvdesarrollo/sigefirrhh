package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class TipoSiniestroPK
  implements Serializable
{
  public long idTipoSiniestro;

  public TipoSiniestroPK()
  {
  }

  public TipoSiniestroPK(long idTipoSiniestro)
  {
    this.idTipoSiniestro = idTipoSiniestro;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoSiniestroPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoSiniestroPK thatPK)
  {
    return 
      this.idTipoSiniestro == thatPK.idTipoSiniestro;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoSiniestro)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoSiniestro);
  }
}