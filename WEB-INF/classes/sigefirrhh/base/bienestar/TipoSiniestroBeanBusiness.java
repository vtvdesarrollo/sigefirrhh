package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoSiniestroBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoSiniestro(TipoSiniestro tipoSiniestro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoSiniestro tipoSiniestroNew = 
      (TipoSiniestro)BeanUtils.cloneBean(
      tipoSiniestro);

    pm.makePersistent(tipoSiniestroNew);
  }

  public void updateTipoSiniestro(TipoSiniestro tipoSiniestro) throws Exception
  {
    TipoSiniestro tipoSiniestroModify = 
      findTipoSiniestroById(tipoSiniestro.getIdTipoSiniestro());

    BeanUtils.copyProperties(tipoSiniestroModify, tipoSiniestro);
  }

  public void deleteTipoSiniestro(TipoSiniestro tipoSiniestro) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoSiniestro tipoSiniestroDelete = 
      findTipoSiniestroById(tipoSiniestro.getIdTipoSiniestro());
    pm.deletePersistent(tipoSiniestroDelete);
  }

  public TipoSiniestro findTipoSiniestroById(long idTipoSiniestro) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoSiniestro == pIdTipoSiniestro";
    Query query = pm.newQuery(TipoSiniestro.class, filter);

    query.declareParameters("long pIdTipoSiniestro");

    parameters.put("pIdTipoSiniestro", new Long(idTipoSiniestro));

    Collection colTipoSiniestro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoSiniestro.iterator();
    return (TipoSiniestro)iterator.next();
  }

  public Collection findTipoSiniestroAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoSiniestroExtent = pm.getExtent(
      TipoSiniestro.class, true);
    Query query = pm.newQuery(tipoSiniestroExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoSiniestro(String codTipoSiniestro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoSiniestro == pCodTipoSiniestro";

    Query query = pm.newQuery(TipoSiniestro.class, filter);

    query.declareParameters("java.lang.String pCodTipoSiniestro");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoSiniestro", new String(codTipoSiniestro));

    query.setOrdering("nombre ascending");

    Collection colTipoSiniestro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoSiniestro);

    return colTipoSiniestro;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(TipoSiniestro.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colTipoSiniestro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoSiniestro);

    return colTipoSiniestro;
  }
}