package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoDotacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoDotacion(TipoDotacion tipoDotacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoDotacion tipoDotacionNew = 
      (TipoDotacion)BeanUtils.cloneBean(
      tipoDotacion);

    pm.makePersistent(tipoDotacionNew);
  }

  public void updateTipoDotacion(TipoDotacion tipoDotacion) throws Exception
  {
    TipoDotacion tipoDotacionModify = 
      findTipoDotacionById(tipoDotacion.getIdTipoDotacion());

    BeanUtils.copyProperties(tipoDotacionModify, tipoDotacion);
  }

  public void deleteTipoDotacion(TipoDotacion tipoDotacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoDotacion tipoDotacionDelete = 
      findTipoDotacionById(tipoDotacion.getIdTipoDotacion());
    pm.deletePersistent(tipoDotacionDelete);
  }

  public TipoDotacion findTipoDotacionById(long idTipoDotacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoDotacion == pIdTipoDotacion";
    Query query = pm.newQuery(TipoDotacion.class, filter);

    query.declareParameters("long pIdTipoDotacion");

    parameters.put("pIdTipoDotacion", new Long(idTipoDotacion));

    Collection colTipoDotacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoDotacion.iterator();
    return (TipoDotacion)iterator.next();
  }

  public Collection findTipoDotacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoDotacionExtent = pm.getExtent(
      TipoDotacion.class, true);
    Query query = pm.newQuery(tipoDotacionExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoDotacion(String codTipoDotacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoDotacion == pCodTipoDotacion";

    Query query = pm.newQuery(TipoDotacion.class, filter);

    query.declareParameters("java.lang.String pCodTipoDotacion");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoDotacion", new String(codTipoDotacion));

    query.setOrdering("nombre ascending");

    Collection colTipoDotacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoDotacion);

    return colTipoDotacion;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(TipoDotacion.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colTipoDotacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoDotacion);

    return colTipoDotacion;
  }
}