package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class NivelBecaPK
  implements Serializable
{
  public long idNivelBeca;

  public NivelBecaPK()
  {
  }

  public NivelBecaPK(long idNivelBeca)
  {
    this.idNivelBeca = idNivelBeca;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((NivelBecaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(NivelBecaPK thatPK)
  {
    return 
      this.idNivelBeca == thatPK.idNivelBeca;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idNivelBeca)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idNivelBeca);
  }
}