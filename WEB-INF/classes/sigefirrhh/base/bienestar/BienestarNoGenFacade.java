package sigefirrhh.base.bienestar;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class BienestarNoGenFacade extends BienestarFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private BienestarNoGenBusiness bienestarNoGenBusiness = new BienestarNoGenBusiness();

  public Collection findPlanPolizaByPersonal(long idPlanPoliza)
    throws Exception
  {
    return this.bienestarNoGenBusiness.findPlanPolizaByPersonal(idPlanPoliza);
  }

  public Collection findPlanPolizaByPersonal(long idPlanPoliza, long idPoliza)
    throws Exception
  {
    return this.bienestarNoGenBusiness.findPlanPolizaByPersonal(idPlanPoliza, idPoliza);
  }

  public long findIdParametroGuarderiaByPersonal(long idPersonal)
    throws Exception
  {
    return this.bienestarNoGenBusiness.findIdParametroGuarderiaByPersonal(idPersonal);
  }
}