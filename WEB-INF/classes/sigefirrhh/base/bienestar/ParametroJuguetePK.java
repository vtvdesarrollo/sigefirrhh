package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class ParametroJuguetePK
  implements Serializable
{
  public long idParametroJuguete;

  public ParametroJuguetePK()
  {
  }

  public ParametroJuguetePK(long idParametroJuguete)
  {
    this.idParametroJuguete = idParametroJuguete;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroJuguetePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroJuguetePK thatPK)
  {
    return 
      this.idParametroJuguete == thatPK.idParametroJuguete;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroJuguete)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroJuguete);
  }
}