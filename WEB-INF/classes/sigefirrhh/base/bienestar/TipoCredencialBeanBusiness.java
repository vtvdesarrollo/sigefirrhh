package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoCredencialBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoCredencial(TipoCredencial tipoCredencial)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoCredencial tipoCredencialNew = 
      (TipoCredencial)BeanUtils.cloneBean(
      tipoCredencial);

    pm.makePersistent(tipoCredencialNew);
  }

  public void updateTipoCredencial(TipoCredencial tipoCredencial) throws Exception
  {
    TipoCredencial tipoCredencialModify = 
      findTipoCredencialById(tipoCredencial.getIdTipoCredencial());

    BeanUtils.copyProperties(tipoCredencialModify, tipoCredencial);
  }

  public void deleteTipoCredencial(TipoCredencial tipoCredencial) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoCredencial tipoCredencialDelete = 
      findTipoCredencialById(tipoCredencial.getIdTipoCredencial());
    pm.deletePersistent(tipoCredencialDelete);
  }

  public TipoCredencial findTipoCredencialById(long idTipoCredencial) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoCredencial == pIdTipoCredencial";
    Query query = pm.newQuery(TipoCredencial.class, filter);

    query.declareParameters("long pIdTipoCredencial");

    parameters.put("pIdTipoCredencial", new Long(idTipoCredencial));

    Collection colTipoCredencial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoCredencial.iterator();
    return (TipoCredencial)iterator.next();
  }

  public Collection findTipoCredencialAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoCredencialExtent = pm.getExtent(
      TipoCredencial.class, true);
    Query query = pm.newQuery(tipoCredencialExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoCredencial(String codTipoCredencial)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoCredencial == pCodTipoCredencial";

    Query query = pm.newQuery(TipoCredencial.class, filter);

    query.declareParameters("java.lang.String pCodTipoCredencial");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoCredencial", new String(codTipoCredencial));

    query.setOrdering("nombre ascending");

    Collection colTipoCredencial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoCredencial);

    return colTipoCredencial;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(TipoCredencial.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colTipoCredencial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoCredencial);

    return colTipoCredencial;
  }
}