package sigefirrhh.base.bienestar;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class TipoCredencialForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TipoCredencialForm.class.getName());
  private TipoCredencial tipoCredencial;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private boolean showTipoCredencialByCodTipoCredencial;
  private boolean showTipoCredencialByNombre;
  private String findCodTipoCredencial;
  private String findNombre;
  private Object stateResultTipoCredencialByCodTipoCredencial = null;

  private Object stateResultTipoCredencialByNombre = null;

  public String getFindCodTipoCredencial()
  {
    return this.findCodTipoCredencial;
  }
  public void setFindCodTipoCredencial(String findCodTipoCredencial) {
    this.findCodTipoCredencial = findCodTipoCredencial;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public TipoCredencial getTipoCredencial() {
    if (this.tipoCredencial == null) {
      this.tipoCredencial = new TipoCredencial();
    }
    return this.tipoCredencial;
  }

  public TipoCredencialForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findTipoCredencialByCodTipoCredencial()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findTipoCredencialByCodTipoCredencial(this.findCodTipoCredencial);
      this.showTipoCredencialByCodTipoCredencial = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoCredencialByCodTipoCredencial)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoCredencial = null;
    this.findNombre = null;

    return null;
  }

  public String findTipoCredencialByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findTipoCredencialByNombre(this.findNombre);
      this.showTipoCredencialByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoCredencialByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoCredencial = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowTipoCredencialByCodTipoCredencial() {
    return this.showTipoCredencialByCodTipoCredencial;
  }
  public boolean isShowTipoCredencialByNombre() {
    return this.showTipoCredencialByNombre;
  }

  public String selectTipoCredencial()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTipoCredencial = 
      Long.parseLong((String)requestParameterMap.get("idTipoCredencial"));
    try
    {
      this.tipoCredencial = 
        this.bienestarFacade.findTipoCredencialById(
        idTipoCredencial);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.tipoCredencial = null;
    this.showTipoCredencialByCodTipoCredencial = false;
    this.showTipoCredencialByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.bienestarFacade.addTipoCredencial(
          this.tipoCredencial);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.bienestarFacade.updateTipoCredencial(
          this.tipoCredencial);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.bienestarFacade.deleteTipoCredencial(
        this.tipoCredencial);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.tipoCredencial = new TipoCredencial();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.tipoCredencial.setIdTipoCredencial(identityGenerator.getNextSequenceNumber("sigefirrhh.base.bienestar.TipoCredencial"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.tipoCredencial = new TipoCredencial();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}