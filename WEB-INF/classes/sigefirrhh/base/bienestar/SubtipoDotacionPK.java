package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class SubtipoDotacionPK
  implements Serializable
{
  public long idSubtipoDotacion;

  public SubtipoDotacionPK()
  {
  }

  public SubtipoDotacionPK(long idSubtipoDotacion)
  {
    this.idSubtipoDotacion = idSubtipoDotacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SubtipoDotacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SubtipoDotacionPK thatPK)
  {
    return 
      this.idSubtipoDotacion == thatPK.idSubtipoDotacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSubtipoDotacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSubtipoDotacion);
  }
}