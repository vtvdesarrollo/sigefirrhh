package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoDotacion
  implements Serializable, PersistenceCapable
{
  private long idTipoDotacion;
  private String codTipoDotacion;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoDotacion", "idTipoDotacion", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodTipoDotacion(this);
  }

  public String getCodTipoDotacion()
  {
    return jdoGetcodTipoDotacion(this);
  }

  public void setCodTipoDotacion(String codTipoDotacion)
  {
    jdoSetcodTipoDotacion(this, codTipoDotacion);
  }

  public long getIdTipoDotacion()
  {
    return jdoGetidTipoDotacion(this);
  }

  public void setIdTipoDotacion(long idTipoDotacion)
  {
    jdoSetidTipoDotacion(this, idTipoDotacion);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.TipoDotacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoDotacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoDotacion localTipoDotacion = new TipoDotacion();
    localTipoDotacion.jdoFlags = 1;
    localTipoDotacion.jdoStateManager = paramStateManager;
    return localTipoDotacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoDotacion localTipoDotacion = new TipoDotacion();
    localTipoDotacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoDotacion.jdoFlags = 1;
    localTipoDotacion.jdoStateManager = paramStateManager;
    return localTipoDotacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoDotacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoDotacion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoDotacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoDotacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoDotacion paramTipoDotacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoDotacion == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoDotacion = paramTipoDotacion.codTipoDotacion;
      return;
    case 1:
      if (paramTipoDotacion == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoDotacion = paramTipoDotacion.idTipoDotacion;
      return;
    case 2:
      if (paramTipoDotacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramTipoDotacion.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoDotacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoDotacion localTipoDotacion = (TipoDotacion)paramObject;
    if (localTipoDotacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoDotacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoDotacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoDotacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoDotacionPK))
      throw new IllegalArgumentException("arg1");
    TipoDotacionPK localTipoDotacionPK = (TipoDotacionPK)paramObject;
    localTipoDotacionPK.idTipoDotacion = this.idTipoDotacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoDotacionPK))
      throw new IllegalArgumentException("arg1");
    TipoDotacionPK localTipoDotacionPK = (TipoDotacionPK)paramObject;
    this.idTipoDotacion = localTipoDotacionPK.idTipoDotacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoDotacionPK))
      throw new IllegalArgumentException("arg2");
    TipoDotacionPK localTipoDotacionPK = (TipoDotacionPK)paramObject;
    localTipoDotacionPK.idTipoDotacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoDotacionPK))
      throw new IllegalArgumentException("arg2");
    TipoDotacionPK localTipoDotacionPK = (TipoDotacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localTipoDotacionPK.idTipoDotacion);
  }

  private static final String jdoGetcodTipoDotacion(TipoDotacion paramTipoDotacion)
  {
    if (paramTipoDotacion.jdoFlags <= 0)
      return paramTipoDotacion.codTipoDotacion;
    StateManager localStateManager = paramTipoDotacion.jdoStateManager;
    if (localStateManager == null)
      return paramTipoDotacion.codTipoDotacion;
    if (localStateManager.isLoaded(paramTipoDotacion, jdoInheritedFieldCount + 0))
      return paramTipoDotacion.codTipoDotacion;
    return localStateManager.getStringField(paramTipoDotacion, jdoInheritedFieldCount + 0, paramTipoDotacion.codTipoDotacion);
  }

  private static final void jdoSetcodTipoDotacion(TipoDotacion paramTipoDotacion, String paramString)
  {
    if (paramTipoDotacion.jdoFlags == 0)
    {
      paramTipoDotacion.codTipoDotacion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoDotacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoDotacion.codTipoDotacion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoDotacion, jdoInheritedFieldCount + 0, paramTipoDotacion.codTipoDotacion, paramString);
  }

  private static final long jdoGetidTipoDotacion(TipoDotacion paramTipoDotacion)
  {
    return paramTipoDotacion.idTipoDotacion;
  }

  private static final void jdoSetidTipoDotacion(TipoDotacion paramTipoDotacion, long paramLong)
  {
    StateManager localStateManager = paramTipoDotacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoDotacion.idTipoDotacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoDotacion, jdoInheritedFieldCount + 1, paramTipoDotacion.idTipoDotacion, paramLong);
  }

  private static final String jdoGetnombre(TipoDotacion paramTipoDotacion)
  {
    if (paramTipoDotacion.jdoFlags <= 0)
      return paramTipoDotacion.nombre;
    StateManager localStateManager = paramTipoDotacion.jdoStateManager;
    if (localStateManager == null)
      return paramTipoDotacion.nombre;
    if (localStateManager.isLoaded(paramTipoDotacion, jdoInheritedFieldCount + 2))
      return paramTipoDotacion.nombre;
    return localStateManager.getStringField(paramTipoDotacion, jdoInheritedFieldCount + 2, paramTipoDotacion.nombre);
  }

  private static final void jdoSetnombre(TipoDotacion paramTipoDotacion, String paramString)
  {
    if (paramTipoDotacion.jdoFlags == 0)
    {
      paramTipoDotacion.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramTipoDotacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoDotacion.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoDotacion, jdoInheritedFieldCount + 2, paramTipoDotacion.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}