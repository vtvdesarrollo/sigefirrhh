package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class DenominacionTicketBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDenominacionTicket(DenominacionTicket denominacionTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DenominacionTicket denominacionTicketNew = 
      (DenominacionTicket)BeanUtils.cloneBean(
      denominacionTicket);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (denominacionTicketNew.getTipoPersonal() != null) {
      denominacionTicketNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        denominacionTicketNew.getTipoPersonal().getIdTipoPersonal()));
    }

    ProveedorTicketBeanBusiness proveedorTicketBeanBusiness = new ProveedorTicketBeanBusiness();

    if (denominacionTicketNew.getProveedorTicket() != null) {
      denominacionTicketNew.setProveedorTicket(
        proveedorTicketBeanBusiness.findProveedorTicketById(
        denominacionTicketNew.getProveedorTicket().getIdProveedorTicket()));
    }
    pm.makePersistent(denominacionTicketNew);
  }

  public void updateDenominacionTicket(DenominacionTicket denominacionTicket) throws Exception
  {
    DenominacionTicket denominacionTicketModify = 
      findDenominacionTicketById(denominacionTicket.getIdDenominacionTicket());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (denominacionTicket.getTipoPersonal() != null) {
      denominacionTicket.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        denominacionTicket.getTipoPersonal().getIdTipoPersonal()));
    }

    ProveedorTicketBeanBusiness proveedorTicketBeanBusiness = new ProveedorTicketBeanBusiness();

    if (denominacionTicket.getProveedorTicket() != null) {
      denominacionTicket.setProveedorTicket(
        proveedorTicketBeanBusiness.findProveedorTicketById(
        denominacionTicket.getProveedorTicket().getIdProveedorTicket()));
    }

    BeanUtils.copyProperties(denominacionTicketModify, denominacionTicket);
  }

  public void deleteDenominacionTicket(DenominacionTicket denominacionTicket) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DenominacionTicket denominacionTicketDelete = 
      findDenominacionTicketById(denominacionTicket.getIdDenominacionTicket());
    pm.deletePersistent(denominacionTicketDelete);
  }

  public DenominacionTicket findDenominacionTicketById(long idDenominacionTicket) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDenominacionTicket == pIdDenominacionTicket";
    Query query = pm.newQuery(DenominacionTicket.class, filter);

    query.declareParameters("long pIdDenominacionTicket");

    parameters.put("pIdDenominacionTicket", new Long(idDenominacionTicket));

    Collection colDenominacionTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDenominacionTicket.iterator();
    return (DenominacionTicket)iterator.next();
  }

  public Collection findDenominacionTicketAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent denominacionTicketExtent = pm.getExtent(
      DenominacionTicket.class, true);
    Query query = pm.newQuery(denominacionTicketExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(DenominacionTicket.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colDenominacionTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDenominacionTicket);

    return colDenominacionTicket;
  }
}