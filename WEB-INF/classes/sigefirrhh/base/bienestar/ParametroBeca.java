package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;

public class ParametroBeca
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SUELDO;
  private long idParametroBeca;
  private TipoPersonal tipoPersonal;
  private NivelBeca nivelbeca;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private int aniosServicio;
  private int mesesServicio;
  private int edadMaxima;
  private int promedioNotas;
  private int numeroBecas;
  private int maximoTrabajador;
  private double montoBeca;
  private double montoExcepcional;
  private double sueldoMaximo;
  private String sueldo;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aniosServicio", "conceptoTipoPersonal", "edadMaxima", "idParametroBeca", "idSitp", "maximoTrabajador", "mesesServicio", "montoBeca", "montoExcepcional", "nivelbeca", "numeroBecas", "promedioNotas", "sueldo", "sueldoMaximo", "tiempoSitp", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Integer.TYPE, Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.bienestar.NivelBeca"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 26, 21, 24, 21, 21, 21, 21, 21, 26, 21, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.ParametroBeca"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroBeca());

    LISTA_SUELDO = 
      new LinkedHashMap();

    LISTA_SUELDO.put("I", "INTEGRAL");
    LISTA_SUELDO.put("B", "BASICO");
  }

  public ParametroBeca()
  {
    jdoSetaniosServicio(this, 0);

    jdoSetmesesServicio(this, 0);

    jdoSetedadMaxima(this, 0);

    jdoSetnumeroBecas(this, 0);

    jdoSetmaximoTrabajador(this, 0);

    jdoSetmontoBeca(this, 0.0D);

    jdoSetmontoExcepcional(this, 0.0D);

    jdoSetsueldoMaximo(this, 0.0D);

    jdoSetsueldo(this, "I");
  }

  public String toString()
  {
    return jdoGetnivelbeca(this).toString();
  }

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }

  public int getEdadMaxima()
  {
    return jdoGetedadMaxima(this);
  }

  public long getIdParametroBeca()
  {
    return jdoGetidParametroBeca(this);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public int getMaximoTrabajador()
  {
    return jdoGetmaximoTrabajador(this);
  }

  public double getMontoBeca()
  {
    return jdoGetmontoBeca(this);
  }

  public double getMontoExcepcional()
  {
    return jdoGetmontoExcepcional(this);
  }

  public NivelBeca getNivelbeca()
  {
    return jdoGetnivelbeca(this);
  }

  public int getNumeroBecas()
  {
    return jdoGetnumeroBecas(this);
  }

  public String getSueldo()
  {
    return jdoGetsueldo(this);
  }

  public double getSueldoMaximo()
  {
    return jdoGetsueldoMaximo(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setAniosServicio(int i)
  {
    jdoSetaniosServicio(this, i);
  }

  public void setEdadMaxima(int i)
  {
    jdoSetedadMaxima(this, i);
  }

  public void setIdParametroBeca(long l)
  {
    jdoSetidParametroBeca(this, l);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setMaximoTrabajador(int i)
  {
    jdoSetmaximoTrabajador(this, i);
  }

  public void setMontoBeca(double d)
  {
    jdoSetmontoBeca(this, d);
  }

  public void setMontoExcepcional(double d)
  {
    jdoSetmontoExcepcional(this, d);
  }

  public void setNivelbeca(NivelBeca beca)
  {
    jdoSetnivelbeca(this, beca);
  }

  public void setNumeroBecas(int i)
  {
    jdoSetnumeroBecas(this, i);
  }

  public void setSueldo(String string)
  {
    jdoSetsueldo(this, string);
  }

  public void setSueldoMaximo(double d)
  {
    jdoSetsueldoMaximo(this, d);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public int getMesesServicio()
  {
    return jdoGetmesesServicio(this);
  }
  public int getPromedioNotas() {
    return jdoGetpromedioNotas(this);
  }
  public void setMesesServicio(int mesesServicio) {
    jdoSetmesesServicio(this, mesesServicio);
  }
  public void setPromedioNotas(int promedioNotas) {
    jdoSetpromedioNotas(this, promedioNotas);
  }
  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 16;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroBeca localParametroBeca = new ParametroBeca();
    localParametroBeca.jdoFlags = 1;
    localParametroBeca.jdoStateManager = paramStateManager;
    return localParametroBeca;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroBeca localParametroBeca = new ParametroBeca();
    localParametroBeca.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroBeca.jdoFlags = 1;
    localParametroBeca.jdoStateManager = paramStateManager;
    return localParametroBeca;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadMaxima);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroBeca);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.maximoTrabajador);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesesServicio);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoBeca);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoExcepcional);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nivelbeca);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroBecas);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.promedioNotas);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sueldo);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoMaximo);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadMaxima = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroBeca = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.maximoTrabajador = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesesServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoBeca = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoExcepcional = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelbeca = ((NivelBeca)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroBecas = localStateManager.replacingIntField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioNotas = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoMaximo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroBeca paramParametroBeca, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramParametroBeca.aniosServicio;
      return;
    case 1:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramParametroBeca.conceptoTipoPersonal;
      return;
    case 2:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.edadMaxima = paramParametroBeca.edadMaxima;
      return;
    case 3:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroBeca = paramParametroBeca.idParametroBeca;
      return;
    case 4:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramParametroBeca.idSitp;
      return;
    case 5:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.maximoTrabajador = paramParametroBeca.maximoTrabajador;
      return;
    case 6:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.mesesServicio = paramParametroBeca.mesesServicio;
      return;
    case 7:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.montoBeca = paramParametroBeca.montoBeca;
      return;
    case 8:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.montoExcepcional = paramParametroBeca.montoExcepcional;
      return;
    case 9:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.nivelbeca = paramParametroBeca.nivelbeca;
      return;
    case 10:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.numeroBecas = paramParametroBeca.numeroBecas;
      return;
    case 11:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.promedioNotas = paramParametroBeca.promedioNotas;
      return;
    case 12:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.sueldo = paramParametroBeca.sueldo;
      return;
    case 13:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoMaximo = paramParametroBeca.sueldoMaximo;
      return;
    case 14:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramParametroBeca.tiempoSitp;
      return;
    case 15:
      if (paramParametroBeca == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramParametroBeca.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroBeca))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroBeca localParametroBeca = (ParametroBeca)paramObject;
    if (localParametroBeca.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroBeca, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroBecaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroBecaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroBecaPK))
      throw new IllegalArgumentException("arg1");
    ParametroBecaPK localParametroBecaPK = (ParametroBecaPK)paramObject;
    localParametroBecaPK.idParametroBeca = this.idParametroBeca;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroBecaPK))
      throw new IllegalArgumentException("arg1");
    ParametroBecaPK localParametroBecaPK = (ParametroBecaPK)paramObject;
    this.idParametroBeca = localParametroBecaPK.idParametroBeca;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroBecaPK))
      throw new IllegalArgumentException("arg2");
    ParametroBecaPK localParametroBecaPK = (ParametroBecaPK)paramObject;
    localParametroBecaPK.idParametroBeca = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroBecaPK))
      throw new IllegalArgumentException("arg2");
    ParametroBecaPK localParametroBecaPK = (ParametroBecaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localParametroBecaPK.idParametroBeca);
  }

  private static final int jdoGetaniosServicio(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.aniosServicio;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.aniosServicio;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 0))
      return paramParametroBeca.aniosServicio;
    return localStateManager.getIntField(paramParametroBeca, jdoInheritedFieldCount + 0, paramParametroBeca.aniosServicio);
  }

  private static final void jdoSetaniosServicio(ParametroBeca paramParametroBeca, int paramInt)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroBeca, jdoInheritedFieldCount + 0, paramParametroBeca.aniosServicio, paramInt);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ParametroBeca paramParametroBeca)
  {
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 1))
      return paramParametroBeca.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramParametroBeca, jdoInheritedFieldCount + 1, paramParametroBeca.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ParametroBeca paramParametroBeca, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramParametroBeca, jdoInheritedFieldCount + 1, paramParametroBeca.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final int jdoGetedadMaxima(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.edadMaxima;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.edadMaxima;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 2))
      return paramParametroBeca.edadMaxima;
    return localStateManager.getIntField(paramParametroBeca, jdoInheritedFieldCount + 2, paramParametroBeca.edadMaxima);
  }

  private static final void jdoSetedadMaxima(ParametroBeca paramParametroBeca, int paramInt)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.edadMaxima = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.edadMaxima = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroBeca, jdoInheritedFieldCount + 2, paramParametroBeca.edadMaxima, paramInt);
  }

  private static final long jdoGetidParametroBeca(ParametroBeca paramParametroBeca)
  {
    return paramParametroBeca.idParametroBeca;
  }

  private static final void jdoSetidParametroBeca(ParametroBeca paramParametroBeca, long paramLong)
  {
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.idParametroBeca = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroBeca, jdoInheritedFieldCount + 3, paramParametroBeca.idParametroBeca, paramLong);
  }

  private static final int jdoGetidSitp(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.idSitp;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.idSitp;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 4))
      return paramParametroBeca.idSitp;
    return localStateManager.getIntField(paramParametroBeca, jdoInheritedFieldCount + 4, paramParametroBeca.idSitp);
  }

  private static final void jdoSetidSitp(ParametroBeca paramParametroBeca, int paramInt)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroBeca, jdoInheritedFieldCount + 4, paramParametroBeca.idSitp, paramInt);
  }

  private static final int jdoGetmaximoTrabajador(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.maximoTrabajador;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.maximoTrabajador;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 5))
      return paramParametroBeca.maximoTrabajador;
    return localStateManager.getIntField(paramParametroBeca, jdoInheritedFieldCount + 5, paramParametroBeca.maximoTrabajador);
  }

  private static final void jdoSetmaximoTrabajador(ParametroBeca paramParametroBeca, int paramInt)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.maximoTrabajador = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.maximoTrabajador = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroBeca, jdoInheritedFieldCount + 5, paramParametroBeca.maximoTrabajador, paramInt);
  }

  private static final int jdoGetmesesServicio(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.mesesServicio;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.mesesServicio;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 6))
      return paramParametroBeca.mesesServicio;
    return localStateManager.getIntField(paramParametroBeca, jdoInheritedFieldCount + 6, paramParametroBeca.mesesServicio);
  }

  private static final void jdoSetmesesServicio(ParametroBeca paramParametroBeca, int paramInt)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.mesesServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.mesesServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroBeca, jdoInheritedFieldCount + 6, paramParametroBeca.mesesServicio, paramInt);
  }

  private static final double jdoGetmontoBeca(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.montoBeca;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.montoBeca;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 7))
      return paramParametroBeca.montoBeca;
    return localStateManager.getDoubleField(paramParametroBeca, jdoInheritedFieldCount + 7, paramParametroBeca.montoBeca);
  }

  private static final void jdoSetmontoBeca(ParametroBeca paramParametroBeca, double paramDouble)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.montoBeca = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.montoBeca = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroBeca, jdoInheritedFieldCount + 7, paramParametroBeca.montoBeca, paramDouble);
  }

  private static final double jdoGetmontoExcepcional(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.montoExcepcional;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.montoExcepcional;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 8))
      return paramParametroBeca.montoExcepcional;
    return localStateManager.getDoubleField(paramParametroBeca, jdoInheritedFieldCount + 8, paramParametroBeca.montoExcepcional);
  }

  private static final void jdoSetmontoExcepcional(ParametroBeca paramParametroBeca, double paramDouble)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.montoExcepcional = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.montoExcepcional = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroBeca, jdoInheritedFieldCount + 8, paramParametroBeca.montoExcepcional, paramDouble);
  }

  private static final NivelBeca jdoGetnivelbeca(ParametroBeca paramParametroBeca)
  {
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.nivelbeca;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 9))
      return paramParametroBeca.nivelbeca;
    return (NivelBeca)localStateManager.getObjectField(paramParametroBeca, jdoInheritedFieldCount + 9, paramParametroBeca.nivelbeca);
  }

  private static final void jdoSetnivelbeca(ParametroBeca paramParametroBeca, NivelBeca paramNivelBeca)
  {
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.nivelbeca = paramNivelBeca;
      return;
    }
    localStateManager.setObjectField(paramParametroBeca, jdoInheritedFieldCount + 9, paramParametroBeca.nivelbeca, paramNivelBeca);
  }

  private static final int jdoGetnumeroBecas(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.numeroBecas;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.numeroBecas;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 10))
      return paramParametroBeca.numeroBecas;
    return localStateManager.getIntField(paramParametroBeca, jdoInheritedFieldCount + 10, paramParametroBeca.numeroBecas);
  }

  private static final void jdoSetnumeroBecas(ParametroBeca paramParametroBeca, int paramInt)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.numeroBecas = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.numeroBecas = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroBeca, jdoInheritedFieldCount + 10, paramParametroBeca.numeroBecas, paramInt);
  }

  private static final int jdoGetpromedioNotas(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.promedioNotas;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.promedioNotas;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 11))
      return paramParametroBeca.promedioNotas;
    return localStateManager.getIntField(paramParametroBeca, jdoInheritedFieldCount + 11, paramParametroBeca.promedioNotas);
  }

  private static final void jdoSetpromedioNotas(ParametroBeca paramParametroBeca, int paramInt)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.promedioNotas = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.promedioNotas = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroBeca, jdoInheritedFieldCount + 11, paramParametroBeca.promedioNotas, paramInt);
  }

  private static final String jdoGetsueldo(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.sueldo;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.sueldo;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 12))
      return paramParametroBeca.sueldo;
    return localStateManager.getStringField(paramParametroBeca, jdoInheritedFieldCount + 12, paramParametroBeca.sueldo);
  }

  private static final void jdoSetsueldo(ParametroBeca paramParametroBeca, String paramString)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.sueldo = paramString;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.sueldo = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroBeca, jdoInheritedFieldCount + 12, paramParametroBeca.sueldo, paramString);
  }

  private static final double jdoGetsueldoMaximo(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.sueldoMaximo;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.sueldoMaximo;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 13))
      return paramParametroBeca.sueldoMaximo;
    return localStateManager.getDoubleField(paramParametroBeca, jdoInheritedFieldCount + 13, paramParametroBeca.sueldoMaximo);
  }

  private static final void jdoSetsueldoMaximo(ParametroBeca paramParametroBeca, double paramDouble)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.sueldoMaximo = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.sueldoMaximo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroBeca, jdoInheritedFieldCount + 13, paramParametroBeca.sueldoMaximo, paramDouble);
  }

  private static final Date jdoGettiempoSitp(ParametroBeca paramParametroBeca)
  {
    if (paramParametroBeca.jdoFlags <= 0)
      return paramParametroBeca.tiempoSitp;
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.tiempoSitp;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 14))
      return paramParametroBeca.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramParametroBeca, jdoInheritedFieldCount + 14, paramParametroBeca.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ParametroBeca paramParametroBeca, Date paramDate)
  {
    if (paramParametroBeca.jdoFlags == 0)
    {
      paramParametroBeca.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramParametroBeca, jdoInheritedFieldCount + 14, paramParametroBeca.tiempoSitp, paramDate);
  }

  private static final TipoPersonal jdoGettipoPersonal(ParametroBeca paramParametroBeca)
  {
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
      return paramParametroBeca.tipoPersonal;
    if (localStateManager.isLoaded(paramParametroBeca, jdoInheritedFieldCount + 15))
      return paramParametroBeca.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramParametroBeca, jdoInheritedFieldCount + 15, paramParametroBeca.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ParametroBeca paramParametroBeca, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramParametroBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroBeca.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramParametroBeca, jdoInheritedFieldCount + 15, paramParametroBeca.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}