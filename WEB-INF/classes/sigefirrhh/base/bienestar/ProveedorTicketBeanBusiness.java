package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ProveedorTicketBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addProveedorTicket(ProveedorTicket proveedorTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ProveedorTicket proveedorTicketNew = 
      (ProveedorTicket)BeanUtils.cloneBean(
      proveedorTicket);

    pm.makePersistent(proveedorTicketNew);
  }

  public void updateProveedorTicket(ProveedorTicket proveedorTicket) throws Exception
  {
    ProveedorTicket proveedorTicketModify = 
      findProveedorTicketById(proveedorTicket.getIdProveedorTicket());

    BeanUtils.copyProperties(proveedorTicketModify, proveedorTicket);
  }

  public void deleteProveedorTicket(ProveedorTicket proveedorTicket) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ProveedorTicket proveedorTicketDelete = 
      findProveedorTicketById(proveedorTicket.getIdProveedorTicket());
    pm.deletePersistent(proveedorTicketDelete);
  }

  public ProveedorTicket findProveedorTicketById(long idProveedorTicket) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idProveedorTicket == pIdProveedorTicket";
    Query query = pm.newQuery(ProveedorTicket.class, filter);

    query.declareParameters("long pIdProveedorTicket");

    parameters.put("pIdProveedorTicket", new Long(idProveedorTicket));

    Collection colProveedorTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colProveedorTicket.iterator();
    return (ProveedorTicket)iterator.next();
  }

  public Collection findProveedorTicketAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent proveedorTicketExtent = pm.getExtent(
      ProveedorTicket.class, true);
    Query query = pm.newQuery(proveedorTicketExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodProveedorTicket(String codProveedorTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codProveedorTicket == pCodProveedorTicket";

    Query query = pm.newQuery(ProveedorTicket.class, filter);

    query.declareParameters("java.lang.String pCodProveedorTicket");
    HashMap parameters = new HashMap();

    parameters.put("pCodProveedorTicket", new String(codProveedorTicket));

    Collection colProveedorTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProveedorTicket);

    return colProveedorTicket;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(ProveedorTicket.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    Collection colProveedorTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProveedorTicket);

    return colProveedorTicket;
  }
}