package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SubtipoDotacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSubtipoDotacion(SubtipoDotacion subtipoDotacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SubtipoDotacion subtipoDotacionNew = 
      (SubtipoDotacion)BeanUtils.cloneBean(
      subtipoDotacion);

    TipoDotacionBeanBusiness tipoDotacionBeanBusiness = new TipoDotacionBeanBusiness();

    if (subtipoDotacionNew.getTipoDotacion() != null) {
      subtipoDotacionNew.setTipoDotacion(
        tipoDotacionBeanBusiness.findTipoDotacionById(
        subtipoDotacionNew.getTipoDotacion().getIdTipoDotacion()));
    }
    pm.makePersistent(subtipoDotacionNew);
  }

  public void updateSubtipoDotacion(SubtipoDotacion subtipoDotacion) throws Exception
  {
    SubtipoDotacion subtipoDotacionModify = 
      findSubtipoDotacionById(subtipoDotacion.getIdSubtipoDotacion());

    TipoDotacionBeanBusiness tipoDotacionBeanBusiness = new TipoDotacionBeanBusiness();

    if (subtipoDotacion.getTipoDotacion() != null) {
      subtipoDotacion.setTipoDotacion(
        tipoDotacionBeanBusiness.findTipoDotacionById(
        subtipoDotacion.getTipoDotacion().getIdTipoDotacion()));
    }

    BeanUtils.copyProperties(subtipoDotacionModify, subtipoDotacion);
  }

  public void deleteSubtipoDotacion(SubtipoDotacion subtipoDotacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SubtipoDotacion subtipoDotacionDelete = 
      findSubtipoDotacionById(subtipoDotacion.getIdSubtipoDotacion());
    pm.deletePersistent(subtipoDotacionDelete);
  }

  public SubtipoDotacion findSubtipoDotacionById(long idSubtipoDotacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSubtipoDotacion == pIdSubtipoDotacion";
    Query query = pm.newQuery(SubtipoDotacion.class, filter);

    query.declareParameters("long pIdSubtipoDotacion");

    parameters.put("pIdSubtipoDotacion", new Long(idSubtipoDotacion));

    Collection colSubtipoDotacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSubtipoDotacion.iterator();
    return (SubtipoDotacion)iterator.next();
  }

  public Collection findSubtipoDotacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent subtipoDotacionExtent = pm.getExtent(
      SubtipoDotacion.class, true);
    Query query = pm.newQuery(subtipoDotacionExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoDotacion(long idTipoDotacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoDotacion.idTipoDotacion == pIdTipoDotacion";

    Query query = pm.newQuery(SubtipoDotacion.class, filter);

    query.declareParameters("long pIdTipoDotacion");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoDotacion", new Long(idTipoDotacion));

    query.setOrdering("nombre ascending");

    Collection colSubtipoDotacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSubtipoDotacion);

    return colSubtipoDotacion;
  }
}