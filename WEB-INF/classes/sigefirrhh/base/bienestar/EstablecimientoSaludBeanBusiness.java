package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;

public class EstablecimientoSaludBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEstablecimientoSalud(EstablecimientoSalud establecimientoSalud)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    EstablecimientoSalud establecimientoSaludNew = 
      (EstablecimientoSalud)BeanUtils.cloneBean(
      establecimientoSalud);

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (establecimientoSaludNew.getCiudad() != null) {
      establecimientoSaludNew.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        establecimientoSaludNew.getCiudad().getIdCiudad()));
    }
    pm.makePersistent(establecimientoSaludNew);
  }

  public void updateEstablecimientoSalud(EstablecimientoSalud establecimientoSalud) throws Exception
  {
    EstablecimientoSalud establecimientoSaludModify = 
      findEstablecimientoSaludById(establecimientoSalud.getIdEstablecimientoSalud());

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (establecimientoSalud.getCiudad() != null) {
      establecimientoSalud.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        establecimientoSalud.getCiudad().getIdCiudad()));
    }

    BeanUtils.copyProperties(establecimientoSaludModify, establecimientoSalud);
  }

  public void deleteEstablecimientoSalud(EstablecimientoSalud establecimientoSalud) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    EstablecimientoSalud establecimientoSaludDelete = 
      findEstablecimientoSaludById(establecimientoSalud.getIdEstablecimientoSalud());
    pm.deletePersistent(establecimientoSaludDelete);
  }

  public EstablecimientoSalud findEstablecimientoSaludById(long idEstablecimientoSalud) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEstablecimientoSalud == pIdEstablecimientoSalud";
    Query query = pm.newQuery(EstablecimientoSalud.class, filter);

    query.declareParameters("long pIdEstablecimientoSalud");

    parameters.put("pIdEstablecimientoSalud", new Long(idEstablecimientoSalud));

    Collection colEstablecimientoSalud = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEstablecimientoSalud.iterator();
    return (EstablecimientoSalud)iterator.next();
  }

  public Collection findEstablecimientoSaludAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent establecimientoSaludExtent = pm.getExtent(
      EstablecimientoSalud.class, true);
    Query query = pm.newQuery(establecimientoSaludExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodEstablecimientoSalud(String codEstablecimientoSalud)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codEstablecimientoSalud == pCodEstablecimientoSalud";

    Query query = pm.newQuery(EstablecimientoSalud.class, filter);

    query.declareParameters("java.lang.String pCodEstablecimientoSalud");
    HashMap parameters = new HashMap();

    parameters.put("pCodEstablecimientoSalud", new String(codEstablecimientoSalud));

    query.setOrdering("nombre ascending");

    Collection colEstablecimientoSalud = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEstablecimientoSalud);

    return colEstablecimientoSalud;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(EstablecimientoSalud.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colEstablecimientoSalud = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEstablecimientoSalud);

    return colEstablecimientoSalud;
  }
}