package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoCredencial
  implements Serializable, PersistenceCapable
{
  private long idTipoCredencial;
  private String codTipoCredencial;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoCredencial", "idTipoCredencial", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodTipoCredencial(this);
  }

  public String getCodTipoCredencial()
  {
    return jdoGetcodTipoCredencial(this);
  }
  public void setCodTipoCredencial(String codTipoCredencial) {
    jdoSetcodTipoCredencial(this, codTipoCredencial);
  }
  public long getIdTipoCredencial() {
    return jdoGetidTipoCredencial(this);
  }
  public void setIdTipoCredencial(long idTipoCredencial) {
    jdoSetidTipoCredencial(this, idTipoCredencial);
  }
  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.TipoCredencial"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoCredencial());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoCredencial localTipoCredencial = new TipoCredencial();
    localTipoCredencial.jdoFlags = 1;
    localTipoCredencial.jdoStateManager = paramStateManager;
    return localTipoCredencial;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoCredencial localTipoCredencial = new TipoCredencial();
    localTipoCredencial.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoCredencial.jdoFlags = 1;
    localTipoCredencial.jdoStateManager = paramStateManager;
    return localTipoCredencial;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoCredencial);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoCredencial);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoCredencial = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoCredencial = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoCredencial paramTipoCredencial, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoCredencial = paramTipoCredencial.codTipoCredencial;
      return;
    case 1:
      if (paramTipoCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoCredencial = paramTipoCredencial.idTipoCredencial;
      return;
    case 2:
      if (paramTipoCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramTipoCredencial.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoCredencial))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoCredencial localTipoCredencial = (TipoCredencial)paramObject;
    if (localTipoCredencial.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoCredencial, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoCredencialPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoCredencialPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoCredencialPK))
      throw new IllegalArgumentException("arg1");
    TipoCredencialPK localTipoCredencialPK = (TipoCredencialPK)paramObject;
    localTipoCredencialPK.idTipoCredencial = this.idTipoCredencial;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoCredencialPK))
      throw new IllegalArgumentException("arg1");
    TipoCredencialPK localTipoCredencialPK = (TipoCredencialPK)paramObject;
    this.idTipoCredencial = localTipoCredencialPK.idTipoCredencial;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoCredencialPK))
      throw new IllegalArgumentException("arg2");
    TipoCredencialPK localTipoCredencialPK = (TipoCredencialPK)paramObject;
    localTipoCredencialPK.idTipoCredencial = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoCredencialPK))
      throw new IllegalArgumentException("arg2");
    TipoCredencialPK localTipoCredencialPK = (TipoCredencialPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localTipoCredencialPK.idTipoCredencial);
  }

  private static final String jdoGetcodTipoCredencial(TipoCredencial paramTipoCredencial)
  {
    if (paramTipoCredencial.jdoFlags <= 0)
      return paramTipoCredencial.codTipoCredencial;
    StateManager localStateManager = paramTipoCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramTipoCredencial.codTipoCredencial;
    if (localStateManager.isLoaded(paramTipoCredencial, jdoInheritedFieldCount + 0))
      return paramTipoCredencial.codTipoCredencial;
    return localStateManager.getStringField(paramTipoCredencial, jdoInheritedFieldCount + 0, paramTipoCredencial.codTipoCredencial);
  }

  private static final void jdoSetcodTipoCredencial(TipoCredencial paramTipoCredencial, String paramString)
  {
    if (paramTipoCredencial.jdoFlags == 0)
    {
      paramTipoCredencial.codTipoCredencial = paramString;
      return;
    }
    StateManager localStateManager = paramTipoCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoCredencial.codTipoCredencial = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoCredencial, jdoInheritedFieldCount + 0, paramTipoCredencial.codTipoCredencial, paramString);
  }

  private static final long jdoGetidTipoCredencial(TipoCredencial paramTipoCredencial)
  {
    return paramTipoCredencial.idTipoCredencial;
  }

  private static final void jdoSetidTipoCredencial(TipoCredencial paramTipoCredencial, long paramLong)
  {
    StateManager localStateManager = paramTipoCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoCredencial.idTipoCredencial = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoCredencial, jdoInheritedFieldCount + 1, paramTipoCredencial.idTipoCredencial, paramLong);
  }

  private static final String jdoGetnombre(TipoCredencial paramTipoCredencial)
  {
    if (paramTipoCredencial.jdoFlags <= 0)
      return paramTipoCredencial.nombre;
    StateManager localStateManager = paramTipoCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramTipoCredencial.nombre;
    if (localStateManager.isLoaded(paramTipoCredencial, jdoInheritedFieldCount + 2))
      return paramTipoCredencial.nombre;
    return localStateManager.getStringField(paramTipoCredencial, jdoInheritedFieldCount + 2, paramTipoCredencial.nombre);
  }

  private static final void jdoSetnombre(TipoCredencial paramTipoCredencial, String paramString)
  {
    if (paramTipoCredencial.jdoFlags == 0)
    {
      paramTipoCredencial.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramTipoCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoCredencial.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoCredencial, jdoInheritedFieldCount + 2, paramTipoCredencial.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}