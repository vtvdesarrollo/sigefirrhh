package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ParametroBecaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParametroBeca(ParametroBeca parametroBeca)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ParametroBeca parametroBecaNew = 
      (ParametroBeca)BeanUtils.cloneBean(
      parametroBeca);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroBecaNew.getTipoPersonal() != null) {
      parametroBecaNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroBecaNew.getTipoPersonal().getIdTipoPersonal()));
    }

    NivelBecaBeanBusiness nivelbecaBeanBusiness = new NivelBecaBeanBusiness();

    if (parametroBecaNew.getNivelbeca() != null) {
      parametroBecaNew.setNivelbeca(
        nivelbecaBeanBusiness.findNivelBecaById(
        parametroBecaNew.getNivelbeca().getIdNivelBeca()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (parametroBecaNew.getConceptoTipoPersonal() != null) {
      parametroBecaNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        parametroBecaNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }
    pm.makePersistent(parametroBecaNew);
  }

  public void updateParametroBeca(ParametroBeca parametroBeca) throws Exception
  {
    ParametroBeca parametroBecaModify = 
      findParametroBecaById(parametroBeca.getIdParametroBeca());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroBeca.getTipoPersonal() != null) {
      parametroBeca.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroBeca.getTipoPersonal().getIdTipoPersonal()));
    }

    NivelBecaBeanBusiness nivelbecaBeanBusiness = new NivelBecaBeanBusiness();

    if (parametroBeca.getNivelbeca() != null) {
      parametroBeca.setNivelbeca(
        nivelbecaBeanBusiness.findNivelBecaById(
        parametroBeca.getNivelbeca().getIdNivelBeca()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (parametroBeca.getConceptoTipoPersonal() != null) {
      parametroBeca.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        parametroBeca.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    BeanUtils.copyProperties(parametroBecaModify, parametroBeca);
  }

  public void deleteParametroBeca(ParametroBeca parametroBeca) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ParametroBeca parametroBecaDelete = 
      findParametroBecaById(parametroBeca.getIdParametroBeca());
    pm.deletePersistent(parametroBecaDelete);
  }

  public ParametroBeca findParametroBecaById(long idParametroBeca) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParametroBeca == pIdParametroBeca";
    Query query = pm.newQuery(ParametroBeca.class, filter);

    query.declareParameters("long pIdParametroBeca");

    parameters.put("pIdParametroBeca", new Long(idParametroBeca));

    Collection colParametroBeca = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParametroBeca.iterator();
    return (ParametroBeca)iterator.next();
  }

  public Collection findParametroBecaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parametroBecaExtent = pm.getExtent(
      ParametroBeca.class, true);
    Query query = pm.newQuery(parametroBecaExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ParametroBeca.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colParametroBeca = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParametroBeca);

    return colParametroBeca;
  }
}