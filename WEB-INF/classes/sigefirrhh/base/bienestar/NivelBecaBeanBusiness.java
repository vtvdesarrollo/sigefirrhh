package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class NivelBecaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addNivelBeca(NivelBeca nivelBeca)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    NivelBeca nivelBecaNew = 
      (NivelBeca)BeanUtils.cloneBean(
      nivelBeca);

    pm.makePersistent(nivelBecaNew);
  }

  public void updateNivelBeca(NivelBeca nivelBeca) throws Exception
  {
    NivelBeca nivelBecaModify = 
      findNivelBecaById(nivelBeca.getIdNivelBeca());

    BeanUtils.copyProperties(nivelBecaModify, nivelBeca);
  }

  public void deleteNivelBeca(NivelBeca nivelBeca) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    NivelBeca nivelBecaDelete = 
      findNivelBecaById(nivelBeca.getIdNivelBeca());
    pm.deletePersistent(nivelBecaDelete);
  }

  public NivelBeca findNivelBecaById(long idNivelBeca) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idNivelBeca == pIdNivelBeca";
    Query query = pm.newQuery(NivelBeca.class, filter);

    query.declareParameters("long pIdNivelBeca");

    parameters.put("pIdNivelBeca", new Long(idNivelBeca));

    Collection colNivelBeca = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colNivelBeca.iterator();
    return (NivelBeca)iterator.next();
  }

  public Collection findNivelBecaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent nivelBecaExtent = pm.getExtent(
      NivelBeca.class, true);
    Query query = pm.newQuery(nivelBecaExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByNivelEducativo(String nivelEducativo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nivelEducativo == pNivelEducativo";

    Query query = pm.newQuery(NivelBeca.class, filter);

    query.declareParameters("java.lang.String pNivelEducativo");
    HashMap parameters = new HashMap();

    parameters.put("pNivelEducativo", new String(nivelEducativo));

    Collection colNivelBeca = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNivelBeca);

    return colNivelBeca;
  }
}