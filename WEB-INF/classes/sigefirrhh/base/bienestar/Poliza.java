package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class Poliza
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  protected static final Map LISTA_SINO;
  private long idPoliza;
  private String codPoliza;
  private String nombre;
  private String tipo;
  private String numeroPoliza;
  private String aseguradora;
  private Date fechaInicio;
  private Date fechaFin;
  private String permiteExceso;
  private String vigente;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aseguradora", "codPoliza", "fechaFin", "fechaInicio", "idPoliza", "nombre", "numeroPoliza", "organismo", "permiteExceso", "tipo", "vigente" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 21, 26, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.Poliza"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Poliza());

    LISTA_TIPO = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_TIPO.put("H", "HCM");
    LISTA_TIPO.put("V", "VIDA");
    LISTA_TIPO.put("A", "ACCIDENTES PERSONALES");
    LISTA_TIPO.put("C", "VEHICULOS");
    LISTA_TIPO.put("F", "SERVICIOS FUNERARIOS");
    LISTA_TIPO.put("O", "OTRO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public Poliza()
  {
    jdoSetpermiteExceso(this, "N");

    jdoSetvigente(this, "S");
  }

  public String toString()
  {
    return jdoGetcodPoliza(this) + "-" + 
      jdoGetnombre(this);
  }

  public String getAseguradora() {
    return jdoGetaseguradora(this);
  }

  public void setAseguradora(String aseguradora) {
    jdoSetaseguradora(this, aseguradora);
  }

  public String getCodPoliza() {
    return jdoGetcodPoliza(this);
  }

  public void setCodPoliza(String codPoliza) {
    jdoSetcodPoliza(this, codPoliza);
  }

  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }

  public void setFechaFin(Date fechaFin) {
    jdoSetfechaFin(this, fechaFin);
  }

  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }

  public void setFechaInicio(Date fechaInicio) {
    jdoSetfechaInicio(this, fechaInicio);
  }

  public long getIdPoliza() {
    return jdoGetidPoliza(this);
  }

  public void setIdPoliza(long idPoliza) {
    jdoSetidPoliza(this, idPoliza);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public String getNumeroPoliza() {
    return jdoGetnumeroPoliza(this);
  }

  public void setNumeroPoliza(String numeroPoliza) {
    jdoSetnumeroPoliza(this, numeroPoliza);
  }

  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }

  public String getPermiteExceso() {
    return jdoGetpermiteExceso(this);
  }

  public void setPermiteExceso(String permiteExceso) {
    jdoSetpermiteExceso(this, permiteExceso);
  }

  public String getTipo() {
    return jdoGettipo(this);
  }

  public void setTipo(String tipo) {
    jdoSettipo(this, tipo);
  }

  public String getVigente() {
    return jdoGetvigente(this);
  }

  public void setVigente(String vigente) {
    jdoSetvigente(this, vigente);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Poliza localPoliza = new Poliza();
    localPoliza.jdoFlags = 1;
    localPoliza.jdoStateManager = paramStateManager;
    return localPoliza;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Poliza localPoliza = new Poliza();
    localPoliza.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPoliza.jdoFlags = 1;
    localPoliza.jdoStateManager = paramStateManager;
    return localPoliza;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aseguradora);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codPoliza);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPoliza);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroPoliza);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.permiteExceso);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigente);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aseguradora = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codPoliza = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPoliza = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroPoliza = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.permiteExceso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigente = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Poliza paramPoliza, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.aseguradora = paramPoliza.aseguradora;
      return;
    case 1:
      if (paramPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.codPoliza = paramPoliza.codPoliza;
      return;
    case 2:
      if (paramPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramPoliza.fechaFin;
      return;
    case 3:
      if (paramPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramPoliza.fechaInicio;
      return;
    case 4:
      if (paramPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.idPoliza = paramPoliza.idPoliza;
      return;
    case 5:
      if (paramPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramPoliza.nombre;
      return;
    case 6:
      if (paramPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.numeroPoliza = paramPoliza.numeroPoliza;
      return;
    case 7:
      if (paramPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramPoliza.organismo;
      return;
    case 8:
      if (paramPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.permiteExceso = paramPoliza.permiteExceso;
      return;
    case 9:
      if (paramPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramPoliza.tipo;
      return;
    case 10:
      if (paramPoliza == null)
        throw new IllegalArgumentException("arg1");
      this.vigente = paramPoliza.vigente;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Poliza))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Poliza localPoliza = (Poliza)paramObject;
    if (localPoliza.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPoliza, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PolizaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PolizaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PolizaPK))
      throw new IllegalArgumentException("arg1");
    PolizaPK localPolizaPK = (PolizaPK)paramObject;
    localPolizaPK.idPoliza = this.idPoliza;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PolizaPK))
      throw new IllegalArgumentException("arg1");
    PolizaPK localPolizaPK = (PolizaPK)paramObject;
    this.idPoliza = localPolizaPK.idPoliza;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PolizaPK))
      throw new IllegalArgumentException("arg2");
    PolizaPK localPolizaPK = (PolizaPK)paramObject;
    localPolizaPK.idPoliza = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PolizaPK))
      throw new IllegalArgumentException("arg2");
    PolizaPK localPolizaPK = (PolizaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localPolizaPK.idPoliza);
  }

  private static final String jdoGetaseguradora(Poliza paramPoliza)
  {
    if (paramPoliza.jdoFlags <= 0)
      return paramPoliza.aseguradora;
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPoliza.aseguradora;
    if (localStateManager.isLoaded(paramPoliza, jdoInheritedFieldCount + 0))
      return paramPoliza.aseguradora;
    return localStateManager.getStringField(paramPoliza, jdoInheritedFieldCount + 0, paramPoliza.aseguradora);
  }

  private static final void jdoSetaseguradora(Poliza paramPoliza, String paramString)
  {
    if (paramPoliza.jdoFlags == 0)
    {
      paramPoliza.aseguradora = paramString;
      return;
    }
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPoliza.aseguradora = paramString;
      return;
    }
    localStateManager.setStringField(paramPoliza, jdoInheritedFieldCount + 0, paramPoliza.aseguradora, paramString);
  }

  private static final String jdoGetcodPoliza(Poliza paramPoliza)
  {
    if (paramPoliza.jdoFlags <= 0)
      return paramPoliza.codPoliza;
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPoliza.codPoliza;
    if (localStateManager.isLoaded(paramPoliza, jdoInheritedFieldCount + 1))
      return paramPoliza.codPoliza;
    return localStateManager.getStringField(paramPoliza, jdoInheritedFieldCount + 1, paramPoliza.codPoliza);
  }

  private static final void jdoSetcodPoliza(Poliza paramPoliza, String paramString)
  {
    if (paramPoliza.jdoFlags == 0)
    {
      paramPoliza.codPoliza = paramString;
      return;
    }
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPoliza.codPoliza = paramString;
      return;
    }
    localStateManager.setStringField(paramPoliza, jdoInheritedFieldCount + 1, paramPoliza.codPoliza, paramString);
  }

  private static final Date jdoGetfechaFin(Poliza paramPoliza)
  {
    if (paramPoliza.jdoFlags <= 0)
      return paramPoliza.fechaFin;
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPoliza.fechaFin;
    if (localStateManager.isLoaded(paramPoliza, jdoInheritedFieldCount + 2))
      return paramPoliza.fechaFin;
    return (Date)localStateManager.getObjectField(paramPoliza, jdoInheritedFieldCount + 2, paramPoliza.fechaFin);
  }

  private static final void jdoSetfechaFin(Poliza paramPoliza, Date paramDate)
  {
    if (paramPoliza.jdoFlags == 0)
    {
      paramPoliza.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPoliza.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPoliza, jdoInheritedFieldCount + 2, paramPoliza.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(Poliza paramPoliza)
  {
    if (paramPoliza.jdoFlags <= 0)
      return paramPoliza.fechaInicio;
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPoliza.fechaInicio;
    if (localStateManager.isLoaded(paramPoliza, jdoInheritedFieldCount + 3))
      return paramPoliza.fechaInicio;
    return (Date)localStateManager.getObjectField(paramPoliza, jdoInheritedFieldCount + 3, paramPoliza.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Poliza paramPoliza, Date paramDate)
  {
    if (paramPoliza.jdoFlags == 0)
    {
      paramPoliza.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPoliza.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPoliza, jdoInheritedFieldCount + 3, paramPoliza.fechaInicio, paramDate);
  }

  private static final long jdoGetidPoliza(Poliza paramPoliza)
  {
    return paramPoliza.idPoliza;
  }

  private static final void jdoSetidPoliza(Poliza paramPoliza, long paramLong)
  {
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPoliza.idPoliza = paramLong;
      return;
    }
    localStateManager.setLongField(paramPoliza, jdoInheritedFieldCount + 4, paramPoliza.idPoliza, paramLong);
  }

  private static final String jdoGetnombre(Poliza paramPoliza)
  {
    if (paramPoliza.jdoFlags <= 0)
      return paramPoliza.nombre;
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPoliza.nombre;
    if (localStateManager.isLoaded(paramPoliza, jdoInheritedFieldCount + 5))
      return paramPoliza.nombre;
    return localStateManager.getStringField(paramPoliza, jdoInheritedFieldCount + 5, paramPoliza.nombre);
  }

  private static final void jdoSetnombre(Poliza paramPoliza, String paramString)
  {
    if (paramPoliza.jdoFlags == 0)
    {
      paramPoliza.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPoliza.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramPoliza, jdoInheritedFieldCount + 5, paramPoliza.nombre, paramString);
  }

  private static final String jdoGetnumeroPoliza(Poliza paramPoliza)
  {
    if (paramPoliza.jdoFlags <= 0)
      return paramPoliza.numeroPoliza;
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPoliza.numeroPoliza;
    if (localStateManager.isLoaded(paramPoliza, jdoInheritedFieldCount + 6))
      return paramPoliza.numeroPoliza;
    return localStateManager.getStringField(paramPoliza, jdoInheritedFieldCount + 6, paramPoliza.numeroPoliza);
  }

  private static final void jdoSetnumeroPoliza(Poliza paramPoliza, String paramString)
  {
    if (paramPoliza.jdoFlags == 0)
    {
      paramPoliza.numeroPoliza = paramString;
      return;
    }
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPoliza.numeroPoliza = paramString;
      return;
    }
    localStateManager.setStringField(paramPoliza, jdoInheritedFieldCount + 6, paramPoliza.numeroPoliza, paramString);
  }

  private static final Organismo jdoGetorganismo(Poliza paramPoliza)
  {
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPoliza.organismo;
    if (localStateManager.isLoaded(paramPoliza, jdoInheritedFieldCount + 7))
      return paramPoliza.organismo;
    return (Organismo)localStateManager.getObjectField(paramPoliza, jdoInheritedFieldCount + 7, paramPoliza.organismo);
  }

  private static final void jdoSetorganismo(Poliza paramPoliza, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPoliza.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramPoliza, jdoInheritedFieldCount + 7, paramPoliza.organismo, paramOrganismo);
  }

  private static final String jdoGetpermiteExceso(Poliza paramPoliza)
  {
    if (paramPoliza.jdoFlags <= 0)
      return paramPoliza.permiteExceso;
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPoliza.permiteExceso;
    if (localStateManager.isLoaded(paramPoliza, jdoInheritedFieldCount + 8))
      return paramPoliza.permiteExceso;
    return localStateManager.getStringField(paramPoliza, jdoInheritedFieldCount + 8, paramPoliza.permiteExceso);
  }

  private static final void jdoSetpermiteExceso(Poliza paramPoliza, String paramString)
  {
    if (paramPoliza.jdoFlags == 0)
    {
      paramPoliza.permiteExceso = paramString;
      return;
    }
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPoliza.permiteExceso = paramString;
      return;
    }
    localStateManager.setStringField(paramPoliza, jdoInheritedFieldCount + 8, paramPoliza.permiteExceso, paramString);
  }

  private static final String jdoGettipo(Poliza paramPoliza)
  {
    if (paramPoliza.jdoFlags <= 0)
      return paramPoliza.tipo;
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPoliza.tipo;
    if (localStateManager.isLoaded(paramPoliza, jdoInheritedFieldCount + 9))
      return paramPoliza.tipo;
    return localStateManager.getStringField(paramPoliza, jdoInheritedFieldCount + 9, paramPoliza.tipo);
  }

  private static final void jdoSettipo(Poliza paramPoliza, String paramString)
  {
    if (paramPoliza.jdoFlags == 0)
    {
      paramPoliza.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPoliza.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramPoliza, jdoInheritedFieldCount + 9, paramPoliza.tipo, paramString);
  }

  private static final String jdoGetvigente(Poliza paramPoliza)
  {
    if (paramPoliza.jdoFlags <= 0)
      return paramPoliza.vigente;
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
      return paramPoliza.vigente;
    if (localStateManager.isLoaded(paramPoliza, jdoInheritedFieldCount + 10))
      return paramPoliza.vigente;
    return localStateManager.getStringField(paramPoliza, jdoInheritedFieldCount + 10, paramPoliza.vigente);
  }

  private static final void jdoSetvigente(Poliza paramPoliza, String paramString)
  {
    if (paramPoliza.jdoFlags == 0)
    {
      paramPoliza.vigente = paramString;
      return;
    }
    StateManager localStateManager = paramPoliza.jdoStateManager;
    if (localStateManager == null)
    {
      paramPoliza.vigente = paramString;
      return;
    }
    localStateManager.setStringField(paramPoliza, jdoInheritedFieldCount + 10, paramPoliza.vigente, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}