package sigefirrhh.base.bienestar;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class PolizaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PolizaForm.class.getName());
  private Poliza poliza;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private boolean showPolizaByCodPoliza;
  private boolean showPolizaByNombre;
  private boolean showPolizaByVigente;
  private String findCodPoliza;
  private String findNombre;
  private String findVigente;
  private Object stateResultPolizaByCodPoliza = null;

  private Object stateResultPolizaByNombre = null;

  private Object stateResultPolizaByVigente = null;

  public String getFindCodPoliza()
  {
    return this.findCodPoliza;
  }
  public void setFindCodPoliza(String findCodPoliza) {
    this.findCodPoliza = findCodPoliza;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }
  public String getFindVigente() {
    return this.findVigente;
  }
  public void setFindVigente(String findVigente) {
    this.findVigente = findVigente;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Poliza getPoliza() {
    if (this.poliza == null) {
      this.poliza = new Poliza();
    }
    return this.poliza;
  }

  public PolizaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListTipo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Poliza.LISTA_TIPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListPermiteExceso()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Poliza.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListVigente() {
    Collection col = new ArrayList();

    Iterator iterEntry = Poliza.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findPolizaByCodPoliza()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.bienestarFacade.findPolizaByCodPoliza(this.findCodPoliza, idOrganismo);
      this.showPolizaByCodPoliza = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPolizaByCodPoliza)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodPoliza = null;
    this.findNombre = null;
    this.findVigente = null;

    return null;
  }

  public String findPolizaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.bienestarFacade.findPolizaByNombre(this.findNombre, idOrganismo);
      this.showPolizaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPolizaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodPoliza = null;
    this.findNombre = null;
    this.findVigente = null;

    return null;
  }

  public String findPolizaByVigente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.bienestarFacade.findPolizaByVigente(this.findVigente, idOrganismo);
      this.showPolizaByVigente = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPolizaByVigente)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodPoliza = null;
    this.findNombre = null;
    this.findVigente = null;

    return null;
  }

  public boolean isShowPolizaByCodPoliza() {
    return this.showPolizaByCodPoliza;
  }
  public boolean isShowPolizaByNombre() {
    return this.showPolizaByNombre;
  }
  public boolean isShowPolizaByVigente() {
    return this.showPolizaByVigente;
  }

  public String selectPoliza()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPoliza = 
      Long.parseLong((String)requestParameterMap.get("idPoliza"));
    try
    {
      this.poliza = 
        this.bienestarFacade.findPolizaById(
        idPoliza);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.poliza = null;
    this.showPolizaByCodPoliza = false;
    this.showPolizaByNombre = false;
    this.showPolizaByVigente = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    try {
      if (this.poliza.getFechaInicio().compareTo(this.poliza.getFechaFin()) >= 0) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Fecha Fin tiene un valor no valido", ""));
        error = true;
      }
    }
    catch (Exception localException1)
    {
    }
    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.bienestarFacade.addPoliza(
          this.poliza);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.bienestarFacade.updatePoliza(
          this.poliza);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.bienestarFacade.deletePoliza(
        this.poliza);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.poliza = new Poliza();

    this.poliza.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.poliza.setIdPoliza(identityGenerator.getNextSequenceNumber("sigefirrhh.base.bienestar.Poliza"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.poliza = new Poliza();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}