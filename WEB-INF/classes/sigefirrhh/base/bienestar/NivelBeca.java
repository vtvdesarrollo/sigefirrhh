package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class NivelBeca
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_NIVEL_EDUCATIVO;
  private long idNivelBeca;
  private String nivelEducativo;
  private int inicioNivel;
  private int finNivel;
  private String proximoNivel;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "finNivel", "idNivelBeca", "inicioNivel", "nivelEducativo", "proximoNivel" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.NivelBeca"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new NivelBeca());

    LISTA_NIVEL_EDUCATIVO = 
      new LinkedHashMap();

    LISTA_NIVEL_EDUCATIVO.put("P", "PREESCOLAR");
    LISTA_NIVEL_EDUCATIVO.put("B", "BASICA");
    LISTA_NIVEL_EDUCATIVO.put("I", "PRIMARIA");
    LISTA_NIVEL_EDUCATIVO.put("D", "DIVERSIFICADO");
    LISTA_NIVEL_EDUCATIVO.put("H", "BACHILLERATO");
    LISTA_NIVEL_EDUCATIVO.put("T", "TECNICO MEDIO");
    LISTA_NIVEL_EDUCATIVO.put("S", "TECNICO SUPERIOR");
    LISTA_NIVEL_EDUCATIVO.put("U", "UNIVERSITARIO");
    LISTA_NIVEL_EDUCATIVO.put("E", "ESPECIALIZACION");
    LISTA_NIVEL_EDUCATIVO.put("M", "MAESTRIA");
    LISTA_NIVEL_EDUCATIVO.put("C", "DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("R", "POST DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("G", "POSTGRADO");
    LISTA_NIVEL_EDUCATIVO.put("L", "DIPLOMADO");
    LISTA_NIVEL_EDUCATIVO.put("O", "OTRO");
  }

  public NivelBeca()
  {
    jdoSetinicioNivel(this, 1);

    jdoSetfinNivel(this, 1);
  }

  public String toString()
  {
    return (String)LISTA_NIVEL_EDUCATIVO.get(jdoGetnivelEducativo(this));
  }

  public static Map getLISTA_NIVEL_EDUCATIVO()
  {
    return LISTA_NIVEL_EDUCATIVO;
  }

  public int getFinNivel()
  {
    return jdoGetfinNivel(this);
  }

  public long getIdNivelBeca()
  {
    return jdoGetidNivelBeca(this);
  }

  public int getInicioNivel()
  {
    return jdoGetinicioNivel(this);
  }

  public String getNivelEducativo()
  {
    return jdoGetnivelEducativo(this);
  }

  public String getProximoNivel()
  {
    return jdoGetproximoNivel(this);
  }

  public void setFinNivel(int i)
  {
    jdoSetfinNivel(this, i);
  }

  public void setIdNivelBeca(long l)
  {
    jdoSetidNivelBeca(this, l);
  }

  public void setInicioNivel(int i)
  {
    jdoSetinicioNivel(this, i);
  }

  public void setNivelEducativo(String string)
  {
    jdoSetnivelEducativo(this, string);
  }

  public void setProximoNivel(String string)
  {
    jdoSetproximoNivel(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    NivelBeca localNivelBeca = new NivelBeca();
    localNivelBeca.jdoFlags = 1;
    localNivelBeca.jdoStateManager = paramStateManager;
    return localNivelBeca;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    NivelBeca localNivelBeca = new NivelBeca();
    localNivelBeca.jdoCopyKeyFieldsFromObjectId(paramObject);
    localNivelBeca.jdoFlags = 1;
    localNivelBeca.jdoStateManager = paramStateManager;
    return localNivelBeca;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.finNivel);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idNivelBeca);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.inicioNivel);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivelEducativo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.proximoNivel);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finNivel = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idNivelBeca = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.inicioNivel = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.proximoNivel = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(NivelBeca paramNivelBeca, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramNivelBeca == null)
        throw new IllegalArgumentException("arg1");
      this.finNivel = paramNivelBeca.finNivel;
      return;
    case 1:
      if (paramNivelBeca == null)
        throw new IllegalArgumentException("arg1");
      this.idNivelBeca = paramNivelBeca.idNivelBeca;
      return;
    case 2:
      if (paramNivelBeca == null)
        throw new IllegalArgumentException("arg1");
      this.inicioNivel = paramNivelBeca.inicioNivel;
      return;
    case 3:
      if (paramNivelBeca == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramNivelBeca.nivelEducativo;
      return;
    case 4:
      if (paramNivelBeca == null)
        throw new IllegalArgumentException("arg1");
      this.proximoNivel = paramNivelBeca.proximoNivel;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof NivelBeca))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    NivelBeca localNivelBeca = (NivelBeca)paramObject;
    if (localNivelBeca.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localNivelBeca, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new NivelBecaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new NivelBecaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NivelBecaPK))
      throw new IllegalArgumentException("arg1");
    NivelBecaPK localNivelBecaPK = (NivelBecaPK)paramObject;
    localNivelBecaPK.idNivelBeca = this.idNivelBeca;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NivelBecaPK))
      throw new IllegalArgumentException("arg1");
    NivelBecaPK localNivelBecaPK = (NivelBecaPK)paramObject;
    this.idNivelBeca = localNivelBecaPK.idNivelBeca;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NivelBecaPK))
      throw new IllegalArgumentException("arg2");
    NivelBecaPK localNivelBecaPK = (NivelBecaPK)paramObject;
    localNivelBecaPK.idNivelBeca = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NivelBecaPK))
      throw new IllegalArgumentException("arg2");
    NivelBecaPK localNivelBecaPK = (NivelBecaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localNivelBecaPK.idNivelBeca);
  }

  private static final int jdoGetfinNivel(NivelBeca paramNivelBeca)
  {
    if (paramNivelBeca.jdoFlags <= 0)
      return paramNivelBeca.finNivel;
    StateManager localStateManager = paramNivelBeca.jdoStateManager;
    if (localStateManager == null)
      return paramNivelBeca.finNivel;
    if (localStateManager.isLoaded(paramNivelBeca, jdoInheritedFieldCount + 0))
      return paramNivelBeca.finNivel;
    return localStateManager.getIntField(paramNivelBeca, jdoInheritedFieldCount + 0, paramNivelBeca.finNivel);
  }

  private static final void jdoSetfinNivel(NivelBeca paramNivelBeca, int paramInt)
  {
    if (paramNivelBeca.jdoFlags == 0)
    {
      paramNivelBeca.finNivel = paramInt;
      return;
    }
    StateManager localStateManager = paramNivelBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelBeca.finNivel = paramInt;
      return;
    }
    localStateManager.setIntField(paramNivelBeca, jdoInheritedFieldCount + 0, paramNivelBeca.finNivel, paramInt);
  }

  private static final long jdoGetidNivelBeca(NivelBeca paramNivelBeca)
  {
    return paramNivelBeca.idNivelBeca;
  }

  private static final void jdoSetidNivelBeca(NivelBeca paramNivelBeca, long paramLong)
  {
    StateManager localStateManager = paramNivelBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelBeca.idNivelBeca = paramLong;
      return;
    }
    localStateManager.setLongField(paramNivelBeca, jdoInheritedFieldCount + 1, paramNivelBeca.idNivelBeca, paramLong);
  }

  private static final int jdoGetinicioNivel(NivelBeca paramNivelBeca)
  {
    if (paramNivelBeca.jdoFlags <= 0)
      return paramNivelBeca.inicioNivel;
    StateManager localStateManager = paramNivelBeca.jdoStateManager;
    if (localStateManager == null)
      return paramNivelBeca.inicioNivel;
    if (localStateManager.isLoaded(paramNivelBeca, jdoInheritedFieldCount + 2))
      return paramNivelBeca.inicioNivel;
    return localStateManager.getIntField(paramNivelBeca, jdoInheritedFieldCount + 2, paramNivelBeca.inicioNivel);
  }

  private static final void jdoSetinicioNivel(NivelBeca paramNivelBeca, int paramInt)
  {
    if (paramNivelBeca.jdoFlags == 0)
    {
      paramNivelBeca.inicioNivel = paramInt;
      return;
    }
    StateManager localStateManager = paramNivelBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelBeca.inicioNivel = paramInt;
      return;
    }
    localStateManager.setIntField(paramNivelBeca, jdoInheritedFieldCount + 2, paramNivelBeca.inicioNivel, paramInt);
  }

  private static final String jdoGetnivelEducativo(NivelBeca paramNivelBeca)
  {
    if (paramNivelBeca.jdoFlags <= 0)
      return paramNivelBeca.nivelEducativo;
    StateManager localStateManager = paramNivelBeca.jdoStateManager;
    if (localStateManager == null)
      return paramNivelBeca.nivelEducativo;
    if (localStateManager.isLoaded(paramNivelBeca, jdoInheritedFieldCount + 3))
      return paramNivelBeca.nivelEducativo;
    return localStateManager.getStringField(paramNivelBeca, jdoInheritedFieldCount + 3, paramNivelBeca.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(NivelBeca paramNivelBeca, String paramString)
  {
    if (paramNivelBeca.jdoFlags == 0)
    {
      paramNivelBeca.nivelEducativo = paramString;
      return;
    }
    StateManager localStateManager = paramNivelBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelBeca.nivelEducativo = paramString;
      return;
    }
    localStateManager.setStringField(paramNivelBeca, jdoInheritedFieldCount + 3, paramNivelBeca.nivelEducativo, paramString);
  }

  private static final String jdoGetproximoNivel(NivelBeca paramNivelBeca)
  {
    if (paramNivelBeca.jdoFlags <= 0)
      return paramNivelBeca.proximoNivel;
    StateManager localStateManager = paramNivelBeca.jdoStateManager;
    if (localStateManager == null)
      return paramNivelBeca.proximoNivel;
    if (localStateManager.isLoaded(paramNivelBeca, jdoInheritedFieldCount + 4))
      return paramNivelBeca.proximoNivel;
    return localStateManager.getStringField(paramNivelBeca, jdoInheritedFieldCount + 4, paramNivelBeca.proximoNivel);
  }

  private static final void jdoSetproximoNivel(NivelBeca paramNivelBeca, String paramString)
  {
    if (paramNivelBeca.jdoFlags == 0)
    {
      paramNivelBeca.proximoNivel = paramString;
      return;
    }
    StateManager localStateManager = paramNivelBeca.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelBeca.proximoNivel = paramString;
      return;
    }
    localStateManager.setStringField(paramNivelBeca, jdoInheritedFieldCount + 4, paramNivelBeca.proximoNivel, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}