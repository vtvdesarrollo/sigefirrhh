package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ParametroBecaGeneralBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParametroBecaGeneral(ParametroBecaGeneral parametroBecaGeneral)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ParametroBecaGeneral parametroBecaGeneralNew = 
      (ParametroBecaGeneral)BeanUtils.cloneBean(
      parametroBecaGeneral);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroBecaGeneralNew.getTipoPersonal() != null) {
      parametroBecaGeneralNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroBecaGeneralNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(parametroBecaGeneralNew);
  }

  public void updateParametroBecaGeneral(ParametroBecaGeneral parametroBecaGeneral) throws Exception
  {
    ParametroBecaGeneral parametroBecaGeneralModify = 
      findParametroBecaGeneralById(parametroBecaGeneral.getIdParametroBecaGeneral());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroBecaGeneral.getTipoPersonal() != null) {
      parametroBecaGeneral.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroBecaGeneral.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(parametroBecaGeneralModify, parametroBecaGeneral);
  }

  public void deleteParametroBecaGeneral(ParametroBecaGeneral parametroBecaGeneral) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ParametroBecaGeneral parametroBecaGeneralDelete = 
      findParametroBecaGeneralById(parametroBecaGeneral.getIdParametroBecaGeneral());
    pm.deletePersistent(parametroBecaGeneralDelete);
  }

  public ParametroBecaGeneral findParametroBecaGeneralById(long idParametroBecaGeneral) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParametroBecaGeneral == pIdParametroBecaGeneral";
    Query query = pm.newQuery(ParametroBecaGeneral.class, filter);

    query.declareParameters("long pIdParametroBecaGeneral");

    parameters.put("pIdParametroBecaGeneral", new Long(idParametroBecaGeneral));

    Collection colParametroBecaGeneral = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParametroBecaGeneral.iterator();
    return (ParametroBecaGeneral)iterator.next();
  }

  public Collection findParametroBecaGeneralAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parametroBecaGeneralExtent = pm.getExtent(
      ParametroBecaGeneral.class, true);
    Query query = pm.newQuery(parametroBecaGeneralExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ParametroBecaGeneral.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colParametroBecaGeneral = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParametroBecaGeneral);

    return colParametroBecaGeneral;
  }
}