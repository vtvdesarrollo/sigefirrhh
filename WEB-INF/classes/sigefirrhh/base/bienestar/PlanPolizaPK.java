package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class PlanPolizaPK
  implements Serializable
{
  public long idPlanPoliza;

  public PlanPolizaPK()
  {
  }

  public PlanPolizaPK(long idPlanPoliza)
  {
    this.idPlanPoliza = idPlanPoliza;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PlanPolizaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PlanPolizaPK thatPK)
  {
    return 
      this.idPlanPoliza == thatPK.idPlanPoliza;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPlanPoliza)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPlanPoliza);
  }
}