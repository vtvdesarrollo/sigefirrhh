package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class PolizaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPoliza(Poliza poliza)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Poliza polizaNew = 
      (Poliza)BeanUtils.cloneBean(
      poliza);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (polizaNew.getOrganismo() != null) {
      polizaNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        polizaNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(polizaNew);
  }

  public void updatePoliza(Poliza poliza) throws Exception
  {
    Poliza polizaModify = 
      findPolizaById(poliza.getIdPoliza());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (poliza.getOrganismo() != null) {
      poliza.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        poliza.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(polizaModify, poliza);
  }

  public void deletePoliza(Poliza poliza) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Poliza polizaDelete = 
      findPolizaById(poliza.getIdPoliza());
    pm.deletePersistent(polizaDelete);
  }

  public Poliza findPolizaById(long idPoliza) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPoliza == pIdPoliza";
    Query query = pm.newQuery(Poliza.class, filter);

    query.declareParameters("long pIdPoliza");

    parameters.put("pIdPoliza", new Long(idPoliza));

    Collection colPoliza = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPoliza.iterator();
    return (Poliza)iterator.next();
  }

  public Collection findPolizaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent polizaExtent = pm.getExtent(
      Poliza.class, true);
    Query query = pm.newQuery(polizaExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodPoliza(String codPoliza, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codPoliza == pCodPoliza &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Poliza.class, filter);

    query.declareParameters("java.lang.String pCodPoliza, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodPoliza", new String(codPoliza));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colPoliza = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPoliza);

    return colPoliza;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Poliza.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colPoliza = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPoliza);

    return colPoliza;
  }

  public Collection findByVigente(String vigente, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "vigente == pVigente &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Poliza.class, filter);

    query.declareParameters("java.lang.String pVigente, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pVigente", new String(vigente));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colPoliza = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPoliza);

    return colPoliza;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Poliza.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colPoliza = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPoliza);

    return colPoliza;
  }
}