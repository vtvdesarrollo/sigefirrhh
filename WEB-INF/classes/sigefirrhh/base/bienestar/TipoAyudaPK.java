package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class TipoAyudaPK
  implements Serializable
{
  public long idTipoAyuda;

  public TipoAyudaPK()
  {
  }

  public TipoAyudaPK(long idTipoAyuda)
  {
    this.idTipoAyuda = idTipoAyuda;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoAyudaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoAyudaPK thatPK)
  {
    return 
      this.idTipoAyuda == thatPK.idTipoAyuda;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoAyuda)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoAyuda);
  }
}