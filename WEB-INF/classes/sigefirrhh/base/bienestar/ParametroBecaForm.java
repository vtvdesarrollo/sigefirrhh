package sigefirrhh.base.bienestar;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ParametroBecaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ParametroBecaForm.class.getName());
  private ParametroBeca parametroBeca;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private boolean showParametroBecaByTipoPersonal;
  private String findSelectTipoPersonal;
  private Collection findColTipoPersonal;
  private Collection colTipoPersonal;
  private Collection colNivelbeca;
  private Collection colConceptoTipoPersonal;
  private String selectTipoPersonal;
  private String selectNivelbeca;
  private String selectConceptoTipoPersonal;
  private Object stateResultParametroBecaByTipoPersonal = null;

  public String getFindSelectTipoPersonal()
  {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.parametroBeca.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.parametroBeca.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectNivelbeca() {
    return this.selectNivelbeca;
  }
  public void setSelectNivelbeca(String valNivelbeca) {
    Iterator iterator = this.colNivelbeca.iterator();
    NivelBeca nivelbeca = null;
    this.parametroBeca.setNivelbeca(null);
    while (iterator.hasNext()) {
      nivelbeca = (NivelBeca)iterator.next();
      if (String.valueOf(nivelbeca.getIdNivelBeca()).equals(
        valNivelbeca)) {
        this.parametroBeca.setNivelbeca(
          nivelbeca);
        break;
      }
    }
    this.selectNivelbeca = valNivelbeca;
  }
  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipo = null;

    this.parametroBeca.setConceptoTipoPersonal(null);

    while (iterator.hasNext()) {
      conceptoTipo = (ConceptoTipoPersonal)iterator.next();
      long id = conceptoTipo.getIdConceptoTipoPersonal();
      if (String.valueOf(id).equals(
        valConceptoTipoPersonal)) {
        try
        {
          this.parametroBeca.setConceptoTipoPersonal(conceptoTipo);
        } catch (Exception e) {
          log.error("Excepcion controlada:", e);
        }
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public ParametroBeca getParametroBeca() {
    if (this.parametroBeca == null) {
      this.parametroBeca = new ParametroBeca();
    }
    return this.parametroBeca;
  }

  public ParametroBecaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public boolean isShowConceptoTipoPersonal() {
    return this.colConceptoTipoPersonal != null;
  }
  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colConceptoTipoPersonal = null;
      log.error("pasa 1");
      if (idTipoPersonal > 0L) {
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonalAndTipoPrestamo(
          idTipoPersonal, "B");
        log.error("size " + this.colConceptoTipoPersonal.size());
      }
      else {
        log.error("pasa 2");
        this.selectConceptoTipoPersonal = null;
        this.parametroBeca.setConceptoTipoPersonal(
          null);
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.selectConceptoTipoPersonal = null;
      this.parametroBeca.setConceptoTipoPersonal(
        null);
    }
  }

  public Collection getColTipoPersonal() { Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColNivelbeca()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colNivelbeca.iterator();
    NivelBeca nivelbeca = null;

    while (iterator.hasNext())
    {
      nivelbeca = (NivelBeca)iterator.next();
      log.error("preuba1" + nivelbeca.getIdNivelBeca() + nivelbeca.toString());
      col.add(new SelectItem(
        String.valueOf(nivelbeca.getIdNivelBeca()), 
        nivelbeca.toString()));
    }

    return col;
  }

  public Collection getColConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipo = null;

    while (iterator.hasNext()) {
      conceptoTipo = (ConceptoTipoPersonal)iterator.next();
      Long id = new Long(conceptoTipo.getIdConceptoTipoPersonal());
      String nombre = conceptoTipo.getConcepto().getDescripcion();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListSueldo() {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroBeca.LISTA_SUELDO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colNivelbeca = 
        this.bienestarFacade.findAllNivelBeca();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findParametroBecaByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findParametroBecaByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showParametroBecaByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showParametroBecaByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;

    return null;
  }

  public boolean isShowParametroBecaByTipoPersonal() {
    return this.showParametroBecaByTipoPersonal;
  }

  public String selectParametroBeca()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectNivelbeca = null;
    this.selectConceptoTipoPersonal = null;

    long idParametroBeca = 
      Long.parseLong((String)requestParameterMap.get("idParametroBeca"));
    try
    {
      this.parametroBeca = 
        this.bienestarFacade.findParametroBecaById(
        idParametroBeca);
      if (this.parametroBeca.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.parametroBeca.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.parametroBeca.getNivelbeca() != null) {
        this.selectNivelbeca = 
          String.valueOf(this.parametroBeca.getNivelbeca().getIdNivelBeca());
      }
      if (this.parametroBeca.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.parametroBeca.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.parametroBeca = null;
    this.showParametroBecaByTipoPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.parametroBeca.getTiempoSitp() != null) && 
      (this.parametroBeca.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.bienestarFacade.addParametroBeca(
          this.parametroBeca);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.parametroBeca);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.bienestarFacade.updateParametroBeca(
          this.parametroBeca);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.parametroBeca);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.bienestarFacade.deleteParametroBeca(
        this.parametroBeca);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.parametroBeca);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }
  public String add() {
    log.error("el 1:");
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.parametroBeca = new ParametroBeca();
    log.error("el 2:");
    this.selectTipoPersonal = null;

    log.error("el 3:");
    this.selectNivelbeca = null;

    this.selectConceptoTipoPersonal = null;
    log.error("el 4:");

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    log.error("el id: " + identityGenerator.getNextSequenceNumber("sigefirrhh.base.bienestar.ParametroBeca"));
    this.parametroBeca.setIdParametroBeca(identityGenerator.getNextSequenceNumber("sigefirrhh.base.bienestar.ParametroBeca"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.parametroBeca = new ParametroBeca();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}