package sigefirrhh.base.bienestar;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class PrimasPlanForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PrimasPlanForm.class.getName());
  private PrimasPlan primasPlan;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private boolean showPrimasPlanByPlanPoliza;
  private boolean showPrimasPlanByParentesco;
  private String findSelectPlanPoliza;
  private String findParentesco;
  private Collection findColPlanPoliza;
  private Collection colPlanPoliza;
  private String selectPlanPoliza;
  private String selectPolizaForPlanPoliza;
  private Collection colPolizaForPlanPoliza;
  private String findSelectPolizaForPlanPoliza;
  private Collection findColPolizaForPlanPoliza;
  private Object stateResultPrimasPlanByPlanPoliza = null;

  private Object stateResultPrimasPlanByParentesco = null;

  public boolean isFindShowPlanPoliza()
  {
    return this.findColPlanPoliza != null;
  }
  public Collection getFindColPolizaForPlanPoliza() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPolizaForPlanPoliza.iterator();
    Poliza polizaForPlanPoliza = null;
    while (iterator.hasNext()) {
      polizaForPlanPoliza = (Poliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(polizaForPlanPoliza.getIdPoliza()), 
        polizaForPlanPoliza.toString()));
    }
    return col;
  }
  public String getFindSelectPolizaForPlanPoliza() {
    return this.findSelectPolizaForPlanPoliza;
  }
  public void setFindSelectPolizaForPlanPoliza(String valPolizaForPlanPoliza) {
    this.findSelectPolizaForPlanPoliza = valPolizaForPlanPoliza;
  }
  public void findChangePolizaForPlanPoliza(ValueChangeEvent event) {
    long idPoliza = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColPlanPoliza = null;
      if (idPoliza > 0L)
        this.findColPlanPoliza = 
          this.bienestarFacade.findPlanPolizaByPoliza(
          idPoliza);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowPolizaForPlanPoliza() { return this.findColPolizaForPlanPoliza != null; }

  public String getFindSelectPlanPoliza()
  {
    return this.findSelectPlanPoliza;
  }
  public void setFindSelectPlanPoliza(String valPlanPoliza) {
    this.findSelectPlanPoliza = valPlanPoliza;
  }

  public Collection getFindColPlanPoliza() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPlanPoliza.iterator();
    PlanPoliza planPoliza = null;
    while (iterator.hasNext()) {
      planPoliza = (PlanPoliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPoliza.getIdPlanPoliza()), 
        planPoliza.toString()));
    }
    return col;
  }
  public String getFindParentesco() {
    return this.findParentesco;
  }
  public void setFindParentesco(String findParentesco) {
    this.findParentesco = findParentesco;
  }

  public String getSelectPolizaForPlanPoliza()
  {
    return this.selectPolizaForPlanPoliza;
  }
  public void setSelectPolizaForPlanPoliza(String valPolizaForPlanPoliza) {
    this.selectPolizaForPlanPoliza = valPolizaForPlanPoliza;
  }
  public void changePolizaForPlanPoliza(ValueChangeEvent event) {
    long idPoliza = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colPlanPoliza = null;
      if (idPoliza > 0L) {
        this.colPlanPoliza = 
          this.bienestarFacade.findPlanPolizaByPoliza(
          idPoliza);
      } else {
        this.selectPlanPoliza = null;
        this.primasPlan.setPlanPoliza(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectPlanPoliza = null;
      this.primasPlan.setPlanPoliza(
        null);
    }
  }

  public boolean isShowPolizaForPlanPoliza() { return this.colPolizaForPlanPoliza != null; }

  public boolean isShowPlanPoliza() {
    return this.colPlanPoliza != null;
  }
  public String getSelectPlanPoliza() {
    return this.selectPlanPoliza;
  }
  public void setSelectPlanPoliza(String valPlanPoliza) {
    Iterator iterator = this.colPlanPoliza.iterator();
    PlanPoliza planPoliza = null;
    this.primasPlan.setPlanPoliza(null);
    while (iterator.hasNext()) {
      planPoliza = (PlanPoliza)iterator.next();
      if (String.valueOf(planPoliza.getIdPlanPoliza()).equals(
        valPlanPoliza)) {
        this.primasPlan.setPlanPoliza(
          planPoliza);
        break;
      }
    }
    this.selectPlanPoliza = valPlanPoliza;
  }
  public Collection getResult() {
    return this.result;
  }

  public PrimasPlan getPrimasPlan() {
    if (this.primasPlan == null) {
      this.primasPlan = new PrimasPlan();
    }
    return this.primasPlan;
  }

  public PrimasPlanForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPolizaForPlanPoliza()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPolizaForPlanPoliza.iterator();
    Poliza polizaForPlanPoliza = null;
    while (iterator.hasNext()) {
      polizaForPlanPoliza = (Poliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(polizaForPlanPoliza.getIdPoliza()), 
        polizaForPlanPoliza.toString()));
    }
    return col;
  }

  public Collection getColPlanPoliza() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPoliza.iterator();
    PlanPoliza planPoliza = null;
    while (iterator.hasNext()) {
      planPoliza = (PlanPoliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPoliza.getIdPlanPoliza()), 
        planPoliza.toString()));
    }
    return col;
  }

  public Collection getListParentesco() {
    Collection col = new ArrayList();

    Iterator iterEntry = PrimasPlan.LISTA_PARENTESCO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColPolizaForPlanPoliza = 
        this.bienestarFacade.findPolizaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colPolizaForPlanPoliza = 
        this.bienestarFacade.findPolizaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPrimasPlanByPlanPoliza()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findPrimasPlanByPlanPoliza(Long.valueOf(this.findSelectPlanPoliza).longValue());
      this.showPrimasPlanByPlanPoliza = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPrimasPlanByPlanPoliza)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }
    this.findSelectPolizaForPlanPoliza = null;
    this.findSelectPlanPoliza = null;
    this.findParentesco = null;

    return null;
  }

  public String findPrimasPlanByParentesco()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findPrimasPlanByParentesco(this.findParentesco);
      this.showPrimasPlanByParentesco = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPrimasPlanByParentesco)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }
    this.findSelectPolizaForPlanPoliza = null;
    this.findSelectPlanPoliza = null;
    this.findParentesco = null;

    return null;
  }

  public boolean isShowPrimasPlanByPlanPoliza() {
    return this.showPrimasPlanByPlanPoliza;
  }
  public boolean isShowPrimasPlanByParentesco() {
    return this.showPrimasPlanByParentesco;
  }

  public String selectPrimasPlan()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPlanPoliza = null;

    long idPrimasPlan = 
      Long.parseLong((String)requestParameterMap.get("idPrimasPlan"));
    try
    {
      this.primasPlan = 
        this.bienestarFacade.findPrimasPlanById(
        idPrimasPlan);
      if (this.primasPlan.getPlanPoliza() != null) {
        this.selectPlanPoliza = 
          String.valueOf(this.primasPlan.getPlanPoliza().getIdPlanPoliza());
      }

      PlanPoliza planPoliza = null;
      Poliza polizaForPlanPoliza = null;

      if (this.primasPlan.getPlanPoliza() != null) {
        long idPlanPoliza = 
          this.primasPlan.getPlanPoliza().getIdPlanPoliza();
        this.selectPlanPoliza = String.valueOf(idPlanPoliza);
        planPoliza = this.bienestarFacade.findPlanPolizaById(
          idPlanPoliza);
        this.colPlanPoliza = this.bienestarFacade.findPlanPolizaByPoliza(
          planPoliza.getPoliza().getIdPoliza());

        long idPolizaForPlanPoliza = 
          this.primasPlan.getPlanPoliza().getPoliza().getIdPoliza();
        this.selectPolizaForPlanPoliza = String.valueOf(idPolizaForPlanPoliza);
        polizaForPlanPoliza = 
          this.bienestarFacade.findPolizaById(
          idPolizaForPlanPoliza);
        this.colPolizaForPlanPoliza = 
          this.bienestarFacade.findAllPoliza();
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.primasPlan = null;
    this.showPrimasPlanByPlanPoliza = false;
    this.showPrimasPlanByParentesco = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (this.primasPlan.getPorcentajePatron() + this.primasPlan.getPorcentajeTrabajador() > 100.0D) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La suma del porcentaje del patron, mas la suma del porcentaje del trabajador, no puede ser mayor a 100% ", ""));
      error = true;
    }

    if (error) {
      return null;
    }

    try
    {
      if (this.adding) {
        this.bienestarFacade.addPrimasPlan(
          this.primasPlan);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.bienestarFacade.updatePrimasPlan(
          this.primasPlan);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.bienestarFacade.deletePrimasPlan(
        this.primasPlan);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.primasPlan = new PrimasPlan();

    this.selectPlanPoliza = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.primasPlan.setIdPrimasPlan(identityGenerator.getNextSequenceNumber("sigefirrhh.base.bienestar.PrimasPlan"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.primasPlan = new PrimasPlan();
    return "cancel";
  }

  public boolean isShowPrimaFemeninoAux()
  {
    try
    {
      return (this.primasPlan.getParentesco().equals("H")) || (this.primasPlan.getParentesco().equals("U")) || (this.primasPlan.getParentesco().equals("E")) || (this.primasPlan.getParentesco().equals("S")) || (this.primasPlan.getParentesco().equals("B")) || (this.primasPlan.getParentesco().equals("A")) || (this.primasPlan.getParentesco().equals("I")) || (this.primasPlan.getParentesco().equals("M")) || (this.primasPlan.getParentesco().equals("T")) || (this.primasPlan.getParentesco().equals("C")) || (this.primasPlan.getParentesco().equals("O")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowPrimaMasculinoAux()
  {
    try {
      return (this.primasPlan.getParentesco().equals("H")) || (this.primasPlan.getParentesco().equals("U")) || (this.primasPlan.getParentesco().equals("E")) || (this.primasPlan.getParentesco().equals("S")) || (this.primasPlan.getParentesco().equals("B")) || (this.primasPlan.getParentesco().equals("A")) || (this.primasPlan.getParentesco().equals("I")) || (this.primasPlan.getParentesco().equals("P")) || (this.primasPlan.getParentesco().equals("T")) || (this.primasPlan.getParentesco().equals("C")) || (this.primasPlan.getParentesco().equals("O")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}