package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ParentescoPlan
  implements Serializable, PersistenceCapable
{
  private long idParentescoPlan;
  private String parentesco;
  private int maximoBeneficiarios;
  private PlanPoliza planPoliza;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idParentescoPlan", "maximoBeneficiarios", "parentesco", "planPoliza" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.bienestar.PlanPoliza") };
  private static final byte[] jdoFieldFlags = { 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public long getIdParentescoPlan()
  {
    return jdoGetidParentescoPlan(this);
  }

  public int getMaximoBeneficiarios()
  {
    return jdoGetmaximoBeneficiarios(this);
  }

  public String getParentesco()
  {
    return jdoGetparentesco(this);
  }

  public PlanPoliza getPlanPoliza()
  {
    return jdoGetplanPoliza(this);
  }

  public void setIdParentescoPlan(long l)
  {
    jdoSetidParentescoPlan(this, l);
  }

  public void setMaximoBeneficiarios(int i)
  {
    jdoSetmaximoBeneficiarios(this, i);
  }

  public void setParentesco(String string)
  {
    jdoSetparentesco(this, string);
  }

  public void setPlanPoliza(PlanPoliza poliza)
  {
    jdoSetplanPoliza(this, poliza);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.ParentescoPlan"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParentescoPlan());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParentescoPlan localParentescoPlan = new ParentescoPlan();
    localParentescoPlan.jdoFlags = 1;
    localParentescoPlan.jdoStateManager = paramStateManager;
    return localParentescoPlan;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParentescoPlan localParentescoPlan = new ParentescoPlan();
    localParentescoPlan.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParentescoPlan.jdoFlags = 1;
    localParentescoPlan.jdoStateManager = paramStateManager;
    return localParentescoPlan;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParentescoPlan);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.maximoBeneficiarios);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.parentesco);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPoliza);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParentescoPlan = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.maximoBeneficiarios = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.parentesco = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPoliza = ((PlanPoliza)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParentescoPlan paramParentescoPlan, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParentescoPlan == null)
        throw new IllegalArgumentException("arg1");
      this.idParentescoPlan = paramParentescoPlan.idParentescoPlan;
      return;
    case 1:
      if (paramParentescoPlan == null)
        throw new IllegalArgumentException("arg1");
      this.maximoBeneficiarios = paramParentescoPlan.maximoBeneficiarios;
      return;
    case 2:
      if (paramParentescoPlan == null)
        throw new IllegalArgumentException("arg1");
      this.parentesco = paramParentescoPlan.parentesco;
      return;
    case 3:
      if (paramParentescoPlan == null)
        throw new IllegalArgumentException("arg1");
      this.planPoliza = paramParentescoPlan.planPoliza;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParentescoPlan))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParentescoPlan localParentescoPlan = (ParentescoPlan)paramObject;
    if (localParentescoPlan.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParentescoPlan, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParentescoPlanPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParentescoPlanPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParentescoPlanPK))
      throw new IllegalArgumentException("arg1");
    ParentescoPlanPK localParentescoPlanPK = (ParentescoPlanPK)paramObject;
    localParentescoPlanPK.idParentescoPlan = this.idParentescoPlan;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParentescoPlanPK))
      throw new IllegalArgumentException("arg1");
    ParentescoPlanPK localParentescoPlanPK = (ParentescoPlanPK)paramObject;
    this.idParentescoPlan = localParentescoPlanPK.idParentescoPlan;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParentescoPlanPK))
      throw new IllegalArgumentException("arg2");
    ParentescoPlanPK localParentescoPlanPK = (ParentescoPlanPK)paramObject;
    localParentescoPlanPK.idParentescoPlan = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParentescoPlanPK))
      throw new IllegalArgumentException("arg2");
    ParentescoPlanPK localParentescoPlanPK = (ParentescoPlanPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localParentescoPlanPK.idParentescoPlan);
  }

  private static final long jdoGetidParentescoPlan(ParentescoPlan paramParentescoPlan)
  {
    return paramParentescoPlan.idParentescoPlan;
  }

  private static final void jdoSetidParentescoPlan(ParentescoPlan paramParentescoPlan, long paramLong)
  {
    StateManager localStateManager = paramParentescoPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramParentescoPlan.idParentescoPlan = paramLong;
      return;
    }
    localStateManager.setLongField(paramParentescoPlan, jdoInheritedFieldCount + 0, paramParentescoPlan.idParentescoPlan, paramLong);
  }

  private static final int jdoGetmaximoBeneficiarios(ParentescoPlan paramParentescoPlan)
  {
    if (paramParentescoPlan.jdoFlags <= 0)
      return paramParentescoPlan.maximoBeneficiarios;
    StateManager localStateManager = paramParentescoPlan.jdoStateManager;
    if (localStateManager == null)
      return paramParentescoPlan.maximoBeneficiarios;
    if (localStateManager.isLoaded(paramParentescoPlan, jdoInheritedFieldCount + 1))
      return paramParentescoPlan.maximoBeneficiarios;
    return localStateManager.getIntField(paramParentescoPlan, jdoInheritedFieldCount + 1, paramParentescoPlan.maximoBeneficiarios);
  }

  private static final void jdoSetmaximoBeneficiarios(ParentescoPlan paramParentescoPlan, int paramInt)
  {
    if (paramParentescoPlan.jdoFlags == 0)
    {
      paramParentescoPlan.maximoBeneficiarios = paramInt;
      return;
    }
    StateManager localStateManager = paramParentescoPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramParentescoPlan.maximoBeneficiarios = paramInt;
      return;
    }
    localStateManager.setIntField(paramParentescoPlan, jdoInheritedFieldCount + 1, paramParentescoPlan.maximoBeneficiarios, paramInt);
  }

  private static final String jdoGetparentesco(ParentescoPlan paramParentescoPlan)
  {
    if (paramParentescoPlan.jdoFlags <= 0)
      return paramParentescoPlan.parentesco;
    StateManager localStateManager = paramParentescoPlan.jdoStateManager;
    if (localStateManager == null)
      return paramParentescoPlan.parentesco;
    if (localStateManager.isLoaded(paramParentescoPlan, jdoInheritedFieldCount + 2))
      return paramParentescoPlan.parentesco;
    return localStateManager.getStringField(paramParentescoPlan, jdoInheritedFieldCount + 2, paramParentescoPlan.parentesco);
  }

  private static final void jdoSetparentesco(ParentescoPlan paramParentescoPlan, String paramString)
  {
    if (paramParentescoPlan.jdoFlags == 0)
    {
      paramParentescoPlan.parentesco = paramString;
      return;
    }
    StateManager localStateManager = paramParentescoPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramParentescoPlan.parentesco = paramString;
      return;
    }
    localStateManager.setStringField(paramParentescoPlan, jdoInheritedFieldCount + 2, paramParentescoPlan.parentesco, paramString);
  }

  private static final PlanPoliza jdoGetplanPoliza(ParentescoPlan paramParentescoPlan)
  {
    StateManager localStateManager = paramParentescoPlan.jdoStateManager;
    if (localStateManager == null)
      return paramParentescoPlan.planPoliza;
    if (localStateManager.isLoaded(paramParentescoPlan, jdoInheritedFieldCount + 3))
      return paramParentescoPlan.planPoliza;
    return (PlanPoliza)localStateManager.getObjectField(paramParentescoPlan, jdoInheritedFieldCount + 3, paramParentescoPlan.planPoliza);
  }

  private static final void jdoSetplanPoliza(ParentescoPlan paramParentescoPlan, PlanPoliza paramPlanPoliza)
  {
    StateManager localStateManager = paramParentescoPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramParentescoPlan.planPoliza = paramPlanPoliza;
      return;
    }
    localStateManager.setObjectField(paramParentescoPlan, jdoInheritedFieldCount + 3, paramParentescoPlan.planPoliza, paramPlanPoliza);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}