package sigefirrhh.base.bienestar;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ProveedorTicket
  implements Serializable, PersistenceCapable
{
  private long idProveedorTicket;
  private String codProveedorTicket;
  private String nombre;
  private String direccion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codProveedorTicket", "direccion", "idProveedorTicket", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcodProveedorTicket(this) + "-" + 
      jdoGetnombre(this);
  }

  public String getCodProveedorTicket()
  {
    return jdoGetcodProveedorTicket(this);
  }

  public String getDireccion()
  {
    return jdoGetdireccion(this);
  }

  public long getIdProveedorTicket()
  {
    return jdoGetidProveedorTicket(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodProveedorTicket(String string)
  {
    jdoSetcodProveedorTicket(this, string);
  }

  public void setDireccion(String string)
  {
    jdoSetdireccion(this, string);
  }

  public void setIdProveedorTicket(long l)
  {
    jdoSetidProveedorTicket(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.bienestar.ProveedorTicket"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ProveedorTicket());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ProveedorTicket localProveedorTicket = new ProveedorTicket();
    localProveedorTicket.jdoFlags = 1;
    localProveedorTicket.jdoStateManager = paramStateManager;
    return localProveedorTicket;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ProveedorTicket localProveedorTicket = new ProveedorTicket();
    localProveedorTicket.jdoCopyKeyFieldsFromObjectId(paramObject);
    localProveedorTicket.jdoFlags = 1;
    localProveedorTicket.jdoStateManager = paramStateManager;
    return localProveedorTicket;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codProveedorTicket);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.direccion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idProveedorTicket);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codProveedorTicket = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.direccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idProveedorTicket = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ProveedorTicket paramProveedorTicket, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramProveedorTicket == null)
        throw new IllegalArgumentException("arg1");
      this.codProveedorTicket = paramProveedorTicket.codProveedorTicket;
      return;
    case 1:
      if (paramProveedorTicket == null)
        throw new IllegalArgumentException("arg1");
      this.direccion = paramProveedorTicket.direccion;
      return;
    case 2:
      if (paramProveedorTicket == null)
        throw new IllegalArgumentException("arg1");
      this.idProveedorTicket = paramProveedorTicket.idProveedorTicket;
      return;
    case 3:
      if (paramProveedorTicket == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramProveedorTicket.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ProveedorTicket))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ProveedorTicket localProveedorTicket = (ProveedorTicket)paramObject;
    if (localProveedorTicket.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localProveedorTicket, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ProveedorTicketPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ProveedorTicketPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProveedorTicketPK))
      throw new IllegalArgumentException("arg1");
    ProveedorTicketPK localProveedorTicketPK = (ProveedorTicketPK)paramObject;
    localProveedorTicketPK.idProveedorTicket = this.idProveedorTicket;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProveedorTicketPK))
      throw new IllegalArgumentException("arg1");
    ProveedorTicketPK localProveedorTicketPK = (ProveedorTicketPK)paramObject;
    this.idProveedorTicket = localProveedorTicketPK.idProveedorTicket;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProveedorTicketPK))
      throw new IllegalArgumentException("arg2");
    ProveedorTicketPK localProveedorTicketPK = (ProveedorTicketPK)paramObject;
    localProveedorTicketPK.idProveedorTicket = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProveedorTicketPK))
      throw new IllegalArgumentException("arg2");
    ProveedorTicketPK localProveedorTicketPK = (ProveedorTicketPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localProveedorTicketPK.idProveedorTicket);
  }

  private static final String jdoGetcodProveedorTicket(ProveedorTicket paramProveedorTicket)
  {
    if (paramProveedorTicket.jdoFlags <= 0)
      return paramProveedorTicket.codProveedorTicket;
    StateManager localStateManager = paramProveedorTicket.jdoStateManager;
    if (localStateManager == null)
      return paramProveedorTicket.codProveedorTicket;
    if (localStateManager.isLoaded(paramProveedorTicket, jdoInheritedFieldCount + 0))
      return paramProveedorTicket.codProveedorTicket;
    return localStateManager.getStringField(paramProveedorTicket, jdoInheritedFieldCount + 0, paramProveedorTicket.codProveedorTicket);
  }

  private static final void jdoSetcodProveedorTicket(ProveedorTicket paramProveedorTicket, String paramString)
  {
    if (paramProveedorTicket.jdoFlags == 0)
    {
      paramProveedorTicket.codProveedorTicket = paramString;
      return;
    }
    StateManager localStateManager = paramProveedorTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorTicket.codProveedorTicket = paramString;
      return;
    }
    localStateManager.setStringField(paramProveedorTicket, jdoInheritedFieldCount + 0, paramProveedorTicket.codProveedorTicket, paramString);
  }

  private static final String jdoGetdireccion(ProveedorTicket paramProveedorTicket)
  {
    if (paramProveedorTicket.jdoFlags <= 0)
      return paramProveedorTicket.direccion;
    StateManager localStateManager = paramProveedorTicket.jdoStateManager;
    if (localStateManager == null)
      return paramProveedorTicket.direccion;
    if (localStateManager.isLoaded(paramProveedorTicket, jdoInheritedFieldCount + 1))
      return paramProveedorTicket.direccion;
    return localStateManager.getStringField(paramProveedorTicket, jdoInheritedFieldCount + 1, paramProveedorTicket.direccion);
  }

  private static final void jdoSetdireccion(ProveedorTicket paramProveedorTicket, String paramString)
  {
    if (paramProveedorTicket.jdoFlags == 0)
    {
      paramProveedorTicket.direccion = paramString;
      return;
    }
    StateManager localStateManager = paramProveedorTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorTicket.direccion = paramString;
      return;
    }
    localStateManager.setStringField(paramProveedorTicket, jdoInheritedFieldCount + 1, paramProveedorTicket.direccion, paramString);
  }

  private static final long jdoGetidProveedorTicket(ProveedorTicket paramProveedorTicket)
  {
    return paramProveedorTicket.idProveedorTicket;
  }

  private static final void jdoSetidProveedorTicket(ProveedorTicket paramProveedorTicket, long paramLong)
  {
    StateManager localStateManager = paramProveedorTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorTicket.idProveedorTicket = paramLong;
      return;
    }
    localStateManager.setLongField(paramProveedorTicket, jdoInheritedFieldCount + 2, paramProveedorTicket.idProveedorTicket, paramLong);
  }

  private static final String jdoGetnombre(ProveedorTicket paramProveedorTicket)
  {
    if (paramProveedorTicket.jdoFlags <= 0)
      return paramProveedorTicket.nombre;
    StateManager localStateManager = paramProveedorTicket.jdoStateManager;
    if (localStateManager == null)
      return paramProveedorTicket.nombre;
    if (localStateManager.isLoaded(paramProveedorTicket, jdoInheritedFieldCount + 3))
      return paramProveedorTicket.nombre;
    return localStateManager.getStringField(paramProveedorTicket, jdoInheritedFieldCount + 3, paramProveedorTicket.nombre);
  }

  private static final void jdoSetnombre(ProveedorTicket paramProveedorTicket, String paramString)
  {
    if (paramProveedorTicket.jdoFlags == 0)
    {
      paramProveedorTicket.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramProveedorTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorTicket.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramProveedorTicket, jdoInheritedFieldCount + 3, paramProveedorTicket.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}