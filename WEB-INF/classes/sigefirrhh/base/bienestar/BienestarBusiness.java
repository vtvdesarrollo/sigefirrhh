package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class BienestarBusiness extends AbstractBusiness
  implements Serializable
{
  private DenominacionTicketBeanBusiness denominacionTicketBeanBusiness = new DenominacionTicketBeanBusiness();

  private EstablecimientoSaludBeanBusiness establecimientoSaludBeanBusiness = new EstablecimientoSaludBeanBusiness();

  private NivelBecaBeanBusiness nivelBecaBeanBusiness = new NivelBecaBeanBusiness();

  private ParametroBecaBeanBusiness parametroBecaBeanBusiness = new ParametroBecaBeanBusiness();

  private ParametroBecaGeneralBeanBusiness parametroBecaGeneralBeanBusiness = new ParametroBecaGeneralBeanBusiness();

  private ParametroGuarderiaBeanBusiness parametroGuarderiaBeanBusiness = new ParametroGuarderiaBeanBusiness();

  private ParametroJugueteBeanBusiness parametroJugueteBeanBusiness = new ParametroJugueteBeanBusiness();

  private ParametroTicketBeanBusiness parametroTicketBeanBusiness = new ParametroTicketBeanBusiness();

  private ParametroUtilesBeanBusiness parametroUtilesBeanBusiness = new ParametroUtilesBeanBusiness();

  private PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

  private PolizaBeanBusiness polizaBeanBusiness = new PolizaBeanBusiness();

  private PrimasPlanBeanBusiness primasPlanBeanBusiness = new PrimasPlanBeanBusiness();

  private ProveedorTicketBeanBusiness proveedorTicketBeanBusiness = new ProveedorTicketBeanBusiness();

  private SubtipoDotacionBeanBusiness subtipoDotacionBeanBusiness = new SubtipoDotacionBeanBusiness();

  private TipoDotacionBeanBusiness tipoDotacionBeanBusiness = new TipoDotacionBeanBusiness();

  private TipoSiniestroBeanBusiness tipoSiniestroBeanBusiness = new TipoSiniestroBeanBusiness();

  private TipoCredencialBeanBusiness tipoCredencialBeanBusiness = new TipoCredencialBeanBusiness();

  private SubtipoCredencialBeanBusiness subtipoCredencialBeanBusiness = new SubtipoCredencialBeanBusiness();

  public void addDenominacionTicket(DenominacionTicket denominacionTicket)
    throws Exception
  {
    this.denominacionTicketBeanBusiness.addDenominacionTicket(denominacionTicket);
  }

  public void updateDenominacionTicket(DenominacionTicket denominacionTicket) throws Exception {
    this.denominacionTicketBeanBusiness.updateDenominacionTicket(denominacionTicket);
  }

  public void deleteDenominacionTicket(DenominacionTicket denominacionTicket) throws Exception {
    this.denominacionTicketBeanBusiness.deleteDenominacionTicket(denominacionTicket);
  }

  public DenominacionTicket findDenominacionTicketById(long denominacionTicketId) throws Exception {
    return this.denominacionTicketBeanBusiness.findDenominacionTicketById(denominacionTicketId);
  }

  public Collection findAllDenominacionTicket() throws Exception {
    return this.denominacionTicketBeanBusiness.findDenominacionTicketAll();
  }

  public Collection findDenominacionTicketByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.denominacionTicketBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addEstablecimientoSalud(EstablecimientoSalud establecimientoSalud)
    throws Exception
  {
    this.establecimientoSaludBeanBusiness.addEstablecimientoSalud(establecimientoSalud);
  }

  public void updateEstablecimientoSalud(EstablecimientoSalud establecimientoSalud) throws Exception {
    this.establecimientoSaludBeanBusiness.updateEstablecimientoSalud(establecimientoSalud);
  }

  public void deleteEstablecimientoSalud(EstablecimientoSalud establecimientoSalud) throws Exception {
    this.establecimientoSaludBeanBusiness.deleteEstablecimientoSalud(establecimientoSalud);
  }

  public EstablecimientoSalud findEstablecimientoSaludById(long establecimientoSaludId) throws Exception {
    return this.establecimientoSaludBeanBusiness.findEstablecimientoSaludById(establecimientoSaludId);
  }

  public Collection findAllEstablecimientoSalud() throws Exception {
    return this.establecimientoSaludBeanBusiness.findEstablecimientoSaludAll();
  }

  public Collection findEstablecimientoSaludByCodEstablecimientoSalud(String codEstablecimientoSalud)
    throws Exception
  {
    return this.establecimientoSaludBeanBusiness.findByCodEstablecimientoSalud(codEstablecimientoSalud);
  }

  public Collection findEstablecimientoSaludByNombre(String nombre)
    throws Exception
  {
    return this.establecimientoSaludBeanBusiness.findByNombre(nombre);
  }

  public void addNivelBeca(NivelBeca nivelBeca)
    throws Exception
  {
    this.nivelBecaBeanBusiness.addNivelBeca(nivelBeca);
  }

  public void updateNivelBeca(NivelBeca nivelBeca) throws Exception {
    this.nivelBecaBeanBusiness.updateNivelBeca(nivelBeca);
  }

  public void deleteNivelBeca(NivelBeca nivelBeca) throws Exception {
    this.nivelBecaBeanBusiness.deleteNivelBeca(nivelBeca);
  }

  public NivelBeca findNivelBecaById(long nivelBecaId) throws Exception {
    return this.nivelBecaBeanBusiness.findNivelBecaById(nivelBecaId);
  }

  public Collection findAllNivelBeca() throws Exception {
    return this.nivelBecaBeanBusiness.findNivelBecaAll();
  }

  public Collection findNivelBecaByNivelEducativo(String nivelEducativo)
    throws Exception
  {
    return this.nivelBecaBeanBusiness.findByNivelEducativo(nivelEducativo);
  }

  public void addParametroBeca(ParametroBeca parametroBeca)
    throws Exception
  {
    this.parametroBecaBeanBusiness.addParametroBeca(parametroBeca);
  }

  public void updateParametroBeca(ParametroBeca parametroBeca) throws Exception {
    this.parametroBecaBeanBusiness.updateParametroBeca(parametroBeca);
  }

  public void deleteParametroBeca(ParametroBeca parametroBeca) throws Exception {
    this.parametroBecaBeanBusiness.deleteParametroBeca(parametroBeca);
  }

  public ParametroBeca findParametroBecaById(long parametroBecaId) throws Exception {
    return this.parametroBecaBeanBusiness.findParametroBecaById(parametroBecaId);
  }

  public Collection findAllParametroBeca() throws Exception {
    return this.parametroBecaBeanBusiness.findParametroBecaAll();
  }

  public Collection findParametroBecaByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.parametroBecaBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addParametroBecaGeneral(ParametroBecaGeneral parametroBecaGeneral)
    throws Exception
  {
    this.parametroBecaGeneralBeanBusiness.addParametroBecaGeneral(parametroBecaGeneral);
  }

  public void updateParametroBecaGeneral(ParametroBecaGeneral parametroBecaGeneral) throws Exception {
    this.parametroBecaGeneralBeanBusiness.updateParametroBecaGeneral(parametroBecaGeneral);
  }

  public void deleteParametroBecaGeneral(ParametroBecaGeneral parametroBecaGeneral) throws Exception {
    this.parametroBecaGeneralBeanBusiness.deleteParametroBecaGeneral(parametroBecaGeneral);
  }

  public ParametroBecaGeneral findParametroBecaGeneralById(long parametroBecaGeneralId) throws Exception {
    return this.parametroBecaGeneralBeanBusiness.findParametroBecaGeneralById(parametroBecaGeneralId);
  }

  public Collection findAllParametroBecaGeneral() throws Exception {
    return this.parametroBecaGeneralBeanBusiness.findParametroBecaGeneralAll();
  }

  public Collection findParametroBecaGeneralByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.parametroBecaGeneralBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addParametroGuarderia(ParametroGuarderia parametroGuarderia)
    throws Exception
  {
    this.parametroGuarderiaBeanBusiness.addParametroGuarderia(parametroGuarderia);
  }

  public void updateParametroGuarderia(ParametroGuarderia parametroGuarderia) throws Exception {
    this.parametroGuarderiaBeanBusiness.updateParametroGuarderia(parametroGuarderia);
  }

  public void deleteParametroGuarderia(ParametroGuarderia parametroGuarderia) throws Exception {
    this.parametroGuarderiaBeanBusiness.deleteParametroGuarderia(parametroGuarderia);
  }

  public ParametroGuarderia findParametroGuarderiaById(long parametroGuarderiaId) throws Exception {
    return this.parametroGuarderiaBeanBusiness.findParametroGuarderiaById(parametroGuarderiaId);
  }

  public Collection findAllParametroGuarderia() throws Exception {
    return this.parametroGuarderiaBeanBusiness.findParametroGuarderiaAll();
  }

  public Collection findParametroGuarderiaByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.parametroGuarderiaBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addParametroJuguete(ParametroJuguete parametroJuguete)
    throws Exception
  {
    this.parametroJugueteBeanBusiness.addParametroJuguete(parametroJuguete);
  }

  public void updateParametroJuguete(ParametroJuguete parametroJuguete) throws Exception {
    this.parametroJugueteBeanBusiness.updateParametroJuguete(parametroJuguete);
  }

  public void deleteParametroJuguete(ParametroJuguete parametroJuguete) throws Exception {
    this.parametroJugueteBeanBusiness.deleteParametroJuguete(parametroJuguete);
  }

  public ParametroJuguete findParametroJugueteById(long parametroJugueteId) throws Exception {
    return this.parametroJugueteBeanBusiness.findParametroJugueteById(parametroJugueteId);
  }

  public Collection findAllParametroJuguete() throws Exception {
    return this.parametroJugueteBeanBusiness.findParametroJugueteAll();
  }

  public Collection findParametroJugueteByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.parametroJugueteBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addParametroTicket(ParametroTicket parametroTicket)
    throws Exception
  {
    this.parametroTicketBeanBusiness.addParametroTicket(parametroTicket);
  }

  public void updateParametroTicket(ParametroTicket parametroTicket) throws Exception {
    this.parametroTicketBeanBusiness.updateParametroTicket(parametroTicket);
  }

  public void deleteParametroTicket(ParametroTicket parametroTicket) throws Exception {
    this.parametroTicketBeanBusiness.deleteParametroTicket(parametroTicket);
  }

  public ParametroTicket findParametroTicketById(long parametroTicketId) throws Exception {
    return this.parametroTicketBeanBusiness.findParametroTicketById(parametroTicketId);
  }

  public Collection findAllParametroTicket() throws Exception {
    return this.parametroTicketBeanBusiness.findParametroTicketAll();
  }

  public Collection findParametroTicketByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.parametroTicketBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addParametroUtiles(ParametroUtiles parametroUtiles)
    throws Exception
  {
    this.parametroUtilesBeanBusiness.addParametroUtiles(parametroUtiles);
  }

  public void updateParametroUtiles(ParametroUtiles parametroUtiles) throws Exception {
    this.parametroUtilesBeanBusiness.updateParametroUtiles(parametroUtiles);
  }

  public void deleteParametroUtiles(ParametroUtiles parametroUtiles) throws Exception {
    this.parametroUtilesBeanBusiness.deleteParametroUtiles(parametroUtiles);
  }

  public ParametroUtiles findParametroUtilesById(long parametroUtilesId) throws Exception {
    return this.parametroUtilesBeanBusiness.findParametroUtilesById(parametroUtilesId);
  }

  public Collection findAllParametroUtiles() throws Exception {
    return this.parametroUtilesBeanBusiness.findParametroUtilesAll();
  }

  public Collection findParametroUtilesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.parametroUtilesBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addPlanPoliza(PlanPoliza planPoliza)
    throws Exception
  {
    this.planPolizaBeanBusiness.addPlanPoliza(planPoliza);
  }

  public void updatePlanPoliza(PlanPoliza planPoliza) throws Exception {
    this.planPolizaBeanBusiness.updatePlanPoliza(planPoliza);
  }

  public void deletePlanPoliza(PlanPoliza planPoliza) throws Exception {
    this.planPolizaBeanBusiness.deletePlanPoliza(planPoliza);
  }

  public PlanPoliza findPlanPolizaById(long planPolizaId) throws Exception {
    return this.planPolizaBeanBusiness.findPlanPolizaById(planPolizaId);
  }

  public Collection findAllPlanPoliza() throws Exception {
    return this.planPolizaBeanBusiness.findPlanPolizaAll();
  }

  public Collection findPlanPolizaByPoliza(long idPoliza)
    throws Exception
  {
    return this.planPolizaBeanBusiness.findByPoliza(idPoliza);
  }

  public Collection findPlanPolizaByCodPlanPoliza(String codPlanPoliza)
    throws Exception
  {
    return this.planPolizaBeanBusiness.findByCodPlanPoliza(codPlanPoliza);
  }

  public Collection findPlanPolizaByNombre(String nombre)
    throws Exception
  {
    return this.planPolizaBeanBusiness.findByNombre(nombre);
  }

  public void addPoliza(Poliza poliza)
    throws Exception
  {
    this.polizaBeanBusiness.addPoliza(poliza);
  }

  public void updatePoliza(Poliza poliza) throws Exception {
    this.polizaBeanBusiness.updatePoliza(poliza);
  }

  public void deletePoliza(Poliza poliza) throws Exception {
    this.polizaBeanBusiness.deletePoliza(poliza);
  }

  public Poliza findPolizaById(long polizaId) throws Exception {
    return this.polizaBeanBusiness.findPolizaById(polizaId);
  }

  public Collection findAllPoliza() throws Exception {
    return this.polizaBeanBusiness.findPolizaAll();
  }

  public Collection findPolizaByCodPoliza(String codPoliza, long idOrganismo)
    throws Exception
  {
    return this.polizaBeanBusiness.findByCodPoliza(codPoliza, idOrganismo);
  }

  public Collection findPolizaByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.polizaBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findPolizaByVigente(String vigente, long idOrganismo)
    throws Exception
  {
    return this.polizaBeanBusiness.findByVigente(vigente, idOrganismo);
  }

  public Collection findPolizaByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.polizaBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addPrimasPlan(PrimasPlan primasPlan)
    throws Exception
  {
    this.primasPlanBeanBusiness.addPrimasPlan(primasPlan);
  }

  public void updatePrimasPlan(PrimasPlan primasPlan) throws Exception {
    this.primasPlanBeanBusiness.updatePrimasPlan(primasPlan);
  }

  public void deletePrimasPlan(PrimasPlan primasPlan) throws Exception {
    this.primasPlanBeanBusiness.deletePrimasPlan(primasPlan);
  }

  public PrimasPlan findPrimasPlanById(long primasPlanId) throws Exception {
    return this.primasPlanBeanBusiness.findPrimasPlanById(primasPlanId);
  }

  public Collection findAllPrimasPlan() throws Exception {
    return this.primasPlanBeanBusiness.findPrimasPlanAll();
  }

  public Collection findPrimasPlanByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    return this.primasPlanBeanBusiness.findByPlanPoliza(idPlanPoliza);
  }

  public Collection findPrimasPlanByParentesco(String parentesco)
    throws Exception
  {
    return this.primasPlanBeanBusiness.findByParentesco(parentesco);
  }

  public void addProveedorTicket(ProveedorTicket proveedorTicket)
    throws Exception
  {
    this.proveedorTicketBeanBusiness.addProveedorTicket(proveedorTicket);
  }

  public void updateProveedorTicket(ProveedorTicket proveedorTicket) throws Exception {
    this.proveedorTicketBeanBusiness.updateProveedorTicket(proveedorTicket);
  }

  public void deleteProveedorTicket(ProveedorTicket proveedorTicket) throws Exception {
    this.proveedorTicketBeanBusiness.deleteProveedorTicket(proveedorTicket);
  }

  public ProveedorTicket findProveedorTicketById(long proveedorTicketId) throws Exception {
    return this.proveedorTicketBeanBusiness.findProveedorTicketById(proveedorTicketId);
  }

  public Collection findAllProveedorTicket() throws Exception {
    return this.proveedorTicketBeanBusiness.findProveedorTicketAll();
  }

  public Collection findProveedorTicketByCodProveedorTicket(String codProveedorTicket)
    throws Exception
  {
    return this.proveedorTicketBeanBusiness.findByCodProveedorTicket(codProveedorTicket);
  }

  public Collection findProveedorTicketByNombre(String nombre)
    throws Exception
  {
    return this.proveedorTicketBeanBusiness.findByNombre(nombre);
  }

  public void addSubtipoDotacion(SubtipoDotacion subtipoDotacion)
    throws Exception
  {
    this.subtipoDotacionBeanBusiness.addSubtipoDotacion(subtipoDotacion);
  }

  public void updateSubtipoDotacion(SubtipoDotacion subtipoDotacion) throws Exception {
    this.subtipoDotacionBeanBusiness.updateSubtipoDotacion(subtipoDotacion);
  }

  public void deleteSubtipoDotacion(SubtipoDotacion subtipoDotacion) throws Exception {
    this.subtipoDotacionBeanBusiness.deleteSubtipoDotacion(subtipoDotacion);
  }

  public SubtipoDotacion findSubtipoDotacionById(long subtipoDotacionId) throws Exception {
    return this.subtipoDotacionBeanBusiness.findSubtipoDotacionById(subtipoDotacionId);
  }

  public Collection findAllSubtipoDotacion() throws Exception {
    return this.subtipoDotacionBeanBusiness.findSubtipoDotacionAll();
  }

  public Collection findSubtipoDotacionByTipoDotacion(long idTipoDotacion)
    throws Exception
  {
    return this.subtipoDotacionBeanBusiness.findByTipoDotacion(idTipoDotacion);
  }

  public void addTipoDotacion(TipoDotacion tipoDotacion)
    throws Exception
  {
    this.tipoDotacionBeanBusiness.addTipoDotacion(tipoDotacion);
  }

  public void updateTipoDotacion(TipoDotacion tipoDotacion) throws Exception {
    this.tipoDotacionBeanBusiness.updateTipoDotacion(tipoDotacion);
  }

  public void deleteTipoDotacion(TipoDotacion tipoDotacion) throws Exception {
    this.tipoDotacionBeanBusiness.deleteTipoDotacion(tipoDotacion);
  }

  public TipoDotacion findTipoDotacionById(long tipoDotacionId) throws Exception {
    return this.tipoDotacionBeanBusiness.findTipoDotacionById(tipoDotacionId);
  }

  public Collection findAllTipoDotacion() throws Exception {
    return this.tipoDotacionBeanBusiness.findTipoDotacionAll();
  }

  public Collection findTipoDotacionByCodTipoDotacion(String codTipoDotacion)
    throws Exception
  {
    return this.tipoDotacionBeanBusiness.findByCodTipoDotacion(codTipoDotacion);
  }

  public Collection findTipoDotacionByNombre(String nombre)
    throws Exception
  {
    return this.tipoDotacionBeanBusiness.findByNombre(nombre);
  }

  public void addTipoSiniestro(TipoSiniestro tipoSiniestro)
    throws Exception
  {
    this.tipoSiniestroBeanBusiness.addTipoSiniestro(tipoSiniestro);
  }

  public void updateTipoSiniestro(TipoSiniestro tipoSiniestro) throws Exception {
    this.tipoSiniestroBeanBusiness.updateTipoSiniestro(tipoSiniestro);
  }

  public void deleteTipoSiniestro(TipoSiniestro tipoSiniestro) throws Exception {
    this.tipoSiniestroBeanBusiness.deleteTipoSiniestro(tipoSiniestro);
  }

  public TipoSiniestro findTipoSiniestroById(long tipoSiniestroId) throws Exception {
    return this.tipoSiniestroBeanBusiness.findTipoSiniestroById(tipoSiniestroId);
  }

  public Collection findAllTipoSiniestro() throws Exception {
    return this.tipoSiniestroBeanBusiness.findTipoSiniestroAll();
  }

  public Collection findTipoSiniestroByCodTipoSiniestro(String codTipoSiniestro)
    throws Exception
  {
    return this.tipoSiniestroBeanBusiness.findByCodTipoSiniestro(codTipoSiniestro);
  }

  public Collection findTipoSiniestroByNombre(String nombre)
    throws Exception
  {
    return this.tipoSiniestroBeanBusiness.findByNombre(nombre);
  }

  public void addTipoCredencial(TipoCredencial tipoCredencial) throws Exception
  {
    this.tipoCredencialBeanBusiness.addTipoCredencial(tipoCredencial);
  }

  public void updateTipoCredencial(TipoCredencial tipoCredencial) throws Exception {
    this.tipoCredencialBeanBusiness.updateTipoCredencial(tipoCredencial);
  }

  public void deleteTipoCredencial(TipoCredencial tipoCredencial) throws Exception {
    this.tipoCredencialBeanBusiness.deleteTipoCredencial(tipoCredencial);
  }

  public TipoCredencial findTipoCredencialById(long tipoCredencialId) throws Exception {
    return this.tipoCredencialBeanBusiness.findTipoCredencialById(tipoCredencialId);
  }

  public Collection findAllTipoCredencial() throws Exception {
    return this.tipoCredencialBeanBusiness.findTipoCredencialAll();
  }

  public Collection findTipoCredencialByCodTipoCredencial(String codTipoCredencial)
    throws Exception
  {
    return this.tipoCredencialBeanBusiness.findByCodTipoCredencial(codTipoCredencial);
  }

  public Collection findTipoCredencialByNombre(String nombre)
    throws Exception
  {
    return this.tipoCredencialBeanBusiness.findByNombre(nombre);
  }

  public void addSubtipoCredencial(SubtipoCredencial subtipoCredencial) throws Exception
  {
    this.subtipoCredencialBeanBusiness.addSubtipoCredencial(subtipoCredencial);
  }

  public void updateSubtipoCredencial(SubtipoCredencial subtipoCredencial) throws Exception {
    this.subtipoCredencialBeanBusiness.updateSubtipoCredencial(subtipoCredencial);
  }

  public void deleteSubtipoCredencial(SubtipoCredencial subtipoCredencial) throws Exception {
    this.subtipoCredencialBeanBusiness.deleteSubtipoCredencial(subtipoCredencial);
  }

  public SubtipoCredencial findSubtipoCredencialById(long subtipoCredencialId) throws Exception {
    return this.subtipoCredencialBeanBusiness.findSubtipoCredencialById(subtipoCredencialId);
  }

  public Collection findAllSubtipoCredencial() throws Exception {
    return this.subtipoCredencialBeanBusiness.findSubtipoCredencialAll();
  }

  public Collection findSubtipoCredencialByTipoCredencial(long idTipoCredencial)
    throws Exception
  {
    return this.subtipoCredencialBeanBusiness.findByTipoCredencial(idTipoCredencial);
  }
}