package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class ProveedorTicketPK
  implements Serializable
{
  public long idProveedorTicket;

  public ProveedorTicketPK()
  {
  }

  public ProveedorTicketPK(long idProveedorTicket)
  {
    this.idProveedorTicket = idProveedorTicket;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ProveedorTicketPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ProveedorTicketPK thatPK)
  {
    return 
      this.idProveedorTicket == thatPK.idProveedorTicket;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idProveedorTicket)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idProveedorTicket);
  }
}