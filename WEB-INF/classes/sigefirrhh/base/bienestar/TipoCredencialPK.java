package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class TipoCredencialPK
  implements Serializable
{
  public long idTipoCredencial;

  public TipoCredencialPK()
  {
  }

  public TipoCredencialPK(long idTipoCredencial)
  {
    this.idTipoCredencial = idTipoCredencial;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoCredencialPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoCredencialPK thatPK)
  {
    return 
      this.idTipoCredencial == thatPK.idTipoCredencial;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoCredencial)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoCredencial);
  }
}