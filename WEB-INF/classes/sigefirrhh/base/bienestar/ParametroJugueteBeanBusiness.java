package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ParametroJugueteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParametroJuguete(ParametroJuguete parametroJuguete)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ParametroJuguete parametroJugueteNew = 
      (ParametroJuguete)BeanUtils.cloneBean(
      parametroJuguete);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroJugueteNew.getTipoPersonal() != null) {
      parametroJugueteNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroJugueteNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(parametroJugueteNew);
  }

  public void updateParametroJuguete(ParametroJuguete parametroJuguete) throws Exception
  {
    ParametroJuguete parametroJugueteModify = 
      findParametroJugueteById(parametroJuguete.getIdParametroJuguete());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroJuguete.getTipoPersonal() != null) {
      parametroJuguete.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroJuguete.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(parametroJugueteModify, parametroJuguete);
  }

  public void deleteParametroJuguete(ParametroJuguete parametroJuguete) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ParametroJuguete parametroJugueteDelete = 
      findParametroJugueteById(parametroJuguete.getIdParametroJuguete());
    pm.deletePersistent(parametroJugueteDelete);
  }

  public ParametroJuguete findParametroJugueteById(long idParametroJuguete) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParametroJuguete == pIdParametroJuguete";
    Query query = pm.newQuery(ParametroJuguete.class, filter);

    query.declareParameters("long pIdParametroJuguete");

    parameters.put("pIdParametroJuguete", new Long(idParametroJuguete));

    Collection colParametroJuguete = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParametroJuguete.iterator();
    return (ParametroJuguete)iterator.next();
  }

  public Collection findParametroJugueteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parametroJugueteExtent = pm.getExtent(
      ParametroJuguete.class, true);
    Query query = pm.newQuery(parametroJugueteExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ParametroJuguete.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colParametroJuguete = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParametroJuguete);

    return colParametroJuguete;
  }
}