package sigefirrhh.base.bienestar;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class TipoDotacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TipoDotacionForm.class.getName());
  private TipoDotacion tipoDotacion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private boolean showTipoDotacionByCodTipoDotacion;
  private boolean showTipoDotacionByNombre;
  private String findCodTipoDotacion;
  private String findNombre;
  private Object stateResultTipoDotacionByCodTipoDotacion = null;

  private Object stateResultTipoDotacionByNombre = null;

  public String getFindCodTipoDotacion()
  {
    return this.findCodTipoDotacion;
  }
  public void setFindCodTipoDotacion(String findCodTipoDotacion) {
    this.findCodTipoDotacion = findCodTipoDotacion;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public TipoDotacion getTipoDotacion() {
    if (this.tipoDotacion == null) {
      this.tipoDotacion = new TipoDotacion();
    }
    return this.tipoDotacion;
  }

  public TipoDotacionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findTipoDotacionByCodTipoDotacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findTipoDotacionByCodTipoDotacion(this.findCodTipoDotacion);
      this.showTipoDotacionByCodTipoDotacion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoDotacionByCodTipoDotacion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoDotacion = null;
    this.findNombre = null;

    return null;
  }

  public String findTipoDotacionByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.bienestarFacade.findTipoDotacionByNombre(this.findNombre);
      this.showTipoDotacionByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoDotacionByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoDotacion = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowTipoDotacionByCodTipoDotacion() {
    return this.showTipoDotacionByCodTipoDotacion;
  }
  public boolean isShowTipoDotacionByNombre() {
    return this.showTipoDotacionByNombre;
  }

  public String selectTipoDotacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTipoDotacion = 
      Long.parseLong((String)requestParameterMap.get("idTipoDotacion"));
    try
    {
      this.tipoDotacion = 
        this.bienestarFacade.findTipoDotacionById(
        idTipoDotacion);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.tipoDotacion = null;
    this.showTipoDotacionByCodTipoDotacion = false;
    this.showTipoDotacionByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.bienestarFacade.addTipoDotacion(
          this.tipoDotacion);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.bienestarFacade.updateTipoDotacion(
          this.tipoDotacion);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.bienestarFacade.deleteTipoDotacion(
        this.tipoDotacion);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.tipoDotacion = new TipoDotacion();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.tipoDotacion.setIdTipoDotacion(identityGenerator.getNextSequenceNumber("sigefirrhh.base.bienestar.TipoDotacion"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.tipoDotacion = new TipoDotacion();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}