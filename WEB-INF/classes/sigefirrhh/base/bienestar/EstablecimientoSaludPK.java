package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class EstablecimientoSaludPK
  implements Serializable
{
  public long idEstablecimientoSalud;

  public EstablecimientoSaludPK()
  {
  }

  public EstablecimientoSaludPK(long idEstablecimientoSalud)
  {
    this.idEstablecimientoSalud = idEstablecimientoSalud;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EstablecimientoSaludPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EstablecimientoSaludPK thatPK)
  {
    return 
      this.idEstablecimientoSalud == thatPK.idEstablecimientoSalud;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEstablecimientoSalud)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEstablecimientoSalud);
  }
}