package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class PrimasPlanPK
  implements Serializable
{
  public long idPrimasPlan;

  public PrimasPlanPK()
  {
  }

  public PrimasPlanPK(long idPrimasPlan)
  {
    this.idPrimasPlan = idPrimasPlan;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PrimasPlanPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PrimasPlanPK thatPK)
  {
    return 
      this.idPrimasPlan == thatPK.idPrimasPlan;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPrimasPlan)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPrimasPlan);
  }
}