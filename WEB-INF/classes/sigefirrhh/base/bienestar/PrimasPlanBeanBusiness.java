package sigefirrhh.base.bienestar;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PrimasPlanBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPrimasPlan(PrimasPlan primasPlan)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PrimasPlan primasPlanNew = 
      (PrimasPlan)BeanUtils.cloneBean(
      primasPlan);

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (primasPlanNew.getPlanPoliza() != null) {
      primasPlanNew.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        primasPlanNew.getPlanPoliza().getIdPlanPoliza()));
    }
    pm.makePersistent(primasPlanNew);
  }

  public void updatePrimasPlan(PrimasPlan primasPlan) throws Exception
  {
    PrimasPlan primasPlanModify = 
      findPrimasPlanById(primasPlan.getIdPrimasPlan());

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (primasPlan.getPlanPoliza() != null) {
      primasPlan.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        primasPlan.getPlanPoliza().getIdPlanPoliza()));
    }

    BeanUtils.copyProperties(primasPlanModify, primasPlan);
  }

  public void deletePrimasPlan(PrimasPlan primasPlan) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PrimasPlan primasPlanDelete = 
      findPrimasPlanById(primasPlan.getIdPrimasPlan());
    pm.deletePersistent(primasPlanDelete);
  }

  public PrimasPlan findPrimasPlanById(long idPrimasPlan) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPrimasPlan == pIdPrimasPlan";
    Query query = pm.newQuery(PrimasPlan.class, filter);

    query.declareParameters("long pIdPrimasPlan");

    parameters.put("pIdPrimasPlan", new Long(idPrimasPlan));

    Collection colPrimasPlan = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPrimasPlan.iterator();
    return (PrimasPlan)iterator.next();
  }

  public Collection findPrimasPlanAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent primasPlanExtent = pm.getExtent(
      PrimasPlan.class, true);
    Query query = pm.newQuery(primasPlanExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPoliza.idPlanPoliza == pIdPlanPoliza";

    Query query = pm.newQuery(PrimasPlan.class, filter);

    query.declareParameters("long pIdPlanPoliza");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPoliza", new Long(idPlanPoliza));

    Collection colPrimasPlan = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrimasPlan);

    return colPrimasPlan;
  }

  public Collection findByParentesco(String parentesco)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "parentesco == pParentesco";

    Query query = pm.newQuery(PrimasPlan.class, filter);

    query.declareParameters("java.lang.String pParentesco");
    HashMap parameters = new HashMap();

    parameters.put("pParentesco", new String(parentesco));

    Collection colPrimasPlan = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrimasPlan);

    return colPrimasPlan;
  }
}