package sigefirrhh.base.bienestar;

import java.io.Serializable;

public class DenominacionTicketPK
  implements Serializable
{
  public long idDenominacionTicket;

  public DenominacionTicketPK()
  {
  }

  public DenominacionTicketPK(long idDenominacionTicket)
  {
    this.idDenominacionTicket = idDenominacionTicket;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DenominacionTicketPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DenominacionTicketPK thatPK)
  {
    return 
      this.idDenominacionTicket == thatPK.idDenominacionTicket;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDenominacionTicket)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDenominacionTicket);
  }
}