package sigefirrhh.base.registro;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class RegistroNoGenFacade extends RegistroFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private RegistroNoGenBusiness registroNoGenBusiness = new RegistroNoGenBusiness();

  public Collection findCausaPersonalByClasificacionPersonalAndCausaMovimiento(long idClasificacionPersonal, long idCausaMovimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroNoGenBusiness.findCausaPersonalByClasificacionPersonalAndCausaMovimiento(idClasificacionPersonal, idCausaMovimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCausaPersonalByClasificacionPersonalAndCausaMovimientoAndSujetoLefp(long idClasificacionPersonal, long idCausaMovimiento, String sujetoLefp) throws Exception
  {
    try { this.txn.open();
      return this.registroNoGenBusiness.findCausaPersonalByClasificacionPersonalAndCausaMovimientoAndSujetoLefp(idClasificacionPersonal, idCausaMovimiento, sujetoLefp);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCausaPersonalByClasificacionPersonalAndCausaMovimientoAndSujetoLefpAndGrado99(long idClasificacionPersonal, long idCausaMovimiento, String sujetoLefp, String grado99) throws Exception
  {
    try { this.txn.open();
      return this.registroNoGenBusiness.findCausaPersonalByClasificacionPersonalAndCausaMovimientoAndSujetoLefpAndGrado99(idClasificacionPersonal, idCausaMovimiento, sujetoLefp, grado99);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroByAprobacionMpd(long idOrganismo, String aprobacionMpd) throws Exception
  {
    try { this.txn.open();
      return this.registroNoGenBusiness.findRegistroByAprobacionMpd(idOrganismo, aprobacionMpd);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}