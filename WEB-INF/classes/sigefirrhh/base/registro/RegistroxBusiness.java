package sigefirrhh.base.registro;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class RegistroxBusiness extends AbstractBusiness
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

  private CausaPersonalBeanBusiness causaPersonalBeanBusiness = new CausaPersonalBeanBusiness();

  private MovimientoCargoBeanBusiness movimientoCargoBeanBusiness = new MovimientoCargoBeanBusiness();

  private MovimientoPersonalBeanBusiness movimientoPersonalBeanBusiness = new MovimientoPersonalBeanBusiness();

  private RegistroBeanBusiness registroBeanBusiness = new RegistroBeanBusiness();

  private RegistroPersonalBeanBusiness registroPersonalBeanBusiness = new RegistroPersonalBeanBusiness();

  public void addCausaMovimiento(CausaMovimiento causaMovimiento)
    throws Exception
  {
    this.causaMovimientoBeanBusiness.addCausaMovimiento(causaMovimiento);
  }

  public void updateCausaMovimiento(CausaMovimiento causaMovimiento) throws Exception {
    this.causaMovimientoBeanBusiness.updateCausaMovimiento(causaMovimiento);
  }

  public void deleteCausaMovimiento(CausaMovimiento causaMovimiento) throws Exception {
    this.causaMovimientoBeanBusiness.deleteCausaMovimiento(causaMovimiento);
  }

  public CausaMovimiento findCausaMovimientoById(long causaMovimientoId) throws Exception {
    return this.causaMovimientoBeanBusiness.findCausaMovimientoById(causaMovimientoId);
  }

  public Collection findAllCausaMovimiento() throws Exception {
    return this.causaMovimientoBeanBusiness.findCausaMovimientoAll();
  }

  public Collection findCausaMovimientoByMovimientoPersonal(long idMovimientoPersonal)
    throws Exception
  {
    return this.causaMovimientoBeanBusiness.findByMovimientoPersonal(idMovimientoPersonal);
  }

  public Collection findCausaMovimientoByCodCausaMovimiento(String codCausaMovimiento)
    throws Exception
  {
    return this.causaMovimientoBeanBusiness.findByCodCausaMovimiento(codCausaMovimiento);
  }

  public Collection findCausaMovimientoByDescripcion(String descripcion)
    throws Exception
  {
    return this.causaMovimientoBeanBusiness.findByDescripcion(descripcion);
  }

  public void addCausaPersonal(CausaPersonal causaPersonal) throws Exception
  {
    this.causaPersonalBeanBusiness.addCausaPersonal(causaPersonal);
  }

  public void updateCausaPersonal(CausaPersonal causaPersonal) throws Exception {
    this.causaPersonalBeanBusiness.updateCausaPersonal(causaPersonal);
  }

  public void deleteCausaPersonal(CausaPersonal causaPersonal) throws Exception {
    this.causaPersonalBeanBusiness.deleteCausaPersonal(causaPersonal);
  }

  public CausaPersonal findCausaPersonalById(long causaPersonalId) throws Exception {
    return this.causaPersonalBeanBusiness.findCausaPersonalById(causaPersonalId);
  }

  public Collection findAllCausaPersonal() throws Exception {
    return this.causaPersonalBeanBusiness.findCausaPersonalAll();
  }

  public Collection findCausaPersonalByClasificacionPersonal(long idClasificacionPersonal)
    throws Exception
  {
    return this.causaPersonalBeanBusiness.findByClasificacionPersonal(idClasificacionPersonal);
  }

  public Collection findCausaPersonalByCausaMovimiento(long idCausaMovimiento)
    throws Exception
  {
    return this.causaPersonalBeanBusiness.findByCausaMovimiento(idCausaMovimiento);
  }

  public void addMovimientoCargo(MovimientoCargo movimientoCargo) throws Exception
  {
    this.movimientoCargoBeanBusiness.addMovimientoCargo(movimientoCargo);
  }

  public void updateMovimientoCargo(MovimientoCargo movimientoCargo) throws Exception {
    this.movimientoCargoBeanBusiness.updateMovimientoCargo(movimientoCargo);
  }

  public void deleteMovimientoCargo(MovimientoCargo movimientoCargo) throws Exception {
    this.movimientoCargoBeanBusiness.deleteMovimientoCargo(movimientoCargo);
  }

  public MovimientoCargo findMovimientoCargoById(long movimientoCargoId) throws Exception {
    return this.movimientoCargoBeanBusiness.findMovimientoCargoById(movimientoCargoId);
  }

  public Collection findAllMovimientoCargo() throws Exception {
    return this.movimientoCargoBeanBusiness.findMovimientoCargoAll();
  }

  public Collection findMovimientoCargoByTipo(String tipo)
    throws Exception
  {
    return this.movimientoCargoBeanBusiness.findByTipo(tipo);
  }

  public void addMovimientoPersonal(MovimientoPersonal movimientoPersonal)
    throws Exception
  {
    this.movimientoPersonalBeanBusiness.addMovimientoPersonal(movimientoPersonal);
  }

  public void updateMovimientoPersonal(MovimientoPersonal movimientoPersonal) throws Exception {
    this.movimientoPersonalBeanBusiness.updateMovimientoPersonal(movimientoPersonal);
  }

  public void deleteMovimientoPersonal(MovimientoPersonal movimientoPersonal) throws Exception {
    this.movimientoPersonalBeanBusiness.deleteMovimientoPersonal(movimientoPersonal);
  }

  public MovimientoPersonal findMovimientoPersonalById(long movimientoPersonalId) throws Exception {
    return this.movimientoPersonalBeanBusiness.findMovimientoPersonalById(movimientoPersonalId);
  }

  public Collection findAllMovimientoPersonal() throws Exception {
    return this.movimientoPersonalBeanBusiness.findMovimientoPersonalAll();
  }

  public Collection findMovimientoPersonalByCodMovimientoPersonal(String codMovimientoPersonal)
    throws Exception
  {
    return this.movimientoPersonalBeanBusiness.findByCodMovimientoPersonal(codMovimientoPersonal);
  }

  public Collection findMovimientoPersonalByDescripcion(String descripcion)
    throws Exception
  {
    return this.movimientoPersonalBeanBusiness.findByDescripcion(descripcion);
  }

  public void addRegistro(Registro registro)
    throws Exception
  {
    this.registroBeanBusiness.addRegistro(registro);
  }

  public void updateRegistro(Registro registro) throws Exception {
    this.registroBeanBusiness.updateRegistro(registro);
  }

  public void deleteRegistro(Registro registro) throws Exception {
    this.registroBeanBusiness.deleteRegistro(registro);
  }

  public Registro findRegistroById(long registroId) throws Exception {
    return this.registroBeanBusiness.findRegistroById(registroId);
  }

  public Collection findAllRegistro() throws Exception {
    return this.registroBeanBusiness.findRegistroAll();
  }

  public Collection findRegistroByNumeroRegistro(int numeroRegistro, long idOrganismo)
    throws Exception
  {
    return this.registroBeanBusiness.findByNumeroRegistro(numeroRegistro, idOrganismo);
  }

  public Collection findRegistroByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.registroBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findRegistroByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.registroBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addRegistroPersonal(RegistroPersonal registroPersonal)
    throws Exception
  {
    this.registroPersonalBeanBusiness.addRegistroPersonal(registroPersonal);
  }

  public void updateRegistroPersonal(RegistroPersonal registroPersonal) throws Exception {
    this.registroPersonalBeanBusiness.updateRegistroPersonal(registroPersonal);
  }

  public void deleteRegistroPersonal(RegistroPersonal registroPersonal) throws Exception {
    this.registroPersonalBeanBusiness.deleteRegistroPersonal(registroPersonal);
  }

  public RegistroPersonal findRegistroPersonalById(long registroPersonalId) throws Exception {
    return this.registroPersonalBeanBusiness.findRegistroPersonalById(registroPersonalId);
  }

  public Collection findAllRegistroPersonal() throws Exception {
    return this.registroPersonalBeanBusiness.findRegistroPersonalAll();
  }

  public Collection findRegistroPersonalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.registroPersonalBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public Collection findRegistroPersonalByRegistro(long idRegistro)
    throws Exception
  {
    return this.registroPersonalBeanBusiness.findByRegistro(idRegistro);
  }
}