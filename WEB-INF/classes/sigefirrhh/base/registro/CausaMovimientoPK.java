package sigefirrhh.base.registro;

import java.io.Serializable;

public class CausaMovimientoPK
  implements Serializable
{
  public long idCausaMovimiento;

  public CausaMovimientoPK()
  {
  }

  public CausaMovimientoPK(long idCausaMovimiento)
  {
    this.idCausaMovimiento = idCausaMovimiento;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CausaMovimientoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CausaMovimientoPK thatPK)
  {
    return 
      this.idCausaMovimiento == thatPK.idCausaMovimiento;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCausaMovimiento)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCausaMovimiento);
  }
}