package sigefirrhh.base.registro;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ClasificacionPersonal;

public class CausaPersonal
  implements Serializable, PersistenceCapable
{
  private long idCausaPersonal;
  private ClasificacionPersonal clasificacionPersonal;
  private CausaMovimiento causaMovimiento;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "causaMovimiento", "clasificacionPersonal", "idCausaPersonal", "idSitp", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.registro.CausaMovimiento"), sunjdo$classForName$("sigefirrhh.base.definiciones.ClasificacionPersonal"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 26, 26, 24, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcausaMovimiento(this).getDescripcion();
  }

  public CausaMovimiento getCausaMovimiento()
  {
    return jdoGetcausaMovimiento(this);
  }

  public void setCausaMovimiento(CausaMovimiento causaMovimiento)
  {
    jdoSetcausaMovimiento(this, causaMovimiento);
  }

  public ClasificacionPersonal getClasificacionPersonal()
  {
    return jdoGetclasificacionPersonal(this);
  }

  public void setClasificacionPersonal(ClasificacionPersonal clasificacionPersonal)
  {
    jdoSetclasificacionPersonal(this, clasificacionPersonal);
  }

  public long getIdCausaPersonal()
  {
    return jdoGetidCausaPersonal(this);
  }

  public void setIdCausaPersonal(long idCausaPersonal)
  {
    jdoSetidCausaPersonal(this, idCausaPersonal);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.registro.CausaPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CausaPersonal());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CausaPersonal localCausaPersonal = new CausaPersonal();
    localCausaPersonal.jdoFlags = 1;
    localCausaPersonal.jdoStateManager = paramStateManager;
    return localCausaPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CausaPersonal localCausaPersonal = new CausaPersonal();
    localCausaPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCausaPersonal.jdoFlags = 1;
    localCausaPersonal.jdoStateManager = paramStateManager;
    return localCausaPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.causaMovimiento);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.clasificacionPersonal);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCausaPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaMovimiento = ((CausaMovimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.clasificacionPersonal = ((ClasificacionPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCausaPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CausaPersonal paramCausaPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCausaPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.causaMovimiento = paramCausaPersonal.causaMovimiento;
      return;
    case 1:
      if (paramCausaPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.clasificacionPersonal = paramCausaPersonal.clasificacionPersonal;
      return;
    case 2:
      if (paramCausaPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idCausaPersonal = paramCausaPersonal.idCausaPersonal;
      return;
    case 3:
      if (paramCausaPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramCausaPersonal.idSitp;
      return;
    case 4:
      if (paramCausaPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramCausaPersonal.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CausaPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CausaPersonal localCausaPersonal = (CausaPersonal)paramObject;
    if (localCausaPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCausaPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CausaPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CausaPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CausaPersonalPK))
      throw new IllegalArgumentException("arg1");
    CausaPersonalPK localCausaPersonalPK = (CausaPersonalPK)paramObject;
    localCausaPersonalPK.idCausaPersonal = this.idCausaPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CausaPersonalPK))
      throw new IllegalArgumentException("arg1");
    CausaPersonalPK localCausaPersonalPK = (CausaPersonalPK)paramObject;
    this.idCausaPersonal = localCausaPersonalPK.idCausaPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CausaPersonalPK))
      throw new IllegalArgumentException("arg2");
    CausaPersonalPK localCausaPersonalPK = (CausaPersonalPK)paramObject;
    localCausaPersonalPK.idCausaPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CausaPersonalPK))
      throw new IllegalArgumentException("arg2");
    CausaPersonalPK localCausaPersonalPK = (CausaPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localCausaPersonalPK.idCausaPersonal);
  }

  private static final CausaMovimiento jdoGetcausaMovimiento(CausaPersonal paramCausaPersonal)
  {
    StateManager localStateManager = paramCausaPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramCausaPersonal.causaMovimiento;
    if (localStateManager.isLoaded(paramCausaPersonal, jdoInheritedFieldCount + 0))
      return paramCausaPersonal.causaMovimiento;
    return (CausaMovimiento)localStateManager.getObjectField(paramCausaPersonal, jdoInheritedFieldCount + 0, paramCausaPersonal.causaMovimiento);
  }

  private static final void jdoSetcausaMovimiento(CausaPersonal paramCausaPersonal, CausaMovimiento paramCausaMovimiento)
  {
    StateManager localStateManager = paramCausaPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaPersonal.causaMovimiento = paramCausaMovimiento;
      return;
    }
    localStateManager.setObjectField(paramCausaPersonal, jdoInheritedFieldCount + 0, paramCausaPersonal.causaMovimiento, paramCausaMovimiento);
  }

  private static final ClasificacionPersonal jdoGetclasificacionPersonal(CausaPersonal paramCausaPersonal)
  {
    StateManager localStateManager = paramCausaPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramCausaPersonal.clasificacionPersonal;
    if (localStateManager.isLoaded(paramCausaPersonal, jdoInheritedFieldCount + 1))
      return paramCausaPersonal.clasificacionPersonal;
    return (ClasificacionPersonal)localStateManager.getObjectField(paramCausaPersonal, jdoInheritedFieldCount + 1, paramCausaPersonal.clasificacionPersonal);
  }

  private static final void jdoSetclasificacionPersonal(CausaPersonal paramCausaPersonal, ClasificacionPersonal paramClasificacionPersonal)
  {
    StateManager localStateManager = paramCausaPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaPersonal.clasificacionPersonal = paramClasificacionPersonal;
      return;
    }
    localStateManager.setObjectField(paramCausaPersonal, jdoInheritedFieldCount + 1, paramCausaPersonal.clasificacionPersonal, paramClasificacionPersonal);
  }

  private static final long jdoGetidCausaPersonal(CausaPersonal paramCausaPersonal)
  {
    return paramCausaPersonal.idCausaPersonal;
  }

  private static final void jdoSetidCausaPersonal(CausaPersonal paramCausaPersonal, long paramLong)
  {
    StateManager localStateManager = paramCausaPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaPersonal.idCausaPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramCausaPersonal, jdoInheritedFieldCount + 2, paramCausaPersonal.idCausaPersonal, paramLong);
  }

  private static final int jdoGetidSitp(CausaPersonal paramCausaPersonal)
  {
    if (paramCausaPersonal.jdoFlags <= 0)
      return paramCausaPersonal.idSitp;
    StateManager localStateManager = paramCausaPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramCausaPersonal.idSitp;
    if (localStateManager.isLoaded(paramCausaPersonal, jdoInheritedFieldCount + 3))
      return paramCausaPersonal.idSitp;
    return localStateManager.getIntField(paramCausaPersonal, jdoInheritedFieldCount + 3, paramCausaPersonal.idSitp);
  }

  private static final void jdoSetidSitp(CausaPersonal paramCausaPersonal, int paramInt)
  {
    if (paramCausaPersonal.jdoFlags == 0)
    {
      paramCausaPersonal.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramCausaPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaPersonal.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramCausaPersonal, jdoInheritedFieldCount + 3, paramCausaPersonal.idSitp, paramInt);
  }

  private static final Date jdoGettiempoSitp(CausaPersonal paramCausaPersonal)
  {
    if (paramCausaPersonal.jdoFlags <= 0)
      return paramCausaPersonal.tiempoSitp;
    StateManager localStateManager = paramCausaPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramCausaPersonal.tiempoSitp;
    if (localStateManager.isLoaded(paramCausaPersonal, jdoInheritedFieldCount + 4))
      return paramCausaPersonal.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramCausaPersonal, jdoInheritedFieldCount + 4, paramCausaPersonal.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(CausaPersonal paramCausaPersonal, Date paramDate)
  {
    if (paramCausaPersonal.jdoFlags == 0)
    {
      paramCausaPersonal.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramCausaPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaPersonal.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCausaPersonal, jdoInheritedFieldCount + 4, paramCausaPersonal.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}