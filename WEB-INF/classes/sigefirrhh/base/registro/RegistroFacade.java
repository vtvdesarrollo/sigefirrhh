package sigefirrhh.base.registro;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class RegistroFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();

  public void addCausaMovimiento(CausaMovimiento causaMovimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.registroBusiness.addCausaMovimiento(causaMovimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCausaMovimiento(CausaMovimiento causaMovimiento) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.updateCausaMovimiento(causaMovimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCausaMovimiento(CausaMovimiento causaMovimiento) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.deleteCausaMovimiento(causaMovimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CausaMovimiento findCausaMovimientoById(long causaMovimientoId) throws Exception
  {
    try { this.txn.open();
      CausaMovimiento causaMovimiento = 
        this.registroBusiness.findCausaMovimientoById(causaMovimientoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(causaMovimiento);
      return causaMovimiento;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCausaMovimiento() throws Exception
  {
    try { this.txn.open();
      return this.registroBusiness.findAllCausaMovimiento();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCausaMovimientoByMovimientoPersonal(long idMovimientoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findCausaMovimientoByMovimientoPersonal(idMovimientoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCausaMovimientoByCodCausaMovimiento(String codCausaMovimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findCausaMovimientoByCodCausaMovimiento(codCausaMovimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCausaMovimientoByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findCausaMovimientoByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCausaPersonal(CausaPersonal causaPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.registroBusiness.addCausaPersonal(causaPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCausaPersonal(CausaPersonal causaPersonal) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.updateCausaPersonal(causaPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCausaPersonal(CausaPersonal causaPersonal) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.deleteCausaPersonal(causaPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CausaPersonal findCausaPersonalById(long causaPersonalId) throws Exception
  {
    try { this.txn.open();
      CausaPersonal causaPersonal = 
        this.registroBusiness.findCausaPersonalById(causaPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(causaPersonal);
      return causaPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCausaPersonal() throws Exception
  {
    try { this.txn.open();
      return this.registroBusiness.findAllCausaPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCausaPersonalByClasificacionPersonal(long idClasificacionPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findCausaPersonalByClasificacionPersonal(idClasificacionPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCausaPersonalByCausaMovimiento(long idCausaMovimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findCausaPersonalByCausaMovimiento(idCausaMovimiento);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addMovimientoCargo(MovimientoCargo movimientoCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.registroBusiness.addMovimientoCargo(movimientoCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateMovimientoCargo(MovimientoCargo movimientoCargo) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.updateMovimientoCargo(movimientoCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteMovimientoCargo(MovimientoCargo movimientoCargo) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.deleteMovimientoCargo(movimientoCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public MovimientoCargo findMovimientoCargoById(long movimientoCargoId) throws Exception
  {
    try { this.txn.open();
      MovimientoCargo movimientoCargo = 
        this.registroBusiness.findMovimientoCargoById(movimientoCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(movimientoCargo);
      return movimientoCargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllMovimientoCargo() throws Exception
  {
    try { this.txn.open();
      return this.registroBusiness.findAllMovimientoCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMovimientoCargoByTipo(String tipo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findMovimientoCargoByTipo(tipo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addMovimientoPersonal(MovimientoPersonal movimientoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.registroBusiness.addMovimientoPersonal(movimientoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateMovimientoPersonal(MovimientoPersonal movimientoPersonal) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.updateMovimientoPersonal(movimientoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteMovimientoPersonal(MovimientoPersonal movimientoPersonal) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.deleteMovimientoPersonal(movimientoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public MovimientoPersonal findMovimientoPersonalById(long movimientoPersonalId) throws Exception
  {
    try { this.txn.open();
      MovimientoPersonal movimientoPersonal = 
        this.registroBusiness.findMovimientoPersonalById(movimientoPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(movimientoPersonal);
      return movimientoPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllMovimientoPersonal() throws Exception
  {
    try { this.txn.open();
      return this.registroBusiness.findAllMovimientoPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMovimientoPersonalByCodMovimientoPersonal(String codMovimientoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findMovimientoPersonalByCodMovimientoPersonal(codMovimientoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMovimientoPersonalByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findMovimientoPersonalByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRegistro(Registro registro)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.registroBusiness.addRegistro(registro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRegistro(Registro registro) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.updateRegistro(registro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRegistro(Registro registro) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.deleteRegistro(registro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Registro findRegistroById(long registroId) throws Exception
  {
    try { this.txn.open();
      Registro registro = 
        this.registroBusiness.findRegistroById(registroId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(registro);
      return registro;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRegistro() throws Exception
  {
    try { this.txn.open();
      return this.registroBusiness.findAllRegistro();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroByNumeroRegistro(int numeroRegistro, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findRegistroByNumeroRegistro(numeroRegistro, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findRegistroByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findRegistroByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRegistroPersonal(RegistroPersonal registroPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.registroBusiness.addRegistroPersonal(registroPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRegistroPersonal(RegistroPersonal registroPersonal) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.updateRegistroPersonal(registroPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRegistroPersonal(RegistroPersonal registroPersonal) throws Exception
  {
    try { this.txn.open();
      this.registroBusiness.deleteRegistroPersonal(registroPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RegistroPersonal findRegistroPersonalById(long registroPersonalId) throws Exception
  {
    try { this.txn.open();
      RegistroPersonal registroPersonal = 
        this.registroBusiness.findRegistroPersonalById(registroPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(registroPersonal);
      return registroPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRegistroPersonal() throws Exception
  {
    try { this.txn.open();
      return this.registroBusiness.findAllRegistroPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroPersonalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findRegistroPersonalByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroPersonalByRegistro(long idRegistro)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroBusiness.findRegistroPersonalByRegistro(idRegistro);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}