package sigefirrhh.base.registro;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class RegistroPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRegistroPersonal(RegistroPersonal registroPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RegistroPersonal registroPersonalNew = 
      (RegistroPersonal)BeanUtils.cloneBean(
      registroPersonal);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (registroPersonalNew.getTipoPersonal() != null) {
      registroPersonalNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        registroPersonalNew.getTipoPersonal().getIdTipoPersonal()));
    }

    RegistroBeanBusiness registroBeanBusiness = new RegistroBeanBusiness();

    if (registroPersonalNew.getRegistro() != null) {
      registroPersonalNew.setRegistro(
        registroBeanBusiness.findRegistroById(
        registroPersonalNew.getRegistro().getIdRegistro()));
    }
    pm.makePersistent(registroPersonalNew);
  }

  public void updateRegistroPersonal(RegistroPersonal registroPersonal) throws Exception
  {
    RegistroPersonal registroPersonalModify = 
      findRegistroPersonalById(registroPersonal.getIdRegistroPersonal());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (registroPersonal.getTipoPersonal() != null) {
      registroPersonal.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        registroPersonal.getTipoPersonal().getIdTipoPersonal()));
    }

    RegistroBeanBusiness registroBeanBusiness = new RegistroBeanBusiness();

    if (registroPersonal.getRegistro() != null) {
      registroPersonal.setRegistro(
        registroBeanBusiness.findRegistroById(
        registroPersonal.getRegistro().getIdRegistro()));
    }

    BeanUtils.copyProperties(registroPersonalModify, registroPersonal);
  }

  public void deleteRegistroPersonal(RegistroPersonal registroPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RegistroPersonal registroPersonalDelete = 
      findRegistroPersonalById(registroPersonal.getIdRegistroPersonal());
    pm.deletePersistent(registroPersonalDelete);
  }

  public RegistroPersonal findRegistroPersonalById(long idRegistroPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRegistroPersonal == pIdRegistroPersonal";
    Query query = pm.newQuery(RegistroPersonal.class, filter);

    query.declareParameters("long pIdRegistroPersonal");

    parameters.put("pIdRegistroPersonal", new Long(idRegistroPersonal));

    Collection colRegistroPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRegistroPersonal.iterator();
    return (RegistroPersonal)iterator.next();
  }

  public Collection findRegistroPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent registroPersonalExtent = pm.getExtent(
      RegistroPersonal.class, true);
    Query query = pm.newQuery(registroPersonalExtent);
    query.setOrdering("tipoPersonal.nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(RegistroPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("tipoPersonal.nombre ascending");

    Collection colRegistroPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroPersonal);

    return colRegistroPersonal;
  }

  public Collection findByRegistro(long idRegistro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registro.idRegistro == pIdRegistro";

    Query query = pm.newQuery(RegistroPersonal.class, filter);

    query.declareParameters("long pIdRegistro");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));

    query.setOrdering("tipoPersonal.nombre ascending");

    Collection colRegistroPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroPersonal);

    return colRegistroPersonal;
  }
}