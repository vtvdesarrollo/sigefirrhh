package sigefirrhh.base.registro;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.ClasificacionPersonalBeanBusiness;

public class CausaPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCausaPersonal(CausaPersonal causaPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CausaPersonal causaPersonalNew = 
      (CausaPersonal)BeanUtils.cloneBean(
      causaPersonal);

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (causaPersonalNew.getClasificacionPersonal() != null) {
      causaPersonalNew.setClasificacionPersonal(
        clasificacionPersonalBeanBusiness.findClasificacionPersonalById(
        causaPersonalNew.getClasificacionPersonal().getIdClasificacionPersonal()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (causaPersonalNew.getCausaMovimiento() != null) {
      causaPersonalNew.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        causaPersonalNew.getCausaMovimiento().getIdCausaMovimiento()));
    }
    pm.makePersistent(causaPersonalNew);
  }

  public void updateCausaPersonal(CausaPersonal causaPersonal) throws Exception
  {
    CausaPersonal causaPersonalModify = 
      findCausaPersonalById(causaPersonal.getIdCausaPersonal());

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (causaPersonal.getClasificacionPersonal() != null) {
      causaPersonal.setClasificacionPersonal(
        clasificacionPersonalBeanBusiness.findClasificacionPersonalById(
        causaPersonal.getClasificacionPersonal().getIdClasificacionPersonal()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (causaPersonal.getCausaMovimiento() != null) {
      causaPersonal.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        causaPersonal.getCausaMovimiento().getIdCausaMovimiento()));
    }

    BeanUtils.copyProperties(causaPersonalModify, causaPersonal);
  }

  public void deleteCausaPersonal(CausaPersonal causaPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CausaPersonal causaPersonalDelete = 
      findCausaPersonalById(causaPersonal.getIdCausaPersonal());
    pm.deletePersistent(causaPersonalDelete);
  }

  public CausaPersonal findCausaPersonalById(long idCausaPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCausaPersonal == pIdCausaPersonal";
    Query query = pm.newQuery(CausaPersonal.class, filter);

    query.declareParameters("long pIdCausaPersonal");

    parameters.put("pIdCausaPersonal", new Long(idCausaPersonal));

    Collection colCausaPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCausaPersonal.iterator();
    return (CausaPersonal)iterator.next();
  }

  public Collection findCausaPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent causaPersonalExtent = pm.getExtent(
      CausaPersonal.class, true);
    Query query = pm.newQuery(causaPersonalExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByClasificacionPersonal(long idClasificacionPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "clasificacionPersonal.idClasificacionPersonal == pIdClasificacionPersonal";

    Query query = pm.newQuery(CausaPersonal.class, filter);

    query.declareParameters("long pIdClasificacionPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdClasificacionPersonal", new Long(idClasificacionPersonal));

    Collection colCausaPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCausaPersonal);

    return colCausaPersonal;
  }

  public Collection findByCausaMovimiento(long idCausaMovimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "causaMovimiento.idCausaMovimiento == pIdCausaMovimiento";

    Query query = pm.newQuery(CausaPersonal.class, filter);

    query.declareParameters("long pIdCausaMovimiento");
    HashMap parameters = new HashMap();

    parameters.put("pIdCausaMovimiento", new Long(idCausaMovimiento));

    Collection colCausaPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCausaPersonal);

    return colCausaPersonal;
  }
}