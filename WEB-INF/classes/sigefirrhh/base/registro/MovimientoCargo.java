package sigefirrhh.base.registro;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class MovimientoCargo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  private long idMovimientoCargo;
  private String tipo;
  private String codMovimientoCargo;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codMovimientoCargo", "descripcion", "idMovimientoCargo", "tipo" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.registro.MovimientoCargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new MovimientoCargo());

    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_TIPO.put("1", "CREACION");
    LISTA_TIPO.put("2", "ELIMINACION");
    LISTA_TIPO.put("3", "CLASIFICACION");
    LISTA_TIPO.put("4", "CAMBIO CLASIFICACION");
    LISTA_TIPO.put("5", "TRASLADO");
  }

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodMovimientoCargo(this);
  }

  public String getCodMovimientoCargo() {
    return jdoGetcodMovimientoCargo(this);
  }

  public void setCodMovimientoCargo(String codMovimientoCargo) {
    jdoSetcodMovimientoCargo(this, codMovimientoCargo);
  }

  public String getDescripcion() {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion) {
    jdoSetdescripcion(this, descripcion);
  }

  public long getIdMovimientoCargo() {
    return jdoGetidMovimientoCargo(this);
  }

  public void setIdMovimientoCargo(long idMovimientoCargo) {
    jdoSetidMovimientoCargo(this, idMovimientoCargo);
  }

  public String getTipo() {
    return jdoGettipo(this);
  }

  public void setTipo(String tipo) {
    jdoSettipo(this, tipo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    MovimientoCargo localMovimientoCargo = new MovimientoCargo();
    localMovimientoCargo.jdoFlags = 1;
    localMovimientoCargo.jdoStateManager = paramStateManager;
    return localMovimientoCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    MovimientoCargo localMovimientoCargo = new MovimientoCargo();
    localMovimientoCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localMovimientoCargo.jdoFlags = 1;
    localMovimientoCargo.jdoStateManager = paramStateManager;
    return localMovimientoCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codMovimientoCargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idMovimientoCargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codMovimientoCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idMovimientoCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(MovimientoCargo paramMovimientoCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramMovimientoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.codMovimientoCargo = paramMovimientoCargo.codMovimientoCargo;
      return;
    case 1:
      if (paramMovimientoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramMovimientoCargo.descripcion;
      return;
    case 2:
      if (paramMovimientoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idMovimientoCargo = paramMovimientoCargo.idMovimientoCargo;
      return;
    case 3:
      if (paramMovimientoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramMovimientoCargo.tipo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof MovimientoCargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    MovimientoCargo localMovimientoCargo = (MovimientoCargo)paramObject;
    if (localMovimientoCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localMovimientoCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new MovimientoCargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new MovimientoCargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MovimientoCargoPK))
      throw new IllegalArgumentException("arg1");
    MovimientoCargoPK localMovimientoCargoPK = (MovimientoCargoPK)paramObject;
    localMovimientoCargoPK.idMovimientoCargo = this.idMovimientoCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MovimientoCargoPK))
      throw new IllegalArgumentException("arg1");
    MovimientoCargoPK localMovimientoCargoPK = (MovimientoCargoPK)paramObject;
    this.idMovimientoCargo = localMovimientoCargoPK.idMovimientoCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MovimientoCargoPK))
      throw new IllegalArgumentException("arg2");
    MovimientoCargoPK localMovimientoCargoPK = (MovimientoCargoPK)paramObject;
    localMovimientoCargoPK.idMovimientoCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MovimientoCargoPK))
      throw new IllegalArgumentException("arg2");
    MovimientoCargoPK localMovimientoCargoPK = (MovimientoCargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localMovimientoCargoPK.idMovimientoCargo);
  }

  private static final String jdoGetcodMovimientoCargo(MovimientoCargo paramMovimientoCargo)
  {
    if (paramMovimientoCargo.jdoFlags <= 0)
      return paramMovimientoCargo.codMovimientoCargo;
    StateManager localStateManager = paramMovimientoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoCargo.codMovimientoCargo;
    if (localStateManager.isLoaded(paramMovimientoCargo, jdoInheritedFieldCount + 0))
      return paramMovimientoCargo.codMovimientoCargo;
    return localStateManager.getStringField(paramMovimientoCargo, jdoInheritedFieldCount + 0, paramMovimientoCargo.codMovimientoCargo);
  }

  private static final void jdoSetcodMovimientoCargo(MovimientoCargo paramMovimientoCargo, String paramString)
  {
    if (paramMovimientoCargo.jdoFlags == 0)
    {
      paramMovimientoCargo.codMovimientoCargo = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoCargo.codMovimientoCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoCargo, jdoInheritedFieldCount + 0, paramMovimientoCargo.codMovimientoCargo, paramString);
  }

  private static final String jdoGetdescripcion(MovimientoCargo paramMovimientoCargo)
  {
    if (paramMovimientoCargo.jdoFlags <= 0)
      return paramMovimientoCargo.descripcion;
    StateManager localStateManager = paramMovimientoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoCargo.descripcion;
    if (localStateManager.isLoaded(paramMovimientoCargo, jdoInheritedFieldCount + 1))
      return paramMovimientoCargo.descripcion;
    return localStateManager.getStringField(paramMovimientoCargo, jdoInheritedFieldCount + 1, paramMovimientoCargo.descripcion);
  }

  private static final void jdoSetdescripcion(MovimientoCargo paramMovimientoCargo, String paramString)
  {
    if (paramMovimientoCargo.jdoFlags == 0)
    {
      paramMovimientoCargo.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoCargo.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoCargo, jdoInheritedFieldCount + 1, paramMovimientoCargo.descripcion, paramString);
  }

  private static final long jdoGetidMovimientoCargo(MovimientoCargo paramMovimientoCargo)
  {
    return paramMovimientoCargo.idMovimientoCargo;
  }

  private static final void jdoSetidMovimientoCargo(MovimientoCargo paramMovimientoCargo, long paramLong)
  {
    StateManager localStateManager = paramMovimientoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoCargo.idMovimientoCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramMovimientoCargo, jdoInheritedFieldCount + 2, paramMovimientoCargo.idMovimientoCargo, paramLong);
  }

  private static final String jdoGettipo(MovimientoCargo paramMovimientoCargo)
  {
    if (paramMovimientoCargo.jdoFlags <= 0)
      return paramMovimientoCargo.tipo;
    StateManager localStateManager = paramMovimientoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoCargo.tipo;
    if (localStateManager.isLoaded(paramMovimientoCargo, jdoInheritedFieldCount + 3))
      return paramMovimientoCargo.tipo;
    return localStateManager.getStringField(paramMovimientoCargo, jdoInheritedFieldCount + 3, paramMovimientoCargo.tipo);
  }

  private static final void jdoSettipo(MovimientoCargo paramMovimientoCargo, String paramString)
  {
    if (paramMovimientoCargo.jdoFlags == 0)
    {
      paramMovimientoCargo.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoCargo.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoCargo, jdoInheritedFieldCount + 3, paramMovimientoCargo.tipo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}