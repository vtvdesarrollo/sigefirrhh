package sigefirrhh.base.registro;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class MovimientoCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addMovimientoCargo(MovimientoCargo movimientoCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    MovimientoCargo movimientoCargoNew = 
      (MovimientoCargo)BeanUtils.cloneBean(
      movimientoCargo);

    pm.makePersistent(movimientoCargoNew);
  }

  public void updateMovimientoCargo(MovimientoCargo movimientoCargo) throws Exception
  {
    MovimientoCargo movimientoCargoModify = 
      findMovimientoCargoById(movimientoCargo.getIdMovimientoCargo());

    BeanUtils.copyProperties(movimientoCargoModify, movimientoCargo);
  }

  public void deleteMovimientoCargo(MovimientoCargo movimientoCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    MovimientoCargo movimientoCargoDelete = 
      findMovimientoCargoById(movimientoCargo.getIdMovimientoCargo());
    pm.deletePersistent(movimientoCargoDelete);
  }

  public MovimientoCargo findMovimientoCargoById(long idMovimientoCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idMovimientoCargo == pIdMovimientoCargo";
    Query query = pm.newQuery(MovimientoCargo.class, filter);

    query.declareParameters("long pIdMovimientoCargo");

    parameters.put("pIdMovimientoCargo", new Long(idMovimientoCargo));

    Collection colMovimientoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colMovimientoCargo.iterator();
    return (MovimientoCargo)iterator.next();
  }

  public Collection findMovimientoCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent movimientoCargoExtent = pm.getExtent(
      MovimientoCargo.class, true);
    Query query = pm.newQuery(movimientoCargoExtent);
    query.setOrdering("codMovimientoCargo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipo(String tipo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipo == pTipo";

    Query query = pm.newQuery(MovimientoCargo.class, filter);

    query.declareParameters("java.lang.String pTipo");
    HashMap parameters = new HashMap();

    parameters.put("pTipo", new String(tipo));

    query.setOrdering("codMovimientoCargo ascending");

    Collection colMovimientoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMovimientoCargo);

    return colMovimientoCargo;
  }
}