package sigefirrhh.base.registro;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CausaMovimientoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCausaMovimiento(CausaMovimiento causaMovimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CausaMovimiento causaMovimientoNew = 
      (CausaMovimiento)BeanUtils.cloneBean(
      causaMovimiento);

    MovimientoPersonalBeanBusiness movimientoPersonalBeanBusiness = new MovimientoPersonalBeanBusiness();

    if (causaMovimientoNew.getMovimientoPersonal() != null) {
      causaMovimientoNew.setMovimientoPersonal(
        movimientoPersonalBeanBusiness.findMovimientoPersonalById(
        causaMovimientoNew.getMovimientoPersonal().getIdMovimientoPersonal()));
    }
    pm.makePersistent(causaMovimientoNew);
  }

  public void updateCausaMovimiento(CausaMovimiento causaMovimiento) throws Exception
  {
    CausaMovimiento causaMovimientoModify = 
      findCausaMovimientoById(causaMovimiento.getIdCausaMovimiento());

    MovimientoPersonalBeanBusiness movimientoPersonalBeanBusiness = new MovimientoPersonalBeanBusiness();

    if (causaMovimiento.getMovimientoPersonal() != null) {
      causaMovimiento.setMovimientoPersonal(
        movimientoPersonalBeanBusiness.findMovimientoPersonalById(
        causaMovimiento.getMovimientoPersonal().getIdMovimientoPersonal()));
    }

    BeanUtils.copyProperties(causaMovimientoModify, causaMovimiento);
  }

  public void deleteCausaMovimiento(CausaMovimiento causaMovimiento) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CausaMovimiento causaMovimientoDelete = 
      findCausaMovimientoById(causaMovimiento.getIdCausaMovimiento());
    pm.deletePersistent(causaMovimientoDelete);
  }

  public CausaMovimiento findCausaMovimientoById(long idCausaMovimiento) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCausaMovimiento == pIdCausaMovimiento";
    Query query = pm.newQuery(CausaMovimiento.class, filter);

    query.declareParameters("long pIdCausaMovimiento");

    parameters.put("pIdCausaMovimiento", new Long(idCausaMovimiento));

    Collection colCausaMovimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCausaMovimiento.iterator();
    return (CausaMovimiento)iterator.next();
  }

  public Collection findCausaMovimientoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent causaMovimientoExtent = pm.getExtent(
      CausaMovimiento.class, true);
    Query query = pm.newQuery(causaMovimientoExtent);
    query.setOrdering("movimientoPersonal.codMovimientoPersonal ascending, codCausaMovimiento ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByMovimientoPersonal(long idMovimientoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "movimientoPersonal.idMovimientoPersonal == pIdMovimientoPersonal";

    Query query = pm.newQuery(CausaMovimiento.class, filter);

    query.declareParameters("long pIdMovimientoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdMovimientoPersonal", new Long(idMovimientoPersonal));

    query.setOrdering("movimientoPersonal.codMovimientoPersonal ascending, codCausaMovimiento ascending");

    Collection colCausaMovimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCausaMovimiento);

    return colCausaMovimiento;
  }

  public Collection findByCodCausaMovimiento(String codCausaMovimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCausaMovimiento == pCodCausaMovimiento";

    Query query = pm.newQuery(CausaMovimiento.class, filter);

    query.declareParameters("java.lang.String pCodCausaMovimiento");
    HashMap parameters = new HashMap();

    parameters.put("pCodCausaMovimiento", new String(codCausaMovimiento));

    query.setOrdering("movimientoPersonal.codMovimientoPersonal ascending, codCausaMovimiento ascending");

    Collection colCausaMovimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCausaMovimiento);

    return colCausaMovimiento;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(CausaMovimiento.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("movimientoPersonal.codMovimientoPersonal ascending, codCausaMovimiento ascending");

    Collection colCausaMovimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCausaMovimiento);

    return colCausaMovimiento;
  }
}