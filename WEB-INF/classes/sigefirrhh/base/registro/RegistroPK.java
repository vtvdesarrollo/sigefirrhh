package sigefirrhh.base.registro;

import java.io.Serializable;

public class RegistroPK
  implements Serializable
{
  public long idRegistro;

  public RegistroPK()
  {
  }

  public RegistroPK(long idRegistro)
  {
    this.idRegistro = idRegistro;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RegistroPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RegistroPK thatPK)
  {
    return 
      this.idRegistro == thatPK.idRegistro;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRegistro)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRegistro);
  }
}