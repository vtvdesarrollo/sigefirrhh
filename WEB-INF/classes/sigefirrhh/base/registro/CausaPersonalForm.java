package sigefirrhh.base.registro;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.login.LoginSession;

public class CausaPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CausaPersonalForm.class.getName());
  private CausaPersonal causaPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private boolean showCausaPersonalByClasificacionPersonal;
  private boolean showCausaPersonalByCausaMovimiento;
  private String findSelectClasificacionPersonal;
  private String findSelectCausaMovimiento;
  private Collection findColClasificacionPersonal;
  private Collection findColCausaMovimiento;
  private Collection colClasificacionPersonal;
  private Collection colCausaMovimiento;
  private String selectClasificacionPersonal;
  private String selectCausaMovimiento;
  private Object stateResultCausaPersonalByClasificacionPersonal = null;

  private Object stateResultCausaPersonalByCausaMovimiento = null;

  public String getFindSelectClasificacionPersonal()
  {
    return this.findSelectClasificacionPersonal;
  }
  public void setFindSelectClasificacionPersonal(String valClasificacionPersonal) {
    this.findSelectClasificacionPersonal = valClasificacionPersonal;
  }

  public Collection getFindColClasificacionPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColClasificacionPersonal.iterator();
    ClasificacionPersonal clasificacionPersonal = null;
    while (iterator.hasNext()) {
      clasificacionPersonal = (ClasificacionPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(clasificacionPersonal.getIdClasificacionPersonal()), 
        clasificacionPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectCausaMovimiento() {
    return this.findSelectCausaMovimiento;
  }
  public void setFindSelectCausaMovimiento(String valCausaMovimiento) {
    this.findSelectCausaMovimiento = valCausaMovimiento;
  }

  public Collection getFindColCausaMovimiento() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCausaMovimiento.iterator();
    CausaMovimiento causaMovimiento = null;
    while (iterator.hasNext()) {
      causaMovimiento = (CausaMovimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaMovimiento.getIdCausaMovimiento()), 
        causaMovimiento.toString()));
    }
    return col;
  }

  public String getSelectClasificacionPersonal()
  {
    return this.selectClasificacionPersonal;
  }
  public void setSelectClasificacionPersonal(String valClasificacionPersonal) {
    Iterator iterator = this.colClasificacionPersonal.iterator();
    ClasificacionPersonal clasificacionPersonal = null;
    this.causaPersonal.setClasificacionPersonal(null);
    while (iterator.hasNext()) {
      clasificacionPersonal = (ClasificacionPersonal)iterator.next();
      if (String.valueOf(clasificacionPersonal.getIdClasificacionPersonal()).equals(
        valClasificacionPersonal)) {
        this.causaPersonal.setClasificacionPersonal(
          clasificacionPersonal);
        break;
      }
    }
    this.selectClasificacionPersonal = valClasificacionPersonal;
  }
  public String getSelectCausaMovimiento() {
    return this.selectCausaMovimiento;
  }
  public void setSelectCausaMovimiento(String valCausaMovimiento) {
    Iterator iterator = this.colCausaMovimiento.iterator();
    CausaMovimiento causaMovimiento = null;
    this.causaPersonal.setCausaMovimiento(null);
    while (iterator.hasNext()) {
      causaMovimiento = (CausaMovimiento)iterator.next();
      if (String.valueOf(causaMovimiento.getIdCausaMovimiento()).equals(
        valCausaMovimiento)) {
        this.causaPersonal.setCausaMovimiento(
          causaMovimiento);
        break;
      }
    }
    this.selectCausaMovimiento = valCausaMovimiento;
  }
  public Collection getResult() {
    return this.result;
  }

  public CausaPersonal getCausaPersonal() {
    if (this.causaPersonal == null) {
      this.causaPersonal = new CausaPersonal();
    }
    return this.causaPersonal;
  }

  public CausaPersonalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColClasificacionPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colClasificacionPersonal.iterator();
    ClasificacionPersonal clasificacionPersonal = null;
    while (iterator.hasNext()) {
      clasificacionPersonal = (ClasificacionPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(clasificacionPersonal.getIdClasificacionPersonal()), 
        clasificacionPersonal.toString()));
    }
    return col;
  }

  public Collection getColCausaMovimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaMovimiento.iterator();
    CausaMovimiento causaMovimiento = null;
    while (iterator.hasNext()) {
      causaMovimiento = (CausaMovimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaMovimiento.getIdCausaMovimiento()), 
        causaMovimiento.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColClasificacionPersonal = 
        this.definicionesFacade.findAllClasificacionPersonal();
      this.findColCausaMovimiento = 
        this.registroFacade.findAllCausaMovimiento();

      this.colClasificacionPersonal = 
        this.definicionesFacade.findAllClasificacionPersonal();
      this.colCausaMovimiento = 
        this.registroFacade.findAllCausaMovimiento();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findCausaPersonalByClasificacionPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroFacade.findCausaPersonalByClasificacionPersonal(Long.valueOf(this.findSelectClasificacionPersonal).longValue());
      this.showCausaPersonalByClasificacionPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCausaPersonalByClasificacionPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectClasificacionPersonal = null;
    this.findSelectCausaMovimiento = null;

    return null;
  }

  public String findCausaPersonalByCausaMovimiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroFacade.findCausaPersonalByCausaMovimiento(Long.valueOf(this.findSelectCausaMovimiento).longValue());
      this.showCausaPersonalByCausaMovimiento = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCausaPersonalByCausaMovimiento)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectClasificacionPersonal = null;
    this.findSelectCausaMovimiento = null;

    return null;
  }

  public boolean isShowCausaPersonalByClasificacionPersonal() {
    return this.showCausaPersonalByClasificacionPersonal;
  }
  public boolean isShowCausaPersonalByCausaMovimiento() {
    return this.showCausaPersonalByCausaMovimiento;
  }

  public String selectCausaPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectClasificacionPersonal = null;
    this.selectCausaMovimiento = null;

    long idCausaPersonal = 
      Long.parseLong((String)requestParameterMap.get("idCausaPersonal"));
    try
    {
      this.causaPersonal = 
        this.registroFacade.findCausaPersonalById(
        idCausaPersonal);
      if (this.causaPersonal.getClasificacionPersonal() != null) {
        this.selectClasificacionPersonal = 
          String.valueOf(this.causaPersonal.getClasificacionPersonal().getIdClasificacionPersonal());
      }
      if (this.causaPersonal.getCausaMovimiento() != null) {
        this.selectCausaMovimiento = 
          String.valueOf(this.causaPersonal.getCausaMovimiento().getIdCausaMovimiento());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.causaPersonal = null;
    this.showCausaPersonalByClasificacionPersonal = false;
    this.showCausaPersonalByCausaMovimiento = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.causaPersonal.getTiempoSitp() != null) && 
      (this.causaPersonal.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.registroFacade.addCausaPersonal(
          this.causaPersonal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.registroFacade.updateCausaPersonal(
          this.causaPersonal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.registroFacade.deleteCausaPersonal(
        this.causaPersonal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.causaPersonal = new CausaPersonal();

    this.selectClasificacionPersonal = null;

    this.selectCausaMovimiento = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.causaPersonal.setIdCausaPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.base.registro.CausaPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.causaPersonal = new CausaPersonal();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}