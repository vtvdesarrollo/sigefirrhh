package sigefirrhh.base.registro;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class CausaMovimiento
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SUJETO;
  protected static final Map LISTA_GRADO99;
  private long idCausaMovimiento;
  private MovimientoPersonal movimientoPersonal;
  private String codCausaMovimiento;
  private String descripcion;
  private String formato;
  private String sujetoLefp;
  private String grado99;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codCausaMovimiento", "descripcion", "formato", "grado99", "idCausaMovimiento", "movimientoPersonal", "sujetoLefp" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.registro.MovimientoPersonal"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.registro.CausaMovimiento"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CausaMovimiento());

    LISTA_SUJETO = 
      new LinkedHashMap();
    LISTA_GRADO99 = 
      new LinkedHashMap();

    LISTA_SUJETO.put("S", "SUJETO A LEFP");
    LISTA_SUJETO.put("N", "NO SUJETO A LEFP");
    LISTA_SUJETO.put("A", "TODOS");
    LISTA_GRADO99.put("N", "FUNCIONARIO DE CARRERA");
    LISTA_GRADO99.put("S", "LIBRE NOMBRAMIENTO Y REMOCION");
    LISTA_GRADO99.put("A", "TODOS");
  }

  public CausaMovimiento()
  {
    jdoSetsujetoLefp(this, "N");

    jdoSetgrado99(this, "N");
  }
  public String toString() {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodCausaMovimiento(this);
  }

  public String getCodCausaMovimiento() {
    return jdoGetcodCausaMovimiento(this);
  }

  public void setCodCausaMovimiento(String codCausaMovimiento) {
    jdoSetcodCausaMovimiento(this, codCausaMovimiento);
  }

  public String getDescripcion() {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion) {
    jdoSetdescripcion(this, descripcion);
  }

  public String getFormato() {
    return jdoGetformato(this);
  }

  public void setFormato(String formato) {
    jdoSetformato(this, formato);
  }

  public String getGrado99() {
    return jdoGetgrado99(this);
  }

  public void setGrado99(String grado99) {
    jdoSetgrado99(this, grado99);
  }

  public long getIdCausaMovimiento() {
    return jdoGetidCausaMovimiento(this);
  }

  public void setIdCausaMovimiento(long idCausaMovimiento) {
    jdoSetidCausaMovimiento(this, idCausaMovimiento);
  }

  public MovimientoPersonal getMovimientoPersonal() {
    return jdoGetmovimientoPersonal(this);
  }

  public void setMovimientoPersonal(MovimientoPersonal movimientoPersonal) {
    jdoSetmovimientoPersonal(this, movimientoPersonal);
  }

  public String getSujetoLefp() {
    return jdoGetsujetoLefp(this);
  }

  public void setSujetoLefp(String sujetoLefp) {
    jdoSetsujetoLefp(this, sujetoLefp);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CausaMovimiento localCausaMovimiento = new CausaMovimiento();
    localCausaMovimiento.jdoFlags = 1;
    localCausaMovimiento.jdoStateManager = paramStateManager;
    return localCausaMovimiento;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CausaMovimiento localCausaMovimiento = new CausaMovimiento();
    localCausaMovimiento.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCausaMovimiento.jdoFlags = 1;
    localCausaMovimiento.jdoStateManager = paramStateManager;
    return localCausaMovimiento;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCausaMovimiento);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.formato);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.grado99);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCausaMovimiento);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.movimientoPersonal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sujetoLefp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCausaMovimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.formato = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado99 = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCausaMovimiento = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.movimientoPersonal = ((MovimientoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sujetoLefp = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CausaMovimiento paramCausaMovimiento, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCausaMovimiento == null)
        throw new IllegalArgumentException("arg1");
      this.codCausaMovimiento = paramCausaMovimiento.codCausaMovimiento;
      return;
    case 1:
      if (paramCausaMovimiento == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramCausaMovimiento.descripcion;
      return;
    case 2:
      if (paramCausaMovimiento == null)
        throw new IllegalArgumentException("arg1");
      this.formato = paramCausaMovimiento.formato;
      return;
    case 3:
      if (paramCausaMovimiento == null)
        throw new IllegalArgumentException("arg1");
      this.grado99 = paramCausaMovimiento.grado99;
      return;
    case 4:
      if (paramCausaMovimiento == null)
        throw new IllegalArgumentException("arg1");
      this.idCausaMovimiento = paramCausaMovimiento.idCausaMovimiento;
      return;
    case 5:
      if (paramCausaMovimiento == null)
        throw new IllegalArgumentException("arg1");
      this.movimientoPersonal = paramCausaMovimiento.movimientoPersonal;
      return;
    case 6:
      if (paramCausaMovimiento == null)
        throw new IllegalArgumentException("arg1");
      this.sujetoLefp = paramCausaMovimiento.sujetoLefp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CausaMovimiento))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CausaMovimiento localCausaMovimiento = (CausaMovimiento)paramObject;
    if (localCausaMovimiento.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCausaMovimiento, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CausaMovimientoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CausaMovimientoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CausaMovimientoPK))
      throw new IllegalArgumentException("arg1");
    CausaMovimientoPK localCausaMovimientoPK = (CausaMovimientoPK)paramObject;
    localCausaMovimientoPK.idCausaMovimiento = this.idCausaMovimiento;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CausaMovimientoPK))
      throw new IllegalArgumentException("arg1");
    CausaMovimientoPK localCausaMovimientoPK = (CausaMovimientoPK)paramObject;
    this.idCausaMovimiento = localCausaMovimientoPK.idCausaMovimiento;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CausaMovimientoPK))
      throw new IllegalArgumentException("arg2");
    CausaMovimientoPK localCausaMovimientoPK = (CausaMovimientoPK)paramObject;
    localCausaMovimientoPK.idCausaMovimiento = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CausaMovimientoPK))
      throw new IllegalArgumentException("arg2");
    CausaMovimientoPK localCausaMovimientoPK = (CausaMovimientoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localCausaMovimientoPK.idCausaMovimiento);
  }

  private static final String jdoGetcodCausaMovimiento(CausaMovimiento paramCausaMovimiento)
  {
    if (paramCausaMovimiento.jdoFlags <= 0)
      return paramCausaMovimiento.codCausaMovimiento;
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
      return paramCausaMovimiento.codCausaMovimiento;
    if (localStateManager.isLoaded(paramCausaMovimiento, jdoInheritedFieldCount + 0))
      return paramCausaMovimiento.codCausaMovimiento;
    return localStateManager.getStringField(paramCausaMovimiento, jdoInheritedFieldCount + 0, paramCausaMovimiento.codCausaMovimiento);
  }

  private static final void jdoSetcodCausaMovimiento(CausaMovimiento paramCausaMovimiento, String paramString)
  {
    if (paramCausaMovimiento.jdoFlags == 0)
    {
      paramCausaMovimiento.codCausaMovimiento = paramString;
      return;
    }
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaMovimiento.codCausaMovimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramCausaMovimiento, jdoInheritedFieldCount + 0, paramCausaMovimiento.codCausaMovimiento, paramString);
  }

  private static final String jdoGetdescripcion(CausaMovimiento paramCausaMovimiento)
  {
    if (paramCausaMovimiento.jdoFlags <= 0)
      return paramCausaMovimiento.descripcion;
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
      return paramCausaMovimiento.descripcion;
    if (localStateManager.isLoaded(paramCausaMovimiento, jdoInheritedFieldCount + 1))
      return paramCausaMovimiento.descripcion;
    return localStateManager.getStringField(paramCausaMovimiento, jdoInheritedFieldCount + 1, paramCausaMovimiento.descripcion);
  }

  private static final void jdoSetdescripcion(CausaMovimiento paramCausaMovimiento, String paramString)
  {
    if (paramCausaMovimiento.jdoFlags == 0)
    {
      paramCausaMovimiento.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaMovimiento.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramCausaMovimiento, jdoInheritedFieldCount + 1, paramCausaMovimiento.descripcion, paramString);
  }

  private static final String jdoGetformato(CausaMovimiento paramCausaMovimiento)
  {
    if (paramCausaMovimiento.jdoFlags <= 0)
      return paramCausaMovimiento.formato;
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
      return paramCausaMovimiento.formato;
    if (localStateManager.isLoaded(paramCausaMovimiento, jdoInheritedFieldCount + 2))
      return paramCausaMovimiento.formato;
    return localStateManager.getStringField(paramCausaMovimiento, jdoInheritedFieldCount + 2, paramCausaMovimiento.formato);
  }

  private static final void jdoSetformato(CausaMovimiento paramCausaMovimiento, String paramString)
  {
    if (paramCausaMovimiento.jdoFlags == 0)
    {
      paramCausaMovimiento.formato = paramString;
      return;
    }
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaMovimiento.formato = paramString;
      return;
    }
    localStateManager.setStringField(paramCausaMovimiento, jdoInheritedFieldCount + 2, paramCausaMovimiento.formato, paramString);
  }

  private static final String jdoGetgrado99(CausaMovimiento paramCausaMovimiento)
  {
    if (paramCausaMovimiento.jdoFlags <= 0)
      return paramCausaMovimiento.grado99;
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
      return paramCausaMovimiento.grado99;
    if (localStateManager.isLoaded(paramCausaMovimiento, jdoInheritedFieldCount + 3))
      return paramCausaMovimiento.grado99;
    return localStateManager.getStringField(paramCausaMovimiento, jdoInheritedFieldCount + 3, paramCausaMovimiento.grado99);
  }

  private static final void jdoSetgrado99(CausaMovimiento paramCausaMovimiento, String paramString)
  {
    if (paramCausaMovimiento.jdoFlags == 0)
    {
      paramCausaMovimiento.grado99 = paramString;
      return;
    }
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaMovimiento.grado99 = paramString;
      return;
    }
    localStateManager.setStringField(paramCausaMovimiento, jdoInheritedFieldCount + 3, paramCausaMovimiento.grado99, paramString);
  }

  private static final long jdoGetidCausaMovimiento(CausaMovimiento paramCausaMovimiento)
  {
    return paramCausaMovimiento.idCausaMovimiento;
  }

  private static final void jdoSetidCausaMovimiento(CausaMovimiento paramCausaMovimiento, long paramLong)
  {
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaMovimiento.idCausaMovimiento = paramLong;
      return;
    }
    localStateManager.setLongField(paramCausaMovimiento, jdoInheritedFieldCount + 4, paramCausaMovimiento.idCausaMovimiento, paramLong);
  }

  private static final MovimientoPersonal jdoGetmovimientoPersonal(CausaMovimiento paramCausaMovimiento)
  {
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
      return paramCausaMovimiento.movimientoPersonal;
    if (localStateManager.isLoaded(paramCausaMovimiento, jdoInheritedFieldCount + 5))
      return paramCausaMovimiento.movimientoPersonal;
    return (MovimientoPersonal)localStateManager.getObjectField(paramCausaMovimiento, jdoInheritedFieldCount + 5, paramCausaMovimiento.movimientoPersonal);
  }

  private static final void jdoSetmovimientoPersonal(CausaMovimiento paramCausaMovimiento, MovimientoPersonal paramMovimientoPersonal)
  {
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaMovimiento.movimientoPersonal = paramMovimientoPersonal;
      return;
    }
    localStateManager.setObjectField(paramCausaMovimiento, jdoInheritedFieldCount + 5, paramCausaMovimiento.movimientoPersonal, paramMovimientoPersonal);
  }

  private static final String jdoGetsujetoLefp(CausaMovimiento paramCausaMovimiento)
  {
    if (paramCausaMovimiento.jdoFlags <= 0)
      return paramCausaMovimiento.sujetoLefp;
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
      return paramCausaMovimiento.sujetoLefp;
    if (localStateManager.isLoaded(paramCausaMovimiento, jdoInheritedFieldCount + 6))
      return paramCausaMovimiento.sujetoLefp;
    return localStateManager.getStringField(paramCausaMovimiento, jdoInheritedFieldCount + 6, paramCausaMovimiento.sujetoLefp);
  }

  private static final void jdoSetsujetoLefp(CausaMovimiento paramCausaMovimiento, String paramString)
  {
    if (paramCausaMovimiento.jdoFlags == 0)
    {
      paramCausaMovimiento.sujetoLefp = paramString;
      return;
    }
    StateManager localStateManager = paramCausaMovimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaMovimiento.sujetoLefp = paramString;
      return;
    }
    localStateManager.setStringField(paramCausaMovimiento, jdoInheritedFieldCount + 6, paramCausaMovimiento.sujetoLefp, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}