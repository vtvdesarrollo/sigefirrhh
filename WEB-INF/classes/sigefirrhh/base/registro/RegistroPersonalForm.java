package sigefirrhh.base.registro;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class RegistroPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RegistroPersonalForm.class.getName());
  private RegistroPersonal registroPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private boolean showRegistroPersonalByTipoPersonal;
  private boolean showRegistroPersonalByRegistro;
  private String findSelectTipoPersonal;
  private String findSelectRegistro;
  private Collection findColTipoPersonal;
  private Collection findColRegistro;
  private Collection colTipoPersonal;
  private Collection colRegistro;
  private String selectTipoPersonal;
  private String selectRegistro;
  private Object stateResultRegistroPersonalByTipoPersonal = null;

  private Object stateResultRegistroPersonalByRegistro = null;

  public String getFindSelectTipoPersonal()
  {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectRegistro() {
    return this.findSelectRegistro;
  }
  public void setFindSelectRegistro(String valRegistro) {
    this.findSelectRegistro = valRegistro;
  }

  public Collection getFindColRegistro() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRegistro.iterator();
    Registro registro = null;
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registro.getIdRegistro()), 
        registro.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.registroPersonal.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.registroPersonal.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectRegistro() {
    return this.selectRegistro;
  }
  public void setSelectRegistro(String valRegistro) {
    Iterator iterator = this.colRegistro.iterator();
    Registro registro = null;
    this.registroPersonal.setRegistro(null);
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      if (String.valueOf(registro.getIdRegistro()).equals(
        valRegistro)) {
        this.registroPersonal.setRegistro(
          registro);
        break;
      }
    }
    this.selectRegistro = valRegistro;
  }
  public Collection getResult() {
    return this.result;
  }

  public RegistroPersonal getRegistroPersonal() {
    if (this.registroPersonal == null) {
      this.registroPersonal = new RegistroPersonal();
    }
    return this.registroPersonal;
  }

  public RegistroPersonalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColRegistro()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistro.iterator();
    Registro registro = null;
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registro.getIdRegistro()), 
        registro.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColRegistro = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colRegistro = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findRegistroPersonalByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroFacade.findRegistroPersonalByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showRegistroPersonalByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRegistroPersonalByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findSelectRegistro = null;

    return null;
  }

  public String findRegistroPersonalByRegistro()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroFacade.findRegistroPersonalByRegistro(Long.valueOf(this.findSelectRegistro).longValue());
      this.showRegistroPersonalByRegistro = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRegistroPersonalByRegistro)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findSelectRegistro = null;

    return null;
  }

  public boolean isShowRegistroPersonalByTipoPersonal() {
    return this.showRegistroPersonalByTipoPersonal;
  }
  public boolean isShowRegistroPersonalByRegistro() {
    return this.showRegistroPersonalByRegistro;
  }

  public String selectRegistroPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectRegistro = null;

    long idRegistroPersonal = 
      Long.parseLong((String)requestParameterMap.get("idRegistroPersonal"));
    try
    {
      this.registroPersonal = 
        this.registroFacade.findRegistroPersonalById(
        idRegistroPersonal);
      if (this.registroPersonal.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.registroPersonal.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.registroPersonal.getRegistro() != null) {
        this.selectRegistro = 
          String.valueOf(this.registroPersonal.getRegistro().getIdRegistro());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.registroPersonal = null;
    this.showRegistroPersonalByTipoPersonal = false;
    this.showRegistroPersonalByRegistro = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.registroPersonal.getTiempoSitp() != null) && 
      (this.registroPersonal.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.registroFacade.addRegistroPersonal(
          this.registroPersonal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.registroFacade.updateRegistroPersonal(
          this.registroPersonal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.registroFacade.deleteRegistroPersonal(
        this.registroPersonal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.registroPersonal = new RegistroPersonal();

    this.selectTipoPersonal = null;

    this.selectRegistro = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.registroPersonal.setIdRegistroPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.base.registro.RegistroPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.registroPersonal = new RegistroPersonal();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}