package sigefirrhh.base.registro;

import java.io.Serializable;

public class RegistroPersonalPK
  implements Serializable
{
  public long idRegistroPersonal;

  public RegistroPersonalPK()
  {
  }

  public RegistroPersonalPK(long idRegistroPersonal)
  {
    this.idRegistroPersonal = idRegistroPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RegistroPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RegistroPersonalPK thatPK)
  {
    return 
      this.idRegistroPersonal == thatPK.idRegistroPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRegistroPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRegistroPersonal);
  }
}