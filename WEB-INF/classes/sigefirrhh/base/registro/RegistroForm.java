package sigefirrhh.base.registro;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class RegistroForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RegistroForm.class.getName());
  private Registro registro;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private boolean showRegistroByNumeroRegistro;
  private boolean showRegistroByNombre;
  private int findNumeroRegistro;
  private String findNombre;
  private Collection colGrupoOrganismo;
  private String selectGrupoOrganismo;
  private Object stateResultRegistroByNumeroRegistro = null;

  private Object stateResultRegistroByNombre = null;

  public int getFindNumeroRegistro()
  {
    return this.findNumeroRegistro;
  }
  public void setFindNumeroRegistro(int findNumeroRegistro) {
    this.findNumeroRegistro = findNumeroRegistro;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectGrupoOrganismo()
  {
    return this.selectGrupoOrganismo;
  }
  public void setSelectGrupoOrganismo(String valGrupoOrganismo) {
    Iterator iterator = this.colGrupoOrganismo.iterator();
    GrupoOrganismo grupoOrganismo = null;
    this.registro.setGrupoOrganismo(null);
    while (iterator.hasNext()) {
      grupoOrganismo = (GrupoOrganismo)iterator.next();
      if (String.valueOf(grupoOrganismo.getIdGrupoOrganismo()).equals(
        valGrupoOrganismo)) {
        this.registro.setGrupoOrganismo(
          grupoOrganismo);
        break;
      }
    }
    this.selectGrupoOrganismo = valGrupoOrganismo;
  }
  public Collection getResult() {
    return this.result;
  }

  public Registro getRegistro() {
    if (this.registro == null) {
      this.registro = new Registro();
    }
    return this.registro;
  }

  public RegistroForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListAprobacionMpd()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Registro.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColGrupoOrganismo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoOrganismo.iterator();
    GrupoOrganismo grupoOrganismo = null;
    while (iterator.hasNext()) {
      grupoOrganismo = (GrupoOrganismo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoOrganismo.getIdGrupoOrganismo()), 
        grupoOrganismo.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colGrupoOrganismo = 
        this.estructuraFacade.findGrupoOrganismoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findRegistroByNumeroRegistro()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.registroFacade.findRegistroByNumeroRegistro(this.findNumeroRegistro, idOrganismo);
      this.showRegistroByNumeroRegistro = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRegistroByNumeroRegistro)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findNumeroRegistro = 0;
    this.findNombre = null;

    return null;
  }

  public String findRegistroByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.registroFacade.findRegistroByNombre(this.findNombre, idOrganismo);
      this.showRegistroByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRegistroByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findNumeroRegistro = 0;
    this.findNombre = null;

    return null;
  }

  public boolean isShowRegistroByNumeroRegistro() {
    return this.showRegistroByNumeroRegistro;
  }
  public boolean isShowRegistroByNombre() {
    return this.showRegistroByNombre;
  }

  public String selectRegistro()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectGrupoOrganismo = null;

    long idRegistro = 
      Long.parseLong((String)requestParameterMap.get("idRegistro"));
    try
    {
      this.registro = 
        this.registroFacade.findRegistroById(
        idRegistro);
      if (this.registro.getGrupoOrganismo() != null) {
        this.selectGrupoOrganismo = 
          String.valueOf(this.registro.getGrupoOrganismo().getIdGrupoOrganismo());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.registro = null;
    this.showRegistroByNumeroRegistro = false;
    this.showRegistroByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.registro.getFechaVigencia() != null) && 
      (this.registro.getFechaVigencia().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Vigencia no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.registro.getTiempoSitp() != null) && 
      (this.registro.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.registroFacade.addRegistro(
          this.registro);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.registroFacade.updateRegistro(
          this.registro);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.registroFacade.deleteRegistro(
        this.registro);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.registro = new Registro();

    this.selectGrupoOrganismo = null;

    this.registro.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.registro.setIdRegistro(identityGenerator.getNextSequenceNumber("sigefirrhh.base.registro.Registro"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.registro = new Registro();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}