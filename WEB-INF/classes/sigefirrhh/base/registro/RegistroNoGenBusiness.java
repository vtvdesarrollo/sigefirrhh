package sigefirrhh.base.registro;

import java.io.Serializable;
import java.util.Collection;

public class RegistroNoGenBusiness extends RegistroxBusiness
  implements Serializable
{
  private CausaPersonalNoGenBeanBusiness causaPersonalNoGenBeanBusiness = new CausaPersonalNoGenBeanBusiness();

  private RegistroNoGenBeanBusiness registroNoGenBeanBusiness = new RegistroNoGenBeanBusiness();

  public Collection findCausaPersonalByClasificacionPersonalAndCausaMovimiento(long idMovimientoPersonal, long idCausaMovimiento)
    throws Exception
  {
    return this.causaPersonalNoGenBeanBusiness.findByClasificacionPersonalAndCausaMovimiento(idMovimientoPersonal, idCausaMovimiento);
  }

  public Collection findCausaPersonalByClasificacionPersonalAndCausaMovimientoAndSujetoLefp(long idMovimientoPersonal, long idCausaMovimiento, String sujetoLefp) throws Exception {
    return this.causaPersonalNoGenBeanBusiness.findByClasificacionPersonalAndCausaMovimientoAndSujetoLefp(idMovimientoPersonal, idCausaMovimiento, sujetoLefp);
  }

  public Collection findCausaPersonalByClasificacionPersonalAndCausaMovimientoAndSujetoLefpAndGrado99(long idMovimientoPersonal, long idCausaMovimiento, String sujetoLefp, String grado99) throws Exception {
    return this.causaPersonalNoGenBeanBusiness.findByClasificacionPersonalAndCausaMovimientoAndSujetoLefpAndGrado99(idMovimientoPersonal, idCausaMovimiento, sujetoLefp, grado99);
  }

  public Collection findRegistroByAprobacionMpd(long idOrganismo, String aprobacionMpd) throws Exception {
    return this.registroNoGenBeanBusiness.findByAprobacionMpd(idOrganismo, aprobacionMpd);
  }
}