package sigefirrhh.base.registro;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class CausaPersonalNoGenBeanBusiness extends CausaPersonalBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(CausaPersonalNoGenBeanBusiness.class.getName());

  public Collection findByClasificacionPersonalAndCausaMovimiento(long idClasificacionPersonal, long idMovimientoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "clasificacionPersonal.idClasificacionPersonal == pIdClasificacionPersonal && causaMovimiento.movimientoPersonal.idMovimientoPersonal == pIdMovimientoPersonal";

    Query query = pm.newQuery(CausaPersonal.class, filter);

    query.declareParameters("long pIdClasificacionPersonal, long pIdMovimientoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdClasificacionPersonal", new Long(idClasificacionPersonal));
    parameters.put("pIdMovimientoPersonal", new Long(idMovimientoPersonal));

    Collection colCausaPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colCausaPersonal;
  }

  public Collection findByClasificacionPersonalAndCausaMovimientoAndSujetoLefp(long idClasificacionPersonal, long idMovimientoPersonal, String sujetoLefp)
    throws Exception
  {
    this.log.error("idClasificacionPersonal " + idClasificacionPersonal);
    this.log.error("idMovimientoPersonal " + idMovimientoPersonal);
    this.log.error("SuejetoLefp " + sujetoLefp);
    PersistenceManager pm = PMThread.getPM();

    String filter = "clasificacionPersonal.idClasificacionPersonal == pIdClasificacionPersonal && causaMovimiento.movimientoPersonal.idMovimientoPersonal == pIdMovimientoPersonal && causaMovimiento.sujetoLefp == pSujetoLefp";

    Query query = pm.newQuery(CausaPersonal.class, filter);

    query.declareParameters("long pIdClasificacionPersonal, long pIdMovimientoPersonal, String pSujetoLefp");
    HashMap parameters = new HashMap();

    parameters.put("pIdClasificacionPersonal", new Long(idClasificacionPersonal));
    parameters.put("pIdMovimientoPersonal", new Long(idMovimientoPersonal));
    parameters.put("pSujetoLefp", sujetoLefp);

    Collection colCausaPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colCausaPersonal;
  }

  public Collection findByClasificacionPersonalAndCausaMovimientoAndSujetoLefpAndGrado99(long idClasificacionPersonal, long idMovimientoPersonal, String sujetoLefp, String grado99) throws Exception
  {
    this.log.error("idClasificacionPersonal " + idClasificacionPersonal);
    this.log.error("idMovimientoPersonal " + idMovimientoPersonal);
    this.log.error("SuejetoLefp " + sujetoLefp);
    PersistenceManager pm = PMThread.getPM();
    String todos = "A";
    String filter = "clasificacionPersonal.idClasificacionPersonal == pIdClasificacionPersonal && causaMovimiento.movimientoPersonal.idMovimientoPersonal == pIdMovimientoPersonal && causaMovimiento.sujetoLefp == pSujetoLefp && (causaMovimiento.grado99 == pGrado99 || causaMovimiento.grado99 == pTodos)";

    Query query = pm.newQuery(CausaPersonal.class, filter);

    query.declareParameters("long pIdClasificacionPersonal, long pIdMovimientoPersonal, String pSujetoLefp, String pGrado99, String pTodos");
    HashMap parameters = new HashMap();

    parameters.put("pIdClasificacionPersonal", new Long(idClasificacionPersonal));
    parameters.put("pIdMovimientoPersonal", new Long(idMovimientoPersonal));
    parameters.put("pSujetoLefp", sujetoLefp);
    parameters.put("pGrado99", grado99);
    parameters.put("pTodos", todos);

    Collection colCausaPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colCausaPersonal;
  }
}