package sigefirrhh.base.registro;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class CausaMovimientoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CausaMovimientoForm.class.getName());
  private CausaMovimiento causaMovimiento;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private RegistroFacade registroFacade = new RegistroFacade();
  private boolean showCausaMovimientoByMovimientoPersonal;
  private boolean showCausaMovimientoByCodCausaMovimiento;
  private boolean showCausaMovimientoByDescripcion;
  private String findSelectMovimientoPersonal;
  private String findCodCausaMovimiento;
  private String findDescripcion;
  private Collection findColMovimientoPersonal;
  private Collection colMovimientoPersonal;
  private String selectMovimientoPersonal;
  private Object stateResultCausaMovimientoByMovimientoPersonal = null;

  private Object stateResultCausaMovimientoByCodCausaMovimiento = null;

  private Object stateResultCausaMovimientoByDescripcion = null;

  public String getFindSelectMovimientoPersonal()
  {
    return this.findSelectMovimientoPersonal;
  }
  public void setFindSelectMovimientoPersonal(String valMovimientoPersonal) {
    this.findSelectMovimientoPersonal = valMovimientoPersonal;
  }

  public Collection getFindColMovimientoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColMovimientoPersonal.iterator();
    MovimientoPersonal movimientoPersonal = null;
    while (iterator.hasNext()) {
      movimientoPersonal = (MovimientoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(movimientoPersonal.getIdMovimientoPersonal()), 
        movimientoPersonal.toString()));
    }
    return col;
  }
  public String getFindCodCausaMovimiento() {
    return this.findCodCausaMovimiento;
  }
  public void setFindCodCausaMovimiento(String findCodCausaMovimiento) {
    this.findCodCausaMovimiento = findCodCausaMovimiento;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public String getSelectMovimientoPersonal()
  {
    return this.selectMovimientoPersonal;
  }
  public void setSelectMovimientoPersonal(String valMovimientoPersonal) {
    Iterator iterator = this.colMovimientoPersonal.iterator();
    MovimientoPersonal movimientoPersonal = null;
    this.causaMovimiento.setMovimientoPersonal(null);
    while (iterator.hasNext()) {
      movimientoPersonal = (MovimientoPersonal)iterator.next();
      if (String.valueOf(movimientoPersonal.getIdMovimientoPersonal()).equals(
        valMovimientoPersonal)) {
        this.causaMovimiento.setMovimientoPersonal(
          movimientoPersonal);
        break;
      }
    }
    this.selectMovimientoPersonal = valMovimientoPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public CausaMovimiento getCausaMovimiento() {
    if (this.causaMovimiento == null) {
      this.causaMovimiento = new CausaMovimiento();
    }
    return this.causaMovimiento;
  }

  public CausaMovimientoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColMovimientoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colMovimientoPersonal.iterator();
    MovimientoPersonal movimientoPersonal = null;
    while (iterator.hasNext()) {
      movimientoPersonal = (MovimientoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(movimientoPersonal.getIdMovimientoPersonal()), 
        movimientoPersonal.toString()));
    }
    return col;
  }

  public Collection getListSujetoLefp()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = CausaMovimiento.LISTA_SUJETO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListGrado99() {
    Collection col = new ArrayList();

    Iterator iterEntry = CausaMovimiento.LISTA_GRADO99.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColMovimientoPersonal = 
        this.registroFacade.findAllMovimientoPersonal();

      this.colMovimientoPersonal = 
        this.registroFacade.findAllMovimientoPersonal();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findCausaMovimientoByMovimientoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroFacade.findCausaMovimientoByMovimientoPersonal(Long.valueOf(this.findSelectMovimientoPersonal).longValue());
      this.showCausaMovimientoByMovimientoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCausaMovimientoByMovimientoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectMovimientoPersonal = null;
    this.findCodCausaMovimiento = null;
    this.findDescripcion = null;

    return null;
  }

  public String findCausaMovimientoByCodCausaMovimiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroFacade.findCausaMovimientoByCodCausaMovimiento(this.findCodCausaMovimiento);
      this.showCausaMovimientoByCodCausaMovimiento = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCausaMovimientoByCodCausaMovimiento)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectMovimientoPersonal = null;
    this.findCodCausaMovimiento = null;
    this.findDescripcion = null;

    return null;
  }

  public String findCausaMovimientoByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroFacade.findCausaMovimientoByDescripcion(this.findDescripcion);
      this.showCausaMovimientoByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCausaMovimientoByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectMovimientoPersonal = null;
    this.findCodCausaMovimiento = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowCausaMovimientoByMovimientoPersonal() {
    return this.showCausaMovimientoByMovimientoPersonal;
  }
  public boolean isShowCausaMovimientoByCodCausaMovimiento() {
    return this.showCausaMovimientoByCodCausaMovimiento;
  }
  public boolean isShowCausaMovimientoByDescripcion() {
    return this.showCausaMovimientoByDescripcion;
  }

  public String selectCausaMovimiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectMovimientoPersonal = null;

    long idCausaMovimiento = 
      Long.parseLong((String)requestParameterMap.get("idCausaMovimiento"));
    try
    {
      this.causaMovimiento = 
        this.registroFacade.findCausaMovimientoById(
        idCausaMovimiento);
      if (this.causaMovimiento.getMovimientoPersonal() != null) {
        this.selectMovimientoPersonal = 
          String.valueOf(this.causaMovimiento.getMovimientoPersonal().getIdMovimientoPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.causaMovimiento = null;
    this.showCausaMovimientoByMovimientoPersonal = false;
    this.showCausaMovimientoByCodCausaMovimiento = false;
    this.showCausaMovimientoByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.registroFacade.addCausaMovimiento(
          this.causaMovimiento);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.registroFacade.updateCausaMovimiento(
          this.causaMovimiento);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.registroFacade.deleteCausaMovimiento(
        this.causaMovimiento);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.causaMovimiento = new CausaMovimiento();

    this.selectMovimientoPersonal = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.causaMovimiento.setIdCausaMovimiento(identityGenerator.getNextSequenceNumber("sigefirrhh.base.registro.CausaMovimiento"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.causaMovimiento = new CausaMovimiento();
    return "cancel";
  }

  public boolean isShowGrado99Aux()
  {
    try
    {
      return (this.causaMovimiento.getSujetoLefp().equals("S")) || (this.causaMovimiento.getSujetoLefp().equals("A")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}