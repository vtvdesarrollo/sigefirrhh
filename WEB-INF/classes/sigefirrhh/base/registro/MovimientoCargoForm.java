package sigefirrhh.base.registro;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class MovimientoCargoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(MovimientoCargoForm.class.getName());
  private MovimientoCargo movimientoCargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private RegistroFacade registroFacade = new RegistroFacade();
  private boolean showMovimientoCargoByTipo;
  private String findTipo;
  private Object stateResultMovimientoCargoByTipo = null;

  public String getFindTipo()
  {
    return this.findTipo;
  }
  public void setFindTipo(String findTipo) {
    this.findTipo = findTipo;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public MovimientoCargo getMovimientoCargo() {
    if (this.movimientoCargo == null) {
      this.movimientoCargo = new MovimientoCargo();
    }
    return this.movimientoCargo;
  }

  public MovimientoCargoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListTipo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = MovimientoCargo.LISTA_TIPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findMovimientoCargoByTipo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroFacade.findMovimientoCargoByTipo(this.findTipo);
      this.showMovimientoCargoByTipo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showMovimientoCargoByTipo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findTipo = null;

    return null;
  }

  public boolean isShowMovimientoCargoByTipo() {
    return this.showMovimientoCargoByTipo;
  }

  public String selectMovimientoCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idMovimientoCargo = 
      Long.parseLong((String)requestParameterMap.get("idMovimientoCargo"));
    try
    {
      this.movimientoCargo = 
        this.registroFacade.findMovimientoCargoById(
        idMovimientoCargo);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.movimientoCargo = null;
    this.showMovimientoCargoByTipo = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.registroFacade.addMovimientoCargo(
          this.movimientoCargo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.registroFacade.updateMovimientoCargo(
          this.movimientoCargo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.registroFacade.deleteMovimientoCargo(
        this.movimientoCargo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.movimientoCargo = new MovimientoCargo();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.movimientoCargo.setIdMovimientoCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.base.registro.MovimientoCargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.movimientoCargo = new MovimientoCargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}