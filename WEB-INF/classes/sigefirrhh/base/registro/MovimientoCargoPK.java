package sigefirrhh.base.registro;

import java.io.Serializable;

public class MovimientoCargoPK
  implements Serializable
{
  public long idMovimientoCargo;

  public MovimientoCargoPK()
  {
  }

  public MovimientoCargoPK(long idMovimientoCargo)
  {
    this.idMovimientoCargo = idMovimientoCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((MovimientoCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(MovimientoCargoPK thatPK)
  {
    return 
      this.idMovimientoCargo == thatPK.idMovimientoCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idMovimientoCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idMovimientoCargo);
  }
}