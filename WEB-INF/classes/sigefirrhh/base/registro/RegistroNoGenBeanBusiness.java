package sigefirrhh.base.registro;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class RegistroNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByAprobacionMpd(long idOrganismo, String aprobacionMpd)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo && aprobacionMpd == pAprobacionMpd";

    Query query = pm.newQuery(Registro.class, filter);

    query.declareParameters("long pIdOrganismo, String pAprobacionMpd");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pAprobacionMpd", aprobacionMpd);

    query.setOrdering("numeroRegistro ascending");

    Collection colRegistro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistro);

    return colRegistro;
  }
}