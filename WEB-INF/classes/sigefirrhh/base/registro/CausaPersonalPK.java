package sigefirrhh.base.registro;

import java.io.Serializable;

public class CausaPersonalPK
  implements Serializable
{
  public long idCausaPersonal;

  public CausaPersonalPK()
  {
  }

  public CausaPersonalPK(long idCausaPersonal)
  {
    this.idCausaPersonal = idCausaPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CausaPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CausaPersonalPK thatPK)
  {
    return 
      this.idCausaPersonal == thatPK.idCausaPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCausaPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCausaPersonal);
  }
}