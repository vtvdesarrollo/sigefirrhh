package sigefirrhh.base.registro;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class MovimientoPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addMovimientoPersonal(MovimientoPersonal movimientoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    MovimientoPersonal movimientoPersonalNew = 
      (MovimientoPersonal)BeanUtils.cloneBean(
      movimientoPersonal);

    pm.makePersistent(movimientoPersonalNew);
  }

  public void updateMovimientoPersonal(MovimientoPersonal movimientoPersonal) throws Exception
  {
    MovimientoPersonal movimientoPersonalModify = 
      findMovimientoPersonalById(movimientoPersonal.getIdMovimientoPersonal());

    BeanUtils.copyProperties(movimientoPersonalModify, movimientoPersonal);
  }

  public void deleteMovimientoPersonal(MovimientoPersonal movimientoPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    MovimientoPersonal movimientoPersonalDelete = 
      findMovimientoPersonalById(movimientoPersonal.getIdMovimientoPersonal());
    pm.deletePersistent(movimientoPersonalDelete);
  }

  public MovimientoPersonal findMovimientoPersonalById(long idMovimientoPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idMovimientoPersonal == pIdMovimientoPersonal";
    Query query = pm.newQuery(MovimientoPersonal.class, filter);

    query.declareParameters("long pIdMovimientoPersonal");

    parameters.put("pIdMovimientoPersonal", new Long(idMovimientoPersonal));

    Collection colMovimientoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colMovimientoPersonal.iterator();
    return (MovimientoPersonal)iterator.next();
  }

  public Collection findMovimientoPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent movimientoPersonalExtent = pm.getExtent(
      MovimientoPersonal.class, true);
    Query query = pm.newQuery(movimientoPersonalExtent);
    query.setOrdering("codMovimientoPersonal ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodMovimientoPersonal(String codMovimientoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codMovimientoPersonal == pCodMovimientoPersonal";

    Query query = pm.newQuery(MovimientoPersonal.class, filter);

    query.declareParameters("java.lang.String pCodMovimientoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pCodMovimientoPersonal", new String(codMovimientoPersonal));

    query.setOrdering("codMovimientoPersonal ascending");

    Collection colMovimientoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMovimientoPersonal);

    return colMovimientoPersonal;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(MovimientoPersonal.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("codMovimientoPersonal ascending");

    Collection colMovimientoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMovimientoPersonal);

    return colMovimientoPersonal;
  }
}