package sigefirrhh.base.registro;

import java.io.Serializable;

public class MovimientoPersonalPK
  implements Serializable
{
  public long idMovimientoPersonal;

  public MovimientoPersonalPK()
  {
  }

  public MovimientoPersonalPK(long idMovimientoPersonal)
  {
    this.idMovimientoPersonal = idMovimientoPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((MovimientoPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(MovimientoPersonalPK thatPK)
  {
    return 
      this.idMovimientoPersonal == thatPK.idMovimientoPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idMovimientoPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idMovimientoPersonal);
  }
}