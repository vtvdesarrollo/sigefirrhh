package sigefirrhh.base.registro;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.base.estructura.GrupoOrganismoBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class RegistroBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRegistro(Registro registro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Registro registroNew = 
      (Registro)BeanUtils.cloneBean(
      registro);

    GrupoOrganismoBeanBusiness grupoOrganismoBeanBusiness = new GrupoOrganismoBeanBusiness();

    if (registroNew.getGrupoOrganismo() != null) {
      registroNew.setGrupoOrganismo(
        grupoOrganismoBeanBusiness.findGrupoOrganismoById(
        registroNew.getGrupoOrganismo().getIdGrupoOrganismo()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (registroNew.getOrganismo() != null) {
      registroNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        registroNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(registroNew);
  }

  public void updateRegistro(Registro registro) throws Exception
  {
    Registro registroModify = 
      findRegistroById(registro.getIdRegistro());

    GrupoOrganismoBeanBusiness grupoOrganismoBeanBusiness = new GrupoOrganismoBeanBusiness();

    if (registro.getGrupoOrganismo() != null) {
      registro.setGrupoOrganismo(
        grupoOrganismoBeanBusiness.findGrupoOrganismoById(
        registro.getGrupoOrganismo().getIdGrupoOrganismo()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (registro.getOrganismo() != null) {
      registro.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        registro.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(registroModify, registro);
  }

  public void deleteRegistro(Registro registro) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Registro registroDelete = 
      findRegistroById(registro.getIdRegistro());
    pm.deletePersistent(registroDelete);
  }

  public Registro findRegistroById(long idRegistro) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRegistro == pIdRegistro";
    Query query = pm.newQuery(Registro.class, filter);

    query.declareParameters("long pIdRegistro");

    parameters.put("pIdRegistro", new Long(idRegistro));

    Collection colRegistro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRegistro.iterator();
    return (Registro)iterator.next();
  }

  public Collection findRegistroAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent registroExtent = pm.getExtent(
      Registro.class, true);
    Query query = pm.newQuery(registroExtent);
    query.setOrdering("numeroRegistro ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByNumeroRegistro(int numeroRegistro, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "numeroRegistro == pNumeroRegistro &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Registro.class, filter);

    query.declareParameters("int pNumeroRegistro, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNumeroRegistro", new Integer(numeroRegistro));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("numeroRegistro ascending");

    Collection colRegistro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistro);

    return colRegistro;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Registro.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("numeroRegistro ascending");

    Collection colRegistro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistro);

    return colRegistro;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Registro.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("numeroRegistro ascending");

    Collection colRegistro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistro);

    return colRegistro;
  }
}