package sigefirrhh.base.registro;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class MovimientoPersonal
  implements Serializable, PersistenceCapable
{
  private long idMovimientoPersonal;
  private String codMovimientoPersonal;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codMovimientoPersonal", "descripcion", "idMovimientoPersonal" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodMovimientoPersonal(this);
  }

  public String getCodMovimientoPersonal() {
    return jdoGetcodMovimientoPersonal(this);
  }

  public void setCodMovimientoPersonal(String codMovimientoPersonal) {
    jdoSetcodMovimientoPersonal(this, codMovimientoPersonal);
  }

  public String getDescripcion() {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion) {
    jdoSetdescripcion(this, descripcion);
  }

  public long getIdMovimientoPersonal() {
    return jdoGetidMovimientoPersonal(this);
  }

  public void setIdMovimientoPersonal(long idMovimientoPersonal) {
    jdoSetidMovimientoPersonal(this, idMovimientoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.registro.MovimientoPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new MovimientoPersonal());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    MovimientoPersonal localMovimientoPersonal = new MovimientoPersonal();
    localMovimientoPersonal.jdoFlags = 1;
    localMovimientoPersonal.jdoStateManager = paramStateManager;
    return localMovimientoPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    MovimientoPersonal localMovimientoPersonal = new MovimientoPersonal();
    localMovimientoPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localMovimientoPersonal.jdoFlags = 1;
    localMovimientoPersonal.jdoStateManager = paramStateManager;
    return localMovimientoPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codMovimientoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idMovimientoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codMovimientoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idMovimientoPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(MovimientoPersonal paramMovimientoPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramMovimientoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.codMovimientoPersonal = paramMovimientoPersonal.codMovimientoPersonal;
      return;
    case 1:
      if (paramMovimientoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramMovimientoPersonal.descripcion;
      return;
    case 2:
      if (paramMovimientoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idMovimientoPersonal = paramMovimientoPersonal.idMovimientoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof MovimientoPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    MovimientoPersonal localMovimientoPersonal = (MovimientoPersonal)paramObject;
    if (localMovimientoPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localMovimientoPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new MovimientoPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new MovimientoPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MovimientoPersonalPK))
      throw new IllegalArgumentException("arg1");
    MovimientoPersonalPK localMovimientoPersonalPK = (MovimientoPersonalPK)paramObject;
    localMovimientoPersonalPK.idMovimientoPersonal = this.idMovimientoPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MovimientoPersonalPK))
      throw new IllegalArgumentException("arg1");
    MovimientoPersonalPK localMovimientoPersonalPK = (MovimientoPersonalPK)paramObject;
    this.idMovimientoPersonal = localMovimientoPersonalPK.idMovimientoPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MovimientoPersonalPK))
      throw new IllegalArgumentException("arg2");
    MovimientoPersonalPK localMovimientoPersonalPK = (MovimientoPersonalPK)paramObject;
    localMovimientoPersonalPK.idMovimientoPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MovimientoPersonalPK))
      throw new IllegalArgumentException("arg2");
    MovimientoPersonalPK localMovimientoPersonalPK = (MovimientoPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localMovimientoPersonalPK.idMovimientoPersonal);
  }

  private static final String jdoGetcodMovimientoPersonal(MovimientoPersonal paramMovimientoPersonal)
  {
    if (paramMovimientoPersonal.jdoFlags <= 0)
      return paramMovimientoPersonal.codMovimientoPersonal;
    StateManager localStateManager = paramMovimientoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoPersonal.codMovimientoPersonal;
    if (localStateManager.isLoaded(paramMovimientoPersonal, jdoInheritedFieldCount + 0))
      return paramMovimientoPersonal.codMovimientoPersonal;
    return localStateManager.getStringField(paramMovimientoPersonal, jdoInheritedFieldCount + 0, paramMovimientoPersonal.codMovimientoPersonal);
  }

  private static final void jdoSetcodMovimientoPersonal(MovimientoPersonal paramMovimientoPersonal, String paramString)
  {
    if (paramMovimientoPersonal.jdoFlags == 0)
    {
      paramMovimientoPersonal.codMovimientoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoPersonal.codMovimientoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoPersonal, jdoInheritedFieldCount + 0, paramMovimientoPersonal.codMovimientoPersonal, paramString);
  }

  private static final String jdoGetdescripcion(MovimientoPersonal paramMovimientoPersonal)
  {
    if (paramMovimientoPersonal.jdoFlags <= 0)
      return paramMovimientoPersonal.descripcion;
    StateManager localStateManager = paramMovimientoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoPersonal.descripcion;
    if (localStateManager.isLoaded(paramMovimientoPersonal, jdoInheritedFieldCount + 1))
      return paramMovimientoPersonal.descripcion;
    return localStateManager.getStringField(paramMovimientoPersonal, jdoInheritedFieldCount + 1, paramMovimientoPersonal.descripcion);
  }

  private static final void jdoSetdescripcion(MovimientoPersonal paramMovimientoPersonal, String paramString)
  {
    if (paramMovimientoPersonal.jdoFlags == 0)
    {
      paramMovimientoPersonal.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoPersonal.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoPersonal, jdoInheritedFieldCount + 1, paramMovimientoPersonal.descripcion, paramString);
  }

  private static final long jdoGetidMovimientoPersonal(MovimientoPersonal paramMovimientoPersonal)
  {
    return paramMovimientoPersonal.idMovimientoPersonal;
  }

  private static final void jdoSetidMovimientoPersonal(MovimientoPersonal paramMovimientoPersonal, long paramLong)
  {
    StateManager localStateManager = paramMovimientoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoPersonal.idMovimientoPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramMovimientoPersonal, jdoInheritedFieldCount + 2, paramMovimientoPersonal.idMovimientoPersonal, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}