package sigefirrhh.base.registro;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class MovimientoPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(MovimientoPersonalForm.class.getName());
  private MovimientoPersonal movimientoPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private RegistroFacade registroFacade = new RegistroFacade();
  private boolean showMovimientoPersonalByCodMovimientoPersonal;
  private boolean showMovimientoPersonalByDescripcion;
  private String findCodMovimientoPersonal;
  private String findDescripcion;
  private Object stateResultMovimientoPersonalByCodMovimientoPersonal = null;

  private Object stateResultMovimientoPersonalByDescripcion = null;

  public String getFindCodMovimientoPersonal()
  {
    return this.findCodMovimientoPersonal;
  }
  public void setFindCodMovimientoPersonal(String findCodMovimientoPersonal) {
    this.findCodMovimientoPersonal = findCodMovimientoPersonal;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public MovimientoPersonal getMovimientoPersonal() {
    if (this.movimientoPersonal == null) {
      this.movimientoPersonal = new MovimientoPersonal();
    }
    return this.movimientoPersonal;
  }

  public MovimientoPersonalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findMovimientoPersonalByCodMovimientoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroFacade.findMovimientoPersonalByCodMovimientoPersonal(this.findCodMovimientoPersonal);
      this.showMovimientoPersonalByCodMovimientoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showMovimientoPersonalByCodMovimientoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodMovimientoPersonal = null;
    this.findDescripcion = null;

    return null;
  }

  public String findMovimientoPersonalByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroFacade.findMovimientoPersonalByDescripcion(this.findDescripcion);
      this.showMovimientoPersonalByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showMovimientoPersonalByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodMovimientoPersonal = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowMovimientoPersonalByCodMovimientoPersonal() {
    return this.showMovimientoPersonalByCodMovimientoPersonal;
  }
  public boolean isShowMovimientoPersonalByDescripcion() {
    return this.showMovimientoPersonalByDescripcion;
  }

  public String selectMovimientoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idMovimientoPersonal = 
      Long.parseLong((String)requestParameterMap.get("idMovimientoPersonal"));
    try
    {
      this.movimientoPersonal = 
        this.registroFacade.findMovimientoPersonalById(
        idMovimientoPersonal);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.movimientoPersonal = null;
    this.showMovimientoPersonalByCodMovimientoPersonal = false;
    this.showMovimientoPersonalByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.registroFacade.addMovimientoPersonal(
          this.movimientoPersonal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.registroFacade.updateMovimientoPersonal(
          this.movimientoPersonal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.registroFacade.deleteMovimientoPersonal(
        this.movimientoPersonal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.movimientoPersonal = new MovimientoPersonal();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.movimientoPersonal.setIdMovimientoPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.base.registro.MovimientoPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.movimientoPersonal = new MovimientoPersonal();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}