package sigefirrhh.base.registro;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.base.estructura.Organismo;

public class Registro
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idRegistro;
  private int numeroRegistro;
  private String nombre;
  private String aprobacionMpd;
  private GrupoOrganismo grupoOrganismo;
  private int anio;
  private Date fechaVigencia;
  private Organismo organismo;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "aprobacionMpd", "fechaVigencia", "grupoOrganismo", "idRegistro", "idSitp", "nombre", "numeroRegistro", "organismo", "tiempoSitp" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.estructura.GrupoOrganismo"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.util.Date") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 24, 21, 21, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.registro.Registro"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Registro());

    LISTA_SI_NO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public Registro()
  {
    jdoSetaprobacionMpd(this, "N");

    jdoSetanio(this, 0);
  }

  public String toString()
  {
    return jdoGetnombre(this);
  }

  public String getAprobacionMpd()
  {
    return jdoGetaprobacionMpd(this);
  }

  public GrupoOrganismo getGrupoOrganismo()
  {
    return jdoGetgrupoOrganismo(this);
  }

  public long getIdRegistro()
  {
    return jdoGetidRegistro(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public int getNumeroRegistro()
  {
    return jdoGetnumeroRegistro(this);
  }

  public void setAprobacionMpd(String string)
  {
    jdoSetaprobacionMpd(this, string);
  }

  public void setGrupoOrganismo(GrupoOrganismo organismo)
  {
    jdoSetgrupoOrganismo(this, organismo);
  }

  public void setIdRegistro(long l)
  {
    jdoSetidRegistro(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setNumeroRegistro(int i)
  {
    jdoSetnumeroRegistro(this, i);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public Date getFechaVigencia() {
    return jdoGetfechaVigencia(this);
  }
  public void setFechaVigencia(Date fechaVigencia) {
    jdoSetfechaVigencia(this, fechaVigencia);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Registro localRegistro = new Registro();
    localRegistro.jdoFlags = 1;
    localRegistro.jdoStateManager = paramStateManager;
    return localRegistro;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Registro localRegistro = new Registro();
    localRegistro.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRegistro.jdoFlags = 1;
    localRegistro.jdoStateManager = paramStateManager;
    return localRegistro;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoOrganismo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRegistro);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroRegistro);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoOrganismo = ((GrupoOrganismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRegistro = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroRegistro = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Registro paramRegistro, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRegistro == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramRegistro.anio;
      return;
    case 1:
      if (paramRegistro == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramRegistro.aprobacionMpd;
      return;
    case 2:
      if (paramRegistro == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramRegistro.fechaVigencia;
      return;
    case 3:
      if (paramRegistro == null)
        throw new IllegalArgumentException("arg1");
      this.grupoOrganismo = paramRegistro.grupoOrganismo;
      return;
    case 4:
      if (paramRegistro == null)
        throw new IllegalArgumentException("arg1");
      this.idRegistro = paramRegistro.idRegistro;
      return;
    case 5:
      if (paramRegistro == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramRegistro.idSitp;
      return;
    case 6:
      if (paramRegistro == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramRegistro.nombre;
      return;
    case 7:
      if (paramRegistro == null)
        throw new IllegalArgumentException("arg1");
      this.numeroRegistro = paramRegistro.numeroRegistro;
      return;
    case 8:
      if (paramRegistro == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramRegistro.organismo;
      return;
    case 9:
      if (paramRegistro == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramRegistro.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Registro))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Registro localRegistro = (Registro)paramObject;
    if (localRegistro.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRegistro, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RegistroPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RegistroPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroPK))
      throw new IllegalArgumentException("arg1");
    RegistroPK localRegistroPK = (RegistroPK)paramObject;
    localRegistroPK.idRegistro = this.idRegistro;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroPK))
      throw new IllegalArgumentException("arg1");
    RegistroPK localRegistroPK = (RegistroPK)paramObject;
    this.idRegistro = localRegistroPK.idRegistro;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroPK))
      throw new IllegalArgumentException("arg2");
    RegistroPK localRegistroPK = (RegistroPK)paramObject;
    localRegistroPK.idRegistro = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroPK))
      throw new IllegalArgumentException("arg2");
    RegistroPK localRegistroPK = (RegistroPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localRegistroPK.idRegistro);
  }

  private static final int jdoGetanio(Registro paramRegistro)
  {
    if (paramRegistro.jdoFlags <= 0)
      return paramRegistro.anio;
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
      return paramRegistro.anio;
    if (localStateManager.isLoaded(paramRegistro, jdoInheritedFieldCount + 0))
      return paramRegistro.anio;
    return localStateManager.getIntField(paramRegistro, jdoInheritedFieldCount + 0, paramRegistro.anio);
  }

  private static final void jdoSetanio(Registro paramRegistro, int paramInt)
  {
    if (paramRegistro.jdoFlags == 0)
    {
      paramRegistro.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistro.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistro, jdoInheritedFieldCount + 0, paramRegistro.anio, paramInt);
  }

  private static final String jdoGetaprobacionMpd(Registro paramRegistro)
  {
    if (paramRegistro.jdoFlags <= 0)
      return paramRegistro.aprobacionMpd;
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
      return paramRegistro.aprobacionMpd;
    if (localStateManager.isLoaded(paramRegistro, jdoInheritedFieldCount + 1))
      return paramRegistro.aprobacionMpd;
    return localStateManager.getStringField(paramRegistro, jdoInheritedFieldCount + 1, paramRegistro.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(Registro paramRegistro, String paramString)
  {
    if (paramRegistro.jdoFlags == 0)
    {
      paramRegistro.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistro.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistro, jdoInheritedFieldCount + 1, paramRegistro.aprobacionMpd, paramString);
  }

  private static final Date jdoGetfechaVigencia(Registro paramRegistro)
  {
    if (paramRegistro.jdoFlags <= 0)
      return paramRegistro.fechaVigencia;
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
      return paramRegistro.fechaVigencia;
    if (localStateManager.isLoaded(paramRegistro, jdoInheritedFieldCount + 2))
      return paramRegistro.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramRegistro, jdoInheritedFieldCount + 2, paramRegistro.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(Registro paramRegistro, Date paramDate)
  {
    if (paramRegistro.jdoFlags == 0)
    {
      paramRegistro.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistro.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistro, jdoInheritedFieldCount + 2, paramRegistro.fechaVigencia, paramDate);
  }

  private static final GrupoOrganismo jdoGetgrupoOrganismo(Registro paramRegistro)
  {
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
      return paramRegistro.grupoOrganismo;
    if (localStateManager.isLoaded(paramRegistro, jdoInheritedFieldCount + 3))
      return paramRegistro.grupoOrganismo;
    return (GrupoOrganismo)localStateManager.getObjectField(paramRegistro, jdoInheritedFieldCount + 3, paramRegistro.grupoOrganismo);
  }

  private static final void jdoSetgrupoOrganismo(Registro paramRegistro, GrupoOrganismo paramGrupoOrganismo)
  {
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistro.grupoOrganismo = paramGrupoOrganismo;
      return;
    }
    localStateManager.setObjectField(paramRegistro, jdoInheritedFieldCount + 3, paramRegistro.grupoOrganismo, paramGrupoOrganismo);
  }

  private static final long jdoGetidRegistro(Registro paramRegistro)
  {
    return paramRegistro.idRegistro;
  }

  private static final void jdoSetidRegistro(Registro paramRegistro, long paramLong)
  {
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistro.idRegistro = paramLong;
      return;
    }
    localStateManager.setLongField(paramRegistro, jdoInheritedFieldCount + 4, paramRegistro.idRegistro, paramLong);
  }

  private static final int jdoGetidSitp(Registro paramRegistro)
  {
    if (paramRegistro.jdoFlags <= 0)
      return paramRegistro.idSitp;
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
      return paramRegistro.idSitp;
    if (localStateManager.isLoaded(paramRegistro, jdoInheritedFieldCount + 5))
      return paramRegistro.idSitp;
    return localStateManager.getIntField(paramRegistro, jdoInheritedFieldCount + 5, paramRegistro.idSitp);
  }

  private static final void jdoSetidSitp(Registro paramRegistro, int paramInt)
  {
    if (paramRegistro.jdoFlags == 0)
    {
      paramRegistro.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistro.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistro, jdoInheritedFieldCount + 5, paramRegistro.idSitp, paramInt);
  }

  private static final String jdoGetnombre(Registro paramRegistro)
  {
    if (paramRegistro.jdoFlags <= 0)
      return paramRegistro.nombre;
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
      return paramRegistro.nombre;
    if (localStateManager.isLoaded(paramRegistro, jdoInheritedFieldCount + 6))
      return paramRegistro.nombre;
    return localStateManager.getStringField(paramRegistro, jdoInheritedFieldCount + 6, paramRegistro.nombre);
  }

  private static final void jdoSetnombre(Registro paramRegistro, String paramString)
  {
    if (paramRegistro.jdoFlags == 0)
    {
      paramRegistro.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistro.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistro, jdoInheritedFieldCount + 6, paramRegistro.nombre, paramString);
  }

  private static final int jdoGetnumeroRegistro(Registro paramRegistro)
  {
    if (paramRegistro.jdoFlags <= 0)
      return paramRegistro.numeroRegistro;
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
      return paramRegistro.numeroRegistro;
    if (localStateManager.isLoaded(paramRegistro, jdoInheritedFieldCount + 7))
      return paramRegistro.numeroRegistro;
    return localStateManager.getIntField(paramRegistro, jdoInheritedFieldCount + 7, paramRegistro.numeroRegistro);
  }

  private static final void jdoSetnumeroRegistro(Registro paramRegistro, int paramInt)
  {
    if (paramRegistro.jdoFlags == 0)
    {
      paramRegistro.numeroRegistro = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistro.numeroRegistro = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistro, jdoInheritedFieldCount + 7, paramRegistro.numeroRegistro, paramInt);
  }

  private static final Organismo jdoGetorganismo(Registro paramRegistro)
  {
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
      return paramRegistro.organismo;
    if (localStateManager.isLoaded(paramRegistro, jdoInheritedFieldCount + 8))
      return paramRegistro.organismo;
    return (Organismo)localStateManager.getObjectField(paramRegistro, jdoInheritedFieldCount + 8, paramRegistro.organismo);
  }

  private static final void jdoSetorganismo(Registro paramRegistro, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistro.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramRegistro, jdoInheritedFieldCount + 8, paramRegistro.organismo, paramOrganismo);
  }

  private static final Date jdoGettiempoSitp(Registro paramRegistro)
  {
    if (paramRegistro.jdoFlags <= 0)
      return paramRegistro.tiempoSitp;
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
      return paramRegistro.tiempoSitp;
    if (localStateManager.isLoaded(paramRegistro, jdoInheritedFieldCount + 9))
      return paramRegistro.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramRegistro, jdoInheritedFieldCount + 9, paramRegistro.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Registro paramRegistro, Date paramDate)
  {
    if (paramRegistro.jdoFlags == 0)
    {
      paramRegistro.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistro.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistro.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistro, jdoInheritedFieldCount + 9, paramRegistro.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}