package sigefirrhh.base.registro;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class RegistroPersonal
  implements Serializable, PersistenceCapable
{
  private long idRegistroPersonal;
  private TipoPersonal tipoPersonal;
  private Registro registro;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idRegistroPersonal", "idSitp", "registro", "tiempoSitp", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.registro.Registro"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") };
  private static final byte[] jdoFieldFlags = { 24, 21, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGettipoPersonal(this).getNombre() + " - " + jdoGetregistro(this).getNombre();
  }

  public long getIdRegistroPersonal()
  {
    return jdoGetidRegistroPersonal(this);
  }

  public Registro getRegistro()
  {
    return jdoGetregistro(this);
  }

  public void setIdRegistroPersonal(long l)
  {
    jdoSetidRegistroPersonal(this, l);
  }

  public void setRegistro(Registro registro)
  {
    jdoSetregistro(this, registro);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.registro.RegistroPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RegistroPersonal());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RegistroPersonal localRegistroPersonal = new RegistroPersonal();
    localRegistroPersonal.jdoFlags = 1;
    localRegistroPersonal.jdoStateManager = paramStateManager;
    return localRegistroPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RegistroPersonal localRegistroPersonal = new RegistroPersonal();
    localRegistroPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRegistroPersonal.jdoFlags = 1;
    localRegistroPersonal.jdoStateManager = paramStateManager;
    return localRegistroPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRegistroPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registro);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRegistroPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registro = ((Registro)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RegistroPersonal paramRegistroPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRegistroPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idRegistroPersonal = paramRegistroPersonal.idRegistroPersonal;
      return;
    case 1:
      if (paramRegistroPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramRegistroPersonal.idSitp;
      return;
    case 2:
      if (paramRegistroPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.registro = paramRegistroPersonal.registro;
      return;
    case 3:
      if (paramRegistroPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramRegistroPersonal.tiempoSitp;
      return;
    case 4:
      if (paramRegistroPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramRegistroPersonal.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RegistroPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RegistroPersonal localRegistroPersonal = (RegistroPersonal)paramObject;
    if (localRegistroPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRegistroPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RegistroPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RegistroPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroPersonalPK))
      throw new IllegalArgumentException("arg1");
    RegistroPersonalPK localRegistroPersonalPK = (RegistroPersonalPK)paramObject;
    localRegistroPersonalPK.idRegistroPersonal = this.idRegistroPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroPersonalPK))
      throw new IllegalArgumentException("arg1");
    RegistroPersonalPK localRegistroPersonalPK = (RegistroPersonalPK)paramObject;
    this.idRegistroPersonal = localRegistroPersonalPK.idRegistroPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroPersonalPK))
      throw new IllegalArgumentException("arg2");
    RegistroPersonalPK localRegistroPersonalPK = (RegistroPersonalPK)paramObject;
    localRegistroPersonalPK.idRegistroPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroPersonalPK))
      throw new IllegalArgumentException("arg2");
    RegistroPersonalPK localRegistroPersonalPK = (RegistroPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localRegistroPersonalPK.idRegistroPersonal);
  }

  private static final long jdoGetidRegistroPersonal(RegistroPersonal paramRegistroPersonal)
  {
    return paramRegistroPersonal.idRegistroPersonal;
  }

  private static final void jdoSetidRegistroPersonal(RegistroPersonal paramRegistroPersonal, long paramLong)
  {
    StateManager localStateManager = paramRegistroPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroPersonal.idRegistroPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramRegistroPersonal, jdoInheritedFieldCount + 0, paramRegistroPersonal.idRegistroPersonal, paramLong);
  }

  private static final int jdoGetidSitp(RegistroPersonal paramRegistroPersonal)
  {
    if (paramRegistroPersonal.jdoFlags <= 0)
      return paramRegistroPersonal.idSitp;
    StateManager localStateManager = paramRegistroPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroPersonal.idSitp;
    if (localStateManager.isLoaded(paramRegistroPersonal, jdoInheritedFieldCount + 1))
      return paramRegistroPersonal.idSitp;
    return localStateManager.getIntField(paramRegistroPersonal, jdoInheritedFieldCount + 1, paramRegistroPersonal.idSitp);
  }

  private static final void jdoSetidSitp(RegistroPersonal paramRegistroPersonal, int paramInt)
  {
    if (paramRegistroPersonal.jdoFlags == 0)
    {
      paramRegistroPersonal.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroPersonal.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroPersonal, jdoInheritedFieldCount + 1, paramRegistroPersonal.idSitp, paramInt);
  }

  private static final Registro jdoGetregistro(RegistroPersonal paramRegistroPersonal)
  {
    StateManager localStateManager = paramRegistroPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroPersonal.registro;
    if (localStateManager.isLoaded(paramRegistroPersonal, jdoInheritedFieldCount + 2))
      return paramRegistroPersonal.registro;
    return (Registro)localStateManager.getObjectField(paramRegistroPersonal, jdoInheritedFieldCount + 2, paramRegistroPersonal.registro);
  }

  private static final void jdoSetregistro(RegistroPersonal paramRegistroPersonal, Registro paramRegistro)
  {
    StateManager localStateManager = paramRegistroPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroPersonal.registro = paramRegistro;
      return;
    }
    localStateManager.setObjectField(paramRegistroPersonal, jdoInheritedFieldCount + 2, paramRegistroPersonal.registro, paramRegistro);
  }

  private static final Date jdoGettiempoSitp(RegistroPersonal paramRegistroPersonal)
  {
    if (paramRegistroPersonal.jdoFlags <= 0)
      return paramRegistroPersonal.tiempoSitp;
    StateManager localStateManager = paramRegistroPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroPersonal.tiempoSitp;
    if (localStateManager.isLoaded(paramRegistroPersonal, jdoInheritedFieldCount + 3))
      return paramRegistroPersonal.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramRegistroPersonal, jdoInheritedFieldCount + 3, paramRegistroPersonal.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(RegistroPersonal paramRegistroPersonal, Date paramDate)
  {
    if (paramRegistroPersonal.jdoFlags == 0)
    {
      paramRegistroPersonal.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroPersonal.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroPersonal, jdoInheritedFieldCount + 3, paramRegistroPersonal.tiempoSitp, paramDate);
  }

  private static final TipoPersonal jdoGettipoPersonal(RegistroPersonal paramRegistroPersonal)
  {
    StateManager localStateManager = paramRegistroPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroPersonal.tipoPersonal;
    if (localStateManager.isLoaded(paramRegistroPersonal, jdoInheritedFieldCount + 4))
      return paramRegistroPersonal.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramRegistroPersonal, jdoInheritedFieldCount + 4, paramRegistroPersonal.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(RegistroPersonal paramRegistroPersonal, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramRegistroPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroPersonal.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramRegistroPersonal, jdoInheritedFieldCount + 4, paramRegistroPersonal.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}