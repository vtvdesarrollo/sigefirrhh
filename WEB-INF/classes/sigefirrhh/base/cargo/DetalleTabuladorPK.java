package sigefirrhh.base.cargo;

import java.io.Serializable;

public class DetalleTabuladorPK
  implements Serializable
{
  public long idDetalleTabulador;

  public DetalleTabuladorPK()
  {
  }

  public DetalleTabuladorPK(long idDetalleTabulador)
  {
    this.idDetalleTabulador = idDetalleTabulador;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DetalleTabuladorPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DetalleTabuladorPK thatPK)
  {
    return 
      this.idDetalleTabulador == thatPK.idDetalleTabulador;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDetalleTabulador)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDetalleTabulador);
  }
}