package sigefirrhh.base.cargo;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.jdo.PersistenceManager;

public class CargoFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private CargoBusiness cargoBusiness = new CargoBusiness();

  public void addCargo(Cargo cargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.cargoBusiness.addCargo(cargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCargo(Cargo cargo) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.updateCargo(cargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCargo(Cargo cargo) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.deleteCargo(cargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Cargo findCargoById(long cargoId) throws Exception
  {
    try { this.txn.open();
      Cargo cargo = 
        this.cargoBusiness.findCargoById(cargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(cargo);
      return cargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCargo() throws Exception
  {
    try { this.txn.open();
      return this.cargoBusiness.findAllCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCargoByManualCargo(long idManualCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findCargoByManualCargo(idManualCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCargoByCodCargo(String codCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findCargoByCodCargo(codCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCargoByDescripcionCargo(String descripcionCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findCargoByDescripcionCargo(descripcionCargo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDetalleTabulador(DetalleTabulador detalleTabulador)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.cargoBusiness.addDetalleTabulador(detalleTabulador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDetalleTabulador(DetalleTabulador detalleTabulador) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.updateDetalleTabulador(detalleTabulador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDetalleTabulador(DetalleTabulador detalleTabulador) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.deleteDetalleTabulador(detalleTabulador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DetalleTabulador findDetalleTabuladorById(long detalleTabuladorId) throws Exception
  {
    try { this.txn.open();
      DetalleTabulador detalleTabulador = 
        this.cargoBusiness.findDetalleTabuladorById(detalleTabuladorId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(detalleTabulador);
      return detalleTabulador;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDetalleTabulador() throws Exception
  {
    try { this.txn.open();
      return this.cargoBusiness.findAllDetalleTabulador();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDetalleTabuladorByTabulador(long idTabulador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findDetalleTabuladorByTabulador(idTabulador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDetalleTabuladorAnios(DetalleTabuladorAnios detalleTabuladorAnios)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.cargoBusiness.addDetalleTabuladorAnios(detalleTabuladorAnios);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDetalleTabuladorAnios(DetalleTabuladorAnios detalleTabuladorAnios) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.updateDetalleTabuladorAnios(detalleTabuladorAnios);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDetalleTabuladorAnios(DetalleTabuladorAnios detalleTabuladorAnios) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.deleteDetalleTabuladorAnios(detalleTabuladorAnios);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DetalleTabuladorAnios findDetalleTabuladorAniosById(long detalleTabuladorAniosId) throws Exception
  {
    try { this.txn.open();
      DetalleTabuladorAnios detalleTabuladorAnios = 
        this.cargoBusiness.findDetalleTabuladorAniosById(detalleTabuladorAniosId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(detalleTabuladorAnios);
      return detalleTabuladorAnios;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDetalleTabuladorAnios() throws Exception
  {
    try { this.txn.open();
      return this.cargoBusiness.findAllDetalleTabuladorAnios();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDetalleTabuladorAniosByAnioDesde(int anioDesde)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findDetalleTabuladorAniosByAnioDesde(anioDesde);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDetalleTabuladorAniosByAnioHasta(int anioHasta)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findDetalleTabuladorAniosByAnioHasta(anioHasta);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDetalleTabuladorAniosByPaso(int paso)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findDetalleTabuladorAniosByPaso(paso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDetalleTabuladorAniosByHoras(int horas)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findDetalleTabuladorAniosByHoras(horas);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDetalleTabuladorAniosByTabulador(long idTabulador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findDetalleTabuladorAniosByTabulador(idTabulador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addGrupoOcupacional(GrupoOcupacional grupoOcupacional)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.cargoBusiness.addGrupoOcupacional(grupoOcupacional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateGrupoOcupacional(GrupoOcupacional grupoOcupacional) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.updateGrupoOcupacional(grupoOcupacional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteGrupoOcupacional(GrupoOcupacional grupoOcupacional) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.deleteGrupoOcupacional(grupoOcupacional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public GrupoOcupacional findGrupoOcupacionalById(long grupoOcupacionalId) throws Exception
  {
    try { this.txn.open();
      GrupoOcupacional grupoOcupacional = 
        this.cargoBusiness.findGrupoOcupacionalById(grupoOcupacionalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(grupoOcupacional);
      return grupoOcupacional;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllGrupoOcupacional() throws Exception
  {
    try { this.txn.open();
      return this.cargoBusiness.findAllGrupoOcupacional();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoOcupacionalByCodGrupoOcupacional(String codGrupoOcupacional)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findGrupoOcupacionalByCodGrupoOcupacional(codGrupoOcupacional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoOcupacionalByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findGrupoOcupacionalByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoOcupacionalByRamoOcupacional(long idRamoOcupacional)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findGrupoOcupacionalByRamoOcupacional(idRamoOcupacional);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addManualCargo(ManualCargo manualCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.cargoBusiness.addManualCargo(manualCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateManualCargo(ManualCargo manualCargo) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.updateManualCargo(manualCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteManualCargo(ManualCargo manualCargo) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.deleteManualCargo(manualCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ManualCargo findManualCargoById(long manualCargoId) throws Exception
  {
    try { this.txn.open();
      ManualCargo manualCargo = 
        this.cargoBusiness.findManualCargoById(manualCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(manualCargo);
      return manualCargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllManualCargo() throws Exception
  {
    try { this.txn.open();
      return this.cargoBusiness.findAllManualCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findManualCargoByCodManualCargo(int codManualCargo, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findManualCargoByCodManualCargo(codManualCargo, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findManualCargoByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findManualCargoByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findManualCargoByTipoCargo(String tipoCargo, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findManualCargoByTipoCargo(tipoCargo, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findManualCargoByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findManualCargoByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addManualPersonal(ManualPersonal manualPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.cargoBusiness.addManualPersonal(manualPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateManualPersonal(ManualPersonal manualPersonal) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.updateManualPersonal(manualPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteManualPersonal(ManualPersonal manualPersonal) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.deleteManualPersonal(manualPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ManualPersonal findManualPersonalById(long manualPersonalId) throws Exception
  {
    try { this.txn.open();
      ManualPersonal manualPersonal = 
        this.cargoBusiness.findManualPersonalById(manualPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(manualPersonal);
      return manualPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllManualPersonal() throws Exception
  {
    try { this.txn.open();
      return this.cargoBusiness.findAllManualPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findManualPersonalByManualCargo(long idManualCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findManualPersonalByManualCargo(idManualCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findManualPersonalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findManualPersonalByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRamoOcupacional(RamoOcupacional ramoOcupacional)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.cargoBusiness.addRamoOcupacional(ramoOcupacional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRamoOcupacional(RamoOcupacional ramoOcupacional) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.updateRamoOcupacional(ramoOcupacional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRamoOcupacional(RamoOcupacional ramoOcupacional) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.deleteRamoOcupacional(ramoOcupacional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RamoOcupacional findRamoOcupacionalById(long ramoOcupacionalId) throws Exception
  {
    try { this.txn.open();
      RamoOcupacional ramoOcupacional = 
        this.cargoBusiness.findRamoOcupacionalById(ramoOcupacionalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(ramoOcupacional);
      return ramoOcupacional;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRamoOcupacional() throws Exception
  {
    try { this.txn.open();
      return this.cargoBusiness.findAllRamoOcupacional();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRamoOcupacionalByCodRamoOcupacional(String codRamoOcupacional)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findRamoOcupacionalByCodRamoOcupacional(codRamoOcupacional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRamoOcupacionalByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findRamoOcupacionalByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSerieCargo(SerieCargo serieCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.cargoBusiness.addSerieCargo(serieCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSerieCargo(SerieCargo serieCargo) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.updateSerieCargo(serieCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSerieCargo(SerieCargo serieCargo) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.deleteSerieCargo(serieCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SerieCargo findSerieCargoById(long serieCargoId) throws Exception
  {
    try { this.txn.open();
      SerieCargo serieCargo = 
        this.cargoBusiness.findSerieCargoById(serieCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(serieCargo);
      return serieCargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSerieCargo() throws Exception
  {
    try { this.txn.open();
      return this.cargoBusiness.findAllSerieCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSerieCargoByCodSerieCargo(String codSerieCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findSerieCargoByCodSerieCargo(codSerieCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSerieCargoByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findSerieCargoByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSerieCargoByGrupoOcupacional(long idGrupoOcupacional)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findSerieCargoByGrupoOcupacional(idGrupoOcupacional);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSueldoMinimo(SueldoMinimo sueldoMinimo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.cargoBusiness.addSueldoMinimo(sueldoMinimo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSueldoMinimo(SueldoMinimo sueldoMinimo) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.updateSueldoMinimo(sueldoMinimo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSueldoMinimo(SueldoMinimo sueldoMinimo) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.deleteSueldoMinimo(sueldoMinimo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SueldoMinimo findSueldoMinimoById(long sueldoMinimoId) throws Exception
  {
    try { this.txn.open();
      SueldoMinimo sueldoMinimo = 
        this.cargoBusiness.findSueldoMinimoById(sueldoMinimoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(sueldoMinimo);
      return sueldoMinimo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSueldoMinimo() throws Exception
  {
    try { this.txn.open();
      return this.cargoBusiness.findAllSueldoMinimo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSueldoMinimoByFechaVigencia(Date fechaVigencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findSueldoMinimoByFechaVigencia(fechaVigencia);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTabulador(Tabulador tabulador)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.cargoBusiness.addTabulador(tabulador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTabulador(Tabulador tabulador) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.updateTabulador(tabulador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTabulador(Tabulador tabulador) throws Exception
  {
    try { this.txn.open();
      this.cargoBusiness.deleteTabulador(tabulador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Tabulador findTabuladorById(long tabuladorId) throws Exception
  {
    try { this.txn.open();
      Tabulador tabulador = 
        this.cargoBusiness.findTabuladorById(tabuladorId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tabulador);
      return tabulador;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTabulador() throws Exception
  {
    try { this.txn.open();
      return this.cargoBusiness.findAllTabulador();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTabuladorByCodTabulador(String codTabulador, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findTabuladorByCodTabulador(codTabulador, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTabuladorByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findTabuladorByDescripcion(descripcion, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTabuladorByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoBusiness.findTabuladorByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}