package sigefirrhh.base.cargo;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class DetalleTabulador
  implements Serializable, PersistenceCapable
{
  private long idDetalleTabulador;
  private Tabulador tabulador;
  private int grado;
  private int subGrado;
  private int paso;
  private double monto;
  private double sueldoHora;
  private double cargaHoraria;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargaHoraria", "grado", "idDetalleTabulador", "monto", "paso", "subGrado", "sueldoHora", "tabulador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Integer.TYPE, Long.TYPE, Double.TYPE, Integer.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Tabulador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public DetalleTabulador()
  {
    jdoSetgrado(this, 1);

    jdoSetsubGrado(this, 1);

    jdoSetpaso(this, 1);

    jdoSetmonto(this, 0.0D);

    jdoSetsueldoHora(this, 0.0D);

    jdoSetcargaHoraria(this, 1.0D);
  }
  public String toString() {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String monto = b.format(jdoGetmonto(this));
    String sueldoHora = b.format(jdoGetsueldoHora(this));
    String cargaHoraria = b.format(jdoGetcargaHoraria(this));

    String valor = jdoGetgrado(this) + "  -  " + 
      jdoGetsubGrado(this) + "  -  " + 
      jdoGetpaso(this) + "  -  " + 
      monto;
    if (jdoGetsueldoHora(this) != 0.0D) {
      valor = valor + " - " + sueldoHora;
    }
    if (jdoGetcargaHoraria(this) != 0.0D) {
      valor = valor + " - " + cargaHoraria;
    }
    return valor;
  }

  public int getGrado()
  {
    return jdoGetgrado(this);
  }

  public long getIdDetalleTabulador()
  {
    return jdoGetidDetalleTabulador(this);
  }

  public int getPaso()
  {
    return jdoGetpaso(this);
  }

  public int getSubGrado()
  {
    return jdoGetsubGrado(this);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public Tabulador getTabulador()
  {
    return jdoGettabulador(this);
  }

  public void setGrado(int i)
  {
    jdoSetgrado(this, i);
  }

  public void setIdDetalleTabulador(long l)
  {
    jdoSetidDetalleTabulador(this, l);
  }

  public void setPaso(int i)
  {
    jdoSetpaso(this, i);
  }

  public void setSubGrado(int i)
  {
    jdoSetsubGrado(this, i);
  }

  public void setMonto(double d)
  {
    jdoSetmonto(this, d);
  }

  public void setTabulador(Tabulador tabulador)
  {
    jdoSettabulador(this, tabulador);
  }

  public double getCargaHoraria() {
    return jdoGetcargaHoraria(this);
  }

  public void setCargaHoraria(double cargaHoraria) {
    jdoSetcargaHoraria(this, cargaHoraria);
  }

  public double getSueldoHora() {
    return jdoGetsueldoHora(this);
  }

  public void setSueldoHora(double sueldoHora) {
    jdoSetsueldoHora(this, sueldoHora);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.cargo.DetalleTabulador"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DetalleTabulador());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DetalleTabulador localDetalleTabulador = new DetalleTabulador();
    localDetalleTabulador.jdoFlags = 1;
    localDetalleTabulador.jdoStateManager = paramStateManager;
    return localDetalleTabulador;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DetalleTabulador localDetalleTabulador = new DetalleTabulador();
    localDetalleTabulador.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDetalleTabulador.jdoFlags = 1;
    localDetalleTabulador.jdoStateManager = paramStateManager;
    return localDetalleTabulador;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.cargaHoraria);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.grado);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDetalleTabulador);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.paso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.subGrado);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoHora);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tabulador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargaHoraria = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDetalleTabulador = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.paso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.subGrado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoHora = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tabulador = ((Tabulador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DetalleTabulador paramDetalleTabulador, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDetalleTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.cargaHoraria = paramDetalleTabulador.cargaHoraria;
      return;
    case 1:
      if (paramDetalleTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.grado = paramDetalleTabulador.grado;
      return;
    case 2:
      if (paramDetalleTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.idDetalleTabulador = paramDetalleTabulador.idDetalleTabulador;
      return;
    case 3:
      if (paramDetalleTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramDetalleTabulador.monto;
      return;
    case 4:
      if (paramDetalleTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.paso = paramDetalleTabulador.paso;
      return;
    case 5:
      if (paramDetalleTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.subGrado = paramDetalleTabulador.subGrado;
      return;
    case 6:
      if (paramDetalleTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoHora = paramDetalleTabulador.sueldoHora;
      return;
    case 7:
      if (paramDetalleTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.tabulador = paramDetalleTabulador.tabulador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DetalleTabulador))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DetalleTabulador localDetalleTabulador = (DetalleTabulador)paramObject;
    if (localDetalleTabulador.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDetalleTabulador, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DetalleTabuladorPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DetalleTabuladorPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DetalleTabuladorPK))
      throw new IllegalArgumentException("arg1");
    DetalleTabuladorPK localDetalleTabuladorPK = (DetalleTabuladorPK)paramObject;
    localDetalleTabuladorPK.idDetalleTabulador = this.idDetalleTabulador;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DetalleTabuladorPK))
      throw new IllegalArgumentException("arg1");
    DetalleTabuladorPK localDetalleTabuladorPK = (DetalleTabuladorPK)paramObject;
    this.idDetalleTabulador = localDetalleTabuladorPK.idDetalleTabulador;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DetalleTabuladorPK))
      throw new IllegalArgumentException("arg2");
    DetalleTabuladorPK localDetalleTabuladorPK = (DetalleTabuladorPK)paramObject;
    localDetalleTabuladorPK.idDetalleTabulador = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DetalleTabuladorPK))
      throw new IllegalArgumentException("arg2");
    DetalleTabuladorPK localDetalleTabuladorPK = (DetalleTabuladorPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localDetalleTabuladorPK.idDetalleTabulador);
  }

  private static final double jdoGetcargaHoraria(DetalleTabulador paramDetalleTabulador)
  {
    if (paramDetalleTabulador.jdoFlags <= 0)
      return paramDetalleTabulador.cargaHoraria;
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabulador.cargaHoraria;
    if (localStateManager.isLoaded(paramDetalleTabulador, jdoInheritedFieldCount + 0))
      return paramDetalleTabulador.cargaHoraria;
    return localStateManager.getDoubleField(paramDetalleTabulador, jdoInheritedFieldCount + 0, paramDetalleTabulador.cargaHoraria);
  }

  private static final void jdoSetcargaHoraria(DetalleTabulador paramDetalleTabulador, double paramDouble)
  {
    if (paramDetalleTabulador.jdoFlags == 0)
    {
      paramDetalleTabulador.cargaHoraria = paramDouble;
      return;
    }
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabulador.cargaHoraria = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramDetalleTabulador, jdoInheritedFieldCount + 0, paramDetalleTabulador.cargaHoraria, paramDouble);
  }

  private static final int jdoGetgrado(DetalleTabulador paramDetalleTabulador)
  {
    if (paramDetalleTabulador.jdoFlags <= 0)
      return paramDetalleTabulador.grado;
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabulador.grado;
    if (localStateManager.isLoaded(paramDetalleTabulador, jdoInheritedFieldCount + 1))
      return paramDetalleTabulador.grado;
    return localStateManager.getIntField(paramDetalleTabulador, jdoInheritedFieldCount + 1, paramDetalleTabulador.grado);
  }

  private static final void jdoSetgrado(DetalleTabulador paramDetalleTabulador, int paramInt)
  {
    if (paramDetalleTabulador.jdoFlags == 0)
    {
      paramDetalleTabulador.grado = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabulador.grado = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleTabulador, jdoInheritedFieldCount + 1, paramDetalleTabulador.grado, paramInt);
  }

  private static final long jdoGetidDetalleTabulador(DetalleTabulador paramDetalleTabulador)
  {
    return paramDetalleTabulador.idDetalleTabulador;
  }

  private static final void jdoSetidDetalleTabulador(DetalleTabulador paramDetalleTabulador, long paramLong)
  {
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabulador.idDetalleTabulador = paramLong;
      return;
    }
    localStateManager.setLongField(paramDetalleTabulador, jdoInheritedFieldCount + 2, paramDetalleTabulador.idDetalleTabulador, paramLong);
  }

  private static final double jdoGetmonto(DetalleTabulador paramDetalleTabulador)
  {
    if (paramDetalleTabulador.jdoFlags <= 0)
      return paramDetalleTabulador.monto;
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabulador.monto;
    if (localStateManager.isLoaded(paramDetalleTabulador, jdoInheritedFieldCount + 3))
      return paramDetalleTabulador.monto;
    return localStateManager.getDoubleField(paramDetalleTabulador, jdoInheritedFieldCount + 3, paramDetalleTabulador.monto);
  }

  private static final void jdoSetmonto(DetalleTabulador paramDetalleTabulador, double paramDouble)
  {
    if (paramDetalleTabulador.jdoFlags == 0)
    {
      paramDetalleTabulador.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabulador.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramDetalleTabulador, jdoInheritedFieldCount + 3, paramDetalleTabulador.monto, paramDouble);
  }

  private static final int jdoGetpaso(DetalleTabulador paramDetalleTabulador)
  {
    if (paramDetalleTabulador.jdoFlags <= 0)
      return paramDetalleTabulador.paso;
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabulador.paso;
    if (localStateManager.isLoaded(paramDetalleTabulador, jdoInheritedFieldCount + 4))
      return paramDetalleTabulador.paso;
    return localStateManager.getIntField(paramDetalleTabulador, jdoInheritedFieldCount + 4, paramDetalleTabulador.paso);
  }

  private static final void jdoSetpaso(DetalleTabulador paramDetalleTabulador, int paramInt)
  {
    if (paramDetalleTabulador.jdoFlags == 0)
    {
      paramDetalleTabulador.paso = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabulador.paso = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleTabulador, jdoInheritedFieldCount + 4, paramDetalleTabulador.paso, paramInt);
  }

  private static final int jdoGetsubGrado(DetalleTabulador paramDetalleTabulador)
  {
    if (paramDetalleTabulador.jdoFlags <= 0)
      return paramDetalleTabulador.subGrado;
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabulador.subGrado;
    if (localStateManager.isLoaded(paramDetalleTabulador, jdoInheritedFieldCount + 5))
      return paramDetalleTabulador.subGrado;
    return localStateManager.getIntField(paramDetalleTabulador, jdoInheritedFieldCount + 5, paramDetalleTabulador.subGrado);
  }

  private static final void jdoSetsubGrado(DetalleTabulador paramDetalleTabulador, int paramInt)
  {
    if (paramDetalleTabulador.jdoFlags == 0)
    {
      paramDetalleTabulador.subGrado = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabulador.subGrado = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleTabulador, jdoInheritedFieldCount + 5, paramDetalleTabulador.subGrado, paramInt);
  }

  private static final double jdoGetsueldoHora(DetalleTabulador paramDetalleTabulador)
  {
    if (paramDetalleTabulador.jdoFlags <= 0)
      return paramDetalleTabulador.sueldoHora;
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabulador.sueldoHora;
    if (localStateManager.isLoaded(paramDetalleTabulador, jdoInheritedFieldCount + 6))
      return paramDetalleTabulador.sueldoHora;
    return localStateManager.getDoubleField(paramDetalleTabulador, jdoInheritedFieldCount + 6, paramDetalleTabulador.sueldoHora);
  }

  private static final void jdoSetsueldoHora(DetalleTabulador paramDetalleTabulador, double paramDouble)
  {
    if (paramDetalleTabulador.jdoFlags == 0)
    {
      paramDetalleTabulador.sueldoHora = paramDouble;
      return;
    }
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabulador.sueldoHora = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramDetalleTabulador, jdoInheritedFieldCount + 6, paramDetalleTabulador.sueldoHora, paramDouble);
  }

  private static final Tabulador jdoGettabulador(DetalleTabulador paramDetalleTabulador)
  {
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabulador.tabulador;
    if (localStateManager.isLoaded(paramDetalleTabulador, jdoInheritedFieldCount + 7))
      return paramDetalleTabulador.tabulador;
    return (Tabulador)localStateManager.getObjectField(paramDetalleTabulador, jdoInheritedFieldCount + 7, paramDetalleTabulador.tabulador);
  }

  private static final void jdoSettabulador(DetalleTabulador paramDetalleTabulador, Tabulador paramTabulador)
  {
    StateManager localStateManager = paramDetalleTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabulador.tabulador = paramTabulador;
      return;
    }
    localStateManager.setObjectField(paramDetalleTabulador, jdoInheritedFieldCount + 7, paramDetalleTabulador.tabulador, paramTabulador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}