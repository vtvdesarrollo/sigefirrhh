package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class TabuladorBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTabulador(Tabulador tabulador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Tabulador tabuladorNew = 
      (Tabulador)BeanUtils.cloneBean(
      tabulador);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (tabuladorNew.getOrganismo() != null) {
      tabuladorNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        tabuladorNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(tabuladorNew);
  }

  public void updateTabulador(Tabulador tabulador) throws Exception
  {
    Tabulador tabuladorModify = 
      findTabuladorById(tabulador.getIdTabulador());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (tabulador.getOrganismo() != null) {
      tabulador.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        tabulador.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(tabuladorModify, tabulador);
  }

  public void deleteTabulador(Tabulador tabulador) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Tabulador tabuladorDelete = 
      findTabuladorById(tabulador.getIdTabulador());
    pm.deletePersistent(tabuladorDelete);
  }

  public Tabulador findTabuladorById(long idTabulador) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTabulador == pIdTabulador";
    Query query = pm.newQuery(Tabulador.class, filter);

    query.declareParameters("long pIdTabulador");

    parameters.put("pIdTabulador", new Long(idTabulador));

    Collection colTabulador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTabulador.iterator();
    return (Tabulador)iterator.next();
  }

  public Collection findTabuladorAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tabuladorExtent = pm.getExtent(
      Tabulador.class, true);
    Query query = pm.newQuery(tabuladorExtent);
    query.setOrdering("codTabulador ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTabulador(String codTabulador, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTabulador == pCodTabulador &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Tabulador.class, filter);

    query.declareParameters("java.lang.String pCodTabulador, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodTabulador", new String(codTabulador));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codTabulador ascending");

    Collection colTabulador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTabulador);

    return colTabulador;
  }

  public Collection findByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Tabulador.class, filter);

    query.declareParameters("java.lang.String pDescripcion, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codTabulador ascending");

    Collection colTabulador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTabulador);

    return colTabulador;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Tabulador.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codTabulador ascending");

    Collection colTabulador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTabulador);

    return colTabulador;
  }
}