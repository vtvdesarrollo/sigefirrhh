package sigefirrhh.base.cargo;

import java.io.Serializable;

public class DetalleTabuladorAniosPK
  implements Serializable
{
  public long idDetalleTabuladorAnios;

  public DetalleTabuladorAniosPK()
  {
  }

  public DetalleTabuladorAniosPK(long idDetalleTabuladorAnios)
  {
    this.idDetalleTabuladorAnios = idDetalleTabuladorAnios;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DetalleTabuladorAniosPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DetalleTabuladorAniosPK thatPK)
  {
    return 
      this.idDetalleTabuladorAnios == thatPK.idDetalleTabuladorAnios;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDetalleTabuladorAnios)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDetalleTabuladorAnios);
  }
}