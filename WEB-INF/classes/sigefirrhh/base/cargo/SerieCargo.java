package sigefirrhh.base.cargo;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class SerieCargo
  implements Serializable, PersistenceCapable
{
  private long idSerieCargo;
  private String codSerieCargo;
  private String nombre;
  private GrupoOcupacional grupoOcupacional;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codSerieCargo", "grupoOcupacional", "idSerieCargo", "idSitp", "nombre", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.cargo.GrupoOcupacional"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 26, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodSerieCargo(this);
  }

  public String getCodSerieCargo() {
    return jdoGetcodSerieCargo(this);
  }

  public GrupoOcupacional getGrupoOcupacional() {
    return jdoGetgrupoOcupacional(this);
  }

  public long getIdSerieCargo() {
    return jdoGetidSerieCargo(this);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setCodSerieCargo(String string) {
    jdoSetcodSerieCargo(this, string);
  }

  public void setGrupoOcupacional(GrupoOcupacional ocupacional) {
    jdoSetgrupoOcupacional(this, ocupacional);
  }

  public void setIdSerieCargo(long l) {
    jdoSetidSerieCargo(this, l);
  }

  public void setNombre(String string) {
    jdoSetnombre(this, string);
  }

  public int getIdSitp() {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i) {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date) {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.cargo.SerieCargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SerieCargo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SerieCargo localSerieCargo = new SerieCargo();
    localSerieCargo.jdoFlags = 1;
    localSerieCargo.jdoStateManager = paramStateManager;
    return localSerieCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SerieCargo localSerieCargo = new SerieCargo();
    localSerieCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSerieCargo.jdoFlags = 1;
    localSerieCargo.jdoStateManager = paramStateManager;
    return localSerieCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSerieCargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoOcupacional);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSerieCargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSerieCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoOcupacional = ((GrupoOcupacional)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSerieCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SerieCargo paramSerieCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSerieCargo == null)
        throw new IllegalArgumentException("arg1");
      this.codSerieCargo = paramSerieCargo.codSerieCargo;
      return;
    case 1:
      if (paramSerieCargo == null)
        throw new IllegalArgumentException("arg1");
      this.grupoOcupacional = paramSerieCargo.grupoOcupacional;
      return;
    case 2:
      if (paramSerieCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idSerieCargo = paramSerieCargo.idSerieCargo;
      return;
    case 3:
      if (paramSerieCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramSerieCargo.idSitp;
      return;
    case 4:
      if (paramSerieCargo == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramSerieCargo.nombre;
      return;
    case 5:
      if (paramSerieCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramSerieCargo.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SerieCargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SerieCargo localSerieCargo = (SerieCargo)paramObject;
    if (localSerieCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSerieCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SerieCargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SerieCargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SerieCargoPK))
      throw new IllegalArgumentException("arg1");
    SerieCargoPK localSerieCargoPK = (SerieCargoPK)paramObject;
    localSerieCargoPK.idSerieCargo = this.idSerieCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SerieCargoPK))
      throw new IllegalArgumentException("arg1");
    SerieCargoPK localSerieCargoPK = (SerieCargoPK)paramObject;
    this.idSerieCargo = localSerieCargoPK.idSerieCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SerieCargoPK))
      throw new IllegalArgumentException("arg2");
    SerieCargoPK localSerieCargoPK = (SerieCargoPK)paramObject;
    localSerieCargoPK.idSerieCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SerieCargoPK))
      throw new IllegalArgumentException("arg2");
    SerieCargoPK localSerieCargoPK = (SerieCargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localSerieCargoPK.idSerieCargo);
  }

  private static final String jdoGetcodSerieCargo(SerieCargo paramSerieCargo)
  {
    if (paramSerieCargo.jdoFlags <= 0)
      return paramSerieCargo.codSerieCargo;
    StateManager localStateManager = paramSerieCargo.jdoStateManager;
    if (localStateManager == null)
      return paramSerieCargo.codSerieCargo;
    if (localStateManager.isLoaded(paramSerieCargo, jdoInheritedFieldCount + 0))
      return paramSerieCargo.codSerieCargo;
    return localStateManager.getStringField(paramSerieCargo, jdoInheritedFieldCount + 0, paramSerieCargo.codSerieCargo);
  }

  private static final void jdoSetcodSerieCargo(SerieCargo paramSerieCargo, String paramString)
  {
    if (paramSerieCargo.jdoFlags == 0)
    {
      paramSerieCargo.codSerieCargo = paramString;
      return;
    }
    StateManager localStateManager = paramSerieCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSerieCargo.codSerieCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramSerieCargo, jdoInheritedFieldCount + 0, paramSerieCargo.codSerieCargo, paramString);
  }

  private static final GrupoOcupacional jdoGetgrupoOcupacional(SerieCargo paramSerieCargo)
  {
    StateManager localStateManager = paramSerieCargo.jdoStateManager;
    if (localStateManager == null)
      return paramSerieCargo.grupoOcupacional;
    if (localStateManager.isLoaded(paramSerieCargo, jdoInheritedFieldCount + 1))
      return paramSerieCargo.grupoOcupacional;
    return (GrupoOcupacional)localStateManager.getObjectField(paramSerieCargo, jdoInheritedFieldCount + 1, paramSerieCargo.grupoOcupacional);
  }

  private static final void jdoSetgrupoOcupacional(SerieCargo paramSerieCargo, GrupoOcupacional paramGrupoOcupacional)
  {
    StateManager localStateManager = paramSerieCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSerieCargo.grupoOcupacional = paramGrupoOcupacional;
      return;
    }
    localStateManager.setObjectField(paramSerieCargo, jdoInheritedFieldCount + 1, paramSerieCargo.grupoOcupacional, paramGrupoOcupacional);
  }

  private static final long jdoGetidSerieCargo(SerieCargo paramSerieCargo)
  {
    return paramSerieCargo.idSerieCargo;
  }

  private static final void jdoSetidSerieCargo(SerieCargo paramSerieCargo, long paramLong)
  {
    StateManager localStateManager = paramSerieCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSerieCargo.idSerieCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramSerieCargo, jdoInheritedFieldCount + 2, paramSerieCargo.idSerieCargo, paramLong);
  }

  private static final int jdoGetidSitp(SerieCargo paramSerieCargo)
  {
    if (paramSerieCargo.jdoFlags <= 0)
      return paramSerieCargo.idSitp;
    StateManager localStateManager = paramSerieCargo.jdoStateManager;
    if (localStateManager == null)
      return paramSerieCargo.idSitp;
    if (localStateManager.isLoaded(paramSerieCargo, jdoInheritedFieldCount + 3))
      return paramSerieCargo.idSitp;
    return localStateManager.getIntField(paramSerieCargo, jdoInheritedFieldCount + 3, paramSerieCargo.idSitp);
  }

  private static final void jdoSetidSitp(SerieCargo paramSerieCargo, int paramInt)
  {
    if (paramSerieCargo.jdoFlags == 0)
    {
      paramSerieCargo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramSerieCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSerieCargo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramSerieCargo, jdoInheritedFieldCount + 3, paramSerieCargo.idSitp, paramInt);
  }

  private static final String jdoGetnombre(SerieCargo paramSerieCargo)
  {
    if (paramSerieCargo.jdoFlags <= 0)
      return paramSerieCargo.nombre;
    StateManager localStateManager = paramSerieCargo.jdoStateManager;
    if (localStateManager == null)
      return paramSerieCargo.nombre;
    if (localStateManager.isLoaded(paramSerieCargo, jdoInheritedFieldCount + 4))
      return paramSerieCargo.nombre;
    return localStateManager.getStringField(paramSerieCargo, jdoInheritedFieldCount + 4, paramSerieCargo.nombre);
  }

  private static final void jdoSetnombre(SerieCargo paramSerieCargo, String paramString)
  {
    if (paramSerieCargo.jdoFlags == 0)
    {
      paramSerieCargo.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramSerieCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSerieCargo.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramSerieCargo, jdoInheritedFieldCount + 4, paramSerieCargo.nombre, paramString);
  }

  private static final Date jdoGettiempoSitp(SerieCargo paramSerieCargo)
  {
    if (paramSerieCargo.jdoFlags <= 0)
      return paramSerieCargo.tiempoSitp;
    StateManager localStateManager = paramSerieCargo.jdoStateManager;
    if (localStateManager == null)
      return paramSerieCargo.tiempoSitp;
    if (localStateManager.isLoaded(paramSerieCargo, jdoInheritedFieldCount + 5))
      return paramSerieCargo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramSerieCargo, jdoInheritedFieldCount + 5, paramSerieCargo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(SerieCargo paramSerieCargo, Date paramDate)
  {
    if (paramSerieCargo.jdoFlags == 0)
    {
      paramSerieCargo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramSerieCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSerieCargo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSerieCargo, jdoInheritedFieldCount + 5, paramSerieCargo.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}