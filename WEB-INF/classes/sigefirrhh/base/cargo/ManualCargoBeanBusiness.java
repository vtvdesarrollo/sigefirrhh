package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class ManualCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addManualCargo(ManualCargo manualCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ManualCargo manualCargoNew = 
      (ManualCargo)BeanUtils.cloneBean(
      manualCargo);

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (manualCargoNew.getTabulador() != null) {
      manualCargoNew.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        manualCargoNew.getTabulador().getIdTabulador()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (manualCargoNew.getOrganismo() != null) {
      manualCargoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        manualCargoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(manualCargoNew);
  }

  public void updateManualCargo(ManualCargo manualCargo) throws Exception
  {
    ManualCargo manualCargoModify = 
      findManualCargoById(manualCargo.getIdManualCargo());

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (manualCargo.getTabulador() != null) {
      manualCargo.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        manualCargo.getTabulador().getIdTabulador()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (manualCargo.getOrganismo() != null) {
      manualCargo.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        manualCargo.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(manualCargoModify, manualCargo);
  }

  public void deleteManualCargo(ManualCargo manualCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ManualCargo manualCargoDelete = 
      findManualCargoById(manualCargo.getIdManualCargo());
    pm.deletePersistent(manualCargoDelete);
  }

  public ManualCargo findManualCargoById(long idManualCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idManualCargo == pIdManualCargo";
    Query query = pm.newQuery(ManualCargo.class, filter);

    query.declareParameters("long pIdManualCargo");

    parameters.put("pIdManualCargo", new Long(idManualCargo));

    Collection colManualCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colManualCargo.iterator();
    return (ManualCargo)iterator.next();
  }

  public Collection findManualCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent manualCargoExtent = pm.getExtent(
      ManualCargo.class, true);
    Query query = pm.newQuery(manualCargoExtent);
    query.setOrdering("codManualCargo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodManualCargo(int codManualCargo, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codManualCargo == pCodManualCargo &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(ManualCargo.class, filter);

    query.declareParameters("int pCodManualCargo, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodManualCargo", new Integer(codManualCargo));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codManualCargo ascending");

    Collection colManualCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colManualCargo);

    return colManualCargo;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(ManualCargo.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codManualCargo ascending");

    Collection colManualCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colManualCargo);

    return colManualCargo;
  }

  public Collection findByTipoCargo(String tipoCargo, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoCargo == pTipoCargo &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(ManualCargo.class, filter);

    query.declareParameters("java.lang.String pTipoCargo, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pTipoCargo", new String(tipoCargo));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codManualCargo ascending");

    Collection colManualCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colManualCargo);

    return colManualCargo;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(ManualCargo.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codManualCargo ascending");

    Collection colManualCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colManualCargo);

    return colManualCargo;
  }
}