package sigefirrhh.base.cargo;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ManualCargoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ManualCargoForm.class.getName());
  private ManualCargo manualCargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoFacade cargoFacade = new CargoFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showManualCargoByCodManualCargo;
  private boolean showManualCargoByNombre;
  private int findCodManualCargo;
  private String findNombre;
  private Collection colTabulador;
  private String selectTabulador;
  private Object stateResultManualCargoByCodManualCargo = null;

  private Object stateResultManualCargoByNombre = null;

  public int getFindCodManualCargo()
  {
    return this.findCodManualCargo;
  }
  public void setFindCodManualCargo(int findCodManualCargo) {
    this.findCodManualCargo = findCodManualCargo;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectTabulador()
  {
    return this.selectTabulador;
  }
  public void setSelectTabulador(String valTabulador) {
    Iterator iterator = this.colTabulador.iterator();
    Tabulador tabulador = null;
    this.manualCargo.setTabulador(null);
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      if (String.valueOf(tabulador.getIdTabulador()).equals(
        valTabulador)) {
        this.manualCargo.setTabulador(
          tabulador);
        break;
      }
    }
    this.selectTabulador = valTabulador;
  }
  public Collection getResult() {
    return this.result;
  }

  public ManualCargo getManualCargo() {
    if (this.manualCargo == null) {
      this.manualCargo = new ManualCargo();
    }
    return this.manualCargo;
  }

  public ManualCargoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListTipoManual()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ManualCargo.LISTA_TIPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListMultipleDescripcion() {
    Collection col = new ArrayList();

    Iterator iterEntry = ManualCargo.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColTabulador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTabulador.iterator();
    Tabulador tabulador = null;
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tabulador.getIdTabulador()), 
        tabulador.toString()));
    }
    return col;
  }

  public Collection getListProcesoSeleccion() {
    Collection col = new ArrayList();

    Iterator iterEntry = ManualCargo.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTipoCargo() {
    Collection col = new ArrayList();

    Iterator iterEntry = ManualCargo.LISTA_TIPO_CARGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colTabulador = 
        this.cargoFacade.findTabuladorByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findManualCargoByCodManualCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.cargoFacade.findManualCargoByCodManualCargo(this.findCodManualCargo, idOrganismo);
      this.showManualCargoByCodManualCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showManualCargoByCodManualCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodManualCargo = 0;
    this.findNombre = null;

    return null;
  }

  public String findManualCargoByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.cargoFacade.findManualCargoByNombre(this.findNombre, idOrganismo);
      this.showManualCargoByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showManualCargoByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodManualCargo = 0;
    this.findNombre = null;

    return null;
  }

  public boolean isShowManualCargoByCodManualCargo() {
    return this.showManualCargoByCodManualCargo;
  }
  public boolean isShowManualCargoByNombre() {
    return this.showManualCargoByNombre;
  }

  public String selectManualCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTabulador = null;

    long idManualCargo = 
      Long.parseLong((String)requestParameterMap.get("idManualCargo"));
    try
    {
      this.manualCargo = 
        this.cargoFacade.findManualCargoById(
        idManualCargo);
      if (this.manualCargo.getTabulador() != null) {
        this.selectTabulador = 
          String.valueOf(this.manualCargo.getTabulador().getIdTabulador());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.manualCargo = null;
    this.showManualCargoByCodManualCargo = false;
    this.showManualCargoByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.manualCargo.getFechaVigencia() != null) && 
      (this.manualCargo.getFechaVigencia().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Vigencia no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.manualCargo.getTiempoSitp() != null) && 
      (this.manualCargo.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.cargoFacade.addManualCargo(
          this.manualCargo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.cargoFacade.updateManualCargo(
          this.manualCargo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.cargoFacade.deleteManualCargo(
        this.manualCargo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.manualCargo = new ManualCargo();

    this.selectTabulador = null;

    this.manualCargo.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.manualCargo.setIdManualCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.base.cargo.ManualCargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.manualCargo = new ManualCargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}