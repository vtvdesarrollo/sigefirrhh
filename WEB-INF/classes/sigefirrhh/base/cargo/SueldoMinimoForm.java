package sigefirrhh.base.cargo;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class SueldoMinimoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SueldoMinimoForm.class.getName());
  private SueldoMinimo sueldoMinimo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoFacade cargoFacade = new CargoFacade();
  private boolean showSueldoMinimoByFechaVigencia;
  private Date findFechaVigencia;
  private Object stateResultSueldoMinimoByFechaVigencia = null;

  public Date getFindFechaVigencia()
  {
    return this.findFechaVigencia;
  }
  public void setFindFechaVigencia(Date findFechaVigencia) {
    this.findFechaVigencia = findFechaVigencia;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public SueldoMinimo getSueldoMinimo() {
    if (this.sueldoMinimo == null) {
      this.sueldoMinimo = new SueldoMinimo();
    }
    return this.sueldoMinimo;
  }

  public SueldoMinimoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findSueldoMinimoByFechaVigencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findSueldoMinimoByFechaVigencia(this.findFechaVigencia);
      this.showSueldoMinimoByFechaVigencia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSueldoMinimoByFechaVigencia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findFechaVigencia = null;

    return null;
  }

  public boolean isShowSueldoMinimoByFechaVigencia() {
    return this.showSueldoMinimoByFechaVigencia;
  }

  public String selectSueldoMinimo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idSueldoMinimo = 
      Long.parseLong((String)requestParameterMap.get("idSueldoMinimo"));
    try
    {
      this.sueldoMinimo = 
        this.cargoFacade.findSueldoMinimoById(
        idSueldoMinimo);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.sueldoMinimo = null;
    this.showSueldoMinimoByFechaVigencia = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.sueldoMinimo.getFechaVigencia() != null) && 
      (this.sueldoMinimo.getFechaVigencia().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha de Vigencia no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.sueldoMinimo.getTiempoSitp() != null) && 
      (this.sueldoMinimo.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.cargoFacade.addSueldoMinimo(
          this.sueldoMinimo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.cargoFacade.updateSueldoMinimo(
          this.sueldoMinimo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.cargoFacade.deleteSueldoMinimo(
        this.sueldoMinimo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.sueldoMinimo = new SueldoMinimo();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.sueldoMinimo.setIdSueldoMinimo(identityGenerator.getNextSequenceNumber("sigefirrhh.base.cargo.SueldoMinimo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.sueldoMinimo = new SueldoMinimo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}