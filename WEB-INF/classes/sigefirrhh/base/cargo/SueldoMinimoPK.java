package sigefirrhh.base.cargo;

import java.io.Serializable;

public class SueldoMinimoPK
  implements Serializable
{
  public long idSueldoMinimo;

  public SueldoMinimoPK()
  {
  }

  public SueldoMinimoPK(long idSueldoMinimo)
  {
    this.idSueldoMinimo = idSueldoMinimo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SueldoMinimoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SueldoMinimoPK thatPK)
  {
    return 
      this.idSueldoMinimo == thatPK.idSueldoMinimo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSueldoMinimo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSueldoMinimo);
  }
}