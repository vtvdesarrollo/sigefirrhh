package sigefirrhh.base.cargo;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class ManualCargo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  protected static final Map LISTA_SINO;
  protected static final Map LISTA_TIPO_CARGO;
  private long idManualCargo;
  private int codManualCargo;
  private String codMpd;
  private String nombre;
  private Date fechaVigencia;
  private String tipoManual;
  private String multipleDescripcion;
  private Tabulador tabulador;
  private String procesoSeleccion;
  private String tipoCargo;
  private Organismo organismo;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codManualCargo", "codMpd", "fechaVigencia", "idManualCargo", "idSitp", "multipleDescripcion", "nombre", "organismo", "procesoSeleccion", "tabulador", "tiempoSitp", "tipoCargo", "tipoManual" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.cargo.Tabulador"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21, 26, 21, 26, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.cargo.ManualCargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ManualCargo());

    LISTA_TIPO = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_TIPO_CARGO = 
      new LinkedHashMap();

    LISTA_TIPO.put("1", "MANUAL MPD");
    LISTA_TIPO.put("2", "SERIE ADELANTADA");
    LISTA_TIPO.put("3", "PROPIO ORGANISMO");
    LISTA_TIPO.put("4", "OTRO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
    LISTA_TIPO_CARGO.put("AN", "ALTO NIVEL");
    LISTA_TIPO_CARGO.put("TA", "TECNICO-PROFESIONAL-ADMINISTRATIVO");
    LISTA_TIPO_CARGO.put("OB", "OBRERO");
    LISTA_TIPO_CARGO.put("NC", "NO CLASIFICADO");
    LISTA_TIPO_CARGO.put("NA", "NO APLICA");
    LISTA_TIPO_CARGO.put("DO", "PERSONAL DOCENTE");
    LISTA_TIPO_CARGO.put("DI", "PERSONAL DIPLOMATICO");
  }

  public ManualCargo()
  {
    jdoSettipoManual(this, "NA");

    jdoSetmultipleDescripcion(this, "N");

    jdoSetprocesoSeleccion(this, "N");

    jdoSettipoCargo(this, "TA");
  }

  public String toString()
  {
    return jdoGetnombre(this);
  }

  public int getCodManualCargo()
  {
    return jdoGetcodManualCargo(this);
  }

  public Date getFechaVigencia() {
    return jdoGetfechaVigencia(this);
  }

  public long getIdManualCargo() {
    return jdoGetidManualCargo(this);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }

  public Tabulador getTabulador() {
    return jdoGettabulador(this);
  }

  public String getTipoManual() {
    return jdoGettipoManual(this);
  }

  public void setCodManualCargo(int i) {
    jdoSetcodManualCargo(this, i);
  }

  public void setFechaVigencia(Date date) {
    jdoSetfechaVigencia(this, date);
  }

  public void setIdManualCargo(long l) {
    jdoSetidManualCargo(this, l);
  }

  public void setNombre(String string) {
    jdoSetnombre(this, string);
  }

  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }

  public void setTabulador(Tabulador tabulador) {
    jdoSettabulador(this, tabulador);
  }

  public void setTipoManual(String string) {
    jdoSettipoManual(this, string);
  }

  public String getMultipleDescripcion() {
    return jdoGetmultipleDescripcion(this);
  }

  public void setMultipleDescripcion(String string) {
    jdoSetmultipleDescripcion(this, string);
  }

  public int getIdSitp() {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i) {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date) {
    jdoSettiempoSitp(this, date);
  }

  public String getCodMpd()
  {
    return jdoGetcodMpd(this);
  }

  public void setCodMpd(String string)
  {
    jdoSetcodMpd(this, string);
  }

  public String getProcesoSeleccion()
  {
    return jdoGetprocesoSeleccion(this);
  }

  public void setProcesoSeleccion(String string)
  {
    jdoSetprocesoSeleccion(this, string);
  }

  public String getTipoCargo() {
    return jdoGettipoCargo(this);
  }
  public void setTipoCargo(String tipoCargo) {
    jdoSettipoCargo(this, tipoCargo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ManualCargo localManualCargo = new ManualCargo();
    localManualCargo.jdoFlags = 1;
    localManualCargo.jdoStateManager = paramStateManager;
    return localManualCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ManualCargo localManualCargo = new ManualCargo();
    localManualCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localManualCargo.jdoFlags = 1;
    localManualCargo.jdoStateManager = paramStateManager;
    return localManualCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codManualCargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idManualCargo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.multipleDescripcion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.procesoSeleccion);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tabulador);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoCargo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoManual);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codManualCargo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idManualCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.multipleDescripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.procesoSeleccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tabulador = ((Tabulador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoManual = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ManualCargo paramManualCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.codManualCargo = paramManualCargo.codManualCargo;
      return;
    case 1:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.codMpd = paramManualCargo.codMpd;
      return;
    case 2:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramManualCargo.fechaVigencia;
      return;
    case 3:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idManualCargo = paramManualCargo.idManualCargo;
      return;
    case 4:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramManualCargo.idSitp;
      return;
    case 5:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.multipleDescripcion = paramManualCargo.multipleDescripcion;
      return;
    case 6:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramManualCargo.nombre;
      return;
    case 7:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramManualCargo.organismo;
      return;
    case 8:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.procesoSeleccion = paramManualCargo.procesoSeleccion;
      return;
    case 9:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tabulador = paramManualCargo.tabulador;
      return;
    case 10:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramManualCargo.tiempoSitp;
      return;
    case 11:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCargo = paramManualCargo.tipoCargo;
      return;
    case 12:
      if (paramManualCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tipoManual = paramManualCargo.tipoManual;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ManualCargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ManualCargo localManualCargo = (ManualCargo)paramObject;
    if (localManualCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localManualCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ManualCargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ManualCargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ManualCargoPK))
      throw new IllegalArgumentException("arg1");
    ManualCargoPK localManualCargoPK = (ManualCargoPK)paramObject;
    localManualCargoPK.idManualCargo = this.idManualCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ManualCargoPK))
      throw new IllegalArgumentException("arg1");
    ManualCargoPK localManualCargoPK = (ManualCargoPK)paramObject;
    this.idManualCargo = localManualCargoPK.idManualCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ManualCargoPK))
      throw new IllegalArgumentException("arg2");
    ManualCargoPK localManualCargoPK = (ManualCargoPK)paramObject;
    localManualCargoPK.idManualCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ManualCargoPK))
      throw new IllegalArgumentException("arg2");
    ManualCargoPK localManualCargoPK = (ManualCargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localManualCargoPK.idManualCargo);
  }

  private static final int jdoGetcodManualCargo(ManualCargo paramManualCargo)
  {
    if (paramManualCargo.jdoFlags <= 0)
      return paramManualCargo.codManualCargo;
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.codManualCargo;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 0))
      return paramManualCargo.codManualCargo;
    return localStateManager.getIntField(paramManualCargo, jdoInheritedFieldCount + 0, paramManualCargo.codManualCargo);
  }

  private static final void jdoSetcodManualCargo(ManualCargo paramManualCargo, int paramInt)
  {
    if (paramManualCargo.jdoFlags == 0)
    {
      paramManualCargo.codManualCargo = paramInt;
      return;
    }
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.codManualCargo = paramInt;
      return;
    }
    localStateManager.setIntField(paramManualCargo, jdoInheritedFieldCount + 0, paramManualCargo.codManualCargo, paramInt);
  }

  private static final String jdoGetcodMpd(ManualCargo paramManualCargo)
  {
    if (paramManualCargo.jdoFlags <= 0)
      return paramManualCargo.codMpd;
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.codMpd;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 1))
      return paramManualCargo.codMpd;
    return localStateManager.getStringField(paramManualCargo, jdoInheritedFieldCount + 1, paramManualCargo.codMpd);
  }

  private static final void jdoSetcodMpd(ManualCargo paramManualCargo, String paramString)
  {
    if (paramManualCargo.jdoFlags == 0)
    {
      paramManualCargo.codMpd = paramString;
      return;
    }
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.codMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramManualCargo, jdoInheritedFieldCount + 1, paramManualCargo.codMpd, paramString);
  }

  private static final Date jdoGetfechaVigencia(ManualCargo paramManualCargo)
  {
    if (paramManualCargo.jdoFlags <= 0)
      return paramManualCargo.fechaVigencia;
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.fechaVigencia;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 2))
      return paramManualCargo.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramManualCargo, jdoInheritedFieldCount + 2, paramManualCargo.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(ManualCargo paramManualCargo, Date paramDate)
  {
    if (paramManualCargo.jdoFlags == 0)
    {
      paramManualCargo.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramManualCargo, jdoInheritedFieldCount + 2, paramManualCargo.fechaVigencia, paramDate);
  }

  private static final long jdoGetidManualCargo(ManualCargo paramManualCargo)
  {
    return paramManualCargo.idManualCargo;
  }

  private static final void jdoSetidManualCargo(ManualCargo paramManualCargo, long paramLong)
  {
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.idManualCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramManualCargo, jdoInheritedFieldCount + 3, paramManualCargo.idManualCargo, paramLong);
  }

  private static final int jdoGetidSitp(ManualCargo paramManualCargo)
  {
    if (paramManualCargo.jdoFlags <= 0)
      return paramManualCargo.idSitp;
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.idSitp;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 4))
      return paramManualCargo.idSitp;
    return localStateManager.getIntField(paramManualCargo, jdoInheritedFieldCount + 4, paramManualCargo.idSitp);
  }

  private static final void jdoSetidSitp(ManualCargo paramManualCargo, int paramInt)
  {
    if (paramManualCargo.jdoFlags == 0)
    {
      paramManualCargo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramManualCargo, jdoInheritedFieldCount + 4, paramManualCargo.idSitp, paramInt);
  }

  private static final String jdoGetmultipleDescripcion(ManualCargo paramManualCargo)
  {
    if (paramManualCargo.jdoFlags <= 0)
      return paramManualCargo.multipleDescripcion;
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.multipleDescripcion;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 5))
      return paramManualCargo.multipleDescripcion;
    return localStateManager.getStringField(paramManualCargo, jdoInheritedFieldCount + 5, paramManualCargo.multipleDescripcion);
  }

  private static final void jdoSetmultipleDescripcion(ManualCargo paramManualCargo, String paramString)
  {
    if (paramManualCargo.jdoFlags == 0)
    {
      paramManualCargo.multipleDescripcion = paramString;
      return;
    }
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.multipleDescripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramManualCargo, jdoInheritedFieldCount + 5, paramManualCargo.multipleDescripcion, paramString);
  }

  private static final String jdoGetnombre(ManualCargo paramManualCargo)
  {
    if (paramManualCargo.jdoFlags <= 0)
      return paramManualCargo.nombre;
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.nombre;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 6))
      return paramManualCargo.nombre;
    return localStateManager.getStringField(paramManualCargo, jdoInheritedFieldCount + 6, paramManualCargo.nombre);
  }

  private static final void jdoSetnombre(ManualCargo paramManualCargo, String paramString)
  {
    if (paramManualCargo.jdoFlags == 0)
    {
      paramManualCargo.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramManualCargo, jdoInheritedFieldCount + 6, paramManualCargo.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(ManualCargo paramManualCargo)
  {
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.organismo;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 7))
      return paramManualCargo.organismo;
    return (Organismo)localStateManager.getObjectField(paramManualCargo, jdoInheritedFieldCount + 7, paramManualCargo.organismo);
  }

  private static final void jdoSetorganismo(ManualCargo paramManualCargo, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramManualCargo, jdoInheritedFieldCount + 7, paramManualCargo.organismo, paramOrganismo);
  }

  private static final String jdoGetprocesoSeleccion(ManualCargo paramManualCargo)
  {
    if (paramManualCargo.jdoFlags <= 0)
      return paramManualCargo.procesoSeleccion;
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.procesoSeleccion;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 8))
      return paramManualCargo.procesoSeleccion;
    return localStateManager.getStringField(paramManualCargo, jdoInheritedFieldCount + 8, paramManualCargo.procesoSeleccion);
  }

  private static final void jdoSetprocesoSeleccion(ManualCargo paramManualCargo, String paramString)
  {
    if (paramManualCargo.jdoFlags == 0)
    {
      paramManualCargo.procesoSeleccion = paramString;
      return;
    }
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.procesoSeleccion = paramString;
      return;
    }
    localStateManager.setStringField(paramManualCargo, jdoInheritedFieldCount + 8, paramManualCargo.procesoSeleccion, paramString);
  }

  private static final Tabulador jdoGettabulador(ManualCargo paramManualCargo)
  {
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.tabulador;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 9))
      return paramManualCargo.tabulador;
    return (Tabulador)localStateManager.getObjectField(paramManualCargo, jdoInheritedFieldCount + 9, paramManualCargo.tabulador);
  }

  private static final void jdoSettabulador(ManualCargo paramManualCargo, Tabulador paramTabulador)
  {
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.tabulador = paramTabulador;
      return;
    }
    localStateManager.setObjectField(paramManualCargo, jdoInheritedFieldCount + 9, paramManualCargo.tabulador, paramTabulador);
  }

  private static final Date jdoGettiempoSitp(ManualCargo paramManualCargo)
  {
    if (paramManualCargo.jdoFlags <= 0)
      return paramManualCargo.tiempoSitp;
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.tiempoSitp;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 10))
      return paramManualCargo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramManualCargo, jdoInheritedFieldCount + 10, paramManualCargo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ManualCargo paramManualCargo, Date paramDate)
  {
    if (paramManualCargo.jdoFlags == 0)
    {
      paramManualCargo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramManualCargo, jdoInheritedFieldCount + 10, paramManualCargo.tiempoSitp, paramDate);
  }

  private static final String jdoGettipoCargo(ManualCargo paramManualCargo)
  {
    if (paramManualCargo.jdoFlags <= 0)
      return paramManualCargo.tipoCargo;
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.tipoCargo;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 11))
      return paramManualCargo.tipoCargo;
    return localStateManager.getStringField(paramManualCargo, jdoInheritedFieldCount + 11, paramManualCargo.tipoCargo);
  }

  private static final void jdoSettipoCargo(ManualCargo paramManualCargo, String paramString)
  {
    if (paramManualCargo.jdoFlags == 0)
    {
      paramManualCargo.tipoCargo = paramString;
      return;
    }
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.tipoCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramManualCargo, jdoInheritedFieldCount + 11, paramManualCargo.tipoCargo, paramString);
  }

  private static final String jdoGettipoManual(ManualCargo paramManualCargo)
  {
    if (paramManualCargo.jdoFlags <= 0)
      return paramManualCargo.tipoManual;
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
      return paramManualCargo.tipoManual;
    if (localStateManager.isLoaded(paramManualCargo, jdoInheritedFieldCount + 12))
      return paramManualCargo.tipoManual;
    return localStateManager.getStringField(paramManualCargo, jdoInheritedFieldCount + 12, paramManualCargo.tipoManual);
  }

  private static final void jdoSettipoManual(ManualCargo paramManualCargo, String paramString)
  {
    if (paramManualCargo.jdoFlags == 0)
    {
      paramManualCargo.tipoManual = paramString;
      return;
    }
    StateManager localStateManager = paramManualCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualCargo.tipoManual = paramString;
      return;
    }
    localStateManager.setStringField(paramManualCargo, jdoInheritedFieldCount + 12, paramManualCargo.tipoManual, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}