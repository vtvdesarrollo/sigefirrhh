package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ManualPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addManualPersonal(ManualPersonal manualPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ManualPersonal manualPersonalNew = 
      (ManualPersonal)BeanUtils.cloneBean(
      manualPersonal);

    ManualCargoBeanBusiness manualCargoBeanBusiness = new ManualCargoBeanBusiness();

    if (manualPersonalNew.getManualCargo() != null) {
      manualPersonalNew.setManualCargo(
        manualCargoBeanBusiness.findManualCargoById(
        manualPersonalNew.getManualCargo().getIdManualCargo()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (manualPersonalNew.getTipoPersonal() != null) {
      manualPersonalNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        manualPersonalNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(manualPersonalNew);
  }

  public void updateManualPersonal(ManualPersonal manualPersonal) throws Exception
  {
    ManualPersonal manualPersonalModify = 
      findManualPersonalById(manualPersonal.getIdManualPersonal());

    ManualCargoBeanBusiness manualCargoBeanBusiness = new ManualCargoBeanBusiness();

    if (manualPersonal.getManualCargo() != null) {
      manualPersonal.setManualCargo(
        manualCargoBeanBusiness.findManualCargoById(
        manualPersonal.getManualCargo().getIdManualCargo()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (manualPersonal.getTipoPersonal() != null) {
      manualPersonal.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        manualPersonal.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(manualPersonalModify, manualPersonal);
  }

  public void deleteManualPersonal(ManualPersonal manualPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ManualPersonal manualPersonalDelete = 
      findManualPersonalById(manualPersonal.getIdManualPersonal());
    pm.deletePersistent(manualPersonalDelete);
  }

  public ManualPersonal findManualPersonalById(long idManualPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idManualPersonal == pIdManualPersonal";
    Query query = pm.newQuery(ManualPersonal.class, filter);

    query.declareParameters("long pIdManualPersonal");

    parameters.put("pIdManualPersonal", new Long(idManualPersonal));

    Collection colManualPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colManualPersonal.iterator();
    return (ManualPersonal)iterator.next();
  }

  public Collection findManualPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent manualPersonalExtent = pm.getExtent(
      ManualPersonal.class, true);
    Query query = pm.newQuery(manualPersonalExtent);
    query.setOrdering("tipoPersonal.nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByManualCargo(long idManualCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "manualCargo.idManualCargo == pIdManualCargo";

    Query query = pm.newQuery(ManualPersonal.class, filter);

    query.declareParameters("long pIdManualCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdManualCargo", new Long(idManualCargo));

    query.setOrdering("tipoPersonal.nombre ascending");

    Collection colManualPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colManualPersonal);

    return colManualPersonal;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ManualPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("tipoPersonal.nombre ascending");

    Collection colManualPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colManualPersonal);

    return colManualPersonal;
  }
}