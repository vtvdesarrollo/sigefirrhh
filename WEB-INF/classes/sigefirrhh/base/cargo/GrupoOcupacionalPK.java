package sigefirrhh.base.cargo;

import java.io.Serializable;

public class GrupoOcupacionalPK
  implements Serializable
{
  public long idGrupoOcupacional;

  public GrupoOcupacionalPK()
  {
  }

  public GrupoOcupacionalPK(long idGrupoOcupacional)
  {
    this.idGrupoOcupacional = idGrupoOcupacional;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((GrupoOcupacionalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(GrupoOcupacionalPK thatPK)
  {
    return 
      this.idGrupoOcupacional == thatPK.idGrupoOcupacional;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idGrupoOcupacional)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idGrupoOcupacional);
  }
}