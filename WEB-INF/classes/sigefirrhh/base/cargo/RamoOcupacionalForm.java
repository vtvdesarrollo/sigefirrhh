package sigefirrhh.base.cargo;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class RamoOcupacionalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RamoOcupacionalForm.class.getName());
  private RamoOcupacional ramoOcupacional;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoFacade cargoFacade = new CargoFacade();
  private boolean showRamoOcupacionalByCodRamoOcupacional;
  private boolean showRamoOcupacionalByNombre;
  private String findCodRamoOcupacional;
  private String findNombre;
  private Object stateResultRamoOcupacionalByCodRamoOcupacional = null;

  private Object stateResultRamoOcupacionalByNombre = null;

  public String getFindCodRamoOcupacional()
  {
    return this.findCodRamoOcupacional;
  }
  public void setFindCodRamoOcupacional(String findCodRamoOcupacional) {
    this.findCodRamoOcupacional = findCodRamoOcupacional;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public RamoOcupacional getRamoOcupacional() {
    if (this.ramoOcupacional == null) {
      this.ramoOcupacional = new RamoOcupacional();
    }
    return this.ramoOcupacional;
  }

  public RamoOcupacionalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findRamoOcupacionalByCodRamoOcupacional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findRamoOcupacionalByCodRamoOcupacional(this.findCodRamoOcupacional);
      this.showRamoOcupacionalByCodRamoOcupacional = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRamoOcupacionalByCodRamoOcupacional)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodRamoOcupacional = null;
    this.findNombre = null;

    return null;
  }

  public String findRamoOcupacionalByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findRamoOcupacionalByNombre(this.findNombre);
      this.showRamoOcupacionalByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRamoOcupacionalByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodRamoOcupacional = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowRamoOcupacionalByCodRamoOcupacional() {
    return this.showRamoOcupacionalByCodRamoOcupacional;
  }
  public boolean isShowRamoOcupacionalByNombre() {
    return this.showRamoOcupacionalByNombre;
  }

  public String selectRamoOcupacional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idRamoOcupacional = 
      Long.parseLong((String)requestParameterMap.get("idRamoOcupacional"));
    try
    {
      this.ramoOcupacional = 
        this.cargoFacade.findRamoOcupacionalById(
        idRamoOcupacional);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.ramoOcupacional = null;
    this.showRamoOcupacionalByCodRamoOcupacional = false;
    this.showRamoOcupacionalByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.ramoOcupacional.getTiempoSitp() != null) && 
      (this.ramoOcupacional.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.cargoFacade.addRamoOcupacional(
          this.ramoOcupacional);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.cargoFacade.updateRamoOcupacional(
          this.ramoOcupacional);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.cargoFacade.deleteRamoOcupacional(
        this.ramoOcupacional);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.ramoOcupacional = new RamoOcupacional();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.ramoOcupacional.setIdRamoOcupacional(identityGenerator.getNextSequenceNumber("sigefirrhh.base.cargo.RamoOcupacional"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.ramoOcupacional = new RamoOcupacional();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}