package sigefirrhh.base.cargo;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class DetalleTabuladorForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(DetalleTabuladorForm.class.getName());
  private DetalleTabulador detalleTabulador;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoFacade cargoFacade = new CargoFacade();
  private boolean showDetalleTabuladorByTabulador;
  private String findSelectTabulador;
  private Collection findColTabulador;
  private Collection colTabulador;
  private String selectTabulador;
  private Object stateResultDetalleTabuladorByTabulador = null;

  public String getFindSelectTabulador()
  {
    return this.findSelectTabulador;
  }
  public void setFindSelectTabulador(String valTabulador) {
    this.findSelectTabulador = valTabulador;
  }

  public Collection getFindColTabulador() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTabulador.iterator();
    Tabulador tabulador = null;
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tabulador.getIdTabulador()), 
        tabulador.toString()));
    }
    return col;
  }

  public String getSelectTabulador()
  {
    return this.selectTabulador;
  }
  public void setSelectTabulador(String valTabulador) {
    Iterator iterator = this.colTabulador.iterator();
    Tabulador tabulador = null;
    this.detalleTabulador.setTabulador(null);
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      if (String.valueOf(tabulador.getIdTabulador()).equals(
        valTabulador)) {
        this.detalleTabulador.setTabulador(
          tabulador);
        break;
      }
    }
    this.selectTabulador = valTabulador;
  }
  public Collection getResult() {
    return this.result;
  }

  public DetalleTabulador getDetalleTabulador() {
    if (this.detalleTabulador == null) {
      this.detalleTabulador = new DetalleTabulador();
    }
    return this.detalleTabulador;
  }

  public DetalleTabuladorForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTabulador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTabulador.iterator();
    Tabulador tabulador = null;
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tabulador.getIdTabulador()), 
        tabulador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTabulador = 
        this.cargoFacade.findTabuladorByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTabulador = 
        this.cargoFacade.findTabuladorByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findDetalleTabuladorByTabulador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findDetalleTabuladorByTabulador(Long.valueOf(this.findSelectTabulador).longValue());
      this.showDetalleTabuladorByTabulador = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDetalleTabuladorByTabulador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTabulador = null;

    return null;
  }

  public boolean isShowDetalleTabuladorByTabulador() {
    return this.showDetalleTabuladorByTabulador;
  }

  public String selectDetalleTabulador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTabulador = null;

    long idDetalleTabulador = 
      Long.parseLong((String)requestParameterMap.get("idDetalleTabulador"));
    try
    {
      this.detalleTabulador = 
        this.cargoFacade.findDetalleTabuladorById(
        idDetalleTabulador);
      if (this.detalleTabulador.getTabulador() != null) {
        this.selectTabulador = 
          String.valueOf(this.detalleTabulador.getTabulador().getIdTabulador());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.detalleTabulador = null;
    this.showDetalleTabuladorByTabulador = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.cargoFacade.addDetalleTabulador(
          this.detalleTabulador);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.cargoFacade.updateDetalleTabulador(
          this.detalleTabulador);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.cargoFacade.deleteDetalleTabulador(
        this.detalleTabulador);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.detalleTabulador = new DetalleTabulador();

    this.selectTabulador = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.detalleTabulador.setIdDetalleTabulador(identityGenerator.getNextSequenceNumber("sigefirrhh.base.cargo.DetalleTabulador"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.detalleTabulador = new DetalleTabulador();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}