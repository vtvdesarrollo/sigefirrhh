package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class CargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCargo(Cargo cargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Cargo cargoNew = 
      (Cargo)BeanUtils.cloneBean(
      cargo);

    if (cargoNew.getManualCargo().getMultipleDescripcion().equals("S")) {
      if (searchCargo(cargoNew.getCodCargo(), cargoNew.getManualCargo().getIdManualCargo(), 0L, cargoNew.getDescripcionCargo()) != 0) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("El Código Cargo y Descripción ya existe");
        throw error;
      }
    } else if ((cargoNew.getManualCargo().getMultipleDescripcion().equals("N")) && 
      (searchCargo(cargoNew.getCodCargo(), cargoNew.getManualCargo().getIdManualCargo(), 0L) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Código Cargo ya existe");
      throw error;
    }

    ManualCargoBeanBusiness manualCargoBeanBusiness = new ManualCargoBeanBusiness();

    if (cargoNew.getManualCargo() != null) {
      cargoNew.setManualCargo(
        manualCargoBeanBusiness.findManualCargoById(
        cargoNew.getManualCargo().getIdManualCargo()));
    }

    SerieCargoBeanBusiness serieCargoBeanBusiness = new SerieCargoBeanBusiness();

    if (cargoNew.getSerieCargo() != null) {
      cargoNew.setSerieCargo(
        serieCargoBeanBusiness.findSerieCargoById(
        cargoNew.getSerieCargo().getIdSerieCargo()));
    }

    pm.makePersistent(cargoNew);
  }

  public void updateCargo(Cargo cargo) throws Exception
  {
    Cargo cargoModify = 
      findCargoById(cargo.getIdCargo());

    if (cargoModify.getManualCargo().getMultipleDescripcion().equals("S")) {
      if (searchCargo(cargo.getCodCargo(), cargo.getManualCargo().getIdManualCargo(), cargoModify.getIdCargo(), cargo.getDescripcionCargo()) != 0) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("El Código Cargo y Descripción ya existe");
        throw error;
      }
    } else if ((cargoModify.getManualCargo().getMultipleDescripcion().equals("N")) && 
      (searchCargo(cargo.getCodCargo(), cargo.getManualCargo().getIdManualCargo(), cargoModify.getIdCargo()) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Código Cargo ya existe");
      throw error;
    }

    ManualCargoBeanBusiness manualCargoBeanBusiness = new ManualCargoBeanBusiness();

    if (cargo.getManualCargo() != null) {
      cargo.setManualCargo(
        manualCargoBeanBusiness.findManualCargoById(
        cargo.getManualCargo().getIdManualCargo()));
    }

    SerieCargoBeanBusiness serieCargoBeanBusiness = new SerieCargoBeanBusiness();

    if (cargo.getSerieCargo() != null) {
      cargo.setSerieCargo(
        serieCargoBeanBusiness.findSerieCargoById(
        cargo.getSerieCargo().getIdSerieCargo()));
    }

    BeanUtils.copyProperties(cargoModify, cargo);
  }

  public void deleteCargo(Cargo cargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Cargo cargoDelete = 
      findCargoById(cargo.getIdCargo());
    pm.deletePersistent(cargoDelete);
  }

  public Cargo findCargoById(long idCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCargo == pIdCargo";
    Query query = pm.newQuery(Cargo.class, filter);

    query.declareParameters("long pIdCargo");

    parameters.put("pIdCargo", new Long(idCargo));

    Collection colCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCargo.iterator();
    return (Cargo)iterator.next();
  }

  public Collection findCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent cargoExtent = pm.getExtent(
      Cargo.class, true);
    Query query = pm.newQuery(cargoExtent);
    query.setOrdering("descripcionCargo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByManualCargo(long idManualCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "manualCargo.idManualCargo == pIdManualCargo";

    Query query = pm.newQuery(Cargo.class, filter);

    query.declareParameters("long pIdManualCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdManualCargo", new Long(idManualCargo));

    query.setOrdering("descripcionCargo ascending");

    Collection colCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargo);

    return colCargo;
  }

  public Collection findByCodCargo(String codCargo, long idManualCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCargo == pCodCargo && manualCargo.idManualCargo == pIdManualCargo";

    Query query = pm.newQuery(Cargo.class, filter);

    query.declareParameters("java.lang.String pCodCargo, long pIdManualCargo");
    HashMap parameters = new HashMap();

    parameters.put("pCodCargo", new String(codCargo));
    parameters.put("pIdManualCargo", new Long(idManualCargo));

    query.setOrdering("descripcionCargo ascending");

    Collection colCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargo);

    return colCargo;
  }

  public Collection findByCodCargo(String codCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCargo == pCodCargo";

    Query query = pm.newQuery(Cargo.class, filter);

    query.declareParameters("java.lang.String pCodCargo");
    HashMap parameters = new HashMap();

    parameters.put("pCodCargo", new String(codCargo));

    query.setOrdering("descripcionCargo ascending");

    Collection colCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargo);

    return colCargo;
  }

  public Collection findByDescripcionCargo(String descripcionCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcionCargo.startsWith(pDescripcionCargo)";

    Query query = pm.newQuery(Cargo.class, filter);

    query.declareParameters("java.lang.String pDescripcionCargo");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcionCargo", new String(descripcionCargo));

    query.setOrdering("descripcionCargo ascending");

    Collection colCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargo);

    return colCargo;
  }

  public Collection findByDescripcionCargo(String descripcionCargo, long idManualCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcionCargo.startsWith(pDescripcionCargo) && manualCargo.idManualCargo == pIdManualCargo";

    Query query = pm.newQuery(Cargo.class, filter);

    query.declareParameters("java.lang.String pDescripcionCargo, long pIdManualCargo");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcionCargo", new String(descripcionCargo));
    parameters.put("pIdManualCargo", new Long(idManualCargo));

    query.setOrdering("descripcionCargo ascending");

    Collection colCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargo);

    return colCargo;
  }

  public int searchCargo(String codCargo, long idManualCargo, long idCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCargo == pCodCargo && manualCargo.idManualCargo == pIdManualCargo && idCargo != pIdCargo";

    Query query = pm.newQuery(Cargo.class, filter);

    query.declareParameters("String pCodCargo, long pIdManualCargo, long pIdCargo");
    HashMap parameters = new HashMap();

    parameters.put("pCodCargo", new String(codCargo));
    parameters.put("pIdManualCargo", new Long(idManualCargo));
    parameters.put("pIdCargo", new Long(idCargo));

    Collection colCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargo);

    return colCargo.size();
  }

  public int searchCargo(String codCargo, long idManualCargo, long idCargo, String descripcionCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCargo == pCodCargo && manualCargo.idManualCargo == pIdManualCargo && descripcionCargo == pDescripcionCargo && idCargo != pIdCargo";

    Query query = pm.newQuery(Cargo.class, filter);

    query.declareParameters("String pCodCargo, long pIdManualCargo, String pDescripcionCargo, long pIdCargo");
    HashMap parameters = new HashMap();

    parameters.put("pCodCargo", new String(codCargo));
    parameters.put("pIdManualCargo", new Long(idManualCargo));
    parameters.put("pDescripcionCargo", new String(descripcionCargo));
    parameters.put("pIdCargo", new Long(idCargo));

    Collection colCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargo);

    return colCargo.size();
  }
}