package sigefirrhh.base.cargo;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class CargoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CargoForm.class.getName());
  private Cargo cargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private CargoFacade cargoFacade = new CargoFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private boolean showCargoByManualCargo;
  private boolean showCargoByCodCargo;
  private boolean showCargoByDescripcionCargo;
  private String findSelectManualCargo;
  private String findCodCargo;
  private String findDescripcionCargo;
  private Collection findColManualCargo;
  private Collection colManualCargo;
  private Collection colSerieCargo;
  private String selectManualCargo;
  private String selectSerieCargo;
  private Object stateScrollCargoByManualCargo = null;
  private Object stateResultCargoByManualCargo = null;

  private Object stateScrollCargoByCodCargo = null;
  private Object stateResultCargoByCodCargo = null;

  private Object stateScrollCargoByDescripcionCargo = null;
  private Object stateResultCargoByDescripcionCargo = null;

  public String getFindSelectManualCargo()
  {
    return this.findSelectManualCargo;
  }
  public void setFindSelectManualCargo(String valManualCargo) {
    this.findSelectManualCargo = valManualCargo;
  }

  public Collection getFindColManualCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColManualCargo.iterator();
    ManualCargo manualCargo = null;
    while (iterator.hasNext()) {
      manualCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargo.getIdManualCargo()), 
        manualCargo.toString()));
    }
    return col;
  }
  public String getFindCodCargo() {
    return this.findCodCargo;
  }
  public void setFindCodCargo(String findCodCargo) {
    this.findCodCargo = findCodCargo;
  }
  public String getFindDescripcionCargo() {
    return this.findDescripcionCargo;
  }
  public void setFindDescripcionCargo(String findDescripcionCargo) {
    this.findDescripcionCargo = findDescripcionCargo;
  }

  public String getSelectManualCargo()
  {
    return this.selectManualCargo;
  }
  public void setSelectManualCargo(String valManualCargo) {
    Iterator iterator = this.colManualCargo.iterator();
    ManualCargo manualCargo = null;
    this.cargo.setManualCargo(null);
    while (iterator.hasNext()) {
      manualCargo = (ManualCargo)iterator.next();
      if (String.valueOf(manualCargo.getIdManualCargo()).equals(
        valManualCargo)) {
        this.cargo.setManualCargo(
          manualCargo);
      }
    }
    this.selectManualCargo = valManualCargo;
  }
  public String getSelectSerieCargo() {
    return this.selectSerieCargo;
  }
  public void setSelectSerieCargo(String valSerieCargo) {
    Iterator iterator = this.colSerieCargo.iterator();
    SerieCargo serieCargo = null;
    this.cargo.setSerieCargo(null);
    while (iterator.hasNext()) {
      serieCargo = (SerieCargo)iterator.next();
      if (String.valueOf(serieCargo.getIdSerieCargo()).equals(
        valSerieCargo)) {
        this.cargo.setSerieCargo(
          serieCargo);
      }
    }
    this.selectSerieCargo = valSerieCargo;
  }
  public Collection getResult() {
    return this.result;
  }

  public Cargo getCargo() {
    if (this.cargo == null) {
      this.cargo = new Cargo();
    }
    return this.cargo;
  }

  public CargoForm() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColManualCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargo.iterator();
    ManualCargo manualCargo = null;
    while (iterator.hasNext()) {
      manualCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargo.getIdManualCargo()), 
        manualCargo.toString()));
    }
    return col;
  }

  public Collection getListTipoCargo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Cargo.LISTA_TIPO_CARGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCaucion()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Cargo.LISTA_CAUCION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColSerieCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colSerieCargo.iterator();
    SerieCargo serieCargo = null;
    while (iterator.hasNext()) {
      serieCargo = (SerieCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(serieCargo.getIdSerieCargo()), 
        serieCargo.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColManualCargo = 
        this.cargoFacade.findManualCargoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colManualCargo = 
        this.cargoFacade.findManualCargoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colSerieCargo = 
        this.cargoFacade.findAllSerieCargo();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findCargoByManualCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findCargoByManualCargo(Long.valueOf(this.findSelectManualCargo).longValue());
      this.showCargoByManualCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCargoByManualCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectManualCargo = null;
    this.findCodCargo = null;
    this.findDescripcionCargo = null;

    return null;
  }

  public String findCargoByCodCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoNoGenFacade.findCargoByCodCargo(this.findCodCargo, Long.valueOf(this.findSelectManualCargo).longValue());
      this.showCargoByCodCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCargoByCodCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectManualCargo = null;
    this.findCodCargo = null;
    this.findDescripcionCargo = null;

    return null;
  }

  public String findCargoByDescripcionCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoNoGenFacade.findCargoByDescripcionCargo(this.findDescripcionCargo, Long.valueOf(this.findSelectManualCargo).longValue());
      this.showCargoByDescripcionCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCargoByDescripcionCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectManualCargo = null;
    this.findCodCargo = null;
    this.findDescripcionCargo = null;

    return null;
  }

  public boolean isShowCargoByManualCargo() {
    return this.showCargoByManualCargo;
  }
  public boolean isShowCargoByCodCargo() {
    return this.showCargoByCodCargo;
  }
  public boolean isShowCargoByDescripcionCargo() {
    return this.showCargoByDescripcionCargo;
  }

  public String selectCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectManualCargo = null;
    this.selectSerieCargo = null;

    long idCargo = 
      Long.parseLong((String)requestParameterMap.get("idCargo"));
    try
    {
      this.cargo = 
        this.cargoFacade.findCargoById(
        idCargo);
      if (this.cargo.getManualCargo() != null) {
        this.selectManualCargo = 
          String.valueOf(this.cargo.getManualCargo().getIdManualCargo());
      }
      if (this.cargo.getSerieCargo() != null) {
        this.selectSerieCargo = 
          String.valueOf(this.cargo.getSerieCargo().getIdSerieCargo());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.cargo = null;
    this.showCargoByManualCargo = false;
    this.showCargoByCodCargo = false;
    this.showCargoByDescripcionCargo = false;
  }

  public String edit()
  {
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.cargoFacade.addCargo(
          this.cargo);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.cargo);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.cargoFacade.updateCargo(
          this.cargo);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.cargo);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      ErrorSistema a = (ErrorSistema)e;
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.cargoFacade.deleteCargo(
        this.cargo);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.cargo);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;
    this.cargo = new Cargo();

    this.selectManualCargo = null;

    this.selectSerieCargo = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.cargo.setIdCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.base.cargo.Cargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.scrollx = 0;
    this.scrolly = 0;
    resetResult();

    this.cargo = new Cargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }
  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }

  public LoginSession getLogin() {
    return this.login;
  }
}