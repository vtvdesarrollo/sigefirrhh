package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class DetalleTabuladorBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDetalleTabulador(DetalleTabulador detalleTabulador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DetalleTabulador detalleTabuladorNew = 
      (DetalleTabulador)BeanUtils.cloneBean(
      detalleTabulador);

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (detalleTabuladorNew.getTabulador() != null) {
      detalleTabuladorNew.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        detalleTabuladorNew.getTabulador().getIdTabulador()));
    }
    pm.makePersistent(detalleTabuladorNew);
  }

  public void updateDetalleTabulador(DetalleTabulador detalleTabulador) throws Exception
  {
    DetalleTabulador detalleTabuladorModify = 
      findDetalleTabuladorById(detalleTabulador.getIdDetalleTabulador());

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (detalleTabulador.getTabulador() != null) {
      detalleTabulador.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        detalleTabulador.getTabulador().getIdTabulador()));
    }

    BeanUtils.copyProperties(detalleTabuladorModify, detalleTabulador);
  }

  public void deleteDetalleTabulador(DetalleTabulador detalleTabulador) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DetalleTabulador detalleTabuladorDelete = 
      findDetalleTabuladorById(detalleTabulador.getIdDetalleTabulador());
    pm.deletePersistent(detalleTabuladorDelete);
  }

  public DetalleTabulador findDetalleTabuladorById(long idDetalleTabulador) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDetalleTabulador == pIdDetalleTabulador";
    Query query = pm.newQuery(DetalleTabulador.class, filter);

    query.declareParameters("long pIdDetalleTabulador");

    parameters.put("pIdDetalleTabulador", new Long(idDetalleTabulador));

    Collection colDetalleTabulador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDetalleTabulador.iterator();
    return (DetalleTabulador)iterator.next();
  }

  public Collection findDetalleTabuladorAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent detalleTabuladorExtent = pm.getExtent(
      DetalleTabulador.class, true);
    Query query = pm.newQuery(detalleTabuladorExtent);
    query.setOrdering("grado ascending, subGrado ascending, paso ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTabulador(long idTabulador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tabulador.idTabulador == pIdTabulador";

    Query query = pm.newQuery(DetalleTabulador.class, filter);

    query.declareParameters("long pIdTabulador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTabulador", new Long(idTabulador));

    query.setOrdering("grado ascending, subGrado ascending, paso ascending");

    Collection colDetalleTabulador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDetalleTabulador);

    return colDetalleTabulador;
  }
}