package sigefirrhh.base.cargo;

import java.io.Serializable;
import java.util.Collection;

public class CargoNoGenBusiness extends CargoBusiness
  implements Serializable
{
  private CargoNoGenBeanBusiness cargoNoGenBeanBusiness = new CargoNoGenBeanBusiness();

  private CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

  private DetalleTabuladorNoGenBeanBusiness detalleTabuladorNoGenBeanBusiness = new DetalleTabuladorNoGenBeanBusiness();

  private ManualCargoNoGenBeanBusiness manualCargoNoGenBeanBusiness = new ManualCargoNoGenBeanBusiness();

  public Collection findCargoByDescripcionCargo(String descripcionCargo, long idManualCargo)
    throws Exception
  {
    return this.cargoBeanBusiness.findByDescripcionCargo(descripcionCargo, idManualCargo);
  }

  public Collection findCargoByCodCargo(String codCargo, long idManualCargo) throws Exception
  {
    return this.cargoBeanBusiness.findByCodCargo(codCargo, idManualCargo);
  }

  public DetalleTabulador findDetalleTabuladorForRegistroCargos(long idTabulador, int grado, int subGrado, int paso) throws Exception {
    return this.detalleTabuladorNoGenBeanBusiness.findForRegistroCargos(idTabulador, grado, subGrado, paso);
  }

  public Collection findManualCargoByRegistro(long idRegistro) throws Exception {
    return this.manualCargoNoGenBeanBusiness.findByRegistro(idRegistro);
  }

  public Collection findManualCargoByProcesoSeleccion(String procesoSeleccion, long idOrganismo) throws Exception {
    return this.manualCargoNoGenBeanBusiness.findByProcesoSeleccion(procesoSeleccion, idOrganismo);
  }

  public Collection findManualCargoByManualPersonal(long idTipoPersonal) throws Exception {
    return this.manualCargoNoGenBeanBusiness.findByManualPersonal(idTipoPersonal);
  }

  public DetalleTabulador findDetalleTabuladorByMonto(long idTabulador, int grado, int subGrado, double monto) throws Exception {
    return this.detalleTabuladorNoGenBeanBusiness.findByMonto(idTabulador, grado, subGrado, monto);
  }

  public DetalleTabulador findDetalleTabuladorMaximoPaso(long idTabulador, int grado, int subGrado) throws Exception {
    return this.detalleTabuladorNoGenBeanBusiness.findMaximoPaso(idTabulador, grado, subGrado);
  }

  public Collection findCargoByIdManualCargo(long idManualCargo) throws Exception {
    return this.cargoNoGenBeanBusiness.findByIdManualCargo(idManualCargo);
  }

  public Collection findCargoByPersonal(long idPersonal, String estatus) throws Exception {
    return this.cargoNoGenBeanBusiness.findByPersonal(idPersonal, estatus);
  }

  public Collection findCargoByIdManualCargoAndIdDependencia(long idManualCargo, long idDependencia) throws Exception {
    return this.cargoNoGenBeanBusiness.findByIdManualCargoAndIdDependencia(idManualCargo, idDependencia);
  }

  public Collection findCargoByIdManualCargoAndIdRegion(long idManualCargo, long idRegion) throws Exception {
    return this.cargoNoGenBeanBusiness.findByIdManualCargoAndIdRegion(idManualCargo, idRegion);
  }

  public Collection findCargoByIdManualCargoNoLista(long idManualCargo) throws Exception {
    return this.cargoNoGenBeanBusiness.findByIdManualCargoNoLista(idManualCargo);
  }

  public Cargo findCargoByCodCargoAndCodManualCargo(String codCargo, int codManualCargo) throws Exception {
    return this.cargoNoGenBeanBusiness.findByCodCargoAndCodManualCargo(codCargo, codManualCargo);
  }
}