package sigefirrhh.base.cargo;

import java.io.Serializable;

public class ManualPersonalPK
  implements Serializable
{
  public long idManualPersonal;

  public ManualPersonalPK()
  {
  }

  public ManualPersonalPK(long idManualPersonal)
  {
    this.idManualPersonal = idManualPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ManualPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ManualPersonalPK thatPK)
  {
    return 
      this.idManualPersonal == thatPK.idManualPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idManualPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idManualPersonal);
  }
}