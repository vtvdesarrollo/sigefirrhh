package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import sigefirrhh.general.Lista;

public class CargoNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByIdManualCargo(long id)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsData = null;
    PreparedStatement stData = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select a.id_cargo as id, (a.descripcion_cargo || ' - ' || a.cod_cargo)  as descripcion  ");
      sql.append(" from cargo a");
      sql.append(" where a.id_manual_cargo = ?");
      sql.append(" order by a.descripcion_cargo");

      stData = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stData.setLong(1, id);
      rsData = stData.executeQuery();
      Lista lista = new Lista();
      while (rsData.next()) {
        lista = new Lista();
        lista.setId(rsData.getLong("id"));
        lista.setNombre(rsData.getString("descripcion"));
        col.add(lista);
      }

      return col;
    } finally {
      if (rsData != null) try {
          rsData.close();
        } catch (Exception localException3) {
        } if (stData != null) try {
          stData.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findByPersonal(long idPersonal, String estatus) throws Exception {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select a.id_cargo as id, (a.descripcion_cargo || ' - ' || a.cod_cargo)  as descripcion   ");
      sql.append(" from cargo a");
      sql.append(" where id_cargo in (");
      sql.append(" select id_cargo from trabajador");
      sql.append(" where id_personal = ?");
      sql.append(" and estatus = ?)");
      sql.append(" order by a.descripcion_cargo");
      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idPersonal);
      stRegistros.setString(2, estatus);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findByIdManualCargoAndIdDependencia(long idManualCargo, long idDependencia) throws Exception {
    Collection col = new ArrayList();
    Connection connection = Resource.getConnection();
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select a.id_cargo as id, (a.descripcion_cargo || ' - ' || a.cod_cargo)  as descripcion   ");
      sql.append(" from cargo a");
      sql.append(" where id_cargo in (");
      sql.append(" select distinct id_cargo from registrocargos");
      sql.append(" where id_dependencia = ?)");
      sql.append(" and id_manual_cargo = ?");
      sql.append(" order by a.descripcion_cargo");
      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idDependencia);
      stRegistros.setLong(2, idManualCargo);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findByIdManualCargoAndIdRegion(long idManualCargo, long idRegion) throws Exception { Collection col = new ArrayList();
    Connection connection = Resource.getConnection();
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      sql.append("select a.id_cargo as id, (a.descripcion_cargo || ' - ' || a.cod_cargo)  as descripcion   ");
      sql.append(" from cargo a");
      sql.append(" where id_cargo in (");
      sql.append(" select distinct id_cargo from registrocargos rc, dependencia d, sede s ");
      sql.append(" where rc.id_dependencia = d.id_dependencia");
      sql.append(" and d.id_sede = s.id_sede");
      sql.append(" and s.id_region = ?)");
      sql.append(" and id_manual_cargo = ?");
      sql.append(" order by a.descripcion_cargo");
      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idRegion);
      stRegistros.setLong(2, idManualCargo);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    } } 
  public Collection findByIdManualCargoNoLista(long idManualCargo) throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsData = null;
    PreparedStatement stData = null;
    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select a.id_cargo as id, (a.descripcion_cargo || ' - ' || a.cod_cargo)  as descripcion  ");
      sql.append(" from cargo a");
      sql.append(" where a.id_manual_cargo = ?");
      sql.append(" order by a.descripcion_cargo");

      stData = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stData.setLong(1, idManualCargo);
      rsData = stData.executeQuery();

      while (rsData.next()) {
        col.add(Long.valueOf(String.valueOf(rsData.getLong("id"))));
        col.add(rsData.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsData != null) try {
          rsData.close();
        } catch (Exception localException3) {
        } if (stData != null) try {
          stData.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Cargo findByCodCargoAndCodManualCargo(String codCargo, int codManualCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCargo == pCodCargo && manualCargo.codManualCargo == pCodManualCargo";

    Query query = pm.newQuery(Cargo.class, filter);

    query.declareParameters("String pCodCargo, int pCodManualCargo");
    HashMap parameters = new HashMap();

    parameters.put("pCodCargo", new String(codCargo));
    parameters.put("pCodManualCargo", new Integer(codManualCargo));

    Collection colCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargo);
    Cargo cargo = (Cargo)colCargo.iterator().next();

    return cargo;
  }
}