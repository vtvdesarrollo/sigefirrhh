package sigefirrhh.base.cargo;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class SueldoMinimo
  implements Serializable, PersistenceCapable
{
  private long idSueldoMinimo;
  private Date fechaVigencia;
  private double sueldoMinimo;
  private double sueldoRural;
  private String baseLegal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "baseLegal", "fechaVigencia", "idSitp", "idSueldoMinimo", "sueldoMinimo", "sueldoRural", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Integer.TYPE, Long.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public SueldoMinimo()
  {
    jdoSetsueldoRural(this, 0.0D);
  }

  public String toString()
  {
    return jdoGetfechaVigencia(this) + "  -  " + 
      jdoGetsueldoMinimo(this);
  }

  public String getBaseLegal() {
    return jdoGetbaseLegal(this);
  }

  public Date getFechaVigencia() {
    return jdoGetfechaVigencia(this);
  }

  public long getIdSueldoMinimo() {
    return jdoGetidSueldoMinimo(this);
  }

  public double getSueldoMinimo() {
    return jdoGetsueldoMinimo(this);
  }

  public double getSueldoRural() {
    return jdoGetsueldoRural(this);
  }

  public void setBaseLegal(String string) {
    jdoSetbaseLegal(this, string);
  }

  public void setFechaVigencia(Date date) {
    jdoSetfechaVigencia(this, date);
  }

  public void setIdSueldoMinimo(long l) {
    jdoSetidSueldoMinimo(this, l);
  }

  public void setSueldoMinimo(double d) {
    jdoSetsueldoMinimo(this, d);
  }

  public void setSueldoRural(double d) {
    jdoSetsueldoRural(this, d);
  }

  public int getIdSitp() {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i) {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date) {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.cargo.SueldoMinimo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SueldoMinimo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SueldoMinimo localSueldoMinimo = new SueldoMinimo();
    localSueldoMinimo.jdoFlags = 1;
    localSueldoMinimo.jdoStateManager = paramStateManager;
    return localSueldoMinimo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SueldoMinimo localSueldoMinimo = new SueldoMinimo();
    localSueldoMinimo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSueldoMinimo.jdoFlags = 1;
    localSueldoMinimo.jdoStateManager = paramStateManager;
    return localSueldoMinimo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.baseLegal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSueldoMinimo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoMinimo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoRural);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseLegal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSueldoMinimo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoMinimo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoRural = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SueldoMinimo paramSueldoMinimo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.baseLegal = paramSueldoMinimo.baseLegal;
      return;
    case 1:
      if (paramSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramSueldoMinimo.fechaVigencia;
      return;
    case 2:
      if (paramSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramSueldoMinimo.idSitp;
      return;
    case 3:
      if (paramSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.idSueldoMinimo = paramSueldoMinimo.idSueldoMinimo;
      return;
    case 4:
      if (paramSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoMinimo = paramSueldoMinimo.sueldoMinimo;
      return;
    case 5:
      if (paramSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoRural = paramSueldoMinimo.sueldoRural;
      return;
    case 6:
      if (paramSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramSueldoMinimo.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SueldoMinimo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SueldoMinimo localSueldoMinimo = (SueldoMinimo)paramObject;
    if (localSueldoMinimo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSueldoMinimo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SueldoMinimoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SueldoMinimoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SueldoMinimoPK))
      throw new IllegalArgumentException("arg1");
    SueldoMinimoPK localSueldoMinimoPK = (SueldoMinimoPK)paramObject;
    localSueldoMinimoPK.idSueldoMinimo = this.idSueldoMinimo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SueldoMinimoPK))
      throw new IllegalArgumentException("arg1");
    SueldoMinimoPK localSueldoMinimoPK = (SueldoMinimoPK)paramObject;
    this.idSueldoMinimo = localSueldoMinimoPK.idSueldoMinimo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SueldoMinimoPK))
      throw new IllegalArgumentException("arg2");
    SueldoMinimoPK localSueldoMinimoPK = (SueldoMinimoPK)paramObject;
    localSueldoMinimoPK.idSueldoMinimo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SueldoMinimoPK))
      throw new IllegalArgumentException("arg2");
    SueldoMinimoPK localSueldoMinimoPK = (SueldoMinimoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localSueldoMinimoPK.idSueldoMinimo);
  }

  private static final String jdoGetbaseLegal(SueldoMinimo paramSueldoMinimo)
  {
    if (paramSueldoMinimo.jdoFlags <= 0)
      return paramSueldoMinimo.baseLegal;
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoMinimo.baseLegal;
    if (localStateManager.isLoaded(paramSueldoMinimo, jdoInheritedFieldCount + 0))
      return paramSueldoMinimo.baseLegal;
    return localStateManager.getStringField(paramSueldoMinimo, jdoInheritedFieldCount + 0, paramSueldoMinimo.baseLegal);
  }

  private static final void jdoSetbaseLegal(SueldoMinimo paramSueldoMinimo, String paramString)
  {
    if (paramSueldoMinimo.jdoFlags == 0)
    {
      paramSueldoMinimo.baseLegal = paramString;
      return;
    }
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoMinimo.baseLegal = paramString;
      return;
    }
    localStateManager.setStringField(paramSueldoMinimo, jdoInheritedFieldCount + 0, paramSueldoMinimo.baseLegal, paramString);
  }

  private static final Date jdoGetfechaVigencia(SueldoMinimo paramSueldoMinimo)
  {
    if (paramSueldoMinimo.jdoFlags <= 0)
      return paramSueldoMinimo.fechaVigencia;
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoMinimo.fechaVigencia;
    if (localStateManager.isLoaded(paramSueldoMinimo, jdoInheritedFieldCount + 1))
      return paramSueldoMinimo.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramSueldoMinimo, jdoInheritedFieldCount + 1, paramSueldoMinimo.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(SueldoMinimo paramSueldoMinimo, Date paramDate)
  {
    if (paramSueldoMinimo.jdoFlags == 0)
    {
      paramSueldoMinimo.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoMinimo.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSueldoMinimo, jdoInheritedFieldCount + 1, paramSueldoMinimo.fechaVigencia, paramDate);
  }

  private static final int jdoGetidSitp(SueldoMinimo paramSueldoMinimo)
  {
    if (paramSueldoMinimo.jdoFlags <= 0)
      return paramSueldoMinimo.idSitp;
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoMinimo.idSitp;
    if (localStateManager.isLoaded(paramSueldoMinimo, jdoInheritedFieldCount + 2))
      return paramSueldoMinimo.idSitp;
    return localStateManager.getIntField(paramSueldoMinimo, jdoInheritedFieldCount + 2, paramSueldoMinimo.idSitp);
  }

  private static final void jdoSetidSitp(SueldoMinimo paramSueldoMinimo, int paramInt)
  {
    if (paramSueldoMinimo.jdoFlags == 0)
    {
      paramSueldoMinimo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoMinimo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramSueldoMinimo, jdoInheritedFieldCount + 2, paramSueldoMinimo.idSitp, paramInt);
  }

  private static final long jdoGetidSueldoMinimo(SueldoMinimo paramSueldoMinimo)
  {
    return paramSueldoMinimo.idSueldoMinimo;
  }

  private static final void jdoSetidSueldoMinimo(SueldoMinimo paramSueldoMinimo, long paramLong)
  {
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoMinimo.idSueldoMinimo = paramLong;
      return;
    }
    localStateManager.setLongField(paramSueldoMinimo, jdoInheritedFieldCount + 3, paramSueldoMinimo.idSueldoMinimo, paramLong);
  }

  private static final double jdoGetsueldoMinimo(SueldoMinimo paramSueldoMinimo)
  {
    if (paramSueldoMinimo.jdoFlags <= 0)
      return paramSueldoMinimo.sueldoMinimo;
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoMinimo.sueldoMinimo;
    if (localStateManager.isLoaded(paramSueldoMinimo, jdoInheritedFieldCount + 4))
      return paramSueldoMinimo.sueldoMinimo;
    return localStateManager.getDoubleField(paramSueldoMinimo, jdoInheritedFieldCount + 4, paramSueldoMinimo.sueldoMinimo);
  }

  private static final void jdoSetsueldoMinimo(SueldoMinimo paramSueldoMinimo, double paramDouble)
  {
    if (paramSueldoMinimo.jdoFlags == 0)
    {
      paramSueldoMinimo.sueldoMinimo = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoMinimo.sueldoMinimo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoMinimo, jdoInheritedFieldCount + 4, paramSueldoMinimo.sueldoMinimo, paramDouble);
  }

  private static final double jdoGetsueldoRural(SueldoMinimo paramSueldoMinimo)
  {
    if (paramSueldoMinimo.jdoFlags <= 0)
      return paramSueldoMinimo.sueldoRural;
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoMinimo.sueldoRural;
    if (localStateManager.isLoaded(paramSueldoMinimo, jdoInheritedFieldCount + 5))
      return paramSueldoMinimo.sueldoRural;
    return localStateManager.getDoubleField(paramSueldoMinimo, jdoInheritedFieldCount + 5, paramSueldoMinimo.sueldoRural);
  }

  private static final void jdoSetsueldoRural(SueldoMinimo paramSueldoMinimo, double paramDouble)
  {
    if (paramSueldoMinimo.jdoFlags == 0)
    {
      paramSueldoMinimo.sueldoRural = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoMinimo.sueldoRural = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoMinimo, jdoInheritedFieldCount + 5, paramSueldoMinimo.sueldoRural, paramDouble);
  }

  private static final Date jdoGettiempoSitp(SueldoMinimo paramSueldoMinimo)
  {
    if (paramSueldoMinimo.jdoFlags <= 0)
      return paramSueldoMinimo.tiempoSitp;
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoMinimo.tiempoSitp;
    if (localStateManager.isLoaded(paramSueldoMinimo, jdoInheritedFieldCount + 6))
      return paramSueldoMinimo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramSueldoMinimo, jdoInheritedFieldCount + 6, paramSueldoMinimo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(SueldoMinimo paramSueldoMinimo, Date paramDate)
  {
    if (paramSueldoMinimo.jdoFlags == 0)
    {
      paramSueldoMinimo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoMinimo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSueldoMinimo, jdoInheritedFieldCount + 6, paramSueldoMinimo.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}