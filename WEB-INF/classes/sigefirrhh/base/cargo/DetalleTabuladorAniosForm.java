package sigefirrhh.base.cargo;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class DetalleTabuladorAniosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(DetalleTabuladorAniosForm.class.getName());
  private DetalleTabuladorAnios detalleTabuladorAnios;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoFacade cargoFacade = new CargoFacade();
  private boolean showDetalleTabuladorAniosByAnioDesde;
  private boolean showDetalleTabuladorAniosByAnioHasta;
  private boolean showDetalleTabuladorAniosByPaso;
  private boolean showDetalleTabuladorAniosByHoras;
  private boolean showDetalleTabuladorAniosByTabulador;
  private int findAnioDesde;
  private int findAnioHasta;
  private int findPaso;
  private int findHoras;
  private String findSelectTabulador;
  private Collection findColTabulador;
  private Collection colManualCargoForCargo;
  private Collection colCargo;
  private Collection colTabulador;
  private String selectManualCargoForCargo;
  private String selectCargo;
  private String selectTabulador;
  private Object stateResultDetalleTabuladorAniosByAnioDesde = null;

  private Object stateResultDetalleTabuladorAniosByAnioHasta = null;

  private Object stateResultDetalleTabuladorAniosByPaso = null;

  private Object stateResultDetalleTabuladorAniosByHoras = null;

  private Object stateResultDetalleTabuladorAniosByTabulador = null;

  public int getFindAnioDesde()
  {
    return this.findAnioDesde;
  }
  public void setFindAnioDesde(int findAnioDesde) {
    this.findAnioDesde = findAnioDesde;
  }
  public int getFindAnioHasta() {
    return this.findAnioHasta;
  }
  public void setFindAnioHasta(int findAnioHasta) {
    this.findAnioHasta = findAnioHasta;
  }
  public int getFindPaso() {
    return this.findPaso;
  }
  public void setFindPaso(int findPaso) {
    this.findPaso = findPaso;
  }
  public int getFindHoras() {
    return this.findHoras;
  }
  public void setFindHoras(int findHoras) {
    this.findHoras = findHoras;
  }
  public String getFindSelectTabulador() {
    return this.findSelectTabulador;
  }
  public void setFindSelectTabulador(String valTabulador) {
    this.findSelectTabulador = valTabulador;
  }

  public Collection getFindColTabulador() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTabulador.iterator();
    Tabulador tabulador = null;
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tabulador.getIdTabulador()), 
        tabulador.toString()));
    }
    return col;
  }

  public String getSelectManualCargoForCargo()
  {
    return this.selectManualCargoForCargo;
  }
  public void setSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.selectManualCargoForCargo = valManualCargoForCargo;
  }
  public void changeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;
      if (idManualCargo > 0L) {
        this.colCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
      } else {
        this.selectCargo = null;
        this.detalleTabuladorAnios.setCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCargo = null;
      this.detalleTabuladorAnios.setCargo(
        null);
    }
  }

  public boolean isShowManualCargoForCargo() { return this.colManualCargoForCargo != null; }

  public String getSelectCargo() {
    return this.selectCargo;
  }
  public void setSelectCargo(String valCargo) {
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    this.detalleTabuladorAnios.setCargo(null);
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        valCargo)) {
        this.detalleTabuladorAnios.setCargo(
          cargo);
        break;
      }
    }
    this.selectCargo = valCargo;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public String getSelectTabulador() {
    return this.selectTabulador;
  }
  public void setSelectTabulador(String valTabulador) {
    Iterator iterator = this.colTabulador.iterator();
    Tabulador tabulador = null;
    this.detalleTabuladorAnios.setTabulador(null);
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      if (String.valueOf(tabulador.getIdTabulador()).equals(
        valTabulador)) {
        this.detalleTabuladorAnios.setTabulador(
          tabulador);
        break;
      }
    }
    this.selectTabulador = valTabulador;
  }
  public Collection getResult() {
    return this.result;
  }

  public DetalleTabuladorAnios getDetalleTabuladorAnios() {
    if (this.detalleTabuladorAnios == null) {
      this.detalleTabuladorAnios = new DetalleTabuladorAnios();
    }
    return this.detalleTabuladorAnios;
  }

  public DetalleTabuladorAniosForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColManualCargoForCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getColTabulador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTabulador.iterator();
    Tabulador tabulador = null;
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tabulador.getIdTabulador()), 
        tabulador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTabulador = 
        this.cargoFacade.findTabuladorByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colManualCargoForCargo = 
        this.cargoFacade.findManualCargoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colTabulador = 
        this.cargoFacade.findTabuladorByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findDetalleTabuladorAniosByAnioDesde()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findDetalleTabuladorAniosByAnioDesde(this.findAnioDesde);
      this.showDetalleTabuladorAniosByAnioDesde = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDetalleTabuladorAniosByAnioDesde)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnioDesde = 0;
    this.findAnioHasta = 0;
    this.findPaso = 0;
    this.findHoras = 0;
    this.findSelectTabulador = null;

    return null;
  }

  public String findDetalleTabuladorAniosByAnioHasta()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findDetalleTabuladorAniosByAnioHasta(this.findAnioHasta);
      this.showDetalleTabuladorAniosByAnioHasta = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDetalleTabuladorAniosByAnioHasta)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnioDesde = 0;
    this.findAnioHasta = 0;
    this.findPaso = 0;
    this.findHoras = 0;
    this.findSelectTabulador = null;

    return null;
  }

  public String findDetalleTabuladorAniosByPaso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findDetalleTabuladorAniosByPaso(this.findPaso);
      this.showDetalleTabuladorAniosByPaso = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDetalleTabuladorAniosByPaso)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnioDesde = 0;
    this.findAnioHasta = 0;
    this.findPaso = 0;
    this.findHoras = 0;
    this.findSelectTabulador = null;

    return null;
  }

  public String findDetalleTabuladorAniosByHoras()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findDetalleTabuladorAniosByHoras(this.findHoras);
      this.showDetalleTabuladorAniosByHoras = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDetalleTabuladorAniosByHoras)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnioDesde = 0;
    this.findAnioHasta = 0;
    this.findPaso = 0;
    this.findHoras = 0;
    this.findSelectTabulador = null;

    return null;
  }

  public String findDetalleTabuladorAniosByTabulador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findDetalleTabuladorAniosByTabulador(Long.valueOf(this.findSelectTabulador).longValue());
      this.showDetalleTabuladorAniosByTabulador = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDetalleTabuladorAniosByTabulador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnioDesde = 0;
    this.findAnioHasta = 0;
    this.findPaso = 0;
    this.findHoras = 0;
    this.findSelectTabulador = null;

    return null;
  }

  public boolean isShowDetalleTabuladorAniosByAnioDesde() {
    return this.showDetalleTabuladorAniosByAnioDesde;
  }
  public boolean isShowDetalleTabuladorAniosByAnioHasta() {
    return this.showDetalleTabuladorAniosByAnioHasta;
  }
  public boolean isShowDetalleTabuladorAniosByPaso() {
    return this.showDetalleTabuladorAniosByPaso;
  }
  public boolean isShowDetalleTabuladorAniosByHoras() {
    return this.showDetalleTabuladorAniosByHoras;
  }
  public boolean isShowDetalleTabuladorAniosByTabulador() {
    return this.showDetalleTabuladorAniosByTabulador;
  }

  public String selectDetalleTabuladorAnios()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectCargo = null;
    this.selectManualCargoForCargo = null;

    this.selectTabulador = null;

    long idDetalleTabuladorAnios = 
      Long.parseLong((String)requestParameterMap.get("idDetalleTabuladorAnios"));
    try
    {
      this.detalleTabuladorAnios = 
        this.cargoFacade.findDetalleTabuladorAniosById(
        idDetalleTabuladorAnios);
      if (this.detalleTabuladorAnios.getCargo() != null) {
        this.selectCargo = 
          String.valueOf(this.detalleTabuladorAnios.getCargo().getIdCargo());
      }
      if (this.detalleTabuladorAnios.getTabulador() != null) {
        this.selectTabulador = 
          String.valueOf(this.detalleTabuladorAnios.getTabulador().getIdTabulador());
      }

      Cargo cargo = null;
      ManualCargo manualCargoForCargo = null;

      if (this.detalleTabuladorAnios.getCargo() != null) {
        long idCargo = 
          this.detalleTabuladorAnios.getCargo().getIdCargo();
        this.selectCargo = String.valueOf(idCargo);
        cargo = this.cargoFacade.findCargoById(
          idCargo);
        this.colCargo = this.cargoFacade.findCargoByManualCargo(
          cargo.getManualCargo().getIdManualCargo());

        long idManualCargoForCargo = 
          this.detalleTabuladorAnios.getCargo().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargo = String.valueOf(idManualCargoForCargo);
        manualCargoForCargo = 
          this.cargoFacade.findManualCargoById(
          idManualCargoForCargo);
        this.colManualCargoForCargo = 
          this.cargoFacade.findAllManualCargo();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.detalleTabuladorAnios = null;
    this.showDetalleTabuladorAniosByAnioDesde = false;
    this.showDetalleTabuladorAniosByAnioHasta = false;
    this.showDetalleTabuladorAniosByPaso = false;
    this.showDetalleTabuladorAniosByHoras = false;
    this.showDetalleTabuladorAniosByTabulador = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.detalleTabuladorAnios.getTiempoSitp() != null) && 
      (this.detalleTabuladorAnios.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.cargoFacade.addDetalleTabuladorAnios(
          this.detalleTabuladorAnios);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.cargoFacade.updateDetalleTabuladorAnios(
          this.detalleTabuladorAnios);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.cargoFacade.deleteDetalleTabuladorAnios(
        this.detalleTabuladorAnios);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.detalleTabuladorAnios = new DetalleTabuladorAnios();

    this.selectCargo = null;

    this.selectManualCargoForCargo = null;

    this.selectTabulador = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.detalleTabuladorAnios.setIdDetalleTabuladorAnios(identityGenerator.getNextSequenceNumber("sigefirrhh.base.cargo.DetalleTabuladorAnios"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.detalleTabuladorAnios = new DetalleTabuladorAnios();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}