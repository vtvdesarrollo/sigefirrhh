package sigefirrhh.base.cargo;

import java.io.Serializable;

public class ManualCargoPK
  implements Serializable
{
  public long idManualCargo;

  public ManualCargoPK()
  {
  }

  public ManualCargoPK(long idManualCargo)
  {
    this.idManualCargo = idManualCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ManualCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ManualCargoPK thatPK)
  {
    return 
      this.idManualCargo == thatPK.idManualCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idManualCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idManualCargo);
  }
}