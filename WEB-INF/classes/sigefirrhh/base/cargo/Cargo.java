package sigefirrhh.base.cargo;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Cargo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO_CARGO;
  protected static final Map LISTA_CAUCION;
  private long idCargo;
  private ManualCargo manualCargo;
  private String codCargo;
  private String descripcionCargo;
  private String tipoCargo;
  private int grado;
  private int subGrado;
  private String caucion;
  private SerieCargo serieCargo;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "caucion", "codCargo", "descripcionCargo", "grado", "idCargo", "idSitp", "manualCargo", "serieCargo", "subGrado", "tiempoSitp", "tipoCargo" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.ManualCargo"), sunjdo$classForName$("sigefirrhh.base.cargo.SerieCargo"), Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 26, 26, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Cargo());

    LISTA_TIPO_CARGO = 
      new LinkedHashMap();
    LISTA_CAUCION = 
      new LinkedHashMap();

    LISTA_TIPO_CARGO.put("0", "ALTO NIVEL");
    LISTA_TIPO_CARGO.put("1", "ADMINISTRATIVO");
    LISTA_TIPO_CARGO.put("2", "TECNICO-PROFESIONAL");
    LISTA_TIPO_CARGO.put("3", "NO CLASIFICADO");
    LISTA_TIPO_CARGO.put("4", "OBRERO");
    LISTA_TIPO_CARGO.put("5", "DOCENTE");
    LISTA_CAUCION.put("0", "NO APLICA");
    LISTA_CAUCION.put("1", "CAUCION");
    LISTA_CAUCION.put("2", "DECLARACION JURADA DE BIENES");
    LISTA_CAUCION.put("3", "DECLARACION JURADA Y CAUCION");
  }

  public Cargo()
  {
    jdoSettipoCargo(this, "1");

    jdoSetgrado(this, 1);

    jdoSetsubGrado(this, 0);

    jdoSetcaucion(this, "0");
  }

  public String toString()
  {
    return jdoGetdescripcionCargo(this) + "  -  " + 
      jdoGetcodCargo(this) + " - " + jdoGetgrado(this);
  }

  public String getCaucion()
  {
    return jdoGetcaucion(this);
  }

  public String getCodCargo()
  {
    return jdoGetcodCargo(this);
  }

  public String getDescripcionCargo()
  {
    return jdoGetdescripcionCargo(this);
  }

  public int getGrado()
  {
    return jdoGetgrado(this);
  }

  public long getIdCargo()
  {
    return jdoGetidCargo(this);
  }

  public ManualCargo getManualCargo()
  {
    return jdoGetmanualCargo(this);
  }

  public SerieCargo getSerieCargo()
  {
    return jdoGetserieCargo(this);
  }

  public int getSubGrado()
  {
    return jdoGetsubGrado(this);
  }

  public String getTipoCargo()
  {
    return jdoGettipoCargo(this);
  }

  public void setCaucion(String string)
  {
    jdoSetcaucion(this, string);
  }

  public void setCodCargo(String string)
  {
    jdoSetcodCargo(this, string);
  }

  public void setDescripcionCargo(String string)
  {
    jdoSetdescripcionCargo(this, string);
  }

  public void setGrado(int i)
  {
    jdoSetgrado(this, i);
  }

  public void setIdCargo(long l)
  {
    jdoSetidCargo(this, l);
  }

  public void setManualCargo(ManualCargo cargo)
  {
    jdoSetmanualCargo(this, cargo);
  }

  public void setSerieCargo(SerieCargo cargo)
  {
    jdoSetserieCargo(this, cargo);
  }

  public void setSubGrado(int i)
  {
    jdoSetsubGrado(this, i);
  }

  public void setTipoCargo(String string)
  {
    jdoSettipoCargo(this, string);
  }

  public int getIdSitp() {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i) {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date) {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Cargo localCargo = new Cargo();
    localCargo.jdoFlags = 1;
    localCargo.jdoStateManager = paramStateManager;
    return localCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Cargo localCargo = new Cargo();
    localCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCargo.jdoFlags = 1;
    localCargo.jdoStateManager = paramStateManager;
    return localCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.caucion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcionCargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.grado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCargo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.manualCargo);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.serieCargo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.subGrado);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoCargo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.caucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcionCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.manualCargo = ((ManualCargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.serieCargo = ((SerieCargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.subGrado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Cargo paramCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCargo == null)
        throw new IllegalArgumentException("arg1");
      this.caucion = paramCargo.caucion;
      return;
    case 1:
      if (paramCargo == null)
        throw new IllegalArgumentException("arg1");
      this.codCargo = paramCargo.codCargo;
      return;
    case 2:
      if (paramCargo == null)
        throw new IllegalArgumentException("arg1");
      this.descripcionCargo = paramCargo.descripcionCargo;
      return;
    case 3:
      if (paramCargo == null)
        throw new IllegalArgumentException("arg1");
      this.grado = paramCargo.grado;
      return;
    case 4:
      if (paramCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idCargo = paramCargo.idCargo;
      return;
    case 5:
      if (paramCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramCargo.idSitp;
      return;
    case 6:
      if (paramCargo == null)
        throw new IllegalArgumentException("arg1");
      this.manualCargo = paramCargo.manualCargo;
      return;
    case 7:
      if (paramCargo == null)
        throw new IllegalArgumentException("arg1");
      this.serieCargo = paramCargo.serieCargo;
      return;
    case 8:
      if (paramCargo == null)
        throw new IllegalArgumentException("arg1");
      this.subGrado = paramCargo.subGrado;
      return;
    case 9:
      if (paramCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramCargo.tiempoSitp;
      return;
    case 10:
      if (paramCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCargo = paramCargo.tipoCargo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Cargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Cargo localCargo = (Cargo)paramObject;
    if (localCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CargoPK))
      throw new IllegalArgumentException("arg1");
    CargoPK localCargoPK = (CargoPK)paramObject;
    localCargoPK.idCargo = this.idCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CargoPK))
      throw new IllegalArgumentException("arg1");
    CargoPK localCargoPK = (CargoPK)paramObject;
    this.idCargo = localCargoPK.idCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CargoPK))
      throw new IllegalArgumentException("arg2");
    CargoPK localCargoPK = (CargoPK)paramObject;
    localCargoPK.idCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CargoPK))
      throw new IllegalArgumentException("arg2");
    CargoPK localCargoPK = (CargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localCargoPK.idCargo);
  }

  private static final String jdoGetcaucion(Cargo paramCargo)
  {
    if (paramCargo.jdoFlags <= 0)
      return paramCargo.caucion;
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
      return paramCargo.caucion;
    if (localStateManager.isLoaded(paramCargo, jdoInheritedFieldCount + 0))
      return paramCargo.caucion;
    return localStateManager.getStringField(paramCargo, jdoInheritedFieldCount + 0, paramCargo.caucion);
  }

  private static final void jdoSetcaucion(Cargo paramCargo, String paramString)
  {
    if (paramCargo.jdoFlags == 0)
    {
      paramCargo.caucion = paramString;
      return;
    }
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargo.caucion = paramString;
      return;
    }
    localStateManager.setStringField(paramCargo, jdoInheritedFieldCount + 0, paramCargo.caucion, paramString);
  }

  private static final String jdoGetcodCargo(Cargo paramCargo)
  {
    if (paramCargo.jdoFlags <= 0)
      return paramCargo.codCargo;
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
      return paramCargo.codCargo;
    if (localStateManager.isLoaded(paramCargo, jdoInheritedFieldCount + 1))
      return paramCargo.codCargo;
    return localStateManager.getStringField(paramCargo, jdoInheritedFieldCount + 1, paramCargo.codCargo);
  }

  private static final void jdoSetcodCargo(Cargo paramCargo, String paramString)
  {
    if (paramCargo.jdoFlags == 0)
    {
      paramCargo.codCargo = paramString;
      return;
    }
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargo.codCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramCargo, jdoInheritedFieldCount + 1, paramCargo.codCargo, paramString);
  }

  private static final String jdoGetdescripcionCargo(Cargo paramCargo)
  {
    if (paramCargo.jdoFlags <= 0)
      return paramCargo.descripcionCargo;
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
      return paramCargo.descripcionCargo;
    if (localStateManager.isLoaded(paramCargo, jdoInheritedFieldCount + 2))
      return paramCargo.descripcionCargo;
    return localStateManager.getStringField(paramCargo, jdoInheritedFieldCount + 2, paramCargo.descripcionCargo);
  }

  private static final void jdoSetdescripcionCargo(Cargo paramCargo, String paramString)
  {
    if (paramCargo.jdoFlags == 0)
    {
      paramCargo.descripcionCargo = paramString;
      return;
    }
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargo.descripcionCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramCargo, jdoInheritedFieldCount + 2, paramCargo.descripcionCargo, paramString);
  }

  private static final int jdoGetgrado(Cargo paramCargo)
  {
    if (paramCargo.jdoFlags <= 0)
      return paramCargo.grado;
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
      return paramCargo.grado;
    if (localStateManager.isLoaded(paramCargo, jdoInheritedFieldCount + 3))
      return paramCargo.grado;
    return localStateManager.getIntField(paramCargo, jdoInheritedFieldCount + 3, paramCargo.grado);
  }

  private static final void jdoSetgrado(Cargo paramCargo, int paramInt)
  {
    if (paramCargo.jdoFlags == 0)
    {
      paramCargo.grado = paramInt;
      return;
    }
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargo.grado = paramInt;
      return;
    }
    localStateManager.setIntField(paramCargo, jdoInheritedFieldCount + 3, paramCargo.grado, paramInt);
  }

  private static final long jdoGetidCargo(Cargo paramCargo)
  {
    return paramCargo.idCargo;
  }

  private static final void jdoSetidCargo(Cargo paramCargo, long paramLong)
  {
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargo.idCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramCargo, jdoInheritedFieldCount + 4, paramCargo.idCargo, paramLong);
  }

  private static final int jdoGetidSitp(Cargo paramCargo)
  {
    if (paramCargo.jdoFlags <= 0)
      return paramCargo.idSitp;
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
      return paramCargo.idSitp;
    if (localStateManager.isLoaded(paramCargo, jdoInheritedFieldCount + 5))
      return paramCargo.idSitp;
    return localStateManager.getIntField(paramCargo, jdoInheritedFieldCount + 5, paramCargo.idSitp);
  }

  private static final void jdoSetidSitp(Cargo paramCargo, int paramInt)
  {
    if (paramCargo.jdoFlags == 0)
    {
      paramCargo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramCargo, jdoInheritedFieldCount + 5, paramCargo.idSitp, paramInt);
  }

  private static final ManualCargo jdoGetmanualCargo(Cargo paramCargo)
  {
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
      return paramCargo.manualCargo;
    if (localStateManager.isLoaded(paramCargo, jdoInheritedFieldCount + 6))
      return paramCargo.manualCargo;
    return (ManualCargo)localStateManager.getObjectField(paramCargo, jdoInheritedFieldCount + 6, paramCargo.manualCargo);
  }

  private static final void jdoSetmanualCargo(Cargo paramCargo, ManualCargo paramManualCargo)
  {
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargo.manualCargo = paramManualCargo;
      return;
    }
    localStateManager.setObjectField(paramCargo, jdoInheritedFieldCount + 6, paramCargo.manualCargo, paramManualCargo);
  }

  private static final SerieCargo jdoGetserieCargo(Cargo paramCargo)
  {
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
      return paramCargo.serieCargo;
    if (localStateManager.isLoaded(paramCargo, jdoInheritedFieldCount + 7))
      return paramCargo.serieCargo;
    return (SerieCargo)localStateManager.getObjectField(paramCargo, jdoInheritedFieldCount + 7, paramCargo.serieCargo);
  }

  private static final void jdoSetserieCargo(Cargo paramCargo, SerieCargo paramSerieCargo)
  {
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargo.serieCargo = paramSerieCargo;
      return;
    }
    localStateManager.setObjectField(paramCargo, jdoInheritedFieldCount + 7, paramCargo.serieCargo, paramSerieCargo);
  }

  private static final int jdoGetsubGrado(Cargo paramCargo)
  {
    if (paramCargo.jdoFlags <= 0)
      return paramCargo.subGrado;
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
      return paramCargo.subGrado;
    if (localStateManager.isLoaded(paramCargo, jdoInheritedFieldCount + 8))
      return paramCargo.subGrado;
    return localStateManager.getIntField(paramCargo, jdoInheritedFieldCount + 8, paramCargo.subGrado);
  }

  private static final void jdoSetsubGrado(Cargo paramCargo, int paramInt)
  {
    if (paramCargo.jdoFlags == 0)
    {
      paramCargo.subGrado = paramInt;
      return;
    }
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargo.subGrado = paramInt;
      return;
    }
    localStateManager.setIntField(paramCargo, jdoInheritedFieldCount + 8, paramCargo.subGrado, paramInt);
  }

  private static final Date jdoGettiempoSitp(Cargo paramCargo)
  {
    if (paramCargo.jdoFlags <= 0)
      return paramCargo.tiempoSitp;
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
      return paramCargo.tiempoSitp;
    if (localStateManager.isLoaded(paramCargo, jdoInheritedFieldCount + 9))
      return paramCargo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramCargo, jdoInheritedFieldCount + 9, paramCargo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Cargo paramCargo, Date paramDate)
  {
    if (paramCargo.jdoFlags == 0)
    {
      paramCargo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCargo, jdoInheritedFieldCount + 9, paramCargo.tiempoSitp, paramDate);
  }

  private static final String jdoGettipoCargo(Cargo paramCargo)
  {
    if (paramCargo.jdoFlags <= 0)
      return paramCargo.tipoCargo;
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
      return paramCargo.tipoCargo;
    if (localStateManager.isLoaded(paramCargo, jdoInheritedFieldCount + 10))
      return paramCargo.tipoCargo;
    return localStateManager.getStringField(paramCargo, jdoInheritedFieldCount + 10, paramCargo.tipoCargo);
  }

  private static final void jdoSettipoCargo(Cargo paramCargo, String paramString)
  {
    if (paramCargo.jdoFlags == 0)
    {
      paramCargo.tipoCargo = paramString;
      return;
    }
    StateManager localStateManager = paramCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargo.tipoCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramCargo, jdoInheritedFieldCount + 10, paramCargo.tipoCargo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}