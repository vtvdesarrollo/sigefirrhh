package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class RamoOcupacionalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRamoOcupacional(RamoOcupacional ramoOcupacional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RamoOcupacional ramoOcupacionalNew = 
      (RamoOcupacional)BeanUtils.cloneBean(
      ramoOcupacional);

    pm.makePersistent(ramoOcupacionalNew);
  }

  public void updateRamoOcupacional(RamoOcupacional ramoOcupacional) throws Exception
  {
    RamoOcupacional ramoOcupacionalModify = 
      findRamoOcupacionalById(ramoOcupacional.getIdRamoOcupacional());

    BeanUtils.copyProperties(ramoOcupacionalModify, ramoOcupacional);
  }

  public void deleteRamoOcupacional(RamoOcupacional ramoOcupacional) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RamoOcupacional ramoOcupacionalDelete = 
      findRamoOcupacionalById(ramoOcupacional.getIdRamoOcupacional());
    pm.deletePersistent(ramoOcupacionalDelete);
  }

  public RamoOcupacional findRamoOcupacionalById(long idRamoOcupacional) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRamoOcupacional == pIdRamoOcupacional";
    Query query = pm.newQuery(RamoOcupacional.class, filter);

    query.declareParameters("long pIdRamoOcupacional");

    parameters.put("pIdRamoOcupacional", new Long(idRamoOcupacional));

    Collection colRamoOcupacional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRamoOcupacional.iterator();
    return (RamoOcupacional)iterator.next();
  }

  public Collection findRamoOcupacionalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent ramoOcupacionalExtent = pm.getExtent(
      RamoOcupacional.class, true);
    Query query = pm.newQuery(ramoOcupacionalExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodRamoOcupacional(String codRamoOcupacional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codRamoOcupacional == pCodRamoOcupacional";

    Query query = pm.newQuery(RamoOcupacional.class, filter);

    query.declareParameters("java.lang.String pCodRamoOcupacional");
    HashMap parameters = new HashMap();

    parameters.put("pCodRamoOcupacional", new String(codRamoOcupacional));

    query.setOrdering("nombre ascending");

    Collection colRamoOcupacional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRamoOcupacional);

    return colRamoOcupacional;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(RamoOcupacional.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colRamoOcupacional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRamoOcupacional);

    return colRamoOcupacional;
  }
}