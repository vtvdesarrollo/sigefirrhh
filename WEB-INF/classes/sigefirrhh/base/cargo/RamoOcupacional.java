package sigefirrhh.base.cargo;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class RamoOcupacional
  implements Serializable, PersistenceCapable
{
  private long idRamoOcupacional;
  private String codRamoOcupacional;
  private String nombre;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codRamoOcupacional", "idRamoOcupacional", "idSitp", "nombre", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodRamoOcupacional(this);
  }

  public String getCodRamoOcupacional()
  {
    return jdoGetcodRamoOcupacional(this);
  }

  public long getIdRamoOcupacional() {
    return jdoGetidRamoOcupacional(this);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setCodRamoOcupacional(String string) {
    jdoSetcodRamoOcupacional(this, string);
  }

  public void setIdRamoOcupacional(long l) {
    jdoSetidRamoOcupacional(this, l);
  }

  public void setNombre(String string) {
    jdoSetnombre(this, string);
  }

  public int getIdSitp() {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i) {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date) {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.cargo.RamoOcupacional"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RamoOcupacional());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RamoOcupacional localRamoOcupacional = new RamoOcupacional();
    localRamoOcupacional.jdoFlags = 1;
    localRamoOcupacional.jdoStateManager = paramStateManager;
    return localRamoOcupacional;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RamoOcupacional localRamoOcupacional = new RamoOcupacional();
    localRamoOcupacional.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRamoOcupacional.jdoFlags = 1;
    localRamoOcupacional.jdoStateManager = paramStateManager;
    return localRamoOcupacional;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codRamoOcupacional);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRamoOcupacional);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRamoOcupacional = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRamoOcupacional = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RamoOcupacional paramRamoOcupacional, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRamoOcupacional == null)
        throw new IllegalArgumentException("arg1");
      this.codRamoOcupacional = paramRamoOcupacional.codRamoOcupacional;
      return;
    case 1:
      if (paramRamoOcupacional == null)
        throw new IllegalArgumentException("arg1");
      this.idRamoOcupacional = paramRamoOcupacional.idRamoOcupacional;
      return;
    case 2:
      if (paramRamoOcupacional == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramRamoOcupacional.idSitp;
      return;
    case 3:
      if (paramRamoOcupacional == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramRamoOcupacional.nombre;
      return;
    case 4:
      if (paramRamoOcupacional == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramRamoOcupacional.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RamoOcupacional))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RamoOcupacional localRamoOcupacional = (RamoOcupacional)paramObject;
    if (localRamoOcupacional.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRamoOcupacional, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RamoOcupacionalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RamoOcupacionalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RamoOcupacionalPK))
      throw new IllegalArgumentException("arg1");
    RamoOcupacionalPK localRamoOcupacionalPK = (RamoOcupacionalPK)paramObject;
    localRamoOcupacionalPK.idRamoOcupacional = this.idRamoOcupacional;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RamoOcupacionalPK))
      throw new IllegalArgumentException("arg1");
    RamoOcupacionalPK localRamoOcupacionalPK = (RamoOcupacionalPK)paramObject;
    this.idRamoOcupacional = localRamoOcupacionalPK.idRamoOcupacional;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RamoOcupacionalPK))
      throw new IllegalArgumentException("arg2");
    RamoOcupacionalPK localRamoOcupacionalPK = (RamoOcupacionalPK)paramObject;
    localRamoOcupacionalPK.idRamoOcupacional = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RamoOcupacionalPK))
      throw new IllegalArgumentException("arg2");
    RamoOcupacionalPK localRamoOcupacionalPK = (RamoOcupacionalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localRamoOcupacionalPK.idRamoOcupacional);
  }

  private static final String jdoGetcodRamoOcupacional(RamoOcupacional paramRamoOcupacional)
  {
    if (paramRamoOcupacional.jdoFlags <= 0)
      return paramRamoOcupacional.codRamoOcupacional;
    StateManager localStateManager = paramRamoOcupacional.jdoStateManager;
    if (localStateManager == null)
      return paramRamoOcupacional.codRamoOcupacional;
    if (localStateManager.isLoaded(paramRamoOcupacional, jdoInheritedFieldCount + 0))
      return paramRamoOcupacional.codRamoOcupacional;
    return localStateManager.getStringField(paramRamoOcupacional, jdoInheritedFieldCount + 0, paramRamoOcupacional.codRamoOcupacional);
  }

  private static final void jdoSetcodRamoOcupacional(RamoOcupacional paramRamoOcupacional, String paramString)
  {
    if (paramRamoOcupacional.jdoFlags == 0)
    {
      paramRamoOcupacional.codRamoOcupacional = paramString;
      return;
    }
    StateManager localStateManager = paramRamoOcupacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramRamoOcupacional.codRamoOcupacional = paramString;
      return;
    }
    localStateManager.setStringField(paramRamoOcupacional, jdoInheritedFieldCount + 0, paramRamoOcupacional.codRamoOcupacional, paramString);
  }

  private static final long jdoGetidRamoOcupacional(RamoOcupacional paramRamoOcupacional)
  {
    return paramRamoOcupacional.idRamoOcupacional;
  }

  private static final void jdoSetidRamoOcupacional(RamoOcupacional paramRamoOcupacional, long paramLong)
  {
    StateManager localStateManager = paramRamoOcupacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramRamoOcupacional.idRamoOcupacional = paramLong;
      return;
    }
    localStateManager.setLongField(paramRamoOcupacional, jdoInheritedFieldCount + 1, paramRamoOcupacional.idRamoOcupacional, paramLong);
  }

  private static final int jdoGetidSitp(RamoOcupacional paramRamoOcupacional)
  {
    if (paramRamoOcupacional.jdoFlags <= 0)
      return paramRamoOcupacional.idSitp;
    StateManager localStateManager = paramRamoOcupacional.jdoStateManager;
    if (localStateManager == null)
      return paramRamoOcupacional.idSitp;
    if (localStateManager.isLoaded(paramRamoOcupacional, jdoInheritedFieldCount + 2))
      return paramRamoOcupacional.idSitp;
    return localStateManager.getIntField(paramRamoOcupacional, jdoInheritedFieldCount + 2, paramRamoOcupacional.idSitp);
  }

  private static final void jdoSetidSitp(RamoOcupacional paramRamoOcupacional, int paramInt)
  {
    if (paramRamoOcupacional.jdoFlags == 0)
    {
      paramRamoOcupacional.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramRamoOcupacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramRamoOcupacional.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramRamoOcupacional, jdoInheritedFieldCount + 2, paramRamoOcupacional.idSitp, paramInt);
  }

  private static final String jdoGetnombre(RamoOcupacional paramRamoOcupacional)
  {
    if (paramRamoOcupacional.jdoFlags <= 0)
      return paramRamoOcupacional.nombre;
    StateManager localStateManager = paramRamoOcupacional.jdoStateManager;
    if (localStateManager == null)
      return paramRamoOcupacional.nombre;
    if (localStateManager.isLoaded(paramRamoOcupacional, jdoInheritedFieldCount + 3))
      return paramRamoOcupacional.nombre;
    return localStateManager.getStringField(paramRamoOcupacional, jdoInheritedFieldCount + 3, paramRamoOcupacional.nombre);
  }

  private static final void jdoSetnombre(RamoOcupacional paramRamoOcupacional, String paramString)
  {
    if (paramRamoOcupacional.jdoFlags == 0)
    {
      paramRamoOcupacional.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramRamoOcupacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramRamoOcupacional.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramRamoOcupacional, jdoInheritedFieldCount + 3, paramRamoOcupacional.nombre, paramString);
  }

  private static final Date jdoGettiempoSitp(RamoOcupacional paramRamoOcupacional)
  {
    if (paramRamoOcupacional.jdoFlags <= 0)
      return paramRamoOcupacional.tiempoSitp;
    StateManager localStateManager = paramRamoOcupacional.jdoStateManager;
    if (localStateManager == null)
      return paramRamoOcupacional.tiempoSitp;
    if (localStateManager.isLoaded(paramRamoOcupacional, jdoInheritedFieldCount + 4))
      return paramRamoOcupacional.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramRamoOcupacional, jdoInheritedFieldCount + 4, paramRamoOcupacional.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(RamoOcupacional paramRamoOcupacional, Date paramDate)
  {
    if (paramRamoOcupacional.jdoFlags == 0)
    {
      paramRamoOcupacional.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramRamoOcupacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramRamoOcupacional.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRamoOcupacional, jdoInheritedFieldCount + 4, paramRamoOcupacional.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}