package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class DetalleTabuladorAniosBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDetalleTabuladorAnios(DetalleTabuladorAnios detalleTabuladorAnios)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DetalleTabuladorAnios detalleTabuladorAniosNew = 
      (DetalleTabuladorAnios)BeanUtils.cloneBean(
      detalleTabuladorAnios);

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (detalleTabuladorAniosNew.getCargo() != null) {
      detalleTabuladorAniosNew.setCargo(
        cargoBeanBusiness.findCargoById(
        detalleTabuladorAniosNew.getCargo().getIdCargo()));
    }

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (detalleTabuladorAniosNew.getTabulador() != null) {
      detalleTabuladorAniosNew.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        detalleTabuladorAniosNew.getTabulador().getIdTabulador()));
    }
    pm.makePersistent(detalleTabuladorAniosNew);
  }

  public void updateDetalleTabuladorAnios(DetalleTabuladorAnios detalleTabuladorAnios) throws Exception
  {
    DetalleTabuladorAnios detalleTabuladorAniosModify = 
      findDetalleTabuladorAniosById(detalleTabuladorAnios.getIdDetalleTabuladorAnios());

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (detalleTabuladorAnios.getCargo() != null) {
      detalleTabuladorAnios.setCargo(
        cargoBeanBusiness.findCargoById(
        detalleTabuladorAnios.getCargo().getIdCargo()));
    }

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (detalleTabuladorAnios.getTabulador() != null) {
      detalleTabuladorAnios.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        detalleTabuladorAnios.getTabulador().getIdTabulador()));
    }

    BeanUtils.copyProperties(detalleTabuladorAniosModify, detalleTabuladorAnios);
  }

  public void deleteDetalleTabuladorAnios(DetalleTabuladorAnios detalleTabuladorAnios) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DetalleTabuladorAnios detalleTabuladorAniosDelete = 
      findDetalleTabuladorAniosById(detalleTabuladorAnios.getIdDetalleTabuladorAnios());
    pm.deletePersistent(detalleTabuladorAniosDelete);
  }

  public DetalleTabuladorAnios findDetalleTabuladorAniosById(long idDetalleTabuladorAnios) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDetalleTabuladorAnios == pIdDetalleTabuladorAnios";
    Query query = pm.newQuery(DetalleTabuladorAnios.class, filter);

    query.declareParameters("long pIdDetalleTabuladorAnios");

    parameters.put("pIdDetalleTabuladorAnios", new Long(idDetalleTabuladorAnios));

    Collection colDetalleTabuladorAnios = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDetalleTabuladorAnios.iterator();
    return (DetalleTabuladorAnios)iterator.next();
  }

  public Collection findDetalleTabuladorAniosAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent detalleTabuladorAniosExtent = pm.getExtent(
      DetalleTabuladorAnios.class, true);
    Query query = pm.newQuery(detalleTabuladorAniosExtent);
    query.setOrdering("cargo.codClase ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnioDesde(int anioDesde)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anioDesde == pAnioDesde";

    Query query = pm.newQuery(DetalleTabuladorAnios.class, filter);

    query.declareParameters("int pAnioDesde");
    HashMap parameters = new HashMap();

    parameters.put("pAnioDesde", new Integer(anioDesde));

    query.setOrdering("cargo.codClase ascending");

    Collection colDetalleTabuladorAnios = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDetalleTabuladorAnios);

    return colDetalleTabuladorAnios;
  }

  public Collection findByAnioHasta(int anioHasta)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anioHasta == pAnioHasta";

    Query query = pm.newQuery(DetalleTabuladorAnios.class, filter);

    query.declareParameters("int pAnioHasta");
    HashMap parameters = new HashMap();

    parameters.put("pAnioHasta", new Integer(anioHasta));

    query.setOrdering("cargo.codClase ascending");

    Collection colDetalleTabuladorAnios = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDetalleTabuladorAnios);

    return colDetalleTabuladorAnios;
  }

  public Collection findByPaso(int paso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "paso == pPaso";

    Query query = pm.newQuery(DetalleTabuladorAnios.class, filter);

    query.declareParameters("int pPaso");
    HashMap parameters = new HashMap();

    parameters.put("pPaso", new Integer(paso));

    query.setOrdering("cargo.codClase ascending");

    Collection colDetalleTabuladorAnios = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDetalleTabuladorAnios);

    return colDetalleTabuladorAnios;
  }

  public Collection findByHoras(int horas)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "horas == pHoras";

    Query query = pm.newQuery(DetalleTabuladorAnios.class, filter);

    query.declareParameters("int pHoras");
    HashMap parameters = new HashMap();

    parameters.put("pHoras", new Integer(horas));

    query.setOrdering("cargo.codClase ascending");

    Collection colDetalleTabuladorAnios = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDetalleTabuladorAnios);

    return colDetalleTabuladorAnios;
  }

  public Collection findByTabulador(long idTabulador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tabulador.idTabulador == pIdTabulador";

    Query query = pm.newQuery(DetalleTabuladorAnios.class, filter);

    query.declareParameters("long pIdTabulador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTabulador", new Long(idTabulador));

    query.setOrdering("cargo.codClase ascending");

    Collection colDetalleTabuladorAnios = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDetalleTabuladorAnios);

    return colDetalleTabuladorAnios;
  }
}