package sigefirrhh.base.cargo;

import java.io.Serializable;

public class SerieCargoPK
  implements Serializable
{
  public long idSerieCargo;

  public SerieCargoPK()
  {
  }

  public SerieCargoPK(long idSerieCargo)
  {
    this.idSerieCargo = idSerieCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SerieCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SerieCargoPK thatPK)
  {
    return 
      this.idSerieCargo == thatPK.idSerieCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSerieCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSerieCargo);
  }
}