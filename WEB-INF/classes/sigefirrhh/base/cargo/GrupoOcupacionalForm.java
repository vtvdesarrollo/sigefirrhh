package sigefirrhh.base.cargo;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class GrupoOcupacionalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GrupoOcupacionalForm.class.getName());
  private GrupoOcupacional grupoOcupacional;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoFacade cargoFacade = new CargoFacade();
  private boolean showGrupoOcupacionalByCodGrupoOcupacional;
  private boolean showGrupoOcupacionalByNombre;
  private boolean showGrupoOcupacionalByRamoOcupacional;
  private String findCodGrupoOcupacional;
  private String findNombre;
  private String findSelectRamoOcupacional;
  private Collection findColRamoOcupacional;
  private Collection colRamoOcupacional;
  private String selectRamoOcupacional;
  private Object stateResultGrupoOcupacionalByCodGrupoOcupacional = null;

  private Object stateResultGrupoOcupacionalByNombre = null;

  private Object stateResultGrupoOcupacionalByRamoOcupacional = null;

  public String getFindCodGrupoOcupacional()
  {
    return this.findCodGrupoOcupacional;
  }
  public void setFindCodGrupoOcupacional(String findCodGrupoOcupacional) {
    this.findCodGrupoOcupacional = findCodGrupoOcupacional;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }
  public String getFindSelectRamoOcupacional() {
    return this.findSelectRamoOcupacional;
  }
  public void setFindSelectRamoOcupacional(String valRamoOcupacional) {
    this.findSelectRamoOcupacional = valRamoOcupacional;
  }

  public Collection getFindColRamoOcupacional() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRamoOcupacional.iterator();
    RamoOcupacional ramoOcupacional = null;
    while (iterator.hasNext()) {
      ramoOcupacional = (RamoOcupacional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ramoOcupacional.getIdRamoOcupacional()), 
        ramoOcupacional.toString()));
    }
    return col;
  }

  public String getSelectRamoOcupacional()
  {
    return this.selectRamoOcupacional;
  }
  public void setSelectRamoOcupacional(String valRamoOcupacional) {
    Iterator iterator = this.colRamoOcupacional.iterator();
    RamoOcupacional ramoOcupacional = null;
    this.grupoOcupacional.setRamoOcupacional(null);
    while (iterator.hasNext()) {
      ramoOcupacional = (RamoOcupacional)iterator.next();
      if (String.valueOf(ramoOcupacional.getIdRamoOcupacional()).equals(
        valRamoOcupacional)) {
        this.grupoOcupacional.setRamoOcupacional(
          ramoOcupacional);
        break;
      }
    }
    this.selectRamoOcupacional = valRamoOcupacional;
  }
  public Collection getResult() {
    return this.result;
  }

  public GrupoOcupacional getGrupoOcupacional() {
    if (this.grupoOcupacional == null) {
      this.grupoOcupacional = new GrupoOcupacional();
    }
    return this.grupoOcupacional;
  }

  public GrupoOcupacionalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRamoOcupacional()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRamoOcupacional.iterator();
    RamoOcupacional ramoOcupacional = null;
    while (iterator.hasNext()) {
      ramoOcupacional = (RamoOcupacional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ramoOcupacional.getIdRamoOcupacional()), 
        ramoOcupacional.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColRamoOcupacional = 
        this.cargoFacade.findAllRamoOcupacional();

      this.colRamoOcupacional = 
        this.cargoFacade.findAllRamoOcupacional();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findGrupoOcupacionalByCodGrupoOcupacional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findGrupoOcupacionalByCodGrupoOcupacional(this.findCodGrupoOcupacional);
      this.showGrupoOcupacionalByCodGrupoOcupacional = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGrupoOcupacionalByCodGrupoOcupacional)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodGrupoOcupacional = null;
    this.findNombre = null;
    this.findSelectRamoOcupacional = null;

    return null;
  }

  public String findGrupoOcupacionalByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findGrupoOcupacionalByNombre(this.findNombre);
      this.showGrupoOcupacionalByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGrupoOcupacionalByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodGrupoOcupacional = null;
    this.findNombre = null;
    this.findSelectRamoOcupacional = null;

    return null;
  }

  public String findGrupoOcupacionalByRamoOcupacional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findGrupoOcupacionalByRamoOcupacional(Long.valueOf(this.findSelectRamoOcupacional).longValue());
      this.showGrupoOcupacionalByRamoOcupacional = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGrupoOcupacionalByRamoOcupacional)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodGrupoOcupacional = null;
    this.findNombre = null;
    this.findSelectRamoOcupacional = null;

    return null;
  }

  public boolean isShowGrupoOcupacionalByCodGrupoOcupacional() {
    return this.showGrupoOcupacionalByCodGrupoOcupacional;
  }
  public boolean isShowGrupoOcupacionalByNombre() {
    return this.showGrupoOcupacionalByNombre;
  }
  public boolean isShowGrupoOcupacionalByRamoOcupacional() {
    return this.showGrupoOcupacionalByRamoOcupacional;
  }

  public String selectGrupoOcupacional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRamoOcupacional = null;

    long idGrupoOcupacional = 
      Long.parseLong((String)requestParameterMap.get("idGrupoOcupacional"));
    try
    {
      this.grupoOcupacional = 
        this.cargoFacade.findGrupoOcupacionalById(
        idGrupoOcupacional);
      if (this.grupoOcupacional.getRamoOcupacional() != null) {
        this.selectRamoOcupacional = 
          String.valueOf(this.grupoOcupacional.getRamoOcupacional().getIdRamoOcupacional());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.grupoOcupacional = null;
    this.showGrupoOcupacionalByCodGrupoOcupacional = false;
    this.showGrupoOcupacionalByNombre = false;
    this.showGrupoOcupacionalByRamoOcupacional = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.grupoOcupacional.getTiempoSitp() != null) && 
      (this.grupoOcupacional.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.cargoFacade.addGrupoOcupacional(
          this.grupoOcupacional);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.cargoFacade.updateGrupoOcupacional(
          this.grupoOcupacional);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.cargoFacade.deleteGrupoOcupacional(
        this.grupoOcupacional);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.grupoOcupacional = new GrupoOcupacional();

    this.selectRamoOcupacional = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.grupoOcupacional.setIdGrupoOcupacional(identityGenerator.getNextSequenceNumber("sigefirrhh.base.cargo.GrupoOcupacional"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.grupoOcupacional = new GrupoOcupacional();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}