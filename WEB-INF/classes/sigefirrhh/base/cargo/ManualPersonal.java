package sigefirrhh.base.cargo;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class ManualPersonal
  implements Serializable, PersistenceCapable
{
  private long idManualPersonal;
  private ManualCargo manualCargo;
  private TipoPersonal tipoPersonal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idManualPersonal", "idSitp", "manualCargo", "tiempoSitp", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.ManualCargo"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") };
  private static final byte[] jdoFieldFlags = { 24, 21, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGettipoPersonal(this).getNombre() + "  -  " + 
      jdoGetmanualCargo(this).getNombre();
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public long getIdManualPersonal() {
    return jdoGetidManualPersonal(this);
  }

  public ManualCargo getManualCargo() {
    return jdoGetmanualCargo(this);
  }

  public void setTipoPersonal(TipoPersonal personal) {
    jdoSettipoPersonal(this, personal);
  }

  public void setIdManualPersonal(long l) {
    jdoSetidManualPersonal(this, l);
  }

  public void setManualCargo(ManualCargo cargo) {
    jdoSetmanualCargo(this, cargo);
  }

  public int getIdSitp() {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i) {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date) {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.cargo.ManualPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ManualPersonal());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ManualPersonal localManualPersonal = new ManualPersonal();
    localManualPersonal.jdoFlags = 1;
    localManualPersonal.jdoStateManager = paramStateManager;
    return localManualPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ManualPersonal localManualPersonal = new ManualPersonal();
    localManualPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localManualPersonal.jdoFlags = 1;
    localManualPersonal.jdoStateManager = paramStateManager;
    return localManualPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idManualPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.manualCargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idManualPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.manualCargo = ((ManualCargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ManualPersonal paramManualPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramManualPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idManualPersonal = paramManualPersonal.idManualPersonal;
      return;
    case 1:
      if (paramManualPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramManualPersonal.idSitp;
      return;
    case 2:
      if (paramManualPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.manualCargo = paramManualPersonal.manualCargo;
      return;
    case 3:
      if (paramManualPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramManualPersonal.tiempoSitp;
      return;
    case 4:
      if (paramManualPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramManualPersonal.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ManualPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ManualPersonal localManualPersonal = (ManualPersonal)paramObject;
    if (localManualPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localManualPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ManualPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ManualPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ManualPersonalPK))
      throw new IllegalArgumentException("arg1");
    ManualPersonalPK localManualPersonalPK = (ManualPersonalPK)paramObject;
    localManualPersonalPK.idManualPersonal = this.idManualPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ManualPersonalPK))
      throw new IllegalArgumentException("arg1");
    ManualPersonalPK localManualPersonalPK = (ManualPersonalPK)paramObject;
    this.idManualPersonal = localManualPersonalPK.idManualPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ManualPersonalPK))
      throw new IllegalArgumentException("arg2");
    ManualPersonalPK localManualPersonalPK = (ManualPersonalPK)paramObject;
    localManualPersonalPK.idManualPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ManualPersonalPK))
      throw new IllegalArgumentException("arg2");
    ManualPersonalPK localManualPersonalPK = (ManualPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localManualPersonalPK.idManualPersonal);
  }

  private static final long jdoGetidManualPersonal(ManualPersonal paramManualPersonal)
  {
    return paramManualPersonal.idManualPersonal;
  }

  private static final void jdoSetidManualPersonal(ManualPersonal paramManualPersonal, long paramLong)
  {
    StateManager localStateManager = paramManualPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualPersonal.idManualPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramManualPersonal, jdoInheritedFieldCount + 0, paramManualPersonal.idManualPersonal, paramLong);
  }

  private static final int jdoGetidSitp(ManualPersonal paramManualPersonal)
  {
    if (paramManualPersonal.jdoFlags <= 0)
      return paramManualPersonal.idSitp;
    StateManager localStateManager = paramManualPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramManualPersonal.idSitp;
    if (localStateManager.isLoaded(paramManualPersonal, jdoInheritedFieldCount + 1))
      return paramManualPersonal.idSitp;
    return localStateManager.getIntField(paramManualPersonal, jdoInheritedFieldCount + 1, paramManualPersonal.idSitp);
  }

  private static final void jdoSetidSitp(ManualPersonal paramManualPersonal, int paramInt)
  {
    if (paramManualPersonal.jdoFlags == 0)
    {
      paramManualPersonal.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramManualPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualPersonal.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramManualPersonal, jdoInheritedFieldCount + 1, paramManualPersonal.idSitp, paramInt);
  }

  private static final ManualCargo jdoGetmanualCargo(ManualPersonal paramManualPersonal)
  {
    StateManager localStateManager = paramManualPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramManualPersonal.manualCargo;
    if (localStateManager.isLoaded(paramManualPersonal, jdoInheritedFieldCount + 2))
      return paramManualPersonal.manualCargo;
    return (ManualCargo)localStateManager.getObjectField(paramManualPersonal, jdoInheritedFieldCount + 2, paramManualPersonal.manualCargo);
  }

  private static final void jdoSetmanualCargo(ManualPersonal paramManualPersonal, ManualCargo paramManualCargo)
  {
    StateManager localStateManager = paramManualPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualPersonal.manualCargo = paramManualCargo;
      return;
    }
    localStateManager.setObjectField(paramManualPersonal, jdoInheritedFieldCount + 2, paramManualPersonal.manualCargo, paramManualCargo);
  }

  private static final Date jdoGettiempoSitp(ManualPersonal paramManualPersonal)
  {
    if (paramManualPersonal.jdoFlags <= 0)
      return paramManualPersonal.tiempoSitp;
    StateManager localStateManager = paramManualPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramManualPersonal.tiempoSitp;
    if (localStateManager.isLoaded(paramManualPersonal, jdoInheritedFieldCount + 3))
      return paramManualPersonal.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramManualPersonal, jdoInheritedFieldCount + 3, paramManualPersonal.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ManualPersonal paramManualPersonal, Date paramDate)
  {
    if (paramManualPersonal.jdoFlags == 0)
    {
      paramManualPersonal.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramManualPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualPersonal.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramManualPersonal, jdoInheritedFieldCount + 3, paramManualPersonal.tiempoSitp, paramDate);
  }

  private static final TipoPersonal jdoGettipoPersonal(ManualPersonal paramManualPersonal)
  {
    StateManager localStateManager = paramManualPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramManualPersonal.tipoPersonal;
    if (localStateManager.isLoaded(paramManualPersonal, jdoInheritedFieldCount + 4))
      return paramManualPersonal.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramManualPersonal, jdoInheritedFieldCount + 4, paramManualPersonal.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ManualPersonal paramManualPersonal, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramManualPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramManualPersonal.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramManualPersonal, jdoInheritedFieldCount + 4, paramManualPersonal.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}