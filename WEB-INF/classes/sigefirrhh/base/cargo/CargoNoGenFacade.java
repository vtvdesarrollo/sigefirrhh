package sigefirrhh.base.cargo;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class CargoNoGenFacade extends CargoFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private CargoNoGenBusiness cargoNoGenBusiness = new CargoNoGenBusiness();

  public Collection findCargoByDescripcionCargo(String descripcionCargo, long idManualCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.cargoNoGenBusiness.findCargoByDescripcionCargo(descripcionCargo, idManualCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCargoByCodCargo(String codCargo, long idManualCargo) throws Exception
  {
    try {
      this.txn.open();
      return this.cargoNoGenBusiness.findCargoByCodCargo(codCargo, idManualCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DetalleTabulador findDetalleTabuladorForRegistroCargos(long idTabulador, int grado, int subGrado, int paso) throws Exception
  {
    try {
      this.txn.open();
      return this.cargoNoGenBusiness.findDetalleTabuladorForRegistroCargos(idTabulador, grado, subGrado, paso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findManualCargoByRegistro(long idRegistro) throws Exception
  {
    try {
      this.txn.open();
      return this.cargoNoGenBusiness.findManualCargoByRegistro(idRegistro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findManualCargoByProcesoSeleccion(String procesoSeleccion, long idOrganismo) throws Exception
  {
    try { this.txn.open();
      return this.cargoNoGenBusiness.findManualCargoByProcesoSeleccion(procesoSeleccion, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findManualCargoByManualPersonal(long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.cargoNoGenBusiness.findManualCargoByManualPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findCargoByIdManualCargo(long idManualCargo)
    throws Exception
  {
    return this.cargoNoGenBusiness.findCargoByIdManualCargo(idManualCargo);
  }

  public Collection findCargoByPersonal(long idPersonal, String estatus)
    throws Exception
  {
    return this.cargoNoGenBusiness.findCargoByPersonal(idPersonal, estatus);
  }

  public Collection findCargoByIdManualCargoAndIdDependencia(long idManualCargo, long idDependencia)
    throws Exception
  {
    return this.cargoNoGenBusiness.findCargoByIdManualCargoAndIdDependencia(idManualCargo, idDependencia);
  }

  public Collection findCargoByIdManualCargoAndIdRegion(long idManualCargo, long idRegion)
    throws Exception
  {
    return this.cargoNoGenBusiness.findCargoByIdManualCargoAndIdRegion(idManualCargo, idRegion);
  }

  public Collection findCargoByIdManualCargoNoLista(long idManualCargo)
    throws Exception
  {
    return this.cargoNoGenBusiness.findCargoByIdManualCargoNoLista(idManualCargo);
  }

  public Cargo findCargoByCodCargoAndCodManualCargo(String codCargo, int codManualCargo) throws Exception
  {
    try {
      this.txn.open();
      return this.cargoNoGenBusiness.findCargoByCodCargoAndCodManualCargo(codCargo, codManualCargo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}