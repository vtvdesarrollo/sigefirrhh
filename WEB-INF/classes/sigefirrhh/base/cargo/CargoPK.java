package sigefirrhh.base.cargo;

import java.io.Serializable;

public class CargoPK
  implements Serializable
{
  public long idCargo;

  public CargoPK()
  {
  }

  public CargoPK(long idCargo)
  {
    this.idCargo = idCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CargoPK thatPK)
  {
    return 
      this.idCargo == thatPK.idCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCargo);
  }
}