package sigefirrhh.base.cargo;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class CargoBusiness extends AbstractBusiness
  implements Serializable
{
  private CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

  private DetalleTabuladorBeanBusiness detalleTabuladorBeanBusiness = new DetalleTabuladorBeanBusiness();

  private DetalleTabuladorAniosBeanBusiness detalleTabuladorAniosBeanBusiness = new DetalleTabuladorAniosBeanBusiness();

  private GrupoOcupacionalBeanBusiness grupoOcupacionalBeanBusiness = new GrupoOcupacionalBeanBusiness();

  private ManualCargoBeanBusiness manualCargoBeanBusiness = new ManualCargoBeanBusiness();

  private ManualPersonalBeanBusiness manualPersonalBeanBusiness = new ManualPersonalBeanBusiness();

  private RamoOcupacionalBeanBusiness ramoOcupacionalBeanBusiness = new RamoOcupacionalBeanBusiness();

  private SerieCargoBeanBusiness serieCargoBeanBusiness = new SerieCargoBeanBusiness();

  private SueldoMinimoBeanBusiness sueldoMinimoBeanBusiness = new SueldoMinimoBeanBusiness();

  private TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

  public void addCargo(Cargo cargo)
    throws Exception
  {
    this.cargoBeanBusiness.addCargo(cargo);
  }

  public void updateCargo(Cargo cargo) throws Exception {
    this.cargoBeanBusiness.updateCargo(cargo);
  }

  public void deleteCargo(Cargo cargo) throws Exception {
    this.cargoBeanBusiness.deleteCargo(cargo);
  }

  public Cargo findCargoById(long cargoId) throws Exception {
    return this.cargoBeanBusiness.findCargoById(cargoId);
  }

  public Collection findAllCargo() throws Exception {
    return this.cargoBeanBusiness.findCargoAll();
  }

  public Collection findCargoByManualCargo(long idManualCargo)
    throws Exception
  {
    return this.cargoBeanBusiness.findByManualCargo(idManualCargo);
  }

  public Collection findCargoByCodCargo(String codCargo)
    throws Exception
  {
    return this.cargoBeanBusiness.findByCodCargo(codCargo);
  }

  public Collection findCargoByDescripcionCargo(String descripcionCargo)
    throws Exception
  {
    return this.cargoBeanBusiness.findByDescripcionCargo(descripcionCargo);
  }

  public void addDetalleTabulador(DetalleTabulador detalleTabulador)
    throws Exception
  {
    this.detalleTabuladorBeanBusiness.addDetalleTabulador(detalleTabulador);
  }

  public void updateDetalleTabulador(DetalleTabulador detalleTabulador) throws Exception {
    this.detalleTabuladorBeanBusiness.updateDetalleTabulador(detalleTabulador);
  }

  public void deleteDetalleTabulador(DetalleTabulador detalleTabulador) throws Exception {
    this.detalleTabuladorBeanBusiness.deleteDetalleTabulador(detalleTabulador);
  }

  public DetalleTabulador findDetalleTabuladorById(long detalleTabuladorId) throws Exception {
    return this.detalleTabuladorBeanBusiness.findDetalleTabuladorById(detalleTabuladorId);
  }

  public Collection findAllDetalleTabulador() throws Exception {
    return this.detalleTabuladorBeanBusiness.findDetalleTabuladorAll();
  }

  public Collection findDetalleTabuladorByTabulador(long idTabulador)
    throws Exception
  {
    return this.detalleTabuladorBeanBusiness.findByTabulador(idTabulador);
  }

  public void addDetalleTabuladorAnios(DetalleTabuladorAnios detalleTabuladorAnios)
    throws Exception
  {
    this.detalleTabuladorAniosBeanBusiness.addDetalleTabuladorAnios(detalleTabuladorAnios);
  }

  public void updateDetalleTabuladorAnios(DetalleTabuladorAnios detalleTabuladorAnios) throws Exception {
    this.detalleTabuladorAniosBeanBusiness.updateDetalleTabuladorAnios(detalleTabuladorAnios);
  }

  public void deleteDetalleTabuladorAnios(DetalleTabuladorAnios detalleTabuladorAnios) throws Exception {
    this.detalleTabuladorAniosBeanBusiness.deleteDetalleTabuladorAnios(detalleTabuladorAnios);
  }

  public DetalleTabuladorAnios findDetalleTabuladorAniosById(long detalleTabuladorAniosId) throws Exception {
    return this.detalleTabuladorAniosBeanBusiness.findDetalleTabuladorAniosById(detalleTabuladorAniosId);
  }

  public Collection findAllDetalleTabuladorAnios() throws Exception {
    return this.detalleTabuladorAniosBeanBusiness.findDetalleTabuladorAniosAll();
  }

  public Collection findDetalleTabuladorAniosByAnioDesde(int anioDesde)
    throws Exception
  {
    return this.detalleTabuladorAniosBeanBusiness.findByAnioDesde(anioDesde);
  }

  public Collection findDetalleTabuladorAniosByAnioHasta(int anioHasta)
    throws Exception
  {
    return this.detalleTabuladorAniosBeanBusiness.findByAnioHasta(anioHasta);
  }

  public Collection findDetalleTabuladorAniosByPaso(int paso)
    throws Exception
  {
    return this.detalleTabuladorAniosBeanBusiness.findByPaso(paso);
  }

  public Collection findDetalleTabuladorAniosByHoras(int horas)
    throws Exception
  {
    return this.detalleTabuladorAniosBeanBusiness.findByHoras(horas);
  }

  public Collection findDetalleTabuladorAniosByTabulador(long idTabulador)
    throws Exception
  {
    return this.detalleTabuladorAniosBeanBusiness.findByTabulador(idTabulador);
  }

  public void addGrupoOcupacional(GrupoOcupacional grupoOcupacional)
    throws Exception
  {
    this.grupoOcupacionalBeanBusiness.addGrupoOcupacional(grupoOcupacional);
  }

  public void updateGrupoOcupacional(GrupoOcupacional grupoOcupacional) throws Exception {
    this.grupoOcupacionalBeanBusiness.updateGrupoOcupacional(grupoOcupacional);
  }

  public void deleteGrupoOcupacional(GrupoOcupacional grupoOcupacional) throws Exception {
    this.grupoOcupacionalBeanBusiness.deleteGrupoOcupacional(grupoOcupacional);
  }

  public GrupoOcupacional findGrupoOcupacionalById(long grupoOcupacionalId) throws Exception {
    return this.grupoOcupacionalBeanBusiness.findGrupoOcupacionalById(grupoOcupacionalId);
  }

  public Collection findAllGrupoOcupacional() throws Exception {
    return this.grupoOcupacionalBeanBusiness.findGrupoOcupacionalAll();
  }

  public Collection findGrupoOcupacionalByCodGrupoOcupacional(String codGrupoOcupacional)
    throws Exception
  {
    return this.grupoOcupacionalBeanBusiness.findByCodGrupoOcupacional(codGrupoOcupacional);
  }

  public Collection findGrupoOcupacionalByNombre(String nombre)
    throws Exception
  {
    return this.grupoOcupacionalBeanBusiness.findByNombre(nombre);
  }

  public Collection findGrupoOcupacionalByRamoOcupacional(long idRamoOcupacional)
    throws Exception
  {
    return this.grupoOcupacionalBeanBusiness.findByRamoOcupacional(idRamoOcupacional);
  }

  public void addManualCargo(ManualCargo manualCargo)
    throws Exception
  {
    this.manualCargoBeanBusiness.addManualCargo(manualCargo);
  }

  public void updateManualCargo(ManualCargo manualCargo) throws Exception {
    this.manualCargoBeanBusiness.updateManualCargo(manualCargo);
  }

  public void deleteManualCargo(ManualCargo manualCargo) throws Exception {
    this.manualCargoBeanBusiness.deleteManualCargo(manualCargo);
  }

  public ManualCargo findManualCargoById(long manualCargoId) throws Exception {
    return this.manualCargoBeanBusiness.findManualCargoById(manualCargoId);
  }

  public Collection findAllManualCargo() throws Exception {
    return this.manualCargoBeanBusiness.findManualCargoAll();
  }

  public Collection findManualCargoByCodManualCargo(int codManualCargo, long idOrganismo)
    throws Exception
  {
    return this.manualCargoBeanBusiness.findByCodManualCargo(codManualCargo, idOrganismo);
  }

  public Collection findManualCargoByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.manualCargoBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findManualCargoByTipoCargo(String tipoCargo, long idOrganismo)
    throws Exception
  {
    return this.manualCargoBeanBusiness.findByTipoCargo(tipoCargo, idOrganismo);
  }

  public Collection findManualCargoByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.manualCargoBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addManualPersonal(ManualPersonal manualPersonal)
    throws Exception
  {
    this.manualPersonalBeanBusiness.addManualPersonal(manualPersonal);
  }

  public void updateManualPersonal(ManualPersonal manualPersonal) throws Exception {
    this.manualPersonalBeanBusiness.updateManualPersonal(manualPersonal);
  }

  public void deleteManualPersonal(ManualPersonal manualPersonal) throws Exception {
    this.manualPersonalBeanBusiness.deleteManualPersonal(manualPersonal);
  }

  public ManualPersonal findManualPersonalById(long manualPersonalId) throws Exception {
    return this.manualPersonalBeanBusiness.findManualPersonalById(manualPersonalId);
  }

  public Collection findAllManualPersonal() throws Exception {
    return this.manualPersonalBeanBusiness.findManualPersonalAll();
  }

  public Collection findManualPersonalByManualCargo(long idManualCargo)
    throws Exception
  {
    return this.manualPersonalBeanBusiness.findByManualCargo(idManualCargo);
  }

  public Collection findManualPersonalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.manualPersonalBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addRamoOcupacional(RamoOcupacional ramoOcupacional)
    throws Exception
  {
    this.ramoOcupacionalBeanBusiness.addRamoOcupacional(ramoOcupacional);
  }

  public void updateRamoOcupacional(RamoOcupacional ramoOcupacional) throws Exception {
    this.ramoOcupacionalBeanBusiness.updateRamoOcupacional(ramoOcupacional);
  }

  public void deleteRamoOcupacional(RamoOcupacional ramoOcupacional) throws Exception {
    this.ramoOcupacionalBeanBusiness.deleteRamoOcupacional(ramoOcupacional);
  }

  public RamoOcupacional findRamoOcupacionalById(long ramoOcupacionalId) throws Exception {
    return this.ramoOcupacionalBeanBusiness.findRamoOcupacionalById(ramoOcupacionalId);
  }

  public Collection findAllRamoOcupacional() throws Exception {
    return this.ramoOcupacionalBeanBusiness.findRamoOcupacionalAll();
  }

  public Collection findRamoOcupacionalByCodRamoOcupacional(String codRamoOcupacional)
    throws Exception
  {
    return this.ramoOcupacionalBeanBusiness.findByCodRamoOcupacional(codRamoOcupacional);
  }

  public Collection findRamoOcupacionalByNombre(String nombre)
    throws Exception
  {
    return this.ramoOcupacionalBeanBusiness.findByNombre(nombre);
  }

  public void addSerieCargo(SerieCargo serieCargo)
    throws Exception
  {
    this.serieCargoBeanBusiness.addSerieCargo(serieCargo);
  }

  public void updateSerieCargo(SerieCargo serieCargo) throws Exception {
    this.serieCargoBeanBusiness.updateSerieCargo(serieCargo);
  }

  public void deleteSerieCargo(SerieCargo serieCargo) throws Exception {
    this.serieCargoBeanBusiness.deleteSerieCargo(serieCargo);
  }

  public SerieCargo findSerieCargoById(long serieCargoId) throws Exception {
    return this.serieCargoBeanBusiness.findSerieCargoById(serieCargoId);
  }

  public Collection findAllSerieCargo() throws Exception {
    return this.serieCargoBeanBusiness.findSerieCargoAll();
  }

  public Collection findSerieCargoByCodSerieCargo(String codSerieCargo)
    throws Exception
  {
    return this.serieCargoBeanBusiness.findByCodSerieCargo(codSerieCargo);
  }

  public Collection findSerieCargoByNombre(String nombre)
    throws Exception
  {
    return this.serieCargoBeanBusiness.findByNombre(nombre);
  }

  public Collection findSerieCargoByGrupoOcupacional(long idGrupoOcupacional)
    throws Exception
  {
    return this.serieCargoBeanBusiness.findByGrupoOcupacional(idGrupoOcupacional);
  }

  public void addSueldoMinimo(SueldoMinimo sueldoMinimo)
    throws Exception
  {
    this.sueldoMinimoBeanBusiness.addSueldoMinimo(sueldoMinimo);
  }

  public void updateSueldoMinimo(SueldoMinimo sueldoMinimo) throws Exception {
    this.sueldoMinimoBeanBusiness.updateSueldoMinimo(sueldoMinimo);
  }

  public void deleteSueldoMinimo(SueldoMinimo sueldoMinimo) throws Exception {
    this.sueldoMinimoBeanBusiness.deleteSueldoMinimo(sueldoMinimo);
  }

  public SueldoMinimo findSueldoMinimoById(long sueldoMinimoId) throws Exception {
    return this.sueldoMinimoBeanBusiness.findSueldoMinimoById(sueldoMinimoId);
  }

  public Collection findAllSueldoMinimo() throws Exception {
    return this.sueldoMinimoBeanBusiness.findSueldoMinimoAll();
  }

  public Collection findSueldoMinimoByFechaVigencia(Date fechaVigencia)
    throws Exception
  {
    return this.sueldoMinimoBeanBusiness.findByFechaVigencia(fechaVigencia);
  }

  public void addTabulador(Tabulador tabulador)
    throws Exception
  {
    this.tabuladorBeanBusiness.addTabulador(tabulador);
  }

  public void updateTabulador(Tabulador tabulador) throws Exception {
    this.tabuladorBeanBusiness.updateTabulador(tabulador);
  }

  public void deleteTabulador(Tabulador tabulador) throws Exception {
    this.tabuladorBeanBusiness.deleteTabulador(tabulador);
  }

  public Tabulador findTabuladorById(long tabuladorId) throws Exception {
    return this.tabuladorBeanBusiness.findTabuladorById(tabuladorId);
  }

  public Collection findAllTabulador() throws Exception {
    return this.tabuladorBeanBusiness.findTabuladorAll();
  }

  public Collection findTabuladorByCodTabulador(String codTabulador, long idOrganismo)
    throws Exception
  {
    return this.tabuladorBeanBusiness.findByCodTabulador(codTabulador, idOrganismo);
  }

  public Collection findTabuladorByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    return this.tabuladorBeanBusiness.findByDescripcion(descripcion, idOrganismo);
  }

  public Collection findTabuladorByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.tabuladorBeanBusiness.findByOrganismo(idOrganismo);
  }
}