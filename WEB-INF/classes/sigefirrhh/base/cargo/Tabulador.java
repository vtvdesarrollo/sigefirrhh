package sigefirrhh.base.cargo;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class Tabulador
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_TIPO;
  private long idTabulador;
  private String codTabulador;
  private String descripcion;
  private String vigente;
  private Date fechaVigencia;
  private String indicadorPaso;
  private String tipoTabulador;
  private String aprobacionMpd;
  private String nombreMoneda;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aprobacionMpd", "codTabulador", "descripcion", "fechaVigencia", "idTabulador", "indicadorPaso", "nombreMoneda", "organismo", "tipoTabulador", "vigente" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 21, 26, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.cargo.Tabulador"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Tabulador());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_TIPO.put("1", "PASO REFLEJA COMPENSACION");
    LISTA_TIPO.put("2", "PASO REFLEJA SUELDO BASICO");
    LISTA_TIPO.put("3", "PASO REFLEJA RANGOS");
    LISTA_TIPO.put("4", "NO MANEJA PASOS");
  }

  public Tabulador()
  {
    jdoSetvigente(this, "S");

    jdoSetindicadorPaso(this, "S");

    jdoSettipoTabulador(this, "1");

    jdoSetaprobacionMpd(this, "S");

    jdoSetnombreMoneda(this, "BOLIVAR");
  }

  public String toString()
  {
    return jdoGetdescripcion(this) + "  -  " + 
      jdoGetcodTabulador(this);
  }

  public String getAprobacionMpd() {
    return jdoGetaprobacionMpd(this);
  }

  public void setAprobacionMpd(String aprobacionMpd) {
    jdoSetaprobacionMpd(this, aprobacionMpd);
  }

  public String getCodTabulador() {
    return jdoGetcodTabulador(this);
  }

  public void setCodTabulador(String codTabulador) {
    jdoSetcodTabulador(this, codTabulador);
  }

  public String getDescripcion() {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion) {
    jdoSetdescripcion(this, descripcion);
  }

  public Date getFechaVigencia() {
    return jdoGetfechaVigencia(this);
  }

  public void setFechaVigencia(Date fechaVigencia) {
    jdoSetfechaVigencia(this, fechaVigencia);
  }

  public long getIdTabulador() {
    return jdoGetidTabulador(this);
  }

  public void setIdTabulador(long idTabulador) {
    jdoSetidTabulador(this, idTabulador);
  }

  public String getIndicadorPaso() {
    return jdoGetindicadorPaso(this);
  }

  public void setIndicadorPaso(String indicadorPaso) {
    jdoSetindicadorPaso(this, indicadorPaso);
  }

  public String getNombreMoneda() {
    return jdoGetnombreMoneda(this);
  }

  public void setNombreMoneda(String nombreMoneda) {
    jdoSetnombreMoneda(this, nombreMoneda);
  }

  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }

  public String getTipoTabulador() {
    return jdoGettipoTabulador(this);
  }

  public void setTipoTabulador(String tipoTabulador) {
    jdoSettipoTabulador(this, tipoTabulador);
  }

  public String getVigente() {
    return jdoGetvigente(this);
  }

  public void setVigente(String vigente) {
    jdoSetvigente(this, vigente);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Tabulador localTabulador = new Tabulador();
    localTabulador.jdoFlags = 1;
    localTabulador.jdoStateManager = paramStateManager;
    return localTabulador;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Tabulador localTabulador = new Tabulador();
    localTabulador.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTabulador.jdoFlags = 1;
    localTabulador.jdoStateManager = paramStateManager;
    return localTabulador;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTabulador);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTabulador);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.indicadorPaso);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreMoneda);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoTabulador);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigente);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTabulador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTabulador = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.indicadorPaso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreMoneda = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoTabulador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigente = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Tabulador paramTabulador, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramTabulador.aprobacionMpd;
      return;
    case 1:
      if (paramTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.codTabulador = paramTabulador.codTabulador;
      return;
    case 2:
      if (paramTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTabulador.descripcion;
      return;
    case 3:
      if (paramTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramTabulador.fechaVigencia;
      return;
    case 4:
      if (paramTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.idTabulador = paramTabulador.idTabulador;
      return;
    case 5:
      if (paramTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.indicadorPaso = paramTabulador.indicadorPaso;
      return;
    case 6:
      if (paramTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.nombreMoneda = paramTabulador.nombreMoneda;
      return;
    case 7:
      if (paramTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramTabulador.organismo;
      return;
    case 8:
      if (paramTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.tipoTabulador = paramTabulador.tipoTabulador;
      return;
    case 9:
      if (paramTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.vigente = paramTabulador.vigente;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Tabulador))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Tabulador localTabulador = (Tabulador)paramObject;
    if (localTabulador.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTabulador, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TabuladorPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TabuladorPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TabuladorPK))
      throw new IllegalArgumentException("arg1");
    TabuladorPK localTabuladorPK = (TabuladorPK)paramObject;
    localTabuladorPK.idTabulador = this.idTabulador;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TabuladorPK))
      throw new IllegalArgumentException("arg1");
    TabuladorPK localTabuladorPK = (TabuladorPK)paramObject;
    this.idTabulador = localTabuladorPK.idTabulador;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TabuladorPK))
      throw new IllegalArgumentException("arg2");
    TabuladorPK localTabuladorPK = (TabuladorPK)paramObject;
    localTabuladorPK.idTabulador = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TabuladorPK))
      throw new IllegalArgumentException("arg2");
    TabuladorPK localTabuladorPK = (TabuladorPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localTabuladorPK.idTabulador);
  }

  private static final String jdoGetaprobacionMpd(Tabulador paramTabulador)
  {
    if (paramTabulador.jdoFlags <= 0)
      return paramTabulador.aprobacionMpd;
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramTabulador.aprobacionMpd;
    if (localStateManager.isLoaded(paramTabulador, jdoInheritedFieldCount + 0))
      return paramTabulador.aprobacionMpd;
    return localStateManager.getStringField(paramTabulador, jdoInheritedFieldCount + 0, paramTabulador.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(Tabulador paramTabulador, String paramString)
  {
    if (paramTabulador.jdoFlags == 0)
    {
      paramTabulador.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTabulador.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramTabulador, jdoInheritedFieldCount + 0, paramTabulador.aprobacionMpd, paramString);
  }

  private static final String jdoGetcodTabulador(Tabulador paramTabulador)
  {
    if (paramTabulador.jdoFlags <= 0)
      return paramTabulador.codTabulador;
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramTabulador.codTabulador;
    if (localStateManager.isLoaded(paramTabulador, jdoInheritedFieldCount + 1))
      return paramTabulador.codTabulador;
    return localStateManager.getStringField(paramTabulador, jdoInheritedFieldCount + 1, paramTabulador.codTabulador);
  }

  private static final void jdoSetcodTabulador(Tabulador paramTabulador, String paramString)
  {
    if (paramTabulador.jdoFlags == 0)
    {
      paramTabulador.codTabulador = paramString;
      return;
    }
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTabulador.codTabulador = paramString;
      return;
    }
    localStateManager.setStringField(paramTabulador, jdoInheritedFieldCount + 1, paramTabulador.codTabulador, paramString);
  }

  private static final String jdoGetdescripcion(Tabulador paramTabulador)
  {
    if (paramTabulador.jdoFlags <= 0)
      return paramTabulador.descripcion;
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramTabulador.descripcion;
    if (localStateManager.isLoaded(paramTabulador, jdoInheritedFieldCount + 2))
      return paramTabulador.descripcion;
    return localStateManager.getStringField(paramTabulador, jdoInheritedFieldCount + 2, paramTabulador.descripcion);
  }

  private static final void jdoSetdescripcion(Tabulador paramTabulador, String paramString)
  {
    if (paramTabulador.jdoFlags == 0)
    {
      paramTabulador.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTabulador.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTabulador, jdoInheritedFieldCount + 2, paramTabulador.descripcion, paramString);
  }

  private static final Date jdoGetfechaVigencia(Tabulador paramTabulador)
  {
    if (paramTabulador.jdoFlags <= 0)
      return paramTabulador.fechaVigencia;
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramTabulador.fechaVigencia;
    if (localStateManager.isLoaded(paramTabulador, jdoInheritedFieldCount + 3))
      return paramTabulador.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramTabulador, jdoInheritedFieldCount + 3, paramTabulador.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(Tabulador paramTabulador, Date paramDate)
  {
    if (paramTabulador.jdoFlags == 0)
    {
      paramTabulador.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTabulador.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTabulador, jdoInheritedFieldCount + 3, paramTabulador.fechaVigencia, paramDate);
  }

  private static final long jdoGetidTabulador(Tabulador paramTabulador)
  {
    return paramTabulador.idTabulador;
  }

  private static final void jdoSetidTabulador(Tabulador paramTabulador, long paramLong)
  {
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTabulador.idTabulador = paramLong;
      return;
    }
    localStateManager.setLongField(paramTabulador, jdoInheritedFieldCount + 4, paramTabulador.idTabulador, paramLong);
  }

  private static final String jdoGetindicadorPaso(Tabulador paramTabulador)
  {
    if (paramTabulador.jdoFlags <= 0)
      return paramTabulador.indicadorPaso;
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramTabulador.indicadorPaso;
    if (localStateManager.isLoaded(paramTabulador, jdoInheritedFieldCount + 5))
      return paramTabulador.indicadorPaso;
    return localStateManager.getStringField(paramTabulador, jdoInheritedFieldCount + 5, paramTabulador.indicadorPaso);
  }

  private static final void jdoSetindicadorPaso(Tabulador paramTabulador, String paramString)
  {
    if (paramTabulador.jdoFlags == 0)
    {
      paramTabulador.indicadorPaso = paramString;
      return;
    }
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTabulador.indicadorPaso = paramString;
      return;
    }
    localStateManager.setStringField(paramTabulador, jdoInheritedFieldCount + 5, paramTabulador.indicadorPaso, paramString);
  }

  private static final String jdoGetnombreMoneda(Tabulador paramTabulador)
  {
    if (paramTabulador.jdoFlags <= 0)
      return paramTabulador.nombreMoneda;
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramTabulador.nombreMoneda;
    if (localStateManager.isLoaded(paramTabulador, jdoInheritedFieldCount + 6))
      return paramTabulador.nombreMoneda;
    return localStateManager.getStringField(paramTabulador, jdoInheritedFieldCount + 6, paramTabulador.nombreMoneda);
  }

  private static final void jdoSetnombreMoneda(Tabulador paramTabulador, String paramString)
  {
    if (paramTabulador.jdoFlags == 0)
    {
      paramTabulador.nombreMoneda = paramString;
      return;
    }
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTabulador.nombreMoneda = paramString;
      return;
    }
    localStateManager.setStringField(paramTabulador, jdoInheritedFieldCount + 6, paramTabulador.nombreMoneda, paramString);
  }

  private static final Organismo jdoGetorganismo(Tabulador paramTabulador)
  {
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramTabulador.organismo;
    if (localStateManager.isLoaded(paramTabulador, jdoInheritedFieldCount + 7))
      return paramTabulador.organismo;
    return (Organismo)localStateManager.getObjectField(paramTabulador, jdoInheritedFieldCount + 7, paramTabulador.organismo);
  }

  private static final void jdoSetorganismo(Tabulador paramTabulador, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTabulador.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramTabulador, jdoInheritedFieldCount + 7, paramTabulador.organismo, paramOrganismo);
  }

  private static final String jdoGettipoTabulador(Tabulador paramTabulador)
  {
    if (paramTabulador.jdoFlags <= 0)
      return paramTabulador.tipoTabulador;
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramTabulador.tipoTabulador;
    if (localStateManager.isLoaded(paramTabulador, jdoInheritedFieldCount + 8))
      return paramTabulador.tipoTabulador;
    return localStateManager.getStringField(paramTabulador, jdoInheritedFieldCount + 8, paramTabulador.tipoTabulador);
  }

  private static final void jdoSettipoTabulador(Tabulador paramTabulador, String paramString)
  {
    if (paramTabulador.jdoFlags == 0)
    {
      paramTabulador.tipoTabulador = paramString;
      return;
    }
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTabulador.tipoTabulador = paramString;
      return;
    }
    localStateManager.setStringField(paramTabulador, jdoInheritedFieldCount + 8, paramTabulador.tipoTabulador, paramString);
  }

  private static final String jdoGetvigente(Tabulador paramTabulador)
  {
    if (paramTabulador.jdoFlags <= 0)
      return paramTabulador.vigente;
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramTabulador.vigente;
    if (localStateManager.isLoaded(paramTabulador, jdoInheritedFieldCount + 9))
      return paramTabulador.vigente;
    return localStateManager.getStringField(paramTabulador, jdoInheritedFieldCount + 9, paramTabulador.vigente);
  }

  private static final void jdoSetvigente(Tabulador paramTabulador, String paramString)
  {
    if (paramTabulador.jdoFlags == 0)
    {
      paramTabulador.vigente = paramString;
      return;
    }
    StateManager localStateManager = paramTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTabulador.vigente = paramString;
      return;
    }
    localStateManager.setStringField(paramTabulador, jdoInheritedFieldCount + 9, paramTabulador.vigente, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}