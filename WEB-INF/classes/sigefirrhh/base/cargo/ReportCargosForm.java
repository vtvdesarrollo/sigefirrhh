package sigefirrhh.base.cargo;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportCargosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportCargosForm.class.getName());
  private int reportId;
  private long idManualCargo;
  private String formato = "1";
  private String reportName;
  private Collection listManualCargo;
  private CargoFacade cargoFacade;
  private LoginSession login;

  public ReportCargosForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    this.cargoFacade = new CargoFacade();

    this.reportName = "cargos";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportCargosForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listManualCargo = this.cargoFacade.findManualCargoByOrganismo(this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      this.listManualCargo = new ArrayList();
    }
  }

  public Collection getListManualCargo()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listManualCargo, "sigefirrhh.base.cargo.ManualCargo");
  }
  public void cambiarNombreAReporte() {
    try {
      this.reportName = "cargos";
      if (this.formato.equals("2"))
        this.reportName = ("a_" + this.reportName);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "cargos";
      if (this.formato.equals("2")) {
        this.reportName = ("a_" + this.reportName);
      }

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_manual_cargo", new Long(this.idManualCargo));

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/base/cargo");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public int getReportId()
  {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String formato)
  {
    this.formato = formato;
  }

  public long getIdManualCargo()
  {
    return this.idManualCargo;
  }

  public void setIdManualCargo(long idManualCargo)
  {
    this.idManualCargo = idManualCargo;
  }
}