package sigefirrhh.base.cargo;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ManualPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ManualPersonalForm.class.getName());
  private ManualPersonal manualPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoFacade cargoFacade = new CargoFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showManualPersonalByManualCargo;
  private boolean showManualPersonalByTipoPersonal;
  private String findSelectManualCargo;
  private String findSelectTipoPersonal;
  private Collection findColManualCargo;
  private Collection findColTipoPersonal;
  private Collection colManualCargo;
  private Collection colTipoPersonal;
  private String selectManualCargo;
  private String selectTipoPersonal;
  private Object stateResultManualPersonalByManualCargo = null;

  private Object stateResultManualPersonalByTipoPersonal = null;

  public String getFindSelectManualCargo()
  {
    return this.findSelectManualCargo;
  }
  public void setFindSelectManualCargo(String valManualCargo) {
    this.findSelectManualCargo = valManualCargo;
  }

  public Collection getFindColManualCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColManualCargo.iterator();
    ManualCargo manualCargo = null;
    while (iterator.hasNext()) {
      manualCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargo.getIdManualCargo()), 
        manualCargo.toString()));
    }
    return col;
  }
  public String getFindSelectTipoPersonal() {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectManualCargo()
  {
    return this.selectManualCargo;
  }
  public void setSelectManualCargo(String valManualCargo) {
    Iterator iterator = this.colManualCargo.iterator();
    ManualCargo manualCargo = null;
    this.manualPersonal.setManualCargo(null);
    while (iterator.hasNext()) {
      manualCargo = (ManualCargo)iterator.next();
      if (String.valueOf(manualCargo.getIdManualCargo()).equals(
        valManualCargo)) {
        this.manualPersonal.setManualCargo(
          manualCargo);
        break;
      }
    }
    this.selectManualCargo = valManualCargo;
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.manualPersonal.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.manualPersonal.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public ManualPersonal getManualPersonal() {
    if (this.manualPersonal == null) {
      this.manualPersonal = new ManualPersonal();
    }
    return this.manualPersonal;
  }

  public ManualPersonalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColManualCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargo.iterator();
    ManualCargo manualCargo = null;
    while (iterator.hasNext()) {
      manualCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargo.getIdManualCargo()), 
        manualCargo.toString()));
    }
    return col;
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColManualCargo = 
        this.cargoFacade.findManualCargoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colManualCargo = 
        this.cargoFacade.findManualCargoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findManualPersonalByManualCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findManualPersonalByManualCargo(Long.valueOf(this.findSelectManualCargo).longValue());
      this.showManualPersonalByManualCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showManualPersonalByManualCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectManualCargo = null;
    this.findSelectTipoPersonal = null;

    return null;
  }

  public String findManualPersonalByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findManualPersonalByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showManualPersonalByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showManualPersonalByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectManualCargo = null;
    this.findSelectTipoPersonal = null;

    return null;
  }

  public boolean isShowManualPersonalByManualCargo() {
    return this.showManualPersonalByManualCargo;
  }
  public boolean isShowManualPersonalByTipoPersonal() {
    return this.showManualPersonalByTipoPersonal;
  }

  public String selectManualPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectManualCargo = null;
    this.selectTipoPersonal = null;

    long idManualPersonal = 
      Long.parseLong((String)requestParameterMap.get("idManualPersonal"));
    try
    {
      this.manualPersonal = 
        this.cargoFacade.findManualPersonalById(
        idManualPersonal);
      if (this.manualPersonal.getManualCargo() != null) {
        this.selectManualCargo = 
          String.valueOf(this.manualPersonal.getManualCargo().getIdManualCargo());
      }
      if (this.manualPersonal.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.manualPersonal.getTipoPersonal().getIdTipoPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.manualPersonal = null;
    this.showManualPersonalByManualCargo = false;
    this.showManualPersonalByTipoPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.manualPersonal.getTiempoSitp() != null) && 
      (this.manualPersonal.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.cargoFacade.addManualPersonal(
          this.manualPersonal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.cargoFacade.updateManualPersonal(
          this.manualPersonal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.cargoFacade.deleteManualPersonal(
        this.manualPersonal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.manualPersonal = new ManualPersonal();

    this.selectManualCargo = null;

    this.selectTipoPersonal = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.manualPersonal.setIdManualPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.base.cargo.ManualPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.manualPersonal = new ManualPersonal();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}