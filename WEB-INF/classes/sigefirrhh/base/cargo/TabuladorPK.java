package sigefirrhh.base.cargo;

import java.io.Serializable;

public class TabuladorPK
  implements Serializable
{
  public long idTabulador;

  public TabuladorPK()
  {
  }

  public TabuladorPK(long idTabulador)
  {
    this.idTabulador = idTabulador;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TabuladorPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TabuladorPK thatPK)
  {
    return 
      this.idTabulador == thatPK.idTabulador;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTabulador)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTabulador);
  }
}