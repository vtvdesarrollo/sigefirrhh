package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class GrupoOcupacionalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addGrupoOcupacional(GrupoOcupacional grupoOcupacional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    GrupoOcupacional grupoOcupacionalNew = 
      (GrupoOcupacional)BeanUtils.cloneBean(
      grupoOcupacional);

    RamoOcupacionalBeanBusiness ramoOcupacionalBeanBusiness = new RamoOcupacionalBeanBusiness();

    if (grupoOcupacionalNew.getRamoOcupacional() != null) {
      grupoOcupacionalNew.setRamoOcupacional(
        ramoOcupacionalBeanBusiness.findRamoOcupacionalById(
        grupoOcupacionalNew.getRamoOcupacional().getIdRamoOcupacional()));
    }
    pm.makePersistent(grupoOcupacionalNew);
  }

  public void updateGrupoOcupacional(GrupoOcupacional grupoOcupacional) throws Exception
  {
    GrupoOcupacional grupoOcupacionalModify = 
      findGrupoOcupacionalById(grupoOcupacional.getIdGrupoOcupacional());

    RamoOcupacionalBeanBusiness ramoOcupacionalBeanBusiness = new RamoOcupacionalBeanBusiness();

    if (grupoOcupacional.getRamoOcupacional() != null) {
      grupoOcupacional.setRamoOcupacional(
        ramoOcupacionalBeanBusiness.findRamoOcupacionalById(
        grupoOcupacional.getRamoOcupacional().getIdRamoOcupacional()));
    }

    BeanUtils.copyProperties(grupoOcupacionalModify, grupoOcupacional);
  }

  public void deleteGrupoOcupacional(GrupoOcupacional grupoOcupacional) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    GrupoOcupacional grupoOcupacionalDelete = 
      findGrupoOcupacionalById(grupoOcupacional.getIdGrupoOcupacional());
    pm.deletePersistent(grupoOcupacionalDelete);
  }

  public GrupoOcupacional findGrupoOcupacionalById(long idGrupoOcupacional) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idGrupoOcupacional == pIdGrupoOcupacional";
    Query query = pm.newQuery(GrupoOcupacional.class, filter);

    query.declareParameters("long pIdGrupoOcupacional");

    parameters.put("pIdGrupoOcupacional", new Long(idGrupoOcupacional));

    Collection colGrupoOcupacional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colGrupoOcupacional.iterator();
    return (GrupoOcupacional)iterator.next();
  }

  public Collection findGrupoOcupacionalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent grupoOcupacionalExtent = pm.getExtent(
      GrupoOcupacional.class, true);
    Query query = pm.newQuery(grupoOcupacionalExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodGrupoOcupacional(String codGrupoOcupacional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codGrupoOcupacional == pCodGrupoOcupacional";

    Query query = pm.newQuery(GrupoOcupacional.class, filter);

    query.declareParameters("java.lang.String pCodGrupoOcupacional");
    HashMap parameters = new HashMap();

    parameters.put("pCodGrupoOcupacional", new String(codGrupoOcupacional));

    query.setOrdering("nombre ascending");

    Collection colGrupoOcupacional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoOcupacional);

    return colGrupoOcupacional;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(GrupoOcupacional.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colGrupoOcupacional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoOcupacional);

    return colGrupoOcupacional;
  }

  public Collection findByRamoOcupacional(long idRamoOcupacional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "ramoOcupacional.idRamoOcupacional == pIdRamoOcupacional";

    Query query = pm.newQuery(GrupoOcupacional.class, filter);

    query.declareParameters("long pIdRamoOcupacional");
    HashMap parameters = new HashMap();

    parameters.put("pIdRamoOcupacional", new Long(idRamoOcupacional));

    query.setOrdering("nombre ascending");

    Collection colGrupoOcupacional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoOcupacional);

    return colGrupoOcupacional;
  }
}