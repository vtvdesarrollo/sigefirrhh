package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SerieCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSerieCargo(SerieCargo serieCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SerieCargo serieCargoNew = 
      (SerieCargo)BeanUtils.cloneBean(
      serieCargo);

    GrupoOcupacionalBeanBusiness grupoOcupacionalBeanBusiness = new GrupoOcupacionalBeanBusiness();

    if (serieCargoNew.getGrupoOcupacional() != null) {
      serieCargoNew.setGrupoOcupacional(
        grupoOcupacionalBeanBusiness.findGrupoOcupacionalById(
        serieCargoNew.getGrupoOcupacional().getIdGrupoOcupacional()));
    }
    pm.makePersistent(serieCargoNew);
  }

  public void updateSerieCargo(SerieCargo serieCargo) throws Exception
  {
    SerieCargo serieCargoModify = 
      findSerieCargoById(serieCargo.getIdSerieCargo());

    GrupoOcupacionalBeanBusiness grupoOcupacionalBeanBusiness = new GrupoOcupacionalBeanBusiness();

    if (serieCargo.getGrupoOcupacional() != null) {
      serieCargo.setGrupoOcupacional(
        grupoOcupacionalBeanBusiness.findGrupoOcupacionalById(
        serieCargo.getGrupoOcupacional().getIdGrupoOcupacional()));
    }

    BeanUtils.copyProperties(serieCargoModify, serieCargo);
  }

  public void deleteSerieCargo(SerieCargo serieCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SerieCargo serieCargoDelete = 
      findSerieCargoById(serieCargo.getIdSerieCargo());
    pm.deletePersistent(serieCargoDelete);
  }

  public SerieCargo findSerieCargoById(long idSerieCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSerieCargo == pIdSerieCargo";
    Query query = pm.newQuery(SerieCargo.class, filter);

    query.declareParameters("long pIdSerieCargo");

    parameters.put("pIdSerieCargo", new Long(idSerieCargo));

    Collection colSerieCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSerieCargo.iterator();
    return (SerieCargo)iterator.next();
  }

  public Collection findSerieCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent serieCargoExtent = pm.getExtent(
      SerieCargo.class, true);
    Query query = pm.newQuery(serieCargoExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodSerieCargo(String codSerieCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codSerieCargo == pCodSerieCargo";

    Query query = pm.newQuery(SerieCargo.class, filter);

    query.declareParameters("java.lang.String pCodSerieCargo");
    HashMap parameters = new HashMap();

    parameters.put("pCodSerieCargo", new String(codSerieCargo));

    query.setOrdering("nombre ascending");

    Collection colSerieCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSerieCargo);

    return colSerieCargo;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(SerieCargo.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colSerieCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSerieCargo);

    return colSerieCargo;
  }

  public Collection findByGrupoOcupacional(long idGrupoOcupacional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoOcupacional.idGrupoOcupacional == pIdGrupoOcupacional";

    Query query = pm.newQuery(SerieCargo.class, filter);

    query.declareParameters("long pIdGrupoOcupacional");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoOcupacional", new Long(idGrupoOcupacional));

    query.setOrdering("nombre ascending");

    Collection colSerieCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSerieCargo);

    return colSerieCargo;
  }
}