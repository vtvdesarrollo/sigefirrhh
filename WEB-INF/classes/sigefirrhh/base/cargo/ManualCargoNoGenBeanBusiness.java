package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class ManualCargoNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(ManualCargoNoGenBeanBusiness.class.getName());

  public Collection findByRegistro(long idRegistro) throws Exception
  {
    long idManualCargo = 0L;
    PersistenceManager pm = PMThread.getPM();
    String filter = "idManualCargo == pIdManualCargo";
    Query query = pm.newQuery(ManualCargo.class, filter);
    query.declareParameters("long pIdManualCargo");
    Collection colManualCargo = new ArrayList();

    Connection connection = null;
    ResultSet rsRegistroPersonal = null;
    PreparedStatement stRegistroPersonal = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      StringBuffer sql = new StringBuffer();
      sql.append("select id_tipo_personal ");
      sql.append(" from registropersonal");
      sql.append("  where id_registro = ? ");

      stRegistroPersonal = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistroPersonal.setLong(1, idRegistro);
      rsRegistroPersonal = stRegistroPersonal.executeQuery();

      ResultSet rsManualPersonal = null;
      PreparedStatement stManualPersonal = null;

      this.log.error("manualcargo 0 ");
      this.log.error("idRegistro " + idRegistro);
      for (; rsRegistroPersonal.next(); 
        rsManualPersonal.next())
      {
        this.log.error("manualcargo 1 ");
        this.log.error("idtp " + rsRegistroPersonal.getLong("id_tipo_personal"));
        sql = new StringBuffer();
        sql.append("select id_manual_cargo ");
        sql.append(" from manualpersonal ");
        sql.append("  where id_tipo_personal = ? ");

        stManualPersonal = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stManualPersonal.setLong(1, rsRegistroPersonal.getLong("id_tipo_personal"));
        rsManualPersonal = stManualPersonal.executeQuery();
        this.log.error("manualcargo 2 ");

        continue;
        this.log.error("manualcargo 3 ");
        idManualCargo = rsManualPersonal.getLong("id_manual_cargo");
        this.log.error("idManualCargo " + idManualCargo);
        HashMap parameters = new HashMap();
        parameters.put("pIdManualCargo", new Long(idManualCargo));
        colManualCargo.addAll(
          (Collection)query.executeWithMap(parameters));
      }

      pm.makeTransientAll(colManualCargo);

      return colManualCargo;
    } finally {
      if (rsRegistroPersonal != null) try {
          rsRegistroPersonal.close();
        } catch (Exception localException3) {
        } if (stRegistroPersonal != null) try {
          stRegistroPersonal.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findByProcesoSeleccion(String procesoSeleccion, long idOrganismo) throws Exception { PersistenceManager pm = PMThread.getPM();

    String filter = "procesoSeleccion == pProcesoSeleccion &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(ManualCargo.class, filter);

    query.declareParameters("String pProcesoSeleccion, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pProcesoSeleccion", procesoSeleccion);
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codManualCargo ascending");

    Collection colManualCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colManualCargo);

    return colManualCargo; }

  public Collection findByManualPersonal(long idTipoPersonal) throws Exception
  {
    long idManualCargo = 0L;
    PersistenceManager pm = PMThread.getPM();
    String filter = "idManualCargo == pIdManualCargo";
    Query query = pm.newQuery(ManualCargo.class, filter);
    query.declareParameters("long pIdManualCargo");
    Collection colManualCargo = new ArrayList();

    Connection connection = null;
    ResultSet rsManualPersonal = null;
    PreparedStatement stManualPersonal = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      StringBuffer sql = new StringBuffer();
      sql.append("select id_manual_cargo ");
      sql.append(" from manualpersonal ");
      sql.append("  where id_tipo_personal = ? ");

      stManualPersonal = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stManualPersonal.setLong(1, idTipoPersonal);
      rsManualPersonal = stManualPersonal.executeQuery();

      while (rsManualPersonal.next()) {
        idManualCargo = rsManualPersonal.getLong("id_manual_cargo");

        HashMap parameters = new HashMap();
        parameters.put("pIdManualCargo", new Long(idManualCargo));
        colManualCargo.addAll(
          (Collection)query.executeWithMap(parameters));
      }

      pm.makeTransientAll(colManualCargo);

      return colManualCargo;
    } finally {
      if (rsManualPersonal != null) try {
          rsManualPersonal.close();
        } catch (Exception localException3) {
        } if (stManualPersonal != null) try {
          stManualPersonal.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}