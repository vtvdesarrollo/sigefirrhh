package sigefirrhh.base.cargo;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class DetalleTabuladorAnios
  implements Serializable, PersistenceCapable
{
  private long idDetalleTabuladorAnios;
  private int anioDesde;
  private int anioHasta;
  private int paso;
  private int horas;
  private double sueldo;
  private Cargo cargo;
  private Tabulador tabulador;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anioDesde", "anioHasta", "cargo", "horas", "idDetalleTabuladorAnios", "idSitp", "paso", "sueldo", "tabulador", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Integer.TYPE, Long.TYPE, Integer.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Tabulador"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 21, 26, 21, 24, 21, 21, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcargo(this).getCodCargo() + "  -  " + 
      jdoGetanioDesde(this) + "  -  " + 
      jdoGetanioHasta(this) + "  -  " + 
      jdoGethoras(this) + "H  -  " + 
      jdoGetsueldo(this);
  }

  public int getAnioDesde()
  {
    return jdoGetanioDesde(this);
  }

  public int getAnioHasta()
  {
    return jdoGetanioHasta(this);
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public int getHoras()
  {
    return jdoGethoras(this);
  }

  public long getIdDetalleTabuladorAnios()
  {
    return jdoGetidDetalleTabuladorAnios(this);
  }

  public int getPaso()
  {
    return jdoGetpaso(this);
  }

  public double getSueldo()
  {
    return jdoGetsueldo(this);
  }

  public Tabulador getTabulador()
  {
    return jdoGettabulador(this);
  }

  public void setAnioDesde(int i)
  {
    jdoSetanioDesde(this, i);
  }

  public void setAnioHasta(int i)
  {
    jdoSetanioHasta(this, i);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public void setHoras(int i)
  {
    jdoSethoras(this, i);
  }

  public void setIdDetalleTabuladorAnios(long l)
  {
    jdoSetidDetalleTabuladorAnios(this, l);
  }

  public void setPaso(int i)
  {
    jdoSetpaso(this, i);
  }

  public void setSueldo(double d)
  {
    jdoSetsueldo(this, d);
  }

  public void setTabulador(Tabulador tabulador)
  {
    jdoSettabulador(this, tabulador);
  }

  public int getIdSitp() {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i) {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date) {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.cargo.DetalleTabuladorAnios"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DetalleTabuladorAnios());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DetalleTabuladorAnios localDetalleTabuladorAnios = new DetalleTabuladorAnios();
    localDetalleTabuladorAnios.jdoFlags = 1;
    localDetalleTabuladorAnios.jdoStateManager = paramStateManager;
    return localDetalleTabuladorAnios;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DetalleTabuladorAnios localDetalleTabuladorAnios = new DetalleTabuladorAnios();
    localDetalleTabuladorAnios.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDetalleTabuladorAnios.jdoFlags = 1;
    localDetalleTabuladorAnios.jdoStateManager = paramStateManager;
    return localDetalleTabuladorAnios;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioDesde);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioHasta);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.horas);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDetalleTabuladorAnios);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.paso);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tabulador);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioDesde = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioHasta = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horas = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDetalleTabuladorAnios = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.paso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tabulador = ((Tabulador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DetalleTabuladorAnios paramDetalleTabuladorAnios, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDetalleTabuladorAnios == null)
        throw new IllegalArgumentException("arg1");
      this.anioDesde = paramDetalleTabuladorAnios.anioDesde;
      return;
    case 1:
      if (paramDetalleTabuladorAnios == null)
        throw new IllegalArgumentException("arg1");
      this.anioHasta = paramDetalleTabuladorAnios.anioHasta;
      return;
    case 2:
      if (paramDetalleTabuladorAnios == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramDetalleTabuladorAnios.cargo;
      return;
    case 3:
      if (paramDetalleTabuladorAnios == null)
        throw new IllegalArgumentException("arg1");
      this.horas = paramDetalleTabuladorAnios.horas;
      return;
    case 4:
      if (paramDetalleTabuladorAnios == null)
        throw new IllegalArgumentException("arg1");
      this.idDetalleTabuladorAnios = paramDetalleTabuladorAnios.idDetalleTabuladorAnios;
      return;
    case 5:
      if (paramDetalleTabuladorAnios == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramDetalleTabuladorAnios.idSitp;
      return;
    case 6:
      if (paramDetalleTabuladorAnios == null)
        throw new IllegalArgumentException("arg1");
      this.paso = paramDetalleTabuladorAnios.paso;
      return;
    case 7:
      if (paramDetalleTabuladorAnios == null)
        throw new IllegalArgumentException("arg1");
      this.sueldo = paramDetalleTabuladorAnios.sueldo;
      return;
    case 8:
      if (paramDetalleTabuladorAnios == null)
        throw new IllegalArgumentException("arg1");
      this.tabulador = paramDetalleTabuladorAnios.tabulador;
      return;
    case 9:
      if (paramDetalleTabuladorAnios == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramDetalleTabuladorAnios.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DetalleTabuladorAnios))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DetalleTabuladorAnios localDetalleTabuladorAnios = (DetalleTabuladorAnios)paramObject;
    if (localDetalleTabuladorAnios.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDetalleTabuladorAnios, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DetalleTabuladorAniosPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DetalleTabuladorAniosPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DetalleTabuladorAniosPK))
      throw new IllegalArgumentException("arg1");
    DetalleTabuladorAniosPK localDetalleTabuladorAniosPK = (DetalleTabuladorAniosPK)paramObject;
    localDetalleTabuladorAniosPK.idDetalleTabuladorAnios = this.idDetalleTabuladorAnios;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DetalleTabuladorAniosPK))
      throw new IllegalArgumentException("arg1");
    DetalleTabuladorAniosPK localDetalleTabuladorAniosPK = (DetalleTabuladorAniosPK)paramObject;
    this.idDetalleTabuladorAnios = localDetalleTabuladorAniosPK.idDetalleTabuladorAnios;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DetalleTabuladorAniosPK))
      throw new IllegalArgumentException("arg2");
    DetalleTabuladorAniosPK localDetalleTabuladorAniosPK = (DetalleTabuladorAniosPK)paramObject;
    localDetalleTabuladorAniosPK.idDetalleTabuladorAnios = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DetalleTabuladorAniosPK))
      throw new IllegalArgumentException("arg2");
    DetalleTabuladorAniosPK localDetalleTabuladorAniosPK = (DetalleTabuladorAniosPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localDetalleTabuladorAniosPK.idDetalleTabuladorAnios);
  }

  private static final int jdoGetanioDesde(DetalleTabuladorAnios paramDetalleTabuladorAnios)
  {
    if (paramDetalleTabuladorAnios.jdoFlags <= 0)
      return paramDetalleTabuladorAnios.anioDesde;
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorAnios.anioDesde;
    if (localStateManager.isLoaded(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 0))
      return paramDetalleTabuladorAnios.anioDesde;
    return localStateManager.getIntField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 0, paramDetalleTabuladorAnios.anioDesde);
  }

  private static final void jdoSetanioDesde(DetalleTabuladorAnios paramDetalleTabuladorAnios, int paramInt)
  {
    if (paramDetalleTabuladorAnios.jdoFlags == 0)
    {
      paramDetalleTabuladorAnios.anioDesde = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorAnios.anioDesde = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 0, paramDetalleTabuladorAnios.anioDesde, paramInt);
  }

  private static final int jdoGetanioHasta(DetalleTabuladorAnios paramDetalleTabuladorAnios)
  {
    if (paramDetalleTabuladorAnios.jdoFlags <= 0)
      return paramDetalleTabuladorAnios.anioHasta;
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorAnios.anioHasta;
    if (localStateManager.isLoaded(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 1))
      return paramDetalleTabuladorAnios.anioHasta;
    return localStateManager.getIntField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 1, paramDetalleTabuladorAnios.anioHasta);
  }

  private static final void jdoSetanioHasta(DetalleTabuladorAnios paramDetalleTabuladorAnios, int paramInt)
  {
    if (paramDetalleTabuladorAnios.jdoFlags == 0)
    {
      paramDetalleTabuladorAnios.anioHasta = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorAnios.anioHasta = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 1, paramDetalleTabuladorAnios.anioHasta, paramInt);
  }

  private static final Cargo jdoGetcargo(DetalleTabuladorAnios paramDetalleTabuladorAnios)
  {
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorAnios.cargo;
    if (localStateManager.isLoaded(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 2))
      return paramDetalleTabuladorAnios.cargo;
    return (Cargo)localStateManager.getObjectField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 2, paramDetalleTabuladorAnios.cargo);
  }

  private static final void jdoSetcargo(DetalleTabuladorAnios paramDetalleTabuladorAnios, Cargo paramCargo)
  {
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorAnios.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 2, paramDetalleTabuladorAnios.cargo, paramCargo);
  }

  private static final int jdoGethoras(DetalleTabuladorAnios paramDetalleTabuladorAnios)
  {
    if (paramDetalleTabuladorAnios.jdoFlags <= 0)
      return paramDetalleTabuladorAnios.horas;
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorAnios.horas;
    if (localStateManager.isLoaded(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 3))
      return paramDetalleTabuladorAnios.horas;
    return localStateManager.getIntField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 3, paramDetalleTabuladorAnios.horas);
  }

  private static final void jdoSethoras(DetalleTabuladorAnios paramDetalleTabuladorAnios, int paramInt)
  {
    if (paramDetalleTabuladorAnios.jdoFlags == 0)
    {
      paramDetalleTabuladorAnios.horas = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorAnios.horas = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 3, paramDetalleTabuladorAnios.horas, paramInt);
  }

  private static final long jdoGetidDetalleTabuladorAnios(DetalleTabuladorAnios paramDetalleTabuladorAnios)
  {
    return paramDetalleTabuladorAnios.idDetalleTabuladorAnios;
  }

  private static final void jdoSetidDetalleTabuladorAnios(DetalleTabuladorAnios paramDetalleTabuladorAnios, long paramLong)
  {
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorAnios.idDetalleTabuladorAnios = paramLong;
      return;
    }
    localStateManager.setLongField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 4, paramDetalleTabuladorAnios.idDetalleTabuladorAnios, paramLong);
  }

  private static final int jdoGetidSitp(DetalleTabuladorAnios paramDetalleTabuladorAnios)
  {
    if (paramDetalleTabuladorAnios.jdoFlags <= 0)
      return paramDetalleTabuladorAnios.idSitp;
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorAnios.idSitp;
    if (localStateManager.isLoaded(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 5))
      return paramDetalleTabuladorAnios.idSitp;
    return localStateManager.getIntField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 5, paramDetalleTabuladorAnios.idSitp);
  }

  private static final void jdoSetidSitp(DetalleTabuladorAnios paramDetalleTabuladorAnios, int paramInt)
  {
    if (paramDetalleTabuladorAnios.jdoFlags == 0)
    {
      paramDetalleTabuladorAnios.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorAnios.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 5, paramDetalleTabuladorAnios.idSitp, paramInt);
  }

  private static final int jdoGetpaso(DetalleTabuladorAnios paramDetalleTabuladorAnios)
  {
    if (paramDetalleTabuladorAnios.jdoFlags <= 0)
      return paramDetalleTabuladorAnios.paso;
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorAnios.paso;
    if (localStateManager.isLoaded(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 6))
      return paramDetalleTabuladorAnios.paso;
    return localStateManager.getIntField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 6, paramDetalleTabuladorAnios.paso);
  }

  private static final void jdoSetpaso(DetalleTabuladorAnios paramDetalleTabuladorAnios, int paramInt)
  {
    if (paramDetalleTabuladorAnios.jdoFlags == 0)
    {
      paramDetalleTabuladorAnios.paso = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorAnios.paso = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 6, paramDetalleTabuladorAnios.paso, paramInt);
  }

  private static final double jdoGetsueldo(DetalleTabuladorAnios paramDetalleTabuladorAnios)
  {
    if (paramDetalleTabuladorAnios.jdoFlags <= 0)
      return paramDetalleTabuladorAnios.sueldo;
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorAnios.sueldo;
    if (localStateManager.isLoaded(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 7))
      return paramDetalleTabuladorAnios.sueldo;
    return localStateManager.getDoubleField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 7, paramDetalleTabuladorAnios.sueldo);
  }

  private static final void jdoSetsueldo(DetalleTabuladorAnios paramDetalleTabuladorAnios, double paramDouble)
  {
    if (paramDetalleTabuladorAnios.jdoFlags == 0)
    {
      paramDetalleTabuladorAnios.sueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorAnios.sueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 7, paramDetalleTabuladorAnios.sueldo, paramDouble);
  }

  private static final Tabulador jdoGettabulador(DetalleTabuladorAnios paramDetalleTabuladorAnios)
  {
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorAnios.tabulador;
    if (localStateManager.isLoaded(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 8))
      return paramDetalleTabuladorAnios.tabulador;
    return (Tabulador)localStateManager.getObjectField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 8, paramDetalleTabuladorAnios.tabulador);
  }

  private static final void jdoSettabulador(DetalleTabuladorAnios paramDetalleTabuladorAnios, Tabulador paramTabulador)
  {
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorAnios.tabulador = paramTabulador;
      return;
    }
    localStateManager.setObjectField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 8, paramDetalleTabuladorAnios.tabulador, paramTabulador);
  }

  private static final Date jdoGettiempoSitp(DetalleTabuladorAnios paramDetalleTabuladorAnios)
  {
    if (paramDetalleTabuladorAnios.jdoFlags <= 0)
      return paramDetalleTabuladorAnios.tiempoSitp;
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorAnios.tiempoSitp;
    if (localStateManager.isLoaded(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 9))
      return paramDetalleTabuladorAnios.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 9, paramDetalleTabuladorAnios.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(DetalleTabuladorAnios paramDetalleTabuladorAnios, Date paramDate)
  {
    if (paramDetalleTabuladorAnios.jdoFlags == 0)
    {
      paramDetalleTabuladorAnios.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorAnios.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorAnios.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramDetalleTabuladorAnios, jdoInheritedFieldCount + 9, paramDetalleTabuladorAnios.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}