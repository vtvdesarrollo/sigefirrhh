package sigefirrhh.base.cargo;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SueldoMinimoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSueldoMinimo(SueldoMinimo sueldoMinimo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SueldoMinimo sueldoMinimoNew = 
      (SueldoMinimo)BeanUtils.cloneBean(
      sueldoMinimo);

    pm.makePersistent(sueldoMinimoNew);
  }

  public void updateSueldoMinimo(SueldoMinimo sueldoMinimo) throws Exception
  {
    SueldoMinimo sueldoMinimoModify = 
      findSueldoMinimoById(sueldoMinimo.getIdSueldoMinimo());

    BeanUtils.copyProperties(sueldoMinimoModify, sueldoMinimo);
  }

  public void deleteSueldoMinimo(SueldoMinimo sueldoMinimo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SueldoMinimo sueldoMinimoDelete = 
      findSueldoMinimoById(sueldoMinimo.getIdSueldoMinimo());
    pm.deletePersistent(sueldoMinimoDelete);
  }

  public SueldoMinimo findSueldoMinimoById(long idSueldoMinimo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSueldoMinimo == pIdSueldoMinimo";
    Query query = pm.newQuery(SueldoMinimo.class, filter);

    query.declareParameters("long pIdSueldoMinimo");

    parameters.put("pIdSueldoMinimo", new Long(idSueldoMinimo));

    Collection colSueldoMinimo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSueldoMinimo.iterator();
    return (SueldoMinimo)iterator.next();
  }

  public Collection findSueldoMinimoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent sueldoMinimoExtent = pm.getExtent(
      SueldoMinimo.class, true);
    Query query = pm.newQuery(sueldoMinimoExtent);
    query.setOrdering("fechaVigencia ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByFechaVigencia(Date fechaVigencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "fechaVigencia == pFechaVigencia";

    Query query = pm.newQuery(SueldoMinimo.class, filter);

    query.declareParameters("java.util.Date pFechaVigencia");
    HashMap parameters = new HashMap();

    parameters.put("pFechaVigencia", fechaVigencia);

    query.setOrdering("fechaVigencia ascending");

    Collection colSueldoMinimo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSueldoMinimo);

    return colSueldoMinimo;
  }
}