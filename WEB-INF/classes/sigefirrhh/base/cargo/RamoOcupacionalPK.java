package sigefirrhh.base.cargo;

import java.io.Serializable;

public class RamoOcupacionalPK
  implements Serializable
{
  public long idRamoOcupacional;

  public RamoOcupacionalPK()
  {
  }

  public RamoOcupacionalPK(long idRamoOcupacional)
  {
    this.idRamoOcupacional = idRamoOcupacional;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RamoOcupacionalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RamoOcupacionalPK thatPK)
  {
    return 
      this.idRamoOcupacional == thatPK.idRamoOcupacional;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRamoOcupacional)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRamoOcupacional);
  }
}