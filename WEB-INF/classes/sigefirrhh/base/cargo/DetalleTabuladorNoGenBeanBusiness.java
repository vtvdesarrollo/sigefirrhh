package sigefirrhh.base.cargo;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class DetalleTabuladorNoGenBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(DetalleTabuladorNoGenBeanBusiness.class.getName());

  public DetalleTabulador findForRegistroCargos(long idTabulador, int grado, int subGrado, int paso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    this.log.error("idTabulador" + idTabulador);
    this.log.error("grado" + grado);
    this.log.error("subgrado" + subGrado);
    this.log.error("paso" + paso);

    String filter = "tabulador.idTabulador == pIdTabulador && grado == pGrado && subGrado == pSubGrado && paso == pPaso";

    Query query = pm.newQuery(DetalleTabulador.class, filter);

    query.declareParameters("long pIdTabulador, int pGrado, int pSubGrado, int pPaso");

    HashMap parameters = new HashMap();

    parameters.put("pIdTabulador", new Long(idTabulador));
    parameters.put("pGrado", new Integer(grado));
    parameters.put("pSubGrado", new Integer(subGrado));
    parameters.put("pPaso", new Integer(paso));

    query.setOrdering("grado ascending, paso ascending");

    Collection colDetalleTabulador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDetalleTabulador.iterator();
    return (DetalleTabulador)iterator.next();
  }

  public DetalleTabulador findMaximoPaso(long idTabulador, int grado, int subGrado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tabulador.idTabulador == pIdTabulador && grado == pGrado && subGrado == pSubGrado";

    Query query = pm.newQuery(DetalleTabulador.class, filter);

    query.declareParameters("long pIdTabulador, int pGrado, int pSubGrado");

    HashMap parameters = new HashMap();

    parameters.put("pIdTabulador", new Long(idTabulador));
    parameters.put("pGrado", new Integer(grado));
    parameters.put("pSubGrado", new Integer(subGrado));

    query.setOrdering("paso descending");

    Collection colDetalleTabulador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDetalleTabulador.iterator();
    return (DetalleTabulador)iterator.next();
  }

  public DetalleTabulador findByMonto(long idTabulador, int grado, int subGrado, double monto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    this.log.error("grado " + grado);
    this.log.error("subGrado" + subGrado);
    this.log.error("idTabualdor" + idTabulador);
    this.log.error("monto" + monto);
    String filter = "tabulador.idTabulador == pIdTabulador && grado == pGrado && subGrado == pSubGrado && monto >= pMonto";

    Query query = pm.newQuery(DetalleTabulador.class, filter);

    query.declareParameters("long pIdTabulador, int pGrado, int pSubGrado, double pMonto");

    HashMap parameters = new HashMap();

    parameters.put("pIdTabulador", new Long(idTabulador));
    parameters.put("pGrado", new Integer(grado));
    parameters.put("pSubGrado", new Integer(subGrado));
    parameters.put("pMonto", new Double(monto));

    query.setOrdering("monto ascending");

    Collection colDetalleTabulador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDetalleTabulador.iterator();
    return (DetalleTabulador)iterator.next();
  }
}