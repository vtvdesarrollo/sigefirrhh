package sigefirrhh.base.cargo;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class SerieCargoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SerieCargoForm.class.getName());
  private SerieCargo serieCargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoFacade cargoFacade = new CargoFacade();
  private boolean showSerieCargoByCodSerieCargo;
  private boolean showSerieCargoByNombre;
  private boolean showSerieCargoByGrupoOcupacional;
  private String findCodSerieCargo;
  private String findNombre;
  private String findSelectRamoOcupacionalForGrupoOcupacional;
  private String findSelectGrupoOcupacional;
  private Collection findColRamoOcupacionalForGrupoOcupacional;
  private Collection findColGrupoOcupacional;
  private Collection colRamoOcupacionalForGrupoOcupacional;
  private Collection colGrupoOcupacional;
  private String selectRamoOcupacionalForGrupoOcupacional;
  private String selectGrupoOcupacional;
  private Object stateResultSerieCargoByCodSerieCargo = null;

  private Object stateResultSerieCargoByNombre = null;

  private Object stateResultSerieCargoByGrupoOcupacional = null;

  public String getFindCodSerieCargo()
  {
    return this.findCodSerieCargo;
  }
  public void setFindCodSerieCargo(String findCodSerieCargo) {
    this.findCodSerieCargo = findCodSerieCargo;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }
  public Collection getFindColRamoOcupacionalForGrupoOcupacional() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRamoOcupacionalForGrupoOcupacional.iterator();
    RamoOcupacional ramoOcupacionalForGrupoOcupacional = null;
    while (iterator.hasNext()) {
      ramoOcupacionalForGrupoOcupacional = (RamoOcupacional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ramoOcupacionalForGrupoOcupacional.getIdRamoOcupacional()), 
        ramoOcupacionalForGrupoOcupacional.toString()));
    }
    return col;
  }
  public String getFindSelectRamoOcupacionalForGrupoOcupacional() {
    return this.findSelectRamoOcupacionalForGrupoOcupacional;
  }
  public void setFindSelectRamoOcupacionalForGrupoOcupacional(String valRamoOcupacionalForGrupoOcupacional) {
    this.findSelectRamoOcupacionalForGrupoOcupacional = valRamoOcupacionalForGrupoOcupacional;
  }
  public void findChangeRamoOcupacionalForGrupoOcupacional(ValueChangeEvent event) {
    long idRamoOcupacional = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColGrupoOcupacional = null;
      if (idRamoOcupacional > 0L)
        this.findColGrupoOcupacional = 
          this.cargoFacade.findGrupoOcupacionalByRamoOcupacional(
          idRamoOcupacional);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowRamoOcupacionalForGrupoOcupacional() { return this.findColRamoOcupacionalForGrupoOcupacional != null; }

  public String getFindSelectGrupoOcupacional() {
    return this.findSelectGrupoOcupacional;
  }
  public void setFindSelectGrupoOcupacional(String valGrupoOcupacional) {
    this.findSelectGrupoOcupacional = valGrupoOcupacional;
  }

  public Collection getFindColGrupoOcupacional() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColGrupoOcupacional.iterator();
    GrupoOcupacional grupoOcupacional = null;
    while (iterator.hasNext()) {
      grupoOcupacional = (GrupoOcupacional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoOcupacional.getIdGrupoOcupacional()), 
        grupoOcupacional.toString()));
    }
    return col;
  }
  public boolean isFindShowGrupoOcupacional() {
    return this.findColGrupoOcupacional != null;
  }

  public String getSelectRamoOcupacionalForGrupoOcupacional()
  {
    return this.selectRamoOcupacionalForGrupoOcupacional;
  }
  public void setSelectRamoOcupacionalForGrupoOcupacional(String valRamoOcupacionalForGrupoOcupacional) {
    this.selectRamoOcupacionalForGrupoOcupacional = valRamoOcupacionalForGrupoOcupacional;
  }
  public void changeRamoOcupacionalForGrupoOcupacional(ValueChangeEvent event) {
    long idRamoOcupacional = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colGrupoOcupacional = null;
      if (idRamoOcupacional > 0L) {
        this.colGrupoOcupacional = 
          this.cargoFacade.findGrupoOcupacionalByRamoOcupacional(
          idRamoOcupacional);
      } else {
        this.selectGrupoOcupacional = null;
        this.serieCargo.setGrupoOcupacional(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectGrupoOcupacional = null;
      this.serieCargo.setGrupoOcupacional(
        null);
    }
  }

  public boolean isShowRamoOcupacionalForGrupoOcupacional() { return this.colRamoOcupacionalForGrupoOcupacional != null; }

  public String getSelectGrupoOcupacional() {
    return this.selectGrupoOcupacional;
  }
  public void setSelectGrupoOcupacional(String valGrupoOcupacional) {
    Iterator iterator = this.colGrupoOcupacional.iterator();
    GrupoOcupacional grupoOcupacional = null;
    this.serieCargo.setGrupoOcupacional(null);
    while (iterator.hasNext()) {
      grupoOcupacional = (GrupoOcupacional)iterator.next();
      if (String.valueOf(grupoOcupacional.getIdGrupoOcupacional()).equals(
        valGrupoOcupacional)) {
        this.serieCargo.setGrupoOcupacional(
          grupoOcupacional);
        break;
      }
    }
    this.selectGrupoOcupacional = valGrupoOcupacional;
  }
  public boolean isShowGrupoOcupacional() {
    return this.colGrupoOcupacional != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public SerieCargo getSerieCargo() {
    if (this.serieCargo == null) {
      this.serieCargo = new SerieCargo();
    }
    return this.serieCargo;
  }

  public SerieCargoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRamoOcupacionalForGrupoOcupacional()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRamoOcupacionalForGrupoOcupacional.iterator();
    RamoOcupacional ramoOcupacionalForGrupoOcupacional = null;
    while (iterator.hasNext()) {
      ramoOcupacionalForGrupoOcupacional = (RamoOcupacional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ramoOcupacionalForGrupoOcupacional.getIdRamoOcupacional()), 
        ramoOcupacionalForGrupoOcupacional.toString()));
    }
    return col;
  }

  public Collection getColGrupoOcupacional()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoOcupacional.iterator();
    GrupoOcupacional grupoOcupacional = null;
    while (iterator.hasNext()) {
      grupoOcupacional = (GrupoOcupacional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoOcupacional.getIdGrupoOcupacional()), 
        grupoOcupacional.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColRamoOcupacionalForGrupoOcupacional = 
        this.cargoFacade.findAllRamoOcupacional();

      this.colRamoOcupacionalForGrupoOcupacional = 
        this.cargoFacade.findAllRamoOcupacional();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSerieCargoByCodSerieCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findSerieCargoByCodSerieCargo(this.findCodSerieCargo);
      this.showSerieCargoByCodSerieCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSerieCargoByCodSerieCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodSerieCargo = null;
    this.findNombre = null;
    this.findSelectRamoOcupacionalForGrupoOcupacional = null;
    this.findSelectGrupoOcupacional = null;

    return null;
  }

  public String findSerieCargoByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findSerieCargoByNombre(this.findNombre);
      this.showSerieCargoByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSerieCargoByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodSerieCargo = null;
    this.findNombre = null;
    this.findSelectRamoOcupacionalForGrupoOcupacional = null;
    this.findSelectGrupoOcupacional = null;

    return null;
  }

  public String findSerieCargoByGrupoOcupacional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.cargoFacade.findSerieCargoByGrupoOcupacional(Long.valueOf(this.findSelectGrupoOcupacional).longValue());
      this.showSerieCargoByGrupoOcupacional = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSerieCargoByGrupoOcupacional)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodSerieCargo = null;
    this.findNombre = null;
    this.findSelectRamoOcupacionalForGrupoOcupacional = null;
    this.findSelectGrupoOcupacional = null;

    return null;
  }

  public boolean isShowSerieCargoByCodSerieCargo() {
    return this.showSerieCargoByCodSerieCargo;
  }
  public boolean isShowSerieCargoByNombre() {
    return this.showSerieCargoByNombre;
  }
  public boolean isShowSerieCargoByGrupoOcupacional() {
    return this.showSerieCargoByGrupoOcupacional;
  }

  public String selectSerieCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectGrupoOcupacional = null;
    this.selectRamoOcupacionalForGrupoOcupacional = null;

    long idSerieCargo = 
      Long.parseLong((String)requestParameterMap.get("idSerieCargo"));
    try
    {
      this.serieCargo = 
        this.cargoFacade.findSerieCargoById(
        idSerieCargo);
      if (this.serieCargo.getGrupoOcupacional() != null) {
        this.selectGrupoOcupacional = 
          String.valueOf(this.serieCargo.getGrupoOcupacional().getIdGrupoOcupacional());
      }

      GrupoOcupacional grupoOcupacional = null;
      RamoOcupacional ramoOcupacionalForGrupoOcupacional = null;

      if (this.serieCargo.getGrupoOcupacional() != null) {
        long idGrupoOcupacional = 
          this.serieCargo.getGrupoOcupacional().getIdGrupoOcupacional();
        this.selectGrupoOcupacional = String.valueOf(idGrupoOcupacional);
        grupoOcupacional = this.cargoFacade.findGrupoOcupacionalById(
          idGrupoOcupacional);
        this.colGrupoOcupacional = this.cargoFacade.findGrupoOcupacionalByRamoOcupacional(
          grupoOcupacional.getRamoOcupacional().getIdRamoOcupacional());

        long idRamoOcupacionalForGrupoOcupacional = 
          this.serieCargo.getGrupoOcupacional().getRamoOcupacional().getIdRamoOcupacional();
        this.selectRamoOcupacionalForGrupoOcupacional = String.valueOf(idRamoOcupacionalForGrupoOcupacional);
        ramoOcupacionalForGrupoOcupacional = 
          this.cargoFacade.findRamoOcupacionalById(
          idRamoOcupacionalForGrupoOcupacional);
        this.colRamoOcupacionalForGrupoOcupacional = 
          this.cargoFacade.findAllRamoOcupacional();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.serieCargo = null;
    this.showSerieCargoByCodSerieCargo = false;
    this.showSerieCargoByNombre = false;
    this.showSerieCargoByGrupoOcupacional = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.serieCargo.getTiempoSitp() != null) && 
      (this.serieCargo.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.cargoFacade.addSerieCargo(
          this.serieCargo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.cargoFacade.updateSerieCargo(
          this.serieCargo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.cargoFacade.deleteSerieCargo(
        this.serieCargo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.serieCargo = new SerieCargo();

    this.selectGrupoOcupacional = null;

    this.selectRamoOcupacionalForGrupoOcupacional = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.serieCargo.setIdSerieCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.base.cargo.SerieCargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.serieCargo = new SerieCargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}