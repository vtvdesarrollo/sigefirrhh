package sigefirrhh.base.cargo;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class GrupoOcupacional
  implements Serializable, PersistenceCapable
{
  private long idGrupoOcupacional;
  private String codGrupoOcupacional;
  private String nombre;
  private RamoOcupacional ramoOcupacional;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codGrupoOcupacional", "idGrupoOcupacional", "idSitp", "nombre", "ramoOcupacional", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.cargo.RamoOcupacional"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodGrupoOcupacional(this);
  }

  public String getCodGrupoOcupacional()
  {
    return jdoGetcodGrupoOcupacional(this);
  }

  public long getIdGrupoOcupacional() {
    return jdoGetidGrupoOcupacional(this);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public RamoOcupacional getRamoOcupacional() {
    return jdoGetramoOcupacional(this);
  }

  public void setCodGrupoOcupacional(String string) {
    jdoSetcodGrupoOcupacional(this, string);
  }

  public void setIdGrupoOcupacional(long l) {
    jdoSetidGrupoOcupacional(this, l);
  }

  public void setNombre(String string) {
    jdoSetnombre(this, string);
  }

  public void setRamoOcupacional(RamoOcupacional ocupacional) {
    jdoSetramoOcupacional(this, ocupacional);
  }

  public int getIdSitp() {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i) {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date) {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.cargo.GrupoOcupacional"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new GrupoOcupacional());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    GrupoOcupacional localGrupoOcupacional = new GrupoOcupacional();
    localGrupoOcupacional.jdoFlags = 1;
    localGrupoOcupacional.jdoStateManager = paramStateManager;
    return localGrupoOcupacional;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    GrupoOcupacional localGrupoOcupacional = new GrupoOcupacional();
    localGrupoOcupacional.jdoCopyKeyFieldsFromObjectId(paramObject);
    localGrupoOcupacional.jdoFlags = 1;
    localGrupoOcupacional.jdoStateManager = paramStateManager;
    return localGrupoOcupacional;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codGrupoOcupacional);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idGrupoOcupacional);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ramoOcupacional);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codGrupoOcupacional = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idGrupoOcupacional = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ramoOcupacional = ((RamoOcupacional)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(GrupoOcupacional paramGrupoOcupacional, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramGrupoOcupacional == null)
        throw new IllegalArgumentException("arg1");
      this.codGrupoOcupacional = paramGrupoOcupacional.codGrupoOcupacional;
      return;
    case 1:
      if (paramGrupoOcupacional == null)
        throw new IllegalArgumentException("arg1");
      this.idGrupoOcupacional = paramGrupoOcupacional.idGrupoOcupacional;
      return;
    case 2:
      if (paramGrupoOcupacional == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramGrupoOcupacional.idSitp;
      return;
    case 3:
      if (paramGrupoOcupacional == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramGrupoOcupacional.nombre;
      return;
    case 4:
      if (paramGrupoOcupacional == null)
        throw new IllegalArgumentException("arg1");
      this.ramoOcupacional = paramGrupoOcupacional.ramoOcupacional;
      return;
    case 5:
      if (paramGrupoOcupacional == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramGrupoOcupacional.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof GrupoOcupacional))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    GrupoOcupacional localGrupoOcupacional = (GrupoOcupacional)paramObject;
    if (localGrupoOcupacional.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localGrupoOcupacional, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new GrupoOcupacionalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new GrupoOcupacionalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GrupoOcupacionalPK))
      throw new IllegalArgumentException("arg1");
    GrupoOcupacionalPK localGrupoOcupacionalPK = (GrupoOcupacionalPK)paramObject;
    localGrupoOcupacionalPK.idGrupoOcupacional = this.idGrupoOcupacional;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GrupoOcupacionalPK))
      throw new IllegalArgumentException("arg1");
    GrupoOcupacionalPK localGrupoOcupacionalPK = (GrupoOcupacionalPK)paramObject;
    this.idGrupoOcupacional = localGrupoOcupacionalPK.idGrupoOcupacional;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GrupoOcupacionalPK))
      throw new IllegalArgumentException("arg2");
    GrupoOcupacionalPK localGrupoOcupacionalPK = (GrupoOcupacionalPK)paramObject;
    localGrupoOcupacionalPK.idGrupoOcupacional = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GrupoOcupacionalPK))
      throw new IllegalArgumentException("arg2");
    GrupoOcupacionalPK localGrupoOcupacionalPK = (GrupoOcupacionalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localGrupoOcupacionalPK.idGrupoOcupacional);
  }

  private static final String jdoGetcodGrupoOcupacional(GrupoOcupacional paramGrupoOcupacional)
  {
    if (paramGrupoOcupacional.jdoFlags <= 0)
      return paramGrupoOcupacional.codGrupoOcupacional;
    StateManager localStateManager = paramGrupoOcupacional.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoOcupacional.codGrupoOcupacional;
    if (localStateManager.isLoaded(paramGrupoOcupacional, jdoInheritedFieldCount + 0))
      return paramGrupoOcupacional.codGrupoOcupacional;
    return localStateManager.getStringField(paramGrupoOcupacional, jdoInheritedFieldCount + 0, paramGrupoOcupacional.codGrupoOcupacional);
  }

  private static final void jdoSetcodGrupoOcupacional(GrupoOcupacional paramGrupoOcupacional, String paramString)
  {
    if (paramGrupoOcupacional.jdoFlags == 0)
    {
      paramGrupoOcupacional.codGrupoOcupacional = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoOcupacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoOcupacional.codGrupoOcupacional = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoOcupacional, jdoInheritedFieldCount + 0, paramGrupoOcupacional.codGrupoOcupacional, paramString);
  }

  private static final long jdoGetidGrupoOcupacional(GrupoOcupacional paramGrupoOcupacional)
  {
    return paramGrupoOcupacional.idGrupoOcupacional;
  }

  private static final void jdoSetidGrupoOcupacional(GrupoOcupacional paramGrupoOcupacional, long paramLong)
  {
    StateManager localStateManager = paramGrupoOcupacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoOcupacional.idGrupoOcupacional = paramLong;
      return;
    }
    localStateManager.setLongField(paramGrupoOcupacional, jdoInheritedFieldCount + 1, paramGrupoOcupacional.idGrupoOcupacional, paramLong);
  }

  private static final int jdoGetidSitp(GrupoOcupacional paramGrupoOcupacional)
  {
    if (paramGrupoOcupacional.jdoFlags <= 0)
      return paramGrupoOcupacional.idSitp;
    StateManager localStateManager = paramGrupoOcupacional.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoOcupacional.idSitp;
    if (localStateManager.isLoaded(paramGrupoOcupacional, jdoInheritedFieldCount + 2))
      return paramGrupoOcupacional.idSitp;
    return localStateManager.getIntField(paramGrupoOcupacional, jdoInheritedFieldCount + 2, paramGrupoOcupacional.idSitp);
  }

  private static final void jdoSetidSitp(GrupoOcupacional paramGrupoOcupacional, int paramInt)
  {
    if (paramGrupoOcupacional.jdoFlags == 0)
    {
      paramGrupoOcupacional.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramGrupoOcupacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoOcupacional.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramGrupoOcupacional, jdoInheritedFieldCount + 2, paramGrupoOcupacional.idSitp, paramInt);
  }

  private static final String jdoGetnombre(GrupoOcupacional paramGrupoOcupacional)
  {
    if (paramGrupoOcupacional.jdoFlags <= 0)
      return paramGrupoOcupacional.nombre;
    StateManager localStateManager = paramGrupoOcupacional.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoOcupacional.nombre;
    if (localStateManager.isLoaded(paramGrupoOcupacional, jdoInheritedFieldCount + 3))
      return paramGrupoOcupacional.nombre;
    return localStateManager.getStringField(paramGrupoOcupacional, jdoInheritedFieldCount + 3, paramGrupoOcupacional.nombre);
  }

  private static final void jdoSetnombre(GrupoOcupacional paramGrupoOcupacional, String paramString)
  {
    if (paramGrupoOcupacional.jdoFlags == 0)
    {
      paramGrupoOcupacional.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoOcupacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoOcupacional.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoOcupacional, jdoInheritedFieldCount + 3, paramGrupoOcupacional.nombre, paramString);
  }

  private static final RamoOcupacional jdoGetramoOcupacional(GrupoOcupacional paramGrupoOcupacional)
  {
    StateManager localStateManager = paramGrupoOcupacional.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoOcupacional.ramoOcupacional;
    if (localStateManager.isLoaded(paramGrupoOcupacional, jdoInheritedFieldCount + 4))
      return paramGrupoOcupacional.ramoOcupacional;
    return (RamoOcupacional)localStateManager.getObjectField(paramGrupoOcupacional, jdoInheritedFieldCount + 4, paramGrupoOcupacional.ramoOcupacional);
  }

  private static final void jdoSetramoOcupacional(GrupoOcupacional paramGrupoOcupacional, RamoOcupacional paramRamoOcupacional)
  {
    StateManager localStateManager = paramGrupoOcupacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoOcupacional.ramoOcupacional = paramRamoOcupacional;
      return;
    }
    localStateManager.setObjectField(paramGrupoOcupacional, jdoInheritedFieldCount + 4, paramGrupoOcupacional.ramoOcupacional, paramRamoOcupacional);
  }

  private static final Date jdoGettiempoSitp(GrupoOcupacional paramGrupoOcupacional)
  {
    if (paramGrupoOcupacional.jdoFlags <= 0)
      return paramGrupoOcupacional.tiempoSitp;
    StateManager localStateManager = paramGrupoOcupacional.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoOcupacional.tiempoSitp;
    if (localStateManager.isLoaded(paramGrupoOcupacional, jdoInheritedFieldCount + 5))
      return paramGrupoOcupacional.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramGrupoOcupacional, jdoInheritedFieldCount + 5, paramGrupoOcupacional.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(GrupoOcupacional paramGrupoOcupacional, Date paramDate)
  {
    if (paramGrupoOcupacional.jdoFlags == 0)
    {
      paramGrupoOcupacional.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramGrupoOcupacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoOcupacional.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramGrupoOcupacional, jdoInheritedFieldCount + 5, paramGrupoOcupacional.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}