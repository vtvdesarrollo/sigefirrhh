package sigefirrhh.base.personal;

import java.io.Serializable;

public class TipoOtraActividadPK
  implements Serializable
{
  public long idTipoOtraActividad;

  public TipoOtraActividadPK()
  {
  }

  public TipoOtraActividadPK(long idTipoOtraActividad)
  {
    this.idTipoOtraActividad = idTipoOtraActividad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoOtraActividadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoOtraActividadPK thatPK)
  {
    return 
      this.idTipoOtraActividad == thatPK.idTipoOtraActividad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoOtraActividad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoOtraActividad);
  }
}