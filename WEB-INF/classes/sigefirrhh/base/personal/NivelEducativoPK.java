package sigefirrhh.base.personal;

import java.io.Serializable;

public class NivelEducativoPK
  implements Serializable
{
  public long idNivelEducativo;

  public NivelEducativoPK()
  {
  }

  public NivelEducativoPK(long idNivelEducativo)
  {
    this.idNivelEducativo = idNivelEducativo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((NivelEducativoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(NivelEducativoPK thatPK)
  {
    return 
      this.idNivelEducativo == thatPK.idNivelEducativo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idNivelEducativo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idNivelEducativo);
  }
}