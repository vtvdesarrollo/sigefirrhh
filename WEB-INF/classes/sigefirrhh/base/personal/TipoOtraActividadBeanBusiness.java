package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoOtraActividadBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoOtraActividad(TipoOtraActividad tipoOtraActividad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoOtraActividad tipoOtraActividadNew = 
      (TipoOtraActividad)BeanUtils.cloneBean(
      tipoOtraActividad);

    pm.makePersistent(tipoOtraActividadNew);
  }

  public void updateTipoOtraActividad(TipoOtraActividad tipoOtraActividad) throws Exception
  {
    TipoOtraActividad tipoOtraActividadModify = 
      findTipoOtraActividadById(tipoOtraActividad.getIdTipoOtraActividad());

    BeanUtils.copyProperties(tipoOtraActividadModify, tipoOtraActividad);
  }

  public void deleteTipoOtraActividad(TipoOtraActividad tipoOtraActividad) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoOtraActividad tipoOtraActividadDelete = 
      findTipoOtraActividadById(tipoOtraActividad.getIdTipoOtraActividad());
    pm.deletePersistent(tipoOtraActividadDelete);
  }

  public TipoOtraActividad findTipoOtraActividadById(long idTipoOtraActividad) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoOtraActividad == pIdTipoOtraActividad";
    Query query = pm.newQuery(TipoOtraActividad.class, filter);

    query.declareParameters("long pIdTipoOtraActividad");

    parameters.put("pIdTipoOtraActividad", new Long(idTipoOtraActividad));

    Collection colTipoOtraActividad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoOtraActividad.iterator();
    return (TipoOtraActividad)iterator.next();
  }

  public Collection findTipoOtraActividadAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoOtraActividadExtent = pm.getExtent(
      TipoOtraActividad.class, true);
    Query query = pm.newQuery(tipoOtraActividadExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodOtraActividad(String codOtraActividad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codOtraActividad == pCodOtraActividad";

    Query query = pm.newQuery(TipoOtraActividad.class, filter);

    query.declareParameters("java.lang.String pCodOtraActividad");
    HashMap parameters = new HashMap();

    parameters.put("pCodOtraActividad", new String(codOtraActividad));

    query.setOrdering("descripcion ascending");

    Collection colTipoOtraActividad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoOtraActividad);

    return colTipoOtraActividad;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(TipoOtraActividad.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTipoOtraActividad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoOtraActividad);

    return colTipoOtraActividad;
  }
}