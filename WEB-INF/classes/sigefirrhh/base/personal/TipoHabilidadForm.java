package sigefirrhh.base.personal;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class TipoHabilidadForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TipoHabilidadForm.class.getName());
  private TipoHabilidad tipoHabilidad;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showTipoHabilidadByCodTipoHabilidad;
  private boolean showTipoHabilidadByDescripcion;
  private String findCodTipoHabilidad;
  private String findDescripcion;
  private Object stateResultTipoHabilidadByCodTipoHabilidad = null;

  private Object stateResultTipoHabilidadByDescripcion = null;

  public String getFindCodTipoHabilidad()
  {
    return this.findCodTipoHabilidad;
  }
  public void setFindCodTipoHabilidad(String findCodTipoHabilidad) {
    this.findCodTipoHabilidad = findCodTipoHabilidad;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public TipoHabilidad getTipoHabilidad() {
    if (this.tipoHabilidad == null) {
      this.tipoHabilidad = new TipoHabilidad();
    }
    return this.tipoHabilidad;
  }

  public TipoHabilidadForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findTipoHabilidadByCodTipoHabilidad()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findTipoHabilidadByCodTipoHabilidad(this.findCodTipoHabilidad);
      this.showTipoHabilidadByCodTipoHabilidad = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoHabilidadByCodTipoHabilidad)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoHabilidad = null;
    this.findDescripcion = null;

    return null;
  }

  public String findTipoHabilidadByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findTipoHabilidadByDescripcion(this.findDescripcion);
      this.showTipoHabilidadByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoHabilidadByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoHabilidad = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowTipoHabilidadByCodTipoHabilidad() {
    return this.showTipoHabilidadByCodTipoHabilidad;
  }
  public boolean isShowTipoHabilidadByDescripcion() {
    return this.showTipoHabilidadByDescripcion;
  }

  public String selectTipoHabilidad()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTipoHabilidad = 
      Long.parseLong((String)requestParameterMap.get("idTipoHabilidad"));
    try
    {
      this.tipoHabilidad = 
        this.personalFacade.findTipoHabilidadById(
        idTipoHabilidad);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.tipoHabilidad = null;
    this.showTipoHabilidadByCodTipoHabilidad = false;
    this.showTipoHabilidadByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.personalFacade.addTipoHabilidad(
          this.tipoHabilidad);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.personalFacade.updateTipoHabilidad(
          this.tipoHabilidad);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.personalFacade.deleteTipoHabilidad(
        this.tipoHabilidad);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada, " + e, ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.tipoHabilidad = new TipoHabilidad();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.tipoHabilidad.setIdTipoHabilidad(identityGenerator.getNextSequenceNumber("sigefirrhh.base.personal.TipoHabilidad"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.tipoHabilidad = new TipoHabilidad();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("TipoHabilidad");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/personal");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "TipoHabilidad" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}