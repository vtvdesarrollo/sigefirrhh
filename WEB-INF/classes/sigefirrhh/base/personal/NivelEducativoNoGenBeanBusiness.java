package sigefirrhh.base.personal;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class NivelEducativoNoGenBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(NivelEducativoNoGenBeanBusiness.class.getName());

  public Collection findForTitulo()
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String pCod1 = "T";
    String pCod2 = "S";
    String pCod3 = "U";
    String pCod4 = "D";
    String pCod5 = "H";

    String filter = "codNivelEducativo == pCod1 || codNivelEducativo == pCod2 || codNivelEducativo == pCod3 || codNivelEducativo == pCod4 || codNivelEducativo == pCod5 ";

    Query query = pm.newQuery(NivelEducativo.class, filter);

    query.declareParameters("String pCod1, String pCod2, String pCod3, String pCod4, String pCod5");
    HashMap parameters = new HashMap();

    parameters.put("pCod1", pCod1);
    parameters.put("pCod2", pCod2);
    parameters.put("pCod3", pCod3);
    parameters.put("pCod4", pCod4);
    parameters.put("pCod5", pCod5);

    query.setOrdering("orden ascending");

    Collection colNivelEducativo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNivelEducativo);
    this.log.error("size" + colNivelEducativo.size());
    return colNivelEducativo;
  }
}