package sigefirrhh.base.personal;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class GremioForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GremioForm.class.getName());
  private Gremio gremio;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showGremioByCodGremio;
  private boolean showGremioByNombre;
  private String findCodGremio;
  private String findNombre;
  private Object stateResultGremioByCodGremio = null;

  private Object stateResultGremioByNombre = null;

  public String getFindCodGremio()
  {
    return this.findCodGremio;
  }
  public void setFindCodGremio(String findCodGremio) {
    this.findCodGremio = findCodGremio;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Gremio getGremio() {
    if (this.gremio == null) {
      this.gremio = new Gremio();
    }
    return this.gremio;
  }

  public GremioForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findGremioByCodGremio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findGremioByCodGremio(this.findCodGremio);
      this.showGremioByCodGremio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGremioByCodGremio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodGremio = null;
    this.findNombre = null;

    return null;
  }

  public String findGremioByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findGremioByNombre(this.findNombre);
      this.showGremioByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGremioByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodGremio = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowGremioByCodGremio() {
    return this.showGremioByCodGremio;
  }
  public boolean isShowGremioByNombre() {
    return this.showGremioByNombre;
  }

  public String selectGremio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idGremio = 
      Long.parseLong((String)requestParameterMap.get("idGremio"));
    try
    {
      this.gremio = 
        this.personalFacade.findGremioById(
        idGremio);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.gremio = null;
    this.showGremioByCodGremio = false;
    this.showGremioByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.personalFacade.addGremio(
          this.gremio);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.personalFacade.updateGremio(
          this.gremio);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.personalFacade.deleteGremio(
        this.gremio);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.gremio = new Gremio();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.gremio.setIdGremio(identityGenerator.getNextSequenceNumber("sigefirrhh.base.personal.Gremio"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.gremio = new Gremio();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}