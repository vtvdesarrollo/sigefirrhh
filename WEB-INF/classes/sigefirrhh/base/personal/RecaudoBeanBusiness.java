package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class RecaudoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRecaudo(Recaudo recaudo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Recaudo recaudoNew = 
      (Recaudo)BeanUtils.cloneBean(
      recaudo);

    pm.makePersistent(recaudoNew);
  }

  public void updateRecaudo(Recaudo recaudo) throws Exception
  {
    Recaudo recaudoModify = 
      findRecaudoById(recaudo.getIdRecaudo());

    BeanUtils.copyProperties(recaudoModify, recaudo);
  }

  public void deleteRecaudo(Recaudo recaudo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Recaudo recaudoDelete = 
      findRecaudoById(recaudo.getIdRecaudo());
    pm.deletePersistent(recaudoDelete);
  }

  public Recaudo findRecaudoById(long idRecaudo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRecaudo == pIdRecaudo";
    Query query = pm.newQuery(Recaudo.class, filter);

    query.declareParameters("long pIdRecaudo");

    parameters.put("pIdRecaudo", new Long(idRecaudo));

    Collection colRecaudo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRecaudo.iterator();
    return (Recaudo)iterator.next();
  }

  public Collection findRecaudoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent recaudoExtent = pm.getExtent(
      Recaudo.class, true);
    Query query = pm.newQuery(recaudoExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodRecaudo(int codRecaudo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codRecaudo == pCodRecaudo";

    Query query = pm.newQuery(Recaudo.class, filter);

    query.declareParameters("int pCodRecaudo");
    HashMap parameters = new HashMap();

    parameters.put("pCodRecaudo", new Integer(codRecaudo));

    query.setOrdering("descripcion ascending");

    Collection colRecaudo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRecaudo);

    return colRecaudo;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(Recaudo.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colRecaudo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRecaudo);

    return colRecaudo;
  }
}