package sigefirrhh.base.personal;

import java.io.Serializable;

public class TipoAusenciaPK
  implements Serializable
{
  public long idTipoAusencia;

  public TipoAusenciaPK()
  {
  }

  public TipoAusenciaPK(long idTipoAusencia)
  {
    this.idTipoAusencia = idTipoAusencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoAusenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoAusenciaPK thatPK)
  {
    return 
      this.idTipoAusencia == thatPK.idTipoAusencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoAusencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoAusencia);
  }
}