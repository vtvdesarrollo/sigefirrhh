package sigefirrhh.base.personal;

import java.io.Serializable;

public class GrupoProfesionPK
  implements Serializable
{
  public long idGrupoProfesion;

  public GrupoProfesionPK()
  {
  }

  public GrupoProfesionPK(long idGrupoProfesion)
  {
    this.idGrupoProfesion = idGrupoProfesion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((GrupoProfesionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(GrupoProfesionPK thatPK)
  {
    return 
      this.idGrupoProfesion == thatPK.idGrupoProfesion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idGrupoProfesion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idGrupoProfesion);
  }
}