package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoIdioma
  implements Serializable, PersistenceCapable
{
  private long idTipoIdioma;
  private String codTipoIdioma;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoIdioma", "descripcion", "idTipoIdioma" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodTipoIdioma(this);
  }

  public String getCodTipoIdioma()
  {
    return jdoGetcodTipoIdioma(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdTipoIdioma()
  {
    return jdoGetidTipoIdioma(this);
  }

  public void setCodTipoIdioma(String string)
  {
    jdoSetcodTipoIdioma(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdTipoIdioma(long l)
  {
    jdoSetidTipoIdioma(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.TipoIdioma"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoIdioma());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoIdioma localTipoIdioma = new TipoIdioma();
    localTipoIdioma.jdoFlags = 1;
    localTipoIdioma.jdoStateManager = paramStateManager;
    return localTipoIdioma;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoIdioma localTipoIdioma = new TipoIdioma();
    localTipoIdioma.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoIdioma.jdoFlags = 1;
    localTipoIdioma.jdoStateManager = paramStateManager;
    return localTipoIdioma;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoIdioma);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoIdioma);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoIdioma = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoIdioma = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoIdioma paramTipoIdioma, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoIdioma = paramTipoIdioma.codTipoIdioma;
      return;
    case 1:
      if (paramTipoIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTipoIdioma.descripcion;
      return;
    case 2:
      if (paramTipoIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoIdioma = paramTipoIdioma.idTipoIdioma;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoIdioma))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoIdioma localTipoIdioma = (TipoIdioma)paramObject;
    if (localTipoIdioma.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoIdioma, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoIdiomaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoIdiomaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoIdiomaPK))
      throw new IllegalArgumentException("arg1");
    TipoIdiomaPK localTipoIdiomaPK = (TipoIdiomaPK)paramObject;
    localTipoIdiomaPK.idTipoIdioma = this.idTipoIdioma;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoIdiomaPK))
      throw new IllegalArgumentException("arg1");
    TipoIdiomaPK localTipoIdiomaPK = (TipoIdiomaPK)paramObject;
    this.idTipoIdioma = localTipoIdiomaPK.idTipoIdioma;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoIdiomaPK))
      throw new IllegalArgumentException("arg2");
    TipoIdiomaPK localTipoIdiomaPK = (TipoIdiomaPK)paramObject;
    localTipoIdiomaPK.idTipoIdioma = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoIdiomaPK))
      throw new IllegalArgumentException("arg2");
    TipoIdiomaPK localTipoIdiomaPK = (TipoIdiomaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localTipoIdiomaPK.idTipoIdioma);
  }

  private static final String jdoGetcodTipoIdioma(TipoIdioma paramTipoIdioma)
  {
    if (paramTipoIdioma.jdoFlags <= 0)
      return paramTipoIdioma.codTipoIdioma;
    StateManager localStateManager = paramTipoIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramTipoIdioma.codTipoIdioma;
    if (localStateManager.isLoaded(paramTipoIdioma, jdoInheritedFieldCount + 0))
      return paramTipoIdioma.codTipoIdioma;
    return localStateManager.getStringField(paramTipoIdioma, jdoInheritedFieldCount + 0, paramTipoIdioma.codTipoIdioma);
  }

  private static final void jdoSetcodTipoIdioma(TipoIdioma paramTipoIdioma, String paramString)
  {
    if (paramTipoIdioma.jdoFlags == 0)
    {
      paramTipoIdioma.codTipoIdioma = paramString;
      return;
    }
    StateManager localStateManager = paramTipoIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoIdioma.codTipoIdioma = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoIdioma, jdoInheritedFieldCount + 0, paramTipoIdioma.codTipoIdioma, paramString);
  }

  private static final String jdoGetdescripcion(TipoIdioma paramTipoIdioma)
  {
    if (paramTipoIdioma.jdoFlags <= 0)
      return paramTipoIdioma.descripcion;
    StateManager localStateManager = paramTipoIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramTipoIdioma.descripcion;
    if (localStateManager.isLoaded(paramTipoIdioma, jdoInheritedFieldCount + 1))
      return paramTipoIdioma.descripcion;
    return localStateManager.getStringField(paramTipoIdioma, jdoInheritedFieldCount + 1, paramTipoIdioma.descripcion);
  }

  private static final void jdoSetdescripcion(TipoIdioma paramTipoIdioma, String paramString)
  {
    if (paramTipoIdioma.jdoFlags == 0)
    {
      paramTipoIdioma.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoIdioma.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoIdioma, jdoInheritedFieldCount + 1, paramTipoIdioma.descripcion, paramString);
  }

  private static final long jdoGetidTipoIdioma(TipoIdioma paramTipoIdioma)
  {
    return paramTipoIdioma.idTipoIdioma;
  }

  private static final void jdoSetidTipoIdioma(TipoIdioma paramTipoIdioma, long paramLong)
  {
    StateManager localStateManager = paramTipoIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoIdioma.idTipoIdioma = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoIdioma, jdoInheritedFieldCount + 2, paramTipoIdioma.idTipoIdioma, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}