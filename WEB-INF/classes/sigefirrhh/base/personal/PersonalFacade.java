package sigefirrhh.base.personal;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class PersonalFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private PersonalBusiness personalBusiness = new PersonalBusiness();

  public void addAreaCarrera(AreaCarrera areaCarrera)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addAreaCarrera(areaCarrera);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAreaCarrera(AreaCarrera areaCarrera) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateAreaCarrera(areaCarrera);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAreaCarrera(AreaCarrera areaCarrera) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteAreaCarrera(areaCarrera);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public AreaCarrera findAreaCarreraById(long areaCarreraId) throws Exception
  {
    try { this.txn.open();
      AreaCarrera areaCarrera = 
        this.personalBusiness.findAreaCarreraById(areaCarreraId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(areaCarrera);
      return areaCarrera;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAreaCarrera() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllAreaCarrera();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAreaCarreraByCodAreaCarrera(String codAreaCarrera)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findAreaCarreraByCodAreaCarrera(codAreaCarrera);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAreaCarreraByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findAreaCarreraByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCarrera(Carrera carrera)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addCarrera(carrera);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCarrera(Carrera carrera) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateCarrera(carrera);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCarrera(Carrera carrera) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteCarrera(carrera);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Carrera findCarreraById(long carreraId) throws Exception
  {
    try { this.txn.open();
      Carrera carrera = 
        this.personalBusiness.findCarreraById(carreraId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(carrera);
      return carrera;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCarrera() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllCarrera();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCarreraByCodCarrera(String codCarrera)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findCarreraByCodCarrera(codCarrera);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCarreraByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findCarreraByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCarreraArea(CarreraArea carreraArea)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addCarreraArea(carreraArea);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCarreraArea(CarreraArea carreraArea) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateCarreraArea(carreraArea);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCarreraArea(CarreraArea carreraArea) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteCarreraArea(carreraArea);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CarreraArea findCarreraAreaById(long carreraAreaId) throws Exception
  {
    try { this.txn.open();
      CarreraArea carreraArea = 
        this.personalBusiness.findCarreraAreaById(carreraAreaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(carreraArea);
      return carreraArea;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCarreraArea() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllCarreraArea();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCarreraAreaByAreaCarrera(long idAreaCarrera)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findCarreraAreaByAreaCarrera(idAreaCarrera);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCarreraAreaByCarrera(long idCarrera)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findCarreraAreaByCarrera(idCarrera);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addGremio(Gremio gremio)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addGremio(gremio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateGremio(Gremio gremio) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateGremio(gremio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteGremio(Gremio gremio) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteGremio(gremio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Gremio findGremioById(long gremioId) throws Exception
  {
    try { this.txn.open();
      Gremio gremio = 
        this.personalBusiness.findGremioById(gremioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(gremio);
      return gremio;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllGremio() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllGremio();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGremioByCodGremio(String codGremio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findGremioByCodGremio(codGremio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGremioByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findGremioByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addGrupoProfesion(GrupoProfesion grupoProfesion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addGrupoProfesion(grupoProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateGrupoProfesion(GrupoProfesion grupoProfesion) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateGrupoProfesion(grupoProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteGrupoProfesion(GrupoProfesion grupoProfesion) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteGrupoProfesion(grupoProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public GrupoProfesion findGrupoProfesionById(long grupoProfesionId) throws Exception
  {
    try { this.txn.open();
      GrupoProfesion grupoProfesion = 
        this.personalBusiness.findGrupoProfesionById(grupoProfesionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(grupoProfesion);
      return grupoProfesion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllGrupoProfesion() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllGrupoProfesion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoProfesionByCodGrupoProfesion(String codGrupoProfesion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findGrupoProfesionByCodGrupoProfesion(codGrupoProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoProfesionByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findGrupoProfesionByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addInstitucion(Institucion institucion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addInstitucion(institucion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateInstitucion(Institucion institucion) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateInstitucion(institucion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteInstitucion(Institucion institucion) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteInstitucion(institucion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Institucion findInstitucionById(long institucionId) throws Exception
  {
    try { this.txn.open();
      Institucion institucion = 
        this.personalBusiness.findInstitucionById(institucionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(institucion);
      return institucion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllInstitucion() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllInstitucion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findInstitucionByCodInstitucion(String codInstitucion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findInstitucionByCodInstitucion(codInstitucion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findInstitucionByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findInstitucionByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addNivelEducativo(NivelEducativo nivelEducativo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addNivelEducativo(nivelEducativo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateNivelEducativo(NivelEducativo nivelEducativo) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateNivelEducativo(nivelEducativo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteNivelEducativo(NivelEducativo nivelEducativo) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteNivelEducativo(nivelEducativo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public NivelEducativo findNivelEducativoById(long nivelEducativoId) throws Exception
  {
    try { this.txn.open();
      NivelEducativo nivelEducativo = 
        this.personalBusiness.findNivelEducativoById(nivelEducativoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(nivelEducativo);
      return nivelEducativo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllNivelEducativo() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllNivelEducativo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNivelEducativoByCodNivelEducativo(String codNivelEducativo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findNivelEducativoByCodNivelEducativo(codNivelEducativo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNivelEducativoByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findNivelEducativoByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addProfesion(Profesion profesion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addProfesion(profesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateProfesion(Profesion profesion) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateProfesion(profesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteProfesion(Profesion profesion) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteProfesion(profesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Profesion findProfesionById(long profesionId) throws Exception
  {
    try { this.txn.open();
      Profesion profesion = 
        this.personalBusiness.findProfesionById(profesionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(profesion);
      return profesion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllProfesion() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllProfesion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProfesionBySubgrupoProfesion(long idSubgrupoProfesion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findProfesionBySubgrupoProfesion(idSubgrupoProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProfesionByCodProfesion(String codProfesion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findProfesionByCodProfesion(codProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProfesionByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findProfesionByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRecaudo(Recaudo recaudo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addRecaudo(recaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRecaudo(Recaudo recaudo) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateRecaudo(recaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRecaudo(Recaudo recaudo) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteRecaudo(recaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Recaudo findRecaudoById(long recaudoId) throws Exception
  {
    try { this.txn.open();
      Recaudo recaudo = 
        this.personalBusiness.findRecaudoById(recaudoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(recaudo);
      return recaudo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRecaudo() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllRecaudo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRecaudoByCodRecaudo(int codRecaudo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findRecaudoByCodRecaudo(codRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRecaudoByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findRecaudoByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRelacionRecaudo(RelacionRecaudo relacionRecaudo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addRelacionRecaudo(relacionRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRelacionRecaudo(RelacionRecaudo relacionRecaudo) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateRelacionRecaudo(relacionRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRelacionRecaudo(RelacionRecaudo relacionRecaudo) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteRelacionRecaudo(relacionRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RelacionRecaudo findRelacionRecaudoById(long relacionRecaudoId) throws Exception
  {
    try { this.txn.open();
      RelacionRecaudo relacionRecaudo = 
        this.personalBusiness.findRelacionRecaudoById(relacionRecaudoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(relacionRecaudo);
      return relacionRecaudo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRelacionRecaudo() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllRelacionRecaudo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRelacionRecaudoByEvento(int evento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findRelacionRecaudoByEvento(evento);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSubgrupoProfesion(SubgrupoProfesion subgrupoProfesion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addSubgrupoProfesion(subgrupoProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSubgrupoProfesion(SubgrupoProfesion subgrupoProfesion) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateSubgrupoProfesion(subgrupoProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSubgrupoProfesion(SubgrupoProfesion subgrupoProfesion) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteSubgrupoProfesion(subgrupoProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SubgrupoProfesion findSubgrupoProfesionById(long subgrupoProfesionId) throws Exception
  {
    try { this.txn.open();
      SubgrupoProfesion subgrupoProfesion = 
        this.personalBusiness.findSubgrupoProfesionById(subgrupoProfesionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(subgrupoProfesion);
      return subgrupoProfesion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSubgrupoProfesion() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllSubgrupoProfesion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSubgrupoProfesionByGrupoProfesion(long idGrupoProfesion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findSubgrupoProfesionByGrupoProfesion(idGrupoProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSubgrupoProfesionByCodSubgrupoProfesion(String codSubgrupoProfesion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findSubgrupoProfesionByCodSubgrupoProfesion(codSubgrupoProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSubgrupoProfesionByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findSubgrupoProfesionByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoAcreencia(TipoAcreencia tipoAcreencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addTipoAcreencia(tipoAcreencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoAcreencia(TipoAcreencia tipoAcreencia) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateTipoAcreencia(tipoAcreencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoAcreencia(TipoAcreencia tipoAcreencia) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteTipoAcreencia(tipoAcreencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoAcreencia findTipoAcreenciaById(long tipoAcreenciaId) throws Exception
  {
    try { this.txn.open();
      TipoAcreencia tipoAcreencia = 
        this.personalBusiness.findTipoAcreenciaById(tipoAcreenciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoAcreencia);
      return tipoAcreencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoAcreencia() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllTipoAcreencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoAcreenciaByCodTipoAcreencia(String codTipoAcreencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoAcreenciaByCodTipoAcreencia(codTipoAcreencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoAcreenciaByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoAcreenciaByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoAmonestacion(TipoAmonestacion tipoAmonestacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addTipoAmonestacion(tipoAmonestacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoAmonestacion(TipoAmonestacion tipoAmonestacion) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateTipoAmonestacion(tipoAmonestacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoAmonestacion(TipoAmonestacion tipoAmonestacion) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteTipoAmonestacion(tipoAmonestacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoAmonestacion findTipoAmonestacionById(long tipoAmonestacionId) throws Exception
  {
    try { this.txn.open();
      TipoAmonestacion tipoAmonestacion = 
        this.personalBusiness.findTipoAmonestacionById(tipoAmonestacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoAmonestacion);
      return tipoAmonestacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoAmonestacion() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllTipoAmonestacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoAmonestacionByCodTipoAmonestacion(String codTipoAmonestacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoAmonestacionByCodTipoAmonestacion(codTipoAmonestacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoAmonestacionByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoAmonestacionByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoAusencia(TipoAusencia tipoAusencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addTipoAusencia(tipoAusencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoAusencia(TipoAusencia tipoAusencia) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateTipoAusencia(tipoAusencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoAusencia(TipoAusencia tipoAusencia) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteTipoAusencia(tipoAusencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoAusencia findTipoAusenciaById(long tipoAusenciaId) throws Exception
  {
    try { this.txn.open();
      TipoAusencia tipoAusencia = 
        this.personalBusiness.findTipoAusenciaById(tipoAusenciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoAusencia);
      return tipoAusencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoAusencia() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllTipoAusencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoAusenciaByCodTipoAusencia(String codTipoAusencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoAusenciaByCodTipoAusencia(codTipoAusencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoAusenciaByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoAusenciaByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoHabilidad(TipoHabilidad tipoHabilidad)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addTipoHabilidad(tipoHabilidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoHabilidad(TipoHabilidad tipoHabilidad) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateTipoHabilidad(tipoHabilidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoHabilidad(TipoHabilidad tipoHabilidad) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteTipoHabilidad(tipoHabilidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoHabilidad findTipoHabilidadById(long tipoHabilidadId) throws Exception
  {
    try { this.txn.open();
      TipoHabilidad tipoHabilidad = 
        this.personalBusiness.findTipoHabilidadById(tipoHabilidadId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoHabilidad);
      return tipoHabilidad;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoHabilidad() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllTipoHabilidad();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoHabilidadByCodTipoHabilidad(String codTipoHabilidad)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoHabilidadByCodTipoHabilidad(codTipoHabilidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoHabilidadByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoHabilidadByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoIdioma(TipoIdioma tipoIdioma)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addTipoIdioma(tipoIdioma);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoIdioma(TipoIdioma tipoIdioma) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateTipoIdioma(tipoIdioma);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoIdioma(TipoIdioma tipoIdioma) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteTipoIdioma(tipoIdioma);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoIdioma findTipoIdiomaById(long tipoIdiomaId) throws Exception
  {
    try { this.txn.open();
      TipoIdioma tipoIdioma = 
        this.personalBusiness.findTipoIdiomaById(tipoIdiomaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoIdioma);
      return tipoIdioma;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoIdioma() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllTipoIdioma();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoIdiomaByCodTipoIdioma(String codTipoIdioma)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoIdiomaByCodTipoIdioma(codTipoIdioma);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoIdiomaByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoIdiomaByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoOtraActividad(TipoOtraActividad tipoOtraActividad)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addTipoOtraActividad(tipoOtraActividad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoOtraActividad(TipoOtraActividad tipoOtraActividad) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateTipoOtraActividad(tipoOtraActividad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoOtraActividad(TipoOtraActividad tipoOtraActividad) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteTipoOtraActividad(tipoOtraActividad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoOtraActividad findTipoOtraActividadById(long tipoOtraActividadId) throws Exception
  {
    try { this.txn.open();
      TipoOtraActividad tipoOtraActividad = 
        this.personalBusiness.findTipoOtraActividadById(tipoOtraActividadId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoOtraActividad);
      return tipoOtraActividad;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoOtraActividad() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllTipoOtraActividad();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoOtraActividadByCodOtraActividad(String codOtraActividad)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoOtraActividadByCodOtraActividad(codOtraActividad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoOtraActividadByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoOtraActividadByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoReconocimiento(TipoReconocimiento tipoReconocimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addTipoReconocimiento(tipoReconocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoReconocimiento(TipoReconocimiento tipoReconocimiento) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateTipoReconocimiento(tipoReconocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoReconocimiento(TipoReconocimiento tipoReconocimiento) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteTipoReconocimiento(tipoReconocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoReconocimiento findTipoReconocimientoById(long tipoReconocimientoId) throws Exception
  {
    try { this.txn.open();
      TipoReconocimiento tipoReconocimiento = 
        this.personalBusiness.findTipoReconocimientoById(tipoReconocimientoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoReconocimiento);
      return tipoReconocimiento;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoReconocimiento() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllTipoReconocimiento();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoReconocimientoByCodTipoReconocimiento(String codTipoReconocimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoReconocimientoByCodTipoReconocimiento(codTipoReconocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoReconocimientoByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTipoReconocimientoByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTitulo(Titulo titulo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.personalBusiness.addTitulo(titulo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTitulo(Titulo titulo) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.updateTitulo(titulo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTitulo(Titulo titulo) throws Exception
  {
    try { this.txn.open();
      this.personalBusiness.deleteTitulo(titulo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Titulo findTituloById(long tituloId) throws Exception
  {
    try { this.txn.open();
      Titulo titulo = 
        this.personalBusiness.findTituloById(tituloId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(titulo);
      return titulo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTitulo() throws Exception
  {
    try { this.txn.open();
      return this.personalBusiness.findAllTitulo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTituloByCodTitulo(String codTitulo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTituloByCodTitulo(codTitulo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTituloByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTituloByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTituloByNivelEducativo(long idNivelEducativo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTituloByNivelEducativo(idNivelEducativo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTituloByGrupoProfesion(long idGrupoProfesion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalBusiness.findTituloByGrupoProfesion(idGrupoProfesion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}