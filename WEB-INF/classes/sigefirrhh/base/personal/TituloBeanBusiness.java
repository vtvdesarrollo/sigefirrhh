package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TituloBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTitulo(Titulo titulo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Titulo tituloNew = 
      (Titulo)BeanUtils.cloneBean(
      titulo);

    NivelEducativoBeanBusiness nivelEducativoBeanBusiness = new NivelEducativoBeanBusiness();

    if (tituloNew.getNivelEducativo() != null) {
      tituloNew.setNivelEducativo(
        nivelEducativoBeanBusiness.findNivelEducativoById(
        tituloNew.getNivelEducativo().getIdNivelEducativo()));
    }

    GrupoProfesionBeanBusiness grupoProfesionBeanBusiness = new GrupoProfesionBeanBusiness();

    if (tituloNew.getGrupoProfesion() != null) {
      tituloNew.setGrupoProfesion(
        grupoProfesionBeanBusiness.findGrupoProfesionById(
        tituloNew.getGrupoProfesion().getIdGrupoProfesion()));
    }
    pm.makePersistent(tituloNew);
  }

  public void updateTitulo(Titulo titulo) throws Exception
  {
    Titulo tituloModify = 
      findTituloById(titulo.getIdTitulo());

    NivelEducativoBeanBusiness nivelEducativoBeanBusiness = new NivelEducativoBeanBusiness();

    if (titulo.getNivelEducativo() != null) {
      titulo.setNivelEducativo(
        nivelEducativoBeanBusiness.findNivelEducativoById(
        titulo.getNivelEducativo().getIdNivelEducativo()));
    }

    GrupoProfesionBeanBusiness grupoProfesionBeanBusiness = new GrupoProfesionBeanBusiness();

    if (titulo.getGrupoProfesion() != null) {
      titulo.setGrupoProfesion(
        grupoProfesionBeanBusiness.findGrupoProfesionById(
        titulo.getGrupoProfesion().getIdGrupoProfesion()));
    }

    BeanUtils.copyProperties(tituloModify, titulo);
  }

  public void deleteTitulo(Titulo titulo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Titulo tituloDelete = 
      findTituloById(titulo.getIdTitulo());
    pm.deletePersistent(tituloDelete);
  }

  public Titulo findTituloById(long idTitulo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTitulo == pIdTitulo";
    Query query = pm.newQuery(Titulo.class, filter);

    query.declareParameters("long pIdTitulo");

    parameters.put("pIdTitulo", new Long(idTitulo));

    Collection colTitulo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTitulo.iterator();
    return (Titulo)iterator.next();
  }

  public Collection findTituloAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tituloExtent = pm.getExtent(
      Titulo.class, true);
    Query query = pm.newQuery(tituloExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTitulo(String codTitulo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTitulo == pCodTitulo";

    Query query = pm.newQuery(Titulo.class, filter);

    query.declareParameters("java.lang.String pCodTitulo");
    HashMap parameters = new HashMap();

    parameters.put("pCodTitulo", new String(codTitulo));

    query.setOrdering("descripcion ascending");

    Collection colTitulo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTitulo);

    return colTitulo;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(Titulo.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTitulo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTitulo);

    return colTitulo;
  }

  public Collection findByNivelEducativo(long idNivelEducativo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nivelEducativo.idNivelEducativo == pIdNivelEducativo";

    Query query = pm.newQuery(Titulo.class, filter);

    query.declareParameters("long pIdNivelEducativo");
    HashMap parameters = new HashMap();

    parameters.put("pIdNivelEducativo", new Long(idNivelEducativo));

    query.setOrdering("descripcion ascending");

    Collection colTitulo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTitulo);

    return colTitulo;
  }

  public Collection findByGrupoProfesion(long idGrupoProfesion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoProfesion.idGrupoProfesion == pIdGrupoProfesion";

    Query query = pm.newQuery(Titulo.class, filter);

    query.declareParameters("long pIdGrupoProfesion");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoProfesion", new Long(idGrupoProfesion));

    query.setOrdering("descripcion ascending");

    Collection colTitulo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTitulo);

    return colTitulo;
  }
}