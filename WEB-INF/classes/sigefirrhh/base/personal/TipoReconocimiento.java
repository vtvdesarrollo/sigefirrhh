package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoReconocimiento
  implements Serializable, PersistenceCapable
{
  private long idTipoReconocimiento;
  private String codTipoReconocimiento;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoReconocimiento", "descripcion", "idTipoReconocimiento" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodTipoReconocimiento(this);
  }

  public String getCodTipoReconocimiento()
  {
    return jdoGetcodTipoReconocimiento(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdTipoReconocimiento()
  {
    return jdoGetidTipoReconocimiento(this);
  }

  public void setCodTipoReconocimiento(String string)
  {
    jdoSetcodTipoReconocimiento(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdTipoReconocimiento(long l)
  {
    jdoSetidTipoReconocimiento(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.TipoReconocimiento"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoReconocimiento());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoReconocimiento localTipoReconocimiento = new TipoReconocimiento();
    localTipoReconocimiento.jdoFlags = 1;
    localTipoReconocimiento.jdoStateManager = paramStateManager;
    return localTipoReconocimiento;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoReconocimiento localTipoReconocimiento = new TipoReconocimiento();
    localTipoReconocimiento.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoReconocimiento.jdoFlags = 1;
    localTipoReconocimiento.jdoStateManager = paramStateManager;
    return localTipoReconocimiento;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoReconocimiento);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoReconocimiento);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoReconocimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoReconocimiento = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoReconocimiento paramTipoReconocimiento, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoReconocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoReconocimiento = paramTipoReconocimiento.codTipoReconocimiento;
      return;
    case 1:
      if (paramTipoReconocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTipoReconocimiento.descripcion;
      return;
    case 2:
      if (paramTipoReconocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoReconocimiento = paramTipoReconocimiento.idTipoReconocimiento;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoReconocimiento))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoReconocimiento localTipoReconocimiento = (TipoReconocimiento)paramObject;
    if (localTipoReconocimiento.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoReconocimiento, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoReconocimientoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoReconocimientoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoReconocimientoPK))
      throw new IllegalArgumentException("arg1");
    TipoReconocimientoPK localTipoReconocimientoPK = (TipoReconocimientoPK)paramObject;
    localTipoReconocimientoPK.idTipoReconocimiento = this.idTipoReconocimiento;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoReconocimientoPK))
      throw new IllegalArgumentException("arg1");
    TipoReconocimientoPK localTipoReconocimientoPK = (TipoReconocimientoPK)paramObject;
    this.idTipoReconocimiento = localTipoReconocimientoPK.idTipoReconocimiento;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoReconocimientoPK))
      throw new IllegalArgumentException("arg2");
    TipoReconocimientoPK localTipoReconocimientoPK = (TipoReconocimientoPK)paramObject;
    localTipoReconocimientoPK.idTipoReconocimiento = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoReconocimientoPK))
      throw new IllegalArgumentException("arg2");
    TipoReconocimientoPK localTipoReconocimientoPK = (TipoReconocimientoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localTipoReconocimientoPK.idTipoReconocimiento);
  }

  private static final String jdoGetcodTipoReconocimiento(TipoReconocimiento paramTipoReconocimiento)
  {
    if (paramTipoReconocimiento.jdoFlags <= 0)
      return paramTipoReconocimiento.codTipoReconocimiento;
    StateManager localStateManager = paramTipoReconocimiento.jdoStateManager;
    if (localStateManager == null)
      return paramTipoReconocimiento.codTipoReconocimiento;
    if (localStateManager.isLoaded(paramTipoReconocimiento, jdoInheritedFieldCount + 0))
      return paramTipoReconocimiento.codTipoReconocimiento;
    return localStateManager.getStringField(paramTipoReconocimiento, jdoInheritedFieldCount + 0, paramTipoReconocimiento.codTipoReconocimiento);
  }

  private static final void jdoSetcodTipoReconocimiento(TipoReconocimiento paramTipoReconocimiento, String paramString)
  {
    if (paramTipoReconocimiento.jdoFlags == 0)
    {
      paramTipoReconocimiento.codTipoReconocimiento = paramString;
      return;
    }
    StateManager localStateManager = paramTipoReconocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoReconocimiento.codTipoReconocimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoReconocimiento, jdoInheritedFieldCount + 0, paramTipoReconocimiento.codTipoReconocimiento, paramString);
  }

  private static final String jdoGetdescripcion(TipoReconocimiento paramTipoReconocimiento)
  {
    if (paramTipoReconocimiento.jdoFlags <= 0)
      return paramTipoReconocimiento.descripcion;
    StateManager localStateManager = paramTipoReconocimiento.jdoStateManager;
    if (localStateManager == null)
      return paramTipoReconocimiento.descripcion;
    if (localStateManager.isLoaded(paramTipoReconocimiento, jdoInheritedFieldCount + 1))
      return paramTipoReconocimiento.descripcion;
    return localStateManager.getStringField(paramTipoReconocimiento, jdoInheritedFieldCount + 1, paramTipoReconocimiento.descripcion);
  }

  private static final void jdoSetdescripcion(TipoReconocimiento paramTipoReconocimiento, String paramString)
  {
    if (paramTipoReconocimiento.jdoFlags == 0)
    {
      paramTipoReconocimiento.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoReconocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoReconocimiento.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoReconocimiento, jdoInheritedFieldCount + 1, paramTipoReconocimiento.descripcion, paramString);
  }

  private static final long jdoGetidTipoReconocimiento(TipoReconocimiento paramTipoReconocimiento)
  {
    return paramTipoReconocimiento.idTipoReconocimiento;
  }

  private static final void jdoSetidTipoReconocimiento(TipoReconocimiento paramTipoReconocimiento, long paramLong)
  {
    StateManager localStateManager = paramTipoReconocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoReconocimiento.idTipoReconocimiento = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoReconocimiento, jdoInheritedFieldCount + 2, paramTipoReconocimiento.idTipoReconocimiento, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}