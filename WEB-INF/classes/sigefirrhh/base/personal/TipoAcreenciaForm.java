package sigefirrhh.base.personal;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class TipoAcreenciaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TipoAcreenciaForm.class.getName());
  private TipoAcreencia tipoAcreencia;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showTipoAcreenciaByCodTipoAcreencia;
  private boolean showTipoAcreenciaByDescripcion;
  private String findCodTipoAcreencia;
  private String findDescripcion;
  private Object stateResultTipoAcreenciaByCodTipoAcreencia = null;

  private Object stateResultTipoAcreenciaByDescripcion = null;

  public String getFindCodTipoAcreencia()
  {
    return this.findCodTipoAcreencia;
  }
  public void setFindCodTipoAcreencia(String findCodTipoAcreencia) {
    this.findCodTipoAcreencia = findCodTipoAcreencia;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public TipoAcreencia getTipoAcreencia() {
    if (this.tipoAcreencia == null) {
      this.tipoAcreencia = new TipoAcreencia();
    }
    return this.tipoAcreencia;
  }

  public TipoAcreenciaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findTipoAcreenciaByCodTipoAcreencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findTipoAcreenciaByCodTipoAcreencia(this.findCodTipoAcreencia);
      this.showTipoAcreenciaByCodTipoAcreencia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoAcreenciaByCodTipoAcreencia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoAcreencia = null;
    this.findDescripcion = null;

    return null;
  }

  public String findTipoAcreenciaByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findTipoAcreenciaByDescripcion(this.findDescripcion);
      this.showTipoAcreenciaByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoAcreenciaByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoAcreencia = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowTipoAcreenciaByCodTipoAcreencia() {
    return this.showTipoAcreenciaByCodTipoAcreencia;
  }
  public boolean isShowTipoAcreenciaByDescripcion() {
    return this.showTipoAcreenciaByDescripcion;
  }

  public String selectTipoAcreencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTipoAcreencia = 
      Long.parseLong((String)requestParameterMap.get("idTipoAcreencia"));
    try
    {
      this.tipoAcreencia = 
        this.personalFacade.findTipoAcreenciaById(
        idTipoAcreencia);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.tipoAcreencia = null;
    this.showTipoAcreenciaByCodTipoAcreencia = false;
    this.showTipoAcreenciaByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.personalFacade.addTipoAcreencia(
          this.tipoAcreencia);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.personalFacade.updateTipoAcreencia(
          this.tipoAcreencia);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.personalFacade.deleteTipoAcreencia(
        this.tipoAcreencia);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.tipoAcreencia = new TipoAcreencia();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.tipoAcreencia.setIdTipoAcreencia(identityGenerator.getNextSequenceNumber("sigefirrhh.base.personal.TipoAcreencia"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.tipoAcreencia = new TipoAcreencia();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}