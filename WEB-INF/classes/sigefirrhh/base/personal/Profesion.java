package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Profesion
  implements Serializable, PersistenceCapable
{
  private long idProfesion;
  private SubgrupoProfesion subgrupoProfesion;
  private String codProfesion;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codProfesion", "idProfesion", "nombre", "subgrupoProfesion" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.personal.SubgrupoProfesion") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + 
      jdoGetcodProfesion(this);
  }

  public String getCodProfesion()
  {
    return jdoGetcodProfesion(this);
  }

  public long getIdProfesion()
  {
    return jdoGetidProfesion(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public SubgrupoProfesion getSubgrupoProfesion()
  {
    return jdoGetsubgrupoProfesion(this);
  }

  public void setCodProfesion(String string)
  {
    jdoSetcodProfesion(this, string);
  }

  public void setIdProfesion(long l)
  {
    jdoSetidProfesion(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setSubgrupoProfesion(SubgrupoProfesion profesion)
  {
    jdoSetsubgrupoProfesion(this, profesion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.Profesion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Profesion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Profesion localProfesion = new Profesion();
    localProfesion.jdoFlags = 1;
    localProfesion.jdoStateManager = paramStateManager;
    return localProfesion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Profesion localProfesion = new Profesion();
    localProfesion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localProfesion.jdoFlags = 1;
    localProfesion.jdoStateManager = paramStateManager;
    return localProfesion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codProfesion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idProfesion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.subgrupoProfesion);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codProfesion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idProfesion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.subgrupoProfesion = ((SubgrupoProfesion)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Profesion paramProfesion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.codProfesion = paramProfesion.codProfesion;
      return;
    case 1:
      if (paramProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.idProfesion = paramProfesion.idProfesion;
      return;
    case 2:
      if (paramProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramProfesion.nombre;
      return;
    case 3:
      if (paramProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.subgrupoProfesion = paramProfesion.subgrupoProfesion;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Profesion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Profesion localProfesion = (Profesion)paramObject;
    if (localProfesion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localProfesion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ProfesionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ProfesionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProfesionPK))
      throw new IllegalArgumentException("arg1");
    ProfesionPK localProfesionPK = (ProfesionPK)paramObject;
    localProfesionPK.idProfesion = this.idProfesion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProfesionPK))
      throw new IllegalArgumentException("arg1");
    ProfesionPK localProfesionPK = (ProfesionPK)paramObject;
    this.idProfesion = localProfesionPK.idProfesion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProfesionPK))
      throw new IllegalArgumentException("arg2");
    ProfesionPK localProfesionPK = (ProfesionPK)paramObject;
    localProfesionPK.idProfesion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProfesionPK))
      throw new IllegalArgumentException("arg2");
    ProfesionPK localProfesionPK = (ProfesionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localProfesionPK.idProfesion);
  }

  private static final String jdoGetcodProfesion(Profesion paramProfesion)
  {
    if (paramProfesion.jdoFlags <= 0)
      return paramProfesion.codProfesion;
    StateManager localStateManager = paramProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramProfesion.codProfesion;
    if (localStateManager.isLoaded(paramProfesion, jdoInheritedFieldCount + 0))
      return paramProfesion.codProfesion;
    return localStateManager.getStringField(paramProfesion, jdoInheritedFieldCount + 0, paramProfesion.codProfesion);
  }

  private static final void jdoSetcodProfesion(Profesion paramProfesion, String paramString)
  {
    if (paramProfesion.jdoFlags == 0)
    {
      paramProfesion.codProfesion = paramString;
      return;
    }
    StateManager localStateManager = paramProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesion.codProfesion = paramString;
      return;
    }
    localStateManager.setStringField(paramProfesion, jdoInheritedFieldCount + 0, paramProfesion.codProfesion, paramString);
  }

  private static final long jdoGetidProfesion(Profesion paramProfesion)
  {
    return paramProfesion.idProfesion;
  }

  private static final void jdoSetidProfesion(Profesion paramProfesion, long paramLong)
  {
    StateManager localStateManager = paramProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesion.idProfesion = paramLong;
      return;
    }
    localStateManager.setLongField(paramProfesion, jdoInheritedFieldCount + 1, paramProfesion.idProfesion, paramLong);
  }

  private static final String jdoGetnombre(Profesion paramProfesion)
  {
    if (paramProfesion.jdoFlags <= 0)
      return paramProfesion.nombre;
    StateManager localStateManager = paramProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramProfesion.nombre;
    if (localStateManager.isLoaded(paramProfesion, jdoInheritedFieldCount + 2))
      return paramProfesion.nombre;
    return localStateManager.getStringField(paramProfesion, jdoInheritedFieldCount + 2, paramProfesion.nombre);
  }

  private static final void jdoSetnombre(Profesion paramProfesion, String paramString)
  {
    if (paramProfesion.jdoFlags == 0)
    {
      paramProfesion.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesion.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramProfesion, jdoInheritedFieldCount + 2, paramProfesion.nombre, paramString);
  }

  private static final SubgrupoProfesion jdoGetsubgrupoProfesion(Profesion paramProfesion)
  {
    StateManager localStateManager = paramProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramProfesion.subgrupoProfesion;
    if (localStateManager.isLoaded(paramProfesion, jdoInheritedFieldCount + 3))
      return paramProfesion.subgrupoProfesion;
    return (SubgrupoProfesion)localStateManager.getObjectField(paramProfesion, jdoInheritedFieldCount + 3, paramProfesion.subgrupoProfesion);
  }

  private static final void jdoSetsubgrupoProfesion(Profesion paramProfesion, SubgrupoProfesion paramSubgrupoProfesion)
  {
    StateManager localStateManager = paramProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesion.subgrupoProfesion = paramSubgrupoProfesion;
      return;
    }
    localStateManager.setObjectField(paramProfesion, jdoInheritedFieldCount + 3, paramProfesion.subgrupoProfesion, paramSubgrupoProfesion);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}