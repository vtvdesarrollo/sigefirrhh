package sigefirrhh.base.personal;

import java.io.Serializable;

public class TituloPK
  implements Serializable
{
  public long idTitulo;

  public TituloPK()
  {
  }

  public TituloPK(long idTitulo)
  {
    this.idTitulo = idTitulo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TituloPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TituloPK thatPK)
  {
    return 
      this.idTitulo == thatPK.idTitulo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTitulo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTitulo);
  }
}