package sigefirrhh.base.personal;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class PersonalBusiness extends AbstractBusiness
  implements Serializable
{
  private AreaCarreraBeanBusiness areaCarreraBeanBusiness = new AreaCarreraBeanBusiness();

  private CarreraBeanBusiness carreraBeanBusiness = new CarreraBeanBusiness();

  private CarreraAreaBeanBusiness carreraAreaBeanBusiness = new CarreraAreaBeanBusiness();

  private GremioBeanBusiness gremioBeanBusiness = new GremioBeanBusiness();

  private GrupoProfesionBeanBusiness grupoProfesionBeanBusiness = new GrupoProfesionBeanBusiness();

  private InstitucionBeanBusiness institucionBeanBusiness = new InstitucionBeanBusiness();

  private NivelEducativoBeanBusiness nivelEducativoBeanBusiness = new NivelEducativoBeanBusiness();

  private ProfesionBeanBusiness profesionBeanBusiness = new ProfesionBeanBusiness();

  private RecaudoBeanBusiness recaudoBeanBusiness = new RecaudoBeanBusiness();

  private RelacionRecaudoBeanBusiness relacionRecaudoBeanBusiness = new RelacionRecaudoBeanBusiness();

  private SubgrupoProfesionBeanBusiness subgrupoProfesionBeanBusiness = new SubgrupoProfesionBeanBusiness();

  private TipoAcreenciaBeanBusiness tipoAcreenciaBeanBusiness = new TipoAcreenciaBeanBusiness();

  private TipoAmonestacionBeanBusiness tipoAmonestacionBeanBusiness = new TipoAmonestacionBeanBusiness();

  private TipoAusenciaBeanBusiness tipoAusenciaBeanBusiness = new TipoAusenciaBeanBusiness();

  private TipoHabilidadBeanBusiness tipoHabilidadBeanBusiness = new TipoHabilidadBeanBusiness();

  private TipoIdiomaBeanBusiness tipoIdiomaBeanBusiness = new TipoIdiomaBeanBusiness();

  private TipoOtraActividadBeanBusiness tipoOtraActividadBeanBusiness = new TipoOtraActividadBeanBusiness();

  private TipoReconocimientoBeanBusiness tipoReconocimientoBeanBusiness = new TipoReconocimientoBeanBusiness();

  private TituloBeanBusiness tituloBeanBusiness = new TituloBeanBusiness();

  public void addAreaCarrera(AreaCarrera areaCarrera)
    throws Exception
  {
    this.areaCarreraBeanBusiness.addAreaCarrera(areaCarrera);
  }

  public void updateAreaCarrera(AreaCarrera areaCarrera) throws Exception {
    this.areaCarreraBeanBusiness.updateAreaCarrera(areaCarrera);
  }

  public void deleteAreaCarrera(AreaCarrera areaCarrera) throws Exception {
    this.areaCarreraBeanBusiness.deleteAreaCarrera(areaCarrera);
  }

  public AreaCarrera findAreaCarreraById(long areaCarreraId) throws Exception {
    return this.areaCarreraBeanBusiness.findAreaCarreraById(areaCarreraId);
  }

  public Collection findAllAreaCarrera() throws Exception {
    return this.areaCarreraBeanBusiness.findAreaCarreraAll();
  }

  public Collection findAreaCarreraByCodAreaCarrera(String codAreaCarrera)
    throws Exception
  {
    return this.areaCarreraBeanBusiness.findByCodAreaCarrera(codAreaCarrera);
  }

  public Collection findAreaCarreraByDescripcion(String descripcion)
    throws Exception
  {
    return this.areaCarreraBeanBusiness.findByDescripcion(descripcion);
  }

  public void addCarrera(Carrera carrera)
    throws Exception
  {
    this.carreraBeanBusiness.addCarrera(carrera);
  }

  public void updateCarrera(Carrera carrera) throws Exception {
    this.carreraBeanBusiness.updateCarrera(carrera);
  }

  public void deleteCarrera(Carrera carrera) throws Exception {
    this.carreraBeanBusiness.deleteCarrera(carrera);
  }

  public Carrera findCarreraById(long carreraId) throws Exception {
    return this.carreraBeanBusiness.findCarreraById(carreraId);
  }

  public Collection findAllCarrera() throws Exception {
    return this.carreraBeanBusiness.findCarreraAll();
  }

  public Collection findCarreraByCodCarrera(String codCarrera)
    throws Exception
  {
    return this.carreraBeanBusiness.findByCodCarrera(codCarrera);
  }

  public Collection findCarreraByNombre(String nombre)
    throws Exception
  {
    return this.carreraBeanBusiness.findByNombre(nombre);
  }

  public void addCarreraArea(CarreraArea carreraArea)
    throws Exception
  {
    this.carreraAreaBeanBusiness.addCarreraArea(carreraArea);
  }

  public void updateCarreraArea(CarreraArea carreraArea) throws Exception {
    this.carreraAreaBeanBusiness.updateCarreraArea(carreraArea);
  }

  public void deleteCarreraArea(CarreraArea carreraArea) throws Exception {
    this.carreraAreaBeanBusiness.deleteCarreraArea(carreraArea);
  }

  public CarreraArea findCarreraAreaById(long carreraAreaId) throws Exception {
    return this.carreraAreaBeanBusiness.findCarreraAreaById(carreraAreaId);
  }

  public Collection findAllCarreraArea() throws Exception {
    return this.carreraAreaBeanBusiness.findCarreraAreaAll();
  }

  public Collection findCarreraAreaByAreaCarrera(long idAreaCarrera)
    throws Exception
  {
    return this.carreraAreaBeanBusiness.findByAreaCarrera(idAreaCarrera);
  }

  public Collection findCarreraAreaByCarrera(long idCarrera)
    throws Exception
  {
    return this.carreraAreaBeanBusiness.findByCarrera(idCarrera);
  }

  public void addGremio(Gremio gremio)
    throws Exception
  {
    this.gremioBeanBusiness.addGremio(gremio);
  }

  public void updateGremio(Gremio gremio) throws Exception {
    this.gremioBeanBusiness.updateGremio(gremio);
  }

  public void deleteGremio(Gremio gremio) throws Exception {
    this.gremioBeanBusiness.deleteGremio(gremio);
  }

  public Gremio findGremioById(long gremioId) throws Exception {
    return this.gremioBeanBusiness.findGremioById(gremioId);
  }

  public Collection findAllGremio() throws Exception {
    return this.gremioBeanBusiness.findGremioAll();
  }

  public Collection findGremioByCodGremio(String codGremio)
    throws Exception
  {
    return this.gremioBeanBusiness.findByCodGremio(codGremio);
  }

  public Collection findGremioByNombre(String nombre)
    throws Exception
  {
    return this.gremioBeanBusiness.findByNombre(nombre);
  }

  public void addGrupoProfesion(GrupoProfesion grupoProfesion)
    throws Exception
  {
    this.grupoProfesionBeanBusiness.addGrupoProfesion(grupoProfesion);
  }

  public void updateGrupoProfesion(GrupoProfesion grupoProfesion) throws Exception {
    this.grupoProfesionBeanBusiness.updateGrupoProfesion(grupoProfesion);
  }

  public void deleteGrupoProfesion(GrupoProfesion grupoProfesion) throws Exception {
    this.grupoProfesionBeanBusiness.deleteGrupoProfesion(grupoProfesion);
  }

  public GrupoProfesion findGrupoProfesionById(long grupoProfesionId) throws Exception {
    return this.grupoProfesionBeanBusiness.findGrupoProfesionById(grupoProfesionId);
  }

  public Collection findAllGrupoProfesion() throws Exception {
    return this.grupoProfesionBeanBusiness.findGrupoProfesionAll();
  }

  public Collection findGrupoProfesionByCodGrupoProfesion(String codGrupoProfesion)
    throws Exception
  {
    return this.grupoProfesionBeanBusiness.findByCodGrupoProfesion(codGrupoProfesion);
  }

  public Collection findGrupoProfesionByNombre(String nombre)
    throws Exception
  {
    return this.grupoProfesionBeanBusiness.findByNombre(nombre);
  }

  public void addInstitucion(Institucion institucion)
    throws Exception
  {
    this.institucionBeanBusiness.addInstitucion(institucion);
  }

  public void updateInstitucion(Institucion institucion) throws Exception {
    this.institucionBeanBusiness.updateInstitucion(institucion);
  }

  public void deleteInstitucion(Institucion institucion) throws Exception {
    this.institucionBeanBusiness.deleteInstitucion(institucion);
  }

  public Institucion findInstitucionById(long institucionId) throws Exception {
    return this.institucionBeanBusiness.findInstitucionById(institucionId);
  }

  public Collection findAllInstitucion() throws Exception {
    return this.institucionBeanBusiness.findInstitucionAll();
  }

  public Collection findInstitucionByCodInstitucion(String codInstitucion)
    throws Exception
  {
    return this.institucionBeanBusiness.findByCodInstitucion(codInstitucion);
  }

  public Collection findInstitucionByNombre(String nombre)
    throws Exception
  {
    return this.institucionBeanBusiness.findByNombre(nombre);
  }

  public void addNivelEducativo(NivelEducativo nivelEducativo)
    throws Exception
  {
    this.nivelEducativoBeanBusiness.addNivelEducativo(nivelEducativo);
  }

  public void updateNivelEducativo(NivelEducativo nivelEducativo) throws Exception {
    this.nivelEducativoBeanBusiness.updateNivelEducativo(nivelEducativo);
  }

  public void deleteNivelEducativo(NivelEducativo nivelEducativo) throws Exception {
    this.nivelEducativoBeanBusiness.deleteNivelEducativo(nivelEducativo);
  }

  public NivelEducativo findNivelEducativoById(long nivelEducativoId) throws Exception {
    return this.nivelEducativoBeanBusiness.findNivelEducativoById(nivelEducativoId);
  }

  public Collection findAllNivelEducativo() throws Exception {
    return this.nivelEducativoBeanBusiness.findNivelEducativoAll();
  }

  public Collection findNivelEducativoByCodNivelEducativo(String codNivelEducativo)
    throws Exception
  {
    return this.nivelEducativoBeanBusiness.findByCodNivelEducativo(codNivelEducativo);
  }

  public Collection findNivelEducativoByDescripcion(String descripcion)
    throws Exception
  {
    return this.nivelEducativoBeanBusiness.findByDescripcion(descripcion);
  }

  public void addProfesion(Profesion profesion)
    throws Exception
  {
    this.profesionBeanBusiness.addProfesion(profesion);
  }

  public void updateProfesion(Profesion profesion) throws Exception {
    this.profesionBeanBusiness.updateProfesion(profesion);
  }

  public void deleteProfesion(Profesion profesion) throws Exception {
    this.profesionBeanBusiness.deleteProfesion(profesion);
  }

  public Profesion findProfesionById(long profesionId) throws Exception {
    return this.profesionBeanBusiness.findProfesionById(profesionId);
  }

  public Collection findAllProfesion() throws Exception {
    return this.profesionBeanBusiness.findProfesionAll();
  }

  public Collection findProfesionBySubgrupoProfesion(long idSubgrupoProfesion)
    throws Exception
  {
    return this.profesionBeanBusiness.findBySubgrupoProfesion(idSubgrupoProfesion);
  }

  public Collection findProfesionByCodProfesion(String codProfesion)
    throws Exception
  {
    return this.profesionBeanBusiness.findByCodProfesion(codProfesion);
  }

  public Collection findProfesionByNombre(String nombre)
    throws Exception
  {
    return this.profesionBeanBusiness.findByNombre(nombre);
  }

  public void addRecaudo(Recaudo recaudo)
    throws Exception
  {
    this.recaudoBeanBusiness.addRecaudo(recaudo);
  }

  public void updateRecaudo(Recaudo recaudo) throws Exception {
    this.recaudoBeanBusiness.updateRecaudo(recaudo);
  }

  public void deleteRecaudo(Recaudo recaudo) throws Exception {
    this.recaudoBeanBusiness.deleteRecaudo(recaudo);
  }

  public Recaudo findRecaudoById(long recaudoId) throws Exception {
    return this.recaudoBeanBusiness.findRecaudoById(recaudoId);
  }

  public Collection findAllRecaudo() throws Exception {
    return this.recaudoBeanBusiness.findRecaudoAll();
  }

  public Collection findRecaudoByCodRecaudo(int codRecaudo)
    throws Exception
  {
    return this.recaudoBeanBusiness.findByCodRecaudo(codRecaudo);
  }

  public Collection findRecaudoByDescripcion(String descripcion)
    throws Exception
  {
    return this.recaudoBeanBusiness.findByDescripcion(descripcion);
  }

  public void addRelacionRecaudo(RelacionRecaudo relacionRecaudo)
    throws Exception
  {
    this.relacionRecaudoBeanBusiness.addRelacionRecaudo(relacionRecaudo);
  }

  public void updateRelacionRecaudo(RelacionRecaudo relacionRecaudo) throws Exception {
    this.relacionRecaudoBeanBusiness.updateRelacionRecaudo(relacionRecaudo);
  }

  public void deleteRelacionRecaudo(RelacionRecaudo relacionRecaudo) throws Exception {
    this.relacionRecaudoBeanBusiness.deleteRelacionRecaudo(relacionRecaudo);
  }

  public RelacionRecaudo findRelacionRecaudoById(long relacionRecaudoId) throws Exception {
    return this.relacionRecaudoBeanBusiness.findRelacionRecaudoById(relacionRecaudoId);
  }

  public Collection findAllRelacionRecaudo() throws Exception {
    return this.relacionRecaudoBeanBusiness.findRelacionRecaudoAll();
  }

  public Collection findRelacionRecaudoByEvento(int evento)
    throws Exception
  {
    return this.relacionRecaudoBeanBusiness.findByEvento(evento);
  }

  public void addSubgrupoProfesion(SubgrupoProfesion subgrupoProfesion)
    throws Exception
  {
    this.subgrupoProfesionBeanBusiness.addSubgrupoProfesion(subgrupoProfesion);
  }

  public void updateSubgrupoProfesion(SubgrupoProfesion subgrupoProfesion) throws Exception {
    this.subgrupoProfesionBeanBusiness.updateSubgrupoProfesion(subgrupoProfesion);
  }

  public void deleteSubgrupoProfesion(SubgrupoProfesion subgrupoProfesion) throws Exception {
    this.subgrupoProfesionBeanBusiness.deleteSubgrupoProfesion(subgrupoProfesion);
  }

  public SubgrupoProfesion findSubgrupoProfesionById(long subgrupoProfesionId) throws Exception {
    return this.subgrupoProfesionBeanBusiness.findSubgrupoProfesionById(subgrupoProfesionId);
  }

  public Collection findAllSubgrupoProfesion() throws Exception {
    return this.subgrupoProfesionBeanBusiness.findSubgrupoProfesionAll();
  }

  public Collection findSubgrupoProfesionByGrupoProfesion(long idGrupoProfesion)
    throws Exception
  {
    return this.subgrupoProfesionBeanBusiness.findByGrupoProfesion(idGrupoProfesion);
  }

  public Collection findSubgrupoProfesionByCodSubgrupoProfesion(String codSubgrupoProfesion)
    throws Exception
  {
    return this.subgrupoProfesionBeanBusiness.findByCodSubgrupoProfesion(codSubgrupoProfesion);
  }

  public Collection findSubgrupoProfesionByNombre(String nombre)
    throws Exception
  {
    return this.subgrupoProfesionBeanBusiness.findByNombre(nombre);
  }

  public void addTipoAcreencia(TipoAcreencia tipoAcreencia)
    throws Exception
  {
    this.tipoAcreenciaBeanBusiness.addTipoAcreencia(tipoAcreencia);
  }

  public void updateTipoAcreencia(TipoAcreencia tipoAcreencia) throws Exception {
    this.tipoAcreenciaBeanBusiness.updateTipoAcreencia(tipoAcreencia);
  }

  public void deleteTipoAcreencia(TipoAcreencia tipoAcreencia) throws Exception {
    this.tipoAcreenciaBeanBusiness.deleteTipoAcreencia(tipoAcreencia);
  }

  public TipoAcreencia findTipoAcreenciaById(long tipoAcreenciaId) throws Exception {
    return this.tipoAcreenciaBeanBusiness.findTipoAcreenciaById(tipoAcreenciaId);
  }

  public Collection findAllTipoAcreencia() throws Exception {
    return this.tipoAcreenciaBeanBusiness.findTipoAcreenciaAll();
  }

  public Collection findTipoAcreenciaByCodTipoAcreencia(String codTipoAcreencia)
    throws Exception
  {
    return this.tipoAcreenciaBeanBusiness.findByCodTipoAcreencia(codTipoAcreencia);
  }

  public Collection findTipoAcreenciaByDescripcion(String descripcion)
    throws Exception
  {
    return this.tipoAcreenciaBeanBusiness.findByDescripcion(descripcion);
  }

  public void addTipoAmonestacion(TipoAmonestacion tipoAmonestacion)
    throws Exception
  {
    this.tipoAmonestacionBeanBusiness.addTipoAmonestacion(tipoAmonestacion);
  }

  public void updateTipoAmonestacion(TipoAmonestacion tipoAmonestacion) throws Exception {
    this.tipoAmonestacionBeanBusiness.updateTipoAmonestacion(tipoAmonestacion);
  }

  public void deleteTipoAmonestacion(TipoAmonestacion tipoAmonestacion) throws Exception {
    this.tipoAmonestacionBeanBusiness.deleteTipoAmonestacion(tipoAmonestacion);
  }

  public TipoAmonestacion findTipoAmonestacionById(long tipoAmonestacionId) throws Exception {
    return this.tipoAmonestacionBeanBusiness.findTipoAmonestacionById(tipoAmonestacionId);
  }

  public Collection findAllTipoAmonestacion() throws Exception {
    return this.tipoAmonestacionBeanBusiness.findTipoAmonestacionAll();
  }

  public Collection findTipoAmonestacionByCodTipoAmonestacion(String codTipoAmonestacion)
    throws Exception
  {
    return this.tipoAmonestacionBeanBusiness.findByCodTipoAmonestacion(codTipoAmonestacion);
  }

  public Collection findTipoAmonestacionByDescripcion(String descripcion)
    throws Exception
  {
    return this.tipoAmonestacionBeanBusiness.findByDescripcion(descripcion);
  }

  public void addTipoAusencia(TipoAusencia tipoAusencia)
    throws Exception
  {
    this.tipoAusenciaBeanBusiness.addTipoAusencia(tipoAusencia);
  }

  public void updateTipoAusencia(TipoAusencia tipoAusencia) throws Exception {
    this.tipoAusenciaBeanBusiness.updateTipoAusencia(tipoAusencia);
  }

  public void deleteTipoAusencia(TipoAusencia tipoAusencia) throws Exception {
    this.tipoAusenciaBeanBusiness.deleteTipoAusencia(tipoAusencia);
  }

  public TipoAusencia findTipoAusenciaById(long tipoAusenciaId) throws Exception {
    return this.tipoAusenciaBeanBusiness.findTipoAusenciaById(tipoAusenciaId);
  }

  public Collection findAllTipoAusencia() throws Exception {
    return this.tipoAusenciaBeanBusiness.findTipoAusenciaAll();
  }

  public Collection findTipoAusenciaByCodTipoAusencia(String codTipoAusencia)
    throws Exception
  {
    return this.tipoAusenciaBeanBusiness.findByCodTipoAusencia(codTipoAusencia);
  }

  public Collection findTipoAusenciaByDescripcion(String descripcion)
    throws Exception
  {
    return this.tipoAusenciaBeanBusiness.findByDescripcion(descripcion);
  }

  public void addTipoHabilidad(TipoHabilidad tipoHabilidad)
    throws Exception
  {
    this.tipoHabilidadBeanBusiness.addTipoHabilidad(tipoHabilidad);
  }

  public void updateTipoHabilidad(TipoHabilidad tipoHabilidad) throws Exception {
    this.tipoHabilidadBeanBusiness.updateTipoHabilidad(tipoHabilidad);
  }

  public void deleteTipoHabilidad(TipoHabilidad tipoHabilidad) throws Exception {
    this.tipoHabilidadBeanBusiness.deleteTipoHabilidad(tipoHabilidad);
  }

  public TipoHabilidad findTipoHabilidadById(long tipoHabilidadId) throws Exception {
    return this.tipoHabilidadBeanBusiness.findTipoHabilidadById(tipoHabilidadId);
  }

  public Collection findAllTipoHabilidad() throws Exception {
    return this.tipoHabilidadBeanBusiness.findTipoHabilidadAll();
  }

  public Collection findTipoHabilidadByCodTipoHabilidad(String codTipoHabilidad)
    throws Exception
  {
    return this.tipoHabilidadBeanBusiness.findByCodTipoHabilidad(codTipoHabilidad);
  }

  public Collection findTipoHabilidadByDescripcion(String descripcion)
    throws Exception
  {
    return this.tipoHabilidadBeanBusiness.findByDescripcion(descripcion);
  }

  public void addTipoIdioma(TipoIdioma tipoIdioma)
    throws Exception
  {
    this.tipoIdiomaBeanBusiness.addTipoIdioma(tipoIdioma);
  }

  public void updateTipoIdioma(TipoIdioma tipoIdioma) throws Exception {
    this.tipoIdiomaBeanBusiness.updateTipoIdioma(tipoIdioma);
  }

  public void deleteTipoIdioma(TipoIdioma tipoIdioma) throws Exception {
    this.tipoIdiomaBeanBusiness.deleteTipoIdioma(tipoIdioma);
  }

  public TipoIdioma findTipoIdiomaById(long tipoIdiomaId) throws Exception {
    return this.tipoIdiomaBeanBusiness.findTipoIdiomaById(tipoIdiomaId);
  }

  public Collection findAllTipoIdioma() throws Exception {
    return this.tipoIdiomaBeanBusiness.findTipoIdiomaAll();
  }

  public Collection findTipoIdiomaByCodTipoIdioma(String codTipoIdioma)
    throws Exception
  {
    return this.tipoIdiomaBeanBusiness.findByCodTipoIdioma(codTipoIdioma);
  }

  public Collection findTipoIdiomaByDescripcion(String descripcion)
    throws Exception
  {
    return this.tipoIdiomaBeanBusiness.findByDescripcion(descripcion);
  }

  public void addTipoOtraActividad(TipoOtraActividad tipoOtraActividad)
    throws Exception
  {
    this.tipoOtraActividadBeanBusiness.addTipoOtraActividad(tipoOtraActividad);
  }

  public void updateTipoOtraActividad(TipoOtraActividad tipoOtraActividad) throws Exception {
    this.tipoOtraActividadBeanBusiness.updateTipoOtraActividad(tipoOtraActividad);
  }

  public void deleteTipoOtraActividad(TipoOtraActividad tipoOtraActividad) throws Exception {
    this.tipoOtraActividadBeanBusiness.deleteTipoOtraActividad(tipoOtraActividad);
  }

  public TipoOtraActividad findTipoOtraActividadById(long tipoOtraActividadId) throws Exception {
    return this.tipoOtraActividadBeanBusiness.findTipoOtraActividadById(tipoOtraActividadId);
  }

  public Collection findAllTipoOtraActividad() throws Exception {
    return this.tipoOtraActividadBeanBusiness.findTipoOtraActividadAll();
  }

  public Collection findTipoOtraActividadByCodOtraActividad(String codOtraActividad)
    throws Exception
  {
    return this.tipoOtraActividadBeanBusiness.findByCodOtraActividad(codOtraActividad);
  }

  public Collection findTipoOtraActividadByDescripcion(String descripcion)
    throws Exception
  {
    return this.tipoOtraActividadBeanBusiness.findByDescripcion(descripcion);
  }

  public void addTipoReconocimiento(TipoReconocimiento tipoReconocimiento)
    throws Exception
  {
    this.tipoReconocimientoBeanBusiness.addTipoReconocimiento(tipoReconocimiento);
  }

  public void updateTipoReconocimiento(TipoReconocimiento tipoReconocimiento) throws Exception {
    this.tipoReconocimientoBeanBusiness.updateTipoReconocimiento(tipoReconocimiento);
  }

  public void deleteTipoReconocimiento(TipoReconocimiento tipoReconocimiento) throws Exception {
    this.tipoReconocimientoBeanBusiness.deleteTipoReconocimiento(tipoReconocimiento);
  }

  public TipoReconocimiento findTipoReconocimientoById(long tipoReconocimientoId) throws Exception {
    return this.tipoReconocimientoBeanBusiness.findTipoReconocimientoById(tipoReconocimientoId);
  }

  public Collection findAllTipoReconocimiento() throws Exception {
    return this.tipoReconocimientoBeanBusiness.findTipoReconocimientoAll();
  }

  public Collection findTipoReconocimientoByCodTipoReconocimiento(String codTipoReconocimiento)
    throws Exception
  {
    return this.tipoReconocimientoBeanBusiness.findByCodTipoReconocimiento(codTipoReconocimiento);
  }

  public Collection findTipoReconocimientoByDescripcion(String descripcion)
    throws Exception
  {
    return this.tipoReconocimientoBeanBusiness.findByDescripcion(descripcion);
  }

  public void addTitulo(Titulo titulo)
    throws Exception
  {
    this.tituloBeanBusiness.addTitulo(titulo);
  }

  public void updateTitulo(Titulo titulo) throws Exception {
    this.tituloBeanBusiness.updateTitulo(titulo);
  }

  public void deleteTitulo(Titulo titulo) throws Exception {
    this.tituloBeanBusiness.deleteTitulo(titulo);
  }

  public Titulo findTituloById(long tituloId) throws Exception {
    return this.tituloBeanBusiness.findTituloById(tituloId);
  }

  public Collection findAllTitulo() throws Exception {
    return this.tituloBeanBusiness.findTituloAll();
  }

  public Collection findTituloByCodTitulo(String codTitulo)
    throws Exception
  {
    return this.tituloBeanBusiness.findByCodTitulo(codTitulo);
  }

  public Collection findTituloByDescripcion(String descripcion)
    throws Exception
  {
    return this.tituloBeanBusiness.findByDescripcion(descripcion);
  }

  public Collection findTituloByNivelEducativo(long idNivelEducativo)
    throws Exception
  {
    return this.tituloBeanBusiness.findByNivelEducativo(idNivelEducativo);
  }

  public Collection findTituloByGrupoProfesion(long idGrupoProfesion)
    throws Exception
  {
    return this.tituloBeanBusiness.findByGrupoProfesion(idGrupoProfesion);
  }
}