package sigefirrhh.base.personal;

import java.io.Serializable;

public class SubgrupoProfesionPK
  implements Serializable
{
  public long idSubgrupoProfesion;

  public SubgrupoProfesionPK()
  {
  }

  public SubgrupoProfesionPK(long idSubgrupoProfesion)
  {
    this.idSubgrupoProfesion = idSubgrupoProfesion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SubgrupoProfesionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SubgrupoProfesionPK thatPK)
  {
    return 
      this.idSubgrupoProfesion == thatPK.idSubgrupoProfesion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSubgrupoProfesion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSubgrupoProfesion);
  }
}