package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoHabilidadBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoHabilidad(TipoHabilidad tipoHabilidad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoHabilidad tipoHabilidadNew = 
      (TipoHabilidad)BeanUtils.cloneBean(
      tipoHabilidad);

    pm.makePersistent(tipoHabilidadNew);
  }

  public void updateTipoHabilidad(TipoHabilidad tipoHabilidad) throws Exception
  {
    TipoHabilidad tipoHabilidadModify = 
      findTipoHabilidadById(tipoHabilidad.getIdTipoHabilidad());

    BeanUtils.copyProperties(tipoHabilidadModify, tipoHabilidad);
  }

  public void deleteTipoHabilidad(TipoHabilidad tipoHabilidad) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoHabilidad tipoHabilidadDelete = 
      findTipoHabilidadById(tipoHabilidad.getIdTipoHabilidad());
    pm.deletePersistent(tipoHabilidadDelete);
  }

  public TipoHabilidad findTipoHabilidadById(long idTipoHabilidad) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoHabilidad == pIdTipoHabilidad";
    Query query = pm.newQuery(TipoHabilidad.class, filter);

    query.declareParameters("long pIdTipoHabilidad");

    parameters.put("pIdTipoHabilidad", new Long(idTipoHabilidad));

    Collection colTipoHabilidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoHabilidad.iterator();
    return (TipoHabilidad)iterator.next();
  }

  public Collection findTipoHabilidadAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoHabilidadExtent = pm.getExtent(
      TipoHabilidad.class, true);
    Query query = pm.newQuery(tipoHabilidadExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoHabilidad(String codTipoHabilidad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoHabilidad == pCodTipoHabilidad";

    Query query = pm.newQuery(TipoHabilidad.class, filter);

    query.declareParameters("java.lang.String pCodTipoHabilidad");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoHabilidad", new String(codTipoHabilidad));

    query.setOrdering("descripcion ascending");

    Collection colTipoHabilidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoHabilidad);

    return colTipoHabilidad;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(TipoHabilidad.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTipoHabilidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoHabilidad);

    return colTipoHabilidad;
  }
}