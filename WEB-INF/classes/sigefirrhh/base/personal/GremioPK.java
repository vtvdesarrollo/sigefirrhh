package sigefirrhh.base.personal;

import java.io.Serializable;

public class GremioPK
  implements Serializable
{
  public long idGremio;

  public GremioPK()
  {
  }

  public GremioPK(long idGremio)
  {
    this.idGremio = idGremio;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((GremioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(GremioPK thatPK)
  {
    return 
      this.idGremio == thatPK.idGremio;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idGremio)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idGremio);
  }
}