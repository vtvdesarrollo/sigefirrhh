package sigefirrhh.base.personal;

import java.io.Serializable;

public class TipoAmonestacionPK
  implements Serializable
{
  public long idTipoAmonestacion;

  public TipoAmonestacionPK()
  {
  }

  public TipoAmonestacionPK(long idTipoAmonestacion)
  {
    this.idTipoAmonestacion = idTipoAmonestacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoAmonestacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoAmonestacionPK thatPK)
  {
    return 
      this.idTipoAmonestacion == thatPK.idTipoAmonestacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoAmonestacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoAmonestacion);
  }
}