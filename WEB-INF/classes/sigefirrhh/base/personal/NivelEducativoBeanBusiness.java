package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class NivelEducativoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addNivelEducativo(NivelEducativo nivelEducativo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    NivelEducativo nivelEducativoNew = 
      (NivelEducativo)BeanUtils.cloneBean(
      nivelEducativo);

    pm.makePersistent(nivelEducativoNew);
  }

  public void updateNivelEducativo(NivelEducativo nivelEducativo) throws Exception
  {
    NivelEducativo nivelEducativoModify = 
      findNivelEducativoById(nivelEducativo.getIdNivelEducativo());

    BeanUtils.copyProperties(nivelEducativoModify, nivelEducativo);
  }

  public void deleteNivelEducativo(NivelEducativo nivelEducativo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    NivelEducativo nivelEducativoDelete = 
      findNivelEducativoById(nivelEducativo.getIdNivelEducativo());
    pm.deletePersistent(nivelEducativoDelete);
  }

  public NivelEducativo findNivelEducativoById(long idNivelEducativo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idNivelEducativo == pIdNivelEducativo";
    Query query = pm.newQuery(NivelEducativo.class, filter);

    query.declareParameters("long pIdNivelEducativo");

    parameters.put("pIdNivelEducativo", new Long(idNivelEducativo));

    Collection colNivelEducativo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colNivelEducativo.iterator();
    return (NivelEducativo)iterator.next();
  }

  public Collection findNivelEducativoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent nivelEducativoExtent = pm.getExtent(
      NivelEducativo.class, true);
    Query query = pm.newQuery(nivelEducativoExtent);
    query.setOrdering("orden ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodNivelEducativo(String codNivelEducativo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codNivelEducativo == pCodNivelEducativo";

    Query query = pm.newQuery(NivelEducativo.class, filter);

    query.declareParameters("java.lang.String pCodNivelEducativo");
    HashMap parameters = new HashMap();

    parameters.put("pCodNivelEducativo", new String(codNivelEducativo));

    query.setOrdering("orden ascending");

    Collection colNivelEducativo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNivelEducativo);

    return colNivelEducativo;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(NivelEducativo.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("orden ascending");

    Collection colNivelEducativo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNivelEducativo);

    return colNivelEducativo;
  }
}