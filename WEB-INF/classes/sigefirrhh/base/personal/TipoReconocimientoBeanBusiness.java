package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoReconocimientoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoReconocimiento(TipoReconocimiento tipoReconocimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoReconocimiento tipoReconocimientoNew = 
      (TipoReconocimiento)BeanUtils.cloneBean(
      tipoReconocimiento);

    pm.makePersistent(tipoReconocimientoNew);
  }

  public void updateTipoReconocimiento(TipoReconocimiento tipoReconocimiento) throws Exception
  {
    TipoReconocimiento tipoReconocimientoModify = 
      findTipoReconocimientoById(tipoReconocimiento.getIdTipoReconocimiento());

    BeanUtils.copyProperties(tipoReconocimientoModify, tipoReconocimiento);
  }

  public void deleteTipoReconocimiento(TipoReconocimiento tipoReconocimiento) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoReconocimiento tipoReconocimientoDelete = 
      findTipoReconocimientoById(tipoReconocimiento.getIdTipoReconocimiento());
    pm.deletePersistent(tipoReconocimientoDelete);
  }

  public TipoReconocimiento findTipoReconocimientoById(long idTipoReconocimiento) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoReconocimiento == pIdTipoReconocimiento";
    Query query = pm.newQuery(TipoReconocimiento.class, filter);

    query.declareParameters("long pIdTipoReconocimiento");

    parameters.put("pIdTipoReconocimiento", new Long(idTipoReconocimiento));

    Collection colTipoReconocimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoReconocimiento.iterator();
    return (TipoReconocimiento)iterator.next();
  }

  public Collection findTipoReconocimientoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoReconocimientoExtent = pm.getExtent(
      TipoReconocimiento.class, true);
    Query query = pm.newQuery(tipoReconocimientoExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoReconocimiento(String codTipoReconocimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoReconocimiento == pCodTipoReconocimiento";

    Query query = pm.newQuery(TipoReconocimiento.class, filter);

    query.declareParameters("java.lang.String pCodTipoReconocimiento");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoReconocimiento", new String(codTipoReconocimiento));

    query.setOrdering("descripcion ascending");

    Collection colTipoReconocimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoReconocimiento);

    return colTipoReconocimiento;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(TipoReconocimiento.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTipoReconocimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoReconocimiento);

    return colTipoReconocimiento;
  }
}