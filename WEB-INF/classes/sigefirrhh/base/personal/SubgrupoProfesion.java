package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class SubgrupoProfesion
  implements Serializable, PersistenceCapable
{
  private long idSubgrupoProfesion;
  private GrupoProfesion grupoProfesion;
  private String codSubgrupoProfesion;
  private String nombre;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codSubgrupoProfesion", "descripcion", "grupoProfesion", "idSubgrupoProfesion", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.personal.GrupoProfesion"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 26, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodSubgrupoProfesion(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public GrupoProfesion getGrupoProfesion()
  {
    return jdoGetgrupoProfesion(this);
  }

  public long getIdSubgrupoProfesion()
  {
    return jdoGetidSubgrupoProfesion(this);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setGrupoProfesion(GrupoProfesion profesion)
  {
    jdoSetgrupoProfesion(this, profesion);
  }

  public void setIdSubgrupoProfesion(long l)
  {
    jdoSetidSubgrupoProfesion(this, l);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public String getCodSubgrupoProfesion()
  {
    return jdoGetcodSubgrupoProfesion(this);
  }

  public void setCodSubgrupoProfesion(String string)
  {
    jdoSetcodSubgrupoProfesion(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.SubgrupoProfesion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SubgrupoProfesion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SubgrupoProfesion localSubgrupoProfesion = new SubgrupoProfesion();
    localSubgrupoProfesion.jdoFlags = 1;
    localSubgrupoProfesion.jdoStateManager = paramStateManager;
    return localSubgrupoProfesion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SubgrupoProfesion localSubgrupoProfesion = new SubgrupoProfesion();
    localSubgrupoProfesion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSubgrupoProfesion.jdoFlags = 1;
    localSubgrupoProfesion.jdoStateManager = paramStateManager;
    return localSubgrupoProfesion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSubgrupoProfesion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoProfesion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSubgrupoProfesion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSubgrupoProfesion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoProfesion = ((GrupoProfesion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSubgrupoProfesion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SubgrupoProfesion paramSubgrupoProfesion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSubgrupoProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.codSubgrupoProfesion = paramSubgrupoProfesion.codSubgrupoProfesion;
      return;
    case 1:
      if (paramSubgrupoProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramSubgrupoProfesion.descripcion;
      return;
    case 2:
      if (paramSubgrupoProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.grupoProfesion = paramSubgrupoProfesion.grupoProfesion;
      return;
    case 3:
      if (paramSubgrupoProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.idSubgrupoProfesion = paramSubgrupoProfesion.idSubgrupoProfesion;
      return;
    case 4:
      if (paramSubgrupoProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramSubgrupoProfesion.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SubgrupoProfesion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SubgrupoProfesion localSubgrupoProfesion = (SubgrupoProfesion)paramObject;
    if (localSubgrupoProfesion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSubgrupoProfesion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SubgrupoProfesionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SubgrupoProfesionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SubgrupoProfesionPK))
      throw new IllegalArgumentException("arg1");
    SubgrupoProfesionPK localSubgrupoProfesionPK = (SubgrupoProfesionPK)paramObject;
    localSubgrupoProfesionPK.idSubgrupoProfesion = this.idSubgrupoProfesion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SubgrupoProfesionPK))
      throw new IllegalArgumentException("arg1");
    SubgrupoProfesionPK localSubgrupoProfesionPK = (SubgrupoProfesionPK)paramObject;
    this.idSubgrupoProfesion = localSubgrupoProfesionPK.idSubgrupoProfesion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SubgrupoProfesionPK))
      throw new IllegalArgumentException("arg2");
    SubgrupoProfesionPK localSubgrupoProfesionPK = (SubgrupoProfesionPK)paramObject;
    localSubgrupoProfesionPK.idSubgrupoProfesion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SubgrupoProfesionPK))
      throw new IllegalArgumentException("arg2");
    SubgrupoProfesionPK localSubgrupoProfesionPK = (SubgrupoProfesionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localSubgrupoProfesionPK.idSubgrupoProfesion);
  }

  private static final String jdoGetcodSubgrupoProfesion(SubgrupoProfesion paramSubgrupoProfesion)
  {
    if (paramSubgrupoProfesion.jdoFlags <= 0)
      return paramSubgrupoProfesion.codSubgrupoProfesion;
    StateManager localStateManager = paramSubgrupoProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramSubgrupoProfesion.codSubgrupoProfesion;
    if (localStateManager.isLoaded(paramSubgrupoProfesion, jdoInheritedFieldCount + 0))
      return paramSubgrupoProfesion.codSubgrupoProfesion;
    return localStateManager.getStringField(paramSubgrupoProfesion, jdoInheritedFieldCount + 0, paramSubgrupoProfesion.codSubgrupoProfesion);
  }

  private static final void jdoSetcodSubgrupoProfesion(SubgrupoProfesion paramSubgrupoProfesion, String paramString)
  {
    if (paramSubgrupoProfesion.jdoFlags == 0)
    {
      paramSubgrupoProfesion.codSubgrupoProfesion = paramString;
      return;
    }
    StateManager localStateManager = paramSubgrupoProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubgrupoProfesion.codSubgrupoProfesion = paramString;
      return;
    }
    localStateManager.setStringField(paramSubgrupoProfesion, jdoInheritedFieldCount + 0, paramSubgrupoProfesion.codSubgrupoProfesion, paramString);
  }

  private static final String jdoGetdescripcion(SubgrupoProfesion paramSubgrupoProfesion)
  {
    if (paramSubgrupoProfesion.jdoFlags <= 0)
      return paramSubgrupoProfesion.descripcion;
    StateManager localStateManager = paramSubgrupoProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramSubgrupoProfesion.descripcion;
    if (localStateManager.isLoaded(paramSubgrupoProfesion, jdoInheritedFieldCount + 1))
      return paramSubgrupoProfesion.descripcion;
    return localStateManager.getStringField(paramSubgrupoProfesion, jdoInheritedFieldCount + 1, paramSubgrupoProfesion.descripcion);
  }

  private static final void jdoSetdescripcion(SubgrupoProfesion paramSubgrupoProfesion, String paramString)
  {
    if (paramSubgrupoProfesion.jdoFlags == 0)
    {
      paramSubgrupoProfesion.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramSubgrupoProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubgrupoProfesion.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramSubgrupoProfesion, jdoInheritedFieldCount + 1, paramSubgrupoProfesion.descripcion, paramString);
  }

  private static final GrupoProfesion jdoGetgrupoProfesion(SubgrupoProfesion paramSubgrupoProfesion)
  {
    StateManager localStateManager = paramSubgrupoProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramSubgrupoProfesion.grupoProfesion;
    if (localStateManager.isLoaded(paramSubgrupoProfesion, jdoInheritedFieldCount + 2))
      return paramSubgrupoProfesion.grupoProfesion;
    return (GrupoProfesion)localStateManager.getObjectField(paramSubgrupoProfesion, jdoInheritedFieldCount + 2, paramSubgrupoProfesion.grupoProfesion);
  }

  private static final void jdoSetgrupoProfesion(SubgrupoProfesion paramSubgrupoProfesion, GrupoProfesion paramGrupoProfesion)
  {
    StateManager localStateManager = paramSubgrupoProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubgrupoProfesion.grupoProfesion = paramGrupoProfesion;
      return;
    }
    localStateManager.setObjectField(paramSubgrupoProfesion, jdoInheritedFieldCount + 2, paramSubgrupoProfesion.grupoProfesion, paramGrupoProfesion);
  }

  private static final long jdoGetidSubgrupoProfesion(SubgrupoProfesion paramSubgrupoProfesion)
  {
    return paramSubgrupoProfesion.idSubgrupoProfesion;
  }

  private static final void jdoSetidSubgrupoProfesion(SubgrupoProfesion paramSubgrupoProfesion, long paramLong)
  {
    StateManager localStateManager = paramSubgrupoProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubgrupoProfesion.idSubgrupoProfesion = paramLong;
      return;
    }
    localStateManager.setLongField(paramSubgrupoProfesion, jdoInheritedFieldCount + 3, paramSubgrupoProfesion.idSubgrupoProfesion, paramLong);
  }

  private static final String jdoGetnombre(SubgrupoProfesion paramSubgrupoProfesion)
  {
    if (paramSubgrupoProfesion.jdoFlags <= 0)
      return paramSubgrupoProfesion.nombre;
    StateManager localStateManager = paramSubgrupoProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramSubgrupoProfesion.nombre;
    if (localStateManager.isLoaded(paramSubgrupoProfesion, jdoInheritedFieldCount + 4))
      return paramSubgrupoProfesion.nombre;
    return localStateManager.getStringField(paramSubgrupoProfesion, jdoInheritedFieldCount + 4, paramSubgrupoProfesion.nombre);
  }

  private static final void jdoSetnombre(SubgrupoProfesion paramSubgrupoProfesion, String paramString)
  {
    if (paramSubgrupoProfesion.jdoFlags == 0)
    {
      paramSubgrupoProfesion.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramSubgrupoProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubgrupoProfesion.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramSubgrupoProfesion, jdoInheritedFieldCount + 4, paramSubgrupoProfesion.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}