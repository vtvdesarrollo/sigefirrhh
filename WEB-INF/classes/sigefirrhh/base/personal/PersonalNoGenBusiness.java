package sigefirrhh.base.personal;

import java.io.Serializable;
import java.util.Collection;

public class PersonalNoGenBusiness extends PersonalBusiness
  implements Serializable
{
  private NivelEducativoNoGenBeanBusiness nivelEducativoNoGenBeanBusiness = new NivelEducativoNoGenBeanBusiness();

  private ProfesionNoGenBeanBusiness profesionNoGenBeanBusiness = new ProfesionNoGenBeanBusiness();

  private TituloNoGenBeanBusiness tituloNoGenBeanBusiness = new TituloNoGenBeanBusiness();

  private CarreraNoGenBeanBusiness carreraNoGenBeanBusiness = new CarreraNoGenBeanBusiness();

  public Collection findNivelEducativoForTitulo()
    throws Exception
  {
    return this.nivelEducativoNoGenBeanBusiness.findForTitulo();
  }

  public Collection findProfesionAll() throws Exception
  {
    return this.profesionNoGenBeanBusiness.findAll();
  }

  public Collection findTituloByIdNivelEducativo(long idNivelEducativo) throws Exception
  {
    return this.tituloNoGenBeanBusiness.findByIdNivelEducativo(idNivelEducativo);
  }

  public Collection findCarreraAll() throws Exception {
    return this.carreraNoGenBeanBusiness.findAll();
  }
}