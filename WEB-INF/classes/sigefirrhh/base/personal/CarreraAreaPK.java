package sigefirrhh.base.personal;

import java.io.Serializable;

public class CarreraAreaPK
  implements Serializable
{
  public long idCarreraArea;

  public CarreraAreaPK()
  {
  }

  public CarreraAreaPK(long idCarreraArea)
  {
    this.idCarreraArea = idCarreraArea;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CarreraAreaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CarreraAreaPK thatPK)
  {
    return 
      this.idCarreraArea == thatPK.idCarreraArea;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCarreraArea)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCarreraArea);
  }
}