package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoAcreenciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoAcreencia(TipoAcreencia tipoAcreencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoAcreencia tipoAcreenciaNew = 
      (TipoAcreencia)BeanUtils.cloneBean(
      tipoAcreencia);

    pm.makePersistent(tipoAcreenciaNew);
  }

  public void updateTipoAcreencia(TipoAcreencia tipoAcreencia) throws Exception
  {
    TipoAcreencia tipoAcreenciaModify = 
      findTipoAcreenciaById(tipoAcreencia.getIdTipoAcreencia());

    BeanUtils.copyProperties(tipoAcreenciaModify, tipoAcreencia);
  }

  public void deleteTipoAcreencia(TipoAcreencia tipoAcreencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoAcreencia tipoAcreenciaDelete = 
      findTipoAcreenciaById(tipoAcreencia.getIdTipoAcreencia());
    pm.deletePersistent(tipoAcreenciaDelete);
  }

  public TipoAcreencia findTipoAcreenciaById(long idTipoAcreencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoAcreencia == pIdTipoAcreencia";
    Query query = pm.newQuery(TipoAcreencia.class, filter);

    query.declareParameters("long pIdTipoAcreencia");

    parameters.put("pIdTipoAcreencia", new Long(idTipoAcreencia));

    Collection colTipoAcreencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoAcreencia.iterator();
    return (TipoAcreencia)iterator.next();
  }

  public Collection findTipoAcreenciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoAcreenciaExtent = pm.getExtent(
      TipoAcreencia.class, true);
    Query query = pm.newQuery(tipoAcreenciaExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoAcreencia(String codTipoAcreencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoAcreencia == pCodTipoAcreencia";

    Query query = pm.newQuery(TipoAcreencia.class, filter);

    query.declareParameters("java.lang.String pCodTipoAcreencia");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoAcreencia", new String(codTipoAcreencia));

    query.setOrdering("descripcion ascending");

    Collection colTipoAcreencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoAcreencia);

    return colTipoAcreencia;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(TipoAcreencia.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTipoAcreencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoAcreencia);

    return colTipoAcreencia;
  }
}