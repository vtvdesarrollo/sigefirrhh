package sigefirrhh.base.personal;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class TituloForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TituloForm.class.getName());
  private Titulo titulo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showTituloByCodTitulo;
  private boolean showTituloByDescripcion;
  private String findCodTitulo;
  private String findDescripcion;
  private Collection colNivelEducativo;
  private Collection colGrupoProfesion;
  private String selectNivelEducativo;
  private String selectGrupoProfesion;
  private Object stateResultTituloByCodTitulo = null;

  private Object stateResultTituloByDescripcion = null;

  public String getFindCodTitulo()
  {
    return this.findCodTitulo;
  }
  public void setFindCodTitulo(String findCodTitulo) {
    this.findCodTitulo = findCodTitulo;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public String getSelectNivelEducativo()
  {
    return this.selectNivelEducativo;
  }
  public void setSelectNivelEducativo(String valNivelEducativo) {
    Iterator iterator = this.colNivelEducativo.iterator();
    NivelEducativo nivelEducativo = null;
    this.titulo.setNivelEducativo(null);
    while (iterator.hasNext()) {
      nivelEducativo = (NivelEducativo)iterator.next();
      if (String.valueOf(nivelEducativo.getIdNivelEducativo()).equals(
        valNivelEducativo)) {
        this.titulo.setNivelEducativo(
          nivelEducativo);
        break;
      }
    }
    this.selectNivelEducativo = valNivelEducativo;
  }
  public String getSelectGrupoProfesion() {
    return this.selectGrupoProfesion;
  }
  public void setSelectGrupoProfesion(String valGrupoProfesion) {
    Iterator iterator = this.colGrupoProfesion.iterator();
    GrupoProfesion grupoProfesion = null;
    this.titulo.setGrupoProfesion(null);
    while (iterator.hasNext()) {
      grupoProfesion = (GrupoProfesion)iterator.next();
      if (String.valueOf(grupoProfesion.getIdGrupoProfesion()).equals(
        valGrupoProfesion)) {
        this.titulo.setGrupoProfesion(
          grupoProfesion);
        break;
      }
    }
    this.selectGrupoProfesion = valGrupoProfesion;
  }
  public Collection getResult() {
    return this.result;
  }

  public Titulo getTitulo() {
    if (this.titulo == null) {
      this.titulo = new Titulo();
    }
    return this.titulo;
  }

  public TituloForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColNivelEducativo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colNivelEducativo.iterator();
    NivelEducativo nivelEducativo = null;
    while (iterator.hasNext()) {
      nivelEducativo = (NivelEducativo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nivelEducativo.getIdNivelEducativo()), 
        nivelEducativo.toString()));
    }
    return col;
  }

  public Collection getColGrupoProfesion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoProfesion.iterator();
    GrupoProfesion grupoProfesion = null;
    while (iterator.hasNext()) {
      grupoProfesion = (GrupoProfesion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoProfesion.getIdGrupoProfesion()), 
        grupoProfesion.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colNivelEducativo = 
        this.personalFacade.findAllNivelEducativo();
      this.colGrupoProfesion = 
        this.personalFacade.findAllGrupoProfesion();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTituloByCodTitulo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findTituloByCodTitulo(this.findCodTitulo);
      this.showTituloByCodTitulo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTituloByCodTitulo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTitulo = null;
    this.findDescripcion = null;

    return null;
  }

  public String findTituloByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findTituloByDescripcion(this.findDescripcion);
      this.showTituloByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTituloByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTitulo = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowTituloByCodTitulo() {
    return this.showTituloByCodTitulo;
  }
  public boolean isShowTituloByDescripcion() {
    return this.showTituloByDescripcion;
  }

  public String selectTitulo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectNivelEducativo = null;
    this.selectGrupoProfesion = null;

    long idTitulo = 
      Long.parseLong((String)requestParameterMap.get("idTitulo"));
    try
    {
      this.titulo = 
        this.personalFacade.findTituloById(
        idTitulo);
      if (this.titulo.getNivelEducativo() != null) {
        this.selectNivelEducativo = 
          String.valueOf(this.titulo.getNivelEducativo().getIdNivelEducativo());
      }
      if (this.titulo.getGrupoProfesion() != null) {
        this.selectGrupoProfesion = 
          String.valueOf(this.titulo.getGrupoProfesion().getIdGrupoProfesion());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.titulo = null;
    this.showTituloByCodTitulo = false;
    this.showTituloByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.personalFacade.addTitulo(
          this.titulo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.personalFacade.updateTitulo(
          this.titulo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.personalFacade.deleteTitulo(
        this.titulo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.titulo = new Titulo();

    this.selectNivelEducativo = null;

    this.selectGrupoProfesion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.titulo.setIdTitulo(identityGenerator.getNextSequenceNumber("sigefirrhh.base.personal.Titulo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.titulo = new Titulo();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("Titulo");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/personal");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "Titulo" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}