package sigefirrhh.base.personal;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class PersonalNoGenFacade extends PersonalFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private PersonalNoGenBusiness personalNoGenBusiness = new PersonalNoGenBusiness();

  public Collection findNivelEducativoForTitulo() throws Exception
  {
    try {
      this.txn.open();
      return this.personalNoGenBusiness.findNivelEducativoForTitulo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProfesionAll()
    throws Exception
  {
    return this.personalNoGenBusiness.findProfesionAll();
  }

  public Collection findTituloByIdNivelEducativo(long idNivelEducativo) throws Exception
  {
    return this.personalNoGenBusiness.findTituloByIdNivelEducativo(idNivelEducativo);
  }

  public Collection findCarreraAll() throws Exception {
    return this.personalNoGenBusiness.findCarreraAll();
  }
}