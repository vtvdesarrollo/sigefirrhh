package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class GrupoProfesion
  implements Serializable, PersistenceCapable
{
  private long idGrupoProfesion;
  private String codGrupoProfesion;
  private String nombre;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codGrupoProfesion", "descripcion", "idGrupoProfesion", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + 
      jdoGetcodGrupoProfesion(this);
  }

  public String getCodGrupoProfesion()
  {
    return jdoGetcodGrupoProfesion(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdGrupoProfesion()
  {
    return jdoGetidGrupoProfesion(this);
  }

  public void setCodGrupoProfesion(String string)
  {
    jdoSetcodGrupoProfesion(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdGrupoProfesion(long l)
  {
    jdoSetidGrupoProfesion(this, l);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.GrupoProfesion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new GrupoProfesion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    GrupoProfesion localGrupoProfesion = new GrupoProfesion();
    localGrupoProfesion.jdoFlags = 1;
    localGrupoProfesion.jdoStateManager = paramStateManager;
    return localGrupoProfesion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    GrupoProfesion localGrupoProfesion = new GrupoProfesion();
    localGrupoProfesion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localGrupoProfesion.jdoFlags = 1;
    localGrupoProfesion.jdoStateManager = paramStateManager;
    return localGrupoProfesion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codGrupoProfesion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idGrupoProfesion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codGrupoProfesion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idGrupoProfesion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(GrupoProfesion paramGrupoProfesion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramGrupoProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.codGrupoProfesion = paramGrupoProfesion.codGrupoProfesion;
      return;
    case 1:
      if (paramGrupoProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramGrupoProfesion.descripcion;
      return;
    case 2:
      if (paramGrupoProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.idGrupoProfesion = paramGrupoProfesion.idGrupoProfesion;
      return;
    case 3:
      if (paramGrupoProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramGrupoProfesion.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof GrupoProfesion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    GrupoProfesion localGrupoProfesion = (GrupoProfesion)paramObject;
    if (localGrupoProfesion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localGrupoProfesion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new GrupoProfesionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new GrupoProfesionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GrupoProfesionPK))
      throw new IllegalArgumentException("arg1");
    GrupoProfesionPK localGrupoProfesionPK = (GrupoProfesionPK)paramObject;
    localGrupoProfesionPK.idGrupoProfesion = this.idGrupoProfesion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GrupoProfesionPK))
      throw new IllegalArgumentException("arg1");
    GrupoProfesionPK localGrupoProfesionPK = (GrupoProfesionPK)paramObject;
    this.idGrupoProfesion = localGrupoProfesionPK.idGrupoProfesion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GrupoProfesionPK))
      throw new IllegalArgumentException("arg2");
    GrupoProfesionPK localGrupoProfesionPK = (GrupoProfesionPK)paramObject;
    localGrupoProfesionPK.idGrupoProfesion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GrupoProfesionPK))
      throw new IllegalArgumentException("arg2");
    GrupoProfesionPK localGrupoProfesionPK = (GrupoProfesionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localGrupoProfesionPK.idGrupoProfesion);
  }

  private static final String jdoGetcodGrupoProfesion(GrupoProfesion paramGrupoProfesion)
  {
    if (paramGrupoProfesion.jdoFlags <= 0)
      return paramGrupoProfesion.codGrupoProfesion;
    StateManager localStateManager = paramGrupoProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoProfesion.codGrupoProfesion;
    if (localStateManager.isLoaded(paramGrupoProfesion, jdoInheritedFieldCount + 0))
      return paramGrupoProfesion.codGrupoProfesion;
    return localStateManager.getStringField(paramGrupoProfesion, jdoInheritedFieldCount + 0, paramGrupoProfesion.codGrupoProfesion);
  }

  private static final void jdoSetcodGrupoProfesion(GrupoProfesion paramGrupoProfesion, String paramString)
  {
    if (paramGrupoProfesion.jdoFlags == 0)
    {
      paramGrupoProfesion.codGrupoProfesion = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoProfesion.codGrupoProfesion = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoProfesion, jdoInheritedFieldCount + 0, paramGrupoProfesion.codGrupoProfesion, paramString);
  }

  private static final String jdoGetdescripcion(GrupoProfesion paramGrupoProfesion)
  {
    if (paramGrupoProfesion.jdoFlags <= 0)
      return paramGrupoProfesion.descripcion;
    StateManager localStateManager = paramGrupoProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoProfesion.descripcion;
    if (localStateManager.isLoaded(paramGrupoProfesion, jdoInheritedFieldCount + 1))
      return paramGrupoProfesion.descripcion;
    return localStateManager.getStringField(paramGrupoProfesion, jdoInheritedFieldCount + 1, paramGrupoProfesion.descripcion);
  }

  private static final void jdoSetdescripcion(GrupoProfesion paramGrupoProfesion, String paramString)
  {
    if (paramGrupoProfesion.jdoFlags == 0)
    {
      paramGrupoProfesion.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoProfesion.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoProfesion, jdoInheritedFieldCount + 1, paramGrupoProfesion.descripcion, paramString);
  }

  private static final long jdoGetidGrupoProfesion(GrupoProfesion paramGrupoProfesion)
  {
    return paramGrupoProfesion.idGrupoProfesion;
  }

  private static final void jdoSetidGrupoProfesion(GrupoProfesion paramGrupoProfesion, long paramLong)
  {
    StateManager localStateManager = paramGrupoProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoProfesion.idGrupoProfesion = paramLong;
      return;
    }
    localStateManager.setLongField(paramGrupoProfesion, jdoInheritedFieldCount + 2, paramGrupoProfesion.idGrupoProfesion, paramLong);
  }

  private static final String jdoGetnombre(GrupoProfesion paramGrupoProfesion)
  {
    if (paramGrupoProfesion.jdoFlags <= 0)
      return paramGrupoProfesion.nombre;
    StateManager localStateManager = paramGrupoProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoProfesion.nombre;
    if (localStateManager.isLoaded(paramGrupoProfesion, jdoInheritedFieldCount + 3))
      return paramGrupoProfesion.nombre;
    return localStateManager.getStringField(paramGrupoProfesion, jdoInheritedFieldCount + 3, paramGrupoProfesion.nombre);
  }

  private static final void jdoSetnombre(GrupoProfesion paramGrupoProfesion, String paramString)
  {
    if (paramGrupoProfesion.jdoFlags == 0)
    {
      paramGrupoProfesion.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoProfesion.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoProfesion, jdoInheritedFieldCount + 3, paramGrupoProfesion.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}