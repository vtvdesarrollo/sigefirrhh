package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoAcreencia
  implements Serializable, PersistenceCapable
{
  private long idTipoAcreencia;
  private String codTipoAcreencia;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoAcreencia", "descripcion", "idTipoAcreencia" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodTipoAcreencia(this);
  }

  public String getCodTipoAcreencia()
  {
    return jdoGetcodTipoAcreencia(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdTipoAcreencia()
  {
    return jdoGetidTipoAcreencia(this);
  }

  public void setCodTipoAcreencia(String string)
  {
    jdoSetcodTipoAcreencia(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdTipoAcreencia(long l)
  {
    jdoSetidTipoAcreencia(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.TipoAcreencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoAcreencia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoAcreencia localTipoAcreencia = new TipoAcreencia();
    localTipoAcreencia.jdoFlags = 1;
    localTipoAcreencia.jdoStateManager = paramStateManager;
    return localTipoAcreencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoAcreencia localTipoAcreencia = new TipoAcreencia();
    localTipoAcreencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoAcreencia.jdoFlags = 1;
    localTipoAcreencia.jdoStateManager = paramStateManager;
    return localTipoAcreencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoAcreencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoAcreencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoAcreencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoAcreencia = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoAcreencia paramTipoAcreencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoAcreencia == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoAcreencia = paramTipoAcreencia.codTipoAcreencia;
      return;
    case 1:
      if (paramTipoAcreencia == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTipoAcreencia.descripcion;
      return;
    case 2:
      if (paramTipoAcreencia == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoAcreencia = paramTipoAcreencia.idTipoAcreencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoAcreencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoAcreencia localTipoAcreencia = (TipoAcreencia)paramObject;
    if (localTipoAcreencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoAcreencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoAcreenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoAcreenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoAcreenciaPK))
      throw new IllegalArgumentException("arg1");
    TipoAcreenciaPK localTipoAcreenciaPK = (TipoAcreenciaPK)paramObject;
    localTipoAcreenciaPK.idTipoAcreencia = this.idTipoAcreencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoAcreenciaPK))
      throw new IllegalArgumentException("arg1");
    TipoAcreenciaPK localTipoAcreenciaPK = (TipoAcreenciaPK)paramObject;
    this.idTipoAcreencia = localTipoAcreenciaPK.idTipoAcreencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoAcreenciaPK))
      throw new IllegalArgumentException("arg2");
    TipoAcreenciaPK localTipoAcreenciaPK = (TipoAcreenciaPK)paramObject;
    localTipoAcreenciaPK.idTipoAcreencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoAcreenciaPK))
      throw new IllegalArgumentException("arg2");
    TipoAcreenciaPK localTipoAcreenciaPK = (TipoAcreenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localTipoAcreenciaPK.idTipoAcreencia);
  }

  private static final String jdoGetcodTipoAcreencia(TipoAcreencia paramTipoAcreencia)
  {
    if (paramTipoAcreencia.jdoFlags <= 0)
      return paramTipoAcreencia.codTipoAcreencia;
    StateManager localStateManager = paramTipoAcreencia.jdoStateManager;
    if (localStateManager == null)
      return paramTipoAcreencia.codTipoAcreencia;
    if (localStateManager.isLoaded(paramTipoAcreencia, jdoInheritedFieldCount + 0))
      return paramTipoAcreencia.codTipoAcreencia;
    return localStateManager.getStringField(paramTipoAcreencia, jdoInheritedFieldCount + 0, paramTipoAcreencia.codTipoAcreencia);
  }

  private static final void jdoSetcodTipoAcreencia(TipoAcreencia paramTipoAcreencia, String paramString)
  {
    if (paramTipoAcreencia.jdoFlags == 0)
    {
      paramTipoAcreencia.codTipoAcreencia = paramString;
      return;
    }
    StateManager localStateManager = paramTipoAcreencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAcreencia.codTipoAcreencia = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoAcreencia, jdoInheritedFieldCount + 0, paramTipoAcreencia.codTipoAcreencia, paramString);
  }

  private static final String jdoGetdescripcion(TipoAcreencia paramTipoAcreencia)
  {
    if (paramTipoAcreencia.jdoFlags <= 0)
      return paramTipoAcreencia.descripcion;
    StateManager localStateManager = paramTipoAcreencia.jdoStateManager;
    if (localStateManager == null)
      return paramTipoAcreencia.descripcion;
    if (localStateManager.isLoaded(paramTipoAcreencia, jdoInheritedFieldCount + 1))
      return paramTipoAcreencia.descripcion;
    return localStateManager.getStringField(paramTipoAcreencia, jdoInheritedFieldCount + 1, paramTipoAcreencia.descripcion);
  }

  private static final void jdoSetdescripcion(TipoAcreencia paramTipoAcreencia, String paramString)
  {
    if (paramTipoAcreencia.jdoFlags == 0)
    {
      paramTipoAcreencia.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoAcreencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAcreencia.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoAcreencia, jdoInheritedFieldCount + 1, paramTipoAcreencia.descripcion, paramString);
  }

  private static final long jdoGetidTipoAcreencia(TipoAcreencia paramTipoAcreencia)
  {
    return paramTipoAcreencia.idTipoAcreencia;
  }

  private static final void jdoSetidTipoAcreencia(TipoAcreencia paramTipoAcreencia, long paramLong)
  {
    StateManager localStateManager = paramTipoAcreencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAcreencia.idTipoAcreencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoAcreencia, jdoInheritedFieldCount + 2, paramTipoAcreencia.idTipoAcreencia, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}