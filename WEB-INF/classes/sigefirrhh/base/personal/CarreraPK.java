package sigefirrhh.base.personal;

import java.io.Serializable;

public class CarreraPK
  implements Serializable
{
  public long idCarrera;

  public CarreraPK()
  {
  }

  public CarreraPK(long idCarrera)
  {
    this.idCarrera = idCarrera;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CarreraPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CarreraPK thatPK)
  {
    return 
      this.idCarrera == thatPK.idCarrera;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCarrera)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCarrera);
  }
}