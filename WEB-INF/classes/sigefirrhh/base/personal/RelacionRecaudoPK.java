package sigefirrhh.base.personal;

import java.io.Serializable;

public class RelacionRecaudoPK
  implements Serializable
{
  public long idRelacionRecaudo;

  public RelacionRecaudoPK()
  {
  }

  public RelacionRecaudoPK(long idRelacionRecaudo)
  {
    this.idRelacionRecaudo = idRelacionRecaudo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RelacionRecaudoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RelacionRecaudoPK thatPK)
  {
    return 
      this.idRelacionRecaudo == thatPK.idRelacionRecaudo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRelacionRecaudo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRelacionRecaudo);
  }
}