package sigefirrhh.base.personal;

import java.io.Serializable;

public class InstitucionPK
  implements Serializable
{
  public long idInstitucion;

  public InstitucionPK()
  {
  }

  public InstitucionPK(long idInstitucion)
  {
    this.idInstitucion = idInstitucion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((InstitucionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(InstitucionPK thatPK)
  {
    return 
      this.idInstitucion == thatPK.idInstitucion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idInstitucion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idInstitucion);
  }
}