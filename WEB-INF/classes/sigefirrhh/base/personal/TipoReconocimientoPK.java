package sigefirrhh.base.personal;

import java.io.Serializable;

public class TipoReconocimientoPK
  implements Serializable
{
  public long idTipoReconocimiento;

  public TipoReconocimientoPK()
  {
  }

  public TipoReconocimientoPK(long idTipoReconocimiento)
  {
    this.idTipoReconocimiento = idTipoReconocimiento;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoReconocimientoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoReconocimientoPK thatPK)
  {
    return 
      this.idTipoReconocimiento == thatPK.idTipoReconocimiento;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoReconocimiento)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoReconocimiento);
  }
}