package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoIdiomaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoIdioma(TipoIdioma tipoIdioma)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoIdioma tipoIdiomaNew = 
      (TipoIdioma)BeanUtils.cloneBean(
      tipoIdioma);

    pm.makePersistent(tipoIdiomaNew);
  }

  public void updateTipoIdioma(TipoIdioma tipoIdioma) throws Exception
  {
    TipoIdioma tipoIdiomaModify = 
      findTipoIdiomaById(tipoIdioma.getIdTipoIdioma());

    BeanUtils.copyProperties(tipoIdiomaModify, tipoIdioma);
  }

  public void deleteTipoIdioma(TipoIdioma tipoIdioma) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoIdioma tipoIdiomaDelete = 
      findTipoIdiomaById(tipoIdioma.getIdTipoIdioma());
    pm.deletePersistent(tipoIdiomaDelete);
  }

  public TipoIdioma findTipoIdiomaById(long idTipoIdioma) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoIdioma == pIdTipoIdioma";
    Query query = pm.newQuery(TipoIdioma.class, filter);

    query.declareParameters("long pIdTipoIdioma");

    parameters.put("pIdTipoIdioma", new Long(idTipoIdioma));

    Collection colTipoIdioma = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoIdioma.iterator();
    return (TipoIdioma)iterator.next();
  }

  public Collection findTipoIdiomaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoIdiomaExtent = pm.getExtent(
      TipoIdioma.class, true);
    Query query = pm.newQuery(tipoIdiomaExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoIdioma(String codTipoIdioma)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoIdioma == pCodTipoIdioma";

    Query query = pm.newQuery(TipoIdioma.class, filter);

    query.declareParameters("java.lang.String pCodTipoIdioma");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoIdioma", new String(codTipoIdioma));

    query.setOrdering("descripcion ascending");

    Collection colTipoIdioma = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoIdioma);

    return colTipoIdioma;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(TipoIdioma.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTipoIdioma = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoIdioma);

    return colTipoIdioma;
  }
}