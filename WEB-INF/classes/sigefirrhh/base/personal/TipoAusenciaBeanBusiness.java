package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoAusenciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoAusencia(TipoAusencia tipoAusencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoAusencia tipoAusenciaNew = 
      (TipoAusencia)BeanUtils.cloneBean(
      tipoAusencia);

    pm.makePersistent(tipoAusenciaNew);
  }

  public void updateTipoAusencia(TipoAusencia tipoAusencia) throws Exception
  {
    TipoAusencia tipoAusenciaModify = 
      findTipoAusenciaById(tipoAusencia.getIdTipoAusencia());

    BeanUtils.copyProperties(tipoAusenciaModify, tipoAusencia);
  }

  public void deleteTipoAusencia(TipoAusencia tipoAusencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoAusencia tipoAusenciaDelete = 
      findTipoAusenciaById(tipoAusencia.getIdTipoAusencia());
    pm.deletePersistent(tipoAusenciaDelete);
  }

  public TipoAusencia findTipoAusenciaById(long idTipoAusencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoAusencia == pIdTipoAusencia";
    Query query = pm.newQuery(TipoAusencia.class, filter);

    query.declareParameters("long pIdTipoAusencia");

    parameters.put("pIdTipoAusencia", new Long(idTipoAusencia));

    Collection colTipoAusencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoAusencia.iterator();
    return (TipoAusencia)iterator.next();
  }

  public Collection findTipoAusenciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoAusenciaExtent = pm.getExtent(
      TipoAusencia.class, true);
    Query query = pm.newQuery(tipoAusenciaExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoAusencia(String codTipoAusencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoAusencia == pCodTipoAusencia";

    Query query = pm.newQuery(TipoAusencia.class, filter);

    query.declareParameters("java.lang.String pCodTipoAusencia");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoAusencia", new String(codTipoAusencia));

    query.setOrdering("descripcion ascending");

    Collection colTipoAusencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoAusencia);

    return colTipoAusencia;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(TipoAusencia.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTipoAusencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoAusencia);

    return colTipoAusencia;
  }
}