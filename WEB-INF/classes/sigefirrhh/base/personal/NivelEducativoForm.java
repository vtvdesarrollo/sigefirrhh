package sigefirrhh.base.personal;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class NivelEducativoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(NivelEducativoForm.class.getName());
  private NivelEducativo nivelEducativo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showNivelEducativoByCodNivelEducativo;
  private boolean showNivelEducativoByDescripcion;
  private String findCodNivelEducativo;
  private String findDescripcion;
  private Object stateResultNivelEducativoByCodNivelEducativo = null;

  private Object stateResultNivelEducativoByDescripcion = null;

  public String getFindCodNivelEducativo()
  {
    return this.findCodNivelEducativo;
  }
  public void setFindCodNivelEducativo(String findCodNivelEducativo) {
    this.findCodNivelEducativo = findCodNivelEducativo;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public NivelEducativo getNivelEducativo() {
    if (this.nivelEducativo == null) {
      this.nivelEducativo = new NivelEducativo();
    }
    return this.nivelEducativo;
  }

  public NivelEducativoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findNivelEducativoByCodNivelEducativo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findNivelEducativoByCodNivelEducativo(this.findCodNivelEducativo);
      this.showNivelEducativoByCodNivelEducativo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showNivelEducativoByCodNivelEducativo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodNivelEducativo = null;
    this.findDescripcion = null;

    return null;
  }

  public String findNivelEducativoByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findNivelEducativoByDescripcion(this.findDescripcion);
      this.showNivelEducativoByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showNivelEducativoByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodNivelEducativo = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowNivelEducativoByCodNivelEducativo() {
    return this.showNivelEducativoByCodNivelEducativo;
  }
  public boolean isShowNivelEducativoByDescripcion() {
    return this.showNivelEducativoByDescripcion;
  }

  public String selectNivelEducativo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idNivelEducativo = 
      Long.parseLong((String)requestParameterMap.get("idNivelEducativo"));
    try
    {
      this.nivelEducativo = 
        this.personalFacade.findNivelEducativoById(
        idNivelEducativo);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.nivelEducativo = null;
    this.showNivelEducativoByCodNivelEducativo = false;
    this.showNivelEducativoByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.personalFacade.addNivelEducativo(
          this.nivelEducativo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.personalFacade.updateNivelEducativo(
          this.nivelEducativo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.personalFacade.deleteNivelEducativo(
        this.nivelEducativo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.nivelEducativo = new NivelEducativo();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.nivelEducativo.setIdNivelEducativo(identityGenerator.getNextSequenceNumber("sigefirrhh.base.personal.NivelEducativo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.nivelEducativo = new NivelEducativo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}