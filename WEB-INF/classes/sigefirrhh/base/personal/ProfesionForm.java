package sigefirrhh.base.personal;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ProfesionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ProfesionForm.class.getName());
  private Profesion profesion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showProfesionBySubgrupoProfesion;
  private boolean showProfesionByCodProfesion;
  private boolean showProfesionByNombre;
  private String findSelectGrupoProfesionForSubgrupoProfesion;
  private String findSelectSubgrupoProfesion;
  private String findCodProfesion;
  private String findNombre;
  private Collection findColGrupoProfesionForSubgrupoProfesion;
  private Collection findColSubgrupoProfesion;
  private Collection colGrupoProfesionForSubgrupoProfesion;
  private Collection colSubgrupoProfesion;
  private String selectGrupoProfesionForSubgrupoProfesion;
  private String selectSubgrupoProfesion;
  private Object stateResultProfesionBySubgrupoProfesion = null;

  private Object stateResultProfesionByCodProfesion = null;

  private Object stateResultProfesionByNombre = null;

  public Collection getFindColGrupoProfesionForSubgrupoProfesion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColGrupoProfesionForSubgrupoProfesion.iterator();
    GrupoProfesion grupoProfesionForSubgrupoProfesion = null;
    while (iterator.hasNext()) {
      grupoProfesionForSubgrupoProfesion = (GrupoProfesion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoProfesionForSubgrupoProfesion.getIdGrupoProfesion()), 
        grupoProfesionForSubgrupoProfesion.toString()));
    }
    return col;
  }
  public String getFindSelectGrupoProfesionForSubgrupoProfesion() {
    return this.findSelectGrupoProfesionForSubgrupoProfesion;
  }
  public void setFindSelectGrupoProfesionForSubgrupoProfesion(String valGrupoProfesionForSubgrupoProfesion) {
    this.findSelectGrupoProfesionForSubgrupoProfesion = valGrupoProfesionForSubgrupoProfesion;
  }
  public void findChangeGrupoProfesionForSubgrupoProfesion(ValueChangeEvent event) {
    long idGrupoProfesion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColSubgrupoProfesion = null;
      if (idGrupoProfesion > 0L)
        this.findColSubgrupoProfesion = 
          this.personalFacade.findSubgrupoProfesionByGrupoProfesion(
          idGrupoProfesion);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowGrupoProfesionForSubgrupoProfesion() { return this.findColGrupoProfesionForSubgrupoProfesion != null; }

  public String getFindSelectSubgrupoProfesion() {
    return this.findSelectSubgrupoProfesion;
  }
  public void setFindSelectSubgrupoProfesion(String valSubgrupoProfesion) {
    this.findSelectSubgrupoProfesion = valSubgrupoProfesion;
  }

  public Collection getFindColSubgrupoProfesion() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColSubgrupoProfesion.iterator();
    SubgrupoProfesion subgrupoProfesion = null;
    while (iterator.hasNext()) {
      subgrupoProfesion = (SubgrupoProfesion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(subgrupoProfesion.getIdSubgrupoProfesion()), 
        subgrupoProfesion.toString()));
    }
    return col;
  }
  public boolean isFindShowSubgrupoProfesion() {
    return this.findColSubgrupoProfesion != null;
  }
  public String getFindCodProfesion() {
    return this.findCodProfesion;
  }
  public void setFindCodProfesion(String findCodProfesion) {
    this.findCodProfesion = findCodProfesion;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectGrupoProfesionForSubgrupoProfesion()
  {
    return this.selectGrupoProfesionForSubgrupoProfesion;
  }
  public void setSelectGrupoProfesionForSubgrupoProfesion(String valGrupoProfesionForSubgrupoProfesion) {
    this.selectGrupoProfesionForSubgrupoProfesion = valGrupoProfesionForSubgrupoProfesion;
  }
  public void changeGrupoProfesionForSubgrupoProfesion(ValueChangeEvent event) {
    long idGrupoProfesion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colSubgrupoProfesion = null;
      if (idGrupoProfesion > 0L) {
        this.colSubgrupoProfesion = 
          this.personalFacade.findSubgrupoProfesionByGrupoProfesion(
          idGrupoProfesion);
      } else {
        this.selectSubgrupoProfesion = null;
        this.profesion.setSubgrupoProfesion(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectSubgrupoProfesion = null;
      this.profesion.setSubgrupoProfesion(
        null);
    }
  }

  public boolean isShowGrupoProfesionForSubgrupoProfesion() { return this.colGrupoProfesionForSubgrupoProfesion != null; }

  public String getSelectSubgrupoProfesion() {
    return this.selectSubgrupoProfesion;
  }
  public void setSelectSubgrupoProfesion(String valSubgrupoProfesion) {
    Iterator iterator = this.colSubgrupoProfesion.iterator();
    SubgrupoProfesion subgrupoProfesion = null;
    this.profesion.setSubgrupoProfesion(null);
    while (iterator.hasNext()) {
      subgrupoProfesion = (SubgrupoProfesion)iterator.next();
      if (String.valueOf(subgrupoProfesion.getIdSubgrupoProfesion()).equals(
        valSubgrupoProfesion)) {
        this.profesion.setSubgrupoProfesion(
          subgrupoProfesion);
        break;
      }
    }
    this.selectSubgrupoProfesion = valSubgrupoProfesion;
  }
  public boolean isShowSubgrupoProfesion() {
    return this.colSubgrupoProfesion != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public Profesion getProfesion() {
    if (this.profesion == null) {
      this.profesion = new Profesion();
    }
    return this.profesion;
  }

  public ProfesionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColGrupoProfesionForSubgrupoProfesion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoProfesionForSubgrupoProfesion.iterator();
    GrupoProfesion grupoProfesionForSubgrupoProfesion = null;
    while (iterator.hasNext()) {
      grupoProfesionForSubgrupoProfesion = (GrupoProfesion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoProfesionForSubgrupoProfesion.getIdGrupoProfesion()), 
        grupoProfesionForSubgrupoProfesion.toString()));
    }
    return col;
  }

  public Collection getColSubgrupoProfesion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colSubgrupoProfesion.iterator();
    SubgrupoProfesion subgrupoProfesion = null;
    while (iterator.hasNext()) {
      subgrupoProfesion = (SubgrupoProfesion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(subgrupoProfesion.getIdSubgrupoProfesion()), 
        subgrupoProfesion.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColGrupoProfesionForSubgrupoProfesion = 
        this.personalFacade.findAllGrupoProfesion();

      this.colGrupoProfesionForSubgrupoProfesion = 
        this.personalFacade.findAllGrupoProfesion();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findProfesionBySubgrupoProfesion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findProfesionBySubgrupoProfesion(Long.valueOf(this.findSelectSubgrupoProfesion).longValue());
      this.showProfesionBySubgrupoProfesion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showProfesionBySubgrupoProfesion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoProfesionForSubgrupoProfesion = null;
    this.findSelectSubgrupoProfesion = null;
    this.findCodProfesion = null;
    this.findNombre = null;

    return null;
  }

  public String findProfesionByCodProfesion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findProfesionByCodProfesion(this.findCodProfesion);
      this.showProfesionByCodProfesion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showProfesionByCodProfesion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoProfesionForSubgrupoProfesion = null;
    this.findSelectSubgrupoProfesion = null;
    this.findCodProfesion = null;
    this.findNombre = null;

    return null;
  }

  public String findProfesionByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findProfesionByNombre(this.findNombre);
      this.showProfesionByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showProfesionByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoProfesionForSubgrupoProfesion = null;
    this.findSelectSubgrupoProfesion = null;
    this.findCodProfesion = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowProfesionBySubgrupoProfesion() {
    return this.showProfesionBySubgrupoProfesion;
  }
  public boolean isShowProfesionByCodProfesion() {
    return this.showProfesionByCodProfesion;
  }
  public boolean isShowProfesionByNombre() {
    return this.showProfesionByNombre;
  }

  public String selectProfesion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectSubgrupoProfesion = null;
    this.selectGrupoProfesionForSubgrupoProfesion = null;

    long idProfesion = 
      Long.parseLong((String)requestParameterMap.get("idProfesion"));
    try
    {
      this.profesion = 
        this.personalFacade.findProfesionById(
        idProfesion);
      if (this.profesion.getSubgrupoProfesion() != null) {
        this.selectSubgrupoProfesion = 
          String.valueOf(this.profesion.getSubgrupoProfesion().getIdSubgrupoProfesion());
      }

      SubgrupoProfesion subgrupoProfesion = null;
      GrupoProfesion grupoProfesionForSubgrupoProfesion = null;

      if (this.profesion.getSubgrupoProfesion() != null) {
        long idSubgrupoProfesion = 
          this.profesion.getSubgrupoProfesion().getIdSubgrupoProfesion();
        this.selectSubgrupoProfesion = String.valueOf(idSubgrupoProfesion);
        subgrupoProfesion = this.personalFacade.findSubgrupoProfesionById(
          idSubgrupoProfesion);
        this.colSubgrupoProfesion = this.personalFacade.findSubgrupoProfesionByGrupoProfesion(
          subgrupoProfesion.getGrupoProfesion().getIdGrupoProfesion());

        long idGrupoProfesionForSubgrupoProfesion = 
          this.profesion.getSubgrupoProfesion().getGrupoProfesion().getIdGrupoProfesion();
        this.selectGrupoProfesionForSubgrupoProfesion = String.valueOf(idGrupoProfesionForSubgrupoProfesion);
        grupoProfesionForSubgrupoProfesion = 
          this.personalFacade.findGrupoProfesionById(
          idGrupoProfesionForSubgrupoProfesion);
        this.colGrupoProfesionForSubgrupoProfesion = 
          this.personalFacade.findAllGrupoProfesion();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.profesion = null;
    this.showProfesionBySubgrupoProfesion = false;
    this.showProfesionByCodProfesion = false;
    this.showProfesionByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.personalFacade.addProfesion(
          this.profesion);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.personalFacade.updateProfesion(
          this.profesion);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.personalFacade.deleteProfesion(
        this.profesion);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.profesion = new Profesion();

    this.selectSubgrupoProfesion = null;

    this.selectGrupoProfesionForSubgrupoProfesion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.profesion.setIdProfesion(identityGenerator.getNextSequenceNumber("sigefirrhh.base.personal.Profesion"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.profesion = new Profesion();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("Profesion");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/personal");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "Profesion" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}