package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Institucion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_SINO;
  private long idInstitucion;
  private String codInstitucion;
  private String nombre;
  private String nombreCorto;
  private String estatus;
  private String leyEstatuto;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codInstitucion", "estatus", "idInstitucion", "leyEstatuto", "nombre", "nombreCorto" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.Institucion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Institucion());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("A", "ACTIVA");
    LISTA_ESTATUS.put("I", "INACTIVA");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public Institucion()
  {
    jdoSetestatus(this, "A");

    jdoSetleyEstatuto(this, "S");
  }

  public String toString() {
    return jdoGetnombre(this) + " - " + 
      jdoGetcodInstitucion(this);
  }

  public String getCodInstitucion()
  {
    return jdoGetcodInstitucion(this);
  }

  public long getIdInstitucion()
  {
    return jdoGetidInstitucion(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodInstitucion(String string)
  {
    jdoSetcodInstitucion(this, string);
  }

  public void setIdInstitucion(long l)
  {
    jdoSetidInstitucion(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public String getNombreCorto()
  {
    return jdoGetnombreCorto(this);
  }

  public void setNombreCorto(String string)
  {
    jdoSetnombreCorto(this, string);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public String getLeyEstatuto()
  {
    return jdoGetleyEstatuto(this);
  }

  public void setLeyEstatuto(String string)
  {
    jdoSetleyEstatuto(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Institucion localInstitucion = new Institucion();
    localInstitucion.jdoFlags = 1;
    localInstitucion.jdoStateManager = paramStateManager;
    return localInstitucion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Institucion localInstitucion = new Institucion();
    localInstitucion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localInstitucion.jdoFlags = 1;
    localInstitucion.jdoStateManager = paramStateManager;
    return localInstitucion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codInstitucion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idInstitucion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.leyEstatuto);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCorto);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codInstitucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idInstitucion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.leyEstatuto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCorto = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Institucion paramInstitucion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramInstitucion == null)
        throw new IllegalArgumentException("arg1");
      this.codInstitucion = paramInstitucion.codInstitucion;
      return;
    case 1:
      if (paramInstitucion == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramInstitucion.estatus;
      return;
    case 2:
      if (paramInstitucion == null)
        throw new IllegalArgumentException("arg1");
      this.idInstitucion = paramInstitucion.idInstitucion;
      return;
    case 3:
      if (paramInstitucion == null)
        throw new IllegalArgumentException("arg1");
      this.leyEstatuto = paramInstitucion.leyEstatuto;
      return;
    case 4:
      if (paramInstitucion == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramInstitucion.nombre;
      return;
    case 5:
      if (paramInstitucion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCorto = paramInstitucion.nombreCorto;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Institucion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Institucion localInstitucion = (Institucion)paramObject;
    if (localInstitucion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localInstitucion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new InstitucionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new InstitucionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof InstitucionPK))
      throw new IllegalArgumentException("arg1");
    InstitucionPK localInstitucionPK = (InstitucionPK)paramObject;
    localInstitucionPK.idInstitucion = this.idInstitucion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof InstitucionPK))
      throw new IllegalArgumentException("arg1");
    InstitucionPK localInstitucionPK = (InstitucionPK)paramObject;
    this.idInstitucion = localInstitucionPK.idInstitucion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof InstitucionPK))
      throw new IllegalArgumentException("arg2");
    InstitucionPK localInstitucionPK = (InstitucionPK)paramObject;
    localInstitucionPK.idInstitucion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof InstitucionPK))
      throw new IllegalArgumentException("arg2");
    InstitucionPK localInstitucionPK = (InstitucionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localInstitucionPK.idInstitucion);
  }

  private static final String jdoGetcodInstitucion(Institucion paramInstitucion)
  {
    if (paramInstitucion.jdoFlags <= 0)
      return paramInstitucion.codInstitucion;
    StateManager localStateManager = paramInstitucion.jdoStateManager;
    if (localStateManager == null)
      return paramInstitucion.codInstitucion;
    if (localStateManager.isLoaded(paramInstitucion, jdoInheritedFieldCount + 0))
      return paramInstitucion.codInstitucion;
    return localStateManager.getStringField(paramInstitucion, jdoInheritedFieldCount + 0, paramInstitucion.codInstitucion);
  }

  private static final void jdoSetcodInstitucion(Institucion paramInstitucion, String paramString)
  {
    if (paramInstitucion.jdoFlags == 0)
    {
      paramInstitucion.codInstitucion = paramString;
      return;
    }
    StateManager localStateManager = paramInstitucion.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstitucion.codInstitucion = paramString;
      return;
    }
    localStateManager.setStringField(paramInstitucion, jdoInheritedFieldCount + 0, paramInstitucion.codInstitucion, paramString);
  }

  private static final String jdoGetestatus(Institucion paramInstitucion)
  {
    if (paramInstitucion.jdoFlags <= 0)
      return paramInstitucion.estatus;
    StateManager localStateManager = paramInstitucion.jdoStateManager;
    if (localStateManager == null)
      return paramInstitucion.estatus;
    if (localStateManager.isLoaded(paramInstitucion, jdoInheritedFieldCount + 1))
      return paramInstitucion.estatus;
    return localStateManager.getStringField(paramInstitucion, jdoInheritedFieldCount + 1, paramInstitucion.estatus);
  }

  private static final void jdoSetestatus(Institucion paramInstitucion, String paramString)
  {
    if (paramInstitucion.jdoFlags == 0)
    {
      paramInstitucion.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramInstitucion.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstitucion.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramInstitucion, jdoInheritedFieldCount + 1, paramInstitucion.estatus, paramString);
  }

  private static final long jdoGetidInstitucion(Institucion paramInstitucion)
  {
    return paramInstitucion.idInstitucion;
  }

  private static final void jdoSetidInstitucion(Institucion paramInstitucion, long paramLong)
  {
    StateManager localStateManager = paramInstitucion.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstitucion.idInstitucion = paramLong;
      return;
    }
    localStateManager.setLongField(paramInstitucion, jdoInheritedFieldCount + 2, paramInstitucion.idInstitucion, paramLong);
  }

  private static final String jdoGetleyEstatuto(Institucion paramInstitucion)
  {
    if (paramInstitucion.jdoFlags <= 0)
      return paramInstitucion.leyEstatuto;
    StateManager localStateManager = paramInstitucion.jdoStateManager;
    if (localStateManager == null)
      return paramInstitucion.leyEstatuto;
    if (localStateManager.isLoaded(paramInstitucion, jdoInheritedFieldCount + 3))
      return paramInstitucion.leyEstatuto;
    return localStateManager.getStringField(paramInstitucion, jdoInheritedFieldCount + 3, paramInstitucion.leyEstatuto);
  }

  private static final void jdoSetleyEstatuto(Institucion paramInstitucion, String paramString)
  {
    if (paramInstitucion.jdoFlags == 0)
    {
      paramInstitucion.leyEstatuto = paramString;
      return;
    }
    StateManager localStateManager = paramInstitucion.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstitucion.leyEstatuto = paramString;
      return;
    }
    localStateManager.setStringField(paramInstitucion, jdoInheritedFieldCount + 3, paramInstitucion.leyEstatuto, paramString);
  }

  private static final String jdoGetnombre(Institucion paramInstitucion)
  {
    if (paramInstitucion.jdoFlags <= 0)
      return paramInstitucion.nombre;
    StateManager localStateManager = paramInstitucion.jdoStateManager;
    if (localStateManager == null)
      return paramInstitucion.nombre;
    if (localStateManager.isLoaded(paramInstitucion, jdoInheritedFieldCount + 4))
      return paramInstitucion.nombre;
    return localStateManager.getStringField(paramInstitucion, jdoInheritedFieldCount + 4, paramInstitucion.nombre);
  }

  private static final void jdoSetnombre(Institucion paramInstitucion, String paramString)
  {
    if (paramInstitucion.jdoFlags == 0)
    {
      paramInstitucion.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramInstitucion.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstitucion.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramInstitucion, jdoInheritedFieldCount + 4, paramInstitucion.nombre, paramString);
  }

  private static final String jdoGetnombreCorto(Institucion paramInstitucion)
  {
    if (paramInstitucion.jdoFlags <= 0)
      return paramInstitucion.nombreCorto;
    StateManager localStateManager = paramInstitucion.jdoStateManager;
    if (localStateManager == null)
      return paramInstitucion.nombreCorto;
    if (localStateManager.isLoaded(paramInstitucion, jdoInheritedFieldCount + 5))
      return paramInstitucion.nombreCorto;
    return localStateManager.getStringField(paramInstitucion, jdoInheritedFieldCount + 5, paramInstitucion.nombreCorto);
  }

  private static final void jdoSetnombreCorto(Institucion paramInstitucion, String paramString)
  {
    if (paramInstitucion.jdoFlags == 0)
    {
      paramInstitucion.nombreCorto = paramString;
      return;
    }
    StateManager localStateManager = paramInstitucion.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstitucion.nombreCorto = paramString;
      return;
    }
    localStateManager.setStringField(paramInstitucion, jdoInheritedFieldCount + 5, paramInstitucion.nombreCorto, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}