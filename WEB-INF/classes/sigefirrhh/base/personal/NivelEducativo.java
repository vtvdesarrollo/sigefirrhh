package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class NivelEducativo
  implements Serializable, PersistenceCapable
{
  private long idNivelEducativo;
  private int orden;
  private String codNivelEducativo;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codNivelEducativo", "descripcion", "idNivelEducativo", "orden" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodNivelEducativo(this);
  }

  public String getCodNivelEducativo()
  {
    return jdoGetcodNivelEducativo(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdNivelEducativo()
  {
    return jdoGetidNivelEducativo(this);
  }

  public void setCodNivelEducativo(String string)
  {
    jdoSetcodNivelEducativo(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdNivelEducativo(long l)
  {
    jdoSetidNivelEducativo(this, l);
  }

  public int getOrden()
  {
    return jdoGetorden(this);
  }

  public void setOrden(int i)
  {
    jdoSetorden(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.NivelEducativo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new NivelEducativo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    NivelEducativo localNivelEducativo = new NivelEducativo();
    localNivelEducativo.jdoFlags = 1;
    localNivelEducativo.jdoStateManager = paramStateManager;
    return localNivelEducativo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    NivelEducativo localNivelEducativo = new NivelEducativo();
    localNivelEducativo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localNivelEducativo.jdoFlags = 1;
    localNivelEducativo.jdoStateManager = paramStateManager;
    return localNivelEducativo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codNivelEducativo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idNivelEducativo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.orden);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codNivelEducativo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idNivelEducativo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.orden = localStateManager.replacingIntField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(NivelEducativo paramNivelEducativo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramNivelEducativo == null)
        throw new IllegalArgumentException("arg1");
      this.codNivelEducativo = paramNivelEducativo.codNivelEducativo;
      return;
    case 1:
      if (paramNivelEducativo == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramNivelEducativo.descripcion;
      return;
    case 2:
      if (paramNivelEducativo == null)
        throw new IllegalArgumentException("arg1");
      this.idNivelEducativo = paramNivelEducativo.idNivelEducativo;
      return;
    case 3:
      if (paramNivelEducativo == null)
        throw new IllegalArgumentException("arg1");
      this.orden = paramNivelEducativo.orden;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof NivelEducativo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    NivelEducativo localNivelEducativo = (NivelEducativo)paramObject;
    if (localNivelEducativo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localNivelEducativo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new NivelEducativoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new NivelEducativoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NivelEducativoPK))
      throw new IllegalArgumentException("arg1");
    NivelEducativoPK localNivelEducativoPK = (NivelEducativoPK)paramObject;
    localNivelEducativoPK.idNivelEducativo = this.idNivelEducativo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NivelEducativoPK))
      throw new IllegalArgumentException("arg1");
    NivelEducativoPK localNivelEducativoPK = (NivelEducativoPK)paramObject;
    this.idNivelEducativo = localNivelEducativoPK.idNivelEducativo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NivelEducativoPK))
      throw new IllegalArgumentException("arg2");
    NivelEducativoPK localNivelEducativoPK = (NivelEducativoPK)paramObject;
    localNivelEducativoPK.idNivelEducativo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NivelEducativoPK))
      throw new IllegalArgumentException("arg2");
    NivelEducativoPK localNivelEducativoPK = (NivelEducativoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localNivelEducativoPK.idNivelEducativo);
  }

  private static final String jdoGetcodNivelEducativo(NivelEducativo paramNivelEducativo)
  {
    if (paramNivelEducativo.jdoFlags <= 0)
      return paramNivelEducativo.codNivelEducativo;
    StateManager localStateManager = paramNivelEducativo.jdoStateManager;
    if (localStateManager == null)
      return paramNivelEducativo.codNivelEducativo;
    if (localStateManager.isLoaded(paramNivelEducativo, jdoInheritedFieldCount + 0))
      return paramNivelEducativo.codNivelEducativo;
    return localStateManager.getStringField(paramNivelEducativo, jdoInheritedFieldCount + 0, paramNivelEducativo.codNivelEducativo);
  }

  private static final void jdoSetcodNivelEducativo(NivelEducativo paramNivelEducativo, String paramString)
  {
    if (paramNivelEducativo.jdoFlags == 0)
    {
      paramNivelEducativo.codNivelEducativo = paramString;
      return;
    }
    StateManager localStateManager = paramNivelEducativo.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelEducativo.codNivelEducativo = paramString;
      return;
    }
    localStateManager.setStringField(paramNivelEducativo, jdoInheritedFieldCount + 0, paramNivelEducativo.codNivelEducativo, paramString);
  }

  private static final String jdoGetdescripcion(NivelEducativo paramNivelEducativo)
  {
    if (paramNivelEducativo.jdoFlags <= 0)
      return paramNivelEducativo.descripcion;
    StateManager localStateManager = paramNivelEducativo.jdoStateManager;
    if (localStateManager == null)
      return paramNivelEducativo.descripcion;
    if (localStateManager.isLoaded(paramNivelEducativo, jdoInheritedFieldCount + 1))
      return paramNivelEducativo.descripcion;
    return localStateManager.getStringField(paramNivelEducativo, jdoInheritedFieldCount + 1, paramNivelEducativo.descripcion);
  }

  private static final void jdoSetdescripcion(NivelEducativo paramNivelEducativo, String paramString)
  {
    if (paramNivelEducativo.jdoFlags == 0)
    {
      paramNivelEducativo.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramNivelEducativo.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelEducativo.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramNivelEducativo, jdoInheritedFieldCount + 1, paramNivelEducativo.descripcion, paramString);
  }

  private static final long jdoGetidNivelEducativo(NivelEducativo paramNivelEducativo)
  {
    return paramNivelEducativo.idNivelEducativo;
  }

  private static final void jdoSetidNivelEducativo(NivelEducativo paramNivelEducativo, long paramLong)
  {
    StateManager localStateManager = paramNivelEducativo.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelEducativo.idNivelEducativo = paramLong;
      return;
    }
    localStateManager.setLongField(paramNivelEducativo, jdoInheritedFieldCount + 2, paramNivelEducativo.idNivelEducativo, paramLong);
  }

  private static final int jdoGetorden(NivelEducativo paramNivelEducativo)
  {
    if (paramNivelEducativo.jdoFlags <= 0)
      return paramNivelEducativo.orden;
    StateManager localStateManager = paramNivelEducativo.jdoStateManager;
    if (localStateManager == null)
      return paramNivelEducativo.orden;
    if (localStateManager.isLoaded(paramNivelEducativo, jdoInheritedFieldCount + 3))
      return paramNivelEducativo.orden;
    return localStateManager.getIntField(paramNivelEducativo, jdoInheritedFieldCount + 3, paramNivelEducativo.orden);
  }

  private static final void jdoSetorden(NivelEducativo paramNivelEducativo, int paramInt)
  {
    if (paramNivelEducativo.jdoFlags == 0)
    {
      paramNivelEducativo.orden = paramInt;
      return;
    }
    StateManager localStateManager = paramNivelEducativo.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelEducativo.orden = paramInt;
      return;
    }
    localStateManager.setIntField(paramNivelEducativo, jdoInheritedFieldCount + 3, paramNivelEducativo.orden, paramInt);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}