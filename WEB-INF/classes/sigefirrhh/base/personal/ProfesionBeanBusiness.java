package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ProfesionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addProfesion(Profesion profesion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Profesion profesionNew = 
      (Profesion)BeanUtils.cloneBean(
      profesion);

    SubgrupoProfesionBeanBusiness subgrupoProfesionBeanBusiness = new SubgrupoProfesionBeanBusiness();

    if (profesionNew.getSubgrupoProfesion() != null) {
      profesionNew.setSubgrupoProfesion(
        subgrupoProfesionBeanBusiness.findSubgrupoProfesionById(
        profesionNew.getSubgrupoProfesion().getIdSubgrupoProfesion()));
    }
    pm.makePersistent(profesionNew);

    pm = null;
  }

  public void updateProfesion(Profesion profesion) throws Exception
  {
    Profesion profesionModify = 
      findProfesionById(profesion.getIdProfesion());

    SubgrupoProfesionBeanBusiness subgrupoProfesionBeanBusiness = new SubgrupoProfesionBeanBusiness();

    if (profesion.getSubgrupoProfesion() != null) {
      profesion.setSubgrupoProfesion(
        subgrupoProfesionBeanBusiness.findSubgrupoProfesionById(
        profesion.getSubgrupoProfesion().getIdSubgrupoProfesion()));
    }

    BeanUtils.copyProperties(profesionModify, profesion);
  }

  public void deleteProfesion(Profesion profesion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Profesion profesionDelete = 
      findProfesionById(profesion.getIdProfesion());
    pm.deletePersistent(profesionDelete);

    pm = null;
  }

  public Profesion findProfesionById(long idProfesion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idProfesion == pIdProfesion";
    Query query = pm.newQuery(Profesion.class, filter);

    query.declareParameters("long pIdProfesion");

    parameters.put("pIdProfesion", new Long(idProfesion));

    Collection colProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colProfesion.iterator();

    query = null;

    pm = null;
    return (Profesion)iterator.next();
  }

  public Collection findProfesionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent profesionExtent = pm.getExtent(
      Profesion.class, true);
    Query query = pm.newQuery(profesionExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());

    query = null;

    pm = null;
    return collection;
  }

  public Collection findBySubgrupoProfesion(long idSubgrupoProfesion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "subgrupoProfesion.idSubgrupoProfesion == pIdSubgrupoProfesion";

    Query query = pm.newQuery(Profesion.class, filter);

    query.declareParameters("long pIdSubgrupoProfesion");
    HashMap parameters = new HashMap();

    parameters.put("pIdSubgrupoProfesion", new Long(idSubgrupoProfesion));

    query.setOrdering("nombre ascending");

    Collection colProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProfesion);

    query = null;

    pm = null;

    return colProfesion;
  }

  public Collection findByCodProfesion(String codProfesion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codProfesion == pCodProfesion";

    Query query = pm.newQuery(Profesion.class, filter);

    query.declareParameters("java.lang.String pCodProfesion");
    HashMap parameters = new HashMap();

    parameters.put("pCodProfesion", new String(codProfesion));

    query.setOrdering("nombre ascending");

    Collection colProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProfesion);

    query = null;

    pm = null;

    return colProfesion;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Profesion.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProfesion);

    query = null;

    pm = null;

    return colProfesion;
  }

  public Collection findAll()
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = Resource.getConnection();
    connection = Resource.getConnection();
    connection.setAutoCommit(true);
    StringBuffer sql = new StringBuffer();

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    sql.append("select id_profesion, nombre ");
    sql.append(" from profesion");
    sql.append(" order by nombre");

    stRegistros = connection.prepareStatement(
      sql.toString(), 
      1003, 
      1007);
    rsRegistros = stRegistros.executeQuery();

    while (rsRegistros.next()) {
      col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_profesion"))));
      col.add(rsRegistros.getString("nombre"));
    }
    connection.close(); connection = null;
    connection = null;
    return col;
  }
}