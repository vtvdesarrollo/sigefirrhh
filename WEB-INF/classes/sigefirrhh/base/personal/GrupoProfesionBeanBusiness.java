package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class GrupoProfesionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addGrupoProfesion(GrupoProfesion grupoProfesion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    GrupoProfesion grupoProfesionNew = 
      (GrupoProfesion)BeanUtils.cloneBean(
      grupoProfesion);

    pm.makePersistent(grupoProfesionNew);
  }

  public void updateGrupoProfesion(GrupoProfesion grupoProfesion) throws Exception
  {
    GrupoProfesion grupoProfesionModify = 
      findGrupoProfesionById(grupoProfesion.getIdGrupoProfesion());

    BeanUtils.copyProperties(grupoProfesionModify, grupoProfesion);
  }

  public void deleteGrupoProfesion(GrupoProfesion grupoProfesion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    GrupoProfesion grupoProfesionDelete = 
      findGrupoProfesionById(grupoProfesion.getIdGrupoProfesion());
    pm.deletePersistent(grupoProfesionDelete);
  }

  public GrupoProfesion findGrupoProfesionById(long idGrupoProfesion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idGrupoProfesion == pIdGrupoProfesion";
    Query query = pm.newQuery(GrupoProfesion.class, filter);

    query.declareParameters("long pIdGrupoProfesion");

    parameters.put("pIdGrupoProfesion", new Long(idGrupoProfesion));

    Collection colGrupoProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colGrupoProfesion.iterator();
    return (GrupoProfesion)iterator.next();
  }

  public Collection findGrupoProfesionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent grupoProfesionExtent = pm.getExtent(
      GrupoProfesion.class, true);
    Query query = pm.newQuery(grupoProfesionExtent);
    query.setOrdering("codGrupoProfesion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodGrupoProfesion(String codGrupoProfesion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codGrupoProfesion == pCodGrupoProfesion";

    Query query = pm.newQuery(GrupoProfesion.class, filter);

    query.declareParameters("java.lang.String pCodGrupoProfesion");
    HashMap parameters = new HashMap();

    parameters.put("pCodGrupoProfesion", new String(codGrupoProfesion));

    query.setOrdering("codGrupoProfesion ascending");

    Collection colGrupoProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoProfesion);

    return colGrupoProfesion;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(GrupoProfesion.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codGrupoProfesion ascending");

    Collection colGrupoProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoProfesion);

    return colGrupoProfesion;
  }
}