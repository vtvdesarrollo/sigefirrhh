package sigefirrhh.base.personal;

import java.io.Serializable;

public class TipoIdiomaPK
  implements Serializable
{
  public long idTipoIdioma;

  public TipoIdiomaPK()
  {
  }

  public TipoIdiomaPK(long idTipoIdioma)
  {
    this.idTipoIdioma = idTipoIdioma;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoIdiomaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoIdiomaPK thatPK)
  {
    return 
      this.idTipoIdioma == thatPK.idTipoIdioma;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoIdioma)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoIdioma);
  }
}