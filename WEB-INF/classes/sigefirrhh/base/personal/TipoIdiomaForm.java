package sigefirrhh.base.personal;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class TipoIdiomaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TipoIdiomaForm.class.getName());
  private TipoIdioma tipoIdioma;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showTipoIdiomaByCodTipoIdioma;
  private boolean showTipoIdiomaByDescripcion;
  private String findCodTipoIdioma;
  private String findDescripcion;
  private Object stateResultTipoIdiomaByCodTipoIdioma = null;

  private Object stateResultTipoIdiomaByDescripcion = null;

  public String getFindCodTipoIdioma()
  {
    return this.findCodTipoIdioma;
  }
  public void setFindCodTipoIdioma(String findCodTipoIdioma) {
    this.findCodTipoIdioma = findCodTipoIdioma;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public TipoIdioma getTipoIdioma() {
    if (this.tipoIdioma == null) {
      this.tipoIdioma = new TipoIdioma();
    }
    return this.tipoIdioma;
  }

  public TipoIdiomaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findTipoIdiomaByCodTipoIdioma()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findTipoIdiomaByCodTipoIdioma(this.findCodTipoIdioma);
      this.showTipoIdiomaByCodTipoIdioma = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoIdiomaByCodTipoIdioma)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoIdioma = null;
    this.findDescripcion = null;

    return null;
  }

  public String findTipoIdiomaByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findTipoIdiomaByDescripcion(this.findDescripcion);
      this.showTipoIdiomaByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoIdiomaByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoIdioma = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowTipoIdiomaByCodTipoIdioma() {
    return this.showTipoIdiomaByCodTipoIdioma;
  }
  public boolean isShowTipoIdiomaByDescripcion() {
    return this.showTipoIdiomaByDescripcion;
  }

  public String selectTipoIdioma()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTipoIdioma = 
      Long.parseLong((String)requestParameterMap.get("idTipoIdioma"));
    try
    {
      this.tipoIdioma = 
        this.personalFacade.findTipoIdiomaById(
        idTipoIdioma);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.tipoIdioma = null;
    this.showTipoIdiomaByCodTipoIdioma = false;
    this.showTipoIdiomaByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.personalFacade.addTipoIdioma(
          this.tipoIdioma);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.personalFacade.updateTipoIdioma(
          this.tipoIdioma);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.personalFacade.deleteTipoIdioma(
        this.tipoIdioma);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.tipoIdioma = new TipoIdioma();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.tipoIdioma.setIdTipoIdioma(identityGenerator.getNextSequenceNumber("sigefirrhh.base.personal.TipoIdioma"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.tipoIdioma = new TipoIdioma();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("TipoIdioma");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/personal");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "TipoIdioma" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}