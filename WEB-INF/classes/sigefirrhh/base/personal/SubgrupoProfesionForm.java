package sigefirrhh.base.personal;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class SubgrupoProfesionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SubgrupoProfesionForm.class.getName());
  private SubgrupoProfesion subgrupoProfesion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showSubgrupoProfesionByGrupoProfesion;
  private boolean showSubgrupoProfesionByCodSubgrupoProfesion;
  private boolean showSubgrupoProfesionByNombre;
  private String findSelectGrupoProfesion;
  private String findCodSubgrupoProfesion;
  private String findNombre;
  private Collection findColGrupoProfesion;
  private Collection colGrupoProfesion;
  private String selectGrupoProfesion;
  private Object stateResultSubgrupoProfesionByGrupoProfesion = null;

  private Object stateResultSubgrupoProfesionByCodSubgrupoProfesion = null;

  private Object stateResultSubgrupoProfesionByNombre = null;

  public String getFindSelectGrupoProfesion()
  {
    return this.findSelectGrupoProfesion;
  }
  public void setFindSelectGrupoProfesion(String valGrupoProfesion) {
    this.findSelectGrupoProfesion = valGrupoProfesion;
  }

  public Collection getFindColGrupoProfesion() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColGrupoProfesion.iterator();
    GrupoProfesion grupoProfesion = null;
    while (iterator.hasNext()) {
      grupoProfesion = (GrupoProfesion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoProfesion.getIdGrupoProfesion()), 
        grupoProfesion.toString()));
    }
    return col;
  }
  public String getFindCodSubgrupoProfesion() {
    return this.findCodSubgrupoProfesion;
  }
  public void setFindCodSubgrupoProfesion(String findCodSubgrupoProfesion) {
    this.findCodSubgrupoProfesion = findCodSubgrupoProfesion;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectGrupoProfesion()
  {
    return this.selectGrupoProfesion;
  }
  public void setSelectGrupoProfesion(String valGrupoProfesion) {
    Iterator iterator = this.colGrupoProfesion.iterator();
    GrupoProfesion grupoProfesion = null;
    this.subgrupoProfesion.setGrupoProfesion(null);
    while (iterator.hasNext()) {
      grupoProfesion = (GrupoProfesion)iterator.next();
      if (String.valueOf(grupoProfesion.getIdGrupoProfesion()).equals(
        valGrupoProfesion)) {
        this.subgrupoProfesion.setGrupoProfesion(
          grupoProfesion);
        break;
      }
    }
    this.selectGrupoProfesion = valGrupoProfesion;
  }
  public Collection getResult() {
    return this.result;
  }

  public SubgrupoProfesion getSubgrupoProfesion() {
    if (this.subgrupoProfesion == null) {
      this.subgrupoProfesion = new SubgrupoProfesion();
    }
    return this.subgrupoProfesion;
  }

  public SubgrupoProfesionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColGrupoProfesion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoProfesion.iterator();
    GrupoProfesion grupoProfesion = null;
    while (iterator.hasNext()) {
      grupoProfesion = (GrupoProfesion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoProfesion.getIdGrupoProfesion()), 
        grupoProfesion.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColGrupoProfesion = 
        this.personalFacade.findAllGrupoProfesion();

      this.colGrupoProfesion = 
        this.personalFacade.findAllGrupoProfesion();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSubgrupoProfesionByGrupoProfesion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findSubgrupoProfesionByGrupoProfesion(Long.valueOf(this.findSelectGrupoProfesion).longValue());
      this.showSubgrupoProfesionByGrupoProfesion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSubgrupoProfesionByGrupoProfesion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoProfesion = null;
    this.findCodSubgrupoProfesion = null;
    this.findNombre = null;

    return null;
  }

  public String findSubgrupoProfesionByCodSubgrupoProfesion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findSubgrupoProfesionByCodSubgrupoProfesion(this.findCodSubgrupoProfesion);
      this.showSubgrupoProfesionByCodSubgrupoProfesion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSubgrupoProfesionByCodSubgrupoProfesion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoProfesion = null;
    this.findCodSubgrupoProfesion = null;
    this.findNombre = null;

    return null;
  }

  public String findSubgrupoProfesionByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findSubgrupoProfesionByNombre(this.findNombre);
      this.showSubgrupoProfesionByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSubgrupoProfesionByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoProfesion = null;
    this.findCodSubgrupoProfesion = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowSubgrupoProfesionByGrupoProfesion() {
    return this.showSubgrupoProfesionByGrupoProfesion;
  }
  public boolean isShowSubgrupoProfesionByCodSubgrupoProfesion() {
    return this.showSubgrupoProfesionByCodSubgrupoProfesion;
  }
  public boolean isShowSubgrupoProfesionByNombre() {
    return this.showSubgrupoProfesionByNombre;
  }

  public String selectSubgrupoProfesion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectGrupoProfesion = null;

    long idSubgrupoProfesion = 
      Long.parseLong((String)requestParameterMap.get("idSubgrupoProfesion"));
    try
    {
      this.subgrupoProfesion = 
        this.personalFacade.findSubgrupoProfesionById(
        idSubgrupoProfesion);
      if (this.subgrupoProfesion.getGrupoProfesion() != null) {
        this.selectGrupoProfesion = 
          String.valueOf(this.subgrupoProfesion.getGrupoProfesion().getIdGrupoProfesion());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.subgrupoProfesion = null;
    this.showSubgrupoProfesionByGrupoProfesion = false;
    this.showSubgrupoProfesionByCodSubgrupoProfesion = false;
    this.showSubgrupoProfesionByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.personalFacade.addSubgrupoProfesion(
          this.subgrupoProfesion);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.personalFacade.updateSubgrupoProfesion(
          this.subgrupoProfesion);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.personalFacade.deleteSubgrupoProfesion(
        this.subgrupoProfesion);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.subgrupoProfesion = new SubgrupoProfesion();

    this.selectGrupoProfesion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.subgrupoProfesion.setIdSubgrupoProfesion(identityGenerator.getNextSequenceNumber("sigefirrhh.base.personal.SubgrupoProfesion"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.subgrupoProfesion = new SubgrupoProfesion();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("SubgrupoProfesion");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/personal");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "SubgrupoProfesion" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}