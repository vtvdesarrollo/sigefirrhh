package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class InstitucionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addInstitucion(Institucion institucion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Institucion institucionNew = 
      (Institucion)BeanUtils.cloneBean(
      institucion);

    pm.makePersistent(institucionNew);
  }

  public void updateInstitucion(Institucion institucion) throws Exception
  {
    Institucion institucionModify = 
      findInstitucionById(institucion.getIdInstitucion());

    BeanUtils.copyProperties(institucionModify, institucion);
  }

  public void deleteInstitucion(Institucion institucion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Institucion institucionDelete = 
      findInstitucionById(institucion.getIdInstitucion());
    pm.deletePersistent(institucionDelete);
  }

  public Institucion findInstitucionById(long idInstitucion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idInstitucion == pIdInstitucion";
    Query query = pm.newQuery(Institucion.class, filter);

    query.declareParameters("long pIdInstitucion");

    parameters.put("pIdInstitucion", new Long(idInstitucion));

    Collection colInstitucion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colInstitucion.iterator();
    return (Institucion)iterator.next();
  }

  public Collection findInstitucionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent institucionExtent = pm.getExtent(
      Institucion.class, true);
    Query query = pm.newQuery(institucionExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodInstitucion(String codInstitucion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codInstitucion == pCodInstitucion";

    Query query = pm.newQuery(Institucion.class, filter);

    query.declareParameters("java.lang.String pCodInstitucion");
    HashMap parameters = new HashMap();

    parameters.put("pCodInstitucion", new String(codInstitucion));

    query.setOrdering("nombre ascending");

    Collection colInstitucion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colInstitucion);

    return colInstitucion;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Institucion.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colInstitucion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colInstitucion);

    return colInstitucion;
  }
}