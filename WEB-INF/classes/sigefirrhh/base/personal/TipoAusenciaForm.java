package sigefirrhh.base.personal;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class TipoAusenciaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TipoAusenciaForm.class.getName());
  private TipoAusencia tipoAusencia;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showTipoAusenciaByCodTipoAusencia;
  private boolean showTipoAusenciaByDescripcion;
  private String findCodTipoAusencia;
  private String findDescripcion;
  private Object stateResultTipoAusenciaByCodTipoAusencia = null;

  private Object stateResultTipoAusenciaByDescripcion = null;

  public String getFindCodTipoAusencia()
  {
    return this.findCodTipoAusencia;
  }
  public void setFindCodTipoAusencia(String findCodTipoAusencia) {
    this.findCodTipoAusencia = findCodTipoAusencia;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public TipoAusencia getTipoAusencia() {
    if (this.tipoAusencia == null) {
      this.tipoAusencia = new TipoAusencia();
    }
    return this.tipoAusencia;
  }

  public TipoAusenciaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListClase()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoAusencia.LISTA_CLASE.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findTipoAusenciaByCodTipoAusencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findTipoAusenciaByCodTipoAusencia(this.findCodTipoAusencia);
      this.showTipoAusenciaByCodTipoAusencia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoAusenciaByCodTipoAusencia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoAusencia = null;
    this.findDescripcion = null;

    return null;
  }

  public String findTipoAusenciaByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findTipoAusenciaByDescripcion(this.findDescripcion);
      this.showTipoAusenciaByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoAusenciaByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoAusencia = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowTipoAusenciaByCodTipoAusencia() {
    return this.showTipoAusenciaByCodTipoAusencia;
  }
  public boolean isShowTipoAusenciaByDescripcion() {
    return this.showTipoAusenciaByDescripcion;
  }

  public String selectTipoAusencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTipoAusencia = 
      Long.parseLong((String)requestParameterMap.get("idTipoAusencia"));
    try
    {
      this.tipoAusencia = 
        this.personalFacade.findTipoAusenciaById(
        idTipoAusencia);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.tipoAusencia = null;
    this.showTipoAusenciaByCodTipoAusencia = false;
    this.showTipoAusenciaByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.personalFacade.addTipoAusencia(
          this.tipoAusencia);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.personalFacade.updateTipoAusencia(
          this.tipoAusencia);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar: " + e, ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.personalFacade.deleteTipoAusencia(
        this.tipoAusencia);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.tipoAusencia = new TipoAusencia();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.tipoAusencia.setIdTipoAusencia(identityGenerator.getNextSequenceNumber("sigefirrhh.base.personal.TipoAusencia"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.tipoAusencia = new TipoAusencia();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("TipoAusencia");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/personal");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "TipoAusencia" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}