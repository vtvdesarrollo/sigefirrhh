package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SubgrupoProfesionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSubgrupoProfesion(SubgrupoProfesion subgrupoProfesion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SubgrupoProfesion subgrupoProfesionNew = 
      (SubgrupoProfesion)BeanUtils.cloneBean(
      subgrupoProfesion);

    GrupoProfesionBeanBusiness grupoProfesionBeanBusiness = new GrupoProfesionBeanBusiness();

    if (subgrupoProfesionNew.getGrupoProfesion() != null) {
      subgrupoProfesionNew.setGrupoProfesion(
        grupoProfesionBeanBusiness.findGrupoProfesionById(
        subgrupoProfesionNew.getGrupoProfesion().getIdGrupoProfesion()));
    }
    pm.makePersistent(subgrupoProfesionNew);
  }

  public void updateSubgrupoProfesion(SubgrupoProfesion subgrupoProfesion) throws Exception
  {
    SubgrupoProfesion subgrupoProfesionModify = 
      findSubgrupoProfesionById(subgrupoProfesion.getIdSubgrupoProfesion());

    GrupoProfesionBeanBusiness grupoProfesionBeanBusiness = new GrupoProfesionBeanBusiness();

    if (subgrupoProfesion.getGrupoProfesion() != null) {
      subgrupoProfesion.setGrupoProfesion(
        grupoProfesionBeanBusiness.findGrupoProfesionById(
        subgrupoProfesion.getGrupoProfesion().getIdGrupoProfesion()));
    }

    BeanUtils.copyProperties(subgrupoProfesionModify, subgrupoProfesion);
  }

  public void deleteSubgrupoProfesion(SubgrupoProfesion subgrupoProfesion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SubgrupoProfesion subgrupoProfesionDelete = 
      findSubgrupoProfesionById(subgrupoProfesion.getIdSubgrupoProfesion());
    pm.deletePersistent(subgrupoProfesionDelete);
  }

  public SubgrupoProfesion findSubgrupoProfesionById(long idSubgrupoProfesion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSubgrupoProfesion == pIdSubgrupoProfesion";
    Query query = pm.newQuery(SubgrupoProfesion.class, filter);

    query.declareParameters("long pIdSubgrupoProfesion");

    parameters.put("pIdSubgrupoProfesion", new Long(idSubgrupoProfesion));

    Collection colSubgrupoProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSubgrupoProfesion.iterator();
    return (SubgrupoProfesion)iterator.next();
  }

  public Collection findSubgrupoProfesionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent subgrupoProfesionExtent = pm.getExtent(
      SubgrupoProfesion.class, true);
    Query query = pm.newQuery(subgrupoProfesionExtent);
    query.setOrdering("codSubgrupoProfesion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByGrupoProfesion(long idGrupoProfesion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoProfesion.idGrupoProfesion == pIdGrupoProfesion";

    Query query = pm.newQuery(SubgrupoProfesion.class, filter);

    query.declareParameters("long pIdGrupoProfesion");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoProfesion", new Long(idGrupoProfesion));

    query.setOrdering("codSubgrupoProfesion ascending");

    Collection colSubgrupoProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSubgrupoProfesion);

    return colSubgrupoProfesion;
  }

  public Collection findByCodSubgrupoProfesion(String codSubgrupoProfesion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codSubgrupoProfesion == pCodSubgrupoProfesion";

    Query query = pm.newQuery(SubgrupoProfesion.class, filter);

    query.declareParameters("java.lang.String pCodSubgrupoProfesion");
    HashMap parameters = new HashMap();

    parameters.put("pCodSubgrupoProfesion", new String(codSubgrupoProfesion));

    query.setOrdering("codSubgrupoProfesion ascending");

    Collection colSubgrupoProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSubgrupoProfesion);

    return colSubgrupoProfesion;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(SubgrupoProfesion.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codSubgrupoProfesion ascending");

    Collection colSubgrupoProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSubgrupoProfesion);

    return colSubgrupoProfesion;
  }
}