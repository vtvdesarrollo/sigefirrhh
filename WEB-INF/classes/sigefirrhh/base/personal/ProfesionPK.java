package sigefirrhh.base.personal;

import java.io.Serializable;

public class ProfesionPK
  implements Serializable
{
  public long idProfesion;

  public ProfesionPK()
  {
  }

  public ProfesionPK(long idProfesion)
  {
    this.idProfesion = idProfesion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ProfesionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ProfesionPK thatPK)
  {
    return 
      this.idProfesion == thatPK.idProfesion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idProfesion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idProfesion);
  }
}