package sigefirrhh.base.personal;

import eforserver.common.Resource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

public class ProfesionNoGenBeanBusiness
{
  public Collection findAll()
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    StringBuffer sql = new StringBuffer();

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    sql.append("select id_profesion, nombre ");
    sql.append(" from profesion");
    sql.append(" order by nombre");
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next()) {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_profesion"))));
        col.add(rsRegistros.getString("nombre"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}