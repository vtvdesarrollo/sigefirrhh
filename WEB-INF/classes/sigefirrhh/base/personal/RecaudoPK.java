package sigefirrhh.base.personal;

import java.io.Serializable;

public class RecaudoPK
  implements Serializable
{
  public long idRecaudo;

  public RecaudoPK()
  {
  }

  public RecaudoPK(long idRecaudo)
  {
    this.idRecaudo = idRecaudo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RecaudoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RecaudoPK thatPK)
  {
    return 
      this.idRecaudo == thatPK.idRecaudo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRecaudo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRecaudo);
  }
}