package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class CarreraArea
  implements Serializable, PersistenceCapable
{
  private long idCarreraArea;
  private AreaCarrera areaCarrera;
  private Carrera carrera;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "areaCarrera", "carrera", "idCarreraArea" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.personal.AreaCarrera"), sunjdo$classForName$("sigefirrhh.base.personal.Carrera"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 26, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcarrera(this).getNombre() + " - " + 
      jdoGetareaCarrera(this).getDescripcion();
  }

  public AreaCarrera getAreaCarrera()
  {
    return jdoGetareaCarrera(this);
  }

  public Carrera getCarrera()
  {
    return jdoGetcarrera(this);
  }

  public long getIdCarreraArea()
  {
    return jdoGetidCarreraArea(this);
  }

  public void setAreaCarrera(AreaCarrera carrera)
  {
    jdoSetareaCarrera(this, carrera);
  }

  public void setCarrera(Carrera carrera)
  {
    jdoSetcarrera(this, carrera);
  }

  public void setIdCarreraArea(long l)
  {
    jdoSetidCarreraArea(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.CarreraArea"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CarreraArea());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CarreraArea localCarreraArea = new CarreraArea();
    localCarreraArea.jdoFlags = 1;
    localCarreraArea.jdoStateManager = paramStateManager;
    return localCarreraArea;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CarreraArea localCarreraArea = new CarreraArea();
    localCarreraArea.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCarreraArea.jdoFlags = 1;
    localCarreraArea.jdoStateManager = paramStateManager;
    return localCarreraArea;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.areaCarrera);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.carrera);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCarreraArea);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.areaCarrera = ((AreaCarrera)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.carrera = ((Carrera)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCarreraArea = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CarreraArea paramCarreraArea, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCarreraArea == null)
        throw new IllegalArgumentException("arg1");
      this.areaCarrera = paramCarreraArea.areaCarrera;
      return;
    case 1:
      if (paramCarreraArea == null)
        throw new IllegalArgumentException("arg1");
      this.carrera = paramCarreraArea.carrera;
      return;
    case 2:
      if (paramCarreraArea == null)
        throw new IllegalArgumentException("arg1");
      this.idCarreraArea = paramCarreraArea.idCarreraArea;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CarreraArea))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CarreraArea localCarreraArea = (CarreraArea)paramObject;
    if (localCarreraArea.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCarreraArea, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CarreraAreaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CarreraAreaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CarreraAreaPK))
      throw new IllegalArgumentException("arg1");
    CarreraAreaPK localCarreraAreaPK = (CarreraAreaPK)paramObject;
    localCarreraAreaPK.idCarreraArea = this.idCarreraArea;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CarreraAreaPK))
      throw new IllegalArgumentException("arg1");
    CarreraAreaPK localCarreraAreaPK = (CarreraAreaPK)paramObject;
    this.idCarreraArea = localCarreraAreaPK.idCarreraArea;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CarreraAreaPK))
      throw new IllegalArgumentException("arg2");
    CarreraAreaPK localCarreraAreaPK = (CarreraAreaPK)paramObject;
    localCarreraAreaPK.idCarreraArea = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CarreraAreaPK))
      throw new IllegalArgumentException("arg2");
    CarreraAreaPK localCarreraAreaPK = (CarreraAreaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localCarreraAreaPK.idCarreraArea);
  }

  private static final AreaCarrera jdoGetareaCarrera(CarreraArea paramCarreraArea)
  {
    StateManager localStateManager = paramCarreraArea.jdoStateManager;
    if (localStateManager == null)
      return paramCarreraArea.areaCarrera;
    if (localStateManager.isLoaded(paramCarreraArea, jdoInheritedFieldCount + 0))
      return paramCarreraArea.areaCarrera;
    return (AreaCarrera)localStateManager.getObjectField(paramCarreraArea, jdoInheritedFieldCount + 0, paramCarreraArea.areaCarrera);
  }

  private static final void jdoSetareaCarrera(CarreraArea paramCarreraArea, AreaCarrera paramAreaCarrera)
  {
    StateManager localStateManager = paramCarreraArea.jdoStateManager;
    if (localStateManager == null)
    {
      paramCarreraArea.areaCarrera = paramAreaCarrera;
      return;
    }
    localStateManager.setObjectField(paramCarreraArea, jdoInheritedFieldCount + 0, paramCarreraArea.areaCarrera, paramAreaCarrera);
  }

  private static final Carrera jdoGetcarrera(CarreraArea paramCarreraArea)
  {
    StateManager localStateManager = paramCarreraArea.jdoStateManager;
    if (localStateManager == null)
      return paramCarreraArea.carrera;
    if (localStateManager.isLoaded(paramCarreraArea, jdoInheritedFieldCount + 1))
      return paramCarreraArea.carrera;
    return (Carrera)localStateManager.getObjectField(paramCarreraArea, jdoInheritedFieldCount + 1, paramCarreraArea.carrera);
  }

  private static final void jdoSetcarrera(CarreraArea paramCarreraArea, Carrera paramCarrera)
  {
    StateManager localStateManager = paramCarreraArea.jdoStateManager;
    if (localStateManager == null)
    {
      paramCarreraArea.carrera = paramCarrera;
      return;
    }
    localStateManager.setObjectField(paramCarreraArea, jdoInheritedFieldCount + 1, paramCarreraArea.carrera, paramCarrera);
  }

  private static final long jdoGetidCarreraArea(CarreraArea paramCarreraArea)
  {
    return paramCarreraArea.idCarreraArea;
  }

  private static final void jdoSetidCarreraArea(CarreraArea paramCarreraArea, long paramLong)
  {
    StateManager localStateManager = paramCarreraArea.jdoStateManager;
    if (localStateManager == null)
    {
      paramCarreraArea.idCarreraArea = paramLong;
      return;
    }
    localStateManager.setLongField(paramCarreraArea, jdoInheritedFieldCount + 2, paramCarreraArea.idCarreraArea, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}