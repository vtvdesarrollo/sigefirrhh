package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Gremio
  implements Serializable, PersistenceCapable
{
  private long idGremio;
  private String codGremio;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codGremio", "idGremio", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + 
      jdoGetcodGremio(this);
  }

  public String getCodGremio()
  {
    return jdoGetcodGremio(this);
  }

  public long getIdGremio()
  {
    return jdoGetidGremio(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodGremio(String string)
  {
    jdoSetcodGremio(this, string);
  }

  public void setIdGremio(long l)
  {
    jdoSetidGremio(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.Gremio"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Gremio());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Gremio localGremio = new Gremio();
    localGremio.jdoFlags = 1;
    localGremio.jdoStateManager = paramStateManager;
    return localGremio;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Gremio localGremio = new Gremio();
    localGremio.jdoCopyKeyFieldsFromObjectId(paramObject);
    localGremio.jdoFlags = 1;
    localGremio.jdoStateManager = paramStateManager;
    return localGremio;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codGremio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idGremio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codGremio = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idGremio = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Gremio paramGremio, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramGremio == null)
        throw new IllegalArgumentException("arg1");
      this.codGremio = paramGremio.codGremio;
      return;
    case 1:
      if (paramGremio == null)
        throw new IllegalArgumentException("arg1");
      this.idGremio = paramGremio.idGremio;
      return;
    case 2:
      if (paramGremio == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramGremio.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Gremio))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Gremio localGremio = (Gremio)paramObject;
    if (localGremio.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localGremio, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new GremioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new GremioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GremioPK))
      throw new IllegalArgumentException("arg1");
    GremioPK localGremioPK = (GremioPK)paramObject;
    localGremioPK.idGremio = this.idGremio;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GremioPK))
      throw new IllegalArgumentException("arg1");
    GremioPK localGremioPK = (GremioPK)paramObject;
    this.idGremio = localGremioPK.idGremio;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GremioPK))
      throw new IllegalArgumentException("arg2");
    GremioPK localGremioPK = (GremioPK)paramObject;
    localGremioPK.idGremio = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GremioPK))
      throw new IllegalArgumentException("arg2");
    GremioPK localGremioPK = (GremioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localGremioPK.idGremio);
  }

  private static final String jdoGetcodGremio(Gremio paramGremio)
  {
    if (paramGremio.jdoFlags <= 0)
      return paramGremio.codGremio;
    StateManager localStateManager = paramGremio.jdoStateManager;
    if (localStateManager == null)
      return paramGremio.codGremio;
    if (localStateManager.isLoaded(paramGremio, jdoInheritedFieldCount + 0))
      return paramGremio.codGremio;
    return localStateManager.getStringField(paramGremio, jdoInheritedFieldCount + 0, paramGremio.codGremio);
  }

  private static final void jdoSetcodGremio(Gremio paramGremio, String paramString)
  {
    if (paramGremio.jdoFlags == 0)
    {
      paramGremio.codGremio = paramString;
      return;
    }
    StateManager localStateManager = paramGremio.jdoStateManager;
    if (localStateManager == null)
    {
      paramGremio.codGremio = paramString;
      return;
    }
    localStateManager.setStringField(paramGremio, jdoInheritedFieldCount + 0, paramGremio.codGremio, paramString);
  }

  private static final long jdoGetidGremio(Gremio paramGremio)
  {
    return paramGremio.idGremio;
  }

  private static final void jdoSetidGremio(Gremio paramGremio, long paramLong)
  {
    StateManager localStateManager = paramGremio.jdoStateManager;
    if (localStateManager == null)
    {
      paramGremio.idGremio = paramLong;
      return;
    }
    localStateManager.setLongField(paramGremio, jdoInheritedFieldCount + 1, paramGremio.idGremio, paramLong);
  }

  private static final String jdoGetnombre(Gremio paramGremio)
  {
    if (paramGremio.jdoFlags <= 0)
      return paramGremio.nombre;
    StateManager localStateManager = paramGremio.jdoStateManager;
    if (localStateManager == null)
      return paramGremio.nombre;
    if (localStateManager.isLoaded(paramGremio, jdoInheritedFieldCount + 2))
      return paramGremio.nombre;
    return localStateManager.getStringField(paramGremio, jdoInheritedFieldCount + 2, paramGremio.nombre);
  }

  private static final void jdoSetnombre(Gremio paramGremio, String paramString)
  {
    if (paramGremio.jdoFlags == 0)
    {
      paramGremio.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramGremio.jdoStateManager;
    if (localStateManager == null)
    {
      paramGremio.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramGremio, jdoInheritedFieldCount + 2, paramGremio.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}