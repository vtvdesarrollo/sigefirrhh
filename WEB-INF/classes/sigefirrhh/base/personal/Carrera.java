package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Carrera
  implements Serializable, PersistenceCapable
{
  private long idCarrera;
  private String codCarrera;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codCarrera", "idCarrera", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + 
      jdoGetcodCarrera(this);
  }

  public String getCodCarrera()
  {
    return jdoGetcodCarrera(this);
  }

  public long getIdCarrera()
  {
    return jdoGetidCarrera(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodCarrera(String string)
  {
    jdoSetcodCarrera(this, string);
  }

  public void setIdCarrera(long l)
  {
    jdoSetidCarrera(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.Carrera"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Carrera());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Carrera localCarrera = new Carrera();
    localCarrera.jdoFlags = 1;
    localCarrera.jdoStateManager = paramStateManager;
    return localCarrera;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Carrera localCarrera = new Carrera();
    localCarrera.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCarrera.jdoFlags = 1;
    localCarrera.jdoStateManager = paramStateManager;
    return localCarrera;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCarrera);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCarrera);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCarrera = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCarrera = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Carrera paramCarrera, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCarrera == null)
        throw new IllegalArgumentException("arg1");
      this.codCarrera = paramCarrera.codCarrera;
      return;
    case 1:
      if (paramCarrera == null)
        throw new IllegalArgumentException("arg1");
      this.idCarrera = paramCarrera.idCarrera;
      return;
    case 2:
      if (paramCarrera == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramCarrera.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Carrera))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Carrera localCarrera = (Carrera)paramObject;
    if (localCarrera.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCarrera, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CarreraPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CarreraPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CarreraPK))
      throw new IllegalArgumentException("arg1");
    CarreraPK localCarreraPK = (CarreraPK)paramObject;
    localCarreraPK.idCarrera = this.idCarrera;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CarreraPK))
      throw new IllegalArgumentException("arg1");
    CarreraPK localCarreraPK = (CarreraPK)paramObject;
    this.idCarrera = localCarreraPK.idCarrera;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CarreraPK))
      throw new IllegalArgumentException("arg2");
    CarreraPK localCarreraPK = (CarreraPK)paramObject;
    localCarreraPK.idCarrera = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CarreraPK))
      throw new IllegalArgumentException("arg2");
    CarreraPK localCarreraPK = (CarreraPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localCarreraPK.idCarrera);
  }

  private static final String jdoGetcodCarrera(Carrera paramCarrera)
  {
    if (paramCarrera.jdoFlags <= 0)
      return paramCarrera.codCarrera;
    StateManager localStateManager = paramCarrera.jdoStateManager;
    if (localStateManager == null)
      return paramCarrera.codCarrera;
    if (localStateManager.isLoaded(paramCarrera, jdoInheritedFieldCount + 0))
      return paramCarrera.codCarrera;
    return localStateManager.getStringField(paramCarrera, jdoInheritedFieldCount + 0, paramCarrera.codCarrera);
  }

  private static final void jdoSetcodCarrera(Carrera paramCarrera, String paramString)
  {
    if (paramCarrera.jdoFlags == 0)
    {
      paramCarrera.codCarrera = paramString;
      return;
    }
    StateManager localStateManager = paramCarrera.jdoStateManager;
    if (localStateManager == null)
    {
      paramCarrera.codCarrera = paramString;
      return;
    }
    localStateManager.setStringField(paramCarrera, jdoInheritedFieldCount + 0, paramCarrera.codCarrera, paramString);
  }

  private static final long jdoGetidCarrera(Carrera paramCarrera)
  {
    return paramCarrera.idCarrera;
  }

  private static final void jdoSetidCarrera(Carrera paramCarrera, long paramLong)
  {
    StateManager localStateManager = paramCarrera.jdoStateManager;
    if (localStateManager == null)
    {
      paramCarrera.idCarrera = paramLong;
      return;
    }
    localStateManager.setLongField(paramCarrera, jdoInheritedFieldCount + 1, paramCarrera.idCarrera, paramLong);
  }

  private static final String jdoGetnombre(Carrera paramCarrera)
  {
    if (paramCarrera.jdoFlags <= 0)
      return paramCarrera.nombre;
    StateManager localStateManager = paramCarrera.jdoStateManager;
    if (localStateManager == null)
      return paramCarrera.nombre;
    if (localStateManager.isLoaded(paramCarrera, jdoInheritedFieldCount + 2))
      return paramCarrera.nombre;
    return localStateManager.getStringField(paramCarrera, jdoInheritedFieldCount + 2, paramCarrera.nombre);
  }

  private static final void jdoSetnombre(Carrera paramCarrera, String paramString)
  {
    if (paramCarrera.jdoFlags == 0)
    {
      paramCarrera.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramCarrera.jdoStateManager;
    if (localStateManager == null)
    {
      paramCarrera.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramCarrera, jdoInheritedFieldCount + 2, paramCarrera.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}