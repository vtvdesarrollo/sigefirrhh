package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoHabilidad
  implements Serializable, PersistenceCapable
{
  private long idTipoHabilidad;
  private String codTipoHabilidad;
  private String descripcion;
  private String detalle;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoHabilidad", "descripcion", "detalle", "idTipoHabilidad" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodTipoHabilidad(this);
  }

  public String getCodTipoHabilidad()
  {
    return jdoGetcodTipoHabilidad(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public String getDetalle()
  {
    return jdoGetdetalle(this);
  }

  public long getIdTipoHabilidad()
  {
    return jdoGetidTipoHabilidad(this);
  }

  public void setCodTipoHabilidad(String string)
  {
    jdoSetcodTipoHabilidad(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setDetalle(String string)
  {
    jdoSetdetalle(this, string);
  }

  public void setIdTipoHabilidad(long l)
  {
    jdoSetidTipoHabilidad(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.TipoHabilidad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoHabilidad());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoHabilidad localTipoHabilidad = new TipoHabilidad();
    localTipoHabilidad.jdoFlags = 1;
    localTipoHabilidad.jdoStateManager = paramStateManager;
    return localTipoHabilidad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoHabilidad localTipoHabilidad = new TipoHabilidad();
    localTipoHabilidad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoHabilidad.jdoFlags = 1;
    localTipoHabilidad.jdoStateManager = paramStateManager;
    return localTipoHabilidad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoHabilidad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.detalle);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoHabilidad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoHabilidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.detalle = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoHabilidad = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoHabilidad paramTipoHabilidad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoHabilidad = paramTipoHabilidad.codTipoHabilidad;
      return;
    case 1:
      if (paramTipoHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTipoHabilidad.descripcion;
      return;
    case 2:
      if (paramTipoHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.detalle = paramTipoHabilidad.detalle;
      return;
    case 3:
      if (paramTipoHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoHabilidad = paramTipoHabilidad.idTipoHabilidad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoHabilidad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoHabilidad localTipoHabilidad = (TipoHabilidad)paramObject;
    if (localTipoHabilidad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoHabilidad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoHabilidadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoHabilidadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoHabilidadPK))
      throw new IllegalArgumentException("arg1");
    TipoHabilidadPK localTipoHabilidadPK = (TipoHabilidadPK)paramObject;
    localTipoHabilidadPK.idTipoHabilidad = this.idTipoHabilidad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoHabilidadPK))
      throw new IllegalArgumentException("arg1");
    TipoHabilidadPK localTipoHabilidadPK = (TipoHabilidadPK)paramObject;
    this.idTipoHabilidad = localTipoHabilidadPK.idTipoHabilidad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoHabilidadPK))
      throw new IllegalArgumentException("arg2");
    TipoHabilidadPK localTipoHabilidadPK = (TipoHabilidadPK)paramObject;
    localTipoHabilidadPK.idTipoHabilidad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoHabilidadPK))
      throw new IllegalArgumentException("arg2");
    TipoHabilidadPK localTipoHabilidadPK = (TipoHabilidadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localTipoHabilidadPK.idTipoHabilidad);
  }

  private static final String jdoGetcodTipoHabilidad(TipoHabilidad paramTipoHabilidad)
  {
    if (paramTipoHabilidad.jdoFlags <= 0)
      return paramTipoHabilidad.codTipoHabilidad;
    StateManager localStateManager = paramTipoHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramTipoHabilidad.codTipoHabilidad;
    if (localStateManager.isLoaded(paramTipoHabilidad, jdoInheritedFieldCount + 0))
      return paramTipoHabilidad.codTipoHabilidad;
    return localStateManager.getStringField(paramTipoHabilidad, jdoInheritedFieldCount + 0, paramTipoHabilidad.codTipoHabilidad);
  }

  private static final void jdoSetcodTipoHabilidad(TipoHabilidad paramTipoHabilidad, String paramString)
  {
    if (paramTipoHabilidad.jdoFlags == 0)
    {
      paramTipoHabilidad.codTipoHabilidad = paramString;
      return;
    }
    StateManager localStateManager = paramTipoHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoHabilidad.codTipoHabilidad = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoHabilidad, jdoInheritedFieldCount + 0, paramTipoHabilidad.codTipoHabilidad, paramString);
  }

  private static final String jdoGetdescripcion(TipoHabilidad paramTipoHabilidad)
  {
    if (paramTipoHabilidad.jdoFlags <= 0)
      return paramTipoHabilidad.descripcion;
    StateManager localStateManager = paramTipoHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramTipoHabilidad.descripcion;
    if (localStateManager.isLoaded(paramTipoHabilidad, jdoInheritedFieldCount + 1))
      return paramTipoHabilidad.descripcion;
    return localStateManager.getStringField(paramTipoHabilidad, jdoInheritedFieldCount + 1, paramTipoHabilidad.descripcion);
  }

  private static final void jdoSetdescripcion(TipoHabilidad paramTipoHabilidad, String paramString)
  {
    if (paramTipoHabilidad.jdoFlags == 0)
    {
      paramTipoHabilidad.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoHabilidad.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoHabilidad, jdoInheritedFieldCount + 1, paramTipoHabilidad.descripcion, paramString);
  }

  private static final String jdoGetdetalle(TipoHabilidad paramTipoHabilidad)
  {
    if (paramTipoHabilidad.jdoFlags <= 0)
      return paramTipoHabilidad.detalle;
    StateManager localStateManager = paramTipoHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramTipoHabilidad.detalle;
    if (localStateManager.isLoaded(paramTipoHabilidad, jdoInheritedFieldCount + 2))
      return paramTipoHabilidad.detalle;
    return localStateManager.getStringField(paramTipoHabilidad, jdoInheritedFieldCount + 2, paramTipoHabilidad.detalle);
  }

  private static final void jdoSetdetalle(TipoHabilidad paramTipoHabilidad, String paramString)
  {
    if (paramTipoHabilidad.jdoFlags == 0)
    {
      paramTipoHabilidad.detalle = paramString;
      return;
    }
    StateManager localStateManager = paramTipoHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoHabilidad.detalle = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoHabilidad, jdoInheritedFieldCount + 2, paramTipoHabilidad.detalle, paramString);
  }

  private static final long jdoGetidTipoHabilidad(TipoHabilidad paramTipoHabilidad)
  {
    return paramTipoHabilidad.idTipoHabilidad;
  }

  private static final void jdoSetidTipoHabilidad(TipoHabilidad paramTipoHabilidad, long paramLong)
  {
    StateManager localStateManager = paramTipoHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoHabilidad.idTipoHabilidad = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoHabilidad, jdoInheritedFieldCount + 3, paramTipoHabilidad.idTipoHabilidad, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}