package sigefirrhh.base.personal;

import java.io.Serializable;

public class AreaCarreraPK
  implements Serializable
{
  public long idAreaCarrera;

  public AreaCarreraPK()
  {
  }

  public AreaCarreraPK(long idAreaCarrera)
  {
    this.idAreaCarrera = idAreaCarrera;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AreaCarreraPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AreaCarreraPK thatPK)
  {
    return 
      this.idAreaCarrera == thatPK.idAreaCarrera;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAreaCarrera)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAreaCarrera);
  }
}