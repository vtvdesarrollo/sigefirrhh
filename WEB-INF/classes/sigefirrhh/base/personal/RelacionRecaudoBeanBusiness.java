package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class RelacionRecaudoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRelacionRecaudo(RelacionRecaudo relacionRecaudo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RelacionRecaudo relacionRecaudoNew = 
      (RelacionRecaudo)BeanUtils.cloneBean(
      relacionRecaudo);

    RecaudoBeanBusiness recaudoBeanBusiness = new RecaudoBeanBusiness();

    if (relacionRecaudoNew.getRecaudo() != null) {
      relacionRecaudoNew.setRecaudo(
        recaudoBeanBusiness.findRecaudoById(
        relacionRecaudoNew.getRecaudo().getIdRecaudo()));
    }
    pm.makePersistent(relacionRecaudoNew);
  }

  public void updateRelacionRecaudo(RelacionRecaudo relacionRecaudo) throws Exception
  {
    RelacionRecaudo relacionRecaudoModify = 
      findRelacionRecaudoById(relacionRecaudo.getIdRelacionRecaudo());

    RecaudoBeanBusiness recaudoBeanBusiness = new RecaudoBeanBusiness();

    if (relacionRecaudo.getRecaudo() != null) {
      relacionRecaudo.setRecaudo(
        recaudoBeanBusiness.findRecaudoById(
        relacionRecaudo.getRecaudo().getIdRecaudo()));
    }

    BeanUtils.copyProperties(relacionRecaudoModify, relacionRecaudo);
  }

  public void deleteRelacionRecaudo(RelacionRecaudo relacionRecaudo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RelacionRecaudo relacionRecaudoDelete = 
      findRelacionRecaudoById(relacionRecaudo.getIdRelacionRecaudo());
    pm.deletePersistent(relacionRecaudoDelete);
  }

  public RelacionRecaudo findRelacionRecaudoById(long idRelacionRecaudo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRelacionRecaudo == pIdRelacionRecaudo";
    Query query = pm.newQuery(RelacionRecaudo.class, filter);

    query.declareParameters("long pIdRelacionRecaudo");

    parameters.put("pIdRelacionRecaudo", new Long(idRelacionRecaudo));

    Collection colRelacionRecaudo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRelacionRecaudo.iterator();
    return (RelacionRecaudo)iterator.next();
  }

  public Collection findRelacionRecaudoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent relacionRecaudoExtent = pm.getExtent(
      RelacionRecaudo.class, true);
    Query query = pm.newQuery(relacionRecaudoExtent);
    query.setOrdering("recaudo.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByEvento(int evento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "evento == pEvento";

    Query query = pm.newQuery(RelacionRecaudo.class, filter);

    query.declareParameters("int pEvento");
    HashMap parameters = new HashMap();

    parameters.put("pEvento", new Integer(evento));

    query.setOrdering("recaudo.descripcion ascending");

    Collection colRelacionRecaudo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRelacionRecaudo);

    return colRelacionRecaudo;
  }
}