package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoAmonestacion
  implements Serializable, PersistenceCapable
{
  private long idTipoAmonestacion;
  private String codTipoAmonestacion;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoAmonestacion", "descripcion", "idTipoAmonestacion" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodTipoAmonestacion(this);
  }

  public String getCodTipoAmonestacion()
  {
    return jdoGetcodTipoAmonestacion(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdTipoAmonestacion()
  {
    return jdoGetidTipoAmonestacion(this);
  }

  public void setCodTipoAmonestacion(String string)
  {
    jdoSetcodTipoAmonestacion(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdTipoAmonestacion(long l)
  {
    jdoSetidTipoAmonestacion(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.TipoAmonestacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoAmonestacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoAmonestacion localTipoAmonestacion = new TipoAmonestacion();
    localTipoAmonestacion.jdoFlags = 1;
    localTipoAmonestacion.jdoStateManager = paramStateManager;
    return localTipoAmonestacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoAmonestacion localTipoAmonestacion = new TipoAmonestacion();
    localTipoAmonestacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoAmonestacion.jdoFlags = 1;
    localTipoAmonestacion.jdoStateManager = paramStateManager;
    return localTipoAmonestacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoAmonestacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoAmonestacion);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoAmonestacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoAmonestacion = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoAmonestacion paramTipoAmonestacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoAmonestacion == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoAmonestacion = paramTipoAmonestacion.codTipoAmonestacion;
      return;
    case 1:
      if (paramTipoAmonestacion == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTipoAmonestacion.descripcion;
      return;
    case 2:
      if (paramTipoAmonestacion == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoAmonestacion = paramTipoAmonestacion.idTipoAmonestacion;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoAmonestacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoAmonestacion localTipoAmonestacion = (TipoAmonestacion)paramObject;
    if (localTipoAmonestacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoAmonestacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoAmonestacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoAmonestacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoAmonestacionPK))
      throw new IllegalArgumentException("arg1");
    TipoAmonestacionPK localTipoAmonestacionPK = (TipoAmonestacionPK)paramObject;
    localTipoAmonestacionPK.idTipoAmonestacion = this.idTipoAmonestacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoAmonestacionPK))
      throw new IllegalArgumentException("arg1");
    TipoAmonestacionPK localTipoAmonestacionPK = (TipoAmonestacionPK)paramObject;
    this.idTipoAmonestacion = localTipoAmonestacionPK.idTipoAmonestacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoAmonestacionPK))
      throw new IllegalArgumentException("arg2");
    TipoAmonestacionPK localTipoAmonestacionPK = (TipoAmonestacionPK)paramObject;
    localTipoAmonestacionPK.idTipoAmonestacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoAmonestacionPK))
      throw new IllegalArgumentException("arg2");
    TipoAmonestacionPK localTipoAmonestacionPK = (TipoAmonestacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localTipoAmonestacionPK.idTipoAmonestacion);
  }

  private static final String jdoGetcodTipoAmonestacion(TipoAmonestacion paramTipoAmonestacion)
  {
    if (paramTipoAmonestacion.jdoFlags <= 0)
      return paramTipoAmonestacion.codTipoAmonestacion;
    StateManager localStateManager = paramTipoAmonestacion.jdoStateManager;
    if (localStateManager == null)
      return paramTipoAmonestacion.codTipoAmonestacion;
    if (localStateManager.isLoaded(paramTipoAmonestacion, jdoInheritedFieldCount + 0))
      return paramTipoAmonestacion.codTipoAmonestacion;
    return localStateManager.getStringField(paramTipoAmonestacion, jdoInheritedFieldCount + 0, paramTipoAmonestacion.codTipoAmonestacion);
  }

  private static final void jdoSetcodTipoAmonestacion(TipoAmonestacion paramTipoAmonestacion, String paramString)
  {
    if (paramTipoAmonestacion.jdoFlags == 0)
    {
      paramTipoAmonestacion.codTipoAmonestacion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoAmonestacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAmonestacion.codTipoAmonestacion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoAmonestacion, jdoInheritedFieldCount + 0, paramTipoAmonestacion.codTipoAmonestacion, paramString);
  }

  private static final String jdoGetdescripcion(TipoAmonestacion paramTipoAmonestacion)
  {
    if (paramTipoAmonestacion.jdoFlags <= 0)
      return paramTipoAmonestacion.descripcion;
    StateManager localStateManager = paramTipoAmonestacion.jdoStateManager;
    if (localStateManager == null)
      return paramTipoAmonestacion.descripcion;
    if (localStateManager.isLoaded(paramTipoAmonestacion, jdoInheritedFieldCount + 1))
      return paramTipoAmonestacion.descripcion;
    return localStateManager.getStringField(paramTipoAmonestacion, jdoInheritedFieldCount + 1, paramTipoAmonestacion.descripcion);
  }

  private static final void jdoSetdescripcion(TipoAmonestacion paramTipoAmonestacion, String paramString)
  {
    if (paramTipoAmonestacion.jdoFlags == 0)
    {
      paramTipoAmonestacion.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoAmonestacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAmonestacion.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoAmonestacion, jdoInheritedFieldCount + 1, paramTipoAmonestacion.descripcion, paramString);
  }

  private static final long jdoGetidTipoAmonestacion(TipoAmonestacion paramTipoAmonestacion)
  {
    return paramTipoAmonestacion.idTipoAmonestacion;
  }

  private static final void jdoSetidTipoAmonestacion(TipoAmonestacion paramTipoAmonestacion, long paramLong)
  {
    StateManager localStateManager = paramTipoAmonestacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAmonestacion.idTipoAmonestacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoAmonestacion, jdoInheritedFieldCount + 2, paramTipoAmonestacion.idTipoAmonestacion, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}