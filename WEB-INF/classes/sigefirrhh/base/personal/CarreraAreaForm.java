package sigefirrhh.base.personal;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class CarreraAreaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CarreraAreaForm.class.getName());
  private CarreraArea carreraArea;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showCarreraAreaByAreaCarrera;
  private boolean showCarreraAreaByCarrera;
  private String findSelectAreaCarrera;
  private String findSelectCarrera;
  private Collection findColAreaCarrera;
  private Collection findColCarrera;
  private Collection colAreaCarrera;
  private Collection colCarrera;
  private String selectAreaCarrera;
  private String selectCarrera;
  private Object stateResultCarreraAreaByAreaCarrera = null;

  private Object stateResultCarreraAreaByCarrera = null;

  public String getFindSelectAreaCarrera()
  {
    return this.findSelectAreaCarrera;
  }
  public void setFindSelectAreaCarrera(String valAreaCarrera) {
    this.findSelectAreaCarrera = valAreaCarrera;
  }

  public Collection getFindColAreaCarrera() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColAreaCarrera.iterator();
    AreaCarrera areaCarrera = null;
    while (iterator.hasNext()) {
      areaCarrera = (AreaCarrera)iterator.next();
      col.add(new SelectItem(
        String.valueOf(areaCarrera.getIdAreaCarrera()), 
        areaCarrera.toString()));
    }
    return col;
  }
  public String getFindSelectCarrera() {
    return this.findSelectCarrera;
  }
  public void setFindSelectCarrera(String valCarrera) {
    this.findSelectCarrera = valCarrera;
  }

  public Collection getFindColCarrera() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCarrera.iterator();
    Carrera carrera = null;
    while (iterator.hasNext()) {
      carrera = (Carrera)iterator.next();
      col.add(new SelectItem(
        String.valueOf(carrera.getIdCarrera()), 
        carrera.toString()));
    }
    return col;
  }

  public String getSelectAreaCarrera()
  {
    return this.selectAreaCarrera;
  }
  public void setSelectAreaCarrera(String valAreaCarrera) {
    Iterator iterator = this.colAreaCarrera.iterator();
    AreaCarrera areaCarrera = null;
    this.carreraArea.setAreaCarrera(null);
    while (iterator.hasNext()) {
      areaCarrera = (AreaCarrera)iterator.next();
      if (String.valueOf(areaCarrera.getIdAreaCarrera()).equals(
        valAreaCarrera)) {
        this.carreraArea.setAreaCarrera(
          areaCarrera);
        break;
      }
    }
    this.selectAreaCarrera = valAreaCarrera;
  }
  public String getSelectCarrera() {
    return this.selectCarrera;
  }
  public void setSelectCarrera(String valCarrera) {
    Iterator iterator = this.colCarrera.iterator();
    Carrera carrera = null;
    this.carreraArea.setCarrera(null);
    while (iterator.hasNext()) {
      carrera = (Carrera)iterator.next();
      if (String.valueOf(carrera.getIdCarrera()).equals(
        valCarrera)) {
        this.carreraArea.setCarrera(
          carrera);
        break;
      }
    }
    this.selectCarrera = valCarrera;
  }
  public Collection getResult() {
    return this.result;
  }

  public CarreraArea getCarreraArea() {
    if (this.carreraArea == null) {
      this.carreraArea = new CarreraArea();
    }
    return this.carreraArea;
  }

  public CarreraAreaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColAreaCarrera()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAreaCarrera.iterator();
    AreaCarrera areaCarrera = null;
    while (iterator.hasNext()) {
      areaCarrera = (AreaCarrera)iterator.next();
      col.add(new SelectItem(
        String.valueOf(areaCarrera.getIdAreaCarrera()), 
        areaCarrera.toString()));
    }
    return col;
  }

  public Collection getColCarrera()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCarrera.iterator();
    Carrera carrera = null;
    while (iterator.hasNext()) {
      carrera = (Carrera)iterator.next();
      col.add(new SelectItem(
        String.valueOf(carrera.getIdCarrera()), 
        carrera.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColAreaCarrera = 
        this.personalFacade.findAllAreaCarrera();
      this.findColCarrera = 
        this.personalFacade.findAllCarrera();

      this.colAreaCarrera = 
        this.personalFacade.findAllAreaCarrera();
      this.colCarrera = 
        this.personalFacade.findAllCarrera();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findCarreraAreaByAreaCarrera()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findCarreraAreaByAreaCarrera(Long.valueOf(this.findSelectAreaCarrera).longValue());
      this.showCarreraAreaByAreaCarrera = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCarreraAreaByAreaCarrera)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectAreaCarrera = null;
    this.findSelectCarrera = null;

    return null;
  }

  public String findCarreraAreaByCarrera()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.personalFacade.findCarreraAreaByCarrera(Long.valueOf(this.findSelectCarrera).longValue());
      this.showCarreraAreaByCarrera = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCarreraAreaByCarrera)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectAreaCarrera = null;
    this.findSelectCarrera = null;

    return null;
  }

  public boolean isShowCarreraAreaByAreaCarrera() {
    return this.showCarreraAreaByAreaCarrera;
  }
  public boolean isShowCarreraAreaByCarrera() {
    return this.showCarreraAreaByCarrera;
  }

  public String selectCarreraArea()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectAreaCarrera = null;
    this.selectCarrera = null;

    long idCarreraArea = 
      Long.parseLong((String)requestParameterMap.get("idCarreraArea"));
    try
    {
      this.carreraArea = 
        this.personalFacade.findCarreraAreaById(
        idCarreraArea);
      if (this.carreraArea.getAreaCarrera() != null) {
        this.selectAreaCarrera = 
          String.valueOf(this.carreraArea.getAreaCarrera().getIdAreaCarrera());
      }
      if (this.carreraArea.getCarrera() != null) {
        this.selectCarrera = 
          String.valueOf(this.carreraArea.getCarrera().getIdCarrera());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.carreraArea = null;
    this.showCarreraAreaByAreaCarrera = false;
    this.showCarreraAreaByCarrera = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.personalFacade.addCarreraArea(
          this.carreraArea);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.personalFacade.updateCarreraArea(
          this.carreraArea);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.personalFacade.deleteCarreraArea(
        this.carreraArea);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.carreraArea = new CarreraArea();

    this.selectAreaCarrera = null;

    this.selectCarrera = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.carreraArea.setIdCarreraArea(identityGenerator.getNextSequenceNumber("sigefirrhh.base.personal.CarreraArea"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.carreraArea = new CarreraArea();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("CarreraArea");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/personal");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "CarreraArea" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}