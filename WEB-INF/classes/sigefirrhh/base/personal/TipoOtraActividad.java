package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoOtraActividad
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  private long idTipoOtraActividad;
  private String codOtraActividad;
  private String descripcion;
  private String tipo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codOtraActividad", "descripcion", "idTipoOtraActividad", "tipo" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.TipoOtraActividad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoOtraActividad());

    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_TIPO.put("D", "DEPORTE");
    LISTA_TIPO.put("H", "HOBBIE");
    LISTA_TIPO.put("C", "CULTURAL");
    LISTA_TIPO.put("S", "SOCIAL");
  }

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodOtraActividad(this);
  }

  public String getCodOtraActividad()
  {
    return jdoGetcodOtraActividad(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdTipoOtraActividad()
  {
    return jdoGetidTipoOtraActividad(this);
  }

  public String getTipo()
  {
    return jdoGettipo(this);
  }

  public void setCodOtraActividad(String string)
  {
    jdoSetcodOtraActividad(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdTipoOtraActividad(long l)
  {
    jdoSetidTipoOtraActividad(this, l);
  }

  public void setTipo(String string)
  {
    jdoSettipo(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoOtraActividad localTipoOtraActividad = new TipoOtraActividad();
    localTipoOtraActividad.jdoFlags = 1;
    localTipoOtraActividad.jdoStateManager = paramStateManager;
    return localTipoOtraActividad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoOtraActividad localTipoOtraActividad = new TipoOtraActividad();
    localTipoOtraActividad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoOtraActividad.jdoFlags = 1;
    localTipoOtraActividad.jdoStateManager = paramStateManager;
    return localTipoOtraActividad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codOtraActividad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoOtraActividad);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codOtraActividad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoOtraActividad = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoOtraActividad paramTipoOtraActividad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.codOtraActividad = paramTipoOtraActividad.codOtraActividad;
      return;
    case 1:
      if (paramTipoOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTipoOtraActividad.descripcion;
      return;
    case 2:
      if (paramTipoOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoOtraActividad = paramTipoOtraActividad.idTipoOtraActividad;
      return;
    case 3:
      if (paramTipoOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramTipoOtraActividad.tipo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoOtraActividad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoOtraActividad localTipoOtraActividad = (TipoOtraActividad)paramObject;
    if (localTipoOtraActividad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoOtraActividad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoOtraActividadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoOtraActividadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoOtraActividadPK))
      throw new IllegalArgumentException("arg1");
    TipoOtraActividadPK localTipoOtraActividadPK = (TipoOtraActividadPK)paramObject;
    localTipoOtraActividadPK.idTipoOtraActividad = this.idTipoOtraActividad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoOtraActividadPK))
      throw new IllegalArgumentException("arg1");
    TipoOtraActividadPK localTipoOtraActividadPK = (TipoOtraActividadPK)paramObject;
    this.idTipoOtraActividad = localTipoOtraActividadPK.idTipoOtraActividad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoOtraActividadPK))
      throw new IllegalArgumentException("arg2");
    TipoOtraActividadPK localTipoOtraActividadPK = (TipoOtraActividadPK)paramObject;
    localTipoOtraActividadPK.idTipoOtraActividad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoOtraActividadPK))
      throw new IllegalArgumentException("arg2");
    TipoOtraActividadPK localTipoOtraActividadPK = (TipoOtraActividadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localTipoOtraActividadPK.idTipoOtraActividad);
  }

  private static final String jdoGetcodOtraActividad(TipoOtraActividad paramTipoOtraActividad)
  {
    if (paramTipoOtraActividad.jdoFlags <= 0)
      return paramTipoOtraActividad.codOtraActividad;
    StateManager localStateManager = paramTipoOtraActividad.jdoStateManager;
    if (localStateManager == null)
      return paramTipoOtraActividad.codOtraActividad;
    if (localStateManager.isLoaded(paramTipoOtraActividad, jdoInheritedFieldCount + 0))
      return paramTipoOtraActividad.codOtraActividad;
    return localStateManager.getStringField(paramTipoOtraActividad, jdoInheritedFieldCount + 0, paramTipoOtraActividad.codOtraActividad);
  }

  private static final void jdoSetcodOtraActividad(TipoOtraActividad paramTipoOtraActividad, String paramString)
  {
    if (paramTipoOtraActividad.jdoFlags == 0)
    {
      paramTipoOtraActividad.codOtraActividad = paramString;
      return;
    }
    StateManager localStateManager = paramTipoOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoOtraActividad.codOtraActividad = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoOtraActividad, jdoInheritedFieldCount + 0, paramTipoOtraActividad.codOtraActividad, paramString);
  }

  private static final String jdoGetdescripcion(TipoOtraActividad paramTipoOtraActividad)
  {
    if (paramTipoOtraActividad.jdoFlags <= 0)
      return paramTipoOtraActividad.descripcion;
    StateManager localStateManager = paramTipoOtraActividad.jdoStateManager;
    if (localStateManager == null)
      return paramTipoOtraActividad.descripcion;
    if (localStateManager.isLoaded(paramTipoOtraActividad, jdoInheritedFieldCount + 1))
      return paramTipoOtraActividad.descripcion;
    return localStateManager.getStringField(paramTipoOtraActividad, jdoInheritedFieldCount + 1, paramTipoOtraActividad.descripcion);
  }

  private static final void jdoSetdescripcion(TipoOtraActividad paramTipoOtraActividad, String paramString)
  {
    if (paramTipoOtraActividad.jdoFlags == 0)
    {
      paramTipoOtraActividad.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoOtraActividad.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoOtraActividad, jdoInheritedFieldCount + 1, paramTipoOtraActividad.descripcion, paramString);
  }

  private static final long jdoGetidTipoOtraActividad(TipoOtraActividad paramTipoOtraActividad)
  {
    return paramTipoOtraActividad.idTipoOtraActividad;
  }

  private static final void jdoSetidTipoOtraActividad(TipoOtraActividad paramTipoOtraActividad, long paramLong)
  {
    StateManager localStateManager = paramTipoOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoOtraActividad.idTipoOtraActividad = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoOtraActividad, jdoInheritedFieldCount + 2, paramTipoOtraActividad.idTipoOtraActividad, paramLong);
  }

  private static final String jdoGettipo(TipoOtraActividad paramTipoOtraActividad)
  {
    if (paramTipoOtraActividad.jdoFlags <= 0)
      return paramTipoOtraActividad.tipo;
    StateManager localStateManager = paramTipoOtraActividad.jdoStateManager;
    if (localStateManager == null)
      return paramTipoOtraActividad.tipo;
    if (localStateManager.isLoaded(paramTipoOtraActividad, jdoInheritedFieldCount + 3))
      return paramTipoOtraActividad.tipo;
    return localStateManager.getStringField(paramTipoOtraActividad, jdoInheritedFieldCount + 3, paramTipoOtraActividad.tipo);
  }

  private static final void jdoSettipo(TipoOtraActividad paramTipoOtraActividad, String paramString)
  {
    if (paramTipoOtraActividad.jdoFlags == 0)
    {
      paramTipoOtraActividad.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramTipoOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoOtraActividad.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoOtraActividad, jdoInheritedFieldCount + 3, paramTipoOtraActividad.tipo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}