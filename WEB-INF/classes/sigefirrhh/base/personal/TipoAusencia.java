package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoAusencia
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_CLASE;
  private long idTipoAusencia;
  private String codTipoAusencia;
  private String clase;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "clase", "codTipoAusencia", "descripcion", "idTipoAusencia" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.TipoAusencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoAusencia());

    LISTA_CLASE = 
      new LinkedHashMap();

    LISTA_CLASE.put("A", "AUSENCIA");
    LISTA_CLASE.put("P", "PERMISO");
    LISTA_CLASE.put("R", "REPOSO");
  }

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodTipoAusencia(this) + " - " + 
      jdoGetclase(this);
  }

  public String getClase()
  {
    return jdoGetclase(this);
  }

  public String getCodTipoAusencia()
  {
    return jdoGetcodTipoAusencia(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdTipoAusencia()
  {
    return jdoGetidTipoAusencia(this);
  }

  public void setClase(String string)
  {
    jdoSetclase(this, string);
  }

  public void setCodTipoAusencia(String string)
  {
    jdoSetcodTipoAusencia(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdTipoAusencia(long l)
  {
    jdoSetidTipoAusencia(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoAusencia localTipoAusencia = new TipoAusencia();
    localTipoAusencia.jdoFlags = 1;
    localTipoAusencia.jdoStateManager = paramStateManager;
    return localTipoAusencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoAusencia localTipoAusencia = new TipoAusencia();
    localTipoAusencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoAusencia.jdoFlags = 1;
    localTipoAusencia.jdoStateManager = paramStateManager;
    return localTipoAusencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.clase);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoAusencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoAusencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.clase = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoAusencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoAusencia = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoAusencia paramTipoAusencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.clase = paramTipoAusencia.clase;
      return;
    case 1:
      if (paramTipoAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoAusencia = paramTipoAusencia.codTipoAusencia;
      return;
    case 2:
      if (paramTipoAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTipoAusencia.descripcion;
      return;
    case 3:
      if (paramTipoAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoAusencia = paramTipoAusencia.idTipoAusencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoAusencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoAusencia localTipoAusencia = (TipoAusencia)paramObject;
    if (localTipoAusencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoAusencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoAusenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoAusenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoAusenciaPK))
      throw new IllegalArgumentException("arg1");
    TipoAusenciaPK localTipoAusenciaPK = (TipoAusenciaPK)paramObject;
    localTipoAusenciaPK.idTipoAusencia = this.idTipoAusencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoAusenciaPK))
      throw new IllegalArgumentException("arg1");
    TipoAusenciaPK localTipoAusenciaPK = (TipoAusenciaPK)paramObject;
    this.idTipoAusencia = localTipoAusenciaPK.idTipoAusencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoAusenciaPK))
      throw new IllegalArgumentException("arg2");
    TipoAusenciaPK localTipoAusenciaPK = (TipoAusenciaPK)paramObject;
    localTipoAusenciaPK.idTipoAusencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoAusenciaPK))
      throw new IllegalArgumentException("arg2");
    TipoAusenciaPK localTipoAusenciaPK = (TipoAusenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localTipoAusenciaPK.idTipoAusencia);
  }

  private static final String jdoGetclase(TipoAusencia paramTipoAusencia)
  {
    if (paramTipoAusencia.jdoFlags <= 0)
      return paramTipoAusencia.clase;
    StateManager localStateManager = paramTipoAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramTipoAusencia.clase;
    if (localStateManager.isLoaded(paramTipoAusencia, jdoInheritedFieldCount + 0))
      return paramTipoAusencia.clase;
    return localStateManager.getStringField(paramTipoAusencia, jdoInheritedFieldCount + 0, paramTipoAusencia.clase);
  }

  private static final void jdoSetclase(TipoAusencia paramTipoAusencia, String paramString)
  {
    if (paramTipoAusencia.jdoFlags == 0)
    {
      paramTipoAusencia.clase = paramString;
      return;
    }
    StateManager localStateManager = paramTipoAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAusencia.clase = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoAusencia, jdoInheritedFieldCount + 0, paramTipoAusencia.clase, paramString);
  }

  private static final String jdoGetcodTipoAusencia(TipoAusencia paramTipoAusencia)
  {
    if (paramTipoAusencia.jdoFlags <= 0)
      return paramTipoAusencia.codTipoAusencia;
    StateManager localStateManager = paramTipoAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramTipoAusencia.codTipoAusencia;
    if (localStateManager.isLoaded(paramTipoAusencia, jdoInheritedFieldCount + 1))
      return paramTipoAusencia.codTipoAusencia;
    return localStateManager.getStringField(paramTipoAusencia, jdoInheritedFieldCount + 1, paramTipoAusencia.codTipoAusencia);
  }

  private static final void jdoSetcodTipoAusencia(TipoAusencia paramTipoAusencia, String paramString)
  {
    if (paramTipoAusencia.jdoFlags == 0)
    {
      paramTipoAusencia.codTipoAusencia = paramString;
      return;
    }
    StateManager localStateManager = paramTipoAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAusencia.codTipoAusencia = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoAusencia, jdoInheritedFieldCount + 1, paramTipoAusencia.codTipoAusencia, paramString);
  }

  private static final String jdoGetdescripcion(TipoAusencia paramTipoAusencia)
  {
    if (paramTipoAusencia.jdoFlags <= 0)
      return paramTipoAusencia.descripcion;
    StateManager localStateManager = paramTipoAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramTipoAusencia.descripcion;
    if (localStateManager.isLoaded(paramTipoAusencia, jdoInheritedFieldCount + 2))
      return paramTipoAusencia.descripcion;
    return localStateManager.getStringField(paramTipoAusencia, jdoInheritedFieldCount + 2, paramTipoAusencia.descripcion);
  }

  private static final void jdoSetdescripcion(TipoAusencia paramTipoAusencia, String paramString)
  {
    if (paramTipoAusencia.jdoFlags == 0)
    {
      paramTipoAusencia.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAusencia.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoAusencia, jdoInheritedFieldCount + 2, paramTipoAusencia.descripcion, paramString);
  }

  private static final long jdoGetidTipoAusencia(TipoAusencia paramTipoAusencia)
  {
    return paramTipoAusencia.idTipoAusencia;
  }

  private static final void jdoSetidTipoAusencia(TipoAusencia paramTipoAusencia, long paramLong)
  {
    StateManager localStateManager = paramTipoAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoAusencia.idTipoAusencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoAusencia, jdoInheritedFieldCount + 3, paramTipoAusencia.idTipoAusencia, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}