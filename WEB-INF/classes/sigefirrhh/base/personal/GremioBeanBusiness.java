package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class GremioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addGremio(Gremio gremio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Gremio gremioNew = 
      (Gremio)BeanUtils.cloneBean(
      gremio);

    pm.makePersistent(gremioNew);
  }

  public void updateGremio(Gremio gremio) throws Exception
  {
    Gremio gremioModify = 
      findGremioById(gremio.getIdGremio());

    BeanUtils.copyProperties(gremioModify, gremio);
  }

  public void deleteGremio(Gremio gremio) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Gremio gremioDelete = 
      findGremioById(gremio.getIdGremio());
    pm.deletePersistent(gremioDelete);
  }

  public Gremio findGremioById(long idGremio) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idGremio == pIdGremio";
    Query query = pm.newQuery(Gremio.class, filter);

    query.declareParameters("long pIdGremio");

    parameters.put("pIdGremio", new Long(idGremio));

    Collection colGremio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colGremio.iterator();
    return (Gremio)iterator.next();
  }

  public Collection findGremioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent gremioExtent = pm.getExtent(
      Gremio.class, true);
    Query query = pm.newQuery(gremioExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodGremio(String codGremio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codGremio == pCodGremio";

    Query query = pm.newQuery(Gremio.class, filter);

    query.declareParameters("java.lang.String pCodGremio");
    HashMap parameters = new HashMap();

    parameters.put("pCodGremio", new String(codGremio));

    query.setOrdering("nombre ascending");

    Collection colGremio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGremio);

    return colGremio;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Gremio.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colGremio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGremio);

    return colGremio;
  }
}