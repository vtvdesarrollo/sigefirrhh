package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoAmonestacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoAmonestacion(TipoAmonestacion tipoAmonestacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoAmonestacion tipoAmonestacionNew = 
      (TipoAmonestacion)BeanUtils.cloneBean(
      tipoAmonestacion);

    pm.makePersistent(tipoAmonestacionNew);
  }

  public void updateTipoAmonestacion(TipoAmonestacion tipoAmonestacion) throws Exception
  {
    TipoAmonestacion tipoAmonestacionModify = 
      findTipoAmonestacionById(tipoAmonestacion.getIdTipoAmonestacion());

    BeanUtils.copyProperties(tipoAmonestacionModify, tipoAmonestacion);
  }

  public void deleteTipoAmonestacion(TipoAmonestacion tipoAmonestacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoAmonestacion tipoAmonestacionDelete = 
      findTipoAmonestacionById(tipoAmonestacion.getIdTipoAmonestacion());
    pm.deletePersistent(tipoAmonestacionDelete);
  }

  public TipoAmonestacion findTipoAmonestacionById(long idTipoAmonestacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoAmonestacion == pIdTipoAmonestacion";
    Query query = pm.newQuery(TipoAmonestacion.class, filter);

    query.declareParameters("long pIdTipoAmonestacion");

    parameters.put("pIdTipoAmonestacion", new Long(idTipoAmonestacion));

    Collection colTipoAmonestacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoAmonestacion.iterator();
    return (TipoAmonestacion)iterator.next();
  }

  public Collection findTipoAmonestacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoAmonestacionExtent = pm.getExtent(
      TipoAmonestacion.class, true);
    Query query = pm.newQuery(tipoAmonestacionExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoAmonestacion(String codTipoAmonestacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoAmonestacion == pCodTipoAmonestacion";

    Query query = pm.newQuery(TipoAmonestacion.class, filter);

    query.declareParameters("java.lang.String pCodTipoAmonestacion");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoAmonestacion", new String(codTipoAmonestacion));

    query.setOrdering("descripcion ascending");

    Collection colTipoAmonestacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoAmonestacion);

    return colTipoAmonestacion;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(TipoAmonestacion.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTipoAmonestacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoAmonestacion);

    return colTipoAmonestacion;
  }
}