package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CarreraAreaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCarreraArea(CarreraArea carreraArea)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CarreraArea carreraAreaNew = 
      (CarreraArea)BeanUtils.cloneBean(
      carreraArea);

    AreaCarreraBeanBusiness areaCarreraBeanBusiness = new AreaCarreraBeanBusiness();

    if (carreraAreaNew.getAreaCarrera() != null) {
      carreraAreaNew.setAreaCarrera(
        areaCarreraBeanBusiness.findAreaCarreraById(
        carreraAreaNew.getAreaCarrera().getIdAreaCarrera()));
    }

    CarreraBeanBusiness carreraBeanBusiness = new CarreraBeanBusiness();

    if (carreraAreaNew.getCarrera() != null) {
      carreraAreaNew.setCarrera(
        carreraBeanBusiness.findCarreraById(
        carreraAreaNew.getCarrera().getIdCarrera()));
    }
    pm.makePersistent(carreraAreaNew);
  }

  public void updateCarreraArea(CarreraArea carreraArea) throws Exception
  {
    CarreraArea carreraAreaModify = 
      findCarreraAreaById(carreraArea.getIdCarreraArea());

    AreaCarreraBeanBusiness areaCarreraBeanBusiness = new AreaCarreraBeanBusiness();

    if (carreraArea.getAreaCarrera() != null) {
      carreraArea.setAreaCarrera(
        areaCarreraBeanBusiness.findAreaCarreraById(
        carreraArea.getAreaCarrera().getIdAreaCarrera()));
    }

    CarreraBeanBusiness carreraBeanBusiness = new CarreraBeanBusiness();

    if (carreraArea.getCarrera() != null) {
      carreraArea.setCarrera(
        carreraBeanBusiness.findCarreraById(
        carreraArea.getCarrera().getIdCarrera()));
    }

    BeanUtils.copyProperties(carreraAreaModify, carreraArea);
  }

  public void deleteCarreraArea(CarreraArea carreraArea) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CarreraArea carreraAreaDelete = 
      findCarreraAreaById(carreraArea.getIdCarreraArea());
    pm.deletePersistent(carreraAreaDelete);
  }

  public CarreraArea findCarreraAreaById(long idCarreraArea) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCarreraArea == pIdCarreraArea";
    Query query = pm.newQuery(CarreraArea.class, filter);

    query.declareParameters("long pIdCarreraArea");

    parameters.put("pIdCarreraArea", new Long(idCarreraArea));

    Collection colCarreraArea = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCarreraArea.iterator();
    return (CarreraArea)iterator.next();
  }

  public Collection findCarreraAreaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent carreraAreaExtent = pm.getExtent(
      CarreraArea.class, true);
    Query query = pm.newQuery(carreraAreaExtent);
    query.setOrdering("carrera.nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAreaCarrera(long idAreaCarrera)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "areaCarrera.idAreaCarrera == pIdAreaCarrera";

    Query query = pm.newQuery(CarreraArea.class, filter);

    query.declareParameters("long pIdAreaCarrera");
    HashMap parameters = new HashMap();

    parameters.put("pIdAreaCarrera", new Long(idAreaCarrera));

    query.setOrdering("carrera.nombre ascending");

    Collection colCarreraArea = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCarreraArea);

    return colCarreraArea;
  }

  public Collection findByCarrera(long idCarrera)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "carrera.idCarrera == pIdCarrera";

    Query query = pm.newQuery(CarreraArea.class, filter);

    query.declareParameters("long pIdCarrera");
    HashMap parameters = new HashMap();

    parameters.put("pIdCarrera", new Long(idCarrera));

    query.setOrdering("carrera.nombre ascending");

    Collection colCarreraArea = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCarreraArea);

    return colCarreraArea;
  }
}