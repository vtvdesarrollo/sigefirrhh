package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class AreaCarrera
  implements Serializable, PersistenceCapable
{
  private long idAreaCarrera;
  private String codAreaCarrera;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codAreaCarrera", "descripcion", "idAreaCarrera" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodAreaCarrera(this);
  }

  public String getCodAreaCarrera()
  {
    return jdoGetcodAreaCarrera(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdAreaCarrera()
  {
    return jdoGetidAreaCarrera(this);
  }

  public void setCodAreaCarrera(String string)
  {
    jdoSetcodAreaCarrera(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdAreaCarrera(long l)
  {
    jdoSetidAreaCarrera(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.AreaCarrera"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AreaCarrera());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AreaCarrera localAreaCarrera = new AreaCarrera();
    localAreaCarrera.jdoFlags = 1;
    localAreaCarrera.jdoStateManager = paramStateManager;
    return localAreaCarrera;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AreaCarrera localAreaCarrera = new AreaCarrera();
    localAreaCarrera.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAreaCarrera.jdoFlags = 1;
    localAreaCarrera.jdoStateManager = paramStateManager;
    return localAreaCarrera;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codAreaCarrera);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAreaCarrera);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codAreaCarrera = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAreaCarrera = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AreaCarrera paramAreaCarrera, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAreaCarrera == null)
        throw new IllegalArgumentException("arg1");
      this.codAreaCarrera = paramAreaCarrera.codAreaCarrera;
      return;
    case 1:
      if (paramAreaCarrera == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramAreaCarrera.descripcion;
      return;
    case 2:
      if (paramAreaCarrera == null)
        throw new IllegalArgumentException("arg1");
      this.idAreaCarrera = paramAreaCarrera.idAreaCarrera;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AreaCarrera))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AreaCarrera localAreaCarrera = (AreaCarrera)paramObject;
    if (localAreaCarrera.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAreaCarrera, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AreaCarreraPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AreaCarreraPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AreaCarreraPK))
      throw new IllegalArgumentException("arg1");
    AreaCarreraPK localAreaCarreraPK = (AreaCarreraPK)paramObject;
    localAreaCarreraPK.idAreaCarrera = this.idAreaCarrera;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AreaCarreraPK))
      throw new IllegalArgumentException("arg1");
    AreaCarreraPK localAreaCarreraPK = (AreaCarreraPK)paramObject;
    this.idAreaCarrera = localAreaCarreraPK.idAreaCarrera;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AreaCarreraPK))
      throw new IllegalArgumentException("arg2");
    AreaCarreraPK localAreaCarreraPK = (AreaCarreraPK)paramObject;
    localAreaCarreraPK.idAreaCarrera = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AreaCarreraPK))
      throw new IllegalArgumentException("arg2");
    AreaCarreraPK localAreaCarreraPK = (AreaCarreraPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localAreaCarreraPK.idAreaCarrera);
  }

  private static final String jdoGetcodAreaCarrera(AreaCarrera paramAreaCarrera)
  {
    if (paramAreaCarrera.jdoFlags <= 0)
      return paramAreaCarrera.codAreaCarrera;
    StateManager localStateManager = paramAreaCarrera.jdoStateManager;
    if (localStateManager == null)
      return paramAreaCarrera.codAreaCarrera;
    if (localStateManager.isLoaded(paramAreaCarrera, jdoInheritedFieldCount + 0))
      return paramAreaCarrera.codAreaCarrera;
    return localStateManager.getStringField(paramAreaCarrera, jdoInheritedFieldCount + 0, paramAreaCarrera.codAreaCarrera);
  }

  private static final void jdoSetcodAreaCarrera(AreaCarrera paramAreaCarrera, String paramString)
  {
    if (paramAreaCarrera.jdoFlags == 0)
    {
      paramAreaCarrera.codAreaCarrera = paramString;
      return;
    }
    StateManager localStateManager = paramAreaCarrera.jdoStateManager;
    if (localStateManager == null)
    {
      paramAreaCarrera.codAreaCarrera = paramString;
      return;
    }
    localStateManager.setStringField(paramAreaCarrera, jdoInheritedFieldCount + 0, paramAreaCarrera.codAreaCarrera, paramString);
  }

  private static final String jdoGetdescripcion(AreaCarrera paramAreaCarrera)
  {
    if (paramAreaCarrera.jdoFlags <= 0)
      return paramAreaCarrera.descripcion;
    StateManager localStateManager = paramAreaCarrera.jdoStateManager;
    if (localStateManager == null)
      return paramAreaCarrera.descripcion;
    if (localStateManager.isLoaded(paramAreaCarrera, jdoInheritedFieldCount + 1))
      return paramAreaCarrera.descripcion;
    return localStateManager.getStringField(paramAreaCarrera, jdoInheritedFieldCount + 1, paramAreaCarrera.descripcion);
  }

  private static final void jdoSetdescripcion(AreaCarrera paramAreaCarrera, String paramString)
  {
    if (paramAreaCarrera.jdoFlags == 0)
    {
      paramAreaCarrera.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramAreaCarrera.jdoStateManager;
    if (localStateManager == null)
    {
      paramAreaCarrera.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramAreaCarrera, jdoInheritedFieldCount + 1, paramAreaCarrera.descripcion, paramString);
  }

  private static final long jdoGetidAreaCarrera(AreaCarrera paramAreaCarrera)
  {
    return paramAreaCarrera.idAreaCarrera;
  }

  private static final void jdoSetidAreaCarrera(AreaCarrera paramAreaCarrera, long paramLong)
  {
    StateManager localStateManager = paramAreaCarrera.jdoStateManager;
    if (localStateManager == null)
    {
      paramAreaCarrera.idAreaCarrera = paramLong;
      return;
    }
    localStateManager.setLongField(paramAreaCarrera, jdoInheritedFieldCount + 2, paramAreaCarrera.idAreaCarrera, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}