package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class AreaCarreraBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAreaCarrera(AreaCarrera areaCarrera)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    AreaCarrera areaCarreraNew = 
      (AreaCarrera)BeanUtils.cloneBean(
      areaCarrera);

    pm.makePersistent(areaCarreraNew);
  }

  public void updateAreaCarrera(AreaCarrera areaCarrera) throws Exception
  {
    AreaCarrera areaCarreraModify = 
      findAreaCarreraById(areaCarrera.getIdAreaCarrera());

    BeanUtils.copyProperties(areaCarreraModify, areaCarrera);
  }

  public void deleteAreaCarrera(AreaCarrera areaCarrera) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    AreaCarrera areaCarreraDelete = 
      findAreaCarreraById(areaCarrera.getIdAreaCarrera());
    pm.deletePersistent(areaCarreraDelete);
  }

  public AreaCarrera findAreaCarreraById(long idAreaCarrera) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAreaCarrera == pIdAreaCarrera";
    Query query = pm.newQuery(AreaCarrera.class, filter);

    query.declareParameters("long pIdAreaCarrera");

    parameters.put("pIdAreaCarrera", new Long(idAreaCarrera));

    Collection colAreaCarrera = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAreaCarrera.iterator();
    return (AreaCarrera)iterator.next();
  }

  public Collection findAreaCarreraAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent areaCarreraExtent = pm.getExtent(
      AreaCarrera.class, true);
    Query query = pm.newQuery(areaCarreraExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodAreaCarrera(String codAreaCarrera)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codAreaCarrera == pCodAreaCarrera";

    Query query = pm.newQuery(AreaCarrera.class, filter);

    query.declareParameters("java.lang.String pCodAreaCarrera");
    HashMap parameters = new HashMap();

    parameters.put("pCodAreaCarrera", new String(codAreaCarrera));

    query.setOrdering("descripcion ascending");

    Collection colAreaCarrera = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAreaCarrera);

    return colAreaCarrera;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(AreaCarrera.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colAreaCarrera = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAreaCarrera);

    return colAreaCarrera;
  }
}