package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Recaudo
  implements Serializable, PersistenceCapable
{
  private long idRecaudo;
  private int codRecaudo;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codRecaudo", "descripcion", "idRecaudo" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodRecaudo(this);
  }

  public int getCodRecaudo()
  {
    return jdoGetcodRecaudo(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdRecaudo()
  {
    return jdoGetidRecaudo(this);
  }

  public void setCodRecaudo(int i)
  {
    jdoSetcodRecaudo(this, i);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdRecaudo(long l)
  {
    jdoSetidRecaudo(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.Recaudo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Recaudo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Recaudo localRecaudo = new Recaudo();
    localRecaudo.jdoFlags = 1;
    localRecaudo.jdoStateManager = paramStateManager;
    return localRecaudo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Recaudo localRecaudo = new Recaudo();
    localRecaudo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRecaudo.jdoFlags = 1;
    localRecaudo.jdoStateManager = paramStateManager;
    return localRecaudo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codRecaudo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRecaudo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRecaudo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRecaudo = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Recaudo paramRecaudo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.codRecaudo = paramRecaudo.codRecaudo;
      return;
    case 1:
      if (paramRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramRecaudo.descripcion;
      return;
    case 2:
      if (paramRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.idRecaudo = paramRecaudo.idRecaudo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Recaudo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Recaudo localRecaudo = (Recaudo)paramObject;
    if (localRecaudo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRecaudo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RecaudoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RecaudoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RecaudoPK))
      throw new IllegalArgumentException("arg1");
    RecaudoPK localRecaudoPK = (RecaudoPK)paramObject;
    localRecaudoPK.idRecaudo = this.idRecaudo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RecaudoPK))
      throw new IllegalArgumentException("arg1");
    RecaudoPK localRecaudoPK = (RecaudoPK)paramObject;
    this.idRecaudo = localRecaudoPK.idRecaudo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RecaudoPK))
      throw new IllegalArgumentException("arg2");
    RecaudoPK localRecaudoPK = (RecaudoPK)paramObject;
    localRecaudoPK.idRecaudo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RecaudoPK))
      throw new IllegalArgumentException("arg2");
    RecaudoPK localRecaudoPK = (RecaudoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localRecaudoPK.idRecaudo);
  }

  private static final int jdoGetcodRecaudo(Recaudo paramRecaudo)
  {
    if (paramRecaudo.jdoFlags <= 0)
      return paramRecaudo.codRecaudo;
    StateManager localStateManager = paramRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramRecaudo.codRecaudo;
    if (localStateManager.isLoaded(paramRecaudo, jdoInheritedFieldCount + 0))
      return paramRecaudo.codRecaudo;
    return localStateManager.getIntField(paramRecaudo, jdoInheritedFieldCount + 0, paramRecaudo.codRecaudo);
  }

  private static final void jdoSetcodRecaudo(Recaudo paramRecaudo, int paramInt)
  {
    if (paramRecaudo.jdoFlags == 0)
    {
      paramRecaudo.codRecaudo = paramInt;
      return;
    }
    StateManager localStateManager = paramRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramRecaudo.codRecaudo = paramInt;
      return;
    }
    localStateManager.setIntField(paramRecaudo, jdoInheritedFieldCount + 0, paramRecaudo.codRecaudo, paramInt);
  }

  private static final String jdoGetdescripcion(Recaudo paramRecaudo)
  {
    if (paramRecaudo.jdoFlags <= 0)
      return paramRecaudo.descripcion;
    StateManager localStateManager = paramRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramRecaudo.descripcion;
    if (localStateManager.isLoaded(paramRecaudo, jdoInheritedFieldCount + 1))
      return paramRecaudo.descripcion;
    return localStateManager.getStringField(paramRecaudo, jdoInheritedFieldCount + 1, paramRecaudo.descripcion);
  }

  private static final void jdoSetdescripcion(Recaudo paramRecaudo, String paramString)
  {
    if (paramRecaudo.jdoFlags == 0)
    {
      paramRecaudo.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramRecaudo.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramRecaudo, jdoInheritedFieldCount + 1, paramRecaudo.descripcion, paramString);
  }

  private static final long jdoGetidRecaudo(Recaudo paramRecaudo)
  {
    return paramRecaudo.idRecaudo;
  }

  private static final void jdoSetidRecaudo(Recaudo paramRecaudo, long paramLong)
  {
    StateManager localStateManager = paramRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramRecaudo.idRecaudo = paramLong;
      return;
    }
    localStateManager.setLongField(paramRecaudo, jdoInheritedFieldCount + 2, paramRecaudo.idRecaudo, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}