package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class RelacionRecaudo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_EVENTO;
  private long idRelacionRecaudo;
  private int evento;
  private Recaudo recaudo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "evento", "idRelacionRecaudo", "recaudo" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.base.personal.Recaudo") }; private static final byte[] jdoFieldFlags = { 21, 24, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.RelacionRecaudo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RelacionRecaudo());

    LISTA_EVENTO = 
      new LinkedHashMap();

    LISTA_EVENTO.put("1", "PERSONAL");
    LISTA_EVENTO.put("2", "FAMILIAR");
    LISTA_EVENTO.put("3", "AUSENCIA");
    LISTA_EVENTO.put("4", "SINIESTRO");
    LISTA_EVENTO.put("5", "CONCURSO");
    LISTA_EVENTO.put("6", "JUBILACION");
    LISTA_EVENTO.put("7", "PENSION");
    LISTA_EVENTO.put("8", "LIQUIDACION");
  }

  public String toString()
  {
    return jdoGetrecaudo(this).getDescripcion() + " - " + 
      jdoGetrecaudo(this).getCodRecaudo() + " - " + 
      jdoGetevento(this);
  }

  public int getEvento()
  {
    return jdoGetevento(this);
  }

  public long getIdRelacionRecaudo()
  {
    return jdoGetidRelacionRecaudo(this);
  }

  public Recaudo getRecaudo()
  {
    return jdoGetrecaudo(this);
  }

  public void setEvento(int i)
  {
    jdoSetevento(this, i);
  }

  public void setIdRelacionRecaudo(long l)
  {
    jdoSetidRelacionRecaudo(this, l);
  }

  public void setRecaudo(Recaudo recaudo)
  {
    jdoSetrecaudo(this, recaudo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RelacionRecaudo localRelacionRecaudo = new RelacionRecaudo();
    localRelacionRecaudo.jdoFlags = 1;
    localRelacionRecaudo.jdoStateManager = paramStateManager;
    return localRelacionRecaudo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RelacionRecaudo localRelacionRecaudo = new RelacionRecaudo();
    localRelacionRecaudo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRelacionRecaudo.jdoFlags = 1;
    localRelacionRecaudo.jdoStateManager = paramStateManager;
    return localRelacionRecaudo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.evento);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRelacionRecaudo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.recaudo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.evento = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRelacionRecaudo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.recaudo = ((Recaudo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RelacionRecaudo paramRelacionRecaudo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRelacionRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.evento = paramRelacionRecaudo.evento;
      return;
    case 1:
      if (paramRelacionRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.idRelacionRecaudo = paramRelacionRecaudo.idRelacionRecaudo;
      return;
    case 2:
      if (paramRelacionRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.recaudo = paramRelacionRecaudo.recaudo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RelacionRecaudo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RelacionRecaudo localRelacionRecaudo = (RelacionRecaudo)paramObject;
    if (localRelacionRecaudo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRelacionRecaudo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RelacionRecaudoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RelacionRecaudoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RelacionRecaudoPK))
      throw new IllegalArgumentException("arg1");
    RelacionRecaudoPK localRelacionRecaudoPK = (RelacionRecaudoPK)paramObject;
    localRelacionRecaudoPK.idRelacionRecaudo = this.idRelacionRecaudo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RelacionRecaudoPK))
      throw new IllegalArgumentException("arg1");
    RelacionRecaudoPK localRelacionRecaudoPK = (RelacionRecaudoPK)paramObject;
    this.idRelacionRecaudo = localRelacionRecaudoPK.idRelacionRecaudo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RelacionRecaudoPK))
      throw new IllegalArgumentException("arg2");
    RelacionRecaudoPK localRelacionRecaudoPK = (RelacionRecaudoPK)paramObject;
    localRelacionRecaudoPK.idRelacionRecaudo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RelacionRecaudoPK))
      throw new IllegalArgumentException("arg2");
    RelacionRecaudoPK localRelacionRecaudoPK = (RelacionRecaudoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localRelacionRecaudoPK.idRelacionRecaudo);
  }

  private static final int jdoGetevento(RelacionRecaudo paramRelacionRecaudo)
  {
    if (paramRelacionRecaudo.jdoFlags <= 0)
      return paramRelacionRecaudo.evento;
    StateManager localStateManager = paramRelacionRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramRelacionRecaudo.evento;
    if (localStateManager.isLoaded(paramRelacionRecaudo, jdoInheritedFieldCount + 0))
      return paramRelacionRecaudo.evento;
    return localStateManager.getIntField(paramRelacionRecaudo, jdoInheritedFieldCount + 0, paramRelacionRecaudo.evento);
  }

  private static final void jdoSetevento(RelacionRecaudo paramRelacionRecaudo, int paramInt)
  {
    if (paramRelacionRecaudo.jdoFlags == 0)
    {
      paramRelacionRecaudo.evento = paramInt;
      return;
    }
    StateManager localStateManager = paramRelacionRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramRelacionRecaudo.evento = paramInt;
      return;
    }
    localStateManager.setIntField(paramRelacionRecaudo, jdoInheritedFieldCount + 0, paramRelacionRecaudo.evento, paramInt);
  }

  private static final long jdoGetidRelacionRecaudo(RelacionRecaudo paramRelacionRecaudo)
  {
    return paramRelacionRecaudo.idRelacionRecaudo;
  }

  private static final void jdoSetidRelacionRecaudo(RelacionRecaudo paramRelacionRecaudo, long paramLong)
  {
    StateManager localStateManager = paramRelacionRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramRelacionRecaudo.idRelacionRecaudo = paramLong;
      return;
    }
    localStateManager.setLongField(paramRelacionRecaudo, jdoInheritedFieldCount + 1, paramRelacionRecaudo.idRelacionRecaudo, paramLong);
  }

  private static final Recaudo jdoGetrecaudo(RelacionRecaudo paramRelacionRecaudo)
  {
    StateManager localStateManager = paramRelacionRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramRelacionRecaudo.recaudo;
    if (localStateManager.isLoaded(paramRelacionRecaudo, jdoInheritedFieldCount + 2))
      return paramRelacionRecaudo.recaudo;
    return (Recaudo)localStateManager.getObjectField(paramRelacionRecaudo, jdoInheritedFieldCount + 2, paramRelacionRecaudo.recaudo);
  }

  private static final void jdoSetrecaudo(RelacionRecaudo paramRelacionRecaudo, Recaudo paramRecaudo)
  {
    StateManager localStateManager = paramRelacionRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramRelacionRecaudo.recaudo = paramRecaudo;
      return;
    }
    localStateManager.setObjectField(paramRelacionRecaudo, jdoInheritedFieldCount + 2, paramRelacionRecaudo.recaudo, paramRecaudo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}