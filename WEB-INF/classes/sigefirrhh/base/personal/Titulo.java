package sigefirrhh.base.personal;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Titulo
  implements Serializable, PersistenceCapable
{
  private long idTitulo;
  private String codTitulo;
  private String descripcion;
  private NivelEducativo nivelEducativo;
  private GrupoProfesion grupoProfesion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTitulo", "descripcion", "grupoProfesion", "idTitulo", "nivelEducativo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.personal.GrupoProfesion"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.personal.NivelEducativo") };
  private static final byte[] jdoFieldFlags = { 21, 21, 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodTitulo(this);
  }

  public String getCodTitulo()
  {
    return jdoGetcodTitulo(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdTitulo()
  {
    return jdoGetidTitulo(this);
  }

  public NivelEducativo getNivelEducativo()
  {
    return jdoGetnivelEducativo(this);
  }

  public void setCodTitulo(String string)
  {
    jdoSetcodTitulo(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdTitulo(long l)
  {
    jdoSetidTitulo(this, l);
  }

  public void setNivelEducativo(NivelEducativo educativo)
  {
    jdoSetnivelEducativo(this, educativo);
  }

  public GrupoProfesion getGrupoProfesion()
  {
    return jdoGetgrupoProfesion(this);
  }

  public void setGrupoProfesion(GrupoProfesion profesion)
  {
    jdoSetgrupoProfesion(this, profesion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.personal.Titulo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Titulo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Titulo localTitulo = new Titulo();
    localTitulo.jdoFlags = 1;
    localTitulo.jdoStateManager = paramStateManager;
    return localTitulo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Titulo localTitulo = new Titulo();
    localTitulo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTitulo.jdoFlags = 1;
    localTitulo.jdoStateManager = paramStateManager;
    return localTitulo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTitulo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoProfesion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTitulo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nivelEducativo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTitulo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoProfesion = ((GrupoProfesion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTitulo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = ((NivelEducativo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Titulo paramTitulo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTitulo == null)
        throw new IllegalArgumentException("arg1");
      this.codTitulo = paramTitulo.codTitulo;
      return;
    case 1:
      if (paramTitulo == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTitulo.descripcion;
      return;
    case 2:
      if (paramTitulo == null)
        throw new IllegalArgumentException("arg1");
      this.grupoProfesion = paramTitulo.grupoProfesion;
      return;
    case 3:
      if (paramTitulo == null)
        throw new IllegalArgumentException("arg1");
      this.idTitulo = paramTitulo.idTitulo;
      return;
    case 4:
      if (paramTitulo == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramTitulo.nivelEducativo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Titulo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Titulo localTitulo = (Titulo)paramObject;
    if (localTitulo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTitulo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TituloPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TituloPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TituloPK))
      throw new IllegalArgumentException("arg1");
    TituloPK localTituloPK = (TituloPK)paramObject;
    localTituloPK.idTitulo = this.idTitulo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TituloPK))
      throw new IllegalArgumentException("arg1");
    TituloPK localTituloPK = (TituloPK)paramObject;
    this.idTitulo = localTituloPK.idTitulo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TituloPK))
      throw new IllegalArgumentException("arg2");
    TituloPK localTituloPK = (TituloPK)paramObject;
    localTituloPK.idTitulo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TituloPK))
      throw new IllegalArgumentException("arg2");
    TituloPK localTituloPK = (TituloPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localTituloPK.idTitulo);
  }

  private static final String jdoGetcodTitulo(Titulo paramTitulo)
  {
    if (paramTitulo.jdoFlags <= 0)
      return paramTitulo.codTitulo;
    StateManager localStateManager = paramTitulo.jdoStateManager;
    if (localStateManager == null)
      return paramTitulo.codTitulo;
    if (localStateManager.isLoaded(paramTitulo, jdoInheritedFieldCount + 0))
      return paramTitulo.codTitulo;
    return localStateManager.getStringField(paramTitulo, jdoInheritedFieldCount + 0, paramTitulo.codTitulo);
  }

  private static final void jdoSetcodTitulo(Titulo paramTitulo, String paramString)
  {
    if (paramTitulo.jdoFlags == 0)
    {
      paramTitulo.codTitulo = paramString;
      return;
    }
    StateManager localStateManager = paramTitulo.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitulo.codTitulo = paramString;
      return;
    }
    localStateManager.setStringField(paramTitulo, jdoInheritedFieldCount + 0, paramTitulo.codTitulo, paramString);
  }

  private static final String jdoGetdescripcion(Titulo paramTitulo)
  {
    if (paramTitulo.jdoFlags <= 0)
      return paramTitulo.descripcion;
    StateManager localStateManager = paramTitulo.jdoStateManager;
    if (localStateManager == null)
      return paramTitulo.descripcion;
    if (localStateManager.isLoaded(paramTitulo, jdoInheritedFieldCount + 1))
      return paramTitulo.descripcion;
    return localStateManager.getStringField(paramTitulo, jdoInheritedFieldCount + 1, paramTitulo.descripcion);
  }

  private static final void jdoSetdescripcion(Titulo paramTitulo, String paramString)
  {
    if (paramTitulo.jdoFlags == 0)
    {
      paramTitulo.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTitulo.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitulo.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTitulo, jdoInheritedFieldCount + 1, paramTitulo.descripcion, paramString);
  }

  private static final GrupoProfesion jdoGetgrupoProfesion(Titulo paramTitulo)
  {
    StateManager localStateManager = paramTitulo.jdoStateManager;
    if (localStateManager == null)
      return paramTitulo.grupoProfesion;
    if (localStateManager.isLoaded(paramTitulo, jdoInheritedFieldCount + 2))
      return paramTitulo.grupoProfesion;
    return (GrupoProfesion)localStateManager.getObjectField(paramTitulo, jdoInheritedFieldCount + 2, paramTitulo.grupoProfesion);
  }

  private static final void jdoSetgrupoProfesion(Titulo paramTitulo, GrupoProfesion paramGrupoProfesion)
  {
    StateManager localStateManager = paramTitulo.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitulo.grupoProfesion = paramGrupoProfesion;
      return;
    }
    localStateManager.setObjectField(paramTitulo, jdoInheritedFieldCount + 2, paramTitulo.grupoProfesion, paramGrupoProfesion);
  }

  private static final long jdoGetidTitulo(Titulo paramTitulo)
  {
    return paramTitulo.idTitulo;
  }

  private static final void jdoSetidTitulo(Titulo paramTitulo, long paramLong)
  {
    StateManager localStateManager = paramTitulo.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitulo.idTitulo = paramLong;
      return;
    }
    localStateManager.setLongField(paramTitulo, jdoInheritedFieldCount + 3, paramTitulo.idTitulo, paramLong);
  }

  private static final NivelEducativo jdoGetnivelEducativo(Titulo paramTitulo)
  {
    StateManager localStateManager = paramTitulo.jdoStateManager;
    if (localStateManager == null)
      return paramTitulo.nivelEducativo;
    if (localStateManager.isLoaded(paramTitulo, jdoInheritedFieldCount + 4))
      return paramTitulo.nivelEducativo;
    return (NivelEducativo)localStateManager.getObjectField(paramTitulo, jdoInheritedFieldCount + 4, paramTitulo.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(Titulo paramTitulo, NivelEducativo paramNivelEducativo)
  {
    StateManager localStateManager = paramTitulo.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitulo.nivelEducativo = paramNivelEducativo;
      return;
    }
    localStateManager.setObjectField(paramTitulo, jdoInheritedFieldCount + 4, paramTitulo.nivelEducativo, paramNivelEducativo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}