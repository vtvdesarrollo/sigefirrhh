package sigefirrhh.base.personal;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CarreraBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCarrera(Carrera carrera)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Carrera carreraNew = 
      (Carrera)BeanUtils.cloneBean(
      carrera);

    pm.makePersistent(carreraNew);
  }

  public void updateCarrera(Carrera carrera) throws Exception
  {
    Carrera carreraModify = 
      findCarreraById(carrera.getIdCarrera());

    BeanUtils.copyProperties(carreraModify, carrera);
  }

  public void deleteCarrera(Carrera carrera) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Carrera carreraDelete = 
      findCarreraById(carrera.getIdCarrera());
    pm.deletePersistent(carreraDelete);
  }

  public Carrera findCarreraById(long idCarrera) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCarrera == pIdCarrera";
    Query query = pm.newQuery(Carrera.class, filter);

    query.declareParameters("long pIdCarrera");

    parameters.put("pIdCarrera", new Long(idCarrera));

    Collection colCarrera = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCarrera.iterator();
    return (Carrera)iterator.next();
  }

  public Collection findCarreraAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent carreraExtent = pm.getExtent(
      Carrera.class, true);
    Query query = pm.newQuery(carreraExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());

    query = null;

    pm = null;
    return collection;
  }

  public Collection findByCodCarrera(String codCarrera)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCarrera == pCodCarrera";

    Query query = pm.newQuery(Carrera.class, filter);

    query.declareParameters("java.lang.String pCodCarrera");
    HashMap parameters = new HashMap();

    parameters.put("pCodCarrera", new String(codCarrera));

    query.setOrdering("nombre ascending");

    Collection colCarrera = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCarrera);

    query = null;

    pm = null;

    return colCarrera;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Carrera.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colCarrera = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCarrera);

    query = null;

    pm = null;

    return colCarrera;
  }

  public Collection findAll() throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = Resource.getConnection();
    connection = Resource.getConnection();
    connection.setAutoCommit(true);
    StringBuffer sql = new StringBuffer();

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    sql.append("select id_carrera, (cod_carrera || ' - ' || nombre)  as descripcion  ");
    sql.append(" from carrera ");
    sql.append(" order by nombre");

    stRegistros = connection.prepareStatement(
      sql.toString(), 
      1003, 
      1007);
    rsRegistros = stRegistros.executeQuery();

    while (rsRegistros.next())
    {
      col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_carrera"))));
      col.add(rsRegistros.getString("descripcion"));
    }
    connection.close(); connection = null;
    connection = null;

    return col;
  }
}