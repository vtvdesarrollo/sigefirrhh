package sigefirrhh.base.personal;

import java.io.Serializable;

public class TipoHabilidadPK
  implements Serializable
{
  public long idTipoHabilidad;

  public TipoHabilidadPK()
  {
  }

  public TipoHabilidadPK(long idTipoHabilidad)
  {
    this.idTipoHabilidad = idTipoHabilidad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoHabilidadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoHabilidadPK thatPK)
  {
    return 
      this.idTipoHabilidad == thatPK.idTipoHabilidad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoHabilidad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoHabilidad);
  }
}