package sigefirrhh.base.definiciones;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class TipoPersonalBeanBusinessExtend extends TipoPersonalBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(TipoPersonalBeanBusinessExtend.class.getName());

  public Collection findByGrupoNomina(long idGrupoNomina) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina";

    Query query = pm.newQuery(TipoPersonal.class, filter);

    query.declareParameters("long pIdGrupoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));

    query.setOrdering("idTipoPersonal ascending");

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }
  public Collection findByManejaRac(String manejaRac, long idUsuario, String administrador) throws Exception {
    PersistenceManager pm = PMThread.getPM();

    HashMap parameters = new HashMap();
    StringBuffer filter = new StringBuffer();
    Query query = pm.newQuery(TipoPersonal.class);

    filter.append("manejaRac == pManejaRac ");

    if (administrador.equals("N")) {
      filter.append(" && idTipoPersonal == utp.tipoPersonal.idTipoPersonal && utp.usuario.idUsuario == pIdUsuario");

      query.declareImports("import sigefirrhh.sistema.UsuarioTipoPersonal");
      query.declareVariables("UsuarioTipoPersonal utp");

      query.declareParameters("String pManejaRac, long pIdUsuario");

      parameters.put("pIdUsuario", new Long(idUsuario));
    }
    else {
      query.declareParameters("String pManejaRac");
    }

    parameters.put("pManejaRac", manejaRac);

    query.setFilter(filter.toString());
    query.setOrdering("idTipoPersonal ascending");

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }
  public Collection findByAprobacionMpd(String aprobacionMpd, long idUsuario, String administrador) throws Exception {
    PersistenceManager pm = PMThread.getPM();

    HashMap parameters = new HashMap();
    StringBuffer filter = new StringBuffer();
    Query query = pm.newQuery(TipoPersonal.class);

    filter.append("aprobacionMpd == pAprobacionMpd ");

    if (administrador.equals("N")) {
      filter.append(" && idTipoPersonal == utp.tipoPersonal.idTipoPersonal && utp.usuario.idUsuario == pIdUsuario");

      query.declareImports("import sigefirrhh.sistema.UsuarioTipoPersonal");
      query.declareVariables("UsuarioTipoPersonal utp");

      query.declareParameters("String pAprobacionMpd, long pIdUsuario");

      parameters.put("pIdUsuario", new Long(idUsuario));
      this.log.error("!!!");
    }
    else {
      query.declareParameters("String pAprobacionMpd");
    }

    parameters.put("pAprobacionMpd", aprobacionMpd);

    query.setFilter(filter.toString());
    query.setOrdering("idTipoPersonal ascending");

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }
  public Collection findByManejaRacAndAprobacionMpd(String manejaRac, String aprobacionMpd, long idUsuario, String administrador) throws Exception {
    PersistenceManager pm = PMThread.getPM();

    HashMap parameters = new HashMap();
    StringBuffer filter = new StringBuffer();
    Query query = pm.newQuery(TipoPersonal.class);

    filter.append("aprobacionMpd == pAprobacionMpd && manejaRac == pManejaRac");

    if (administrador.equals("N")) {
      filter.append(" && idTipoPersonal == utp.tipoPersonal.idTipoPersonal && utp.usuario.idUsuario == pIdUsuario");

      query.declareImports("import sigefirrhh.sistema.UsuarioTipoPersonal");
      query.declareVariables("UsuarioTipoPersonal utp");

      query.declareParameters("String pAprobacionMpd, String pManejaRac, long pIdUsuario");

      parameters.put("pIdUsuario", new Long(idUsuario));
      this.log.error("!!!");
    }
    else {
      query.declareParameters("String pAprobacionMpd, String pManejaRac");
    }

    parameters.put("pAprobacionMpd", aprobacionMpd);
    parameters.put("pManejaRac", manejaRac);

    query.setFilter(filter.toString());
    query.setOrdering("idTipoPersonal ascending");

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }

  public Collection findByManejaRacAprobacionMpdClasificacionPersonal(String manejaRac, String aprobacionMpd, long idClasificacionPersonal, long idUsuario, String administrador) throws Exception {
    PersistenceManager pm = PMThread.getPM();

    HashMap parameters = new HashMap();
    StringBuffer filter = new StringBuffer();
    Query query = pm.newQuery(TipoPersonal.class);

    filter.append("aprobacionMpd == pAprobacionMpd && manejaRac == pManejaRac && clasificacionPersonal.idClasificacionPersonal == pIdClasificacionPersonal");

    if (administrador.equals("N")) {
      filter.append(" && idTipoPersonal == utp.tipoPersonal.idTipoPersonal && utp.usuario.idUsuario == pIdUsuario");

      query.declareImports("import sigefirrhh.sistema.UsuarioTipoPersonal");
      query.declareVariables("UsuarioTipoPersonal utp");

      query.declareParameters("String pAprobacionMpd, String pManejaRac, long pIdUsuario, long idClasificacionPersonal");

      parameters.put("pIdUsuario", new Long(idUsuario));
    }
    else {
      query.declareParameters("String pAprobacionMpd, String pManejaRac, long pIdClasificacionPersonal");
    }

    parameters.put("pAprobacionMpd", aprobacionMpd);
    parameters.put("pManejaRac", manejaRac);
    parameters.put("pIdClasificacionPersonal", new Long(idClasificacionPersonal));

    query.setFilter(filter.toString());
    query.setOrdering("idTipoPersonal ascending");

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }
}