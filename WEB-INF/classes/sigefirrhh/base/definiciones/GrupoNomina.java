package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class GrupoNomina
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_PERIODICIDAD;
  protected static final Map LISTA_SI_NO;
  private long idGrupoNomina;
  private int codGrupoNomina;
  private String nombre;
  private String periodicidad;
  private double anticipoQuincenal;
  private String pagosNominaEgresados;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anticipoQuincenal", "codGrupoNomina", "idGrupoNomina", "nombre", "organismo", "pagosNominaEgresados", "periodicidad" }; private static final Class[] jdoFieldTypes = { Double.TYPE, Integer.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 26, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new GrupoNomina());

    LISTA_PERIODICIDAD = 
      new LinkedHashMap();
    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_PERIODICIDAD.put("Q", "QUINCENAL");
    LISTA_PERIODICIDAD.put("S", "SEMANAL");
    LISTA_PERIODICIDAD.put("M", "MENSUAL");
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public GrupoNomina()
  {
    jdoSetperiodicidad(this, "Q");

    jdoSetanticipoQuincenal(this, 0.0D);

    jdoSetpagosNominaEgresados(this, "N");
  }

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodGrupoNomina(this);
  }

  public double getAnticipoQuincenal() {
    return jdoGetanticipoQuincenal(this);
  }

  public void setAnticipoQuincenal(double anticipoQuincenal) {
    jdoSetanticipoQuincenal(this, anticipoQuincenal);
  }

  public int getCodGrupoNomina() {
    return jdoGetcodGrupoNomina(this);
  }

  public void setCodGrupoNomina(int codGrupoNomina) {
    jdoSetcodGrupoNomina(this, codGrupoNomina);
  }

  public long getIdGrupoNomina() {
    return jdoGetidGrupoNomina(this);
  }

  public void setIdGrupoNomina(long idGrupoNomina) {
    jdoSetidGrupoNomina(this, idGrupoNomina);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }

  public String getPagosNominaEgresados() {
    return jdoGetpagosNominaEgresados(this);
  }

  public void setPagosNominaEgresados(String pagosNominaEgresados) {
    jdoSetpagosNominaEgresados(this, pagosNominaEgresados);
  }

  public String getPeriodicidad() {
    return jdoGetperiodicidad(this);
  }

  public void setPeriodicidad(String periodicidad) {
    jdoSetperiodicidad(this, periodicidad);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    GrupoNomina localGrupoNomina = new GrupoNomina();
    localGrupoNomina.jdoFlags = 1;
    localGrupoNomina.jdoStateManager = paramStateManager;
    return localGrupoNomina;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    GrupoNomina localGrupoNomina = new GrupoNomina();
    localGrupoNomina.jdoCopyKeyFieldsFromObjectId(paramObject);
    localGrupoNomina.jdoFlags = 1;
    localGrupoNomina.jdoStateManager = paramStateManager;
    return localGrupoNomina;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anticipoQuincenal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codGrupoNomina);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idGrupoNomina);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.pagosNominaEgresados);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.periodicidad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anticipoQuincenal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codGrupoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idGrupoNomina = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pagosNominaEgresados = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.periodicidad = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(GrupoNomina paramGrupoNomina, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramGrupoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.anticipoQuincenal = paramGrupoNomina.anticipoQuincenal;
      return;
    case 1:
      if (paramGrupoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.codGrupoNomina = paramGrupoNomina.codGrupoNomina;
      return;
    case 2:
      if (paramGrupoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.idGrupoNomina = paramGrupoNomina.idGrupoNomina;
      return;
    case 3:
      if (paramGrupoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramGrupoNomina.nombre;
      return;
    case 4:
      if (paramGrupoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramGrupoNomina.organismo;
      return;
    case 5:
      if (paramGrupoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.pagosNominaEgresados = paramGrupoNomina.pagosNominaEgresados;
      return;
    case 6:
      if (paramGrupoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.periodicidad = paramGrupoNomina.periodicidad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof GrupoNomina))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    GrupoNomina localGrupoNomina = (GrupoNomina)paramObject;
    if (localGrupoNomina.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localGrupoNomina, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new GrupoNominaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new GrupoNominaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GrupoNominaPK))
      throw new IllegalArgumentException("arg1");
    GrupoNominaPK localGrupoNominaPK = (GrupoNominaPK)paramObject;
    localGrupoNominaPK.idGrupoNomina = this.idGrupoNomina;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GrupoNominaPK))
      throw new IllegalArgumentException("arg1");
    GrupoNominaPK localGrupoNominaPK = (GrupoNominaPK)paramObject;
    this.idGrupoNomina = localGrupoNominaPK.idGrupoNomina;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GrupoNominaPK))
      throw new IllegalArgumentException("arg2");
    GrupoNominaPK localGrupoNominaPK = (GrupoNominaPK)paramObject;
    localGrupoNominaPK.idGrupoNomina = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GrupoNominaPK))
      throw new IllegalArgumentException("arg2");
    GrupoNominaPK localGrupoNominaPK = (GrupoNominaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localGrupoNominaPK.idGrupoNomina);
  }

  private static final double jdoGetanticipoQuincenal(GrupoNomina paramGrupoNomina)
  {
    if (paramGrupoNomina.jdoFlags <= 0)
      return paramGrupoNomina.anticipoQuincenal;
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoNomina.anticipoQuincenal;
    if (localStateManager.isLoaded(paramGrupoNomina, jdoInheritedFieldCount + 0))
      return paramGrupoNomina.anticipoQuincenal;
    return localStateManager.getDoubleField(paramGrupoNomina, jdoInheritedFieldCount + 0, paramGrupoNomina.anticipoQuincenal);
  }

  private static final void jdoSetanticipoQuincenal(GrupoNomina paramGrupoNomina, double paramDouble)
  {
    if (paramGrupoNomina.jdoFlags == 0)
    {
      paramGrupoNomina.anticipoQuincenal = paramDouble;
      return;
    }
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoNomina.anticipoQuincenal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramGrupoNomina, jdoInheritedFieldCount + 0, paramGrupoNomina.anticipoQuincenal, paramDouble);
  }

  private static final int jdoGetcodGrupoNomina(GrupoNomina paramGrupoNomina)
  {
    if (paramGrupoNomina.jdoFlags <= 0)
      return paramGrupoNomina.codGrupoNomina;
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoNomina.codGrupoNomina;
    if (localStateManager.isLoaded(paramGrupoNomina, jdoInheritedFieldCount + 1))
      return paramGrupoNomina.codGrupoNomina;
    return localStateManager.getIntField(paramGrupoNomina, jdoInheritedFieldCount + 1, paramGrupoNomina.codGrupoNomina);
  }

  private static final void jdoSetcodGrupoNomina(GrupoNomina paramGrupoNomina, int paramInt)
  {
    if (paramGrupoNomina.jdoFlags == 0)
    {
      paramGrupoNomina.codGrupoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoNomina.codGrupoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramGrupoNomina, jdoInheritedFieldCount + 1, paramGrupoNomina.codGrupoNomina, paramInt);
  }

  private static final long jdoGetidGrupoNomina(GrupoNomina paramGrupoNomina)
  {
    return paramGrupoNomina.idGrupoNomina;
  }

  private static final void jdoSetidGrupoNomina(GrupoNomina paramGrupoNomina, long paramLong)
  {
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoNomina.idGrupoNomina = paramLong;
      return;
    }
    localStateManager.setLongField(paramGrupoNomina, jdoInheritedFieldCount + 2, paramGrupoNomina.idGrupoNomina, paramLong);
  }

  private static final String jdoGetnombre(GrupoNomina paramGrupoNomina)
  {
    if (paramGrupoNomina.jdoFlags <= 0)
      return paramGrupoNomina.nombre;
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoNomina.nombre;
    if (localStateManager.isLoaded(paramGrupoNomina, jdoInheritedFieldCount + 3))
      return paramGrupoNomina.nombre;
    return localStateManager.getStringField(paramGrupoNomina, jdoInheritedFieldCount + 3, paramGrupoNomina.nombre);
  }

  private static final void jdoSetnombre(GrupoNomina paramGrupoNomina, String paramString)
  {
    if (paramGrupoNomina.jdoFlags == 0)
    {
      paramGrupoNomina.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoNomina.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoNomina, jdoInheritedFieldCount + 3, paramGrupoNomina.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoNomina.organismo;
    if (localStateManager.isLoaded(paramGrupoNomina, jdoInheritedFieldCount + 4))
      return paramGrupoNomina.organismo;
    return (Organismo)localStateManager.getObjectField(paramGrupoNomina, jdoInheritedFieldCount + 4, paramGrupoNomina.organismo);
  }

  private static final void jdoSetorganismo(GrupoNomina paramGrupoNomina, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoNomina.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramGrupoNomina, jdoInheritedFieldCount + 4, paramGrupoNomina.organismo, paramOrganismo);
  }

  private static final String jdoGetpagosNominaEgresados(GrupoNomina paramGrupoNomina)
  {
    if (paramGrupoNomina.jdoFlags <= 0)
      return paramGrupoNomina.pagosNominaEgresados;
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoNomina.pagosNominaEgresados;
    if (localStateManager.isLoaded(paramGrupoNomina, jdoInheritedFieldCount + 5))
      return paramGrupoNomina.pagosNominaEgresados;
    return localStateManager.getStringField(paramGrupoNomina, jdoInheritedFieldCount + 5, paramGrupoNomina.pagosNominaEgresados);
  }

  private static final void jdoSetpagosNominaEgresados(GrupoNomina paramGrupoNomina, String paramString)
  {
    if (paramGrupoNomina.jdoFlags == 0)
    {
      paramGrupoNomina.pagosNominaEgresados = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoNomina.pagosNominaEgresados = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoNomina, jdoInheritedFieldCount + 5, paramGrupoNomina.pagosNominaEgresados, paramString);
  }

  private static final String jdoGetperiodicidad(GrupoNomina paramGrupoNomina)
  {
    if (paramGrupoNomina.jdoFlags <= 0)
      return paramGrupoNomina.periodicidad;
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoNomina.periodicidad;
    if (localStateManager.isLoaded(paramGrupoNomina, jdoInheritedFieldCount + 6))
      return paramGrupoNomina.periodicidad;
    return localStateManager.getStringField(paramGrupoNomina, jdoInheritedFieldCount + 6, paramGrupoNomina.periodicidad);
  }

  private static final void jdoSetperiodicidad(GrupoNomina paramGrupoNomina, String paramString)
  {
    if (paramGrupoNomina.jdoFlags == 0)
    {
      paramGrupoNomina.periodicidad = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoNomina.periodicidad = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoNomina, jdoInheritedFieldCount + 6, paramGrupoNomina.periodicidad, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}