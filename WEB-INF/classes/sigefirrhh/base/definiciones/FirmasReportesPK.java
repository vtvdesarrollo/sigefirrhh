package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class FirmasReportesPK
  implements Serializable
{
  public long idFirmasReportes;

  public FirmasReportesPK()
  {
  }

  public FirmasReportesPK(long idFirmasReportes)
  {
    this.idFirmasReportes = idFirmasReportes;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((FirmasReportesPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(FirmasReportesPK thatPK)
  {
    return 
      this.idFirmasReportes == thatPK.idFirmasReportes;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idFirmasReportes)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idFirmasReportes);
  }
}