package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class BeneficiarioGlobalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addBeneficiarioGlobal(BeneficiarioGlobal beneficiarioGlobal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    BeneficiarioGlobal beneficiarioGlobalNew = 
      (BeneficiarioGlobal)BeanUtils.cloneBean(
      beneficiarioGlobal);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (beneficiarioGlobalNew.getConceptoTipoPersonal() != null) {
      beneficiarioGlobalNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        beneficiarioGlobalNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    BancoBeanBusiness bancoBeanBusiness = new BancoBeanBusiness();

    if (beneficiarioGlobalNew.getBanco() != null) {
      beneficiarioGlobalNew.setBanco(
        bancoBeanBusiness.findBancoById(
        beneficiarioGlobalNew.getBanco().getIdBanco()));
    }
    pm.makePersistent(beneficiarioGlobalNew);
  }

  public void updateBeneficiarioGlobal(BeneficiarioGlobal beneficiarioGlobal) throws Exception
  {
    BeneficiarioGlobal beneficiarioGlobalModify = 
      findBeneficiarioGlobalById(beneficiarioGlobal.getIdBeneficiarioGlobal());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (beneficiarioGlobal.getConceptoTipoPersonal() != null) {
      beneficiarioGlobal.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        beneficiarioGlobal.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    BancoBeanBusiness bancoBeanBusiness = new BancoBeanBusiness();

    if (beneficiarioGlobal.getBanco() != null) {
      beneficiarioGlobal.setBanco(
        bancoBeanBusiness.findBancoById(
        beneficiarioGlobal.getBanco().getIdBanco()));
    }

    BeanUtils.copyProperties(beneficiarioGlobalModify, beneficiarioGlobal);
  }

  public void deleteBeneficiarioGlobal(BeneficiarioGlobal beneficiarioGlobal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    BeneficiarioGlobal beneficiarioGlobalDelete = 
      findBeneficiarioGlobalById(beneficiarioGlobal.getIdBeneficiarioGlobal());
    pm.deletePersistent(beneficiarioGlobalDelete);
  }

  public BeneficiarioGlobal findBeneficiarioGlobalById(long idBeneficiarioGlobal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idBeneficiarioGlobal == pIdBeneficiarioGlobal";
    Query query = pm.newQuery(BeneficiarioGlobal.class, filter);

    query.declareParameters("long pIdBeneficiarioGlobal");

    parameters.put("pIdBeneficiarioGlobal", new Long(idBeneficiarioGlobal));

    Collection colBeneficiarioGlobal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colBeneficiarioGlobal.iterator();
    return (BeneficiarioGlobal)iterator.next();
  }

  public Collection findBeneficiarioGlobalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent beneficiarioGlobalExtent = pm.getExtent(
      BeneficiarioGlobal.class, true);
    Query query = pm.newQuery(beneficiarioGlobalExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal";

    Query query = pm.newQuery(BeneficiarioGlobal.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    Collection colBeneficiarioGlobal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colBeneficiarioGlobal);

    return colBeneficiarioGlobal;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(BeneficiarioGlobal.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    Collection colBeneficiarioGlobal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colBeneficiarioGlobal);

    return colBeneficiarioGlobal;
  }
}