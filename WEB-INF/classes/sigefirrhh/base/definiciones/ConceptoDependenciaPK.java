package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ConceptoDependenciaPK
  implements Serializable
{
  public long idConceptoDependencia;

  public ConceptoDependenciaPK()
  {
  }

  public ConceptoDependenciaPK(long idConceptoDependencia)
  {
    this.idConceptoDependencia = idConceptoDependencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoDependenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoDependenciaPK thatPK)
  {
    return 
      this.idConceptoDependencia == thatPK.idConceptoDependencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoDependencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoDependencia);
  }
}