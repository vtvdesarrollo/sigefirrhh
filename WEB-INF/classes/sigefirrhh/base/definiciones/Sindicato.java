package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Sindicato
  implements Serializable, PersistenceCapable
{
  private long idSindicato;
  private String codSindicato;
  private String nombre;
  private Concepto concepto;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codSindicato", "concepto", "idSindicato", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 26, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcodSindicato(this) + "  -  " + 
      jdoGetnombre(this);
  }

  public String getCodSindicato()
  {
    return jdoGetcodSindicato(this);
  }

  public Concepto getConcepto()
  {
    return jdoGetconcepto(this);
  }

  public long getIdSindicato()
  {
    return jdoGetidSindicato(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodSindicato(String string)
  {
    jdoSetcodSindicato(this, string);
  }

  public void setConcepto(Concepto concepto)
  {
    jdoSetconcepto(this, concepto);
  }

  public void setIdSindicato(long l)
  {
    jdoSetidSindicato(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.Sindicato"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Sindicato());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Sindicato localSindicato = new Sindicato();
    localSindicato.jdoFlags = 1;
    localSindicato.jdoStateManager = paramStateManager;
    return localSindicato;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Sindicato localSindicato = new Sindicato();
    localSindicato.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSindicato.jdoFlags = 1;
    localSindicato.jdoStateManager = paramStateManager;
    return localSindicato;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSindicato);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.concepto);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSindicato);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSindicato = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.concepto = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSindicato = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Sindicato paramSindicato, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSindicato == null)
        throw new IllegalArgumentException("arg1");
      this.codSindicato = paramSindicato.codSindicato;
      return;
    case 1:
      if (paramSindicato == null)
        throw new IllegalArgumentException("arg1");
      this.concepto = paramSindicato.concepto;
      return;
    case 2:
      if (paramSindicato == null)
        throw new IllegalArgumentException("arg1");
      this.idSindicato = paramSindicato.idSindicato;
      return;
    case 3:
      if (paramSindicato == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramSindicato.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Sindicato))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Sindicato localSindicato = (Sindicato)paramObject;
    if (localSindicato.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSindicato, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SindicatoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SindicatoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SindicatoPK))
      throw new IllegalArgumentException("arg1");
    SindicatoPK localSindicatoPK = (SindicatoPK)paramObject;
    localSindicatoPK.idSindicato = this.idSindicato;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SindicatoPK))
      throw new IllegalArgumentException("arg1");
    SindicatoPK localSindicatoPK = (SindicatoPK)paramObject;
    this.idSindicato = localSindicatoPK.idSindicato;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SindicatoPK))
      throw new IllegalArgumentException("arg2");
    SindicatoPK localSindicatoPK = (SindicatoPK)paramObject;
    localSindicatoPK.idSindicato = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SindicatoPK))
      throw new IllegalArgumentException("arg2");
    SindicatoPK localSindicatoPK = (SindicatoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localSindicatoPK.idSindicato);
  }

  private static final String jdoGetcodSindicato(Sindicato paramSindicato)
  {
    if (paramSindicato.jdoFlags <= 0)
      return paramSindicato.codSindicato;
    StateManager localStateManager = paramSindicato.jdoStateManager;
    if (localStateManager == null)
      return paramSindicato.codSindicato;
    if (localStateManager.isLoaded(paramSindicato, jdoInheritedFieldCount + 0))
      return paramSindicato.codSindicato;
    return localStateManager.getStringField(paramSindicato, jdoInheritedFieldCount + 0, paramSindicato.codSindicato);
  }

  private static final void jdoSetcodSindicato(Sindicato paramSindicato, String paramString)
  {
    if (paramSindicato.jdoFlags == 0)
    {
      paramSindicato.codSindicato = paramString;
      return;
    }
    StateManager localStateManager = paramSindicato.jdoStateManager;
    if (localStateManager == null)
    {
      paramSindicato.codSindicato = paramString;
      return;
    }
    localStateManager.setStringField(paramSindicato, jdoInheritedFieldCount + 0, paramSindicato.codSindicato, paramString);
  }

  private static final Concepto jdoGetconcepto(Sindicato paramSindicato)
  {
    StateManager localStateManager = paramSindicato.jdoStateManager;
    if (localStateManager == null)
      return paramSindicato.concepto;
    if (localStateManager.isLoaded(paramSindicato, jdoInheritedFieldCount + 1))
      return paramSindicato.concepto;
    return (Concepto)localStateManager.getObjectField(paramSindicato, jdoInheritedFieldCount + 1, paramSindicato.concepto);
  }

  private static final void jdoSetconcepto(Sindicato paramSindicato, Concepto paramConcepto)
  {
    StateManager localStateManager = paramSindicato.jdoStateManager;
    if (localStateManager == null)
    {
      paramSindicato.concepto = paramConcepto;
      return;
    }
    localStateManager.setObjectField(paramSindicato, jdoInheritedFieldCount + 1, paramSindicato.concepto, paramConcepto);
  }

  private static final long jdoGetidSindicato(Sindicato paramSindicato)
  {
    return paramSindicato.idSindicato;
  }

  private static final void jdoSetidSindicato(Sindicato paramSindicato, long paramLong)
  {
    StateManager localStateManager = paramSindicato.jdoStateManager;
    if (localStateManager == null)
    {
      paramSindicato.idSindicato = paramLong;
      return;
    }
    localStateManager.setLongField(paramSindicato, jdoInheritedFieldCount + 2, paramSindicato.idSindicato, paramLong);
  }

  private static final String jdoGetnombre(Sindicato paramSindicato)
  {
    if (paramSindicato.jdoFlags <= 0)
      return paramSindicato.nombre;
    StateManager localStateManager = paramSindicato.jdoStateManager;
    if (localStateManager == null)
      return paramSindicato.nombre;
    if (localStateManager.isLoaded(paramSindicato, jdoInheritedFieldCount + 3))
      return paramSindicato.nombre;
    return localStateManager.getStringField(paramSindicato, jdoInheritedFieldCount + 3, paramSindicato.nombre);
  }

  private static final void jdoSetnombre(Sindicato paramSindicato, String paramString)
  {
    if (paramSindicato.jdoFlags == 0)
    {
      paramSindicato.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramSindicato.jdoStateManager;
    if (localStateManager == null)
    {
      paramSindicato.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramSindicato, jdoInheritedFieldCount + 3, paramSindicato.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}