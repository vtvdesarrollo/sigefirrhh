package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ContratoColectivo
  implements Serializable, PersistenceCapable
{
  private long idContratoColectivo;
  private String codContratoColectivo;
  private String denominacion;
  private String descripcion;
  private String sector;
  private Date fechaVigencia;
  private Date fechaVencimiento;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codContratoColectivo", "denominacion", "descripcion", "fechaVencimiento", "fechaVigencia", "idContratoColectivo", "idSitp", "sector", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdenominacion(this);
  }

  public String getCodContratoColectivo()
  {
    return jdoGetcodContratoColectivo(this);
  }

  public String getDenominacion()
  {
    return jdoGetdenominacion(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public Date getFechaVencimiento()
  {
    return jdoGetfechaVencimiento(this);
  }

  public Date getFechaVigencia()
  {
    return jdoGetfechaVigencia(this);
  }

  public long getIdContratoColectivo()
  {
    return jdoGetidContratoColectivo(this);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public String getSector()
  {
    return jdoGetsector(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setCodContratoColectivo(String string)
  {
    jdoSetcodContratoColectivo(this, string);
  }

  public void setDenominacion(String string)
  {
    jdoSetdenominacion(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setFechaVencimiento(Date date)
  {
    jdoSetfechaVencimiento(this, date);
  }

  public void setFechaVigencia(Date date)
  {
    jdoSetfechaVigencia(this, date);
  }

  public void setIdContratoColectivo(long l)
  {
    jdoSetidContratoColectivo(this, l);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setSector(String string)
  {
    jdoSetsector(this, string);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ContratoColectivo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ContratoColectivo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ContratoColectivo localContratoColectivo = new ContratoColectivo();
    localContratoColectivo.jdoFlags = 1;
    localContratoColectivo.jdoStateManager = paramStateManager;
    return localContratoColectivo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ContratoColectivo localContratoColectivo = new ContratoColectivo();
    localContratoColectivo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localContratoColectivo.jdoFlags = 1;
    localContratoColectivo.jdoStateManager = paramStateManager;
    return localContratoColectivo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codContratoColectivo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.denominacion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVencimiento);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idContratoColectivo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sector);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codContratoColectivo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.denominacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVencimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idContratoColectivo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sector = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ContratoColectivo paramContratoColectivo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramContratoColectivo == null)
        throw new IllegalArgumentException("arg1");
      this.codContratoColectivo = paramContratoColectivo.codContratoColectivo;
      return;
    case 1:
      if (paramContratoColectivo == null)
        throw new IllegalArgumentException("arg1");
      this.denominacion = paramContratoColectivo.denominacion;
      return;
    case 2:
      if (paramContratoColectivo == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramContratoColectivo.descripcion;
      return;
    case 3:
      if (paramContratoColectivo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVencimiento = paramContratoColectivo.fechaVencimiento;
      return;
    case 4:
      if (paramContratoColectivo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramContratoColectivo.fechaVigencia;
      return;
    case 5:
      if (paramContratoColectivo == null)
        throw new IllegalArgumentException("arg1");
      this.idContratoColectivo = paramContratoColectivo.idContratoColectivo;
      return;
    case 6:
      if (paramContratoColectivo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramContratoColectivo.idSitp;
      return;
    case 7:
      if (paramContratoColectivo == null)
        throw new IllegalArgumentException("arg1");
      this.sector = paramContratoColectivo.sector;
      return;
    case 8:
      if (paramContratoColectivo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramContratoColectivo.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ContratoColectivo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ContratoColectivo localContratoColectivo = (ContratoColectivo)paramObject;
    if (localContratoColectivo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localContratoColectivo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ContratoColectivoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ContratoColectivoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ContratoColectivoPK))
      throw new IllegalArgumentException("arg1");
    ContratoColectivoPK localContratoColectivoPK = (ContratoColectivoPK)paramObject;
    localContratoColectivoPK.idContratoColectivo = this.idContratoColectivo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ContratoColectivoPK))
      throw new IllegalArgumentException("arg1");
    ContratoColectivoPK localContratoColectivoPK = (ContratoColectivoPK)paramObject;
    this.idContratoColectivo = localContratoColectivoPK.idContratoColectivo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ContratoColectivoPK))
      throw new IllegalArgumentException("arg2");
    ContratoColectivoPK localContratoColectivoPK = (ContratoColectivoPK)paramObject;
    localContratoColectivoPK.idContratoColectivo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ContratoColectivoPK))
      throw new IllegalArgumentException("arg2");
    ContratoColectivoPK localContratoColectivoPK = (ContratoColectivoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localContratoColectivoPK.idContratoColectivo);
  }

  private static final String jdoGetcodContratoColectivo(ContratoColectivo paramContratoColectivo)
  {
    if (paramContratoColectivo.jdoFlags <= 0)
      return paramContratoColectivo.codContratoColectivo;
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
      return paramContratoColectivo.codContratoColectivo;
    if (localStateManager.isLoaded(paramContratoColectivo, jdoInheritedFieldCount + 0))
      return paramContratoColectivo.codContratoColectivo;
    return localStateManager.getStringField(paramContratoColectivo, jdoInheritedFieldCount + 0, paramContratoColectivo.codContratoColectivo);
  }

  private static final void jdoSetcodContratoColectivo(ContratoColectivo paramContratoColectivo, String paramString)
  {
    if (paramContratoColectivo.jdoFlags == 0)
    {
      paramContratoColectivo.codContratoColectivo = paramString;
      return;
    }
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratoColectivo.codContratoColectivo = paramString;
      return;
    }
    localStateManager.setStringField(paramContratoColectivo, jdoInheritedFieldCount + 0, paramContratoColectivo.codContratoColectivo, paramString);
  }

  private static final String jdoGetdenominacion(ContratoColectivo paramContratoColectivo)
  {
    if (paramContratoColectivo.jdoFlags <= 0)
      return paramContratoColectivo.denominacion;
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
      return paramContratoColectivo.denominacion;
    if (localStateManager.isLoaded(paramContratoColectivo, jdoInheritedFieldCount + 1))
      return paramContratoColectivo.denominacion;
    return localStateManager.getStringField(paramContratoColectivo, jdoInheritedFieldCount + 1, paramContratoColectivo.denominacion);
  }

  private static final void jdoSetdenominacion(ContratoColectivo paramContratoColectivo, String paramString)
  {
    if (paramContratoColectivo.jdoFlags == 0)
    {
      paramContratoColectivo.denominacion = paramString;
      return;
    }
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratoColectivo.denominacion = paramString;
      return;
    }
    localStateManager.setStringField(paramContratoColectivo, jdoInheritedFieldCount + 1, paramContratoColectivo.denominacion, paramString);
  }

  private static final String jdoGetdescripcion(ContratoColectivo paramContratoColectivo)
  {
    if (paramContratoColectivo.jdoFlags <= 0)
      return paramContratoColectivo.descripcion;
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
      return paramContratoColectivo.descripcion;
    if (localStateManager.isLoaded(paramContratoColectivo, jdoInheritedFieldCount + 2))
      return paramContratoColectivo.descripcion;
    return localStateManager.getStringField(paramContratoColectivo, jdoInheritedFieldCount + 2, paramContratoColectivo.descripcion);
  }

  private static final void jdoSetdescripcion(ContratoColectivo paramContratoColectivo, String paramString)
  {
    if (paramContratoColectivo.jdoFlags == 0)
    {
      paramContratoColectivo.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratoColectivo.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramContratoColectivo, jdoInheritedFieldCount + 2, paramContratoColectivo.descripcion, paramString);
  }

  private static final Date jdoGetfechaVencimiento(ContratoColectivo paramContratoColectivo)
  {
    if (paramContratoColectivo.jdoFlags <= 0)
      return paramContratoColectivo.fechaVencimiento;
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
      return paramContratoColectivo.fechaVencimiento;
    if (localStateManager.isLoaded(paramContratoColectivo, jdoInheritedFieldCount + 3))
      return paramContratoColectivo.fechaVencimiento;
    return (Date)localStateManager.getObjectField(paramContratoColectivo, jdoInheritedFieldCount + 3, paramContratoColectivo.fechaVencimiento);
  }

  private static final void jdoSetfechaVencimiento(ContratoColectivo paramContratoColectivo, Date paramDate)
  {
    if (paramContratoColectivo.jdoFlags == 0)
    {
      paramContratoColectivo.fechaVencimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratoColectivo.fechaVencimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramContratoColectivo, jdoInheritedFieldCount + 3, paramContratoColectivo.fechaVencimiento, paramDate);
  }

  private static final Date jdoGetfechaVigencia(ContratoColectivo paramContratoColectivo)
  {
    if (paramContratoColectivo.jdoFlags <= 0)
      return paramContratoColectivo.fechaVigencia;
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
      return paramContratoColectivo.fechaVigencia;
    if (localStateManager.isLoaded(paramContratoColectivo, jdoInheritedFieldCount + 4))
      return paramContratoColectivo.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramContratoColectivo, jdoInheritedFieldCount + 4, paramContratoColectivo.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(ContratoColectivo paramContratoColectivo, Date paramDate)
  {
    if (paramContratoColectivo.jdoFlags == 0)
    {
      paramContratoColectivo.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratoColectivo.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramContratoColectivo, jdoInheritedFieldCount + 4, paramContratoColectivo.fechaVigencia, paramDate);
  }

  private static final long jdoGetidContratoColectivo(ContratoColectivo paramContratoColectivo)
  {
    return paramContratoColectivo.idContratoColectivo;
  }

  private static final void jdoSetidContratoColectivo(ContratoColectivo paramContratoColectivo, long paramLong)
  {
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratoColectivo.idContratoColectivo = paramLong;
      return;
    }
    localStateManager.setLongField(paramContratoColectivo, jdoInheritedFieldCount + 5, paramContratoColectivo.idContratoColectivo, paramLong);
  }

  private static final int jdoGetidSitp(ContratoColectivo paramContratoColectivo)
  {
    if (paramContratoColectivo.jdoFlags <= 0)
      return paramContratoColectivo.idSitp;
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
      return paramContratoColectivo.idSitp;
    if (localStateManager.isLoaded(paramContratoColectivo, jdoInheritedFieldCount + 6))
      return paramContratoColectivo.idSitp;
    return localStateManager.getIntField(paramContratoColectivo, jdoInheritedFieldCount + 6, paramContratoColectivo.idSitp);
  }

  private static final void jdoSetidSitp(ContratoColectivo paramContratoColectivo, int paramInt)
  {
    if (paramContratoColectivo.jdoFlags == 0)
    {
      paramContratoColectivo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratoColectivo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramContratoColectivo, jdoInheritedFieldCount + 6, paramContratoColectivo.idSitp, paramInt);
  }

  private static final String jdoGetsector(ContratoColectivo paramContratoColectivo)
  {
    if (paramContratoColectivo.jdoFlags <= 0)
      return paramContratoColectivo.sector;
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
      return paramContratoColectivo.sector;
    if (localStateManager.isLoaded(paramContratoColectivo, jdoInheritedFieldCount + 7))
      return paramContratoColectivo.sector;
    return localStateManager.getStringField(paramContratoColectivo, jdoInheritedFieldCount + 7, paramContratoColectivo.sector);
  }

  private static final void jdoSetsector(ContratoColectivo paramContratoColectivo, String paramString)
  {
    if (paramContratoColectivo.jdoFlags == 0)
    {
      paramContratoColectivo.sector = paramString;
      return;
    }
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratoColectivo.sector = paramString;
      return;
    }
    localStateManager.setStringField(paramContratoColectivo, jdoInheritedFieldCount + 7, paramContratoColectivo.sector, paramString);
  }

  private static final Date jdoGettiempoSitp(ContratoColectivo paramContratoColectivo)
  {
    if (paramContratoColectivo.jdoFlags <= 0)
      return paramContratoColectivo.tiempoSitp;
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
      return paramContratoColectivo.tiempoSitp;
    if (localStateManager.isLoaded(paramContratoColectivo, jdoInheritedFieldCount + 8))
      return paramContratoColectivo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramContratoColectivo, jdoInheritedFieldCount + 8, paramContratoColectivo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ContratoColectivo paramContratoColectivo, Date paramDate)
  {
    if (paramContratoColectivo.jdoFlags == 0)
    {
      paramContratoColectivo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramContratoColectivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratoColectivo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramContratoColectivo, jdoInheritedFieldCount + 8, paramContratoColectivo.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}