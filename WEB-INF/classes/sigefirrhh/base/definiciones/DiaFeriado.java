package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class DiaFeriado
  implements Serializable, PersistenceCapable
{
  private long idDiaFeriado;
  private Date dia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "dia", "idDiaFeriado" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.util.Date"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 24, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Date getDia()
  {
    return jdoGetdia(this);
  }

  public long getIdDiaFeriado()
  {
    return jdoGetidDiaFeriado(this);
  }

  public void setDia(Date date)
  {
    jdoSetdia(this, date);
  }

  public void setIdDiaFeriado(long l)
  {
    jdoSetidDiaFeriado(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 2;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.DiaFeriado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DiaFeriado());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DiaFeriado localDiaFeriado = new DiaFeriado();
    localDiaFeriado.jdoFlags = 1;
    localDiaFeriado.jdoStateManager = paramStateManager;
    return localDiaFeriado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DiaFeriado localDiaFeriado = new DiaFeriado();
    localDiaFeriado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDiaFeriado.jdoFlags = 1;
    localDiaFeriado.jdoStateManager = paramStateManager;
    return localDiaFeriado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDiaFeriado);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDiaFeriado = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DiaFeriado paramDiaFeriado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDiaFeriado == null)
        throw new IllegalArgumentException("arg1");
      this.dia = paramDiaFeriado.dia;
      return;
    case 1:
      if (paramDiaFeriado == null)
        throw new IllegalArgumentException("arg1");
      this.idDiaFeriado = paramDiaFeriado.idDiaFeriado;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DiaFeriado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DiaFeriado localDiaFeriado = (DiaFeriado)paramObject;
    if (localDiaFeriado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDiaFeriado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DiaFeriadoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DiaFeriadoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DiaFeriadoPK))
      throw new IllegalArgumentException("arg1");
    DiaFeriadoPK localDiaFeriadoPK = (DiaFeriadoPK)paramObject;
    localDiaFeriadoPK.dia = this.dia;
    localDiaFeriadoPK.idDiaFeriado = this.idDiaFeriado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DiaFeriadoPK))
      throw new IllegalArgumentException("arg1");
    DiaFeriadoPK localDiaFeriadoPK = (DiaFeriadoPK)paramObject;
    this.dia = localDiaFeriadoPK.dia;
    this.idDiaFeriado = localDiaFeriadoPK.idDiaFeriado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DiaFeriadoPK))
      throw new IllegalArgumentException("arg2");
    DiaFeriadoPK localDiaFeriadoPK = (DiaFeriadoPK)paramObject;
    localDiaFeriadoPK.dia = ((Date)paramObjectIdFieldSupplier.fetchObjectField(jdoInheritedFieldCount + 0));
    localDiaFeriadoPK.idDiaFeriado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DiaFeriadoPK))
      throw new IllegalArgumentException("arg2");
    DiaFeriadoPK localDiaFeriadoPK = (DiaFeriadoPK)paramObject;
    paramObjectIdFieldConsumer.storeObjectField(jdoInheritedFieldCount + 0, localDiaFeriadoPK.dia);
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localDiaFeriadoPK.idDiaFeriado);
  }

  private static final Date jdoGetdia(DiaFeriado paramDiaFeriado)
  {
    return paramDiaFeriado.dia;
  }

  private static final void jdoSetdia(DiaFeriado paramDiaFeriado, Date paramDate)
  {
    StateManager localStateManager = paramDiaFeriado.jdoStateManager;
    if (localStateManager == null)
    {
      paramDiaFeriado.dia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramDiaFeriado, jdoInheritedFieldCount + 0, paramDiaFeriado.dia, paramDate);
  }

  private static final long jdoGetidDiaFeriado(DiaFeriado paramDiaFeriado)
  {
    return paramDiaFeriado.idDiaFeriado;
  }

  private static final void jdoSetidDiaFeriado(DiaFeriado paramDiaFeriado, long paramLong)
  {
    StateManager localStateManager = paramDiaFeriado.jdoStateManager;
    if (localStateManager == null)
    {
      paramDiaFeriado.idDiaFeriado = paramLong;
      return;
    }
    localStateManager.setLongField(paramDiaFeriado, jdoInheritedFieldCount + 1, paramDiaFeriado.idDiaFeriado, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}