package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.bienestar.ticketAlimentacion.GrupoTicket;
import sigefirrhh.bienestar.ticketAlimentacion.TicketAlimentacionFacade;
import sigefirrhh.login.LoginSession;

public class TipoPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TipoPersonalForm.class.getName());
  private TipoPersonal tipoPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private TicketAlimentacionFacade ticketAlimentacionFacade = new TicketAlimentacionFacade();
  private boolean showTipoPersonalByCodTipoPersonal;
  private boolean showTipoPersonalByNombre;
  private String findCodTipoPersonal;
  private String findNombre;
  private Collection colClasificacionPersonal;
  private Collection colGrupoNomina;
  private Collection colGrupoOrganismo;
  private Collection colGrupoTicket;
  private Collection colTurno;
  private Collection colBancoNomina;
  private Collection colBancoLph;
  private Collection colBancoFid;
  private String selectClasificacionPersonal;
  private String selectGrupoNomina;
  private String selectGrupoOrganismo;
  private String selectGrupoTicket;
  private String selectTurno;
  private String selectBancoNomina;
  private String selectBancoLph;
  private String selectBancoFid;
  private Object stateResultTipoPersonalByCodTipoPersonal = null;

  private Object stateResultTipoPersonalByNombre = null;

  public String getFindCodTipoPersonal()
  {
    return this.findCodTipoPersonal;
  }
  public void setFindCodTipoPersonal(String findCodTipoPersonal) {
    this.findCodTipoPersonal = findCodTipoPersonal;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectClasificacionPersonal()
  {
    return this.selectClasificacionPersonal;
  }
  public void setSelectClasificacionPersonal(String valClasificacionPersonal) {
    Iterator iterator = this.colClasificacionPersonal.iterator();
    ClasificacionPersonal clasificacionPersonal = null;
    this.tipoPersonal.setClasificacionPersonal(null);
    while (iterator.hasNext()) {
      clasificacionPersonal = (ClasificacionPersonal)iterator.next();
      if (String.valueOf(clasificacionPersonal.getIdClasificacionPersonal()).equals(
        valClasificacionPersonal)) {
        this.tipoPersonal.setClasificacionPersonal(
          clasificacionPersonal);
        break;
      }
    }
    this.selectClasificacionPersonal = valClasificacionPersonal;
  }
  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String valGrupoNomina) {
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    this.tipoPersonal.setGrupoNomina(null);
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      if (String.valueOf(grupoNomina.getIdGrupoNomina()).equals(
        valGrupoNomina)) {
        this.tipoPersonal.setGrupoNomina(
          grupoNomina);
        break;
      }
    }
    this.selectGrupoNomina = valGrupoNomina;
  }
  public String getSelectGrupoOrganismo() {
    return this.selectGrupoOrganismo;
  }
  public void setSelectGrupoOrganismo(String valGrupoOrganismo) {
    Iterator iterator = this.colGrupoOrganismo.iterator();
    GrupoOrganismo grupoOrganismo = null;
    this.tipoPersonal.setGrupoOrganismo(null);
    while (iterator.hasNext()) {
      grupoOrganismo = (GrupoOrganismo)iterator.next();
      if (String.valueOf(grupoOrganismo.getIdGrupoOrganismo()).equals(
        valGrupoOrganismo)) {
        this.tipoPersonal.setGrupoOrganismo(
          grupoOrganismo);
        break;
      }
    }
    this.selectGrupoOrganismo = valGrupoOrganismo;
  }
  public String getSelectGrupoTicket() {
    return this.selectGrupoTicket;
  }
  public void setSelectGrupoTicket(String valGrupoTicket) {
    Iterator iterator = this.colGrupoTicket.iterator();
    GrupoTicket grupoTicket = null;
    this.tipoPersonal.setGrupoTicket(null);
    while (iterator.hasNext()) {
      grupoTicket = (GrupoTicket)iterator.next();
      if (String.valueOf(grupoTicket.getIdGrupoTicket()).equals(
        valGrupoTicket)) {
        this.tipoPersonal.setGrupoTicket(
          grupoTicket);
        break;
      }
    }
    this.selectGrupoTicket = valGrupoTicket;
  }
  public String getSelectTurno() {
    return this.selectTurno;
  }
  public void setSelectTurno(String valTurno) {
    Iterator iterator = this.colTurno.iterator();
    Turno turno = null;
    this.tipoPersonal.setTurno(null);
    while (iterator.hasNext()) {
      turno = (Turno)iterator.next();
      if (String.valueOf(turno.getIdTurno()).equals(
        valTurno)) {
        this.tipoPersonal.setTurno(
          turno);
        break;
      }
    }
    this.selectTurno = valTurno;
  }
  public String getSelectBancoNomina() {
    return this.selectBancoNomina;
  }
  public void setSelectBancoNomina(String valBancoNomina) {
    Iterator iterator = this.colBancoNomina.iterator();
    Banco bancoNomina = null;
    this.tipoPersonal.setBancoNomina(null);
    while (iterator.hasNext()) {
      bancoNomina = (Banco)iterator.next();
      if (String.valueOf(bancoNomina.getIdBanco()).equals(
        valBancoNomina)) {
        this.tipoPersonal.setBancoNomina(
          bancoNomina);
        break;
      }
    }
    this.selectBancoNomina = valBancoNomina;
  }
  public String getSelectBancoLph() {
    return this.selectBancoLph;
  }
  public void setSelectBancoLph(String valBancoLph) {
    Iterator iterator = this.colBancoLph.iterator();
    Banco bancoLph = null;
    this.tipoPersonal.setBancoLph(null);
    while (iterator.hasNext()) {
      bancoLph = (Banco)iterator.next();
      if (String.valueOf(bancoLph.getIdBanco()).equals(
        valBancoLph)) {
        this.tipoPersonal.setBancoLph(
          bancoLph);
        break;
      }
    }
    this.selectBancoLph = valBancoLph;
  }
  public String getSelectBancoFid() {
    return this.selectBancoFid;
  }
  public void setSelectBancoFid(String valBancoFid) {
    Iterator iterator = this.colBancoFid.iterator();
    Banco bancoFid = null;
    this.tipoPersonal.setBancoFid(null);
    while (iterator.hasNext()) {
      bancoFid = (Banco)iterator.next();
      if (String.valueOf(bancoFid.getIdBanco()).equals(
        valBancoFid)) {
        this.tipoPersonal.setBancoFid(
          bancoFid);
        break;
      }
    }
    this.selectBancoFid = valBancoFid;
  }
  public Collection getResult() {
    return this.result;
  }

  public TipoPersonal getTipoPersonal() {
    if (this.tipoPersonal == null) {
      this.tipoPersonal = new TipoPersonal();
    }
    return this.tipoPersonal;
  }

  public TipoPersonalForm() throws Exception
  {
    newReportId();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColClasificacionPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colClasificacionPersonal.iterator();
    ClasificacionPersonal clasificacionPersonal = null;
    while (iterator.hasNext()) {
      clasificacionPersonal = (ClasificacionPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(clasificacionPersonal.getIdClasificacionPersonal()), 
        clasificacionPersonal.toString()));
    }
    return col;
  }

  public Collection getColGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoNomina.getIdGrupoNomina()), 
        grupoNomina.toString()));
    }
    return col;
  }

  public Collection getColGrupoOrganismo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoOrganismo.iterator();
    GrupoOrganismo grupoOrganismo = null;
    while (iterator.hasNext()) {
      grupoOrganismo = (GrupoOrganismo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoOrganismo.getIdGrupoOrganismo()), 
        grupoOrganismo.toString()));
    }
    return col;
  }

  public Collection getListAprobacionMpd() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListManejaRac() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCalculaPrestaciones() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAsignanDotaciones() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAumentoEvaluacion() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListBeneficioCestaTicket() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColGrupoTicket()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoTicket.iterator();
    GrupoTicket grupoTicket = null;
    while (iterator.hasNext()) {
      grupoTicket = (GrupoTicket)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoTicket.getIdGrupoTicket()), 
        grupoTicket.toString()));
    }
    return col;
  }

  public Collection getListFormulaIntegral() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_INTEGRAL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListFormulaSemanal() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SEMANAL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListMultipleRegistro() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCotizaSso() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCotizaSpf() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCotizaLph() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCotizaFju() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListDisfrutaVacaciones() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColTurno()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTurno.iterator();
    Turno turno = null;
    while (iterator.hasNext()) {
      turno = (Turno)iterator.next();
      col.add(new SelectItem(
        String.valueOf(turno.getIdTurno()), 
        turno.toString()));
    }
    return col;
  }

  public Collection getListDeudaRegimenDerogado() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColBancoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colBancoNomina.iterator();
    Banco bancoNomina = null;
    while (iterator.hasNext()) {
      bancoNomina = (Banco)iterator.next();
      col.add(new SelectItem(
        String.valueOf(bancoNomina.getIdBanco()), 
        bancoNomina.toString()));
    }
    return col;
  }

  public Collection getListFormaPagoNomina() {
    Collection col = new ArrayList();

    Iterator iterEntry = TipoPersonal.LISTA_FORMA_PAGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColBancoLph()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colBancoLph.iterator();
    Banco bancoLph = null;
    while (iterator.hasNext()) {
      bancoLph = (Banco)iterator.next();
      col.add(new SelectItem(
        String.valueOf(bancoLph.getIdBanco()), 
        bancoLph.toString()));
    }
    return col;
  }

  public Collection getColBancoFid()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colBancoFid.iterator();
    Banco bancoFid = null;
    while (iterator.hasNext()) {
      bancoFid = (Banco)iterator.next();
      col.add(new SelectItem(
        String.valueOf(bancoFid.getIdBanco()), 
        bancoFid.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colClasificacionPersonal = 
        this.definicionesFacade.findAllClasificacionPersonal();
      this.colGrupoNomina = 
        this.definicionesFacade.findGrupoNominaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colGrupoOrganismo = 
        this.estructuraFacade.findGrupoOrganismoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colGrupoTicket = 
        this.ticketAlimentacionFacade.findGrupoTicketByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colTurno = 
        this.definicionesFacade.findTurnoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colBancoNomina = 
        this.definicionesFacade.findAllBanco();
      this.colBancoLph = 
        this.definicionesFacade.findAllBanco();
      this.colBancoFid = 
        this.definicionesFacade.findAllBanco();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTipoPersonalByCodTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findTipoPersonalByCodTipoPersonal(this.findCodTipoPersonal, idOrganismo);
      this.showTipoPersonalByCodTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoPersonalByCodTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoPersonal = null;
    this.findNombre = null;

    return null;
  }

  public String findTipoPersonalByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findTipoPersonalByNombre(this.findNombre, idOrganismo);
      this.showTipoPersonalByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoPersonalByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoPersonal = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowTipoPersonalByCodTipoPersonal() {
    return this.showTipoPersonalByCodTipoPersonal;
  }
  public boolean isShowTipoPersonalByNombre() {
    return this.showTipoPersonalByNombre;
  }

  public String selectTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectClasificacionPersonal = null;
    this.selectGrupoNomina = null;
    this.selectGrupoOrganismo = null;
    this.selectGrupoTicket = null;
    this.selectTurno = null;
    this.selectBancoNomina = null;
    this.selectBancoLph = null;
    this.selectBancoFid = null;

    long idTipoPersonal = 
      Long.parseLong((String)requestParameterMap.get("idTipoPersonal"));
    try
    {
      this.tipoPersonal = 
        this.definicionesFacade.findTipoPersonalById(
        idTipoPersonal);
      if (this.tipoPersonal.getClasificacionPersonal() != null) {
        this.selectClasificacionPersonal = 
          String.valueOf(this.tipoPersonal.getClasificacionPersonal().getIdClasificacionPersonal());
      }
      if (this.tipoPersonal.getGrupoNomina() != null) {
        this.selectGrupoNomina = 
          String.valueOf(this.tipoPersonal.getGrupoNomina().getIdGrupoNomina());
      }
      if (this.tipoPersonal.getGrupoOrganismo() != null) {
        this.selectGrupoOrganismo = 
          String.valueOf(this.tipoPersonal.getGrupoOrganismo().getIdGrupoOrganismo());
      }
      if (this.tipoPersonal.getGrupoTicket() != null) {
        this.selectGrupoTicket = 
          String.valueOf(this.tipoPersonal.getGrupoTicket().getIdGrupoTicket());
      }
      if (this.tipoPersonal.getTurno() != null) {
        this.selectTurno = 
          String.valueOf(this.tipoPersonal.getTurno().getIdTurno());
      }
      if (this.tipoPersonal.getBancoNomina() != null) {
        this.selectBancoNomina = 
          String.valueOf(this.tipoPersonal.getBancoNomina().getIdBanco());
      }
      if (this.tipoPersonal.getBancoLph() != null) {
        this.selectBancoLph = 
          String.valueOf(this.tipoPersonal.getBancoLph().getIdBanco());
      }
      if (this.tipoPersonal.getBancoFid() != null) {
        this.selectBancoFid = 
          String.valueOf(this.tipoPersonal.getBancoFid().getIdBanco());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.tipoPersonal = null;
    this.showTipoPersonalByCodTipoPersonal = false;
    this.showTipoPersonalByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error) {
      return null;
    }

    if (this.tipoPersonal.getGrupoOrganismo() == null) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar el grupo de organismo ", ""));
      error = true;
    }
    try
    {
      if (this.adding) {
        this.definicionesFacade.addTipoPersonal(
          this.tipoPersonal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateTipoPersonal(
          this.tipoPersonal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteTipoPersonal(
        this.tipoPersonal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.tipoPersonal = new TipoPersonal();

    this.selectClasificacionPersonal = null;

    this.selectGrupoNomina = null;

    this.selectGrupoOrganismo = null;

    this.selectGrupoTicket = null;

    this.selectTurno = null;

    this.selectBancoNomina = null;

    this.selectBancoLph = null;

    this.selectBancoFid = null;

    this.tipoPersonal.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.tipoPersonal.setIdTipoPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.TipoPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.tipoPersonal = new TipoPersonal();
    return "cancel";
  }

  public boolean isShowGrupoTicketAux()
  {
    try
    {
      return this.tipoPersonal.getBeneficioCestaTicket().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowFormulaIntegralAux()
  {
    try {
      return this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowFormulaSemanalAux()
  {
    try {
      return this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("TipoPersonal");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "TipoPersonal" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}