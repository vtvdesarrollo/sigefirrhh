package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class EscalaCuadroOnapreBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEscalaCuadroOnapre(EscalaCuadroOnapre escalaCuadroOnapre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    EscalaCuadroOnapre escalaCuadroOnapreNew = 
      (EscalaCuadroOnapre)BeanUtils.cloneBean(
      escalaCuadroOnapre);

    pm.makePersistent(escalaCuadroOnapreNew);
  }

  public void updateEscalaCuadroOnapre(EscalaCuadroOnapre escalaCuadroOnapre) throws Exception
  {
    EscalaCuadroOnapre escalaCuadroOnapreModify = 
      findEscalaCuadroOnapreById(escalaCuadroOnapre.getIdEscalaCuadroOnapre());

    BeanUtils.copyProperties(escalaCuadroOnapreModify, escalaCuadroOnapre);
  }

  public void deleteEscalaCuadroOnapre(EscalaCuadroOnapre escalaCuadroOnapre) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    EscalaCuadroOnapre escalaCuadroOnapreDelete = 
      findEscalaCuadroOnapreById(escalaCuadroOnapre.getIdEscalaCuadroOnapre());
    pm.deletePersistent(escalaCuadroOnapreDelete);
  }

  public EscalaCuadroOnapre findEscalaCuadroOnapreById(long idEscalaCuadroOnapre) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEscalaCuadroOnapre == pIdEscalaCuadroOnapre";
    Query query = pm.newQuery(EscalaCuadroOnapre.class, filter);

    query.declareParameters("long pIdEscalaCuadroOnapre");

    parameters.put("pIdEscalaCuadroOnapre", new Long(idEscalaCuadroOnapre));

    Collection colEscalaCuadroOnapre = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEscalaCuadroOnapre.iterator();
    return (EscalaCuadroOnapre)iterator.next();
  }

  public Collection findEscalaCuadroOnapreAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent escalaCuadroOnapreExtent = pm.getExtent(
      EscalaCuadroOnapre.class, true);
    Query query = pm.newQuery(escalaCuadroOnapreExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodigo(String codigo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codigo == pCodigo";

    Query query = pm.newQuery(EscalaCuadroOnapre.class, filter);

    query.declareParameters("java.lang.String pCodigo");
    HashMap parameters = new HashMap();

    parameters.put("pCodigo", codigo);

    Collection colCodigoEscala = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCodigoEscala);

    return colCodigoEscala;
  }
}