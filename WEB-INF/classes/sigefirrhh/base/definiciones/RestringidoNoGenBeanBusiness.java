package sigefirrhh.base.definiciones;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;

public class RestringidoNoGenBeanBusiness extends RestringidoBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(RestringidoNoGenBeanBusiness.class.getName());

  public boolean estanRestringidos(long idTipoPersonal, long idTipoPersonal2)
    throws Exception
  {
    Connection connection = null;
    ResultSet rsRestringido = null;
    PreparedStatement stRestringido = null;
    try {
      connection = Resource.getConnection();
      StringBuffer sql = new StringBuffer();
      sql.append("select count(*) as cantidad from restringido ");
      sql.append(" where id_tipo_personal = ?");
      sql.append("  and id_personal_restringido= ? ");

      stRestringido = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRestringido.setLong(1, idTipoPersonal);
      stRestringido.setLong(2, idTipoPersonal2);
      rsRestringido = stRestringido.executeQuery();
      rsRestringido.next();
      int cantidad = rsRestringido.getInt("cantidad");
      this.log.error("rsRestringido " + rsRestringido.getInt("cantidad"));
      return cantidad != 0;
    } finally {
      if (rsRestringido != null) try {
          rsRestringido.close();
        } catch (Exception localException3) {
        } if (stRestringido != null) try {
          stRestringido.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}