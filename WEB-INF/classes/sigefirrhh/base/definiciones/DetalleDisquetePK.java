package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class DetalleDisquetePK
  implements Serializable
{
  public long idDetalleDisquete;

  public DetalleDisquetePK()
  {
  }

  public DetalleDisquetePK(long idDetalleDisquete)
  {
    this.idDetalleDisquete = idDetalleDisquete;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DetalleDisquetePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DetalleDisquetePK thatPK)
  {
    return 
      this.idDetalleDisquete == thatPK.idDetalleDisquete;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDetalleDisquete)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDetalleDisquete);
  }
}