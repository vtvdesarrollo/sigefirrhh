package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ContratoColectivoPK
  implements Serializable
{
  public long idContratoColectivo;

  public ContratoColectivoPK()
  {
  }

  public ContratoColectivoPK(long idContratoColectivo)
  {
    this.idContratoColectivo = idContratoColectivo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ContratoColectivoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ContratoColectivoPK thatPK)
  {
    return 
      this.idContratoColectivo == thatPK.idContratoColectivo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idContratoColectivo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idContratoColectivo);
  }
}