package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class UtilidadesPorAnioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addUtilidadesPorAnio(UtilidadesPorAnio utilidadesPorAnio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    UtilidadesPorAnio utilidadesPorAnioNew = 
      (UtilidadesPorAnio)BeanUtils.cloneBean(
      utilidadesPorAnio);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (utilidadesPorAnioNew.getTipoPersonal() != null) {
      utilidadesPorAnioNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        utilidadesPorAnioNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(utilidadesPorAnioNew);
  }

  public void updateUtilidadesPorAnio(UtilidadesPorAnio utilidadesPorAnio) throws Exception
  {
    UtilidadesPorAnio utilidadesPorAnioModify = 
      findUtilidadesPorAnioById(utilidadesPorAnio.getIdUtilidadesPorAnio());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (utilidadesPorAnio.getTipoPersonal() != null) {
      utilidadesPorAnio.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        utilidadesPorAnio.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(utilidadesPorAnioModify, utilidadesPorAnio);
  }

  public void deleteUtilidadesPorAnio(UtilidadesPorAnio utilidadesPorAnio) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    UtilidadesPorAnio utilidadesPorAnioDelete = 
      findUtilidadesPorAnioById(utilidadesPorAnio.getIdUtilidadesPorAnio());
    pm.deletePersistent(utilidadesPorAnioDelete);
  }

  public UtilidadesPorAnio findUtilidadesPorAnioById(long idUtilidadesPorAnio) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idUtilidadesPorAnio == pIdUtilidadesPorAnio";
    Query query = pm.newQuery(UtilidadesPorAnio.class, filter);

    query.declareParameters("long pIdUtilidadesPorAnio");

    parameters.put("pIdUtilidadesPorAnio", new Long(idUtilidadesPorAnio));

    Collection colUtilidadesPorAnio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colUtilidadesPorAnio.iterator();
    return (UtilidadesPorAnio)iterator.next();
  }

  public Collection findUtilidadesPorAnioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent utilidadesPorAnioExtent = pm.getExtent(
      UtilidadesPorAnio.class, true);
    Query query = pm.newQuery(utilidadesPorAnioExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(UtilidadesPorAnio.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colUtilidadesPorAnio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUtilidadesPorAnio);

    return colUtilidadesPorAnio;
  }
}