package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ConceptoUtilidadesBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoUtilidades(ConceptoUtilidades conceptoUtilidades)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoUtilidades conceptoUtilidadesNew = 
      (ConceptoUtilidades)BeanUtils.cloneBean(
      conceptoUtilidades);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (conceptoUtilidadesNew.getTipoPersonal() != null) {
      conceptoUtilidadesNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        conceptoUtilidadesNew.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoUtilidadesNew.getConceptoTipoPersonal() != null) {
      conceptoUtilidadesNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoUtilidadesNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }
    pm.makePersistent(conceptoUtilidadesNew);
  }

  public void updateConceptoUtilidades(ConceptoUtilidades conceptoUtilidades) throws Exception
  {
    ConceptoUtilidades conceptoUtilidadesModify = 
      findConceptoUtilidadesById(conceptoUtilidades.getIdConceptoUtilidades());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (conceptoUtilidades.getTipoPersonal() != null) {
      conceptoUtilidades.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        conceptoUtilidades.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoUtilidades.getConceptoTipoPersonal() != null) {
      conceptoUtilidades.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoUtilidades.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    BeanUtils.copyProperties(conceptoUtilidadesModify, conceptoUtilidades);
  }

  public void deleteConceptoUtilidades(ConceptoUtilidades conceptoUtilidades) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoUtilidades conceptoUtilidadesDelete = 
      findConceptoUtilidadesById(conceptoUtilidades.getIdConceptoUtilidades());
    pm.deletePersistent(conceptoUtilidadesDelete);
  }

  public ConceptoUtilidades findConceptoUtilidadesById(long idConceptoUtilidades) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoUtilidades == pIdConceptoUtilidades";
    Query query = pm.newQuery(ConceptoUtilidades.class, filter);

    query.declareParameters("long pIdConceptoUtilidades");

    parameters.put("pIdConceptoUtilidades", new Long(idConceptoUtilidades));

    Collection colConceptoUtilidades = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoUtilidades.iterator();
    return (ConceptoUtilidades)iterator.next();
  }

  public Collection findConceptoUtilidadesAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoUtilidadesExtent = pm.getExtent(
      ConceptoUtilidades.class, true);
    Query query = pm.newQuery(conceptoUtilidadesExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ConceptoUtilidades.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colConceptoUtilidades = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoUtilidades);

    return colConceptoUtilidades;
  }
}