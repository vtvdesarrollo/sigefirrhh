package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class ParametroReportesBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParametroReportes(ParametroReportes parametroReportes)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ParametroReportes parametroReportesNew = 
      (ParametroReportes)BeanUtils.cloneBean(
      parametroReportes);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (parametroReportesNew.getOrganismo() != null) {
      parametroReportesNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        parametroReportesNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(parametroReportesNew);
  }

  public void updateParametroReportes(ParametroReportes parametroReportes) throws Exception
  {
    ParametroReportes parametroReportesModify = 
      findParametroReportesById(parametroReportes.getIdParametroReportes());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (parametroReportes.getOrganismo() != null) {
      parametroReportes.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        parametroReportes.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(parametroReportesModify, parametroReportes);
  }

  public void deleteParametroReportes(ParametroReportes parametroReportes) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ParametroReportes parametroReportesDelete = 
      findParametroReportesById(parametroReportes.getIdParametroReportes());
    pm.deletePersistent(parametroReportesDelete);
  }

  public ParametroReportes findParametroReportesById(long idParametroReportes) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParametroReportes == pIdParametroReportes";
    Query query = pm.newQuery(ParametroReportes.class, filter);

    query.declareParameters("long pIdParametroReportes");

    parameters.put("pIdParametroReportes", new Long(idParametroReportes));

    Collection colParametroReportes = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParametroReportes.iterator();
    return (ParametroReportes)iterator.next();
  }

  public Collection findParametroReportesAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parametroReportesExtent = pm.getExtent(
      ParametroReportes.class, true);
    Query query = pm.newQuery(parametroReportesExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}