package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.bienestar.ticketAlimentacion.GrupoTicket;

public class TipoPersonal
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_PERIODICIDAD;
  protected static final Map LISTA_INTEGRAL;
  protected static final Map LISTA_SEMANAL;
  protected static final Map LISTA_FORMA_PAGO;
  private long idTipoPersonal;
  private String codTipoPersonal;
  private String nombre;
  private ClasificacionPersonal clasificacionPersonal;
  private GrupoNomina grupoNomina;
  private GrupoOrganismo grupoOrganismo;
  private String aprobacionMpd;
  private String manejaRac;
  private String calculaPrestaciones;
  private String asignanDotaciones;
  private String aumentoEvaluacion;
  private String beneficioCestaTicket;
  private GrupoTicket grupoTicket;
  private String formulaIntegral;
  private String formulaSemanal;
  private String multipleRegistro;
  private String cotizaSso;
  private String cotizaSpf;
  private String cotizaLph;
  private String cotizaFju;
  private String disfrutaVacaciones;
  private Turno turno;
  private String deudaRegimenDerogado;
  private Banco bancoNomina;
  private String formaPagoNomina;
  private Banco bancoLph;
  private Banco bancoFid;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aprobacionMpd", "asignanDotaciones", "aumentoEvaluacion", "bancoFid", "bancoLph", "bancoNomina", "beneficioCestaTicket", "calculaPrestaciones", "clasificacionPersonal", "codTipoPersonal", "cotizaFju", "cotizaLph", "cotizaSpf", "cotizaSso", "deudaRegimenDerogado", "disfrutaVacaciones", "formaPagoNomina", "formulaIntegral", "formulaSemanal", "grupoNomina", "grupoOrganismo", "grupoTicket", "idTipoPersonal", "manejaRac", "multipleRegistro", "nombre", "organismo", "turno" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.ClasificacionPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), sunjdo$classForName$("sigefirrhh.base.estructura.GrupoOrganismo"), sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.GrupoTicket"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("sigefirrhh.base.definiciones.Turno") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 26, 26, 21, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 26, 26, 24, 21, 21, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoPersonal());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_PERIODICIDAD = 
      new LinkedHashMap();
    LISTA_INTEGRAL = 
      new LinkedHashMap();
    LISTA_SEMANAL = 
      new LinkedHashMap();
    LISTA_FORMA_PAGO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_INTEGRAL.put("0", "No Aplica");
    LISTA_INTEGRAL.put("1", "(Semanales/7 + Mensuales/30) *365/12)");
    LISTA_INTEGRAL.put("2", "Semanales / 7 * 30 + Mensuales");
    LISTA_INTEGRAL.put("3", "Semanales * 52 / 12 + Mensuales");
    LISTA_INTEGRAL.put("4", "((Semanales / 7) + (Mensuales*12/52)/7)*30");
    LISTA_SEMANAL.put("0", "No Aplica");
    LISTA_SEMANAL.put("1", "Semanales + (mensuales / 30 *7)");
    LISTA_SEMANAL.put("2", "Semanales + mensuales (de esa semana)");
    LISTA_SEMANAL.put("3", "Semanales + (mensuales *12/52)");
    LISTA_SEMANAL.put("4", "(Semanales/7*30+mensuales)*12/52");
    LISTA_FORMA_PAGO.put("1", "DEPOSITO");
    LISTA_FORMA_PAGO.put("2", "CHEQUE");
    LISTA_FORMA_PAGO.put("3", "EFECTIVO");
    LISTA_FORMA_PAGO.put("T", "TRANSFERENCIA");
  }

  public TipoPersonal()
  {
    jdoSetaprobacionMpd(this, "N");

    jdoSetmanejaRac(this, "N");

    jdoSetcalculaPrestaciones(this, "S");

    jdoSetasignanDotaciones(this, "S");

    jdoSetaumentoEvaluacion(this, "S");

    jdoSetbeneficioCestaTicket(this, "S");

    jdoSetformulaIntegral(this, "0");

    jdoSetformulaSemanal(this, "0");

    jdoSetmultipleRegistro(this, "N");

    jdoSetcotizaSso(this, "S");

    jdoSetcotizaSpf(this, "S");

    jdoSetcotizaLph(this, "S");

    jdoSetcotizaFju(this, "S");

    jdoSetdisfrutaVacaciones(this, "S");

    jdoSetdeudaRegimenDerogado(this, "N");

    jdoSetformaPagoNomina(this, "2");
  }

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodTipoPersonal(this);
  }

  public String getAprobacionMpd()
  {
    return jdoGetaprobacionMpd(this);
  }

  public String getCalculaPrestaciones()
  {
    return jdoGetcalculaPrestaciones(this);
  }

  public ClasificacionPersonal getClasificacionPersonal()
  {
    return jdoGetclasificacionPersonal(this);
  }

  public String getCodTipoPersonal()
  {
    return jdoGetcodTipoPersonal(this);
  }

  public String getFormulaIntegral()
  {
    return jdoGetformulaIntegral(this);
  }

  public GrupoNomina getGrupoNomina()
  {
    return jdoGetgrupoNomina(this);
  }

  public GrupoOrganismo getGrupoOrganismo()
  {
    return jdoGetgrupoOrganismo(this);
  }

  public long getIdTipoPersonal()
  {
    return jdoGetidTipoPersonal(this);
  }

  public String getManejaRac()
  {
    return jdoGetmanejaRac(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setAprobacionMpd(String string)
  {
    jdoSetaprobacionMpd(this, string);
  }

  public void setCalculaPrestaciones(String string)
  {
    jdoSetcalculaPrestaciones(this, string);
  }

  public void setClasificacionPersonal(ClasificacionPersonal personal)
  {
    jdoSetclasificacionPersonal(this, personal);
  }

  public void setCodTipoPersonal(String string)
  {
    jdoSetcodTipoPersonal(this, string);
  }

  public void setFormulaIntegral(String string)
  {
    jdoSetformulaIntegral(this, string);
  }

  public void setGrupoNomina(GrupoNomina nomina)
  {
    jdoSetgrupoNomina(this, nomina);
  }

  public void setGrupoOrganismo(GrupoOrganismo organismo)
  {
    jdoSetgrupoOrganismo(this, organismo);
  }

  public void setIdTipoPersonal(long l)
  {
    jdoSetidTipoPersonal(this, l);
  }

  public void setManejaRac(String string)
  {
    jdoSetmanejaRac(this, string);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public String getFormulaSemanal()
  {
    return jdoGetformulaSemanal(this);
  }

  public String getMultipleRegistro()
  {
    return jdoGetmultipleRegistro(this);
  }

  public void setFormulaSemanal(String string)
  {
    jdoSetformulaSemanal(this, string);
  }

  public void setMultipleRegistro(String string)
  {
    jdoSetmultipleRegistro(this, string);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }
  public String getCotizaFju() {
    return jdoGetcotizaFju(this);
  }
  public void setCotizaFju(String cotizaFju) {
    jdoSetcotizaFju(this, cotizaFju);
  }
  public String getCotizaLph() {
    return jdoGetcotizaLph(this);
  }
  public void setCotizaLph(String cotizaLph) {
    jdoSetcotizaLph(this, cotizaLph);
  }
  public String getCotizaSpf() {
    return jdoGetcotizaSpf(this);
  }
  public void setCotizaSpf(String cotizaSpf) {
    jdoSetcotizaSpf(this, cotizaSpf);
  }
  public String getCotizaSso() {
    return jdoGetcotizaSso(this);
  }
  public void setCotizaSso(String cotizaSso) {
    jdoSetcotizaSso(this, cotizaSso);
  }
  public Turno getTurno() {
    return jdoGetturno(this);
  }
  public void setTurno(Turno turno) {
    jdoSetturno(this, turno);
  }
  public String getDisfrutaVacaciones() {
    return jdoGetdisfrutaVacaciones(this);
  }
  public void setDisfrutaVacaciones(String disfrutaVacaciones) {
    jdoSetdisfrutaVacaciones(this, disfrutaVacaciones);
  }
  public String getAsignanDotaciones() {
    return jdoGetasignanDotaciones(this);
  }
  public void setAsignanDotaciones(String asignanDotaciones) {
    jdoSetasignanDotaciones(this, asignanDotaciones);
  }
  public String getAumentoEvaluacion() {
    return jdoGetaumentoEvaluacion(this);
  }
  public void setAumentoEvaluacion(String aumentoEvaluacion) {
    jdoSetaumentoEvaluacion(this, aumentoEvaluacion);
  }
  public Banco getBancoFid() {
    return jdoGetbancoFid(this);
  }
  public void setBancoFid(Banco bancoFid) {
    jdoSetbancoFid(this, bancoFid);
  }
  public Banco getBancoLph() {
    return jdoGetbancoLph(this);
  }
  public void setBancoLph(Banco bancoLph) {
    jdoSetbancoLph(this, bancoLph);
  }
  public Banco getBancoNomina() {
    return jdoGetbancoNomina(this);
  }
  public void setBancoNomina(Banco bancoNomina) {
    jdoSetbancoNomina(this, bancoNomina);
  }
  public String getBeneficioCestaTicket() {
    return jdoGetbeneficioCestaTicket(this);
  }
  public void setBeneficioCestaTicket(String beneficioCestaTicket) {
    jdoSetbeneficioCestaTicket(this, beneficioCestaTicket);
  }
  public String getDeudaRegimenDerogado() {
    return jdoGetdeudaRegimenDerogado(this);
  }
  public void setDeudaRegimenDerogado(String deudaRegimenDerogado) {
    jdoSetdeudaRegimenDerogado(this, deudaRegimenDerogado);
  }
  public String getFormaPagoNomina() {
    return jdoGetformaPagoNomina(this);
  }
  public void setFormaPagoNomina(String formaPagoNomina) {
    jdoSetformaPagoNomina(this, formaPagoNomina);
  }
  public GrupoTicket getGrupoTicket() {
    return jdoGetgrupoTicket(this);
  }
  public void setGrupoTicket(GrupoTicket grupoTicket) {
    jdoSetgrupoTicket(this, grupoTicket);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 28;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoPersonal localTipoPersonal = new TipoPersonal();
    localTipoPersonal.jdoFlags = 1;
    localTipoPersonal.jdoStateManager = paramStateManager;
    return localTipoPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoPersonal localTipoPersonal = new TipoPersonal();
    localTipoPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoPersonal.jdoFlags = 1;
    localTipoPersonal.jdoStateManager = paramStateManager;
    return localTipoPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.asignanDotaciones);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aumentoEvaluacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.bancoFid);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.bancoLph);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.bancoNomina);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.beneficioCestaTicket);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.calculaPrestaciones);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.clasificacionPersonal);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoPersonal);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cotizaFju);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cotizaLph);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cotizaSpf);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cotizaSso);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.deudaRegimenDerogado);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.disfrutaVacaciones);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.formaPagoNomina);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.formulaIntegral);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.formulaSemanal);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoOrganismo);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoTicket);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoPersonal);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.manejaRac);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.multipleRegistro);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.turno);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asignanDotaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aumentoEvaluacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bancoFid = ((Banco)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bancoLph = ((Banco)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bancoNomina = ((Banco)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.beneficioCestaTicket = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.calculaPrestaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.clasificacionPersonal = ((ClasificacionPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cotizaFju = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cotizaLph = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cotizaSpf = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cotizaSso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.deudaRegimenDerogado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.disfrutaVacaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.formaPagoNomina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.formulaIntegral = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.formulaSemanal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoOrganismo = ((GrupoOrganismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoTicket = ((GrupoTicket)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.manejaRac = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.multipleRegistro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.turno = ((Turno)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoPersonal paramTipoPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramTipoPersonal.aprobacionMpd;
      return;
    case 1:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.asignanDotaciones = paramTipoPersonal.asignanDotaciones;
      return;
    case 2:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.aumentoEvaluacion = paramTipoPersonal.aumentoEvaluacion;
      return;
    case 3:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.bancoFid = paramTipoPersonal.bancoFid;
      return;
    case 4:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.bancoLph = paramTipoPersonal.bancoLph;
      return;
    case 5:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.bancoNomina = paramTipoPersonal.bancoNomina;
      return;
    case 6:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.beneficioCestaTicket = paramTipoPersonal.beneficioCestaTicket;
      return;
    case 7:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.calculaPrestaciones = paramTipoPersonal.calculaPrestaciones;
      return;
    case 8:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.clasificacionPersonal = paramTipoPersonal.clasificacionPersonal;
      return;
    case 9:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoPersonal = paramTipoPersonal.codTipoPersonal;
      return;
    case 10:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.cotizaFju = paramTipoPersonal.cotizaFju;
      return;
    case 11:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.cotizaLph = paramTipoPersonal.cotizaLph;
      return;
    case 12:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.cotizaSpf = paramTipoPersonal.cotizaSpf;
      return;
    case 13:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.cotizaSso = paramTipoPersonal.cotizaSso;
      return;
    case 14:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.deudaRegimenDerogado = paramTipoPersonal.deudaRegimenDerogado;
      return;
    case 15:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.disfrutaVacaciones = paramTipoPersonal.disfrutaVacaciones;
      return;
    case 16:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.formaPagoNomina = paramTipoPersonal.formaPagoNomina;
      return;
    case 17:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.formulaIntegral = paramTipoPersonal.formulaIntegral;
      return;
    case 18:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.formulaSemanal = paramTipoPersonal.formulaSemanal;
      return;
    case 19:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramTipoPersonal.grupoNomina;
      return;
    case 20:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.grupoOrganismo = paramTipoPersonal.grupoOrganismo;
      return;
    case 21:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.grupoTicket = paramTipoPersonal.grupoTicket;
      return;
    case 22:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoPersonal = paramTipoPersonal.idTipoPersonal;
      return;
    case 23:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.manejaRac = paramTipoPersonal.manejaRac;
      return;
    case 24:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.multipleRegistro = paramTipoPersonal.multipleRegistro;
      return;
    case 25:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramTipoPersonal.nombre;
      return;
    case 26:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramTipoPersonal.organismo;
      return;
    case 27:
      if (paramTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.turno = paramTipoPersonal.turno;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoPersonal localTipoPersonal = (TipoPersonal)paramObject;
    if (localTipoPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoPersonalPK))
      throw new IllegalArgumentException("arg1");
    TipoPersonalPK localTipoPersonalPK = (TipoPersonalPK)paramObject;
    localTipoPersonalPK.idTipoPersonal = this.idTipoPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoPersonalPK))
      throw new IllegalArgumentException("arg1");
    TipoPersonalPK localTipoPersonalPK = (TipoPersonalPK)paramObject;
    this.idTipoPersonal = localTipoPersonalPK.idTipoPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoPersonalPK))
      throw new IllegalArgumentException("arg2");
    TipoPersonalPK localTipoPersonalPK = (TipoPersonalPK)paramObject;
    localTipoPersonalPK.idTipoPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 22);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoPersonalPK))
      throw new IllegalArgumentException("arg2");
    TipoPersonalPK localTipoPersonalPK = (TipoPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 22, localTipoPersonalPK.idTipoPersonal);
  }

  private static final String jdoGetaprobacionMpd(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.aprobacionMpd;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.aprobacionMpd;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 0))
      return paramTipoPersonal.aprobacionMpd;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 0, paramTipoPersonal.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 0, paramTipoPersonal.aprobacionMpd, paramString);
  }

  private static final String jdoGetasignanDotaciones(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.asignanDotaciones;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.asignanDotaciones;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 1))
      return paramTipoPersonal.asignanDotaciones;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 1, paramTipoPersonal.asignanDotaciones);
  }

  private static final void jdoSetasignanDotaciones(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.asignanDotaciones = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.asignanDotaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 1, paramTipoPersonal.asignanDotaciones, paramString);
  }

  private static final String jdoGetaumentoEvaluacion(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.aumentoEvaluacion;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.aumentoEvaluacion;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 2))
      return paramTipoPersonal.aumentoEvaluacion;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 2, paramTipoPersonal.aumentoEvaluacion);
  }

  private static final void jdoSetaumentoEvaluacion(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.aumentoEvaluacion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.aumentoEvaluacion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 2, paramTipoPersonal.aumentoEvaluacion, paramString);
  }

  private static final Banco jdoGetbancoFid(TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.bancoFid;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 3))
      return paramTipoPersonal.bancoFid;
    return (Banco)localStateManager.getObjectField(paramTipoPersonal, jdoInheritedFieldCount + 3, paramTipoPersonal.bancoFid);
  }

  private static final void jdoSetbancoFid(TipoPersonal paramTipoPersonal, Banco paramBanco)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.bancoFid = paramBanco;
      return;
    }
    localStateManager.setObjectField(paramTipoPersonal, jdoInheritedFieldCount + 3, paramTipoPersonal.bancoFid, paramBanco);
  }

  private static final Banco jdoGetbancoLph(TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.bancoLph;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 4))
      return paramTipoPersonal.bancoLph;
    return (Banco)localStateManager.getObjectField(paramTipoPersonal, jdoInheritedFieldCount + 4, paramTipoPersonal.bancoLph);
  }

  private static final void jdoSetbancoLph(TipoPersonal paramTipoPersonal, Banco paramBanco)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.bancoLph = paramBanco;
      return;
    }
    localStateManager.setObjectField(paramTipoPersonal, jdoInheritedFieldCount + 4, paramTipoPersonal.bancoLph, paramBanco);
  }

  private static final Banco jdoGetbancoNomina(TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.bancoNomina;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 5))
      return paramTipoPersonal.bancoNomina;
    return (Banco)localStateManager.getObjectField(paramTipoPersonal, jdoInheritedFieldCount + 5, paramTipoPersonal.bancoNomina);
  }

  private static final void jdoSetbancoNomina(TipoPersonal paramTipoPersonal, Banco paramBanco)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.bancoNomina = paramBanco;
      return;
    }
    localStateManager.setObjectField(paramTipoPersonal, jdoInheritedFieldCount + 5, paramTipoPersonal.bancoNomina, paramBanco);
  }

  private static final String jdoGetbeneficioCestaTicket(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.beneficioCestaTicket;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.beneficioCestaTicket;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 6))
      return paramTipoPersonal.beneficioCestaTicket;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 6, paramTipoPersonal.beneficioCestaTicket);
  }

  private static final void jdoSetbeneficioCestaTicket(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.beneficioCestaTicket = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.beneficioCestaTicket = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 6, paramTipoPersonal.beneficioCestaTicket, paramString);
  }

  private static final String jdoGetcalculaPrestaciones(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.calculaPrestaciones;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.calculaPrestaciones;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 7))
      return paramTipoPersonal.calculaPrestaciones;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 7, paramTipoPersonal.calculaPrestaciones);
  }

  private static final void jdoSetcalculaPrestaciones(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.calculaPrestaciones = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.calculaPrestaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 7, paramTipoPersonal.calculaPrestaciones, paramString);
  }

  private static final ClasificacionPersonal jdoGetclasificacionPersonal(TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.clasificacionPersonal;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 8))
      return paramTipoPersonal.clasificacionPersonal;
    return (ClasificacionPersonal)localStateManager.getObjectField(paramTipoPersonal, jdoInheritedFieldCount + 8, paramTipoPersonal.clasificacionPersonal);
  }

  private static final void jdoSetclasificacionPersonal(TipoPersonal paramTipoPersonal, ClasificacionPersonal paramClasificacionPersonal)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.clasificacionPersonal = paramClasificacionPersonal;
      return;
    }
    localStateManager.setObjectField(paramTipoPersonal, jdoInheritedFieldCount + 8, paramTipoPersonal.clasificacionPersonal, paramClasificacionPersonal);
  }

  private static final String jdoGetcodTipoPersonal(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.codTipoPersonal;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.codTipoPersonal;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 9))
      return paramTipoPersonal.codTipoPersonal;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 9, paramTipoPersonal.codTipoPersonal);
  }

  private static final void jdoSetcodTipoPersonal(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.codTipoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.codTipoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 9, paramTipoPersonal.codTipoPersonal, paramString);
  }

  private static final String jdoGetcotizaFju(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.cotizaFju;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.cotizaFju;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 10))
      return paramTipoPersonal.cotizaFju;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 10, paramTipoPersonal.cotizaFju);
  }

  private static final void jdoSetcotizaFju(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.cotizaFju = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.cotizaFju = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 10, paramTipoPersonal.cotizaFju, paramString);
  }

  private static final String jdoGetcotizaLph(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.cotizaLph;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.cotizaLph;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 11))
      return paramTipoPersonal.cotizaLph;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 11, paramTipoPersonal.cotizaLph);
  }

  private static final void jdoSetcotizaLph(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.cotizaLph = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.cotizaLph = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 11, paramTipoPersonal.cotizaLph, paramString);
  }

  private static final String jdoGetcotizaSpf(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.cotizaSpf;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.cotizaSpf;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 12))
      return paramTipoPersonal.cotizaSpf;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 12, paramTipoPersonal.cotizaSpf);
  }

  private static final void jdoSetcotizaSpf(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.cotizaSpf = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.cotizaSpf = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 12, paramTipoPersonal.cotizaSpf, paramString);
  }

  private static final String jdoGetcotizaSso(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.cotizaSso;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.cotizaSso;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 13))
      return paramTipoPersonal.cotizaSso;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 13, paramTipoPersonal.cotizaSso);
  }

  private static final void jdoSetcotizaSso(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.cotizaSso = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.cotizaSso = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 13, paramTipoPersonal.cotizaSso, paramString);
  }

  private static final String jdoGetdeudaRegimenDerogado(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.deudaRegimenDerogado;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.deudaRegimenDerogado;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 14))
      return paramTipoPersonal.deudaRegimenDerogado;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 14, paramTipoPersonal.deudaRegimenDerogado);
  }

  private static final void jdoSetdeudaRegimenDerogado(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.deudaRegimenDerogado = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.deudaRegimenDerogado = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 14, paramTipoPersonal.deudaRegimenDerogado, paramString);
  }

  private static final String jdoGetdisfrutaVacaciones(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.disfrutaVacaciones;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.disfrutaVacaciones;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 15))
      return paramTipoPersonal.disfrutaVacaciones;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 15, paramTipoPersonal.disfrutaVacaciones);
  }

  private static final void jdoSetdisfrutaVacaciones(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.disfrutaVacaciones = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.disfrutaVacaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 15, paramTipoPersonal.disfrutaVacaciones, paramString);
  }

  private static final String jdoGetformaPagoNomina(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.formaPagoNomina;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.formaPagoNomina;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 16))
      return paramTipoPersonal.formaPagoNomina;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 16, paramTipoPersonal.formaPagoNomina);
  }

  private static final void jdoSetformaPagoNomina(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.formaPagoNomina = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.formaPagoNomina = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 16, paramTipoPersonal.formaPagoNomina, paramString);
  }

  private static final String jdoGetformulaIntegral(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.formulaIntegral;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.formulaIntegral;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 17))
      return paramTipoPersonal.formulaIntegral;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 17, paramTipoPersonal.formulaIntegral);
  }

  private static final void jdoSetformulaIntegral(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.formulaIntegral = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.formulaIntegral = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 17, paramTipoPersonal.formulaIntegral, paramString);
  }

  private static final String jdoGetformulaSemanal(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.formulaSemanal;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.formulaSemanal;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 18))
      return paramTipoPersonal.formulaSemanal;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 18, paramTipoPersonal.formulaSemanal);
  }

  private static final void jdoSetformulaSemanal(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.formulaSemanal = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.formulaSemanal = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 18, paramTipoPersonal.formulaSemanal, paramString);
  }

  private static final GrupoNomina jdoGetgrupoNomina(TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.grupoNomina;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 19))
      return paramTipoPersonal.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramTipoPersonal, jdoInheritedFieldCount + 19, paramTipoPersonal.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(TipoPersonal paramTipoPersonal, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramTipoPersonal, jdoInheritedFieldCount + 19, paramTipoPersonal.grupoNomina, paramGrupoNomina);
  }

  private static final GrupoOrganismo jdoGetgrupoOrganismo(TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.grupoOrganismo;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 20))
      return paramTipoPersonal.grupoOrganismo;
    return (GrupoOrganismo)localStateManager.getObjectField(paramTipoPersonal, jdoInheritedFieldCount + 20, paramTipoPersonal.grupoOrganismo);
  }

  private static final void jdoSetgrupoOrganismo(TipoPersonal paramTipoPersonal, GrupoOrganismo paramGrupoOrganismo)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.grupoOrganismo = paramGrupoOrganismo;
      return;
    }
    localStateManager.setObjectField(paramTipoPersonal, jdoInheritedFieldCount + 20, paramTipoPersonal.grupoOrganismo, paramGrupoOrganismo);
  }

  private static final GrupoTicket jdoGetgrupoTicket(TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.grupoTicket;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 21))
      return paramTipoPersonal.grupoTicket;
    return (GrupoTicket)localStateManager.getObjectField(paramTipoPersonal, jdoInheritedFieldCount + 21, paramTipoPersonal.grupoTicket);
  }

  private static final void jdoSetgrupoTicket(TipoPersonal paramTipoPersonal, GrupoTicket paramGrupoTicket)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.grupoTicket = paramGrupoTicket;
      return;
    }
    localStateManager.setObjectField(paramTipoPersonal, jdoInheritedFieldCount + 21, paramTipoPersonal.grupoTicket, paramGrupoTicket);
  }

  private static final long jdoGetidTipoPersonal(TipoPersonal paramTipoPersonal)
  {
    return paramTipoPersonal.idTipoPersonal;
  }

  private static final void jdoSetidTipoPersonal(TipoPersonal paramTipoPersonal, long paramLong)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.idTipoPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoPersonal, jdoInheritedFieldCount + 22, paramTipoPersonal.idTipoPersonal, paramLong);
  }

  private static final String jdoGetmanejaRac(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.manejaRac;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.manejaRac;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 23))
      return paramTipoPersonal.manejaRac;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 23, paramTipoPersonal.manejaRac);
  }

  private static final void jdoSetmanejaRac(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.manejaRac = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.manejaRac = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 23, paramTipoPersonal.manejaRac, paramString);
  }

  private static final String jdoGetmultipleRegistro(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.multipleRegistro;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.multipleRegistro;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 24))
      return paramTipoPersonal.multipleRegistro;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 24, paramTipoPersonal.multipleRegistro);
  }

  private static final void jdoSetmultipleRegistro(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.multipleRegistro = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.multipleRegistro = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 24, paramTipoPersonal.multipleRegistro, paramString);
  }

  private static final String jdoGetnombre(TipoPersonal paramTipoPersonal)
  {
    if (paramTipoPersonal.jdoFlags <= 0)
      return paramTipoPersonal.nombre;
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.nombre;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 25))
      return paramTipoPersonal.nombre;
    return localStateManager.getStringField(paramTipoPersonal, jdoInheritedFieldCount + 25, paramTipoPersonal.nombre);
  }

  private static final void jdoSetnombre(TipoPersonal paramTipoPersonal, String paramString)
  {
    if (paramTipoPersonal.jdoFlags == 0)
    {
      paramTipoPersonal.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoPersonal, jdoInheritedFieldCount + 25, paramTipoPersonal.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.organismo;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 26))
      return paramTipoPersonal.organismo;
    return (Organismo)localStateManager.getObjectField(paramTipoPersonal, jdoInheritedFieldCount + 26, paramTipoPersonal.organismo);
  }

  private static final void jdoSetorganismo(TipoPersonal paramTipoPersonal, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramTipoPersonal, jdoInheritedFieldCount + 26, paramTipoPersonal.organismo, paramOrganismo);
  }

  private static final Turno jdoGetturno(TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramTipoPersonal.turno;
    if (localStateManager.isLoaded(paramTipoPersonal, jdoInheritedFieldCount + 27))
      return paramTipoPersonal.turno;
    return (Turno)localStateManager.getObjectField(paramTipoPersonal, jdoInheritedFieldCount + 27, paramTipoPersonal.turno);
  }

  private static final void jdoSetturno(TipoPersonal paramTipoPersonal, Turno paramTurno)
  {
    StateManager localStateManager = paramTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoPersonal.turno = paramTurno;
      return;
    }
    localStateManager.setObjectField(paramTipoPersonal, jdoInheritedFieldCount + 27, paramTipoPersonal.turno, paramTurno);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}