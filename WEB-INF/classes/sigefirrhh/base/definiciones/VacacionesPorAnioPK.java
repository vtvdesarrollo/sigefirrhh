package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class VacacionesPorAnioPK
  implements Serializable
{
  public long idVacacionesPorAnio;

  public VacacionesPorAnioPK()
  {
  }

  public VacacionesPorAnioPK(long idVacacionesPorAnio)
  {
    this.idVacacionesPorAnio = idVacacionesPorAnio;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((VacacionesPorAnioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(VacacionesPorAnioPK thatPK)
  {
    return 
      this.idVacacionesPorAnio == thatPK.idVacacionesPorAnio;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idVacacionesPorAnio)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idVacacionesPorAnio);
  }
}