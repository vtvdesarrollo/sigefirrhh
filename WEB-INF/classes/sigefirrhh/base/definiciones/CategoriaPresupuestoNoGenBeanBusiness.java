package sigefirrhh.base.definiciones;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

public class CategoriaPresupuestoNoGenBeanBusiness extends CategoriaPresupuestoBeanBusiness
  implements Serializable
{
  public Collection findByCestaTicket(long idUsuario, long idOrganismo, String administrador)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      if (administrador.equals("N")) {
        sql.append("select distinct cat.id_categoria_presupuesto, (cat.desc_categoria || ' ' || cat.cod_categoria)  as descripcion  ");
        sql.append(" from tipopersonal tp, usuariotipopersonal utp, clasificacionpersonal cla, categoriapresupuesto cat");
        sql.append(" where utp.id_usuario = ?");
        sql.append(" and tp.id_organismo = ?");
        sql.append(" and tp.beneficio_cesta_ticket = 'S'");
        sql.append(" and utp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and tp.id_clasificacion_personal = cla.id_clasificacion_personal");
        sql.append(" and cla.id_categoria_presupuesto = cat.id_categoria_presupuesto");
        sql.append(" order by (cat.desc_categoria || ' ' || cat.cod_categoria)");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setLong(2, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      } else {
        sql.append("select distinct cat.id_categoria_presupuesto, (cat.desc_categoria || ' ' || cat.cod_categoria)  as descripcion  ");
        sql.append(" from tipopersonal tp, clasificacionpersonal cla, categoriapresupuesto cat");
        sql.append(" where ");
        sql.append(" tp.id_organismo = ?");
        sql.append(" and tp.beneficio_cesta_ticket = 'S'");
        sql.append(" and tp.id_clasificacion_personal = cla.id_clasificacion_personal");
        sql.append(" and cla.id_categoria_presupuesto = cat.id_categoria_presupuesto");
        sql.append(" order by (cat.desc_categoria || ' ' || cat.cod_categoria)");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      }

      while (rsRegistros.next()) {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_categoria_presupuesto"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}