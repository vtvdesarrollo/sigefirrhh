package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ParametroGobiernoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ParametroGobiernoForm.class.getName());
  private ParametroGobierno parametroGobierno;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showParametroGobiernoByGrupoOrganismo;
  private String findSelectGrupoOrganismo;
  private Collection findColGrupoOrganismo;
  private Collection colGrupoOrganismo;
  private String selectGrupoOrganismo;
  private Object stateResultParametroGobiernoByGrupoOrganismo = null;

  public String getFindSelectGrupoOrganismo()
  {
    return this.findSelectGrupoOrganismo;
  }
  public void setFindSelectGrupoOrganismo(String valGrupoOrganismo) {
    this.findSelectGrupoOrganismo = valGrupoOrganismo;
  }

  public Collection getFindColGrupoOrganismo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColGrupoOrganismo.iterator();
    GrupoOrganismo grupoOrganismo = null;
    while (iterator.hasNext()) {
      grupoOrganismo = (GrupoOrganismo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoOrganismo.getIdGrupoOrganismo()), 
        grupoOrganismo.toString()));
    }
    return col;
  }

  public String getSelectGrupoOrganismo()
  {
    return this.selectGrupoOrganismo;
  }
  public void setSelectGrupoOrganismo(String valGrupoOrganismo) {
    Iterator iterator = this.colGrupoOrganismo.iterator();
    GrupoOrganismo grupoOrganismo = null;
    this.parametroGobierno.setGrupoOrganismo(null);
    while (iterator.hasNext()) {
      grupoOrganismo = (GrupoOrganismo)iterator.next();
      if (String.valueOf(grupoOrganismo.getIdGrupoOrganismo()).equals(
        valGrupoOrganismo)) {
        this.parametroGobierno.setGrupoOrganismo(
          grupoOrganismo);
        break;
      }
    }
    this.selectGrupoOrganismo = valGrupoOrganismo;
  }
  public Collection getResult() {
    return this.result;
  }

  public ParametroGobierno getParametroGobierno() {
    if (this.parametroGobierno == null) {
      this.parametroGobierno = new ParametroGobierno();
    }
    return this.parametroGobierno;
  }

  public ParametroGobiernoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColGrupoOrganismo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoOrganismo.iterator();
    GrupoOrganismo grupoOrganismo = null;
    while (iterator.hasNext()) {
      grupoOrganismo = (GrupoOrganismo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoOrganismo.getIdGrupoOrganismo()), 
        grupoOrganismo.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColGrupoOrganismo = 
        this.estructuraFacade.findGrupoOrganismoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colGrupoOrganismo = 
        this.estructuraFacade.findGrupoOrganismoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findParametroGobiernoByGrupoOrganismo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findParametroGobiernoByGrupoOrganismo(Long.valueOf(this.findSelectGrupoOrganismo).longValue());
      this.showParametroGobiernoByGrupoOrganismo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showParametroGobiernoByGrupoOrganismo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoOrganismo = null;

    return null;
  }

  public boolean isShowParametroGobiernoByGrupoOrganismo() {
    return this.showParametroGobiernoByGrupoOrganismo;
  }

  public String selectParametroGobierno()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectGrupoOrganismo = null;

    long idParametroGobierno = 
      Long.parseLong((String)requestParameterMap.get("idParametroGobierno"));
    try
    {
      this.parametroGobierno = 
        this.definicionesFacade.findParametroGobiernoById(
        idParametroGobierno);
      if (this.parametroGobierno.getGrupoOrganismo() != null) {
        this.selectGrupoOrganismo = 
          String.valueOf(this.parametroGobierno.getGrupoOrganismo().getIdGrupoOrganismo());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.parametroGobierno = null;
    this.showParametroGobiernoByGrupoOrganismo = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addParametroGobierno(
          this.parametroGobierno);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateParametroGobierno(
          this.parametroGobierno);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteParametroGobierno(
        this.parametroGobierno);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.parametroGobierno = new ParametroGobierno();

    this.selectGrupoOrganismo = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.parametroGobierno.setIdParametroGobierno(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.ParametroGobierno"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.parametroGobierno = new ParametroGobierno();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}