package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ConceptoTipoPersonalPK
  implements Serializable
{
  public long idConceptoTipoPersonal;

  public ConceptoTipoPersonalPK()
  {
  }

  public ConceptoTipoPersonalPK(long idConceptoTipoPersonal)
  {
    this.idConceptoTipoPersonal = idConceptoTipoPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoTipoPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoTipoPersonalPK thatPK)
  {
    return 
      this.idConceptoTipoPersonal == thatPK.idConceptoTipoPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoTipoPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoTipoPersonal);
  }
}