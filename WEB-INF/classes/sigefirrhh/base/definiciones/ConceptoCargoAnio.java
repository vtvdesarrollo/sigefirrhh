package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;

public class ConceptoCargoAnio
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idConceptoCargoAnio;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private Cargo cargo;
  private double unidades;
  private double monto;
  private double porcentaje;
  private int anios;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anios", "cargo", "conceptoTipoPersonal", "idConceptoCargoAnio", "idSitp", "monto", "porcentaje", "tiempoSitp", "unidades" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.util.Date"), Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 26, 26, 24, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoCargoAnio"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoCargoAnio());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public ConceptoCargoAnio()
  {
    jdoSetunidades(this, 0.0D);

    jdoSetmonto(this, 0.0D);

    jdoSetporcentaje(this, 0.0D);

    jdoSetanios(this, 0);
  }

  public String toString()
  {
    return jdoGetcargo(this).getDescripcionCargo() + " " + jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion();
  }

  public int getAnios() {
    return jdoGetanios(this);
  }
  public void setAnios(int anios) {
    jdoSetanios(this, anios);
  }
  public Cargo getCargo() {
    return jdoGetcargo(this);
  }
  public void setCargo(Cargo cargo) {
    jdoSetcargo(this, cargo);
  }
  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public long getIdConceptoCargoAnio() {
    return jdoGetidConceptoCargoAnio(this);
  }
  public void setIdConceptoCargoAnio(long idConceptoCargoAnio) {
    jdoSetidConceptoCargoAnio(this, idConceptoCargoAnio);
  }
  public int getIdSitp() {
    return jdoGetidSitp(this);
  }
  public void setIdSitp(int idSitp) {
    jdoSetidSitp(this, idSitp);
  }
  public double getMonto() {
    return jdoGetmonto(this);
  }
  public void setMonto(double monto) {
    jdoSetmonto(this, monto);
  }
  public double getPorcentaje() {
    return jdoGetporcentaje(this);
  }
  public void setPorcentaje(double porcentaje) {
    jdoSetporcentaje(this, porcentaje);
  }
  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }
  public void setTiempoSitp(Date tiempoSitp) {
    jdoSettiempoSitp(this, tiempoSitp);
  }
  public double getUnidades() {
    return jdoGetunidades(this);
  }
  public void setUnidades(double unidades) {
    jdoSetunidades(this, unidades);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoCargoAnio localConceptoCargoAnio = new ConceptoCargoAnio();
    localConceptoCargoAnio.jdoFlags = 1;
    localConceptoCargoAnio.jdoStateManager = paramStateManager;
    return localConceptoCargoAnio;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoCargoAnio localConceptoCargoAnio = new ConceptoCargoAnio();
    localConceptoCargoAnio.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoCargoAnio.jdoFlags = 1;
    localConceptoCargoAnio.jdoStateManager = paramStateManager;
    return localConceptoCargoAnio;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anios);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoCargoAnio);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anios = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoCargoAnio = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoCargoAnio paramConceptoCargoAnio, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoCargoAnio == null)
        throw new IllegalArgumentException("arg1");
      this.anios = paramConceptoCargoAnio.anios;
      return;
    case 1:
      if (paramConceptoCargoAnio == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramConceptoCargoAnio.cargo;
      return;
    case 2:
      if (paramConceptoCargoAnio == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoCargoAnio.conceptoTipoPersonal;
      return;
    case 3:
      if (paramConceptoCargoAnio == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoCargoAnio = paramConceptoCargoAnio.idConceptoCargoAnio;
      return;
    case 4:
      if (paramConceptoCargoAnio == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramConceptoCargoAnio.idSitp;
      return;
    case 5:
      if (paramConceptoCargoAnio == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramConceptoCargoAnio.monto;
      return;
    case 6:
      if (paramConceptoCargoAnio == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramConceptoCargoAnio.porcentaje;
      return;
    case 7:
      if (paramConceptoCargoAnio == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramConceptoCargoAnio.tiempoSitp;
      return;
    case 8:
      if (paramConceptoCargoAnio == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramConceptoCargoAnio.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoCargoAnio))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoCargoAnio localConceptoCargoAnio = (ConceptoCargoAnio)paramObject;
    if (localConceptoCargoAnio.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoCargoAnio, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoCargoAnioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoCargoAnioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoCargoAnioPK))
      throw new IllegalArgumentException("arg1");
    ConceptoCargoAnioPK localConceptoCargoAnioPK = (ConceptoCargoAnioPK)paramObject;
    localConceptoCargoAnioPK.idConceptoCargoAnio = this.idConceptoCargoAnio;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoCargoAnioPK))
      throw new IllegalArgumentException("arg1");
    ConceptoCargoAnioPK localConceptoCargoAnioPK = (ConceptoCargoAnioPK)paramObject;
    this.idConceptoCargoAnio = localConceptoCargoAnioPK.idConceptoCargoAnio;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoCargoAnioPK))
      throw new IllegalArgumentException("arg2");
    ConceptoCargoAnioPK localConceptoCargoAnioPK = (ConceptoCargoAnioPK)paramObject;
    localConceptoCargoAnioPK.idConceptoCargoAnio = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoCargoAnioPK))
      throw new IllegalArgumentException("arg2");
    ConceptoCargoAnioPK localConceptoCargoAnioPK = (ConceptoCargoAnioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localConceptoCargoAnioPK.idConceptoCargoAnio);
  }

  private static final int jdoGetanios(ConceptoCargoAnio paramConceptoCargoAnio)
  {
    if (paramConceptoCargoAnio.jdoFlags <= 0)
      return paramConceptoCargoAnio.anios;
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargoAnio.anios;
    if (localStateManager.isLoaded(paramConceptoCargoAnio, jdoInheritedFieldCount + 0))
      return paramConceptoCargoAnio.anios;
    return localStateManager.getIntField(paramConceptoCargoAnio, jdoInheritedFieldCount + 0, paramConceptoCargoAnio.anios);
  }

  private static final void jdoSetanios(ConceptoCargoAnio paramConceptoCargoAnio, int paramInt)
  {
    if (paramConceptoCargoAnio.jdoFlags == 0)
    {
      paramConceptoCargoAnio.anios = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargoAnio.anios = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoCargoAnio, jdoInheritedFieldCount + 0, paramConceptoCargoAnio.anios, paramInt);
  }

  private static final Cargo jdoGetcargo(ConceptoCargoAnio paramConceptoCargoAnio)
  {
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargoAnio.cargo;
    if (localStateManager.isLoaded(paramConceptoCargoAnio, jdoInheritedFieldCount + 1))
      return paramConceptoCargoAnio.cargo;
    return (Cargo)localStateManager.getObjectField(paramConceptoCargoAnio, jdoInheritedFieldCount + 1, paramConceptoCargoAnio.cargo);
  }

  private static final void jdoSetcargo(ConceptoCargoAnio paramConceptoCargoAnio, Cargo paramCargo)
  {
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargoAnio.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramConceptoCargoAnio, jdoInheritedFieldCount + 1, paramConceptoCargoAnio.cargo, paramCargo);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoCargoAnio paramConceptoCargoAnio)
  {
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargoAnio.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoCargoAnio, jdoInheritedFieldCount + 2))
      return paramConceptoCargoAnio.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoCargoAnio, jdoInheritedFieldCount + 2, paramConceptoCargoAnio.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoCargoAnio paramConceptoCargoAnio, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargoAnio.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoCargoAnio, jdoInheritedFieldCount + 2, paramConceptoCargoAnio.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final long jdoGetidConceptoCargoAnio(ConceptoCargoAnio paramConceptoCargoAnio)
  {
    return paramConceptoCargoAnio.idConceptoCargoAnio;
  }

  private static final void jdoSetidConceptoCargoAnio(ConceptoCargoAnio paramConceptoCargoAnio, long paramLong)
  {
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargoAnio.idConceptoCargoAnio = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoCargoAnio, jdoInheritedFieldCount + 3, paramConceptoCargoAnio.idConceptoCargoAnio, paramLong);
  }

  private static final int jdoGetidSitp(ConceptoCargoAnio paramConceptoCargoAnio)
  {
    if (paramConceptoCargoAnio.jdoFlags <= 0)
      return paramConceptoCargoAnio.idSitp;
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargoAnio.idSitp;
    if (localStateManager.isLoaded(paramConceptoCargoAnio, jdoInheritedFieldCount + 4))
      return paramConceptoCargoAnio.idSitp;
    return localStateManager.getIntField(paramConceptoCargoAnio, jdoInheritedFieldCount + 4, paramConceptoCargoAnio.idSitp);
  }

  private static final void jdoSetidSitp(ConceptoCargoAnio paramConceptoCargoAnio, int paramInt)
  {
    if (paramConceptoCargoAnio.jdoFlags == 0)
    {
      paramConceptoCargoAnio.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargoAnio.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoCargoAnio, jdoInheritedFieldCount + 4, paramConceptoCargoAnio.idSitp, paramInt);
  }

  private static final double jdoGetmonto(ConceptoCargoAnio paramConceptoCargoAnio)
  {
    if (paramConceptoCargoAnio.jdoFlags <= 0)
      return paramConceptoCargoAnio.monto;
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargoAnio.monto;
    if (localStateManager.isLoaded(paramConceptoCargoAnio, jdoInheritedFieldCount + 5))
      return paramConceptoCargoAnio.monto;
    return localStateManager.getDoubleField(paramConceptoCargoAnio, jdoInheritedFieldCount + 5, paramConceptoCargoAnio.monto);
  }

  private static final void jdoSetmonto(ConceptoCargoAnio paramConceptoCargoAnio, double paramDouble)
  {
    if (paramConceptoCargoAnio.jdoFlags == 0)
    {
      paramConceptoCargoAnio.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargoAnio.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoCargoAnio, jdoInheritedFieldCount + 5, paramConceptoCargoAnio.monto, paramDouble);
  }

  private static final double jdoGetporcentaje(ConceptoCargoAnio paramConceptoCargoAnio)
  {
    if (paramConceptoCargoAnio.jdoFlags <= 0)
      return paramConceptoCargoAnio.porcentaje;
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargoAnio.porcentaje;
    if (localStateManager.isLoaded(paramConceptoCargoAnio, jdoInheritedFieldCount + 6))
      return paramConceptoCargoAnio.porcentaje;
    return localStateManager.getDoubleField(paramConceptoCargoAnio, jdoInheritedFieldCount + 6, paramConceptoCargoAnio.porcentaje);
  }

  private static final void jdoSetporcentaje(ConceptoCargoAnio paramConceptoCargoAnio, double paramDouble)
  {
    if (paramConceptoCargoAnio.jdoFlags == 0)
    {
      paramConceptoCargoAnio.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargoAnio.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoCargoAnio, jdoInheritedFieldCount + 6, paramConceptoCargoAnio.porcentaje, paramDouble);
  }

  private static final Date jdoGettiempoSitp(ConceptoCargoAnio paramConceptoCargoAnio)
  {
    if (paramConceptoCargoAnio.jdoFlags <= 0)
      return paramConceptoCargoAnio.tiempoSitp;
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargoAnio.tiempoSitp;
    if (localStateManager.isLoaded(paramConceptoCargoAnio, jdoInheritedFieldCount + 7))
      return paramConceptoCargoAnio.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramConceptoCargoAnio, jdoInheritedFieldCount + 7, paramConceptoCargoAnio.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ConceptoCargoAnio paramConceptoCargoAnio, Date paramDate)
  {
    if (paramConceptoCargoAnio.jdoFlags == 0)
    {
      paramConceptoCargoAnio.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargoAnio.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoCargoAnio, jdoInheritedFieldCount + 7, paramConceptoCargoAnio.tiempoSitp, paramDate);
  }

  private static final double jdoGetunidades(ConceptoCargoAnio paramConceptoCargoAnio)
  {
    if (paramConceptoCargoAnio.jdoFlags <= 0)
      return paramConceptoCargoAnio.unidades;
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargoAnio.unidades;
    if (localStateManager.isLoaded(paramConceptoCargoAnio, jdoInheritedFieldCount + 8))
      return paramConceptoCargoAnio.unidades;
    return localStateManager.getDoubleField(paramConceptoCargoAnio, jdoInheritedFieldCount + 8, paramConceptoCargoAnio.unidades);
  }

  private static final void jdoSetunidades(ConceptoCargoAnio paramConceptoCargoAnio, double paramDouble)
  {
    if (paramConceptoCargoAnio.jdoFlags == 0)
    {
      paramConceptoCargoAnio.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoCargoAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargoAnio.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoCargoAnio, jdoInheritedFieldCount + 8, paramConceptoCargoAnio.unidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}