package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class GrupoNominaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addGrupoNomina(GrupoNomina grupoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    GrupoNomina grupoNominaNew = 
      (GrupoNomina)BeanUtils.cloneBean(
      grupoNomina);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (grupoNominaNew.getOrganismo() != null) {
      grupoNominaNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        grupoNominaNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(grupoNominaNew);
  }

  public void updateGrupoNomina(GrupoNomina grupoNomina) throws Exception
  {
    GrupoNomina grupoNominaModify = 
      findGrupoNominaById(grupoNomina.getIdGrupoNomina());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (grupoNomina.getOrganismo() != null) {
      grupoNomina.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        grupoNomina.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(grupoNominaModify, grupoNomina);
  }

  public void deleteGrupoNomina(GrupoNomina grupoNomina) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    GrupoNomina grupoNominaDelete = 
      findGrupoNominaById(grupoNomina.getIdGrupoNomina());
    pm.deletePersistent(grupoNominaDelete);
  }

  public GrupoNomina findGrupoNominaById(long idGrupoNomina) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idGrupoNomina == pIdGrupoNomina";
    Query query = pm.newQuery(GrupoNomina.class, filter);

    query.declareParameters("long pIdGrupoNomina");

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));

    Collection colGrupoNomina = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colGrupoNomina.iterator();
    return (GrupoNomina)iterator.next();
  }

  public Collection findGrupoNominaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent grupoNominaExtent = pm.getExtent(
      GrupoNomina.class, true);
    Query query = pm.newQuery(grupoNominaExtent);
    query.setOrdering("codGrupoNomina ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodGrupoNomina(int codGrupoNomina, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codGrupoNomina == pCodGrupoNomina &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(GrupoNomina.class, filter);

    query.declareParameters("int pCodGrupoNomina, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodGrupoNomina", new Integer(codGrupoNomina));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codGrupoNomina ascending");

    Collection colGrupoNomina = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoNomina);

    return colGrupoNomina;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(GrupoNomina.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codGrupoNomina ascending");

    Collection colGrupoNomina = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoNomina);

    return colGrupoNomina;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(GrupoNomina.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codGrupoNomina ascending");

    Collection colGrupoNomina = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoNomina);

    return colGrupoNomina;
  }

  public Collection findByOrganismoAndPeriodicidad(long idOrganismo, String periodicidad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo && periodicidad == pPeriodicidad";

    Query query = pm.newQuery(GrupoNomina.class, filter);

    query.declareParameters("long pIdOrganismo, String pPeriodicidad");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pPeriodicidad", new String(periodicidad));

    query.setOrdering("codGrupoNomina ascending");

    Collection colGrupoNomina = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoNomina);

    return colGrupoNomina;
  }
}