package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class FrecuenciaPagoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(FrecuenciaPagoForm.class.getName());
  private FrecuenciaPago frecuenciaPago;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showFrecuenciaPagoByCodFrecuenciaPago;
  private boolean showFrecuenciaPagoByNombre;
  private int findCodFrecuenciaPago;
  private String findNombre;
  private Object stateResultFrecuenciaPagoByCodFrecuenciaPago = null;

  private Object stateResultFrecuenciaPagoByNombre = null;

  public int getFindCodFrecuenciaPago()
  {
    return this.findCodFrecuenciaPago;
  }
  public void setFindCodFrecuenciaPago(int findCodFrecuenciaPago) {
    this.findCodFrecuenciaPago = findCodFrecuenciaPago;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public FrecuenciaPago getFrecuenciaPago() {
    if (this.frecuenciaPago == null) {
      this.frecuenciaPago = new FrecuenciaPago();
    }
    return this.frecuenciaPago;
  }

  public FrecuenciaPagoForm() throws Exception
  {
    newReportId();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListReservado()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = FrecuenciaPago.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findFrecuenciaPagoByCodFrecuenciaPago()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findFrecuenciaPagoByCodFrecuenciaPago(this.findCodFrecuenciaPago, idOrganismo);
      this.showFrecuenciaPagoByCodFrecuenciaPago = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showFrecuenciaPagoByCodFrecuenciaPago)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodFrecuenciaPago = 0;
    this.findNombre = null;

    return null;
  }

  public String findFrecuenciaPagoByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findFrecuenciaPagoByNombre(this.findNombre, idOrganismo);
      this.showFrecuenciaPagoByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showFrecuenciaPagoByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodFrecuenciaPago = 0;
    this.findNombre = null;

    return null;
  }

  public boolean isShowFrecuenciaPagoByCodFrecuenciaPago() {
    return this.showFrecuenciaPagoByCodFrecuenciaPago;
  }
  public boolean isShowFrecuenciaPagoByNombre() {
    return this.showFrecuenciaPagoByNombre;
  }

  public String selectFrecuenciaPago()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idFrecuenciaPago = 
      Long.parseLong((String)requestParameterMap.get("idFrecuenciaPago"));
    try
    {
      this.frecuenciaPago = 
        this.definicionesFacade.findFrecuenciaPagoById(
        idFrecuenciaPago);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.frecuenciaPago = null;
    this.showFrecuenciaPagoByCodFrecuenciaPago = false;
    this.showFrecuenciaPagoByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addFrecuenciaPago(
          this.frecuenciaPago);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateFrecuenciaPago(
          this.frecuenciaPago);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteFrecuenciaPago(
        this.frecuenciaPago);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.frecuenciaPago = new FrecuenciaPago();

    this.frecuenciaPago.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.frecuenciaPago.setIdFrecuenciaPago(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.FrecuenciaPago"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.frecuenciaPago = new FrecuenciaPago();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("FrecuenciaPago");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "FrecuenciaPago" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}