package sigefirrhh.base.definiciones;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class TarifaAriNoGenBeanBusiness extends TarifaAriBeanBusiness
  implements Serializable
{
  public TarifaAri findByTarifaAproximada(double tarifa)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tarifa > pTarifa";

    Query query = pm.newQuery(TarifaAri.class, filter);

    query.declareParameters("double pTarifa");
    HashMap parameters = new HashMap();

    parameters.put("pTarifa", new Double(tarifa));

    query.setOrdering("tarifa ascending");

    Collection colTarifaAri = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTarifaAri);

    return (TarifaAri)colTarifaAri.iterator().next();
  }
}