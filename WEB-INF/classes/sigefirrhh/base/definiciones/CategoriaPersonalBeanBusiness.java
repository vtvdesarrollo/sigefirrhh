package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CategoriaPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCategoriaPersonal(CategoriaPersonal categoriaPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CategoriaPersonal categoriaPersonalNew = 
      (CategoriaPersonal)BeanUtils.cloneBean(
      categoriaPersonal);

    pm.makePersistent(categoriaPersonalNew);
  }

  public void updateCategoriaPersonal(CategoriaPersonal categoriaPersonal) throws Exception
  {
    CategoriaPersonal categoriaPersonalModify = 
      findCategoriaPersonalById(categoriaPersonal.getIdCategoriaPersonal());

    BeanUtils.copyProperties(categoriaPersonalModify, categoriaPersonal);
  }

  public void deleteCategoriaPersonal(CategoriaPersonal categoriaPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CategoriaPersonal categoriaPersonalDelete = 
      findCategoriaPersonalById(categoriaPersonal.getIdCategoriaPersonal());
    pm.deletePersistent(categoriaPersonalDelete);
  }

  public CategoriaPersonal findCategoriaPersonalById(long idCategoriaPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCategoriaPersonal == pIdCategoriaPersonal";
    Query query = pm.newQuery(CategoriaPersonal.class, filter);

    query.declareParameters("long pIdCategoriaPersonal");

    parameters.put("pIdCategoriaPersonal", new Long(idCategoriaPersonal));

    Collection colCategoriaPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCategoriaPersonal.iterator();
    return (CategoriaPersonal)iterator.next();
  }

  public Collection findCategoriaPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent categoriaPersonalExtent = pm.getExtent(
      CategoriaPersonal.class, true);
    Query query = pm.newQuery(categoriaPersonalExtent);
    query.setOrdering("codCategoria ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodCategoria(String codCategoria)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCategoria == pCodCategoria";

    Query query = pm.newQuery(CategoriaPersonal.class, filter);

    query.declareParameters("java.lang.String pCodCategoria");
    HashMap parameters = new HashMap();

    parameters.put("pCodCategoria", new String(codCategoria));

    query.setOrdering("codCategoria ascending");

    Collection colCategoriaPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCategoriaPersonal);

    return colCategoriaPersonal;
  }

  public Collection findByDescCategoria(String descCategoria)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descCategoria.startsWith(pDescCategoria)";

    Query query = pm.newQuery(CategoriaPersonal.class, filter);

    query.declareParameters("java.lang.String pDescCategoria");
    HashMap parameters = new HashMap();

    parameters.put("pDescCategoria", new String(descCategoria));

    query.setOrdering("codCategoria ascending");

    Collection colCategoriaPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCategoriaPersonal);

    return colCategoriaPersonal;
  }
}