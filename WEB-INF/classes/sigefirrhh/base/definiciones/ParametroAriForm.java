package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ParametroAriForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ParametroAriForm.class.getName());
  private ParametroAri parametroAri;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showParametroAriByUnidadTributaria;
  private double findUnidadTributaria;
  private Object stateResultParametroAriByUnidadTributaria = null;

  public double getFindUnidadTributaria()
  {
    return this.findUnidadTributaria;
  }
  public void setFindUnidadTributaria(double findUnidadTributaria) {
    this.findUnidadTributaria = findUnidadTributaria;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public ParametroAri getParametroAri() {
    if (this.parametroAri == null) {
      this.parametroAri = new ParametroAri();
    }
    return this.parametroAri;
  }

  public ParametroAriForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findParametroAri()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findAllParametroAri();
      this.showParametroAriByUnidadTributaria = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showParametroAriByUnidadTributaria)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findUnidadTributaria = 0.0D;

    return null;
  }

  public boolean isShowParametroAriByUnidadTributaria() {
    return this.showParametroAriByUnidadTributaria;
  }

  public String selectParametroAri()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idParametroAri = 
      Long.parseLong((String)requestParameterMap.get("idParametroAri"));
    try
    {
      this.parametroAri = 
        this.definicionesFacade.findParametroAriById(
        idParametroAri);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.parametroAri = null;
    this.showParametroAriByUnidadTributaria = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.parametroAri.getFechaVigencia() != null) && 
      (this.parametroAri.getFechaVigencia().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Vigencia no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addParametroAri(
          this.parametroAri);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.parametroAri);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateParametroAri(
          this.parametroAri);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.parametroAri);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteParametroAri(
        this.parametroAri);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.parametroAri);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.parametroAri = new ParametroAri();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.parametroAri.setIdParametroAri(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.ParametroAri"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.parametroAri = new ParametroAri();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}