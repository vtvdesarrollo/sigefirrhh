package sigefirrhh.base.definiciones;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class FrecuenciaPagoBeanBusinessExtend extends FrecuenciaPagoBeanBusiness
  implements Serializable
{
  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(FrecuenciaTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("frecuenciaPago.nombre ascending");

    Collection colFrecuenciaTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFrecuenciaTipoPersonal);

    Iterator iterator = colFrecuenciaTipoPersonal.iterator();

    Collection colFrecuenciaPago = new ArrayList();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      colFrecuenciaPago.add(frecuenciaTipoPersonal.getFrecuenciaPago());
    }
    return colFrecuenciaPago;
  }

  public Collection findByOrganismoMayorA10(long idOrganismo) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo && codFrecuenciaPago > 10";

    Query query = pm.newQuery(FrecuenciaPago.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colFrecuenciaPago = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFrecuenciaPago);

    return colFrecuenciaPago;
  }
}