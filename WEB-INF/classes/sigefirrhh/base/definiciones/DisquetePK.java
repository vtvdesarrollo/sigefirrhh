package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class DisquetePK
  implements Serializable
{
  public long idDisquete;

  public DisquetePK()
  {
  }

  public DisquetePK(long idDisquete)
  {
    this.idDisquete = idDisquete;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DisquetePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DisquetePK thatPK)
  {
    return 
      this.idDisquete == thatPK.idDisquete;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDisquete)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDisquete);
  }
}