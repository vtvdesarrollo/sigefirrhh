package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.GrupoOrganismo;

public class ParametroVarios
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_PAGO;
  protected static final Map LISTA_ALICUOTA;
  protected static final Map LISTA_CALCULO;
  private long idParametroVarios;
  private TipoPersonal tipoPersonal;
  private GrupoOrganismo grupoOrganismo;
  private String nuevoRegimen;
  private String alicuotaBfaPrestac;
  private String alicuotaBfaBvac;
  private String calculoAlicuotaBfa;
  private String alicuotaBvacPrestac;
  private Date fechaAperturaFideicomiso;
  private String alicuotaBvacBfa;
  private String tipoCalculoBfa;
  private int diasAnio;
  private int calculoUtilidades;
  private Date fechaTopeUtilidades;
  private String aniversarioDisfrute;
  private String bonoExtra;
  private String sumoApn;
  private int diasBfa1997;
  private String regimenDerogadoProcesado;
  private String interesesAdicionales;
  private String ausenciaInjustificada;
  private String alicuotaBonoPetrolero;
  private int constantePetroleroA;
  private int constantePetroleroB;
  private int constantePetroleroC;
  private double cambioMoneda;
  private double topeHorasExtra;
  private double topeHorasExtraMensual;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "alicuotaBfaBvac", "alicuotaBfaPrestac", "alicuotaBonoPetrolero", "alicuotaBvacBfa", "alicuotaBvacPrestac", "aniversarioDisfrute", "ausenciaInjustificada", "bonoExtra", "calculoAlicuotaBfa", "calculoUtilidades", "cambioMoneda", "constantePetroleroA", "constantePetroleroB", "constantePetroleroC", "diasAnio", "diasBfa1997", "fechaAperturaFideicomiso", "fechaTopeUtilidades", "grupoOrganismo", "idParametroVarios", "interesesAdicionales", "nuevoRegimen", "regimenDerogadoProcesado", "sumoApn", "tipoCalculoBfa", "tipoPersonal", "topeHorasExtra", "topeHorasExtraMensual" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Double.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.estructura.GrupoOrganismo"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Double.TYPE, Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 24, 21, 21, 21, 21, 21, 26, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ParametroVarios"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroVarios());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_PAGO = 
      new LinkedHashMap();
    LISTA_ALICUOTA = 
      new LinkedHashMap();
    LISTA_CALCULO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_PAGO.put("A", "MES ANIVERSARIO");
    LISTA_PAGO.put("D", "AL DISFRUTE");
    LISTA_ALICUOTA.put("F", "CONCEPTOS FIJOS");
    LISTA_ALICUOTA.put("D", "DEVENGADO MENSUAL");
    LISTA_CALCULO.put("D", "DIAS");
    LISTA_CALCULO.put("P", "PORCENTAJE");
  }

  public ParametroVarios()
  {
    jdoSetnuevoRegimen(this, "S");

    jdoSetalicuotaBfaPrestac(this, "N");

    jdoSetalicuotaBfaBvac(this, "N");

    jdoSetcalculoAlicuotaBfa(this, "F");

    jdoSetalicuotaBvacPrestac(this, "N");

    jdoSetalicuotaBvacBfa(this, "N");

    jdoSettipoCalculoBfa(this, "D");

    jdoSetdiasAnio(this, 360);

    jdoSetcalculoUtilidades(this, 300);

    jdoSetaniversarioDisfrute(this, "A");

    jdoSetbonoExtra(this, "N");

    jdoSetsumoApn(this, "N");

    jdoSetdiasBfa1997(this, 30);

    jdoSetregimenDerogadoProcesado(this, "N");

    jdoSetinteresesAdicionales(this, "N");

    jdoSetausenciaInjustificada(this, "N");

    jdoSetalicuotaBonoPetrolero(this, "N");

    jdoSetconstantePetroleroA(this, 0);

    jdoSetconstantePetroleroB(this, 0);

    jdoSetconstantePetroleroC(this, 0);

    jdoSetcambioMoneda(this, 0.0D);

    jdoSettopeHorasExtra(this, 0.0D);

    jdoSettopeHorasExtraMensual(this, 0.0D);
  }
  public String toString() {
    return jdoGettipoPersonal(this).getNombre();
  }

  public String getAlicuotaBfaBvac() {
    return jdoGetalicuotaBfaBvac(this);
  }

  public void setAlicuotaBfaBvac(String alicuotaBfaBvac) {
    jdoSetalicuotaBfaBvac(this, alicuotaBfaBvac);
  }

  public String getAlicuotaBfaPrestac() {
    return jdoGetalicuotaBfaPrestac(this);
  }

  public void setAlicuotaBfaPrestac(String alicuotaBfaPrestac) {
    jdoSetalicuotaBfaPrestac(this, alicuotaBfaPrestac);
  }

  public String getAlicuotaBonoPetrolero() {
    return jdoGetalicuotaBonoPetrolero(this);
  }

  public void setAlicuotaBonoPetrolero(String alicuotaBonoPetrolero) {
    jdoSetalicuotaBonoPetrolero(this, alicuotaBonoPetrolero);
  }

  public String getAlicuotaBvacBfa() {
    return jdoGetalicuotaBvacBfa(this);
  }

  public void setAlicuotaBvacBfa(String alicuotaBvacBfa) {
    jdoSetalicuotaBvacBfa(this, alicuotaBvacBfa);
  }

  public String getAlicuotaBvacPrestac() {
    return jdoGetalicuotaBvacPrestac(this);
  }

  public void setAlicuotaBvacPrestac(String alicuotaBvacPrestac) {
    jdoSetalicuotaBvacPrestac(this, alicuotaBvacPrestac);
  }

  public String getAniversarioDisfrute() {
    return jdoGetaniversarioDisfrute(this);
  }

  public void setAniversarioDisfrute(String aniversarioDisfrute) {
    jdoSetaniversarioDisfrute(this, aniversarioDisfrute);
  }

  public String getBonoExtra() {
    return jdoGetbonoExtra(this);
  }

  public void setBonoExtra(String bonoExtra) {
    jdoSetbonoExtra(this, bonoExtra);
  }

  public String getCalculoAlicuotaBfa() {
    return jdoGetcalculoAlicuotaBfa(this);
  }

  public void setCalculoAlicuotaBfa(String calculoAlicuotaBfa) {
    jdoSetcalculoAlicuotaBfa(this, calculoAlicuotaBfa);
  }

  public int getCalculoUtilidades() {
    return jdoGetcalculoUtilidades(this);
  }

  public void setCalculoUtilidades(int calculoUtilidades) {
    jdoSetcalculoUtilidades(this, calculoUtilidades);
  }

  public int getConstantePetroleroA() {
    return jdoGetconstantePetroleroA(this);
  }

  public void setConstantePetroleroA(int constantePetroleroA) {
    jdoSetconstantePetroleroA(this, constantePetroleroA);
  }

  public int getConstantePetroleroB() {
    return jdoGetconstantePetroleroB(this);
  }

  public void setConstantePetroleroB(int constantePetroleroB) {
    jdoSetconstantePetroleroB(this, constantePetroleroB);
  }

  public int getConstantePetroleroC() {
    return jdoGetconstantePetroleroC(this);
  }

  public void setConstantePetroleroC(int constantePetroleroC) {
    jdoSetconstantePetroleroC(this, constantePetroleroC);
  }

  public int getDiasAnio() {
    return jdoGetdiasAnio(this);
  }

  public void setDiasAnio(int diasAnio) {
    jdoSetdiasAnio(this, diasAnio);
  }

  public int getDiasBfa1997() {
    return jdoGetdiasBfa1997(this);
  }

  public void setDiasBfa1997(int diasBfa1997) {
    jdoSetdiasBfa1997(this, diasBfa1997);
  }

  public Date getFechaAperturaFideicomiso() {
    return jdoGetfechaAperturaFideicomiso(this);
  }

  public void setFechaAperturaFideicomiso(Date fechaAperturaFideicomiso) {
    jdoSetfechaAperturaFideicomiso(this, fechaAperturaFideicomiso);
  }

  public Date getFechaTopeUtilidades() {
    return jdoGetfechaTopeUtilidades(this);
  }

  public void setFechaTopeUtilidades(Date fechaTopeUtilidades) {
    jdoSetfechaTopeUtilidades(this, fechaTopeUtilidades);
  }

  public GrupoOrganismo getGrupoOrganismo() {
    return jdoGetgrupoOrganismo(this);
  }

  public void setGrupoOrganismo(GrupoOrganismo grupoOrganismo) {
    jdoSetgrupoOrganismo(this, grupoOrganismo);
  }

  public long getIdParametroVarios() {
    return jdoGetidParametroVarios(this);
  }

  public void setIdParametroVarios(long idParametroVarios) {
    jdoSetidParametroVarios(this, idParametroVarios);
  }

  public String getNuevoRegimen() {
    return jdoGetnuevoRegimen(this);
  }

  public void setNuevoRegimen(String nuevoRegimen) {
    jdoSetnuevoRegimen(this, nuevoRegimen);
  }

  public String getRegimenDerogadoProcesado() {
    return jdoGetregimenDerogadoProcesado(this);
  }

  public void setRegimenDerogadoProcesado(String regimenDerogadoProcesado) {
    jdoSetregimenDerogadoProcesado(this, regimenDerogadoProcesado);
  }

  public String getSumoApn() {
    return jdoGetsumoApn(this);
  }

  public void setSumoApn(String sumoApn) {
    jdoSetsumoApn(this, sumoApn);
  }

  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public String getInteresesAdicionales() {
    return jdoGetinteresesAdicionales(this);
  }

  public void setInteresesAdicionales(String interesesAdicionales) {
    jdoSetinteresesAdicionales(this, interesesAdicionales);
  }

  public String getAusenciaInjustificada() {
    return jdoGetausenciaInjustificada(this);
  }

  public void setAusenciaInjustificada(String ausenciaInjustificada) {
    jdoSetausenciaInjustificada(this, ausenciaInjustificada);
  }

  public double getCambioMoneda() {
    return jdoGetcambioMoneda(this);
  }

  public void setCambioMoneda(double cambioMoneda) {
    jdoSetcambioMoneda(this, cambioMoneda);
  }

  public String getTipoCalculoBfa() {
    return jdoGettipoCalculoBfa(this);
  }
  public void setTipoCalculoBfa(String tipoCalculoBfa) {
    jdoSettipoCalculoBfa(this, tipoCalculoBfa);
  }

  public double getTopeHorasExtra() {
    return jdoGettopeHorasExtra(this);
  }

  public void setTopeHorasExtra(double topeHorasExtra) {
    jdoSettopeHorasExtra(this, topeHorasExtra);
  }

  public double getTopeHorasExtraMensual() {
    return jdoGettopeHorasExtraMensual(this);
  }

  public void setTopeHorasExtraMensual(double topeHorasExtraMensual) {
    jdoSettopeHorasExtraMensual(this, topeHorasExtraMensual);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 28;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroVarios localParametroVarios = new ParametroVarios();
    localParametroVarios.jdoFlags = 1;
    localParametroVarios.jdoStateManager = paramStateManager;
    return localParametroVarios;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroVarios localParametroVarios = new ParametroVarios();
    localParametroVarios.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroVarios.jdoFlags = 1;
    localParametroVarios.jdoStateManager = paramStateManager;
    return localParametroVarios;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.alicuotaBfaBvac);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.alicuotaBfaPrestac);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.alicuotaBonoPetrolero);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.alicuotaBvacBfa);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.alicuotaBvacPrestac);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aniversarioDisfrute);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.ausenciaInjustificada);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.bonoExtra);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.calculoAlicuotaBfa);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.calculoUtilidades);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.cambioMoneda);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.constantePetroleroA);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.constantePetroleroB);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.constantePetroleroC);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasAnio);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasBfa1997);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaAperturaFideicomiso);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaTopeUtilidades);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoOrganismo);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroVarios);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.interesesAdicionales);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nuevoRegimen);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.regimenDerogadoProcesado);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sumoApn);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoCalculoBfa);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.topeHorasExtra);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.topeHorasExtraMensual);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alicuotaBfaBvac = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alicuotaBfaPrestac = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alicuotaBonoPetrolero = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alicuotaBvacBfa = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alicuotaBvacPrestac = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniversarioDisfrute = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ausenciaInjustificada = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bonoExtra = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.calculoAlicuotaBfa = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.calculoUtilidades = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cambioMoneda = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.constantePetroleroA = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.constantePetroleroB = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.constantePetroleroC = localStateManager.replacingIntField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasAnio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasBfa1997 = localStateManager.replacingIntField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaAperturaFideicomiso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaTopeUtilidades = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoOrganismo = ((GrupoOrganismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroVarios = localStateManager.replacingLongField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.interesesAdicionales = localStateManager.replacingStringField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nuevoRegimen = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.regimenDerogadoProcesado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sumoApn = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCalculoBfa = localStateManager.replacingStringField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.topeHorasExtra = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.topeHorasExtraMensual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroVarios paramParametroVarios, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.alicuotaBfaBvac = paramParametroVarios.alicuotaBfaBvac;
      return;
    case 1:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.alicuotaBfaPrestac = paramParametroVarios.alicuotaBfaPrestac;
      return;
    case 2:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.alicuotaBonoPetrolero = paramParametroVarios.alicuotaBonoPetrolero;
      return;
    case 3:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.alicuotaBvacBfa = paramParametroVarios.alicuotaBvacBfa;
      return;
    case 4:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.alicuotaBvacPrestac = paramParametroVarios.alicuotaBvacPrestac;
      return;
    case 5:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.aniversarioDisfrute = paramParametroVarios.aniversarioDisfrute;
      return;
    case 6:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.ausenciaInjustificada = paramParametroVarios.ausenciaInjustificada;
      return;
    case 7:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.bonoExtra = paramParametroVarios.bonoExtra;
      return;
    case 8:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.calculoAlicuotaBfa = paramParametroVarios.calculoAlicuotaBfa;
      return;
    case 9:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.calculoUtilidades = paramParametroVarios.calculoUtilidades;
      return;
    case 10:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.cambioMoneda = paramParametroVarios.cambioMoneda;
      return;
    case 11:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.constantePetroleroA = paramParametroVarios.constantePetroleroA;
      return;
    case 12:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.constantePetroleroB = paramParametroVarios.constantePetroleroB;
      return;
    case 13:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.constantePetroleroC = paramParametroVarios.constantePetroleroC;
      return;
    case 14:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.diasAnio = paramParametroVarios.diasAnio;
      return;
    case 15:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.diasBfa1997 = paramParametroVarios.diasBfa1997;
      return;
    case 16:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.fechaAperturaFideicomiso = paramParametroVarios.fechaAperturaFideicomiso;
      return;
    case 17:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.fechaTopeUtilidades = paramParametroVarios.fechaTopeUtilidades;
      return;
    case 18:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.grupoOrganismo = paramParametroVarios.grupoOrganismo;
      return;
    case 19:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroVarios = paramParametroVarios.idParametroVarios;
      return;
    case 20:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.interesesAdicionales = paramParametroVarios.interesesAdicionales;
      return;
    case 21:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.nuevoRegimen = paramParametroVarios.nuevoRegimen;
      return;
    case 22:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.regimenDerogadoProcesado = paramParametroVarios.regimenDerogadoProcesado;
      return;
    case 23:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.sumoApn = paramParametroVarios.sumoApn;
      return;
    case 24:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCalculoBfa = paramParametroVarios.tipoCalculoBfa;
      return;
    case 25:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramParametroVarios.tipoPersonal;
      return;
    case 26:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.topeHorasExtra = paramParametroVarios.topeHorasExtra;
      return;
    case 27:
      if (paramParametroVarios == null)
        throw new IllegalArgumentException("arg1");
      this.topeHorasExtraMensual = paramParametroVarios.topeHorasExtraMensual;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroVarios))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroVarios localParametroVarios = (ParametroVarios)paramObject;
    if (localParametroVarios.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroVarios, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroVariosPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroVariosPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroVariosPK))
      throw new IllegalArgumentException("arg1");
    ParametroVariosPK localParametroVariosPK = (ParametroVariosPK)paramObject;
    localParametroVariosPK.idParametroVarios = this.idParametroVarios;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroVariosPK))
      throw new IllegalArgumentException("arg1");
    ParametroVariosPK localParametroVariosPK = (ParametroVariosPK)paramObject;
    this.idParametroVarios = localParametroVariosPK.idParametroVarios;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroVariosPK))
      throw new IllegalArgumentException("arg2");
    ParametroVariosPK localParametroVariosPK = (ParametroVariosPK)paramObject;
    localParametroVariosPK.idParametroVarios = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 19);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroVariosPK))
      throw new IllegalArgumentException("arg2");
    ParametroVariosPK localParametroVariosPK = (ParametroVariosPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 19, localParametroVariosPK.idParametroVarios);
  }

  private static final String jdoGetalicuotaBfaBvac(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.alicuotaBfaBvac;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.alicuotaBfaBvac;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 0))
      return paramParametroVarios.alicuotaBfaBvac;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 0, paramParametroVarios.alicuotaBfaBvac);
  }

  private static final void jdoSetalicuotaBfaBvac(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.alicuotaBfaBvac = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.alicuotaBfaBvac = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 0, paramParametroVarios.alicuotaBfaBvac, paramString);
  }

  private static final String jdoGetalicuotaBfaPrestac(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.alicuotaBfaPrestac;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.alicuotaBfaPrestac;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 1))
      return paramParametroVarios.alicuotaBfaPrestac;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 1, paramParametroVarios.alicuotaBfaPrestac);
  }

  private static final void jdoSetalicuotaBfaPrestac(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.alicuotaBfaPrestac = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.alicuotaBfaPrestac = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 1, paramParametroVarios.alicuotaBfaPrestac, paramString);
  }

  private static final String jdoGetalicuotaBonoPetrolero(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.alicuotaBonoPetrolero;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.alicuotaBonoPetrolero;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 2))
      return paramParametroVarios.alicuotaBonoPetrolero;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 2, paramParametroVarios.alicuotaBonoPetrolero);
  }

  private static final void jdoSetalicuotaBonoPetrolero(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.alicuotaBonoPetrolero = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.alicuotaBonoPetrolero = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 2, paramParametroVarios.alicuotaBonoPetrolero, paramString);
  }

  private static final String jdoGetalicuotaBvacBfa(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.alicuotaBvacBfa;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.alicuotaBvacBfa;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 3))
      return paramParametroVarios.alicuotaBvacBfa;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 3, paramParametroVarios.alicuotaBvacBfa);
  }

  private static final void jdoSetalicuotaBvacBfa(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.alicuotaBvacBfa = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.alicuotaBvacBfa = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 3, paramParametroVarios.alicuotaBvacBfa, paramString);
  }

  private static final String jdoGetalicuotaBvacPrestac(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.alicuotaBvacPrestac;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.alicuotaBvacPrestac;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 4))
      return paramParametroVarios.alicuotaBvacPrestac;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 4, paramParametroVarios.alicuotaBvacPrestac);
  }

  private static final void jdoSetalicuotaBvacPrestac(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.alicuotaBvacPrestac = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.alicuotaBvacPrestac = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 4, paramParametroVarios.alicuotaBvacPrestac, paramString);
  }

  private static final String jdoGetaniversarioDisfrute(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.aniversarioDisfrute;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.aniversarioDisfrute;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 5))
      return paramParametroVarios.aniversarioDisfrute;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 5, paramParametroVarios.aniversarioDisfrute);
  }

  private static final void jdoSetaniversarioDisfrute(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.aniversarioDisfrute = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.aniversarioDisfrute = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 5, paramParametroVarios.aniversarioDisfrute, paramString);
  }

  private static final String jdoGetausenciaInjustificada(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.ausenciaInjustificada;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.ausenciaInjustificada;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 6))
      return paramParametroVarios.ausenciaInjustificada;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 6, paramParametroVarios.ausenciaInjustificada);
  }

  private static final void jdoSetausenciaInjustificada(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.ausenciaInjustificada = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.ausenciaInjustificada = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 6, paramParametroVarios.ausenciaInjustificada, paramString);
  }

  private static final String jdoGetbonoExtra(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.bonoExtra;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.bonoExtra;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 7))
      return paramParametroVarios.bonoExtra;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 7, paramParametroVarios.bonoExtra);
  }

  private static final void jdoSetbonoExtra(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.bonoExtra = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.bonoExtra = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 7, paramParametroVarios.bonoExtra, paramString);
  }

  private static final String jdoGetcalculoAlicuotaBfa(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.calculoAlicuotaBfa;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.calculoAlicuotaBfa;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 8))
      return paramParametroVarios.calculoAlicuotaBfa;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 8, paramParametroVarios.calculoAlicuotaBfa);
  }

  private static final void jdoSetcalculoAlicuotaBfa(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.calculoAlicuotaBfa = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.calculoAlicuotaBfa = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 8, paramParametroVarios.calculoAlicuotaBfa, paramString);
  }

  private static final int jdoGetcalculoUtilidades(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.calculoUtilidades;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.calculoUtilidades;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 9))
      return paramParametroVarios.calculoUtilidades;
    return localStateManager.getIntField(paramParametroVarios, jdoInheritedFieldCount + 9, paramParametroVarios.calculoUtilidades);
  }

  private static final void jdoSetcalculoUtilidades(ParametroVarios paramParametroVarios, int paramInt)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.calculoUtilidades = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.calculoUtilidades = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroVarios, jdoInheritedFieldCount + 9, paramParametroVarios.calculoUtilidades, paramInt);
  }

  private static final double jdoGetcambioMoneda(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.cambioMoneda;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.cambioMoneda;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 10))
      return paramParametroVarios.cambioMoneda;
    return localStateManager.getDoubleField(paramParametroVarios, jdoInheritedFieldCount + 10, paramParametroVarios.cambioMoneda);
  }

  private static final void jdoSetcambioMoneda(ParametroVarios paramParametroVarios, double paramDouble)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.cambioMoneda = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.cambioMoneda = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroVarios, jdoInheritedFieldCount + 10, paramParametroVarios.cambioMoneda, paramDouble);
  }

  private static final int jdoGetconstantePetroleroA(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.constantePetroleroA;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.constantePetroleroA;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 11))
      return paramParametroVarios.constantePetroleroA;
    return localStateManager.getIntField(paramParametroVarios, jdoInheritedFieldCount + 11, paramParametroVarios.constantePetroleroA);
  }

  private static final void jdoSetconstantePetroleroA(ParametroVarios paramParametroVarios, int paramInt)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.constantePetroleroA = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.constantePetroleroA = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroVarios, jdoInheritedFieldCount + 11, paramParametroVarios.constantePetroleroA, paramInt);
  }

  private static final int jdoGetconstantePetroleroB(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.constantePetroleroB;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.constantePetroleroB;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 12))
      return paramParametroVarios.constantePetroleroB;
    return localStateManager.getIntField(paramParametroVarios, jdoInheritedFieldCount + 12, paramParametroVarios.constantePetroleroB);
  }

  private static final void jdoSetconstantePetroleroB(ParametroVarios paramParametroVarios, int paramInt)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.constantePetroleroB = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.constantePetroleroB = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroVarios, jdoInheritedFieldCount + 12, paramParametroVarios.constantePetroleroB, paramInt);
  }

  private static final int jdoGetconstantePetroleroC(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.constantePetroleroC;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.constantePetroleroC;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 13))
      return paramParametroVarios.constantePetroleroC;
    return localStateManager.getIntField(paramParametroVarios, jdoInheritedFieldCount + 13, paramParametroVarios.constantePetroleroC);
  }

  private static final void jdoSetconstantePetroleroC(ParametroVarios paramParametroVarios, int paramInt)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.constantePetroleroC = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.constantePetroleroC = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroVarios, jdoInheritedFieldCount + 13, paramParametroVarios.constantePetroleroC, paramInt);
  }

  private static final int jdoGetdiasAnio(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.diasAnio;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.diasAnio;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 14))
      return paramParametroVarios.diasAnio;
    return localStateManager.getIntField(paramParametroVarios, jdoInheritedFieldCount + 14, paramParametroVarios.diasAnio);
  }

  private static final void jdoSetdiasAnio(ParametroVarios paramParametroVarios, int paramInt)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.diasAnio = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.diasAnio = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroVarios, jdoInheritedFieldCount + 14, paramParametroVarios.diasAnio, paramInt);
  }

  private static final int jdoGetdiasBfa1997(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.diasBfa1997;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.diasBfa1997;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 15))
      return paramParametroVarios.diasBfa1997;
    return localStateManager.getIntField(paramParametroVarios, jdoInheritedFieldCount + 15, paramParametroVarios.diasBfa1997);
  }

  private static final void jdoSetdiasBfa1997(ParametroVarios paramParametroVarios, int paramInt)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.diasBfa1997 = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.diasBfa1997 = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroVarios, jdoInheritedFieldCount + 15, paramParametroVarios.diasBfa1997, paramInt);
  }

  private static final Date jdoGetfechaAperturaFideicomiso(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.fechaAperturaFideicomiso;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.fechaAperturaFideicomiso;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 16))
      return paramParametroVarios.fechaAperturaFideicomiso;
    return (Date)localStateManager.getObjectField(paramParametroVarios, jdoInheritedFieldCount + 16, paramParametroVarios.fechaAperturaFideicomiso);
  }

  private static final void jdoSetfechaAperturaFideicomiso(ParametroVarios paramParametroVarios, Date paramDate)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.fechaAperturaFideicomiso = paramDate;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.fechaAperturaFideicomiso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramParametroVarios, jdoInheritedFieldCount + 16, paramParametroVarios.fechaAperturaFideicomiso, paramDate);
  }

  private static final Date jdoGetfechaTopeUtilidades(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.fechaTopeUtilidades;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.fechaTopeUtilidades;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 17))
      return paramParametroVarios.fechaTopeUtilidades;
    return (Date)localStateManager.getObjectField(paramParametroVarios, jdoInheritedFieldCount + 17, paramParametroVarios.fechaTopeUtilidades);
  }

  private static final void jdoSetfechaTopeUtilidades(ParametroVarios paramParametroVarios, Date paramDate)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.fechaTopeUtilidades = paramDate;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.fechaTopeUtilidades = paramDate;
      return;
    }
    localStateManager.setObjectField(paramParametroVarios, jdoInheritedFieldCount + 17, paramParametroVarios.fechaTopeUtilidades, paramDate);
  }

  private static final GrupoOrganismo jdoGetgrupoOrganismo(ParametroVarios paramParametroVarios)
  {
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.grupoOrganismo;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 18))
      return paramParametroVarios.grupoOrganismo;
    return (GrupoOrganismo)localStateManager.getObjectField(paramParametroVarios, jdoInheritedFieldCount + 18, paramParametroVarios.grupoOrganismo);
  }

  private static final void jdoSetgrupoOrganismo(ParametroVarios paramParametroVarios, GrupoOrganismo paramGrupoOrganismo)
  {
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.grupoOrganismo = paramGrupoOrganismo;
      return;
    }
    localStateManager.setObjectField(paramParametroVarios, jdoInheritedFieldCount + 18, paramParametroVarios.grupoOrganismo, paramGrupoOrganismo);
  }

  private static final long jdoGetidParametroVarios(ParametroVarios paramParametroVarios)
  {
    return paramParametroVarios.idParametroVarios;
  }

  private static final void jdoSetidParametroVarios(ParametroVarios paramParametroVarios, long paramLong)
  {
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.idParametroVarios = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroVarios, jdoInheritedFieldCount + 19, paramParametroVarios.idParametroVarios, paramLong);
  }

  private static final String jdoGetinteresesAdicionales(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.interesesAdicionales;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.interesesAdicionales;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 20))
      return paramParametroVarios.interesesAdicionales;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 20, paramParametroVarios.interesesAdicionales);
  }

  private static final void jdoSetinteresesAdicionales(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.interesesAdicionales = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.interesesAdicionales = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 20, paramParametroVarios.interesesAdicionales, paramString);
  }

  private static final String jdoGetnuevoRegimen(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.nuevoRegimen;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.nuevoRegimen;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 21))
      return paramParametroVarios.nuevoRegimen;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 21, paramParametroVarios.nuevoRegimen);
  }

  private static final void jdoSetnuevoRegimen(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.nuevoRegimen = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.nuevoRegimen = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 21, paramParametroVarios.nuevoRegimen, paramString);
  }

  private static final String jdoGetregimenDerogadoProcesado(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.regimenDerogadoProcesado;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.regimenDerogadoProcesado;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 22))
      return paramParametroVarios.regimenDerogadoProcesado;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 22, paramParametroVarios.regimenDerogadoProcesado);
  }

  private static final void jdoSetregimenDerogadoProcesado(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.regimenDerogadoProcesado = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.regimenDerogadoProcesado = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 22, paramParametroVarios.regimenDerogadoProcesado, paramString);
  }

  private static final String jdoGetsumoApn(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.sumoApn;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.sumoApn;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 23))
      return paramParametroVarios.sumoApn;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 23, paramParametroVarios.sumoApn);
  }

  private static final void jdoSetsumoApn(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.sumoApn = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.sumoApn = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 23, paramParametroVarios.sumoApn, paramString);
  }

  private static final String jdoGettipoCalculoBfa(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.tipoCalculoBfa;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.tipoCalculoBfa;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 24))
      return paramParametroVarios.tipoCalculoBfa;
    return localStateManager.getStringField(paramParametroVarios, jdoInheritedFieldCount + 24, paramParametroVarios.tipoCalculoBfa);
  }

  private static final void jdoSettipoCalculoBfa(ParametroVarios paramParametroVarios, String paramString)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.tipoCalculoBfa = paramString;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.tipoCalculoBfa = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroVarios, jdoInheritedFieldCount + 24, paramParametroVarios.tipoCalculoBfa, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(ParametroVarios paramParametroVarios)
  {
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.tipoPersonal;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 25))
      return paramParametroVarios.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramParametroVarios, jdoInheritedFieldCount + 25, paramParametroVarios.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ParametroVarios paramParametroVarios, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramParametroVarios, jdoInheritedFieldCount + 25, paramParametroVarios.tipoPersonal, paramTipoPersonal);
  }

  private static final double jdoGettopeHorasExtra(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.topeHorasExtra;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.topeHorasExtra;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 26))
      return paramParametroVarios.topeHorasExtra;
    return localStateManager.getDoubleField(paramParametroVarios, jdoInheritedFieldCount + 26, paramParametroVarios.topeHorasExtra);
  }

  private static final void jdoSettopeHorasExtra(ParametroVarios paramParametroVarios, double paramDouble)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.topeHorasExtra = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.topeHorasExtra = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroVarios, jdoInheritedFieldCount + 26, paramParametroVarios.topeHorasExtra, paramDouble);
  }

  private static final double jdoGettopeHorasExtraMensual(ParametroVarios paramParametroVarios)
  {
    if (paramParametroVarios.jdoFlags <= 0)
      return paramParametroVarios.topeHorasExtraMensual;
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
      return paramParametroVarios.topeHorasExtraMensual;
    if (localStateManager.isLoaded(paramParametroVarios, jdoInheritedFieldCount + 27))
      return paramParametroVarios.topeHorasExtraMensual;
    return localStateManager.getDoubleField(paramParametroVarios, jdoInheritedFieldCount + 27, paramParametroVarios.topeHorasExtraMensual);
  }

  private static final void jdoSettopeHorasExtraMensual(ParametroVarios paramParametroVarios, double paramDouble)
  {
    if (paramParametroVarios.jdoFlags == 0)
    {
      paramParametroVarios.topeHorasExtraMensual = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroVarios.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroVarios.topeHorasExtraMensual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroVarios, jdoInheritedFieldCount + 27, paramParametroVarios.topeHorasExtraMensual, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}