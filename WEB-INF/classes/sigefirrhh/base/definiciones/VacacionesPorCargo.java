package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;

public class VacacionesPorCargo
  implements Serializable, PersistenceCapable
{
  private long idVacacionesPorCargo;
  private TipoPersonal tipoPersonal;
  private int aniosServicio;
  private int diasDisfrutar;
  private double diasBono;
  private double diasExtra;
  private Cargo cargo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aniosServicio", "cargo", "diasBono", "diasDisfrutar", "diasExtra", "idVacacionesPorCargo", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Double.TYPE, Integer.TYPE, Double.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") };
  private static final byte[] jdoFieldFlags = { 21, 26, 21, 21, 21, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public double getDiasBono()
  {
    return jdoGetdiasBono(this);
  }

  public int getDiasDisfrutar()
  {
    return jdoGetdiasDisfrutar(this);
  }

  public double getDiasExtra()
  {
    return jdoGetdiasExtra(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setAniosServicio(int i)
  {
    jdoSetaniosServicio(this, i);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public void setDiasBono(double d)
  {
    jdoSetdiasBono(this, d);
  }

  public void setDiasDisfrutar(int i)
  {
    jdoSetdiasDisfrutar(this, i);
  }

  public void setDiasExtra(double d)
  {
    jdoSetdiasExtra(this, d);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public long getIdVacacionesPorCargo()
  {
    return jdoGetidVacacionesPorCargo(this);
  }

  public void setIdVacacionesPorCargo(long l)
  {
    jdoSetidVacacionesPorCargo(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.VacacionesPorCargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new VacacionesPorCargo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    VacacionesPorCargo localVacacionesPorCargo = new VacacionesPorCargo();
    localVacacionesPorCargo.jdoFlags = 1;
    localVacacionesPorCargo.jdoStateManager = paramStateManager;
    return localVacacionesPorCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    VacacionesPorCargo localVacacionesPorCargo = new VacacionesPorCargo();
    localVacacionesPorCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localVacacionesPorCargo.jdoFlags = 1;
    localVacacionesPorCargo.jdoStateManager = paramStateManager;
    return localVacacionesPorCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasBono);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasDisfrutar);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasExtra);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idVacacionesPorCargo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasBono = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasDisfrutar = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasExtra = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idVacacionesPorCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(VacacionesPorCargo paramVacacionesPorCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramVacacionesPorCargo == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramVacacionesPorCargo.aniosServicio;
      return;
    case 1:
      if (paramVacacionesPorCargo == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramVacacionesPorCargo.cargo;
      return;
    case 2:
      if (paramVacacionesPorCargo == null)
        throw new IllegalArgumentException("arg1");
      this.diasBono = paramVacacionesPorCargo.diasBono;
      return;
    case 3:
      if (paramVacacionesPorCargo == null)
        throw new IllegalArgumentException("arg1");
      this.diasDisfrutar = paramVacacionesPorCargo.diasDisfrutar;
      return;
    case 4:
      if (paramVacacionesPorCargo == null)
        throw new IllegalArgumentException("arg1");
      this.diasExtra = paramVacacionesPorCargo.diasExtra;
      return;
    case 5:
      if (paramVacacionesPorCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idVacacionesPorCargo = paramVacacionesPorCargo.idVacacionesPorCargo;
      return;
    case 6:
      if (paramVacacionesPorCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramVacacionesPorCargo.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof VacacionesPorCargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    VacacionesPorCargo localVacacionesPorCargo = (VacacionesPorCargo)paramObject;
    if (localVacacionesPorCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localVacacionesPorCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new VacacionesPorCargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new VacacionesPorCargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VacacionesPorCargoPK))
      throw new IllegalArgumentException("arg1");
    VacacionesPorCargoPK localVacacionesPorCargoPK = (VacacionesPorCargoPK)paramObject;
    localVacacionesPorCargoPK.idVacacionesPorCargo = this.idVacacionesPorCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VacacionesPorCargoPK))
      throw new IllegalArgumentException("arg1");
    VacacionesPorCargoPK localVacacionesPorCargoPK = (VacacionesPorCargoPK)paramObject;
    this.idVacacionesPorCargo = localVacacionesPorCargoPK.idVacacionesPorCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VacacionesPorCargoPK))
      throw new IllegalArgumentException("arg2");
    VacacionesPorCargoPK localVacacionesPorCargoPK = (VacacionesPorCargoPK)paramObject;
    localVacacionesPorCargoPK.idVacacionesPorCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VacacionesPorCargoPK))
      throw new IllegalArgumentException("arg2");
    VacacionesPorCargoPK localVacacionesPorCargoPK = (VacacionesPorCargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localVacacionesPorCargoPK.idVacacionesPorCargo);
  }

  private static final int jdoGetaniosServicio(VacacionesPorCargo paramVacacionesPorCargo)
  {
    if (paramVacacionesPorCargo.jdoFlags <= 0)
      return paramVacacionesPorCargo.aniosServicio;
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionesPorCargo.aniosServicio;
    if (localStateManager.isLoaded(paramVacacionesPorCargo, jdoInheritedFieldCount + 0))
      return paramVacacionesPorCargo.aniosServicio;
    return localStateManager.getIntField(paramVacacionesPorCargo, jdoInheritedFieldCount + 0, paramVacacionesPorCargo.aniosServicio);
  }

  private static final void jdoSetaniosServicio(VacacionesPorCargo paramVacacionesPorCargo, int paramInt)
  {
    if (paramVacacionesPorCargo.jdoFlags == 0)
    {
      paramVacacionesPorCargo.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorCargo.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacionesPorCargo, jdoInheritedFieldCount + 0, paramVacacionesPorCargo.aniosServicio, paramInt);
  }

  private static final Cargo jdoGetcargo(VacacionesPorCargo paramVacacionesPorCargo)
  {
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionesPorCargo.cargo;
    if (localStateManager.isLoaded(paramVacacionesPorCargo, jdoInheritedFieldCount + 1))
      return paramVacacionesPorCargo.cargo;
    return (Cargo)localStateManager.getObjectField(paramVacacionesPorCargo, jdoInheritedFieldCount + 1, paramVacacionesPorCargo.cargo);
  }

  private static final void jdoSetcargo(VacacionesPorCargo paramVacacionesPorCargo, Cargo paramCargo)
  {
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorCargo.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramVacacionesPorCargo, jdoInheritedFieldCount + 1, paramVacacionesPorCargo.cargo, paramCargo);
  }

  private static final double jdoGetdiasBono(VacacionesPorCargo paramVacacionesPorCargo)
  {
    if (paramVacacionesPorCargo.jdoFlags <= 0)
      return paramVacacionesPorCargo.diasBono;
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionesPorCargo.diasBono;
    if (localStateManager.isLoaded(paramVacacionesPorCargo, jdoInheritedFieldCount + 2))
      return paramVacacionesPorCargo.diasBono;
    return localStateManager.getDoubleField(paramVacacionesPorCargo, jdoInheritedFieldCount + 2, paramVacacionesPorCargo.diasBono);
  }

  private static final void jdoSetdiasBono(VacacionesPorCargo paramVacacionesPorCargo, double paramDouble)
  {
    if (paramVacacionesPorCargo.jdoFlags == 0)
    {
      paramVacacionesPorCargo.diasBono = paramDouble;
      return;
    }
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorCargo.diasBono = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramVacacionesPorCargo, jdoInheritedFieldCount + 2, paramVacacionesPorCargo.diasBono, paramDouble);
  }

  private static final int jdoGetdiasDisfrutar(VacacionesPorCargo paramVacacionesPorCargo)
  {
    if (paramVacacionesPorCargo.jdoFlags <= 0)
      return paramVacacionesPorCargo.diasDisfrutar;
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionesPorCargo.diasDisfrutar;
    if (localStateManager.isLoaded(paramVacacionesPorCargo, jdoInheritedFieldCount + 3))
      return paramVacacionesPorCargo.diasDisfrutar;
    return localStateManager.getIntField(paramVacacionesPorCargo, jdoInheritedFieldCount + 3, paramVacacionesPorCargo.diasDisfrutar);
  }

  private static final void jdoSetdiasDisfrutar(VacacionesPorCargo paramVacacionesPorCargo, int paramInt)
  {
    if (paramVacacionesPorCargo.jdoFlags == 0)
    {
      paramVacacionesPorCargo.diasDisfrutar = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorCargo.diasDisfrutar = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacionesPorCargo, jdoInheritedFieldCount + 3, paramVacacionesPorCargo.diasDisfrutar, paramInt);
  }

  private static final double jdoGetdiasExtra(VacacionesPorCargo paramVacacionesPorCargo)
  {
    if (paramVacacionesPorCargo.jdoFlags <= 0)
      return paramVacacionesPorCargo.diasExtra;
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionesPorCargo.diasExtra;
    if (localStateManager.isLoaded(paramVacacionesPorCargo, jdoInheritedFieldCount + 4))
      return paramVacacionesPorCargo.diasExtra;
    return localStateManager.getDoubleField(paramVacacionesPorCargo, jdoInheritedFieldCount + 4, paramVacacionesPorCargo.diasExtra);
  }

  private static final void jdoSetdiasExtra(VacacionesPorCargo paramVacacionesPorCargo, double paramDouble)
  {
    if (paramVacacionesPorCargo.jdoFlags == 0)
    {
      paramVacacionesPorCargo.diasExtra = paramDouble;
      return;
    }
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorCargo.diasExtra = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramVacacionesPorCargo, jdoInheritedFieldCount + 4, paramVacacionesPorCargo.diasExtra, paramDouble);
  }

  private static final long jdoGetidVacacionesPorCargo(VacacionesPorCargo paramVacacionesPorCargo)
  {
    return paramVacacionesPorCargo.idVacacionesPorCargo;
  }

  private static final void jdoSetidVacacionesPorCargo(VacacionesPorCargo paramVacacionesPorCargo, long paramLong)
  {
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorCargo.idVacacionesPorCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramVacacionesPorCargo, jdoInheritedFieldCount + 5, paramVacacionesPorCargo.idVacacionesPorCargo, paramLong);
  }

  private static final TipoPersonal jdoGettipoPersonal(VacacionesPorCargo paramVacacionesPorCargo)
  {
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionesPorCargo.tipoPersonal;
    if (localStateManager.isLoaded(paramVacacionesPorCargo, jdoInheritedFieldCount + 6))
      return paramVacacionesPorCargo.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramVacacionesPorCargo, jdoInheritedFieldCount + 6, paramVacacionesPorCargo.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(VacacionesPorCargo paramVacacionesPorCargo, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramVacacionesPorCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorCargo.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramVacacionesPorCargo, jdoInheritedFieldCount + 6, paramVacacionesPorCargo.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}