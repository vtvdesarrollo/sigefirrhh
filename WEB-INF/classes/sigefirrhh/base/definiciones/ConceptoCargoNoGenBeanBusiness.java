package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ConceptoCargoNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByCargo(long idCargo, String excluir, String automaticoIngreso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cargo.idCargo == pIdCargo && excluir == pExcluir && automaticoIngreso == pAutomaticoIngreso";

    Query query = pm.newQuery(ConceptoCargo.class, filter);

    query.declareParameters("long pIdCargo, String pExcluir, String pAutomaticoIngreso");
    HashMap parameters = new HashMap();

    parameters.put("pIdCargo", new Long(idCargo));
    parameters.put("pExcluir", excluir);
    parameters.put("pAutomaticoIngreso", automaticoIngreso);

    query.setOrdering("conceptoTipoPersonal.codConcepto ascending");

    Collection colConceptoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoCargo);

    return colConceptoCargo;
  }
}