package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoContratoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoContrato(TipoContrato tipoContrato)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoContrato tipoContratoNew = 
      (TipoContrato)BeanUtils.cloneBean(
      tipoContrato);

    pm.makePersistent(tipoContratoNew);
  }

  public void updateTipoContrato(TipoContrato tipoContrato) throws Exception
  {
    TipoContrato tipoContratoModify = 
      findTipoContratoById(tipoContrato.getIdTipoContrato());

    BeanUtils.copyProperties(tipoContratoModify, tipoContrato);
  }

  public void deleteTipoContrato(TipoContrato tipoContrato) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoContrato tipoContratoDelete = 
      findTipoContratoById(tipoContrato.getIdTipoContrato());
    pm.deletePersistent(tipoContratoDelete);
  }

  public TipoContrato findTipoContratoById(long idTipoContrato) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoContrato == pIdTipoContrato";
    Query query = pm.newQuery(TipoContrato.class, filter);

    query.declareParameters("long pIdTipoContrato");

    parameters.put("pIdTipoContrato", new Long(idTipoContrato));

    Collection colTipoContrato = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoContrato.iterator();
    return (TipoContrato)iterator.next();
  }

  public Collection findTipoContratoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoContratoExtent = pm.getExtent(
      TipoContrato.class, true);
    Query query = pm.newQuery(tipoContratoExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoContrato(String codTipoContrato)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoContrato == pCodTipoContrato";

    Query query = pm.newQuery(TipoContrato.class, filter);

    query.declareParameters("java.lang.String pCodTipoContrato");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoContrato", new String(codTipoContrato));

    query.setOrdering("descripcion ascending");

    Collection colTipoContrato = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoContrato);

    return colTipoContrato;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(TipoContrato.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTipoContrato = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoContrato);

    return colTipoContrato;
  }
}