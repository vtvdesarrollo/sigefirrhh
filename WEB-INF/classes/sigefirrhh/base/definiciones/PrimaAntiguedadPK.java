package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class PrimaAntiguedadPK
  implements Serializable
{
  public long idPrimaAntiguedad;

  public PrimaAntiguedadPK()
  {
  }

  public PrimaAntiguedadPK(long idPrimaAntiguedad)
  {
    this.idPrimaAntiguedad = idPrimaAntiguedad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PrimaAntiguedadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PrimaAntiguedadPK thatPK)
  {
    return 
      this.idPrimaAntiguedad == thatPK.idPrimaAntiguedad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPrimaAntiguedad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPrimaAntiguedad);
  }
}