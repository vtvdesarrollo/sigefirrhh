package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ClasificacionPersonal
  implements Serializable, PersistenceCapable
{
  private long idClasificacionPersonal;
  private CategoriaPersonal categoriaPersonal;
  private RelacionPersonal relacionPersonal;
  private CategoriaPresupuesto categoriaPresupuesto;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "categoriaPersonal", "categoriaPresupuesto", "idClasificacionPersonal", "relacionPersonal" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.CategoriaPersonal"), sunjdo$classForName$("sigefirrhh.base.definiciones.CategoriaPresupuesto"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.RelacionPersonal") };
  private static final byte[] jdoFieldFlags = { 26, 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcategoriaPersonal(this).getDescCategoria() + " - " + 
      jdoGetrelacionPersonal(this).getDescRelacion();
  }

  public CategoriaPersonal getCategoriaPersonal()
  {
    return jdoGetcategoriaPersonal(this);
  }

  public long getIdClasificacionPersonal()
  {
    return jdoGetidClasificacionPersonal(this);
  }

  public RelacionPersonal getRelacionPersonal()
  {
    return jdoGetrelacionPersonal(this);
  }

  public void setCategoriaPersonal(CategoriaPersonal personal)
  {
    jdoSetcategoriaPersonal(this, personal);
  }

  public void setIdClasificacionPersonal(long l)
  {
    jdoSetidClasificacionPersonal(this, l);
  }

  public void setRelacionPersonal(RelacionPersonal personal)
  {
    jdoSetrelacionPersonal(this, personal);
  }

  public CategoriaPresupuesto getCategoriaPresupuesto()
  {
    return jdoGetcategoriaPresupuesto(this);
  }

  public void setCategoriaPresupuesto(CategoriaPresupuesto presupuesto)
  {
    jdoSetcategoriaPresupuesto(this, presupuesto);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ClasificacionPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ClasificacionPersonal());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ClasificacionPersonal localClasificacionPersonal = new ClasificacionPersonal();
    localClasificacionPersonal.jdoFlags = 1;
    localClasificacionPersonal.jdoStateManager = paramStateManager;
    return localClasificacionPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ClasificacionPersonal localClasificacionPersonal = new ClasificacionPersonal();
    localClasificacionPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localClasificacionPersonal.jdoFlags = 1;
    localClasificacionPersonal.jdoStateManager = paramStateManager;
    return localClasificacionPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.categoriaPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.categoriaPresupuesto);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idClasificacionPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.relacionPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.categoriaPersonal = ((CategoriaPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.categoriaPresupuesto = ((CategoriaPresupuesto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idClasificacionPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.relacionPersonal = ((RelacionPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ClasificacionPersonal paramClasificacionPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramClasificacionPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.categoriaPersonal = paramClasificacionPersonal.categoriaPersonal;
      return;
    case 1:
      if (paramClasificacionPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.categoriaPresupuesto = paramClasificacionPersonal.categoriaPresupuesto;
      return;
    case 2:
      if (paramClasificacionPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idClasificacionPersonal = paramClasificacionPersonal.idClasificacionPersonal;
      return;
    case 3:
      if (paramClasificacionPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.relacionPersonal = paramClasificacionPersonal.relacionPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ClasificacionPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ClasificacionPersonal localClasificacionPersonal = (ClasificacionPersonal)paramObject;
    if (localClasificacionPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localClasificacionPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ClasificacionPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ClasificacionPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ClasificacionPersonalPK))
      throw new IllegalArgumentException("arg1");
    ClasificacionPersonalPK localClasificacionPersonalPK = (ClasificacionPersonalPK)paramObject;
    localClasificacionPersonalPK.idClasificacionPersonal = this.idClasificacionPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ClasificacionPersonalPK))
      throw new IllegalArgumentException("arg1");
    ClasificacionPersonalPK localClasificacionPersonalPK = (ClasificacionPersonalPK)paramObject;
    this.idClasificacionPersonal = localClasificacionPersonalPK.idClasificacionPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ClasificacionPersonalPK))
      throw new IllegalArgumentException("arg2");
    ClasificacionPersonalPK localClasificacionPersonalPK = (ClasificacionPersonalPK)paramObject;
    localClasificacionPersonalPK.idClasificacionPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ClasificacionPersonalPK))
      throw new IllegalArgumentException("arg2");
    ClasificacionPersonalPK localClasificacionPersonalPK = (ClasificacionPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localClasificacionPersonalPK.idClasificacionPersonal);
  }

  private static final CategoriaPersonal jdoGetcategoriaPersonal(ClasificacionPersonal paramClasificacionPersonal)
  {
    StateManager localStateManager = paramClasificacionPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramClasificacionPersonal.categoriaPersonal;
    if (localStateManager.isLoaded(paramClasificacionPersonal, jdoInheritedFieldCount + 0))
      return paramClasificacionPersonal.categoriaPersonal;
    return (CategoriaPersonal)localStateManager.getObjectField(paramClasificacionPersonal, jdoInheritedFieldCount + 0, paramClasificacionPersonal.categoriaPersonal);
  }

  private static final void jdoSetcategoriaPersonal(ClasificacionPersonal paramClasificacionPersonal, CategoriaPersonal paramCategoriaPersonal)
  {
    StateManager localStateManager = paramClasificacionPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacionPersonal.categoriaPersonal = paramCategoriaPersonal;
      return;
    }
    localStateManager.setObjectField(paramClasificacionPersonal, jdoInheritedFieldCount + 0, paramClasificacionPersonal.categoriaPersonal, paramCategoriaPersonal);
  }

  private static final CategoriaPresupuesto jdoGetcategoriaPresupuesto(ClasificacionPersonal paramClasificacionPersonal)
  {
    StateManager localStateManager = paramClasificacionPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramClasificacionPersonal.categoriaPresupuesto;
    if (localStateManager.isLoaded(paramClasificacionPersonal, jdoInheritedFieldCount + 1))
      return paramClasificacionPersonal.categoriaPresupuesto;
    return (CategoriaPresupuesto)localStateManager.getObjectField(paramClasificacionPersonal, jdoInheritedFieldCount + 1, paramClasificacionPersonal.categoriaPresupuesto);
  }

  private static final void jdoSetcategoriaPresupuesto(ClasificacionPersonal paramClasificacionPersonal, CategoriaPresupuesto paramCategoriaPresupuesto)
  {
    StateManager localStateManager = paramClasificacionPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacionPersonal.categoriaPresupuesto = paramCategoriaPresupuesto;
      return;
    }
    localStateManager.setObjectField(paramClasificacionPersonal, jdoInheritedFieldCount + 1, paramClasificacionPersonal.categoriaPresupuesto, paramCategoriaPresupuesto);
  }

  private static final long jdoGetidClasificacionPersonal(ClasificacionPersonal paramClasificacionPersonal)
  {
    return paramClasificacionPersonal.idClasificacionPersonal;
  }

  private static final void jdoSetidClasificacionPersonal(ClasificacionPersonal paramClasificacionPersonal, long paramLong)
  {
    StateManager localStateManager = paramClasificacionPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacionPersonal.idClasificacionPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramClasificacionPersonal, jdoInheritedFieldCount + 2, paramClasificacionPersonal.idClasificacionPersonal, paramLong);
  }

  private static final RelacionPersonal jdoGetrelacionPersonal(ClasificacionPersonal paramClasificacionPersonal)
  {
    StateManager localStateManager = paramClasificacionPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramClasificacionPersonal.relacionPersonal;
    if (localStateManager.isLoaded(paramClasificacionPersonal, jdoInheritedFieldCount + 3))
      return paramClasificacionPersonal.relacionPersonal;
    return (RelacionPersonal)localStateManager.getObjectField(paramClasificacionPersonal, jdoInheritedFieldCount + 3, paramClasificacionPersonal.relacionPersonal);
  }

  private static final void jdoSetrelacionPersonal(ClasificacionPersonal paramClasificacionPersonal, RelacionPersonal paramRelacionPersonal)
  {
    StateManager localStateManager = paramClasificacionPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacionPersonal.relacionPersonal = paramRelacionPersonal;
      return;
    }
    localStateManager.setObjectField(paramClasificacionPersonal, jdoInheritedFieldCount + 3, paramClasificacionPersonal.relacionPersonal, paramRelacionPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}