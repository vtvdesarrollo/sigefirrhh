package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ParametroVariosPK
  implements Serializable
{
  public long idParametroVarios;

  public ParametroVariosPK()
  {
  }

  public ParametroVariosPK(long idParametroVarios)
  {
    this.idParametroVarios = idParametroVarios;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroVariosPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroVariosPK thatPK)
  {
    return 
      this.idParametroVarios == thatPK.idParametroVarios;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroVarios)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroVarios);
  }
}