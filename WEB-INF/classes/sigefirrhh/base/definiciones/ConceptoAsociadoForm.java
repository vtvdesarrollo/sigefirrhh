package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ConceptoAsociadoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoAsociadoForm.class.getName());
  private ConceptoAsociado conceptoAsociado;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private int reportId;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private boolean showConceptoAsociadoByConceptoTipoPersonal;
  private String findSelectTipoPersonalForConceptoTipoPersonal;
  private String findSelectConceptoTipoPersonal;
  private Collection findColTipoPersonalForConceptoTipoPersonal;
  private Collection findColConceptoTipoPersonal;
  private Collection colTipoPersonalForConceptoTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private Collection colTipoPersonalForConceptoAsociar;
  private Collection colConceptoAsociar;
  private String selectTipoPersonalForConceptoTipoPersonal;
  private String selectConceptoTipoPersonal;
  private String selectTipoPersonalForConceptoAsociar;
  private String selectConceptoAsociar;
  private Object stateScrollConceptoAsociadoByConceptoTipoPersonal = null;
  private Object stateResultConceptoAsociadoByConceptoTipoPersonal = null;

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
    if (this.conceptoAsociado != null) {
      parameters.put("id_tipo_personal", new Long(this.conceptoAsociado.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal()));
    }
    JasperForWeb report = new JasperForWeb();

    report.setParameters(parameters);

    report.setReportName("ConceptoAsociado");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "ConceptoAsociado" + this.reportId, report);
    newReportId();
    return null;
  }

  public Collection getFindColTipoPersonalForConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectTipoPersonalForConceptoTipoPersonal() {
    return this.findSelectTipoPersonalForConceptoTipoPersonal;
  }
  public void setFindSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.findSelectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }
  public void findChangeTipoPersonalForConceptoTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.findColConceptoTipoPersonal = null;
      if (idTipoPersonal > 0L)
        this.findColConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(
          idTipoPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowTipoPersonalForConceptoTipoPersonal() { return this.findColTipoPersonalForConceptoTipoPersonal != null; }

  public String getFindSelectConceptoTipoPersonal() {
    return this.findSelectConceptoTipoPersonal;
  }
  public void setFindSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    this.findSelectConceptoTipoPersonal = valConceptoTipoPersonal;
  }

  public Collection getFindColConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConceptoTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public boolean isFindShowConceptoTipoPersonal()
  {
    return this.findColConceptoTipoPersonal != null;
  }

  public String getSelectTipoPersonalForConceptoTipoPersonal()
  {
    return this.selectTipoPersonalForConceptoTipoPersonal;
  }
  public void setSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.selectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }
  public void changeTipoPersonalForConceptoTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colConceptoTipoPersonal = null;
      this.colConceptoAsociar = null;
      if (idTipoPersonal > 0L) {
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(
          idTipoPersonal);
        this.colConceptoAsociar = 
          this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(
          idTipoPersonal);
      } else {
        this.selectConceptoAsociar = null;
        this.selectConceptoTipoPersonal = null;
        this.conceptoAsociado.setConceptoTipoPersonal(
          null);
        this.conceptoAsociado.setConceptoAsociar(null);
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectConceptoTipoPersonal = null;
      this.conceptoAsociado.setConceptoTipoPersonal(
        null);
    }
  }

  public boolean isShowTipoPersonalForConceptoTipoPersonal() { return this.colTipoPersonalForConceptoTipoPersonal != null; }

  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();

    this.conceptoAsociado.setConceptoTipoPersonal(null);

    while (iterator.hasNext())
    {
      Long id = (Long)iterator.next();
      iterator.next();
      if (String.valueOf(id).equals(
        valConceptoTipoPersonal)) {
        try {
          this.conceptoAsociado.setConceptoTipoPersonal(this.definicionesFacade.findConceptoTipoPersonalById(id.longValue()));
        } catch (Exception e) {
          log.error("Excepcion controlada:", e);
        }
      }

    }

    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public boolean isShowConceptoTipoPersonal() {
    return this.colConceptoTipoPersonal != null;
  }
  public String getSelectTipoPersonalForConceptoAsociar() {
    return this.selectTipoPersonalForConceptoAsociar;
  }
  public void setSelectTipoPersonalForConceptoAsociar(String valTipoPersonalForConceptoAsociar) {
    this.selectTipoPersonalForConceptoAsociar = valTipoPersonalForConceptoAsociar;
  }
  public void changeTipoPersonalForConceptoAsociar(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colConceptoAsociar = null;
      if (idTipoPersonal > 0L) {
        this.colConceptoAsociar = 
          this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(
          idTipoPersonal);
      } else {
        this.selectConceptoAsociar = null;
        this.conceptoAsociado.setConceptoAsociar(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectConceptoAsociar = null;
      this.conceptoAsociado.setConceptoAsociar(
        null);
    }
  }

  public boolean isShowTipoPersonalForConceptoAsociar() { return this.colTipoPersonalForConceptoAsociar != null; }

  public String getSelectConceptoAsociar() {
    return this.selectConceptoAsociar;
  }
  public void setSelectConceptoAsociar(String valConceptoAsociar) {
    Iterator iterator = this.colConceptoAsociar.iterator();

    this.conceptoAsociado.setConceptoAsociar(null);

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      iterator.next();
      if (String.valueOf(id).equals(
        valConceptoAsociar)) {
        try {
          this.conceptoAsociado.setConceptoAsociar(this.definicionesFacade.findConceptoTipoPersonalById(id.longValue()));
        } catch (Exception e) {
          log.error("Excepcion controlada:", e);
        }
      }

    }

    this.selectConceptoAsociar = valConceptoAsociar;
  }
  public boolean isShowConceptoAsociar() {
    return this.colConceptoAsociar != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoAsociado getConceptoAsociado() {
    if (this.conceptoAsociado == null) {
      this.conceptoAsociado = new ConceptoAsociado();
    }
    return this.conceptoAsociado;
  }

  public ConceptoAsociadoForm() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    newReportId();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  private void newReportId()
  {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public Collection getColTipoPersonalForConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getColTipoPersonalForConceptoAsociar()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonalForConceptoAsociar.iterator();
    TipoPersonal tipoPersonalForConceptoAsociar = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoAsociar = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoAsociar.getIdTipoPersonal()), 
        tipoPersonalForConceptoAsociar.toString()));
    }
    return col;
  }

  public Collection getColConceptoAsociar()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoAsociar.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListBase()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoAsociado.LISTA_FM.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colTipoPersonalForConceptoAsociar = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConceptoAsociadoByConceptoTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findConceptoAsociadoByConceptoTipoPersonal(Long.valueOf(this.findSelectConceptoTipoPersonal).longValue());
      this.showConceptoAsociadoByConceptoTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoAsociadoByConceptoTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public boolean isShowConceptoAsociadoByConceptoTipoPersonal() {
    return this.showConceptoAsociadoByConceptoTipoPersonal;
  }

  public String selectConceptoAsociado()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConceptoTipoPersonal = null;
    this.selectTipoPersonalForConceptoTipoPersonal = null;

    this.selectConceptoAsociar = null;
    this.selectTipoPersonalForConceptoAsociar = null;

    long idConceptoAsociado = 
      Long.parseLong((String)requestParameterMap.get("idConceptoAsociado"));
    try
    {
      this.conceptoAsociado = 
        this.definicionesFacade.findConceptoAsociadoById(
        idConceptoAsociado);
      if (this.conceptoAsociado.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.conceptoAsociado.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }
      if (this.conceptoAsociado.getConceptoAsociar() != null) {
        this.selectConceptoAsociar = 
          String.valueOf(this.conceptoAsociado.getConceptoAsociar().getIdConceptoTipoPersonal());
      }

      ConceptoTipoPersonal conceptoTipoPersonal = null;
      TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
      ConceptoTipoPersonal conceptoAsociar = null;
      TipoPersonal tipoPersonalForConceptoAsociar = null;

      if (this.conceptoAsociado.getConceptoTipoPersonal() != null) {
        long idConceptoTipoPersonal = 
          this.conceptoAsociado.getConceptoTipoPersonal().getIdConceptoTipoPersonal();
        this.selectConceptoTipoPersonal = String.valueOf(idConceptoTipoPersonal);
        conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(
          idConceptoTipoPersonal);
        this.colConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(
          conceptoTipoPersonal.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonalForConceptoTipoPersonal = 
          this.conceptoAsociado.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal();
        this.selectTipoPersonalForConceptoTipoPersonal = String.valueOf(idTipoPersonalForConceptoTipoPersonal);
        tipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findTipoPersonalById(
          idTipoPersonalForConceptoTipoPersonal);
        this.colTipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findAllTipoPersonal();
      }
      if (this.conceptoAsociado.getConceptoAsociar() != null) {
        long idConceptoAsociar = 
          this.conceptoAsociado.getConceptoAsociar().getIdConceptoTipoPersonal();
        this.selectConceptoAsociar = String.valueOf(idConceptoAsociar);
        conceptoAsociar = this.definicionesFacade.findConceptoTipoPersonalById(
          idConceptoAsociar);
        this.colConceptoAsociar = this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(
          conceptoAsociar.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonalForConceptoAsociar = 
          this.conceptoAsociado.getConceptoAsociar().getTipoPersonal().getIdTipoPersonal();
        this.selectTipoPersonalForConceptoAsociar = String.valueOf(idTipoPersonalForConceptoAsociar);
        tipoPersonalForConceptoAsociar = 
          this.definicionesFacade.findTipoPersonalById(
          idTipoPersonalForConceptoAsociar);
        this.colTipoPersonalForConceptoAsociar = 
          this.definicionesFacade.findAllTipoPersonal();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.conceptoAsociado = null;
    this.showConceptoAsociadoByConceptoTipoPersonal = false;
  }

  public String edit()
  {
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addConceptoAsociado(
          this.conceptoAsociado);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.conceptoAsociado);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateConceptoAsociado(
          this.conceptoAsociado);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.conceptoAsociado);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }

    if ((this.findSelectTipoPersonalForConceptoTipoPersonal != this.selectTipoPersonalForConceptoTipoPersonal) || 
      (this.selectConceptoTipoPersonal != this.findSelectConceptoTipoPersonal))
    {
      if (this.findSelectTipoPersonalForConceptoTipoPersonal != this.selectTipoPersonalForConceptoTipoPersonal)
      {
        this.findSelectTipoPersonalForConceptoTipoPersonal = this.selectTipoPersonalForConceptoTipoPersonal;

        this.findColConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(
          Long.valueOf(this.selectTipoPersonalForConceptoTipoPersonal).longValue());
      }

      this.findSelectConceptoTipoPersonal = this.selectConceptoTipoPersonal;
    }

    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteConceptoAsociado(
        this.conceptoAsociado);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.conceptoAsociado);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;
    this.conceptoAsociado = new ConceptoAsociado();

    this.selectConceptoAsociar = null;

    this.selectTipoPersonalForConceptoAsociar = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoAsociado.setIdConceptoAsociado(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.ConceptoAsociado"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.scrollx = 0;
    this.scrolly = 0;
    resetResult();

    this.conceptoAsociado = new ConceptoAsociado();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }
  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }

  public LoginSession getLogin() {
    return this.login;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
}