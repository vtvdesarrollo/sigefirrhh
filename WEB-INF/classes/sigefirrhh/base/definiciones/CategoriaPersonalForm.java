package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class CategoriaPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CategoriaPersonalForm.class.getName());
  private CategoriaPersonal categoriaPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showCategoriaPersonalByCodCategoria;
  private boolean showCategoriaPersonalByDescCategoria;
  private String findCodCategoria;
  private String findDescCategoria;
  private Object stateResultCategoriaPersonalByCodCategoria = null;

  private Object stateResultCategoriaPersonalByDescCategoria = null;

  public String getFindCodCategoria()
  {
    return this.findCodCategoria;
  }
  public void setFindCodCategoria(String findCodCategoria) {
    this.findCodCategoria = findCodCategoria;
  }
  public String getFindDescCategoria() {
    return this.findDescCategoria;
  }
  public void setFindDescCategoria(String findDescCategoria) {
    this.findDescCategoria = findDescCategoria;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public CategoriaPersonal getCategoriaPersonal() {
    if (this.categoriaPersonal == null) {
      this.categoriaPersonal = new CategoriaPersonal();
    }
    return this.categoriaPersonal;
  }

  public CategoriaPersonalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findCategoriaPersonalByCodCategoria()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findCategoriaPersonalByCodCategoria(this.findCodCategoria);
      this.showCategoriaPersonalByCodCategoria = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCategoriaPersonalByCodCategoria)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodCategoria = null;
    this.findDescCategoria = null;

    return null;
  }

  public String findCategoriaPersonalByDescCategoria()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findCategoriaPersonalByDescCategoria(this.findDescCategoria);
      this.showCategoriaPersonalByDescCategoria = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCategoriaPersonalByDescCategoria)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodCategoria = null;
    this.findDescCategoria = null;

    return null;
  }

  public boolean isShowCategoriaPersonalByCodCategoria() {
    return this.showCategoriaPersonalByCodCategoria;
  }
  public boolean isShowCategoriaPersonalByDescCategoria() {
    return this.showCategoriaPersonalByDescCategoria;
  }

  public String selectCategoriaPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idCategoriaPersonal = 
      Long.parseLong((String)requestParameterMap.get("idCategoriaPersonal"));
    try
    {
      this.categoriaPersonal = 
        this.definicionesFacade.findCategoriaPersonalById(
        idCategoriaPersonal);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.categoriaPersonal = null;
    this.showCategoriaPersonalByCodCategoria = false;
    this.showCategoriaPersonalByDescCategoria = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addCategoriaPersonal(
          this.categoriaPersonal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateCategoriaPersonal(
          this.categoriaPersonal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteCategoriaPersonal(
        this.categoriaPersonal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.categoriaPersonal = new CategoriaPersonal();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.categoriaPersonal.setIdCategoriaPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.CategoriaPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.categoriaPersonal = new CategoriaPersonal();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("CategoriaPersonal");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "CategoriaPersonal" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}