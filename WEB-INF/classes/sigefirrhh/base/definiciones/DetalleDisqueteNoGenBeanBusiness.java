package sigefirrhh.base.definiciones;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DetalleDisqueteNoGenBeanBusiness
  implements Serializable
{
  public int findLastNumeroCampoByDisquete(long idDisquete, String tipoRegistro)
    throws Exception
  {
    Connection connection = null;
    ResultSet rsDetalleDisquete = null;
    PreparedStatement stDetalleDisquete = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      StringBuffer sql = new StringBuffer();
      sql.append("select max(numero_campo) as ultimo from detalledisquete ");
      sql.append(" where id_disquete = ? and tipo_registro = ?");
      stDetalleDisquete = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stDetalleDisquete.setLong(1, idDisquete);
      stDetalleDisquete.setString(2, tipoRegistro);
      rsDetalleDisquete = stDetalleDisquete.executeQuery();
      rsDetalleDisquete.next();
      return rsDetalleDisquete.getInt("ultimo");
    } finally {
      if (rsDetalleDisquete != null) try {
          rsDetalleDisquete.close();
        } catch (Exception localException3) {
        } if (stDetalleDisquete != null) try {
          stDetalleDisquete.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}