package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class RelacionPersonal
  implements Serializable, PersistenceCapable
{
  private long idRelacionPersonal;
  private String codRelacion;
  private String descRelacion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codRelacion", "descRelacion", "idRelacionPersonal" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescRelacion(this);
  }

  public String getCodRelacion()
  {
    return jdoGetcodRelacion(this);
  }

  public String getDescRelacion()
  {
    return jdoGetdescRelacion(this);
  }

  public long getIdRelacionPersonal()
  {
    return jdoGetidRelacionPersonal(this);
  }

  public void setCodRelacion(String string)
  {
    jdoSetcodRelacion(this, string);
  }

  public void setDescRelacion(String string)
  {
    jdoSetdescRelacion(this, string);
  }

  public void setIdRelacionPersonal(long l)
  {
    jdoSetidRelacionPersonal(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.RelacionPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RelacionPersonal());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RelacionPersonal localRelacionPersonal = new RelacionPersonal();
    localRelacionPersonal.jdoFlags = 1;
    localRelacionPersonal.jdoStateManager = paramStateManager;
    return localRelacionPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RelacionPersonal localRelacionPersonal = new RelacionPersonal();
    localRelacionPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRelacionPersonal.jdoFlags = 1;
    localRelacionPersonal.jdoStateManager = paramStateManager;
    return localRelacionPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codRelacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descRelacion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRelacionPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRelacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descRelacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRelacionPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RelacionPersonal paramRelacionPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRelacionPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.codRelacion = paramRelacionPersonal.codRelacion;
      return;
    case 1:
      if (paramRelacionPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.descRelacion = paramRelacionPersonal.descRelacion;
      return;
    case 2:
      if (paramRelacionPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idRelacionPersonal = paramRelacionPersonal.idRelacionPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RelacionPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RelacionPersonal localRelacionPersonal = (RelacionPersonal)paramObject;
    if (localRelacionPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRelacionPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RelacionPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RelacionPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RelacionPersonalPK))
      throw new IllegalArgumentException("arg1");
    RelacionPersonalPK localRelacionPersonalPK = (RelacionPersonalPK)paramObject;
    localRelacionPersonalPK.idRelacionPersonal = this.idRelacionPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RelacionPersonalPK))
      throw new IllegalArgumentException("arg1");
    RelacionPersonalPK localRelacionPersonalPK = (RelacionPersonalPK)paramObject;
    this.idRelacionPersonal = localRelacionPersonalPK.idRelacionPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RelacionPersonalPK))
      throw new IllegalArgumentException("arg2");
    RelacionPersonalPK localRelacionPersonalPK = (RelacionPersonalPK)paramObject;
    localRelacionPersonalPK.idRelacionPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RelacionPersonalPK))
      throw new IllegalArgumentException("arg2");
    RelacionPersonalPK localRelacionPersonalPK = (RelacionPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localRelacionPersonalPK.idRelacionPersonal);
  }

  private static final String jdoGetcodRelacion(RelacionPersonal paramRelacionPersonal)
  {
    if (paramRelacionPersonal.jdoFlags <= 0)
      return paramRelacionPersonal.codRelacion;
    StateManager localStateManager = paramRelacionPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramRelacionPersonal.codRelacion;
    if (localStateManager.isLoaded(paramRelacionPersonal, jdoInheritedFieldCount + 0))
      return paramRelacionPersonal.codRelacion;
    return localStateManager.getStringField(paramRelacionPersonal, jdoInheritedFieldCount + 0, paramRelacionPersonal.codRelacion);
  }

  private static final void jdoSetcodRelacion(RelacionPersonal paramRelacionPersonal, String paramString)
  {
    if (paramRelacionPersonal.jdoFlags == 0)
    {
      paramRelacionPersonal.codRelacion = paramString;
      return;
    }
    StateManager localStateManager = paramRelacionPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramRelacionPersonal.codRelacion = paramString;
      return;
    }
    localStateManager.setStringField(paramRelacionPersonal, jdoInheritedFieldCount + 0, paramRelacionPersonal.codRelacion, paramString);
  }

  private static final String jdoGetdescRelacion(RelacionPersonal paramRelacionPersonal)
  {
    if (paramRelacionPersonal.jdoFlags <= 0)
      return paramRelacionPersonal.descRelacion;
    StateManager localStateManager = paramRelacionPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramRelacionPersonal.descRelacion;
    if (localStateManager.isLoaded(paramRelacionPersonal, jdoInheritedFieldCount + 1))
      return paramRelacionPersonal.descRelacion;
    return localStateManager.getStringField(paramRelacionPersonal, jdoInheritedFieldCount + 1, paramRelacionPersonal.descRelacion);
  }

  private static final void jdoSetdescRelacion(RelacionPersonal paramRelacionPersonal, String paramString)
  {
    if (paramRelacionPersonal.jdoFlags == 0)
    {
      paramRelacionPersonal.descRelacion = paramString;
      return;
    }
    StateManager localStateManager = paramRelacionPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramRelacionPersonal.descRelacion = paramString;
      return;
    }
    localStateManager.setStringField(paramRelacionPersonal, jdoInheritedFieldCount + 1, paramRelacionPersonal.descRelacion, paramString);
  }

  private static final long jdoGetidRelacionPersonal(RelacionPersonal paramRelacionPersonal)
  {
    return paramRelacionPersonal.idRelacionPersonal;
  }

  private static final void jdoSetidRelacionPersonal(RelacionPersonal paramRelacionPersonal, long paramLong)
  {
    StateManager localStateManager = paramRelacionPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramRelacionPersonal.idRelacionPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramRelacionPersonal, jdoInheritedFieldCount + 2, paramRelacionPersonal.idRelacionPersonal, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}