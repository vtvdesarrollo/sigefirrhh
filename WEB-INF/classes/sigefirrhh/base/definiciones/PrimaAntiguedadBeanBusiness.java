package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PrimaAntiguedadBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPrimaAntiguedad(PrimaAntiguedad primaAntiguedad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PrimaAntiguedad primaAntiguedadNew = 
      (PrimaAntiguedad)BeanUtils.cloneBean(
      primaAntiguedad);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (primaAntiguedadNew.getTipoPersonal() != null) {
      primaAntiguedadNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        primaAntiguedadNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(primaAntiguedadNew);
  }

  public void updatePrimaAntiguedad(PrimaAntiguedad primaAntiguedad) throws Exception
  {
    PrimaAntiguedad primaAntiguedadModify = 
      findPrimaAntiguedadById(primaAntiguedad.getIdPrimaAntiguedad());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (primaAntiguedad.getTipoPersonal() != null) {
      primaAntiguedad.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        primaAntiguedad.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(primaAntiguedadModify, primaAntiguedad);
  }

  public void deletePrimaAntiguedad(PrimaAntiguedad primaAntiguedad) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PrimaAntiguedad primaAntiguedadDelete = 
      findPrimaAntiguedadById(primaAntiguedad.getIdPrimaAntiguedad());
    pm.deletePersistent(primaAntiguedadDelete);
  }

  public PrimaAntiguedad findPrimaAntiguedadById(long idPrimaAntiguedad) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPrimaAntiguedad == pIdPrimaAntiguedad";
    Query query = pm.newQuery(PrimaAntiguedad.class, filter);

    query.declareParameters("long pIdPrimaAntiguedad");

    parameters.put("pIdPrimaAntiguedad", new Long(idPrimaAntiguedad));

    Collection colPrimaAntiguedad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPrimaAntiguedad.iterator();
    return (PrimaAntiguedad)iterator.next();
  }

  public Collection findPrimaAntiguedadAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent primaAntiguedadExtent = pm.getExtent(
      PrimaAntiguedad.class, true);
    Query query = pm.newQuery(primaAntiguedadExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(PrimaAntiguedad.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colPrimaAntiguedad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrimaAntiguedad);

    return colPrimaAntiguedad;
  }
}