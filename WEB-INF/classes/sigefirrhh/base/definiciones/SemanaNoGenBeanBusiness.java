package sigefirrhh.base.definiciones;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class SemanaNoGenBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(SemanaNoGenBeanBusiness.class.getName());

  public Semana findByGrupoNominaAndFechaInicio(long idGrupoNomina, Date fechaInicio)
    throws Exception
  {
    this.log.error("FechaInicio" + fechaInicio);
    this.log.error("idGN " + idGrupoNomina);
    PersistenceManager pm = PMThread.getPM();
    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina && fechaInicio == pFechaInicio";

    Query query = pm.newQuery(Semana.class, filter);

    query.declareParameters("long pIdGrupoNomina, java.util.Date pFechaInicio");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pFechaInicio", fechaInicio);

    Collection colSemana = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    this.log.error("size " + colSemana.size());
    Iterator iterator = colSemana.iterator();
    try
    {
      return (Semana)iterator.next();
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No existe el registro para este período semanal");
      throw error;
    }
  }

  public boolean tieneSemana5(int anio, String mes)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "anio == pAnio && mes == pMes && semanaMes == 5";

    Query query = pm.newQuery(Semana.class, filter);

    query.declareParameters("int pAnio, String pMes");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));
    parameters.put("pMes", mes);

    Collection colSemana = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    if (colSemana.size() != 0) {
      return true;
    }
    return false;
  }

  public Collection findByGrupoNominaAndAnio(long idGrupoNomina, int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina && anio == pAnio";

    Query query = pm.newQuery(Semana.class, filter);

    query.declareParameters("long pIdGrupoNomina, int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, semanaAnio ascending");

    Collection colSemana = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSemana);

    return colSemana;
  }

  public Semana findNumeroSemanasMes(long idGrupoNomina, int anio, String mes)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina && anio == pAnio && mes == pMes";

    Query query = pm.newQuery(Semana.class, filter);

    query.declareParameters("long pIdGrupoNomina, int pAnio, String pMes");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pAnio", new Integer(anio));
    parameters.put("pMes", mes);

    query.setOrdering("semanaMes descending");

    Collection colSemana = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSemana.iterator();
    try
    {
      return (Semana)iterator.next();
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No existe el registro para este período semanal");
      throw error;
    }
  }

  public Semana findByAnioMesSemana(long idGrupoNomina, int anio, String mes, int semanaMes) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina && anio == pAnio && mes == pMes && semanaMes == pSemanaMes";

    Query query = pm.newQuery(Semana.class, filter);

    query.declareParameters("long pIdGrupoNomina, int pAnio, String pMes, int pSemanaMes");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pAnio", new Integer(anio));
    parameters.put("pMes", mes);
    parameters.put("pSemanaMes", new Integer(semanaMes));

    query.setOrdering("semanaMes descending");

    Collection colSemana = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSemana.iterator();
    try
    {
      return (Semana)iterator.next();
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No existe el registro para este período semanal");
      throw error;
    }
  }
}