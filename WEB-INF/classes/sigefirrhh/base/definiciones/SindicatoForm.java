package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class SindicatoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SindicatoForm.class.getName());
  private Sindicato sindicato;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showSindicatoByCodSindicato;
  private boolean showSindicatoByNombre;
  private String findCodSindicato;
  private String findNombre;
  private Collection colConcepto;
  private String selectConcepto;
  private Object stateResultSindicatoByCodSindicato = null;

  private Object stateResultSindicatoByNombre = null;

  public String getFindCodSindicato()
  {
    return this.findCodSindicato;
  }
  public void setFindCodSindicato(String findCodSindicato) {
    this.findCodSindicato = findCodSindicato;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectConcepto()
  {
    return this.selectConcepto;
  }
  public void setSelectConcepto(String valConcepto) {
    Iterator iterator = this.colConcepto.iterator();
    Concepto concepto = null;
    this.sindicato.setConcepto(null);
    while (iterator.hasNext()) {
      concepto = (Concepto)iterator.next();
      if (String.valueOf(concepto.getIdConcepto()).equals(
        valConcepto)) {
        this.sindicato.setConcepto(
          concepto);
        break;
      }
    }
    this.selectConcepto = valConcepto;
  }
  public Collection getResult() {
    return this.result;
  }

  public Sindicato getSindicato() {
    if (this.sindicato == null) {
      this.sindicato = new Sindicato();
    }
    return this.sindicato;
  }

  public SindicatoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColConcepto()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConcepto.iterator();
    Concepto concepto = null;
    while (iterator.hasNext()) {
      concepto = (Concepto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concepto.getIdConcepto()), 
        concepto.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colConcepto = 
        this.definicionesFacade.findConceptoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSindicatoByCodSindicato()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findSindicatoByCodSindicato(this.findCodSindicato);
      this.showSindicatoByCodSindicato = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSindicatoByCodSindicato)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodSindicato = null;
    this.findNombre = null;

    return null;
  }

  public String findSindicatoByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findSindicatoByNombre(this.findNombre);
      this.showSindicatoByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSindicatoByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodSindicato = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowSindicatoByCodSindicato() {
    return this.showSindicatoByCodSindicato;
  }
  public boolean isShowSindicatoByNombre() {
    return this.showSindicatoByNombre;
  }

  public String selectSindicato()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConcepto = null;

    long idSindicato = 
      Long.parseLong((String)requestParameterMap.get("idSindicato"));
    try
    {
      this.sindicato = 
        this.definicionesFacade.findSindicatoById(
        idSindicato);
      if (this.sindicato.getConcepto() != null) {
        this.selectConcepto = 
          String.valueOf(this.sindicato.getConcepto().getIdConcepto());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.sindicato = null;
    this.showSindicatoByCodSindicato = false;
    this.showSindicatoByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addSindicato(
          this.sindicato);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateSindicato(
          this.sindicato);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteSindicato(
        this.sindicato);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.sindicato = new Sindicato();

    this.selectConcepto = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.sindicato.setIdSindicato(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.Sindicato"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.sindicato = new Sindicato();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("Sindicato");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "Sindicato" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}