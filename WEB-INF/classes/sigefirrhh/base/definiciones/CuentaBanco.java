package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.ubicacion.Ciudad;

public class CuentaBanco
  implements Serializable, PersistenceCapable
{
  private long idCuentaBanco;
  private Banco banco;
  private String codCuentaBanco;
  private String agencia;
  private Ciudad ciudad;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "agencia", "banco", "ciudad", "codCuentaBanco", "idCuentaBanco", "organismo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 24, 26, 26, 24, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetbanco(this).getNombre() + "  -  " + 
      jdoGetcodCuentaBanco(this);
  }

  public Banco getBanco()
  {
    return jdoGetbanco(this);
  }

  public Ciudad getCiudad()
  {
    return jdoGetciudad(this);
  }

  public String getCodCuentaBanco()
  {
    return jdoGetcodCuentaBanco(this);
  }

  public long getIdCuentaBanco()
  {
    return jdoGetidCuentaBanco(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setBanco(Banco banco)
  {
    jdoSetbanco(this, banco);
  }

  public void setCiudad(Ciudad ciudad)
  {
    jdoSetciudad(this, ciudad);
  }

  public void setCodCuentaBanco(String string)
  {
    jdoSetcodCuentaBanco(this, string);
  }

  public void setIdCuentaBanco(long l)
  {
    jdoSetidCuentaBanco(this, l);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public String getAgencia()
  {
    return jdoGetagencia(this);
  }

  public void setAgencia(String string)
  {
    jdoSetagencia(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.CuentaBanco"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CuentaBanco());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CuentaBanco localCuentaBanco = new CuentaBanco();
    localCuentaBanco.jdoFlags = 1;
    localCuentaBanco.jdoStateManager = paramStateManager;
    return localCuentaBanco;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CuentaBanco localCuentaBanco = new CuentaBanco();
    localCuentaBanco.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCuentaBanco.jdoFlags = 1;
    localCuentaBanco.jdoStateManager = paramStateManager;
    return localCuentaBanco;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.agencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.banco);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCuentaBanco);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCuentaBanco);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.agencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.banco = ((Banco)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCuentaBanco = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCuentaBanco = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CuentaBanco paramCuentaBanco, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCuentaBanco == null)
        throw new IllegalArgumentException("arg1");
      this.agencia = paramCuentaBanco.agencia;
      return;
    case 1:
      if (paramCuentaBanco == null)
        throw new IllegalArgumentException("arg1");
      this.banco = paramCuentaBanco.banco;
      return;
    case 2:
      if (paramCuentaBanco == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramCuentaBanco.ciudad;
      return;
    case 3:
      if (paramCuentaBanco == null)
        throw new IllegalArgumentException("arg1");
      this.codCuentaBanco = paramCuentaBanco.codCuentaBanco;
      return;
    case 4:
      if (paramCuentaBanco == null)
        throw new IllegalArgumentException("arg1");
      this.idCuentaBanco = paramCuentaBanco.idCuentaBanco;
      return;
    case 5:
      if (paramCuentaBanco == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramCuentaBanco.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CuentaBanco))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CuentaBanco localCuentaBanco = (CuentaBanco)paramObject;
    if (localCuentaBanco.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCuentaBanco, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CuentaBancoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CuentaBancoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CuentaBancoPK))
      throw new IllegalArgumentException("arg1");
    CuentaBancoPK localCuentaBancoPK = (CuentaBancoPK)paramObject;
    localCuentaBancoPK.agencia = this.agencia;
    localCuentaBancoPK.codCuentaBanco = this.codCuentaBanco;
    localCuentaBancoPK.idCuentaBanco = this.idCuentaBanco;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CuentaBancoPK))
      throw new IllegalArgumentException("arg1");
    CuentaBancoPK localCuentaBancoPK = (CuentaBancoPK)paramObject;
    this.agencia = localCuentaBancoPK.agencia;
    this.codCuentaBanco = localCuentaBancoPK.codCuentaBanco;
    this.idCuentaBanco = localCuentaBancoPK.idCuentaBanco;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CuentaBancoPK))
      throw new IllegalArgumentException("arg2");
    CuentaBancoPK localCuentaBancoPK = (CuentaBancoPK)paramObject;
    localCuentaBancoPK.agencia = paramObjectIdFieldSupplier.fetchStringField(jdoInheritedFieldCount + 0);
    localCuentaBancoPK.codCuentaBanco = paramObjectIdFieldSupplier.fetchStringField(jdoInheritedFieldCount + 3);
    localCuentaBancoPK.idCuentaBanco = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CuentaBancoPK))
      throw new IllegalArgumentException("arg2");
    CuentaBancoPK localCuentaBancoPK = (CuentaBancoPK)paramObject;
    paramObjectIdFieldConsumer.storeStringField(jdoInheritedFieldCount + 0, localCuentaBancoPK.agencia);
    paramObjectIdFieldConsumer.storeStringField(jdoInheritedFieldCount + 3, localCuentaBancoPK.codCuentaBanco);
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localCuentaBancoPK.idCuentaBanco);
  }

  private static final String jdoGetagencia(CuentaBanco paramCuentaBanco)
  {
    return paramCuentaBanco.agencia;
  }

  private static final void jdoSetagencia(CuentaBanco paramCuentaBanco, String paramString)
  {
    StateManager localStateManager = paramCuentaBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaBanco.agencia = paramString;
      return;
    }
    localStateManager.setStringField(paramCuentaBanco, jdoInheritedFieldCount + 0, paramCuentaBanco.agencia, paramString);
  }

  private static final Banco jdoGetbanco(CuentaBanco paramCuentaBanco)
  {
    StateManager localStateManager = paramCuentaBanco.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaBanco.banco;
    if (localStateManager.isLoaded(paramCuentaBanco, jdoInheritedFieldCount + 1))
      return paramCuentaBanco.banco;
    return (Banco)localStateManager.getObjectField(paramCuentaBanco, jdoInheritedFieldCount + 1, paramCuentaBanco.banco);
  }

  private static final void jdoSetbanco(CuentaBanco paramCuentaBanco, Banco paramBanco)
  {
    StateManager localStateManager = paramCuentaBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaBanco.banco = paramBanco;
      return;
    }
    localStateManager.setObjectField(paramCuentaBanco, jdoInheritedFieldCount + 1, paramCuentaBanco.banco, paramBanco);
  }

  private static final Ciudad jdoGetciudad(CuentaBanco paramCuentaBanco)
  {
    StateManager localStateManager = paramCuentaBanco.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaBanco.ciudad;
    if (localStateManager.isLoaded(paramCuentaBanco, jdoInheritedFieldCount + 2))
      return paramCuentaBanco.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramCuentaBanco, jdoInheritedFieldCount + 2, paramCuentaBanco.ciudad);
  }

  private static final void jdoSetciudad(CuentaBanco paramCuentaBanco, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramCuentaBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaBanco.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramCuentaBanco, jdoInheritedFieldCount + 2, paramCuentaBanco.ciudad, paramCiudad);
  }

  private static final String jdoGetcodCuentaBanco(CuentaBanco paramCuentaBanco)
  {
    return paramCuentaBanco.codCuentaBanco;
  }

  private static final void jdoSetcodCuentaBanco(CuentaBanco paramCuentaBanco, String paramString)
  {
    StateManager localStateManager = paramCuentaBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaBanco.codCuentaBanco = paramString;
      return;
    }
    localStateManager.setStringField(paramCuentaBanco, jdoInheritedFieldCount + 3, paramCuentaBanco.codCuentaBanco, paramString);
  }

  private static final long jdoGetidCuentaBanco(CuentaBanco paramCuentaBanco)
  {
    return paramCuentaBanco.idCuentaBanco;
  }

  private static final void jdoSetidCuentaBanco(CuentaBanco paramCuentaBanco, long paramLong)
  {
    StateManager localStateManager = paramCuentaBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaBanco.idCuentaBanco = paramLong;
      return;
    }
    localStateManager.setLongField(paramCuentaBanco, jdoInheritedFieldCount + 4, paramCuentaBanco.idCuentaBanco, paramLong);
  }

  private static final Organismo jdoGetorganismo(CuentaBanco paramCuentaBanco)
  {
    StateManager localStateManager = paramCuentaBanco.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaBanco.organismo;
    if (localStateManager.isLoaded(paramCuentaBanco, jdoInheritedFieldCount + 5))
      return paramCuentaBanco.organismo;
    return (Organismo)localStateManager.getObjectField(paramCuentaBanco, jdoInheritedFieldCount + 5, paramCuentaBanco.organismo);
  }

  private static final void jdoSetorganismo(CuentaBanco paramCuentaBanco, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramCuentaBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaBanco.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramCuentaBanco, jdoInheritedFieldCount + 5, paramCuentaBanco.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}