package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class BancoPK
  implements Serializable
{
  public long idBanco;

  public BancoPK()
  {
  }

  public BancoPK(long idBanco)
  {
    this.idBanco = idBanco;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((BancoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(BancoPK thatPK)
  {
    return 
      this.idBanco == thatPK.idBanco;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idBanco)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idBanco);
  }
}