package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class VacacionesPorCargoPK
  implements Serializable
{
  public long idVacacionesPorCargo;

  public VacacionesPorCargoPK()
  {
  }

  public VacacionesPorCargoPK(long idVacacionesPorCargo)
  {
    this.idVacacionesPorCargo = idVacacionesPorCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((VacacionesPorCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(VacacionesPorCargoPK thatPK)
  {
    return 
      this.idVacacionesPorCargo == thatPK.idVacacionesPorCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idVacacionesPorCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idVacacionesPorCargo);
  }
}