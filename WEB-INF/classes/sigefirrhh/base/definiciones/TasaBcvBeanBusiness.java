package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.pasivoLaboral.TasaBcv;

public class TasaBcvBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTasaBcv(TasaBcv tasaBcv)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TasaBcv tasaBcvNew = 
      (TasaBcv)BeanUtils.cloneBean(
      tasaBcv);

    pm.makePersistent(tasaBcvNew);
  }

  public void updateTasaBcv(TasaBcv tasaBcv) throws Exception
  {
    TasaBcv tasaBcvModify = 
      findTasaBcvById(tasaBcv.getIdTasaBcv());

    BeanUtils.copyProperties(tasaBcvModify, tasaBcv);
  }

  public void deleteTasaBcv(TasaBcv tasaBcv) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TasaBcv tasaBcvDelete = 
      findTasaBcvById(tasaBcv.getIdTasaBcv());
    pm.deletePersistent(tasaBcvDelete);
  }

  public TasaBcv findTasaBcvById(long idTasaBcv) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTasaBcv == pIdTasaBcv";
    Query query = pm.newQuery(TasaBcv.class, filter);

    query.declareParameters("long pIdTasaBcv");

    parameters.put("pIdTasaBcv", new Long(idTasaBcv));

    Collection colTasaBcv = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTasaBcv.iterator();
    return (TasaBcv)iterator.next();
  }

  public Collection findTasaBcvAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tasaBcvExtent = pm.getExtent(
      TasaBcv.class, true);
    Query query = pm.newQuery(tasaBcvExtent);
    query.setOrdering("mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(TasaBcv.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("mes ascending");

    Collection colTasaBcv = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTasaBcv);

    return colTasaBcv;
  }
}