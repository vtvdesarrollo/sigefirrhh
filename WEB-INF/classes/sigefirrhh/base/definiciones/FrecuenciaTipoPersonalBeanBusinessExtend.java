package sigefirrhh.base.definiciones;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class FrecuenciaTipoPersonalBeanBusinessExtend extends FrecuenciaTipoPersonalBeanBusiness
  implements Serializable
{
  public FrecuenciaTipoPersonal findByTipoPersonal(long idFrecuenciaPago, long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "frecuenciaPago.idFrecuenciaPago == pIdFrecuenciaPago && tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(FrecuenciaTipoPersonal.class, filter);

    query.declareParameters("long pIdFrecuenciaPago, long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdFrecuenciaPago", new Long(idFrecuenciaPago));
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colFrecuenciaTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    FrecuenciaTipoPersonal frecuenciaTipoPersonal = 
      (FrecuenciaTipoPersonal)colFrecuenciaTipoPersonal.iterator().next();

    pm.makeTransient(frecuenciaTipoPersonal);

    return frecuenciaTipoPersonal;
  }

  public FrecuenciaTipoPersonal findByTipoPersonal(int codFrecuenciaPago, long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "frecuenciaPago.codFrecuenciaPago == pCodFrecuenciaPago && tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(FrecuenciaTipoPersonal.class, filter);

    query.declareParameters("int pCodFrecuenciaPago, long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pCodFrecuenciaPago", new Integer(codFrecuenciaPago));
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colFrecuenciaTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    FrecuenciaTipoPersonal frecuenciaTipoPersonal = 
      (FrecuenciaTipoPersonal)colFrecuenciaTipoPersonal.iterator().next();

    pm.makeTransient(frecuenciaTipoPersonal);

    return frecuenciaTipoPersonal;
  }

  public FrecuenciaTipoPersonal findByTipoPersonalNoPersistente(int codFrecuenciaPago, long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "frecuenciaPago.codFrecuenciaPago == pCodFrecuenciaPago && tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(FrecuenciaTipoPersonal.class, filter);

    query.declareParameters("int pCodFrecuenciaPago, long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pCodFrecuenciaPago", new Integer(codFrecuenciaPago));
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colFrecuenciaTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    FrecuenciaTipoPersonal frecuenciaTipoPersonal = 
      (FrecuenciaTipoPersonal)colFrecuenciaTipoPersonal.iterator().next();

    return frecuenciaTipoPersonal;
  }
}