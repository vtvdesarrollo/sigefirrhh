package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CategoriaPresupuestoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCategoriaPresupuesto(CategoriaPresupuesto categoriaPresupuesto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CategoriaPresupuesto categoriaPresupuestoNew = 
      (CategoriaPresupuesto)BeanUtils.cloneBean(
      categoriaPresupuesto);

    pm.makePersistent(categoriaPresupuestoNew);
  }

  public void updateCategoriaPresupuesto(CategoriaPresupuesto categoriaPresupuesto) throws Exception
  {
    CategoriaPresupuesto categoriaPresupuestoModify = 
      findCategoriaPresupuestoById(categoriaPresupuesto.getIdCategoriaPresupuesto());

    BeanUtils.copyProperties(categoriaPresupuestoModify, categoriaPresupuesto);
  }

  public void deleteCategoriaPresupuesto(CategoriaPresupuesto categoriaPresupuesto) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CategoriaPresupuesto categoriaPresupuestoDelete = 
      findCategoriaPresupuestoById(categoriaPresupuesto.getIdCategoriaPresupuesto());
    pm.deletePersistent(categoriaPresupuestoDelete);
  }

  public CategoriaPresupuesto findCategoriaPresupuestoById(long idCategoriaPresupuesto) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCategoriaPresupuesto == pIdCategoriaPresupuesto";
    Query query = pm.newQuery(CategoriaPresupuesto.class, filter);

    query.declareParameters("long pIdCategoriaPresupuesto");

    parameters.put("pIdCategoriaPresupuesto", new Long(idCategoriaPresupuesto));

    Collection colCategoriaPresupuesto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCategoriaPresupuesto.iterator();
    return (CategoriaPresupuesto)iterator.next();
  }

  public Collection findCategoriaPresupuestoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent categoriaPresupuestoExtent = pm.getExtent(
      CategoriaPresupuesto.class, true);
    Query query = pm.newQuery(categoriaPresupuestoExtent);
    query.setOrdering("codCategoria ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodCategoria(String codCategoria)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCategoria == pCodCategoria";

    Query query = pm.newQuery(CategoriaPresupuesto.class, filter);

    query.declareParameters("java.lang.String pCodCategoria");
    HashMap parameters = new HashMap();

    parameters.put("pCodCategoria", new String(codCategoria));

    query.setOrdering("codCategoria ascending");

    Collection colCategoriaPresupuesto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCategoriaPresupuesto);

    return colCategoriaPresupuesto;
  }

  public Collection findByDescCategoria(String descCategoria)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descCategoria.startsWith(pDescCategoria)";

    Query query = pm.newQuery(CategoriaPresupuesto.class, filter);

    query.declareParameters("java.lang.String pDescCategoria");
    HashMap parameters = new HashMap();

    parameters.put("pDescCategoria", new String(descCategoria));

    query.setOrdering("codCategoria ascending");

    Collection colCategoriaPresupuesto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCategoriaPresupuesto);

    return colCategoriaPresupuesto;
  }
}