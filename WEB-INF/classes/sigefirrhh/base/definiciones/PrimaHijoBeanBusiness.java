package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PrimaHijoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPrimaHijo(PrimaHijo primaHijo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PrimaHijo primaHijoNew = 
      (PrimaHijo)BeanUtils.cloneBean(
      primaHijo);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (primaHijoNew.getTipoPersonal() != null) {
      primaHijoNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        primaHijoNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(primaHijoNew);
  }

  public void updatePrimaHijo(PrimaHijo primaHijo) throws Exception
  {
    PrimaHijo primaHijoModify = 
      findPrimaHijoById(primaHijo.getIdPrimaHijo());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (primaHijo.getTipoPersonal() != null) {
      primaHijo.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        primaHijo.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(primaHijoModify, primaHijo);
  }

  public void deletePrimaHijo(PrimaHijo primaHijo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PrimaHijo primaHijoDelete = 
      findPrimaHijoById(primaHijo.getIdPrimaHijo());
    pm.deletePersistent(primaHijoDelete);
  }

  public PrimaHijo findPrimaHijoById(long idPrimaHijo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPrimaHijo == pIdPrimaHijo";
    Query query = pm.newQuery(PrimaHijo.class, filter);

    query.declareParameters("long pIdPrimaHijo");

    parameters.put("pIdPrimaHijo", new Long(idPrimaHijo));

    Collection colPrimaHijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPrimaHijo.iterator();
    return (PrimaHijo)iterator.next();
  }

  public Collection findPrimaHijoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent primaHijoExtent = pm.getExtent(
      PrimaHijo.class, true);
    Query query = pm.newQuery(primaHijoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(PrimaHijo.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colPrimaHijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrimaHijo);

    return colPrimaHijo;
  }
}