package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ConceptoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoForm.class.getName());
  private Concepto concepto;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showConceptoByCodConcepto;
  private boolean showConceptoByDescripcion;
  private String findCodConcepto;
  private String findDescripcion;
  private Collection colConceptoCaja;
  private Collection colConceptoAporte;
  private Collection colConceptoRetroactivo;
  private Collection colConceptoRetroactivoAnterior;
  private Collection colConceptoAusencia;
  private String selectConceptoAporte;
  private String selectConceptoRetroactivo;
  private String selectConceptoRetroactivoAnterior;
  private String selectConceptoCaja;
  private String selectConceptoAusencia;
  private Object stateResultConceptoByCodConcepto = null;

  private Object stateResultConceptoByDescripcion = null;

  public String getFindCodConcepto()
  {
    return this.findCodConcepto;
  }
  public void setFindCodConcepto(String findCodConcepto) {
    this.findCodConcepto = findCodConcepto;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public String getSelectConceptoCaja()
  {
    return this.selectConceptoCaja;
  }

  public void setSelectConceptoCaja(String valConceptoCaja) {
    Iterator iterator = this.colConceptoCaja.iterator();
    Concepto conceptoCaja = null;
    this.concepto.setConceptoCaja(null);
    while (iterator.hasNext()) {
      conceptoCaja = (Concepto)iterator.next();
      if (String.valueOf(conceptoCaja.getIdConcepto()).equals(valConceptoCaja)) {
        this.concepto.setConceptoCaja(conceptoCaja);
      }
    }
    this.selectConceptoCaja = valConceptoCaja;
  }

  public String getSelectConceptoAporte() {
    return this.selectConceptoAporte;
  }
  public void setSelectConceptoAporte(String valConceptoAporte) {
    Iterator iterator = this.colConceptoAporte.iterator();
    Concepto conceptoAporte = null;
    this.concepto.setConceptoAporte(null);
    while (iterator.hasNext()) {
      conceptoAporte = (Concepto)iterator.next();
      if (String.valueOf(conceptoAporte.getIdConcepto()).equals(
        valConceptoAporte)) {
        this.concepto.setConceptoAporte(
          conceptoAporte);
      }
    }
    this.selectConceptoAporte = valConceptoAporte;
  }
  public String getSelectConceptoRetroactivo() {
    return this.selectConceptoRetroactivo;
  }
  public void setSelectConceptoRetroactivo(String valConceptoRetroactivo) {
    Iterator iterator = this.colConceptoRetroactivo.iterator();
    Concepto conceptoRetroactivo = null;
    this.concepto.setConceptoRetroactivo(null);
    while (iterator.hasNext()) {
      conceptoRetroactivo = (Concepto)iterator.next();
      if (String.valueOf(conceptoRetroactivo.getIdConcepto()).equals(
        valConceptoRetroactivo)) {
        this.concepto.setConceptoRetroactivo(
          conceptoRetroactivo);
      }
    }
    this.selectConceptoRetroactivo = valConceptoRetroactivo;
  }
  public String getSelectConceptoAusencia() {
    return this.selectConceptoAusencia;
  }
  public void setSelectConceptoAusencia(String valConceptoAusencia) {
    Iterator iterator = this.colConceptoAusencia.iterator();
    Concepto conceptoAusencia = null;
    this.concepto.setConceptoAusencia(null);
    while (iterator.hasNext()) {
      conceptoAusencia = (Concepto)iterator.next();
      if (String.valueOf(conceptoAusencia.getIdConcepto()).equals(
        valConceptoAusencia)) {
        this.concepto.setConceptoAusencia(
          conceptoAusencia);
      }
    }
    this.selectConceptoAusencia = valConceptoAusencia;
  }

  public String getSelectConceptoRetroactivoAnterior()
  {
    return this.selectConceptoRetroactivoAnterior;
  }
  public void setSelectConceptoRetroactivoAnterior(String valConceptoRetroactivoAnterior) {
    Iterator iterator = this.colConceptoRetroactivoAnterior.iterator();
    Concepto conceptoRetroactivoAnterior = null;
    this.concepto.setConceptoRetroactivoAnterior(null);
    while (iterator.hasNext()) {
      conceptoRetroactivoAnterior = (Concepto)iterator.next();
      if (String.valueOf(conceptoRetroactivoAnterior.getIdConcepto()).equals(
        valConceptoRetroactivoAnterior)) {
        this.concepto.setConceptoRetroactivoAnterior(
          conceptoRetroactivoAnterior);
      }
    }
    this.selectConceptoRetroactivoAnterior = valConceptoRetroactivoAnterior;
  }
  public Collection getResult() {
    return this.result;
  }

  public Concepto getConcepto() {
    if (this.concepto == null) {
      this.concepto = new Concepto();
    }
    return this.concepto;
  }

  public ConceptoForm() throws Exception
  {
    newReportId();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListUnidad()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_TIPO_UNIDAD.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListReservado()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListSueldoIntegral() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListSueldoBasico() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAjusteSueldo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCompensacion() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListPrimasCargo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListPrimasTrabajador() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListGravable() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTipoPrestamo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_TIPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListDeduccionSindicato() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListDeduccionGremio() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListSobretiempo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListBeneficio() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListJubilacion() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListDeduccionCaja()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAportePatronal() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColConceptoAporte()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoAporte.iterator();
    Concepto conceptoAporte = null;
    while (iterator.hasNext()) {
      conceptoAporte = (Concepto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoAporte.getIdConcepto()), 
        conceptoAporte.toString()));
    }
    return col;
  }

  public Collection getListRetroactivo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAusencia() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListRetroactivoAnterior() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColConceptoCaja() {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoCaja.iterator();
    Concepto conceptoCaja = null;
    while (iterator.hasNext()) {
      conceptoCaja = (Concepto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoCaja.getIdConcepto()), 
        conceptoCaja.toString()));
    }
    return col;
  }

  public Collection getColConceptoRetroactivo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoRetroactivo.iterator();
    Concepto conceptoRetroactivo = null;
    while (iterator.hasNext()) {
      conceptoRetroactivo = (Concepto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoRetroactivo.getIdConcepto()), 
        conceptoRetroactivo.toString()));
    }
    return col;
  }

  public Collection getColConceptoAusencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoAusencia.iterator();
    Concepto conceptoAusencia = null;
    while (iterator.hasNext()) {
      conceptoAusencia = (Concepto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoAusencia.getIdConcepto()), 
        conceptoAusencia.toString()));
    }
    return col;
  }

  public Collection getColConceptoRetroactivoAnterior() {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoRetroactivoAnterior.iterator();
    Concepto conceptoRetroactivoAnterior = null;
    while (iterator.hasNext()) {
      conceptoRetroactivoAnterior = (Concepto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoRetroactivoAnterior.getIdConcepto()), 
        conceptoRetroactivoAnterior.toString()));
    }
    return col;
  }

  public Collection getListPrestacionesNr() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListPrestacionesVr() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concepto.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colConceptoCaja = 
        this.definicionesFacade.findConceptoByTipoPrestamo(
        this.login.getOrganismo().getIdOrganismo(), "H");
      this.colConceptoAporte = 
        this.definicionesFacade.findConceptoByTipoPrestamo(
        this.login.getOrganismo().getIdOrganismo(), "A");
      this.colConceptoRetroactivo = 
        this.definicionesFacade.findConceptoByTipoPrestamo(
        this.login.getOrganismo().getIdOrganismo(), "R");

      this.colConceptoRetroactivoAnterior = 
        this.definicionesFacade.findConceptoByTipoPrestamo(
        this.login.getOrganismo().getIdOrganismo(), "R");
      this.colConceptoAusencia = 
        this.definicionesFacade.findConceptoByTipoPrestamo(
        this.login.getOrganismo().getIdOrganismo(), "D");
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConceptoByCodConcepto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findConceptoByCodConcepto(this.findCodConcepto, idOrganismo);
      this.showConceptoByCodConcepto = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoByCodConcepto)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodConcepto = null;
    this.findDescripcion = null;

    return null;
  }

  public String findConceptoByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findConceptoByDescripcion(this.findDescripcion, idOrganismo);
      this.showConceptoByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodConcepto = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowConceptoByCodConcepto() {
    return this.showConceptoByCodConcepto;
  }
  public boolean isShowConceptoByDescripcion() {
    return this.showConceptoByDescripcion;
  }

  public String selectConcepto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConceptoAporte = null;
    this.selectConceptoRetroactivo = null;
    this.selectConceptoRetroactivoAnterior = null;
    this.selectConceptoCaja = null;

    long idConcepto = 
      Long.parseLong((String)requestParameterMap.get("idConcepto"));
    try
    {
      this.concepto = 
        this.definicionesFacade.findConceptoById(
        idConcepto);
      if (this.concepto.getConceptoCaja() != null) {
        this.selectConceptoCaja = 
          String.valueOf(this.concepto.getConceptoCaja().getIdConcepto());
      }
      if (this.concepto.getConceptoAporte() != null) {
        this.selectConceptoAporte = 
          String.valueOf(this.concepto.getConceptoAporte().getIdConcepto());
      }
      if (this.concepto.getConceptoRetroactivo() != null) {
        this.selectConceptoRetroactivo = 
          String.valueOf(this.concepto.getConceptoRetroactivo().getIdConcepto());
      }
      if (this.concepto.getConceptoRetroactivoAnterior() != null) {
        this.selectConceptoRetroactivoAnterior = 
          String.valueOf(this.concepto.getConceptoRetroactivoAnterior().getIdConcepto());
      }
      if (this.concepto.getConceptoAusencia() != null) {
        this.selectConceptoAusencia = 
          String.valueOf(this.concepto.getConceptoAusencia().getIdConcepto());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.concepto = null;
    this.showConceptoByCodConcepto = false;
    this.showConceptoByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.concepto.getTiempoSitp() != null) && 
      (this.concepto.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        log.error("concepto_caja " + this.concepto.getConceptoCaja());
        this.definicionesFacade.addConcepto(
          this.concepto);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      }
      else if (this.concepto != null) {
        log.error("concepto_caja Modificado " + this.concepto.getConceptoCaja());
        this.definicionesFacade.updateConcepto(this.concepto);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Concepto viene en nulo", ""));
      }

      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    refresh();
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteConcepto(
        this.concepto);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
      refresh();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.concepto = new Concepto();

    this.selectConceptoAporte = null;

    this.selectConceptoRetroactivo = null;

    this.selectConceptoCaja = null;
    this.selectConceptoAusencia = null;
    this.selectConceptoRetroactivoAnterior = null;

    this.concepto.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.concepto.setIdConcepto(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.Concepto"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.concepto = new Concepto();
    return "cancel";
  }

  public boolean isShowConceptoAporteAux() {
    try {
      return this.concepto.getAportePatronal().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowConceptoRetroactivoAux()
  {
    try
    {
      return this.concepto.getRetroactivo().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowConceptoAusenciaAux() {
    try {
      return this.concepto.getAusencia().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowConceptoRetroactivoAnteriorAux() {
    try {
      return this.concepto.getRetroactivoAnterior().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowConceptoCajaAux() {
    try {
      return this.concepto.getDeduccionCaja().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public String runReport() {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("Concepto");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "Concepto" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}