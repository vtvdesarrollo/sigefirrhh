package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class PrimaAntiguedadNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public PrimaAntiguedad findForAniosServicio(long idTipoPersonal, int aniosCumple)
    throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && aniosServicio <= pAniosCumple";
    Query query = pm.newQuery(PrimaAntiguedad.class, filter);

    query.declareParameters("long pIdTipoPersonal, int pAniosCumple");

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pAniosCumple", new Integer(aniosCumple));

    query.setOrdering("aniosServicio descending");
    Collection colPrimaAntiguedad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPrimaAntiguedad.iterator();
    return (PrimaAntiguedad)iterator.next();
  }
}