package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ConceptoTipoPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoTipoPersonalForm.class.getName());
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private int reportId;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private DefinicionesNoGenFacade definicionesNoGenFacade = new DefinicionesNoGenFacade();
  private boolean showConceptoTipoPersonalByTipoPersonal;
  private boolean showConceptoTipoPersonalByConcepto;
  private String findSelectTipoPersonal;
  private String findSelectConcepto;
  private Collection findColTipoPersonal;
  private Collection findColConcepto;
  private String codTipoPersonal;
  private int codFrecuenciaPago;
  private String codConcepto;
  private Collection colTipoPersonal;
  private Collection colConcepto;
  private Collection colTipoPersonalForFrecuenciaTipoPersonal;
  private Collection colFrecuenciaTipoPersonal;
  private Collection colContratoColectivo;
  private String selectTipoPersonal;
  private String selectConcepto;
  private String selectTipoPersonalForFrecuenciaTipoPersonal;
  private String selectFrecuenciaTipoPersonal = "1";
  private String selectContratoColectivo;
  private Object stateScrollConceptoTipoPersonalByTipoPersonal = null;
  private Object stateResultConceptoTipoPersonalByTipoPersonal = null;

  private Object stateScrollConceptoTipoPersonalByConcepto = null;
  private Object stateResultConceptoTipoPersonalByConcepto = null;

  public String getFindSelectTipoPersonal()
  {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectConcepto() {
    return this.findSelectConcepto;
  }
  public void setFindSelectConcepto(String valConcepto) {
    this.findSelectConcepto = valConcepto;
  }

  public Collection getFindColConcepto() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConcepto.iterator();
    Concepto concepto = null;
    while (iterator.hasNext()) {
      concepto = (Concepto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concepto.getIdConcepto()), 
        concepto.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.conceptoTipoPersonal.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.conceptoTipoPersonal.setTipoPersonal(
          tipoPersonal);
        this.codTipoPersonal = tipoPersonal.getCodTipoPersonal();
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectContratoColectivo() {
    return this.selectContratoColectivo;
  }
  public void setSelectContratoColectivo(String valContratoColectivo) {
    Iterator iterator = this.colContratoColectivo.iterator();
    ContratoColectivo contratoColectivo = null;
    this.conceptoTipoPersonal.setContratoColectivo(null);
    while (iterator.hasNext()) {
      contratoColectivo = (ContratoColectivo)iterator.next();
      if (String.valueOf(contratoColectivo.getIdContratoColectivo()).equals(
        valContratoColectivo)) {
        this.conceptoTipoPersonal.setContratoColectivo(
          contratoColectivo);

        break;
      }
    }
    this.selectContratoColectivo = valContratoColectivo;
  }
  public String getSelectConcepto() {
    return this.selectConcepto;
  }
  public void setSelectConcepto(String valConcepto) {
    Iterator iterator = this.colConcepto.iterator();
    Concepto concepto = null;
    this.conceptoTipoPersonal.setConcepto(null);
    while (iterator.hasNext()) {
      concepto = (Concepto)iterator.next();
      if (String.valueOf(concepto.getIdConcepto()).equals(
        valConcepto)) {
        this.conceptoTipoPersonal.setConcepto(
          concepto);
        this.codConcepto = concepto.getCodConcepto();
      }
    }
    this.selectConcepto = valConcepto;
  }
  public String getSelectTipoPersonalForFrecuenciaTipoPersonal() {
    return this.selectTipoPersonalForFrecuenciaTipoPersonal;
  }
  public void setSelectTipoPersonalForFrecuenciaTipoPersonal(String valTipoPersonalForFrecuenciaTipoPersonal) {
    this.selectTipoPersonalForFrecuenciaTipoPersonal = valTipoPersonalForFrecuenciaTipoPersonal;
  }

  public boolean isShowMultiplicadorFormula() {
    try {
      return this.conceptoTipoPersonal.getFormulaConcepto().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public void changeTipoPersonalForFrecuenciaTipoPersonal(ValueChangeEvent event)
  {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colFrecuenciaTipoPersonal = null;
      if (idTipoPersonal > 0L) {
        this.colFrecuenciaTipoPersonal = 
          this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(
          idTipoPersonal);
      } else {
        this.selectFrecuenciaTipoPersonal = null;
        this.conceptoTipoPersonal.setFrecuenciaTipoPersonal(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectFrecuenciaTipoPersonal = null;
      this.conceptoTipoPersonal.setFrecuenciaTipoPersonal(
        null);
    }
  }

  public boolean isShowTipoPersonalForFrecuenciaTipoPersonal() { return this.colTipoPersonalForFrecuenciaTipoPersonal != null; }

  public String getSelectFrecuenciaTipoPersonal() {
    return this.selectFrecuenciaTipoPersonal;
  }
  public void setSelectFrecuenciaTipoPersonal(String valFrecuenciaTipoPersonal) {
    Iterator iterator = this.colFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
    this.conceptoTipoPersonal.setFrecuenciaTipoPersonal(null);
    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      if (String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()).equals(
        valFrecuenciaTipoPersonal)) {
        this.conceptoTipoPersonal.setFrecuenciaTipoPersonal(
          frecuenciaTipoPersonal);
        this.codFrecuenciaPago = frecuenciaTipoPersonal.getFrecuenciaPago().getCodFrecuenciaPago();
      }
    }
    this.selectFrecuenciaTipoPersonal = valFrecuenciaTipoPersonal;
  }
  public boolean isShowFrecuenciaTipoPersonal() {
    return this.colFrecuenciaTipoPersonal != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    if (this.conceptoTipoPersonal == null) {
      this.conceptoTipoPersonal = new ConceptoTipoPersonal();
    }
    return this.conceptoTipoPersonal;
  }

  public ConceptoTipoPersonalForm() throws Exception
  {
    newReportId();
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("ConceptoTipoPersonal");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "ConceptoTipoPersonal" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public Collection getColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConcepto()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConcepto.iterator();
    Concepto concepto = null;
    while (iterator.hasNext()) {
      concepto = (Concepto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concepto.getIdConcepto()), 
        concepto.toString()));
    }
    return col;
  }
  public Collection getColContratoColectivo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colContratoColectivo.iterator();
    ContratoColectivo contratoColectivo = null;
    while (iterator.hasNext()) {
      contratoColectivo = (ContratoColectivo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(contratoColectivo.getIdContratoColectivo()), 
        contratoColectivo.toString()));
    }
    return col;
  }
  public Collection getColTipoPersonalForFrecuenciaTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonalForFrecuenciaTipoPersonal.iterator();
    TipoPersonal tipoPersonalForFrecuenciaTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForFrecuenciaTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForFrecuenciaTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForFrecuenciaTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColFrecuenciaTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()), 
        frecuenciaTipoPersonal.toString()));
    }
    return col;
  }
  public Collection getListRecalculo() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoTipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListBaseJubilacion() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoTipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListTipo() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoTipoPersonal.LISTA_TIPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListDistribucion() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoTipoPersonal.LISTA_DISTRIBUCION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAutomaticoIngreso() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoTipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListReflejaMovimiento() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoTipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListAprobacionMpd() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoTipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListFormulaConcepto() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoTipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListOtraMoneda()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoTipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListHomologacion() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoTipoPersonal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColConcepto = 
        this.definicionesFacade.findConceptoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colConcepto = 
        this.definicionesFacade.findConceptoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colTipoPersonalForFrecuenciaTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colContratoColectivo = 
        this.definicionesFacade.findAllContratoColectivo();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConceptoTipoPersonalByTipoPersonalAndConcepto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      if (this.findSelectConcepto.equals("0"))
        this.result = 
          this.definicionesNoGenFacade.findConceptoTipoPersonalByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      else {
        this.result = 
          this.definicionesNoGenFacade.findConceptoTipoPersonalByTipoPersonalAndConcepto(Long.valueOf(this.findSelectTipoPersonal).longValue(), Long.valueOf(this.findSelectConcepto).longValue());
      }
      this.showConceptoTipoPersonalByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoTipoPersonalByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findSelectConcepto = null;

    return null;
  }

  public String findConceptoTipoPersonalByConcepto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findConceptoTipoPersonalByConcepto(Long.valueOf(this.findSelectConcepto).longValue());
      this.showConceptoTipoPersonalByConcepto = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoTipoPersonalByConcepto)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findSelectConcepto = null;

    return null;
  }

  public boolean isShowConceptoTipoPersonalByTipoPersonal() {
    return this.showConceptoTipoPersonalByTipoPersonal;
  }
  public boolean isShowConceptoTipoPersonalByConcepto() {
    return this.showConceptoTipoPersonalByConcepto;
  }

  public String selectConceptoTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectConcepto = null;
    this.selectFrecuenciaTipoPersonal = null;
    this.selectTipoPersonalForFrecuenciaTipoPersonal = null;

    long idConceptoTipoPersonal = 
      Long.parseLong((String)requestParameterMap.get("idConceptoTipoPersonal"));
    try
    {
      this.conceptoTipoPersonal = 
        this.definicionesFacade.findConceptoTipoPersonalById(
        idConceptoTipoPersonal);
      if (this.conceptoTipoPersonal.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.conceptoTipoPersonal.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.conceptoTipoPersonal.getConcepto() != null) {
        this.selectConcepto = 
          String.valueOf(this.conceptoTipoPersonal.getConcepto().getIdConcepto());
      }
      if (this.conceptoTipoPersonal.getFrecuenciaTipoPersonal() != null) {
        this.selectFrecuenciaTipoPersonal = 
          String.valueOf(this.conceptoTipoPersonal.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal());
      }
      if (this.conceptoTipoPersonal.getContratoColectivo() != null) {
        this.selectContratoColectivo = 
          String.valueOf(this.conceptoTipoPersonal.getContratoColectivo().getIdContratoColectivo());
      }
      FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
      TipoPersonal tipoPersonalForFrecuenciaTipoPersonal = null;

      if (this.conceptoTipoPersonal.getFrecuenciaTipoPersonal() != null) {
        long idFrecuenciaTipoPersonal = 
          this.conceptoTipoPersonal.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal();
        this.selectFrecuenciaTipoPersonal = String.valueOf(idFrecuenciaTipoPersonal);
        frecuenciaTipoPersonal = this.definicionesFacade.findFrecuenciaTipoPersonalById(
          idFrecuenciaTipoPersonal);
        this.colFrecuenciaTipoPersonal = this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(
          frecuenciaTipoPersonal.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonalForFrecuenciaTipoPersonal = 
          this.conceptoTipoPersonal.getFrecuenciaTipoPersonal().getTipoPersonal().getIdTipoPersonal();
        this.selectTipoPersonalForFrecuenciaTipoPersonal = String.valueOf(idTipoPersonalForFrecuenciaTipoPersonal);
        tipoPersonalForFrecuenciaTipoPersonal = 
          this.definicionesFacade.findTipoPersonalById(
          idTipoPersonalForFrecuenciaTipoPersonal);
        this.colTipoPersonalForFrecuenciaTipoPersonal = 
          this.definicionesFacade.findAllTipoPersonal();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.conceptoTipoPersonal = null;
    this.showConceptoTipoPersonalByTipoPersonal = false;
    this.showConceptoTipoPersonalByConcepto = false;
  }

  public String edit()
  {
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    try {
      this.conceptoTipoPersonal.setCodTipoPersonal(this.codTipoPersonal);
      this.conceptoTipoPersonal.setCodFrecuenciaPago(this.codFrecuenciaPago);
      this.conceptoTipoPersonal.setCodConcepto(this.codConcepto);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addConceptoTipoPersonal(
          this.conceptoTipoPersonal);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.conceptoTipoPersonal);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateConceptoTipoPersonal(
          this.conceptoTipoPersonal);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.conceptoTipoPersonal);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteConceptoTipoPersonal(
        this.conceptoTipoPersonal);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.conceptoTipoPersonal);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;
    this.conceptoTipoPersonal = new ConceptoTipoPersonal();

    this.selectConcepto = null;

    this.selectFrecuenciaTipoPersonal = null;

    this.selectTipoPersonalForFrecuenciaTipoPersonal = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoTipoPersonal.setIdConceptoTipoPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.ConceptoTipoPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.scrollx = 0;
    this.scrolly = 0;
    resetResult();

    this.conceptoTipoPersonal = new ConceptoTipoPersonal();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }
  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public void setColTipoPersonalForFrecuenciaTipoPersonal(Collection collection)
  {
    this.colTipoPersonalForFrecuenciaTipoPersonal = collection;
  }
}