package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class TipoAcreenciaPK
  implements Serializable
{
  public long idTipoAcreencia;

  public TipoAcreenciaPK()
  {
  }

  public TipoAcreenciaPK(long idTipoAcreencia)
  {
    this.idTipoAcreencia = idTipoAcreencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoAcreenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoAcreenciaPK thatPK)
  {
    return 
      this.idTipoAcreencia == thatPK.idTipoAcreencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoAcreencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoAcreencia);
  }
}