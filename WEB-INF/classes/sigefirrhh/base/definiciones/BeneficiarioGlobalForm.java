package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class BeneficiarioGlobalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(BeneficiarioGlobalForm.class.getName());
  private BeneficiarioGlobal beneficiarioGlobal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showBeneficiarioGlobalByConceptoTipoPersonal;
  private boolean showBeneficiarioGlobalByNombre;
  private String findSelectTipoPersonalForConceptoTipoPersonal;
  private String findSelectConceptoTipoPersonal;
  private String findNombre;
  private Collection findColTipoPersonalForConceptoTipoPersonal;
  private Collection findColConceptoTipoPersonal;
  private Collection colTipoPersonalForConceptoTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private Collection colBanco;
  private String selectTipoPersonalForConceptoTipoPersonal;
  private String selectConceptoTipoPersonal;
  private String selectBanco;
  private Object stateResultBeneficiarioGlobalByConceptoTipoPersonal = null;

  private Object stateResultBeneficiarioGlobalByNombre = null;

  public Collection getFindColTipoPersonalForConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectTipoPersonalForConceptoTipoPersonal() {
    return this.findSelectTipoPersonalForConceptoTipoPersonal;
  }
  public void setFindSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.findSelectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }
  public void findChangeTipoPersonalForConceptoTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColConceptoTipoPersonal = null;
      if (idTipoPersonal > 0L)
        this.findColConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowTipoPersonalForConceptoTipoPersonal() { return this.findColTipoPersonalForConceptoTipoPersonal != null; }

  public String getFindSelectConceptoTipoPersonal() {
    return this.findSelectConceptoTipoPersonal;
  }
  public void setFindSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    this.findSelectConceptoTipoPersonal = valConceptoTipoPersonal;
  }

  public Collection getFindColConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }
  public boolean isFindShowConceptoTipoPersonal() {
    return this.findColConceptoTipoPersonal != null;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectTipoPersonalForConceptoTipoPersonal()
  {
    return this.selectTipoPersonalForConceptoTipoPersonal;
  }
  public void setSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.selectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }
  public void changeTipoPersonalForConceptoTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colConceptoTipoPersonal = null;
      if (idTipoPersonal > 0L) {
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
      } else {
        this.selectConceptoTipoPersonal = null;
        this.beneficiarioGlobal.setConceptoTipoPersonal(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectConceptoTipoPersonal = null;
      this.beneficiarioGlobal.setConceptoTipoPersonal(
        null);
    }
  }

  public boolean isShowTipoPersonalForConceptoTipoPersonal() { return this.colTipoPersonalForConceptoTipoPersonal != null; }

  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    this.beneficiarioGlobal.setConceptoTipoPersonal(null);
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()).equals(
        valConceptoTipoPersonal)) {
        this.beneficiarioGlobal.setConceptoTipoPersonal(
          conceptoTipoPersonal);
        break;
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public boolean isShowConceptoTipoPersonal() {
    return this.colConceptoTipoPersonal != null;
  }
  public String getSelectBanco() {
    return this.selectBanco;
  }
  public void setSelectBanco(String valBanco) {
    Iterator iterator = this.colBanco.iterator();
    Banco banco = null;
    this.beneficiarioGlobal.setBanco(null);
    while (iterator.hasNext()) {
      banco = (Banco)iterator.next();
      if (String.valueOf(banco.getIdBanco()).equals(
        valBanco)) {
        this.beneficiarioGlobal.setBanco(
          banco);
        break;
      }
    }
    this.selectBanco = valBanco;
  }
  public Collection getResult() {
    return this.result;
  }

  public BeneficiarioGlobal getBeneficiarioGlobal() {
    if (this.beneficiarioGlobal == null) {
      this.beneficiarioGlobal = new BeneficiarioGlobal();
    }
    return this.beneficiarioGlobal;
  }

  public BeneficiarioGlobalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonalForConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColBanco()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colBanco.iterator();
    Banco banco = null;
    while (iterator.hasNext()) {
      banco = (Banco)iterator.next();
      col.add(new SelectItem(
        String.valueOf(banco.getIdBanco()), 
        banco.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colBanco = 
        this.definicionesFacade.findAllBanco();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findBeneficiarioGlobalByConceptoTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findBeneficiarioGlobalByConceptoTipoPersonal(Long.valueOf(this.findSelectConceptoTipoPersonal).longValue());
      this.showBeneficiarioGlobalByConceptoTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showBeneficiarioGlobalByConceptoTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonalForConceptoTipoPersonal = null;
    this.findSelectConceptoTipoPersonal = null;
    this.findNombre = null;

    return null;
  }

  public String findBeneficiarioGlobalByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findBeneficiarioGlobalByNombre(this.findNombre);
      this.showBeneficiarioGlobalByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showBeneficiarioGlobalByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonalForConceptoTipoPersonal = null;
    this.findSelectConceptoTipoPersonal = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowBeneficiarioGlobalByConceptoTipoPersonal() {
    return this.showBeneficiarioGlobalByConceptoTipoPersonal;
  }
  public boolean isShowBeneficiarioGlobalByNombre() {
    return this.showBeneficiarioGlobalByNombre;
  }

  public String selectBeneficiarioGlobal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConceptoTipoPersonal = null;
    this.selectTipoPersonalForConceptoTipoPersonal = null;

    this.selectBanco = null;

    long idBeneficiarioGlobal = 
      Long.parseLong((String)requestParameterMap.get("idBeneficiarioGlobal"));
    try
    {
      this.beneficiarioGlobal = 
        this.definicionesFacade.findBeneficiarioGlobalById(
        idBeneficiarioGlobal);
      if (this.beneficiarioGlobal.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.beneficiarioGlobal.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }
      if (this.beneficiarioGlobal.getBanco() != null) {
        this.selectBanco = 
          String.valueOf(this.beneficiarioGlobal.getBanco().getIdBanco());
      }

      ConceptoTipoPersonal conceptoTipoPersonal = null;
      TipoPersonal tipoPersonalForConceptoTipoPersonal = null;

      if (this.beneficiarioGlobal.getConceptoTipoPersonal() != null) {
        long idConceptoTipoPersonal = 
          this.beneficiarioGlobal.getConceptoTipoPersonal().getIdConceptoTipoPersonal();
        this.selectConceptoTipoPersonal = String.valueOf(idConceptoTipoPersonal);
        conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(
          idConceptoTipoPersonal);
        this.colConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          conceptoTipoPersonal.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonalForConceptoTipoPersonal = 
          this.beneficiarioGlobal.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal();
        this.selectTipoPersonalForConceptoTipoPersonal = String.valueOf(idTipoPersonalForConceptoTipoPersonal);
        tipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findTipoPersonalById(
          idTipoPersonalForConceptoTipoPersonal);
        this.colTipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findAllTipoPersonal();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.beneficiarioGlobal = null;
    this.showBeneficiarioGlobalByConceptoTipoPersonal = false;
    this.showBeneficiarioGlobalByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addBeneficiarioGlobal(
          this.beneficiarioGlobal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateBeneficiarioGlobal(
          this.beneficiarioGlobal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteBeneficiarioGlobal(
        this.beneficiarioGlobal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.beneficiarioGlobal = new BeneficiarioGlobal();

    this.selectConceptoTipoPersonal = null;

    this.selectTipoPersonalForConceptoTipoPersonal = null;

    this.selectBanco = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.beneficiarioGlobal.setIdBeneficiarioGlobal(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.BeneficiarioGlobal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.beneficiarioGlobal = new BeneficiarioGlobal();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}