package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class RelacionPersonalPK
  implements Serializable
{
  public long idRelacionPersonal;

  public RelacionPersonalPK()
  {
  }

  public RelacionPersonalPK(long idRelacionPersonal)
  {
    this.idRelacionPersonal = idRelacionPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RelacionPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RelacionPersonalPK thatPK)
  {
    return 
      this.idRelacionPersonal == thatPK.idRelacionPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRelacionPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRelacionPersonal);
  }
}