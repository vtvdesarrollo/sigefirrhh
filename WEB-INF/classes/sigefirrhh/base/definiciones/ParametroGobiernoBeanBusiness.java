package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.base.estructura.GrupoOrganismoBeanBusiness;

public class ParametroGobiernoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParametroGobierno(ParametroGobierno parametroGobierno)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ParametroGobierno parametroGobiernoNew = 
      (ParametroGobierno)BeanUtils.cloneBean(
      parametroGobierno);

    GrupoOrganismoBeanBusiness grupoOrganismoBeanBusiness = new GrupoOrganismoBeanBusiness();

    if (parametroGobiernoNew.getGrupoOrganismo() != null) {
      parametroGobiernoNew.setGrupoOrganismo(
        grupoOrganismoBeanBusiness.findGrupoOrganismoById(
        parametroGobiernoNew.getGrupoOrganismo().getIdGrupoOrganismo()));
    }
    pm.makePersistent(parametroGobiernoNew);
  }

  public void updateParametroGobierno(ParametroGobierno parametroGobierno) throws Exception
  {
    ParametroGobierno parametroGobiernoModify = 
      findParametroGobiernoById(parametroGobierno.getIdParametroGobierno());

    GrupoOrganismoBeanBusiness grupoOrganismoBeanBusiness = new GrupoOrganismoBeanBusiness();

    if (parametroGobierno.getGrupoOrganismo() != null) {
      parametroGobierno.setGrupoOrganismo(
        grupoOrganismoBeanBusiness.findGrupoOrganismoById(
        parametroGobierno.getGrupoOrganismo().getIdGrupoOrganismo()));
    }

    BeanUtils.copyProperties(parametroGobiernoModify, parametroGobierno);
  }

  public void deleteParametroGobierno(ParametroGobierno parametroGobierno) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ParametroGobierno parametroGobiernoDelete = 
      findParametroGobiernoById(parametroGobierno.getIdParametroGobierno());
    pm.deletePersistent(parametroGobiernoDelete);
  }

  public ParametroGobierno findParametroGobiernoById(long idParametroGobierno) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParametroGobierno == pIdParametroGobierno";
    Query query = pm.newQuery(ParametroGobierno.class, filter);

    query.declareParameters("long pIdParametroGobierno");

    parameters.put("pIdParametroGobierno", new Long(idParametroGobierno));

    Collection colParametroGobierno = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParametroGobierno.iterator();
    return (ParametroGobierno)iterator.next();
  }

  public Collection findParametroGobiernoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parametroGobiernoExtent = pm.getExtent(
      ParametroGobierno.class, true);
    Query query = pm.newQuery(parametroGobiernoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByGrupoOrganismo(long idGrupoOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoOrganismo.idGrupoOrganismo == pIdGrupoOrganismo";

    Query query = pm.newQuery(ParametroGobierno.class, filter);

    query.declareParameters("long pIdGrupoOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoOrganismo", new Long(idGrupoOrganismo));

    Collection colParametroGobierno = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParametroGobierno);

    return colParametroGobierno;
  }
}