package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ParametroAri
  implements Serializable, PersistenceCapable
{
  private long idParametroAri;
  private double unidadTributaria;
  private Date fechaVigencia;
  private double unDesgravamenes;
  private double unPersonaNatural;
  private double unCarga;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "fechaVigencia", "idParametroAri", "unCarga", "unDesgravamenes", "unPersonaNatural", "unidadTributaria" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.util.Date"), Long.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ParametroAri()
  {
    jdoSetunidadTributaria(this, 0.0D);

    jdoSetunDesgravamenes(this, 0.0D);

    jdoSetunPersonaNatural(this, 0.0D);

    jdoSetunCarga(this, 0.0D);
  }
  public String toString() {
    return "UT - " + jdoGetunidadTributaria(this) + "  -  Desgravamen -" + 
      jdoGetunDesgravamenes(this) + "  -  Persona Natural -" + 
      jdoGetunPersonaNatural(this) + "  - Carga Familiar -" + 
      jdoGetunCarga(this);
  }

  public long getIdParametroAri()
  {
    return jdoGetidParametroAri(this);
  }

  public double getUnCarga()
  {
    return jdoGetunCarga(this);
  }

  public double getUnDesgravamenes()
  {
    return jdoGetunDesgravamenes(this);
  }

  public double getUnidadTributaria()
  {
    return jdoGetunidadTributaria(this);
  }

  public double getUnPersonaNatural()
  {
    return jdoGetunPersonaNatural(this);
  }

  public void setIdParametroAri(long l)
  {
    jdoSetidParametroAri(this, l);
  }

  public void setUnCarga(double d)
  {
    jdoSetunCarga(this, d);
  }

  public void setUnDesgravamenes(double d)
  {
    jdoSetunDesgravamenes(this, d);
  }

  public void setUnidadTributaria(double d)
  {
    jdoSetunidadTributaria(this, d);
  }

  public void setUnPersonaNatural(double d)
  {
    jdoSetunPersonaNatural(this, d);
  }

  public Date getFechaVigencia()
  {
    return jdoGetfechaVigencia(this);
  }

  public void setFechaVigencia(Date date)
  {
    jdoSetfechaVigencia(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ParametroAri"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroAri());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroAri localParametroAri = new ParametroAri();
    localParametroAri.jdoFlags = 1;
    localParametroAri.jdoStateManager = paramStateManager;
    return localParametroAri;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroAri localParametroAri = new ParametroAri();
    localParametroAri.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroAri.jdoFlags = 1;
    localParametroAri.jdoStateManager = paramStateManager;
    return localParametroAri;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroAri);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unCarga);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unDesgravamenes);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unPersonaNatural);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidadTributaria);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroAri = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unCarga = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unDesgravamenes = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unPersonaNatural = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadTributaria = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroAri paramParametroAri, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroAri == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramParametroAri.fechaVigencia;
      return;
    case 1:
      if (paramParametroAri == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroAri = paramParametroAri.idParametroAri;
      return;
    case 2:
      if (paramParametroAri == null)
        throw new IllegalArgumentException("arg1");
      this.unCarga = paramParametroAri.unCarga;
      return;
    case 3:
      if (paramParametroAri == null)
        throw new IllegalArgumentException("arg1");
      this.unDesgravamenes = paramParametroAri.unDesgravamenes;
      return;
    case 4:
      if (paramParametroAri == null)
        throw new IllegalArgumentException("arg1");
      this.unPersonaNatural = paramParametroAri.unPersonaNatural;
      return;
    case 5:
      if (paramParametroAri == null)
        throw new IllegalArgumentException("arg1");
      this.unidadTributaria = paramParametroAri.unidadTributaria;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroAri))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroAri localParametroAri = (ParametroAri)paramObject;
    if (localParametroAri.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroAri, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroAriPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroAriPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroAriPK))
      throw new IllegalArgumentException("arg1");
    ParametroAriPK localParametroAriPK = (ParametroAriPK)paramObject;
    localParametroAriPK.idParametroAri = this.idParametroAri;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroAriPK))
      throw new IllegalArgumentException("arg1");
    ParametroAriPK localParametroAriPK = (ParametroAriPK)paramObject;
    this.idParametroAri = localParametroAriPK.idParametroAri;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroAriPK))
      throw new IllegalArgumentException("arg2");
    ParametroAriPK localParametroAriPK = (ParametroAriPK)paramObject;
    localParametroAriPK.idParametroAri = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroAriPK))
      throw new IllegalArgumentException("arg2");
    ParametroAriPK localParametroAriPK = (ParametroAriPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localParametroAriPK.idParametroAri);
  }

  private static final Date jdoGetfechaVigencia(ParametroAri paramParametroAri)
  {
    if (paramParametroAri.jdoFlags <= 0)
      return paramParametroAri.fechaVigencia;
    StateManager localStateManager = paramParametroAri.jdoStateManager;
    if (localStateManager == null)
      return paramParametroAri.fechaVigencia;
    if (localStateManager.isLoaded(paramParametroAri, jdoInheritedFieldCount + 0))
      return paramParametroAri.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramParametroAri, jdoInheritedFieldCount + 0, paramParametroAri.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(ParametroAri paramParametroAri, Date paramDate)
  {
    if (paramParametroAri.jdoFlags == 0)
    {
      paramParametroAri.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramParametroAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroAri.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramParametroAri, jdoInheritedFieldCount + 0, paramParametroAri.fechaVigencia, paramDate);
  }

  private static final long jdoGetidParametroAri(ParametroAri paramParametroAri)
  {
    return paramParametroAri.idParametroAri;
  }

  private static final void jdoSetidParametroAri(ParametroAri paramParametroAri, long paramLong)
  {
    StateManager localStateManager = paramParametroAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroAri.idParametroAri = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroAri, jdoInheritedFieldCount + 1, paramParametroAri.idParametroAri, paramLong);
  }

  private static final double jdoGetunCarga(ParametroAri paramParametroAri)
  {
    if (paramParametroAri.jdoFlags <= 0)
      return paramParametroAri.unCarga;
    StateManager localStateManager = paramParametroAri.jdoStateManager;
    if (localStateManager == null)
      return paramParametroAri.unCarga;
    if (localStateManager.isLoaded(paramParametroAri, jdoInheritedFieldCount + 2))
      return paramParametroAri.unCarga;
    return localStateManager.getDoubleField(paramParametroAri, jdoInheritedFieldCount + 2, paramParametroAri.unCarga);
  }

  private static final void jdoSetunCarga(ParametroAri paramParametroAri, double paramDouble)
  {
    if (paramParametroAri.jdoFlags == 0)
    {
      paramParametroAri.unCarga = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroAri.unCarga = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroAri, jdoInheritedFieldCount + 2, paramParametroAri.unCarga, paramDouble);
  }

  private static final double jdoGetunDesgravamenes(ParametroAri paramParametroAri)
  {
    if (paramParametroAri.jdoFlags <= 0)
      return paramParametroAri.unDesgravamenes;
    StateManager localStateManager = paramParametroAri.jdoStateManager;
    if (localStateManager == null)
      return paramParametroAri.unDesgravamenes;
    if (localStateManager.isLoaded(paramParametroAri, jdoInheritedFieldCount + 3))
      return paramParametroAri.unDesgravamenes;
    return localStateManager.getDoubleField(paramParametroAri, jdoInheritedFieldCount + 3, paramParametroAri.unDesgravamenes);
  }

  private static final void jdoSetunDesgravamenes(ParametroAri paramParametroAri, double paramDouble)
  {
    if (paramParametroAri.jdoFlags == 0)
    {
      paramParametroAri.unDesgravamenes = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroAri.unDesgravamenes = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroAri, jdoInheritedFieldCount + 3, paramParametroAri.unDesgravamenes, paramDouble);
  }

  private static final double jdoGetunPersonaNatural(ParametroAri paramParametroAri)
  {
    if (paramParametroAri.jdoFlags <= 0)
      return paramParametroAri.unPersonaNatural;
    StateManager localStateManager = paramParametroAri.jdoStateManager;
    if (localStateManager == null)
      return paramParametroAri.unPersonaNatural;
    if (localStateManager.isLoaded(paramParametroAri, jdoInheritedFieldCount + 4))
      return paramParametroAri.unPersonaNatural;
    return localStateManager.getDoubleField(paramParametroAri, jdoInheritedFieldCount + 4, paramParametroAri.unPersonaNatural);
  }

  private static final void jdoSetunPersonaNatural(ParametroAri paramParametroAri, double paramDouble)
  {
    if (paramParametroAri.jdoFlags == 0)
    {
      paramParametroAri.unPersonaNatural = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroAri.unPersonaNatural = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroAri, jdoInheritedFieldCount + 4, paramParametroAri.unPersonaNatural, paramDouble);
  }

  private static final double jdoGetunidadTributaria(ParametroAri paramParametroAri)
  {
    if (paramParametroAri.jdoFlags <= 0)
      return paramParametroAri.unidadTributaria;
    StateManager localStateManager = paramParametroAri.jdoStateManager;
    if (localStateManager == null)
      return paramParametroAri.unidadTributaria;
    if (localStateManager.isLoaded(paramParametroAri, jdoInheritedFieldCount + 5))
      return paramParametroAri.unidadTributaria;
    return localStateManager.getDoubleField(paramParametroAri, jdoInheritedFieldCount + 5, paramParametroAri.unidadTributaria);
  }

  private static final void jdoSetunidadTributaria(ParametroAri paramParametroAri, double paramDouble)
  {
    if (paramParametroAri.jdoFlags == 0)
    {
      paramParametroAri.unidadTributaria = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroAri.unidadTributaria = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroAri, jdoInheritedFieldCount + 5, paramParametroAri.unidadTributaria, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}