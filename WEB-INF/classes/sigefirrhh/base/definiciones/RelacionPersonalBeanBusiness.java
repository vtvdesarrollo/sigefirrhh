package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class RelacionPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRelacionPersonal(RelacionPersonal relacionPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RelacionPersonal relacionPersonalNew = 
      (RelacionPersonal)BeanUtils.cloneBean(
      relacionPersonal);

    pm.makePersistent(relacionPersonalNew);
  }

  public void updateRelacionPersonal(RelacionPersonal relacionPersonal) throws Exception
  {
    RelacionPersonal relacionPersonalModify = 
      findRelacionPersonalById(relacionPersonal.getIdRelacionPersonal());

    BeanUtils.copyProperties(relacionPersonalModify, relacionPersonal);
  }

  public void deleteRelacionPersonal(RelacionPersonal relacionPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RelacionPersonal relacionPersonalDelete = 
      findRelacionPersonalById(relacionPersonal.getIdRelacionPersonal());
    pm.deletePersistent(relacionPersonalDelete);
  }

  public RelacionPersonal findRelacionPersonalById(long idRelacionPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRelacionPersonal == pIdRelacionPersonal";
    Query query = pm.newQuery(RelacionPersonal.class, filter);

    query.declareParameters("long pIdRelacionPersonal");

    parameters.put("pIdRelacionPersonal", new Long(idRelacionPersonal));

    Collection colRelacionPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRelacionPersonal.iterator();
    return (RelacionPersonal)iterator.next();
  }

  public Collection findRelacionPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent relacionPersonalExtent = pm.getExtent(
      RelacionPersonal.class, true);
    Query query = pm.newQuery(relacionPersonalExtent);
    query.setOrdering("codRelacion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodRelacion(String codRelacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codRelacion == pCodRelacion";

    Query query = pm.newQuery(RelacionPersonal.class, filter);

    query.declareParameters("java.lang.String pCodRelacion");
    HashMap parameters = new HashMap();

    parameters.put("pCodRelacion", new String(codRelacion));

    query.setOrdering("codRelacion ascending");

    Collection colRelacionPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRelacionPersonal);

    return colRelacionPersonal;
  }

  public Collection findByDescRelacion(String descRelacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descRelacion.startsWith(pDescRelacion)";

    Query query = pm.newQuery(RelacionPersonal.class, filter);

    query.declareParameters("java.lang.String pDescRelacion");
    HashMap parameters = new HashMap();

    parameters.put("pDescRelacion", new String(descRelacion));

    query.setOrdering("codRelacion ascending");

    Collection colRelacionPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRelacionPersonal);

    return colRelacionPersonal;
  }
}