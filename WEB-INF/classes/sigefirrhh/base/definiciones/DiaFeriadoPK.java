package sigefirrhh.base.definiciones;

import java.io.Serializable;
import java.util.Date;

public class DiaFeriadoPK
  implements Serializable
{
  public long idDiaFeriado;
  public Date dia;

  public DiaFeriadoPK()
  {
  }

  public DiaFeriadoPK(long idDiaFeriado, Date dia)
  {
    this.idDiaFeriado = idDiaFeriado;
    this.dia = dia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DiaFeriadoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DiaFeriadoPK thatPK)
  {
    if (
      this.idDiaFeriado == thatPK.idDiaFeriado)
    {
      if (this.dia.equals(thatPK.dia)) return true;
    }
    return 
      false;
  }

  public int hashCode()
  {
    return 
      (String.valueOf(this.idDiaFeriado) + 
      this.dia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDiaFeriado) + 
      this.dia;
  }
}