package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ConceptoVacaciones
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO_PAGO;
  protected static final Map LISTA_SINO;
  private long idConceptoVacaciones;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private String tipo;
  private String mesCerrado;
  private String mes30;
  private int numeroMeses;
  private double factor;
  private double topeMonto;
  private double topeUnidades;
  private String alicuotaVacacional;
  private ConceptoTipoPersonal conceptoAlicuota;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "alicuotaVacacional", "conceptoAlicuota", "conceptoTipoPersonal", "factor", "idConceptoVacaciones", "idSitp", "mes30", "mesCerrado", "numeroMeses", "tiempoSitp", "tipo", "tipoPersonal", "topeMonto", "topeUnidades" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Double.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Double.TYPE, Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 26, 26, 21, 24, 21, 21, 21, 21, 21, 21, 26, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoVacaciones"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoVacaciones());

    LISTA_TIPO_PAGO = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_TIPO_PAGO.put("B", "BASICO");
    LISTA_TIPO_PAGO.put("P", "PROMEDIO");
    LISTA_TIPO_PAGO.put("D", "DEVENGADO");
    LISTA_TIPO_PAGO.put("Y", "PROYECTADO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public ConceptoVacaciones()
  {
    jdoSettipo(this, "B");

    jdoSetmesCerrado(this, "S");

    jdoSetmes30(this, "S");

    jdoSetnumeroMeses(this, 0);

    jdoSetfactor(this, 1.0D);

    jdoSettopeMonto(this, 0.0D);

    jdoSettopeUnidades(this, 0.0D);

    jdoSetalicuotaVacacional(this, "N");

    jdoSetidSitp(this, 0);
  }

  public String toString()
  {
    return jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + jdoGettipo(this);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public double getFactor()
  {
    return jdoGetfactor(this);
  }

  public long getIdConceptoVacaciones()
  {
    return jdoGetidConceptoVacaciones(this);
  }

  public int getNumeroMeses()
  {
    return jdoGetnumeroMeses(this);
  }

  public String getTipo()
  {
    return jdoGettipo(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setFactor(double d)
  {
    jdoSetfactor(this, d);
  }

  public void setIdConceptoVacaciones(long l)
  {
    jdoSetidConceptoVacaciones(this, l);
  }

  public void setNumeroMeses(int i)
  {
    jdoSetnumeroMeses(this, i);
  }

  public void setTipo(String string)
  {
    jdoSettipo(this, string);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public String getMesCerrado()
  {
    return jdoGetmesCerrado(this);
  }

  public void setMesCerrado(String string)
  {
    jdoSetmesCerrado(this, string);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public String getMes30()
  {
    return jdoGetmes30(this);
  }

  public double getTopeMonto()
  {
    return jdoGettopeMonto(this);
  }

  public double getTopeUnidades()
  {
    return jdoGettopeUnidades(this);
  }

  public void setMes30(String string)
  {
    jdoSetmes30(this, string);
  }

  public void setTopeMonto(double d)
  {
    jdoSettopeMonto(this, d);
  }

  public void setTopeUnidades(double d)
  {
    jdoSettopeUnidades(this, d);
  }

  public String getAlicuotaVacacional()
  {
    return jdoGetalicuotaVacacional(this);
  }

  public ConceptoTipoPersonal getConceptoAlicuota()
  {
    return jdoGetconceptoAlicuota(this);
  }

  public void setAlicuotaVacacional(String string)
  {
    jdoSetalicuotaVacacional(this, string);
  }

  public void setConceptoAlicuota(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoAlicuota(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoVacaciones localConceptoVacaciones = new ConceptoVacaciones();
    localConceptoVacaciones.jdoFlags = 1;
    localConceptoVacaciones.jdoStateManager = paramStateManager;
    return localConceptoVacaciones;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoVacaciones localConceptoVacaciones = new ConceptoVacaciones();
    localConceptoVacaciones.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoVacaciones.jdoFlags = 1;
    localConceptoVacaciones.jdoStateManager = paramStateManager;
    return localConceptoVacaciones;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.alicuotaVacacional);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoAlicuota);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.factor);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoVacaciones);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mes30);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mesCerrado);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroMeses);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.topeMonto);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.topeUnidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alicuotaVacacional = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoAlicuota = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.factor = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoVacaciones = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes30 = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesCerrado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMeses = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.topeMonto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.topeUnidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoVacaciones paramConceptoVacaciones, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.alicuotaVacacional = paramConceptoVacaciones.alicuotaVacacional;
      return;
    case 1:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoAlicuota = paramConceptoVacaciones.conceptoAlicuota;
      return;
    case 2:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoVacaciones.conceptoTipoPersonal;
      return;
    case 3:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.factor = paramConceptoVacaciones.factor;
      return;
    case 4:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoVacaciones = paramConceptoVacaciones.idConceptoVacaciones;
      return;
    case 5:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramConceptoVacaciones.idSitp;
      return;
    case 6:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.mes30 = paramConceptoVacaciones.mes30;
      return;
    case 7:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.mesCerrado = paramConceptoVacaciones.mesCerrado;
      return;
    case 8:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMeses = paramConceptoVacaciones.numeroMeses;
      return;
    case 9:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramConceptoVacaciones.tiempoSitp;
      return;
    case 10:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramConceptoVacaciones.tipo;
      return;
    case 11:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramConceptoVacaciones.tipoPersonal;
      return;
    case 12:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.topeMonto = paramConceptoVacaciones.topeMonto;
      return;
    case 13:
      if (paramConceptoVacaciones == null)
        throw new IllegalArgumentException("arg1");
      this.topeUnidades = paramConceptoVacaciones.topeUnidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoVacaciones))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoVacaciones localConceptoVacaciones = (ConceptoVacaciones)paramObject;
    if (localConceptoVacaciones.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoVacaciones, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoVacacionesPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoVacacionesPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoVacacionesPK))
      throw new IllegalArgumentException("arg1");
    ConceptoVacacionesPK localConceptoVacacionesPK = (ConceptoVacacionesPK)paramObject;
    localConceptoVacacionesPK.idConceptoVacaciones = this.idConceptoVacaciones;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoVacacionesPK))
      throw new IllegalArgumentException("arg1");
    ConceptoVacacionesPK localConceptoVacacionesPK = (ConceptoVacacionesPK)paramObject;
    this.idConceptoVacaciones = localConceptoVacacionesPK.idConceptoVacaciones;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoVacacionesPK))
      throw new IllegalArgumentException("arg2");
    ConceptoVacacionesPK localConceptoVacacionesPK = (ConceptoVacacionesPK)paramObject;
    localConceptoVacacionesPK.idConceptoVacaciones = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoVacacionesPK))
      throw new IllegalArgumentException("arg2");
    ConceptoVacacionesPK localConceptoVacacionesPK = (ConceptoVacacionesPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localConceptoVacacionesPK.idConceptoVacaciones);
  }

  private static final String jdoGetalicuotaVacacional(ConceptoVacaciones paramConceptoVacaciones)
  {
    if (paramConceptoVacaciones.jdoFlags <= 0)
      return paramConceptoVacaciones.alicuotaVacacional;
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.alicuotaVacacional;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 0))
      return paramConceptoVacaciones.alicuotaVacacional;
    return localStateManager.getStringField(paramConceptoVacaciones, jdoInheritedFieldCount + 0, paramConceptoVacaciones.alicuotaVacacional);
  }

  private static final void jdoSetalicuotaVacacional(ConceptoVacaciones paramConceptoVacaciones, String paramString)
  {
    if (paramConceptoVacaciones.jdoFlags == 0)
    {
      paramConceptoVacaciones.alicuotaVacacional = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.alicuotaVacacional = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoVacaciones, jdoInheritedFieldCount + 0, paramConceptoVacaciones.alicuotaVacacional, paramString);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoAlicuota(ConceptoVacaciones paramConceptoVacaciones)
  {
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.conceptoAlicuota;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 1))
      return paramConceptoVacaciones.conceptoAlicuota;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoVacaciones, jdoInheritedFieldCount + 1, paramConceptoVacaciones.conceptoAlicuota);
  }

  private static final void jdoSetconceptoAlicuota(ConceptoVacaciones paramConceptoVacaciones, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.conceptoAlicuota = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoVacaciones, jdoInheritedFieldCount + 1, paramConceptoVacaciones.conceptoAlicuota, paramConceptoTipoPersonal);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoVacaciones paramConceptoVacaciones)
  {
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 2))
      return paramConceptoVacaciones.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoVacaciones, jdoInheritedFieldCount + 2, paramConceptoVacaciones.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoVacaciones paramConceptoVacaciones, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoVacaciones, jdoInheritedFieldCount + 2, paramConceptoVacaciones.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final double jdoGetfactor(ConceptoVacaciones paramConceptoVacaciones)
  {
    if (paramConceptoVacaciones.jdoFlags <= 0)
      return paramConceptoVacaciones.factor;
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.factor;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 3))
      return paramConceptoVacaciones.factor;
    return localStateManager.getDoubleField(paramConceptoVacaciones, jdoInheritedFieldCount + 3, paramConceptoVacaciones.factor);
  }

  private static final void jdoSetfactor(ConceptoVacaciones paramConceptoVacaciones, double paramDouble)
  {
    if (paramConceptoVacaciones.jdoFlags == 0)
    {
      paramConceptoVacaciones.factor = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.factor = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoVacaciones, jdoInheritedFieldCount + 3, paramConceptoVacaciones.factor, paramDouble);
  }

  private static final long jdoGetidConceptoVacaciones(ConceptoVacaciones paramConceptoVacaciones)
  {
    return paramConceptoVacaciones.idConceptoVacaciones;
  }

  private static final void jdoSetidConceptoVacaciones(ConceptoVacaciones paramConceptoVacaciones, long paramLong)
  {
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.idConceptoVacaciones = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoVacaciones, jdoInheritedFieldCount + 4, paramConceptoVacaciones.idConceptoVacaciones, paramLong);
  }

  private static final int jdoGetidSitp(ConceptoVacaciones paramConceptoVacaciones)
  {
    if (paramConceptoVacaciones.jdoFlags <= 0)
      return paramConceptoVacaciones.idSitp;
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.idSitp;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 5))
      return paramConceptoVacaciones.idSitp;
    return localStateManager.getIntField(paramConceptoVacaciones, jdoInheritedFieldCount + 5, paramConceptoVacaciones.idSitp);
  }

  private static final void jdoSetidSitp(ConceptoVacaciones paramConceptoVacaciones, int paramInt)
  {
    if (paramConceptoVacaciones.jdoFlags == 0)
    {
      paramConceptoVacaciones.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoVacaciones, jdoInheritedFieldCount + 5, paramConceptoVacaciones.idSitp, paramInt);
  }

  private static final String jdoGetmes30(ConceptoVacaciones paramConceptoVacaciones)
  {
    if (paramConceptoVacaciones.jdoFlags <= 0)
      return paramConceptoVacaciones.mes30;
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.mes30;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 6))
      return paramConceptoVacaciones.mes30;
    return localStateManager.getStringField(paramConceptoVacaciones, jdoInheritedFieldCount + 6, paramConceptoVacaciones.mes30);
  }

  private static final void jdoSetmes30(ConceptoVacaciones paramConceptoVacaciones, String paramString)
  {
    if (paramConceptoVacaciones.jdoFlags == 0)
    {
      paramConceptoVacaciones.mes30 = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.mes30 = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoVacaciones, jdoInheritedFieldCount + 6, paramConceptoVacaciones.mes30, paramString);
  }

  private static final String jdoGetmesCerrado(ConceptoVacaciones paramConceptoVacaciones)
  {
    if (paramConceptoVacaciones.jdoFlags <= 0)
      return paramConceptoVacaciones.mesCerrado;
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.mesCerrado;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 7))
      return paramConceptoVacaciones.mesCerrado;
    return localStateManager.getStringField(paramConceptoVacaciones, jdoInheritedFieldCount + 7, paramConceptoVacaciones.mesCerrado);
  }

  private static final void jdoSetmesCerrado(ConceptoVacaciones paramConceptoVacaciones, String paramString)
  {
    if (paramConceptoVacaciones.jdoFlags == 0)
    {
      paramConceptoVacaciones.mesCerrado = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.mesCerrado = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoVacaciones, jdoInheritedFieldCount + 7, paramConceptoVacaciones.mesCerrado, paramString);
  }

  private static final int jdoGetnumeroMeses(ConceptoVacaciones paramConceptoVacaciones)
  {
    if (paramConceptoVacaciones.jdoFlags <= 0)
      return paramConceptoVacaciones.numeroMeses;
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.numeroMeses;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 8))
      return paramConceptoVacaciones.numeroMeses;
    return localStateManager.getIntField(paramConceptoVacaciones, jdoInheritedFieldCount + 8, paramConceptoVacaciones.numeroMeses);
  }

  private static final void jdoSetnumeroMeses(ConceptoVacaciones paramConceptoVacaciones, int paramInt)
  {
    if (paramConceptoVacaciones.jdoFlags == 0)
    {
      paramConceptoVacaciones.numeroMeses = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.numeroMeses = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoVacaciones, jdoInheritedFieldCount + 8, paramConceptoVacaciones.numeroMeses, paramInt);
  }

  private static final Date jdoGettiempoSitp(ConceptoVacaciones paramConceptoVacaciones)
  {
    if (paramConceptoVacaciones.jdoFlags <= 0)
      return paramConceptoVacaciones.tiempoSitp;
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.tiempoSitp;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 9))
      return paramConceptoVacaciones.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramConceptoVacaciones, jdoInheritedFieldCount + 9, paramConceptoVacaciones.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ConceptoVacaciones paramConceptoVacaciones, Date paramDate)
  {
    if (paramConceptoVacaciones.jdoFlags == 0)
    {
      paramConceptoVacaciones.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoVacaciones, jdoInheritedFieldCount + 9, paramConceptoVacaciones.tiempoSitp, paramDate);
  }

  private static final String jdoGettipo(ConceptoVacaciones paramConceptoVacaciones)
  {
    if (paramConceptoVacaciones.jdoFlags <= 0)
      return paramConceptoVacaciones.tipo;
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.tipo;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 10))
      return paramConceptoVacaciones.tipo;
    return localStateManager.getStringField(paramConceptoVacaciones, jdoInheritedFieldCount + 10, paramConceptoVacaciones.tipo);
  }

  private static final void jdoSettipo(ConceptoVacaciones paramConceptoVacaciones, String paramString)
  {
    if (paramConceptoVacaciones.jdoFlags == 0)
    {
      paramConceptoVacaciones.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoVacaciones, jdoInheritedFieldCount + 10, paramConceptoVacaciones.tipo, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(ConceptoVacaciones paramConceptoVacaciones)
  {
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.tipoPersonal;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 11))
      return paramConceptoVacaciones.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramConceptoVacaciones, jdoInheritedFieldCount + 11, paramConceptoVacaciones.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ConceptoVacaciones paramConceptoVacaciones, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoVacaciones, jdoInheritedFieldCount + 11, paramConceptoVacaciones.tipoPersonal, paramTipoPersonal);
  }

  private static final double jdoGettopeMonto(ConceptoVacaciones paramConceptoVacaciones)
  {
    if (paramConceptoVacaciones.jdoFlags <= 0)
      return paramConceptoVacaciones.topeMonto;
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.topeMonto;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 12))
      return paramConceptoVacaciones.topeMonto;
    return localStateManager.getDoubleField(paramConceptoVacaciones, jdoInheritedFieldCount + 12, paramConceptoVacaciones.topeMonto);
  }

  private static final void jdoSettopeMonto(ConceptoVacaciones paramConceptoVacaciones, double paramDouble)
  {
    if (paramConceptoVacaciones.jdoFlags == 0)
    {
      paramConceptoVacaciones.topeMonto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.topeMonto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoVacaciones, jdoInheritedFieldCount + 12, paramConceptoVacaciones.topeMonto, paramDouble);
  }

  private static final double jdoGettopeUnidades(ConceptoVacaciones paramConceptoVacaciones)
  {
    if (paramConceptoVacaciones.jdoFlags <= 0)
      return paramConceptoVacaciones.topeUnidades;
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVacaciones.topeUnidades;
    if (localStateManager.isLoaded(paramConceptoVacaciones, jdoInheritedFieldCount + 13))
      return paramConceptoVacaciones.topeUnidades;
    return localStateManager.getDoubleField(paramConceptoVacaciones, jdoInheritedFieldCount + 13, paramConceptoVacaciones.topeUnidades);
  }

  private static final void jdoSettopeUnidades(ConceptoVacaciones paramConceptoVacaciones, double paramDouble)
  {
    if (paramConceptoVacaciones.jdoFlags == 0)
    {
      paramConceptoVacaciones.topeUnidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoVacaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVacaciones.topeUnidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoVacaciones, jdoInheritedFieldCount + 13, paramConceptoVacaciones.topeUnidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}