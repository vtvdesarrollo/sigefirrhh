package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class TipoPersonalPK
  implements Serializable
{
  public long idTipoPersonal;

  public TipoPersonalPK()
  {
  }

  public TipoPersonalPK(long idTipoPersonal)
  {
    this.idTipoPersonal = idTipoPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoPersonalPK thatPK)
  {
    return 
      this.idTipoPersonal == thatPK.idTipoPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoPersonal);
  }
}