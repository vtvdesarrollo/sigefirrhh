package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class VacacionesPorAnio
  implements Serializable, PersistenceCapable
{
  private long idVacacionesPorAnio;
  private TipoPersonal tipoPersonal;
  private int aniosServicio;
  private int diasDisfrutar;
  private double diasBono;
  private double diasExtra;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aniosServicio", "diasBono", "diasDisfrutar", "diasExtra", "idVacacionesPorAnio", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Double.TYPE, Integer.TYPE, Double.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public VacacionesPorAnio()
  {
    jdoSetaniosServicio(this, 99);

    jdoSetdiasDisfrutar(this, 15);

    jdoSetdiasBono(this, 40.0D);

    jdoSetdiasExtra(this, 0.0D);
  }
  public String toString() {
    return jdoGettipoPersonal(this).getNombre() + " - " + 
      jdoGetaniosServicio(this) + " - " + 
      jdoGetdiasDisfrutar(this) + " - " + 
      jdoGetdiasBono(this);
  }

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }

  public double getDiasBono()
  {
    return jdoGetdiasBono(this);
  }

  public int getDiasDisfrutar()
  {
    return jdoGetdiasDisfrutar(this);
  }

  public double getDiasExtra()
  {
    return jdoGetdiasExtra(this);
  }

  public long getIdVacacionesPorAnio()
  {
    return jdoGetidVacacionesPorAnio(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setAniosServicio(int i)
  {
    jdoSetaniosServicio(this, i);
  }

  public void setDiasBono(double d)
  {
    jdoSetdiasBono(this, d);
  }

  public void setDiasDisfrutar(int i)
  {
    jdoSetdiasDisfrutar(this, i);
  }

  public void setDiasExtra(double d)
  {
    jdoSetdiasExtra(this, d);
  }

  public void setIdVacacionesPorAnio(long l)
  {
    jdoSetidVacacionesPorAnio(this, l);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.VacacionesPorAnio"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new VacacionesPorAnio());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    VacacionesPorAnio localVacacionesPorAnio = new VacacionesPorAnio();
    localVacacionesPorAnio.jdoFlags = 1;
    localVacacionesPorAnio.jdoStateManager = paramStateManager;
    return localVacacionesPorAnio;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    VacacionesPorAnio localVacacionesPorAnio = new VacacionesPorAnio();
    localVacacionesPorAnio.jdoCopyKeyFieldsFromObjectId(paramObject);
    localVacacionesPorAnio.jdoFlags = 1;
    localVacacionesPorAnio.jdoStateManager = paramStateManager;
    return localVacacionesPorAnio;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasBono);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasDisfrutar);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasExtra);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idVacacionesPorAnio);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasBono = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasDisfrutar = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasExtra = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idVacacionesPorAnio = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(VacacionesPorAnio paramVacacionesPorAnio, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramVacacionesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramVacacionesPorAnio.aniosServicio;
      return;
    case 1:
      if (paramVacacionesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.diasBono = paramVacacionesPorAnio.diasBono;
      return;
    case 2:
      if (paramVacacionesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.diasDisfrutar = paramVacacionesPorAnio.diasDisfrutar;
      return;
    case 3:
      if (paramVacacionesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.diasExtra = paramVacacionesPorAnio.diasExtra;
      return;
    case 4:
      if (paramVacacionesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.idVacacionesPorAnio = paramVacacionesPorAnio.idVacacionesPorAnio;
      return;
    case 5:
      if (paramVacacionesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramVacacionesPorAnio.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof VacacionesPorAnio))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    VacacionesPorAnio localVacacionesPorAnio = (VacacionesPorAnio)paramObject;
    if (localVacacionesPorAnio.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localVacacionesPorAnio, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new VacacionesPorAnioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new VacacionesPorAnioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VacacionesPorAnioPK))
      throw new IllegalArgumentException("arg1");
    VacacionesPorAnioPK localVacacionesPorAnioPK = (VacacionesPorAnioPK)paramObject;
    localVacacionesPorAnioPK.idVacacionesPorAnio = this.idVacacionesPorAnio;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VacacionesPorAnioPK))
      throw new IllegalArgumentException("arg1");
    VacacionesPorAnioPK localVacacionesPorAnioPK = (VacacionesPorAnioPK)paramObject;
    this.idVacacionesPorAnio = localVacacionesPorAnioPK.idVacacionesPorAnio;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VacacionesPorAnioPK))
      throw new IllegalArgumentException("arg2");
    VacacionesPorAnioPK localVacacionesPorAnioPK = (VacacionesPorAnioPK)paramObject;
    localVacacionesPorAnioPK.idVacacionesPorAnio = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VacacionesPorAnioPK))
      throw new IllegalArgumentException("arg2");
    VacacionesPorAnioPK localVacacionesPorAnioPK = (VacacionesPorAnioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localVacacionesPorAnioPK.idVacacionesPorAnio);
  }

  private static final int jdoGetaniosServicio(VacacionesPorAnio paramVacacionesPorAnio)
  {
    if (paramVacacionesPorAnio.jdoFlags <= 0)
      return paramVacacionesPorAnio.aniosServicio;
    StateManager localStateManager = paramVacacionesPorAnio.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionesPorAnio.aniosServicio;
    if (localStateManager.isLoaded(paramVacacionesPorAnio, jdoInheritedFieldCount + 0))
      return paramVacacionesPorAnio.aniosServicio;
    return localStateManager.getIntField(paramVacacionesPorAnio, jdoInheritedFieldCount + 0, paramVacacionesPorAnio.aniosServicio);
  }

  private static final void jdoSetaniosServicio(VacacionesPorAnio paramVacacionesPorAnio, int paramInt)
  {
    if (paramVacacionesPorAnio.jdoFlags == 0)
    {
      paramVacacionesPorAnio.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacionesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorAnio.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacionesPorAnio, jdoInheritedFieldCount + 0, paramVacacionesPorAnio.aniosServicio, paramInt);
  }

  private static final double jdoGetdiasBono(VacacionesPorAnio paramVacacionesPorAnio)
  {
    if (paramVacacionesPorAnio.jdoFlags <= 0)
      return paramVacacionesPorAnio.diasBono;
    StateManager localStateManager = paramVacacionesPorAnio.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionesPorAnio.diasBono;
    if (localStateManager.isLoaded(paramVacacionesPorAnio, jdoInheritedFieldCount + 1))
      return paramVacacionesPorAnio.diasBono;
    return localStateManager.getDoubleField(paramVacacionesPorAnio, jdoInheritedFieldCount + 1, paramVacacionesPorAnio.diasBono);
  }

  private static final void jdoSetdiasBono(VacacionesPorAnio paramVacacionesPorAnio, double paramDouble)
  {
    if (paramVacacionesPorAnio.jdoFlags == 0)
    {
      paramVacacionesPorAnio.diasBono = paramDouble;
      return;
    }
    StateManager localStateManager = paramVacacionesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorAnio.diasBono = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramVacacionesPorAnio, jdoInheritedFieldCount + 1, paramVacacionesPorAnio.diasBono, paramDouble);
  }

  private static final int jdoGetdiasDisfrutar(VacacionesPorAnio paramVacacionesPorAnio)
  {
    if (paramVacacionesPorAnio.jdoFlags <= 0)
      return paramVacacionesPorAnio.diasDisfrutar;
    StateManager localStateManager = paramVacacionesPorAnio.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionesPorAnio.diasDisfrutar;
    if (localStateManager.isLoaded(paramVacacionesPorAnio, jdoInheritedFieldCount + 2))
      return paramVacacionesPorAnio.diasDisfrutar;
    return localStateManager.getIntField(paramVacacionesPorAnio, jdoInheritedFieldCount + 2, paramVacacionesPorAnio.diasDisfrutar);
  }

  private static final void jdoSetdiasDisfrutar(VacacionesPorAnio paramVacacionesPorAnio, int paramInt)
  {
    if (paramVacacionesPorAnio.jdoFlags == 0)
    {
      paramVacacionesPorAnio.diasDisfrutar = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacionesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorAnio.diasDisfrutar = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacionesPorAnio, jdoInheritedFieldCount + 2, paramVacacionesPorAnio.diasDisfrutar, paramInt);
  }

  private static final double jdoGetdiasExtra(VacacionesPorAnio paramVacacionesPorAnio)
  {
    if (paramVacacionesPorAnio.jdoFlags <= 0)
      return paramVacacionesPorAnio.diasExtra;
    StateManager localStateManager = paramVacacionesPorAnio.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionesPorAnio.diasExtra;
    if (localStateManager.isLoaded(paramVacacionesPorAnio, jdoInheritedFieldCount + 3))
      return paramVacacionesPorAnio.diasExtra;
    return localStateManager.getDoubleField(paramVacacionesPorAnio, jdoInheritedFieldCount + 3, paramVacacionesPorAnio.diasExtra);
  }

  private static final void jdoSetdiasExtra(VacacionesPorAnio paramVacacionesPorAnio, double paramDouble)
  {
    if (paramVacacionesPorAnio.jdoFlags == 0)
    {
      paramVacacionesPorAnio.diasExtra = paramDouble;
      return;
    }
    StateManager localStateManager = paramVacacionesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorAnio.diasExtra = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramVacacionesPorAnio, jdoInheritedFieldCount + 3, paramVacacionesPorAnio.diasExtra, paramDouble);
  }

  private static final long jdoGetidVacacionesPorAnio(VacacionesPorAnio paramVacacionesPorAnio)
  {
    return paramVacacionesPorAnio.idVacacionesPorAnio;
  }

  private static final void jdoSetidVacacionesPorAnio(VacacionesPorAnio paramVacacionesPorAnio, long paramLong)
  {
    StateManager localStateManager = paramVacacionesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorAnio.idVacacionesPorAnio = paramLong;
      return;
    }
    localStateManager.setLongField(paramVacacionesPorAnio, jdoInheritedFieldCount + 4, paramVacacionesPorAnio.idVacacionesPorAnio, paramLong);
  }

  private static final TipoPersonal jdoGettipoPersonal(VacacionesPorAnio paramVacacionesPorAnio)
  {
    StateManager localStateManager = paramVacacionesPorAnio.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionesPorAnio.tipoPersonal;
    if (localStateManager.isLoaded(paramVacacionesPorAnio, jdoInheritedFieldCount + 5))
      return paramVacacionesPorAnio.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramVacacionesPorAnio, jdoInheritedFieldCount + 5, paramVacacionesPorAnio.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(VacacionesPorAnio paramVacacionesPorAnio, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramVacacionesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionesPorAnio.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramVacacionesPorAnio, jdoInheritedFieldCount + 5, paramVacacionesPorAnio.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}