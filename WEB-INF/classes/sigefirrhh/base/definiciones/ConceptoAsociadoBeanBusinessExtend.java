package sigefirrhh.base.definiciones;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ConceptoAsociadoBeanBusinessExtend extends ConceptoAsociadoBeanBusiness
  implements Serializable
{
  public Collection findByIdTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ConceptoAsociado.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colConceptoAsociado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoAsociado);

    return colConceptoAsociado;
  }
}