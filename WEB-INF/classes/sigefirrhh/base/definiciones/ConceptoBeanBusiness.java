package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class ConceptoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(ConceptoBeanBusiness.class.getName());

  public void addConcepto(Concepto concepto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Concepto conceptoNew = 
      (Concepto)BeanUtils.cloneBean(
      concepto);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (conceptoNew.getConceptoAporte() != null) {
      conceptoNew.setConceptoAporte(
        findConceptoById(
        conceptoNew.getConceptoAporte().getIdConcepto()));
    }

    if (conceptoNew.getConceptoAusencia() != null) {
      conceptoNew.setConceptoAusencia(
        findConceptoById(
        conceptoNew.getConceptoAusencia().getIdConcepto()));
    }

    if (conceptoNew.getConceptoRetroactivo() != null) {
      conceptoNew.setConceptoRetroactivo(
        findConceptoById(
        conceptoNew.getConceptoRetroactivo().getIdConcepto()));
    }
    if (conceptoNew.getConceptoCaja() != null) {
      conceptoNew.setConceptoCaja(
        findConceptoById(
        conceptoNew.getConceptoCaja().getIdConcepto()));
    }
    if (conceptoNew.getOrganismo() != null) {
      conceptoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        conceptoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(conceptoNew);
  }

  public void updateConcepto(Concepto concepto) throws Exception
  {
    try {
      Concepto conceptoModify = 
        findConceptoById(concepto.getIdConcepto());

      OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

      if (concepto.getOrganismo() != null) {
        concepto.setOrganismo(
          organismoBeanBusiness.findOrganismoById(
          concepto.getOrganismo().getIdOrganismo()));
      }

      if (concepto.getConceptoAporte() != null) {
        concepto.setConceptoAporte(
          findConceptoById(
          concepto.getConceptoAporte().getIdConcepto()));
      }

      if (concepto.getConceptoAusencia() != null) {
        concepto.setConceptoAusencia(
          findConceptoById(
          concepto.getConceptoAusencia().getIdConcepto()));
      }

      if (concepto.getConceptoRetroactivo() != null) {
        concepto.setConceptoRetroactivo(
          findConceptoById(
          concepto.getConceptoRetroactivo().getIdConcepto()));
      }
      if (concepto.getConceptoRetroactivoAnterior() != null) {
        concepto.setConceptoRetroactivoAnterior(
          findConceptoById(
          concepto.getConceptoRetroactivoAnterior().getIdConcepto()));
      }

      if (concepto.getConceptoCaja() != null) {
        concepto.setConceptoCaja(
          findConceptoById(
          concepto.getConceptoCaja().getIdConcepto()));
      }

      BeanUtils.copyProperties(conceptoModify, concepto);
    } catch (Exception e) {
      this.log.error("Error en ConceptoBeanBusiness.updateConcepto:" + e);
      this.log.error("Excepcion controlada:", e);
      throw e;
    }
  }

  public void deleteConcepto(Concepto concepto) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Concepto conceptoDelete = 
      findConceptoById(concepto.getIdConcepto());
    pm.deletePersistent(conceptoDelete);
  }

  public Concepto findConceptoById(long idConcepto) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConcepto == pIdConcepto";
    Query query = pm.newQuery(Concepto.class, filter);

    query.declareParameters("long pIdConcepto");

    parameters.put("pIdConcepto", new Long(idConcepto));

    Collection colConcepto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConcepto.iterator();
    return (Concepto)iterator.next();
  }

  public Collection findConceptoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoExtent = pm.getExtent(
      Concepto.class, true);
    Query query = pm.newQuery(conceptoExtent);
    query.setOrdering("codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodConcepto(String codConcepto, long idOrganismo) throws Exception
  {
    this.log.error("findByCodConcepto---------------------");
    PersistenceManager pm = PMThread.getPM();

    String filter = "codConcepto == pCodConcepto &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Concepto.class, filter);

    query.declareParameters("java.lang.String pCodConcepto, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodConcepto", new String(codConcepto));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codConcepto ascending");

    Collection colConcepto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcepto);

    return colConcepto;
  }

  public Collection findByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Concepto.class, filter);

    query.declareParameters("java.lang.String pDescripcion, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codConcepto ascending");

    Collection colConcepto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcepto);

    return colConcepto;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Concepto.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codConcepto ascending");

    Collection colConcepto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcepto);

    return colConcepto;
  }

  public Collection findByOrganismoAndDeduccionSindicato(long idOrganismo, String deduccionSindicato) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo && deduccionSindicato == pDeduccionSindicato";

    Query query = pm.newQuery(Concepto.class, filter);

    query.declareParameters("long pIdOrganismo, String pDeduccionSindicato");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pDeduccionSindicato", new String(deduccionSindicato));

    query.setOrdering("codConcepto ascending");

    Collection colConcepto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcepto);

    return colConcepto;
  }

  public Collection findByTipoPrestamo(long idOrganismo, String tipoPrestamo) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo && tipoPrestamo == pTipoPrestamo";

    Query query = pm.newQuery(Concepto.class, filter);

    query.declareParameters("long pIdOrganismo, String pTipoPrestamo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pTipoPrestamo", new String(tipoPrestamo));

    query.setOrdering("codConcepto ascending");

    Collection colConcepto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcepto);

    return colConcepto;
  }

  public Collection findByAportePatronal(String aportePatronal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "aportePatronal == pAportePatronal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Concepto.class, filter);

    query.declareParameters("java.lang.String pAportePatronal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pAportePatronal", new String(aportePatronal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codConcepto ascending");

    Collection colConcepto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcepto);

    return colConcepto;
  }
}