package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class GrupoNominaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GrupoNominaForm.class.getName());
  private GrupoNomina grupoNomina;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showGrupoNominaByCodGrupoNomina;
  private boolean showGrupoNominaByNombre;
  private int findCodGrupoNomina;
  private String findNombre;
  private Object stateResultGrupoNominaByCodGrupoNomina = null;

  private Object stateResultGrupoNominaByNombre = null;

  public int getFindCodGrupoNomina()
  {
    return this.findCodGrupoNomina;
  }
  public void setFindCodGrupoNomina(int findCodGrupoNomina) {
    this.findCodGrupoNomina = findCodGrupoNomina;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public GrupoNomina getGrupoNomina() {
    if (this.grupoNomina == null) {
      this.grupoNomina = new GrupoNomina();
    }
    return this.grupoNomina;
  }

  public GrupoNominaForm() throws Exception
  {
    newReportId();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListPeriodicidad()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = GrupoNomina.LISTA_PERIODICIDAD.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListPagosNominaEgresados()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = GrupoNomina.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findGrupoNominaByCodGrupoNomina()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findGrupoNominaByCodGrupoNomina(this.findCodGrupoNomina, idOrganismo);
      this.showGrupoNominaByCodGrupoNomina = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGrupoNominaByCodGrupoNomina)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodGrupoNomina = 0;
    this.findNombre = null;

    return null;
  }

  public String findGrupoNominaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findGrupoNominaByNombre(this.findNombre, idOrganismo);
      this.showGrupoNominaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGrupoNominaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodGrupoNomina = 0;
    this.findNombre = null;

    return null;
  }

  public boolean isShowGrupoNominaByCodGrupoNomina() {
    return this.showGrupoNominaByCodGrupoNomina;
  }
  public boolean isShowGrupoNominaByNombre() {
    return this.showGrupoNominaByNombre;
  }

  public String selectGrupoNomina()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idGrupoNomina = 
      Long.parseLong((String)requestParameterMap.get("idGrupoNomina"));
    try
    {
      this.grupoNomina = 
        this.definicionesFacade.findGrupoNominaById(
        idGrupoNomina);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.grupoNomina = null;
    this.showGrupoNominaByCodGrupoNomina = false;
    this.showGrupoNominaByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addGrupoNomina(
          this.grupoNomina);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateGrupoNomina(
          this.grupoNomina);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteGrupoNomina(
        this.grupoNomina);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.grupoNomina = new GrupoNomina();

    this.grupoNomina.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.grupoNomina.setIdGrupoNomina(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.GrupoNomina"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.grupoNomina = new GrupoNomina();
    return "cancel";
  }

  public boolean isShowAnticipoQuincenalAux() {
    try {
      return this.grupoNomina.getPeriodicidad().equals("M"); } catch (Exception e) {
    }
    return false;
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("GrupoNomina");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "GrupoNomina" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}