package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ConceptoAsociadoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoAsociado(ConceptoAsociado conceptoAsociado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoAsociado conceptoAsociadoNew = 
      (ConceptoAsociado)BeanUtils.cloneBean(
      conceptoAsociado);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoAsociadoNew.getConceptoTipoPersonal() != null) {
      conceptoAsociadoNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoAsociadoNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoAsociarBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoAsociadoNew.getConceptoAsociar() != null) {
      conceptoAsociadoNew.setConceptoAsociar(
        conceptoAsociarBeanBusiness.findConceptoTipoPersonalById(
        conceptoAsociadoNew.getConceptoAsociar().getIdConceptoTipoPersonal()));
    }
    pm.makePersistent(conceptoAsociadoNew);
  }

  public void updateConceptoAsociado(ConceptoAsociado conceptoAsociado) throws Exception
  {
    ConceptoAsociado conceptoAsociadoModify = 
      findConceptoAsociadoById(conceptoAsociado.getIdConceptoAsociado());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoAsociado.getConceptoTipoPersonal() != null) {
      conceptoAsociado.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoAsociado.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoAsociarBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoAsociado.getConceptoAsociar() != null) {
      conceptoAsociado.setConceptoAsociar(
        conceptoAsociarBeanBusiness.findConceptoTipoPersonalById(
        conceptoAsociado.getConceptoAsociar().getIdConceptoTipoPersonal()));
    }

    BeanUtils.copyProperties(conceptoAsociadoModify, conceptoAsociado);
  }

  public void deleteConceptoAsociado(ConceptoAsociado conceptoAsociado) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoAsociado conceptoAsociadoDelete = 
      findConceptoAsociadoById(conceptoAsociado.getIdConceptoAsociado());
    pm.deletePersistent(conceptoAsociadoDelete);
  }

  public ConceptoAsociado findConceptoAsociadoById(long idConceptoAsociado) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoAsociado == pIdConceptoAsociado";
    Query query = pm.newQuery(ConceptoAsociado.class, filter);

    query.declareParameters("long pIdConceptoAsociado");

    parameters.put("pIdConceptoAsociado", new Long(idConceptoAsociado));

    Collection colConceptoAsociado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoAsociado.iterator();
    return (ConceptoAsociado)iterator.next();
  }

  public Collection findConceptoAsociadoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoAsociadoExtent = pm.getExtent(
      ConceptoAsociado.class, true);
    Query query = pm.newQuery(conceptoAsociadoExtent);
    query.setOrdering("conceptoAsociar.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal";

    Query query = pm.newQuery(ConceptoAsociado.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    query.setOrdering("conceptoAsociar.concepto.codConcepto ascending");

    Collection colConceptoAsociado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoAsociado);

    return colConceptoAsociado;
  }
}