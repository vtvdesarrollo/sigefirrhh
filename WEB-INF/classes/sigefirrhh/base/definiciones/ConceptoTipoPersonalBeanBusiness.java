package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ConceptoTipoPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoTipoPersonal conceptoTipoPersonalNew = 
      (ConceptoTipoPersonal)BeanUtils.cloneBean(
      conceptoTipoPersonal);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (conceptoTipoPersonalNew.getTipoPersonal() != null) {
      conceptoTipoPersonalNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        conceptoTipoPersonalNew.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (conceptoTipoPersonalNew.getConcepto() != null) {
      conceptoTipoPersonalNew.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        conceptoTipoPersonalNew.getConcepto().getIdConcepto()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (conceptoTipoPersonalNew.getFrecuenciaTipoPersonal() != null) {
      conceptoTipoPersonalNew.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        conceptoTipoPersonalNew.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    ContratoColectivoBeanBusiness contratoColectivoBeanBusiness = new ContratoColectivoBeanBusiness();

    if (conceptoTipoPersonalNew.getContratoColectivo() != null) {
      conceptoTipoPersonalNew.setContratoColectivo(
        contratoColectivoBeanBusiness.findContratoColectivoById(
        conceptoTipoPersonalNew.getContratoColectivo().getIdContratoColectivo()));
    }
    pm.makePersistent(conceptoTipoPersonalNew);
  }

  public void updateConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) throws Exception
  {
    ConceptoTipoPersonal conceptoTipoPersonalModify = 
      findConceptoTipoPersonalById(conceptoTipoPersonal.getIdConceptoTipoPersonal());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (conceptoTipoPersonal.getTipoPersonal() != null) {
      conceptoTipoPersonal.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        conceptoTipoPersonal.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (conceptoTipoPersonal.getConcepto() != null) {
      conceptoTipoPersonal.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        conceptoTipoPersonal.getConcepto().getIdConcepto()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (conceptoTipoPersonal.getFrecuenciaTipoPersonal() != null) {
      conceptoTipoPersonal.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        conceptoTipoPersonal.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    ContratoColectivoBeanBusiness contratoColectivoBeanBusiness = new ContratoColectivoBeanBusiness();

    if (conceptoTipoPersonal.getContratoColectivo() != null) {
      conceptoTipoPersonal.setContratoColectivo(
        contratoColectivoBeanBusiness.findContratoColectivoById(
        conceptoTipoPersonal.getContratoColectivo().getIdContratoColectivo()));
    }

    BeanUtils.copyProperties(conceptoTipoPersonalModify, conceptoTipoPersonal);
  }

  public void deleteConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoTipoPersonal conceptoTipoPersonalDelete = 
      findConceptoTipoPersonalById(conceptoTipoPersonal.getIdConceptoTipoPersonal());
    pm.deletePersistent(conceptoTipoPersonalDelete);
  }

  public ConceptoTipoPersonal findConceptoTipoPersonalById(long idConceptoTipoPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoTipoPersonal == pIdConceptoTipoPersonal";
    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal");

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoTipoPersonal.iterator();
    return (ConceptoTipoPersonal)iterator.next();
  }

  public Collection findConceptoTipoPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoTipoPersonalExtent = pm.getExtent(
      ConceptoTipoPersonal.class, true);
    Query query = pm.newQuery(conceptoTipoPersonalExtent);
    query.setOrdering("concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoTipoPersonal);

    return colConceptoTipoPersonal;
  }

  public Collection findByConcepto(long idConcepto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "concepto.idConcepto == pIdConcepto";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdConcepto");
    HashMap parameters = new HashMap();

    parameters.put("pIdConcepto", new Long(idConcepto));

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoTipoPersonal);

    return colConceptoTipoPersonal;
  }
}