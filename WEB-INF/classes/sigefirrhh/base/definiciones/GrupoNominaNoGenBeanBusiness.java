package sigefirrhh.base.definiciones;

import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class GrupoNominaNoGenBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(GrupoNominaNoGenBeanBusiness.class.getName());

  public Collection findByOrganismoAndPeriodicidad(long idOrganismo, String periodicidad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo && periodicidad == pPeriodicidad";

    Query query = pm.newQuery(GrupoNomina.class, filter);

    query.declareParameters("long pIdOrganismo, String periodicidad");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pPeriodicidad", periodicidad);

    query.setOrdering("codGrupoNomina ascending");

    Collection colGrupoNomina = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoNomina);

    return colGrupoNomina;
  }

  public Collection findWithSeguridad(long idUsuario, long idOrganismo, String administrador)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();

    this.log.error("id_usuario" + idUsuario);
    this.log.error("id_organismo" + idOrganismo);
    this.log.error("administrador" + administrador);
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      if (administrador.equals("N")) {
        sql.append("select gn.id_grupo_nomina as id, (gn.nombre || ' ' || gn.cod_grupo_nomina)  as descripcion  ");
        sql.append(" from gruponomina gn");
        sql.append(" where gn.id_grupo_nomina in (");
        sql.append(" select tp.id_grupo_nomina ");
        sql.append(" from tipopersonal tp, usuariotipopersonal utp");
        sql.append(" where utp.id_usuario = ?");
        sql.append(" and tp.id_organismo = ?");
        sql.append(" and utp.id_tipo_personal = tp.id_tipo_personal)");
        sql.append(" order by gn.cod_grupo_nomina");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setLong(2, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      } else {
        sql.append("select gn.id_grupo_nomina as id, (gn.nombre || ' ' || gn.cod_grupo_nomina)  as descripcion  ");
        sql.append(" from gruponomina gn");
        sql.append(" where ");
        sql.append(" gn.id_organismo = ?");
        sql.append(" order by gn.cod_grupo_nomina");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      }

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}