package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Mes
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_MES;
  private long idMes;
  private int anio;
  private String mes;
  private int lunesPrQuincena;
  private int lunesSeQuincena;
  private int diasHabiles;
  private int conSabados;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "conSabados", "diasHabiles", "idMes", "lunesPrQuincena", "lunesSeQuincena", "mes" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, Long.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.Mes"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Mes());

    LISTA_MES = 
      new LinkedHashMap();
    LISTA_MES.put("1", "ENERO");
    LISTA_MES.put("2", "FEBRERO");
    LISTA_MES.put("3", "MARZO");
    LISTA_MES.put("4", "ABRIL");
    LISTA_MES.put("5", "MAYO");
    LISTA_MES.put("6", "JUNIO");
    LISTA_MES.put("7", "JULIO");
    LISTA_MES.put("8", "AGOSTO");
    LISTA_MES.put("9", "SEPTIEMBRE");
    LISTA_MES.put("10", "OCTUBRE");
    LISTA_MES.put("11", "NOVIEMBRE");
    LISTA_MES.put("12", "DICIEMBRE");
  }

  public Mes()
  {
    jdoSetlunesPrQuincena(this, 0);

    jdoSetlunesSeQuincena(this, 0);

    jdoSetdiasHabiles(this, 0);

    jdoSetconSabados(this, 0);
  }
  public String toString() {
    return jdoGetanio(this) + " - " + 
      LISTA_MES.get(jdoGetmes(this)) + " - " + 
      jdoGetlunesPrQuincena(this) + " - " + 
      jdoGetlunesSeQuincena(this);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public int getDiasHabiles()
  {
    return jdoGetdiasHabiles(this);
  }

  public long getIdMes()
  {
    return jdoGetidMes(this);
  }

  public int getLunesPrQuincena()
  {
    return jdoGetlunesPrQuincena(this);
  }

  public int getLunesSeQuincena()
  {
    return jdoGetlunesSeQuincena(this);
  }

  public String getMes()
  {
    return jdoGetmes(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setDiasHabiles(int i)
  {
    jdoSetdiasHabiles(this, i);
  }

  public void setIdMes(long l)
  {
    jdoSetidMes(this, l);
  }

  public void setLunesPrQuincena(int i)
  {
    jdoSetlunesPrQuincena(this, i);
  }

  public void setLunesSeQuincena(int i)
  {
    jdoSetlunesSeQuincena(this, i);
  }

  public void setMes(String string)
  {
    jdoSetmes(this, string);
  }
  public int getConSabados() {
    return jdoGetconSabados(this);
  }
  public void setConSabados(int conSabados) {
    jdoSetconSabados(this, conSabados);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Mes localMes = new Mes();
    localMes.jdoFlags = 1;
    localMes.jdoStateManager = paramStateManager;
    return localMes;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Mes localMes = new Mes();
    localMes.jdoCopyKeyFieldsFromObjectId(paramObject);
    localMes.jdoFlags = 1;
    localMes.jdoStateManager = paramStateManager;
    return localMes;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.conSabados);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasHabiles);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idMes);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.lunesPrQuincena);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.lunesSeQuincena);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mes);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conSabados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasHabiles = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idMes = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lunesPrQuincena = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lunesSeQuincena = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Mes paramMes, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramMes == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramMes.anio;
      return;
    case 1:
      if (paramMes == null)
        throw new IllegalArgumentException("arg1");
      this.conSabados = paramMes.conSabados;
      return;
    case 2:
      if (paramMes == null)
        throw new IllegalArgumentException("arg1");
      this.diasHabiles = paramMes.diasHabiles;
      return;
    case 3:
      if (paramMes == null)
        throw new IllegalArgumentException("arg1");
      this.idMes = paramMes.idMes;
      return;
    case 4:
      if (paramMes == null)
        throw new IllegalArgumentException("arg1");
      this.lunesPrQuincena = paramMes.lunesPrQuincena;
      return;
    case 5:
      if (paramMes == null)
        throw new IllegalArgumentException("arg1");
      this.lunesSeQuincena = paramMes.lunesSeQuincena;
      return;
    case 6:
      if (paramMes == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramMes.mes;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Mes))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Mes localMes = (Mes)paramObject;
    if (localMes.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localMes, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new MesPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new MesPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MesPK))
      throw new IllegalArgumentException("arg1");
    MesPK localMesPK = (MesPK)paramObject;
    localMesPK.idMes = this.idMes;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MesPK))
      throw new IllegalArgumentException("arg1");
    MesPK localMesPK = (MesPK)paramObject;
    this.idMes = localMesPK.idMes;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MesPK))
      throw new IllegalArgumentException("arg2");
    MesPK localMesPK = (MesPK)paramObject;
    localMesPK.idMes = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MesPK))
      throw new IllegalArgumentException("arg2");
    MesPK localMesPK = (MesPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localMesPK.idMes);
  }

  private static final int jdoGetanio(Mes paramMes)
  {
    if (paramMes.jdoFlags <= 0)
      return paramMes.anio;
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
      return paramMes.anio;
    if (localStateManager.isLoaded(paramMes, jdoInheritedFieldCount + 0))
      return paramMes.anio;
    return localStateManager.getIntField(paramMes, jdoInheritedFieldCount + 0, paramMes.anio);
  }

  private static final void jdoSetanio(Mes paramMes, int paramInt)
  {
    if (paramMes.jdoFlags == 0)
    {
      paramMes.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
    {
      paramMes.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramMes, jdoInheritedFieldCount + 0, paramMes.anio, paramInt);
  }

  private static final int jdoGetconSabados(Mes paramMes)
  {
    if (paramMes.jdoFlags <= 0)
      return paramMes.conSabados;
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
      return paramMes.conSabados;
    if (localStateManager.isLoaded(paramMes, jdoInheritedFieldCount + 1))
      return paramMes.conSabados;
    return localStateManager.getIntField(paramMes, jdoInheritedFieldCount + 1, paramMes.conSabados);
  }

  private static final void jdoSetconSabados(Mes paramMes, int paramInt)
  {
    if (paramMes.jdoFlags == 0)
    {
      paramMes.conSabados = paramInt;
      return;
    }
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
    {
      paramMes.conSabados = paramInt;
      return;
    }
    localStateManager.setIntField(paramMes, jdoInheritedFieldCount + 1, paramMes.conSabados, paramInt);
  }

  private static final int jdoGetdiasHabiles(Mes paramMes)
  {
    if (paramMes.jdoFlags <= 0)
      return paramMes.diasHabiles;
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
      return paramMes.diasHabiles;
    if (localStateManager.isLoaded(paramMes, jdoInheritedFieldCount + 2))
      return paramMes.diasHabiles;
    return localStateManager.getIntField(paramMes, jdoInheritedFieldCount + 2, paramMes.diasHabiles);
  }

  private static final void jdoSetdiasHabiles(Mes paramMes, int paramInt)
  {
    if (paramMes.jdoFlags == 0)
    {
      paramMes.diasHabiles = paramInt;
      return;
    }
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
    {
      paramMes.diasHabiles = paramInt;
      return;
    }
    localStateManager.setIntField(paramMes, jdoInheritedFieldCount + 2, paramMes.diasHabiles, paramInt);
  }

  private static final long jdoGetidMes(Mes paramMes)
  {
    return paramMes.idMes;
  }

  private static final void jdoSetidMes(Mes paramMes, long paramLong)
  {
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
    {
      paramMes.idMes = paramLong;
      return;
    }
    localStateManager.setLongField(paramMes, jdoInheritedFieldCount + 3, paramMes.idMes, paramLong);
  }

  private static final int jdoGetlunesPrQuincena(Mes paramMes)
  {
    if (paramMes.jdoFlags <= 0)
      return paramMes.lunesPrQuincena;
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
      return paramMes.lunesPrQuincena;
    if (localStateManager.isLoaded(paramMes, jdoInheritedFieldCount + 4))
      return paramMes.lunesPrQuincena;
    return localStateManager.getIntField(paramMes, jdoInheritedFieldCount + 4, paramMes.lunesPrQuincena);
  }

  private static final void jdoSetlunesPrQuincena(Mes paramMes, int paramInt)
  {
    if (paramMes.jdoFlags == 0)
    {
      paramMes.lunesPrQuincena = paramInt;
      return;
    }
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
    {
      paramMes.lunesPrQuincena = paramInt;
      return;
    }
    localStateManager.setIntField(paramMes, jdoInheritedFieldCount + 4, paramMes.lunesPrQuincena, paramInt);
  }

  private static final int jdoGetlunesSeQuincena(Mes paramMes)
  {
    if (paramMes.jdoFlags <= 0)
      return paramMes.lunesSeQuincena;
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
      return paramMes.lunesSeQuincena;
    if (localStateManager.isLoaded(paramMes, jdoInheritedFieldCount + 5))
      return paramMes.lunesSeQuincena;
    return localStateManager.getIntField(paramMes, jdoInheritedFieldCount + 5, paramMes.lunesSeQuincena);
  }

  private static final void jdoSetlunesSeQuincena(Mes paramMes, int paramInt)
  {
    if (paramMes.jdoFlags == 0)
    {
      paramMes.lunesSeQuincena = paramInt;
      return;
    }
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
    {
      paramMes.lunesSeQuincena = paramInt;
      return;
    }
    localStateManager.setIntField(paramMes, jdoInheritedFieldCount + 5, paramMes.lunesSeQuincena, paramInt);
  }

  private static final String jdoGetmes(Mes paramMes)
  {
    if (paramMes.jdoFlags <= 0)
      return paramMes.mes;
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
      return paramMes.mes;
    if (localStateManager.isLoaded(paramMes, jdoInheritedFieldCount + 6))
      return paramMes.mes;
    return localStateManager.getStringField(paramMes, jdoInheritedFieldCount + 6, paramMes.mes);
  }

  private static final void jdoSetmes(Mes paramMes, String paramString)
  {
    if (paramMes.jdoFlags == 0)
    {
      paramMes.mes = paramString;
      return;
    }
    StateManager localStateManager = paramMes.jdoStateManager;
    if (localStateManager == null)
    {
      paramMes.mes = paramString;
      return;
    }
    localStateManager.setStringField(paramMes, jdoInheritedFieldCount + 6, paramMes.mes, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}