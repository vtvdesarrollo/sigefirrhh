package sigefirrhh.base.definiciones;

import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ConceptoTipoPersonalNoGenBeanBusiness extends ConceptoTipoPersonalBeanBusiness
  implements Serializable
{
  public Collection finForRetenciones(long idTipoPersonal, Long idFrecuenciaPago, String codConcepto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.codConcepto == pCodConcepto && frecuenciaTipoPersonal.frecuenciaPago.idFrecuenciaPago == pIdFrecuenciaPago";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, long pIdFrecuenciaPago, String pCodConcepto");
    HashMap parameters = new HashMap();

    query.setOrdering("concepto.codConcepto ascending");

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pIdFrecuenciaPago", idFrecuenciaPago);
    parameters.put("pCodConcepto", codConcepto);

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findForPrestamo(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String tipoPrestamo = "S";
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.tipoPrestamo == pTipoPrestamo";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pTipoPrestamo");
    HashMap parameters = new HashMap();

    query.setOrdering("concepto.codConcepto ascending");

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pTipoPrestamo", tipoPrestamo);

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findByCodConceptoAndIdTipoPersonal(String codConcepto, long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.codConcepto == pCodConcepto";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pCodConcepto");
    HashMap parameters = new HashMap();

    query.setOrdering("concepto.codConcepto ascending");

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pCodConcepto", codConcepto);

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findBySobretiempo(long idTipoPersonal, String sobretiempo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.sobretiempo == pSobretiempo";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pSobretiempo");
    HashMap parameters = new HashMap();

    query.setOrdering("concepto.codConcepto ascending");

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pSobretiempo", sobretiempo);

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findByTipoPersonalAndConcepto(long idTipoPersonal, long idConcepto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.idConcepto == pIdConcepto";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, long pIdConcepto");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pIdConcepto", new Long(idConcepto));

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findForIngresoTrabajador(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String automaticoIngreso = "S";
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && automaticoIngreso == pAutomaticoIngreso";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pAutomaticoIngreso");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pAutomaticoIngreso", automaticoIngreso);

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findForIngresoTrabajadorCargo(long idTipoPersonal, long idCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String automaticoIngreso = "S";
    String excluir = "N";

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && cca.conceptoTipoPersonal.idConceptoTipoPersonal == idConceptoTipoPersonal && cca.cargo.idCargo = pIdCargo && cca.automaticoIngreso == pAutomaticoIngreso && cca.excluir == pExcluir && cca.anios == 0";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareImports("import sigefirrhh.base.definiciones.ConceptoCargoAnio");
    query.declareVariables("ConceptoCargoAnio cca");

    query.declareParameters("long pIdTipoPersonal, long pIdCargo, String pAutomaticoIngreso, String pExcluir");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pIdCargo", new Long(idCargo));
    parameters.put("pAutomaticoIngreso", automaticoIngreso);
    parameters.put("pExcluir", automaticoIngreso);

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findByTipoPersonalAsignaciones(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String codConcepto = "5000";
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.codConcepto < pCodConcepto";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pCodConcepto");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pCodConcepto", codConcepto);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findByTipoPersonalSueldoBasico(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String criterio = "S";
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.sueldoBasico == pCriterio";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pCriterio");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pCriterio", criterio);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findByTipoPersonalCompensacion(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String criterio = "S";
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.compensacion == pCriterio";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pCriterio");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pCriterio", criterio);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findByTipoPersonalAjusteSueldo(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String criterio = "S";
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.ajusteSueldo == pCriterio";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pCriterio");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pCriterio", criterio);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findByTipoPersonalPrimasCargo(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String criterio = "S";
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.primasCargo == pCriterio";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pCriterio");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pCriterio", criterio);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findByTipoPersonalPrimasTrabajador(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String criterio = "S";
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.primasTrabajador == pCriterio";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pCriterio");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pCriterio", criterio);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findForAportePatronal(long idTipoPersonal, String aportePatronal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.aportePatronal == pAportePatronal";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pAportePatronal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pAportePatronal", aportePatronal);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findByRecalculo(long idTipoPersonal, String recalculo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && recalculo == pRecalculo";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pRecalculo");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pRecalculo", recalculo);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findForConceptoCuenta(long idTipoPersonal, long idConceptoTipoPersonal)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsConceptos = null;
    PreparedStatement stConceptos = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select ctp.id_concepto_tipo_personal, (c.cod_concepto || ' - ' || c.descripcion)  as descripcion ");
      sql.append(" from conceptotipopersonal ctp, concepto c");
      sql.append(" where ctp.id_tipo_personal = ?");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and c.cod_concepto < '5000'");
      sql.append(" and ((ctp.id_concepto_tipo_personal not in (");
      sql.append(" select id_concepto_tipo_personal ");
      sql.append(" from conceptocuenta)) or ctp.id_concepto_tipo_personal = ? )");
      sql.append(" order by c.cod_concepto");

      stConceptos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptos.setLong(1, idTipoPersonal);
      stConceptos.setLong(2, idConceptoTipoPersonal);
      rsConceptos = stConceptos.executeQuery();

      while (rsConceptos.next())
      {
        col.add(Long.valueOf(String.valueOf(rsConceptos.getLong("id_concepto_tipo_personal"))));
        col.add(rsConceptos.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsConceptos != null) try {
          rsConceptos.close();
        } catch (Exception localException3) {
        } if (stConceptos != null) try {
          stConceptos.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5) {
        } 
    }
  }

  public Collection findForConceptoCuenta(long idTipoPersonal) throws Exception { Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsConceptos = null;
    PreparedStatement stConceptos = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select ctp.id_concepto_tipo_personal, (c.cod_concepto || ' - ' || c.descripcion)  as descripcion ");
      sql.append(" from conceptotipopersonal ctp, concepto c");
      sql.append(" where ctp.id_tipo_personal = ?");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and c.cod_concepto < '5000'");
      sql.append(" order by c.cod_concepto");

      stConceptos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptos.setLong(1, idTipoPersonal);
      rsConceptos = stConceptos.executeQuery();

      while (rsConceptos.next())
      {
        col.add(Long.valueOf(String.valueOf(rsConceptos.getLong("id_concepto_tipo_personal"))));
        col.add(rsConceptos.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsConceptos != null) try {
          rsConceptos.close();
        } catch (Exception localException3) {
        } if (stConceptos != null) try {
          stConceptos.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    } } 
  public Collection findByIdTipoPersonal(long idTipoPersonal) throws Exception {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsConceptos = null;
    PreparedStatement stConceptos = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select ctp.id_concepto_tipo_personal, (c.cod_concepto || ' - ' || c.descripcion)  as descripcion  ");
      sql.append(" from conceptotipopersonal ctp, concepto c");
      sql.append(" where ctp.id_tipo_personal = ?");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" order by c.cod_concepto");

      stConceptos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptos.setLong(1, idTipoPersonal);
      rsConceptos = stConceptos.executeQuery();

      while (rsConceptos.next())
      {
        col.add(Long.valueOf(String.valueOf(rsConceptos.getLong("id_concepto_tipo_personal"))));
        col.add(rsConceptos.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsConceptos != null) try {
          rsConceptos.close();
        } catch (Exception localException3) {
        } if (stConceptos != null) try {
          stConceptos.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public double verificarTopeSobretiempo(long idConceptoTipoPersonal, long idTrabajador, int mes, int anio, double unidadesAgregar) throws Exception { Connection connection = null;

    StringBuffer sql = new StringBuffer();

    double unidades = 0.0D;
    double tope = 0.0D;

    ResultSet rsConceptos = null;
    PreparedStatement stConceptos = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select tope_maximo");
      sql.append(" from conceptovariable cv");
      sql.append(" where cv.id_trabajador = ?");
      sql.append(" and cv.id_concepto_tipo_personal = ?");
      sql.append(" and cv.mes_sobretiempo = ?");
      sql.append(" and cv.anio_sobretiempo = ?");
      sql.append(" group by cv.id_trabajador");

      stConceptos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptos.setLong(1, idTrabajador);
      stConceptos.setLong(2, idConceptoTipoPersonal);
      stConceptos.setInt(3, mes);
      stConceptos.setInt(4, anio);
      rsConceptos = stConceptos.executeQuery();

      if (rsConceptos.next()) {
        unidades = rsConceptos.getDouble("unidades");
      }

      sql.append("select sum(cv.unidades) as unidades");
      sql.append(" from conceptovariable cv");
      sql.append(" where cv.id_trabajador = ?");
      sql.append(" and cv.id_concepto_tipo_personal = ?");
      sql.append(" and cv.mes_sobretiempo = ?");
      sql.append(" and cv.anio_sobretiempo = ?");
      sql.append(" group by cv.id_trabajador");

      stConceptos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptos.setLong(1, idTrabajador);
      stConceptos.setLong(2, idConceptoTipoPersonal);
      stConceptos.setInt(3, mes);
      stConceptos.setInt(4, anio);
      rsConceptos = stConceptos.executeQuery();

      if (rsConceptos.next()) {
        unidades = rsConceptos.getDouble("unidades");
      }

      sql.append("select sum(hq.unidades) as unidades");
      sql.append(" from historicoquincena hq");
      sql.append(" where hq.id_trabajador = ?");
      sql.append(" and hq.id_concepto = ?");
      sql.append(" and hq.mes_sobretiempo = ?");
      sql.append(" and hq.anio_sobretiempo = ?");
      sql.append(" group by hq.id_trabajador");

      stConceptos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptos.setLong(1, idTrabajador);
      stConceptos.setLong(2, idConceptoTipoPersonal);
      stConceptos.setInt(3, mes);
      stConceptos.setInt(4, anio);
      rsConceptos = stConceptos.executeQuery();

      if (rsConceptos.next()) {
        unidades += rsConceptos.getDouble("unidades");
      }

      if ((tope == unidades) || (tope < unidades)) {
        return -1.0D;
      }
      if (unidades + unidadesAgregar > tope) {
        return unidades + unidadesAgregar - tope;
      }
      return 0.0D;
    } finally {
      if (rsConceptos != null) try {
          rsConceptos.close();
        } catch (Exception localException9) {
        } if (stConceptos != null) try {
          stConceptos.close();
        } catch (Exception localException10) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException11)
        {
        } 
    } } 
  public Collection findByBeneficio(long idTipoPersonal, String beneficio)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsConceptos = null;
    PreparedStatement stConceptos = null;
    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select ctp.id_concepto_tipo_personal, (c.cod_concepto || ' - ' || c.descripcion)  as descripcion  ");
      sql.append(" from conceptotipopersonal ctp, concepto c");
      sql.append(" where ctp.id_tipo_personal = ?");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and c.beneficio = ?");
      sql.append(" order by c.cod_concepto");

      stConceptos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptos.setLong(1, idTipoPersonal);
      stConceptos.setString(2, beneficio);
      rsConceptos = stConceptos.executeQuery();

      while (rsConceptos.next())
      {
        col.add(Long.valueOf(String.valueOf(rsConceptos.getLong("id_concepto_tipo_personal"))));
        col.add(rsConceptos.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsConceptos != null) try {
          rsConceptos.close();
        } catch (Exception localException3) {
        } if (stConceptos != null) try {
          stConceptos.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findByDistribucion(long idTipoPersonal, String distribucion) throws Exception { PersistenceManager pm = PMThread.getPM();
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && distribucion == pDistribucion";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pDistribucion");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pDistribucion", distribucion);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }

  public Collection findByRetroactivo(long idTipoPersonal, String retroactivo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.retroactivo == pRetroactivo";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pRetroactivo");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pRetroactivo", retroactivo);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoTipoPersonal;
  }
}