package sigefirrhh.base.definiciones;

import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class TipoPersonalNoGenBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(TipoPersonalNoGenBeanBusiness.class.getName());

  public Collection findByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina";

    Query query = pm.newQuery(TipoPersonal.class, filter);

    query.declareParameters("long pIdGrupoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }

  public Collection findByCategoriaPresupuesto(long idCategoriaPresupuesto) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "clasificacionPersonal.categoriaPresupuesto.idCategoriaPresupuesto == pIdCategoriaPresupuesto";

    Query query = pm.newQuery(TipoPersonal.class, filter);

    query.declareParameters("long pIdCategoriaPresupuesto");
    HashMap parameters = new HashMap();

    parameters.put("pIdCategoriaPresupuesto", new Long(idCategoriaPresupuesto));

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }

  public Collection findWithSeguridadAndRelacionPersonal(long idUsuario, long idOrganismo, String administrador, String codRelacionPersonal) throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      if (administrador.equals("N")) {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, usuariotipopersonal utp, clasificacionpersonal cp, relacionpersonal rp ");
        sql.append(" where utp.id_usuario = ?");
        sql.append(" and tp.id_organismo = ?");
        sql.append(" and utp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and tp.id_clasificacion_personal = cp.id_clasificacion_personal");
        sql.append(" and cp.id_relacion_personal = rp.id_relacion_personal");
        sql.append(" and rp.id_relacion_personal = ?");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setLong(2, idOrganismo);
        stRegistros.setString(3, codRelacionPersonal);
        rsRegistros = stRegistros.executeQuery();
      } else {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp,  clasificacionpersonal cp, relacionpersonal rp");
        sql.append(" where ");
        sql.append(" tp.id_organismo = ?");
        sql.append(" and tp.id_clasificacion_personal = cp.id_clasificacion_personal");
        sql.append(" and cp.id_relacion_personal = rp.id_relacion_personal");
        sql.append(" and rp.id_relacion_personal = ?");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idOrganismo);
        stRegistros.setString(2, codRelacionPersonal);
        rsRegistros = stRegistros.executeQuery();
      }

      while (rsRegistros.next()) {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_tipo_personal"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findWithSeguridadRelacionCategoriaAprobacionMpd(long idUsuario, long idOrganismo, String administrador, String codRelacionPersonal, String codCategoriaPersonal, String aprobacionMpd) throws Exception { Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      if (administrador.equals("N")) {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, usuariotipopersonal utp, clasificacionpersonal cp, relacionpersonal rp, categoriapersonal cat ");
        sql.append(" where utp.id_usuario = ?");
        sql.append(" and tp.id_organismo = ?");
        sql.append(" and utp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and tp.id_clasificacion_personal = cp.id_clasificacion_personal");
        sql.append(" and cp.id_relacion_personal = rp.id_relacion_personal");
        sql.append(" and cp.id_categoria_personal = cat.id_categoria_personal");
        sql.append(" and rp.cod_relacion = ?");
        sql.append(" and cat.cod_categoria = ?");
        sql.append(" and tp.aprobacion_mpd = ?");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setLong(2, idOrganismo);
        stRegistros.setString(3, codRelacionPersonal);
        stRegistros.setString(4, codCategoriaPersonal);
        stRegistros.setString(5, aprobacionMpd);
        rsRegistros = stRegistros.executeQuery();
      } else {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp,  clasificacionpersonal cp, relacionpersonal rp, categoriapersonal cat ");
        sql.append(" where ");
        sql.append(" tp.id_organismo = ?");
        sql.append(" and tp.id_clasificacion_personal = cp.id_clasificacion_personal");
        sql.append(" and cp.id_relacion_personal = rp.id_relacion_personal");
        sql.append(" and cp.id_categoria_personal = cat.id_categoria_personal");
        sql.append(" and rp.cod_relacion = ?");
        sql.append(" and cat.cod_categoria = ?");
        sql.append(" and tp.aprobacion_mpd = ?");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idOrganismo);
        stRegistros.setString(2, codRelacionPersonal);
        stRegistros.setString(3, codCategoriaPersonal);
        stRegistros.setString(4, aprobacionMpd);
        rsRegistros = stRegistros.executeQuery();
      }

      while (rsRegistros.next()) {
        this.log.error("tiene");
        this.log.error(rsRegistros.getString("descripcion"));
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_tipo_personal"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    } } 
  public Collection findWithSeguridad(long idUsuario, long idOrganismo, String administrador) throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      if (administrador.equals("N")) {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, usuariotipopersonal utp");
        sql.append(" where utp.id_usuario = ?");
        sql.append(" and tp.id_organismo = ?");
        sql.append(" and utp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setLong(2, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      } else {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp ");
        sql.append(" where ");
        sql.append(" tp.id_organismo = ?");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      }

      while (rsRegistros.next()) {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_tipo_personal"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findByUsuarioAndOrganismo(long idUsuario, long idOrganismo, String administrador) throws Exception { PersistenceManager pm = PMThread.getPM();

    HashMap parameters = new HashMap();
    Query query;
    if (administrador.equals("N"))
    {
      String filter = "organismo.idOrganismo == pIdOrganismo && utp.tipoPersonal.idTipoPersonal == idTipoPersonal && utp.usuario.idUsuario == pIdUsuario";

      Query query = pm.newQuery(TipoPersonal.class, filter);

      query.declareImports("import sigefirrhh.sistema.UsuarioTipoPersonal");
      query.declareVariables("UsuarioTipoPersonal utp");

      query.declareParameters("long pIdUsuario, long pIdOrganismo");

      parameters.put("pIdUsuario", new Long(idUsuario));
      parameters.put("pIdOrganismo", new Long(idOrganismo));
    }
    else {
      String filter = "organismo.idOrganismo == pIdOrganismo ";

      query = pm.newQuery(TipoPersonal.class, filter);

      query.declareParameters("long pIdOrganismo");

      parameters.put("pIdOrganismo", new Long(idOrganismo));
    }

    query.setOrdering("codTipoPersonal ascending");
    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }

  public Collection findByPrestaciones(long idOrganismo, long idUsuario, String administrador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String calculaPrestaciones = "S";
    String filter = "calculaPrestaciones == pCalculaPrestaciones &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(TipoPersonal.class, filter);

    query.declareParameters("String pCalculaPrestaciones, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCalculaPrestaciones", new String(calculaPrestaciones));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codTipoPersonal ascending");

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }

  public Collection findByArc(long idOrganismo, long idUsuario, String administrador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = " organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(TipoPersonal.class, filter);

    query.declareParameters(" long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codTipoPersonal ascending");

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }

  public Collection findForContratados(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo && (clasificacionPersonal.idClasificacionPersonal == 2 || clasificacionPersonal.idClasificacionPersonal == 12 || clasificacionPersonal.idClasificacionPersonal == 22 || clasificacionPersonal.idClasificacionPersonal == 52)";

    Query query = pm.newQuery(TipoPersonal.class, filter);
    query.declareParameters("long pIdOrganismo");

    HashMap parameters = new HashMap();
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }

  public Collection findByIdRegistro(long idRegistro) throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    StringBuffer sql = new StringBuffer();

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append(" select tp.id_tipo_personal as id, (tp.cod_tipo_personal || ' - ' || tp.nombre)  as descripcion");
      sql.append(" from tipopersonal tp, registropersonal rc");
      sql.append(" where rc.id_registro = ?");
      sql.append(" and rc.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" order by tp.cod_tipo_personal");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idRegistro);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findPensionadoJubilado() throws Exception { Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append(" select tp.id_tipo_personal as id, (tp.cod_tipo_personal || ' - ' || tp.nombre)  as descripcion");
      sql.append(" from tipopersonal tp, clasificacionpersonal cp");
      sql.append(" where cp.id_clasificacion_personal = tp.id_clasificacion_personal");
      sql.append(" and cp.id_relacion_personal in (4,5)");
      sql.append(" order by tp.cod_tipo_personal");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findForJubilado()
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append(" select tp.id_tipo_personal as id, (tp.cod_tipo_personal || ' - ' || tp.nombre)  as descripcion");
      sql.append(" from tipopersonal tp, clasificacionpersonal cp");
      sql.append(" where cp.id_clasificacion_personal = tp.id_clasificacion_personal");
      sql.append(" and cp.id_relacion_personal in (4)");
      sql.append(" order by tp.cod_tipo_personal");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findForPensionado() throws Exception {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append(" select tp.id_tipo_personal as id, (tp.cod_tipo_personal || ' - ' || tp.nombre)  as descripcion");
      sql.append(" from tipopersonal tp, clasificacionpersonal cp");
      sql.append(" where cp.id_clasificacion_personal = tp.id_clasificacion_personal");
      sql.append(" and cp.id_relacion_personal in (4)");
      sql.append(" order by tp.cod_tipo_personal");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }

  public Collection findByConcepto(long idUsuario, long idOrganismo, String administrador, String codConcepto)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();

    this.log.error("iOrganismo " + idOrganismo);
    this.log.error("admin " + administrador);
    this.log.error("codConcepto " + codConcepto);
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      if (administrador.equals("N")) {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, usuariotipopersonal utp, conceptotipopersonal ctp, concepto c");
        sql.append(" where utp.id_usuario = ?");
        sql.append(" and tp.id_organismo = ?");
        sql.append(" and utp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and ctp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and ctp.id_concepto = c.id_concepto");
        sql.append(" and c.cod_concepto = ?");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setLong(2, idOrganismo);
        stRegistros.setString(3, codConcepto);
        rsRegistros = stRegistros.executeQuery();
      } else {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, conceptotipopersonal ctp, concepto c ");
        sql.append(" where ");
        sql.append(" tp.id_organismo = ?");
        sql.append(" and ctp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and ctp.id_concepto = c.id_concepto");
        sql.append(" and c.cod_concepto = ?");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idOrganismo);
        stRegistros.setString(2, codConcepto);
        rsRegistros = stRegistros.executeQuery();
      }

      while (rsRegistros.next()) {
        this.log.warn("tp " + rsRegistros.getString("descripcion"));
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_tipo_personal"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findByPersonal(long idPersonal, String estatus) throws Exception {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
      sql.append(" from tipopersonal tp");
      sql.append(" where id_tipo_personal in (");
      sql.append(" select id_tipo_personal from trabajador");
      sql.append(" where id_personal  = ?");
      sql.append(" and estatus = ?)");
      sql.append(" order by tp.cod_tipo_personal");
      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idPersonal);
      stRegistros.setString(2, estatus);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next()) {
        this.log.error("tp " + rsRegistros.getString("descripcion"));
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_tipo_personal"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findWithSeguridadDocenteIngreso(long idUsuario, long idOrganismo, String administrador) throws Exception { Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      if (administrador.equals("N")) {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, usuariotipopersonal utp, clasificacionpersonal cp");
        sql.append(" where utp.id_usuario = ?");
        sql.append(" and tp.id_organismo = ?");
        sql.append(" and utp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and tp.id_clasificacion_personal = cp.id_clasificacion_personal");
        sql.append(" and cp.id_categoria_personal = 3");
        sql.append(" and cp.id_relacion_personal in (1,2,6,9)");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setLong(2, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      } else {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, clasificacionpersonal cp");
        sql.append(" where ");
        sql.append(" tp.id_organismo = ?");
        sql.append(" and tp.id_clasificacion_personal = cp.id_clasificacion_personal");
        sql.append(" and cp.id_categoria_personal = 3");
        sql.append(" and cp.id_relacion_personal in (1,2,6,9)");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      }

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_tipo_personal"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    } } 
  public Collection findWithSeguridadForViejoRegimen(long idUsuario, long idOrganismo, String administrador)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      if (administrador.equals("N")) {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, usuariotipopersonal utp, ");
        sql.append(" parametrovarios pv, clasificacionpersonal cp, relacionpersonal rp");
        sql.append(" where utp.id_usuario = ?");
        sql.append(" and tp.id_organismo = ?");
        sql.append(" and utp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and pv.id_tipo_personal = tp.id_tipo_personal ");
        sql.append(" and pv.regimen_derogado_procesado = 'N' ");
        sql.append(" and tp.deuda_regimen_derogado = 'S' ");
        sql.append(" and tp.id_clasificacion_personal = cp.id_clasificacion_personal");
        sql.append(" and cp.id_relacion_personal = rp.id_relacion_personal");
        sql.append(" and rp.cod_relacion = '1'");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setLong(2, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      } else {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, ");
        sql.append(" parametrovarios pv, clasificacionpersonal cp, relacionpersonal rp");
        sql.append(" where ");
        sql.append(" tp.id_organismo = ?");
        sql.append(" and pv.id_tipo_personal = tp.id_tipo_personal ");
        sql.append(" and pv.regimen_derogado_procesado = 'N' ");
        sql.append(" and tp.deuda_regimen_derogado = 'S' ");
        sql.append(" and tp.id_clasificacion_personal = cp.id_clasificacion_personal");
        sql.append(" and cp.id_relacion_personal = rp.id_relacion_personal");
        sql.append(" and rp.cod_relacion = '1'");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      }

      while (rsRegistros.next()) {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_tipo_personal"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findWithSeguridadForInteresAdicional(long idUsuario, long idOrganismo, String administrador, String estatus) throws Exception {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      if (administrador.equals("N")) {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, usuariotipopersonal utp, ");
        sql.append(" parametrovarios pv, clasificacionpersonal cp, relacionpersonal rp");
        sql.append(" where utp.id_usuario = ?");
        sql.append(" and tp.id_organismo = ?");
        sql.append(" and utp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and pv.id_tipo_personal = tp.id_tipo_personal ");
        sql.append(" and pv.regimen_derogado_procesado = 'S' ");
        sql.append(" and pv.intereses_adicionales = ? ");
        sql.append(" and tp.deuda_regimen_derogado = 'S' ");
        sql.append(" and tp.id_clasificacion_personal = cp.id_clasificacion_personal");
        sql.append(" and cp.id_relacion_personal = rp.id_relacion_personal");
        sql.append(" and rp.cod_relacion = '1'");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setLong(2, idOrganismo);
        stRegistros.setString(3, estatus);

        rsRegistros = stRegistros.executeQuery();
      } else {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, ");
        sql.append(" parametrovarios pv, clasificacionpersonal cp, relacionpersonal rp");
        sql.append(" where ");
        sql.append(" tp.id_organismo = ?");
        sql.append(" and pv.id_tipo_personal = tp.id_tipo_personal ");
        sql.append(" and pv.regimen_derogado_procesado = 'S' ");
        sql.append(" and pv.intereses_adicionales = ? ");
        sql.append(" and tp.deuda_regimen_derogado = 'S' ");
        sql.append(" and tp.id_clasificacion_personal = cp.id_clasificacion_personal");
        sql.append(" and cp.id_relacion_personal = rp.id_relacion_personal");
        sql.append(" and rp.cod_relacion = '1'");
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);

        stRegistros.setLong(1, idOrganismo);
        stRegistros.setString(2, estatus);
        rsRegistros = stRegistros.executeQuery();
      }

      while (rsRegistros.next()) {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_tipo_personal"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findWithSeguridadAndCriterio(long idUsuario, long idOrganismo, String administrador, String criterio) throws Exception { Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      if (administrador.equals("N")) {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp, usuariotipopersonal utp");
        sql.append(" where utp.id_usuario = ?");
        sql.append(" and tp.id_organismo = ?");
        sql.append(" and utp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and " + criterio);
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setLong(2, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      } else {
        sql.append("select tp.id_tipo_personal, (tp.nombre || ' ' || tp.cod_tipo_personal)  as descripcion  ");
        sql.append(" from tipopersonal tp ");
        sql.append(" where ");
        sql.append(" tp.id_organismo = ?");
        sql.append(" and " + criterio);
        sql.append(" order by tp.cod_tipo_personal");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      }

      while (rsRegistros.next()) {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_tipo_personal"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}