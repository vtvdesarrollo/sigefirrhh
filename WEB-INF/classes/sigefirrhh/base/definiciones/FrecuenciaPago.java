package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class FrecuenciaPago
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idFrecuenciaPago;
  private int codFrecuenciaPago;
  private String nombre;
  private String reservado;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codFrecuenciaPago", "idFrecuenciaPago", "nombre", "organismo", "reservado" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaPago"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new FrecuenciaPago());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public FrecuenciaPago()
  {
    jdoSetreservado(this, "N");
  }

  public String toString()
  {
    return jdoGetcodFrecuenciaPago(this) + "  -  " + 
      jdoGetnombre(this);
  }

  public int getCodFrecuenciaPago()
  {
    return jdoGetcodFrecuenciaPago(this);
  }

  public long getIdFrecuenciaPago()
  {
    return jdoGetidFrecuenciaPago(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public String getReservado()
  {
    return jdoGetreservado(this);
  }

  public void setCodFrecuenciaPago(int i)
  {
    jdoSetcodFrecuenciaPago(this, i);
  }

  public void setIdFrecuenciaPago(long l)
  {
    jdoSetidFrecuenciaPago(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setReservado(String string)
  {
    jdoSetreservado(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    FrecuenciaPago localFrecuenciaPago = new FrecuenciaPago();
    localFrecuenciaPago.jdoFlags = 1;
    localFrecuenciaPago.jdoStateManager = paramStateManager;
    return localFrecuenciaPago;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    FrecuenciaPago localFrecuenciaPago = new FrecuenciaPago();
    localFrecuenciaPago.jdoCopyKeyFieldsFromObjectId(paramObject);
    localFrecuenciaPago.jdoFlags = 1;
    localFrecuenciaPago.jdoStateManager = paramStateManager;
    return localFrecuenciaPago;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codFrecuenciaPago);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idFrecuenciaPago);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.reservado);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codFrecuenciaPago = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idFrecuenciaPago = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.reservado = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(FrecuenciaPago paramFrecuenciaPago, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramFrecuenciaPago == null)
        throw new IllegalArgumentException("arg1");
      this.codFrecuenciaPago = paramFrecuenciaPago.codFrecuenciaPago;
      return;
    case 1:
      if (paramFrecuenciaPago == null)
        throw new IllegalArgumentException("arg1");
      this.idFrecuenciaPago = paramFrecuenciaPago.idFrecuenciaPago;
      return;
    case 2:
      if (paramFrecuenciaPago == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramFrecuenciaPago.nombre;
      return;
    case 3:
      if (paramFrecuenciaPago == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramFrecuenciaPago.organismo;
      return;
    case 4:
      if (paramFrecuenciaPago == null)
        throw new IllegalArgumentException("arg1");
      this.reservado = paramFrecuenciaPago.reservado;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof FrecuenciaPago))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    FrecuenciaPago localFrecuenciaPago = (FrecuenciaPago)paramObject;
    if (localFrecuenciaPago.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localFrecuenciaPago, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new FrecuenciaPagoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new FrecuenciaPagoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FrecuenciaPagoPK))
      throw new IllegalArgumentException("arg1");
    FrecuenciaPagoPK localFrecuenciaPagoPK = (FrecuenciaPagoPK)paramObject;
    localFrecuenciaPagoPK.idFrecuenciaPago = this.idFrecuenciaPago;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FrecuenciaPagoPK))
      throw new IllegalArgumentException("arg1");
    FrecuenciaPagoPK localFrecuenciaPagoPK = (FrecuenciaPagoPK)paramObject;
    this.idFrecuenciaPago = localFrecuenciaPagoPK.idFrecuenciaPago;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FrecuenciaPagoPK))
      throw new IllegalArgumentException("arg2");
    FrecuenciaPagoPK localFrecuenciaPagoPK = (FrecuenciaPagoPK)paramObject;
    localFrecuenciaPagoPK.idFrecuenciaPago = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FrecuenciaPagoPK))
      throw new IllegalArgumentException("arg2");
    FrecuenciaPagoPK localFrecuenciaPagoPK = (FrecuenciaPagoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localFrecuenciaPagoPK.idFrecuenciaPago);
  }

  private static final int jdoGetcodFrecuenciaPago(FrecuenciaPago paramFrecuenciaPago)
  {
    if (paramFrecuenciaPago.jdoFlags <= 0)
      return paramFrecuenciaPago.codFrecuenciaPago;
    StateManager localStateManager = paramFrecuenciaPago.jdoStateManager;
    if (localStateManager == null)
      return paramFrecuenciaPago.codFrecuenciaPago;
    if (localStateManager.isLoaded(paramFrecuenciaPago, jdoInheritedFieldCount + 0))
      return paramFrecuenciaPago.codFrecuenciaPago;
    return localStateManager.getIntField(paramFrecuenciaPago, jdoInheritedFieldCount + 0, paramFrecuenciaPago.codFrecuenciaPago);
  }

  private static final void jdoSetcodFrecuenciaPago(FrecuenciaPago paramFrecuenciaPago, int paramInt)
  {
    if (paramFrecuenciaPago.jdoFlags == 0)
    {
      paramFrecuenciaPago.codFrecuenciaPago = paramInt;
      return;
    }
    StateManager localStateManager = paramFrecuenciaPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaPago.codFrecuenciaPago = paramInt;
      return;
    }
    localStateManager.setIntField(paramFrecuenciaPago, jdoInheritedFieldCount + 0, paramFrecuenciaPago.codFrecuenciaPago, paramInt);
  }

  private static final long jdoGetidFrecuenciaPago(FrecuenciaPago paramFrecuenciaPago)
  {
    return paramFrecuenciaPago.idFrecuenciaPago;
  }

  private static final void jdoSetidFrecuenciaPago(FrecuenciaPago paramFrecuenciaPago, long paramLong)
  {
    StateManager localStateManager = paramFrecuenciaPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaPago.idFrecuenciaPago = paramLong;
      return;
    }
    localStateManager.setLongField(paramFrecuenciaPago, jdoInheritedFieldCount + 1, paramFrecuenciaPago.idFrecuenciaPago, paramLong);
  }

  private static final String jdoGetnombre(FrecuenciaPago paramFrecuenciaPago)
  {
    if (paramFrecuenciaPago.jdoFlags <= 0)
      return paramFrecuenciaPago.nombre;
    StateManager localStateManager = paramFrecuenciaPago.jdoStateManager;
    if (localStateManager == null)
      return paramFrecuenciaPago.nombre;
    if (localStateManager.isLoaded(paramFrecuenciaPago, jdoInheritedFieldCount + 2))
      return paramFrecuenciaPago.nombre;
    return localStateManager.getStringField(paramFrecuenciaPago, jdoInheritedFieldCount + 2, paramFrecuenciaPago.nombre);
  }

  private static final void jdoSetnombre(FrecuenciaPago paramFrecuenciaPago, String paramString)
  {
    if (paramFrecuenciaPago.jdoFlags == 0)
    {
      paramFrecuenciaPago.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramFrecuenciaPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaPago.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramFrecuenciaPago, jdoInheritedFieldCount + 2, paramFrecuenciaPago.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(FrecuenciaPago paramFrecuenciaPago)
  {
    StateManager localStateManager = paramFrecuenciaPago.jdoStateManager;
    if (localStateManager == null)
      return paramFrecuenciaPago.organismo;
    if (localStateManager.isLoaded(paramFrecuenciaPago, jdoInheritedFieldCount + 3))
      return paramFrecuenciaPago.organismo;
    return (Organismo)localStateManager.getObjectField(paramFrecuenciaPago, jdoInheritedFieldCount + 3, paramFrecuenciaPago.organismo);
  }

  private static final void jdoSetorganismo(FrecuenciaPago paramFrecuenciaPago, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramFrecuenciaPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaPago.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramFrecuenciaPago, jdoInheritedFieldCount + 3, paramFrecuenciaPago.organismo, paramOrganismo);
  }

  private static final String jdoGetreservado(FrecuenciaPago paramFrecuenciaPago)
  {
    if (paramFrecuenciaPago.jdoFlags <= 0)
      return paramFrecuenciaPago.reservado;
    StateManager localStateManager = paramFrecuenciaPago.jdoStateManager;
    if (localStateManager == null)
      return paramFrecuenciaPago.reservado;
    if (localStateManager.isLoaded(paramFrecuenciaPago, jdoInheritedFieldCount + 4))
      return paramFrecuenciaPago.reservado;
    return localStateManager.getStringField(paramFrecuenciaPago, jdoInheritedFieldCount + 4, paramFrecuenciaPago.reservado);
  }

  private static final void jdoSetreservado(FrecuenciaPago paramFrecuenciaPago, String paramString)
  {
    if (paramFrecuenciaPago.jdoFlags == 0)
    {
      paramFrecuenciaPago.reservado = paramString;
      return;
    }
    StateManager localStateManager = paramFrecuenciaPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaPago.reservado = paramString;
      return;
    }
    localStateManager.setStringField(paramFrecuenciaPago, jdoInheritedFieldCount + 4, paramFrecuenciaPago.reservado, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}