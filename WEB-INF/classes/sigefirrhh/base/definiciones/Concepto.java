package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class Concepto
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO_UNIDAD;
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_TIPO;
  private long idConcepto;
  private String codConcepto;
  private String descripcion;
  private String unidad;
  private double valor;
  private String reservado;
  private String sueldoIntegral;
  private String sueldoBasico;
  private String ajusteSueldo;
  private String compensacion;
  private String primasCargo;
  private String primasTrabajador;
  private String gravable;
  private String tipoPrestamo;
  private String deduccionSindicato;
  private String deduccionGremio;
  private String sobretiempo;
  private String beneficio;
  private String jubilacion;
  private String anticipo;
  private String deduccionCaja;
  private Concepto conceptoCaja;
  private String aportePatronal;
  private Concepto conceptoAporte;
  private String retroactivo;
  private Concepto conceptoRetroactivo;
  private String ausencia;
  private Concepto conceptoAusencia;
  private String retroactivoAnterior;
  private Concepto conceptoRetroactivoAnterior;
  private String embargo;
  private String prestacionesNr;
  private String prestacionesVr;
  private String onapre;
  private String recalculo;
  private String anual;
  private double ocurrencia;
  private String movimiento;
  private String proyeccion;
  private String baseLegal;
  private Organismo organismo;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "ajusteSueldo", "anticipo", "anual", "aportePatronal", "ausencia", "baseLegal", "beneficio", "codConcepto", "compensacion", "conceptoAporte", "conceptoAusencia", "conceptoCaja", "conceptoRetroactivo", "conceptoRetroactivoAnterior", "deduccionCaja", "deduccionGremio", "deduccionSindicato", "descripcion", "embargo", "gravable", "idConcepto", "idSitp", "jubilacion", "movimiento", "ocurrencia", "onapre", "organismo", "prestacionesNr", "prestacionesVr", "primasCargo", "primasTrabajador", "proyeccion", "recalculo", "reservado", "retroactivo", "retroactivoAnterior", "sobretiempo", "sueldoBasico", "sueldoIntegral", "tiempoSitp", "tipoPrestamo", "unidad", "valor" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 26, 26, 26, 26, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Concepto());

    LISTA_TIPO_UNIDAD = 
      new LinkedHashMap();
    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_TIPO_UNIDAD.put("H", "HORAS");
    LISTA_TIPO_UNIDAD.put("D", "DIAS");
    LISTA_TIPO_UNIDAD.put("M", "MONTO");
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_TIPO.put("S", "PRESTAMO");
    LISTA_TIPO.put("A", "APORTE PATRONAL");
    LISTA_TIPO.put("L", "ALICUOTA");
    LISTA_TIPO.put("R", "RETROACTIVO");
    LISTA_TIPO.put("T", "TICKETS");
    LISTA_TIPO.put("B", "BECA");
    LISTA_TIPO.put("C", "ANIVERSARIO");
    LISTA_TIPO.put("H", "DEDUCCION CAJA AHORRO");
    LISTA_TIPO.put("D", "DESCUENTO POR AUSENCIA");
    LISTA_TIPO.put("N", "OTRO");
  }

  public Concepto()
  {
    jdoSetunidad(this, "M");

    jdoSetvalor(this, 0.0D);

    jdoSetreservado(this, "N");

    jdoSetsueldoIntegral(this, "N");

    jdoSetsueldoBasico(this, "N");

    jdoSetajusteSueldo(this, "N");

    jdoSetcompensacion(this, "N");

    jdoSetprimasCargo(this, "N");

    jdoSetprimasTrabajador(this, "N");

    jdoSetgravable(this, "N");

    jdoSettipoPrestamo(this, "N");

    jdoSetdeduccionSindicato(this, "N");

    jdoSetdeduccionGremio(this, "N");

    jdoSetsobretiempo(this, "N");

    jdoSetbeneficio(this, "N");

    jdoSetjubilacion(this, "N");

    jdoSetanticipo(this, "N");

    jdoSetdeduccionCaja(this, "N");

    jdoSetaportePatronal(this, "N");

    jdoSetretroactivo(this, "N");

    jdoSetausencia(this, "N");

    jdoSetretroactivoAnterior(this, "N");

    jdoSetembargo(this, "N");

    jdoSetprestacionesNr(this, "N");

    jdoSetprestacionesVr(this, "N");

    jdoSetonapre(this, "N");

    jdoSetrecalculo(this, "N");

    jdoSetanual(this, "N");

    jdoSetocurrencia(this, 1.0D);

    jdoSetmovimiento(this, "N");

    jdoSetproyeccion(this, "N");
  }

  public String toString()
  {
    return jdoGetcodConcepto(this) + "  -  " + 
      jdoGetdescripcion(this);
  }

  public String getAnticipo()
  {
    return jdoGetanticipo(this);
  }

  public String getAnual()
  {
    return jdoGetanual(this);
  }

  public String getAportePatronal()
  {
    return jdoGetaportePatronal(this);
  }

  public String getBaseLegal()
  {
    return jdoGetbaseLegal(this);
  }

  public String getBeneficio()
  {
    return jdoGetbeneficio(this);
  }

  public String getCodConcepto()
  {
    return jdoGetcodConcepto(this);
  }

  public String getCompensacion()
  {
    return jdoGetcompensacion(this);
  }

  public String getDeduccionCaja()
  {
    return jdoGetdeduccionCaja(this);
  }

  public String getDeduccionGremio()
  {
    return jdoGetdeduccionGremio(this);
  }

  public String getDeduccionSindicato()
  {
    return jdoGetdeduccionSindicato(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public String getEmbargo()
  {
    return jdoGetembargo(this);
  }

  public String getGravable()
  {
    return jdoGetgravable(this);
  }

  public long getIdConcepto()
  {
    return jdoGetidConcepto(this);
  }

  public String getJubilacion()
  {
    return jdoGetjubilacion(this);
  }

  public String getMovimiento()
  {
    return jdoGetmovimiento(this);
  }

  public double getOcurrencia()
  {
    return jdoGetocurrencia(this);
  }

  public String getOnapre()
  {
    return jdoGetonapre(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public String getPrestacionesNr()
  {
    return jdoGetprestacionesNr(this);
  }

  public String getPrestacionesVr()
  {
    return jdoGetprestacionesVr(this);
  }

  public String getPrimasCargo()
  {
    return jdoGetprimasCargo(this);
  }

  public String getPrimasTrabajador()
  {
    return jdoGetprimasTrabajador(this);
  }

  public String getProyeccion()
  {
    return jdoGetproyeccion(this);
  }

  public String getRecalculo()
  {
    return jdoGetrecalculo(this);
  }

  public String getReservado()
  {
    return jdoGetreservado(this);
  }

  public String getSobretiempo()
  {
    return jdoGetsobretiempo(this);
  }

  public String getSueldoBasico()
  {
    return jdoGetsueldoBasico(this);
  }

  public String getUnidad()
  {
    return jdoGetunidad(this);
  }

  public double getValor()
  {
    return jdoGetvalor(this);
  }

  public void setAnticipo(String string)
  {
    jdoSetanticipo(this, string);
  }

  public void setAnual(String string)
  {
    jdoSetanual(this, string);
  }

  public void setAportePatronal(String string)
  {
    jdoSetaportePatronal(this, string);
  }

  public void setBaseLegal(String string)
  {
    jdoSetbaseLegal(this, string);
  }

  public void setBeneficio(String string)
  {
    jdoSetbeneficio(this, string);
  }

  public void setCodConcepto(String string)
  {
    jdoSetcodConcepto(this, string);
  }

  public void setCompensacion(String string)
  {
    jdoSetcompensacion(this, string);
  }

  public void setDeduccionCaja(String string)
  {
    jdoSetdeduccionCaja(this, string);
  }

  public void setDeduccionGremio(String string)
  {
    jdoSetdeduccionGremio(this, string);
  }

  public void setDeduccionSindicato(String string)
  {
    jdoSetdeduccionSindicato(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setEmbargo(String string)
  {
    jdoSetembargo(this, string);
  }

  public void setGravable(String string)
  {
    jdoSetgravable(this, string);
  }

  public void setIdConcepto(long l)
  {
    jdoSetidConcepto(this, l);
  }

  public void setJubilacion(String string)
  {
    jdoSetjubilacion(this, string);
  }

  public void setMovimiento(String string)
  {
    jdoSetmovimiento(this, string);
  }

  public void setOcurrencia(double d)
  {
    jdoSetocurrencia(this, d);
  }

  public void setOnapre(String string)
  {
    jdoSetonapre(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setPrestacionesNr(String string)
  {
    jdoSetprestacionesNr(this, string);
  }

  public void setPrestacionesVr(String string)
  {
    jdoSetprestacionesVr(this, string);
  }

  public void setPrimasCargo(String string)
  {
    jdoSetprimasCargo(this, string);
  }

  public void setPrimasTrabajador(String string)
  {
    jdoSetprimasTrabajador(this, string);
  }

  public void setProyeccion(String string)
  {
    jdoSetproyeccion(this, string);
  }

  public void setRecalculo(String string)
  {
    jdoSetrecalculo(this, string);
  }

  public void setReservado(String string)
  {
    jdoSetreservado(this, string);
  }

  public void setSobretiempo(String string)
  {
    jdoSetsobretiempo(this, string);
  }

  public void setSueldoBasico(String string)
  {
    jdoSetsueldoBasico(this, string);
  }

  public void setUnidad(String string)
  {
    jdoSetunidad(this, string);
  }

  public void setValor(double d)
  {
    jdoSetvalor(this, d);
  }

  public String getTipoPrestamo()
  {
    return jdoGettipoPrestamo(this);
  }

  public void setTipoPrestamo(String string)
  {
    jdoSettipoPrestamo(this, string);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public Concepto getConceptoAporte()
  {
    return jdoGetconceptoAporte(this);
  }

  public void setConceptoAporte(Concepto concepto)
  {
    jdoSetconceptoAporte(this, concepto);
  }

  public String getAjusteSueldo()
  {
    return jdoGetajusteSueldo(this);
  }

  public void setAjusteSueldo(String ajusteSueldo)
  {
    jdoSetajusteSueldo(this, ajusteSueldo);
  }

  public String getSueldoIntegral()
  {
    return jdoGetsueldoIntegral(this);
  }

  public void setSueldoIntegral(String string)
  {
    jdoSetsueldoIntegral(this, string);
  }

  public Concepto getConceptoRetroactivo()
  {
    return jdoGetconceptoRetroactivo(this);
  }

  public String getRetroactivo()
  {
    return jdoGetretroactivo(this);
  }

  public String getAusencia() {
    return jdoGetausencia(this);
  }

  public void setAusencia(String ausencia)
  {
    jdoSetausencia(this, ausencia);
  }

  public Concepto getConceptoAusencia()
  {
    return jdoGetconceptoAusencia(this);
  }

  public void setConceptoAusencia(Concepto conceptoAusencia)
  {
    jdoSetconceptoAusencia(this, conceptoAusencia);
  }

  public void setConceptoRetroactivo(Concepto concepto)
  {
    jdoSetconceptoRetroactivo(this, concepto);
  }

  public void setRetroactivo(String string) {
    jdoSetretroactivo(this, string);
  }

  public Concepto getConceptoCaja() {
    return jdoGetconceptoCaja(this);
  }
  public void setConceptoCaja(Concepto conceptoCaja) {
    jdoSetconceptoCaja(this, conceptoCaja);
  }

  public Concepto getConceptoRetroactivoAnterior()
  {
    return jdoGetconceptoRetroactivoAnterior(this);
  }

  public void setConceptoRetroactivoAnterior(Concepto conceptoRetroactivoAnterior)
  {
    jdoSetconceptoRetroactivoAnterior(this, conceptoRetroactivoAnterior);
  }

  public String getRetroactivoAnterior()
  {
    return jdoGetretroactivoAnterior(this);
  }

  public void setRetroactivoAnterior(String retroactivoAnterior)
  {
    jdoSetretroactivoAnterior(this, retroactivoAnterior);
  }

  public boolean isProtegido() {
    if (jdoGetreservado(this).equals("S")) {
      return true;
    }
    return false;
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 43;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Concepto localConcepto = new Concepto();
    localConcepto.jdoFlags = 1;
    localConcepto.jdoStateManager = paramStateManager;
    return localConcepto;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Concepto localConcepto = new Concepto();
    localConcepto.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConcepto.jdoFlags = 1;
    localConcepto.jdoStateManager = paramStateManager;
    return localConcepto;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.ajusteSueldo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anticipo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anual);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aportePatronal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.ausencia);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.baseLegal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.beneficio);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codConcepto);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.compensacion);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoAporte);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoAusencia);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoCaja);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoRetroactivo);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoRetroactivoAnterior);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.deduccionCaja);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.deduccionGremio);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.deduccionSindicato);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.embargo);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gravable);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConcepto);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.jubilacion);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.movimiento);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.ocurrencia);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.onapre);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.prestacionesNr);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.prestacionesVr);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primasCargo);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primasTrabajador);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.proyeccion);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.recalculo);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.reservado);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.retroactivo);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.retroactivoAnterior);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sobretiempo);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sueldoBasico);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sueldoIntegral);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoPrestamo);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.unidad);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.valor);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ajusteSueldo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anticipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anual = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aportePatronal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ausencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseLegal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.beneficio = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codConcepto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoAporte = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoAusencia = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoCaja = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoRetroactivo = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoRetroactivoAnterior = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.deduccionCaja = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.deduccionGremio = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.deduccionSindicato = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.embargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gravable = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConcepto = localStateManager.replacingLongField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.jubilacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.movimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ocurrencia = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.onapre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.prestacionesNr = localStateManager.replacingStringField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.prestacionesVr = localStateManager.replacingStringField(this, paramInt);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasTrabajador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.proyeccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.recalculo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.reservado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retroactivo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retroactivoAnterior = localStateManager.replacingStringField(this, paramInt);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sobretiempo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoBasico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoIntegral = localStateManager.replacingStringField(this, paramInt);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPrestamo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.valor = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Concepto paramConcepto, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.ajusteSueldo = paramConcepto.ajusteSueldo;
      return;
    case 1:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.anticipo = paramConcepto.anticipo;
      return;
    case 2:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.anual = paramConcepto.anual;
      return;
    case 3:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.aportePatronal = paramConcepto.aportePatronal;
      return;
    case 4:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.ausencia = paramConcepto.ausencia;
      return;
    case 5:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.baseLegal = paramConcepto.baseLegal;
      return;
    case 6:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.beneficio = paramConcepto.beneficio;
      return;
    case 7:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.codConcepto = paramConcepto.codConcepto;
      return;
    case 8:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.compensacion = paramConcepto.compensacion;
      return;
    case 9:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoAporte = paramConcepto.conceptoAporte;
      return;
    case 10:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoAusencia = paramConcepto.conceptoAusencia;
      return;
    case 11:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoCaja = paramConcepto.conceptoCaja;
      return;
    case 12:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoRetroactivo = paramConcepto.conceptoRetroactivo;
      return;
    case 13:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoRetroactivoAnterior = paramConcepto.conceptoRetroactivoAnterior;
      return;
    case 14:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.deduccionCaja = paramConcepto.deduccionCaja;
      return;
    case 15:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.deduccionGremio = paramConcepto.deduccionGremio;
      return;
    case 16:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.deduccionSindicato = paramConcepto.deduccionSindicato;
      return;
    case 17:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramConcepto.descripcion;
      return;
    case 18:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.embargo = paramConcepto.embargo;
      return;
    case 19:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.gravable = paramConcepto.gravable;
      return;
    case 20:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.idConcepto = paramConcepto.idConcepto;
      return;
    case 21:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramConcepto.idSitp;
      return;
    case 22:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.jubilacion = paramConcepto.jubilacion;
      return;
    case 23:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.movimiento = paramConcepto.movimiento;
      return;
    case 24:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.ocurrencia = paramConcepto.ocurrencia;
      return;
    case 25:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.onapre = paramConcepto.onapre;
      return;
    case 26:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramConcepto.organismo;
      return;
    case 27:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.prestacionesNr = paramConcepto.prestacionesNr;
      return;
    case 28:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.prestacionesVr = paramConcepto.prestacionesVr;
      return;
    case 29:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.primasCargo = paramConcepto.primasCargo;
      return;
    case 30:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.primasTrabajador = paramConcepto.primasTrabajador;
      return;
    case 31:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.proyeccion = paramConcepto.proyeccion;
      return;
    case 32:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.recalculo = paramConcepto.recalculo;
      return;
    case 33:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.reservado = paramConcepto.reservado;
      return;
    case 34:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.retroactivo = paramConcepto.retroactivo;
      return;
    case 35:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.retroactivoAnterior = paramConcepto.retroactivoAnterior;
      return;
    case 36:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.sobretiempo = paramConcepto.sobretiempo;
      return;
    case 37:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoBasico = paramConcepto.sueldoBasico;
      return;
    case 38:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoIntegral = paramConcepto.sueldoIntegral;
      return;
    case 39:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramConcepto.tiempoSitp;
      return;
    case 40:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPrestamo = paramConcepto.tipoPrestamo;
      return;
    case 41:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.unidad = paramConcepto.unidad;
      return;
    case 42:
      if (paramConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.valor = paramConcepto.valor;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Concepto))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Concepto localConcepto = (Concepto)paramObject;
    if (localConcepto.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConcepto, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoPK localConceptoPK = (ConceptoPK)paramObject;
    localConceptoPK.idConcepto = this.idConcepto;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoPK localConceptoPK = (ConceptoPK)paramObject;
    this.idConcepto = localConceptoPK.idConcepto;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoPK localConceptoPK = (ConceptoPK)paramObject;
    localConceptoPK.idConcepto = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 20);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoPK localConceptoPK = (ConceptoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 20, localConceptoPK.idConcepto);
  }

  private static final String jdoGetajusteSueldo(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.ajusteSueldo;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.ajusteSueldo;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 0))
      return paramConcepto.ajusteSueldo;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 0, paramConcepto.ajusteSueldo);
  }

  private static final void jdoSetajusteSueldo(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.ajusteSueldo = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.ajusteSueldo = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 0, paramConcepto.ajusteSueldo, paramString);
  }

  private static final String jdoGetanticipo(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.anticipo;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.anticipo;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 1))
      return paramConcepto.anticipo;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 1, paramConcepto.anticipo);
  }

  private static final void jdoSetanticipo(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.anticipo = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.anticipo = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 1, paramConcepto.anticipo, paramString);
  }

  private static final String jdoGetanual(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.anual;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.anual;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 2))
      return paramConcepto.anual;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 2, paramConcepto.anual);
  }

  private static final void jdoSetanual(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.anual = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.anual = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 2, paramConcepto.anual, paramString);
  }

  private static final String jdoGetaportePatronal(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.aportePatronal;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.aportePatronal;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 3))
      return paramConcepto.aportePatronal;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 3, paramConcepto.aportePatronal);
  }

  private static final void jdoSetaportePatronal(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.aportePatronal = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.aportePatronal = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 3, paramConcepto.aportePatronal, paramString);
  }

  private static final String jdoGetausencia(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.ausencia;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.ausencia;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 4))
      return paramConcepto.ausencia;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 4, paramConcepto.ausencia);
  }

  private static final void jdoSetausencia(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.ausencia = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.ausencia = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 4, paramConcepto.ausencia, paramString);
  }

  private static final String jdoGetbaseLegal(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.baseLegal;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.baseLegal;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 5))
      return paramConcepto.baseLegal;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 5, paramConcepto.baseLegal);
  }

  private static final void jdoSetbaseLegal(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.baseLegal = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.baseLegal = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 5, paramConcepto.baseLegal, paramString);
  }

  private static final String jdoGetbeneficio(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.beneficio;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.beneficio;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 6))
      return paramConcepto.beneficio;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 6, paramConcepto.beneficio);
  }

  private static final void jdoSetbeneficio(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.beneficio = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.beneficio = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 6, paramConcepto.beneficio, paramString);
  }

  private static final String jdoGetcodConcepto(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.codConcepto;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.codConcepto;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 7))
      return paramConcepto.codConcepto;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 7, paramConcepto.codConcepto);
  }

  private static final void jdoSetcodConcepto(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.codConcepto = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.codConcepto = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 7, paramConcepto.codConcepto, paramString);
  }

  private static final String jdoGetcompensacion(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.compensacion;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.compensacion;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 8))
      return paramConcepto.compensacion;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 8, paramConcepto.compensacion);
  }

  private static final void jdoSetcompensacion(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.compensacion = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.compensacion = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 8, paramConcepto.compensacion, paramString);
  }

  private static final Concepto jdoGetconceptoAporte(Concepto paramConcepto)
  {
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.conceptoAporte;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 9))
      return paramConcepto.conceptoAporte;
    return (Concepto)localStateManager.getObjectField(paramConcepto, jdoInheritedFieldCount + 9, paramConcepto.conceptoAporte);
  }

  private static final void jdoSetconceptoAporte(Concepto paramConcepto1, Concepto paramConcepto2)
  {
    StateManager localStateManager = paramConcepto1.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto1.conceptoAporte = paramConcepto2;
      return;
    }
    localStateManager.setObjectField(paramConcepto1, jdoInheritedFieldCount + 9, paramConcepto1.conceptoAporte, paramConcepto2);
  }

  private static final Concepto jdoGetconceptoAusencia(Concepto paramConcepto)
  {
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.conceptoAusencia;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 10))
      return paramConcepto.conceptoAusencia;
    return (Concepto)localStateManager.getObjectField(paramConcepto, jdoInheritedFieldCount + 10, paramConcepto.conceptoAusencia);
  }

  private static final void jdoSetconceptoAusencia(Concepto paramConcepto1, Concepto paramConcepto2)
  {
    StateManager localStateManager = paramConcepto1.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto1.conceptoAusencia = paramConcepto2;
      return;
    }
    localStateManager.setObjectField(paramConcepto1, jdoInheritedFieldCount + 10, paramConcepto1.conceptoAusencia, paramConcepto2);
  }

  private static final Concepto jdoGetconceptoCaja(Concepto paramConcepto)
  {
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.conceptoCaja;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 11))
      return paramConcepto.conceptoCaja;
    return (Concepto)localStateManager.getObjectField(paramConcepto, jdoInheritedFieldCount + 11, paramConcepto.conceptoCaja);
  }

  private static final void jdoSetconceptoCaja(Concepto paramConcepto1, Concepto paramConcepto2)
  {
    StateManager localStateManager = paramConcepto1.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto1.conceptoCaja = paramConcepto2;
      return;
    }
    localStateManager.setObjectField(paramConcepto1, jdoInheritedFieldCount + 11, paramConcepto1.conceptoCaja, paramConcepto2);
  }

  private static final Concepto jdoGetconceptoRetroactivo(Concepto paramConcepto)
  {
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.conceptoRetroactivo;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 12))
      return paramConcepto.conceptoRetroactivo;
    return (Concepto)localStateManager.getObjectField(paramConcepto, jdoInheritedFieldCount + 12, paramConcepto.conceptoRetroactivo);
  }

  private static final void jdoSetconceptoRetroactivo(Concepto paramConcepto1, Concepto paramConcepto2)
  {
    StateManager localStateManager = paramConcepto1.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto1.conceptoRetroactivo = paramConcepto2;
      return;
    }
    localStateManager.setObjectField(paramConcepto1, jdoInheritedFieldCount + 12, paramConcepto1.conceptoRetroactivo, paramConcepto2);
  }

  private static final Concepto jdoGetconceptoRetroactivoAnterior(Concepto paramConcepto)
  {
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.conceptoRetroactivoAnterior;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 13))
      return paramConcepto.conceptoRetroactivoAnterior;
    return (Concepto)localStateManager.getObjectField(paramConcepto, jdoInheritedFieldCount + 13, paramConcepto.conceptoRetroactivoAnterior);
  }

  private static final void jdoSetconceptoRetroactivoAnterior(Concepto paramConcepto1, Concepto paramConcepto2)
  {
    StateManager localStateManager = paramConcepto1.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto1.conceptoRetroactivoAnterior = paramConcepto2;
      return;
    }
    localStateManager.setObjectField(paramConcepto1, jdoInheritedFieldCount + 13, paramConcepto1.conceptoRetroactivoAnterior, paramConcepto2);
  }

  private static final String jdoGetdeduccionCaja(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.deduccionCaja;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.deduccionCaja;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 14))
      return paramConcepto.deduccionCaja;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 14, paramConcepto.deduccionCaja);
  }

  private static final void jdoSetdeduccionCaja(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.deduccionCaja = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.deduccionCaja = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 14, paramConcepto.deduccionCaja, paramString);
  }

  private static final String jdoGetdeduccionGremio(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.deduccionGremio;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.deduccionGremio;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 15))
      return paramConcepto.deduccionGremio;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 15, paramConcepto.deduccionGremio);
  }

  private static final void jdoSetdeduccionGremio(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.deduccionGremio = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.deduccionGremio = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 15, paramConcepto.deduccionGremio, paramString);
  }

  private static final String jdoGetdeduccionSindicato(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.deduccionSindicato;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.deduccionSindicato;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 16))
      return paramConcepto.deduccionSindicato;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 16, paramConcepto.deduccionSindicato);
  }

  private static final void jdoSetdeduccionSindicato(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.deduccionSindicato = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.deduccionSindicato = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 16, paramConcepto.deduccionSindicato, paramString);
  }

  private static final String jdoGetdescripcion(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.descripcion;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.descripcion;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 17))
      return paramConcepto.descripcion;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 17, paramConcepto.descripcion);
  }

  private static final void jdoSetdescripcion(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 17, paramConcepto.descripcion, paramString);
  }

  private static final String jdoGetembargo(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.embargo;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.embargo;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 18))
      return paramConcepto.embargo;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 18, paramConcepto.embargo);
  }

  private static final void jdoSetembargo(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.embargo = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.embargo = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 18, paramConcepto.embargo, paramString);
  }

  private static final String jdoGetgravable(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.gravable;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.gravable;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 19))
      return paramConcepto.gravable;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 19, paramConcepto.gravable);
  }

  private static final void jdoSetgravable(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.gravable = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.gravable = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 19, paramConcepto.gravable, paramString);
  }

  private static final long jdoGetidConcepto(Concepto paramConcepto)
  {
    return paramConcepto.idConcepto;
  }

  private static final void jdoSetidConcepto(Concepto paramConcepto, long paramLong)
  {
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.idConcepto = paramLong;
      return;
    }
    localStateManager.setLongField(paramConcepto, jdoInheritedFieldCount + 20, paramConcepto.idConcepto, paramLong);
  }

  private static final int jdoGetidSitp(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.idSitp;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.idSitp;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 21))
      return paramConcepto.idSitp;
    return localStateManager.getIntField(paramConcepto, jdoInheritedFieldCount + 21, paramConcepto.idSitp);
  }

  private static final void jdoSetidSitp(Concepto paramConcepto, int paramInt)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramConcepto, jdoInheritedFieldCount + 21, paramConcepto.idSitp, paramInt);
  }

  private static final String jdoGetjubilacion(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.jubilacion;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.jubilacion;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 22))
      return paramConcepto.jubilacion;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 22, paramConcepto.jubilacion);
  }

  private static final void jdoSetjubilacion(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.jubilacion = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.jubilacion = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 22, paramConcepto.jubilacion, paramString);
  }

  private static final String jdoGetmovimiento(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.movimiento;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.movimiento;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 23))
      return paramConcepto.movimiento;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 23, paramConcepto.movimiento);
  }

  private static final void jdoSetmovimiento(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.movimiento = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.movimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 23, paramConcepto.movimiento, paramString);
  }

  private static final double jdoGetocurrencia(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.ocurrencia;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.ocurrencia;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 24))
      return paramConcepto.ocurrencia;
    return localStateManager.getDoubleField(paramConcepto, jdoInheritedFieldCount + 24, paramConcepto.ocurrencia);
  }

  private static final void jdoSetocurrencia(Concepto paramConcepto, double paramDouble)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.ocurrencia = paramDouble;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.ocurrencia = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConcepto, jdoInheritedFieldCount + 24, paramConcepto.ocurrencia, paramDouble);
  }

  private static final String jdoGetonapre(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.onapre;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.onapre;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 25))
      return paramConcepto.onapre;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 25, paramConcepto.onapre);
  }

  private static final void jdoSetonapre(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.onapre = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.onapre = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 25, paramConcepto.onapre, paramString);
  }

  private static final Organismo jdoGetorganismo(Concepto paramConcepto)
  {
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.organismo;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 26))
      return paramConcepto.organismo;
    return (Organismo)localStateManager.getObjectField(paramConcepto, jdoInheritedFieldCount + 26, paramConcepto.organismo);
  }

  private static final void jdoSetorganismo(Concepto paramConcepto, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramConcepto, jdoInheritedFieldCount + 26, paramConcepto.organismo, paramOrganismo);
  }

  private static final String jdoGetprestacionesNr(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.prestacionesNr;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.prestacionesNr;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 27))
      return paramConcepto.prestacionesNr;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 27, paramConcepto.prestacionesNr);
  }

  private static final void jdoSetprestacionesNr(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.prestacionesNr = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.prestacionesNr = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 27, paramConcepto.prestacionesNr, paramString);
  }

  private static final String jdoGetprestacionesVr(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.prestacionesVr;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.prestacionesVr;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 28))
      return paramConcepto.prestacionesVr;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 28, paramConcepto.prestacionesVr);
  }

  private static final void jdoSetprestacionesVr(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.prestacionesVr = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.prestacionesVr = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 28, paramConcepto.prestacionesVr, paramString);
  }

  private static final String jdoGetprimasCargo(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.primasCargo;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.primasCargo;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 29))
      return paramConcepto.primasCargo;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 29, paramConcepto.primasCargo);
  }

  private static final void jdoSetprimasCargo(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.primasCargo = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.primasCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 29, paramConcepto.primasCargo, paramString);
  }

  private static final String jdoGetprimasTrabajador(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.primasTrabajador;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.primasTrabajador;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 30))
      return paramConcepto.primasTrabajador;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 30, paramConcepto.primasTrabajador);
  }

  private static final void jdoSetprimasTrabajador(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.primasTrabajador = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.primasTrabajador = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 30, paramConcepto.primasTrabajador, paramString);
  }

  private static final String jdoGetproyeccion(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.proyeccion;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.proyeccion;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 31))
      return paramConcepto.proyeccion;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 31, paramConcepto.proyeccion);
  }

  private static final void jdoSetproyeccion(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.proyeccion = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.proyeccion = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 31, paramConcepto.proyeccion, paramString);
  }

  private static final String jdoGetrecalculo(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.recalculo;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.recalculo;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 32))
      return paramConcepto.recalculo;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 32, paramConcepto.recalculo);
  }

  private static final void jdoSetrecalculo(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.recalculo = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.recalculo = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 32, paramConcepto.recalculo, paramString);
  }

  private static final String jdoGetreservado(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.reservado;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.reservado;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 33))
      return paramConcepto.reservado;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 33, paramConcepto.reservado);
  }

  private static final void jdoSetreservado(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.reservado = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.reservado = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 33, paramConcepto.reservado, paramString);
  }

  private static final String jdoGetretroactivo(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.retroactivo;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.retroactivo;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 34))
      return paramConcepto.retroactivo;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 34, paramConcepto.retroactivo);
  }

  private static final void jdoSetretroactivo(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.retroactivo = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.retroactivo = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 34, paramConcepto.retroactivo, paramString);
  }

  private static final String jdoGetretroactivoAnterior(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.retroactivoAnterior;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.retroactivoAnterior;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 35))
      return paramConcepto.retroactivoAnterior;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 35, paramConcepto.retroactivoAnterior);
  }

  private static final void jdoSetretroactivoAnterior(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.retroactivoAnterior = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.retroactivoAnterior = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 35, paramConcepto.retroactivoAnterior, paramString);
  }

  private static final String jdoGetsobretiempo(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.sobretiempo;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.sobretiempo;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 36))
      return paramConcepto.sobretiempo;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 36, paramConcepto.sobretiempo);
  }

  private static final void jdoSetsobretiempo(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.sobretiempo = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.sobretiempo = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 36, paramConcepto.sobretiempo, paramString);
  }

  private static final String jdoGetsueldoBasico(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.sueldoBasico;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.sueldoBasico;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 37))
      return paramConcepto.sueldoBasico;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 37, paramConcepto.sueldoBasico);
  }

  private static final void jdoSetsueldoBasico(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.sueldoBasico = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.sueldoBasico = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 37, paramConcepto.sueldoBasico, paramString);
  }

  private static final String jdoGetsueldoIntegral(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.sueldoIntegral;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.sueldoIntegral;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 38))
      return paramConcepto.sueldoIntegral;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 38, paramConcepto.sueldoIntegral);
  }

  private static final void jdoSetsueldoIntegral(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.sueldoIntegral = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.sueldoIntegral = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 38, paramConcepto.sueldoIntegral, paramString);
  }

  private static final Date jdoGettiempoSitp(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.tiempoSitp;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.tiempoSitp;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 39))
      return paramConcepto.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramConcepto, jdoInheritedFieldCount + 39, paramConcepto.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Concepto paramConcepto, Date paramDate)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcepto, jdoInheritedFieldCount + 39, paramConcepto.tiempoSitp, paramDate);
  }

  private static final String jdoGettipoPrestamo(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.tipoPrestamo;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.tipoPrestamo;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 40))
      return paramConcepto.tipoPrestamo;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 40, paramConcepto.tipoPrestamo);
  }

  private static final void jdoSettipoPrestamo(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.tipoPrestamo = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.tipoPrestamo = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 40, paramConcepto.tipoPrestamo, paramString);
  }

  private static final String jdoGetunidad(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.unidad;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.unidad;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 41))
      return paramConcepto.unidad;
    return localStateManager.getStringField(paramConcepto, jdoInheritedFieldCount + 41, paramConcepto.unidad);
  }

  private static final void jdoSetunidad(Concepto paramConcepto, String paramString)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.unidad = paramString;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.unidad = paramString;
      return;
    }
    localStateManager.setStringField(paramConcepto, jdoInheritedFieldCount + 41, paramConcepto.unidad, paramString);
  }

  private static final double jdoGetvalor(Concepto paramConcepto)
  {
    if (paramConcepto.jdoFlags <= 0)
      return paramConcepto.valor;
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramConcepto.valor;
    if (localStateManager.isLoaded(paramConcepto, jdoInheritedFieldCount + 42))
      return paramConcepto.valor;
    return localStateManager.getDoubleField(paramConcepto, jdoInheritedFieldCount + 42, paramConcepto.valor);
  }

  private static final void jdoSetvalor(Concepto paramConcepto, double paramDouble)
  {
    if (paramConcepto.jdoFlags == 0)
    {
      paramConcepto.valor = paramDouble;
      return;
    }
    StateManager localStateManager = paramConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcepto.valor = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConcepto, jdoInheritedFieldCount + 42, paramConcepto.valor, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}