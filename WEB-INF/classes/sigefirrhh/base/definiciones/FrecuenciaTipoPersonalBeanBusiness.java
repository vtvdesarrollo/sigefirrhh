package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class FrecuenciaTipoPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addFrecuenciaTipoPersonal(FrecuenciaTipoPersonal frecuenciaTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    FrecuenciaTipoPersonal frecuenciaTipoPersonalNew = 
      (FrecuenciaTipoPersonal)BeanUtils.cloneBean(
      frecuenciaTipoPersonal);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (frecuenciaTipoPersonalNew.getTipoPersonal() != null) {
      frecuenciaTipoPersonalNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        frecuenciaTipoPersonalNew.getTipoPersonal().getIdTipoPersonal()));
    }

    FrecuenciaPagoBeanBusiness frecuenciaPagoBeanBusiness = new FrecuenciaPagoBeanBusiness();

    if (frecuenciaTipoPersonalNew.getFrecuenciaPago() != null) {
      frecuenciaTipoPersonalNew.setFrecuenciaPago(
        frecuenciaPagoBeanBusiness.findFrecuenciaPagoById(
        frecuenciaTipoPersonalNew.getFrecuenciaPago().getIdFrecuenciaPago()));
    }
    pm.makePersistent(frecuenciaTipoPersonalNew);
  }

  public void updateFrecuenciaTipoPersonal(FrecuenciaTipoPersonal frecuenciaTipoPersonal) throws Exception
  {
    FrecuenciaTipoPersonal frecuenciaTipoPersonalModify = 
      findFrecuenciaTipoPersonalById(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (frecuenciaTipoPersonal.getTipoPersonal() != null) {
      frecuenciaTipoPersonal.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        frecuenciaTipoPersonal.getTipoPersonal().getIdTipoPersonal()));
    }

    FrecuenciaPagoBeanBusiness frecuenciaPagoBeanBusiness = new FrecuenciaPagoBeanBusiness();

    if (frecuenciaTipoPersonal.getFrecuenciaPago() != null) {
      frecuenciaTipoPersonal.setFrecuenciaPago(
        frecuenciaPagoBeanBusiness.findFrecuenciaPagoById(
        frecuenciaTipoPersonal.getFrecuenciaPago().getIdFrecuenciaPago()));
    }

    BeanUtils.copyProperties(frecuenciaTipoPersonalModify, frecuenciaTipoPersonal);
  }

  public void deleteFrecuenciaTipoPersonal(FrecuenciaTipoPersonal frecuenciaTipoPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    FrecuenciaTipoPersonal frecuenciaTipoPersonalDelete = 
      findFrecuenciaTipoPersonalById(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal());
    pm.deletePersistent(frecuenciaTipoPersonalDelete);
  }

  public FrecuenciaTipoPersonal findFrecuenciaTipoPersonalById(long idFrecuenciaTipoPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idFrecuenciaTipoPersonal == pIdFrecuenciaTipoPersonal";
    Query query = pm.newQuery(FrecuenciaTipoPersonal.class, filter);

    query.declareParameters("long pIdFrecuenciaTipoPersonal");

    parameters.put("pIdFrecuenciaTipoPersonal", new Long(idFrecuenciaTipoPersonal));

    Collection colFrecuenciaTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colFrecuenciaTipoPersonal.iterator();
    return (FrecuenciaTipoPersonal)iterator.next();
  }

  public Collection findFrecuenciaTipoPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent frecuenciaTipoPersonalExtent = pm.getExtent(
      FrecuenciaTipoPersonal.class, true);
    Query query = pm.newQuery(frecuenciaTipoPersonalExtent);
    query.setOrdering("frecuenciaPago.codFrecuenciaPago ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(FrecuenciaTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("frecuenciaPago.codFrecuenciaPago ascending");

    Collection colFrecuenciaTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFrecuenciaTipoPersonal);

    return colFrecuenciaTipoPersonal;
  }

  public Collection findByFrecuenciaPago(long idFrecuenciaPago)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "frecuenciaPago.idFrecuenciaPago == pIdFrecuenciaPago";

    Query query = pm.newQuery(FrecuenciaTipoPersonal.class, filter);

    query.declareParameters("long pIdFrecuenciaPago");
    HashMap parameters = new HashMap();

    parameters.put("pIdFrecuenciaPago", new Long(idFrecuenciaPago));

    query.setOrdering("frecuenciaPago.codFrecuenciaPago ascending");

    Collection colFrecuenciaTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFrecuenciaTipoPersonal);

    return colFrecuenciaTipoPersonal;
  }
}