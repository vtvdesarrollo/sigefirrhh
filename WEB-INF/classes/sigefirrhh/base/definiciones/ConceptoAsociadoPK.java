package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ConceptoAsociadoPK
  implements Serializable
{
  public long idConceptoAsociado;

  public ConceptoAsociadoPK()
  {
  }

  public ConceptoAsociadoPK(long idConceptoAsociado)
  {
    this.idConceptoAsociado = idConceptoAsociado;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoAsociadoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoAsociadoPK thatPK)
  {
    return 
      this.idConceptoAsociado == thatPK.idConceptoAsociado;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoAsociado)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoAsociado);
  }
}