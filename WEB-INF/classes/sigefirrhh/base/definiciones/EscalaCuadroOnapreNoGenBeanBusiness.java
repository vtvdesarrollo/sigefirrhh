package sigefirrhh.base.definiciones;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class EscalaCuadroOnapreNoGenBeanBusiness extends EscalaCuadroOnapreBeanBusiness
  implements Serializable
{
  public EscalaCuadroOnapre findByCodigoAproximada(String codigo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codigo > pCodigo";

    Query query = pm.newQuery(EscalaCuadroOnapre.class, filter);

    query.declareParameters("double pCodigo");
    HashMap parameters = new HashMap();

    parameters.put("pCodigo", new Double(codigo));

    query.setOrdering("codigo ascending");

    Collection colEscalaCuadroOnapre = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEscalaCuadroOnapre);

    return (EscalaCuadroOnapre)colEscalaCuadroOnapre.iterator().next();
  }
}