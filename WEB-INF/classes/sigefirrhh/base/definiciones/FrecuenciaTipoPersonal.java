package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class FrecuenciaTipoPersonal
  implements Serializable, PersistenceCapable
{
  private long idFrecuenciaTipoPersonal;
  private TipoPersonal tipoPersonal;
  private FrecuenciaPago frecuenciaPago;
  private int codFrecuenciaPago;
  private String codTipoPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codFrecuenciaPago", "codTipoPersonal", "frecuenciaPago", "idFrecuenciaTipoPersonal", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaPago"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") };
  private static final byte[] jdoFieldFlags = { 21, 21, 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetfrecuenciaPago(this).getCodFrecuenciaPago() + " - " + 
      jdoGetfrecuenciaPago(this).getNombre();
  }

  public FrecuenciaPago getFrecuenciaPago()
  {
    return jdoGetfrecuenciaPago(this);
  }

  public long getIdFrecuenciaTipoPersonal()
  {
    return jdoGetidFrecuenciaTipoPersonal(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setFrecuenciaPago(FrecuenciaPago pago)
  {
    jdoSetfrecuenciaPago(this, pago);
  }

  public void setIdFrecuenciaTipoPersonal(long l)
  {
    jdoSetidFrecuenciaTipoPersonal(this, l);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public int getCodFrecuenciaPago()
  {
    return jdoGetcodFrecuenciaPago(this);
  }

  public String getCodTipoPersonal()
  {
    return jdoGetcodTipoPersonal(this);
  }

  public void setCodFrecuenciaPago(int i)
  {
    jdoSetcodFrecuenciaPago(this, i);
  }

  public void setCodTipoPersonal(String string)
  {
    jdoSetcodTipoPersonal(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new FrecuenciaTipoPersonal());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    FrecuenciaTipoPersonal localFrecuenciaTipoPersonal = new FrecuenciaTipoPersonal();
    localFrecuenciaTipoPersonal.jdoFlags = 1;
    localFrecuenciaTipoPersonal.jdoStateManager = paramStateManager;
    return localFrecuenciaTipoPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    FrecuenciaTipoPersonal localFrecuenciaTipoPersonal = new FrecuenciaTipoPersonal();
    localFrecuenciaTipoPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localFrecuenciaTipoPersonal.jdoFlags = 1;
    localFrecuenciaTipoPersonal.jdoStateManager = paramStateManager;
    return localFrecuenciaTipoPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codFrecuenciaPago);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoPersonal);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaPago);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idFrecuenciaTipoPersonal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codFrecuenciaPago = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaPago = ((FrecuenciaPago)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idFrecuenciaTipoPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramFrecuenciaTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.codFrecuenciaPago = paramFrecuenciaTipoPersonal.codFrecuenciaPago;
      return;
    case 1:
      if (paramFrecuenciaTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoPersonal = paramFrecuenciaTipoPersonal.codTipoPersonal;
      return;
    case 2:
      if (paramFrecuenciaTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaPago = paramFrecuenciaTipoPersonal.frecuenciaPago;
      return;
    case 3:
      if (paramFrecuenciaTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idFrecuenciaTipoPersonal = paramFrecuenciaTipoPersonal.idFrecuenciaTipoPersonal;
      return;
    case 4:
      if (paramFrecuenciaTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramFrecuenciaTipoPersonal.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof FrecuenciaTipoPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    FrecuenciaTipoPersonal localFrecuenciaTipoPersonal = (FrecuenciaTipoPersonal)paramObject;
    if (localFrecuenciaTipoPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localFrecuenciaTipoPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new FrecuenciaTipoPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new FrecuenciaTipoPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FrecuenciaTipoPersonalPK))
      throw new IllegalArgumentException("arg1");
    FrecuenciaTipoPersonalPK localFrecuenciaTipoPersonalPK = (FrecuenciaTipoPersonalPK)paramObject;
    localFrecuenciaTipoPersonalPK.idFrecuenciaTipoPersonal = this.idFrecuenciaTipoPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FrecuenciaTipoPersonalPK))
      throw new IllegalArgumentException("arg1");
    FrecuenciaTipoPersonalPK localFrecuenciaTipoPersonalPK = (FrecuenciaTipoPersonalPK)paramObject;
    this.idFrecuenciaTipoPersonal = localFrecuenciaTipoPersonalPK.idFrecuenciaTipoPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FrecuenciaTipoPersonalPK))
      throw new IllegalArgumentException("arg2");
    FrecuenciaTipoPersonalPK localFrecuenciaTipoPersonalPK = (FrecuenciaTipoPersonalPK)paramObject;
    localFrecuenciaTipoPersonalPK.idFrecuenciaTipoPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FrecuenciaTipoPersonalPK))
      throw new IllegalArgumentException("arg2");
    FrecuenciaTipoPersonalPK localFrecuenciaTipoPersonalPK = (FrecuenciaTipoPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localFrecuenciaTipoPersonalPK.idFrecuenciaTipoPersonal);
  }

  private static final int jdoGetcodFrecuenciaPago(FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    if (paramFrecuenciaTipoPersonal.jdoFlags <= 0)
      return paramFrecuenciaTipoPersonal.codFrecuenciaPago;
    StateManager localStateManager = paramFrecuenciaTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramFrecuenciaTipoPersonal.codFrecuenciaPago;
    if (localStateManager.isLoaded(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 0))
      return paramFrecuenciaTipoPersonal.codFrecuenciaPago;
    return localStateManager.getIntField(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 0, paramFrecuenciaTipoPersonal.codFrecuenciaPago);
  }

  private static final void jdoSetcodFrecuenciaPago(FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal, int paramInt)
  {
    if (paramFrecuenciaTipoPersonal.jdoFlags == 0)
    {
      paramFrecuenciaTipoPersonal.codFrecuenciaPago = paramInt;
      return;
    }
    StateManager localStateManager = paramFrecuenciaTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaTipoPersonal.codFrecuenciaPago = paramInt;
      return;
    }
    localStateManager.setIntField(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 0, paramFrecuenciaTipoPersonal.codFrecuenciaPago, paramInt);
  }

  private static final String jdoGetcodTipoPersonal(FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    if (paramFrecuenciaTipoPersonal.jdoFlags <= 0)
      return paramFrecuenciaTipoPersonal.codTipoPersonal;
    StateManager localStateManager = paramFrecuenciaTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramFrecuenciaTipoPersonal.codTipoPersonal;
    if (localStateManager.isLoaded(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 1))
      return paramFrecuenciaTipoPersonal.codTipoPersonal;
    return localStateManager.getStringField(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 1, paramFrecuenciaTipoPersonal.codTipoPersonal);
  }

  private static final void jdoSetcodTipoPersonal(FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal, String paramString)
  {
    if (paramFrecuenciaTipoPersonal.jdoFlags == 0)
    {
      paramFrecuenciaTipoPersonal.codTipoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramFrecuenciaTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaTipoPersonal.codTipoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 1, paramFrecuenciaTipoPersonal.codTipoPersonal, paramString);
  }

  private static final FrecuenciaPago jdoGetfrecuenciaPago(FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramFrecuenciaTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramFrecuenciaTipoPersonal.frecuenciaPago;
    if (localStateManager.isLoaded(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 2))
      return paramFrecuenciaTipoPersonal.frecuenciaPago;
    return (FrecuenciaPago)localStateManager.getObjectField(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 2, paramFrecuenciaTipoPersonal.frecuenciaPago);
  }

  private static final void jdoSetfrecuenciaPago(FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal, FrecuenciaPago paramFrecuenciaPago)
  {
    StateManager localStateManager = paramFrecuenciaTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaTipoPersonal.frecuenciaPago = paramFrecuenciaPago;
      return;
    }
    localStateManager.setObjectField(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 2, paramFrecuenciaTipoPersonal.frecuenciaPago, paramFrecuenciaPago);
  }

  private static final long jdoGetidFrecuenciaTipoPersonal(FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    return paramFrecuenciaTipoPersonal.idFrecuenciaTipoPersonal;
  }

  private static final void jdoSetidFrecuenciaTipoPersonal(FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal, long paramLong)
  {
    StateManager localStateManager = paramFrecuenciaTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaTipoPersonal.idFrecuenciaTipoPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 3, paramFrecuenciaTipoPersonal.idFrecuenciaTipoPersonal, paramLong);
  }

  private static final TipoPersonal jdoGettipoPersonal(FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramFrecuenciaTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramFrecuenciaTipoPersonal.tipoPersonal;
    if (localStateManager.isLoaded(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 4))
      return paramFrecuenciaTipoPersonal.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 4, paramFrecuenciaTipoPersonal.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramFrecuenciaTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaTipoPersonal.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramFrecuenciaTipoPersonal, jdoInheritedFieldCount + 4, paramFrecuenciaTipoPersonal.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}