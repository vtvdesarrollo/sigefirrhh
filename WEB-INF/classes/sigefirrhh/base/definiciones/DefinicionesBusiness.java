package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class DefinicionesBusiness extends AbstractBusiness
  implements Serializable
{
  private BancoBeanBusiness bancoBeanBusiness = new BancoBeanBusiness();

  private BeneficiarioGlobalBeanBusiness beneficiarioGlobalBeanBusiness = new BeneficiarioGlobalBeanBusiness();

  private CategoriaPersonalBeanBusiness categoriaPersonalBeanBusiness = new CategoriaPersonalBeanBusiness();

  private CategoriaPresupuestoBeanBusiness categoriaPresupuestoBeanBusiness = new CategoriaPresupuestoBeanBusiness();

  private ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

  private ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

  private ConceptoAsociadoBeanBusiness conceptoAsociadoBeanBusiness = new ConceptoAsociadoBeanBusiness();

  private ConceptoCargoBeanBusiness conceptoCargoBeanBusiness = new ConceptoCargoBeanBusiness();

  private ConceptoCargoAnioBeanBusiness conceptoCargoAnioBeanBusiness = new ConceptoCargoAnioBeanBusiness();

  private ConceptoDependenciaBeanBusiness conceptoDependenciaBeanBusiness = new ConceptoDependenciaBeanBusiness();

  private ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

  private ConceptoUtilidadesBeanBusiness conceptoUtilidadesBeanBusiness = new ConceptoUtilidadesBeanBusiness();

  private ConceptoVacacionesBeanBusiness conceptoVacacionesBeanBusiness = new ConceptoVacacionesBeanBusiness();

  private ContratoColectivoBeanBusiness contratoColectivoBeanBusiness = new ContratoColectivoBeanBusiness();

  private CuentaBancoBeanBusiness cuentaBancoBeanBusiness = new CuentaBancoBeanBusiness();

  private DetalleDisqueteBeanBusiness detalleDisqueteBeanBusiness = new DetalleDisqueteBeanBusiness();

  private DiaFeriadoBeanBusiness diaFeriadoBeanBusiness = new DiaFeriadoBeanBusiness();

  private DisqueteBeanBusiness disqueteBeanBusiness = new DisqueteBeanBusiness();

  private FirmasReportesBeanBusiness firmasReportesBeanBusiness = new FirmasReportesBeanBusiness();

  private FrecuenciaPagoBeanBusiness frecuenciaPagoBeanBusiness = new FrecuenciaPagoBeanBusiness();

  private FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

  private GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

  private MesBeanBusiness mesBeanBusiness = new MesBeanBusiness();

  private ParametroAriBeanBusiness parametroAriBeanBusiness = new ParametroAriBeanBusiness();

  private ParametroGobiernoBeanBusiness parametroGobiernoBeanBusiness = new ParametroGobiernoBeanBusiness();

  private ParametroReportesBeanBusiness parametroReportesBeanBusiness = new ParametroReportesBeanBusiness();

  private ParametroVariosBeanBusiness parametroVariosBeanBusiness = new ParametroVariosBeanBusiness();

  private PrimaAntiguedadBeanBusiness primaAntiguedadBeanBusiness = new PrimaAntiguedadBeanBusiness();

  private PrimaHijoBeanBusiness primaHijoBeanBusiness = new PrimaHijoBeanBusiness();

  private RelacionPersonalBeanBusiness relacionPersonalBeanBusiness = new RelacionPersonalBeanBusiness();

  private RestringidoBeanBusiness restringidoBeanBusiness = new RestringidoBeanBusiness();

  private SemanaBeanBusiness semanaBeanBusiness = new SemanaBeanBusiness();

  private SindicatoBeanBusiness sindicatoBeanBusiness = new SindicatoBeanBusiness();

  private TarifaAriBeanBusiness tarifaAriBeanBusiness = new TarifaAriBeanBusiness();

  private EscalaCuadroOnapreBeanBusiness escalaCuadroOnapreBeanBusiness = new EscalaCuadroOnapreBeanBusiness();

  private TipoAcreenciaBeanBusiness tipoAcreenciaBeanBusiness = new TipoAcreenciaBeanBusiness();

  private TipoContratoBeanBusiness tipoContratoBeanBusiness = new TipoContratoBeanBusiness();

  private TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

  private TurnoBeanBusiness turnoBeanBusiness = new TurnoBeanBusiness();

  private VacacionesPorAnioBeanBusiness vacacionesPorAnioBeanBusiness = new VacacionesPorAnioBeanBusiness();

  private VacacionesPorCargoBeanBusiness vacacionesPorCargoBeanBusiness = new VacacionesPorCargoBeanBusiness();

  private UtilidadesPorAnioBeanBusiness utilidadesPorAnioBeanBusiness = new UtilidadesPorAnioBeanBusiness();

  private ParametroJubilacionBeanBusiness parametroJubilacionBeanBusiness = new ParametroJubilacionBeanBusiness();

  public void addBanco(Banco banco)
    throws Exception
  {
    this.bancoBeanBusiness.addBanco(banco);
  }

  public void updateBanco(Banco banco) throws Exception {
    this.bancoBeanBusiness.updateBanco(banco);
  }

  public void deleteBanco(Banco banco) throws Exception {
    this.bancoBeanBusiness.deleteBanco(banco);
  }

  public Banco findBancoById(long bancoId) throws Exception {
    return this.bancoBeanBusiness.findBancoById(bancoId);
  }

  public Collection findAllBanco() throws Exception {
    return this.bancoBeanBusiness.findBancoAll();
  }

  public Collection findBancoByCodBanco(String codBanco)
    throws Exception
  {
    return this.bancoBeanBusiness.findByCodBanco(codBanco);
  }

  public Collection findBancoByNombre(String nombre)
    throws Exception
  {
    return this.bancoBeanBusiness.findByNombre(nombre);
  }

  public void addBeneficiarioGlobal(BeneficiarioGlobal beneficiarioGlobal)
    throws Exception
  {
    this.beneficiarioGlobalBeanBusiness.addBeneficiarioGlobal(beneficiarioGlobal);
  }

  public void updateBeneficiarioGlobal(BeneficiarioGlobal beneficiarioGlobal) throws Exception {
    this.beneficiarioGlobalBeanBusiness.updateBeneficiarioGlobal(beneficiarioGlobal);
  }

  public void deleteBeneficiarioGlobal(BeneficiarioGlobal beneficiarioGlobal) throws Exception {
    this.beneficiarioGlobalBeanBusiness.deleteBeneficiarioGlobal(beneficiarioGlobal);
  }

  public BeneficiarioGlobal findBeneficiarioGlobalById(long beneficiarioGlobalId) throws Exception {
    return this.beneficiarioGlobalBeanBusiness.findBeneficiarioGlobalById(beneficiarioGlobalId);
  }

  public Collection findAllBeneficiarioGlobal() throws Exception {
    return this.beneficiarioGlobalBeanBusiness.findBeneficiarioGlobalAll();
  }

  public Collection findBeneficiarioGlobalByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    return this.beneficiarioGlobalBeanBusiness.findByConceptoTipoPersonal(idConceptoTipoPersonal);
  }

  public Collection findBeneficiarioGlobalByNombre(String nombre)
    throws Exception
  {
    return this.beneficiarioGlobalBeanBusiness.findByNombre(nombre);
  }

  public void addCategoriaPersonal(CategoriaPersonal categoriaPersonal)
    throws Exception
  {
    this.categoriaPersonalBeanBusiness.addCategoriaPersonal(categoriaPersonal);
  }

  public void updateCategoriaPersonal(CategoriaPersonal categoriaPersonal) throws Exception {
    this.categoriaPersonalBeanBusiness.updateCategoriaPersonal(categoriaPersonal);
  }

  public void deleteCategoriaPersonal(CategoriaPersonal categoriaPersonal) throws Exception {
    this.categoriaPersonalBeanBusiness.deleteCategoriaPersonal(categoriaPersonal);
  }

  public CategoriaPersonal findCategoriaPersonalById(long categoriaPersonalId) throws Exception {
    return this.categoriaPersonalBeanBusiness.findCategoriaPersonalById(categoriaPersonalId);
  }

  public Collection findAllCategoriaPersonal() throws Exception {
    return this.categoriaPersonalBeanBusiness.findCategoriaPersonalAll();
  }

  public Collection findCategoriaPersonalByCodCategoria(String codCategoria)
    throws Exception
  {
    return this.categoriaPersonalBeanBusiness.findByCodCategoria(codCategoria);
  }

  public Collection findCategoriaPersonalByDescCategoria(String descCategoria)
    throws Exception
  {
    return this.categoriaPersonalBeanBusiness.findByDescCategoria(descCategoria);
  }

  public void addCategoriaPresupuesto(CategoriaPresupuesto categoriaPresupuesto)
    throws Exception
  {
    this.categoriaPresupuestoBeanBusiness.addCategoriaPresupuesto(categoriaPresupuesto);
  }

  public void updateCategoriaPresupuesto(CategoriaPresupuesto categoriaPresupuesto) throws Exception {
    this.categoriaPresupuestoBeanBusiness.updateCategoriaPresupuesto(categoriaPresupuesto);
  }

  public void deleteCategoriaPresupuesto(CategoriaPresupuesto categoriaPresupuesto) throws Exception {
    this.categoriaPresupuestoBeanBusiness.deleteCategoriaPresupuesto(categoriaPresupuesto);
  }

  public CategoriaPresupuesto findCategoriaPresupuestoById(long categoriaPresupuestoId) throws Exception {
    return this.categoriaPresupuestoBeanBusiness.findCategoriaPresupuestoById(categoriaPresupuestoId);
  }

  public Collection findAllCategoriaPresupuesto() throws Exception {
    return this.categoriaPresupuestoBeanBusiness.findCategoriaPresupuestoAll();
  }

  public Collection findCategoriaPresupuestoByCodCategoria(String codCategoria)
    throws Exception
  {
    return this.categoriaPresupuestoBeanBusiness.findByCodCategoria(codCategoria);
  }

  public Collection findCategoriaPresupuestoByDescCategoria(String descCategoria)
    throws Exception
  {
    return this.categoriaPresupuestoBeanBusiness.findByDescCategoria(descCategoria);
  }

  public void addClasificacionPersonal(ClasificacionPersonal clasificacionPersonal)
    throws Exception
  {
    this.clasificacionPersonalBeanBusiness.addClasificacionPersonal(clasificacionPersonal);
  }

  public void updateClasificacionPersonal(ClasificacionPersonal clasificacionPersonal) throws Exception {
    this.clasificacionPersonalBeanBusiness.updateClasificacionPersonal(clasificacionPersonal);
  }

  public void deleteClasificacionPersonal(ClasificacionPersonal clasificacionPersonal) throws Exception {
    this.clasificacionPersonalBeanBusiness.deleteClasificacionPersonal(clasificacionPersonal);
  }

  public ClasificacionPersonal findClasificacionPersonalById(long clasificacionPersonalId) throws Exception {
    return this.clasificacionPersonalBeanBusiness.findClasificacionPersonalById(clasificacionPersonalId);
  }

  public Collection findAllClasificacionPersonal() throws Exception {
    return this.clasificacionPersonalBeanBusiness.findClasificacionPersonalAll();
  }

  public Collection findClasificacionPersonalByCategoriaPersonal(long idCategoriaPersonal)
    throws Exception
  {
    return this.clasificacionPersonalBeanBusiness.findByCategoriaPersonal(idCategoriaPersonal);
  }

  public Collection findClasificacionPersonalByRelacionPersonal(long idRelacionPersonal)
    throws Exception
  {
    return this.clasificacionPersonalBeanBusiness.findByRelacionPersonal(idRelacionPersonal);
  }

  public void addConcepto(Concepto concepto)
    throws Exception
  {
    this.conceptoBeanBusiness.addConcepto(concepto);
  }

  public void updateConcepto(Concepto concepto) throws Exception {
    this.conceptoBeanBusiness.updateConcepto(concepto);
  }

  public void deleteConcepto(Concepto concepto) throws Exception {
    this.conceptoBeanBusiness.deleteConcepto(concepto);
  }

  public Concepto findConceptoById(long conceptoId) throws Exception {
    return this.conceptoBeanBusiness.findConceptoById(conceptoId);
  }

  public Collection findAllConcepto() throws Exception {
    return this.conceptoBeanBusiness.findConceptoAll();
  }

  public Collection findConceptoByCodConcepto(String codConcepto, long idOrganismo)
    throws Exception
  {
    return this.conceptoBeanBusiness.findByCodConcepto(codConcepto, idOrganismo);
  }

  public Collection findConceptoByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    return this.conceptoBeanBusiness.findByDescripcion(descripcion, idOrganismo);
  }

  public Collection findConceptoByAportePatronal(String aportePatronal, long idOrganismo)
    throws Exception
  {
    return this.conceptoBeanBusiness.findByAportePatronal(aportePatronal, idOrganismo);
  }

  public Collection findConceptoByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.conceptoBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addConceptoAsociado(ConceptoAsociado conceptoAsociado)
    throws Exception
  {
    this.conceptoAsociadoBeanBusiness.addConceptoAsociado(conceptoAsociado);
  }

  public void updateConceptoAsociado(ConceptoAsociado conceptoAsociado) throws Exception {
    this.conceptoAsociadoBeanBusiness.updateConceptoAsociado(conceptoAsociado);
  }

  public void deleteConceptoAsociado(ConceptoAsociado conceptoAsociado) throws Exception {
    this.conceptoAsociadoBeanBusiness.deleteConceptoAsociado(conceptoAsociado);
  }

  public ConceptoAsociado findConceptoAsociadoById(long conceptoAsociadoId) throws Exception {
    return this.conceptoAsociadoBeanBusiness.findConceptoAsociadoById(conceptoAsociadoId);
  }

  public Collection findAllConceptoAsociado() throws Exception {
    return this.conceptoAsociadoBeanBusiness.findConceptoAsociadoAll();
  }

  public Collection findConceptoAsociadoByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    return this.conceptoAsociadoBeanBusiness.findByConceptoTipoPersonal(idConceptoTipoPersonal);
  }

  public void addConceptoCargo(ConceptoCargo conceptoCargo)
    throws Exception
  {
    this.conceptoCargoBeanBusiness.addConceptoCargo(conceptoCargo);
  }

  public void updateConceptoCargo(ConceptoCargo conceptoCargo) throws Exception {
    this.conceptoCargoBeanBusiness.updateConceptoCargo(conceptoCargo);
  }

  public void deleteConceptoCargo(ConceptoCargo conceptoCargo) throws Exception {
    this.conceptoCargoBeanBusiness.deleteConceptoCargo(conceptoCargo);
  }

  public ConceptoCargo findConceptoCargoById(long conceptoCargoId) throws Exception {
    return this.conceptoCargoBeanBusiness.findConceptoCargoById(conceptoCargoId);
  }

  public Collection findAllConceptoCargo() throws Exception {
    return this.conceptoCargoBeanBusiness.findConceptoCargoAll();
  }

  public Collection findConceptoCargoByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    return this.conceptoCargoBeanBusiness.findByConceptoTipoPersonal(idConceptoTipoPersonal);
  }

  public Collection findConceptoCargoByCargo(long idCargo)
    throws Exception
  {
    return this.conceptoCargoBeanBusiness.findByCargo(idCargo);
  }

  public void addConceptoCargoAnio(ConceptoCargoAnio conceptoCargoAnio)
    throws Exception
  {
    this.conceptoCargoAnioBeanBusiness.addConceptoCargoAnio(conceptoCargoAnio);
  }

  public void updateConceptoCargoAnio(ConceptoCargoAnio conceptoCargoAnio) throws Exception {
    this.conceptoCargoAnioBeanBusiness.updateConceptoCargoAnio(conceptoCargoAnio);
  }

  public void deleteConceptoCargoAnio(ConceptoCargoAnio conceptoCargoAnio) throws Exception {
    this.conceptoCargoAnioBeanBusiness.deleteConceptoCargoAnio(conceptoCargoAnio);
  }

  public ConceptoCargoAnio findConceptoCargoAnioById(long conceptoCargoAnioId) throws Exception {
    return this.conceptoCargoAnioBeanBusiness.findConceptoCargoAnioById(conceptoCargoAnioId);
  }

  public Collection findAllConceptoCargoAnio() throws Exception {
    return this.conceptoCargoAnioBeanBusiness.findConceptoCargoAnioAll();
  }

  public Collection findConceptoCargoAnioByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    return this.conceptoCargoAnioBeanBusiness.findByConceptoTipoPersonal(idConceptoTipoPersonal);
  }

  public Collection findConceptoCargoAnioByCargo(long idCargo)
    throws Exception
  {
    return this.conceptoCargoAnioBeanBusiness.findByCargo(idCargo);
  }

  public void addConceptoDependencia(ConceptoDependencia conceptoDependencia)
    throws Exception
  {
    this.conceptoDependenciaBeanBusiness.addConceptoDependencia(conceptoDependencia);
  }

  public void updateConceptoDependencia(ConceptoDependencia conceptoDependencia) throws Exception {
    this.conceptoDependenciaBeanBusiness.updateConceptoDependencia(conceptoDependencia);
  }

  public void deleteConceptoDependencia(ConceptoDependencia conceptoDependencia) throws Exception {
    this.conceptoDependenciaBeanBusiness.deleteConceptoDependencia(conceptoDependencia);
  }

  public ConceptoDependencia findConceptoDependenciaById(long conceptoDependenciaId) throws Exception {
    return this.conceptoDependenciaBeanBusiness.findConceptoDependenciaById(conceptoDependenciaId);
  }

  public Collection findAllConceptoDependencia() throws Exception {
    return this.conceptoDependenciaBeanBusiness.findConceptoDependenciaAll();
  }

  public Collection findConceptoDependenciaByCaracteristicaDependencia(long idCaracteristicaDependencia)
    throws Exception
  {
    return this.conceptoDependenciaBeanBusiness.findByCaracteristicaDependencia(idCaracteristicaDependencia);
  }

  public void addConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal)
    throws Exception
  {
    this.conceptoTipoPersonalBeanBusiness.addConceptoTipoPersonal(conceptoTipoPersonal);
  }

  public void updateConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) throws Exception {
    this.conceptoTipoPersonalBeanBusiness.updateConceptoTipoPersonal(conceptoTipoPersonal);
  }

  public void deleteConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) throws Exception {
    this.conceptoTipoPersonalBeanBusiness.deleteConceptoTipoPersonal(conceptoTipoPersonal);
  }

  public ConceptoTipoPersonal findConceptoTipoPersonalById(long conceptoTipoPersonalId) throws Exception {
    return this.conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(conceptoTipoPersonalId);
  }

  public Collection findAllConceptoTipoPersonal() throws Exception {
    return this.conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalAll();
  }

  public Collection findConceptoTipoPersonalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.conceptoTipoPersonalBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalByConcepto(long idConcepto)
    throws Exception
  {
    return this.conceptoTipoPersonalBeanBusiness.findByConcepto(idConcepto);
  }

  public void addConceptoUtilidades(ConceptoUtilidades conceptoUtilidades)
    throws Exception
  {
    this.conceptoUtilidadesBeanBusiness.addConceptoUtilidades(conceptoUtilidades);
  }

  public void updateConceptoUtilidades(ConceptoUtilidades conceptoUtilidades) throws Exception {
    this.conceptoUtilidadesBeanBusiness.updateConceptoUtilidades(conceptoUtilidades);
  }

  public void deleteConceptoUtilidades(ConceptoUtilidades conceptoUtilidades) throws Exception {
    this.conceptoUtilidadesBeanBusiness.deleteConceptoUtilidades(conceptoUtilidades);
  }

  public ConceptoUtilidades findConceptoUtilidadesById(long conceptoUtilidadesId) throws Exception {
    return this.conceptoUtilidadesBeanBusiness.findConceptoUtilidadesById(conceptoUtilidadesId);
  }

  public Collection findAllConceptoUtilidades() throws Exception {
    return this.conceptoUtilidadesBeanBusiness.findConceptoUtilidadesAll();
  }

  public Collection findConceptoUtilidadesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.conceptoUtilidadesBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addConceptoVacaciones(ConceptoVacaciones conceptoVacaciones)
    throws Exception
  {
    this.conceptoVacacionesBeanBusiness.addConceptoVacaciones(conceptoVacaciones);
  }

  public void updateConceptoVacaciones(ConceptoVacaciones conceptoVacaciones) throws Exception {
    this.conceptoVacacionesBeanBusiness.updateConceptoVacaciones(conceptoVacaciones);
  }

  public void deleteConceptoVacaciones(ConceptoVacaciones conceptoVacaciones) throws Exception {
    this.conceptoVacacionesBeanBusiness.deleteConceptoVacaciones(conceptoVacaciones);
  }

  public ConceptoVacaciones findConceptoVacacionesById(long conceptoVacacionesId) throws Exception {
    return this.conceptoVacacionesBeanBusiness.findConceptoVacacionesById(conceptoVacacionesId);
  }

  public Collection findAllConceptoVacaciones() throws Exception {
    return this.conceptoVacacionesBeanBusiness.findConceptoVacacionesAll();
  }

  public Collection findConceptoVacacionesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.conceptoVacacionesBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addContratoColectivo(ContratoColectivo contratoColectivo)
    throws Exception
  {
    this.contratoColectivoBeanBusiness.addContratoColectivo(contratoColectivo);
  }

  public void updateContratoColectivo(ContratoColectivo contratoColectivo) throws Exception {
    this.contratoColectivoBeanBusiness.updateContratoColectivo(contratoColectivo);
  }

  public void deleteContratoColectivo(ContratoColectivo contratoColectivo) throws Exception {
    this.contratoColectivoBeanBusiness.deleteContratoColectivo(contratoColectivo);
  }

  public ContratoColectivo findContratoColectivoById(long contratoColectivoId) throws Exception {
    return this.contratoColectivoBeanBusiness.findContratoColectivoById(contratoColectivoId);
  }

  public Collection findAllContratoColectivo() throws Exception {
    return this.contratoColectivoBeanBusiness.findContratoColectivoAll();
  }

  public Collection findContratoColectivoByCodContratoColectivo(String codContratoColectivo)
    throws Exception
  {
    return this.contratoColectivoBeanBusiness.findByCodContratoColectivo(codContratoColectivo);
  }

  public Collection findContratoColectivoByDenominacion(String denominacion)
    throws Exception
  {
    return this.contratoColectivoBeanBusiness.findByDenominacion(denominacion);
  }

  public void addCuentaBanco(CuentaBanco cuentaBanco)
    throws Exception
  {
    this.cuentaBancoBeanBusiness.addCuentaBanco(cuentaBanco);
  }

  public void updateCuentaBanco(CuentaBanco cuentaBanco) throws Exception {
    this.cuentaBancoBeanBusiness.updateCuentaBanco(cuentaBanco);
  }

  public void deleteCuentaBanco(CuentaBanco cuentaBanco) throws Exception {
    this.cuentaBancoBeanBusiness.deleteCuentaBanco(cuentaBanco);
  }

  public CuentaBanco findCuentaBancoById(long cuentaBancoId) throws Exception {
    return this.cuentaBancoBeanBusiness.findCuentaBancoById(cuentaBancoId);
  }

  public Collection findAllCuentaBanco() throws Exception {
    return this.cuentaBancoBeanBusiness.findCuentaBancoAll();
  }

  public Collection findCuentaBancoByBanco(long idBanco, long idOrganismo)
    throws Exception
  {
    return this.cuentaBancoBeanBusiness.findByBanco(idBanco, idOrganismo);
  }

  public Collection findCuentaBancoByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.cuentaBancoBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addDetalleDisquete(DetalleDisquete detalleDisquete)
    throws Exception
  {
    this.detalleDisqueteBeanBusiness.addDetalleDisquete(detalleDisquete);
  }

  public void updateDetalleDisquete(DetalleDisquete detalleDisquete) throws Exception {
    this.detalleDisqueteBeanBusiness.updateDetalleDisquete(detalleDisquete);
  }

  public void deleteDetalleDisquete(DetalleDisquete detalleDisquete) throws Exception {
    this.detalleDisqueteBeanBusiness.deleteDetalleDisquete(detalleDisquete);
  }

  public DetalleDisquete findDetalleDisqueteById(long detalleDisqueteId) throws Exception {
    return this.detalleDisqueteBeanBusiness.findDetalleDisqueteById(detalleDisqueteId);
  }

  public Collection findAllDetalleDisquete() throws Exception {
    return this.detalleDisqueteBeanBusiness.findDetalleDisqueteAll();
  }

  public Collection findDetalleDisqueteByDisquete(long idDisquete)
    throws Exception
  {
    return this.detalleDisqueteBeanBusiness.findByDisquete(idDisquete);
  }

  public void addDiaFeriado(DiaFeriado diaFeriado)
    throws Exception
  {
    this.diaFeriadoBeanBusiness.addDiaFeriado(diaFeriado);
  }

  public void updateDiaFeriado(DiaFeriado diaFeriado) throws Exception {
    this.diaFeriadoBeanBusiness.updateDiaFeriado(diaFeriado);
  }

  public void deleteDiaFeriado(DiaFeriado diaFeriado) throws Exception {
    this.diaFeriadoBeanBusiness.deleteDiaFeriado(diaFeriado);
  }

  public DiaFeriado findDiaFeriadoById(long diaFeriadoId) throws Exception {
    return this.diaFeriadoBeanBusiness.findDiaFeriadoById(diaFeriadoId);
  }

  public Collection findAllDiaFeriado() throws Exception {
    return this.diaFeriadoBeanBusiness.findDiaFeriadoAll();
  }

  public Collection findDiaFeriadoByDia(Date dia)
    throws Exception
  {
    return this.diaFeriadoBeanBusiness.findByDia(dia);
  }

  public void addDisquete(Disquete disquete)
    throws Exception
  {
    this.disqueteBeanBusiness.addDisquete(disquete);
  }

  public void updateDisquete(Disquete disquete) throws Exception {
    this.disqueteBeanBusiness.updateDisquete(disquete);
  }

  public void deleteDisquete(Disquete disquete) throws Exception {
    this.disqueteBeanBusiness.deleteDisquete(disquete);
  }

  public Disquete findDisqueteById(long disqueteId) throws Exception {
    return this.disqueteBeanBusiness.findDisqueteById(disqueteId);
  }

  public Collection findAllDisquete() throws Exception {
    return this.disqueteBeanBusiness.findDisqueteAll();
  }

  public Collection findDisqueteByCodDisquete(String codDisquete, long idOrganismo)
    throws Exception
  {
    return this.disqueteBeanBusiness.findByCodDisquete(codDisquete, idOrganismo);
  }

  public Collection findDisqueteByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    return this.disqueteBeanBusiness.findByDescripcion(descripcion, idOrganismo);
  }

  public Collection findDisqueteByTipoDisquete(String tipoDisquete, long idOrganismo)
    throws Exception
  {
    return this.disqueteBeanBusiness.findByTipoDisquete(tipoDisquete, idOrganismo);
  }

  public Collection findDisqueteByIngresos(String ingresos, long idOrganismo)
    throws Exception
  {
    return this.disqueteBeanBusiness.findByIngresos(ingresos, idOrganismo);
  }

  public Collection findDisqueteByEgresos(String egresos, long idOrganismo)
    throws Exception
  {
    return this.disqueteBeanBusiness.findByEgresos(egresos, idOrganismo);
  }

  public Collection findDisqueteByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.disqueteBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addFirmasReportes(FirmasReportes firmasReportes)
    throws Exception
  {
    this.firmasReportesBeanBusiness.addFirmasReportes(firmasReportes);
  }

  public void updateFirmasReportes(FirmasReportes firmasReportes) throws Exception {
    this.firmasReportesBeanBusiness.updateFirmasReportes(firmasReportes);
  }

  public void deleteFirmasReportes(FirmasReportes firmasReportes) throws Exception {
    this.firmasReportesBeanBusiness.deleteFirmasReportes(firmasReportes);
  }

  public FirmasReportes findFirmasReportesById(long firmasReportesId) throws Exception {
    return this.firmasReportesBeanBusiness.findFirmasReportesById(firmasReportesId);
  }

  public Collection findAllFirmasReportes() throws Exception {
    return this.firmasReportesBeanBusiness.findFirmasReportesAll();
  }

  public Collection findFirmasReportesByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.firmasReportesBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findFirmasReportesByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.firmasReportesBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addFrecuenciaPago(FrecuenciaPago frecuenciaPago)
    throws Exception
  {
    this.frecuenciaPagoBeanBusiness.addFrecuenciaPago(frecuenciaPago);
  }

  public void updateFrecuenciaPago(FrecuenciaPago frecuenciaPago) throws Exception {
    this.frecuenciaPagoBeanBusiness.updateFrecuenciaPago(frecuenciaPago);
  }

  public void deleteFrecuenciaPago(FrecuenciaPago frecuenciaPago) throws Exception {
    this.frecuenciaPagoBeanBusiness.deleteFrecuenciaPago(frecuenciaPago);
  }

  public FrecuenciaPago findFrecuenciaPagoById(long frecuenciaPagoId) throws Exception {
    return this.frecuenciaPagoBeanBusiness.findFrecuenciaPagoById(frecuenciaPagoId);
  }

  public Collection findAllFrecuenciaPago() throws Exception {
    return this.frecuenciaPagoBeanBusiness.findFrecuenciaPagoAll();
  }

  public Collection findFrecuenciaPagoByCodFrecuenciaPago(int codFrecuenciaPago, long idOrganismo)
    throws Exception
  {
    return this.frecuenciaPagoBeanBusiness.findByCodFrecuenciaPago(codFrecuenciaPago, idOrganismo);
  }

  public Collection findFrecuenciaPagoByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.frecuenciaPagoBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findFrecuenciaPagoByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.frecuenciaPagoBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addFrecuenciaTipoPersonal(FrecuenciaTipoPersonal frecuenciaTipoPersonal)
    throws Exception
  {
    this.frecuenciaTipoPersonalBeanBusiness.addFrecuenciaTipoPersonal(frecuenciaTipoPersonal);
  }

  public void updateFrecuenciaTipoPersonal(FrecuenciaTipoPersonal frecuenciaTipoPersonal) throws Exception {
    this.frecuenciaTipoPersonalBeanBusiness.updateFrecuenciaTipoPersonal(frecuenciaTipoPersonal);
  }

  public void deleteFrecuenciaTipoPersonal(FrecuenciaTipoPersonal frecuenciaTipoPersonal) throws Exception {
    this.frecuenciaTipoPersonalBeanBusiness.deleteFrecuenciaTipoPersonal(frecuenciaTipoPersonal);
  }

  public FrecuenciaTipoPersonal findFrecuenciaTipoPersonalById(long frecuenciaTipoPersonalId) throws Exception {
    return this.frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(frecuenciaTipoPersonalId);
  }

  public Collection findAllFrecuenciaTipoPersonal() throws Exception {
    return this.frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalAll();
  }

  public Collection findFrecuenciaTipoPersonalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.frecuenciaTipoPersonalBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public Collection findFrecuenciaTipoPersonalByFrecuenciaPago(long idFrecuenciaPago)
    throws Exception
  {
    return this.frecuenciaTipoPersonalBeanBusiness.findByFrecuenciaPago(idFrecuenciaPago);
  }

  public void addGrupoNomina(GrupoNomina grupoNomina)
    throws Exception
  {
    this.grupoNominaBeanBusiness.addGrupoNomina(grupoNomina);
  }

  public void updateGrupoNomina(GrupoNomina grupoNomina) throws Exception {
    this.grupoNominaBeanBusiness.updateGrupoNomina(grupoNomina);
  }

  public void deleteGrupoNomina(GrupoNomina grupoNomina) throws Exception {
    this.grupoNominaBeanBusiness.deleteGrupoNomina(grupoNomina);
  }

  public GrupoNomina findGrupoNominaById(long grupoNominaId) throws Exception {
    return this.grupoNominaBeanBusiness.findGrupoNominaById(grupoNominaId);
  }

  public Collection findAllGrupoNomina() throws Exception {
    return this.grupoNominaBeanBusiness.findGrupoNominaAll();
  }

  public Collection findGrupoNominaByCodGrupoNomina(int codGrupoNomina, long idOrganismo)
    throws Exception
  {
    return this.grupoNominaBeanBusiness.findByCodGrupoNomina(codGrupoNomina, idOrganismo);
  }

  public Collection findGrupoNominaByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.grupoNominaBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findGrupoNominaByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.grupoNominaBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addMes(Mes mes)
    throws Exception
  {
    this.mesBeanBusiness.addMes(mes);
  }

  public void updateMes(Mes mes) throws Exception {
    this.mesBeanBusiness.updateMes(mes);
  }

  public void deleteMes(Mes mes) throws Exception {
    this.mesBeanBusiness.deleteMes(mes);
  }

  public Mes findMesById(long mesId) throws Exception {
    return this.mesBeanBusiness.findMesById(mesId);
  }

  public Collection findAllMes() throws Exception {
    return this.mesBeanBusiness.findMesAll();
  }

  public Collection findMesByAnio(int anio)
    throws Exception
  {
    return this.mesBeanBusiness.findByAnio(anio);
  }

  public void addParametroAri(ParametroAri parametroAri)
    throws Exception
  {
    this.parametroAriBeanBusiness.addParametroAri(parametroAri);
  }

  public void updateParametroAri(ParametroAri parametroAri) throws Exception {
    this.parametroAriBeanBusiness.updateParametroAri(parametroAri);
  }

  public void deleteParametroAri(ParametroAri parametroAri) throws Exception {
    this.parametroAriBeanBusiness.deleteParametroAri(parametroAri);
  }

  public ParametroAri findParametroAriById(long parametroAriId) throws Exception {
    return this.parametroAriBeanBusiness.findParametroAriById(parametroAriId);
  }

  public Collection findAllParametroAri() throws Exception {
    return this.parametroAriBeanBusiness.findParametroAriAll();
  }

  public Collection findParametroAriByUnidadTributaria(double unidadTributaria)
    throws Exception
  {
    return this.parametroAriBeanBusiness.findByUnidadTributaria(unidadTributaria);
  }

  public void addParametroGobierno(ParametroGobierno parametroGobierno)
    throws Exception
  {
    this.parametroGobiernoBeanBusiness.addParametroGobierno(parametroGobierno);
  }

  public void updateParametroGobierno(ParametroGobierno parametroGobierno) throws Exception {
    this.parametroGobiernoBeanBusiness.updateParametroGobierno(parametroGobierno);
  }

  public void deleteParametroGobierno(ParametroGobierno parametroGobierno) throws Exception {
    this.parametroGobiernoBeanBusiness.deleteParametroGobierno(parametroGobierno);
  }

  public ParametroGobierno findParametroGobiernoById(long parametroGobiernoId) throws Exception {
    return this.parametroGobiernoBeanBusiness.findParametroGobiernoById(parametroGobiernoId);
  }

  public Collection findAllParametroGobierno() throws Exception {
    return this.parametroGobiernoBeanBusiness.findParametroGobiernoAll();
  }

  public Collection findParametroGobiernoByGrupoOrganismo(long idGrupoOrganismo)
    throws Exception
  {
    return this.parametroGobiernoBeanBusiness.findByGrupoOrganismo(idGrupoOrganismo);
  }

  public void addParametroReportes(ParametroReportes parametroReportes)
    throws Exception
  {
    this.parametroReportesBeanBusiness.addParametroReportes(parametroReportes);
  }

  public void updateParametroReportes(ParametroReportes parametroReportes) throws Exception {
    this.parametroReportesBeanBusiness.updateParametroReportes(parametroReportes);
  }

  public void deleteParametroReportes(ParametroReportes parametroReportes) throws Exception {
    this.parametroReportesBeanBusiness.deleteParametroReportes(parametroReportes);
  }

  public ParametroReportes findParametroReportesById(long parametroReportesId) throws Exception {
    return this.parametroReportesBeanBusiness.findParametroReportesById(parametroReportesId);
  }

  public Collection findAllParametroReportes() throws Exception {
    return this.parametroReportesBeanBusiness.findParametroReportesAll();
  }

  public void addParametroVarios(ParametroVarios parametroVarios)
    throws Exception
  {
    this.parametroVariosBeanBusiness.addParametroVarios(parametroVarios);
  }

  public void updateParametroVarios(ParametroVarios parametroVarios) throws Exception {
    this.parametroVariosBeanBusiness.updateParametroVarios(parametroVarios);
  }

  public void deleteParametroVarios(ParametroVarios parametroVarios) throws Exception {
    this.parametroVariosBeanBusiness.deleteParametroVarios(parametroVarios);
  }

  public ParametroVarios findParametroVariosById(long parametroVariosId) throws Exception {
    return this.parametroVariosBeanBusiness.findParametroVariosById(parametroVariosId);
  }

  public Collection findAllParametroVarios() throws Exception {
    return this.parametroVariosBeanBusiness.findParametroVariosAll();
  }

  public Collection findParametroVariosByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.parametroVariosBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addPrimaAntiguedad(PrimaAntiguedad primaAntiguedad)
    throws Exception
  {
    this.primaAntiguedadBeanBusiness.addPrimaAntiguedad(primaAntiguedad);
  }

  public void updatePrimaAntiguedad(PrimaAntiguedad primaAntiguedad) throws Exception {
    this.primaAntiguedadBeanBusiness.updatePrimaAntiguedad(primaAntiguedad);
  }

  public void deletePrimaAntiguedad(PrimaAntiguedad primaAntiguedad) throws Exception {
    this.primaAntiguedadBeanBusiness.deletePrimaAntiguedad(primaAntiguedad);
  }

  public PrimaAntiguedad findPrimaAntiguedadById(long primaAntiguedadId) throws Exception {
    return this.primaAntiguedadBeanBusiness.findPrimaAntiguedadById(primaAntiguedadId);
  }

  public Collection findAllPrimaAntiguedad() throws Exception {
    return this.primaAntiguedadBeanBusiness.findPrimaAntiguedadAll();
  }

  public Collection findPrimaAntiguedadByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.primaAntiguedadBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addPrimaHijo(PrimaHijo primaHijo)
    throws Exception
  {
    this.primaHijoBeanBusiness.addPrimaHijo(primaHijo);
  }

  public void updatePrimaHijo(PrimaHijo primaHijo) throws Exception {
    this.primaHijoBeanBusiness.updatePrimaHijo(primaHijo);
  }

  public void deletePrimaHijo(PrimaHijo primaHijo) throws Exception {
    this.primaHijoBeanBusiness.deletePrimaHijo(primaHijo);
  }

  public PrimaHijo findPrimaHijoById(long primaHijoId) throws Exception {
    return this.primaHijoBeanBusiness.findPrimaHijoById(primaHijoId);
  }

  public Collection findAllPrimaHijo() throws Exception {
    return this.primaHijoBeanBusiness.findPrimaHijoAll();
  }

  public Collection findPrimaHijoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.primaHijoBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addRelacionPersonal(RelacionPersonal relacionPersonal)
    throws Exception
  {
    this.relacionPersonalBeanBusiness.addRelacionPersonal(relacionPersonal);
  }

  public void updateRelacionPersonal(RelacionPersonal relacionPersonal) throws Exception {
    this.relacionPersonalBeanBusiness.updateRelacionPersonal(relacionPersonal);
  }

  public void deleteRelacionPersonal(RelacionPersonal relacionPersonal) throws Exception {
    this.relacionPersonalBeanBusiness.deleteRelacionPersonal(relacionPersonal);
  }

  public RelacionPersonal findRelacionPersonalById(long relacionPersonalId) throws Exception {
    return this.relacionPersonalBeanBusiness.findRelacionPersonalById(relacionPersonalId);
  }

  public Collection findAllRelacionPersonal() throws Exception {
    return this.relacionPersonalBeanBusiness.findRelacionPersonalAll();
  }

  public Collection findRelacionPersonalByCodRelacion(String codRelacion)
    throws Exception
  {
    return this.relacionPersonalBeanBusiness.findByCodRelacion(codRelacion);
  }

  public Collection findRelacionPersonalByDescRelacion(String descRelacion)
    throws Exception
  {
    return this.relacionPersonalBeanBusiness.findByDescRelacion(descRelacion);
  }

  public void addRestringido(Restringido restringido)
    throws Exception
  {
    this.restringidoBeanBusiness.addRestringido(restringido);
  }

  public void updateRestringido(Restringido restringido) throws Exception {
    this.restringidoBeanBusiness.updateRestringido(restringido);
  }

  public void deleteRestringido(Restringido restringido) throws Exception {
    this.restringidoBeanBusiness.deleteRestringido(restringido);
  }

  public Restringido findRestringidoById(long restringidoId) throws Exception {
    return this.restringidoBeanBusiness.findRestringidoById(restringidoId);
  }

  public Collection findAllRestringido() throws Exception {
    return this.restringidoBeanBusiness.findRestringidoAll();
  }

  public Collection findRestringidoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.restringidoBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addSemana(Semana semana)
    throws Exception
  {
    this.semanaBeanBusiness.addSemana(semana);
  }

  public void updateSemana(Semana semana) throws Exception {
    this.semanaBeanBusiness.updateSemana(semana);
  }

  public void deleteSemana(Semana semana) throws Exception {
    this.semanaBeanBusiness.deleteSemana(semana);
  }

  public Semana findSemanaById(long semanaId) throws Exception {
    return this.semanaBeanBusiness.findSemanaById(semanaId);
  }

  public Collection findAllSemana() throws Exception {
    return this.semanaBeanBusiness.findSemanaAll();
  }

  public Collection findSemanaByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    return this.semanaBeanBusiness.findByGrupoNomina(idGrupoNomina);
  }

  public Collection findSemanaByAnio(int anio)
    throws Exception
  {
    return this.semanaBeanBusiness.findByAnio(anio);
  }

  public Semana findSemanaByAnioSemanaAnio(int anio, int semanaAnio) throws Exception
  {
    return this.semanaBeanBusiness.findByAnioSemanaAnio(anio, semanaAnio);
  }

  public void addSindicato(Sindicato sindicato)
    throws Exception
  {
    this.sindicatoBeanBusiness.addSindicato(sindicato);
  }

  public void updateSindicato(Sindicato sindicato) throws Exception {
    this.sindicatoBeanBusiness.updateSindicato(sindicato);
  }

  public void deleteSindicato(Sindicato sindicato) throws Exception {
    this.sindicatoBeanBusiness.deleteSindicato(sindicato);
  }

  public Sindicato findSindicatoById(long sindicatoId) throws Exception {
    return this.sindicatoBeanBusiness.findSindicatoById(sindicatoId);
  }

  public Collection findAllSindicato() throws Exception {
    return this.sindicatoBeanBusiness.findSindicatoAll();
  }

  public Collection findSindicatoByCodSindicato(String codSindicato)
    throws Exception
  {
    return this.sindicatoBeanBusiness.findByCodSindicato(codSindicato);
  }

  public Collection findSindicatoByNombre(String nombre)
    throws Exception
  {
    return this.sindicatoBeanBusiness.findByNombre(nombre);
  }

  public void addTarifaAri(TarifaAri tarifaAri)
    throws Exception
  {
    this.tarifaAriBeanBusiness.addTarifaAri(tarifaAri);
  }

  public void updateTarifaAri(TarifaAri tarifaAri) throws Exception {
    this.tarifaAriBeanBusiness.updateTarifaAri(tarifaAri);
  }

  public void deleteTarifaAri(TarifaAri tarifaAri) throws Exception {
    this.tarifaAriBeanBusiness.deleteTarifaAri(tarifaAri);
  }

  public TarifaAri findTarifaAriById(long tarifaAriId) throws Exception {
    return this.tarifaAriBeanBusiness.findTarifaAriById(tarifaAriId);
  }

  public Collection findAllTarifaAri() throws Exception {
    return this.tarifaAriBeanBusiness.findTarifaAriAll();
  }

  public Collection findTarifaAriByTarifa(double tarifa)
    throws Exception
  {
    return this.tarifaAriBeanBusiness.findByTarifa(tarifa);
  }

  public void addEscalaCuadroOnapre(EscalaCuadroOnapre escalaCuadroOnapre)
    throws Exception
  {
    this.escalaCuadroOnapreBeanBusiness.addEscalaCuadroOnapre(escalaCuadroOnapre);
  }

  public void updateEscalaCuadroOnapre(EscalaCuadroOnapre escalaCuadroOnapre) throws Exception {
    this.escalaCuadroOnapreBeanBusiness.updateEscalaCuadroOnapre(escalaCuadroOnapre);
  }

  public void deleteEscalaCuadroOnapre(EscalaCuadroOnapre escalaCuadroOnapre) throws Exception {
    this.escalaCuadroOnapreBeanBusiness.deleteEscalaCuadroOnapre(escalaCuadroOnapre);
  }

  public EscalaCuadroOnapre findEscalaCuadroOnapreById(long escalaCuadroOnapreId) throws Exception {
    return this.escalaCuadroOnapreBeanBusiness.findEscalaCuadroOnapreById(escalaCuadroOnapreId);
  }

  public Collection findAllEscalaCuadroOnapre() throws Exception {
    return this.escalaCuadroOnapreBeanBusiness.findEscalaCuadroOnapreAll();
  }

  public Collection findEscalaCuadroOnapreByCodigo(String codigo)
    throws Exception
  {
    return this.escalaCuadroOnapreBeanBusiness.findByCodigo(codigo);
  }

  public void addTipoAcreencia(TipoAcreencia tipoAcreencia)
    throws Exception
  {
    this.tipoAcreenciaBeanBusiness.addTipoAcreencia(tipoAcreencia);
  }

  public void updateTipoAcreencia(TipoAcreencia tipoAcreencia) throws Exception {
    this.tipoAcreenciaBeanBusiness.updateTipoAcreencia(tipoAcreencia);
  }

  public void deleteTipoAcreencia(TipoAcreencia tipoAcreencia) throws Exception {
    this.tipoAcreenciaBeanBusiness.deleteTipoAcreencia(tipoAcreencia);
  }

  public TipoAcreencia findTipoAcreenciaById(long tipoAcreenciaId) throws Exception {
    return this.tipoAcreenciaBeanBusiness.findTipoAcreenciaById(tipoAcreenciaId);
  }

  public Collection findAllTipoAcreencia() throws Exception {
    return this.tipoAcreenciaBeanBusiness.findTipoAcreenciaAll();
  }

  public void addTipoContrato(TipoContrato tipoContrato)
    throws Exception
  {
    this.tipoContratoBeanBusiness.addTipoContrato(tipoContrato);
  }

  public void updateTipoContrato(TipoContrato tipoContrato) throws Exception {
    this.tipoContratoBeanBusiness.updateTipoContrato(tipoContrato);
  }

  public void deleteTipoContrato(TipoContrato tipoContrato) throws Exception {
    this.tipoContratoBeanBusiness.deleteTipoContrato(tipoContrato);
  }

  public TipoContrato findTipoContratoById(long tipoContratoId) throws Exception {
    return this.tipoContratoBeanBusiness.findTipoContratoById(tipoContratoId);
  }

  public Collection findAllTipoContrato() throws Exception {
    return this.tipoContratoBeanBusiness.findTipoContratoAll();
  }

  public Collection findTipoContratoByCodTipoContrato(String codTipoContrato)
    throws Exception
  {
    return this.tipoContratoBeanBusiness.findByCodTipoContrato(codTipoContrato);
  }

  public Collection findTipoContratoByDescripcion(String descripcion)
    throws Exception
  {
    return this.tipoContratoBeanBusiness.findByDescripcion(descripcion);
  }

  public void addTipoPersonal(TipoPersonal tipoPersonal)
    throws Exception
  {
    this.tipoPersonalBeanBusiness.addTipoPersonal(tipoPersonal);
  }

  public void updateTipoPersonal(TipoPersonal tipoPersonal) throws Exception {
    this.tipoPersonalBeanBusiness.updateTipoPersonal(tipoPersonal);
  }

  public void deleteTipoPersonal(TipoPersonal tipoPersonal) throws Exception {
    this.tipoPersonalBeanBusiness.deleteTipoPersonal(tipoPersonal);
  }

  public TipoPersonal findTipoPersonalById(long tipoPersonalId) throws Exception {
    return this.tipoPersonalBeanBusiness.findTipoPersonalById(tipoPersonalId);
  }

  public Collection findAllTipoPersonal() throws Exception {
    return this.tipoPersonalBeanBusiness.findTipoPersonalAll();
  }

  public Collection findTipoPersonalByCodTipoPersonal(String codTipoPersonal, long idOrganismo)
    throws Exception
  {
    return this.tipoPersonalBeanBusiness.findByCodTipoPersonal(codTipoPersonal, idOrganismo);
  }

  public Collection findTipoPersonalByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.tipoPersonalBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findTipoPersonalByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.tipoPersonalBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addTurno(Turno turno)
    throws Exception
  {
    this.turnoBeanBusiness.addTurno(turno);
  }

  public void updateTurno(Turno turno) throws Exception {
    this.turnoBeanBusiness.updateTurno(turno);
  }

  public void deleteTurno(Turno turno) throws Exception {
    this.turnoBeanBusiness.deleteTurno(turno);
  }

  public Turno findTurnoById(long turnoId) throws Exception {
    return this.turnoBeanBusiness.findTurnoById(turnoId);
  }

  public Collection findAllTurno() throws Exception {
    return this.turnoBeanBusiness.findTurnoAll();
  }

  public Collection findTurnoByCodTurno(String codTurno, long idOrganismo)
    throws Exception
  {
    return this.turnoBeanBusiness.findByCodTurno(codTurno, idOrganismo);
  }

  public Collection findTurnoByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.turnoBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findTurnoByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.turnoBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addVacacionesPorAnio(VacacionesPorAnio vacacionesPorAnio)
    throws Exception
  {
    this.vacacionesPorAnioBeanBusiness.addVacacionesPorAnio(vacacionesPorAnio);
  }

  public void updateVacacionesPorAnio(VacacionesPorAnio vacacionesPorAnio) throws Exception {
    this.vacacionesPorAnioBeanBusiness.updateVacacionesPorAnio(vacacionesPorAnio);
  }

  public void deleteVacacionesPorAnio(VacacionesPorAnio vacacionesPorAnio) throws Exception {
    this.vacacionesPorAnioBeanBusiness.deleteVacacionesPorAnio(vacacionesPorAnio);
  }

  public VacacionesPorAnio findVacacionesPorAnioById(long vacacionesPorAnioId) throws Exception {
    return this.vacacionesPorAnioBeanBusiness.findVacacionesPorAnioById(vacacionesPorAnioId);
  }

  public Collection findAllVacacionesPorAnio() throws Exception {
    return this.vacacionesPorAnioBeanBusiness.findVacacionesPorAnioAll();
  }

  public Collection findVacacionesPorAnioByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.vacacionesPorAnioBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addVacacionesPorCargo(VacacionesPorCargo vacacionesPorCargo)
    throws Exception
  {
    this.vacacionesPorCargoBeanBusiness.addVacacionesPorCargo(vacacionesPorCargo);
  }

  public void updateVacacionesPorCargo(VacacionesPorCargo vacacionesPorCargo) throws Exception {
    this.vacacionesPorCargoBeanBusiness.updateVacacionesPorCargo(vacacionesPorCargo);
  }

  public void deleteVacacionesPorCargo(VacacionesPorCargo vacacionesPorCargo) throws Exception {
    this.vacacionesPorCargoBeanBusiness.deleteVacacionesPorCargo(vacacionesPorCargo);
  }

  public VacacionesPorCargo findVacacionesPorCargoById(long vacacionesPorCargoId) throws Exception {
    return this.vacacionesPorCargoBeanBusiness.findVacacionesPorCargoById(vacacionesPorCargoId);
  }

  public Collection findAllVacacionesPorCargo() throws Exception {
    return this.vacacionesPorCargoBeanBusiness.findVacacionesPorCargoAll();
  }

  public Collection findVacacionesPorCargoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.vacacionesPorCargoBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addUtilidadesPorAnio(UtilidadesPorAnio utilidadesPorAnio)
    throws Exception
  {
    this.utilidadesPorAnioBeanBusiness.addUtilidadesPorAnio(utilidadesPorAnio);
  }

  public void updateUtilidadesPorAnio(UtilidadesPorAnio utilidadesPorAnio) throws Exception {
    this.utilidadesPorAnioBeanBusiness.updateUtilidadesPorAnio(utilidadesPorAnio);
  }

  public void deleteUtilidadesPorAnio(UtilidadesPorAnio utilidadesPorAnio) throws Exception {
    this.utilidadesPorAnioBeanBusiness.deleteUtilidadesPorAnio(utilidadesPorAnio);
  }

  public UtilidadesPorAnio findUtilidadesPorAnioById(long utilidadesPorAnioId) throws Exception {
    return this.utilidadesPorAnioBeanBusiness.findUtilidadesPorAnioById(utilidadesPorAnioId);
  }

  public Collection findAllUtilidadesPorAnio() throws Exception {
    return this.utilidadesPorAnioBeanBusiness.findUtilidadesPorAnioAll();
  }

  public Collection findUtilidadesPorAnioByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.utilidadesPorAnioBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addParametroJubilacion(ParametroJubilacion parametroJubilacion)
    throws Exception
  {
    this.parametroJubilacionBeanBusiness.addParametroJubilacion(parametroJubilacion);
  }

  public void updateParametroJubilacion(ParametroJubilacion parametroJubilacion) throws Exception {
    this.parametroJubilacionBeanBusiness.updateParametroJubilacion(parametroJubilacion);
  }

  public void deleteParametroJubilacion(ParametroJubilacion parametroJubilacion) throws Exception {
    this.parametroJubilacionBeanBusiness.deleteParametroJubilacion(parametroJubilacion);
  }

  public ParametroJubilacion findParametroJubilacionById(long parametroJubilacionId) throws Exception {
    return this.parametroJubilacionBeanBusiness.findParametroJubilacionById(parametroJubilacionId);
  }

  public Collection findAllParametroJubilacion() throws Exception {
    return this.parametroJubilacionBeanBusiness.findParametroJubilacionAll();
  }

  public Collection findParametroJubilacionByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.parametroJubilacionBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }
}