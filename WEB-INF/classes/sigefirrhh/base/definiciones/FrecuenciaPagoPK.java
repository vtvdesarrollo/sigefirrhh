package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class FrecuenciaPagoPK
  implements Serializable
{
  public long idFrecuenciaPago;

  public FrecuenciaPagoPK()
  {
  }

  public FrecuenciaPagoPK(long idFrecuenciaPago)
  {
    this.idFrecuenciaPago = idFrecuenciaPago;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((FrecuenciaPagoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(FrecuenciaPagoPK thatPK)
  {
    return 
      this.idFrecuenciaPago == thatPK.idFrecuenciaPago;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idFrecuenciaPago)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idFrecuenciaPago);
  }
}