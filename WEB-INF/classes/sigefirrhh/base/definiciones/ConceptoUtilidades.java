package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ConceptoUtilidades
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO_PAGO;
  protected static final Map LISTA_SINO;
  private long idConceptoUtilidades;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private String tipo;
  private int mesInicio;
  private int mesFinal;
  private int numeroDias;
  private double numeroMeses;
  private int mesBuscar;
  private int semanaBuscar;
  private double factor;
  private double topeMonto;
  private double topeUnidades;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "factor", "idConceptoUtilidades", "mesBuscar", "mesFinal", "mesInicio", "numeroDias", "numeroMeses", "semanaBuscar", "tipo", "tipoPersonal", "topeMonto", "topeUnidades" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Double.TYPE, Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Double.TYPE, Double.TYPE }; private static final byte[] jdoFieldFlags = { 26, 21, 24, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoUtilidades"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoUtilidades());

    LISTA_TIPO_PAGO = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_TIPO_PAGO.put("B", "BASICO");
    LISTA_TIPO_PAGO.put("P", "PROMEDIO");
    LISTA_TIPO_PAGO.put("D", "DEVENGADO ANUAL");
    LISTA_TIPO_PAGO.put("E", "DEVENGADO ANUAL Y PROYECTADO");
    LISTA_TIPO_PAGO.put("M", "AL MES");
    LISTA_TIPO_PAGO.put("X", "PENDIENTE POR PAGAR (VARIABLE)");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public ConceptoUtilidades()
  {
    jdoSettipo(this, "B");

    jdoSetmesInicio(this, 0);

    jdoSetmesFinal(this, 0);

    jdoSetnumeroDias(this, 0);

    jdoSetnumeroMeses(this, 0.0D);

    jdoSetmesBuscar(this, 0);

    jdoSetsemanaBuscar(this, 0);

    jdoSetfactor(this, 1.0D);

    jdoSettopeMonto(this, 0.0D);

    jdoSettopeUnidades(this, 0.0D);
  }

  public String toString() {
    return jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + jdoGettipo(this);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public double getFactor() {
    return jdoGetfactor(this);
  }
  public void setFactor(double factor) {
    jdoSetfactor(this, factor);
  }
  public long getIdConceptoUtilidades() {
    return jdoGetidConceptoUtilidades(this);
  }
  public void setIdConceptoUtilidades(long idConceptoUtilidades) {
    jdoSetidConceptoUtilidades(this, idConceptoUtilidades);
  }
  public int getMesBuscar() {
    return jdoGetmesBuscar(this);
  }
  public void setMesBuscar(int mesBuscar) {
    jdoSetmesBuscar(this, mesBuscar);
  }
  public int getMesInicio() {
    return jdoGetmesInicio(this);
  }
  public void setMesInicio(int mesInicio) {
    jdoSetmesInicio(this, mesInicio);
  }

  public int getSemanaBuscar() {
    return jdoGetsemanaBuscar(this);
  }
  public void setSemanaBuscar(int semanaBuscar) {
    jdoSetsemanaBuscar(this, semanaBuscar);
  }
  public String getTipo() {
    return jdoGettipo(this);
  }
  public void setTipo(String tipo) {
    jdoSettipo(this, tipo);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }
  public double getTopeMonto() {
    return jdoGettopeMonto(this);
  }
  public void setTopeMonto(double topeMonto) {
    jdoSettopeMonto(this, topeMonto);
  }
  public double getTopeUnidades() {
    return jdoGettopeUnidades(this);
  }
  public void setTopeUnidades(double topeUnidades) {
    jdoSettopeUnidades(this, topeUnidades);
  }

  public int getMesFinal() {
    return jdoGetmesFinal(this);
  }
  public void setMesFinal(int mesFinal) {
    jdoSetmesFinal(this, mesFinal);
  }
  public int getNumeroDias() {
    return jdoGetnumeroDias(this);
  }
  public void setNumeroDias(int numeroDias) {
    jdoSetnumeroDias(this, numeroDias);
  }

  public double getNumeroMeses() {
    return jdoGetnumeroMeses(this);
  }
  public void setNumeroMeses(double numeroMeses) {
    jdoSetnumeroMeses(this, numeroMeses);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoUtilidades localConceptoUtilidades = new ConceptoUtilidades();
    localConceptoUtilidades.jdoFlags = 1;
    localConceptoUtilidades.jdoStateManager = paramStateManager;
    return localConceptoUtilidades;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoUtilidades localConceptoUtilidades = new ConceptoUtilidades();
    localConceptoUtilidades.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoUtilidades.jdoFlags = 1;
    localConceptoUtilidades.jdoStateManager = paramStateManager;
    return localConceptoUtilidades;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.factor);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoUtilidades);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesBuscar);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesFinal);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesInicio);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroDias);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.numeroMeses);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanaBuscar);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.topeMonto);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.topeUnidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.factor = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoUtilidades = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesBuscar = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesFinal = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesInicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroDias = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMeses = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanaBuscar = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.topeMonto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.topeUnidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoUtilidades paramConceptoUtilidades, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoUtilidades.conceptoTipoPersonal;
      return;
    case 1:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.factor = paramConceptoUtilidades.factor;
      return;
    case 2:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoUtilidades = paramConceptoUtilidades.idConceptoUtilidades;
      return;
    case 3:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.mesBuscar = paramConceptoUtilidades.mesBuscar;
      return;
    case 4:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.mesFinal = paramConceptoUtilidades.mesFinal;
      return;
    case 5:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.mesInicio = paramConceptoUtilidades.mesInicio;
      return;
    case 6:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.numeroDias = paramConceptoUtilidades.numeroDias;
      return;
    case 7:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMeses = paramConceptoUtilidades.numeroMeses;
      return;
    case 8:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.semanaBuscar = paramConceptoUtilidades.semanaBuscar;
      return;
    case 9:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramConceptoUtilidades.tipo;
      return;
    case 10:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramConceptoUtilidades.tipoPersonal;
      return;
    case 11:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.topeMonto = paramConceptoUtilidades.topeMonto;
      return;
    case 12:
      if (paramConceptoUtilidades == null)
        throw new IllegalArgumentException("arg1");
      this.topeUnidades = paramConceptoUtilidades.topeUnidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoUtilidades))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoUtilidades localConceptoUtilidades = (ConceptoUtilidades)paramObject;
    if (localConceptoUtilidades.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoUtilidades, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoUtilidadesPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoUtilidadesPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoUtilidadesPK))
      throw new IllegalArgumentException("arg1");
    ConceptoUtilidadesPK localConceptoUtilidadesPK = (ConceptoUtilidadesPK)paramObject;
    localConceptoUtilidadesPK.idConceptoUtilidades = this.idConceptoUtilidades;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoUtilidadesPK))
      throw new IllegalArgumentException("arg1");
    ConceptoUtilidadesPK localConceptoUtilidadesPK = (ConceptoUtilidadesPK)paramObject;
    this.idConceptoUtilidades = localConceptoUtilidadesPK.idConceptoUtilidades;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoUtilidadesPK))
      throw new IllegalArgumentException("arg2");
    ConceptoUtilidadesPK localConceptoUtilidadesPK = (ConceptoUtilidadesPK)paramObject;
    localConceptoUtilidadesPK.idConceptoUtilidades = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoUtilidadesPK))
      throw new IllegalArgumentException("arg2");
    ConceptoUtilidadesPK localConceptoUtilidadesPK = (ConceptoUtilidadesPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localConceptoUtilidadesPK.idConceptoUtilidades);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoUtilidades paramConceptoUtilidades)
  {
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 0))
      return paramConceptoUtilidades.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoUtilidades, jdoInheritedFieldCount + 0, paramConceptoUtilidades.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoUtilidades paramConceptoUtilidades, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoUtilidades, jdoInheritedFieldCount + 0, paramConceptoUtilidades.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final double jdoGetfactor(ConceptoUtilidades paramConceptoUtilidades)
  {
    if (paramConceptoUtilidades.jdoFlags <= 0)
      return paramConceptoUtilidades.factor;
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.factor;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 1))
      return paramConceptoUtilidades.factor;
    return localStateManager.getDoubleField(paramConceptoUtilidades, jdoInheritedFieldCount + 1, paramConceptoUtilidades.factor);
  }

  private static final void jdoSetfactor(ConceptoUtilidades paramConceptoUtilidades, double paramDouble)
  {
    if (paramConceptoUtilidades.jdoFlags == 0)
    {
      paramConceptoUtilidades.factor = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.factor = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoUtilidades, jdoInheritedFieldCount + 1, paramConceptoUtilidades.factor, paramDouble);
  }

  private static final long jdoGetidConceptoUtilidades(ConceptoUtilidades paramConceptoUtilidades)
  {
    return paramConceptoUtilidades.idConceptoUtilidades;
  }

  private static final void jdoSetidConceptoUtilidades(ConceptoUtilidades paramConceptoUtilidades, long paramLong)
  {
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.idConceptoUtilidades = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoUtilidades, jdoInheritedFieldCount + 2, paramConceptoUtilidades.idConceptoUtilidades, paramLong);
  }

  private static final int jdoGetmesBuscar(ConceptoUtilidades paramConceptoUtilidades)
  {
    if (paramConceptoUtilidades.jdoFlags <= 0)
      return paramConceptoUtilidades.mesBuscar;
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.mesBuscar;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 3))
      return paramConceptoUtilidades.mesBuscar;
    return localStateManager.getIntField(paramConceptoUtilidades, jdoInheritedFieldCount + 3, paramConceptoUtilidades.mesBuscar);
  }

  private static final void jdoSetmesBuscar(ConceptoUtilidades paramConceptoUtilidades, int paramInt)
  {
    if (paramConceptoUtilidades.jdoFlags == 0)
    {
      paramConceptoUtilidades.mesBuscar = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.mesBuscar = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoUtilidades, jdoInheritedFieldCount + 3, paramConceptoUtilidades.mesBuscar, paramInt);
  }

  private static final int jdoGetmesFinal(ConceptoUtilidades paramConceptoUtilidades)
  {
    if (paramConceptoUtilidades.jdoFlags <= 0)
      return paramConceptoUtilidades.mesFinal;
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.mesFinal;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 4))
      return paramConceptoUtilidades.mesFinal;
    return localStateManager.getIntField(paramConceptoUtilidades, jdoInheritedFieldCount + 4, paramConceptoUtilidades.mesFinal);
  }

  private static final void jdoSetmesFinal(ConceptoUtilidades paramConceptoUtilidades, int paramInt)
  {
    if (paramConceptoUtilidades.jdoFlags == 0)
    {
      paramConceptoUtilidades.mesFinal = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.mesFinal = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoUtilidades, jdoInheritedFieldCount + 4, paramConceptoUtilidades.mesFinal, paramInt);
  }

  private static final int jdoGetmesInicio(ConceptoUtilidades paramConceptoUtilidades)
  {
    if (paramConceptoUtilidades.jdoFlags <= 0)
      return paramConceptoUtilidades.mesInicio;
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.mesInicio;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 5))
      return paramConceptoUtilidades.mesInicio;
    return localStateManager.getIntField(paramConceptoUtilidades, jdoInheritedFieldCount + 5, paramConceptoUtilidades.mesInicio);
  }

  private static final void jdoSetmesInicio(ConceptoUtilidades paramConceptoUtilidades, int paramInt)
  {
    if (paramConceptoUtilidades.jdoFlags == 0)
    {
      paramConceptoUtilidades.mesInicio = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.mesInicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoUtilidades, jdoInheritedFieldCount + 5, paramConceptoUtilidades.mesInicio, paramInt);
  }

  private static final int jdoGetnumeroDias(ConceptoUtilidades paramConceptoUtilidades)
  {
    if (paramConceptoUtilidades.jdoFlags <= 0)
      return paramConceptoUtilidades.numeroDias;
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.numeroDias;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 6))
      return paramConceptoUtilidades.numeroDias;
    return localStateManager.getIntField(paramConceptoUtilidades, jdoInheritedFieldCount + 6, paramConceptoUtilidades.numeroDias);
  }

  private static final void jdoSetnumeroDias(ConceptoUtilidades paramConceptoUtilidades, int paramInt)
  {
    if (paramConceptoUtilidades.jdoFlags == 0)
    {
      paramConceptoUtilidades.numeroDias = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.numeroDias = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoUtilidades, jdoInheritedFieldCount + 6, paramConceptoUtilidades.numeroDias, paramInt);
  }

  private static final double jdoGetnumeroMeses(ConceptoUtilidades paramConceptoUtilidades)
  {
    if (paramConceptoUtilidades.jdoFlags <= 0)
      return paramConceptoUtilidades.numeroMeses;
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.numeroMeses;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 7))
      return paramConceptoUtilidades.numeroMeses;
    return localStateManager.getDoubleField(paramConceptoUtilidades, jdoInheritedFieldCount + 7, paramConceptoUtilidades.numeroMeses);
  }

  private static final void jdoSetnumeroMeses(ConceptoUtilidades paramConceptoUtilidades, double paramDouble)
  {
    if (paramConceptoUtilidades.jdoFlags == 0)
    {
      paramConceptoUtilidades.numeroMeses = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.numeroMeses = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoUtilidades, jdoInheritedFieldCount + 7, paramConceptoUtilidades.numeroMeses, paramDouble);
  }

  private static final int jdoGetsemanaBuscar(ConceptoUtilidades paramConceptoUtilidades)
  {
    if (paramConceptoUtilidades.jdoFlags <= 0)
      return paramConceptoUtilidades.semanaBuscar;
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.semanaBuscar;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 8))
      return paramConceptoUtilidades.semanaBuscar;
    return localStateManager.getIntField(paramConceptoUtilidades, jdoInheritedFieldCount + 8, paramConceptoUtilidades.semanaBuscar);
  }

  private static final void jdoSetsemanaBuscar(ConceptoUtilidades paramConceptoUtilidades, int paramInt)
  {
    if (paramConceptoUtilidades.jdoFlags == 0)
    {
      paramConceptoUtilidades.semanaBuscar = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.semanaBuscar = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoUtilidades, jdoInheritedFieldCount + 8, paramConceptoUtilidades.semanaBuscar, paramInt);
  }

  private static final String jdoGettipo(ConceptoUtilidades paramConceptoUtilidades)
  {
    if (paramConceptoUtilidades.jdoFlags <= 0)
      return paramConceptoUtilidades.tipo;
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.tipo;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 9))
      return paramConceptoUtilidades.tipo;
    return localStateManager.getStringField(paramConceptoUtilidades, jdoInheritedFieldCount + 9, paramConceptoUtilidades.tipo);
  }

  private static final void jdoSettipo(ConceptoUtilidades paramConceptoUtilidades, String paramString)
  {
    if (paramConceptoUtilidades.jdoFlags == 0)
    {
      paramConceptoUtilidades.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoUtilidades, jdoInheritedFieldCount + 9, paramConceptoUtilidades.tipo, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(ConceptoUtilidades paramConceptoUtilidades)
  {
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.tipoPersonal;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 10))
      return paramConceptoUtilidades.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramConceptoUtilidades, jdoInheritedFieldCount + 10, paramConceptoUtilidades.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ConceptoUtilidades paramConceptoUtilidades, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoUtilidades, jdoInheritedFieldCount + 10, paramConceptoUtilidades.tipoPersonal, paramTipoPersonal);
  }

  private static final double jdoGettopeMonto(ConceptoUtilidades paramConceptoUtilidades)
  {
    if (paramConceptoUtilidades.jdoFlags <= 0)
      return paramConceptoUtilidades.topeMonto;
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.topeMonto;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 11))
      return paramConceptoUtilidades.topeMonto;
    return localStateManager.getDoubleField(paramConceptoUtilidades, jdoInheritedFieldCount + 11, paramConceptoUtilidades.topeMonto);
  }

  private static final void jdoSettopeMonto(ConceptoUtilidades paramConceptoUtilidades, double paramDouble)
  {
    if (paramConceptoUtilidades.jdoFlags == 0)
    {
      paramConceptoUtilidades.topeMonto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.topeMonto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoUtilidades, jdoInheritedFieldCount + 11, paramConceptoUtilidades.topeMonto, paramDouble);
  }

  private static final double jdoGettopeUnidades(ConceptoUtilidades paramConceptoUtilidades)
  {
    if (paramConceptoUtilidades.jdoFlags <= 0)
      return paramConceptoUtilidades.topeUnidades;
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoUtilidades.topeUnidades;
    if (localStateManager.isLoaded(paramConceptoUtilidades, jdoInheritedFieldCount + 12))
      return paramConceptoUtilidades.topeUnidades;
    return localStateManager.getDoubleField(paramConceptoUtilidades, jdoInheritedFieldCount + 12, paramConceptoUtilidades.topeUnidades);
  }

  private static final void jdoSettopeUnidades(ConceptoUtilidades paramConceptoUtilidades, double paramDouble)
  {
    if (paramConceptoUtilidades.jdoFlags == 0)
    {
      paramConceptoUtilidades.topeUnidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoUtilidades.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoUtilidades.topeUnidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoUtilidades, jdoInheritedFieldCount + 12, paramConceptoUtilidades.topeUnidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}