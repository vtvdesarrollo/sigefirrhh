package sigefirrhh.base.definiciones;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class MesNoGenBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(MesNoGenBeanBusiness.class.getName());

  public Mes finMesByAnioAndMes(int anio, String mes)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "anio == pAnio && mes == pMes";

    Query query = pm.newQuery(Mes.class, filter);

    query.declareParameters("int pAnio, String pMes");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));
    parameters.put("pMes", mes);
    this.log.error("anio" + anio);
    this.log.error("mes" + mes);
    Collection colMes = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colMes.iterator();
    try
    {
      return (Mes)iterator.next();
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No existe el registro para este mes");
      throw error;
    }
  }
}