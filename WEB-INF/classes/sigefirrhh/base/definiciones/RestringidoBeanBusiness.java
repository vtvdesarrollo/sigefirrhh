package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class RestringidoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRestringido(Restringido restringido)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Restringido restringidoNew = 
      (Restringido)BeanUtils.cloneBean(
      restringido);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (restringidoNew.getTipoPersonal() != null) {
      restringidoNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        restringidoNew.getTipoPersonal().getIdTipoPersonal()));
    }

    TipoPersonalBeanBusiness personalRestringidoBeanBusiness = new TipoPersonalBeanBusiness();

    if (restringidoNew.getPersonalRestringido() != null) {
      restringidoNew.setPersonalRestringido(
        personalRestringidoBeanBusiness.findTipoPersonalById(
        restringidoNew.getPersonalRestringido().getIdTipoPersonal()));
    }
    pm.makePersistent(restringidoNew);
  }

  public void updateRestringido(Restringido restringido) throws Exception
  {
    Restringido restringidoModify = 
      findRestringidoById(restringido.getIdRestringido());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (restringido.getTipoPersonal() != null) {
      restringido.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        restringido.getTipoPersonal().getIdTipoPersonal()));
    }

    TipoPersonalBeanBusiness personalRestringidoBeanBusiness = new TipoPersonalBeanBusiness();

    if (restringido.getPersonalRestringido() != null) {
      restringido.setPersonalRestringido(
        personalRestringidoBeanBusiness.findTipoPersonalById(
        restringido.getPersonalRestringido().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(restringidoModify, restringido);
  }

  public void deleteRestringido(Restringido restringido) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Restringido restringidoDelete = 
      findRestringidoById(restringido.getIdRestringido());
    pm.deletePersistent(restringidoDelete);
  }

  public Restringido findRestringidoById(long idRestringido) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRestringido == pIdRestringido";
    Query query = pm.newQuery(Restringido.class, filter);

    query.declareParameters("long pIdRestringido");

    parameters.put("pIdRestringido", new Long(idRestringido));

    Collection colRestringido = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRestringido.iterator();
    return (Restringido)iterator.next();
  }

  public Collection findRestringidoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent restringidoExtent = pm.getExtent(
      Restringido.class, true);
    Query query = pm.newQuery(restringidoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(Restringido.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colRestringido = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRestringido);

    return colRestringido;
  }
}