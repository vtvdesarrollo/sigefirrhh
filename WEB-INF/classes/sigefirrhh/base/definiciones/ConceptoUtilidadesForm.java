package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ConceptoUtilidadesForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoUtilidadesForm.class.getName());
  private ConceptoUtilidades conceptoUtilidades;
  private Collection result;
  private int reportId;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private TipoPersonal tipoPersonal;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showConceptoUtilidadesByTipoPersonal;
  private String findSelectTipoPersonal;
  private Collection findColTipoPersonal;
  private Collection colTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private String selectTipoPersonal;
  private String selectConceptoTipoPersonal;
  private Object stateResultConceptoUtilidadesByTipoPersonal = null;

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));

    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("ConceptoUtilidades");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "ConceptoUtilidades" + this.reportId, report);
    newReportId();
    return null;
  }
  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }

  public boolean isShowMesBuscarAux()
  {
    try {
      return (this.conceptoUtilidades.getTipo().equals("M")) && 
        (this.tipoPersonal != null) && 
        (!this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"));
    } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowSemanaBuscarAux()
  {
    try
    {
      return (this.conceptoUtilidades.getTipo().equals("M")) && 
        (this.tipoPersonal != null) && 
        (this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowConcepto()
  {
    return (this.colConceptoTipoPersonal != null) && 
      (!this.colConceptoTipoPersonal.isEmpty());
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.colConceptoTipoPersonal = null;
    try
    {
      if (idTipoPersonal > 0L) {
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String getFindSelectTipoPersonal()
  {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.conceptoUtilidades.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.conceptoUtilidades.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    this.conceptoUtilidades.setConceptoTipoPersonal(null);
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()).equals(
        valConceptoTipoPersonal)) {
        this.conceptoUtilidades.setConceptoTipoPersonal(
          conceptoTipoPersonal);
        break;
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoUtilidades getConceptoUtilidades() {
    if (this.conceptoUtilidades == null) {
      this.conceptoUtilidades = new ConceptoUtilidades();
    }
    return this.conceptoUtilidades;
  }

  public ConceptoUtilidadesForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    newReportId();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getListTipo() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoUtilidades.LISTA_TIPO_PAGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConceptoUtilidadesByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findConceptoUtilidadesByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showConceptoUtilidadesByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoUtilidadesByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;

    return null;
  }

  public boolean isShowConceptoUtilidadesByTipoPersonal() {
    return this.showConceptoUtilidadesByTipoPersonal;
  }

  public String selectConceptoUtilidades()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectConceptoTipoPersonal = null;

    long idConceptoUtilidades = 
      Long.parseLong((String)requestParameterMap.get("idConceptoUtilidades"));
    try
    {
      this.conceptoUtilidades = 
        this.definicionesFacade.findConceptoUtilidadesById(
        idConceptoUtilidades);
      if (this.conceptoUtilidades.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.conceptoUtilidades.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.conceptoUtilidades.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.conceptoUtilidades.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.conceptoUtilidades = null;
    this.showConceptoUtilidadesByTipoPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addConceptoUtilidades(
          this.conceptoUtilidades);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.conceptoUtilidades);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateConceptoUtilidades(
          this.conceptoUtilidades);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.conceptoUtilidades);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteConceptoUtilidades(
        this.conceptoUtilidades);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.conceptoUtilidades);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.conceptoUtilidades = new ConceptoUtilidades();

    this.selectTipoPersonal = null;

    this.selectConceptoTipoPersonal = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoUtilidades.setIdConceptoUtilidades(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.ConceptoUtilidades"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.conceptoUtilidades = new ConceptoUtilidades();
    return "cancel";
  }

  public boolean isShowMesFinalAux() {
    try {
      return this.conceptoUtilidades.getTipo().equals("P"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowNumeroDiasAux()
  {
    try {
      return this.conceptoUtilidades.getTipo().equals("P"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowMesInicioAux()
  {
    try
    {
      return this.conceptoUtilidades.getTipo().equals("P"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowNumeroMesesAux()
  {
    try {
      return this.conceptoUtilidades.getTipo().equals("E"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
}