package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class SemanaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SemanaForm.class.getName());
  private Semana semana;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private int reportId;
  private String reportName;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private DefinicionesNoGenFacade definicionesNoGenFacade = new DefinicionesNoGenFacade();
  private boolean showSemanaByGrupoNomina;
  private boolean showSemanaByAnio;
  private String findSelectGrupoNomina;
  private int findAnio;
  private Collection findColGrupoNomina;
  private Collection colGrupoNomina;
  private String selectGrupoNomina;
  private Object stateScrollSemanaByGrupoNomina = null;
  private Object stateResultSemanaByGrupoNomina = null;

  private Object stateScrollSemanaByAnio = null;
  private Object stateResultSemanaByAnio = null;

  public String getFindSelectGrupoNomina()
  {
    return this.findSelectGrupoNomina;
  }
  public void setFindSelectGrupoNomina(String valGrupoNomina) {
    this.findSelectGrupoNomina = valGrupoNomina;
  }

  public Collection getFindColGrupoNomina() {
    Collection col = new ArrayList();
    if (this.colGrupoNomina != null) {
      Iterator iterator = this.findColGrupoNomina.iterator();
      GrupoNomina grupoNomina = null;
      while (iterator.hasNext()) {
        grupoNomina = (GrupoNomina)iterator.next();
        col.add(new SelectItem(
          String.valueOf(grupoNomina.getIdGrupoNomina()), 
          grupoNomina.toString()));
      }
    }
    return col;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }

  public String getSelectGrupoNomina()
  {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String valGrupoNomina) {
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    this.semana.setGrupoNomina(null);
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      if (String.valueOf(grupoNomina.getIdGrupoNomina()).equals(
        valGrupoNomina)) {
        this.semana.setGrupoNomina(
          grupoNomina);
      }
    }
    this.selectGrupoNomina = valGrupoNomina;
  }
  public Collection getResult() {
    return this.result;
  }

  public Semana getSemana() {
    if (this.semana == null) {
      this.semana = new Semana();
    }
    return this.semana;
  }

  public SemanaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    setReportName("Semana");
    this.reportId = JasperForWeb.newReportId(this.reportId);
    refresh();
  }

  public Collection getColGrupoNomina()
  {
    Collection col = new ArrayList();
    if (this.colGrupoNomina != null) {
      Iterator iterator = this.colGrupoNomina.iterator();
      GrupoNomina grupoNomina = null;
      while (iterator.hasNext()) {
        grupoNomina = (GrupoNomina)iterator.next();
        col.add(new SelectItem(
          String.valueOf(grupoNomina.getIdGrupoNomina()), 
          grupoNomina.toString()));
      }
    }
    return col;
  }

  public Collection getListMes()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Semana.LISTA_MES.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColGrupoNomina = 
        this.definicionesNoGenFacade.findGrupoNominaByOrganismoAndPeriodicidad(
        this.login.getOrganismo().getIdOrganismo(), "S");

      this.colGrupoNomina = 
        this.definicionesNoGenFacade.findGrupoNominaByOrganismoAndPeriodicidad(
        this.login.getOrganismo().getIdOrganismo(), "S");
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSemanaByGrupoNomina()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findSemanaByGrupoNomina(Long.valueOf(this.findSelectGrupoNomina).longValue());
      this.showSemanaByGrupoNomina = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSemanaByGrupoNomina)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoNomina = null;
    this.findAnio = 0;

    return null;
  }

  public String findSemanaByGrupoNominaAndAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesNoGenFacade.findSemanaByGrupoNominaAndAnio(Long.valueOf(this.findSelectGrupoNomina).longValue(), this.findAnio);
      this.showSemanaByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSemanaByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoNomina = null;
    this.findAnio = 0;

    return null;
  }

  public boolean isShowSemanaByGrupoNomina() {
    return this.showSemanaByGrupoNomina;
  }
  public boolean isShowSemanaByAnio() {
    return this.showSemanaByAnio;
  }

  public String selectSemana()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectGrupoNomina = null;

    long idSemana = 
      Long.parseLong((String)requestParameterMap.get("idSemana"));
    try
    {
      this.semana = 
        this.definicionesFacade.findSemanaById(
        idSemana);
      if (this.semana.getGrupoNomina() != null) {
        this.selectGrupoNomina = 
          String.valueOf(this.semana.getGrupoNomina().getIdGrupoNomina());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.semana = null;
    this.showSemanaByGrupoNomina = false;
    this.showSemanaByAnio = false;
  }

  public String edit()
  {
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    try {
      if (this.semana.getFechaInicio().compareTo(this.semana.getFechaFin()) >= 0) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Fecha Fin tiene un valor no valido", ""));
        error = true;
      }
    }
    catch (Exception localException1)
    {
    }
    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addSemana(
          this.semana);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.semana);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateSemana(
          this.semana);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.semana);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteSemana(
        this.semana);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.semana);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;
    this.semana = new Semana();

    this.selectGrupoNomina = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.semana.setIdSemana(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.Semana"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.scrollx = 0;
    this.scrolly = 0;
    resetResult();
    this.semana = new Semana();
    return "cancel";
  }

  public String runReport() {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/base/definiciones");

    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);
    setReportName("Semana");

    report.setReportName(getReportName());
    report.setPath(((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      getReportName() + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);
    return null;
  }

  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }
  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public String getReportName()
  {
    return this.reportName;
  }
  public void setReportName(String reportName) {
    this.reportName = reportName;
  }
}