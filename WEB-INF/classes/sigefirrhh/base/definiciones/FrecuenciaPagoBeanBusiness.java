package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class FrecuenciaPagoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addFrecuenciaPago(FrecuenciaPago frecuenciaPago)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    FrecuenciaPago frecuenciaPagoNew = 
      (FrecuenciaPago)BeanUtils.cloneBean(
      frecuenciaPago);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (frecuenciaPagoNew.getOrganismo() != null) {
      frecuenciaPagoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        frecuenciaPagoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(frecuenciaPagoNew);
  }

  public void updateFrecuenciaPago(FrecuenciaPago frecuenciaPago) throws Exception
  {
    FrecuenciaPago frecuenciaPagoModify = 
      findFrecuenciaPagoById(frecuenciaPago.getIdFrecuenciaPago());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (frecuenciaPago.getOrganismo() != null) {
      frecuenciaPago.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        frecuenciaPago.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(frecuenciaPagoModify, frecuenciaPago);
  }

  public void deleteFrecuenciaPago(FrecuenciaPago frecuenciaPago) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    FrecuenciaPago frecuenciaPagoDelete = 
      findFrecuenciaPagoById(frecuenciaPago.getIdFrecuenciaPago());
    pm.deletePersistent(frecuenciaPagoDelete);
  }

  public FrecuenciaPago findFrecuenciaPagoById(long idFrecuenciaPago) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idFrecuenciaPago == pIdFrecuenciaPago";
    Query query = pm.newQuery(FrecuenciaPago.class, filter);

    query.declareParameters("long pIdFrecuenciaPago");

    parameters.put("pIdFrecuenciaPago", new Long(idFrecuenciaPago));

    Collection colFrecuenciaPago = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colFrecuenciaPago.iterator();
    return (FrecuenciaPago)iterator.next();
  }

  public Collection findFrecuenciaPagoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent frecuenciaPagoExtent = pm.getExtent(
      FrecuenciaPago.class, true);
    Query query = pm.newQuery(frecuenciaPagoExtent);
    query.setOrdering("codFrecuenciaPago ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodFrecuenciaPago(int codFrecuenciaPago, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codFrecuenciaPago == pCodFrecuenciaPago &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(FrecuenciaPago.class, filter);

    query.declareParameters("int pCodFrecuenciaPago, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodFrecuenciaPago", new Integer(codFrecuenciaPago));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codFrecuenciaPago ascending");

    Collection colFrecuenciaPago = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFrecuenciaPago);

    return colFrecuenciaPago;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(FrecuenciaPago.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codFrecuenciaPago ascending");

    Collection colFrecuenciaPago = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFrecuenciaPago);

    return colFrecuenciaPago;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(FrecuenciaPago.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codFrecuenciaPago ascending");

    Collection colFrecuenciaPago = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFrecuenciaPago);

    return colFrecuenciaPago;
  }
}