package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class CategoriaPresupuesto
  implements Serializable, PersistenceCapable
{
  private long idCategoriaPresupuesto;
  private String codCategoria;
  private String descCategoria;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codCategoria", "descCategoria", "idCategoriaPresupuesto" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescCategoria(this);
  }

  public String getCodCategoria()
  {
    return jdoGetcodCategoria(this);
  }

  public String getDescCategoria()
  {
    return jdoGetdescCategoria(this);
  }

  public long getIdCategoriaPresupuesto()
  {
    return jdoGetidCategoriaPresupuesto(this);
  }

  public void setCodCategoria(String string)
  {
    jdoSetcodCategoria(this, string);
  }

  public void setDescCategoria(String string)
  {
    jdoSetdescCategoria(this, string);
  }

  public void setIdCategoriaPresupuesto(long l)
  {
    jdoSetidCategoriaPresupuesto(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.CategoriaPresupuesto"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CategoriaPresupuesto());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CategoriaPresupuesto localCategoriaPresupuesto = new CategoriaPresupuesto();
    localCategoriaPresupuesto.jdoFlags = 1;
    localCategoriaPresupuesto.jdoStateManager = paramStateManager;
    return localCategoriaPresupuesto;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CategoriaPresupuesto localCategoriaPresupuesto = new CategoriaPresupuesto();
    localCategoriaPresupuesto.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCategoriaPresupuesto.jdoFlags = 1;
    localCategoriaPresupuesto.jdoStateManager = paramStateManager;
    return localCategoriaPresupuesto;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCategoria);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descCategoria);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCategoriaPresupuesto);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCategoria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descCategoria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCategoriaPresupuesto = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CategoriaPresupuesto paramCategoriaPresupuesto, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCategoriaPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.codCategoria = paramCategoriaPresupuesto.codCategoria;
      return;
    case 1:
      if (paramCategoriaPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.descCategoria = paramCategoriaPresupuesto.descCategoria;
      return;
    case 2:
      if (paramCategoriaPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.idCategoriaPresupuesto = paramCategoriaPresupuesto.idCategoriaPresupuesto;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CategoriaPresupuesto))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CategoriaPresupuesto localCategoriaPresupuesto = (CategoriaPresupuesto)paramObject;
    if (localCategoriaPresupuesto.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCategoriaPresupuesto, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CategoriaPresupuestoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CategoriaPresupuestoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CategoriaPresupuestoPK))
      throw new IllegalArgumentException("arg1");
    CategoriaPresupuestoPK localCategoriaPresupuestoPK = (CategoriaPresupuestoPK)paramObject;
    localCategoriaPresupuestoPK.idCategoriaPresupuesto = this.idCategoriaPresupuesto;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CategoriaPresupuestoPK))
      throw new IllegalArgumentException("arg1");
    CategoriaPresupuestoPK localCategoriaPresupuestoPK = (CategoriaPresupuestoPK)paramObject;
    this.idCategoriaPresupuesto = localCategoriaPresupuestoPK.idCategoriaPresupuesto;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CategoriaPresupuestoPK))
      throw new IllegalArgumentException("arg2");
    CategoriaPresupuestoPK localCategoriaPresupuestoPK = (CategoriaPresupuestoPK)paramObject;
    localCategoriaPresupuestoPK.idCategoriaPresupuesto = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CategoriaPresupuestoPK))
      throw new IllegalArgumentException("arg2");
    CategoriaPresupuestoPK localCategoriaPresupuestoPK = (CategoriaPresupuestoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localCategoriaPresupuestoPK.idCategoriaPresupuesto);
  }

  private static final String jdoGetcodCategoria(CategoriaPresupuesto paramCategoriaPresupuesto)
  {
    if (paramCategoriaPresupuesto.jdoFlags <= 0)
      return paramCategoriaPresupuesto.codCategoria;
    StateManager localStateManager = paramCategoriaPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramCategoriaPresupuesto.codCategoria;
    if (localStateManager.isLoaded(paramCategoriaPresupuesto, jdoInheritedFieldCount + 0))
      return paramCategoriaPresupuesto.codCategoria;
    return localStateManager.getStringField(paramCategoriaPresupuesto, jdoInheritedFieldCount + 0, paramCategoriaPresupuesto.codCategoria);
  }

  private static final void jdoSetcodCategoria(CategoriaPresupuesto paramCategoriaPresupuesto, String paramString)
  {
    if (paramCategoriaPresupuesto.jdoFlags == 0)
    {
      paramCategoriaPresupuesto.codCategoria = paramString;
      return;
    }
    StateManager localStateManager = paramCategoriaPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramCategoriaPresupuesto.codCategoria = paramString;
      return;
    }
    localStateManager.setStringField(paramCategoriaPresupuesto, jdoInheritedFieldCount + 0, paramCategoriaPresupuesto.codCategoria, paramString);
  }

  private static final String jdoGetdescCategoria(CategoriaPresupuesto paramCategoriaPresupuesto)
  {
    if (paramCategoriaPresupuesto.jdoFlags <= 0)
      return paramCategoriaPresupuesto.descCategoria;
    StateManager localStateManager = paramCategoriaPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramCategoriaPresupuesto.descCategoria;
    if (localStateManager.isLoaded(paramCategoriaPresupuesto, jdoInheritedFieldCount + 1))
      return paramCategoriaPresupuesto.descCategoria;
    return localStateManager.getStringField(paramCategoriaPresupuesto, jdoInheritedFieldCount + 1, paramCategoriaPresupuesto.descCategoria);
  }

  private static final void jdoSetdescCategoria(CategoriaPresupuesto paramCategoriaPresupuesto, String paramString)
  {
    if (paramCategoriaPresupuesto.jdoFlags == 0)
    {
      paramCategoriaPresupuesto.descCategoria = paramString;
      return;
    }
    StateManager localStateManager = paramCategoriaPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramCategoriaPresupuesto.descCategoria = paramString;
      return;
    }
    localStateManager.setStringField(paramCategoriaPresupuesto, jdoInheritedFieldCount + 1, paramCategoriaPresupuesto.descCategoria, paramString);
  }

  private static final long jdoGetidCategoriaPresupuesto(CategoriaPresupuesto paramCategoriaPresupuesto)
  {
    return paramCategoriaPresupuesto.idCategoriaPresupuesto;
  }

  private static final void jdoSetidCategoriaPresupuesto(CategoriaPresupuesto paramCategoriaPresupuesto, long paramLong)
  {
    StateManager localStateManager = paramCategoriaPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramCategoriaPresupuesto.idCategoriaPresupuesto = paramLong;
      return;
    }
    localStateManager.setLongField(paramCategoriaPresupuesto, jdoInheritedFieldCount + 2, paramCategoriaPresupuesto.idCategoriaPresupuesto, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}