package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ConceptoUtilidadesPK
  implements Serializable
{
  public long idConceptoUtilidades;

  public ConceptoUtilidadesPK()
  {
  }

  public ConceptoUtilidadesPK(long idConceptoUtilidades)
  {
    this.idConceptoUtilidades = idConceptoUtilidades;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoUtilidadesPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoUtilidadesPK thatPK)
  {
    return 
      this.idConceptoUtilidades == thatPK.idConceptoUtilidades;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoUtilidades)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoUtilidades);
  }
}