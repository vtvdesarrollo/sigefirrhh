package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Restringido
  implements Serializable, PersistenceCapable
{
  private long idRestringido;
  private TipoPersonal tipoPersonal;
  private TipoPersonal personalRestringido;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idRestringido", "personalRestringido", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") };
  private static final byte[] jdoFieldFlags = { 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGettipoPersonal(this).toString() + " - " + jdoGetpersonalRestringido(this).toString();
  }

  public long getIdRestringido()
  {
    return jdoGetidRestringido(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setIdRestringido(long l)
  {
    jdoSetidRestringido(this, l);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public TipoPersonal getPersonalRestringido()
  {
    return jdoGetpersonalRestringido(this);
  }

  public void setPersonalRestringido(TipoPersonal personal)
  {
    jdoSetpersonalRestringido(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.Restringido"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Restringido());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Restringido localRestringido = new Restringido();
    localRestringido.jdoFlags = 1;
    localRestringido.jdoStateManager = paramStateManager;
    return localRestringido;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Restringido localRestringido = new Restringido();
    localRestringido.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRestringido.jdoFlags = 1;
    localRestringido.jdoStateManager = paramStateManager;
    return localRestringido;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRestringido);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personalRestringido);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRestringido = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personalRestringido = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Restringido paramRestringido, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRestringido == null)
        throw new IllegalArgumentException("arg1");
      this.idRestringido = paramRestringido.idRestringido;
      return;
    case 1:
      if (paramRestringido == null)
        throw new IllegalArgumentException("arg1");
      this.personalRestringido = paramRestringido.personalRestringido;
      return;
    case 2:
      if (paramRestringido == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramRestringido.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Restringido))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Restringido localRestringido = (Restringido)paramObject;
    if (localRestringido.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRestringido, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RestringidoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RestringidoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RestringidoPK))
      throw new IllegalArgumentException("arg1");
    RestringidoPK localRestringidoPK = (RestringidoPK)paramObject;
    localRestringidoPK.idRestringido = this.idRestringido;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RestringidoPK))
      throw new IllegalArgumentException("arg1");
    RestringidoPK localRestringidoPK = (RestringidoPK)paramObject;
    this.idRestringido = localRestringidoPK.idRestringido;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RestringidoPK))
      throw new IllegalArgumentException("arg2");
    RestringidoPK localRestringidoPK = (RestringidoPK)paramObject;
    localRestringidoPK.idRestringido = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RestringidoPK))
      throw new IllegalArgumentException("arg2");
    RestringidoPK localRestringidoPK = (RestringidoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localRestringidoPK.idRestringido);
  }

  private static final long jdoGetidRestringido(Restringido paramRestringido)
  {
    return paramRestringido.idRestringido;
  }

  private static final void jdoSetidRestringido(Restringido paramRestringido, long paramLong)
  {
    StateManager localStateManager = paramRestringido.jdoStateManager;
    if (localStateManager == null)
    {
      paramRestringido.idRestringido = paramLong;
      return;
    }
    localStateManager.setLongField(paramRestringido, jdoInheritedFieldCount + 0, paramRestringido.idRestringido, paramLong);
  }

  private static final TipoPersonal jdoGetpersonalRestringido(Restringido paramRestringido)
  {
    StateManager localStateManager = paramRestringido.jdoStateManager;
    if (localStateManager == null)
      return paramRestringido.personalRestringido;
    if (localStateManager.isLoaded(paramRestringido, jdoInheritedFieldCount + 1))
      return paramRestringido.personalRestringido;
    return (TipoPersonal)localStateManager.getObjectField(paramRestringido, jdoInheritedFieldCount + 1, paramRestringido.personalRestringido);
  }

  private static final void jdoSetpersonalRestringido(Restringido paramRestringido, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramRestringido.jdoStateManager;
    if (localStateManager == null)
    {
      paramRestringido.personalRestringido = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramRestringido, jdoInheritedFieldCount + 1, paramRestringido.personalRestringido, paramTipoPersonal);
  }

  private static final TipoPersonal jdoGettipoPersonal(Restringido paramRestringido)
  {
    StateManager localStateManager = paramRestringido.jdoStateManager;
    if (localStateManager == null)
      return paramRestringido.tipoPersonal;
    if (localStateManager.isLoaded(paramRestringido, jdoInheritedFieldCount + 2))
      return paramRestringido.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramRestringido, jdoInheritedFieldCount + 2, paramRestringido.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(Restringido paramRestringido, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramRestringido.jdoStateManager;
    if (localStateManager == null)
    {
      paramRestringido.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramRestringido, jdoInheritedFieldCount + 2, paramRestringido.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}