package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoContrato
  implements Serializable, PersistenceCapable
{
  private long idTipoContrato;
  private String codTipoContrato;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoContrato", "descripcion", "idTipoContrato" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + jdoGetcodTipoContrato(this);
  }

  public String getCodTipoContrato()
  {
    return jdoGetcodTipoContrato(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdTipoContrato()
  {
    return jdoGetidTipoContrato(this);
  }

  public void setCodTipoContrato(String string)
  {
    jdoSetcodTipoContrato(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdTipoContrato(long l)
  {
    jdoSetidTipoContrato(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.TipoContrato"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoContrato());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoContrato localTipoContrato = new TipoContrato();
    localTipoContrato.jdoFlags = 1;
    localTipoContrato.jdoStateManager = paramStateManager;
    return localTipoContrato;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoContrato localTipoContrato = new TipoContrato();
    localTipoContrato.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoContrato.jdoFlags = 1;
    localTipoContrato.jdoStateManager = paramStateManager;
    return localTipoContrato;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoContrato);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoContrato);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoContrato = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoContrato = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoContrato paramTipoContrato, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoContrato == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoContrato = paramTipoContrato.codTipoContrato;
      return;
    case 1:
      if (paramTipoContrato == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTipoContrato.descripcion;
      return;
    case 2:
      if (paramTipoContrato == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoContrato = paramTipoContrato.idTipoContrato;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoContrato))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoContrato localTipoContrato = (TipoContrato)paramObject;
    if (localTipoContrato.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoContrato, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoContratoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoContratoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoContratoPK))
      throw new IllegalArgumentException("arg1");
    TipoContratoPK localTipoContratoPK = (TipoContratoPK)paramObject;
    localTipoContratoPK.idTipoContrato = this.idTipoContrato;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoContratoPK))
      throw new IllegalArgumentException("arg1");
    TipoContratoPK localTipoContratoPK = (TipoContratoPK)paramObject;
    this.idTipoContrato = localTipoContratoPK.idTipoContrato;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoContratoPK))
      throw new IllegalArgumentException("arg2");
    TipoContratoPK localTipoContratoPK = (TipoContratoPK)paramObject;
    localTipoContratoPK.idTipoContrato = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoContratoPK))
      throw new IllegalArgumentException("arg2");
    TipoContratoPK localTipoContratoPK = (TipoContratoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localTipoContratoPK.idTipoContrato);
  }

  private static final String jdoGetcodTipoContrato(TipoContrato paramTipoContrato)
  {
    if (paramTipoContrato.jdoFlags <= 0)
      return paramTipoContrato.codTipoContrato;
    StateManager localStateManager = paramTipoContrato.jdoStateManager;
    if (localStateManager == null)
      return paramTipoContrato.codTipoContrato;
    if (localStateManager.isLoaded(paramTipoContrato, jdoInheritedFieldCount + 0))
      return paramTipoContrato.codTipoContrato;
    return localStateManager.getStringField(paramTipoContrato, jdoInheritedFieldCount + 0, paramTipoContrato.codTipoContrato);
  }

  private static final void jdoSetcodTipoContrato(TipoContrato paramTipoContrato, String paramString)
  {
    if (paramTipoContrato.jdoFlags == 0)
    {
      paramTipoContrato.codTipoContrato = paramString;
      return;
    }
    StateManager localStateManager = paramTipoContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoContrato.codTipoContrato = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoContrato, jdoInheritedFieldCount + 0, paramTipoContrato.codTipoContrato, paramString);
  }

  private static final String jdoGetdescripcion(TipoContrato paramTipoContrato)
  {
    if (paramTipoContrato.jdoFlags <= 0)
      return paramTipoContrato.descripcion;
    StateManager localStateManager = paramTipoContrato.jdoStateManager;
    if (localStateManager == null)
      return paramTipoContrato.descripcion;
    if (localStateManager.isLoaded(paramTipoContrato, jdoInheritedFieldCount + 1))
      return paramTipoContrato.descripcion;
    return localStateManager.getStringField(paramTipoContrato, jdoInheritedFieldCount + 1, paramTipoContrato.descripcion);
  }

  private static final void jdoSetdescripcion(TipoContrato paramTipoContrato, String paramString)
  {
    if (paramTipoContrato.jdoFlags == 0)
    {
      paramTipoContrato.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoContrato.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoContrato, jdoInheritedFieldCount + 1, paramTipoContrato.descripcion, paramString);
  }

  private static final long jdoGetidTipoContrato(TipoContrato paramTipoContrato)
  {
    return paramTipoContrato.idTipoContrato;
  }

  private static final void jdoSetidTipoContrato(TipoContrato paramTipoContrato, long paramLong)
  {
    StateManager localStateManager = paramTipoContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoContrato.idTipoContrato = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoContrato, jdoInheritedFieldCount + 2, paramTipoContrato.idTipoContrato, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}