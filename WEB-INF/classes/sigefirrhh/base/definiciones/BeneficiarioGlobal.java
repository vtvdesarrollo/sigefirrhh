package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class BeneficiarioGlobal
  implements Serializable, PersistenceCapable
{
  private long idBeneficiarioGlobal;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private String nombre;
  private int cedulaBeneficiario;
  private String rifBeneficiario;
  private String cuenta;
  private Banco banco;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "banco", "cedulaBeneficiario", "conceptoTipoPersonal", "cuenta", "idBeneficiarioGlobal", "nombre", "rifBeneficiario" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 26, 21, 26, 21, 24, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this);
  }

  public Banco getBanco()
  {
    return jdoGetbanco(this);
  }

  public int getCedulaBeneficiario()
  {
    return jdoGetcedulaBeneficiario(this);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public String getCuenta()
  {
    return jdoGetcuenta(this);
  }

  public long getIdBeneficiarioGlobal()
  {
    return jdoGetidBeneficiarioGlobal(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public String getRifBeneficiario()
  {
    return jdoGetrifBeneficiario(this);
  }

  public void setBanco(Banco banco)
  {
    jdoSetbanco(this, banco);
  }

  public void setCedulaBeneficiario(int i)
  {
    jdoSetcedulaBeneficiario(this, i);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setCuenta(String string)
  {
    jdoSetcuenta(this, string);
  }

  public void setIdBeneficiarioGlobal(long l)
  {
    jdoSetidBeneficiarioGlobal(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setRifBeneficiario(String string)
  {
    jdoSetrifBeneficiario(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.BeneficiarioGlobal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new BeneficiarioGlobal());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    BeneficiarioGlobal localBeneficiarioGlobal = new BeneficiarioGlobal();
    localBeneficiarioGlobal.jdoFlags = 1;
    localBeneficiarioGlobal.jdoStateManager = paramStateManager;
    return localBeneficiarioGlobal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    BeneficiarioGlobal localBeneficiarioGlobal = new BeneficiarioGlobal();
    localBeneficiarioGlobal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localBeneficiarioGlobal.jdoFlags = 1;
    localBeneficiarioGlobal.jdoStateManager = paramStateManager;
    return localBeneficiarioGlobal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.banco);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaBeneficiario);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cuenta);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idBeneficiarioGlobal);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.rifBeneficiario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.banco = ((Banco)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaBeneficiario = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuenta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idBeneficiarioGlobal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.rifBeneficiario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(BeneficiarioGlobal paramBeneficiarioGlobal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramBeneficiarioGlobal == null)
        throw new IllegalArgumentException("arg1");
      this.banco = paramBeneficiarioGlobal.banco;
      return;
    case 1:
      if (paramBeneficiarioGlobal == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaBeneficiario = paramBeneficiarioGlobal.cedulaBeneficiario;
      return;
    case 2:
      if (paramBeneficiarioGlobal == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramBeneficiarioGlobal.conceptoTipoPersonal;
      return;
    case 3:
      if (paramBeneficiarioGlobal == null)
        throw new IllegalArgumentException("arg1");
      this.cuenta = paramBeneficiarioGlobal.cuenta;
      return;
    case 4:
      if (paramBeneficiarioGlobal == null)
        throw new IllegalArgumentException("arg1");
      this.idBeneficiarioGlobal = paramBeneficiarioGlobal.idBeneficiarioGlobal;
      return;
    case 5:
      if (paramBeneficiarioGlobal == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramBeneficiarioGlobal.nombre;
      return;
    case 6:
      if (paramBeneficiarioGlobal == null)
        throw new IllegalArgumentException("arg1");
      this.rifBeneficiario = paramBeneficiarioGlobal.rifBeneficiario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof BeneficiarioGlobal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    BeneficiarioGlobal localBeneficiarioGlobal = (BeneficiarioGlobal)paramObject;
    if (localBeneficiarioGlobal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localBeneficiarioGlobal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new BeneficiarioGlobalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new BeneficiarioGlobalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BeneficiarioGlobalPK))
      throw new IllegalArgumentException("arg1");
    BeneficiarioGlobalPK localBeneficiarioGlobalPK = (BeneficiarioGlobalPK)paramObject;
    localBeneficiarioGlobalPK.idBeneficiarioGlobal = this.idBeneficiarioGlobal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BeneficiarioGlobalPK))
      throw new IllegalArgumentException("arg1");
    BeneficiarioGlobalPK localBeneficiarioGlobalPK = (BeneficiarioGlobalPK)paramObject;
    this.idBeneficiarioGlobal = localBeneficiarioGlobalPK.idBeneficiarioGlobal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BeneficiarioGlobalPK))
      throw new IllegalArgumentException("arg2");
    BeneficiarioGlobalPK localBeneficiarioGlobalPK = (BeneficiarioGlobalPK)paramObject;
    localBeneficiarioGlobalPK.idBeneficiarioGlobal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BeneficiarioGlobalPK))
      throw new IllegalArgumentException("arg2");
    BeneficiarioGlobalPK localBeneficiarioGlobalPK = (BeneficiarioGlobalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localBeneficiarioGlobalPK.idBeneficiarioGlobal);
  }

  private static final Banco jdoGetbanco(BeneficiarioGlobal paramBeneficiarioGlobal)
  {
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiarioGlobal.banco;
    if (localStateManager.isLoaded(paramBeneficiarioGlobal, jdoInheritedFieldCount + 0))
      return paramBeneficiarioGlobal.banco;
    return (Banco)localStateManager.getObjectField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 0, paramBeneficiarioGlobal.banco);
  }

  private static final void jdoSetbanco(BeneficiarioGlobal paramBeneficiarioGlobal, Banco paramBanco)
  {
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiarioGlobal.banco = paramBanco;
      return;
    }
    localStateManager.setObjectField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 0, paramBeneficiarioGlobal.banco, paramBanco);
  }

  private static final int jdoGetcedulaBeneficiario(BeneficiarioGlobal paramBeneficiarioGlobal)
  {
    if (paramBeneficiarioGlobal.jdoFlags <= 0)
      return paramBeneficiarioGlobal.cedulaBeneficiario;
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiarioGlobal.cedulaBeneficiario;
    if (localStateManager.isLoaded(paramBeneficiarioGlobal, jdoInheritedFieldCount + 1))
      return paramBeneficiarioGlobal.cedulaBeneficiario;
    return localStateManager.getIntField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 1, paramBeneficiarioGlobal.cedulaBeneficiario);
  }

  private static final void jdoSetcedulaBeneficiario(BeneficiarioGlobal paramBeneficiarioGlobal, int paramInt)
  {
    if (paramBeneficiarioGlobal.jdoFlags == 0)
    {
      paramBeneficiarioGlobal.cedulaBeneficiario = paramInt;
      return;
    }
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiarioGlobal.cedulaBeneficiario = paramInt;
      return;
    }
    localStateManager.setIntField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 1, paramBeneficiarioGlobal.cedulaBeneficiario, paramInt);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(BeneficiarioGlobal paramBeneficiarioGlobal)
  {
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiarioGlobal.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramBeneficiarioGlobal, jdoInheritedFieldCount + 2))
      return paramBeneficiarioGlobal.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 2, paramBeneficiarioGlobal.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(BeneficiarioGlobal paramBeneficiarioGlobal, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiarioGlobal.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 2, paramBeneficiarioGlobal.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGetcuenta(BeneficiarioGlobal paramBeneficiarioGlobal)
  {
    if (paramBeneficiarioGlobal.jdoFlags <= 0)
      return paramBeneficiarioGlobal.cuenta;
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiarioGlobal.cuenta;
    if (localStateManager.isLoaded(paramBeneficiarioGlobal, jdoInheritedFieldCount + 3))
      return paramBeneficiarioGlobal.cuenta;
    return localStateManager.getStringField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 3, paramBeneficiarioGlobal.cuenta);
  }

  private static final void jdoSetcuenta(BeneficiarioGlobal paramBeneficiarioGlobal, String paramString)
  {
    if (paramBeneficiarioGlobal.jdoFlags == 0)
    {
      paramBeneficiarioGlobal.cuenta = paramString;
      return;
    }
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiarioGlobal.cuenta = paramString;
      return;
    }
    localStateManager.setStringField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 3, paramBeneficiarioGlobal.cuenta, paramString);
  }

  private static final long jdoGetidBeneficiarioGlobal(BeneficiarioGlobal paramBeneficiarioGlobal)
  {
    return paramBeneficiarioGlobal.idBeneficiarioGlobal;
  }

  private static final void jdoSetidBeneficiarioGlobal(BeneficiarioGlobal paramBeneficiarioGlobal, long paramLong)
  {
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiarioGlobal.idBeneficiarioGlobal = paramLong;
      return;
    }
    localStateManager.setLongField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 4, paramBeneficiarioGlobal.idBeneficiarioGlobal, paramLong);
  }

  private static final String jdoGetnombre(BeneficiarioGlobal paramBeneficiarioGlobal)
  {
    if (paramBeneficiarioGlobal.jdoFlags <= 0)
      return paramBeneficiarioGlobal.nombre;
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiarioGlobal.nombre;
    if (localStateManager.isLoaded(paramBeneficiarioGlobal, jdoInheritedFieldCount + 5))
      return paramBeneficiarioGlobal.nombre;
    return localStateManager.getStringField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 5, paramBeneficiarioGlobal.nombre);
  }

  private static final void jdoSetnombre(BeneficiarioGlobal paramBeneficiarioGlobal, String paramString)
  {
    if (paramBeneficiarioGlobal.jdoFlags == 0)
    {
      paramBeneficiarioGlobal.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiarioGlobal.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 5, paramBeneficiarioGlobal.nombre, paramString);
  }

  private static final String jdoGetrifBeneficiario(BeneficiarioGlobal paramBeneficiarioGlobal)
  {
    if (paramBeneficiarioGlobal.jdoFlags <= 0)
      return paramBeneficiarioGlobal.rifBeneficiario;
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiarioGlobal.rifBeneficiario;
    if (localStateManager.isLoaded(paramBeneficiarioGlobal, jdoInheritedFieldCount + 6))
      return paramBeneficiarioGlobal.rifBeneficiario;
    return localStateManager.getStringField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 6, paramBeneficiarioGlobal.rifBeneficiario);
  }

  private static final void jdoSetrifBeneficiario(BeneficiarioGlobal paramBeneficiarioGlobal, String paramString)
  {
    if (paramBeneficiarioGlobal.jdoFlags == 0)
    {
      paramBeneficiarioGlobal.rifBeneficiario = paramString;
      return;
    }
    StateManager localStateManager = paramBeneficiarioGlobal.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiarioGlobal.rifBeneficiario = paramString;
      return;
    }
    localStateManager.setStringField(paramBeneficiarioGlobal, jdoInheritedFieldCount + 6, paramBeneficiarioGlobal.rifBeneficiario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}