package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class DisqueteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(DisqueteForm.class.getName());
  private Disquete disquete;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showDisqueteByCodDisquete;
  private boolean showDisqueteByDescripcion;
  private boolean showDisqueteByTipoDisquete;
  private boolean showDisqueteByIngresos;
  private boolean showDisqueteByEgresos;
  private String findCodDisquete;
  private String findDescripcion;
  private String findTipoDisquete;
  private String findIngresos;
  private String findEgresos;
  private Collection colBanco;
  private Collection colConcepto;
  private String selectBanco;
  private String selectConcepto;
  private Object stateResultDisqueteByCodDisquete = null;

  private Object stateResultDisqueteByDescripcion = null;

  private Object stateResultDisqueteByTipoDisquete = null;

  private Object stateResultDisqueteByIngresos = null;

  private Object stateResultDisqueteByEgresos = null;

  public String getFindCodDisquete()
  {
    return this.findCodDisquete;
  }
  public void setFindCodDisquete(String findCodDisquete) {
    this.findCodDisquete = findCodDisquete;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }
  public String getFindTipoDisquete() {
    return this.findTipoDisquete;
  }
  public void setFindTipoDisquete(String findTipoDisquete) {
    this.findTipoDisquete = findTipoDisquete;
  }
  public String getFindIngresos() {
    return this.findIngresos;
  }
  public void setFindIngresos(String findIngresos) {
    this.findIngresos = findIngresos;
  }
  public String getFindEgresos() {
    return this.findEgresos;
  }
  public void setFindEgresos(String findEgresos) {
    this.findEgresos = findEgresos;
  }

  public String getSelectBanco()
  {
    return this.selectBanco;
  }
  public void setSelectBanco(String valBanco) {
    Iterator iterator = this.colBanco.iterator();
    Banco banco = null;
    this.disquete.setBanco(null);
    while (iterator.hasNext()) {
      banco = (Banco)iterator.next();
      if (String.valueOf(banco.getIdBanco()).equals(
        valBanco)) {
        this.disquete.setBanco(
          banco);
        break;
      }
    }
    this.selectBanco = valBanco;
  }
  public String getSelectConcepto() {
    return this.selectConcepto;
  }
  public void setSelectConcepto(String valConcepto) {
    Iterator iterator = this.colConcepto.iterator();
    Concepto concepto = null;
    this.disquete.setConcepto(null);
    while (iterator.hasNext()) {
      concepto = (Concepto)iterator.next();
      if (String.valueOf(concepto.getIdConcepto()).equals(
        valConcepto)) {
        this.disquete.setConcepto(
          concepto);
        break;
      }
    }
    this.selectConcepto = valConcepto;
  }
  public Collection getResult() {
    return this.result;
  }

  public Disquete getDisquete() {
    if (this.disquete == null) {
      this.disquete = new Disquete();
    }
    return this.disquete;
  }

  public DisqueteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListTipoDisquete()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Disquete.LISTA_TIPO_DISQUETE.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListIngresos() {
    Collection col = new ArrayList();

    Iterator iterEntry = Disquete.LISTA_INGRESOS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListEgresos() {
    Collection col = new ArrayList();

    Iterator iterEntry = Disquete.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColBanco()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colBanco.iterator();
    Banco banco = null;
    while (iterator.hasNext()) {
      banco = (Banco)iterator.next();
      col.add(new SelectItem(
        String.valueOf(banco.getIdBanco()), 
        banco.toString()));
    }
    return col;
  }

  public Collection getColConcepto()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConcepto.iterator();
    Concepto concepto = null;
    while (iterator.hasNext()) {
      concepto = (Concepto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concepto.getIdConcepto()), 
        concepto.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colBanco = 
        this.definicionesFacade.findAllBanco();
      this.colConcepto = 
        this.definicionesFacade.findConceptoByAportePatronal(
        "S", this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findDisqueteByCodDisquete()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findDisqueteByCodDisquete(this.findCodDisquete, idOrganismo);
      this.showDisqueteByCodDisquete = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDisqueteByCodDisquete)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodDisquete = null;
    this.findDescripcion = null;
    this.findTipoDisquete = null;
    this.findIngresos = null;
    this.findEgresos = null;

    return null;
  }

  public String findDisqueteByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findDisqueteByDescripcion(this.findDescripcion, idOrganismo);
      this.showDisqueteByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDisqueteByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodDisquete = null;
    this.findDescripcion = null;
    this.findTipoDisquete = null;
    this.findIngresos = null;
    this.findEgresos = null;

    return null;
  }

  public String findDisqueteByTipoDisquete()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findDisqueteByTipoDisquete(this.findTipoDisquete, idOrganismo);
      this.showDisqueteByTipoDisquete = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDisqueteByTipoDisquete)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodDisquete = null;
    this.findDescripcion = null;
    this.findTipoDisquete = null;
    this.findIngresos = null;
    this.findEgresos = null;

    return null;
  }

  public String findDisqueteByIngresos()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findDisqueteByIngresos(this.findIngresos, idOrganismo);
      this.showDisqueteByIngresos = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDisqueteByIngresos)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodDisquete = null;
    this.findDescripcion = null;
    this.findTipoDisquete = null;
    this.findIngresos = null;
    this.findEgresos = null;

    return null;
  }

  public String findDisqueteByEgresos()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.definicionesFacade.findDisqueteByEgresos(this.findEgresos, idOrganismo);
      this.showDisqueteByEgresos = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDisqueteByEgresos)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodDisquete = null;
    this.findDescripcion = null;
    this.findTipoDisquete = null;
    this.findIngresos = null;
    this.findEgresos = null;

    return null;
  }

  public boolean isShowDisqueteByCodDisquete() {
    return this.showDisqueteByCodDisquete;
  }
  public boolean isShowDisqueteByDescripcion() {
    return this.showDisqueteByDescripcion;
  }
  public boolean isShowDisqueteByTipoDisquete() {
    return this.showDisqueteByTipoDisquete;
  }
  public boolean isShowDisqueteByIngresos() {
    return this.showDisqueteByIngresos;
  }
  public boolean isShowDisqueteByEgresos() {
    return this.showDisqueteByEgresos;
  }

  public String selectDisquete()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectBanco = null;
    this.selectConcepto = null;

    long idDisquete = 
      Long.parseLong((String)requestParameterMap.get("idDisquete"));
    try
    {
      this.disquete = 
        this.definicionesFacade.findDisqueteById(
        idDisquete);
      if (this.disquete.getBanco() != null) {
        this.selectBanco = 
          String.valueOf(this.disquete.getBanco().getIdBanco());
      }
      if (this.disquete.getConcepto() != null) {
        this.selectConcepto = 
          String.valueOf(this.disquete.getConcepto().getIdConcepto());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.disquete = null;
    this.showDisqueteByCodDisquete = false;
    this.showDisqueteByDescripcion = false;
    this.showDisqueteByTipoDisquete = false;
    this.showDisqueteByIngresos = false;
    this.showDisqueteByEgresos = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addDisquete(
          this.disquete);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.disquete);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateDisquete(
          this.disquete);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.disquete);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteDisquete(
        this.disquete);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.disquete);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.disquete = new Disquete();

    this.selectBanco = null;

    this.selectConcepto = null;

    this.disquete.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.disquete.setIdDisquete(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.Disquete"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.disquete = new Disquete();
    return "cancel";
  }

  public boolean isShowIngresosAux()
  {
    try
    {
      return (this.disquete.getTipoDisquete().equals("L")) || (this.disquete.getTipoDisquete().equals("F")) || (this.disquete.getTipoDisquete().equals("S")) || (this.disquete.getTipoDisquete().equals("A")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowEgresosAux()
  {
    try {
      return (this.disquete.getTipoDisquete().equals("A")) || (this.disquete.getTipoDisquete().equals("F")) || (this.disquete.getTipoDisquete().equals("S")) || (this.disquete.getTipoDisquete().equals("A")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowConceptoAux()
  {
    try
    {
      return (this.disquete.getTipoDisquete().equals("A")) || (this.disquete.getTipoDisquete().equals("S")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}