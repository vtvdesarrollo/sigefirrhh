package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class UtilidadesPorAnio
  implements Serializable, PersistenceCapable
{
  private long idUtilidadesPorAnio;
  private int aniosServicio;
  private int mesesServicios;
  private double diasBono;
  private double diasExtra;
  private TipoPersonal tipoPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aniosServicio", "diasBono", "diasExtra", "idUtilidadesPorAnio", "mesesServicios", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Double.TYPE, Double.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public UtilidadesPorAnio()
  {
    jdoSetaniosServicio(this, 0);

    jdoSetmesesServicios(this, 0);

    jdoSetdiasBono(this, 0.0D);

    jdoSetdiasExtra(this, 0.0D);
  }

  public String toString()
  {
    return jdoGettipoPersonal(this).getNombre() + " - " + 
      jdoGetaniosServicio(this) + " - " + 
      jdoGetdiasBono(this);
  }

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }

  public void setAniosServicio(int aniosServicio)
  {
    jdoSetaniosServicio(this, aniosServicio);
  }

  public double getDiasBono()
  {
    return jdoGetdiasBono(this);
  }

  public void setDiasBono(double diasBono)
  {
    jdoSetdiasBono(this, diasBono);
  }

  public double getDiasExtra()
  {
    return jdoGetdiasExtra(this);
  }

  public void setDiasExtra(double diasExtra)
  {
    jdoSetdiasExtra(this, diasExtra);
  }

  public long getIdUtilidadesPorAnio()
  {
    return jdoGetidUtilidadesPorAnio(this);
  }

  public void setIdUtilidadesPorAnio(long idUtilidadesPorAnio)
  {
    jdoSetidUtilidadesPorAnio(this, idUtilidadesPorAnio);
  }

  public int getMesesServicios()
  {
    return jdoGetmesesServicios(this);
  }

  public void setMesesServicios(int mesesServicios)
  {
    jdoSetmesesServicios(this, mesesServicios);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal)
  {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.UtilidadesPorAnio"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UtilidadesPorAnio());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UtilidadesPorAnio localUtilidadesPorAnio = new UtilidadesPorAnio();
    localUtilidadesPorAnio.jdoFlags = 1;
    localUtilidadesPorAnio.jdoStateManager = paramStateManager;
    return localUtilidadesPorAnio;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UtilidadesPorAnio localUtilidadesPorAnio = new UtilidadesPorAnio();
    localUtilidadesPorAnio.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUtilidadesPorAnio.jdoFlags = 1;
    localUtilidadesPorAnio.jdoStateManager = paramStateManager;
    return localUtilidadesPorAnio;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasBono);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasExtra);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUtilidadesPorAnio);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesesServicios);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasBono = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasExtra = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUtilidadesPorAnio = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesesServicios = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UtilidadesPorAnio paramUtilidadesPorAnio, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUtilidadesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramUtilidadesPorAnio.aniosServicio;
      return;
    case 1:
      if (paramUtilidadesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.diasBono = paramUtilidadesPorAnio.diasBono;
      return;
    case 2:
      if (paramUtilidadesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.diasExtra = paramUtilidadesPorAnio.diasExtra;
      return;
    case 3:
      if (paramUtilidadesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.idUtilidadesPorAnio = paramUtilidadesPorAnio.idUtilidadesPorAnio;
      return;
    case 4:
      if (paramUtilidadesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.mesesServicios = paramUtilidadesPorAnio.mesesServicios;
      return;
    case 5:
      if (paramUtilidadesPorAnio == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramUtilidadesPorAnio.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UtilidadesPorAnio))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UtilidadesPorAnio localUtilidadesPorAnio = (UtilidadesPorAnio)paramObject;
    if (localUtilidadesPorAnio.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUtilidadesPorAnio, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UtilidadesPorAnioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UtilidadesPorAnioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UtilidadesPorAnioPK))
      throw new IllegalArgumentException("arg1");
    UtilidadesPorAnioPK localUtilidadesPorAnioPK = (UtilidadesPorAnioPK)paramObject;
    localUtilidadesPorAnioPK.idUtilidadesPorAnio = this.idUtilidadesPorAnio;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UtilidadesPorAnioPK))
      throw new IllegalArgumentException("arg1");
    UtilidadesPorAnioPK localUtilidadesPorAnioPK = (UtilidadesPorAnioPK)paramObject;
    this.idUtilidadesPorAnio = localUtilidadesPorAnioPK.idUtilidadesPorAnio;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UtilidadesPorAnioPK))
      throw new IllegalArgumentException("arg2");
    UtilidadesPorAnioPK localUtilidadesPorAnioPK = (UtilidadesPorAnioPK)paramObject;
    localUtilidadesPorAnioPK.idUtilidadesPorAnio = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UtilidadesPorAnioPK))
      throw new IllegalArgumentException("arg2");
    UtilidadesPorAnioPK localUtilidadesPorAnioPK = (UtilidadesPorAnioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localUtilidadesPorAnioPK.idUtilidadesPorAnio);
  }

  private static final int jdoGetaniosServicio(UtilidadesPorAnio paramUtilidadesPorAnio)
  {
    if (paramUtilidadesPorAnio.jdoFlags <= 0)
      return paramUtilidadesPorAnio.aniosServicio;
    StateManager localStateManager = paramUtilidadesPorAnio.jdoStateManager;
    if (localStateManager == null)
      return paramUtilidadesPorAnio.aniosServicio;
    if (localStateManager.isLoaded(paramUtilidadesPorAnio, jdoInheritedFieldCount + 0))
      return paramUtilidadesPorAnio.aniosServicio;
    return localStateManager.getIntField(paramUtilidadesPorAnio, jdoInheritedFieldCount + 0, paramUtilidadesPorAnio.aniosServicio);
  }

  private static final void jdoSetaniosServicio(UtilidadesPorAnio paramUtilidadesPorAnio, int paramInt)
  {
    if (paramUtilidadesPorAnio.jdoFlags == 0)
    {
      paramUtilidadesPorAnio.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramUtilidadesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramUtilidadesPorAnio.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramUtilidadesPorAnio, jdoInheritedFieldCount + 0, paramUtilidadesPorAnio.aniosServicio, paramInt);
  }

  private static final double jdoGetdiasBono(UtilidadesPorAnio paramUtilidadesPorAnio)
  {
    if (paramUtilidadesPorAnio.jdoFlags <= 0)
      return paramUtilidadesPorAnio.diasBono;
    StateManager localStateManager = paramUtilidadesPorAnio.jdoStateManager;
    if (localStateManager == null)
      return paramUtilidadesPorAnio.diasBono;
    if (localStateManager.isLoaded(paramUtilidadesPorAnio, jdoInheritedFieldCount + 1))
      return paramUtilidadesPorAnio.diasBono;
    return localStateManager.getDoubleField(paramUtilidadesPorAnio, jdoInheritedFieldCount + 1, paramUtilidadesPorAnio.diasBono);
  }

  private static final void jdoSetdiasBono(UtilidadesPorAnio paramUtilidadesPorAnio, double paramDouble)
  {
    if (paramUtilidadesPorAnio.jdoFlags == 0)
    {
      paramUtilidadesPorAnio.diasBono = paramDouble;
      return;
    }
    StateManager localStateManager = paramUtilidadesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramUtilidadesPorAnio.diasBono = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramUtilidadesPorAnio, jdoInheritedFieldCount + 1, paramUtilidadesPorAnio.diasBono, paramDouble);
  }

  private static final double jdoGetdiasExtra(UtilidadesPorAnio paramUtilidadesPorAnio)
  {
    if (paramUtilidadesPorAnio.jdoFlags <= 0)
      return paramUtilidadesPorAnio.diasExtra;
    StateManager localStateManager = paramUtilidadesPorAnio.jdoStateManager;
    if (localStateManager == null)
      return paramUtilidadesPorAnio.diasExtra;
    if (localStateManager.isLoaded(paramUtilidadesPorAnio, jdoInheritedFieldCount + 2))
      return paramUtilidadesPorAnio.diasExtra;
    return localStateManager.getDoubleField(paramUtilidadesPorAnio, jdoInheritedFieldCount + 2, paramUtilidadesPorAnio.diasExtra);
  }

  private static final void jdoSetdiasExtra(UtilidadesPorAnio paramUtilidadesPorAnio, double paramDouble)
  {
    if (paramUtilidadesPorAnio.jdoFlags == 0)
    {
      paramUtilidadesPorAnio.diasExtra = paramDouble;
      return;
    }
    StateManager localStateManager = paramUtilidadesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramUtilidadesPorAnio.diasExtra = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramUtilidadesPorAnio, jdoInheritedFieldCount + 2, paramUtilidadesPorAnio.diasExtra, paramDouble);
  }

  private static final long jdoGetidUtilidadesPorAnio(UtilidadesPorAnio paramUtilidadesPorAnio)
  {
    return paramUtilidadesPorAnio.idUtilidadesPorAnio;
  }

  private static final void jdoSetidUtilidadesPorAnio(UtilidadesPorAnio paramUtilidadesPorAnio, long paramLong)
  {
    StateManager localStateManager = paramUtilidadesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramUtilidadesPorAnio.idUtilidadesPorAnio = paramLong;
      return;
    }
    localStateManager.setLongField(paramUtilidadesPorAnio, jdoInheritedFieldCount + 3, paramUtilidadesPorAnio.idUtilidadesPorAnio, paramLong);
  }

  private static final int jdoGetmesesServicios(UtilidadesPorAnio paramUtilidadesPorAnio)
  {
    if (paramUtilidadesPorAnio.jdoFlags <= 0)
      return paramUtilidadesPorAnio.mesesServicios;
    StateManager localStateManager = paramUtilidadesPorAnio.jdoStateManager;
    if (localStateManager == null)
      return paramUtilidadesPorAnio.mesesServicios;
    if (localStateManager.isLoaded(paramUtilidadesPorAnio, jdoInheritedFieldCount + 4))
      return paramUtilidadesPorAnio.mesesServicios;
    return localStateManager.getIntField(paramUtilidadesPorAnio, jdoInheritedFieldCount + 4, paramUtilidadesPorAnio.mesesServicios);
  }

  private static final void jdoSetmesesServicios(UtilidadesPorAnio paramUtilidadesPorAnio, int paramInt)
  {
    if (paramUtilidadesPorAnio.jdoFlags == 0)
    {
      paramUtilidadesPorAnio.mesesServicios = paramInt;
      return;
    }
    StateManager localStateManager = paramUtilidadesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramUtilidadesPorAnio.mesesServicios = paramInt;
      return;
    }
    localStateManager.setIntField(paramUtilidadesPorAnio, jdoInheritedFieldCount + 4, paramUtilidadesPorAnio.mesesServicios, paramInt);
  }

  private static final TipoPersonal jdoGettipoPersonal(UtilidadesPorAnio paramUtilidadesPorAnio)
  {
    StateManager localStateManager = paramUtilidadesPorAnio.jdoStateManager;
    if (localStateManager == null)
      return paramUtilidadesPorAnio.tipoPersonal;
    if (localStateManager.isLoaded(paramUtilidadesPorAnio, jdoInheritedFieldCount + 5))
      return paramUtilidadesPorAnio.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramUtilidadesPorAnio, jdoInheritedFieldCount + 5, paramUtilidadesPorAnio.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(UtilidadesPorAnio paramUtilidadesPorAnio, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramUtilidadesPorAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramUtilidadesPorAnio.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramUtilidadesPorAnio, jdoInheritedFieldCount + 5, paramUtilidadesPorAnio.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}