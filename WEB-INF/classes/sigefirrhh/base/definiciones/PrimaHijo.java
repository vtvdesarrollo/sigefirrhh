package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PrimaHijo
  implements Serializable, PersistenceCapable
{
  private long idPrimaHijo;
  private TipoPersonal tipoPersonal;
  private int edadMinima;
  private int edadMaxima;
  private int edadExcepcional;
  private double monto;
  private int numeroMaximoHijos;
  private int edadEstudiante;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "edadEstudiante", "edadExcepcional", "edadMaxima", "edadMinima", "idPrimaHijo", "idSitp", "monto", "numeroMaximoHijos", "tiempoSitp", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Long.TYPE, Integer.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public PrimaHijo()
  {
    jdoSetedadMinima(this, 0);

    jdoSetedadMaxima(this, 18);

    jdoSetedadExcepcional(this, 99);

    jdoSetmonto(this, 0.0D);

    jdoSetnumeroMaximoHijos(this, 0);

    jdoSetedadEstudiante(this, 0);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmonto(this));

    return jdoGettipoPersonal(this).getNombre() + " - " + 
      a;
  }

  public int getEdadEstudiante()
  {
    return jdoGetedadEstudiante(this);
  }

  public int getEdadExcepcional()
  {
    return jdoGetedadExcepcional(this);
  }

  public int getEdadMaxima()
  {
    return jdoGetedadMaxima(this);
  }

  public int getEdadMinima()
  {
    return jdoGetedadMinima(this);
  }

  public long getIdPrimaHijo()
  {
    return jdoGetidPrimaHijo(this);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public int getNumeroMaximoHijos()
  {
    return jdoGetnumeroMaximoHijos(this);
  }

  public void setEdadEstudiante(int i)
  {
    jdoSetedadEstudiante(this, i);
  }

  public void setEdadExcepcional(int i)
  {
    jdoSetedadExcepcional(this, i);
  }

  public void setEdadMaxima(int i)
  {
    jdoSetedadMaxima(this, i);
  }

  public void setEdadMinima(int i)
  {
    jdoSetedadMinima(this, i);
  }

  public void setIdPrimaHijo(long l)
  {
    jdoSetidPrimaHijo(this, l);
  }

  public void setMonto(double d)
  {
    jdoSetmonto(this, d);
  }

  public void setNumeroMaximoHijos(int i)
  {
    jdoSetnumeroMaximoHijos(this, i);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.PrimaHijo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PrimaHijo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PrimaHijo localPrimaHijo = new PrimaHijo();
    localPrimaHijo.jdoFlags = 1;
    localPrimaHijo.jdoStateManager = paramStateManager;
    return localPrimaHijo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PrimaHijo localPrimaHijo = new PrimaHijo();
    localPrimaHijo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPrimaHijo.jdoFlags = 1;
    localPrimaHijo.jdoStateManager = paramStateManager;
    return localPrimaHijo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadEstudiante);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadExcepcional);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadMaxima);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadMinima);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPrimaHijo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroMaximoHijos);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadEstudiante = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadExcepcional = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadMaxima = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadMinima = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPrimaHijo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMaximoHijos = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PrimaHijo paramPrimaHijo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPrimaHijo == null)
        throw new IllegalArgumentException("arg1");
      this.edadEstudiante = paramPrimaHijo.edadEstudiante;
      return;
    case 1:
      if (paramPrimaHijo == null)
        throw new IllegalArgumentException("arg1");
      this.edadExcepcional = paramPrimaHijo.edadExcepcional;
      return;
    case 2:
      if (paramPrimaHijo == null)
        throw new IllegalArgumentException("arg1");
      this.edadMaxima = paramPrimaHijo.edadMaxima;
      return;
    case 3:
      if (paramPrimaHijo == null)
        throw new IllegalArgumentException("arg1");
      this.edadMinima = paramPrimaHijo.edadMinima;
      return;
    case 4:
      if (paramPrimaHijo == null)
        throw new IllegalArgumentException("arg1");
      this.idPrimaHijo = paramPrimaHijo.idPrimaHijo;
      return;
    case 5:
      if (paramPrimaHijo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramPrimaHijo.idSitp;
      return;
    case 6:
      if (paramPrimaHijo == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramPrimaHijo.monto;
      return;
    case 7:
      if (paramPrimaHijo == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMaximoHijos = paramPrimaHijo.numeroMaximoHijos;
      return;
    case 8:
      if (paramPrimaHijo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramPrimaHijo.tiempoSitp;
      return;
    case 9:
      if (paramPrimaHijo == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramPrimaHijo.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PrimaHijo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PrimaHijo localPrimaHijo = (PrimaHijo)paramObject;
    if (localPrimaHijo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPrimaHijo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PrimaHijoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PrimaHijoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrimaHijoPK))
      throw new IllegalArgumentException("arg1");
    PrimaHijoPK localPrimaHijoPK = (PrimaHijoPK)paramObject;
    localPrimaHijoPK.idPrimaHijo = this.idPrimaHijo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrimaHijoPK))
      throw new IllegalArgumentException("arg1");
    PrimaHijoPK localPrimaHijoPK = (PrimaHijoPK)paramObject;
    this.idPrimaHijo = localPrimaHijoPK.idPrimaHijo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrimaHijoPK))
      throw new IllegalArgumentException("arg2");
    PrimaHijoPK localPrimaHijoPK = (PrimaHijoPK)paramObject;
    localPrimaHijoPK.idPrimaHijo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrimaHijoPK))
      throw new IllegalArgumentException("arg2");
    PrimaHijoPK localPrimaHijoPK = (PrimaHijoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localPrimaHijoPK.idPrimaHijo);
  }

  private static final int jdoGetedadEstudiante(PrimaHijo paramPrimaHijo)
  {
    if (paramPrimaHijo.jdoFlags <= 0)
      return paramPrimaHijo.edadEstudiante;
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaHijo.edadEstudiante;
    if (localStateManager.isLoaded(paramPrimaHijo, jdoInheritedFieldCount + 0))
      return paramPrimaHijo.edadEstudiante;
    return localStateManager.getIntField(paramPrimaHijo, jdoInheritedFieldCount + 0, paramPrimaHijo.edadEstudiante);
  }

  private static final void jdoSetedadEstudiante(PrimaHijo paramPrimaHijo, int paramInt)
  {
    if (paramPrimaHijo.jdoFlags == 0)
    {
      paramPrimaHijo.edadEstudiante = paramInt;
      return;
    }
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaHijo.edadEstudiante = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrimaHijo, jdoInheritedFieldCount + 0, paramPrimaHijo.edadEstudiante, paramInt);
  }

  private static final int jdoGetedadExcepcional(PrimaHijo paramPrimaHijo)
  {
    if (paramPrimaHijo.jdoFlags <= 0)
      return paramPrimaHijo.edadExcepcional;
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaHijo.edadExcepcional;
    if (localStateManager.isLoaded(paramPrimaHijo, jdoInheritedFieldCount + 1))
      return paramPrimaHijo.edadExcepcional;
    return localStateManager.getIntField(paramPrimaHijo, jdoInheritedFieldCount + 1, paramPrimaHijo.edadExcepcional);
  }

  private static final void jdoSetedadExcepcional(PrimaHijo paramPrimaHijo, int paramInt)
  {
    if (paramPrimaHijo.jdoFlags == 0)
    {
      paramPrimaHijo.edadExcepcional = paramInt;
      return;
    }
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaHijo.edadExcepcional = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrimaHijo, jdoInheritedFieldCount + 1, paramPrimaHijo.edadExcepcional, paramInt);
  }

  private static final int jdoGetedadMaxima(PrimaHijo paramPrimaHijo)
  {
    if (paramPrimaHijo.jdoFlags <= 0)
      return paramPrimaHijo.edadMaxima;
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaHijo.edadMaxima;
    if (localStateManager.isLoaded(paramPrimaHijo, jdoInheritedFieldCount + 2))
      return paramPrimaHijo.edadMaxima;
    return localStateManager.getIntField(paramPrimaHijo, jdoInheritedFieldCount + 2, paramPrimaHijo.edadMaxima);
  }

  private static final void jdoSetedadMaxima(PrimaHijo paramPrimaHijo, int paramInt)
  {
    if (paramPrimaHijo.jdoFlags == 0)
    {
      paramPrimaHijo.edadMaxima = paramInt;
      return;
    }
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaHijo.edadMaxima = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrimaHijo, jdoInheritedFieldCount + 2, paramPrimaHijo.edadMaxima, paramInt);
  }

  private static final int jdoGetedadMinima(PrimaHijo paramPrimaHijo)
  {
    if (paramPrimaHijo.jdoFlags <= 0)
      return paramPrimaHijo.edadMinima;
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaHijo.edadMinima;
    if (localStateManager.isLoaded(paramPrimaHijo, jdoInheritedFieldCount + 3))
      return paramPrimaHijo.edadMinima;
    return localStateManager.getIntField(paramPrimaHijo, jdoInheritedFieldCount + 3, paramPrimaHijo.edadMinima);
  }

  private static final void jdoSetedadMinima(PrimaHijo paramPrimaHijo, int paramInt)
  {
    if (paramPrimaHijo.jdoFlags == 0)
    {
      paramPrimaHijo.edadMinima = paramInt;
      return;
    }
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaHijo.edadMinima = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrimaHijo, jdoInheritedFieldCount + 3, paramPrimaHijo.edadMinima, paramInt);
  }

  private static final long jdoGetidPrimaHijo(PrimaHijo paramPrimaHijo)
  {
    return paramPrimaHijo.idPrimaHijo;
  }

  private static final void jdoSetidPrimaHijo(PrimaHijo paramPrimaHijo, long paramLong)
  {
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaHijo.idPrimaHijo = paramLong;
      return;
    }
    localStateManager.setLongField(paramPrimaHijo, jdoInheritedFieldCount + 4, paramPrimaHijo.idPrimaHijo, paramLong);
  }

  private static final int jdoGetidSitp(PrimaHijo paramPrimaHijo)
  {
    if (paramPrimaHijo.jdoFlags <= 0)
      return paramPrimaHijo.idSitp;
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaHijo.idSitp;
    if (localStateManager.isLoaded(paramPrimaHijo, jdoInheritedFieldCount + 5))
      return paramPrimaHijo.idSitp;
    return localStateManager.getIntField(paramPrimaHijo, jdoInheritedFieldCount + 5, paramPrimaHijo.idSitp);
  }

  private static final void jdoSetidSitp(PrimaHijo paramPrimaHijo, int paramInt)
  {
    if (paramPrimaHijo.jdoFlags == 0)
    {
      paramPrimaHijo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaHijo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrimaHijo, jdoInheritedFieldCount + 5, paramPrimaHijo.idSitp, paramInt);
  }

  private static final double jdoGetmonto(PrimaHijo paramPrimaHijo)
  {
    if (paramPrimaHijo.jdoFlags <= 0)
      return paramPrimaHijo.monto;
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaHijo.monto;
    if (localStateManager.isLoaded(paramPrimaHijo, jdoInheritedFieldCount + 6))
      return paramPrimaHijo.monto;
    return localStateManager.getDoubleField(paramPrimaHijo, jdoInheritedFieldCount + 6, paramPrimaHijo.monto);
  }

  private static final void jdoSetmonto(PrimaHijo paramPrimaHijo, double paramDouble)
  {
    if (paramPrimaHijo.jdoFlags == 0)
    {
      paramPrimaHijo.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaHijo.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrimaHijo, jdoInheritedFieldCount + 6, paramPrimaHijo.monto, paramDouble);
  }

  private static final int jdoGetnumeroMaximoHijos(PrimaHijo paramPrimaHijo)
  {
    if (paramPrimaHijo.jdoFlags <= 0)
      return paramPrimaHijo.numeroMaximoHijos;
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaHijo.numeroMaximoHijos;
    if (localStateManager.isLoaded(paramPrimaHijo, jdoInheritedFieldCount + 7))
      return paramPrimaHijo.numeroMaximoHijos;
    return localStateManager.getIntField(paramPrimaHijo, jdoInheritedFieldCount + 7, paramPrimaHijo.numeroMaximoHijos);
  }

  private static final void jdoSetnumeroMaximoHijos(PrimaHijo paramPrimaHijo, int paramInt)
  {
    if (paramPrimaHijo.jdoFlags == 0)
    {
      paramPrimaHijo.numeroMaximoHijos = paramInt;
      return;
    }
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaHijo.numeroMaximoHijos = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrimaHijo, jdoInheritedFieldCount + 7, paramPrimaHijo.numeroMaximoHijos, paramInt);
  }

  private static final Date jdoGettiempoSitp(PrimaHijo paramPrimaHijo)
  {
    if (paramPrimaHijo.jdoFlags <= 0)
      return paramPrimaHijo.tiempoSitp;
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaHijo.tiempoSitp;
    if (localStateManager.isLoaded(paramPrimaHijo, jdoInheritedFieldCount + 8))
      return paramPrimaHijo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramPrimaHijo, jdoInheritedFieldCount + 8, paramPrimaHijo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(PrimaHijo paramPrimaHijo, Date paramDate)
  {
    if (paramPrimaHijo.jdoFlags == 0)
    {
      paramPrimaHijo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaHijo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPrimaHijo, jdoInheritedFieldCount + 8, paramPrimaHijo.tiempoSitp, paramDate);
  }

  private static final TipoPersonal jdoGettipoPersonal(PrimaHijo paramPrimaHijo)
  {
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaHijo.tipoPersonal;
    if (localStateManager.isLoaded(paramPrimaHijo, jdoInheritedFieldCount + 9))
      return paramPrimaHijo.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramPrimaHijo, jdoInheritedFieldCount + 9, paramPrimaHijo.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(PrimaHijo paramPrimaHijo, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramPrimaHijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaHijo.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramPrimaHijo, jdoInheritedFieldCount + 9, paramPrimaHijo.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}