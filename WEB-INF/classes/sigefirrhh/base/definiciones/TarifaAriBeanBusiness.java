package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TarifaAriBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTarifaAri(TarifaAri tarifaAri)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TarifaAri tarifaAriNew = 
      (TarifaAri)BeanUtils.cloneBean(
      tarifaAri);

    pm.makePersistent(tarifaAriNew);
  }

  public void updateTarifaAri(TarifaAri tarifaAri) throws Exception
  {
    TarifaAri tarifaAriModify = 
      findTarifaAriById(tarifaAri.getIdTarifaAri());

    BeanUtils.copyProperties(tarifaAriModify, tarifaAri);
  }

  public void deleteTarifaAri(TarifaAri tarifaAri) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TarifaAri tarifaAriDelete = 
      findTarifaAriById(tarifaAri.getIdTarifaAri());
    pm.deletePersistent(tarifaAriDelete);
  }

  public TarifaAri findTarifaAriById(long idTarifaAri) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTarifaAri == pIdTarifaAri";
    Query query = pm.newQuery(TarifaAri.class, filter);

    query.declareParameters("long pIdTarifaAri");

    parameters.put("pIdTarifaAri", new Long(idTarifaAri));

    Collection colTarifaAri = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTarifaAri.iterator();
    return (TarifaAri)iterator.next();
  }

  public Collection findTarifaAriAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tarifaAriExtent = pm.getExtent(
      TarifaAri.class, true);
    Query query = pm.newQuery(tarifaAriExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTarifa(double tarifa)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tarifa == pTarifa";

    Query query = pm.newQuery(TarifaAri.class, filter);

    query.declareParameters("double pTarifa");
    HashMap parameters = new HashMap();

    parameters.put("pTarifa", new Double(tarifa));

    Collection colTarifaAri = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTarifaAri);

    return colTarifaAri;
  }
}