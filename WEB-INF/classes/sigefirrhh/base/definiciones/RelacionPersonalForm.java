package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class RelacionPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RelacionPersonalForm.class.getName());
  private RelacionPersonal relacionPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showRelacionPersonalByCodRelacion;
  private boolean showRelacionPersonalByDescRelacion;
  private String findCodRelacion;
  private String findDescRelacion;
  private Object stateResultRelacionPersonalByCodRelacion = null;

  private Object stateResultRelacionPersonalByDescRelacion = null;

  public String getFindCodRelacion()
  {
    return this.findCodRelacion;
  }
  public void setFindCodRelacion(String findCodRelacion) {
    this.findCodRelacion = findCodRelacion;
  }
  public String getFindDescRelacion() {
    return this.findDescRelacion;
  }
  public void setFindDescRelacion(String findDescRelacion) {
    this.findDescRelacion = findDescRelacion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public RelacionPersonal getRelacionPersonal() {
    if (this.relacionPersonal == null) {
      this.relacionPersonal = new RelacionPersonal();
    }
    return this.relacionPersonal;
  }

  public RelacionPersonalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findRelacionPersonalByCodRelacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findRelacionPersonalByCodRelacion(this.findCodRelacion);
      this.showRelacionPersonalByCodRelacion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRelacionPersonalByCodRelacion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodRelacion = null;
    this.findDescRelacion = null;

    return null;
  }

  public String findRelacionPersonalByDescRelacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findRelacionPersonalByDescRelacion(this.findDescRelacion);
      this.showRelacionPersonalByDescRelacion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRelacionPersonalByDescRelacion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodRelacion = null;
    this.findDescRelacion = null;

    return null;
  }

  public boolean isShowRelacionPersonalByCodRelacion() {
    return this.showRelacionPersonalByCodRelacion;
  }
  public boolean isShowRelacionPersonalByDescRelacion() {
    return this.showRelacionPersonalByDescRelacion;
  }

  public String selectRelacionPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idRelacionPersonal = 
      Long.parseLong((String)requestParameterMap.get("idRelacionPersonal"));
    try
    {
      this.relacionPersonal = 
        this.definicionesFacade.findRelacionPersonalById(
        idRelacionPersonal);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.relacionPersonal = null;
    this.showRelacionPersonalByCodRelacion = false;
    this.showRelacionPersonalByDescRelacion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addRelacionPersonal(
          this.relacionPersonal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateRelacionPersonal(
          this.relacionPersonal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteRelacionPersonal(
        this.relacionPersonal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.relacionPersonal = new RelacionPersonal();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.relacionPersonal.setIdRelacionPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.RelacionPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.relacionPersonal = new RelacionPersonal();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("RelacionPersonal");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "RelacionPersonal" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}