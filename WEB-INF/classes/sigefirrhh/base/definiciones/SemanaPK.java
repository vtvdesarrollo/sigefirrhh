package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class SemanaPK
  implements Serializable
{
  public long idSemana;

  public SemanaPK()
  {
  }

  public SemanaPK(long idSemana)
  {
    this.idSemana = idSemana;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SemanaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SemanaPK thatPK)
  {
    return 
      this.idSemana == thatPK.idSemana;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSemana)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSemana);
  }
}