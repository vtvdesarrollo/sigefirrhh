package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class GrupoNominaPK
  implements Serializable
{
  public long idGrupoNomina;

  public GrupoNominaPK()
  {
  }

  public GrupoNominaPK(long idGrupoNomina)
  {
    this.idGrupoNomina = idGrupoNomina;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((GrupoNominaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(GrupoNominaPK thatPK)
  {
    return 
      this.idGrupoNomina == thatPK.idGrupoNomina;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idGrupoNomina)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idGrupoNomina);
  }
}