package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class BancoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addBanco(Banco banco)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Banco bancoNew = 
      (Banco)BeanUtils.cloneBean(
      banco);

    pm.makePersistent(bancoNew);

    pm = null;
  }

  public void updateBanco(Banco banco) throws Exception {
    Banco bancoModify = 
      findBancoById(banco.getIdBanco());

    BeanUtils.copyProperties(bancoModify, banco);
  }

  public void deleteBanco(Banco banco) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Banco bancoDelete = 
      findBancoById(banco.getIdBanco());
    pm.deletePersistent(bancoDelete);

    pm = null;
  }

  public Banco findBancoById(long idBanco) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idBanco == pIdBanco";
    Query query = pm.newQuery(Banco.class, filter);

    query.declareParameters("long pIdBanco");

    parameters.put("pIdBanco", new Long(idBanco));

    Collection colBanco = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colBanco.iterator();
    Banco localBanc = (Banco)iterator.next();

    query = null;

    pm = null;

    parameters = null;

    return localBanc;
  }

  public Collection findBancoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent bancoExtent = pm.getExtent(
      Banco.class, true);
    Query query = pm.newQuery(bancoExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());

    query = null;

    pm = null;

    return collection;
  }

  public Collection findByCodBanco(String codBanco)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codBanco == pCodBanco";

    Query query = pm.newQuery(Banco.class, filter);

    query.declareParameters("java.lang.String pCodBanco");
    HashMap parameters = new HashMap();

    parameters.put("pCodBanco", new String(codBanco));

    query.setOrdering("nombre ascending");

    Collection colBanco = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colBanco);

    query = null;

    pm = null;

    return colBanco;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Banco.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colBanco = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colBanco);

    query = null;

    pm = null;

    return colBanco;
  }
}