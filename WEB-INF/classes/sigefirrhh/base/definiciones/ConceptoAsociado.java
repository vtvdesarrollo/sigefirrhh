package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ConceptoAsociado
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_FM;
  private long idConceptoAsociado;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private ConceptoTipoPersonal conceptoAsociar;
  private String base;
  private double factor;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "base", "conceptoAsociar", "conceptoTipoPersonal", "factor", "idConceptoAsociado", "idSitp", "tiempoSitp" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Double.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date") }; private static final byte[] jdoFieldFlags = { 21, 26, 26, 21, 24, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoAsociado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoAsociado());

    LISTA_FM = 
      new LinkedHashMap();

    LISTA_FM.put("N", "NINGUNA");
    LISTA_FM.put("H", "HORAS");
    LISTA_FM.put("D", "DIAS");
    LISTA_FM.put("S", "SEMANAL");
    LISTA_FM.put("M", "MENSUAL");
  }

  public ConceptoAsociado()
  {
    jdoSetbase(this, "M");

    jdoSetfactor(this, 1.0D);
  }

  public String toString()
  {
    return jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + "-" + 
      jdoGetconceptoAsociar(this).getConcepto().getDescripcion() + " - " + jdoGetbase(this);
  }

  public ConceptoTipoPersonal getConceptoAsociar()
  {
    return jdoGetconceptoAsociar(this);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public double getFactor()
  {
    return jdoGetfactor(this);
  }

  public long getIdConceptoAsociado()
  {
    return jdoGetidConceptoAsociado(this);
  }
  public void setConceptoAsociar(ConceptoTipoPersonal personal) {
    jdoSetconceptoAsociar(this, personal);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setFactor(double d)
  {
    jdoSetfactor(this, d);
  }

  public void setIdConceptoAsociado(long l)
  {
    jdoSetidConceptoAsociado(this, l);
  }

  public String getBase()
  {
    return jdoGetbase(this);
  }

  public void setBase(String string)
  {
    jdoSetbase(this, string);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoAsociado localConceptoAsociado = new ConceptoAsociado();
    localConceptoAsociado.jdoFlags = 1;
    localConceptoAsociado.jdoStateManager = paramStateManager;
    return localConceptoAsociado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoAsociado localConceptoAsociado = new ConceptoAsociado();
    localConceptoAsociado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoAsociado.jdoFlags = 1;
    localConceptoAsociado.jdoStateManager = paramStateManager;
    return localConceptoAsociado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.base);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoAsociar);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.factor);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoAsociado);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.base = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoAsociar = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.factor = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoAsociado = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoAsociado paramConceptoAsociado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoAsociado == null)
        throw new IllegalArgumentException("arg1");
      this.base = paramConceptoAsociado.base;
      return;
    case 1:
      if (paramConceptoAsociado == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoAsociar = paramConceptoAsociado.conceptoAsociar;
      return;
    case 2:
      if (paramConceptoAsociado == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoAsociado.conceptoTipoPersonal;
      return;
    case 3:
      if (paramConceptoAsociado == null)
        throw new IllegalArgumentException("arg1");
      this.factor = paramConceptoAsociado.factor;
      return;
    case 4:
      if (paramConceptoAsociado == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoAsociado = paramConceptoAsociado.idConceptoAsociado;
      return;
    case 5:
      if (paramConceptoAsociado == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramConceptoAsociado.idSitp;
      return;
    case 6:
      if (paramConceptoAsociado == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramConceptoAsociado.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoAsociado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoAsociado localConceptoAsociado = (ConceptoAsociado)paramObject;
    if (localConceptoAsociado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoAsociado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoAsociadoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoAsociadoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoAsociadoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoAsociadoPK localConceptoAsociadoPK = (ConceptoAsociadoPK)paramObject;
    localConceptoAsociadoPK.idConceptoAsociado = this.idConceptoAsociado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoAsociadoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoAsociadoPK localConceptoAsociadoPK = (ConceptoAsociadoPK)paramObject;
    this.idConceptoAsociado = localConceptoAsociadoPK.idConceptoAsociado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoAsociadoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoAsociadoPK localConceptoAsociadoPK = (ConceptoAsociadoPK)paramObject;
    localConceptoAsociadoPK.idConceptoAsociado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoAsociadoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoAsociadoPK localConceptoAsociadoPK = (ConceptoAsociadoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localConceptoAsociadoPK.idConceptoAsociado);
  }

  private static final String jdoGetbase(ConceptoAsociado paramConceptoAsociado)
  {
    if (paramConceptoAsociado.jdoFlags <= 0)
      return paramConceptoAsociado.base;
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoAsociado.base;
    if (localStateManager.isLoaded(paramConceptoAsociado, jdoInheritedFieldCount + 0))
      return paramConceptoAsociado.base;
    return localStateManager.getStringField(paramConceptoAsociado, jdoInheritedFieldCount + 0, paramConceptoAsociado.base);
  }

  private static final void jdoSetbase(ConceptoAsociado paramConceptoAsociado, String paramString)
  {
    if (paramConceptoAsociado.jdoFlags == 0)
    {
      paramConceptoAsociado.base = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoAsociado.base = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoAsociado, jdoInheritedFieldCount + 0, paramConceptoAsociado.base, paramString);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoAsociar(ConceptoAsociado paramConceptoAsociado)
  {
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoAsociado.conceptoAsociar;
    if (localStateManager.isLoaded(paramConceptoAsociado, jdoInheritedFieldCount + 1))
      return paramConceptoAsociado.conceptoAsociar;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoAsociado, jdoInheritedFieldCount + 1, paramConceptoAsociado.conceptoAsociar);
  }

  private static final void jdoSetconceptoAsociar(ConceptoAsociado paramConceptoAsociado, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoAsociado.conceptoAsociar = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoAsociado, jdoInheritedFieldCount + 1, paramConceptoAsociado.conceptoAsociar, paramConceptoTipoPersonal);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoAsociado paramConceptoAsociado)
  {
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoAsociado.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoAsociado, jdoInheritedFieldCount + 2))
      return paramConceptoAsociado.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoAsociado, jdoInheritedFieldCount + 2, paramConceptoAsociado.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoAsociado paramConceptoAsociado, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoAsociado.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoAsociado, jdoInheritedFieldCount + 2, paramConceptoAsociado.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final double jdoGetfactor(ConceptoAsociado paramConceptoAsociado)
  {
    if (paramConceptoAsociado.jdoFlags <= 0)
      return paramConceptoAsociado.factor;
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoAsociado.factor;
    if (localStateManager.isLoaded(paramConceptoAsociado, jdoInheritedFieldCount + 3))
      return paramConceptoAsociado.factor;
    return localStateManager.getDoubleField(paramConceptoAsociado, jdoInheritedFieldCount + 3, paramConceptoAsociado.factor);
  }

  private static final void jdoSetfactor(ConceptoAsociado paramConceptoAsociado, double paramDouble)
  {
    if (paramConceptoAsociado.jdoFlags == 0)
    {
      paramConceptoAsociado.factor = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoAsociado.factor = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoAsociado, jdoInheritedFieldCount + 3, paramConceptoAsociado.factor, paramDouble);
  }

  private static final long jdoGetidConceptoAsociado(ConceptoAsociado paramConceptoAsociado)
  {
    return paramConceptoAsociado.idConceptoAsociado;
  }

  private static final void jdoSetidConceptoAsociado(ConceptoAsociado paramConceptoAsociado, long paramLong)
  {
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoAsociado.idConceptoAsociado = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoAsociado, jdoInheritedFieldCount + 4, paramConceptoAsociado.idConceptoAsociado, paramLong);
  }

  private static final int jdoGetidSitp(ConceptoAsociado paramConceptoAsociado)
  {
    if (paramConceptoAsociado.jdoFlags <= 0)
      return paramConceptoAsociado.idSitp;
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoAsociado.idSitp;
    if (localStateManager.isLoaded(paramConceptoAsociado, jdoInheritedFieldCount + 5))
      return paramConceptoAsociado.idSitp;
    return localStateManager.getIntField(paramConceptoAsociado, jdoInheritedFieldCount + 5, paramConceptoAsociado.idSitp);
  }

  private static final void jdoSetidSitp(ConceptoAsociado paramConceptoAsociado, int paramInt)
  {
    if (paramConceptoAsociado.jdoFlags == 0)
    {
      paramConceptoAsociado.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoAsociado.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoAsociado, jdoInheritedFieldCount + 5, paramConceptoAsociado.idSitp, paramInt);
  }

  private static final Date jdoGettiempoSitp(ConceptoAsociado paramConceptoAsociado)
  {
    if (paramConceptoAsociado.jdoFlags <= 0)
      return paramConceptoAsociado.tiempoSitp;
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoAsociado.tiempoSitp;
    if (localStateManager.isLoaded(paramConceptoAsociado, jdoInheritedFieldCount + 6))
      return paramConceptoAsociado.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramConceptoAsociado, jdoInheritedFieldCount + 6, paramConceptoAsociado.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ConceptoAsociado paramConceptoAsociado, Date paramDate)
  {
    if (paramConceptoAsociado.jdoFlags == 0)
    {
      paramConceptoAsociado.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoAsociado.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoAsociado.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoAsociado, jdoInheritedFieldCount + 6, paramConceptoAsociado.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}