package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class DetalleDisqueteFormbackup extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(DetalleDisqueteForm.class.getName());
  private DetalleDisquete detalleDisquete;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private boolean showDetalleDisqueteByDisquete;
  private String findSelectDisquete;
  private Collection findColDisquete;
  private Collection colDisquete;
  private Collection colConcepto;
  private String selectDisquete;
  private String selectConcepto;
  private Object stateResultDetalleDisqueteByDisquete = null;

  public String getFindSelectDisquete()
  {
    return this.findSelectDisquete;
  }
  public void setFindSelectDisquete(String valDisquete) {
    this.findSelectDisquete = valDisquete;
  }

  public Collection getFindColDisquete() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColDisquete.iterator();
    Disquete disquete = null;
    while (iterator.hasNext()) {
      disquete = (Disquete)iterator.next();
      col.add(new SelectItem(
        String.valueOf(disquete.getIdDisquete()), 
        disquete.toString()));
    }
    return col;
  }

  public String getSelectDisquete()
  {
    return this.selectDisquete;
  }
  public void setSelectDisquete(String valDisquete) {
    Iterator iterator = this.colDisquete.iterator();
    Disquete disquete = null;
    this.detalleDisquete.setDisquete(null);
    while (iterator.hasNext()) {
      disquete = (Disquete)iterator.next();
      if (String.valueOf(disquete.getIdDisquete()).equals(
        valDisquete)) {
        this.detalleDisquete.setDisquete(
          disquete);
      }
    }
    this.selectDisquete = valDisquete;
  }
  public String getSelectConcepto() {
    return this.selectConcepto;
  }
  public void setSelectConcepto(String valConcepto) {
    Iterator iterator = this.colConcepto.iterator();
    Concepto concepto = null;
    this.detalleDisquete.setConcepto(null);
    while (iterator.hasNext()) {
      concepto = (Concepto)iterator.next();
      if (String.valueOf(concepto.getIdConcepto()).equals(
        valConcepto)) {
        this.detalleDisquete.setConcepto(
          concepto);
      }
    }
    this.selectConcepto = valConcepto;
  }
  public Collection getResult() {
    return this.result;
  }

  public DetalleDisquete getDetalleDisquete() {
    if (this.detalleDisquete == null) {
      this.detalleDisquete = new DetalleDisquete();
    }
    return this.detalleDisquete;
  }

  public DetalleDisqueteFormbackup()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColDisquete()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colDisquete.iterator();
    Disquete disquete = null;
    while (iterator.hasNext()) {
      disquete = (Disquete)iterator.next();
      col.add(new SelectItem(
        String.valueOf(disquete.getIdDisquete()), 
        disquete.toString()));
    }
    return col;
  }

  public Collection getListTipoRegistro() {
    Collection col = new ArrayList();

    Iterator iterEntry = DetalleDisquete.LISTA_TIPO_REGISTRO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTipoCampo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = DetalleDisquete.LISTA_TIPO_CAMPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCampoBaseDatos()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = DetalleDisquete.LISTA_CAMPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCampoTotales() {
    Collection col = new ArrayList();

    Iterator iterEntry = DetalleDisquete.LISTA_TOTALES.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCampoEntrada()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = DetalleDisquete.LISTA_ENTRADA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListSeparadorDecimal() {
    Collection col = new ArrayList();

    Iterator iterEntry = DetalleDisquete.LISTA_SEPARADOR.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAlineacionCampo() {
    Collection col = new ArrayList();

    Iterator iterEntry = DetalleDisquete.LISTA_ALINEACION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListRellenarCero() {
    Collection col = new ArrayList();

    Iterator iterEntry = DetalleDisquete.LISTA_RELLENO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColConcepto()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConcepto.iterator();
    Concepto concepto = null;
    while (iterator.hasNext()) {
      concepto = (Concepto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concepto.getIdConcepto()), 
        concepto.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColDisquete = 
        this.definicionesFacade.findDisqueteByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colDisquete = 
        this.definicionesFacade.findDisqueteByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colConcepto = 
        this.definicionesFacade.findConceptoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findDetalleDisqueteByDisquete()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findDetalleDisqueteByDisquete(Long.valueOf(this.findSelectDisquete).longValue());
      this.showDetalleDisqueteByDisquete = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDetalleDisqueteByDisquete)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectDisquete = null;

    return null;
  }

  public boolean isShowDetalleDisqueteByDisquete() {
    return this.showDetalleDisqueteByDisquete;
  }

  public String selectDetalleDisquete()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectDisquete = null;
    this.selectConcepto = null;

    long idDetalleDisquete = 
      Long.parseLong((String)requestParameterMap.get("idDetalleDisquete"));
    try
    {
      this.detalleDisquete = 
        this.definicionesFacade.findDetalleDisqueteById(
        idDetalleDisquete);
      if (this.detalleDisquete.getDisquete() != null) {
        this.selectDisquete = 
          String.valueOf(this.detalleDisquete.getDisquete().getIdDisquete());
      }
      if (this.detalleDisquete.getConcepto() != null) {
        this.selectConcepto = 
          String.valueOf(this.detalleDisquete.getConcepto().getIdConcepto());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.detalleDisquete = null;
    this.showDetalleDisqueteByDisquete = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.detalleDisquete.setNumeroCampo(this.definicionesFacade.findLastNumeroCampoByDisquete(Long.valueOf(this.selectDisquete).longValue(), this.detalleDisquete.getTipoRegistro()) + 1);
        this.definicionesFacade.addDetalleDisquete(
          this.detalleDisquete);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.detalleDisquete);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateDetalleDisquete(
          this.detalleDisquete);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.detalleDisquete);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteDetalleDisquete(
        this.detalleDisquete);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.detalleDisquete);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.detalleDisquete = new DetalleDisquete();

    this.selectDisquete = null;

    this.selectConcepto = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.detalleDisquete.setIdDetalleDisquete(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.DetalleDisquete"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.detalleDisquete = new DetalleDisquete();
    return "cancel";
  }

  public boolean isShowCampoBaseDatosAux()
  {
    try
    {
      return this.detalleDisquete.getTipoCampo().equals("V"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowCampoTotalesAux()
  {
    try {
      return ((!this.detalleDisquete.getTipoRegistro().equals("2")) || (this.detalleDisquete.getTipoRegistro().equals("3"))) && (this.detalleDisquete.getTipoCampo().equals("V")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowCampoUsuarioAux()
  {
    try {
      return this.detalleDisquete.getTipoCampo().equals("F"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowCampoEntradaAux()
  {
    try {
      return (this.detalleDisquete.getTipoCampo().equals("E")) || (this.detalleDisquete.getTipoCampo().equals("D")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowSeparadorDecimalAux()
  {
    try {
      return this.detalleDisquete.getTipoCampo().equals("V"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowRellenarCeroAux()
  {
    try
    {
      return this.detalleDisquete.getTipoCampo().equals("V"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowMultiplicadorAux()
  {
    try {
      return this.detalleDisquete.getTipoCampo().equals("V"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}