package sigefirrhh.base.definiciones;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ConceptoAsociadoNoGenBeanBusiness
  implements Serializable
{
  public ConceptoAsociado findByConceptoTipoPersonalAndConceptoAsociado(long idConceptoTipoPersonal, long idConceptoAsociado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal && conceptoAsociar.idConceptoTipoPersonal == pIdConceptoAsociado";

    Query query = pm.newQuery(ConceptoAsociado.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal, long pIdConceptoAsociado");
    HashMap parameters = new HashMap();

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));
    parameters.put("pIdConceptoAsociado", new Long(idConceptoAsociado));

    Collection colConceptoAsociado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoAsociado.iterator();
    return (ConceptoAsociado)iterator.next();
  }
}