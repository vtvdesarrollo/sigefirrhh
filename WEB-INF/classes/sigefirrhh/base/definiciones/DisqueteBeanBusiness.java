package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class DisqueteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDisquete(Disquete disquete)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Disquete disqueteNew = 
      (Disquete)BeanUtils.cloneBean(
      disquete);

    BancoBeanBusiness bancoBeanBusiness = new BancoBeanBusiness();

    if (disqueteNew.getBanco() != null) {
      disqueteNew.setBanco(
        bancoBeanBusiness.findBancoById(
        disqueteNew.getBanco().getIdBanco()));
    }

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (disqueteNew.getConcepto() != null) {
      disqueteNew.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        disqueteNew.getConcepto().getIdConcepto()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (disqueteNew.getOrganismo() != null) {
      disqueteNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        disqueteNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(disqueteNew);
  }

  public void updateDisquete(Disquete disquete) throws Exception
  {
    Disquete disqueteModify = 
      findDisqueteById(disquete.getIdDisquete());

    BancoBeanBusiness bancoBeanBusiness = new BancoBeanBusiness();

    if (disquete.getBanco() != null) {
      disquete.setBanco(
        bancoBeanBusiness.findBancoById(
        disquete.getBanco().getIdBanco()));
    }

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (disquete.getConcepto() != null) {
      disquete.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        disquete.getConcepto().getIdConcepto()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (disquete.getOrganismo() != null) {
      disquete.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        disquete.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(disqueteModify, disquete);
  }

  public void deleteDisquete(Disquete disquete) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Disquete disqueteDelete = 
      findDisqueteById(disquete.getIdDisquete());
    pm.deletePersistent(disqueteDelete);
  }

  public Disquete findDisqueteById(long idDisquete) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDisquete == pIdDisquete";
    Query query = pm.newQuery(Disquete.class, filter);

    query.declareParameters("long pIdDisquete");

    parameters.put("pIdDisquete", new Long(idDisquete));

    Collection colDisquete = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDisquete.iterator();
    return (Disquete)iterator.next();
  }

  public Collection findDisqueteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent disqueteExtent = pm.getExtent(
      Disquete.class, true);
    Query query = pm.newQuery(disqueteExtent);
    query.setOrdering("codDisquete ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodDisquete(String codDisquete, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codDisquete == pCodDisquete &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Disquete.class, filter);

    query.declareParameters("java.lang.String pCodDisquete, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodDisquete", new String(codDisquete));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDisquete ascending");

    Collection colDisquete = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDisquete);

    return colDisquete;
  }

  public Collection findByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Disquete.class, filter);

    query.declareParameters("java.lang.String pDescripcion, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDisquete ascending");

    Collection colDisquete = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDisquete);

    return colDisquete;
  }

  public Collection findByTipoDisquete(String tipoDisquete, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoDisquete == pTipoDisquete &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Disquete.class, filter);

    query.declareParameters("java.lang.String pTipoDisquete, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pTipoDisquete", new String(tipoDisquete));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDisquete ascending");

    Collection colDisquete = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDisquete);

    return colDisquete;
  }

  public Collection findByIngresos(String ingresos, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "ingresos == pIngresos &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Disquete.class, filter);

    query.declareParameters("java.lang.String pIngresos, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIngresos", new String(ingresos));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDisquete ascending");

    Collection colDisquete = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDisquete);

    return colDisquete;
  }

  public Collection findByEgresos(String egresos, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "egresos == pEgresos &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Disquete.class, filter);

    query.declareParameters("java.lang.String pEgresos, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pEgresos", new String(egresos));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDisquete ascending");

    Collection colDisquete = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDisquete);

    return colDisquete;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Disquete.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDisquete ascending");

    Collection colDisquete = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDisquete);

    return colDisquete;
  }
}