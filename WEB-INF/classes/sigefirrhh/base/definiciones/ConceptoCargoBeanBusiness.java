package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;

public class ConceptoCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoCargo(ConceptoCargo conceptoCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoCargo conceptoCargoNew = 
      (ConceptoCargo)BeanUtils.cloneBean(
      conceptoCargo);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoCargoNew.getConceptoTipoPersonal() != null) {
      conceptoCargoNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoCargoNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (conceptoCargoNew.getCargo() != null) {
      conceptoCargoNew.setCargo(
        cargoBeanBusiness.findCargoById(
        conceptoCargoNew.getCargo().getIdCargo()));
    }
    pm.makePersistent(conceptoCargoNew);
  }

  public void updateConceptoCargo(ConceptoCargo conceptoCargo) throws Exception
  {
    ConceptoCargo conceptoCargoModify = 
      findConceptoCargoById(conceptoCargo.getIdConceptoCargo());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoCargo.getConceptoTipoPersonal() != null) {
      conceptoCargo.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoCargo.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (conceptoCargo.getCargo() != null) {
      conceptoCargo.setCargo(
        cargoBeanBusiness.findCargoById(
        conceptoCargo.getCargo().getIdCargo()));
    }

    BeanUtils.copyProperties(conceptoCargoModify, conceptoCargo);
  }

  public void deleteConceptoCargo(ConceptoCargo conceptoCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoCargo conceptoCargoDelete = 
      findConceptoCargoById(conceptoCargo.getIdConceptoCargo());
    pm.deletePersistent(conceptoCargoDelete);
  }

  public ConceptoCargo findConceptoCargoById(long idConceptoCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoCargo == pIdConceptoCargo";
    Query query = pm.newQuery(ConceptoCargo.class, filter);

    query.declareParameters("long pIdConceptoCargo");

    parameters.put("pIdConceptoCargo", new Long(idConceptoCargo));

    Collection colConceptoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoCargo.iterator();
    return (ConceptoCargo)iterator.next();
  }

  public Collection findConceptoCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoCargoExtent = pm.getExtent(
      ConceptoCargo.class, true);
    Query query = pm.newQuery(conceptoCargoExtent);
    query.setOrdering("conceptoTipoPersonal.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal";

    Query query = pm.newQuery(ConceptoCargo.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    query.setOrdering("conceptoTipoPersonal.codConcepto ascending");

    Collection colConceptoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoCargo);

    return colConceptoCargo;
  }

  public Collection findByCargo(long idCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cargo.idCargo == pIdCargo";

    Query query = pm.newQuery(ConceptoCargo.class, filter);

    query.declareParameters("long pIdCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdCargo", new Long(idCargo));

    query.setOrdering("conceptoTipoPersonal.codConcepto ascending");

    Collection colConceptoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoCargo);

    return colConceptoCargo;
  }
}