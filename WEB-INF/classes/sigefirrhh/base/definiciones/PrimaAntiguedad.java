package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PrimaAntiguedad
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_CALCULO;
  protected static final Map LISTA_OPERACION;
  private long idPrimaAntiguedad;
  private TipoPersonal tipoPersonal;
  private int aniosServicio;
  private String tipo;
  private String operacion;
  private double monto;
  private double porcentaje;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aniosServicio", "idPrimaAntiguedad", "idSitp", "monto", "operacion", "porcentaje", "tiempoSitp", "tipo", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.PrimaAntiguedad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PrimaAntiguedad());

    LISTA_CALCULO = 
      new LinkedHashMap();
    LISTA_OPERACION = 
      new LinkedHashMap();
    LISTA_CALCULO.put("F", "FIJO");
    LISTA_CALCULO.put("P", "PORCENTAJE");
    LISTA_OPERACION.put("I", "INCREMENTA");
    LISTA_OPERACION.put("M", "MULTIPLICA");
    LISTA_OPERACION.put("F", "FIJO");
  }

  public PrimaAntiguedad()
  {
    jdoSetaniosServicio(this, 1);

    jdoSettipo(this, "F");

    jdoSetoperacion(this, "M");

    jdoSetmonto(this, 0.0D);

    jdoSetporcentaje(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmonto(this));

    return jdoGettipoPersonal(this).getNombre() + " - " + 
      jdoGetaniosServicio(this) + " - " + 
      a;
  }

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }

  public long getIdPrimaAntiguedad()
  {
    return jdoGetidPrimaAntiguedad(this);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public String getOperacion()
  {
    return jdoGetoperacion(this);
  }

  public double getPorcentaje()
  {
    return jdoGetporcentaje(this);
  }

  public String getTipo()
  {
    return jdoGettipo(this);
  }

  public void setAniosServicio(int i)
  {
    jdoSetaniosServicio(this, i);
  }

  public void setIdPrimaAntiguedad(long l)
  {
    jdoSetidPrimaAntiguedad(this, l);
  }

  public void setMonto(double d)
  {
    jdoSetmonto(this, d);
  }

  public void setOperacion(String string)
  {
    jdoSetoperacion(this, string);
  }

  public void setPorcentaje(double d)
  {
    jdoSetporcentaje(this, d);
  }

  public void setTipo(String string)
  {
    jdoSettipo(this, string);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PrimaAntiguedad localPrimaAntiguedad = new PrimaAntiguedad();
    localPrimaAntiguedad.jdoFlags = 1;
    localPrimaAntiguedad.jdoStateManager = paramStateManager;
    return localPrimaAntiguedad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PrimaAntiguedad localPrimaAntiguedad = new PrimaAntiguedad();
    localPrimaAntiguedad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPrimaAntiguedad.jdoFlags = 1;
    localPrimaAntiguedad.jdoStateManager = paramStateManager;
    return localPrimaAntiguedad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPrimaAntiguedad);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.operacion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPrimaAntiguedad = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.operacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PrimaAntiguedad paramPrimaAntiguedad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPrimaAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramPrimaAntiguedad.aniosServicio;
      return;
    case 1:
      if (paramPrimaAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.idPrimaAntiguedad = paramPrimaAntiguedad.idPrimaAntiguedad;
      return;
    case 2:
      if (paramPrimaAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramPrimaAntiguedad.idSitp;
      return;
    case 3:
      if (paramPrimaAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramPrimaAntiguedad.monto;
      return;
    case 4:
      if (paramPrimaAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.operacion = paramPrimaAntiguedad.operacion;
      return;
    case 5:
      if (paramPrimaAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramPrimaAntiguedad.porcentaje;
      return;
    case 6:
      if (paramPrimaAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramPrimaAntiguedad.tiempoSitp;
      return;
    case 7:
      if (paramPrimaAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramPrimaAntiguedad.tipo;
      return;
    case 8:
      if (paramPrimaAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramPrimaAntiguedad.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PrimaAntiguedad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PrimaAntiguedad localPrimaAntiguedad = (PrimaAntiguedad)paramObject;
    if (localPrimaAntiguedad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPrimaAntiguedad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PrimaAntiguedadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PrimaAntiguedadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrimaAntiguedadPK))
      throw new IllegalArgumentException("arg1");
    PrimaAntiguedadPK localPrimaAntiguedadPK = (PrimaAntiguedadPK)paramObject;
    localPrimaAntiguedadPK.idPrimaAntiguedad = this.idPrimaAntiguedad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrimaAntiguedadPK))
      throw new IllegalArgumentException("arg1");
    PrimaAntiguedadPK localPrimaAntiguedadPK = (PrimaAntiguedadPK)paramObject;
    this.idPrimaAntiguedad = localPrimaAntiguedadPK.idPrimaAntiguedad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrimaAntiguedadPK))
      throw new IllegalArgumentException("arg2");
    PrimaAntiguedadPK localPrimaAntiguedadPK = (PrimaAntiguedadPK)paramObject;
    localPrimaAntiguedadPK.idPrimaAntiguedad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrimaAntiguedadPK))
      throw new IllegalArgumentException("arg2");
    PrimaAntiguedadPK localPrimaAntiguedadPK = (PrimaAntiguedadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localPrimaAntiguedadPK.idPrimaAntiguedad);
  }

  private static final int jdoGetaniosServicio(PrimaAntiguedad paramPrimaAntiguedad)
  {
    if (paramPrimaAntiguedad.jdoFlags <= 0)
      return paramPrimaAntiguedad.aniosServicio;
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaAntiguedad.aniosServicio;
    if (localStateManager.isLoaded(paramPrimaAntiguedad, jdoInheritedFieldCount + 0))
      return paramPrimaAntiguedad.aniosServicio;
    return localStateManager.getIntField(paramPrimaAntiguedad, jdoInheritedFieldCount + 0, paramPrimaAntiguedad.aniosServicio);
  }

  private static final void jdoSetaniosServicio(PrimaAntiguedad paramPrimaAntiguedad, int paramInt)
  {
    if (paramPrimaAntiguedad.jdoFlags == 0)
    {
      paramPrimaAntiguedad.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaAntiguedad.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrimaAntiguedad, jdoInheritedFieldCount + 0, paramPrimaAntiguedad.aniosServicio, paramInt);
  }

  private static final long jdoGetidPrimaAntiguedad(PrimaAntiguedad paramPrimaAntiguedad)
  {
    return paramPrimaAntiguedad.idPrimaAntiguedad;
  }

  private static final void jdoSetidPrimaAntiguedad(PrimaAntiguedad paramPrimaAntiguedad, long paramLong)
  {
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaAntiguedad.idPrimaAntiguedad = paramLong;
      return;
    }
    localStateManager.setLongField(paramPrimaAntiguedad, jdoInheritedFieldCount + 1, paramPrimaAntiguedad.idPrimaAntiguedad, paramLong);
  }

  private static final int jdoGetidSitp(PrimaAntiguedad paramPrimaAntiguedad)
  {
    if (paramPrimaAntiguedad.jdoFlags <= 0)
      return paramPrimaAntiguedad.idSitp;
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaAntiguedad.idSitp;
    if (localStateManager.isLoaded(paramPrimaAntiguedad, jdoInheritedFieldCount + 2))
      return paramPrimaAntiguedad.idSitp;
    return localStateManager.getIntField(paramPrimaAntiguedad, jdoInheritedFieldCount + 2, paramPrimaAntiguedad.idSitp);
  }

  private static final void jdoSetidSitp(PrimaAntiguedad paramPrimaAntiguedad, int paramInt)
  {
    if (paramPrimaAntiguedad.jdoFlags == 0)
    {
      paramPrimaAntiguedad.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaAntiguedad.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrimaAntiguedad, jdoInheritedFieldCount + 2, paramPrimaAntiguedad.idSitp, paramInt);
  }

  private static final double jdoGetmonto(PrimaAntiguedad paramPrimaAntiguedad)
  {
    if (paramPrimaAntiguedad.jdoFlags <= 0)
      return paramPrimaAntiguedad.monto;
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaAntiguedad.monto;
    if (localStateManager.isLoaded(paramPrimaAntiguedad, jdoInheritedFieldCount + 3))
      return paramPrimaAntiguedad.monto;
    return localStateManager.getDoubleField(paramPrimaAntiguedad, jdoInheritedFieldCount + 3, paramPrimaAntiguedad.monto);
  }

  private static final void jdoSetmonto(PrimaAntiguedad paramPrimaAntiguedad, double paramDouble)
  {
    if (paramPrimaAntiguedad.jdoFlags == 0)
    {
      paramPrimaAntiguedad.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaAntiguedad.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrimaAntiguedad, jdoInheritedFieldCount + 3, paramPrimaAntiguedad.monto, paramDouble);
  }

  private static final String jdoGetoperacion(PrimaAntiguedad paramPrimaAntiguedad)
  {
    if (paramPrimaAntiguedad.jdoFlags <= 0)
      return paramPrimaAntiguedad.operacion;
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaAntiguedad.operacion;
    if (localStateManager.isLoaded(paramPrimaAntiguedad, jdoInheritedFieldCount + 4))
      return paramPrimaAntiguedad.operacion;
    return localStateManager.getStringField(paramPrimaAntiguedad, jdoInheritedFieldCount + 4, paramPrimaAntiguedad.operacion);
  }

  private static final void jdoSetoperacion(PrimaAntiguedad paramPrimaAntiguedad, String paramString)
  {
    if (paramPrimaAntiguedad.jdoFlags == 0)
    {
      paramPrimaAntiguedad.operacion = paramString;
      return;
    }
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaAntiguedad.operacion = paramString;
      return;
    }
    localStateManager.setStringField(paramPrimaAntiguedad, jdoInheritedFieldCount + 4, paramPrimaAntiguedad.operacion, paramString);
  }

  private static final double jdoGetporcentaje(PrimaAntiguedad paramPrimaAntiguedad)
  {
    if (paramPrimaAntiguedad.jdoFlags <= 0)
      return paramPrimaAntiguedad.porcentaje;
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaAntiguedad.porcentaje;
    if (localStateManager.isLoaded(paramPrimaAntiguedad, jdoInheritedFieldCount + 5))
      return paramPrimaAntiguedad.porcentaje;
    return localStateManager.getDoubleField(paramPrimaAntiguedad, jdoInheritedFieldCount + 5, paramPrimaAntiguedad.porcentaje);
  }

  private static final void jdoSetporcentaje(PrimaAntiguedad paramPrimaAntiguedad, double paramDouble)
  {
    if (paramPrimaAntiguedad.jdoFlags == 0)
    {
      paramPrimaAntiguedad.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaAntiguedad.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrimaAntiguedad, jdoInheritedFieldCount + 5, paramPrimaAntiguedad.porcentaje, paramDouble);
  }

  private static final Date jdoGettiempoSitp(PrimaAntiguedad paramPrimaAntiguedad)
  {
    if (paramPrimaAntiguedad.jdoFlags <= 0)
      return paramPrimaAntiguedad.tiempoSitp;
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaAntiguedad.tiempoSitp;
    if (localStateManager.isLoaded(paramPrimaAntiguedad, jdoInheritedFieldCount + 6))
      return paramPrimaAntiguedad.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramPrimaAntiguedad, jdoInheritedFieldCount + 6, paramPrimaAntiguedad.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(PrimaAntiguedad paramPrimaAntiguedad, Date paramDate)
  {
    if (paramPrimaAntiguedad.jdoFlags == 0)
    {
      paramPrimaAntiguedad.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaAntiguedad.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPrimaAntiguedad, jdoInheritedFieldCount + 6, paramPrimaAntiguedad.tiempoSitp, paramDate);
  }

  private static final String jdoGettipo(PrimaAntiguedad paramPrimaAntiguedad)
  {
    if (paramPrimaAntiguedad.jdoFlags <= 0)
      return paramPrimaAntiguedad.tipo;
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaAntiguedad.tipo;
    if (localStateManager.isLoaded(paramPrimaAntiguedad, jdoInheritedFieldCount + 7))
      return paramPrimaAntiguedad.tipo;
    return localStateManager.getStringField(paramPrimaAntiguedad, jdoInheritedFieldCount + 7, paramPrimaAntiguedad.tipo);
  }

  private static final void jdoSettipo(PrimaAntiguedad paramPrimaAntiguedad, String paramString)
  {
    if (paramPrimaAntiguedad.jdoFlags == 0)
    {
      paramPrimaAntiguedad.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaAntiguedad.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramPrimaAntiguedad, jdoInheritedFieldCount + 7, paramPrimaAntiguedad.tipo, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(PrimaAntiguedad paramPrimaAntiguedad)
  {
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramPrimaAntiguedad.tipoPersonal;
    if (localStateManager.isLoaded(paramPrimaAntiguedad, jdoInheritedFieldCount + 8))
      return paramPrimaAntiguedad.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramPrimaAntiguedad, jdoInheritedFieldCount + 8, paramPrimaAntiguedad.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(PrimaAntiguedad paramPrimaAntiguedad, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramPrimaAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimaAntiguedad.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramPrimaAntiguedad, jdoInheritedFieldCount + 8, paramPrimaAntiguedad.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}