package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class BeneficiarioGlobalPK
  implements Serializable
{
  public long idBeneficiarioGlobal;

  public BeneficiarioGlobalPK()
  {
  }

  public BeneficiarioGlobalPK(long idBeneficiarioGlobal)
  {
    this.idBeneficiarioGlobal = idBeneficiarioGlobal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((BeneficiarioGlobalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(BeneficiarioGlobalPK thatPK)
  {
    return 
      this.idBeneficiarioGlobal == thatPK.idBeneficiarioGlobal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idBeneficiarioGlobal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idBeneficiarioGlobal);
  }
}