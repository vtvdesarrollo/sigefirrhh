package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.base.estructura.GrupoOrganismoBeanBusiness;

public class ParametroVariosBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParametroVarios(ParametroVarios parametroVarios)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ParametroVarios parametroVariosNew = 
      (ParametroVarios)BeanUtils.cloneBean(
      parametroVarios);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroVariosNew.getTipoPersonal() != null) {
      parametroVariosNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroVariosNew.getTipoPersonal().getIdTipoPersonal()));
    }

    GrupoOrganismoBeanBusiness grupoOrganismoBeanBusiness = new GrupoOrganismoBeanBusiness();

    if (parametroVariosNew.getGrupoOrganismo() != null) {
      parametroVariosNew.setGrupoOrganismo(
        grupoOrganismoBeanBusiness.findGrupoOrganismoById(
        parametroVariosNew.getGrupoOrganismo().getIdGrupoOrganismo()));
    }
    pm.makePersistent(parametroVariosNew);
  }

  public void updateParametroVarios(ParametroVarios parametroVarios) throws Exception
  {
    ParametroVarios parametroVariosModify = 
      findParametroVariosById(parametroVarios.getIdParametroVarios());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroVarios.getTipoPersonal() != null) {
      parametroVarios.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroVarios.getTipoPersonal().getIdTipoPersonal()));
    }

    GrupoOrganismoBeanBusiness grupoOrganismoBeanBusiness = new GrupoOrganismoBeanBusiness();

    if (parametroVarios.getGrupoOrganismo() != null) {
      parametroVarios.setGrupoOrganismo(
        grupoOrganismoBeanBusiness.findGrupoOrganismoById(
        parametroVarios.getGrupoOrganismo().getIdGrupoOrganismo()));
    }

    BeanUtils.copyProperties(parametroVariosModify, parametroVarios);
  }

  public void deleteParametroVarios(ParametroVarios parametroVarios) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ParametroVarios parametroVariosDelete = 
      findParametroVariosById(parametroVarios.getIdParametroVarios());
    pm.deletePersistent(parametroVariosDelete);
  }

  public ParametroVarios findParametroVariosById(long idParametroVarios) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParametroVarios == pIdParametroVarios";
    Query query = pm.newQuery(ParametroVarios.class, filter);

    query.declareParameters("long pIdParametroVarios");

    parameters.put("pIdParametroVarios", new Long(idParametroVarios));

    Collection colParametroVarios = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParametroVarios.iterator();
    return (ParametroVarios)iterator.next();
  }

  public Collection findParametroVariosAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parametroVariosExtent = pm.getExtent(
      ParametroVarios.class, true);
    Query query = pm.newQuery(parametroVariosExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ParametroVarios.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colParametroVarios = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParametroVarios);

    return colParametroVarios;
  }
}