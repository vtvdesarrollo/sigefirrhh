package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Semana
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_MES;
  private long idSemana;
  private GrupoNomina grupoNomina;
  private int anio;
  private int semanaAnio;
  private String mes;
  private int semanaMes;
  private Date fechaInicio;
  private Date fechaFin;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "fechaFin", "fechaInicio", "grupoNomina", "idSemana", "mes", "semanaAnio", "semanaMes" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), Long.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 24, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.Semana"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Semana());

    LISTA_MES = 
      new LinkedHashMap();
    LISTA_MES.put("1", "ENERO");
    LISTA_MES.put("2", "FEBRERO");
    LISTA_MES.put("3", "MARZO");
    LISTA_MES.put("4", "ABRIL");
    LISTA_MES.put("5", "MAYO");
    LISTA_MES.put("6", "JUNIO");
    LISTA_MES.put("7", "JULIO");
    LISTA_MES.put("8", "AGOSTO");
    LISTA_MES.put("9", "SEPTIEMBRE");
    LISTA_MES.put("10", "OCTUBRE");
    LISTA_MES.put("11", "NOVIEMBRE");
    LISTA_MES.put("12", "DICIEMBRE");
  }

  public String toString()
  {
    return jdoGetgrupoNomina(this).getNombre() + " - " + 
      jdoGetsemanaAnio(this) + " - " + 
      LISTA_MES.get(jdoGetmes(this)) + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this)) + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaFin(this));
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public Date getFechaFin()
  {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio()
  {
    return jdoGetfechaInicio(this);
  }

  public long getIdSemana()
  {
    return jdoGetidSemana(this);
  }

  public String getMes()
  {
    return jdoGetmes(this);
  }

  public int getSemanaAnio()
  {
    return jdoGetsemanaAnio(this);
  }

  public int getSemanaMes()
  {
    return jdoGetsemanaMes(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setFechaFin(Date date)
  {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date)
  {
    jdoSetfechaInicio(this, date);
  }

  public void setIdSemana(long l)
  {
    jdoSetidSemana(this, l);
  }

  public void setMes(String string)
  {
    jdoSetmes(this, string);
  }

  public void setSemanaAnio(int i)
  {
    jdoSetsemanaAnio(this, i);
  }

  public void setSemanaMes(int i)
  {
    jdoSetsemanaMes(this, i);
  }

  public GrupoNomina getGrupoNomina()
  {
    return jdoGetgrupoNomina(this);
  }

  public void setGrupoNomina(GrupoNomina nomina)
  {
    jdoSetgrupoNomina(this, nomina);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Semana localSemana = new Semana();
    localSemana.jdoFlags = 1;
    localSemana.jdoStateManager = paramStateManager;
    return localSemana;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Semana localSemana = new Semana();
    localSemana.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSemana.jdoFlags = 1;
    localSemana.jdoStateManager = paramStateManager;
    return localSemana;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSemana);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mes);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanaAnio);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanaMes);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSemana = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanaAnio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanaMes = localStateManager.replacingIntField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Semana paramSemana, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSemana == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramSemana.anio;
      return;
    case 1:
      if (paramSemana == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramSemana.fechaFin;
      return;
    case 2:
      if (paramSemana == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramSemana.fechaInicio;
      return;
    case 3:
      if (paramSemana == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramSemana.grupoNomina;
      return;
    case 4:
      if (paramSemana == null)
        throw new IllegalArgumentException("arg1");
      this.idSemana = paramSemana.idSemana;
      return;
    case 5:
      if (paramSemana == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramSemana.mes;
      return;
    case 6:
      if (paramSemana == null)
        throw new IllegalArgumentException("arg1");
      this.semanaAnio = paramSemana.semanaAnio;
      return;
    case 7:
      if (paramSemana == null)
        throw new IllegalArgumentException("arg1");
      this.semanaMes = paramSemana.semanaMes;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Semana))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Semana localSemana = (Semana)paramObject;
    if (localSemana.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSemana, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SemanaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SemanaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SemanaPK))
      throw new IllegalArgumentException("arg1");
    SemanaPK localSemanaPK = (SemanaPK)paramObject;
    localSemanaPK.idSemana = this.idSemana;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SemanaPK))
      throw new IllegalArgumentException("arg1");
    SemanaPK localSemanaPK = (SemanaPK)paramObject;
    this.idSemana = localSemanaPK.idSemana;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SemanaPK))
      throw new IllegalArgumentException("arg2");
    SemanaPK localSemanaPK = (SemanaPK)paramObject;
    localSemanaPK.idSemana = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SemanaPK))
      throw new IllegalArgumentException("arg2");
    SemanaPK localSemanaPK = (SemanaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localSemanaPK.idSemana);
  }

  private static final int jdoGetanio(Semana paramSemana)
  {
    if (paramSemana.jdoFlags <= 0)
      return paramSemana.anio;
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
      return paramSemana.anio;
    if (localStateManager.isLoaded(paramSemana, jdoInheritedFieldCount + 0))
      return paramSemana.anio;
    return localStateManager.getIntField(paramSemana, jdoInheritedFieldCount + 0, paramSemana.anio);
  }

  private static final void jdoSetanio(Semana paramSemana, int paramInt)
  {
    if (paramSemana.jdoFlags == 0)
    {
      paramSemana.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramSemana.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramSemana, jdoInheritedFieldCount + 0, paramSemana.anio, paramInt);
  }

  private static final Date jdoGetfechaFin(Semana paramSemana)
  {
    if (paramSemana.jdoFlags <= 0)
      return paramSemana.fechaFin;
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
      return paramSemana.fechaFin;
    if (localStateManager.isLoaded(paramSemana, jdoInheritedFieldCount + 1))
      return paramSemana.fechaFin;
    return (Date)localStateManager.getObjectField(paramSemana, jdoInheritedFieldCount + 1, paramSemana.fechaFin);
  }

  private static final void jdoSetfechaFin(Semana paramSemana, Date paramDate)
  {
    if (paramSemana.jdoFlags == 0)
    {
      paramSemana.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramSemana.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSemana, jdoInheritedFieldCount + 1, paramSemana.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(Semana paramSemana)
  {
    if (paramSemana.jdoFlags <= 0)
      return paramSemana.fechaInicio;
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
      return paramSemana.fechaInicio;
    if (localStateManager.isLoaded(paramSemana, jdoInheritedFieldCount + 2))
      return paramSemana.fechaInicio;
    return (Date)localStateManager.getObjectField(paramSemana, jdoInheritedFieldCount + 2, paramSemana.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Semana paramSemana, Date paramDate)
  {
    if (paramSemana.jdoFlags == 0)
    {
      paramSemana.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramSemana.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSemana, jdoInheritedFieldCount + 2, paramSemana.fechaInicio, paramDate);
  }

  private static final GrupoNomina jdoGetgrupoNomina(Semana paramSemana)
  {
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
      return paramSemana.grupoNomina;
    if (localStateManager.isLoaded(paramSemana, jdoInheritedFieldCount + 3))
      return paramSemana.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramSemana, jdoInheritedFieldCount + 3, paramSemana.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(Semana paramSemana, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramSemana.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramSemana, jdoInheritedFieldCount + 3, paramSemana.grupoNomina, paramGrupoNomina);
  }

  private static final long jdoGetidSemana(Semana paramSemana)
  {
    return paramSemana.idSemana;
  }

  private static final void jdoSetidSemana(Semana paramSemana, long paramLong)
  {
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramSemana.idSemana = paramLong;
      return;
    }
    localStateManager.setLongField(paramSemana, jdoInheritedFieldCount + 4, paramSemana.idSemana, paramLong);
  }

  private static final String jdoGetmes(Semana paramSemana)
  {
    if (paramSemana.jdoFlags <= 0)
      return paramSemana.mes;
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
      return paramSemana.mes;
    if (localStateManager.isLoaded(paramSemana, jdoInheritedFieldCount + 5))
      return paramSemana.mes;
    return localStateManager.getStringField(paramSemana, jdoInheritedFieldCount + 5, paramSemana.mes);
  }

  private static final void jdoSetmes(Semana paramSemana, String paramString)
  {
    if (paramSemana.jdoFlags == 0)
    {
      paramSemana.mes = paramString;
      return;
    }
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramSemana.mes = paramString;
      return;
    }
    localStateManager.setStringField(paramSemana, jdoInheritedFieldCount + 5, paramSemana.mes, paramString);
  }

  private static final int jdoGetsemanaAnio(Semana paramSemana)
  {
    if (paramSemana.jdoFlags <= 0)
      return paramSemana.semanaAnio;
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
      return paramSemana.semanaAnio;
    if (localStateManager.isLoaded(paramSemana, jdoInheritedFieldCount + 6))
      return paramSemana.semanaAnio;
    return localStateManager.getIntField(paramSemana, jdoInheritedFieldCount + 6, paramSemana.semanaAnio);
  }

  private static final void jdoSetsemanaAnio(Semana paramSemana, int paramInt)
  {
    if (paramSemana.jdoFlags == 0)
    {
      paramSemana.semanaAnio = paramInt;
      return;
    }
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramSemana.semanaAnio = paramInt;
      return;
    }
    localStateManager.setIntField(paramSemana, jdoInheritedFieldCount + 6, paramSemana.semanaAnio, paramInt);
  }

  private static final int jdoGetsemanaMes(Semana paramSemana)
  {
    if (paramSemana.jdoFlags <= 0)
      return paramSemana.semanaMes;
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
      return paramSemana.semanaMes;
    if (localStateManager.isLoaded(paramSemana, jdoInheritedFieldCount + 7))
      return paramSemana.semanaMes;
    return localStateManager.getIntField(paramSemana, jdoInheritedFieldCount + 7, paramSemana.semanaMes);
  }

  private static final void jdoSetsemanaMes(Semana paramSemana, int paramInt)
  {
    if (paramSemana.jdoFlags == 0)
    {
      paramSemana.semanaMes = paramInt;
      return;
    }
    StateManager localStateManager = paramSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramSemana.semanaMes = paramInt;
      return;
    }
    localStateManager.setIntField(paramSemana, jdoInheritedFieldCount + 7, paramSemana.semanaMes, paramInt);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}