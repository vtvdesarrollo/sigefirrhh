package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class TarifaAriPK
  implements Serializable
{
  public long idTarifaAri;

  public TarifaAriPK()
  {
  }

  public TarifaAriPK(long idTarifaAri)
  {
    this.idTarifaAri = idTarifaAri;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TarifaAriPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TarifaAriPK thatPK)
  {
    return 
      this.idTarifaAri == thatPK.idTarifaAri;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTarifaAri)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTarifaAri);
  }
}