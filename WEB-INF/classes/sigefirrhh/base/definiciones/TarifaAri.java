package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TarifaAri
  implements Serializable, PersistenceCapable
{
  private long idTarifaAri;
  private double tarifa;
  private double sustraendo;
  private double porcentaje;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idTarifaAri", "porcentaje", "sustraendo", "tarifa" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, Double.TYPE, Double.TYPE, Double.TYPE };
  private static final byte[] jdoFieldFlags = { 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return "Tarifa " + jdoGettarifa(this) + " - Sustraendo " + 
      jdoGetsustraendo(this) + " - Porcentaje " + 
      jdoGetporcentaje(this);
  }

  public double getPorcentaje()
  {
    return jdoGetporcentaje(this);
  }

  public double getSustraendo()
  {
    return jdoGetsustraendo(this);
  }

  public double getTarifa()
  {
    return jdoGettarifa(this);
  }

  public void setPorcentaje(double d)
  {
    jdoSetporcentaje(this, d);
  }

  public void setSustraendo(double d)
  {
    jdoSetsustraendo(this, d);
  }

  public void setTarifa(double d)
  {
    jdoSettarifa(this, d);
  }

  public long getIdTarifaAri()
  {
    return jdoGetidTarifaAri(this);
  }

  public void setIdTarifaAri(long l)
  {
    jdoSetidTarifaAri(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.TarifaAri"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TarifaAri());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TarifaAri localTarifaAri = new TarifaAri();
    localTarifaAri.jdoFlags = 1;
    localTarifaAri.jdoStateManager = paramStateManager;
    return localTarifaAri;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TarifaAri localTarifaAri = new TarifaAri();
    localTarifaAri.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTarifaAri.jdoFlags = 1;
    localTarifaAri.jdoStateManager = paramStateManager;
    return localTarifaAri;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTarifaAri);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sustraendo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.tarifa);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTarifaAri = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sustraendo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tarifa = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TarifaAri paramTarifaAri, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTarifaAri == null)
        throw new IllegalArgumentException("arg1");
      this.idTarifaAri = paramTarifaAri.idTarifaAri;
      return;
    case 1:
      if (paramTarifaAri == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramTarifaAri.porcentaje;
      return;
    case 2:
      if (paramTarifaAri == null)
        throw new IllegalArgumentException("arg1");
      this.sustraendo = paramTarifaAri.sustraendo;
      return;
    case 3:
      if (paramTarifaAri == null)
        throw new IllegalArgumentException("arg1");
      this.tarifa = paramTarifaAri.tarifa;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TarifaAri))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TarifaAri localTarifaAri = (TarifaAri)paramObject;
    if (localTarifaAri.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTarifaAri, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TarifaAriPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TarifaAriPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TarifaAriPK))
      throw new IllegalArgumentException("arg1");
    TarifaAriPK localTarifaAriPK = (TarifaAriPK)paramObject;
    localTarifaAriPK.idTarifaAri = this.idTarifaAri;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TarifaAriPK))
      throw new IllegalArgumentException("arg1");
    TarifaAriPK localTarifaAriPK = (TarifaAriPK)paramObject;
    this.idTarifaAri = localTarifaAriPK.idTarifaAri;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TarifaAriPK))
      throw new IllegalArgumentException("arg2");
    TarifaAriPK localTarifaAriPK = (TarifaAriPK)paramObject;
    localTarifaAriPK.idTarifaAri = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TarifaAriPK))
      throw new IllegalArgumentException("arg2");
    TarifaAriPK localTarifaAriPK = (TarifaAriPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localTarifaAriPK.idTarifaAri);
  }

  private static final long jdoGetidTarifaAri(TarifaAri paramTarifaAri)
  {
    return paramTarifaAri.idTarifaAri;
  }

  private static final void jdoSetidTarifaAri(TarifaAri paramTarifaAri, long paramLong)
  {
    StateManager localStateManager = paramTarifaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramTarifaAri.idTarifaAri = paramLong;
      return;
    }
    localStateManager.setLongField(paramTarifaAri, jdoInheritedFieldCount + 0, paramTarifaAri.idTarifaAri, paramLong);
  }

  private static final double jdoGetporcentaje(TarifaAri paramTarifaAri)
  {
    if (paramTarifaAri.jdoFlags <= 0)
      return paramTarifaAri.porcentaje;
    StateManager localStateManager = paramTarifaAri.jdoStateManager;
    if (localStateManager == null)
      return paramTarifaAri.porcentaje;
    if (localStateManager.isLoaded(paramTarifaAri, jdoInheritedFieldCount + 1))
      return paramTarifaAri.porcentaje;
    return localStateManager.getDoubleField(paramTarifaAri, jdoInheritedFieldCount + 1, paramTarifaAri.porcentaje);
  }

  private static final void jdoSetporcentaje(TarifaAri paramTarifaAri, double paramDouble)
  {
    if (paramTarifaAri.jdoFlags == 0)
    {
      paramTarifaAri.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramTarifaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramTarifaAri.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTarifaAri, jdoInheritedFieldCount + 1, paramTarifaAri.porcentaje, paramDouble);
  }

  private static final double jdoGetsustraendo(TarifaAri paramTarifaAri)
  {
    if (paramTarifaAri.jdoFlags <= 0)
      return paramTarifaAri.sustraendo;
    StateManager localStateManager = paramTarifaAri.jdoStateManager;
    if (localStateManager == null)
      return paramTarifaAri.sustraendo;
    if (localStateManager.isLoaded(paramTarifaAri, jdoInheritedFieldCount + 2))
      return paramTarifaAri.sustraendo;
    return localStateManager.getDoubleField(paramTarifaAri, jdoInheritedFieldCount + 2, paramTarifaAri.sustraendo);
  }

  private static final void jdoSetsustraendo(TarifaAri paramTarifaAri, double paramDouble)
  {
    if (paramTarifaAri.jdoFlags == 0)
    {
      paramTarifaAri.sustraendo = paramDouble;
      return;
    }
    StateManager localStateManager = paramTarifaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramTarifaAri.sustraendo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTarifaAri, jdoInheritedFieldCount + 2, paramTarifaAri.sustraendo, paramDouble);
  }

  private static final double jdoGettarifa(TarifaAri paramTarifaAri)
  {
    if (paramTarifaAri.jdoFlags <= 0)
      return paramTarifaAri.tarifa;
    StateManager localStateManager = paramTarifaAri.jdoStateManager;
    if (localStateManager == null)
      return paramTarifaAri.tarifa;
    if (localStateManager.isLoaded(paramTarifaAri, jdoInheritedFieldCount + 3))
      return paramTarifaAri.tarifa;
    return localStateManager.getDoubleField(paramTarifaAri, jdoInheritedFieldCount + 3, paramTarifaAri.tarifa);
  }

  private static final void jdoSettarifa(TarifaAri paramTarifaAri, double paramDouble)
  {
    if (paramTarifaAri.jdoFlags == 0)
    {
      paramTarifaAri.tarifa = paramDouble;
      return;
    }
    StateManager localStateManager = paramTarifaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramTarifaAri.tarifa = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTarifaAri, jdoInheritedFieldCount + 3, paramTarifaAri.tarifa, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}