package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ConceptoTipoPersonal
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_TIPO;
  protected static final Map LISTA_DISTRIBUCION;
  private long idConceptoTipoPersonal;
  private TipoPersonal tipoPersonal;
  private Concepto concepto;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private String tipo;
  private double valor;
  private double unidades;
  private String automaticoIngreso;
  private double topeMinimo;
  private double topeMaximo;
  private double topeAnual;
  private String baseJubilacion;
  private double montoEscenario;
  private String formulaConcepto;
  private double multiplicadorFormula;
  private String otraMoneda;
  private String homologacion;
  private String anual;
  private double ocurrencia;
  private String reflejaMovimiento;
  private String aprobacionMpd;
  private String recalculo;
  private String distribucion;
  private ContratoColectivo contratoColectivo;
  private String baseLegal;
  private String codConcepto;
  private int codFrecuenciaPago;
  private String codTipoPersonal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anual", "aprobacionMpd", "automaticoIngreso", "baseJubilacion", "baseLegal", "codConcepto", "codFrecuenciaPago", "codTipoPersonal", "concepto", "contratoColectivo", "distribucion", "formulaConcepto", "frecuenciaTipoPersonal", "homologacion", "idConceptoTipoPersonal", "idSitp", "montoEscenario", "multiplicadorFormula", "ocurrencia", "otraMoneda", "recalculo", "reflejaMovimiento", "tiempoSitp", "tipo", "tipoPersonal", "topeAnual", "topeMaximo", "topeMinimo", "unidades", "valor" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("sigefirrhh.base.definiciones.ContratoColectivo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 26, 26, 21, 21, 26, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoTipoPersonal());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_TIPO = 
      new LinkedHashMap();
    LISTA_DISTRIBUCION = 
      new LinkedHashMap();
    LISTA_TIPO.put("F", "MONTO FIJO");
    LISTA_TIPO.put("P", "PORCENTAJE");
    LISTA_TIPO.put("I", "PORCENTAJE INDIVIDUAL");
    LISTA_TIPO.put("D", "DIFERENCIAL");
    LISTA_TIPO.put("H", "HORAS DOCENTE");
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_DISTRIBUCION.put("S", "POR TRABAJADOR");
    LISTA_DISTRIBUCION.put("N", "GLOBAL");
  }

  public ConceptoTipoPersonal()
  {
    jdoSettipo(this, "F");

    jdoSetvalor(this, 0.0D);

    jdoSetunidades(this, 0.0D);

    jdoSetautomaticoIngreso(this, "N");

    jdoSettopeMinimo(this, 0.0D);

    jdoSettopeMaximo(this, 0.0D);

    jdoSettopeAnual(this, 0.0D);

    jdoSetbaseJubilacion(this, "N");

    jdoSetmontoEscenario(this, 0.0D);

    jdoSetformulaConcepto(this, "S");

    jdoSetmultiplicadorFormula(this, 1.0D);

    jdoSetotraMoneda(this, "N");

    jdoSethomologacion(this, "N");

    jdoSetanual(this, "N");

    jdoSetocurrencia(this, 1.0D);

    jdoSetreflejaMovimiento(this, "N");

    jdoSetaprobacionMpd(this, "N");

    jdoSetrecalculo(this, "N");

    jdoSetdistribucion(this, "S");
  }

  public String toString()
  {
    return jdoGetconcepto(this).getCodConcepto() + "  -  " + 
      jdoGetconcepto(this).getDescripcion();
  }

  public String getAutomaticoIngreso()
  {
    return jdoGetautomaticoIngreso(this);
  }

  public Concepto getConcepto()
  {
    return jdoGetconcepto(this);
  }

  public String getFormulaConcepto()
  {
    return jdoGetformulaConcepto(this);
  }

  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal()
  {
    return jdoGetfrecuenciaTipoPersonal(this);
  }

  public String getHomologacion()
  {
    return jdoGethomologacion(this);
  }

  public long getIdConceptoTipoPersonal()
  {
    return jdoGetidConceptoTipoPersonal(this);
  }

  public double getValor()
  {
    return jdoGetvalor(this);
  }

  public double getMontoEscenario()
  {
    return jdoGetmontoEscenario(this);
  }

  public double getMultiplicadorFormula()
  {
    return jdoGetmultiplicadorFormula(this);
  }

  public String getOtraMoneda()
  {
    return jdoGetotraMoneda(this);
  }

  public double getTopeAnual()
  {
    return jdoGettopeAnual(this);
  }

  public double getUnidades()
  {
    return jdoGetunidades(this);
  }

  public void setAutomaticoIngreso(String string)
  {
    jdoSetautomaticoIngreso(this, string);
  }

  public void setConcepto(Concepto concepto)
  {
    jdoSetconcepto(this, concepto);
  }

  public void setFormulaConcepto(String string)
  {
    jdoSetformulaConcepto(this, string);
  }

  public void setFrecuenciaTipoPersonal(FrecuenciaTipoPersonal personal)
  {
    jdoSetfrecuenciaTipoPersonal(this, personal);
  }

  public void setHomologacion(String string)
  {
    jdoSethomologacion(this, string);
  }

  public void setIdConceptoTipoPersonal(long l)
  {
    jdoSetidConceptoTipoPersonal(this, l);
  }

  public void setValor(double d)
  {
    jdoSetvalor(this, d);
  }

  public void setMontoEscenario(double d)
  {
    jdoSetmontoEscenario(this, d);
  }

  public void setMultiplicadorFormula(double d)
  {
    jdoSetmultiplicadorFormula(this, d);
  }

  public void setOtraMoneda(String string)
  {
    jdoSetotraMoneda(this, string);
  }

  public void setTopeAnual(double d)
  {
    jdoSettopeAnual(this, d);
  }

  public void setUnidades(double d)
  {
    jdoSetunidades(this, d);
  }

  public String getTipo()
  {
    return jdoGettipo(this);
  }

  public void setTipo(String string)
  {
    jdoSettipo(this, string);
  }

  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal personal) {
    jdoSettipoPersonal(this, personal);
  }

  public String getCodConcepto()
  {
    return jdoGetcodConcepto(this);
  }

  public int getCodFrecuenciaPago()
  {
    return jdoGetcodFrecuenciaPago(this);
  }

  public String getCodTipoPersonal()
  {
    return jdoGetcodTipoPersonal(this);
  }

  public void setCodConcepto(String string)
  {
    jdoSetcodConcepto(this, string);
  }

  public void setCodFrecuenciaPago(int i)
  {
    jdoSetcodFrecuenciaPago(this, i);
  }

  public void setCodTipoPersonal(String string)
  {
    jdoSetcodTipoPersonal(this, string);
  }

  public String getBaseLegal()
  {
    return jdoGetbaseLegal(this);
  }

  public void setBaseLegal(String string)
  {
    jdoSetbaseLegal(this, string);
  }

  public String getAnual()
  {
    return jdoGetanual(this);
  }

  public void setAnual(String anual)
  {
    jdoSetanual(this, anual);
  }

  public double getOcurrencia()
  {
    return jdoGetocurrencia(this);
  }

  public void setOcurrencia(double ocurrencia)
  {
    jdoSetocurrencia(this, ocurrencia);
  }

  public ContratoColectivo getContratoColectivo()
  {
    return jdoGetcontratoColectivo(this);
  }

  public void setContratoColectivo(ContratoColectivo colectivo)
  {
    jdoSetcontratoColectivo(this, colectivo);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public double getTopeMaximo()
  {
    return jdoGettopeMaximo(this);
  }

  public double getTopeMinimo()
  {
    return jdoGettopeMinimo(this);
  }

  public void setTopeMaximo(double d)
  {
    jdoSettopeMaximo(this, d);
  }

  public void setTopeMinimo(double d)
  {
    jdoSettopeMinimo(this, d);
  }

  public String getAprobacionMpd()
  {
    return jdoGetaprobacionMpd(this);
  }

  public String getReflejaMovimiento()
  {
    return jdoGetreflejaMovimiento(this);
  }

  public void setAprobacionMpd(String string)
  {
    jdoSetaprobacionMpd(this, string);
  }

  public void setReflejaMovimiento(String string)
  {
    jdoSetreflejaMovimiento(this, string);
  }

  public String getRecalculo()
  {
    return jdoGetrecalculo(this);
  }

  public void setRecalculo(String string)
  {
    jdoSetrecalculo(this, string);
  }

  public String getDistribucion() {
    return jdoGetdistribucion(this);
  }
  public void setDistribucion(String distribucion) {
    jdoSetdistribucion(this, distribucion);
  }
  public String getBaseJubilacion() {
    return jdoGetbaseJubilacion(this);
  }
  public void setBaseJubilacion(String baseJubilacion) {
    jdoSetbaseJubilacion(this, baseJubilacion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 30;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoTipoPersonal localConceptoTipoPersonal = new ConceptoTipoPersonal();
    localConceptoTipoPersonal.jdoFlags = 1;
    localConceptoTipoPersonal.jdoStateManager = paramStateManager;
    return localConceptoTipoPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoTipoPersonal localConceptoTipoPersonal = new ConceptoTipoPersonal();
    localConceptoTipoPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoTipoPersonal.jdoFlags = 1;
    localConceptoTipoPersonal.jdoStateManager = paramStateManager;
    return localConceptoTipoPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anual);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.automaticoIngreso);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.baseJubilacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.baseLegal);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codConcepto);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codFrecuenciaPago);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoPersonal);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.concepto);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.contratoColectivo);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.distribucion);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.formulaConcepto);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaTipoPersonal);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.homologacion);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoTipoPersonal);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoEscenario);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.multiplicadorFormula);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.ocurrencia);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.otraMoneda);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.recalculo);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.reflejaMovimiento);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.topeAnual);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.topeMaximo);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.topeMinimo);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.valor);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anual = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.automaticoIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseJubilacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseLegal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codConcepto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codFrecuenciaPago = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.concepto = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.contratoColectivo = ((ContratoColectivo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.distribucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.formulaConcepto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaTipoPersonal = ((FrecuenciaTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.homologacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoTipoPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoEscenario = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.multiplicadorFormula = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ocurrencia = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.otraMoneda = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.recalculo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.reflejaMovimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.topeAnual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.topeMaximo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.topeMinimo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.valor = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoTipoPersonal paramConceptoTipoPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.anual = paramConceptoTipoPersonal.anual;
      return;
    case 1:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramConceptoTipoPersonal.aprobacionMpd;
      return;
    case 2:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.automaticoIngreso = paramConceptoTipoPersonal.automaticoIngreso;
      return;
    case 3:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.baseJubilacion = paramConceptoTipoPersonal.baseJubilacion;
      return;
    case 4:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.baseLegal = paramConceptoTipoPersonal.baseLegal;
      return;
    case 5:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.codConcepto = paramConceptoTipoPersonal.codConcepto;
      return;
    case 6:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.codFrecuenciaPago = paramConceptoTipoPersonal.codFrecuenciaPago;
      return;
    case 7:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoPersonal = paramConceptoTipoPersonal.codTipoPersonal;
      return;
    case 8:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.concepto = paramConceptoTipoPersonal.concepto;
      return;
    case 9:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.contratoColectivo = paramConceptoTipoPersonal.contratoColectivo;
      return;
    case 10:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.distribucion = paramConceptoTipoPersonal.distribucion;
      return;
    case 11:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.formulaConcepto = paramConceptoTipoPersonal.formulaConcepto;
      return;
    case 12:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaTipoPersonal = paramConceptoTipoPersonal.frecuenciaTipoPersonal;
      return;
    case 13:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.homologacion = paramConceptoTipoPersonal.homologacion;
      return;
    case 14:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoTipoPersonal = paramConceptoTipoPersonal.idConceptoTipoPersonal;
      return;
    case 15:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramConceptoTipoPersonal.idSitp;
      return;
    case 16:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.montoEscenario = paramConceptoTipoPersonal.montoEscenario;
      return;
    case 17:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.multiplicadorFormula = paramConceptoTipoPersonal.multiplicadorFormula;
      return;
    case 18:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.ocurrencia = paramConceptoTipoPersonal.ocurrencia;
      return;
    case 19:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.otraMoneda = paramConceptoTipoPersonal.otraMoneda;
      return;
    case 20:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.recalculo = paramConceptoTipoPersonal.recalculo;
      return;
    case 21:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.reflejaMovimiento = paramConceptoTipoPersonal.reflejaMovimiento;
      return;
    case 22:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramConceptoTipoPersonal.tiempoSitp;
      return;
    case 23:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramConceptoTipoPersonal.tipo;
      return;
    case 24:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramConceptoTipoPersonal.tipoPersonal;
      return;
    case 25:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.topeAnual = paramConceptoTipoPersonal.topeAnual;
      return;
    case 26:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.topeMaximo = paramConceptoTipoPersonal.topeMaximo;
      return;
    case 27:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.topeMinimo = paramConceptoTipoPersonal.topeMinimo;
      return;
    case 28:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramConceptoTipoPersonal.unidades;
      return;
    case 29:
      if (paramConceptoTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.valor = paramConceptoTipoPersonal.valor;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoTipoPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoTipoPersonal localConceptoTipoPersonal = (ConceptoTipoPersonal)paramObject;
    if (localConceptoTipoPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoTipoPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoTipoPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoTipoPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoTipoPersonalPK))
      throw new IllegalArgumentException("arg1");
    ConceptoTipoPersonalPK localConceptoTipoPersonalPK = (ConceptoTipoPersonalPK)paramObject;
    localConceptoTipoPersonalPK.idConceptoTipoPersonal = this.idConceptoTipoPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoTipoPersonalPK))
      throw new IllegalArgumentException("arg1");
    ConceptoTipoPersonalPK localConceptoTipoPersonalPK = (ConceptoTipoPersonalPK)paramObject;
    this.idConceptoTipoPersonal = localConceptoTipoPersonalPK.idConceptoTipoPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoTipoPersonalPK))
      throw new IllegalArgumentException("arg2");
    ConceptoTipoPersonalPK localConceptoTipoPersonalPK = (ConceptoTipoPersonalPK)paramObject;
    localConceptoTipoPersonalPK.idConceptoTipoPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 14);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoTipoPersonalPK))
      throw new IllegalArgumentException("arg2");
    ConceptoTipoPersonalPK localConceptoTipoPersonalPK = (ConceptoTipoPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 14, localConceptoTipoPersonalPK.idConceptoTipoPersonal);
  }

  private static final String jdoGetanual(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.anual;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.anual;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 0))
      return paramConceptoTipoPersonal.anual;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 0, paramConceptoTipoPersonal.anual);
  }

  private static final void jdoSetanual(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.anual = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.anual = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 0, paramConceptoTipoPersonal.anual, paramString);
  }

  private static final String jdoGetaprobacionMpd(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.aprobacionMpd;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.aprobacionMpd;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 1))
      return paramConceptoTipoPersonal.aprobacionMpd;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 1, paramConceptoTipoPersonal.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 1, paramConceptoTipoPersonal.aprobacionMpd, paramString);
  }

  private static final String jdoGetautomaticoIngreso(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.automaticoIngreso;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.automaticoIngreso;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 2))
      return paramConceptoTipoPersonal.automaticoIngreso;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 2, paramConceptoTipoPersonal.automaticoIngreso);
  }

  private static final void jdoSetautomaticoIngreso(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.automaticoIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.automaticoIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 2, paramConceptoTipoPersonal.automaticoIngreso, paramString);
  }

  private static final String jdoGetbaseJubilacion(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.baseJubilacion;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.baseJubilacion;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 3))
      return paramConceptoTipoPersonal.baseJubilacion;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 3, paramConceptoTipoPersonal.baseJubilacion);
  }

  private static final void jdoSetbaseJubilacion(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.baseJubilacion = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.baseJubilacion = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 3, paramConceptoTipoPersonal.baseJubilacion, paramString);
  }

  private static final String jdoGetbaseLegal(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.baseLegal;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.baseLegal;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 4))
      return paramConceptoTipoPersonal.baseLegal;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 4, paramConceptoTipoPersonal.baseLegal);
  }

  private static final void jdoSetbaseLegal(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.baseLegal = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.baseLegal = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 4, paramConceptoTipoPersonal.baseLegal, paramString);
  }

  private static final String jdoGetcodConcepto(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.codConcepto;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.codConcepto;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 5))
      return paramConceptoTipoPersonal.codConcepto;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 5, paramConceptoTipoPersonal.codConcepto);
  }

  private static final void jdoSetcodConcepto(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.codConcepto = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.codConcepto = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 5, paramConceptoTipoPersonal.codConcepto, paramString);
  }

  private static final int jdoGetcodFrecuenciaPago(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.codFrecuenciaPago;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.codFrecuenciaPago;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 6))
      return paramConceptoTipoPersonal.codFrecuenciaPago;
    return localStateManager.getIntField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 6, paramConceptoTipoPersonal.codFrecuenciaPago);
  }

  private static final void jdoSetcodFrecuenciaPago(ConceptoTipoPersonal paramConceptoTipoPersonal, int paramInt)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.codFrecuenciaPago = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.codFrecuenciaPago = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 6, paramConceptoTipoPersonal.codFrecuenciaPago, paramInt);
  }

  private static final String jdoGetcodTipoPersonal(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.codTipoPersonal;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.codTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 7))
      return paramConceptoTipoPersonal.codTipoPersonal;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 7, paramConceptoTipoPersonal.codTipoPersonal);
  }

  private static final void jdoSetcodTipoPersonal(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.codTipoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.codTipoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 7, paramConceptoTipoPersonal.codTipoPersonal, paramString);
  }

  private static final Concepto jdoGetconcepto(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.concepto;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 8))
      return paramConceptoTipoPersonal.concepto;
    return (Concepto)localStateManager.getObjectField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 8, paramConceptoTipoPersonal.concepto);
  }

  private static final void jdoSetconcepto(ConceptoTipoPersonal paramConceptoTipoPersonal, Concepto paramConcepto)
  {
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.concepto = paramConcepto;
      return;
    }
    localStateManager.setObjectField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 8, paramConceptoTipoPersonal.concepto, paramConcepto);
  }

  private static final ContratoColectivo jdoGetcontratoColectivo(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.contratoColectivo;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 9))
      return paramConceptoTipoPersonal.contratoColectivo;
    return (ContratoColectivo)localStateManager.getObjectField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 9, paramConceptoTipoPersonal.contratoColectivo);
  }

  private static final void jdoSetcontratoColectivo(ConceptoTipoPersonal paramConceptoTipoPersonal, ContratoColectivo paramContratoColectivo)
  {
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.contratoColectivo = paramContratoColectivo;
      return;
    }
    localStateManager.setObjectField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 9, paramConceptoTipoPersonal.contratoColectivo, paramContratoColectivo);
  }

  private static final String jdoGetdistribucion(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.distribucion;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.distribucion;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 10))
      return paramConceptoTipoPersonal.distribucion;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 10, paramConceptoTipoPersonal.distribucion);
  }

  private static final void jdoSetdistribucion(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.distribucion = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.distribucion = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 10, paramConceptoTipoPersonal.distribucion, paramString);
  }

  private static final String jdoGetformulaConcepto(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.formulaConcepto;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.formulaConcepto;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 11))
      return paramConceptoTipoPersonal.formulaConcepto;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 11, paramConceptoTipoPersonal.formulaConcepto);
  }

  private static final void jdoSetformulaConcepto(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.formulaConcepto = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.formulaConcepto = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 11, paramConceptoTipoPersonal.formulaConcepto, paramString);
  }

  private static final FrecuenciaTipoPersonal jdoGetfrecuenciaTipoPersonal(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.frecuenciaTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 12))
      return paramConceptoTipoPersonal.frecuenciaTipoPersonal;
    return (FrecuenciaTipoPersonal)localStateManager.getObjectField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 12, paramConceptoTipoPersonal.frecuenciaTipoPersonal);
  }

  private static final void jdoSetfrecuenciaTipoPersonal(ConceptoTipoPersonal paramConceptoTipoPersonal, FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.frecuenciaTipoPersonal = paramFrecuenciaTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 12, paramConceptoTipoPersonal.frecuenciaTipoPersonal, paramFrecuenciaTipoPersonal);
  }

  private static final String jdoGethomologacion(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.homologacion;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.homologacion;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 13))
      return paramConceptoTipoPersonal.homologacion;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 13, paramConceptoTipoPersonal.homologacion);
  }

  private static final void jdoSethomologacion(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.homologacion = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.homologacion = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 13, paramConceptoTipoPersonal.homologacion, paramString);
  }

  private static final long jdoGetidConceptoTipoPersonal(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    return paramConceptoTipoPersonal.idConceptoTipoPersonal;
  }

  private static final void jdoSetidConceptoTipoPersonal(ConceptoTipoPersonal paramConceptoTipoPersonal, long paramLong)
  {
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.idConceptoTipoPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 14, paramConceptoTipoPersonal.idConceptoTipoPersonal, paramLong);
  }

  private static final int jdoGetidSitp(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.idSitp;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.idSitp;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 15))
      return paramConceptoTipoPersonal.idSitp;
    return localStateManager.getIntField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 15, paramConceptoTipoPersonal.idSitp);
  }

  private static final void jdoSetidSitp(ConceptoTipoPersonal paramConceptoTipoPersonal, int paramInt)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 15, paramConceptoTipoPersonal.idSitp, paramInt);
  }

  private static final double jdoGetmontoEscenario(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.montoEscenario;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.montoEscenario;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 16))
      return paramConceptoTipoPersonal.montoEscenario;
    return localStateManager.getDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 16, paramConceptoTipoPersonal.montoEscenario);
  }

  private static final void jdoSetmontoEscenario(ConceptoTipoPersonal paramConceptoTipoPersonal, double paramDouble)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.montoEscenario = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.montoEscenario = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 16, paramConceptoTipoPersonal.montoEscenario, paramDouble);
  }

  private static final double jdoGetmultiplicadorFormula(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.multiplicadorFormula;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.multiplicadorFormula;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 17))
      return paramConceptoTipoPersonal.multiplicadorFormula;
    return localStateManager.getDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 17, paramConceptoTipoPersonal.multiplicadorFormula);
  }

  private static final void jdoSetmultiplicadorFormula(ConceptoTipoPersonal paramConceptoTipoPersonal, double paramDouble)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.multiplicadorFormula = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.multiplicadorFormula = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 17, paramConceptoTipoPersonal.multiplicadorFormula, paramDouble);
  }

  private static final double jdoGetocurrencia(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.ocurrencia;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.ocurrencia;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 18))
      return paramConceptoTipoPersonal.ocurrencia;
    return localStateManager.getDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 18, paramConceptoTipoPersonal.ocurrencia);
  }

  private static final void jdoSetocurrencia(ConceptoTipoPersonal paramConceptoTipoPersonal, double paramDouble)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.ocurrencia = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.ocurrencia = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 18, paramConceptoTipoPersonal.ocurrencia, paramDouble);
  }

  private static final String jdoGetotraMoneda(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.otraMoneda;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.otraMoneda;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 19))
      return paramConceptoTipoPersonal.otraMoneda;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 19, paramConceptoTipoPersonal.otraMoneda);
  }

  private static final void jdoSetotraMoneda(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.otraMoneda = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.otraMoneda = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 19, paramConceptoTipoPersonal.otraMoneda, paramString);
  }

  private static final String jdoGetrecalculo(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.recalculo;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.recalculo;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 20))
      return paramConceptoTipoPersonal.recalculo;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 20, paramConceptoTipoPersonal.recalculo);
  }

  private static final void jdoSetrecalculo(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.recalculo = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.recalculo = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 20, paramConceptoTipoPersonal.recalculo, paramString);
  }

  private static final String jdoGetreflejaMovimiento(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.reflejaMovimiento;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.reflejaMovimiento;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 21))
      return paramConceptoTipoPersonal.reflejaMovimiento;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 21, paramConceptoTipoPersonal.reflejaMovimiento);
  }

  private static final void jdoSetreflejaMovimiento(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.reflejaMovimiento = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.reflejaMovimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 21, paramConceptoTipoPersonal.reflejaMovimiento, paramString);
  }

  private static final Date jdoGettiempoSitp(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.tiempoSitp;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.tiempoSitp;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 22))
      return paramConceptoTipoPersonal.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 22, paramConceptoTipoPersonal.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ConceptoTipoPersonal paramConceptoTipoPersonal, Date paramDate)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 22, paramConceptoTipoPersonal.tiempoSitp, paramDate);
  }

  private static final String jdoGettipo(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.tipo;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.tipo;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 23))
      return paramConceptoTipoPersonal.tipo;
    return localStateManager.getStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 23, paramConceptoTipoPersonal.tipo);
  }

  private static final void jdoSettipo(ConceptoTipoPersonal paramConceptoTipoPersonal, String paramString)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 23, paramConceptoTipoPersonal.tipo, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.tipoPersonal;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 24))
      return paramConceptoTipoPersonal.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 24, paramConceptoTipoPersonal.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ConceptoTipoPersonal paramConceptoTipoPersonal, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 24, paramConceptoTipoPersonal.tipoPersonal, paramTipoPersonal);
  }

  private static final double jdoGettopeAnual(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.topeAnual;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.topeAnual;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 25))
      return paramConceptoTipoPersonal.topeAnual;
    return localStateManager.getDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 25, paramConceptoTipoPersonal.topeAnual);
  }

  private static final void jdoSettopeAnual(ConceptoTipoPersonal paramConceptoTipoPersonal, double paramDouble)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.topeAnual = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.topeAnual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 25, paramConceptoTipoPersonal.topeAnual, paramDouble);
  }

  private static final double jdoGettopeMaximo(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.topeMaximo;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.topeMaximo;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 26))
      return paramConceptoTipoPersonal.topeMaximo;
    return localStateManager.getDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 26, paramConceptoTipoPersonal.topeMaximo);
  }

  private static final void jdoSettopeMaximo(ConceptoTipoPersonal paramConceptoTipoPersonal, double paramDouble)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.topeMaximo = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.topeMaximo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 26, paramConceptoTipoPersonal.topeMaximo, paramDouble);
  }

  private static final double jdoGettopeMinimo(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.topeMinimo;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.topeMinimo;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 27))
      return paramConceptoTipoPersonal.topeMinimo;
    return localStateManager.getDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 27, paramConceptoTipoPersonal.topeMinimo);
  }

  private static final void jdoSettopeMinimo(ConceptoTipoPersonal paramConceptoTipoPersonal, double paramDouble)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.topeMinimo = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.topeMinimo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 27, paramConceptoTipoPersonal.topeMinimo, paramDouble);
  }

  private static final double jdoGetunidades(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.unidades;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.unidades;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 28))
      return paramConceptoTipoPersonal.unidades;
    return localStateManager.getDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 28, paramConceptoTipoPersonal.unidades);
  }

  private static final void jdoSetunidades(ConceptoTipoPersonal paramConceptoTipoPersonal, double paramDouble)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 28, paramConceptoTipoPersonal.unidades, paramDouble);
  }

  private static final double jdoGetvalor(ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    if (paramConceptoTipoPersonal.jdoFlags <= 0)
      return paramConceptoTipoPersonal.valor;
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoTipoPersonal.valor;
    if (localStateManager.isLoaded(paramConceptoTipoPersonal, jdoInheritedFieldCount + 29))
      return paramConceptoTipoPersonal.valor;
    return localStateManager.getDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 29, paramConceptoTipoPersonal.valor);
  }

  private static final void jdoSetvalor(ConceptoTipoPersonal paramConceptoTipoPersonal, double paramDouble)
  {
    if (paramConceptoTipoPersonal.jdoFlags == 0)
    {
      paramConceptoTipoPersonal.valor = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoTipoPersonal.valor = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoTipoPersonal, jdoInheritedFieldCount + 29, paramConceptoTipoPersonal.valor, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}