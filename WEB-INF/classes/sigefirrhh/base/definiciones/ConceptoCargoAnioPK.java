package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ConceptoCargoAnioPK
  implements Serializable
{
  public long idConceptoCargoAnio;

  public ConceptoCargoAnioPK()
  {
  }

  public ConceptoCargoAnioPK(long idConceptoCargoAnio)
  {
    this.idConceptoCargoAnio = idConceptoCargoAnio;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoCargoAnioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoCargoAnioPK thatPK)
  {
    return 
      this.idConceptoCargoAnio == thatPK.idConceptoCargoAnio;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoCargoAnio)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoCargoAnio);
  }
}