package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class TurnoPK
  implements Serializable
{
  public long idTurno;

  public TurnoPK()
  {
  }

  public TurnoPK(long idTurno)
  {
    this.idTurno = idTurno;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TurnoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TurnoPK thatPK)
  {
    return 
      this.idTurno == thatPK.idTurno;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTurno)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTurno);
  }
}