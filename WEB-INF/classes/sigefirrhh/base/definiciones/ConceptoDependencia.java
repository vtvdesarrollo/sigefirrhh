package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.CaracteristicaDependencia;

public class ConceptoDependencia
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idConceptoDependencia;
  private CaracteristicaDependencia caracteristicaDependencia;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private double unidades;
  private double monto;
  private String automaticoIngreso;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "automaticoIngreso", "caracteristicaDependencia", "conceptoTipoPersonal", "idConceptoDependencia", "monto", "unidades" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.CaracteristicaDependencia"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Long.TYPE, Double.TYPE, Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 26, 26, 24, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoDependencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoDependencia());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public ConceptoDependencia()
  {
    jdoSetunidades(this, 0.0D);

    jdoSetmonto(this, 0.0D);

    jdoSetautomaticoIngreso(this, "N");
  }
  public String toString() {
    return jdoGetcaracteristicaDependencia(this) + " - " + jdoGetconceptoTipoPersonal(this).getConcepto().getCodConcepto() + " " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion();
  }

  public String getAutomaticoIngreso() {
    return jdoGetautomaticoIngreso(this);
  }
  public void setAutomaticoIngreso(String automaticoIngreso) {
    jdoSetautomaticoIngreso(this, automaticoIngreso);
  }
  public CaracteristicaDependencia getCaracteristicaDependencia() {
    return jdoGetcaracteristicaDependencia(this);
  }

  public void setCaracteristicaDependencia(CaracteristicaDependencia caracteristicaDependencia) {
    jdoSetcaracteristicaDependencia(this, caracteristicaDependencia);
  }
  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public long getIdConceptoDependencia() {
    return jdoGetidConceptoDependencia(this);
  }
  public void setIdConceptoDependencia(long idConceptoDependencia) {
    jdoSetidConceptoDependencia(this, idConceptoDependencia);
  }
  public double getMonto() {
    return jdoGetmonto(this);
  }
  public void setMonto(double monto) {
    jdoSetmonto(this, monto);
  }
  public double getUnidades() {
    return jdoGetunidades(this);
  }
  public void setUnidades(double unidades) {
    jdoSetunidades(this, unidades);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoDependencia localConceptoDependencia = new ConceptoDependencia();
    localConceptoDependencia.jdoFlags = 1;
    localConceptoDependencia.jdoStateManager = paramStateManager;
    return localConceptoDependencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoDependencia localConceptoDependencia = new ConceptoDependencia();
    localConceptoDependencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoDependencia.jdoFlags = 1;
    localConceptoDependencia.jdoStateManager = paramStateManager;
    return localConceptoDependencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.automaticoIngreso);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.caracteristicaDependencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoDependencia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.automaticoIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.caracteristicaDependencia = ((CaracteristicaDependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoDependencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoDependencia paramConceptoDependencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.automaticoIngreso = paramConceptoDependencia.automaticoIngreso;
      return;
    case 1:
      if (paramConceptoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.caracteristicaDependencia = paramConceptoDependencia.caracteristicaDependencia;
      return;
    case 2:
      if (paramConceptoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoDependencia.conceptoTipoPersonal;
      return;
    case 3:
      if (paramConceptoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoDependencia = paramConceptoDependencia.idConceptoDependencia;
      return;
    case 4:
      if (paramConceptoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramConceptoDependencia.monto;
      return;
    case 5:
      if (paramConceptoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramConceptoDependencia.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoDependencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoDependencia localConceptoDependencia = (ConceptoDependencia)paramObject;
    if (localConceptoDependencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoDependencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoDependenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoDependenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoDependenciaPK))
      throw new IllegalArgumentException("arg1");
    ConceptoDependenciaPK localConceptoDependenciaPK = (ConceptoDependenciaPK)paramObject;
    localConceptoDependenciaPK.idConceptoDependencia = this.idConceptoDependencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoDependenciaPK))
      throw new IllegalArgumentException("arg1");
    ConceptoDependenciaPK localConceptoDependenciaPK = (ConceptoDependenciaPK)paramObject;
    this.idConceptoDependencia = localConceptoDependenciaPK.idConceptoDependencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoDependenciaPK))
      throw new IllegalArgumentException("arg2");
    ConceptoDependenciaPK localConceptoDependenciaPK = (ConceptoDependenciaPK)paramObject;
    localConceptoDependenciaPK.idConceptoDependencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoDependenciaPK))
      throw new IllegalArgumentException("arg2");
    ConceptoDependenciaPK localConceptoDependenciaPK = (ConceptoDependenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localConceptoDependenciaPK.idConceptoDependencia);
  }

  private static final String jdoGetautomaticoIngreso(ConceptoDependencia paramConceptoDependencia)
  {
    if (paramConceptoDependencia.jdoFlags <= 0)
      return paramConceptoDependencia.automaticoIngreso;
    StateManager localStateManager = paramConceptoDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDependencia.automaticoIngreso;
    if (localStateManager.isLoaded(paramConceptoDependencia, jdoInheritedFieldCount + 0))
      return paramConceptoDependencia.automaticoIngreso;
    return localStateManager.getStringField(paramConceptoDependencia, jdoInheritedFieldCount + 0, paramConceptoDependencia.automaticoIngreso);
  }

  private static final void jdoSetautomaticoIngreso(ConceptoDependencia paramConceptoDependencia, String paramString)
  {
    if (paramConceptoDependencia.jdoFlags == 0)
    {
      paramConceptoDependencia.automaticoIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDependencia.automaticoIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoDependencia, jdoInheritedFieldCount + 0, paramConceptoDependencia.automaticoIngreso, paramString);
  }

  private static final CaracteristicaDependencia jdoGetcaracteristicaDependencia(ConceptoDependencia paramConceptoDependencia)
  {
    StateManager localStateManager = paramConceptoDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDependencia.caracteristicaDependencia;
    if (localStateManager.isLoaded(paramConceptoDependencia, jdoInheritedFieldCount + 1))
      return paramConceptoDependencia.caracteristicaDependencia;
    return (CaracteristicaDependencia)localStateManager.getObjectField(paramConceptoDependencia, jdoInheritedFieldCount + 1, paramConceptoDependencia.caracteristicaDependencia);
  }

  private static final void jdoSetcaracteristicaDependencia(ConceptoDependencia paramConceptoDependencia, CaracteristicaDependencia paramCaracteristicaDependencia)
  {
    StateManager localStateManager = paramConceptoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDependencia.caracteristicaDependencia = paramCaracteristicaDependencia;
      return;
    }
    localStateManager.setObjectField(paramConceptoDependencia, jdoInheritedFieldCount + 1, paramConceptoDependencia.caracteristicaDependencia, paramCaracteristicaDependencia);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoDependencia paramConceptoDependencia)
  {
    StateManager localStateManager = paramConceptoDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDependencia.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoDependencia, jdoInheritedFieldCount + 2))
      return paramConceptoDependencia.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoDependencia, jdoInheritedFieldCount + 2, paramConceptoDependencia.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoDependencia paramConceptoDependencia, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDependencia.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoDependencia, jdoInheritedFieldCount + 2, paramConceptoDependencia.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final long jdoGetidConceptoDependencia(ConceptoDependencia paramConceptoDependencia)
  {
    return paramConceptoDependencia.idConceptoDependencia;
  }

  private static final void jdoSetidConceptoDependencia(ConceptoDependencia paramConceptoDependencia, long paramLong)
  {
    StateManager localStateManager = paramConceptoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDependencia.idConceptoDependencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoDependencia, jdoInheritedFieldCount + 3, paramConceptoDependencia.idConceptoDependencia, paramLong);
  }

  private static final double jdoGetmonto(ConceptoDependencia paramConceptoDependencia)
  {
    if (paramConceptoDependencia.jdoFlags <= 0)
      return paramConceptoDependencia.monto;
    StateManager localStateManager = paramConceptoDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDependencia.monto;
    if (localStateManager.isLoaded(paramConceptoDependencia, jdoInheritedFieldCount + 4))
      return paramConceptoDependencia.monto;
    return localStateManager.getDoubleField(paramConceptoDependencia, jdoInheritedFieldCount + 4, paramConceptoDependencia.monto);
  }

  private static final void jdoSetmonto(ConceptoDependencia paramConceptoDependencia, double paramDouble)
  {
    if (paramConceptoDependencia.jdoFlags == 0)
    {
      paramConceptoDependencia.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDependencia.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoDependencia, jdoInheritedFieldCount + 4, paramConceptoDependencia.monto, paramDouble);
  }

  private static final double jdoGetunidades(ConceptoDependencia paramConceptoDependencia)
  {
    if (paramConceptoDependencia.jdoFlags <= 0)
      return paramConceptoDependencia.unidades;
    StateManager localStateManager = paramConceptoDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDependencia.unidades;
    if (localStateManager.isLoaded(paramConceptoDependencia, jdoInheritedFieldCount + 5))
      return paramConceptoDependencia.unidades;
    return localStateManager.getDoubleField(paramConceptoDependencia, jdoInheritedFieldCount + 5, paramConceptoDependencia.unidades);
  }

  private static final void jdoSetunidades(ConceptoDependencia paramConceptoDependencia, double paramDouble)
  {
    if (paramConceptoDependencia.jdoFlags == 0)
    {
      paramConceptoDependencia.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDependencia.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoDependencia, jdoInheritedFieldCount + 5, paramConceptoDependencia.unidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}