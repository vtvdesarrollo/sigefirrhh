package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class FrecuenciaTipoPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(FrecuenciaTipoPersonalForm.class.getName());
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private int reportId;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showFrecuenciaTipoPersonalByTipoPersonal;
  private boolean showFrecuenciaTipoPersonalByFrecuenciaPago;
  private String findSelectTipoPersonal;
  private String findSelectFrecuenciaPago;
  private Collection findColTipoPersonal;
  private Collection findColFrecuenciaPago;
  private String codTipoPersonal;
  private int codFrecuenciaPago;
  private Collection colTipoPersonal;
  private Collection colFrecuenciaPago;
  private String selectTipoPersonal;
  private String selectFrecuenciaPago;
  private Object stateScrollFrecuenciaTipoPersonalByTipoPersonal = null;
  private Object stateResultFrecuenciaTipoPersonalByTipoPersonal = null;

  private Object stateScrollFrecuenciaTipoPersonalByFrecuenciaPago = null;
  private Object stateResultFrecuenciaTipoPersonalByFrecuenciaPago = null;

  public String getFindSelectTipoPersonal()
  {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectFrecuenciaPago() {
    return this.findSelectFrecuenciaPago;
  }
  public void setFindSelectFrecuenciaPago(String valFrecuenciaPago) {
    this.findSelectFrecuenciaPago = valFrecuenciaPago;
  }

  public Collection getFindColFrecuenciaPago() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColFrecuenciaPago.iterator();
    FrecuenciaPago frecuenciaPago = null;
    while (iterator.hasNext()) {
      frecuenciaPago = (FrecuenciaPago)iterator.next();
      col.add(new SelectItem(
        String.valueOf(frecuenciaPago.getIdFrecuenciaPago()), 
        frecuenciaPago.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.frecuenciaTipoPersonal.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.frecuenciaTipoPersonal.setTipoPersonal(
          tipoPersonal);
        this.codTipoPersonal = tipoPersonal.getCodTipoPersonal();
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectFrecuenciaPago() {
    return this.selectFrecuenciaPago;
  }
  public void setSelectFrecuenciaPago(String valFrecuenciaPago) {
    Iterator iterator = this.colFrecuenciaPago.iterator();
    FrecuenciaPago frecuenciaPago = null;
    this.frecuenciaTipoPersonal.setFrecuenciaPago(null);
    while (iterator.hasNext()) {
      frecuenciaPago = (FrecuenciaPago)iterator.next();
      if (String.valueOf(frecuenciaPago.getIdFrecuenciaPago()).equals(
        valFrecuenciaPago)) {
        this.frecuenciaTipoPersonal.setFrecuenciaPago(
          frecuenciaPago);
        this.codFrecuenciaPago = frecuenciaPago.getCodFrecuenciaPago();
      }
    }

    this.selectFrecuenciaPago = valFrecuenciaPago;
  }

  public Collection getResult() {
    return this.result;
  }

  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal() {
    if (this.frecuenciaTipoPersonal == null) {
      this.frecuenciaTipoPersonal = new FrecuenciaTipoPersonal();
    }
    return this.frecuenciaTipoPersonal;
  }

  public FrecuenciaTipoPersonalForm() throws Exception
  {
    newReportId();

    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColFrecuenciaPago()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFrecuenciaPago.iterator();
    FrecuenciaPago frecuenciaPago = null;
    while (iterator.hasNext()) {
      frecuenciaPago = (FrecuenciaPago)iterator.next();
      col.add(new SelectItem(
        String.valueOf(frecuenciaPago.getIdFrecuenciaPago()), 
        frecuenciaPago.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColFrecuenciaPago = 
        this.definicionesFacade.findFrecuenciaPagoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colFrecuenciaPago = 
        this.definicionesFacade.findFrecuenciaPagoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findFrecuenciaTipoPersonalByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showFrecuenciaTipoPersonalByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showFrecuenciaTipoPersonalByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findSelectFrecuenciaPago = null;

    return null;
  }

  public String findFrecuenciaTipoPersonalByFrecuenciaPago()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findFrecuenciaTipoPersonalByFrecuenciaPago(Long.valueOf(this.findSelectFrecuenciaPago).longValue());
      this.showFrecuenciaTipoPersonalByFrecuenciaPago = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showFrecuenciaTipoPersonalByFrecuenciaPago)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findSelectFrecuenciaPago = null;

    return null;
  }

  public boolean isShowFrecuenciaTipoPersonalByTipoPersonal() {
    return this.showFrecuenciaTipoPersonalByTipoPersonal;
  }
  public boolean isShowFrecuenciaTipoPersonalByFrecuenciaPago() {
    return this.showFrecuenciaTipoPersonalByFrecuenciaPago;
  }

  public String selectFrecuenciaTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectFrecuenciaPago = null;

    long idFrecuenciaTipoPersonal = 
      Long.parseLong((String)requestParameterMap.get("idFrecuenciaTipoPersonal"));
    try
    {
      this.frecuenciaTipoPersonal = 
        this.definicionesFacade.findFrecuenciaTipoPersonalById(
        idFrecuenciaTipoPersonal);
      if (this.frecuenciaTipoPersonal.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.frecuenciaTipoPersonal.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.frecuenciaTipoPersonal.getFrecuenciaPago() != null) {
        this.selectFrecuenciaPago = 
          String.valueOf(this.frecuenciaTipoPersonal.getFrecuenciaPago().getIdFrecuenciaPago());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.frecuenciaTipoPersonal = null;
    this.showFrecuenciaTipoPersonalByTipoPersonal = false;
    this.showFrecuenciaTipoPersonalByFrecuenciaPago = false;
  }

  public String edit()
  {
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    try {
      this.frecuenciaTipoPersonal.setCodTipoPersonal(this.codTipoPersonal);
      this.frecuenciaTipoPersonal.setCodFrecuenciaPago(this.codFrecuenciaPago);
    }
    catch (Exception localException1)
    {
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addFrecuenciaTipoPersonal(
          this.frecuenciaTipoPersonal);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.frecuenciaTipoPersonal);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateFrecuenciaTipoPersonal(
          this.frecuenciaTipoPersonal);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.frecuenciaTipoPersonal);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteFrecuenciaTipoPersonal(
        this.frecuenciaTipoPersonal);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.frecuenciaTipoPersonal);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;
    this.frecuenciaTipoPersonal = new FrecuenciaTipoPersonal();

    this.selectTipoPersonal = null;

    this.selectFrecuenciaPago = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.frecuenciaTipoPersonal.setIdFrecuenciaTipoPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.scrollx = 0;
    this.scrolly = 0;
    resetResult();
    this.frecuenciaTipoPersonal = new FrecuenciaTipoPersonal();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("FrecuenciaTipoPersonal");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "FrecuenciaTipoPersonal" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }
  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }

  public LoginSession getLogin() {
    return this.login;
  }
}