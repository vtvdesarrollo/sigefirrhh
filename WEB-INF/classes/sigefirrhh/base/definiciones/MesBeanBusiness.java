package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class MesBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addMes(Mes mes)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Mes mesNew = 
      (Mes)BeanUtils.cloneBean(
      mes);

    pm.makePersistent(mesNew);
  }

  public void updateMes(Mes mes) throws Exception
  {
    Mes mesModify = 
      findMesById(mes.getIdMes());

    BeanUtils.copyProperties(mesModify, mes);
  }

  public void deleteMes(Mes mes) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Mes mesDelete = 
      findMesById(mes.getIdMes());
    pm.deletePersistent(mesDelete);
  }

  public Mes findMesById(long idMes) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idMes == pIdMes";
    Query query = pm.newQuery(Mes.class, filter);

    query.declareParameters("long pIdMes");

    parameters.put("pIdMes", new Long(idMes));

    Collection colMes = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colMes.iterator();
    return (Mes)iterator.next();
  }

  public Collection findMesAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent mesExtent = pm.getExtent(
      Mes.class, true);
    Query query = pm.newQuery(mesExtent);
    query.setOrdering("mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(Mes.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("mes ascending");

    Collection colMes = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMes);

    return colMes;
  }
}