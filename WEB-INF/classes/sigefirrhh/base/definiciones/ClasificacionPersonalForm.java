package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ClasificacionPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ClasificacionPersonalForm.class.getName());
  private ClasificacionPersonal clasificacionPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showClasificacionPersonalByCategoriaPersonal;
  private boolean showClasificacionPersonalByRelacionPersonal;
  private String findSelectCategoriaPersonal;
  private String findSelectRelacionPersonal;
  private Collection findColCategoriaPersonal;
  private Collection findColRelacionPersonal;
  private Collection colCategoriaPersonal;
  private Collection colRelacionPersonal;
  private Collection colCategoriaPresupuesto;
  private String selectCategoriaPersonal;
  private String selectRelacionPersonal;
  private String selectCategoriaPresupuesto;
  private Object stateResultClasificacionPersonalByCategoriaPersonal = null;

  private Object stateResultClasificacionPersonalByRelacionPersonal = null;

  public String getFindSelectCategoriaPersonal()
  {
    return this.findSelectCategoriaPersonal;
  }
  public void setFindSelectCategoriaPersonal(String valCategoriaPersonal) {
    this.findSelectCategoriaPersonal = valCategoriaPersonal;
  }

  public Collection getFindColCategoriaPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCategoriaPersonal.iterator();
    CategoriaPersonal categoriaPersonal = null;
    while (iterator.hasNext()) {
      categoriaPersonal = (CategoriaPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(categoriaPersonal.getIdCategoriaPersonal()), 
        categoriaPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectRelacionPersonal() {
    return this.findSelectRelacionPersonal;
  }
  public void setFindSelectRelacionPersonal(String valRelacionPersonal) {
    this.findSelectRelacionPersonal = valRelacionPersonal;
  }

  public Collection getFindColRelacionPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRelacionPersonal.iterator();
    RelacionPersonal relacionPersonal = null;
    while (iterator.hasNext()) {
      relacionPersonal = (RelacionPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(relacionPersonal.getIdRelacionPersonal()), 
        relacionPersonal.toString()));
    }
    return col;
  }

  public String getSelectCategoriaPersonal()
  {
    return this.selectCategoriaPersonal;
  }
  public void setSelectCategoriaPersonal(String valCategoriaPersonal) {
    Iterator iterator = this.colCategoriaPersonal.iterator();
    CategoriaPersonal categoriaPersonal = null;
    this.clasificacionPersonal.setCategoriaPersonal(null);
    while (iterator.hasNext()) {
      categoriaPersonal = (CategoriaPersonal)iterator.next();
      if (String.valueOf(categoriaPersonal.getIdCategoriaPersonal()).equals(
        valCategoriaPersonal)) {
        this.clasificacionPersonal.setCategoriaPersonal(
          categoriaPersonal);
        break;
      }
    }
    this.selectCategoriaPersonal = valCategoriaPersonal;
  }
  public String getSelectRelacionPersonal() {
    return this.selectRelacionPersonal;
  }
  public void setSelectRelacionPersonal(String valRelacionPersonal) {
    Iterator iterator = this.colRelacionPersonal.iterator();
    RelacionPersonal relacionPersonal = null;
    this.clasificacionPersonal.setRelacionPersonal(null);
    while (iterator.hasNext()) {
      relacionPersonal = (RelacionPersonal)iterator.next();
      if (String.valueOf(relacionPersonal.getIdRelacionPersonal()).equals(
        valRelacionPersonal)) {
        this.clasificacionPersonal.setRelacionPersonal(
          relacionPersonal);
        break;
      }
    }
    this.selectRelacionPersonal = valRelacionPersonal;
  }
  public String getSelectCategoriaPresupuesto() {
    return this.selectCategoriaPresupuesto;
  }
  public void setSelectCategoriaPresupuesto(String valCategoriaPresupuesto) {
    Iterator iterator = this.colCategoriaPresupuesto.iterator();
    CategoriaPresupuesto categoriaPresupuesto = null;
    this.clasificacionPersonal.setCategoriaPresupuesto(null);
    while (iterator.hasNext()) {
      categoriaPresupuesto = (CategoriaPresupuesto)iterator.next();
      if (String.valueOf(categoriaPresupuesto.getIdCategoriaPresupuesto()).equals(
        valCategoriaPresupuesto)) {
        this.clasificacionPersonal.setCategoriaPresupuesto(
          categoriaPresupuesto);
        break;
      }
    }
    this.selectCategoriaPresupuesto = valCategoriaPresupuesto;
  }
  public Collection getResult() {
    return this.result;
  }

  public ClasificacionPersonal getClasificacionPersonal() {
    if (this.clasificacionPersonal == null) {
      this.clasificacionPersonal = new ClasificacionPersonal();
    }
    return this.clasificacionPersonal;
  }

  public ClasificacionPersonalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColCategoriaPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCategoriaPersonal.iterator();
    CategoriaPersonal categoriaPersonal = null;
    while (iterator.hasNext()) {
      categoriaPersonal = (CategoriaPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(categoriaPersonal.getIdCategoriaPersonal()), 
        categoriaPersonal.toString()));
    }
    return col;
  }

  public Collection getColRelacionPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRelacionPersonal.iterator();
    RelacionPersonal relacionPersonal = null;
    while (iterator.hasNext()) {
      relacionPersonal = (RelacionPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(relacionPersonal.getIdRelacionPersonal()), 
        relacionPersonal.toString()));
    }
    return col;
  }

  public Collection getColCategoriaPresupuesto()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCategoriaPresupuesto.iterator();
    CategoriaPresupuesto categoriaPresupuesto = null;
    while (iterator.hasNext()) {
      categoriaPresupuesto = (CategoriaPresupuesto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(categoriaPresupuesto.getIdCategoriaPresupuesto()), 
        categoriaPresupuesto.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColCategoriaPersonal = 
        this.definicionesFacade.findAllCategoriaPersonal();
      this.findColRelacionPersonal = 
        this.definicionesFacade.findAllRelacionPersonal();

      this.colCategoriaPersonal = 
        this.definicionesFacade.findAllCategoriaPersonal();
      this.colRelacionPersonal = 
        this.definicionesFacade.findAllRelacionPersonal();
      this.colCategoriaPresupuesto = 
        this.definicionesFacade.findAllCategoriaPresupuesto();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findClasificacionPersonalByCategoriaPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findClasificacionPersonalByCategoriaPersonal(Long.valueOf(this.findSelectCategoriaPersonal).longValue());
      this.showClasificacionPersonalByCategoriaPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showClasificacionPersonalByCategoriaPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectCategoriaPersonal = null;
    this.findSelectRelacionPersonal = null;

    return null;
  }

  public String findClasificacionPersonalByRelacionPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findClasificacionPersonalByRelacionPersonal(Long.valueOf(this.findSelectRelacionPersonal).longValue());
      this.showClasificacionPersonalByRelacionPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showClasificacionPersonalByRelacionPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectCategoriaPersonal = null;
    this.findSelectRelacionPersonal = null;

    return null;
  }

  public boolean isShowClasificacionPersonalByCategoriaPersonal() {
    return this.showClasificacionPersonalByCategoriaPersonal;
  }
  public boolean isShowClasificacionPersonalByRelacionPersonal() {
    return this.showClasificacionPersonalByRelacionPersonal;
  }

  public String selectClasificacionPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectCategoriaPersonal = null;
    this.selectRelacionPersonal = null;
    this.selectCategoriaPresupuesto = null;

    long idClasificacionPersonal = 
      Long.parseLong((String)requestParameterMap.get("idClasificacionPersonal"));
    try
    {
      this.clasificacionPersonal = 
        this.definicionesFacade.findClasificacionPersonalById(
        idClasificacionPersonal);
      if (this.clasificacionPersonal.getCategoriaPersonal() != null) {
        this.selectCategoriaPersonal = 
          String.valueOf(this.clasificacionPersonal.getCategoriaPersonal().getIdCategoriaPersonal());
      }
      if (this.clasificacionPersonal.getRelacionPersonal() != null) {
        this.selectRelacionPersonal = 
          String.valueOf(this.clasificacionPersonal.getRelacionPersonal().getIdRelacionPersonal());
      }
      if (this.clasificacionPersonal.getCategoriaPresupuesto() != null) {
        this.selectCategoriaPresupuesto = 
          String.valueOf(this.clasificacionPersonal.getCategoriaPresupuesto().getIdCategoriaPresupuesto());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.clasificacionPersonal = null;
    this.showClasificacionPersonalByCategoriaPersonal = false;
    this.showClasificacionPersonalByRelacionPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addClasificacionPersonal(
          this.clasificacionPersonal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateClasificacionPersonal(
          this.clasificacionPersonal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteClasificacionPersonal(
        this.clasificacionPersonal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.clasificacionPersonal = new ClasificacionPersonal();

    this.selectCategoriaPersonal = null;

    this.selectRelacionPersonal = null;

    this.selectCategoriaPresupuesto = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.clasificacionPersonal.setIdClasificacionPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.ClasificacionPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.clasificacionPersonal = new ClasificacionPersonal();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("ClasificacionPersonal");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "ClasificacionPersonal" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}