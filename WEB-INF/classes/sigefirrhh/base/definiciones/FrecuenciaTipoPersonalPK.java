package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class FrecuenciaTipoPersonalPK
  implements Serializable
{
  public long idFrecuenciaTipoPersonal;

  public FrecuenciaTipoPersonalPK()
  {
  }

  public FrecuenciaTipoPersonalPK(long idFrecuenciaTipoPersonal)
  {
    this.idFrecuenciaTipoPersonal = idFrecuenciaTipoPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((FrecuenciaTipoPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(FrecuenciaTipoPersonalPK thatPK)
  {
    return 
      this.idFrecuenciaTipoPersonal == thatPK.idFrecuenciaTipoPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idFrecuenciaTipoPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idFrecuenciaTipoPersonal);
  }
}