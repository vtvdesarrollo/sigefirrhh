package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SemanaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSemana(Semana semana)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Semana semanaNew = 
      (Semana)BeanUtils.cloneBean(
      semana);

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (semanaNew.getGrupoNomina() != null) {
      semanaNew.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        semanaNew.getGrupoNomina().getIdGrupoNomina()));
    }
    pm.makePersistent(semanaNew);
  }

  public void updateSemana(Semana semana) throws Exception
  {
    Semana semanaModify = 
      findSemanaById(semana.getIdSemana());

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (semana.getGrupoNomina() != null) {
      semana.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        semana.getGrupoNomina().getIdGrupoNomina()));
    }

    BeanUtils.copyProperties(semanaModify, semana);
  }

  public void deleteSemana(Semana semana) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Semana semanaDelete = 
      findSemanaById(semana.getIdSemana());
    pm.deletePersistent(semanaDelete);
  }

  public Semana findSemanaById(long idSemana) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSemana == pIdSemana";
    Query query = pm.newQuery(Semana.class, filter);

    query.declareParameters("long pIdSemana");

    parameters.put("pIdSemana", new Long(idSemana));

    Collection colSemana = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSemana.iterator();
    return (Semana)iterator.next();
  }

  public Collection findSemanaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent semanaExtent = pm.getExtent(
      Semana.class, true);
    Query query = pm.newQuery(semanaExtent);
    query.setOrdering("anio ascending, semanaAnio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina";

    Query query = pm.newQuery(Semana.class, filter);

    query.declareParameters("long pIdGrupoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));

    query.setOrdering("anio ascending, semanaAnio ascending");

    Collection colSemana = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSemana);

    return colSemana;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(Semana.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, semanaAnio ascending");

    Collection colSemana = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSemana);

    return colSemana;
  }

  public Semana findByAnioSemanaAnio(int anio, int semanaAnio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    HashMap parameters = new HashMap();

    String filter = "anio == pAnio && semanaAnio == pSemanaAnio";

    Query query = pm.newQuery(Semana.class, filter);

    query.declareParameters("long pAnio, long pSemanaAnio");

    parameters.put("pAnio", new Long(anio));
    parameters.put("pSemanaAnio", new Long(semanaAnio));

    query.setOrdering("anio ascending, semanaAnio ascending");

    Collection colSemana = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSemana.iterator();
    return (Semana)iterator.next();
  }
}