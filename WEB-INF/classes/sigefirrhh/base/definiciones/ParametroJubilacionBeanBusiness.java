package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ParametroJubilacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParametroJubilacion(ParametroJubilacion parametroJubilacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ParametroJubilacion parametroJubilacionNew = 
      (ParametroJubilacion)BeanUtils.cloneBean(
      parametroJubilacion);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroJubilacionNew.getTipoPersonal() != null) {
      parametroJubilacionNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroJubilacionNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(parametroJubilacionNew);
  }

  public void updateParametroJubilacion(ParametroJubilacion parametroJubilacion) throws Exception
  {
    ParametroJubilacion parametroJubilacionModify = 
      findParametroJubilacionById(parametroJubilacion.getIdParametroJubilacion());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (parametroJubilacion.getTipoPersonal() != null) {
      parametroJubilacion.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        parametroJubilacion.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(parametroJubilacionModify, parametroJubilacion);
  }

  public void deleteParametroJubilacion(ParametroJubilacion parametroJubilacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ParametroJubilacion parametroJubilacionDelete = 
      findParametroJubilacionById(parametroJubilacion.getIdParametroJubilacion());
    pm.deletePersistent(parametroJubilacionDelete);
  }

  public ParametroJubilacion findParametroJubilacionById(long idParametroJubilacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParametroJubilacion == pIdParametroJubilacion";
    Query query = pm.newQuery(ParametroJubilacion.class, filter);

    query.declareParameters("long pIdParametroJubilacion");

    parameters.put("pIdParametroJubilacion", new Long(idParametroJubilacion));

    Collection colParametroJubilacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParametroJubilacion.iterator();
    return (ParametroJubilacion)iterator.next();
  }

  public Collection findParametroJubilacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parametroJubilacionExtent = pm.getExtent(
      ParametroJubilacion.class, true);
    Query query = pm.newQuery(parametroJubilacionExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ParametroJubilacion.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colParametroJubilacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParametroJubilacion);

    return colParametroJubilacion;
  }
}