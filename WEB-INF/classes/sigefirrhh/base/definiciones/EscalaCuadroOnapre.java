package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class EscalaCuadroOnapre
  implements Serializable, PersistenceCapable
{
  private long idEscalaCuadroOnapre;
  private String codigo;
  private double desde;
  private double hasta;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codigo", "desde", "hasta", "idEscalaCuadroOnapre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    DecimalFormat a = new DecimalFormat();
    a.applyPattern("##,###,###.00");
    String b = a.format(jdoGetdesde(this));
    DecimalFormat c = new DecimalFormat();
    c.applyPattern("##,###,###.00");
    String d = c.format(jdoGethasta(this));

    return "Tarifa " + jdoGetcodigo(this) + " - Desde " + 
      b + " - Hasta " + d;
  }

  public String getCodigo()
  {
    return jdoGetcodigo(this);
  }
  public void setCodigo(String codigo) {
    jdoSetcodigo(this, codigo);
  }
  public double getDesde() {
    return jdoGetdesde(this);
  }
  public void setDesde(double desde) {
    jdoSetdesde(this, desde);
  }
  public double getHasta() {
    return jdoGethasta(this);
  }
  public void setHasta(double hasta) {
    jdoSethasta(this, hasta);
  }
  public long getIdEscalaCuadroOnapre() {
    return jdoGetidEscalaCuadroOnapre(this);
  }
  public void setIdEscalaCuadroOnapre(long idEscalaCuadroOnapre) {
    jdoSetidEscalaCuadroOnapre(this, idEscalaCuadroOnapre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.EscalaCuadroOnapre"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new EscalaCuadroOnapre());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    EscalaCuadroOnapre localEscalaCuadroOnapre = new EscalaCuadroOnapre();
    localEscalaCuadroOnapre.jdoFlags = 1;
    localEscalaCuadroOnapre.jdoStateManager = paramStateManager;
    return localEscalaCuadroOnapre;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    EscalaCuadroOnapre localEscalaCuadroOnapre = new EscalaCuadroOnapre();
    localEscalaCuadroOnapre.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEscalaCuadroOnapre.jdoFlags = 1;
    localEscalaCuadroOnapre.jdoStateManager = paramStateManager;
    return localEscalaCuadroOnapre;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.desde);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.hasta);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEscalaCuadroOnapre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.desde = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.hasta = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEscalaCuadroOnapre = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(EscalaCuadroOnapre paramEscalaCuadroOnapre, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEscalaCuadroOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.codigo = paramEscalaCuadroOnapre.codigo;
      return;
    case 1:
      if (paramEscalaCuadroOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.desde = paramEscalaCuadroOnapre.desde;
      return;
    case 2:
      if (paramEscalaCuadroOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.hasta = paramEscalaCuadroOnapre.hasta;
      return;
    case 3:
      if (paramEscalaCuadroOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.idEscalaCuadroOnapre = paramEscalaCuadroOnapre.idEscalaCuadroOnapre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof EscalaCuadroOnapre))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    EscalaCuadroOnapre localEscalaCuadroOnapre = (EscalaCuadroOnapre)paramObject;
    if (localEscalaCuadroOnapre.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEscalaCuadroOnapre, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EscalaCuadroOnaprePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EscalaCuadroOnaprePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EscalaCuadroOnaprePK))
      throw new IllegalArgumentException("arg1");
    EscalaCuadroOnaprePK localEscalaCuadroOnaprePK = (EscalaCuadroOnaprePK)paramObject;
    localEscalaCuadroOnaprePK.idEscalaCuadroOnapre = this.idEscalaCuadroOnapre;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EscalaCuadroOnaprePK))
      throw new IllegalArgumentException("arg1");
    EscalaCuadroOnaprePK localEscalaCuadroOnaprePK = (EscalaCuadroOnaprePK)paramObject;
    this.idEscalaCuadroOnapre = localEscalaCuadroOnaprePK.idEscalaCuadroOnapre;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EscalaCuadroOnaprePK))
      throw new IllegalArgumentException("arg2");
    EscalaCuadroOnaprePK localEscalaCuadroOnaprePK = (EscalaCuadroOnaprePK)paramObject;
    localEscalaCuadroOnaprePK.idEscalaCuadroOnapre = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EscalaCuadroOnaprePK))
      throw new IllegalArgumentException("arg2");
    EscalaCuadroOnaprePK localEscalaCuadroOnaprePK = (EscalaCuadroOnaprePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localEscalaCuadroOnaprePK.idEscalaCuadroOnapre);
  }

  private static final String jdoGetcodigo(EscalaCuadroOnapre paramEscalaCuadroOnapre)
  {
    if (paramEscalaCuadroOnapre.jdoFlags <= 0)
      return paramEscalaCuadroOnapre.codigo;
    StateManager localStateManager = paramEscalaCuadroOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramEscalaCuadroOnapre.codigo;
    if (localStateManager.isLoaded(paramEscalaCuadroOnapre, jdoInheritedFieldCount + 0))
      return paramEscalaCuadroOnapre.codigo;
    return localStateManager.getStringField(paramEscalaCuadroOnapre, jdoInheritedFieldCount + 0, paramEscalaCuadroOnapre.codigo);
  }

  private static final void jdoSetcodigo(EscalaCuadroOnapre paramEscalaCuadroOnapre, String paramString)
  {
    if (paramEscalaCuadroOnapre.jdoFlags == 0)
    {
      paramEscalaCuadroOnapre.codigo = paramString;
      return;
    }
    StateManager localStateManager = paramEscalaCuadroOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramEscalaCuadroOnapre.codigo = paramString;
      return;
    }
    localStateManager.setStringField(paramEscalaCuadroOnapre, jdoInheritedFieldCount + 0, paramEscalaCuadroOnapre.codigo, paramString);
  }

  private static final double jdoGetdesde(EscalaCuadroOnapre paramEscalaCuadroOnapre)
  {
    if (paramEscalaCuadroOnapre.jdoFlags <= 0)
      return paramEscalaCuadroOnapre.desde;
    StateManager localStateManager = paramEscalaCuadroOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramEscalaCuadroOnapre.desde;
    if (localStateManager.isLoaded(paramEscalaCuadroOnapre, jdoInheritedFieldCount + 1))
      return paramEscalaCuadroOnapre.desde;
    return localStateManager.getDoubleField(paramEscalaCuadroOnapre, jdoInheritedFieldCount + 1, paramEscalaCuadroOnapre.desde);
  }

  private static final void jdoSetdesde(EscalaCuadroOnapre paramEscalaCuadroOnapre, double paramDouble)
  {
    if (paramEscalaCuadroOnapre.jdoFlags == 0)
    {
      paramEscalaCuadroOnapre.desde = paramDouble;
      return;
    }
    StateManager localStateManager = paramEscalaCuadroOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramEscalaCuadroOnapre.desde = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramEscalaCuadroOnapre, jdoInheritedFieldCount + 1, paramEscalaCuadroOnapre.desde, paramDouble);
  }

  private static final double jdoGethasta(EscalaCuadroOnapre paramEscalaCuadroOnapre)
  {
    if (paramEscalaCuadroOnapre.jdoFlags <= 0)
      return paramEscalaCuadroOnapre.hasta;
    StateManager localStateManager = paramEscalaCuadroOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramEscalaCuadroOnapre.hasta;
    if (localStateManager.isLoaded(paramEscalaCuadroOnapre, jdoInheritedFieldCount + 2))
      return paramEscalaCuadroOnapre.hasta;
    return localStateManager.getDoubleField(paramEscalaCuadroOnapre, jdoInheritedFieldCount + 2, paramEscalaCuadroOnapre.hasta);
  }

  private static final void jdoSethasta(EscalaCuadroOnapre paramEscalaCuadroOnapre, double paramDouble)
  {
    if (paramEscalaCuadroOnapre.jdoFlags == 0)
    {
      paramEscalaCuadroOnapre.hasta = paramDouble;
      return;
    }
    StateManager localStateManager = paramEscalaCuadroOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramEscalaCuadroOnapre.hasta = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramEscalaCuadroOnapre, jdoInheritedFieldCount + 2, paramEscalaCuadroOnapre.hasta, paramDouble);
  }

  private static final long jdoGetidEscalaCuadroOnapre(EscalaCuadroOnapre paramEscalaCuadroOnapre)
  {
    return paramEscalaCuadroOnapre.idEscalaCuadroOnapre;
  }

  private static final void jdoSetidEscalaCuadroOnapre(EscalaCuadroOnapre paramEscalaCuadroOnapre, long paramLong)
  {
    StateManager localStateManager = paramEscalaCuadroOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramEscalaCuadroOnapre.idEscalaCuadroOnapre = paramLong;
      return;
    }
    localStateManager.setLongField(paramEscalaCuadroOnapre, jdoInheritedFieldCount + 3, paramEscalaCuadroOnapre.idEscalaCuadroOnapre, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}