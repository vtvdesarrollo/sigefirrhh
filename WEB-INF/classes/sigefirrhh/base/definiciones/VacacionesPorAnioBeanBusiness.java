package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class VacacionesPorAnioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addVacacionesPorAnio(VacacionesPorAnio vacacionesPorAnio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    VacacionesPorAnio vacacionesPorAnioNew = 
      (VacacionesPorAnio)BeanUtils.cloneBean(
      vacacionesPorAnio);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (vacacionesPorAnioNew.getTipoPersonal() != null) {
      vacacionesPorAnioNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        vacacionesPorAnioNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(vacacionesPorAnioNew);
  }

  public void updateVacacionesPorAnio(VacacionesPorAnio vacacionesPorAnio) throws Exception
  {
    VacacionesPorAnio vacacionesPorAnioModify = 
      findVacacionesPorAnioById(vacacionesPorAnio.getIdVacacionesPorAnio());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (vacacionesPorAnio.getTipoPersonal() != null) {
      vacacionesPorAnio.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        vacacionesPorAnio.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(vacacionesPorAnioModify, vacacionesPorAnio);
  }

  public void deleteVacacionesPorAnio(VacacionesPorAnio vacacionesPorAnio) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    VacacionesPorAnio vacacionesPorAnioDelete = 
      findVacacionesPorAnioById(vacacionesPorAnio.getIdVacacionesPorAnio());
    pm.deletePersistent(vacacionesPorAnioDelete);
  }

  public VacacionesPorAnio findVacacionesPorAnioById(long idVacacionesPorAnio) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idVacacionesPorAnio == pIdVacacionesPorAnio";
    Query query = pm.newQuery(VacacionesPorAnio.class, filter);

    query.declareParameters("long pIdVacacionesPorAnio");

    parameters.put("pIdVacacionesPorAnio", new Long(idVacacionesPorAnio));

    Collection colVacacionesPorAnio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colVacacionesPorAnio.iterator();
    return (VacacionesPorAnio)iterator.next();
  }

  public Collection findVacacionesPorAnioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent vacacionesPorAnioExtent = pm.getExtent(
      VacacionesPorAnio.class, true);
    Query query = pm.newQuery(vacacionesPorAnioExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(VacacionesPorAnio.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colVacacionesPorAnio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colVacacionesPorAnio);

    return colVacacionesPorAnio;
  }
}