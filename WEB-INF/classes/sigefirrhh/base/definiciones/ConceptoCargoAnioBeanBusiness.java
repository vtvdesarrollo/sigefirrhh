package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;

public class ConceptoCargoAnioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoCargoAnio(ConceptoCargoAnio conceptoCargoAnio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoCargoAnio conceptoCargoAnioNew = 
      (ConceptoCargoAnio)BeanUtils.cloneBean(
      conceptoCargoAnio);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoCargoAnioNew.getConceptoTipoPersonal() != null) {
      conceptoCargoAnioNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoCargoAnioNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (conceptoCargoAnioNew.getCargo() != null) {
      conceptoCargoAnioNew.setCargo(
        cargoBeanBusiness.findCargoById(
        conceptoCargoAnioNew.getCargo().getIdCargo()));
    }
    pm.makePersistent(conceptoCargoAnioNew);
  }

  public void updateConceptoCargoAnio(ConceptoCargoAnio conceptoCargoAnio) throws Exception
  {
    ConceptoCargoAnio conceptoCargoAnioModify = 
      findConceptoCargoAnioById(conceptoCargoAnio.getIdConceptoCargoAnio());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoCargoAnio.getConceptoTipoPersonal() != null) {
      conceptoCargoAnio.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoCargoAnio.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (conceptoCargoAnio.getCargo() != null) {
      conceptoCargoAnio.setCargo(
        cargoBeanBusiness.findCargoById(
        conceptoCargoAnio.getCargo().getIdCargo()));
    }

    BeanUtils.copyProperties(conceptoCargoAnioModify, conceptoCargoAnio);
  }

  public void deleteConceptoCargoAnio(ConceptoCargoAnio conceptoCargoAnio) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoCargoAnio conceptoCargoAnioDelete = 
      findConceptoCargoAnioById(conceptoCargoAnio.getIdConceptoCargoAnio());
    pm.deletePersistent(conceptoCargoAnioDelete);
  }

  public ConceptoCargoAnio findConceptoCargoAnioById(long idConceptoCargoAnio) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoCargoAnio == pIdConceptoCargoAnio";
    Query query = pm.newQuery(ConceptoCargoAnio.class, filter);

    query.declareParameters("long pIdConceptoCargoAnio");

    parameters.put("pIdConceptoCargoAnio", new Long(idConceptoCargoAnio));

    Collection colConceptoCargoAnio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoCargoAnio.iterator();
    return (ConceptoCargoAnio)iterator.next();
  }

  public Collection findConceptoCargoAnioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoCargoAnioExtent = pm.getExtent(
      ConceptoCargoAnio.class, true);
    Query query = pm.newQuery(conceptoCargoAnioExtent);
    query.setOrdering("conceptoTipoPersonal.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal";

    Query query = pm.newQuery(ConceptoCargoAnio.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    query.setOrdering("conceptoTipoPersonal.codConcepto ascending");

    Collection colConceptoCargoAnio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoCargoAnio);

    return colConceptoCargoAnio;
  }

  public Collection findByCargo(long idCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cargo.idCargo == pIdCargo";

    Query query = pm.newQuery(ConceptoCargoAnio.class, filter);

    query.declareParameters("long pIdCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdCargo", new Long(idCargo));

    query.setOrdering("conceptoTipoPersonal.codConcepto ascending");

    Collection colConceptoCargoAnio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoCargoAnio);

    return colConceptoCargoAnio;
  }
}