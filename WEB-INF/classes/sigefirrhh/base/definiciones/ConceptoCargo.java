package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;

public class ConceptoCargo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idConceptoCargo;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private Cargo cargo;
  private String excluir;
  private String automaticoIngreso;
  private double unidades;
  private double monto;
  private double porcentaje;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "automaticoIngreso", "cargo", "conceptoTipoPersonal", "excluir", "idConceptoCargo", "idSitp", "monto", "porcentaje", "tiempoSitp", "unidades" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.util.Date"), Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 26, 26, 21, 24, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoCargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoCargo());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public ConceptoCargo()
  {
    jdoSetexcluir(this, "N");

    jdoSetautomaticoIngreso(this, "N");

    jdoSetunidades(this, 0.0D);

    jdoSetmonto(this, 0.0D);

    jdoSetporcentaje(this, 0.0D);
  }

  public String toString()
  {
    return jdoGetcargo(this).getDescripcionCargo() + " " + jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion();
  }

  public String getAutomaticoIngreso() {
    return jdoGetautomaticoIngreso(this);
  }
  public void setAutomaticoIngreso(String automaticoIngreso) {
    jdoSetautomaticoIngreso(this, automaticoIngreso);
  }
  public Cargo getCargo() {
    return jdoGetcargo(this);
  }
  public void setCargo(Cargo cargo) {
    jdoSetcargo(this, cargo);
  }
  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public String getExcluir() {
    return jdoGetexcluir(this);
  }
  public void setExcluir(String excluir) {
    jdoSetexcluir(this, excluir);
  }
  public long getIdConceptoCargo() {
    return jdoGetidConceptoCargo(this);
  }
  public void setIdConceptoCargo(long idConceptoCargo) {
    jdoSetidConceptoCargo(this, idConceptoCargo);
  }
  public int getIdSitp() {
    return jdoGetidSitp(this);
  }
  public void setIdSitp(int idSitp) {
    jdoSetidSitp(this, idSitp);
  }
  public double getMonto() {
    return jdoGetmonto(this);
  }
  public void setMonto(double monto) {
    jdoSetmonto(this, monto);
  }
  public double getPorcentaje() {
    return jdoGetporcentaje(this);
  }
  public void setPorcentaje(double porcentaje) {
    jdoSetporcentaje(this, porcentaje);
  }
  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }
  public void setTiempoSitp(Date tiempoSitp) {
    jdoSettiempoSitp(this, tiempoSitp);
  }
  public double getUnidades() {
    return jdoGetunidades(this);
  }
  public void setUnidades(double unidades) {
    jdoSetunidades(this, unidades);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoCargo localConceptoCargo = new ConceptoCargo();
    localConceptoCargo.jdoFlags = 1;
    localConceptoCargo.jdoStateManager = paramStateManager;
    return localConceptoCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoCargo localConceptoCargo = new ConceptoCargo();
    localConceptoCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoCargo.jdoFlags = 1;
    localConceptoCargo.jdoStateManager = paramStateManager;
    return localConceptoCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.automaticoIngreso);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.excluir);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoCargo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.automaticoIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.excluir = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoCargo paramConceptoCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.automaticoIngreso = paramConceptoCargo.automaticoIngreso;
      return;
    case 1:
      if (paramConceptoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramConceptoCargo.cargo;
      return;
    case 2:
      if (paramConceptoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoCargo.conceptoTipoPersonal;
      return;
    case 3:
      if (paramConceptoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.excluir = paramConceptoCargo.excluir;
      return;
    case 4:
      if (paramConceptoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoCargo = paramConceptoCargo.idConceptoCargo;
      return;
    case 5:
      if (paramConceptoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramConceptoCargo.idSitp;
      return;
    case 6:
      if (paramConceptoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramConceptoCargo.monto;
      return;
    case 7:
      if (paramConceptoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramConceptoCargo.porcentaje;
      return;
    case 8:
      if (paramConceptoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramConceptoCargo.tiempoSitp;
      return;
    case 9:
      if (paramConceptoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramConceptoCargo.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoCargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoCargo localConceptoCargo = (ConceptoCargo)paramObject;
    if (localConceptoCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoCargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoCargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoCargoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoCargoPK localConceptoCargoPK = (ConceptoCargoPK)paramObject;
    localConceptoCargoPK.idConceptoCargo = this.idConceptoCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoCargoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoCargoPK localConceptoCargoPK = (ConceptoCargoPK)paramObject;
    this.idConceptoCargo = localConceptoCargoPK.idConceptoCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoCargoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoCargoPK localConceptoCargoPK = (ConceptoCargoPK)paramObject;
    localConceptoCargoPK.idConceptoCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoCargoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoCargoPK localConceptoCargoPK = (ConceptoCargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localConceptoCargoPK.idConceptoCargo);
  }

  private static final String jdoGetautomaticoIngreso(ConceptoCargo paramConceptoCargo)
  {
    if (paramConceptoCargo.jdoFlags <= 0)
      return paramConceptoCargo.automaticoIngreso;
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargo.automaticoIngreso;
    if (localStateManager.isLoaded(paramConceptoCargo, jdoInheritedFieldCount + 0))
      return paramConceptoCargo.automaticoIngreso;
    return localStateManager.getStringField(paramConceptoCargo, jdoInheritedFieldCount + 0, paramConceptoCargo.automaticoIngreso);
  }

  private static final void jdoSetautomaticoIngreso(ConceptoCargo paramConceptoCargo, String paramString)
  {
    if (paramConceptoCargo.jdoFlags == 0)
    {
      paramConceptoCargo.automaticoIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargo.automaticoIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoCargo, jdoInheritedFieldCount + 0, paramConceptoCargo.automaticoIngreso, paramString);
  }

  private static final Cargo jdoGetcargo(ConceptoCargo paramConceptoCargo)
  {
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargo.cargo;
    if (localStateManager.isLoaded(paramConceptoCargo, jdoInheritedFieldCount + 1))
      return paramConceptoCargo.cargo;
    return (Cargo)localStateManager.getObjectField(paramConceptoCargo, jdoInheritedFieldCount + 1, paramConceptoCargo.cargo);
  }

  private static final void jdoSetcargo(ConceptoCargo paramConceptoCargo, Cargo paramCargo)
  {
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargo.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramConceptoCargo, jdoInheritedFieldCount + 1, paramConceptoCargo.cargo, paramCargo);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoCargo paramConceptoCargo)
  {
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargo.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoCargo, jdoInheritedFieldCount + 2))
      return paramConceptoCargo.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoCargo, jdoInheritedFieldCount + 2, paramConceptoCargo.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoCargo paramConceptoCargo, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargo.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoCargo, jdoInheritedFieldCount + 2, paramConceptoCargo.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGetexcluir(ConceptoCargo paramConceptoCargo)
  {
    if (paramConceptoCargo.jdoFlags <= 0)
      return paramConceptoCargo.excluir;
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargo.excluir;
    if (localStateManager.isLoaded(paramConceptoCargo, jdoInheritedFieldCount + 3))
      return paramConceptoCargo.excluir;
    return localStateManager.getStringField(paramConceptoCargo, jdoInheritedFieldCount + 3, paramConceptoCargo.excluir);
  }

  private static final void jdoSetexcluir(ConceptoCargo paramConceptoCargo, String paramString)
  {
    if (paramConceptoCargo.jdoFlags == 0)
    {
      paramConceptoCargo.excluir = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargo.excluir = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoCargo, jdoInheritedFieldCount + 3, paramConceptoCargo.excluir, paramString);
  }

  private static final long jdoGetidConceptoCargo(ConceptoCargo paramConceptoCargo)
  {
    return paramConceptoCargo.idConceptoCargo;
  }

  private static final void jdoSetidConceptoCargo(ConceptoCargo paramConceptoCargo, long paramLong)
  {
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargo.idConceptoCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoCargo, jdoInheritedFieldCount + 4, paramConceptoCargo.idConceptoCargo, paramLong);
  }

  private static final int jdoGetidSitp(ConceptoCargo paramConceptoCargo)
  {
    if (paramConceptoCargo.jdoFlags <= 0)
      return paramConceptoCargo.idSitp;
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargo.idSitp;
    if (localStateManager.isLoaded(paramConceptoCargo, jdoInheritedFieldCount + 5))
      return paramConceptoCargo.idSitp;
    return localStateManager.getIntField(paramConceptoCargo, jdoInheritedFieldCount + 5, paramConceptoCargo.idSitp);
  }

  private static final void jdoSetidSitp(ConceptoCargo paramConceptoCargo, int paramInt)
  {
    if (paramConceptoCargo.jdoFlags == 0)
    {
      paramConceptoCargo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoCargo, jdoInheritedFieldCount + 5, paramConceptoCargo.idSitp, paramInt);
  }

  private static final double jdoGetmonto(ConceptoCargo paramConceptoCargo)
  {
    if (paramConceptoCargo.jdoFlags <= 0)
      return paramConceptoCargo.monto;
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargo.monto;
    if (localStateManager.isLoaded(paramConceptoCargo, jdoInheritedFieldCount + 6))
      return paramConceptoCargo.monto;
    return localStateManager.getDoubleField(paramConceptoCargo, jdoInheritedFieldCount + 6, paramConceptoCargo.monto);
  }

  private static final void jdoSetmonto(ConceptoCargo paramConceptoCargo, double paramDouble)
  {
    if (paramConceptoCargo.jdoFlags == 0)
    {
      paramConceptoCargo.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargo.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoCargo, jdoInheritedFieldCount + 6, paramConceptoCargo.monto, paramDouble);
  }

  private static final double jdoGetporcentaje(ConceptoCargo paramConceptoCargo)
  {
    if (paramConceptoCargo.jdoFlags <= 0)
      return paramConceptoCargo.porcentaje;
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargo.porcentaje;
    if (localStateManager.isLoaded(paramConceptoCargo, jdoInheritedFieldCount + 7))
      return paramConceptoCargo.porcentaje;
    return localStateManager.getDoubleField(paramConceptoCargo, jdoInheritedFieldCount + 7, paramConceptoCargo.porcentaje);
  }

  private static final void jdoSetporcentaje(ConceptoCargo paramConceptoCargo, double paramDouble)
  {
    if (paramConceptoCargo.jdoFlags == 0)
    {
      paramConceptoCargo.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargo.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoCargo, jdoInheritedFieldCount + 7, paramConceptoCargo.porcentaje, paramDouble);
  }

  private static final Date jdoGettiempoSitp(ConceptoCargo paramConceptoCargo)
  {
    if (paramConceptoCargo.jdoFlags <= 0)
      return paramConceptoCargo.tiempoSitp;
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargo.tiempoSitp;
    if (localStateManager.isLoaded(paramConceptoCargo, jdoInheritedFieldCount + 8))
      return paramConceptoCargo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramConceptoCargo, jdoInheritedFieldCount + 8, paramConceptoCargo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ConceptoCargo paramConceptoCargo, Date paramDate)
  {
    if (paramConceptoCargo.jdoFlags == 0)
    {
      paramConceptoCargo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoCargo, jdoInheritedFieldCount + 8, paramConceptoCargo.tiempoSitp, paramDate);
  }

  private static final double jdoGetunidades(ConceptoCargo paramConceptoCargo)
  {
    if (paramConceptoCargo.jdoFlags <= 0)
      return paramConceptoCargo.unidades;
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCargo.unidades;
    if (localStateManager.isLoaded(paramConceptoCargo, jdoInheritedFieldCount + 9))
      return paramConceptoCargo.unidades;
    return localStateManager.getDoubleField(paramConceptoCargo, jdoInheritedFieldCount + 9, paramConceptoCargo.unidades);
  }

  private static final void jdoSetunidades(ConceptoCargo paramConceptoCargo, double paramDouble)
  {
    if (paramConceptoCargo.jdoFlags == 0)
    {
      paramConceptoCargo.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCargo.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoCargo, jdoInheritedFieldCount + 9, paramConceptoCargo.unidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}