package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class DetalleDisquete
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SEPARADOR;
  protected static final Map LISTA_ALINEACION;
  protected static final Map LISTA_TIPO_REGISTRO;
  protected static final Map LISTA_TIPO_CAMPO;
  protected static final Map LISTA_RELLENO;
  protected static final Map LISTA_CAMPO;
  protected static final Map LISTA_ENTRADA;
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_TOTALES;
  private long idDetalleDisquete;
  private Disquete disquete;
  private String tipoRegistro;
  private int numeroCampo;
  private String tipoCampo;
  private int longitudCampo;
  private String campoBaseDatos;
  private String campoTotales;
  private String campoUsuario;
  private String campoEntrada;
  private String separadorDecimal;
  private String alineacionCampo;
  private String rellenarCero;
  private double multiplicador;
  private Concepto concepto;
  private String separadorMiles;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "alineacionCampo", "campoBaseDatos", "campoEntrada", "campoTotales", "campoUsuario", "concepto", "disquete", "idDetalleDisquete", "longitudCampo", "multiplicador", "numeroCampo", "rellenarCero", "separadorDecimal", "separadorMiles", "tipoCampo", "tipoRegistro" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("sigefirrhh.base.definiciones.Disquete"), Long.TYPE, Integer.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 26, 26, 24, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.DetalleDisquete"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DetalleDisquete());

    LISTA_SEPARADOR = 
      new LinkedHashMap();
    LISTA_ALINEACION = 
      new LinkedHashMap();
    LISTA_TIPO_REGISTRO = 
      new LinkedHashMap();
    LISTA_TIPO_CAMPO = 
      new LinkedHashMap();
    LISTA_RELLENO = 
      new LinkedHashMap();
    LISTA_CAMPO = 
      new LinkedHashMap();
    LISTA_ENTRADA = 
      new LinkedHashMap();
    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_TOTALES = 
      new LinkedHashMap();
    LISTA_TIPO_CAMPO.put("F", "FIJO");
    LISTA_TIPO_CAMPO.put("V", "BASE DE DATOS");
    LISTA_TIPO_CAMPO.put("E", "ENTRADA");
    LISTA_TIPO_CAMPO.put("C", "CONTADOR");
    LISTA_TIPO_REGISTRO.put("1", "ENCABEZADO (1)");
    LISTA_TIPO_REGISTRO.put("4", "ENCABEZADO (2)");
    LISTA_TIPO_REGISTRO.put("5", "ENCABEZADO (3)");
    LISTA_TIPO_REGISTRO.put("2", "DETALLE (1)");
    LISTA_TIPO_REGISTRO.put("6", "DETALLE (2)");
    LISTA_TIPO_REGISTRO.put("3", "TOTAL");
    LISTA_SEPARADOR.put("N", "NO APLICA");
    LISTA_SEPARADOR.put("P", "PUNTO");
    LISTA_SEPARADOR.put("C", "COMA");
    LISTA_ALINEACION.put("I", "IZQUIERDA");
    LISTA_ALINEACION.put("D", "DERECHA");
    LISTA_RELLENO.put("C", "CEROS");
    LISTA_RELLENO.put("B", "BLANCOS");
    LISTA_RELLENO.put("N", "NINGUNO");

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_CAMPO.put("1", "ABONO MENSUAL PRESTACIONES (5 DIAS)");
    LISTA_CAMPO.put("74", "ABONO (5 DIAS) + DIAS ADICIONALES");
    LISTA_CAMPO.put("72", "AÑO APERTURA FIDEICOMISO (AA)");
    LISTA_CAMPO.put("73", "AÑO APERTURA FIDEICOMISO (AAAA)");
    LISTA_CAMPO.put("2", "AÑO EGRESO (AA)");
    LISTA_CAMPO.put("3", "AÑO EGRESO (AAAA)");
    LISTA_CAMPO.put("4", "AÑO HISTORICO (AAAA)");
    LISTA_CAMPO.put("5", "AÑO INGRESO APN (AA)");
    LISTA_CAMPO.put("6", "AÑO INGRESO APN (AAAA)");
    LISTA_CAMPO.put("7", "AÑO INGRESO ORGANISMO (AA)");
    LISTA_CAMPO.put("8", "AÑO INGRESO ORGANISMO (AAAA)");
    LISTA_CAMPO.put("9", "AÑO NACIMIENTO (AA)");
    LISTA_CAMPO.put("10", "AÑO NACIMIENTO (AAAA)");
    LISTA_CAMPO.put("11", "AÑO PRESTACIONES (AA)");
    LISTA_CAMPO.put("12", "AÑO PRESTACIONES (AAAA)");
    LISTA_CAMPO.put("13", "APELLIDOS Y NOMBRES");
    LISTA_CAMPO.put("14", "CEDULA TRABAJADOR");
    LISTA_CAMPO.put("15", "CODIGO CARGO");
    LISTA_CAMPO.put("16", "CODIGO CONCEPTO");
    LISTA_CAMPO.put("78", "CODIGO PROVEEDOR CESTA TICKET");
    LISTA_CAMPO.put("85", "CODIGO LUGAR PAGO");
    LISTA_CAMPO.put("17", "CODIGO DEPENDENCIA");
    LISTA_CAMPO.put("18", "CODIGO NOMINA TRABAJADOR");
    LISTA_CAMPO.put("19", "CODIGO ORGANISMO");
    LISTA_CAMPO.put("20", "CODIGO REGION");
    LISTA_CAMPO.put("21", "CODIGO SEDE");
    LISTA_CAMPO.put("22", "COMPENSACION");
    LISTA_CAMPO.put("23", "CUENTA BANCO ORGANISMO");
    LISTA_CAMPO.put("24", "CUENTA NOMINA TRABAJADOR");
    LISTA_CAMPO.put("67", "CUENTA FIDEICOMISO TRABAJADOR");
    LISTA_CAMPO.put("75", "CUENTA LPH TRABAJADOR");
    LISTA_CAMPO.put("83", "DENOMINACION TICKET");
    LISTA_CAMPO.put("25", "DESCRIPCION CARGO");
    LISTA_CAMPO.put("26", "DESCRIPCION CONCEPTO");
    LISTA_CAMPO.put("27", "DESCRIPCION DEPENDENCIA");
    LISTA_CAMPO.put("28", "DESCRIPCION REGION");
    LISTA_CAMPO.put("29", "DESCRIPCION SEDE");
    LISTA_CAMPO.put("110", "DESCRIPCION LUGAR PAGO");
    LISTA_CAMPO.put("30", "DIAS ADICIONALES PRESTACIONES");
    LISTA_CAMPO.put("70", "DIA APERTURA FIDEICOMISO");
    LISTA_CAMPO.put("31", "DIA EGRESO");
    LISTA_CAMPO.put("32", "DIA INGRESO APN");
    LISTA_CAMPO.put("33", "DIA INGRESO");
    LISTA_CAMPO.put("34", "DIA NACIMIENTO");
    LISTA_CAMPO.put("66", "DIA PRESTACIONES");
    LISTA_CAMPO.put("68", "DIRECCION TRABAJADOR");
    LISTA_CAMPO.put("35", "ESTADO CIVIL");
    LISTA_CAMPO.put("36", "ESTATUS TRABAJADOR");
    LISTA_CAMPO.put("81", "ESTATUS (0 = ACTIVO, 1 = EGRESADO, 2 = NUEVO )");
    LISTA_CAMPO.put("90", "FORMA DE PAGO 1=Depósito/2=Cheque");
    LISTA_CAMPO.put("91", "FORMA DE PAGO D=Depósito/C=Cheque");
    LISTA_CAMPO.put("37", "FRECUENCIA CONCEPTO");
    LISTA_CAMPO.put("71", "MES APERTURA FIDEICOMISO");
    LISTA_CAMPO.put("38", "MES EGRESO");
    LISTA_CAMPO.put("39", "MES HISTORICO");
    LISTA_CAMPO.put("40", "MES INGRESO APN");
    LISTA_CAMPO.put("41", "MES INGRESO");
    LISTA_CAMPO.put("42", "MES NACIMIENTO");
    LISTA_CAMPO.put("43", "MES PRESTACIONES");
    LISTA_CAMPO.put("44", "MONTO APORTE PATRONAL");
    LISTA_CAMPO.put("82", "MONTO APORTE PATRONAL + MONTO RETENCION");
    LISTA_CAMPO.put("45", "MONTO BASE PRESTACIONES MENSUAL");
    LISTA_CAMPO.put("46", "MONTO BASE DIAS ADICIONALES");
    LISTA_CAMPO.put("47", "MONTO POR DIAS ADICIONALES");
    LISTA_CAMPO.put("48", "MONTO CONCEPTO FIJO");
    LISTA_CAMPO.put("49", "MONTO CONCEPTO HISTORICO");
    LISTA_CAMPO.put("50", "MONTO NETO NOMINA TRABAJADOR");
    LISTA_CAMPO.put("51", "MONTO RETENCION");
    LISTA_CAMPO.put("76", "MONTO TOTAL TICKETS");
    LISTA_CAMPO.put("52", "NACIONALIDAD");
    LISTA_CAMPO.put("77", "NUMERO DE TICKETS");
    LISTA_CAMPO.put("53", "NOMBRE DEL ORGANISMO");
    LISTA_CAMPO.put("97", "PORCENTAJE LPH TRABAJADOR");
    LISTA_CAMPO.put("98", "PORCENTAJE LPH PATRONO");
    LISTA_CAMPO.put("99", "PORCENTAJE LPH TRABAJADOR + PORCENTAJE LPH PATRONO");
    LISTA_CAMPO.put("54", "PRIMER APELLIDO");
    LISTA_CAMPO.put("55", "PRIMER NOMBRE");
    LISTA_CAMPO.put("60", "SEGUNDO APELLIDO");
    LISTA_CAMPO.put("61", "SEGUNDO NOMBRE");
    LISTA_CAMPO.put("62", "SEXO");
    LISTA_CAMPO.put("80", "SEXO(M = 1, F = 2)");
    LISTA_CAMPO.put("63", "SUELDO BASICO");
    LISTA_CAMPO.put("64", "SUELDO INTEGRAL");
    LISTA_CAMPO.put("95", "SUELDO PROMEDIO FONDO JUBILACION");
    LISTA_CAMPO.put("94", "SUELDO PROMEDIO LPH");
    LISTA_CAMPO.put("92", "SUELDO PROMEDIO SSO");
    LISTA_CAMPO.put("93", "SUELDO PROMEDIO SPF");
    LISTA_CAMPO.put("69", "TELEFONO TRABAJADOR");
    LISTA_CAMPO.put("56", "TIPO CUENTA");
    LISTA_CAMPO.put("57", "TIPO PERSONAL");
    LISTA_CAMPO.put("96", "TIPO ACCION LPH 1=Activo,2=Retirar,3=Ingresar");
    LISTA_CAMPO.put("65", "TOTAL AJUSTES SUELDOS");
    LISTA_CAMPO.put("58", "TOTAL PRIMAS CARGO");
    LISTA_CAMPO.put("59", "TOTAL PRIMAS TRABAJADOR");
    LISTA_CAMPO.put("86", "EMAIL TRABAJADOR");
    LISTA_CAMPO.put("111", "I.S.R.L.");
    LISTA_CAMPO.put("0", "NO APLICA");

    LISTA_TOTALES.put("1", "TOTAL APORTE ORGANISMO");
    LISTA_TOTALES.put("2", "TOTAL NOMINA A PAGAR");
    LISTA_TOTALES.put("3", "TOTAL RETENCION");
    LISTA_TOTALES.put("4", "TOTAL TRABAJADORES");
    LISTA_TOTALES.put("5", "CORRELATIVO");
    LISTA_TOTALES.put("0", "NO APLICA");
    LISTA_TOTALES.put("6", "TOTAL RETENCION + APORTE ORGANISMO");
    LISTA_TOTALES.put("7", "TOTAL ABONO MENSUAL PRESTACIONES");
    LISTA_TOTALES.put("8", "TOTAL DIAS ADICIONALES PRESTACIONES");
    LISTA_TOTALES.put("9", "TOTAL ABONO MENSUAL + DIAS ADICIONALES");

    LISTA_ENTRADA.put("1", "AÑO ABONO (AA)");
    LISTA_ENTRADA.put("2", "AÑO ABONO (AAAA)");
    LISTA_ENTRADA.put("3", "AÑO EJECUCION (AA)");
    LISTA_ENTRADA.put("4", "AÑO EJECUCION (AAAA)");
    LISTA_ENTRADA.put("12", "AÑO PROCESO (AA)");
    LISTA_ENTRADA.put("13", "AÑO PROCESO (AAAA)");
    LISTA_ENTRADA.put("5", "DIA ABONO");
    LISTA_ENTRADA.put("6", "DIA EJECUCION");
    LISTA_ENTRADA.put("7", "HORA ABONO (HHMMSS)");
    LISTA_ENTRADA.put("8", "HORA EJECUCION (HHMMSS)");
    LISTA_ENTRADA.put("9", "MES ABONO");
    LISTA_ENTRADA.put("10", "MES EJECUCION");
    LISTA_ENTRADA.put("14", "MES PROCESO");
    LISTA_ENTRADA.put("11", "Nº ORDEN DE PAGO");
    LISTA_ENTRADA.put("0", "NO APLICA");
  }

  public DetalleDisquete()
  {
    jdoSettipoRegistro(this, "2");

    jdoSetnumeroCampo(this, 1);

    jdoSettipoCampo(this, "F");

    jdoSetlongitudCampo(this, 1);

    jdoSetcampoBaseDatos(this, "0");

    jdoSetcampoTotales(this, "0");

    jdoSetseparadorDecimal(this, "N");

    jdoSetalineacionCampo(this, "D");

    jdoSetrellenarCero(this, "B");

    jdoSetmultiplicador(this, 1.0D);

    jdoSetseparadorMiles(this, "N");
  }
  public String toString() {
    StringBuffer lista = new StringBuffer();
    lista.append(jdoGettipoRegistro(this) + "- " + jdoGetnumeroCampo(this) + "  -  ");

    if (jdoGettipoCampo(this).equals("V")) {
      if (!jdoGettipoRegistro(this).equals("2"))
        lista.append(" " + LISTA_TOTALES.get(jdoGetcampoTotales(this)));
      else {
        lista.append(" " + LISTA_CAMPO.get(jdoGetcampoBaseDatos(this)));
      }
    }
    else if (jdoGettipoCampo(this).equals("E"))
      lista.append(" " + LISTA_ENTRADA.get(jdoGetcampoEntrada(this)));
    else {
      lista.append(" " + jdoGetcampoUsuario(this));
    }
    lista.append(" - " + jdoGetlongitudCampo(this));
    return 
      lista.toString();
  }

  public String getAlineacionCampo()
  {
    return jdoGetalineacionCampo(this);
  }

  public void setAlineacionCampo(String alineacionCampo)
  {
    jdoSetalineacionCampo(this, alineacionCampo);
  }

  public String getCampoBaseDatos()
  {
    return jdoGetcampoBaseDatos(this);
  }

  public void setCampoBaseDatos(String campoBaseDatos)
  {
    jdoSetcampoBaseDatos(this, campoBaseDatos);
  }

  public String getCampoUsuario()
  {
    return jdoGetcampoUsuario(this);
  }

  public void setCampoUsuario(String campoUsuario)
  {
    jdoSetcampoUsuario(this, campoUsuario);
  }

  public Concepto getConcepto()
  {
    return jdoGetconcepto(this);
  }

  public void setConcepto(Concepto concepto)
  {
    jdoSetconcepto(this, concepto);
  }

  public Disquete getDisquete()
  {
    return jdoGetdisquete(this);
  }

  public void setDisquete(Disquete disquete)
  {
    jdoSetdisquete(this, disquete);
  }

  public long getIdDetalleDisquete()
  {
    return jdoGetidDetalleDisquete(this);
  }

  public void setIdDetalleDisquete(long idDetalleDisquete)
  {
    jdoSetidDetalleDisquete(this, idDetalleDisquete);
  }

  public int getLongitudCampo()
  {
    return jdoGetlongitudCampo(this);
  }

  public void setLongitudCampo(int longitudCampo)
  {
    jdoSetlongitudCampo(this, longitudCampo);
  }

  public double getMultiplicador()
  {
    return jdoGetmultiplicador(this);
  }

  public void setMultiplicador(double multiplicador)
  {
    jdoSetmultiplicador(this, multiplicador);
  }

  public int getNumeroCampo()
  {
    return jdoGetnumeroCampo(this);
  }

  public void setNumeroCampo(int numeroCampo)
  {
    jdoSetnumeroCampo(this, numeroCampo);
  }

  public String getRellenarCero()
  {
    return jdoGetrellenarCero(this);
  }

  public void setRellenarCero(String rellenarCero)
  {
    jdoSetrellenarCero(this, rellenarCero);
  }

  public String getSeparadorDecimal()
  {
    return jdoGetseparadorDecimal(this);
  }

  public void setSeparadorDecimal(String separadorDecimal)
  {
    jdoSetseparadorDecimal(this, separadorDecimal);
  }

  public String getTipoCampo()
  {
    return jdoGettipoCampo(this);
  }

  public void setTipoCampo(String tipoCampo)
  {
    jdoSettipoCampo(this, tipoCampo);
  }

  public String getTipoRegistro()
  {
    return jdoGettipoRegistro(this);
  }

  public void setTipoRegistro(String tipoRegistro)
  {
    jdoSettipoRegistro(this, tipoRegistro);
  }

  public String getCampoEntrada()
  {
    return jdoGetcampoEntrada(this);
  }

  public void setCampoEntrada(String string)
  {
    jdoSetcampoEntrada(this, string);
  }

  public String getCampoTotales()
  {
    return jdoGetcampoTotales(this);
  }

  public void setCampoTotales(String string)
  {
    jdoSetcampoTotales(this, string);
  }

  public String getSeparadorMiles()
  {
    return jdoGetseparadorMiles(this);
  }

  public void setSeparadorMiles(String separadorMiles)
  {
    jdoSetseparadorMiles(this, separadorMiles);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 16;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DetalleDisquete localDetalleDisquete = new DetalleDisquete();
    localDetalleDisquete.jdoFlags = 1;
    localDetalleDisquete.jdoStateManager = paramStateManager;
    return localDetalleDisquete;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DetalleDisquete localDetalleDisquete = new DetalleDisquete();
    localDetalleDisquete.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDetalleDisquete.jdoFlags = 1;
    localDetalleDisquete.jdoStateManager = paramStateManager;
    return localDetalleDisquete;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.alineacionCampo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.campoBaseDatos);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.campoEntrada);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.campoTotales);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.campoUsuario);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.concepto);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.disquete);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDetalleDisquete);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.longitudCampo);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.multiplicador);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroCampo);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.rellenarCero);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.separadorDecimal);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.separadorMiles);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoCampo);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoRegistro);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alineacionCampo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.campoBaseDatos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.campoEntrada = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.campoTotales = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.campoUsuario = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.concepto = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.disquete = ((Disquete)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDetalleDisquete = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.longitudCampo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.multiplicador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroCampo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.rellenarCero = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.separadorDecimal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.separadorMiles = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCampo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoRegistro = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DetalleDisquete paramDetalleDisquete, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.alineacionCampo = paramDetalleDisquete.alineacionCampo;
      return;
    case 1:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.campoBaseDatos = paramDetalleDisquete.campoBaseDatos;
      return;
    case 2:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.campoEntrada = paramDetalleDisquete.campoEntrada;
      return;
    case 3:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.campoTotales = paramDetalleDisquete.campoTotales;
      return;
    case 4:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.campoUsuario = paramDetalleDisquete.campoUsuario;
      return;
    case 5:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.concepto = paramDetalleDisquete.concepto;
      return;
    case 6:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.disquete = paramDetalleDisquete.disquete;
      return;
    case 7:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.idDetalleDisquete = paramDetalleDisquete.idDetalleDisquete;
      return;
    case 8:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.longitudCampo = paramDetalleDisquete.longitudCampo;
      return;
    case 9:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.multiplicador = paramDetalleDisquete.multiplicador;
      return;
    case 10:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.numeroCampo = paramDetalleDisquete.numeroCampo;
      return;
    case 11:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.rellenarCero = paramDetalleDisquete.rellenarCero;
      return;
    case 12:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.separadorDecimal = paramDetalleDisquete.separadorDecimal;
      return;
    case 13:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.separadorMiles = paramDetalleDisquete.separadorMiles;
      return;
    case 14:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCampo = paramDetalleDisquete.tipoCampo;
      return;
    case 15:
      if (paramDetalleDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.tipoRegistro = paramDetalleDisquete.tipoRegistro;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DetalleDisquete))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DetalleDisquete localDetalleDisquete = (DetalleDisquete)paramObject;
    if (localDetalleDisquete.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDetalleDisquete, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DetalleDisquetePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DetalleDisquetePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DetalleDisquetePK))
      throw new IllegalArgumentException("arg1");
    DetalleDisquetePK localDetalleDisquetePK = (DetalleDisquetePK)paramObject;
    localDetalleDisquetePK.idDetalleDisquete = this.idDetalleDisquete;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DetalleDisquetePK))
      throw new IllegalArgumentException("arg1");
    DetalleDisquetePK localDetalleDisquetePK = (DetalleDisquetePK)paramObject;
    this.idDetalleDisquete = localDetalleDisquetePK.idDetalleDisquete;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DetalleDisquetePK))
      throw new IllegalArgumentException("arg2");
    DetalleDisquetePK localDetalleDisquetePK = (DetalleDisquetePK)paramObject;
    localDetalleDisquetePK.idDetalleDisquete = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DetalleDisquetePK))
      throw new IllegalArgumentException("arg2");
    DetalleDisquetePK localDetalleDisquetePK = (DetalleDisquetePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localDetalleDisquetePK.idDetalleDisquete);
  }

  private static final String jdoGetalineacionCampo(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.alineacionCampo;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.alineacionCampo;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 0))
      return paramDetalleDisquete.alineacionCampo;
    return localStateManager.getStringField(paramDetalleDisquete, jdoInheritedFieldCount + 0, paramDetalleDisquete.alineacionCampo);
  }

  private static final void jdoSetalineacionCampo(DetalleDisquete paramDetalleDisquete, String paramString)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.alineacionCampo = paramString;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.alineacionCampo = paramString;
      return;
    }
    localStateManager.setStringField(paramDetalleDisquete, jdoInheritedFieldCount + 0, paramDetalleDisquete.alineacionCampo, paramString);
  }

  private static final String jdoGetcampoBaseDatos(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.campoBaseDatos;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.campoBaseDatos;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 1))
      return paramDetalleDisquete.campoBaseDatos;
    return localStateManager.getStringField(paramDetalleDisquete, jdoInheritedFieldCount + 1, paramDetalleDisquete.campoBaseDatos);
  }

  private static final void jdoSetcampoBaseDatos(DetalleDisquete paramDetalleDisquete, String paramString)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.campoBaseDatos = paramString;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.campoBaseDatos = paramString;
      return;
    }
    localStateManager.setStringField(paramDetalleDisquete, jdoInheritedFieldCount + 1, paramDetalleDisquete.campoBaseDatos, paramString);
  }

  private static final String jdoGetcampoEntrada(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.campoEntrada;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.campoEntrada;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 2))
      return paramDetalleDisquete.campoEntrada;
    return localStateManager.getStringField(paramDetalleDisquete, jdoInheritedFieldCount + 2, paramDetalleDisquete.campoEntrada);
  }

  private static final void jdoSetcampoEntrada(DetalleDisquete paramDetalleDisquete, String paramString)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.campoEntrada = paramString;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.campoEntrada = paramString;
      return;
    }
    localStateManager.setStringField(paramDetalleDisquete, jdoInheritedFieldCount + 2, paramDetalleDisquete.campoEntrada, paramString);
  }

  private static final String jdoGetcampoTotales(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.campoTotales;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.campoTotales;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 3))
      return paramDetalleDisquete.campoTotales;
    return localStateManager.getStringField(paramDetalleDisquete, jdoInheritedFieldCount + 3, paramDetalleDisquete.campoTotales);
  }

  private static final void jdoSetcampoTotales(DetalleDisquete paramDetalleDisquete, String paramString)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.campoTotales = paramString;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.campoTotales = paramString;
      return;
    }
    localStateManager.setStringField(paramDetalleDisquete, jdoInheritedFieldCount + 3, paramDetalleDisquete.campoTotales, paramString);
  }

  private static final String jdoGetcampoUsuario(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.campoUsuario;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.campoUsuario;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 4))
      return paramDetalleDisquete.campoUsuario;
    return localStateManager.getStringField(paramDetalleDisquete, jdoInheritedFieldCount + 4, paramDetalleDisquete.campoUsuario);
  }

  private static final void jdoSetcampoUsuario(DetalleDisquete paramDetalleDisquete, String paramString)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.campoUsuario = paramString;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.campoUsuario = paramString;
      return;
    }
    localStateManager.setStringField(paramDetalleDisquete, jdoInheritedFieldCount + 4, paramDetalleDisquete.campoUsuario, paramString);
  }

  private static final Concepto jdoGetconcepto(DetalleDisquete paramDetalleDisquete)
  {
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.concepto;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 5))
      return paramDetalleDisquete.concepto;
    return (Concepto)localStateManager.getObjectField(paramDetalleDisquete, jdoInheritedFieldCount + 5, paramDetalleDisquete.concepto);
  }

  private static final void jdoSetconcepto(DetalleDisquete paramDetalleDisquete, Concepto paramConcepto)
  {
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.concepto = paramConcepto;
      return;
    }
    localStateManager.setObjectField(paramDetalleDisquete, jdoInheritedFieldCount + 5, paramDetalleDisquete.concepto, paramConcepto);
  }

  private static final Disquete jdoGetdisquete(DetalleDisquete paramDetalleDisquete)
  {
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.disquete;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 6))
      return paramDetalleDisquete.disquete;
    return (Disquete)localStateManager.getObjectField(paramDetalleDisquete, jdoInheritedFieldCount + 6, paramDetalleDisquete.disquete);
  }

  private static final void jdoSetdisquete(DetalleDisquete paramDetalleDisquete, Disquete paramDisquete)
  {
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.disquete = paramDisquete;
      return;
    }
    localStateManager.setObjectField(paramDetalleDisquete, jdoInheritedFieldCount + 6, paramDetalleDisquete.disquete, paramDisquete);
  }

  private static final long jdoGetidDetalleDisquete(DetalleDisquete paramDetalleDisquete)
  {
    return paramDetalleDisquete.idDetalleDisquete;
  }

  private static final void jdoSetidDetalleDisquete(DetalleDisquete paramDetalleDisquete, long paramLong)
  {
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.idDetalleDisquete = paramLong;
      return;
    }
    localStateManager.setLongField(paramDetalleDisquete, jdoInheritedFieldCount + 7, paramDetalleDisquete.idDetalleDisquete, paramLong);
  }

  private static final int jdoGetlongitudCampo(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.longitudCampo;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.longitudCampo;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 8))
      return paramDetalleDisquete.longitudCampo;
    return localStateManager.getIntField(paramDetalleDisquete, jdoInheritedFieldCount + 8, paramDetalleDisquete.longitudCampo);
  }

  private static final void jdoSetlongitudCampo(DetalleDisquete paramDetalleDisquete, int paramInt)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.longitudCampo = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.longitudCampo = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleDisquete, jdoInheritedFieldCount + 8, paramDetalleDisquete.longitudCampo, paramInt);
  }

  private static final double jdoGetmultiplicador(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.multiplicador;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.multiplicador;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 9))
      return paramDetalleDisquete.multiplicador;
    return localStateManager.getDoubleField(paramDetalleDisquete, jdoInheritedFieldCount + 9, paramDetalleDisquete.multiplicador);
  }

  private static final void jdoSetmultiplicador(DetalleDisquete paramDetalleDisquete, double paramDouble)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.multiplicador = paramDouble;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.multiplicador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramDetalleDisquete, jdoInheritedFieldCount + 9, paramDetalleDisquete.multiplicador, paramDouble);
  }

  private static final int jdoGetnumeroCampo(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.numeroCampo;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.numeroCampo;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 10))
      return paramDetalleDisquete.numeroCampo;
    return localStateManager.getIntField(paramDetalleDisquete, jdoInheritedFieldCount + 10, paramDetalleDisquete.numeroCampo);
  }

  private static final void jdoSetnumeroCampo(DetalleDisquete paramDetalleDisquete, int paramInt)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.numeroCampo = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.numeroCampo = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleDisquete, jdoInheritedFieldCount + 10, paramDetalleDisquete.numeroCampo, paramInt);
  }

  private static final String jdoGetrellenarCero(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.rellenarCero;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.rellenarCero;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 11))
      return paramDetalleDisquete.rellenarCero;
    return localStateManager.getStringField(paramDetalleDisquete, jdoInheritedFieldCount + 11, paramDetalleDisquete.rellenarCero);
  }

  private static final void jdoSetrellenarCero(DetalleDisquete paramDetalleDisquete, String paramString)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.rellenarCero = paramString;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.rellenarCero = paramString;
      return;
    }
    localStateManager.setStringField(paramDetalleDisquete, jdoInheritedFieldCount + 11, paramDetalleDisquete.rellenarCero, paramString);
  }

  private static final String jdoGetseparadorDecimal(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.separadorDecimal;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.separadorDecimal;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 12))
      return paramDetalleDisquete.separadorDecimal;
    return localStateManager.getStringField(paramDetalleDisquete, jdoInheritedFieldCount + 12, paramDetalleDisquete.separadorDecimal);
  }

  private static final void jdoSetseparadorDecimal(DetalleDisquete paramDetalleDisquete, String paramString)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.separadorDecimal = paramString;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.separadorDecimal = paramString;
      return;
    }
    localStateManager.setStringField(paramDetalleDisquete, jdoInheritedFieldCount + 12, paramDetalleDisquete.separadorDecimal, paramString);
  }

  private static final String jdoGetseparadorMiles(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.separadorMiles;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.separadorMiles;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 13))
      return paramDetalleDisquete.separadorMiles;
    return localStateManager.getStringField(paramDetalleDisquete, jdoInheritedFieldCount + 13, paramDetalleDisquete.separadorMiles);
  }

  private static final void jdoSetseparadorMiles(DetalleDisquete paramDetalleDisquete, String paramString)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.separadorMiles = paramString;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.separadorMiles = paramString;
      return;
    }
    localStateManager.setStringField(paramDetalleDisquete, jdoInheritedFieldCount + 13, paramDetalleDisquete.separadorMiles, paramString);
  }

  private static final String jdoGettipoCampo(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.tipoCampo;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.tipoCampo;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 14))
      return paramDetalleDisquete.tipoCampo;
    return localStateManager.getStringField(paramDetalleDisquete, jdoInheritedFieldCount + 14, paramDetalleDisquete.tipoCampo);
  }

  private static final void jdoSettipoCampo(DetalleDisquete paramDetalleDisquete, String paramString)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.tipoCampo = paramString;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.tipoCampo = paramString;
      return;
    }
    localStateManager.setStringField(paramDetalleDisquete, jdoInheritedFieldCount + 14, paramDetalleDisquete.tipoCampo, paramString);
  }

  private static final String jdoGettipoRegistro(DetalleDisquete paramDetalleDisquete)
  {
    if (paramDetalleDisquete.jdoFlags <= 0)
      return paramDetalleDisquete.tipoRegistro;
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleDisquete.tipoRegistro;
    if (localStateManager.isLoaded(paramDetalleDisquete, jdoInheritedFieldCount + 15))
      return paramDetalleDisquete.tipoRegistro;
    return localStateManager.getStringField(paramDetalleDisquete, jdoInheritedFieldCount + 15, paramDetalleDisquete.tipoRegistro);
  }

  private static final void jdoSettipoRegistro(DetalleDisquete paramDetalleDisquete, String paramString)
  {
    if (paramDetalleDisquete.jdoFlags == 0)
    {
      paramDetalleDisquete.tipoRegistro = paramString;
      return;
    }
    StateManager localStateManager = paramDetalleDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleDisquete.tipoRegistro = paramString;
      return;
    }
    localStateManager.setStringField(paramDetalleDisquete, jdoInheritedFieldCount + 15, paramDetalleDisquete.tipoRegistro, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}