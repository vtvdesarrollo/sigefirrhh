package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ConceptoVacacionesPK
  implements Serializable
{
  public long idConceptoVacaciones;

  public ConceptoVacacionesPK()
  {
  }

  public ConceptoVacacionesPK(long idConceptoVacaciones)
  {
    this.idConceptoVacaciones = idConceptoVacaciones;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoVacacionesPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoVacacionesPK thatPK)
  {
    return 
      this.idConceptoVacaciones == thatPK.idConceptoVacaciones;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoVacaciones)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoVacaciones);
  }
}