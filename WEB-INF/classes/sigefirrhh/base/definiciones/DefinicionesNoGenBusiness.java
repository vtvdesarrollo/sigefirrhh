package sigefirrhh.base.definiciones;

import java.io.Serializable;
import java.util.Collection;

public class DefinicionesNoGenBusiness extends DefinicionesBusiness
  implements Serializable
{
  private BancoNoGenBeanBusiness bancoNoGenBeanBusiness = new BancoNoGenBeanBusiness();

  private ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

  private GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

  private SemanaNoGenBeanBusiness semanaNoGenBeanBusiness = new SemanaNoGenBeanBusiness();

  private ConceptoTipoPersonalNoGenBeanBusiness conceptoTipoPersonalNoGenBeanBusiness = new ConceptoTipoPersonalNoGenBeanBusiness();

  private PrimaAntiguedadNoGenBeanBusiness primaAntiguedadNoGenBeanBusiness = new PrimaAntiguedadNoGenBeanBusiness();

  private VacacionesPorAnioNoGenBeanBusiness vacacionesPorAnioNoGenBeanBusiness = new VacacionesPorAnioNoGenBeanBusiness();

  private GrupoNominaNoGenBeanBusiness grupoNominaNoGenBeanBusiness = new GrupoNominaNoGenBeanBusiness();

  private ConceptoAsociadoNoGenBeanBusiness conceptoAsociadoNoGenBeanBusiness = new ConceptoAsociadoNoGenBeanBusiness();

  private RestringidoNoGenBeanBusiness restringidoNoGenBeanBusiness = new RestringidoNoGenBeanBusiness();

  private TipoPersonalNoGenBeanBusiness tipoPersonalNoGenBeanBusiness = new TipoPersonalNoGenBeanBusiness();

  private DetalleDisqueteNoGenBeanBusiness detalleDisqueteNoGenBeanBusiness = new DetalleDisqueteNoGenBeanBusiness();

  private ConceptoCargoNoGenBeanBusiness conceptoCargoNoGenBeanBusiness = new ConceptoCargoNoGenBeanBusiness();

  private CategoriaPresupuestoNoGenBeanBusiness categoriaPresupuestoNoGenBeanBusiness = new CategoriaPresupuestoNoGenBeanBusiness();

  private TarifaAriNoGenBeanBusiness tarifaAriNoGenBeanBusiness = new TarifaAriNoGenBeanBusiness();

  private EscalaCuadroOnapreNoGenBeanBusiness escalaCuadroOnapreNoGenBeanBusiness = new EscalaCuadroOnapreNoGenBeanBusiness();

  public Collection findConceptoByOrganismoAndDeduccionSindicato(long idOrganismo, String deduccionSindicato) throws Exception
  {
    return this.conceptoBeanBusiness.findByOrganismoAndDeduccionSindicato(idOrganismo, deduccionSindicato);
  }

  public Collection findConceptoByTipoPrestamo(long idOrganismo, String tipoPrestamo) throws Exception {
    return this.conceptoBeanBusiness.findByTipoPrestamo(idOrganismo, tipoPrestamo);
  }

  public Collection findGrupoNominaByOrganismoAndPeriodicidad(long idOrganismo, String periodicidad) throws Exception {
    return this.grupoNominaBeanBusiness.findByOrganismoAndPeriodicidad(idOrganismo, periodicidad);
  }

  public Collection findSemanaByGrupoNominaAndAnio(long idGrupoNomina, int anio) throws Exception {
    return this.semanaNoGenBeanBusiness.findByGrupoNominaAndAnio(idGrupoNomina, anio);
  }

  public Collection findConceptoTipoPersonalForPrestamo(long idTipoPersonal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findForPrestamo(idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalByCodConceptoAndIdTipoPersonal(String codConcepto, long idTipoPersonal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByCodConceptoAndIdTipoPersonal(codConcepto, idTipoPersonal);
  }

  public PrimaAntiguedad findPrimaAntiguedadForAniosServicio(long idTipoPersonal, int aniosCumple) throws Exception {
    return this.primaAntiguedadNoGenBeanBusiness.findForAniosServicio(idTipoPersonal, aniosCumple);
  }

  public VacacionesPorAnio findVacacionesPorAnioForAniosServicio(long idTipoPersonal, int aniosCumple) throws Exception {
    return this.vacacionesPorAnioNoGenBeanBusiness.findForAniosServicio(idTipoPersonal, aniosCumple);
  }

  public ConceptoAsociado findConceptoAsociadoByConceptoTipoPersonalAndConceptoAsociado(long idConceptoTipoPersonal, long idConceptoAsociado) throws Exception {
    return this.conceptoAsociadoNoGenBeanBusiness.findByConceptoTipoPersonalAndConceptoAsociado(idConceptoTipoPersonal, idConceptoAsociado);
  }

  public Collection findConceptoTipoPersonalByTipoPersonalAndConcepto(long idTipoPersonal, long idConcepto) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByTipoPersonalAndConcepto(idTipoPersonal, idConcepto);
  }

  public Collection findConceptoTipoPersonalForIngresoTrabajador(long idTipoPersonal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findForIngresoTrabajador(idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalForIngresoTrabajadorCargo(long idTipoPersonal, long idCargo) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findForIngresoTrabajadorCargo(idTipoPersonal, idCargo);
  }

  public boolean estanRestringidos(long idTipoPersonal, long idTipoPersonal2) throws Exception {
    return this.restringidoNoGenBeanBusiness.estanRestringidos(idTipoPersonal, idTipoPersonal2);
  }

  public Collection findTipoPersonalByUsuarioAndOrganismo(long idUsuario, long idOrganismo, String administrador) throws Exception {
    return this.tipoPersonalNoGenBeanBusiness.findByUsuarioAndOrganismo(idUsuario, idOrganismo, administrador);
  }

  public Collection findTipoPersonalWithSeguridad(long idUsuario, long idOrganismo, String administrador) throws Exception {
    return this.tipoPersonalNoGenBeanBusiness.findWithSeguridad(idUsuario, idOrganismo, administrador);
  }

  public Collection findTipoPersonalWithSeguridadAndRelacionPersonal(long idUsuario, long idOrganismo, String administrador, String codRelacionPersonal) throws Exception {
    return this.tipoPersonalNoGenBeanBusiness.findWithSeguridadAndRelacionPersonal(idUsuario, idOrganismo, administrador, codRelacionPersonal);
  }

  public Collection findTipoPersonalByPrestaciones(long idOrganismo, long idUsuario, String administrador) throws Exception {
    return this.tipoPersonalNoGenBeanBusiness.findByPrestaciones(idOrganismo, idUsuario, administrador);
  }

  public Collection findTipoPersonalByArc(long idOrganismo, long idUsuario, String administrador) throws Exception
  {
    return this.tipoPersonalNoGenBeanBusiness.findByArc(idOrganismo, idUsuario, administrador);
  }

  public Collection findConceptoTipoPersonalByTipoPersonalAsignaciones(long idTipoPersonal) throws Exception
  {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByTipoPersonalAsignaciones(idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalByTipoPersonalSueldoBasico(long idTipoPersonal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByTipoPersonalSueldoBasico(idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalByTipoPersonalAjusteSueldo(long idTipoPersonal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByTipoPersonalAjusteSueldo(idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalByTipoPersonalCompensacion(long idTipoPersonal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByTipoPersonalCompensacion(idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalByTipoPersonalPrimasCargo(long idTipoPersonal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByTipoPersonalPrimasCargo(idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalByTipoPersonalPrimasTrabajador(long idTipoPersonal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByTipoPersonalPrimasTrabajador(idTipoPersonal);
  }

  public Collection findTipoPersonalForContratados(long idOrganismo) throws Exception {
    return this.tipoPersonalNoGenBeanBusiness.findForContratados(idOrganismo);
  }

  public int findLastNumeroCampoByDisquete(long idDisquete, String tipoRegistro) throws Exception {
    return this.detalleDisqueteNoGenBeanBusiness.findLastNumeroCampoByDisquete(idDisquete, tipoRegistro);
  }

  public Collection findConceptoTipoPersonalForAportePatronal(long idTipoPersonal, String aportePatronal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findForAportePatronal(idTipoPersonal, aportePatronal);
  }

  public Collection findConceptoCargoByCargo(long idCargo, String excluir, String automaticoIngreso) throws Exception {
    return this.conceptoCargoNoGenBeanBusiness.findByCargo(idCargo, excluir, automaticoIngreso);
  }

  public Collection findConceptoTipoPersonalByRecalculo(long idTipoPersonal, String recalculo) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByRecalculo(idTipoPersonal, recalculo);
  }

  public Collection findConceptoTipoPersonalForConceptoCuenta(long idTipoPersonal, long idConceptoTipoPersonal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findForConceptoCuenta(idTipoPersonal, idConceptoTipoPersonal);
  }

  public Collection findConceptoTipoPersonalForConceptoCuenta(long idTipoPersonal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findForConceptoCuenta(idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalByIdTipoPersonal(long idTipoPersonal) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByIdTipoPersonal(idTipoPersonal);
  }

  public Semana findSemanaByAnioMesSemana(long idGrupoNomina, int anio, String mes, int semanaMes) throws Exception {
    return this.semanaNoGenBeanBusiness.findByAnioMesSemana(idGrupoNomina, anio, mes, semanaMes);
  }

  public Collection findBancoAll() throws Exception {
    return this.bancoNoGenBeanBusiness.findAll();
  }

  public Collection findTipoPersonalByIdRegistro(long idRegistro) throws Exception {
    return this.tipoPersonalNoGenBeanBusiness.findByIdRegistro(idRegistro);
  }

  public Collection findTipoPersonalPensionadoJubilado() throws Exception {
    return this.tipoPersonalNoGenBeanBusiness.findPensionadoJubilado();
  }

  public TarifaAri findTarifaAriByTarifaAproximada(double tarifa) throws Exception {
    return this.tarifaAriNoGenBeanBusiness.findByTarifaAproximada(tarifa);
  }

  public EscalaCuadroOnapre findEscalaCuadroOnapreByCodigoAproximada(String codigo)
    throws Exception
  {
    return this.escalaCuadroOnapreNoGenBeanBusiness.findByCodigoAproximada(codigo);
  }

  public Collection findTipoPersonalByConcepto(long idUsuario, long idOrganismo, String administrador, String codConcepto)
    throws Exception
  {
    return this.tipoPersonalNoGenBeanBusiness.findByConcepto(idUsuario, idOrganismo, administrador, codConcepto);
  }

  public Collection findConceptoTipoPersonalBySobretiempo(long idTipoPersonal, String sobretiempo) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findBySobretiempo(idTipoPersonal, sobretiempo);
  }

  public Collection findTipoPersonalByPersonal(long idPersonal, String estatus) throws Exception {
    return this.tipoPersonalNoGenBeanBusiness.findByPersonal(idPersonal, estatus);
  }

  public double verificarTopeSobretiempo(long idConceptoTipoPersonal, long idTrabajador, int mes, int anio, double unidadesAgregar) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.verificarTopeSobretiempo(idConceptoTipoPersonal, idTrabajador, mes, anio, unidadesAgregar);
  }

  public Collection findConceptoTipoPersonalByBeneficio(long idTipoPersonal, String beneficio) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByBeneficio(idTipoPersonal, beneficio);
  }

  public Collection findGrupoNominaWithSeguridad(long idUsuario, long idOrganismo, String administrador) throws Exception {
    return this.grupoNominaNoGenBeanBusiness.findWithSeguridad(idUsuario, idOrganismo, administrador);
  }

  public Collection findConceptoTipoPersonalByDistribucion(long idTipoPersonal, String distribucion) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByDistribucion(idTipoPersonal, distribucion);
  }

  public Collection findConceptoTipoPersonalByRetroactivo(long idTipoPersonal, String retroactivo) throws Exception {
    return this.conceptoTipoPersonalNoGenBeanBusiness.findByRetroactivo(idTipoPersonal, retroactivo);
  }

  public Collection findTipoPersonalByCategoriaPresupuesto(long idCategoriaPresupuesto) throws Exception {
    return this.tipoPersonalNoGenBeanBusiness.findByCategoriaPresupuesto(idCategoriaPresupuesto);
  }

  public Collection findTipoPersonalWithSeguridadDocenteIngreso(long idUsuario, long idOrganismo, String administrador) throws Exception
  {
    return this.tipoPersonalNoGenBeanBusiness.findWithSeguridadDocenteIngreso(idUsuario, idOrganismo, administrador);
  }

  public Collection findTipoPersonalWithSeguridadRelacionCategoriaAprobacionMpd(long idUsuario, long idOrganismo, String administrador, String codRelacionPersonal, String codCategoriaPersonal, String aprobacionMpd) throws Exception {
    return this.tipoPersonalNoGenBeanBusiness.findWithSeguridadRelacionCategoriaAprobacionMpd(idUsuario, idOrganismo, administrador, codRelacionPersonal, codCategoriaPersonal, aprobacionMpd);
  }

  public Collection findTipoPersonalWithSeguridadForViejoRegimen(long idUsuario, long idOrganismo, String administrador) throws Exception
  {
    return this.tipoPersonalNoGenBeanBusiness.findWithSeguridadForViejoRegimen(idUsuario, idOrganismo, administrador);
  }

  public Collection findTipoPersonalWithSeguridadForInteresAdicional(long idUsuario, long idOrganismo, String administrador, String estatus) throws Exception
  {
    return this.tipoPersonalNoGenBeanBusiness.findWithSeguridadForInteresAdicional(idUsuario, idOrganismo, administrador, estatus);
  }

  public Collection findTipoPersonalWithSeguridadAndCriterio(long idUsuario, long idOrganismo, String administrador, String criterio) throws Exception
  {
    return this.tipoPersonalNoGenBeanBusiness.findWithSeguridadAndCriterio(idUsuario, idOrganismo, administrador, criterio);
  }

  public Collection findCategoriaPresupuestoByCestaTicket(long idUsuario, long idOrganismo, String administrador) throws Exception {
    return this.categoriaPresupuestoNoGenBeanBusiness.findByCestaTicket(idUsuario, idOrganismo, administrador);
  }
}