package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ParametroVariosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ParametroVariosForm.class.getName());
  private ParametroVarios parametroVarios;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showParametroVariosByTipoPersonal;
  private String findSelectTipoPersonal;
  private Collection findColTipoPersonal;
  private Collection colTipoPersonal;
  private Collection colGrupoOrganismo;
  private String selectTipoPersonal;
  private String selectGrupoOrganismo;
  private Object stateResultParametroVariosByTipoPersonal = null;

  public String getFindSelectTipoPersonal()
  {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.parametroVarios.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.parametroVarios.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectGrupoOrganismo() {
    return this.selectGrupoOrganismo;
  }
  public void setSelectGrupoOrganismo(String valGrupoOrganismo) {
    Iterator iterator = this.colGrupoOrganismo.iterator();
    GrupoOrganismo grupoOrganismo = null;
    this.parametroVarios.setGrupoOrganismo(null);
    while (iterator.hasNext()) {
      grupoOrganismo = (GrupoOrganismo)iterator.next();
      if (String.valueOf(grupoOrganismo.getIdGrupoOrganismo()).equals(
        valGrupoOrganismo)) {
        this.parametroVarios.setGrupoOrganismo(
          grupoOrganismo);
        break;
      }
    }
    this.selectGrupoOrganismo = valGrupoOrganismo;
  }
  public Collection getResult() {
    return this.result;
  }

  public ParametroVarios getParametroVarios() {
    if (this.parametroVarios == null) {
      this.parametroVarios = new ParametroVarios();
    }
    return this.parametroVarios;
  }

  public ParametroVariosForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColGrupoOrganismo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoOrganismo.iterator();
    GrupoOrganismo grupoOrganismo = null;
    while (iterator.hasNext()) {
      grupoOrganismo = (GrupoOrganismo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoOrganismo.getIdGrupoOrganismo()), 
        grupoOrganismo.toString()));
    }
    return col;
  }

  public Collection getListNuevoRegimen() {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAlicuotaBfaPrestac() {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTipoCalculo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_CALCULO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAlicuotaBfaBvac() {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCalculoAlicuotaBfa() {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_ALICUOTA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAlicuotaBvacPrestac() {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAlicuotaBvacBfa()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAniversarioDisfrute()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_PAGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListBonoExtra() {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListSumoApn() {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListRegimenDerogadoProcesado()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListInteresesAdicionales() {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAusenciaInjustificada() {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAlicuotaBonoPetrolero() {
    Collection col = new ArrayList();

    Iterator iterEntry = ParametroVarios.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colGrupoOrganismo = 
        this.estructuraFacade.findGrupoOrganismoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findParametroVariosByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findParametroVariosByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showParametroVariosByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showParametroVariosByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;

    return null;
  }

  public boolean isShowParametroVariosByTipoPersonal() {
    return this.showParametroVariosByTipoPersonal;
  }

  public String selectParametroVarios()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectGrupoOrganismo = null;

    long idParametroVarios = 
      Long.parseLong((String)requestParameterMap.get("idParametroVarios"));
    try
    {
      this.parametroVarios = 
        this.definicionesFacade.findParametroVariosById(
        idParametroVarios);
      if (this.parametroVarios.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.parametroVarios.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.parametroVarios.getGrupoOrganismo() != null) {
        this.selectGrupoOrganismo = 
          String.valueOf(this.parametroVarios.getGrupoOrganismo().getIdGrupoOrganismo());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.parametroVarios = null;
    this.showParametroVariosByTipoPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.parametroVarios.getFechaAperturaFideicomiso() != null) && 
      (this.parametroVarios.getFechaAperturaFideicomiso().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Apertura Fideicomiso no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addParametroVarios(
          this.parametroVarios);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateParametroVarios(
          this.parametroVarios);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteParametroVarios(
        this.parametroVarios);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.parametroVarios = new ParametroVarios();

    this.selectTipoPersonal = null;

    this.selectGrupoOrganismo = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.parametroVarios.setIdParametroVarios(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.ParametroVarios"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.parametroVarios = new ParametroVarios();
    return "cancel";
  }

  public boolean isShowConstantePetroleroAAux()
  {
    try
    {
      return this.parametroVarios.getAlicuotaBonoPetrolero().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowConstantePetroleroBAux()
  {
    try {
      return this.parametroVarios.getAlicuotaBonoPetrolero().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowConstantePetroleroCAux()
  {
    try {
      return this.parametroVarios.getAlicuotaBonoPetrolero().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}