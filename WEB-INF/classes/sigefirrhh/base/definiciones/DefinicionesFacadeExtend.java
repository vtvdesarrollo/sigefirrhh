package sigefirrhh.base.definiciones;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class DefinicionesFacadeExtend extends DefinicionesFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DefinicionesBusinessExtend definicionesBusinessExtend = new DefinicionesBusinessExtend();

  public Collection findTipoPersonalByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusinessExtend.findTipoPersonalByGrupoNomina(idGrupoNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByCodConcepto(long idTipoPersonal, String codConcepto) throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusinessExtend.findConceptoTipoPersonalByCodConcepto(idTipoPersonal, codConcepto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoAsociadoByIdTipoPersonal(long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusinessExtend.findConceptoAsociadoByIdTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoAsociadoByConceptoTipoPersonal(long idConceptoTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusinessExtend.findConceptoAsociadoByConceptoTipoPersonal(idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFrecuenciaPagoByTipoPersonal(long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusinessExtend.findFrecuenciaPagoByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFrecuenciaPagoByOrganismoMayorA10(long idOrganismo) throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusinessExtend.findFrecuenciaPagoByOrganismoMayorA10(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public FrecuenciaTipoPersonal findFrecuenciaTipoPersonalByTipoPersonal(long idFrecuenciaPago, long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusinessExtend.findFrecuenciaTipoPersonalByTipoPersonal(idFrecuenciaPago, idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public FrecuenciaTipoPersonal findFrecuenciaTipoPersonalByTipoPersonal(int codFrecuenciaPago, long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusinessExtend.findFrecuenciaTipoPersonalByTipoPersonal(codFrecuenciaPago, idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByTipoPersonalAndTipoPrestamo(long idTipoPersonal, String tipoPrestamo) throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusinessExtend.findConceptoTipoPersonalByTipoPersonalAndTipoPrestamo(idTipoPersonal, tipoPrestamo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalByManejaRac(String manejaRac, long idUsuario, String administrador) throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusinessExtend.findTipoPersonalByManejaRac(manejaRac, idUsuario, administrador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalByAprobacionMpd(String aprobacionMpd, long idUsuario, String administrador) throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusinessExtend.findTipoPersonalByAprobacionMpd(aprobacionMpd, idUsuario, administrador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalByManejaRacAndAprobacionMpd(String manejaRac, String aprobacionMpd, long idUsuario, String administrador) throws Exception
  {
    try { this.txn.open();

      return this.definicionesBusinessExtend.findTipoPersonalByAprobacionMpd(aprobacionMpd, idUsuario, administrador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findTipoPersonalByManejaRacAprobacionMpdClasificacionPersonal(String manejaRac, String aprobacionMpd, long idClasificacionPersonal, long idUsuario, String administrador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusinessExtend.findTipoPersonalByManejaRacAprobacionMpdClasificacionPersonal(manejaRac, aprobacionMpd, idClasificacionPersonal, idUsuario, administrador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalAsignacionesByTipoPersonalAndTipoPrestamo(long idTipoPersonal, String tipoPrestamo) throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusinessExtend.findConceptoTipoPersonalAsignacionesByTipoPersonalAndTipoPrestamo(idTipoPersonal, tipoPrestamo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}