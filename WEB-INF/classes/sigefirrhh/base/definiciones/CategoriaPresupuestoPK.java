package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class CategoriaPresupuestoPK
  implements Serializable
{
  public long idCategoriaPresupuesto;

  public CategoriaPresupuestoPK()
  {
  }

  public CategoriaPresupuestoPK(long idCategoriaPresupuesto)
  {
    this.idCategoriaPresupuesto = idCategoriaPresupuesto;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CategoriaPresupuestoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CategoriaPresupuestoPK thatPK)
  {
    return 
      this.idCategoriaPresupuesto == thatPK.idCategoriaPresupuesto;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCategoriaPresupuesto)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCategoriaPresupuesto);
  }
}