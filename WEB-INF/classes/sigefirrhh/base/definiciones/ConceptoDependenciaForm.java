package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.CaracteristicaDependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.TipoCaracteristica;
import sigefirrhh.login.LoginSession;

public class ConceptoDependenciaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoDependenciaForm.class.getName());
  private ConceptoDependencia conceptoDependencia;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showConceptoDependenciaByCaracteristicaDependencia;
  private String findSelectTipoCaracteristicaForCaracteristicaDependencia;
  private String findSelectCaracteristicaDependencia;
  private Collection findColTipoCaracteristicaForCaracteristicaDependencia;
  private Collection findColCaracteristicaDependencia;
  private Collection colTipoCaracteristicaForCaracteristicaDependencia;
  private Collection colCaracteristicaDependencia;
  private Collection colTipoPersonalForConceptoTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private String selectTipoCaracteristicaForCaracteristicaDependencia;
  private String selectCaracteristicaDependencia;
  private String selectTipoPersonalForConceptoTipoPersonal;
  private String selectConceptoTipoPersonal;
  private Object stateResultConceptoDependenciaByCaracteristicaDependencia = null;

  public Collection getFindColTipoCaracteristicaForCaracteristicaDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoCaracteristicaForCaracteristicaDependencia.iterator();
    TipoCaracteristica tipoCaracteristicaForCaracteristicaDependencia = null;
    while (iterator.hasNext()) {
      tipoCaracteristicaForCaracteristicaDependencia = (TipoCaracteristica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCaracteristicaForCaracteristicaDependencia.getIdTipoCaracteristica()), 
        tipoCaracteristicaForCaracteristicaDependencia.toString()));
    }
    return col;
  }
  public String getFindSelectTipoCaracteristicaForCaracteristicaDependencia() {
    return this.findSelectTipoCaracteristicaForCaracteristicaDependencia;
  }
  public void setFindSelectTipoCaracteristicaForCaracteristicaDependencia(String valTipoCaracteristicaForCaracteristicaDependencia) {
    this.findSelectTipoCaracteristicaForCaracteristicaDependencia = valTipoCaracteristicaForCaracteristicaDependencia;
  }
  public void findChangeTipoCaracteristicaForCaracteristicaDependencia(ValueChangeEvent event) {
    long idTipoCaracteristica = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColCaracteristicaDependencia = null;
      if (idTipoCaracteristica > 0L)
        this.findColCaracteristicaDependencia = 
          this.estructuraFacade.findCaracteristicaDependenciaByTipoCaracteristica(
          idTipoCaracteristica);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowTipoCaracteristicaForCaracteristicaDependencia() { return this.findColTipoCaracteristicaForCaracteristicaDependencia != null; }

  public String getFindSelectCaracteristicaDependencia() {
    return this.findSelectCaracteristicaDependencia;
  }
  public void setFindSelectCaracteristicaDependencia(String valCaracteristicaDependencia) {
    this.findSelectCaracteristicaDependencia = valCaracteristicaDependencia;
  }

  public Collection getFindColCaracteristicaDependencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCaracteristicaDependencia.iterator();
    CaracteristicaDependencia caracteristicaDependencia = null;
    while (iterator.hasNext()) {
      caracteristicaDependencia = (CaracteristicaDependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(caracteristicaDependencia.getIdCaracteristicaDependencia()), 
        caracteristicaDependencia.toString()));
    }
    return col;
  }
  public boolean isFindShowCaracteristicaDependencia() {
    return this.findColCaracteristicaDependencia != null;
  }

  public String getSelectTipoCaracteristicaForCaracteristicaDependencia()
  {
    return this.selectTipoCaracteristicaForCaracteristicaDependencia;
  }
  public void setSelectTipoCaracteristicaForCaracteristicaDependencia(String valTipoCaracteristicaForCaracteristicaDependencia) {
    this.selectTipoCaracteristicaForCaracteristicaDependencia = valTipoCaracteristicaForCaracteristicaDependencia;
  }
  public void changeTipoCaracteristicaForCaracteristicaDependencia(ValueChangeEvent event) {
    long idTipoCaracteristica = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCaracteristicaDependencia = null;
      if (idTipoCaracteristica > 0L) {
        this.colCaracteristicaDependencia = 
          this.estructuraFacade.findCaracteristicaDependenciaByTipoCaracteristica(
          idTipoCaracteristica);
      } else {
        this.selectCaracteristicaDependencia = null;
        this.conceptoDependencia.setCaracteristicaDependencia(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCaracteristicaDependencia = null;
      this.conceptoDependencia.setCaracteristicaDependencia(
        null);
    }
  }

  public boolean isShowTipoCaracteristicaForCaracteristicaDependencia() { return this.colTipoCaracteristicaForCaracteristicaDependencia != null; }

  public String getSelectCaracteristicaDependencia() {
    return this.selectCaracteristicaDependencia;
  }
  public void setSelectCaracteristicaDependencia(String valCaracteristicaDependencia) {
    Iterator iterator = this.colCaracteristicaDependencia.iterator();
    CaracteristicaDependencia caracteristicaDependencia = null;
    this.conceptoDependencia.setCaracteristicaDependencia(null);
    while (iterator.hasNext()) {
      caracteristicaDependencia = (CaracteristicaDependencia)iterator.next();
      if (String.valueOf(caracteristicaDependencia.getIdCaracteristicaDependencia()).equals(
        valCaracteristicaDependencia)) {
        this.conceptoDependencia.setCaracteristicaDependencia(
          caracteristicaDependencia);
        break;
      }
    }
    this.selectCaracteristicaDependencia = valCaracteristicaDependencia;
  }
  public boolean isShowCaracteristicaDependencia() {
    return this.colCaracteristicaDependencia != null;
  }
  public String getSelectTipoPersonalForConceptoTipoPersonal() {
    return this.selectTipoPersonalForConceptoTipoPersonal;
  }
  public void setSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.selectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }
  public void changeTipoPersonalForConceptoTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colConceptoTipoPersonal = null;
      if (idTipoPersonal > 0L) {
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
      } else {
        this.selectConceptoTipoPersonal = null;
        this.conceptoDependencia.setConceptoTipoPersonal(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectConceptoTipoPersonal = null;
      this.conceptoDependencia.setConceptoTipoPersonal(
        null);
    }
  }

  public boolean isShowTipoPersonalForConceptoTipoPersonal() { return this.colTipoPersonalForConceptoTipoPersonal != null; }

  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    this.conceptoDependencia.setConceptoTipoPersonal(null);
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()).equals(
        valConceptoTipoPersonal)) {
        this.conceptoDependencia.setConceptoTipoPersonal(
          conceptoTipoPersonal);
        break;
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public boolean isShowConceptoTipoPersonal() {
    return this.colConceptoTipoPersonal != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoDependencia getConceptoDependencia() {
    if (this.conceptoDependencia == null) {
      this.conceptoDependencia = new ConceptoDependencia();
    }
    return this.conceptoDependencia;
  }

  public ConceptoDependenciaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoCaracteristicaForCaracteristicaDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoCaracteristicaForCaracteristicaDependencia.iterator();
    TipoCaracteristica tipoCaracteristicaForCaracteristicaDependencia = null;
    while (iterator.hasNext()) {
      tipoCaracteristicaForCaracteristicaDependencia = (TipoCaracteristica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCaracteristicaForCaracteristicaDependencia.getIdTipoCaracteristica()), 
        tipoCaracteristicaForCaracteristicaDependencia.toString()));
    }
    return col;
  }

  public Collection getColCaracteristicaDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCaracteristicaDependencia.iterator();
    CaracteristicaDependencia caracteristicaDependencia = null;
    while (iterator.hasNext()) {
      caracteristicaDependencia = (CaracteristicaDependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(caracteristicaDependencia.getIdCaracteristicaDependencia()), 
        caracteristicaDependencia.toString()));
    }
    return col;
  }

  public Collection getColTipoPersonalForConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getListAutomaticoIngreso()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoDependencia.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoCaracteristicaForCaracteristicaDependencia = 
        this.estructuraFacade.findAllTipoCaracteristica();

      this.colTipoCaracteristicaForCaracteristicaDependencia = 
        this.estructuraFacade.findAllTipoCaracteristica();
      this.colTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConceptoDependenciaByCaracteristicaDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findConceptoDependenciaByCaracteristicaDependencia(Long.valueOf(this.findSelectCaracteristicaDependencia).longValue());
      this.showConceptoDependenciaByCaracteristicaDependencia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoDependenciaByCaracteristicaDependencia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoCaracteristicaForCaracteristicaDependencia = null;
    this.findSelectCaracteristicaDependencia = null;

    return null;
  }

  public boolean isShowConceptoDependenciaByCaracteristicaDependencia() {
    return this.showConceptoDependenciaByCaracteristicaDependencia;
  }

  public String selectConceptoDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectCaracteristicaDependencia = null;
    this.selectTipoCaracteristicaForCaracteristicaDependencia = null;

    this.selectConceptoTipoPersonal = null;
    this.selectTipoPersonalForConceptoTipoPersonal = null;

    long idConceptoDependencia = 
      Long.parseLong((String)requestParameterMap.get("idConceptoDependencia"));
    try
    {
      this.conceptoDependencia = 
        this.definicionesFacade.findConceptoDependenciaById(
        idConceptoDependencia);
      if (this.conceptoDependencia.getCaracteristicaDependencia() != null) {
        this.selectCaracteristicaDependencia = 
          String.valueOf(this.conceptoDependencia.getCaracteristicaDependencia().getIdCaracteristicaDependencia());
      }
      if (this.conceptoDependencia.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.conceptoDependencia.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }

      CaracteristicaDependencia caracteristicaDependencia = null;
      TipoCaracteristica tipoCaracteristicaForCaracteristicaDependencia = null;
      ConceptoTipoPersonal conceptoTipoPersonal = null;
      TipoPersonal tipoPersonalForConceptoTipoPersonal = null;

      if (this.conceptoDependencia.getCaracteristicaDependencia() != null) {
        long idCaracteristicaDependencia = 
          this.conceptoDependencia.getCaracteristicaDependencia().getIdCaracteristicaDependencia();
        this.selectCaracteristicaDependencia = String.valueOf(idCaracteristicaDependencia);
        caracteristicaDependencia = this.estructuraFacade.findCaracteristicaDependenciaById(
          idCaracteristicaDependencia);
        this.colCaracteristicaDependencia = this.estructuraFacade.findCaracteristicaDependenciaByTipoCaracteristica(
          caracteristicaDependencia.getTipoCaracteristica().getIdTipoCaracteristica());

        long idTipoCaracteristicaForCaracteristicaDependencia = 
          this.conceptoDependencia.getCaracteristicaDependencia().getTipoCaracteristica().getIdTipoCaracteristica();
        this.selectTipoCaracteristicaForCaracteristicaDependencia = String.valueOf(idTipoCaracteristicaForCaracteristicaDependencia);
        tipoCaracteristicaForCaracteristicaDependencia = 
          this.estructuraFacade.findTipoCaracteristicaById(
          idTipoCaracteristicaForCaracteristicaDependencia);
        this.colTipoCaracteristicaForCaracteristicaDependencia = 
          this.estructuraFacade.findAllTipoCaracteristica();
      }
      if (this.conceptoDependencia.getConceptoTipoPersonal() != null) {
        long idConceptoTipoPersonal = 
          this.conceptoDependencia.getConceptoTipoPersonal().getIdConceptoTipoPersonal();
        this.selectConceptoTipoPersonal = String.valueOf(idConceptoTipoPersonal);
        conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(
          idConceptoTipoPersonal);
        this.colConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          conceptoTipoPersonal.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonalForConceptoTipoPersonal = 
          this.conceptoDependencia.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal();
        this.selectTipoPersonalForConceptoTipoPersonal = String.valueOf(idTipoPersonalForConceptoTipoPersonal);
        tipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findTipoPersonalById(
          idTipoPersonalForConceptoTipoPersonal);
        this.colTipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findAllTipoPersonal();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.conceptoDependencia = null;
    this.showConceptoDependenciaByCaracteristicaDependencia = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addConceptoDependencia(
          this.conceptoDependencia);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateConceptoDependencia(
          this.conceptoDependencia);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteConceptoDependencia(
        this.conceptoDependencia);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.conceptoDependencia = new ConceptoDependencia();

    this.selectCaracteristicaDependencia = null;

    this.selectTipoCaracteristicaForCaracteristicaDependencia = null;

    this.selectConceptoTipoPersonal = null;

    this.selectTipoPersonalForConceptoTipoPersonal = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoDependencia.setIdConceptoDependencia(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.ConceptoDependencia"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.conceptoDependencia = new ConceptoDependencia();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}