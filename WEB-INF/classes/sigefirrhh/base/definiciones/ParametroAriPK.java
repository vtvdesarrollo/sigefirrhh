package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ParametroAriPK
  implements Serializable
{
  public long idParametroAri;

  public ParametroAriPK()
  {
  }

  public ParametroAriPK(long idParametroAri)
  {
    this.idParametroAri = idParametroAri;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroAriPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroAriPK thatPK)
  {
    return 
      this.idParametroAri == thatPK.idParametroAri;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroAri)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroAri);
  }
}