package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.base.estructura.GrupoOrganismoBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.bienestar.ticketAlimentacion.GrupoTicket;
import sigefirrhh.bienestar.ticketAlimentacion.GrupoTicketBeanBusiness;

public class TipoPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoPersonal(TipoPersonal tipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoPersonal tipoPersonalNew = 
      (TipoPersonal)BeanUtils.cloneBean(
      tipoPersonal);

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (tipoPersonalNew.getClasificacionPersonal() != null) {
      tipoPersonalNew.setClasificacionPersonal(
        clasificacionPersonalBeanBusiness.findClasificacionPersonalById(
        tipoPersonalNew.getClasificacionPersonal().getIdClasificacionPersonal()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (tipoPersonalNew.getGrupoNomina() != null) {
      tipoPersonalNew.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        tipoPersonalNew.getGrupoNomina().getIdGrupoNomina()));
    }

    GrupoOrganismoBeanBusiness grupoOrganismoBeanBusiness = new GrupoOrganismoBeanBusiness();

    if (tipoPersonalNew.getGrupoOrganismo() != null) {
      tipoPersonalNew.setGrupoOrganismo(
        grupoOrganismoBeanBusiness.findGrupoOrganismoById(
        tipoPersonalNew.getGrupoOrganismo().getIdGrupoOrganismo()));
    }

    GrupoTicketBeanBusiness grupoTicketBeanBusiness = new GrupoTicketBeanBusiness();

    if (tipoPersonalNew.getGrupoTicket() != null) {
      tipoPersonalNew.setGrupoTicket(
        grupoTicketBeanBusiness.findGrupoTicketById(
        tipoPersonalNew.getGrupoTicket().getIdGrupoTicket()));
    }

    TurnoBeanBusiness turnoBeanBusiness = new TurnoBeanBusiness();

    if (tipoPersonalNew.getTurno() != null) {
      tipoPersonalNew.setTurno(
        turnoBeanBusiness.findTurnoById(
        tipoPersonalNew.getTurno().getIdTurno()));
    }

    BancoBeanBusiness bancoNominaBeanBusiness = new BancoBeanBusiness();

    if (tipoPersonalNew.getBancoNomina() != null) {
      tipoPersonalNew.setBancoNomina(
        bancoNominaBeanBusiness.findBancoById(
        tipoPersonalNew.getBancoNomina().getIdBanco()));
    }

    BancoBeanBusiness bancoLphBeanBusiness = new BancoBeanBusiness();

    if (tipoPersonalNew.getBancoLph() != null) {
      tipoPersonalNew.setBancoLph(
        bancoLphBeanBusiness.findBancoById(
        tipoPersonalNew.getBancoLph().getIdBanco()));
    }

    BancoBeanBusiness bancoFidBeanBusiness = new BancoBeanBusiness();

    if (tipoPersonalNew.getBancoFid() != null) {
      tipoPersonalNew.setBancoFid(
        bancoFidBeanBusiness.findBancoById(
        tipoPersonalNew.getBancoFid().getIdBanco()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (tipoPersonalNew.getOrganismo() != null) {
      tipoPersonalNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        tipoPersonalNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(tipoPersonalNew);
  }

  public void updateTipoPersonal(TipoPersonal tipoPersonal) throws Exception
  {
    TipoPersonal tipoPersonalModify = 
      findTipoPersonalById(tipoPersonal.getIdTipoPersonal());

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (tipoPersonal.getClasificacionPersonal() != null) {
      tipoPersonal.setClasificacionPersonal(
        clasificacionPersonalBeanBusiness.findClasificacionPersonalById(
        tipoPersonal.getClasificacionPersonal().getIdClasificacionPersonal()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (tipoPersonal.getGrupoNomina() != null) {
      tipoPersonal.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        tipoPersonal.getGrupoNomina().getIdGrupoNomina()));
    }

    GrupoOrganismoBeanBusiness grupoOrganismoBeanBusiness = new GrupoOrganismoBeanBusiness();

    if (tipoPersonal.getGrupoOrganismo() != null) {
      tipoPersonal.setGrupoOrganismo(
        grupoOrganismoBeanBusiness.findGrupoOrganismoById(
        tipoPersonal.getGrupoOrganismo().getIdGrupoOrganismo()));
    }

    GrupoTicketBeanBusiness grupoTicketBeanBusiness = new GrupoTicketBeanBusiness();

    if (tipoPersonal.getGrupoTicket() != null) {
      tipoPersonal.setGrupoTicket(
        grupoTicketBeanBusiness.findGrupoTicketById(
        tipoPersonal.getGrupoTicket().getIdGrupoTicket()));
    }

    TurnoBeanBusiness turnoBeanBusiness = new TurnoBeanBusiness();

    if (tipoPersonal.getTurno() != null) {
      tipoPersonal.setTurno(
        turnoBeanBusiness.findTurnoById(
        tipoPersonal.getTurno().getIdTurno()));
    }

    BancoBeanBusiness bancoNominaBeanBusiness = new BancoBeanBusiness();

    if (tipoPersonal.getBancoNomina() != null) {
      tipoPersonal.setBancoNomina(
        bancoNominaBeanBusiness.findBancoById(
        tipoPersonal.getBancoNomina().getIdBanco()));
    }

    BancoBeanBusiness bancoLphBeanBusiness = new BancoBeanBusiness();

    if (tipoPersonal.getBancoLph() != null) {
      tipoPersonal.setBancoLph(
        bancoLphBeanBusiness.findBancoById(
        tipoPersonal.getBancoLph().getIdBanco()));
    }

    BancoBeanBusiness bancoFidBeanBusiness = new BancoBeanBusiness();

    if (tipoPersonal.getBancoFid() != null) {
      tipoPersonal.setBancoFid(
        bancoFidBeanBusiness.findBancoById(
        tipoPersonal.getBancoFid().getIdBanco()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (tipoPersonal.getOrganismo() != null) {
      tipoPersonal.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        tipoPersonal.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(tipoPersonalModify, tipoPersonal);
  }

  public void deleteTipoPersonal(TipoPersonal tipoPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoPersonal tipoPersonalDelete = 
      findTipoPersonalById(tipoPersonal.getIdTipoPersonal());
    pm.deletePersistent(tipoPersonalDelete);
  }

  public TipoPersonal findTipoPersonalById(long idTipoPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoPersonal == pIdTipoPersonal";
    Query query = pm.newQuery(TipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal");

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoPersonal.iterator();
    return (TipoPersonal)iterator.next();
  }

  public Collection findTipoPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoPersonalExtent = pm.getExtent(
      TipoPersonal.class, true);
    Query query = pm.newQuery(tipoPersonalExtent);
    query.setOrdering("codTipoPersonal ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoPersonal(String codTipoPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoPersonal == pCodTipoPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(TipoPersonal.class, filter);

    query.declareParameters("java.lang.String pCodTipoPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoPersonal", new String(codTipoPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codTipoPersonal ascending");

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(TipoPersonal.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codTipoPersonal ascending");

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(TipoPersonal.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codTipoPersonal ascending");

    Collection colTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoPersonal);

    return colTipoPersonal;
  }
}