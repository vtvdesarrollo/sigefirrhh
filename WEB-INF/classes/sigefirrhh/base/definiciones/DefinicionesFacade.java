package sigefirrhh.base.definiciones;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.jdo.PersistenceManager;

public class DefinicionesFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DefinicionesBusiness definicionesBusiness = new DefinicionesBusiness();

  public void addBanco(Banco banco)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addBanco(banco);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateBanco(Banco banco) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateBanco(banco);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteBanco(Banco banco) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteBanco(banco);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Banco findBancoById(long bancoId) throws Exception
  {
    try { this.txn.open();
      Banco banco = 
        this.definicionesBusiness.findBancoById(bancoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(banco);
      return banco;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllBanco() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllBanco();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findBancoByCodBanco(String codBanco)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findBancoByCodBanco(codBanco);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findBancoByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findBancoByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addBeneficiarioGlobal(BeneficiarioGlobal beneficiarioGlobal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addBeneficiarioGlobal(beneficiarioGlobal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateBeneficiarioGlobal(BeneficiarioGlobal beneficiarioGlobal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateBeneficiarioGlobal(beneficiarioGlobal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteBeneficiarioGlobal(BeneficiarioGlobal beneficiarioGlobal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteBeneficiarioGlobal(beneficiarioGlobal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public BeneficiarioGlobal findBeneficiarioGlobalById(long beneficiarioGlobalId) throws Exception
  {
    try { this.txn.open();
      BeneficiarioGlobal beneficiarioGlobal = 
        this.definicionesBusiness.findBeneficiarioGlobalById(beneficiarioGlobalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(beneficiarioGlobal);
      return beneficiarioGlobal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllBeneficiarioGlobal() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllBeneficiarioGlobal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findBeneficiarioGlobalByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findBeneficiarioGlobalByConceptoTipoPersonal(idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findBeneficiarioGlobalByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findBeneficiarioGlobalByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCategoriaPersonal(CategoriaPersonal categoriaPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addCategoriaPersonal(categoriaPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCategoriaPersonal(CategoriaPersonal categoriaPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateCategoriaPersonal(categoriaPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCategoriaPersonal(CategoriaPersonal categoriaPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteCategoriaPersonal(categoriaPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CategoriaPersonal findCategoriaPersonalById(long categoriaPersonalId) throws Exception
  {
    try { this.txn.open();
      CategoriaPersonal categoriaPersonal = 
        this.definicionesBusiness.findCategoriaPersonalById(categoriaPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(categoriaPersonal);
      return categoriaPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCategoriaPersonal() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllCategoriaPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCategoriaPersonalByCodCategoria(String codCategoria)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findCategoriaPersonalByCodCategoria(codCategoria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCategoriaPersonalByDescCategoria(String descCategoria)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findCategoriaPersonalByDescCategoria(descCategoria);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCategoriaPresupuesto(CategoriaPresupuesto categoriaPresupuesto)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addCategoriaPresupuesto(categoriaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCategoriaPresupuesto(CategoriaPresupuesto categoriaPresupuesto) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateCategoriaPresupuesto(categoriaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCategoriaPresupuesto(CategoriaPresupuesto categoriaPresupuesto) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteCategoriaPresupuesto(categoriaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CategoriaPresupuesto findCategoriaPresupuestoById(long categoriaPresupuestoId) throws Exception
  {
    try { this.txn.open();
      CategoriaPresupuesto categoriaPresupuesto = 
        this.definicionesBusiness.findCategoriaPresupuestoById(categoriaPresupuestoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(categoriaPresupuesto);
      return categoriaPresupuesto;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCategoriaPresupuesto() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllCategoriaPresupuesto();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCategoriaPresupuestoByCodCategoria(String codCategoria)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findCategoriaPresupuestoByCodCategoria(codCategoria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCategoriaPresupuestoByDescCategoria(String descCategoria)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findCategoriaPresupuestoByDescCategoria(descCategoria);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addClasificacionPersonal(ClasificacionPersonal clasificacionPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addClasificacionPersonal(clasificacionPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateClasificacionPersonal(ClasificacionPersonal clasificacionPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateClasificacionPersonal(clasificacionPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteClasificacionPersonal(ClasificacionPersonal clasificacionPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteClasificacionPersonal(clasificacionPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ClasificacionPersonal findClasificacionPersonalById(long clasificacionPersonalId) throws Exception
  {
    try { this.txn.open();
      ClasificacionPersonal clasificacionPersonal = 
        this.definicionesBusiness.findClasificacionPersonalById(clasificacionPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(clasificacionPersonal);
      return clasificacionPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllClasificacionPersonal() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllClasificacionPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findClasificacionPersonalByCategoriaPersonal(long idCategoriaPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findClasificacionPersonalByCategoriaPersonal(idCategoriaPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findClasificacionPersonalByRelacionPersonal(long idRelacionPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findClasificacionPersonalByRelacionPersonal(idRelacionPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConcepto(Concepto concepto)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addConcepto(concepto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConcepto(Concepto concepto) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateConcepto(concepto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConcepto(Concepto concepto) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteConcepto(concepto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Concepto findConceptoById(long conceptoId) throws Exception
  {
    try { this.txn.open();
      Concepto concepto = 
        this.definicionesBusiness.findConceptoById(conceptoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(concepto);
      return concepto;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConcepto() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllConcepto();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoByCodConcepto(String codConcepto, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoByCodConcepto(codConcepto, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoByDescripcion(descripcion, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoByAportePatronal(String aportePatronal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoByAportePatronal(aportePatronal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoAsociado(ConceptoAsociado conceptoAsociado)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addConceptoAsociado(conceptoAsociado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoAsociado(ConceptoAsociado conceptoAsociado) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateConceptoAsociado(conceptoAsociado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoAsociado(ConceptoAsociado conceptoAsociado) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteConceptoAsociado(conceptoAsociado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoAsociado findConceptoAsociadoById(long conceptoAsociadoId) throws Exception
  {
    try { this.txn.open();
      ConceptoAsociado conceptoAsociado = 
        this.definicionesBusiness.findConceptoAsociadoById(conceptoAsociadoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoAsociado);
      return conceptoAsociado;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoAsociado() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllConceptoAsociado();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoAsociadoByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoAsociadoByConceptoTipoPersonal(idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoCargo(ConceptoCargo conceptoCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addConceptoCargo(conceptoCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoCargo(ConceptoCargo conceptoCargo) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateConceptoCargo(conceptoCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoCargo(ConceptoCargo conceptoCargo) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteConceptoCargo(conceptoCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoCargo findConceptoCargoById(long conceptoCargoId) throws Exception
  {
    try { this.txn.open();
      ConceptoCargo conceptoCargo = 
        this.definicionesBusiness.findConceptoCargoById(conceptoCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoCargo);
      return conceptoCargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoCargo() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllConceptoCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoCargoByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoCargoByConceptoTipoPersonal(idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoCargoByCargo(long idCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoCargoByCargo(idCargo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoCargoAnio(ConceptoCargoAnio conceptoCargoAnio)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addConceptoCargoAnio(conceptoCargoAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoCargoAnio(ConceptoCargoAnio conceptoCargoAnio) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateConceptoCargoAnio(conceptoCargoAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoCargoAnio(ConceptoCargoAnio conceptoCargoAnio) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteConceptoCargoAnio(conceptoCargoAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoCargoAnio findConceptoCargoAnioById(long conceptoCargoAnioId) throws Exception
  {
    try { this.txn.open();
      ConceptoCargoAnio conceptoCargoAnio = 
        this.definicionesBusiness.findConceptoCargoAnioById(conceptoCargoAnioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoCargoAnio);
      return conceptoCargoAnio;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoCargoAnio() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllConceptoCargoAnio();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoCargoAnioByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoCargoAnioByConceptoTipoPersonal(idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoCargoAnioByCargo(long idCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoCargoAnioByCargo(idCargo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoDependencia(ConceptoDependencia conceptoDependencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addConceptoDependencia(conceptoDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoDependencia(ConceptoDependencia conceptoDependencia) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateConceptoDependencia(conceptoDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoDependencia(ConceptoDependencia conceptoDependencia) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteConceptoDependencia(conceptoDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoDependencia findConceptoDependenciaById(long conceptoDependenciaId) throws Exception
  {
    try { this.txn.open();
      ConceptoDependencia conceptoDependencia = 
        this.definicionesBusiness.findConceptoDependenciaById(conceptoDependenciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoDependencia);
      return conceptoDependencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoDependencia() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllConceptoDependencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoDependenciaByCaracteristicaDependencia(long idCaracteristicaDependencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoDependenciaByCaracteristicaDependencia(idCaracteristicaDependencia);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addConceptoTipoPersonal(conceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateConceptoTipoPersonal(conceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteConceptoTipoPersonal(conceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoTipoPersonal findConceptoTipoPersonalById(long conceptoTipoPersonalId) throws Exception
  {
    try { this.txn.open();
      ConceptoTipoPersonal conceptoTipoPersonal = 
        this.definicionesBusiness.findConceptoTipoPersonalById(conceptoTipoPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoTipoPersonal);
      return conceptoTipoPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoTipoPersonal() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllConceptoTipoPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoTipoPersonalByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByConcepto(long idConcepto)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoTipoPersonalByConcepto(idConcepto);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoUtilidades(ConceptoUtilidades conceptoUtilidades)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addConceptoUtilidades(conceptoUtilidades);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoUtilidades(ConceptoUtilidades conceptoUtilidades) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateConceptoUtilidades(conceptoUtilidades);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoUtilidades(ConceptoUtilidades conceptoUtilidades) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteConceptoUtilidades(conceptoUtilidades);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoUtilidades findConceptoUtilidadesById(long conceptoUtilidadesId) throws Exception
  {
    try { this.txn.open();
      ConceptoUtilidades conceptoUtilidades = 
        this.definicionesBusiness.findConceptoUtilidadesById(conceptoUtilidadesId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoUtilidades);
      return conceptoUtilidades;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoUtilidades() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllConceptoUtilidades();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoUtilidadesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoUtilidadesByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoVacaciones(ConceptoVacaciones conceptoVacaciones)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addConceptoVacaciones(conceptoVacaciones);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoVacaciones(ConceptoVacaciones conceptoVacaciones) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateConceptoVacaciones(conceptoVacaciones);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoVacaciones(ConceptoVacaciones conceptoVacaciones) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteConceptoVacaciones(conceptoVacaciones);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoVacaciones findConceptoVacacionesById(long conceptoVacacionesId) throws Exception
  {
    try { this.txn.open();
      ConceptoVacaciones conceptoVacaciones = 
        this.definicionesBusiness.findConceptoVacacionesById(conceptoVacacionesId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoVacaciones);
      return conceptoVacaciones;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoVacaciones() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllConceptoVacaciones();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoVacacionesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findConceptoVacacionesByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addContratoColectivo(ContratoColectivo contratoColectivo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addContratoColectivo(contratoColectivo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateContratoColectivo(ContratoColectivo contratoColectivo) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateContratoColectivo(contratoColectivo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteContratoColectivo(ContratoColectivo contratoColectivo) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteContratoColectivo(contratoColectivo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ContratoColectivo findContratoColectivoById(long contratoColectivoId) throws Exception
  {
    try { this.txn.open();
      ContratoColectivo contratoColectivo = 
        this.definicionesBusiness.findContratoColectivoById(contratoColectivoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(contratoColectivo);
      return contratoColectivo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllContratoColectivo() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllContratoColectivo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findContratoColectivoByCodContratoColectivo(String codContratoColectivo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findContratoColectivoByCodContratoColectivo(codContratoColectivo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findContratoColectivoByDenominacion(String denominacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findContratoColectivoByDenominacion(denominacion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCuentaBanco(CuentaBanco cuentaBanco)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addCuentaBanco(cuentaBanco);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCuentaBanco(CuentaBanco cuentaBanco) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateCuentaBanco(cuentaBanco);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCuentaBanco(CuentaBanco cuentaBanco) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteCuentaBanco(cuentaBanco);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CuentaBanco findCuentaBancoById(long cuentaBancoId) throws Exception
  {
    try { this.txn.open();
      CuentaBanco cuentaBanco = 
        this.definicionesBusiness.findCuentaBancoById(cuentaBancoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(cuentaBanco);
      return cuentaBanco;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCuentaBanco() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllCuentaBanco();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCuentaBancoByBanco(long idBanco, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findCuentaBancoByBanco(idBanco, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCuentaBancoByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findCuentaBancoByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDetalleDisquete(DetalleDisquete detalleDisquete)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addDetalleDisquete(detalleDisquete);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDetalleDisquete(DetalleDisquete detalleDisquete) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateDetalleDisquete(detalleDisquete);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDetalleDisquete(DetalleDisquete detalleDisquete) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteDetalleDisquete(detalleDisquete);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DetalleDisquete findDetalleDisqueteById(long detalleDisqueteId) throws Exception
  {
    try { this.txn.open();
      DetalleDisquete detalleDisquete = 
        this.definicionesBusiness.findDetalleDisqueteById(detalleDisqueteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(detalleDisquete);
      return detalleDisquete;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDetalleDisquete() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllDetalleDisquete();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDetalleDisqueteByDisquete(long idDisquete)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findDetalleDisqueteByDisquete(idDisquete);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDiaFeriado(DiaFeriado diaFeriado)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addDiaFeriado(diaFeriado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDiaFeriado(DiaFeriado diaFeriado) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateDiaFeriado(diaFeriado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDiaFeriado(DiaFeriado diaFeriado) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteDiaFeriado(diaFeriado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DiaFeriado findDiaFeriadoById(long diaFeriadoId) throws Exception
  {
    try { this.txn.open();
      DiaFeriado diaFeriado = 
        this.definicionesBusiness.findDiaFeriadoById(diaFeriadoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(diaFeriado);
      return diaFeriado;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDiaFeriado() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllDiaFeriado();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDiaFeriadoByDia(Date dia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findDiaFeriadoByDia(dia);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDisquete(Disquete disquete)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addDisquete(disquete);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDisquete(Disquete disquete) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateDisquete(disquete);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDisquete(Disquete disquete) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteDisquete(disquete);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Disquete findDisqueteById(long disqueteId) throws Exception
  {
    try { this.txn.open();
      Disquete disquete = 
        this.definicionesBusiness.findDisqueteById(disqueteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(disquete);
      return disquete;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDisquete() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllDisquete();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDisqueteByCodDisquete(String codDisquete, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findDisqueteByCodDisquete(codDisquete, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDisqueteByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findDisqueteByDescripcion(descripcion, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDisqueteByTipoDisquete(String tipoDisquete, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findDisqueteByTipoDisquete(tipoDisquete, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDisqueteByIngresos(String ingresos, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findDisqueteByIngresos(ingresos, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDisqueteByEgresos(String egresos, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findDisqueteByEgresos(egresos, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDisqueteByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findDisqueteByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addFirmasReportes(FirmasReportes firmasReportes)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addFirmasReportes(firmasReportes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateFirmasReportes(FirmasReportes firmasReportes) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateFirmasReportes(firmasReportes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteFirmasReportes(FirmasReportes firmasReportes) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteFirmasReportes(firmasReportes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public FirmasReportes findFirmasReportesById(long firmasReportesId) throws Exception
  {
    try { this.txn.open();
      FirmasReportes firmasReportes = 
        this.definicionesBusiness.findFirmasReportesById(firmasReportesId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(firmasReportes);
      return firmasReportes;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllFirmasReportes() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllFirmasReportes();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFirmasReportesByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findFirmasReportesByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFirmasReportesByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findFirmasReportesByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addFrecuenciaPago(FrecuenciaPago frecuenciaPago)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addFrecuenciaPago(frecuenciaPago);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateFrecuenciaPago(FrecuenciaPago frecuenciaPago) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateFrecuenciaPago(frecuenciaPago);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteFrecuenciaPago(FrecuenciaPago frecuenciaPago) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteFrecuenciaPago(frecuenciaPago);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public FrecuenciaPago findFrecuenciaPagoById(long frecuenciaPagoId) throws Exception
  {
    try { this.txn.open();
      FrecuenciaPago frecuenciaPago = 
        this.definicionesBusiness.findFrecuenciaPagoById(frecuenciaPagoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(frecuenciaPago);
      return frecuenciaPago;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllFrecuenciaPago() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllFrecuenciaPago();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFrecuenciaPagoByCodFrecuenciaPago(int codFrecuenciaPago, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findFrecuenciaPagoByCodFrecuenciaPago(codFrecuenciaPago, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFrecuenciaPagoByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findFrecuenciaPagoByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFrecuenciaPagoByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findFrecuenciaPagoByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addFrecuenciaTipoPersonal(FrecuenciaTipoPersonal frecuenciaTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addFrecuenciaTipoPersonal(frecuenciaTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateFrecuenciaTipoPersonal(FrecuenciaTipoPersonal frecuenciaTipoPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateFrecuenciaTipoPersonal(frecuenciaTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteFrecuenciaTipoPersonal(FrecuenciaTipoPersonal frecuenciaTipoPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteFrecuenciaTipoPersonal(frecuenciaTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public FrecuenciaTipoPersonal findFrecuenciaTipoPersonalById(long frecuenciaTipoPersonalId) throws Exception
  {
    try { this.txn.open();
      FrecuenciaTipoPersonal frecuenciaTipoPersonal = 
        this.definicionesBusiness.findFrecuenciaTipoPersonalById(frecuenciaTipoPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(frecuenciaTipoPersonal);
      return frecuenciaTipoPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllFrecuenciaTipoPersonal() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllFrecuenciaTipoPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFrecuenciaTipoPersonalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findFrecuenciaTipoPersonalByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFrecuenciaTipoPersonalByFrecuenciaPago(long idFrecuenciaPago)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findFrecuenciaTipoPersonalByFrecuenciaPago(idFrecuenciaPago);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addGrupoNomina(GrupoNomina grupoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addGrupoNomina(grupoNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateGrupoNomina(GrupoNomina grupoNomina) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateGrupoNomina(grupoNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteGrupoNomina(GrupoNomina grupoNomina) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteGrupoNomina(grupoNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public GrupoNomina findGrupoNominaById(long grupoNominaId) throws Exception
  {
    try { this.txn.open();
      GrupoNomina grupoNomina = 
        this.definicionesBusiness.findGrupoNominaById(grupoNominaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(grupoNomina);
      return grupoNomina;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllGrupoNomina() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllGrupoNomina();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoNominaByCodGrupoNomina(int codGrupoNomina, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findGrupoNominaByCodGrupoNomina(codGrupoNomina, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoNominaByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findGrupoNominaByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoNominaByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findGrupoNominaByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addMes(Mes mes)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addMes(mes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateMes(Mes mes) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateMes(mes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteMes(Mes mes) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteMes(mes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Mes findMesById(long mesId) throws Exception
  {
    try { this.txn.open();
      Mes mes = 
        this.definicionesBusiness.findMesById(mesId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(mes);
      return mes;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllMes() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllMes();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMesByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findMesByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParametroAri(ParametroAri parametroAri)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addParametroAri(parametroAri);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParametroAri(ParametroAri parametroAri) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateParametroAri(parametroAri);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParametroAri(ParametroAri parametroAri) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteParametroAri(parametroAri);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ParametroAri findParametroAriById(long parametroAriId) throws Exception
  {
    try { this.txn.open();
      ParametroAri parametroAri = 
        this.definicionesBusiness.findParametroAriById(parametroAriId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parametroAri);
      return parametroAri;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParametroAri() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllParametroAri();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParametroAriByUnidadTributaria(double unidadTributaria)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findParametroAriByUnidadTributaria(unidadTributaria);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParametroGobierno(ParametroGobierno parametroGobierno)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addParametroGobierno(parametroGobierno);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParametroGobierno(ParametroGobierno parametroGobierno) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateParametroGobierno(parametroGobierno);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParametroGobierno(ParametroGobierno parametroGobierno) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteParametroGobierno(parametroGobierno);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ParametroGobierno findParametroGobiernoById(long parametroGobiernoId) throws Exception
  {
    try { this.txn.open();
      ParametroGobierno parametroGobierno = 
        this.definicionesBusiness.findParametroGobiernoById(parametroGobiernoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parametroGobierno);
      return parametroGobierno;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParametroGobierno() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllParametroGobierno();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParametroGobiernoByGrupoOrganismo(long idGrupoOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findParametroGobiernoByGrupoOrganismo(idGrupoOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParametroReportes(ParametroReportes parametroReportes)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addParametroReportes(parametroReportes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParametroReportes(ParametroReportes parametroReportes) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateParametroReportes(parametroReportes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParametroReportes(ParametroReportes parametroReportes) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteParametroReportes(parametroReportes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ParametroReportes findParametroReportesById(long parametroReportesId) throws Exception
  {
    try { this.txn.open();
      ParametroReportes parametroReportes = 
        this.definicionesBusiness.findParametroReportesById(parametroReportesId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parametroReportes);
      return parametroReportes;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParametroReportes() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllParametroReportes();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParametroVarios(ParametroVarios parametroVarios)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addParametroVarios(parametroVarios);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParametroVarios(ParametroVarios parametroVarios) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateParametroVarios(parametroVarios);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParametroVarios(ParametroVarios parametroVarios) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteParametroVarios(parametroVarios);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ParametroVarios findParametroVariosById(long parametroVariosId) throws Exception
  {
    try { this.txn.open();
      ParametroVarios parametroVarios = 
        this.definicionesBusiness.findParametroVariosById(parametroVariosId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parametroVarios);
      return parametroVarios;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParametroVarios() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllParametroVarios();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParametroVariosByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findParametroVariosByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPrimaAntiguedad(PrimaAntiguedad primaAntiguedad)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addPrimaAntiguedad(primaAntiguedad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePrimaAntiguedad(PrimaAntiguedad primaAntiguedad) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updatePrimaAntiguedad(primaAntiguedad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePrimaAntiguedad(PrimaAntiguedad primaAntiguedad) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deletePrimaAntiguedad(primaAntiguedad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PrimaAntiguedad findPrimaAntiguedadById(long primaAntiguedadId) throws Exception
  {
    try { this.txn.open();
      PrimaAntiguedad primaAntiguedad = 
        this.definicionesBusiness.findPrimaAntiguedadById(primaAntiguedadId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(primaAntiguedad);
      return primaAntiguedad;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPrimaAntiguedad() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllPrimaAntiguedad();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPrimaAntiguedadByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findPrimaAntiguedadByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPrimaHijo(PrimaHijo primaHijo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addPrimaHijo(primaHijo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePrimaHijo(PrimaHijo primaHijo) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updatePrimaHijo(primaHijo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePrimaHijo(PrimaHijo primaHijo) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deletePrimaHijo(primaHijo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PrimaHijo findPrimaHijoById(long primaHijoId) throws Exception
  {
    try { this.txn.open();
      PrimaHijo primaHijo = 
        this.definicionesBusiness.findPrimaHijoById(primaHijoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(primaHijo);
      return primaHijo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPrimaHijo() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllPrimaHijo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPrimaHijoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findPrimaHijoByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRelacionPersonal(RelacionPersonal relacionPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addRelacionPersonal(relacionPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRelacionPersonal(RelacionPersonal relacionPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateRelacionPersonal(relacionPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRelacionPersonal(RelacionPersonal relacionPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteRelacionPersonal(relacionPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RelacionPersonal findRelacionPersonalById(long relacionPersonalId) throws Exception
  {
    try { this.txn.open();
      RelacionPersonal relacionPersonal = 
        this.definicionesBusiness.findRelacionPersonalById(relacionPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(relacionPersonal);
      return relacionPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRelacionPersonal() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllRelacionPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRelacionPersonalByCodRelacion(String codRelacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findRelacionPersonalByCodRelacion(codRelacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRelacionPersonalByDescRelacion(String descRelacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findRelacionPersonalByDescRelacion(descRelacion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRestringido(Restringido restringido)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addRestringido(restringido);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRestringido(Restringido restringido) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateRestringido(restringido);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRestringido(Restringido restringido) throws Exception
  {
    try {
      this.txn.open();
      this.definicionesBusiness.deleteRestringido(restringido);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Restringido findRestringidoById(long restringidoId) throws Exception
  {
    try { this.txn.open();
      Restringido restringido = 
        this.definicionesBusiness.findRestringidoById(restringidoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(restringido);
      return restringido;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRestringido() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllRestringido();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRestringidoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findRestringidoByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSemana(Semana semana)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addSemana(semana);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSemana(Semana semana) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateSemana(semana);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSemana(Semana semana) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteSemana(semana);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Semana findSemanaById(long semanaId) throws Exception
  {
    try { this.txn.open();
      Semana semana = 
        this.definicionesBusiness.findSemanaById(semanaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(semana);
      return semana;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSemana() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllSemana();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSemanaByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findSemanaByGrupoNomina(idGrupoNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSemanaByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findSemanaByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Semana findSemanaByAnioSemanaAnio(int anio, int semanaAnio) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesBusiness.findSemanaByAnioSemanaAnio(anio, semanaAnio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSindicato(Sindicato sindicato)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addSindicato(sindicato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSindicato(Sindicato sindicato) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateSindicato(sindicato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSindicato(Sindicato sindicato) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteSindicato(sindicato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Sindicato findSindicatoById(long sindicatoId) throws Exception
  {
    try { this.txn.open();
      Sindicato sindicato = 
        this.definicionesBusiness.findSindicatoById(sindicatoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(sindicato);
      return sindicato;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSindicato() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllSindicato();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSindicatoByCodSindicato(String codSindicato)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findSindicatoByCodSindicato(codSindicato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSindicatoByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findSindicatoByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTarifaAri(TarifaAri tarifaAri)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addTarifaAri(tarifaAri);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTarifaAri(TarifaAri tarifaAri) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateTarifaAri(tarifaAri);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTarifaAri(TarifaAri tarifaAri) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteTarifaAri(tarifaAri);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TarifaAri findTarifaAriById(long tarifaAriId) throws Exception
  {
    try { this.txn.open();
      TarifaAri tarifaAri = 
        this.definicionesBusiness.findTarifaAriById(tarifaAriId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tarifaAri);
      return tarifaAri;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTarifaAri() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllTarifaAri();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTarifaAriByTarifa(double tarifa)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findTarifaAriByTarifa(tarifa);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEscalaCuadroOnapre(EscalaCuadroOnapre escalaCuadroOnapre)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addEscalaCuadroOnapre(escalaCuadroOnapre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEscalaCuadroOnapre(EscalaCuadroOnapre escalaCuadroOnapre) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateEscalaCuadroOnapre(escalaCuadroOnapre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEscalaCuadroOnapre(EscalaCuadroOnapre escalaCuadroOnapre) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteEscalaCuadroOnapre(escalaCuadroOnapre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public EscalaCuadroOnapre findEscalaCuadroOnapreById(long escalaCuadroOnapreId) throws Exception
  {
    try { this.txn.open();
      EscalaCuadroOnapre escalaCuadroOnapre = 
        this.definicionesBusiness.findEscalaCuadroOnapreById(escalaCuadroOnapreId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(escalaCuadroOnapre);
      return escalaCuadroOnapre;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEscalaCuadroOnapre() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllEscalaCuadroOnapre();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findEscalaCuadroOnapreByCodigo(String codigo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findEscalaCuadroOnapreByCodigo(codigo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoAcreencia(TipoAcreencia tipoAcreencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addTipoAcreencia(tipoAcreencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoAcreencia(TipoAcreencia tipoAcreencia) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateTipoAcreencia(tipoAcreencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoAcreencia(TipoAcreencia tipoAcreencia) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteTipoAcreencia(tipoAcreencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoAcreencia findTipoAcreenciaById(long tipoAcreenciaId) throws Exception
  {
    try { this.txn.open();
      TipoAcreencia tipoAcreencia = 
        this.definicionesBusiness.findTipoAcreenciaById(tipoAcreenciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoAcreencia);
      return tipoAcreencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoAcreencia() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllTipoAcreencia();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoContrato(TipoContrato tipoContrato)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addTipoContrato(tipoContrato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoContrato(TipoContrato tipoContrato) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateTipoContrato(tipoContrato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoContrato(TipoContrato tipoContrato) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteTipoContrato(tipoContrato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoContrato findTipoContratoById(long tipoContratoId) throws Exception
  {
    try { this.txn.open();
      TipoContrato tipoContrato = 
        this.definicionesBusiness.findTipoContratoById(tipoContratoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoContrato);
      return tipoContrato;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoContrato() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllTipoContrato();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoContratoByCodTipoContrato(String codTipoContrato)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findTipoContratoByCodTipoContrato(codTipoContrato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoContratoByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findTipoContratoByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoPersonal(TipoPersonal tipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addTipoPersonal(tipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoPersonal(TipoPersonal tipoPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateTipoPersonal(tipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoPersonal(TipoPersonal tipoPersonal) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteTipoPersonal(tipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoPersonal findTipoPersonalById(long tipoPersonalId) throws Exception
  {
    try { this.txn.open();
      TipoPersonal tipoPersonal = 
        this.definicionesBusiness.findTipoPersonalById(tipoPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoPersonal);
      return tipoPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoPersonal() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllTipoPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalByCodTipoPersonal(String codTipoPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findTipoPersonalByCodTipoPersonal(codTipoPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findTipoPersonalByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findTipoPersonalByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTurno(Turno turno)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addTurno(turno);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTurno(Turno turno) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateTurno(turno);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTurno(Turno turno) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteTurno(turno);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Turno findTurnoById(long turnoId) throws Exception
  {
    try { this.txn.open();
      Turno turno = 
        this.definicionesBusiness.findTurnoById(turnoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(turno);
      return turno;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTurno() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllTurno();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTurnoByCodTurno(String codTurno, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findTurnoByCodTurno(codTurno, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTurnoByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findTurnoByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTurnoByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findTurnoByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addVacacionesPorAnio(VacacionesPorAnio vacacionesPorAnio)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addVacacionesPorAnio(vacacionesPorAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateVacacionesPorAnio(VacacionesPorAnio vacacionesPorAnio) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateVacacionesPorAnio(vacacionesPorAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteVacacionesPorAnio(VacacionesPorAnio vacacionesPorAnio) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteVacacionesPorAnio(vacacionesPorAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public VacacionesPorAnio findVacacionesPorAnioById(long vacacionesPorAnioId) throws Exception
  {
    try { this.txn.open();
      VacacionesPorAnio vacacionesPorAnio = 
        this.definicionesBusiness.findVacacionesPorAnioById(vacacionesPorAnioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(vacacionesPorAnio);
      return vacacionesPorAnio;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllVacacionesPorAnio() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllVacacionesPorAnio();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findVacacionesPorAnioByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findVacacionesPorAnioByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addVacacionesPorCargo(VacacionesPorCargo vacacionesPorCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addVacacionesPorCargo(vacacionesPorCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateVacacionesPorCargo(VacacionesPorCargo vacacionesPorCargo) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateVacacionesPorCargo(vacacionesPorCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteVacacionesPorCargo(VacacionesPorCargo vacacionesPorCargo) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteVacacionesPorCargo(vacacionesPorCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public VacacionesPorCargo findVacacionesPorCargoById(long vacacionesPorCargoId) throws Exception
  {
    try { this.txn.open();
      VacacionesPorCargo vacacionesPorCargo = 
        this.definicionesBusiness.findVacacionesPorCargoById(vacacionesPorCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(vacacionesPorCargo);
      return vacacionesPorCargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllVacacionesPorCargo() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllVacacionesPorCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findVacacionesPorCargoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findVacacionesPorCargoByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addUtilidadesPorAnio(UtilidadesPorAnio utilidadesPorAnio)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addUtilidadesPorAnio(utilidadesPorAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateUtilidadesPorAnio(UtilidadesPorAnio utilidadesPorAnio) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateUtilidadesPorAnio(utilidadesPorAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteUtilidadesPorAnio(UtilidadesPorAnio utilidadesPorAnio) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteUtilidadesPorAnio(utilidadesPorAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public UtilidadesPorAnio findUtilidadesPorAnioById(long utilidadesPorAnioId) throws Exception
  {
    try { this.txn.open();
      UtilidadesPorAnio utilidadesPorAnio = 
        this.definicionesBusiness.findUtilidadesPorAnioById(utilidadesPorAnioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(utilidadesPorAnio);
      return utilidadesPorAnio;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllUtilidadesPorAnio() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllUtilidadesPorAnio();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUtilidadesPorAnioByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findUtilidadesPorAnioByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParametroJubilacion(ParametroJubilacion parametroJubilacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.definicionesBusiness.addParametroJubilacion(parametroJubilacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParametroJubilacion(ParametroJubilacion parametroJubilacion) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.updateParametroJubilacion(parametroJubilacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParametroJubilacion(ParametroJubilacion parametroJubilacion) throws Exception
  {
    try { this.txn.open();
      this.definicionesBusiness.deleteParametroJubilacion(parametroJubilacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ParametroJubilacion findParametroJubilacionById(long parametroJubilacionId) throws Exception
  {
    try { this.txn.open();
      ParametroJubilacion parametroJubilacion = 
        this.definicionesBusiness.findParametroJubilacionById(parametroJubilacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parametroJubilacion);
      return parametroJubilacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParametroJubilacion() throws Exception
  {
    try { this.txn.open();
      return this.definicionesBusiness.findAllParametroJubilacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParametroJubilacionByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesBusiness.findParametroJubilacionByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}