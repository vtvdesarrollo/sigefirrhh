package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class DetalleDisqueteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDetalleDisquete(DetalleDisquete detalleDisquete)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DetalleDisquete detalleDisqueteNew = 
      (DetalleDisquete)BeanUtils.cloneBean(
      detalleDisquete);

    DisqueteBeanBusiness disqueteBeanBusiness = new DisqueteBeanBusiness();

    if (detalleDisqueteNew.getDisquete() != null) {
      detalleDisqueteNew.setDisquete(
        disqueteBeanBusiness.findDisqueteById(
        detalleDisqueteNew.getDisquete().getIdDisquete()));
    }

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (detalleDisqueteNew.getConcepto() != null) {
      detalleDisqueteNew.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        detalleDisqueteNew.getConcepto().getIdConcepto()));
    }
    pm.makePersistent(detalleDisqueteNew);
  }

  public void updateDetalleDisquete(DetalleDisquete detalleDisquete) throws Exception
  {
    DetalleDisquete detalleDisqueteModify = 
      findDetalleDisqueteById(detalleDisquete.getIdDetalleDisquete());

    DisqueteBeanBusiness disqueteBeanBusiness = new DisqueteBeanBusiness();

    if (detalleDisquete.getDisquete() != null) {
      detalleDisquete.setDisquete(
        disqueteBeanBusiness.findDisqueteById(
        detalleDisquete.getDisquete().getIdDisquete()));
    }

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (detalleDisquete.getConcepto() != null) {
      detalleDisquete.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        detalleDisquete.getConcepto().getIdConcepto()));
    }

    BeanUtils.copyProperties(detalleDisqueteModify, detalleDisquete);
  }

  public void deleteDetalleDisquete(DetalleDisquete detalleDisquete) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DetalleDisquete detalleDisqueteDelete = 
      findDetalleDisqueteById(detalleDisquete.getIdDetalleDisquete());
    pm.deletePersistent(detalleDisqueteDelete);
  }

  public DetalleDisquete findDetalleDisqueteById(long idDetalleDisquete) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDetalleDisquete == pIdDetalleDisquete";
    Query query = pm.newQuery(DetalleDisquete.class, filter);

    query.declareParameters("long pIdDetalleDisquete");

    parameters.put("pIdDetalleDisquete", new Long(idDetalleDisquete));

    Collection colDetalleDisquete = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDetalleDisquete.iterator();
    return (DetalleDisquete)iterator.next();
  }

  public Collection findDetalleDisqueteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent detalleDisqueteExtent = pm.getExtent(
      DetalleDisquete.class, true);
    Query query = pm.newQuery(detalleDisqueteExtent);
    query.setOrdering("tipoRegistro ascending, numeroCampo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByDisquete(long idDisquete)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "disquete.idDisquete == pIdDisquete";

    Query query = pm.newQuery(DetalleDisquete.class, filter);

    query.declareParameters("long pIdDisquete");
    HashMap parameters = new HashMap();

    parameters.put("pIdDisquete", new Long(idDisquete));

    query.setOrdering("tipoRegistro ascending, numeroCampo ascending");

    Collection colDetalleDisquete = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDetalleDisquete);

    return colDetalleDisquete;
  }
}