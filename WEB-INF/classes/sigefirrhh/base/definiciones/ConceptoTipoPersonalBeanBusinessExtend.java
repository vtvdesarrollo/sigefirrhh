package sigefirrhh.base.definiciones;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ConceptoTipoPersonalBeanBusinessExtend extends ConceptoTipoPersonalBeanBusiness
  implements Serializable
{
  public Collection findByCodConcepto(long idTipoPersonal, String codConcepto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "concepto.codConcepto == pCodConcepto && tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("java.lang.String pCodConcepto, long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pCodConcepto", codConcepto);
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoTipoPersonal);

    return colConceptoTipoPersonal;
  }

  public Collection findByTipoPersonalAndTipoPrestamo(long idTipoPersonal, String tipoPrestamo) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.tipoPrestamo == pTipoPrestamo";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pTipoPrestamo");

    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pTipoPrestamo", tipoPrestamo);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoTipoPersonal);

    return colConceptoTipoPersonal;
  }

  public Collection findByTipoPersonalAsignacionesAndTipoPrestamo(long idTipoPersonal, String tipoPrestamo) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String codigo = "5000";
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.tipoPrestamo == pTipoPrestamo && concepto.codConcepto < pCodigo";

    Query query = pm.newQuery(ConceptoTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pTipoPrestamo, String pCodigo");

    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pTipoPrestamo", tipoPrestamo);
    parameters.put("pCodigo", codigo);

    query.setOrdering("concepto.codConcepto ascending");

    Collection colConceptoTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoTipoPersonal);

    return colConceptoTipoPersonal;
  }
}