package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ConceptoPK
  implements Serializable
{
  public long idConcepto;

  public ConceptoPK()
  {
  }

  public ConceptoPK(long idConcepto)
  {
    this.idConcepto = idConcepto;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoPK thatPK)
  {
    return 
      this.idConcepto == thatPK.idConcepto;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConcepto)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConcepto);
  }
}