package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportDefinicionesForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportDefinicionesForm.class.getName());
  private int reportId;
  private String reportName;
  private String reporte = "1";
  private LoginSession login;

  public ReportDefinicionesForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    this.reportName = "ConceptoSso";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportDefinicionesForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      if (this.reporte.equals("1"))
        this.reportName = "ConceptoSso";
      else if (this.reporte.equals("2"))
        this.reportName = "ConceptoLph";
      else if (this.reporte.equals("3"))
        this.reportName = "ConceptoFju";
      else if (this.reporte.equals("4"))
        this.reportName = "ConceptoSueldo";
      else if (this.reporte.equals("5"))
        this.reportName = "ConceptoAjuste";
      else if (this.reporte.equals("6"))
        this.reportName = "ConceptoCompensacion";
      else if (this.reporte.equals("7"))
        this.reportName = "ConceptoPrimasCargo";
      else if (this.reporte.equals("8"))
        this.reportName = "ConceptoPrimasTrabajador";
      else if (this.reporte.equals("9"))
        this.reportName = "ConceptoIntegral";
      else if (this.reporte.equals("10"))
        this.reportName = "ConceptoGravables";
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));

      if (this.reporte.equals("1"))
        this.reportName = "ConceptoSso";
      else if (this.reporte.equals("2"))
        this.reportName = "ConceptoLph";
      else if (this.reporte.equals("3"))
        this.reportName = "ConceptoFju";
      else if (this.reporte.equals("4"))
        this.reportName = "ConceptoSueldo";
      else if (this.reporte.equals("5"))
        this.reportName = "ConceptoAjuste";
      else if (this.reporte.equals("6"))
        this.reportName = "ConceptoCompensacion";
      else if (this.reporte.equals("7"))
        this.reportName = "ConceptoPrimasCargo";
      else if (this.reporte.equals("8"))
        this.reportName = "ConceptoPrimasTrabajador";
      else if (this.reporte.equals("9"))
        this.reportName = "ConceptoIntegral";
      else if (this.reporte.equals("10")) {
        this.reportName = "ConceptoGravables";
      }
      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/base/definiciones");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public int getReportId()
  {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getReporte() {
    return this.reporte;
  }
  public void setReporte(String reporte) {
    this.reporte = reporte;
  }
}