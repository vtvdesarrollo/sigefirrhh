package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class RestringidoPK
  implements Serializable
{
  public long idRestringido;

  public RestringidoPK()
  {
  }

  public RestringidoPK(long idRestringido)
  {
    this.idRestringido = idRestringido;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RestringidoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RestringidoPK thatPK)
  {
    return 
      this.idRestringido == thatPK.idRestringido;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRestringido)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRestringido);
  }
}