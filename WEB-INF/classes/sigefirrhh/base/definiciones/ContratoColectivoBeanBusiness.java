package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ContratoColectivoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addContratoColectivo(ContratoColectivo contratoColectivo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ContratoColectivo contratoColectivoNew = 
      (ContratoColectivo)BeanUtils.cloneBean(
      contratoColectivo);

    pm.makePersistent(contratoColectivoNew);
  }

  public void updateContratoColectivo(ContratoColectivo contratoColectivo) throws Exception
  {
    ContratoColectivo contratoColectivoModify = 
      findContratoColectivoById(contratoColectivo.getIdContratoColectivo());

    BeanUtils.copyProperties(contratoColectivoModify, contratoColectivo);
  }

  public void deleteContratoColectivo(ContratoColectivo contratoColectivo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ContratoColectivo contratoColectivoDelete = 
      findContratoColectivoById(contratoColectivo.getIdContratoColectivo());
    pm.deletePersistent(contratoColectivoDelete);
  }

  public ContratoColectivo findContratoColectivoById(long idContratoColectivo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idContratoColectivo == pIdContratoColectivo";
    Query query = pm.newQuery(ContratoColectivo.class, filter);

    query.declareParameters("long pIdContratoColectivo");

    parameters.put("pIdContratoColectivo", new Long(idContratoColectivo));

    Collection colContratoColectivo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colContratoColectivo.iterator();
    return (ContratoColectivo)iterator.next();
  }

  public Collection findContratoColectivoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent contratoColectivoExtent = pm.getExtent(
      ContratoColectivo.class, true);
    Query query = pm.newQuery(contratoColectivoExtent);
    query.setOrdering("denominacion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodContratoColectivo(String codContratoColectivo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codContratoColectivo == pCodContratoColectivo";

    Query query = pm.newQuery(ContratoColectivo.class, filter);

    query.declareParameters("java.lang.String pCodContratoColectivo");
    HashMap parameters = new HashMap();

    parameters.put("pCodContratoColectivo", new String(codContratoColectivo));

    query.setOrdering("denominacion ascending");

    Collection colContratoColectivo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colContratoColectivo);

    return colContratoColectivo;
  }

  public Collection findByDenominacion(String denominacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "denominacion.startsWith(pDenominacion)";

    Query query = pm.newQuery(ContratoColectivo.class, filter);

    query.declareParameters("java.lang.String pDenominacion");
    HashMap parameters = new HashMap();

    parameters.put("pDenominacion", new String(denominacion));

    query.setOrdering("denominacion ascending");

    Collection colContratoColectivo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colContratoColectivo);

    return colContratoColectivo;
  }
}