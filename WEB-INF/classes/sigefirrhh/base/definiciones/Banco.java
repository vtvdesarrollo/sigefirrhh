package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Banco
  implements Serializable, PersistenceCapable
{
  private long idBanco;
  private String codBanco;
  private String nombre;
  private String identificadorAhorro;
  private String identificadorCorriente;
  private int correlativo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codBanco", "correlativo", "idBanco", "identificadorAhorro", "identificadorCorriente", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Integer.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodBanco(this);
  }

  public String getCodBanco()
  {
    return jdoGetcodBanco(this);
  }

  public long getIdBanco()
  {
    return jdoGetidBanco(this);
  }

  public String getIdentificadorAhorro()
  {
    return jdoGetidentificadorAhorro(this);
  }

  public String getIdentificadorCorriente()
  {
    return jdoGetidentificadorCorriente(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodBanco(String string)
  {
    jdoSetcodBanco(this, string);
  }

  public void setIdBanco(long l)
  {
    jdoSetidBanco(this, l);
  }

  public void setIdentificadorAhorro(String string)
  {
    jdoSetidentificadorAhorro(this, string);
  }

  public void setIdentificadorCorriente(String string)
  {
    jdoSetidentificadorCorriente(this, string);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public int getCorrelativo() {
    return jdoGetcorrelativo(this);
  }
  public void setCorrelativo(int correlativo) {
    jdoSetcorrelativo(this, correlativo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Banco());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Banco localBanco = new Banco();
    localBanco.jdoFlags = 1;
    localBanco.jdoStateManager = paramStateManager;
    return localBanco;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Banco localBanco = new Banco();
    localBanco.jdoCopyKeyFieldsFromObjectId(paramObject);
    localBanco.jdoFlags = 1;
    localBanco.jdoStateManager = paramStateManager;
    return localBanco;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codBanco);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.correlativo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idBanco);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.identificadorAhorro);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.identificadorCorriente);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codBanco = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.correlativo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idBanco = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.identificadorAhorro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.identificadorCorriente = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Banco paramBanco, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramBanco == null)
        throw new IllegalArgumentException("arg1");
      this.codBanco = paramBanco.codBanco;
      return;
    case 1:
      if (paramBanco == null)
        throw new IllegalArgumentException("arg1");
      this.correlativo = paramBanco.correlativo;
      return;
    case 2:
      if (paramBanco == null)
        throw new IllegalArgumentException("arg1");
      this.idBanco = paramBanco.idBanco;
      return;
    case 3:
      if (paramBanco == null)
        throw new IllegalArgumentException("arg1");
      this.identificadorAhorro = paramBanco.identificadorAhorro;
      return;
    case 4:
      if (paramBanco == null)
        throw new IllegalArgumentException("arg1");
      this.identificadorCorriente = paramBanco.identificadorCorriente;
      return;
    case 5:
      if (paramBanco == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramBanco.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Banco))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Banco localBanco = (Banco)paramObject;
    if (localBanco.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localBanco, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new BancoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new BancoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BancoPK))
      throw new IllegalArgumentException("arg1");
    BancoPK localBancoPK = (BancoPK)paramObject;
    localBancoPK.idBanco = this.idBanco;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BancoPK))
      throw new IllegalArgumentException("arg1");
    BancoPK localBancoPK = (BancoPK)paramObject;
    this.idBanco = localBancoPK.idBanco;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BancoPK))
      throw new IllegalArgumentException("arg2");
    BancoPK localBancoPK = (BancoPK)paramObject;
    localBancoPK.idBanco = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BancoPK))
      throw new IllegalArgumentException("arg2");
    BancoPK localBancoPK = (BancoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localBancoPK.idBanco);
  }

  private static final String jdoGetcodBanco(Banco paramBanco)
  {
    if (paramBanco.jdoFlags <= 0)
      return paramBanco.codBanco;
    StateManager localStateManager = paramBanco.jdoStateManager;
    if (localStateManager == null)
      return paramBanco.codBanco;
    if (localStateManager.isLoaded(paramBanco, jdoInheritedFieldCount + 0))
      return paramBanco.codBanco;
    return localStateManager.getStringField(paramBanco, jdoInheritedFieldCount + 0, paramBanco.codBanco);
  }

  private static final void jdoSetcodBanco(Banco paramBanco, String paramString)
  {
    if (paramBanco.jdoFlags == 0)
    {
      paramBanco.codBanco = paramString;
      return;
    }
    StateManager localStateManager = paramBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramBanco.codBanco = paramString;
      return;
    }
    localStateManager.setStringField(paramBanco, jdoInheritedFieldCount + 0, paramBanco.codBanco, paramString);
  }

  private static final int jdoGetcorrelativo(Banco paramBanco)
  {
    if (paramBanco.jdoFlags <= 0)
      return paramBanco.correlativo;
    StateManager localStateManager = paramBanco.jdoStateManager;
    if (localStateManager == null)
      return paramBanco.correlativo;
    if (localStateManager.isLoaded(paramBanco, jdoInheritedFieldCount + 1))
      return paramBanco.correlativo;
    return localStateManager.getIntField(paramBanco, jdoInheritedFieldCount + 1, paramBanco.correlativo);
  }

  private static final void jdoSetcorrelativo(Banco paramBanco, int paramInt)
  {
    if (paramBanco.jdoFlags == 0)
    {
      paramBanco.correlativo = paramInt;
      return;
    }
    StateManager localStateManager = paramBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramBanco.correlativo = paramInt;
      return;
    }
    localStateManager.setIntField(paramBanco, jdoInheritedFieldCount + 1, paramBanco.correlativo, paramInt);
  }

  private static final long jdoGetidBanco(Banco paramBanco)
  {
    return paramBanco.idBanco;
  }

  private static final void jdoSetidBanco(Banco paramBanco, long paramLong)
  {
    StateManager localStateManager = paramBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramBanco.idBanco = paramLong;
      return;
    }
    localStateManager.setLongField(paramBanco, jdoInheritedFieldCount + 2, paramBanco.idBanco, paramLong);
  }

  private static final String jdoGetidentificadorAhorro(Banco paramBanco)
  {
    if (paramBanco.jdoFlags <= 0)
      return paramBanco.identificadorAhorro;
    StateManager localStateManager = paramBanco.jdoStateManager;
    if (localStateManager == null)
      return paramBanco.identificadorAhorro;
    if (localStateManager.isLoaded(paramBanco, jdoInheritedFieldCount + 3))
      return paramBanco.identificadorAhorro;
    return localStateManager.getStringField(paramBanco, jdoInheritedFieldCount + 3, paramBanco.identificadorAhorro);
  }

  private static final void jdoSetidentificadorAhorro(Banco paramBanco, String paramString)
  {
    if (paramBanco.jdoFlags == 0)
    {
      paramBanco.identificadorAhorro = paramString;
      return;
    }
    StateManager localStateManager = paramBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramBanco.identificadorAhorro = paramString;
      return;
    }
    localStateManager.setStringField(paramBanco, jdoInheritedFieldCount + 3, paramBanco.identificadorAhorro, paramString);
  }

  private static final String jdoGetidentificadorCorriente(Banco paramBanco)
  {
    if (paramBanco.jdoFlags <= 0)
      return paramBanco.identificadorCorriente;
    StateManager localStateManager = paramBanco.jdoStateManager;
    if (localStateManager == null)
      return paramBanco.identificadorCorriente;
    if (localStateManager.isLoaded(paramBanco, jdoInheritedFieldCount + 4))
      return paramBanco.identificadorCorriente;
    return localStateManager.getStringField(paramBanco, jdoInheritedFieldCount + 4, paramBanco.identificadorCorriente);
  }

  private static final void jdoSetidentificadorCorriente(Banco paramBanco, String paramString)
  {
    if (paramBanco.jdoFlags == 0)
    {
      paramBanco.identificadorCorriente = paramString;
      return;
    }
    StateManager localStateManager = paramBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramBanco.identificadorCorriente = paramString;
      return;
    }
    localStateManager.setStringField(paramBanco, jdoInheritedFieldCount + 4, paramBanco.identificadorCorriente, paramString);
  }

  private static final String jdoGetnombre(Banco paramBanco)
  {
    if (paramBanco.jdoFlags <= 0)
      return paramBanco.nombre;
    StateManager localStateManager = paramBanco.jdoStateManager;
    if (localStateManager == null)
      return paramBanco.nombre;
    if (localStateManager.isLoaded(paramBanco, jdoInheritedFieldCount + 5))
      return paramBanco.nombre;
    return localStateManager.getStringField(paramBanco, jdoInheritedFieldCount + 5, paramBanco.nombre);
  }

  private static final void jdoSetnombre(Banco paramBanco, String paramString)
  {
    if (paramBanco.jdoFlags == 0)
    {
      paramBanco.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramBanco.jdoStateManager;
    if (localStateManager == null)
    {
      paramBanco.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramBanco, jdoInheritedFieldCount + 5, paramBanco.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}