package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class Disquete
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO_DISQUETE;
  protected static final Map LISTA_SINO;
  protected static final Map LISTA_INGRESOS;
  private long idDisquete;
  private String codDisquete;
  private String descripcion;
  private String tipoDisquete;
  private String ingresos;
  private String egresos;
  private Banco banco;
  private Concepto concepto;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "banco", "codDisquete", "concepto", "descripcion", "egresos", "idDisquete", "ingresos", "organismo", "tipoDisquete" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 26, 21, 26, 21, 21, 24, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.Disquete"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Disquete());

    LISTA_TIPO_DISQUETE = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_INGRESOS = 
      new LinkedHashMap();
    LISTA_TIPO_DISQUETE.put("N", "NOMINA");
    LISTA_TIPO_DISQUETE.put("A", "RETENCION CON BANCO");
    LISTA_TIPO_DISQUETE.put("S", "RETENCION SIN BANCO");
    LISTA_TIPO_DISQUETE.put("F", "FIDEICOMISO");
    LISTA_TIPO_DISQUETE.put("T", "TICKETS");
    LISTA_TIPO_DISQUETE.put("P", "PRESTACIONES");
    LISTA_TIPO_DISQUETE.put("O", "OTRO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
    LISTA_INGRESOS.put("S", "SOLO INCORPORACIONES");
    LISTA_INGRESOS.put("N", "EXCLUIR INCORPORACIONES");
    LISTA_INGRESOS.put("T", "INCLUIR INCORPORACIONES");
  }

  public String toString()
  {
    return jdoGetdescripcion(this) + "  -  " + 
      jdoGetcodDisquete(this);
  }

  public Banco getBanco()
  {
    return jdoGetbanco(this);
  }

  public void setBanco(Banco banco)
  {
    jdoSetbanco(this, banco);
  }

  public String getCodDisquete()
  {
    return jdoGetcodDisquete(this);
  }

  public void setCodDisquete(String codDisquete)
  {
    jdoSetcodDisquete(this, codDisquete);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion)
  {
    jdoSetdescripcion(this, descripcion);
  }

  public long getIdDisquete()
  {
    return jdoGetidDisquete(this);
  }

  public void setIdDisquete(long idDisquete)
  {
    jdoSetidDisquete(this, idDisquete);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public String getTipoDisquete()
  {
    return jdoGettipoDisquete(this);
  }

  public void setTipoDisquete(String tipoDisquete)
  {
    jdoSettipoDisquete(this, tipoDisquete);
  }

  public String getEgresos() {
    return jdoGetegresos(this);
  }
  public void setEgresos(String egresos) {
    jdoSetegresos(this, egresos);
  }
  public String getIngresos() {
    return jdoGetingresos(this);
  }
  public void setIngresos(String ingresos) {
    jdoSetingresos(this, ingresos);
  }
  public Concepto getConcepto() {
    return jdoGetconcepto(this);
  }
  public void setConcepto(Concepto concepto) {
    jdoSetconcepto(this, concepto);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Disquete localDisquete = new Disquete();
    localDisquete.jdoFlags = 1;
    localDisquete.jdoStateManager = paramStateManager;
    return localDisquete;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Disquete localDisquete = new Disquete();
    localDisquete.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDisquete.jdoFlags = 1;
    localDisquete.jdoStateManager = paramStateManager;
    return localDisquete;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.banco);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codDisquete);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.concepto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.egresos);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDisquete);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.ingresos);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoDisquete);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.banco = ((Banco)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codDisquete = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.concepto = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.egresos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDisquete = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ingresos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoDisquete = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Disquete paramDisquete, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.banco = paramDisquete.banco;
      return;
    case 1:
      if (paramDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.codDisquete = paramDisquete.codDisquete;
      return;
    case 2:
      if (paramDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.concepto = paramDisquete.concepto;
      return;
    case 3:
      if (paramDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramDisquete.descripcion;
      return;
    case 4:
      if (paramDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.egresos = paramDisquete.egresos;
      return;
    case 5:
      if (paramDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.idDisquete = paramDisquete.idDisquete;
      return;
    case 6:
      if (paramDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.ingresos = paramDisquete.ingresos;
      return;
    case 7:
      if (paramDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramDisquete.organismo;
      return;
    case 8:
      if (paramDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.tipoDisquete = paramDisquete.tipoDisquete;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Disquete))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Disquete localDisquete = (Disquete)paramObject;
    if (localDisquete.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDisquete, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DisquetePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DisquetePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DisquetePK))
      throw new IllegalArgumentException("arg1");
    DisquetePK localDisquetePK = (DisquetePK)paramObject;
    localDisquetePK.idDisquete = this.idDisquete;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DisquetePK))
      throw new IllegalArgumentException("arg1");
    DisquetePK localDisquetePK = (DisquetePK)paramObject;
    this.idDisquete = localDisquetePK.idDisquete;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DisquetePK))
      throw new IllegalArgumentException("arg2");
    DisquetePK localDisquetePK = (DisquetePK)paramObject;
    localDisquetePK.idDisquete = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DisquetePK))
      throw new IllegalArgumentException("arg2");
    DisquetePK localDisquetePK = (DisquetePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localDisquetePK.idDisquete);
  }

  private static final Banco jdoGetbanco(Disquete paramDisquete)
  {
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDisquete.banco;
    if (localStateManager.isLoaded(paramDisquete, jdoInheritedFieldCount + 0))
      return paramDisquete.banco;
    return (Banco)localStateManager.getObjectField(paramDisquete, jdoInheritedFieldCount + 0, paramDisquete.banco);
  }

  private static final void jdoSetbanco(Disquete paramDisquete, Banco paramBanco)
  {
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDisquete.banco = paramBanco;
      return;
    }
    localStateManager.setObjectField(paramDisquete, jdoInheritedFieldCount + 0, paramDisquete.banco, paramBanco);
  }

  private static final String jdoGetcodDisquete(Disquete paramDisquete)
  {
    if (paramDisquete.jdoFlags <= 0)
      return paramDisquete.codDisquete;
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDisquete.codDisquete;
    if (localStateManager.isLoaded(paramDisquete, jdoInheritedFieldCount + 1))
      return paramDisquete.codDisquete;
    return localStateManager.getStringField(paramDisquete, jdoInheritedFieldCount + 1, paramDisquete.codDisquete);
  }

  private static final void jdoSetcodDisquete(Disquete paramDisquete, String paramString)
  {
    if (paramDisquete.jdoFlags == 0)
    {
      paramDisquete.codDisquete = paramString;
      return;
    }
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDisquete.codDisquete = paramString;
      return;
    }
    localStateManager.setStringField(paramDisquete, jdoInheritedFieldCount + 1, paramDisquete.codDisquete, paramString);
  }

  private static final Concepto jdoGetconcepto(Disquete paramDisquete)
  {
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDisquete.concepto;
    if (localStateManager.isLoaded(paramDisquete, jdoInheritedFieldCount + 2))
      return paramDisquete.concepto;
    return (Concepto)localStateManager.getObjectField(paramDisquete, jdoInheritedFieldCount + 2, paramDisquete.concepto);
  }

  private static final void jdoSetconcepto(Disquete paramDisquete, Concepto paramConcepto)
  {
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDisquete.concepto = paramConcepto;
      return;
    }
    localStateManager.setObjectField(paramDisquete, jdoInheritedFieldCount + 2, paramDisquete.concepto, paramConcepto);
  }

  private static final String jdoGetdescripcion(Disquete paramDisquete)
  {
    if (paramDisquete.jdoFlags <= 0)
      return paramDisquete.descripcion;
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDisquete.descripcion;
    if (localStateManager.isLoaded(paramDisquete, jdoInheritedFieldCount + 3))
      return paramDisquete.descripcion;
    return localStateManager.getStringField(paramDisquete, jdoInheritedFieldCount + 3, paramDisquete.descripcion);
  }

  private static final void jdoSetdescripcion(Disquete paramDisquete, String paramString)
  {
    if (paramDisquete.jdoFlags == 0)
    {
      paramDisquete.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDisquete.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramDisquete, jdoInheritedFieldCount + 3, paramDisquete.descripcion, paramString);
  }

  private static final String jdoGetegresos(Disquete paramDisquete)
  {
    if (paramDisquete.jdoFlags <= 0)
      return paramDisquete.egresos;
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDisquete.egresos;
    if (localStateManager.isLoaded(paramDisquete, jdoInheritedFieldCount + 4))
      return paramDisquete.egresos;
    return localStateManager.getStringField(paramDisquete, jdoInheritedFieldCount + 4, paramDisquete.egresos);
  }

  private static final void jdoSetegresos(Disquete paramDisquete, String paramString)
  {
    if (paramDisquete.jdoFlags == 0)
    {
      paramDisquete.egresos = paramString;
      return;
    }
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDisquete.egresos = paramString;
      return;
    }
    localStateManager.setStringField(paramDisquete, jdoInheritedFieldCount + 4, paramDisquete.egresos, paramString);
  }

  private static final long jdoGetidDisquete(Disquete paramDisquete)
  {
    return paramDisquete.idDisquete;
  }

  private static final void jdoSetidDisquete(Disquete paramDisquete, long paramLong)
  {
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDisquete.idDisquete = paramLong;
      return;
    }
    localStateManager.setLongField(paramDisquete, jdoInheritedFieldCount + 5, paramDisquete.idDisquete, paramLong);
  }

  private static final String jdoGetingresos(Disquete paramDisquete)
  {
    if (paramDisquete.jdoFlags <= 0)
      return paramDisquete.ingresos;
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDisquete.ingresos;
    if (localStateManager.isLoaded(paramDisquete, jdoInheritedFieldCount + 6))
      return paramDisquete.ingresos;
    return localStateManager.getStringField(paramDisquete, jdoInheritedFieldCount + 6, paramDisquete.ingresos);
  }

  private static final void jdoSetingresos(Disquete paramDisquete, String paramString)
  {
    if (paramDisquete.jdoFlags == 0)
    {
      paramDisquete.ingresos = paramString;
      return;
    }
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDisquete.ingresos = paramString;
      return;
    }
    localStateManager.setStringField(paramDisquete, jdoInheritedFieldCount + 6, paramDisquete.ingresos, paramString);
  }

  private static final Organismo jdoGetorganismo(Disquete paramDisquete)
  {
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDisquete.organismo;
    if (localStateManager.isLoaded(paramDisquete, jdoInheritedFieldCount + 7))
      return paramDisquete.organismo;
    return (Organismo)localStateManager.getObjectField(paramDisquete, jdoInheritedFieldCount + 7, paramDisquete.organismo);
  }

  private static final void jdoSetorganismo(Disquete paramDisquete, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDisquete.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramDisquete, jdoInheritedFieldCount + 7, paramDisquete.organismo, paramOrganismo);
  }

  private static final String jdoGettipoDisquete(Disquete paramDisquete)
  {
    if (paramDisquete.jdoFlags <= 0)
      return paramDisquete.tipoDisquete;
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramDisquete.tipoDisquete;
    if (localStateManager.isLoaded(paramDisquete, jdoInheritedFieldCount + 8))
      return paramDisquete.tipoDisquete;
    return localStateManager.getStringField(paramDisquete, jdoInheritedFieldCount + 8, paramDisquete.tipoDisquete);
  }

  private static final void jdoSettipoDisquete(Disquete paramDisquete, String paramString)
  {
    if (paramDisquete.jdoFlags == 0)
    {
      paramDisquete.tipoDisquete = paramString;
      return;
    }
    StateManager localStateManager = paramDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramDisquete.tipoDisquete = paramString;
      return;
    }
    localStateManager.setStringField(paramDisquete, jdoInheritedFieldCount + 8, paramDisquete.tipoDisquete, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}