package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class CuentaBancoPK
  implements Serializable
{
  public long idCuentaBanco;
  public String codCuentaBanco;
  public String agencia;

  public CuentaBancoPK()
  {
  }

  public CuentaBancoPK(long idCuentaBanco, String codCuentaBanco, String agencia)
  {
    this.idCuentaBanco = idCuentaBanco;
    this.codCuentaBanco = codCuentaBanco;
    this.agencia = agencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CuentaBancoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CuentaBancoPK thatPK)
  {
    if (
      this.idCuentaBanco == thatPK.idCuentaBanco)
    {
      if (this.codCuentaBanco.equals(thatPK.codCuentaBanco))
      {
        if (this.agencia.equals(thatPK.agencia)) return true;
      }
    }
    return 
      false;
  }

  public int hashCode()
  {
    return 
      (String.valueOf(this.idCuentaBanco) + 
      this.codCuentaBanco + 
      this.agencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCuentaBanco) + 
      this.codCuentaBanco + 
      this.agencia;
  }
}