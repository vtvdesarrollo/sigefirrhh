package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ParametroJubilacionPK
  implements Serializable
{
  public long idParametroJubilacion;

  public ParametroJubilacionPK()
  {
  }

  public ParametroJubilacionPK(long idParametroJubilacion)
  {
    this.idParametroJubilacion = idParametroJubilacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroJubilacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroJubilacionPK thatPK)
  {
    return 
      this.idParametroJubilacion == thatPK.idParametroJubilacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroJubilacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroJubilacion);
  }
}