package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class DiaFeriadoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDiaFeriado(DiaFeriado diaFeriado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DiaFeriado diaFeriadoNew = 
      (DiaFeriado)BeanUtils.cloneBean(
      diaFeriado);

    pm.makePersistent(diaFeriadoNew);
  }

  public void updateDiaFeriado(DiaFeriado diaFeriado) throws Exception
  {
    DiaFeriado diaFeriadoModify = 
      findDiaFeriadoById(diaFeriado.getIdDiaFeriado());

    BeanUtils.copyProperties(diaFeriadoModify, diaFeriado);
  }

  public void deleteDiaFeriado(DiaFeriado diaFeriado) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DiaFeriado diaFeriadoDelete = 
      findDiaFeriadoById(diaFeriado.getIdDiaFeriado());
    pm.deletePersistent(diaFeriadoDelete);
  }

  public DiaFeriado findDiaFeriadoById(long idDiaFeriado) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDiaFeriado == pIdDiaFeriado";
    Query query = pm.newQuery(DiaFeriado.class, filter);

    query.declareParameters("long pIdDiaFeriado");

    parameters.put("pIdDiaFeriado", new Long(idDiaFeriado));

    Collection colDiaFeriado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDiaFeriado.iterator();
    return (DiaFeriado)iterator.next();
  }

  public Collection findDiaFeriadoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent diaFeriadoExtent = pm.getExtent(
      DiaFeriado.class, true);
    Query query = pm.newQuery(diaFeriadoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByDia(Date dia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "dia == pDia";

    Query query = pm.newQuery(DiaFeriado.class, filter);

    query.declareParameters("java.util.Date pDia");
    HashMap parameters = new HashMap();

    parameters.put("pDia", dia);

    Collection colDiaFeriado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDiaFeriado);

    return colDiaFeriado;
  }
}