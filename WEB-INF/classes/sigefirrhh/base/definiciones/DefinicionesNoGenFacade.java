package sigefirrhh.base.definiciones;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class DefinicionesNoGenFacade extends DefinicionesFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DefinicionesNoGenBusiness definicionesNoGenBusiness = new DefinicionesNoGenBusiness();
  private DefinicionesBusinessExtend definicionesBusinessExtended = new DefinicionesBusinessExtend();

  public Collection findConceptoByOrganismoAndDeduccionSindicato(long idOrganismo, String deduccionSindicato)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoByOrganismoAndDeduccionSindicato(idOrganismo, deduccionSindicato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoByTipoPrestamo(long idOrganismo, String tipoPrestamo) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoByTipoPrestamo(idOrganismo, tipoPrestamo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoNominaByOrganismoAndPeriodicidad(long idOrganismo, String periodicidad) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findGrupoNominaByOrganismoAndPeriodicidad(idOrganismo, periodicidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSemanaByGrupoNominaAndAnio(long idGrupoNomina, int anio) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findSemanaByGrupoNominaAndAnio(idGrupoNomina, anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalForPrestamo(long idConceptoTipoPersonal) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalForPrestamo(idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByTipoPersonalAndConcepto(long idTipoPersonal, long idConcepto) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalByTipoPersonalAndConcepto(idTipoPersonal, idConcepto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalForIngresoTrabajador(long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalForIngresoTrabajador(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByTipoPersonalAndTipoPrestamo(long idTipoPersonal, String tipoPrestamo) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesBusinessExtended.findConceptoTipoPersonalByTipoPersonalAndTipoPrestamo(idTipoPersonal, tipoPrestamo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public boolean estanRestringidos(long idTipoPersonal, long idTipoPersonal2)
    throws Exception
  {
    return this.definicionesNoGenBusiness.estanRestringidos(idTipoPersonal, idTipoPersonal2);
  }

  public Collection findTipoPersonalByUsuaioAndOrganismo(long idUsuario, long idOrganismo, String administrador) throws Exception {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findTipoPersonalByUsuarioAndOrganismo(idUsuario, idOrganismo, administrador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalWithSeguridad(long idUsuario, long idOrganismo, String administrador)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findTipoPersonalWithSeguridad(idUsuario, idOrganismo, administrador);
  }

  public Collection findTipoPersonalWithSeguridadAndRelacionPersonal(long idUsuario, long idOrganismo, String administrador, String codRelacionPersonal)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findTipoPersonalWithSeguridadAndRelacionPersonal(idUsuario, idOrganismo, administrador, codRelacionPersonal);
  }

  public Collection findTipoPersonalByPrestaciones(long idOrganismo, long idUsuario, String administrador) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findTipoPersonalByPrestaciones(idOrganismo, idUsuario, administrador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalByArc(long idOrganismo, long idUsuario, String administrador) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findTipoPersonalByArc(idOrganismo, idUsuario, administrador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByTipoPersonalAsignaciones(long idTipoPersonal) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalByTipoPersonalAsignaciones(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByTipoPersonalSueldoBasico(long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalByTipoPersonalSueldoBasico(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByTipoPersonalAjusteSueldo(long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalByTipoPersonalAjusteSueldo(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByTipoPersonalCompensacion(long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalByTipoPersonalCompensacion(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByTipoPersonalPrimasCargo(long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalByTipoPersonalPrimasCargo(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByTipoPersonalPrimasTrabajador(long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalByTipoPersonalPrimasTrabajador(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalForContratados(long idOrganismo) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findTipoPersonalForContratados(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public int findLastNumeroCampoByDisquete(long idDisquete, String tipoRegistro)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findLastNumeroCampoByDisquete(idDisquete, tipoRegistro);
  }

  public Collection findConceptoTipoPersonalForAportePatronal(long idTipoPersonal, String aportePatronal) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalForAportePatronal(idTipoPersonal, aportePatronal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByRecalculo(long idTipoPersonal, String recalculo) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalByRecalculo(idTipoPersonal, recalculo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalForConceptoCuenta(long idTipoPersonal, long idConceptoTipoPersonal) throws Exception {
    return this.definicionesNoGenBusiness.findConceptoTipoPersonalForConceptoCuenta(idTipoPersonal, idConceptoTipoPersonal);
  }

  public Collection findConceptoTipoPersonalForConceptoCuenta(long idTipoPersonal) throws Exception {
    return this.definicionesNoGenBusiness.findConceptoTipoPersonalForConceptoCuenta(idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalByIdTipoPersonal(long idTipoPersonal) throws Exception {
    return this.definicionesNoGenBusiness.findConceptoTipoPersonalByIdTipoPersonal(idTipoPersonal);
  }

  public Semana findSemanaByAnioMesSemana(long idGrupoNomina, int anio, String mes, int semanaMes) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findSemanaByAnioMesSemana(idGrupoNomina, anio, mes, semanaMes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findBancoAll()
    throws Exception
  {
    return this.definicionesNoGenBusiness.findBancoAll();
  }

  public Collection findTipoPersonalByIdRegistro(long idRegistro)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findTipoPersonalByIdRegistro(idRegistro);
  }

  public Collection findTipoPersonalPensionadoJubilado()
    throws Exception
  {
    return this.definicionesNoGenBusiness.findTipoPersonalPensionadoJubilado();
  }

  public TarifaAri findTarifaAriByTarifaAproximada(double tarifa) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findTarifaAriByTarifaAproximada(tarifa);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public EscalaCuadroOnapre findEscalaCuadroOnapreByCodigoAproximada(String codigo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.definicionesNoGenBusiness.findEscalaCuadroOnapreByCodigoAproximada(codigo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findTipoPersonalByConcepto(long idUsuario, long idOrganismo, String administrador, String codConcepto)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findTipoPersonalByConcepto(idUsuario, idOrganismo, administrador, codConcepto);
  }

  public Collection findConceptoTipoPersonalBySobretiempo(long idTipoPersonal, String sobretiempo) throws Exception
  {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalBySobretiempo(idTipoPersonal, sobretiempo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalByPersonal(long idPersonal, String estatus)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findTipoPersonalByPersonal(idPersonal, estatus);
  }

  public Collection findConceptoTipoPersonalByBeneficio(long idTipoPersonal, String beneficio)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findConceptoTipoPersonalByBeneficio(idTipoPersonal, beneficio);
  }

  public Collection findGrupoNominaWithSeguridad(long idUsuario, long idOrganismo, String administrador)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findGrupoNominaWithSeguridad(idUsuario, idOrganismo, administrador);
  }

  public double verificarTopeSobretiempo(long idConceptoTipoPersonal, long idTrabajador, int mes, int anio, double unidadesAgregar) throws Exception
  {
    return this.definicionesNoGenBusiness.verificarTopeSobretiempo(idConceptoTipoPersonal, idTrabajador, mes, anio, unidadesAgregar);
  }

  public Collection findConceptoTipoPersonalByDistribucion(long idTipoPersonal, String distribucion) throws Exception {
    try {
      this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalByDistribucion(idTipoPersonal, distribucion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoTipoPersonalByRetroactivo(long idTipoPersonal, String retroactivo) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findConceptoTipoPersonalByRetroactivo(idTipoPersonal, retroactivo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalByCategoriaPresupuesto(long idCategoriaPresupuesto) throws Exception
  {
    try { this.txn.open();
      return this.definicionesNoGenBusiness.findTipoPersonalByCategoriaPresupuesto(idCategoriaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoPersonalWithSeguridadDocenteIngreso(long idUsuario, long idOrganismo, String administrador)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findTipoPersonalWithSeguridadDocenteIngreso(idUsuario, idOrganismo, administrador);
  }

  public Collection findTipoPersonalWithSeguridadRelacionCategoriaAprobacionMpd(long idUsuario, long idOrganismo, String administrador, String codRelacionPersonal, String codCategoriaPersonal, String aprobacionMpd)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findTipoPersonalWithSeguridadRelacionCategoriaAprobacionMpd(idUsuario, idOrganismo, administrador, codRelacionPersonal, codCategoriaPersonal, aprobacionMpd);
  }

  public Collection findTipoPersonalWithSeguridadForViejoRegimen(long idUsuario, long idOrganismo, String administrador)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findTipoPersonalWithSeguridadForViejoRegimen(idUsuario, idOrganismo, administrador);
  }

  public Collection findTipoPersonalWithSeguridadForInteresAdicional(long idUsuario, long idOrganismo, String administrador, String estatus)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findTipoPersonalWithSeguridadForInteresAdicional(idUsuario, idOrganismo, administrador, estatus);
  }

  public Collection findTipoPersonalWithSeguridadAndCriterio(long idUsuario, long idOrganismo, String administrador, String criterio)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findTipoPersonalWithSeguridadAndCriterio(idUsuario, idOrganismo, administrador, criterio);
  }

  public Collection findCategoriaPresupuestoByCestaTicket(long idUsuario, long idOrganismo, String administrador)
    throws Exception
  {
    return this.definicionesNoGenBusiness.findCategoriaPresupuestoByCestaTicket(idUsuario, idOrganismo, administrador);
  }
}