package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ClasificacionPersonalPK
  implements Serializable
{
  public long idClasificacionPersonal;

  public ClasificacionPersonalPK()
  {
  }

  public ClasificacionPersonalPK(long idClasificacionPersonal)
  {
    this.idClasificacionPersonal = idClasificacionPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ClasificacionPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ClasificacionPersonalPK thatPK)
  {
    return 
      this.idClasificacionPersonal == thatPK.idClasificacionPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idClasificacionPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idClasificacionPersonal);
  }
}