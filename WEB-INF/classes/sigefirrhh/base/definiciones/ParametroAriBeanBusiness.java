package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ParametroAriBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParametroAri(ParametroAri parametroAri)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ParametroAri parametroAriNew = 
      (ParametroAri)BeanUtils.cloneBean(
      parametroAri);

    pm.makePersistent(parametroAriNew);
  }

  public void updateParametroAri(ParametroAri parametroAri) throws Exception
  {
    ParametroAri parametroAriModify = 
      findParametroAriById(parametroAri.getIdParametroAri());

    BeanUtils.copyProperties(parametroAriModify, parametroAri);
  }

  public void deleteParametroAri(ParametroAri parametroAri) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ParametroAri parametroAriDelete = 
      findParametroAriById(parametroAri.getIdParametroAri());
    pm.deletePersistent(parametroAriDelete);
  }

  public ParametroAri findParametroAriById(long idParametroAri) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParametroAri == pIdParametroAri";
    Query query = pm.newQuery(ParametroAri.class, filter);

    query.declareParameters("long pIdParametroAri");

    parameters.put("pIdParametroAri", new Long(idParametroAri));

    Collection colParametroAri = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParametroAri.iterator();
    return (ParametroAri)iterator.next();
  }

  public Collection findParametroAriAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parametroAriExtent = pm.getExtent(
      ParametroAri.class, true);
    Query query = pm.newQuery(parametroAriExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUnidadTributaria(double unidadTributaria)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadTributaria == pUnidadTributaria";

    Query query = pm.newQuery(ParametroAri.class, filter);

    query.declareParameters("double pUnidadTributaria");
    HashMap parameters = new HashMap();

    parameters.put("pUnidadTributaria", new Double(unidadTributaria));

    Collection colParametroAri = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParametroAri);

    return colParametroAri;
  }
}