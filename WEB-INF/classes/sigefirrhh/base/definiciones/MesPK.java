package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class MesPK
  implements Serializable
{
  public long idMes;

  public MesPK()
  {
  }

  public MesPK(long idMes)
  {
    this.idMes = idMes;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((MesPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(MesPK thatPK)
  {
    return 
      this.idMes == thatPK.idMes;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idMes)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idMes);
  }
}