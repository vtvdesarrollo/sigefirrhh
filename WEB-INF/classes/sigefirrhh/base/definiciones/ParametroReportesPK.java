package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ParametroReportesPK
  implements Serializable
{
  public long idParametroReportes;

  public ParametroReportesPK()
  {
  }

  public ParametroReportesPK(long idParametroReportes)
  {
    this.idParametroReportes = idParametroReportes;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroReportesPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroReportesPK thatPK)
  {
    return 
      this.idParametroReportes == thatPK.idParametroReportes;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroReportes)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroReportes);
  }
}