package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class UtilidadesPorAnioPK
  implements Serializable
{
  public long idUtilidadesPorAnio;

  public UtilidadesPorAnioPK()
  {
  }

  public UtilidadesPorAnioPK(long idUtilidadesPorAnio)
  {
    this.idUtilidadesPorAnio = idUtilidadesPorAnio;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UtilidadesPorAnioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UtilidadesPorAnioPK thatPK)
  {
    return 
      this.idUtilidadesPorAnio == thatPK.idUtilidadesPorAnio;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUtilidadesPorAnio)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUtilidadesPorAnio);
  }
}