package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class TipoContratoPK
  implements Serializable
{
  public long idTipoContrato;

  public TipoContratoPK()
  {
  }

  public TipoContratoPK(long idTipoContrato)
  {
    this.idTipoContrato = idTipoContrato;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoContratoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoContratoPK thatPK)
  {
    return 
      this.idTipoContrato == thatPK.idTipoContrato;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoContrato)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoContrato);
  }
}