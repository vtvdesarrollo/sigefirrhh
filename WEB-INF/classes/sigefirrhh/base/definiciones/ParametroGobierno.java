package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.GrupoOrganismo;

public class ParametroGobierno
  implements Serializable, PersistenceCapable
{
  private long idParametroGobierno;
  private GrupoOrganismo grupoOrganismo;
  private double limsemSso;
  private double limmenSso;
  private double limsemSpf;
  private double limmenSpf;
  private int edadmascSso;
  private int edadfemSso;
  private double porcentajeIntegral;
  private double porcentajeParcial;
  private double porcbpatSso;
  private double porcmpatSso;
  private double porcapatSso;
  private double porctrabSpf;
  private double porcpatSpf;
  private double limsemLph;
  private double limmenLph;
  private int edadmascLph;
  private int edadfemLph;
  private double porctrabLph;
  private double porcpatLph;
  private double limsemFju;
  private double limmenFju;
  private int edadmascFju;
  private int edadfemFju;
  private double porctrabFju;
  private double porcpatFju;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "edadfemFju", "edadfemLph", "edadfemSso", "edadmascFju", "edadmascLph", "edadmascSso", "grupoOrganismo", "idParametroGobierno", "limmenFju", "limmenLph", "limmenSpf", "limmenSso", "limsemFju", "limsemLph", "limsemSpf", "limsemSso", "porcapatSso", "porcbpatSso", "porcentajeIntegral", "porcentajeParcial", "porcmpatSso", "porcpatFju", "porcpatLph", "porcpatSpf", "porctrabFju", "porctrabLph", "porctrabSpf" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.GrupoOrganismo"), Long.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 26, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ParametroGobierno()
  {
    jdoSetlimsemSso(this, 0.0D);

    jdoSetlimmenSso(this, 0.0D);

    jdoSetlimsemSpf(this, 0.0D);

    jdoSetlimmenSpf(this, 0.0D);

    jdoSetedadmascSso(this, 0);

    jdoSetedadfemSso(this, 0);

    jdoSetporcbpatSso(this, 0.0D);

    jdoSetporcmpatSso(this, 0.0D);

    jdoSetporcapatSso(this, 0.0D);

    jdoSetporctrabSpf(this, 0.0D);

    jdoSetporcpatSpf(this, 0.0D);

    jdoSetlimsemLph(this, 0.0D);

    jdoSetlimmenLph(this, 0.0D);

    jdoSetedadmascLph(this, 0);

    jdoSetedadfemLph(this, 0);

    jdoSetporctrabLph(this, 0.0D);

    jdoSetporcpatLph(this, 0.0D);

    jdoSetlimsemFju(this, 0.0D);

    jdoSetlimmenFju(this, 0.0D);

    jdoSetedadmascFju(this, 0);

    jdoSetedadfemFju(this, 0);

    jdoSetporctrabFju(this, 0.0D);

    jdoSetporcpatFju(this, 0.0D);
  }

  public String toString() {
    return "SSO  -  " + jdoGetlimmenSso(this) + "  -  " + 
      jdoGetlimsemSso(this);
  }

  public int getEdadfemFju()
  {
    return jdoGetedadfemFju(this);
  }

  public int getEdadfemLph()
  {
    return jdoGetedadfemLph(this);
  }

  public int getEdadfemSso()
  {
    return jdoGetedadfemSso(this);
  }

  public int getEdadmascFju()
  {
    return jdoGetedadmascFju(this);
  }

  public int getEdadmascLph()
  {
    return jdoGetedadmascLph(this);
  }

  public int getEdadmascSso()
  {
    return jdoGetedadmascSso(this);
  }

  public GrupoOrganismo getGrupoOrganismo()
  {
    return jdoGetgrupoOrganismo(this);
  }

  public long getIdParametroGobierno()
  {
    return jdoGetidParametroGobierno(this);
  }

  public double getLimmenFju()
  {
    return jdoGetlimmenFju(this);
  }

  public double getLimmenLph()
  {
    return jdoGetlimmenLph(this);
  }

  public double getLimmenSso()
  {
    return jdoGetlimmenSso(this);
  }

  public double getLimsemFju()
  {
    return jdoGetlimsemFju(this);
  }

  public double getLimsemLph()
  {
    return jdoGetlimsemLph(this);
  }

  public double getLimsemSso()
  {
    return jdoGetlimsemSso(this);
  }

  public double getPorcapatSso()
  {
    return jdoGetporcapatSso(this);
  }

  public double getPorcbpatSso()
  {
    return jdoGetporcbpatSso(this);
  }

  public double getPorcmpatSso()
  {
    return jdoGetporcmpatSso(this);
  }

  public double getPorcpatFju()
  {
    return jdoGetporcpatFju(this);
  }

  public double getPorcpatLph()
  {
    return jdoGetporcpatLph(this);
  }

  public double getPorcpatSpf()
  {
    return jdoGetporcpatSpf(this);
  }

  public double getPorctrabFju()
  {
    return jdoGetporctrabFju(this);
  }

  public double getPorctrabLph()
  {
    return jdoGetporctrabLph(this);
  }

  public double getPorctrabSpf()
  {
    return jdoGetporctrabSpf(this);
  }

  public void setEdadfemFju(int i)
  {
    jdoSetedadfemFju(this, i);
  }

  public void setEdadfemLph(int i)
  {
    jdoSetedadfemLph(this, i);
  }

  public void setEdadfemSso(int i)
  {
    jdoSetedadfemSso(this, i);
  }

  public void setEdadmascFju(int i)
  {
    jdoSetedadmascFju(this, i);
  }

  public void setEdadmascLph(int i)
  {
    jdoSetedadmascLph(this, i);
  }

  public void setEdadmascSso(int i)
  {
    jdoSetedadmascSso(this, i);
  }

  public void setGrupoOrganismo(GrupoOrganismo organismo)
  {
    jdoSetgrupoOrganismo(this, organismo);
  }

  public void setIdParametroGobierno(long l)
  {
    jdoSetidParametroGobierno(this, l);
  }

  public void setLimmenFju(double d)
  {
    jdoSetlimmenFju(this, d);
  }

  public void setLimmenLph(double d)
  {
    jdoSetlimmenLph(this, d);
  }

  public void setLimmenSso(double d)
  {
    jdoSetlimmenSso(this, d);
  }

  public void setLimsemFju(double d)
  {
    jdoSetlimsemFju(this, d);
  }

  public void setLimsemLph(double d)
  {
    jdoSetlimsemLph(this, d);
  }

  public void setLimsemSso(double d)
  {
    jdoSetlimsemSso(this, d);
  }

  public void setPorcapatSso(double d)
  {
    jdoSetporcapatSso(this, d);
  }

  public void setPorcbpatSso(double d)
  {
    jdoSetporcbpatSso(this, d);
  }

  public void setPorcmpatSso(double d)
  {
    jdoSetporcmpatSso(this, d);
  }

  public void setPorcpatFju(double d)
  {
    jdoSetporcpatFju(this, d);
  }

  public void setPorcpatLph(double d)
  {
    jdoSetporcpatLph(this, d);
  }

  public void setPorcpatSpf(double d)
  {
    jdoSetporcpatSpf(this, d);
  }

  public void setPorctrabFju(double d)
  {
    jdoSetporctrabFju(this, d);
  }

  public void setPorctrabLph(double d)
  {
    jdoSetporctrabLph(this, d);
  }

  public void setPorctrabSpf(double d)
  {
    jdoSetporctrabSpf(this, d);
  }

  public double getPorcentajeIntegral()
  {
    return jdoGetporcentajeIntegral(this);
  }

  public double getPorcentajeParcial()
  {
    return jdoGetporcentajeParcial(this);
  }

  public void setPorcentajeIntegral(double d)
  {
    jdoSetporcentajeIntegral(this, d);
  }

  public void setPorcentajeParcial(double d)
  {
    jdoSetporcentajeParcial(this, d);
  }

  public double getLimmenSpf()
  {
    return jdoGetlimmenSpf(this);
  }

  public double getLimsemSpf()
  {
    return jdoGetlimsemSpf(this);
  }

  public void setLimmenSpf(double d)
  {
    jdoSetlimmenSpf(this, d);
  }

  public void setLimsemSpf(double d)
  {
    jdoSetlimsemSpf(this, d);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 27;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ParametroGobierno"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroGobierno());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroGobierno localParametroGobierno = new ParametroGobierno();
    localParametroGobierno.jdoFlags = 1;
    localParametroGobierno.jdoStateManager = paramStateManager;
    return localParametroGobierno;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroGobierno localParametroGobierno = new ParametroGobierno();
    localParametroGobierno.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroGobierno.jdoFlags = 1;
    localParametroGobierno.jdoStateManager = paramStateManager;
    return localParametroGobierno;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadfemFju);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadfemLph);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadfemSso);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadmascFju);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadmascLph);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadmascSso);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoOrganismo);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroGobierno);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.limmenFju);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.limmenLph);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.limmenSpf);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.limmenSso);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.limsemFju);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.limsemLph);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.limsemSpf);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.limsemSso);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcapatSso);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcbpatSso);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeIntegral);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeParcial);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcmpatSso);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcpatFju);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcpatLph);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcpatSpf);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porctrabFju);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porctrabLph);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porctrabSpf);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadfemFju = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadfemLph = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadfemSso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadmascFju = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadmascLph = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadmascSso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoOrganismo = ((GrupoOrganismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroGobierno = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.limmenFju = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.limmenLph = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.limmenSpf = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.limmenSso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.limsemFju = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.limsemLph = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.limsemSpf = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.limsemSso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcapatSso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcbpatSso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeIntegral = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeParcial = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcmpatSso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcpatFju = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcpatLph = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcpatSpf = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porctrabFju = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porctrabLph = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porctrabSpf = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroGobierno paramParametroGobierno, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.edadfemFju = paramParametroGobierno.edadfemFju;
      return;
    case 1:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.edadfemLph = paramParametroGobierno.edadfemLph;
      return;
    case 2:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.edadfemSso = paramParametroGobierno.edadfemSso;
      return;
    case 3:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.edadmascFju = paramParametroGobierno.edadmascFju;
      return;
    case 4:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.edadmascLph = paramParametroGobierno.edadmascLph;
      return;
    case 5:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.edadmascSso = paramParametroGobierno.edadmascSso;
      return;
    case 6:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.grupoOrganismo = paramParametroGobierno.grupoOrganismo;
      return;
    case 7:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroGobierno = paramParametroGobierno.idParametroGobierno;
      return;
    case 8:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.limmenFju = paramParametroGobierno.limmenFju;
      return;
    case 9:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.limmenLph = paramParametroGobierno.limmenLph;
      return;
    case 10:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.limmenSpf = paramParametroGobierno.limmenSpf;
      return;
    case 11:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.limmenSso = paramParametroGobierno.limmenSso;
      return;
    case 12:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.limsemFju = paramParametroGobierno.limsemFju;
      return;
    case 13:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.limsemLph = paramParametroGobierno.limsemLph;
      return;
    case 14:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.limsemSpf = paramParametroGobierno.limsemSpf;
      return;
    case 15:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.limsemSso = paramParametroGobierno.limsemSso;
      return;
    case 16:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.porcapatSso = paramParametroGobierno.porcapatSso;
      return;
    case 17:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.porcbpatSso = paramParametroGobierno.porcbpatSso;
      return;
    case 18:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeIntegral = paramParametroGobierno.porcentajeIntegral;
      return;
    case 19:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeParcial = paramParametroGobierno.porcentajeParcial;
      return;
    case 20:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.porcmpatSso = paramParametroGobierno.porcmpatSso;
      return;
    case 21:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.porcpatFju = paramParametroGobierno.porcpatFju;
      return;
    case 22:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.porcpatLph = paramParametroGobierno.porcpatLph;
      return;
    case 23:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.porcpatSpf = paramParametroGobierno.porcpatSpf;
      return;
    case 24:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.porctrabFju = paramParametroGobierno.porctrabFju;
      return;
    case 25:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.porctrabLph = paramParametroGobierno.porctrabLph;
      return;
    case 26:
      if (paramParametroGobierno == null)
        throw new IllegalArgumentException("arg1");
      this.porctrabSpf = paramParametroGobierno.porctrabSpf;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroGobierno))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroGobierno localParametroGobierno = (ParametroGobierno)paramObject;
    if (localParametroGobierno.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroGobierno, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroGobiernoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroGobiernoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroGobiernoPK))
      throw new IllegalArgumentException("arg1");
    ParametroGobiernoPK localParametroGobiernoPK = (ParametroGobiernoPK)paramObject;
    localParametroGobiernoPK.idParametroGobierno = this.idParametroGobierno;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroGobiernoPK))
      throw new IllegalArgumentException("arg1");
    ParametroGobiernoPK localParametroGobiernoPK = (ParametroGobiernoPK)paramObject;
    this.idParametroGobierno = localParametroGobiernoPK.idParametroGobierno;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroGobiernoPK))
      throw new IllegalArgumentException("arg2");
    ParametroGobiernoPK localParametroGobiernoPK = (ParametroGobiernoPK)paramObject;
    localParametroGobiernoPK.idParametroGobierno = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroGobiernoPK))
      throw new IllegalArgumentException("arg2");
    ParametroGobiernoPK localParametroGobiernoPK = (ParametroGobiernoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localParametroGobiernoPK.idParametroGobierno);
  }

  private static final int jdoGetedadfemFju(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.edadfemFju;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.edadfemFju;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 0))
      return paramParametroGobierno.edadfemFju;
    return localStateManager.getIntField(paramParametroGobierno, jdoInheritedFieldCount + 0, paramParametroGobierno.edadfemFju);
  }

  private static final void jdoSetedadfemFju(ParametroGobierno paramParametroGobierno, int paramInt)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.edadfemFju = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.edadfemFju = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroGobierno, jdoInheritedFieldCount + 0, paramParametroGobierno.edadfemFju, paramInt);
  }

  private static final int jdoGetedadfemLph(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.edadfemLph;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.edadfemLph;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 1))
      return paramParametroGobierno.edadfemLph;
    return localStateManager.getIntField(paramParametroGobierno, jdoInheritedFieldCount + 1, paramParametroGobierno.edadfemLph);
  }

  private static final void jdoSetedadfemLph(ParametroGobierno paramParametroGobierno, int paramInt)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.edadfemLph = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.edadfemLph = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroGobierno, jdoInheritedFieldCount + 1, paramParametroGobierno.edadfemLph, paramInt);
  }

  private static final int jdoGetedadfemSso(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.edadfemSso;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.edadfemSso;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 2))
      return paramParametroGobierno.edadfemSso;
    return localStateManager.getIntField(paramParametroGobierno, jdoInheritedFieldCount + 2, paramParametroGobierno.edadfemSso);
  }

  private static final void jdoSetedadfemSso(ParametroGobierno paramParametroGobierno, int paramInt)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.edadfemSso = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.edadfemSso = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroGobierno, jdoInheritedFieldCount + 2, paramParametroGobierno.edadfemSso, paramInt);
  }

  private static final int jdoGetedadmascFju(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.edadmascFju;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.edadmascFju;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 3))
      return paramParametroGobierno.edadmascFju;
    return localStateManager.getIntField(paramParametroGobierno, jdoInheritedFieldCount + 3, paramParametroGobierno.edadmascFju);
  }

  private static final void jdoSetedadmascFju(ParametroGobierno paramParametroGobierno, int paramInt)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.edadmascFju = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.edadmascFju = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroGobierno, jdoInheritedFieldCount + 3, paramParametroGobierno.edadmascFju, paramInt);
  }

  private static final int jdoGetedadmascLph(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.edadmascLph;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.edadmascLph;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 4))
      return paramParametroGobierno.edadmascLph;
    return localStateManager.getIntField(paramParametroGobierno, jdoInheritedFieldCount + 4, paramParametroGobierno.edadmascLph);
  }

  private static final void jdoSetedadmascLph(ParametroGobierno paramParametroGobierno, int paramInt)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.edadmascLph = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.edadmascLph = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroGobierno, jdoInheritedFieldCount + 4, paramParametroGobierno.edadmascLph, paramInt);
  }

  private static final int jdoGetedadmascSso(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.edadmascSso;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.edadmascSso;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 5))
      return paramParametroGobierno.edadmascSso;
    return localStateManager.getIntField(paramParametroGobierno, jdoInheritedFieldCount + 5, paramParametroGobierno.edadmascSso);
  }

  private static final void jdoSetedadmascSso(ParametroGobierno paramParametroGobierno, int paramInt)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.edadmascSso = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.edadmascSso = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroGobierno, jdoInheritedFieldCount + 5, paramParametroGobierno.edadmascSso, paramInt);
  }

  private static final GrupoOrganismo jdoGetgrupoOrganismo(ParametroGobierno paramParametroGobierno)
  {
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.grupoOrganismo;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 6))
      return paramParametroGobierno.grupoOrganismo;
    return (GrupoOrganismo)localStateManager.getObjectField(paramParametroGobierno, jdoInheritedFieldCount + 6, paramParametroGobierno.grupoOrganismo);
  }

  private static final void jdoSetgrupoOrganismo(ParametroGobierno paramParametroGobierno, GrupoOrganismo paramGrupoOrganismo)
  {
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.grupoOrganismo = paramGrupoOrganismo;
      return;
    }
    localStateManager.setObjectField(paramParametroGobierno, jdoInheritedFieldCount + 6, paramParametroGobierno.grupoOrganismo, paramGrupoOrganismo);
  }

  private static final long jdoGetidParametroGobierno(ParametroGobierno paramParametroGobierno)
  {
    return paramParametroGobierno.idParametroGobierno;
  }

  private static final void jdoSetidParametroGobierno(ParametroGobierno paramParametroGobierno, long paramLong)
  {
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.idParametroGobierno = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroGobierno, jdoInheritedFieldCount + 7, paramParametroGobierno.idParametroGobierno, paramLong);
  }

  private static final double jdoGetlimmenFju(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.limmenFju;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.limmenFju;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 8))
      return paramParametroGobierno.limmenFju;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 8, paramParametroGobierno.limmenFju);
  }

  private static final void jdoSetlimmenFju(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.limmenFju = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.limmenFju = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 8, paramParametroGobierno.limmenFju, paramDouble);
  }

  private static final double jdoGetlimmenLph(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.limmenLph;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.limmenLph;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 9))
      return paramParametroGobierno.limmenLph;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 9, paramParametroGobierno.limmenLph);
  }

  private static final void jdoSetlimmenLph(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.limmenLph = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.limmenLph = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 9, paramParametroGobierno.limmenLph, paramDouble);
  }

  private static final double jdoGetlimmenSpf(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.limmenSpf;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.limmenSpf;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 10))
      return paramParametroGobierno.limmenSpf;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 10, paramParametroGobierno.limmenSpf);
  }

  private static final void jdoSetlimmenSpf(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.limmenSpf = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.limmenSpf = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 10, paramParametroGobierno.limmenSpf, paramDouble);
  }

  private static final double jdoGetlimmenSso(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.limmenSso;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.limmenSso;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 11))
      return paramParametroGobierno.limmenSso;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 11, paramParametroGobierno.limmenSso);
  }

  private static final void jdoSetlimmenSso(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.limmenSso = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.limmenSso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 11, paramParametroGobierno.limmenSso, paramDouble);
  }

  private static final double jdoGetlimsemFju(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.limsemFju;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.limsemFju;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 12))
      return paramParametroGobierno.limsemFju;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 12, paramParametroGobierno.limsemFju);
  }

  private static final void jdoSetlimsemFju(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.limsemFju = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.limsemFju = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 12, paramParametroGobierno.limsemFju, paramDouble);
  }

  private static final double jdoGetlimsemLph(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.limsemLph;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.limsemLph;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 13))
      return paramParametroGobierno.limsemLph;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 13, paramParametroGobierno.limsemLph);
  }

  private static final void jdoSetlimsemLph(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.limsemLph = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.limsemLph = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 13, paramParametroGobierno.limsemLph, paramDouble);
  }

  private static final double jdoGetlimsemSpf(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.limsemSpf;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.limsemSpf;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 14))
      return paramParametroGobierno.limsemSpf;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 14, paramParametroGobierno.limsemSpf);
  }

  private static final void jdoSetlimsemSpf(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.limsemSpf = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.limsemSpf = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 14, paramParametroGobierno.limsemSpf, paramDouble);
  }

  private static final double jdoGetlimsemSso(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.limsemSso;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.limsemSso;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 15))
      return paramParametroGobierno.limsemSso;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 15, paramParametroGobierno.limsemSso);
  }

  private static final void jdoSetlimsemSso(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.limsemSso = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.limsemSso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 15, paramParametroGobierno.limsemSso, paramDouble);
  }

  private static final double jdoGetporcapatSso(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.porcapatSso;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.porcapatSso;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 16))
      return paramParametroGobierno.porcapatSso;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 16, paramParametroGobierno.porcapatSso);
  }

  private static final void jdoSetporcapatSso(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.porcapatSso = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.porcapatSso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 16, paramParametroGobierno.porcapatSso, paramDouble);
  }

  private static final double jdoGetporcbpatSso(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.porcbpatSso;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.porcbpatSso;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 17))
      return paramParametroGobierno.porcbpatSso;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 17, paramParametroGobierno.porcbpatSso);
  }

  private static final void jdoSetporcbpatSso(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.porcbpatSso = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.porcbpatSso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 17, paramParametroGobierno.porcbpatSso, paramDouble);
  }

  private static final double jdoGetporcentajeIntegral(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.porcentajeIntegral;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.porcentajeIntegral;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 18))
      return paramParametroGobierno.porcentajeIntegral;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 18, paramParametroGobierno.porcentajeIntegral);
  }

  private static final void jdoSetporcentajeIntegral(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.porcentajeIntegral = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.porcentajeIntegral = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 18, paramParametroGobierno.porcentajeIntegral, paramDouble);
  }

  private static final double jdoGetporcentajeParcial(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.porcentajeParcial;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.porcentajeParcial;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 19))
      return paramParametroGobierno.porcentajeParcial;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 19, paramParametroGobierno.porcentajeParcial);
  }

  private static final void jdoSetporcentajeParcial(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.porcentajeParcial = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.porcentajeParcial = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 19, paramParametroGobierno.porcentajeParcial, paramDouble);
  }

  private static final double jdoGetporcmpatSso(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.porcmpatSso;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.porcmpatSso;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 20))
      return paramParametroGobierno.porcmpatSso;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 20, paramParametroGobierno.porcmpatSso);
  }

  private static final void jdoSetporcmpatSso(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.porcmpatSso = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.porcmpatSso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 20, paramParametroGobierno.porcmpatSso, paramDouble);
  }

  private static final double jdoGetporcpatFju(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.porcpatFju;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.porcpatFju;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 21))
      return paramParametroGobierno.porcpatFju;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 21, paramParametroGobierno.porcpatFju);
  }

  private static final void jdoSetporcpatFju(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.porcpatFju = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.porcpatFju = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 21, paramParametroGobierno.porcpatFju, paramDouble);
  }

  private static final double jdoGetporcpatLph(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.porcpatLph;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.porcpatLph;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 22))
      return paramParametroGobierno.porcpatLph;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 22, paramParametroGobierno.porcpatLph);
  }

  private static final void jdoSetporcpatLph(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.porcpatLph = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.porcpatLph = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 22, paramParametroGobierno.porcpatLph, paramDouble);
  }

  private static final double jdoGetporcpatSpf(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.porcpatSpf;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.porcpatSpf;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 23))
      return paramParametroGobierno.porcpatSpf;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 23, paramParametroGobierno.porcpatSpf);
  }

  private static final void jdoSetporcpatSpf(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.porcpatSpf = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.porcpatSpf = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 23, paramParametroGobierno.porcpatSpf, paramDouble);
  }

  private static final double jdoGetporctrabFju(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.porctrabFju;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.porctrabFju;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 24))
      return paramParametroGobierno.porctrabFju;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 24, paramParametroGobierno.porctrabFju);
  }

  private static final void jdoSetporctrabFju(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.porctrabFju = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.porctrabFju = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 24, paramParametroGobierno.porctrabFju, paramDouble);
  }

  private static final double jdoGetporctrabLph(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.porctrabLph;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.porctrabLph;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 25))
      return paramParametroGobierno.porctrabLph;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 25, paramParametroGobierno.porctrabLph);
  }

  private static final void jdoSetporctrabLph(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.porctrabLph = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.porctrabLph = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 25, paramParametroGobierno.porctrabLph, paramDouble);
  }

  private static final double jdoGetporctrabSpf(ParametroGobierno paramParametroGobierno)
  {
    if (paramParametroGobierno.jdoFlags <= 0)
      return paramParametroGobierno.porctrabSpf;
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
      return paramParametroGobierno.porctrabSpf;
    if (localStateManager.isLoaded(paramParametroGobierno, jdoInheritedFieldCount + 26))
      return paramParametroGobierno.porctrabSpf;
    return localStateManager.getDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 26, paramParametroGobierno.porctrabSpf);
  }

  private static final void jdoSetporctrabSpf(ParametroGobierno paramParametroGobierno, double paramDouble)
  {
    if (paramParametroGobierno.jdoFlags == 0)
    {
      paramParametroGobierno.porctrabSpf = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroGobierno.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroGobierno.porctrabSpf = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroGobierno, jdoInheritedFieldCount + 26, paramParametroGobierno.porctrabSpf, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}