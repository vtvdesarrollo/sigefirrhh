package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class SindicatoPK
  implements Serializable
{
  public long idSindicato;

  public SindicatoPK()
  {
  }

  public SindicatoPK(long idSindicato)
  {
    this.idSindicato = idSindicato;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SindicatoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SindicatoPK thatPK)
  {
    return 
      this.idSindicato == thatPK.idSindicato;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSindicato)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSindicato);
  }
}