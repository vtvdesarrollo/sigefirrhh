package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class Turno
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  protected static final Map LISTA_JORNADA;
  private long idTurno;
  private String codTurno;
  private String nombre;
  private String tipoTurno;
  private String jornada;
  private double jornadaSemanal;
  private double jornadaDiaria;
  private double entradaAm;
  private double salidaAm;
  private double entradaPm;
  private double salidaPm;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codTurno", "entradaAm", "entradaPm", "idTurno", "jornada", "jornadaDiaria", "jornadaSemanal", "nombre", "organismo", "salidaAm", "salidaPm", "tipoTurno" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21, 21, 26, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.Turno"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Turno());

    LISTA_TIPO = 
      new LinkedHashMap();
    LISTA_JORNADA = 
      new LinkedHashMap();
    LISTA_TIPO.put("D", "DIURNO");
    LISTA_TIPO.put("N", "NOCTURNO");
    LISTA_TIPO.put("M", "MIXTO");
    LISTA_JORNADA.put("C", "TIEMPO COMPLETO");
    LISTA_JORNADA.put("P", "TIEMPO PARCIAL");
  }

  public Turno()
  {
    jdoSettipoTurno(this, "D");

    jdoSetjornada(this, "D");

    jdoSetjornadaSemanal(this, 0.0D);

    jdoSetjornadaDiaria(this, 0.0D);

    jdoSetentradaAm(this, 0.0D);

    jdoSetsalidaAm(this, 0.0D);

    jdoSetentradaPm(this, 0.0D);

    jdoSetsalidaPm(this, 0.0D);
  }

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodTurno(this);
  }

  public String getCodTurno()
  {
    return jdoGetcodTurno(this);
  }

  public double getEntradaAm()
  {
    return jdoGetentradaAm(this);
  }

  public double getEntradaPm()
  {
    return jdoGetentradaPm(this);
  }

  public long getIdTurno()
  {
    return jdoGetidTurno(this);
  }

  public double getJornadaDiaria()
  {
    return jdoGetjornadaDiaria(this);
  }

  public double getJornadaSemanal()
  {
    return jdoGetjornadaSemanal(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public double getSalidaAm()
  {
    return jdoGetsalidaAm(this);
  }

  public double getSalidaPm()
  {
    return jdoGetsalidaPm(this);
  }

  public String getTipoTurno()
  {
    return jdoGettipoTurno(this);
  }

  public void setCodTurno(String string)
  {
    jdoSetcodTurno(this, string);
  }

  public void setEntradaAm(double d)
  {
    jdoSetentradaAm(this, d);
  }

  public void setEntradaPm(double d)
  {
    jdoSetentradaPm(this, d);
  }

  public void setIdTurno(long l)
  {
    jdoSetidTurno(this, l);
  }

  public void setJornadaDiaria(double d)
  {
    jdoSetjornadaDiaria(this, d);
  }

  public void setJornadaSemanal(double d)
  {
    jdoSetjornadaSemanal(this, d);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setSalidaAm(double d)
  {
    jdoSetsalidaAm(this, d);
  }

  public void setSalidaPm(double d)
  {
    jdoSetsalidaPm(this, d);
  }

  public void setTipoTurno(String string)
  {
    jdoSettipoTurno(this, string);
  }

  public String getJornada()
  {
    return jdoGetjornada(this);
  }

  public void setJornada(String string)
  {
    jdoSetjornada(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 12;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Turno localTurno = new Turno();
    localTurno.jdoFlags = 1;
    localTurno.jdoStateManager = paramStateManager;
    return localTurno;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Turno localTurno = new Turno();
    localTurno.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTurno.jdoFlags = 1;
    localTurno.jdoStateManager = paramStateManager;
    return localTurno;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTurno);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.entradaAm);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.entradaPm);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTurno);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.jornada);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.jornadaDiaria);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.jornadaSemanal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.salidaAm);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.salidaPm);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoTurno);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTurno = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entradaAm = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entradaPm = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTurno = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.jornada = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.jornadaDiaria = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.jornadaSemanal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.salidaAm = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.salidaPm = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoTurno = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Turno paramTurno, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.codTurno = paramTurno.codTurno;
      return;
    case 1:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.entradaAm = paramTurno.entradaAm;
      return;
    case 2:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.entradaPm = paramTurno.entradaPm;
      return;
    case 3:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.idTurno = paramTurno.idTurno;
      return;
    case 4:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.jornada = paramTurno.jornada;
      return;
    case 5:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.jornadaDiaria = paramTurno.jornadaDiaria;
      return;
    case 6:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.jornadaSemanal = paramTurno.jornadaSemanal;
      return;
    case 7:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramTurno.nombre;
      return;
    case 8:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramTurno.organismo;
      return;
    case 9:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.salidaAm = paramTurno.salidaAm;
      return;
    case 10:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.salidaPm = paramTurno.salidaPm;
      return;
    case 11:
      if (paramTurno == null)
        throw new IllegalArgumentException("arg1");
      this.tipoTurno = paramTurno.tipoTurno;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Turno))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Turno localTurno = (Turno)paramObject;
    if (localTurno.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTurno, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TurnoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TurnoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TurnoPK))
      throw new IllegalArgumentException("arg1");
    TurnoPK localTurnoPK = (TurnoPK)paramObject;
    localTurnoPK.idTurno = this.idTurno;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TurnoPK))
      throw new IllegalArgumentException("arg1");
    TurnoPK localTurnoPK = (TurnoPK)paramObject;
    this.idTurno = localTurnoPK.idTurno;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TurnoPK))
      throw new IllegalArgumentException("arg2");
    TurnoPK localTurnoPK = (TurnoPK)paramObject;
    localTurnoPK.idTurno = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TurnoPK))
      throw new IllegalArgumentException("arg2");
    TurnoPK localTurnoPK = (TurnoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localTurnoPK.idTurno);
  }

  private static final String jdoGetcodTurno(Turno paramTurno)
  {
    if (paramTurno.jdoFlags <= 0)
      return paramTurno.codTurno;
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
      return paramTurno.codTurno;
    if (localStateManager.isLoaded(paramTurno, jdoInheritedFieldCount + 0))
      return paramTurno.codTurno;
    return localStateManager.getStringField(paramTurno, jdoInheritedFieldCount + 0, paramTurno.codTurno);
  }

  private static final void jdoSetcodTurno(Turno paramTurno, String paramString)
  {
    if (paramTurno.jdoFlags == 0)
    {
      paramTurno.codTurno = paramString;
      return;
    }
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.codTurno = paramString;
      return;
    }
    localStateManager.setStringField(paramTurno, jdoInheritedFieldCount + 0, paramTurno.codTurno, paramString);
  }

  private static final double jdoGetentradaAm(Turno paramTurno)
  {
    if (paramTurno.jdoFlags <= 0)
      return paramTurno.entradaAm;
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
      return paramTurno.entradaAm;
    if (localStateManager.isLoaded(paramTurno, jdoInheritedFieldCount + 1))
      return paramTurno.entradaAm;
    return localStateManager.getDoubleField(paramTurno, jdoInheritedFieldCount + 1, paramTurno.entradaAm);
  }

  private static final void jdoSetentradaAm(Turno paramTurno, double paramDouble)
  {
    if (paramTurno.jdoFlags == 0)
    {
      paramTurno.entradaAm = paramDouble;
      return;
    }
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.entradaAm = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTurno, jdoInheritedFieldCount + 1, paramTurno.entradaAm, paramDouble);
  }

  private static final double jdoGetentradaPm(Turno paramTurno)
  {
    if (paramTurno.jdoFlags <= 0)
      return paramTurno.entradaPm;
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
      return paramTurno.entradaPm;
    if (localStateManager.isLoaded(paramTurno, jdoInheritedFieldCount + 2))
      return paramTurno.entradaPm;
    return localStateManager.getDoubleField(paramTurno, jdoInheritedFieldCount + 2, paramTurno.entradaPm);
  }

  private static final void jdoSetentradaPm(Turno paramTurno, double paramDouble)
  {
    if (paramTurno.jdoFlags == 0)
    {
      paramTurno.entradaPm = paramDouble;
      return;
    }
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.entradaPm = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTurno, jdoInheritedFieldCount + 2, paramTurno.entradaPm, paramDouble);
  }

  private static final long jdoGetidTurno(Turno paramTurno)
  {
    return paramTurno.idTurno;
  }

  private static final void jdoSetidTurno(Turno paramTurno, long paramLong)
  {
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.idTurno = paramLong;
      return;
    }
    localStateManager.setLongField(paramTurno, jdoInheritedFieldCount + 3, paramTurno.idTurno, paramLong);
  }

  private static final String jdoGetjornada(Turno paramTurno)
  {
    if (paramTurno.jdoFlags <= 0)
      return paramTurno.jornada;
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
      return paramTurno.jornada;
    if (localStateManager.isLoaded(paramTurno, jdoInheritedFieldCount + 4))
      return paramTurno.jornada;
    return localStateManager.getStringField(paramTurno, jdoInheritedFieldCount + 4, paramTurno.jornada);
  }

  private static final void jdoSetjornada(Turno paramTurno, String paramString)
  {
    if (paramTurno.jdoFlags == 0)
    {
      paramTurno.jornada = paramString;
      return;
    }
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.jornada = paramString;
      return;
    }
    localStateManager.setStringField(paramTurno, jdoInheritedFieldCount + 4, paramTurno.jornada, paramString);
  }

  private static final double jdoGetjornadaDiaria(Turno paramTurno)
  {
    if (paramTurno.jdoFlags <= 0)
      return paramTurno.jornadaDiaria;
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
      return paramTurno.jornadaDiaria;
    if (localStateManager.isLoaded(paramTurno, jdoInheritedFieldCount + 5))
      return paramTurno.jornadaDiaria;
    return localStateManager.getDoubleField(paramTurno, jdoInheritedFieldCount + 5, paramTurno.jornadaDiaria);
  }

  private static final void jdoSetjornadaDiaria(Turno paramTurno, double paramDouble)
  {
    if (paramTurno.jdoFlags == 0)
    {
      paramTurno.jornadaDiaria = paramDouble;
      return;
    }
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.jornadaDiaria = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTurno, jdoInheritedFieldCount + 5, paramTurno.jornadaDiaria, paramDouble);
  }

  private static final double jdoGetjornadaSemanal(Turno paramTurno)
  {
    if (paramTurno.jdoFlags <= 0)
      return paramTurno.jornadaSemanal;
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
      return paramTurno.jornadaSemanal;
    if (localStateManager.isLoaded(paramTurno, jdoInheritedFieldCount + 6))
      return paramTurno.jornadaSemanal;
    return localStateManager.getDoubleField(paramTurno, jdoInheritedFieldCount + 6, paramTurno.jornadaSemanal);
  }

  private static final void jdoSetjornadaSemanal(Turno paramTurno, double paramDouble)
  {
    if (paramTurno.jdoFlags == 0)
    {
      paramTurno.jornadaSemanal = paramDouble;
      return;
    }
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.jornadaSemanal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTurno, jdoInheritedFieldCount + 6, paramTurno.jornadaSemanal, paramDouble);
  }

  private static final String jdoGetnombre(Turno paramTurno)
  {
    if (paramTurno.jdoFlags <= 0)
      return paramTurno.nombre;
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
      return paramTurno.nombre;
    if (localStateManager.isLoaded(paramTurno, jdoInheritedFieldCount + 7))
      return paramTurno.nombre;
    return localStateManager.getStringField(paramTurno, jdoInheritedFieldCount + 7, paramTurno.nombre);
  }

  private static final void jdoSetnombre(Turno paramTurno, String paramString)
  {
    if (paramTurno.jdoFlags == 0)
    {
      paramTurno.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTurno, jdoInheritedFieldCount + 7, paramTurno.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(Turno paramTurno)
  {
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
      return paramTurno.organismo;
    if (localStateManager.isLoaded(paramTurno, jdoInheritedFieldCount + 8))
      return paramTurno.organismo;
    return (Organismo)localStateManager.getObjectField(paramTurno, jdoInheritedFieldCount + 8, paramTurno.organismo);
  }

  private static final void jdoSetorganismo(Turno paramTurno, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramTurno, jdoInheritedFieldCount + 8, paramTurno.organismo, paramOrganismo);
  }

  private static final double jdoGetsalidaAm(Turno paramTurno)
  {
    if (paramTurno.jdoFlags <= 0)
      return paramTurno.salidaAm;
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
      return paramTurno.salidaAm;
    if (localStateManager.isLoaded(paramTurno, jdoInheritedFieldCount + 9))
      return paramTurno.salidaAm;
    return localStateManager.getDoubleField(paramTurno, jdoInheritedFieldCount + 9, paramTurno.salidaAm);
  }

  private static final void jdoSetsalidaAm(Turno paramTurno, double paramDouble)
  {
    if (paramTurno.jdoFlags == 0)
    {
      paramTurno.salidaAm = paramDouble;
      return;
    }
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.salidaAm = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTurno, jdoInheritedFieldCount + 9, paramTurno.salidaAm, paramDouble);
  }

  private static final double jdoGetsalidaPm(Turno paramTurno)
  {
    if (paramTurno.jdoFlags <= 0)
      return paramTurno.salidaPm;
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
      return paramTurno.salidaPm;
    if (localStateManager.isLoaded(paramTurno, jdoInheritedFieldCount + 10))
      return paramTurno.salidaPm;
    return localStateManager.getDoubleField(paramTurno, jdoInheritedFieldCount + 10, paramTurno.salidaPm);
  }

  private static final void jdoSetsalidaPm(Turno paramTurno, double paramDouble)
  {
    if (paramTurno.jdoFlags == 0)
    {
      paramTurno.salidaPm = paramDouble;
      return;
    }
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.salidaPm = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTurno, jdoInheritedFieldCount + 10, paramTurno.salidaPm, paramDouble);
  }

  private static final String jdoGettipoTurno(Turno paramTurno)
  {
    if (paramTurno.jdoFlags <= 0)
      return paramTurno.tipoTurno;
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
      return paramTurno.tipoTurno;
    if (localStateManager.isLoaded(paramTurno, jdoInheritedFieldCount + 11))
      return paramTurno.tipoTurno;
    return localStateManager.getStringField(paramTurno, jdoInheritedFieldCount + 11, paramTurno.tipoTurno);
  }

  private static final void jdoSettipoTurno(Turno paramTurno, String paramString)
  {
    if (paramTurno.jdoFlags == 0)
    {
      paramTurno.tipoTurno = paramString;
      return;
    }
    StateManager localStateManager = paramTurno.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurno.tipoTurno = paramString;
      return;
    }
    localStateManager.setStringField(paramTurno, jdoInheritedFieldCount + 11, paramTurno.tipoTurno, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}