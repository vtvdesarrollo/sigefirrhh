package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class FirmasReportes
  implements Serializable, PersistenceCapable
{
  private long idFirmasReportes;
  private String nombre;
  private String cargo;
  private String nombramiento;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargo", "idFirmasReportes", "nombramiento", "nombre", "organismo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this);
  }

  public String getCargo()
  {
    return jdoGetcargo(this);
  }

  public long getIdFirmasReportes()
  {
    return jdoGetidFirmasReportes(this);
  }

  public String getNombramiento()
  {
    return jdoGetnombramiento(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setCargo(String string)
  {
    jdoSetcargo(this, string);
  }

  public void setIdFirmasReportes(long l)
  {
    jdoSetidFirmasReportes(this, l);
  }

  public void setNombramiento(String string)
  {
    jdoSetnombramiento(this, string);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.FirmasReportes"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new FirmasReportes());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    FirmasReportes localFirmasReportes = new FirmasReportes();
    localFirmasReportes.jdoFlags = 1;
    localFirmasReportes.jdoStateManager = paramStateManager;
    return localFirmasReportes;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    FirmasReportes localFirmasReportes = new FirmasReportes();
    localFirmasReportes.jdoCopyKeyFieldsFromObjectId(paramObject);
    localFirmasReportes.jdoFlags = 1;
    localFirmasReportes.jdoStateManager = paramStateManager;
    return localFirmasReportes;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idFirmasReportes);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombramiento);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idFirmasReportes = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombramiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(FirmasReportes paramFirmasReportes, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramFirmasReportes == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramFirmasReportes.cargo;
      return;
    case 1:
      if (paramFirmasReportes == null)
        throw new IllegalArgumentException("arg1");
      this.idFirmasReportes = paramFirmasReportes.idFirmasReportes;
      return;
    case 2:
      if (paramFirmasReportes == null)
        throw new IllegalArgumentException("arg1");
      this.nombramiento = paramFirmasReportes.nombramiento;
      return;
    case 3:
      if (paramFirmasReportes == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramFirmasReportes.nombre;
      return;
    case 4:
      if (paramFirmasReportes == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramFirmasReportes.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof FirmasReportes))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    FirmasReportes localFirmasReportes = (FirmasReportes)paramObject;
    if (localFirmasReportes.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localFirmasReportes, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new FirmasReportesPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new FirmasReportesPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FirmasReportesPK))
      throw new IllegalArgumentException("arg1");
    FirmasReportesPK localFirmasReportesPK = (FirmasReportesPK)paramObject;
    localFirmasReportesPK.idFirmasReportes = this.idFirmasReportes;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FirmasReportesPK))
      throw new IllegalArgumentException("arg1");
    FirmasReportesPK localFirmasReportesPK = (FirmasReportesPK)paramObject;
    this.idFirmasReportes = localFirmasReportesPK.idFirmasReportes;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FirmasReportesPK))
      throw new IllegalArgumentException("arg2");
    FirmasReportesPK localFirmasReportesPK = (FirmasReportesPK)paramObject;
    localFirmasReportesPK.idFirmasReportes = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FirmasReportesPK))
      throw new IllegalArgumentException("arg2");
    FirmasReportesPK localFirmasReportesPK = (FirmasReportesPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localFirmasReportesPK.idFirmasReportes);
  }

  private static final String jdoGetcargo(FirmasReportes paramFirmasReportes)
  {
    if (paramFirmasReportes.jdoFlags <= 0)
      return paramFirmasReportes.cargo;
    StateManager localStateManager = paramFirmasReportes.jdoStateManager;
    if (localStateManager == null)
      return paramFirmasReportes.cargo;
    if (localStateManager.isLoaded(paramFirmasReportes, jdoInheritedFieldCount + 0))
      return paramFirmasReportes.cargo;
    return localStateManager.getStringField(paramFirmasReportes, jdoInheritedFieldCount + 0, paramFirmasReportes.cargo);
  }

  private static final void jdoSetcargo(FirmasReportes paramFirmasReportes, String paramString)
  {
    if (paramFirmasReportes.jdoFlags == 0)
    {
      paramFirmasReportes.cargo = paramString;
      return;
    }
    StateManager localStateManager = paramFirmasReportes.jdoStateManager;
    if (localStateManager == null)
    {
      paramFirmasReportes.cargo = paramString;
      return;
    }
    localStateManager.setStringField(paramFirmasReportes, jdoInheritedFieldCount + 0, paramFirmasReportes.cargo, paramString);
  }

  private static final long jdoGetidFirmasReportes(FirmasReportes paramFirmasReportes)
  {
    return paramFirmasReportes.idFirmasReportes;
  }

  private static final void jdoSetidFirmasReportes(FirmasReportes paramFirmasReportes, long paramLong)
  {
    StateManager localStateManager = paramFirmasReportes.jdoStateManager;
    if (localStateManager == null)
    {
      paramFirmasReportes.idFirmasReportes = paramLong;
      return;
    }
    localStateManager.setLongField(paramFirmasReportes, jdoInheritedFieldCount + 1, paramFirmasReportes.idFirmasReportes, paramLong);
  }

  private static final String jdoGetnombramiento(FirmasReportes paramFirmasReportes)
  {
    if (paramFirmasReportes.jdoFlags <= 0)
      return paramFirmasReportes.nombramiento;
    StateManager localStateManager = paramFirmasReportes.jdoStateManager;
    if (localStateManager == null)
      return paramFirmasReportes.nombramiento;
    if (localStateManager.isLoaded(paramFirmasReportes, jdoInheritedFieldCount + 2))
      return paramFirmasReportes.nombramiento;
    return localStateManager.getStringField(paramFirmasReportes, jdoInheritedFieldCount + 2, paramFirmasReportes.nombramiento);
  }

  private static final void jdoSetnombramiento(FirmasReportes paramFirmasReportes, String paramString)
  {
    if (paramFirmasReportes.jdoFlags == 0)
    {
      paramFirmasReportes.nombramiento = paramString;
      return;
    }
    StateManager localStateManager = paramFirmasReportes.jdoStateManager;
    if (localStateManager == null)
    {
      paramFirmasReportes.nombramiento = paramString;
      return;
    }
    localStateManager.setStringField(paramFirmasReportes, jdoInheritedFieldCount + 2, paramFirmasReportes.nombramiento, paramString);
  }

  private static final String jdoGetnombre(FirmasReportes paramFirmasReportes)
  {
    if (paramFirmasReportes.jdoFlags <= 0)
      return paramFirmasReportes.nombre;
    StateManager localStateManager = paramFirmasReportes.jdoStateManager;
    if (localStateManager == null)
      return paramFirmasReportes.nombre;
    if (localStateManager.isLoaded(paramFirmasReportes, jdoInheritedFieldCount + 3))
      return paramFirmasReportes.nombre;
    return localStateManager.getStringField(paramFirmasReportes, jdoInheritedFieldCount + 3, paramFirmasReportes.nombre);
  }

  private static final void jdoSetnombre(FirmasReportes paramFirmasReportes, String paramString)
  {
    if (paramFirmasReportes.jdoFlags == 0)
    {
      paramFirmasReportes.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramFirmasReportes.jdoStateManager;
    if (localStateManager == null)
    {
      paramFirmasReportes.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramFirmasReportes, jdoInheritedFieldCount + 3, paramFirmasReportes.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(FirmasReportes paramFirmasReportes)
  {
    StateManager localStateManager = paramFirmasReportes.jdoStateManager;
    if (localStateManager == null)
      return paramFirmasReportes.organismo;
    if (localStateManager.isLoaded(paramFirmasReportes, jdoInheritedFieldCount + 4))
      return paramFirmasReportes.organismo;
    return (Organismo)localStateManager.getObjectField(paramFirmasReportes, jdoInheritedFieldCount + 4, paramFirmasReportes.organismo);
  }

  private static final void jdoSetorganismo(FirmasReportes paramFirmasReportes, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramFirmasReportes.jdoStateManager;
    if (localStateManager == null)
    {
      paramFirmasReportes.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramFirmasReportes, jdoInheritedFieldCount + 4, paramFirmasReportes.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}