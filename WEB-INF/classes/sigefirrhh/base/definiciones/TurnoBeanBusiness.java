package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class TurnoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTurno(Turno turno)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Turno turnoNew = 
      (Turno)BeanUtils.cloneBean(
      turno);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (turnoNew.getOrganismo() != null) {
      turnoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        turnoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(turnoNew);
  }

  public void updateTurno(Turno turno) throws Exception
  {
    Turno turnoModify = 
      findTurnoById(turno.getIdTurno());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (turno.getOrganismo() != null) {
      turno.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        turno.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(turnoModify, turno);
  }

  public void deleteTurno(Turno turno) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Turno turnoDelete = 
      findTurnoById(turno.getIdTurno());
    pm.deletePersistent(turnoDelete);
  }

  public Turno findTurnoById(long idTurno) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTurno == pIdTurno";
    Query query = pm.newQuery(Turno.class, filter);

    query.declareParameters("long pIdTurno");

    parameters.put("pIdTurno", new Long(idTurno));

    Collection colTurno = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTurno.iterator();
    return (Turno)iterator.next();
  }

  public Collection findTurnoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent turnoExtent = pm.getExtent(
      Turno.class, true);
    Query query = pm.newQuery(turnoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTurno(String codTurno, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTurno == pCodTurno &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Turno.class, filter);

    query.declareParameters("java.lang.String pCodTurno, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodTurno", new String(codTurno));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colTurno = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTurno);

    return colTurno;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Turno.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colTurno = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTurno);

    return colTurno;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Turno.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colTurno = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTurno);

    return colTurno;
  }
}