package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ParametroGobiernoPK
  implements Serializable
{
  public long idParametroGobierno;

  public ParametroGobiernoPK()
  {
  }

  public ParametroGobiernoPK(long idParametroGobierno)
  {
    this.idParametroGobierno = idParametroGobierno;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroGobiernoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroGobiernoPK thatPK)
  {
    return 
      this.idParametroGobierno == thatPK.idParametroGobierno;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParametroGobierno)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParametroGobierno);
  }
}