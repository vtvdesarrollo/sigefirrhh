package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.CaracteristicaDependencia;
import sigefirrhh.base.estructura.CaracteristicaDependenciaBeanBusiness;

public class ConceptoDependenciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoDependencia(ConceptoDependencia conceptoDependencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoDependencia conceptoDependenciaNew = 
      (ConceptoDependencia)BeanUtils.cloneBean(
      conceptoDependencia);

    CaracteristicaDependenciaBeanBusiness caracteristicaDependenciaBeanBusiness = new CaracteristicaDependenciaBeanBusiness();

    if (conceptoDependenciaNew.getCaracteristicaDependencia() != null) {
      conceptoDependenciaNew.setCaracteristicaDependencia(
        caracteristicaDependenciaBeanBusiness.findCaracteristicaDependenciaById(
        conceptoDependenciaNew.getCaracteristicaDependencia().getIdCaracteristicaDependencia()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoDependenciaNew.getConceptoTipoPersonal() != null) {
      conceptoDependenciaNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoDependenciaNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }
    pm.makePersistent(conceptoDependenciaNew);
  }

  public void updateConceptoDependencia(ConceptoDependencia conceptoDependencia) throws Exception
  {
    ConceptoDependencia conceptoDependenciaModify = 
      findConceptoDependenciaById(conceptoDependencia.getIdConceptoDependencia());

    CaracteristicaDependenciaBeanBusiness caracteristicaDependenciaBeanBusiness = new CaracteristicaDependenciaBeanBusiness();

    if (conceptoDependencia.getCaracteristicaDependencia() != null) {
      conceptoDependencia.setCaracteristicaDependencia(
        caracteristicaDependenciaBeanBusiness.findCaracteristicaDependenciaById(
        conceptoDependencia.getCaracteristicaDependencia().getIdCaracteristicaDependencia()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoDependencia.getConceptoTipoPersonal() != null) {
      conceptoDependencia.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoDependencia.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    BeanUtils.copyProperties(conceptoDependenciaModify, conceptoDependencia);
  }

  public void deleteConceptoDependencia(ConceptoDependencia conceptoDependencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoDependencia conceptoDependenciaDelete = 
      findConceptoDependenciaById(conceptoDependencia.getIdConceptoDependencia());
    pm.deletePersistent(conceptoDependenciaDelete);
  }

  public ConceptoDependencia findConceptoDependenciaById(long idConceptoDependencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoDependencia == pIdConceptoDependencia";
    Query query = pm.newQuery(ConceptoDependencia.class, filter);

    query.declareParameters("long pIdConceptoDependencia");

    parameters.put("pIdConceptoDependencia", new Long(idConceptoDependencia));

    Collection colConceptoDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoDependencia.iterator();
    return (ConceptoDependencia)iterator.next();
  }

  public Collection findConceptoDependenciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoDependenciaExtent = pm.getExtent(
      ConceptoDependencia.class, true);
    Query query = pm.newQuery(conceptoDependenciaExtent);
    query.setOrdering("conceptoTipoPersonal.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCaracteristicaDependencia(long idCaracteristicaDependencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "caracteristicaDependencia.idCaracteristicaDependencia == pIdCaracteristicaDependencia";

    Query query = pm.newQuery(ConceptoDependencia.class, filter);

    query.declareParameters("long pIdCaracteristicaDependencia");
    HashMap parameters = new HashMap();

    parameters.put("pIdCaracteristicaDependencia", new Long(idCaracteristicaDependencia));

    query.setOrdering("conceptoTipoPersonal.codConcepto ascending");

    Collection colConceptoDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoDependencia);

    return colConceptoDependencia;
  }
}