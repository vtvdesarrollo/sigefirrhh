package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ConceptoVacacionesForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoVacacionesForm.class.getName());
  private ConceptoVacaciones conceptoVacaciones;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private boolean showConceptoVacacionesByTipoPersonal;
  private String findSelectTipoPersonal;
  private Collection findColTipoPersonal;
  private Collection colTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private Collection colConceptoAlicuota;
  private String selectTipoPersonal;
  private String selectConceptoTipoPersonal;
  private String selectConceptoAlicuota;
  private Object stateResultConceptoVacacionesByTipoPersonal = null;

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.colConceptoTipoPersonal = null;
    this.colConceptoAlicuota = null;
    try {
      if (idTipoPersonal > 0L)
      {
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
        this.colConceptoAlicuota = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonalAndTipoPrestamo(
          idTipoPersonal, "L");
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
    if (this.conceptoVacaciones != null) {
      parameters.put("id_tipo_personal", new Long(this.conceptoVacaciones.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal()));
    }
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("ConceptoVacaciones");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/base/definiciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "ConceptoVacaciones" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public String getFindSelectTipoPersonal() {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.conceptoVacaciones.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.conceptoVacaciones.setTipoPersonal(
          tipoPersonal);
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    this.conceptoVacaciones.setConceptoTipoPersonal(null);
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()).equals(
        valConceptoTipoPersonal)) {
        this.conceptoVacaciones.setConceptoTipoPersonal(
          conceptoTipoPersonal);
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public String getSelectConceptoAlicuota() {
    return this.selectConceptoAlicuota;
  }
  public void setSelectConceptoAlicuota(String valConceptoAlicuota) {
    Iterator iterator = this.colConceptoAlicuota.iterator();
    ConceptoTipoPersonal conceptoAlicuota = null;
    this.conceptoVacaciones.setConceptoAlicuota(null);
    while (iterator.hasNext()) {
      conceptoAlicuota = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoAlicuota.getIdConceptoTipoPersonal()).equals(
        valConceptoAlicuota)) {
        this.conceptoVacaciones.setConceptoAlicuota(
          conceptoAlicuota);
      }
    }
    this.selectConceptoAlicuota = valConceptoAlicuota;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoVacaciones getConceptoVacaciones() {
    if (this.conceptoVacaciones == null) {
      this.conceptoVacaciones = new ConceptoVacaciones();
    }
    return this.conceptoVacaciones;
  }

  public ConceptoVacacionesForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    newReportId();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getListTipo() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoVacaciones.LISTA_TIPO_PAGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListMesCerrado() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoVacaciones.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListMes30() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoVacaciones.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAlicuotaVacacional() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoVacaciones.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColConceptoAlicuota()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoAlicuota.iterator();
    ConceptoTipoPersonal conceptoAlicuota = null;
    while (iterator.hasNext()) {
      conceptoAlicuota = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoAlicuota.getIdConceptoTipoPersonal()), 
        conceptoAlicuota.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colConceptoAlicuota = new ArrayList();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConceptoVacacionesByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findConceptoVacacionesByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showConceptoVacacionesByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoVacacionesByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;

    return null;
  }

  public boolean isShowConceptoVacacionesByTipoPersonal() {
    return this.showConceptoVacacionesByTipoPersonal;
  }

  public String selectConceptoVacaciones()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectConceptoTipoPersonal = null;
    this.selectConceptoAlicuota = null;

    long idConceptoVacaciones = 
      Long.parseLong((String)requestParameterMap.get("idConceptoVacaciones"));
    try
    {
      this.conceptoVacaciones = 
        this.definicionesFacade.findConceptoVacacionesById(
        idConceptoVacaciones);
      if (this.conceptoVacaciones.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.conceptoVacaciones.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.conceptoVacaciones.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.conceptoVacaciones.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          this.conceptoVacaciones.getTipoPersonal().getIdTipoPersonal());
      }

      if (this.conceptoVacaciones.getConceptoAlicuota() != null) {
        this.selectConceptoAlicuota = 
          String.valueOf(this.conceptoVacaciones.getConceptoAlicuota().getIdConceptoTipoPersonal());

        this.colConceptoAlicuota = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonalAndTipoPrestamo(
          this.conceptoVacaciones.getConceptoAlicuota().getIdConceptoTipoPersonal(), "L");
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.conceptoVacaciones = null;
    this.showConceptoVacacionesByTipoPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.conceptoVacaciones.getTiempoSitp() != null) && 
      (this.conceptoVacaciones.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addConceptoVacaciones(
          this.conceptoVacaciones);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.conceptoVacaciones);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateConceptoVacaciones(
          this.conceptoVacaciones);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.conceptoVacaciones);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteConceptoVacaciones(
        this.conceptoVacaciones);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.conceptoVacaciones);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.conceptoVacaciones = new ConceptoVacaciones();

    this.selectConceptoTipoPersonal = null;

    this.selectConceptoAlicuota = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoVacaciones.setIdConceptoVacaciones(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.ConceptoVacaciones"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.conceptoVacaciones = new ConceptoVacaciones();
    return "cancel";
  }

  public boolean isShowMesCerradoAux()
  {
    try
    {
      return this.conceptoVacaciones.getTipo().equals("P"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowMes30Aux()
  {
    try {
      return this.conceptoVacaciones.getTipo().equals("P"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowNumeroMesesAux()
  {
    try {
      return this.conceptoVacaciones.getTipo().equals("P"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowConceptoAlicuotaAux()
  {
    try
    {
      return this.conceptoVacaciones.getAlicuotaVacacional().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public boolean isShowConcepto()
  {
    return (this.colConceptoTipoPersonal != null) && 
      (!this.colConceptoTipoPersonal.isEmpty());
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
}