package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;

public class CuentaBancoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCuentaBanco(CuentaBanco cuentaBanco)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CuentaBanco cuentaBancoNew = 
      (CuentaBanco)BeanUtils.cloneBean(
      cuentaBanco);

    BancoBeanBusiness bancoBeanBusiness = new BancoBeanBusiness();

    if (cuentaBancoNew.getBanco() != null) {
      cuentaBancoNew.setBanco(
        bancoBeanBusiness.findBancoById(
        cuentaBancoNew.getBanco().getIdBanco()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (cuentaBancoNew.getCiudad() != null) {
      cuentaBancoNew.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        cuentaBancoNew.getCiudad().getIdCiudad()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (cuentaBancoNew.getOrganismo() != null) {
      cuentaBancoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        cuentaBancoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(cuentaBancoNew);
  }

  public void updateCuentaBanco(CuentaBanco cuentaBanco) throws Exception
  {
    CuentaBanco cuentaBancoModify = 
      findCuentaBancoById(cuentaBanco.getIdCuentaBanco());

    BancoBeanBusiness bancoBeanBusiness = new BancoBeanBusiness();

    if (cuentaBanco.getBanco() != null) {
      cuentaBanco.setBanco(
        bancoBeanBusiness.findBancoById(
        cuentaBanco.getBanco().getIdBanco()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (cuentaBanco.getCiudad() != null) {
      cuentaBanco.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        cuentaBanco.getCiudad().getIdCiudad()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (cuentaBanco.getOrganismo() != null) {
      cuentaBanco.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        cuentaBanco.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(cuentaBancoModify, cuentaBanco);
  }

  public void deleteCuentaBanco(CuentaBanco cuentaBanco) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CuentaBanco cuentaBancoDelete = 
      findCuentaBancoById(cuentaBanco.getIdCuentaBanco());
    pm.deletePersistent(cuentaBancoDelete);
  }

  public CuentaBanco findCuentaBancoById(long idCuentaBanco) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCuentaBanco == pIdCuentaBanco";
    Query query = pm.newQuery(CuentaBanco.class, filter);

    query.declareParameters("long pIdCuentaBanco");

    parameters.put("pIdCuentaBanco", new Long(idCuentaBanco));

    Collection colCuentaBanco = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCuentaBanco.iterator();
    return (CuentaBanco)iterator.next();
  }

  public Collection findCuentaBancoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent cuentaBancoExtent = pm.getExtent(
      CuentaBanco.class, true);
    Query query = pm.newQuery(cuentaBancoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByBanco(long idBanco, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "banco.idBanco == pIdBanco &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(CuentaBanco.class, filter);

    query.declareParameters("long pIdBanco, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdBanco", new Long(idBanco));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colCuentaBanco = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCuentaBanco);

    return colCuentaBanco;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(CuentaBanco.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colCuentaBanco = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCuentaBanco);

    return colCuentaBanco;
  }
}