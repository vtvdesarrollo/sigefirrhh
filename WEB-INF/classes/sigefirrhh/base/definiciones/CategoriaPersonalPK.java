package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class CategoriaPersonalPK
  implements Serializable
{
  public long idCategoriaPersonal;

  public CategoriaPersonalPK()
  {
  }

  public CategoriaPersonalPK(long idCategoriaPersonal)
  {
    this.idCategoriaPersonal = idCategoriaPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CategoriaPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CategoriaPersonalPK thatPK)
  {
    return 
      this.idCategoriaPersonal == thatPK.idCategoriaPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCategoriaPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCategoriaPersonal);
  }
}