package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class CategoriaPersonal
  implements Serializable, PersistenceCapable
{
  private long idCategoriaPersonal;
  private String codCategoria;
  private String descCategoria;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codCategoria", "descCategoria", "idCategoriaPersonal" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescCategoria(this);
  }

  public String getCodCategoria()
  {
    return jdoGetcodCategoria(this);
  }

  public String getDescCategoria()
  {
    return jdoGetdescCategoria(this);
  }

  public long getIdCategoriaPersonal()
  {
    return jdoGetidCategoriaPersonal(this);
  }

  public void setCodCategoria(String string)
  {
    jdoSetcodCategoria(this, string);
  }

  public void setDescCategoria(String string)
  {
    jdoSetdescCategoria(this, string);
  }

  public void setIdCategoriaPersonal(long l)
  {
    jdoSetidCategoriaPersonal(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.CategoriaPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CategoriaPersonal());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CategoriaPersonal localCategoriaPersonal = new CategoriaPersonal();
    localCategoriaPersonal.jdoFlags = 1;
    localCategoriaPersonal.jdoStateManager = paramStateManager;
    return localCategoriaPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CategoriaPersonal localCategoriaPersonal = new CategoriaPersonal();
    localCategoriaPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCategoriaPersonal.jdoFlags = 1;
    localCategoriaPersonal.jdoStateManager = paramStateManager;
    return localCategoriaPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCategoria);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descCategoria);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCategoriaPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCategoria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descCategoria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCategoriaPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CategoriaPersonal paramCategoriaPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCategoriaPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.codCategoria = paramCategoriaPersonal.codCategoria;
      return;
    case 1:
      if (paramCategoriaPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.descCategoria = paramCategoriaPersonal.descCategoria;
      return;
    case 2:
      if (paramCategoriaPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idCategoriaPersonal = paramCategoriaPersonal.idCategoriaPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CategoriaPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CategoriaPersonal localCategoriaPersonal = (CategoriaPersonal)paramObject;
    if (localCategoriaPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCategoriaPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CategoriaPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CategoriaPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CategoriaPersonalPK))
      throw new IllegalArgumentException("arg1");
    CategoriaPersonalPK localCategoriaPersonalPK = (CategoriaPersonalPK)paramObject;
    localCategoriaPersonalPK.idCategoriaPersonal = this.idCategoriaPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CategoriaPersonalPK))
      throw new IllegalArgumentException("arg1");
    CategoriaPersonalPK localCategoriaPersonalPK = (CategoriaPersonalPK)paramObject;
    this.idCategoriaPersonal = localCategoriaPersonalPK.idCategoriaPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CategoriaPersonalPK))
      throw new IllegalArgumentException("arg2");
    CategoriaPersonalPK localCategoriaPersonalPK = (CategoriaPersonalPK)paramObject;
    localCategoriaPersonalPK.idCategoriaPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CategoriaPersonalPK))
      throw new IllegalArgumentException("arg2");
    CategoriaPersonalPK localCategoriaPersonalPK = (CategoriaPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localCategoriaPersonalPK.idCategoriaPersonal);
  }

  private static final String jdoGetcodCategoria(CategoriaPersonal paramCategoriaPersonal)
  {
    if (paramCategoriaPersonal.jdoFlags <= 0)
      return paramCategoriaPersonal.codCategoria;
    StateManager localStateManager = paramCategoriaPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramCategoriaPersonal.codCategoria;
    if (localStateManager.isLoaded(paramCategoriaPersonal, jdoInheritedFieldCount + 0))
      return paramCategoriaPersonal.codCategoria;
    return localStateManager.getStringField(paramCategoriaPersonal, jdoInheritedFieldCount + 0, paramCategoriaPersonal.codCategoria);
  }

  private static final void jdoSetcodCategoria(CategoriaPersonal paramCategoriaPersonal, String paramString)
  {
    if (paramCategoriaPersonal.jdoFlags == 0)
    {
      paramCategoriaPersonal.codCategoria = paramString;
      return;
    }
    StateManager localStateManager = paramCategoriaPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramCategoriaPersonal.codCategoria = paramString;
      return;
    }
    localStateManager.setStringField(paramCategoriaPersonal, jdoInheritedFieldCount + 0, paramCategoriaPersonal.codCategoria, paramString);
  }

  private static final String jdoGetdescCategoria(CategoriaPersonal paramCategoriaPersonal)
  {
    if (paramCategoriaPersonal.jdoFlags <= 0)
      return paramCategoriaPersonal.descCategoria;
    StateManager localStateManager = paramCategoriaPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramCategoriaPersonal.descCategoria;
    if (localStateManager.isLoaded(paramCategoriaPersonal, jdoInheritedFieldCount + 1))
      return paramCategoriaPersonal.descCategoria;
    return localStateManager.getStringField(paramCategoriaPersonal, jdoInheritedFieldCount + 1, paramCategoriaPersonal.descCategoria);
  }

  private static final void jdoSetdescCategoria(CategoriaPersonal paramCategoriaPersonal, String paramString)
  {
    if (paramCategoriaPersonal.jdoFlags == 0)
    {
      paramCategoriaPersonal.descCategoria = paramString;
      return;
    }
    StateManager localStateManager = paramCategoriaPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramCategoriaPersonal.descCategoria = paramString;
      return;
    }
    localStateManager.setStringField(paramCategoriaPersonal, jdoInheritedFieldCount + 1, paramCategoriaPersonal.descCategoria, paramString);
  }

  private static final long jdoGetidCategoriaPersonal(CategoriaPersonal paramCategoriaPersonal)
  {
    return paramCategoriaPersonal.idCategoriaPersonal;
  }

  private static final void jdoSetidCategoriaPersonal(CategoriaPersonal paramCategoriaPersonal, long paramLong)
  {
    StateManager localStateManager = paramCategoriaPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramCategoriaPersonal.idCategoriaPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramCategoriaPersonal, jdoInheritedFieldCount + 2, paramCategoriaPersonal.idCategoriaPersonal, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}