package sigefirrhh.base.definiciones;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ConceptoCargoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoCargoForm.class.getName());
  private ConceptoCargo conceptoCargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private CargoFacade cargoFacade = new CargoFacade();
  private boolean showConceptoCargoByConceptoTipoPersonal;
  private boolean showConceptoCargoByCargo;
  private String findSelectTipoPersonalForConceptoTipoPersonal;
  private String findSelectConceptoTipoPersonal;
  private String findSelectManualCargoForCargo;
  private String findSelectCargo;
  private Collection findColTipoPersonalForConceptoTipoPersonal;
  private Collection findColConceptoTipoPersonal;
  private Collection findColManualCargoForCargo;
  private Collection findColCargo;
  private Collection colTipoPersonalForConceptoTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private Collection colManualCargoForCargo;
  private Collection colCargo;
  private String selectTipoPersonalForConceptoTipoPersonal;
  private String selectConceptoTipoPersonal;
  private String selectManualCargoForCargo;
  private String selectCargo;
  private Object stateResultConceptoCargoByConceptoTipoPersonal = null;

  private Object stateResultConceptoCargoByCargo = null;

  public Collection getFindColTipoPersonalForConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectTipoPersonalForConceptoTipoPersonal() {
    return this.findSelectTipoPersonalForConceptoTipoPersonal;
  }
  public void setFindSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.findSelectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }
  public void findChangeTipoPersonalForConceptoTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColConceptoTipoPersonal = null;
      if (idTipoPersonal > 0L)
        this.findColConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowTipoPersonalForConceptoTipoPersonal() { return this.findColTipoPersonalForConceptoTipoPersonal != null; }

  public String getFindSelectConceptoTipoPersonal() {
    return this.findSelectConceptoTipoPersonal;
  }
  public void setFindSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    this.findSelectConceptoTipoPersonal = valConceptoTipoPersonal;
  }

  public Collection getFindColConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }
  public boolean isFindShowConceptoTipoPersonal() {
    return this.findColConceptoTipoPersonal != null;
  }
  public Collection getFindColManualCargoForCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }
  public String getFindSelectManualCargoForCargo() {
    return this.findSelectManualCargoForCargo;
  }
  public void setFindSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.findSelectManualCargoForCargo = valManualCargoForCargo;
  }
  public void findChangeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColCargo = null;
      if (idManualCargo > 0L)
        this.findColCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowManualCargoForCargo() { return this.findColManualCargoForCargo != null; }

  public String getFindSelectCargo() {
    return this.findSelectCargo;
  }
  public void setFindSelectCargo(String valCargo) {
    this.findSelectCargo = valCargo;
  }

  public Collection getFindColCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }
  public boolean isFindShowCargo() {
    return this.findColCargo != null;
  }

  public String getSelectTipoPersonalForConceptoTipoPersonal()
  {
    return this.selectTipoPersonalForConceptoTipoPersonal;
  }
  public void setSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.selectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }
  public void changeTipoPersonalForConceptoTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colConceptoTipoPersonal = null;
      if (idTipoPersonal > 0L) {
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
      } else {
        this.selectConceptoTipoPersonal = null;
        this.conceptoCargo.setConceptoTipoPersonal(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectConceptoTipoPersonal = null;
      this.conceptoCargo.setConceptoTipoPersonal(
        null);
    }
  }

  public boolean isShowTipoPersonalForConceptoTipoPersonal() { return this.colTipoPersonalForConceptoTipoPersonal != null; }

  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    this.conceptoCargo.setConceptoTipoPersonal(null);
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()).equals(
        valConceptoTipoPersonal)) {
        this.conceptoCargo.setConceptoTipoPersonal(
          conceptoTipoPersonal);
        break;
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public boolean isShowConceptoTipoPersonal() {
    return this.colConceptoTipoPersonal != null;
  }
  public String getSelectManualCargoForCargo() {
    return this.selectManualCargoForCargo;
  }
  public void setSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.selectManualCargoForCargo = valManualCargoForCargo;
  }
  public void changeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;
      if (idManualCargo > 0L) {
        this.colCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
      } else {
        this.selectCargo = null;
        this.conceptoCargo.setCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCargo = null;
      this.conceptoCargo.setCargo(
        null);
    }
  }

  public boolean isShowManualCargoForCargo() { return this.colManualCargoForCargo != null; }

  public String getSelectCargo() {
    return this.selectCargo;
  }
  public void setSelectCargo(String valCargo) {
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    this.conceptoCargo.setCargo(null);
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        valCargo)) {
        this.conceptoCargo.setCargo(
          cargo);
        break;
      }
    }
    this.selectCargo = valCargo;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoCargo getConceptoCargo() {
    if (this.conceptoCargo == null) {
      this.conceptoCargo = new ConceptoCargo();
    }
    return this.conceptoCargo;
  }

  public ConceptoCargoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonalForConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColManualCargoForCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getListExcluir() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoCargo.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAutomaticoIngreso() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoCargo.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColManualCargoForCargo = 
        this.cargoFacade.findManualCargoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colManualCargoForCargo = 
        this.cargoFacade.findManualCargoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConceptoCargoByConceptoTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findConceptoCargoByConceptoTipoPersonal(Long.valueOf(this.findSelectConceptoTipoPersonal).longValue());
      this.showConceptoCargoByConceptoTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoCargoByConceptoTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonalForConceptoTipoPersonal = null;
    this.findSelectConceptoTipoPersonal = null;
    this.findSelectManualCargoForCargo = null;
    this.findSelectCargo = null;

    return null;
  }

  public String findConceptoCargoByCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.definicionesFacade.findConceptoCargoByCargo(Long.valueOf(this.findSelectCargo).longValue());
      this.showConceptoCargoByCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoCargoByCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonalForConceptoTipoPersonal = null;
    this.findSelectConceptoTipoPersonal = null;
    this.findSelectManualCargoForCargo = null;
    this.findSelectCargo = null;

    return null;
  }

  public boolean isShowConceptoCargoByConceptoTipoPersonal() {
    return this.showConceptoCargoByConceptoTipoPersonal;
  }
  public boolean isShowConceptoCargoByCargo() {
    return this.showConceptoCargoByCargo;
  }

  public String selectConceptoCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConceptoTipoPersonal = null;
    this.selectTipoPersonalForConceptoTipoPersonal = null;

    this.selectCargo = null;
    this.selectManualCargoForCargo = null;

    long idConceptoCargo = 
      Long.parseLong((String)requestParameterMap.get("idConceptoCargo"));
    try
    {
      this.conceptoCargo = 
        this.definicionesFacade.findConceptoCargoById(
        idConceptoCargo);
      if (this.conceptoCargo.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.conceptoCargo.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }
      if (this.conceptoCargo.getCargo() != null) {
        this.selectCargo = 
          String.valueOf(this.conceptoCargo.getCargo().getIdCargo());
      }

      ConceptoTipoPersonal conceptoTipoPersonal = null;
      TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
      Cargo cargo = null;
      ManualCargo manualCargoForCargo = null;

      if (this.conceptoCargo.getConceptoTipoPersonal() != null) {
        long idConceptoTipoPersonal = 
          this.conceptoCargo.getConceptoTipoPersonal().getIdConceptoTipoPersonal();
        this.selectConceptoTipoPersonal = String.valueOf(idConceptoTipoPersonal);
        conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(
          idConceptoTipoPersonal);
        this.colConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          conceptoTipoPersonal.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonalForConceptoTipoPersonal = 
          this.conceptoCargo.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal();
        this.selectTipoPersonalForConceptoTipoPersonal = String.valueOf(idTipoPersonalForConceptoTipoPersonal);
        tipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findTipoPersonalById(
          idTipoPersonalForConceptoTipoPersonal);
        this.colTipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findAllTipoPersonal();
      }
      if (this.conceptoCargo.getCargo() != null) {
        long idCargo = 
          this.conceptoCargo.getCargo().getIdCargo();
        this.selectCargo = String.valueOf(idCargo);
        cargo = this.cargoFacade.findCargoById(
          idCargo);
        this.colCargo = this.cargoFacade.findCargoByManualCargo(
          cargo.getManualCargo().getIdManualCargo());

        long idManualCargoForCargo = 
          this.conceptoCargo.getCargo().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargo = String.valueOf(idManualCargoForCargo);
        manualCargoForCargo = 
          this.cargoFacade.findManualCargoById(
          idManualCargoForCargo);
        this.colManualCargoForCargo = 
          this.cargoFacade.findAllManualCargo();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.conceptoCargo = null;
    this.showConceptoCargoByConceptoTipoPersonal = false;
    this.showConceptoCargoByCargo = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.conceptoCargo.getTiempoSitp() != null) && 
      (this.conceptoCargo.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.definicionesFacade.addConceptoCargo(
          this.conceptoCargo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.definicionesFacade.updateConceptoCargo(
          this.conceptoCargo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.definicionesFacade.deleteConceptoCargo(
        this.conceptoCargo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.conceptoCargo = new ConceptoCargo();

    this.selectConceptoTipoPersonal = null;

    this.selectTipoPersonalForConceptoTipoPersonal = null;

    this.selectCargo = null;

    this.selectManualCargoForCargo = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoCargo.setIdConceptoCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.base.definiciones.ConceptoCargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.conceptoCargo = new ConceptoCargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}