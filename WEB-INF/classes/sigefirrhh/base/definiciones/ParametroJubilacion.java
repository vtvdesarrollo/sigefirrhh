package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ParametroJubilacion
  implements Serializable, PersistenceCapable
{
  private long idParametroJubilacion;
  private TipoPersonal tipoPersonal;
  private int edad;
  private int edadm;
  private int aniosServicio;
  private int aniosServiciom;
  private int aniosServicioSinEdad;
  private int aniosServicioEspecial;
  private int mesesPromediar;
  private double factor;
  private double porcentaje;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aniosServicio", "aniosServicioEspecial", "aniosServicioSinEdad", "aniosServiciom", "edad", "edadm", "factor", "idParametroJubilacion", "mesesPromediar", "porcentaje", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Double.TYPE, Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ParametroJubilacion()
  {
    jdoSetedad(this, 55);

    jdoSetedadm(this, 55);

    jdoSetaniosServicio(this, 25);

    jdoSetaniosServiciom(this, 25);

    jdoSetaniosServicioSinEdad(this, 35);

    jdoSetaniosServicioEspecial(this, 15);

    jdoSetmesesPromediar(this, 0);

    jdoSetfactor(this, 0.0D);

    jdoSetporcentaje(this, 0.0D);
  }

  public String toString() {
    return jdoGettipoPersonal(this).getNombre();
  }

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }

  public void setAniosServicio(int aniosServicio)
  {
    jdoSetaniosServicio(this, aniosServicio);
  }

  public int getAniosServicioSinEdad()
  {
    return jdoGetaniosServicioSinEdad(this);
  }

  public void setAniosServicioSinEdad(int aniosServicioSinEdad)
  {
    jdoSetaniosServicioSinEdad(this, aniosServicioSinEdad);
  }

  public int getEdad()
  {
    return jdoGetedad(this);
  }

  public void setEdad(int edad)
  {
    jdoSetedad(this, edad);
  }

  public long getIdParametroJubilacion()
  {
    return jdoGetidParametroJubilacion(this);
  }

  public void setIdParametroJubilacion(long idParametroJubilacion)
  {
    jdoSetidParametroJubilacion(this, idParametroJubilacion);
  }

  public int getMesesPromediar()
  {
    return jdoGetmesesPromediar(this);
  }

  public void setMesesPromediar(int mesesPromediar)
  {
    jdoSetmesesPromediar(this, mesesPromediar);
  }

  public double getPorcentaje()
  {
    return jdoGetporcentaje(this);
  }

  public void setPorcentaje(double porcentaje)
  {
    jdoSetporcentaje(this, porcentaje);
  }

  public double getFactor()
  {
    return jdoGetfactor(this);
  }

  public void setFactor(double factor)
  {
    jdoSetfactor(this, factor);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal)
  {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public int getAniosServicioEspecial()
  {
    return jdoGetaniosServicioEspecial(this);
  }

  public void setAniosServicioEspecial(int aniosServicioEspecial)
  {
    jdoSetaniosServicioEspecial(this, aniosServicioEspecial);
  }

  public int getAniosServiciom()
  {
    return jdoGetaniosServiciom(this);
  }
  public void setAniosServiciom(int aniosServiciom) {
    jdoSetaniosServiciom(this, aniosServiciom);
  }
  public int getEdadm() {
    return jdoGetedadm(this);
  }
  public void setEdadm(int edadm) {
    jdoSetedadm(this, edadm);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ParametroJubilacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroJubilacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroJubilacion localParametroJubilacion = new ParametroJubilacion();
    localParametroJubilacion.jdoFlags = 1;
    localParametroJubilacion.jdoStateManager = paramStateManager;
    return localParametroJubilacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroJubilacion localParametroJubilacion = new ParametroJubilacion();
    localParametroJubilacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroJubilacion.jdoFlags = 1;
    localParametroJubilacion.jdoStateManager = paramStateManager;
    return localParametroJubilacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicioEspecial);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicioSinEdad);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServiciom);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edad);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadm);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.factor);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroJubilacion);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesesPromediar);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicioEspecial = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicioSinEdad = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServiciom = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edad = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadm = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.factor = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroJubilacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesesPromediar = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroJubilacion paramParametroJubilacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramParametroJubilacion.aniosServicio;
      return;
    case 1:
      if (paramParametroJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicioEspecial = paramParametroJubilacion.aniosServicioEspecial;
      return;
    case 2:
      if (paramParametroJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicioSinEdad = paramParametroJubilacion.aniosServicioSinEdad;
      return;
    case 3:
      if (paramParametroJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServiciom = paramParametroJubilacion.aniosServiciom;
      return;
    case 4:
      if (paramParametroJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.edad = paramParametroJubilacion.edad;
      return;
    case 5:
      if (paramParametroJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.edadm = paramParametroJubilacion.edadm;
      return;
    case 6:
      if (paramParametroJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.factor = paramParametroJubilacion.factor;
      return;
    case 7:
      if (paramParametroJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroJubilacion = paramParametroJubilacion.idParametroJubilacion;
      return;
    case 8:
      if (paramParametroJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.mesesPromediar = paramParametroJubilacion.mesesPromediar;
      return;
    case 9:
      if (paramParametroJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramParametroJubilacion.porcentaje;
      return;
    case 10:
      if (paramParametroJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramParametroJubilacion.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroJubilacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroJubilacion localParametroJubilacion = (ParametroJubilacion)paramObject;
    if (localParametroJubilacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroJubilacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroJubilacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroJubilacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroJubilacionPK))
      throw new IllegalArgumentException("arg1");
    ParametroJubilacionPK localParametroJubilacionPK = (ParametroJubilacionPK)paramObject;
    localParametroJubilacionPK.idParametroJubilacion = this.idParametroJubilacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroJubilacionPK))
      throw new IllegalArgumentException("arg1");
    ParametroJubilacionPK localParametroJubilacionPK = (ParametroJubilacionPK)paramObject;
    this.idParametroJubilacion = localParametroJubilacionPK.idParametroJubilacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroJubilacionPK))
      throw new IllegalArgumentException("arg2");
    ParametroJubilacionPK localParametroJubilacionPK = (ParametroJubilacionPK)paramObject;
    localParametroJubilacionPK.idParametroJubilacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroJubilacionPK))
      throw new IllegalArgumentException("arg2");
    ParametroJubilacionPK localParametroJubilacionPK = (ParametroJubilacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localParametroJubilacionPK.idParametroJubilacion);
  }

  private static final int jdoGetaniosServicio(ParametroJubilacion paramParametroJubilacion)
  {
    if (paramParametroJubilacion.jdoFlags <= 0)
      return paramParametroJubilacion.aniosServicio;
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJubilacion.aniosServicio;
    if (localStateManager.isLoaded(paramParametroJubilacion, jdoInheritedFieldCount + 0))
      return paramParametroJubilacion.aniosServicio;
    return localStateManager.getIntField(paramParametroJubilacion, jdoInheritedFieldCount + 0, paramParametroJubilacion.aniosServicio);
  }

  private static final void jdoSetaniosServicio(ParametroJubilacion paramParametroJubilacion, int paramInt)
  {
    if (paramParametroJubilacion.jdoFlags == 0)
    {
      paramParametroJubilacion.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJubilacion.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroJubilacion, jdoInheritedFieldCount + 0, paramParametroJubilacion.aniosServicio, paramInt);
  }

  private static final int jdoGetaniosServicioEspecial(ParametroJubilacion paramParametroJubilacion)
  {
    if (paramParametroJubilacion.jdoFlags <= 0)
      return paramParametroJubilacion.aniosServicioEspecial;
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJubilacion.aniosServicioEspecial;
    if (localStateManager.isLoaded(paramParametroJubilacion, jdoInheritedFieldCount + 1))
      return paramParametroJubilacion.aniosServicioEspecial;
    return localStateManager.getIntField(paramParametroJubilacion, jdoInheritedFieldCount + 1, paramParametroJubilacion.aniosServicioEspecial);
  }

  private static final void jdoSetaniosServicioEspecial(ParametroJubilacion paramParametroJubilacion, int paramInt)
  {
    if (paramParametroJubilacion.jdoFlags == 0)
    {
      paramParametroJubilacion.aniosServicioEspecial = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJubilacion.aniosServicioEspecial = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroJubilacion, jdoInheritedFieldCount + 1, paramParametroJubilacion.aniosServicioEspecial, paramInt);
  }

  private static final int jdoGetaniosServicioSinEdad(ParametroJubilacion paramParametroJubilacion)
  {
    if (paramParametroJubilacion.jdoFlags <= 0)
      return paramParametroJubilacion.aniosServicioSinEdad;
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJubilacion.aniosServicioSinEdad;
    if (localStateManager.isLoaded(paramParametroJubilacion, jdoInheritedFieldCount + 2))
      return paramParametroJubilacion.aniosServicioSinEdad;
    return localStateManager.getIntField(paramParametroJubilacion, jdoInheritedFieldCount + 2, paramParametroJubilacion.aniosServicioSinEdad);
  }

  private static final void jdoSetaniosServicioSinEdad(ParametroJubilacion paramParametroJubilacion, int paramInt)
  {
    if (paramParametroJubilacion.jdoFlags == 0)
    {
      paramParametroJubilacion.aniosServicioSinEdad = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJubilacion.aniosServicioSinEdad = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroJubilacion, jdoInheritedFieldCount + 2, paramParametroJubilacion.aniosServicioSinEdad, paramInt);
  }

  private static final int jdoGetaniosServiciom(ParametroJubilacion paramParametroJubilacion)
  {
    if (paramParametroJubilacion.jdoFlags <= 0)
      return paramParametroJubilacion.aniosServiciom;
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJubilacion.aniosServiciom;
    if (localStateManager.isLoaded(paramParametroJubilacion, jdoInheritedFieldCount + 3))
      return paramParametroJubilacion.aniosServiciom;
    return localStateManager.getIntField(paramParametroJubilacion, jdoInheritedFieldCount + 3, paramParametroJubilacion.aniosServiciom);
  }

  private static final void jdoSetaniosServiciom(ParametroJubilacion paramParametroJubilacion, int paramInt)
  {
    if (paramParametroJubilacion.jdoFlags == 0)
    {
      paramParametroJubilacion.aniosServiciom = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJubilacion.aniosServiciom = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroJubilacion, jdoInheritedFieldCount + 3, paramParametroJubilacion.aniosServiciom, paramInt);
  }

  private static final int jdoGetedad(ParametroJubilacion paramParametroJubilacion)
  {
    if (paramParametroJubilacion.jdoFlags <= 0)
      return paramParametroJubilacion.edad;
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJubilacion.edad;
    if (localStateManager.isLoaded(paramParametroJubilacion, jdoInheritedFieldCount + 4))
      return paramParametroJubilacion.edad;
    return localStateManager.getIntField(paramParametroJubilacion, jdoInheritedFieldCount + 4, paramParametroJubilacion.edad);
  }

  private static final void jdoSetedad(ParametroJubilacion paramParametroJubilacion, int paramInt)
  {
    if (paramParametroJubilacion.jdoFlags == 0)
    {
      paramParametroJubilacion.edad = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJubilacion.edad = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroJubilacion, jdoInheritedFieldCount + 4, paramParametroJubilacion.edad, paramInt);
  }

  private static final int jdoGetedadm(ParametroJubilacion paramParametroJubilacion)
  {
    if (paramParametroJubilacion.jdoFlags <= 0)
      return paramParametroJubilacion.edadm;
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJubilacion.edadm;
    if (localStateManager.isLoaded(paramParametroJubilacion, jdoInheritedFieldCount + 5))
      return paramParametroJubilacion.edadm;
    return localStateManager.getIntField(paramParametroJubilacion, jdoInheritedFieldCount + 5, paramParametroJubilacion.edadm);
  }

  private static final void jdoSetedadm(ParametroJubilacion paramParametroJubilacion, int paramInt)
  {
    if (paramParametroJubilacion.jdoFlags == 0)
    {
      paramParametroJubilacion.edadm = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJubilacion.edadm = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroJubilacion, jdoInheritedFieldCount + 5, paramParametroJubilacion.edadm, paramInt);
  }

  private static final double jdoGetfactor(ParametroJubilacion paramParametroJubilacion)
  {
    if (paramParametroJubilacion.jdoFlags <= 0)
      return paramParametroJubilacion.factor;
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJubilacion.factor;
    if (localStateManager.isLoaded(paramParametroJubilacion, jdoInheritedFieldCount + 6))
      return paramParametroJubilacion.factor;
    return localStateManager.getDoubleField(paramParametroJubilacion, jdoInheritedFieldCount + 6, paramParametroJubilacion.factor);
  }

  private static final void jdoSetfactor(ParametroJubilacion paramParametroJubilacion, double paramDouble)
  {
    if (paramParametroJubilacion.jdoFlags == 0)
    {
      paramParametroJubilacion.factor = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJubilacion.factor = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroJubilacion, jdoInheritedFieldCount + 6, paramParametroJubilacion.factor, paramDouble);
  }

  private static final long jdoGetidParametroJubilacion(ParametroJubilacion paramParametroJubilacion)
  {
    return paramParametroJubilacion.idParametroJubilacion;
  }

  private static final void jdoSetidParametroJubilacion(ParametroJubilacion paramParametroJubilacion, long paramLong)
  {
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJubilacion.idParametroJubilacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroJubilacion, jdoInheritedFieldCount + 7, paramParametroJubilacion.idParametroJubilacion, paramLong);
  }

  private static final int jdoGetmesesPromediar(ParametroJubilacion paramParametroJubilacion)
  {
    if (paramParametroJubilacion.jdoFlags <= 0)
      return paramParametroJubilacion.mesesPromediar;
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJubilacion.mesesPromediar;
    if (localStateManager.isLoaded(paramParametroJubilacion, jdoInheritedFieldCount + 8))
      return paramParametroJubilacion.mesesPromediar;
    return localStateManager.getIntField(paramParametroJubilacion, jdoInheritedFieldCount + 8, paramParametroJubilacion.mesesPromediar);
  }

  private static final void jdoSetmesesPromediar(ParametroJubilacion paramParametroJubilacion, int paramInt)
  {
    if (paramParametroJubilacion.jdoFlags == 0)
    {
      paramParametroJubilacion.mesesPromediar = paramInt;
      return;
    }
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJubilacion.mesesPromediar = paramInt;
      return;
    }
    localStateManager.setIntField(paramParametroJubilacion, jdoInheritedFieldCount + 8, paramParametroJubilacion.mesesPromediar, paramInt);
  }

  private static final double jdoGetporcentaje(ParametroJubilacion paramParametroJubilacion)
  {
    if (paramParametroJubilacion.jdoFlags <= 0)
      return paramParametroJubilacion.porcentaje;
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJubilacion.porcentaje;
    if (localStateManager.isLoaded(paramParametroJubilacion, jdoInheritedFieldCount + 9))
      return paramParametroJubilacion.porcentaje;
    return localStateManager.getDoubleField(paramParametroJubilacion, jdoInheritedFieldCount + 9, paramParametroJubilacion.porcentaje);
  }

  private static final void jdoSetporcentaje(ParametroJubilacion paramParametroJubilacion, double paramDouble)
  {
    if (paramParametroJubilacion.jdoFlags == 0)
    {
      paramParametroJubilacion.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJubilacion.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramParametroJubilacion, jdoInheritedFieldCount + 9, paramParametroJubilacion.porcentaje, paramDouble);
  }

  private static final TipoPersonal jdoGettipoPersonal(ParametroJubilacion paramParametroJubilacion)
  {
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramParametroJubilacion.tipoPersonal;
    if (localStateManager.isLoaded(paramParametroJubilacion, jdoInheritedFieldCount + 10))
      return paramParametroJubilacion.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramParametroJubilacion, jdoInheritedFieldCount + 10, paramParametroJubilacion.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ParametroJubilacion paramParametroJubilacion, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramParametroJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroJubilacion.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramParametroJubilacion, jdoInheritedFieldCount + 10, paramParametroJubilacion.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}