package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class FirmasReportesBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addFirmasReportes(FirmasReportes firmasReportes)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    FirmasReportes firmasReportesNew = 
      (FirmasReportes)BeanUtils.cloneBean(
      firmasReportes);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (firmasReportesNew.getOrganismo() != null) {
      firmasReportesNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        firmasReportesNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(firmasReportesNew);
  }

  public void updateFirmasReportes(FirmasReportes firmasReportes) throws Exception
  {
    FirmasReportes firmasReportesModify = 
      findFirmasReportesById(firmasReportes.getIdFirmasReportes());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (firmasReportes.getOrganismo() != null) {
      firmasReportes.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        firmasReportes.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(firmasReportesModify, firmasReportes);
  }

  public void deleteFirmasReportes(FirmasReportes firmasReportes) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    FirmasReportes firmasReportesDelete = 
      findFirmasReportesById(firmasReportes.getIdFirmasReportes());
    pm.deletePersistent(firmasReportesDelete);
  }

  public FirmasReportes findFirmasReportesById(long idFirmasReportes) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idFirmasReportes == pIdFirmasReportes";
    Query query = pm.newQuery(FirmasReportes.class, filter);

    query.declareParameters("long pIdFirmasReportes");

    parameters.put("pIdFirmasReportes", new Long(idFirmasReportes));

    Collection colFirmasReportes = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colFirmasReportes.iterator();
    return (FirmasReportes)iterator.next();
  }

  public Collection findFirmasReportesAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent firmasReportesExtent = pm.getExtent(
      FirmasReportes.class, true);
    Query query = pm.newQuery(firmasReportesExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(FirmasReportes.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colFirmasReportes = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFirmasReportes);

    return colFirmasReportes;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(FirmasReportes.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colFirmasReportes = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFirmasReportes);

    return colFirmasReportes;
  }
}