package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SindicatoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSindicato(Sindicato sindicato)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Sindicato sindicatoNew = 
      (Sindicato)BeanUtils.cloneBean(
      sindicato);

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (sindicatoNew.getConcepto() != null) {
      sindicatoNew.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        sindicatoNew.getConcepto().getIdConcepto()));
    }
    pm.makePersistent(sindicatoNew);
  }

  public void updateSindicato(Sindicato sindicato) throws Exception
  {
    Sindicato sindicatoModify = 
      findSindicatoById(sindicato.getIdSindicato());

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (sindicato.getConcepto() != null) {
      sindicato.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        sindicato.getConcepto().getIdConcepto()));
    }

    BeanUtils.copyProperties(sindicatoModify, sindicato);
  }

  public void deleteSindicato(Sindicato sindicato) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Sindicato sindicatoDelete = 
      findSindicatoById(sindicato.getIdSindicato());
    pm.deletePersistent(sindicatoDelete);
  }

  public Sindicato findSindicatoById(long idSindicato) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSindicato == pIdSindicato";
    Query query = pm.newQuery(Sindicato.class, filter);

    query.declareParameters("long pIdSindicato");

    parameters.put("pIdSindicato", new Long(idSindicato));

    Collection colSindicato = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSindicato.iterator();
    return (Sindicato)iterator.next();
  }

  public Collection findSindicatoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent sindicatoExtent = pm.getExtent(
      Sindicato.class, true);
    Query query = pm.newQuery(sindicatoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodSindicato(String codSindicato)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codSindicato == pCodSindicato";

    Query query = pm.newQuery(Sindicato.class, filter);

    query.declareParameters("java.lang.String pCodSindicato");
    HashMap parameters = new HashMap();

    parameters.put("pCodSindicato", new String(codSindicato));

    Collection colSindicato = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSindicato);

    return colSindicato;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Sindicato.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    Collection colSindicato = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSindicato);

    return colSindicato;
  }
}