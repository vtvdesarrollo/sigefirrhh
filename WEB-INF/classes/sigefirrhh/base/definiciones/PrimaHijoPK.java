package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class PrimaHijoPK
  implements Serializable
{
  public long idPrimaHijo;

  public PrimaHijoPK()
  {
  }

  public PrimaHijoPK(long idPrimaHijo)
  {
    this.idPrimaHijo = idPrimaHijo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PrimaHijoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PrimaHijoPK thatPK)
  {
    return 
      this.idPrimaHijo == thatPK.idPrimaHijo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPrimaHijo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPrimaHijo);
  }
}