package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;

public class VacacionesPorCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addVacacionesPorCargo(VacacionesPorCargo vacacionesPorCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    VacacionesPorCargo vacacionesPorCargoNew = 
      (VacacionesPorCargo)BeanUtils.cloneBean(
      vacacionesPorCargo);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (vacacionesPorCargoNew.getTipoPersonal() != null) {
      vacacionesPorCargoNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        vacacionesPorCargoNew.getTipoPersonal().getIdTipoPersonal()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (vacacionesPorCargoNew.getCargo() != null) {
      vacacionesPorCargoNew.setCargo(
        cargoBeanBusiness.findCargoById(
        vacacionesPorCargoNew.getCargo().getIdCargo()));
    }
    pm.makePersistent(vacacionesPorCargoNew);
  }

  public void updateVacacionesPorCargo(VacacionesPorCargo vacacionesPorCargo) throws Exception
  {
    VacacionesPorCargo vacacionesPorCargoModify = 
      findVacacionesPorCargoById(vacacionesPorCargo.getIdVacacionesPorCargo());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (vacacionesPorCargo.getTipoPersonal() != null) {
      vacacionesPorCargo.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        vacacionesPorCargo.getTipoPersonal().getIdTipoPersonal()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (vacacionesPorCargo.getCargo() != null) {
      vacacionesPorCargo.setCargo(
        cargoBeanBusiness.findCargoById(
        vacacionesPorCargo.getCargo().getIdCargo()));
    }

    BeanUtils.copyProperties(vacacionesPorCargoModify, vacacionesPorCargo);
  }

  public void deleteVacacionesPorCargo(VacacionesPorCargo vacacionesPorCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    VacacionesPorCargo vacacionesPorCargoDelete = 
      findVacacionesPorCargoById(vacacionesPorCargo.getIdVacacionesPorCargo());
    pm.deletePersistent(vacacionesPorCargoDelete);
  }

  public VacacionesPorCargo findVacacionesPorCargoById(long idVacacionesPorCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idVacacionesPorCargo == pIdVacacionesPorCargo";
    Query query = pm.newQuery(VacacionesPorCargo.class, filter);

    query.declareParameters("long pIdVacacionesPorCargo");

    parameters.put("pIdVacacionesPorCargo", new Long(idVacacionesPorCargo));

    Collection colVacacionesPorCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colVacacionesPorCargo.iterator();
    return (VacacionesPorCargo)iterator.next();
  }

  public Collection findVacacionesPorCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent vacacionesPorCargoExtent = pm.getExtent(
      VacacionesPorCargo.class, true);
    Query query = pm.newQuery(vacacionesPorCargoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(VacacionesPorCargo.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colVacacionesPorCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colVacacionesPorCargo);

    return colVacacionesPorCargo;
  }
}