package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class EscalaCuadroOnaprePK
  implements Serializable
{
  public long idEscalaCuadroOnapre;

  public EscalaCuadroOnaprePK()
  {
  }

  public EscalaCuadroOnaprePK(long idEscalaCuadroOnapre)
  {
    this.idEscalaCuadroOnapre = idEscalaCuadroOnapre;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EscalaCuadroOnaprePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EscalaCuadroOnaprePK thatPK)
  {
    return 
      this.idEscalaCuadroOnapre == thatPK.idEscalaCuadroOnapre;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEscalaCuadroOnapre)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEscalaCuadroOnapre);
  }
}