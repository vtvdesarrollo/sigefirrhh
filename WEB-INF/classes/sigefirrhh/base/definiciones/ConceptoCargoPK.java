package sigefirrhh.base.definiciones;

import java.io.Serializable;

public class ConceptoCargoPK
  implements Serializable
{
  public long idConceptoCargo;

  public ConceptoCargoPK()
  {
  }

  public ConceptoCargoPK(long idConceptoCargo)
  {
    this.idConceptoCargo = idConceptoCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoCargoPK thatPK)
  {
    return 
      this.idConceptoCargo == thatPK.idConceptoCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoCargo);
  }
}