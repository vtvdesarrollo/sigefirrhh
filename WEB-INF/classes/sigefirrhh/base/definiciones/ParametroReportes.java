package sigefirrhh.base.definiciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class ParametroReportes
  implements Serializable, PersistenceCapable
{
  private long idParametroReportes;
  private String encabezadoUno;
  private String encabezadoDos;
  private String encabezadoTres;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "encabezadoDos", "encabezadoTres", "encabezadoUno", "idParametroReportes", "organismo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String getEncabezadoDos()
  {
    return jdoGetencabezadoDos(this);
  }

  public String getEncabezadoTres()
  {
    return jdoGetencabezadoTres(this);
  }

  public String getEncabezadoUno()
  {
    return jdoGetencabezadoUno(this);
  }

  public long getIdParametroReportes()
  {
    return jdoGetidParametroReportes(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setEncabezadoDos(String string)
  {
    jdoSetencabezadoDos(this, string);
  }

  public void setEncabezadoTres(String string)
  {
    jdoSetencabezadoTres(this, string);
  }

  public void setEncabezadoUno(String string)
  {
    jdoSetencabezadoUno(this, string);
  }

  public void setIdParametroReportes(long l)
  {
    jdoSetidParametroReportes(this, l);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.definiciones.ParametroReportes"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroReportes());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroReportes localParametroReportes = new ParametroReportes();
    localParametroReportes.jdoFlags = 1;
    localParametroReportes.jdoStateManager = paramStateManager;
    return localParametroReportes;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroReportes localParametroReportes = new ParametroReportes();
    localParametroReportes.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroReportes.jdoFlags = 1;
    localParametroReportes.jdoStateManager = paramStateManager;
    return localParametroReportes;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.encabezadoDos);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.encabezadoTres);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.encabezadoUno);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParametroReportes);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.encabezadoDos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.encabezadoTres = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.encabezadoUno = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParametroReportes = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroReportes paramParametroReportes, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroReportes == null)
        throw new IllegalArgumentException("arg1");
      this.encabezadoDos = paramParametroReportes.encabezadoDos;
      return;
    case 1:
      if (paramParametroReportes == null)
        throw new IllegalArgumentException("arg1");
      this.encabezadoTres = paramParametroReportes.encabezadoTres;
      return;
    case 2:
      if (paramParametroReportes == null)
        throw new IllegalArgumentException("arg1");
      this.encabezadoUno = paramParametroReportes.encabezadoUno;
      return;
    case 3:
      if (paramParametroReportes == null)
        throw new IllegalArgumentException("arg1");
      this.idParametroReportes = paramParametroReportes.idParametroReportes;
      return;
    case 4:
      if (paramParametroReportes == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramParametroReportes.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroReportes))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroReportes localParametroReportes = (ParametroReportes)paramObject;
    if (localParametroReportes.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroReportes, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroReportesPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroReportesPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroReportesPK))
      throw new IllegalArgumentException("arg1");
    ParametroReportesPK localParametroReportesPK = (ParametroReportesPK)paramObject;
    localParametroReportesPK.idParametroReportes = this.idParametroReportes;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroReportesPK))
      throw new IllegalArgumentException("arg1");
    ParametroReportesPK localParametroReportesPK = (ParametroReportesPK)paramObject;
    this.idParametroReportes = localParametroReportesPK.idParametroReportes;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroReportesPK))
      throw new IllegalArgumentException("arg2");
    ParametroReportesPK localParametroReportesPK = (ParametroReportesPK)paramObject;
    localParametroReportesPK.idParametroReportes = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroReportesPK))
      throw new IllegalArgumentException("arg2");
    ParametroReportesPK localParametroReportesPK = (ParametroReportesPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localParametroReportesPK.idParametroReportes);
  }

  private static final String jdoGetencabezadoDos(ParametroReportes paramParametroReportes)
  {
    if (paramParametroReportes.jdoFlags <= 0)
      return paramParametroReportes.encabezadoDos;
    StateManager localStateManager = paramParametroReportes.jdoStateManager;
    if (localStateManager == null)
      return paramParametroReportes.encabezadoDos;
    if (localStateManager.isLoaded(paramParametroReportes, jdoInheritedFieldCount + 0))
      return paramParametroReportes.encabezadoDos;
    return localStateManager.getStringField(paramParametroReportes, jdoInheritedFieldCount + 0, paramParametroReportes.encabezadoDos);
  }

  private static final void jdoSetencabezadoDos(ParametroReportes paramParametroReportes, String paramString)
  {
    if (paramParametroReportes.jdoFlags == 0)
    {
      paramParametroReportes.encabezadoDos = paramString;
      return;
    }
    StateManager localStateManager = paramParametroReportes.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroReportes.encabezadoDos = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroReportes, jdoInheritedFieldCount + 0, paramParametroReportes.encabezadoDos, paramString);
  }

  private static final String jdoGetencabezadoTres(ParametroReportes paramParametroReportes)
  {
    if (paramParametroReportes.jdoFlags <= 0)
      return paramParametroReportes.encabezadoTres;
    StateManager localStateManager = paramParametroReportes.jdoStateManager;
    if (localStateManager == null)
      return paramParametroReportes.encabezadoTres;
    if (localStateManager.isLoaded(paramParametroReportes, jdoInheritedFieldCount + 1))
      return paramParametroReportes.encabezadoTres;
    return localStateManager.getStringField(paramParametroReportes, jdoInheritedFieldCount + 1, paramParametroReportes.encabezadoTres);
  }

  private static final void jdoSetencabezadoTres(ParametroReportes paramParametroReportes, String paramString)
  {
    if (paramParametroReportes.jdoFlags == 0)
    {
      paramParametroReportes.encabezadoTres = paramString;
      return;
    }
    StateManager localStateManager = paramParametroReportes.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroReportes.encabezadoTres = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroReportes, jdoInheritedFieldCount + 1, paramParametroReportes.encabezadoTres, paramString);
  }

  private static final String jdoGetencabezadoUno(ParametroReportes paramParametroReportes)
  {
    if (paramParametroReportes.jdoFlags <= 0)
      return paramParametroReportes.encabezadoUno;
    StateManager localStateManager = paramParametroReportes.jdoStateManager;
    if (localStateManager == null)
      return paramParametroReportes.encabezadoUno;
    if (localStateManager.isLoaded(paramParametroReportes, jdoInheritedFieldCount + 2))
      return paramParametroReportes.encabezadoUno;
    return localStateManager.getStringField(paramParametroReportes, jdoInheritedFieldCount + 2, paramParametroReportes.encabezadoUno);
  }

  private static final void jdoSetencabezadoUno(ParametroReportes paramParametroReportes, String paramString)
  {
    if (paramParametroReportes.jdoFlags == 0)
    {
      paramParametroReportes.encabezadoUno = paramString;
      return;
    }
    StateManager localStateManager = paramParametroReportes.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroReportes.encabezadoUno = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroReportes, jdoInheritedFieldCount + 2, paramParametroReportes.encabezadoUno, paramString);
  }

  private static final long jdoGetidParametroReportes(ParametroReportes paramParametroReportes)
  {
    return paramParametroReportes.idParametroReportes;
  }

  private static final void jdoSetidParametroReportes(ParametroReportes paramParametroReportes, long paramLong)
  {
    StateManager localStateManager = paramParametroReportes.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroReportes.idParametroReportes = paramLong;
      return;
    }
    localStateManager.setLongField(paramParametroReportes, jdoInheritedFieldCount + 3, paramParametroReportes.idParametroReportes, paramLong);
  }

  private static final Organismo jdoGetorganismo(ParametroReportes paramParametroReportes)
  {
    StateManager localStateManager = paramParametroReportes.jdoStateManager;
    if (localStateManager == null)
      return paramParametroReportes.organismo;
    if (localStateManager.isLoaded(paramParametroReportes, jdoInheritedFieldCount + 4))
      return paramParametroReportes.organismo;
    return (Organismo)localStateManager.getObjectField(paramParametroReportes, jdoInheritedFieldCount + 4, paramParametroReportes.organismo);
  }

  private static final void jdoSetorganismo(ParametroReportes paramParametroReportes, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramParametroReportes.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroReportes.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramParametroReportes, jdoInheritedFieldCount + 4, paramParametroReportes.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}