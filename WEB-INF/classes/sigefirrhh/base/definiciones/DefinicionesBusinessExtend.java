package sigefirrhh.base.definiciones;

import java.io.Serializable;
import java.util.Collection;

public class DefinicionesBusinessExtend extends DefinicionesBusiness
  implements Serializable
{
  TipoPersonalBeanBusinessExtend tipoPersonalBeanBusiness = new TipoPersonalBeanBusinessExtend();

  ConceptoTipoPersonalBeanBusinessExtend conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusinessExtend();

  ConceptoAsociadoBeanBusinessExtend conceptoAsociadoBeanBusiness = new ConceptoAsociadoBeanBusinessExtend();

  FrecuenciaPagoBeanBusinessExtend frecuenciaPagoBeanBusiness = new FrecuenciaPagoBeanBusinessExtend();

  FrecuenciaTipoPersonalBeanBusinessExtend frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusinessExtend();

  public Collection findTipoPersonalByGrupoNomina(long idGrupoNomina) throws Exception
  {
    return this.tipoPersonalBeanBusiness.findByGrupoNomina(idGrupoNomina);
  }

  public Collection findConceptoTipoPersonalByCodConcepto(long idTipoPersonal, String codConcepto) throws Exception {
    return this.conceptoTipoPersonalBeanBusiness.findByCodConcepto(idTipoPersonal, codConcepto);
  }

  public Collection findConceptoAsociadoByIdTipoPersonal(long idTipoPersonal) throws Exception {
    return this.conceptoAsociadoBeanBusiness.findByIdTipoPersonal(idTipoPersonal);
  }

  public Collection findConceptoAsociadoByConceptoTipoPersonal(long idConceptoTipoPersonal) throws Exception {
    return this.conceptoAsociadoBeanBusiness.findByConceptoTipoPersonal(idConceptoTipoPersonal);
  }

  public Collection findFrecuenciaPagoByTipoPersonal(long idTipoPersonal) throws Exception {
    return this.frecuenciaPagoBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public Collection findFrecuenciaPagoByOrganismoMayorA10(long idOrganismo) throws Exception {
    return this.frecuenciaPagoBeanBusiness.findByOrganismoMayorA10(idOrganismo);
  }

  public FrecuenciaTipoPersonal findFrecuenciaTipoPersonalByTipoPersonal(long idFrecuenciaPago, long idTipoPersonal) throws Exception {
    return this.frecuenciaTipoPersonalBeanBusiness.findByTipoPersonal(idFrecuenciaPago, idTipoPersonal);
  }

  public FrecuenciaTipoPersonal findFrecuenciaTipoPersonalByTipoPersonalNoPersistente(int idFrecuenciaPago, long idTipoPersonal) throws Exception {
    return this.frecuenciaTipoPersonalBeanBusiness.findByTipoPersonalNoPersistente(idFrecuenciaPago, idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalByTipoPersonalAndTipoPrestamo(long idTipoPersonal, String tipoPrestamo) throws Exception {
    return this.conceptoTipoPersonalBeanBusiness.findByTipoPersonalAndTipoPrestamo(idTipoPersonal, tipoPrestamo);
  }

  public Collection findTipoPersonalByManejaRac(String manejaRac, long idUsuario, String administrador) throws Exception {
    return this.tipoPersonalBeanBusiness.findByManejaRac(manejaRac, idUsuario, administrador);
  }

  public Collection findTipoPersonalByAprobacionMpd(String aprobacionMpd, long idUsuario, String administrador) throws Exception {
    return this.tipoPersonalBeanBusiness.findByAprobacionMpd(aprobacionMpd, idUsuario, administrador);
  }

  public Collection findTipoPersonalByManejaRacAndAprobacionMpd(String manejaRac, String aprobacionMpd, long idUsuario, String administrador) throws Exception {
    return this.tipoPersonalBeanBusiness.findByManejaRacAndAprobacionMpd(manejaRac, aprobacionMpd, idUsuario, administrador);
  }

  public Collection findTipoPersonalByManejaRacAprobacionMpdClasificacionPersonal(String manejaRac, String aprobacionMpd, long idClasificacionPersonal, long idUsuario, String administrador) throws Exception {
    return this.tipoPersonalBeanBusiness.findByManejaRacAprobacionMpdClasificacionPersonal(manejaRac, aprobacionMpd, idClasificacionPersonal, idUsuario, administrador);
  }

  public FrecuenciaTipoPersonal findFrecuenciaTipoPersonalByTipoPersonal(int codFrecuenciaPago, long idTipoPersonal) throws Exception {
    return this.frecuenciaTipoPersonalBeanBusiness.findByTipoPersonal(codFrecuenciaPago, idTipoPersonal);
  }

  public Collection findConceptoTipoPersonalAsignacionesByTipoPersonalAndTipoPrestamo(long idTipoPersonal, String tipoPrestamo) throws Exception {
    return this.conceptoTipoPersonalBeanBusiness.findByTipoPersonalAsignacionesAndTipoPrestamo(idTipoPersonal, tipoPrestamo);
  }
}