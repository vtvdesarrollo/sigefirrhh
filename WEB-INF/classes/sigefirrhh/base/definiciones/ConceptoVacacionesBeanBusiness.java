package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ConceptoVacacionesBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoVacaciones(ConceptoVacaciones conceptoVacaciones)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoVacaciones conceptoVacacionesNew = 
      (ConceptoVacaciones)BeanUtils.cloneBean(
      conceptoVacaciones);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (conceptoVacacionesNew.getTipoPersonal() != null) {
      conceptoVacacionesNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        conceptoVacacionesNew.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoVacacionesNew.getConceptoTipoPersonal() != null) {
      conceptoVacacionesNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoVacacionesNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoAlicuotaBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoVacacionesNew.getConceptoAlicuota() != null) {
      conceptoVacacionesNew.setConceptoAlicuota(
        conceptoAlicuotaBeanBusiness.findConceptoTipoPersonalById(
        conceptoVacacionesNew.getConceptoAlicuota().getIdConceptoTipoPersonal()));
    }
    pm.makePersistent(conceptoVacacionesNew);
  }

  public void updateConceptoVacaciones(ConceptoVacaciones conceptoVacaciones) throws Exception
  {
    ConceptoVacaciones conceptoVacacionesModify = 
      findConceptoVacacionesById(conceptoVacaciones.getIdConceptoVacaciones());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (conceptoVacaciones.getTipoPersonal() != null) {
      conceptoVacaciones.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        conceptoVacaciones.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoVacaciones.getConceptoTipoPersonal() != null) {
      conceptoVacaciones.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoVacaciones.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoAlicuotaBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoVacaciones.getConceptoAlicuota() != null) {
      conceptoVacaciones.setConceptoAlicuota(
        conceptoAlicuotaBeanBusiness.findConceptoTipoPersonalById(
        conceptoVacaciones.getConceptoAlicuota().getIdConceptoTipoPersonal()));
    }

    BeanUtils.copyProperties(conceptoVacacionesModify, conceptoVacaciones);
  }

  public void deleteConceptoVacaciones(ConceptoVacaciones conceptoVacaciones) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoVacaciones conceptoVacacionesDelete = 
      findConceptoVacacionesById(conceptoVacaciones.getIdConceptoVacaciones());
    pm.deletePersistent(conceptoVacacionesDelete);
  }

  public ConceptoVacaciones findConceptoVacacionesById(long idConceptoVacaciones) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoVacaciones == pIdConceptoVacaciones";
    Query query = pm.newQuery(ConceptoVacaciones.class, filter);

    query.declareParameters("long pIdConceptoVacaciones");

    parameters.put("pIdConceptoVacaciones", new Long(idConceptoVacaciones));

    Collection colConceptoVacaciones = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoVacaciones.iterator();
    return (ConceptoVacaciones)iterator.next();
  }

  public Collection findConceptoVacacionesAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoVacacionesExtent = pm.getExtent(
      ConceptoVacaciones.class, true);
    Query query = pm.newQuery(conceptoVacacionesExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ConceptoVacaciones.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoVacaciones = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoVacaciones);

    return colConceptoVacaciones;
  }
}