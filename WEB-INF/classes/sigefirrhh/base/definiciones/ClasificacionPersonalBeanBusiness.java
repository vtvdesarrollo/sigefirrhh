package sigefirrhh.base.definiciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ClasificacionPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addClasificacionPersonal(ClasificacionPersonal clasificacionPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ClasificacionPersonal clasificacionPersonalNew = 
      (ClasificacionPersonal)BeanUtils.cloneBean(
      clasificacionPersonal);

    CategoriaPersonalBeanBusiness categoriaPersonalBeanBusiness = new CategoriaPersonalBeanBusiness();

    if (clasificacionPersonalNew.getCategoriaPersonal() != null) {
      clasificacionPersonalNew.setCategoriaPersonal(
        categoriaPersonalBeanBusiness.findCategoriaPersonalById(
        clasificacionPersonalNew.getCategoriaPersonal().getIdCategoriaPersonal()));
    }

    RelacionPersonalBeanBusiness relacionPersonalBeanBusiness = new RelacionPersonalBeanBusiness();

    if (clasificacionPersonalNew.getRelacionPersonal() != null) {
      clasificacionPersonalNew.setRelacionPersonal(
        relacionPersonalBeanBusiness.findRelacionPersonalById(
        clasificacionPersonalNew.getRelacionPersonal().getIdRelacionPersonal()));
    }

    CategoriaPresupuestoBeanBusiness categoriaPresupuestoBeanBusiness = new CategoriaPresupuestoBeanBusiness();

    if (clasificacionPersonalNew.getCategoriaPresupuesto() != null) {
      clasificacionPersonalNew.setCategoriaPresupuesto(
        categoriaPresupuestoBeanBusiness.findCategoriaPresupuestoById(
        clasificacionPersonalNew.getCategoriaPresupuesto().getIdCategoriaPresupuesto()));
    }
    pm.makePersistent(clasificacionPersonalNew);
  }

  public void updateClasificacionPersonal(ClasificacionPersonal clasificacionPersonal) throws Exception
  {
    ClasificacionPersonal clasificacionPersonalModify = 
      findClasificacionPersonalById(clasificacionPersonal.getIdClasificacionPersonal());

    CategoriaPersonalBeanBusiness categoriaPersonalBeanBusiness = new CategoriaPersonalBeanBusiness();

    if (clasificacionPersonal.getCategoriaPersonal() != null) {
      clasificacionPersonal.setCategoriaPersonal(
        categoriaPersonalBeanBusiness.findCategoriaPersonalById(
        clasificacionPersonal.getCategoriaPersonal().getIdCategoriaPersonal()));
    }

    RelacionPersonalBeanBusiness relacionPersonalBeanBusiness = new RelacionPersonalBeanBusiness();

    if (clasificacionPersonal.getRelacionPersonal() != null) {
      clasificacionPersonal.setRelacionPersonal(
        relacionPersonalBeanBusiness.findRelacionPersonalById(
        clasificacionPersonal.getRelacionPersonal().getIdRelacionPersonal()));
    }

    CategoriaPresupuestoBeanBusiness categoriaPresupuestoBeanBusiness = new CategoriaPresupuestoBeanBusiness();

    if (clasificacionPersonal.getCategoriaPresupuesto() != null) {
      clasificacionPersonal.setCategoriaPresupuesto(
        categoriaPresupuestoBeanBusiness.findCategoriaPresupuestoById(
        clasificacionPersonal.getCategoriaPresupuesto().getIdCategoriaPresupuesto()));
    }

    BeanUtils.copyProperties(clasificacionPersonalModify, clasificacionPersonal);
  }

  public void deleteClasificacionPersonal(ClasificacionPersonal clasificacionPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ClasificacionPersonal clasificacionPersonalDelete = 
      findClasificacionPersonalById(clasificacionPersonal.getIdClasificacionPersonal());
    pm.deletePersistent(clasificacionPersonalDelete);
  }

  public ClasificacionPersonal findClasificacionPersonalById(long idClasificacionPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idClasificacionPersonal == pIdClasificacionPersonal";
    Query query = pm.newQuery(ClasificacionPersonal.class, filter);

    query.declareParameters("long pIdClasificacionPersonal");

    parameters.put("pIdClasificacionPersonal", new Long(idClasificacionPersonal));

    Collection colClasificacionPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colClasificacionPersonal.iterator();
    return (ClasificacionPersonal)iterator.next();
  }

  public Collection findClasificacionPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent clasificacionPersonalExtent = pm.getExtent(
      ClasificacionPersonal.class, true);
    Query query = pm.newQuery(clasificacionPersonalExtent);
    query.setOrdering("categoriaPersonal.codCategoria ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCategoriaPersonal(long idCategoriaPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "categoriaPersonal.idCategoriaPersonal == pIdCategoriaPersonal";

    Query query = pm.newQuery(ClasificacionPersonal.class, filter);

    query.declareParameters("long pIdCategoriaPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdCategoriaPersonal", new Long(idCategoriaPersonal));

    query.setOrdering("categoriaPersonal.codCategoria ascending");

    Collection colClasificacionPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colClasificacionPersonal);

    return colClasificacionPersonal;
  }

  public Collection findByRelacionPersonal(long idRelacionPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "relacionPersonal.idRelacionPersonal == pIdRelacionPersonal";

    Query query = pm.newQuery(ClasificacionPersonal.class, filter);

    query.declareParameters("long pIdRelacionPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdRelacionPersonal", new Long(idRelacionPersonal));

    query.setOrdering("categoriaPersonal.codCategoria ascending");

    Collection colClasificacionPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colClasificacionPersonal);

    return colClasificacionPersonal;
  }
}