package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class AdministradoraUelForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(AdministradoraUelForm.class.getName());
  private AdministradoraUel administradoraUel;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showAdministradoraUelByUnidadAdministradora;
  private String findSelectUnidadAdministradora;
  private Collection findColUnidadAdministradora;
  private Collection colUnidadAdministradora;
  private Collection colUnidadEjecutora;
  private String selectUnidadAdministradora;
  private String selectUnidadEjecutora;
  private Object stateResultAdministradoraUelByUnidadAdministradora = null;

  public String getFindSelectUnidadAdministradora()
  {
    return this.findSelectUnidadAdministradora;
  }
  public void setFindSelectUnidadAdministradora(String valUnidadAdministradora) {
    this.findSelectUnidadAdministradora = valUnidadAdministradora;
  }

  public Collection getFindColUnidadAdministradora() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUnidadAdministradora.iterator();
    UnidadAdministradora unidadAdministradora = null;
    while (iterator.hasNext()) {
      unidadAdministradora = (UnidadAdministradora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadAdministradora.getIdUnidadAdministradora()), 
        unidadAdministradora.toString()));
    }
    return col;
  }

  public String getSelectUnidadAdministradora()
  {
    return this.selectUnidadAdministradora;
  }
  public void setSelectUnidadAdministradora(String valUnidadAdministradora) {
    Iterator iterator = this.colUnidadAdministradora.iterator();
    UnidadAdministradora unidadAdministradora = null;
    this.administradoraUel.setUnidadAdministradora(null);
    while (iterator.hasNext()) {
      unidadAdministradora = (UnidadAdministradora)iterator.next();
      if (String.valueOf(unidadAdministradora.getIdUnidadAdministradora()).equals(
        valUnidadAdministradora)) {
        this.administradoraUel.setUnidadAdministradora(
          unidadAdministradora);
        break;
      }
    }
    this.selectUnidadAdministradora = valUnidadAdministradora;
  }
  public String getSelectUnidadEjecutora() {
    return this.selectUnidadEjecutora;
  }
  public void setSelectUnidadEjecutora(String valUnidadEjecutora) {
    Iterator iterator = this.colUnidadEjecutora.iterator();
    UnidadEjecutora unidadEjecutora = null;
    this.administradoraUel.setUnidadEjecutora(null);
    while (iterator.hasNext()) {
      unidadEjecutora = (UnidadEjecutora)iterator.next();
      if (String.valueOf(unidadEjecutora.getIdUnidadEjecutora()).equals(
        valUnidadEjecutora)) {
        this.administradoraUel.setUnidadEjecutora(
          unidadEjecutora);
        break;
      }
    }
    this.selectUnidadEjecutora = valUnidadEjecutora;
  }
  public Collection getResult() {
    return this.result;
  }

  public AdministradoraUel getAdministradoraUel() {
    if (this.administradoraUel == null) {
      this.administradoraUel = new AdministradoraUel();
    }
    return this.administradoraUel;
  }

  public AdministradoraUelForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColUnidadAdministradora()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadAdministradora.iterator();
    UnidadAdministradora unidadAdministradora = null;
    while (iterator.hasNext()) {
      unidadAdministradora = (UnidadAdministradora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadAdministradora.getIdUnidadAdministradora()), 
        unidadAdministradora.toString()));
    }
    return col;
  }

  public Collection getColUnidadEjecutora()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadEjecutora.iterator();
    UnidadEjecutora unidadEjecutora = null;
    while (iterator.hasNext()) {
      unidadEjecutora = (UnidadEjecutora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadEjecutora.getIdUnidadEjecutora()), 
        unidadEjecutora.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColUnidadAdministradora = 
        this.estructuraFacade.findUnidadAdministradoraByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colUnidadAdministradora = 
        this.estructuraFacade.findUnidadAdministradoraByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colUnidadEjecutora = 
        this.estructuraFacade.findAllUnidadEjecutora();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findAdministradoraUelByUnidadAdministradora()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findAdministradoraUelByUnidadAdministradora(Long.valueOf(this.findSelectUnidadAdministradora).longValue());
      this.showAdministradoraUelByUnidadAdministradora = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAdministradoraUelByUnidadAdministradora)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUnidadAdministradora = null;

    return null;
  }

  public boolean isShowAdministradoraUelByUnidadAdministradora() {
    return this.showAdministradoraUelByUnidadAdministradora;
  }

  public String selectAdministradoraUel()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUnidadAdministradora = null;
    this.selectUnidadEjecutora = null;

    long idAdministradoraUel = 
      Long.parseLong((String)requestParameterMap.get("idAdministradoraUel"));
    try
    {
      this.administradoraUel = 
        this.estructuraFacade.findAdministradoraUelById(
        idAdministradoraUel);
      if (this.administradoraUel.getUnidadAdministradora() != null) {
        this.selectUnidadAdministradora = 
          String.valueOf(this.administradoraUel.getUnidadAdministradora().getIdUnidadAdministradora());
      }
      if (this.administradoraUel.getUnidadEjecutora() != null) {
        this.selectUnidadEjecutora = 
          String.valueOf(this.administradoraUel.getUnidadEjecutora().getIdUnidadEjecutora());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.administradoraUel = null;
    this.showAdministradoraUelByUnidadAdministradora = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addAdministradoraUel(
          this.administradoraUel);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateAdministradoraUel(
          this.administradoraUel);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteAdministradoraUel(
        this.administradoraUel);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.administradoraUel = new AdministradoraUel();

    this.selectUnidadAdministradora = null;

    this.selectUnidadEjecutora = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.administradoraUel.setIdAdministradoraUel(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.AdministradoraUel"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.administradoraUel = new AdministradoraUel();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}