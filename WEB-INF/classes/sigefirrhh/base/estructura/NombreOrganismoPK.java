package sigefirrhh.base.estructura;

import java.io.Serializable;

public class NombreOrganismoPK
  implements Serializable
{
  public long idNombreOrganismo;

  public NombreOrganismoPK()
  {
  }

  public NombreOrganismoPK(long idNombreOrganismo)
  {
    this.idNombreOrganismo = idNombreOrganismo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((NombreOrganismoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(NombreOrganismoPK thatPK)
  {
    return 
      this.idNombreOrganismo == thatPK.idNombreOrganismo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idNombreOrganismo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idNombreOrganismo);
  }
}