package sigefirrhh.base.estructura;

import java.io.Serializable;

public class LugarPagoPK
  implements Serializable
{
  public long idLugarPago;

  public LugarPagoPK()
  {
  }

  public LugarPagoPK(long idLugarPago)
  {
    this.idLugarPago = idLugarPago;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((LugarPagoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(LugarPagoPK thatPK)
  {
    return 
      this.idLugarPago == thatPK.idLugarPago;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idLugarPago)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idLugarPago);
  }
}