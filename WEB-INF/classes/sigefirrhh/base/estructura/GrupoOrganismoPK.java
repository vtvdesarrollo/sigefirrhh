package sigefirrhh.base.estructura;

import java.io.Serializable;

public class GrupoOrganismoPK
  implements Serializable
{
  public long idGrupoOrganismo;

  public GrupoOrganismoPK()
  {
  }

  public GrupoOrganismoPK(long idGrupoOrganismo)
  {
    this.idGrupoOrganismo = idGrupoOrganismo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((GrupoOrganismoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(GrupoOrganismoPK thatPK)
  {
    return 
      this.idGrupoOrganismo == thatPK.idGrupoOrganismo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idGrupoOrganismo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idGrupoOrganismo);
  }
}