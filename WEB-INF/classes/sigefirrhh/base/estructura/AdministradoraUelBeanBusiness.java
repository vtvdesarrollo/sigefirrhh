package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class AdministradoraUelBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAdministradoraUel(AdministradoraUel administradoraUel)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    AdministradoraUel administradoraUelNew = 
      (AdministradoraUel)BeanUtils.cloneBean(
      administradoraUel);

    UnidadAdministradoraBeanBusiness unidadAdministradoraBeanBusiness = new UnidadAdministradoraBeanBusiness();

    if (administradoraUelNew.getUnidadAdministradora() != null) {
      administradoraUelNew.setUnidadAdministradora(
        unidadAdministradoraBeanBusiness.findUnidadAdministradoraById(
        administradoraUelNew.getUnidadAdministradora().getIdUnidadAdministradora()));
    }

    UnidadEjecutoraBeanBusiness unidadEjecutoraBeanBusiness = new UnidadEjecutoraBeanBusiness();

    if (administradoraUelNew.getUnidadEjecutora() != null) {
      administradoraUelNew.setUnidadEjecutora(
        unidadEjecutoraBeanBusiness.findUnidadEjecutoraById(
        administradoraUelNew.getUnidadEjecutora().getIdUnidadEjecutora()));
    }
    pm.makePersistent(administradoraUelNew);
  }

  public void updateAdministradoraUel(AdministradoraUel administradoraUel) throws Exception
  {
    AdministradoraUel administradoraUelModify = 
      findAdministradoraUelById(administradoraUel.getIdAdministradoraUel());

    UnidadAdministradoraBeanBusiness unidadAdministradoraBeanBusiness = new UnidadAdministradoraBeanBusiness();

    if (administradoraUel.getUnidadAdministradora() != null) {
      administradoraUel.setUnidadAdministradora(
        unidadAdministradoraBeanBusiness.findUnidadAdministradoraById(
        administradoraUel.getUnidadAdministradora().getIdUnidadAdministradora()));
    }

    UnidadEjecutoraBeanBusiness unidadEjecutoraBeanBusiness = new UnidadEjecutoraBeanBusiness();

    if (administradoraUel.getUnidadEjecutora() != null) {
      administradoraUel.setUnidadEjecutora(
        unidadEjecutoraBeanBusiness.findUnidadEjecutoraById(
        administradoraUel.getUnidadEjecutora().getIdUnidadEjecutora()));
    }

    BeanUtils.copyProperties(administradoraUelModify, administradoraUel);
  }

  public void deleteAdministradoraUel(AdministradoraUel administradoraUel) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    AdministradoraUel administradoraUelDelete = 
      findAdministradoraUelById(administradoraUel.getIdAdministradoraUel());
    pm.deletePersistent(administradoraUelDelete);
  }

  public AdministradoraUel findAdministradoraUelById(long idAdministradoraUel) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAdministradoraUel == pIdAdministradoraUel";
    Query query = pm.newQuery(AdministradoraUel.class, filter);

    query.declareParameters("long pIdAdministradoraUel");

    parameters.put("pIdAdministradoraUel", new Long(idAdministradoraUel));

    Collection colAdministradoraUel = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAdministradoraUel.iterator();
    return (AdministradoraUel)iterator.next();
  }

  public Collection findAdministradoraUelAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent administradoraUelExtent = pm.getExtent(
      AdministradoraUel.class, true);
    Query query = pm.newQuery(administradoraUelExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadAdministradora.idUnidadAdministradora == pIdUnidadAdministradora";

    Query query = pm.newQuery(AdministradoraUel.class, filter);

    query.declareParameters("long pIdUnidadAdministradora");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadAdministradora", new Long(idUnidadAdministradora));

    Collection colAdministradoraUel = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAdministradoraUel);

    return colAdministradoraUel;
  }
}