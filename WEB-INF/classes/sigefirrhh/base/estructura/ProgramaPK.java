package sigefirrhh.base.estructura;

import java.io.Serializable;

public class ProgramaPK
  implements Serializable
{
  public long idPrograma;

  public ProgramaPK()
  {
  }

  public ProgramaPK(long idPrograma)
  {
    this.idPrograma = idPrograma;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ProgramaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ProgramaPK thatPK)
  {
    return 
      this.idPrograma == thatPK.idPrograma;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPrograma)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPrograma);
  }
}