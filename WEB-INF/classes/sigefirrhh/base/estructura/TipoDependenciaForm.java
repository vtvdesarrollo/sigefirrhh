package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class TipoDependenciaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TipoDependenciaForm.class.getName());
  private TipoDependencia tipoDependencia;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showTipoDependenciaByCodTipoDependencia;
  private boolean showTipoDependenciaByNombre;
  private String findCodTipoDependencia;
  private String findNombre;
  private Object stateResultTipoDependenciaByCodTipoDependencia = null;

  private Object stateResultTipoDependenciaByNombre = null;

  public String getFindCodTipoDependencia()
  {
    return this.findCodTipoDependencia;
  }
  public void setFindCodTipoDependencia(String findCodTipoDependencia) {
    this.findCodTipoDependencia = findCodTipoDependencia;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public TipoDependencia getTipoDependencia() {
    if (this.tipoDependencia == null) {
      this.tipoDependencia = new TipoDependencia();
    }
    return this.tipoDependencia;
  }

  public TipoDependenciaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findTipoDependenciaByCodTipoDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findTipoDependenciaByCodTipoDependencia(this.findCodTipoDependencia);
      this.showTipoDependenciaByCodTipoDependencia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoDependenciaByCodTipoDependencia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoDependencia = null;
    this.findNombre = null;

    return null;
  }

  public String findTipoDependenciaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findTipoDependenciaByNombre(this.findNombre);
      this.showTipoDependenciaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoDependenciaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoDependencia = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowTipoDependenciaByCodTipoDependencia() {
    return this.showTipoDependenciaByCodTipoDependencia;
  }
  public boolean isShowTipoDependenciaByNombre() {
    return this.showTipoDependenciaByNombre;
  }

  public String selectTipoDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTipoDependencia = 
      Long.parseLong((String)requestParameterMap.get("idTipoDependencia"));
    try
    {
      this.tipoDependencia = 
        this.estructuraFacade.findTipoDependenciaById(
        idTipoDependencia);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.tipoDependencia = null;
    this.showTipoDependenciaByCodTipoDependencia = false;
    this.showTipoDependenciaByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addTipoDependencia(
          this.tipoDependencia);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateTipoDependencia(
          this.tipoDependencia);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteTipoDependencia(
        this.tipoDependencia);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.tipoDependencia = new TipoDependencia();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.tipoDependencia.setIdTipoDependencia(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.TipoDependencia"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.tipoDependencia = new TipoDependencia();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}