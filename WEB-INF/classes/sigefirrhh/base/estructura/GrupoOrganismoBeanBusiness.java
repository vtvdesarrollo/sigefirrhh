package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class GrupoOrganismoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addGrupoOrganismo(GrupoOrganismo grupoOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    GrupoOrganismo grupoOrganismoNew = 
      (GrupoOrganismo)BeanUtils.cloneBean(
      grupoOrganismo);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (grupoOrganismoNew.getOrganismo() != null) {
      grupoOrganismoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        grupoOrganismoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(grupoOrganismoNew);
  }

  public void updateGrupoOrganismo(GrupoOrganismo grupoOrganismo) throws Exception
  {
    GrupoOrganismo grupoOrganismoModify = 
      findGrupoOrganismoById(grupoOrganismo.getIdGrupoOrganismo());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (grupoOrganismo.getOrganismo() != null) {
      grupoOrganismo.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        grupoOrganismo.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(grupoOrganismoModify, grupoOrganismo);
  }

  public void deleteGrupoOrganismo(GrupoOrganismo grupoOrganismo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    GrupoOrganismo grupoOrganismoDelete = 
      findGrupoOrganismoById(grupoOrganismo.getIdGrupoOrganismo());
    pm.deletePersistent(grupoOrganismoDelete);
  }

  public GrupoOrganismo findGrupoOrganismoById(long idGrupoOrganismo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idGrupoOrganismo == pIdGrupoOrganismo";
    Query query = pm.newQuery(GrupoOrganismo.class, filter);

    query.declareParameters("long pIdGrupoOrganismo");

    parameters.put("pIdGrupoOrganismo", new Long(idGrupoOrganismo));

    Collection colGrupoOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colGrupoOrganismo.iterator();
    return (GrupoOrganismo)iterator.next();
  }

  public Collection findGrupoOrganismoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent grupoOrganismoExtent = pm.getExtent(
      GrupoOrganismo.class, true);
    Query query = pm.newQuery(grupoOrganismoExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodGrupoOrganismo(String codGrupoOrganismo, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codGrupoOrganismo == pCodGrupoOrganismo &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(GrupoOrganismo.class, filter);

    query.declareParameters("java.lang.String pCodGrupoOrganismo, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodGrupoOrganismo", new String(codGrupoOrganismo));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colGrupoOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoOrganismo);

    return colGrupoOrganismo;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(GrupoOrganismo.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colGrupoOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoOrganismo);

    return colGrupoOrganismo;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(GrupoOrganismo.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colGrupoOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoOrganismo);

    return colGrupoOrganismo;
  }
}