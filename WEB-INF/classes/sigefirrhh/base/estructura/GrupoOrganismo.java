package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class GrupoOrganismo
  implements Serializable, PersistenceCapable
{
  private long idGrupoOrganismo;
  private String codGrupoOrganismo;
  private String nombre;
  private String nombreCorto;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codGrupoOrganismo", "idGrupoOrganismo", "nombre", "nombreCorto", "organismo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + jdoGetcodGrupoOrganismo(this);
  }

  public String getCodGrupoOrganismo()
  {
    return jdoGetcodGrupoOrganismo(this);
  }

  public long getIdGrupoOrganismo()
  {
    return jdoGetidGrupoOrganismo(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setCodGrupoOrganismo(String string)
  {
    jdoSetcodGrupoOrganismo(this, string);
  }

  public void setIdGrupoOrganismo(long l)
  {
    jdoSetidGrupoOrganismo(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public String getNombreCorto() {
    return jdoGetnombreCorto(this);
  }
  public void setNombreCorto(String nombreCorto) {
    jdoSetnombreCorto(this, nombreCorto);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.GrupoOrganismo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new GrupoOrganismo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    GrupoOrganismo localGrupoOrganismo = new GrupoOrganismo();
    localGrupoOrganismo.jdoFlags = 1;
    localGrupoOrganismo.jdoStateManager = paramStateManager;
    return localGrupoOrganismo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    GrupoOrganismo localGrupoOrganismo = new GrupoOrganismo();
    localGrupoOrganismo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localGrupoOrganismo.jdoFlags = 1;
    localGrupoOrganismo.jdoStateManager = paramStateManager;
    return localGrupoOrganismo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codGrupoOrganismo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idGrupoOrganismo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCorto);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codGrupoOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idGrupoOrganismo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCorto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(GrupoOrganismo paramGrupoOrganismo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramGrupoOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codGrupoOrganismo = paramGrupoOrganismo.codGrupoOrganismo;
      return;
    case 1:
      if (paramGrupoOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.idGrupoOrganismo = paramGrupoOrganismo.idGrupoOrganismo;
      return;
    case 2:
      if (paramGrupoOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramGrupoOrganismo.nombre;
      return;
    case 3:
      if (paramGrupoOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCorto = paramGrupoOrganismo.nombreCorto;
      return;
    case 4:
      if (paramGrupoOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramGrupoOrganismo.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof GrupoOrganismo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    GrupoOrganismo localGrupoOrganismo = (GrupoOrganismo)paramObject;
    if (localGrupoOrganismo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localGrupoOrganismo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new GrupoOrganismoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new GrupoOrganismoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GrupoOrganismoPK))
      throw new IllegalArgumentException("arg1");
    GrupoOrganismoPK localGrupoOrganismoPK = (GrupoOrganismoPK)paramObject;
    localGrupoOrganismoPK.idGrupoOrganismo = this.idGrupoOrganismo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GrupoOrganismoPK))
      throw new IllegalArgumentException("arg1");
    GrupoOrganismoPK localGrupoOrganismoPK = (GrupoOrganismoPK)paramObject;
    this.idGrupoOrganismo = localGrupoOrganismoPK.idGrupoOrganismo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GrupoOrganismoPK))
      throw new IllegalArgumentException("arg2");
    GrupoOrganismoPK localGrupoOrganismoPK = (GrupoOrganismoPK)paramObject;
    localGrupoOrganismoPK.idGrupoOrganismo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GrupoOrganismoPK))
      throw new IllegalArgumentException("arg2");
    GrupoOrganismoPK localGrupoOrganismoPK = (GrupoOrganismoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localGrupoOrganismoPK.idGrupoOrganismo);
  }

  private static final String jdoGetcodGrupoOrganismo(GrupoOrganismo paramGrupoOrganismo)
  {
    if (paramGrupoOrganismo.jdoFlags <= 0)
      return paramGrupoOrganismo.codGrupoOrganismo;
    StateManager localStateManager = paramGrupoOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoOrganismo.codGrupoOrganismo;
    if (localStateManager.isLoaded(paramGrupoOrganismo, jdoInheritedFieldCount + 0))
      return paramGrupoOrganismo.codGrupoOrganismo;
    return localStateManager.getStringField(paramGrupoOrganismo, jdoInheritedFieldCount + 0, paramGrupoOrganismo.codGrupoOrganismo);
  }

  private static final void jdoSetcodGrupoOrganismo(GrupoOrganismo paramGrupoOrganismo, String paramString)
  {
    if (paramGrupoOrganismo.jdoFlags == 0)
    {
      paramGrupoOrganismo.codGrupoOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoOrganismo.codGrupoOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoOrganismo, jdoInheritedFieldCount + 0, paramGrupoOrganismo.codGrupoOrganismo, paramString);
  }

  private static final long jdoGetidGrupoOrganismo(GrupoOrganismo paramGrupoOrganismo)
  {
    return paramGrupoOrganismo.idGrupoOrganismo;
  }

  private static final void jdoSetidGrupoOrganismo(GrupoOrganismo paramGrupoOrganismo, long paramLong)
  {
    StateManager localStateManager = paramGrupoOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoOrganismo.idGrupoOrganismo = paramLong;
      return;
    }
    localStateManager.setLongField(paramGrupoOrganismo, jdoInheritedFieldCount + 1, paramGrupoOrganismo.idGrupoOrganismo, paramLong);
  }

  private static final String jdoGetnombre(GrupoOrganismo paramGrupoOrganismo)
  {
    if (paramGrupoOrganismo.jdoFlags <= 0)
      return paramGrupoOrganismo.nombre;
    StateManager localStateManager = paramGrupoOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoOrganismo.nombre;
    if (localStateManager.isLoaded(paramGrupoOrganismo, jdoInheritedFieldCount + 2))
      return paramGrupoOrganismo.nombre;
    return localStateManager.getStringField(paramGrupoOrganismo, jdoInheritedFieldCount + 2, paramGrupoOrganismo.nombre);
  }

  private static final void jdoSetnombre(GrupoOrganismo paramGrupoOrganismo, String paramString)
  {
    if (paramGrupoOrganismo.jdoFlags == 0)
    {
      paramGrupoOrganismo.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoOrganismo.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoOrganismo, jdoInheritedFieldCount + 2, paramGrupoOrganismo.nombre, paramString);
  }

  private static final String jdoGetnombreCorto(GrupoOrganismo paramGrupoOrganismo)
  {
    if (paramGrupoOrganismo.jdoFlags <= 0)
      return paramGrupoOrganismo.nombreCorto;
    StateManager localStateManager = paramGrupoOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoOrganismo.nombreCorto;
    if (localStateManager.isLoaded(paramGrupoOrganismo, jdoInheritedFieldCount + 3))
      return paramGrupoOrganismo.nombreCorto;
    return localStateManager.getStringField(paramGrupoOrganismo, jdoInheritedFieldCount + 3, paramGrupoOrganismo.nombreCorto);
  }

  private static final void jdoSetnombreCorto(GrupoOrganismo paramGrupoOrganismo, String paramString)
  {
    if (paramGrupoOrganismo.jdoFlags == 0)
    {
      paramGrupoOrganismo.nombreCorto = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoOrganismo.nombreCorto = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoOrganismo, jdoInheritedFieldCount + 3, paramGrupoOrganismo.nombreCorto, paramString);
  }

  private static final Organismo jdoGetorganismo(GrupoOrganismo paramGrupoOrganismo)
  {
    StateManager localStateManager = paramGrupoOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoOrganismo.organismo;
    if (localStateManager.isLoaded(paramGrupoOrganismo, jdoInheritedFieldCount + 4))
      return paramGrupoOrganismo.organismo;
    return (Organismo)localStateManager.getObjectField(paramGrupoOrganismo, jdoInheritedFieldCount + 4, paramGrupoOrganismo.organismo);
  }

  private static final void jdoSetorganismo(GrupoOrganismo paramGrupoOrganismo, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramGrupoOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoOrganismo.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramGrupoOrganismo, jdoInheritedFieldCount + 4, paramGrupoOrganismo.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}