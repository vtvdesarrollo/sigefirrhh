package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class TipoCaracteristicaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TipoCaracteristicaForm.class.getName());
  private TipoCaracteristica tipoCaracteristica;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showTipoCaracteristicaByCodTipoCaracteristica;
  private boolean showTipoCaracteristicaByNombre;
  private String findCodTipoCaracteristica;
  private String findNombre;
  private Object stateResultTipoCaracteristicaByCodTipoCaracteristica = null;

  private Object stateResultTipoCaracteristicaByNombre = null;

  public String getFindCodTipoCaracteristica()
  {
    return this.findCodTipoCaracteristica;
  }
  public void setFindCodTipoCaracteristica(String findCodTipoCaracteristica) {
    this.findCodTipoCaracteristica = findCodTipoCaracteristica;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public TipoCaracteristica getTipoCaracteristica() {
    if (this.tipoCaracteristica == null) {
      this.tipoCaracteristica = new TipoCaracteristica();
    }
    return this.tipoCaracteristica;
  }

  public TipoCaracteristicaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findTipoCaracteristicaByCodTipoCaracteristica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findTipoCaracteristicaByCodTipoCaracteristica(this.findCodTipoCaracteristica);
      this.showTipoCaracteristicaByCodTipoCaracteristica = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoCaracteristicaByCodTipoCaracteristica)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoCaracteristica = null;
    this.findNombre = null;

    return null;
  }

  public String findTipoCaracteristicaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findTipoCaracteristicaByNombre(this.findNombre);
      this.showTipoCaracteristicaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoCaracteristicaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoCaracteristica = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowTipoCaracteristicaByCodTipoCaracteristica() {
    return this.showTipoCaracteristicaByCodTipoCaracteristica;
  }
  public boolean isShowTipoCaracteristicaByNombre() {
    return this.showTipoCaracteristicaByNombre;
  }

  public String selectTipoCaracteristica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTipoCaracteristica = 
      Long.parseLong((String)requestParameterMap.get("idTipoCaracteristica"));
    try
    {
      this.tipoCaracteristica = 
        this.estructuraFacade.findTipoCaracteristicaById(
        idTipoCaracteristica);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.tipoCaracteristica = null;
    this.showTipoCaracteristicaByCodTipoCaracteristica = false;
    this.showTipoCaracteristicaByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addTipoCaracteristica(
          this.tipoCaracteristica);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateTipoCaracteristica(
          this.tipoCaracteristica);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteTipoCaracteristica(
        this.tipoCaracteristica);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.tipoCaracteristica = new TipoCaracteristica();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.tipoCaracteristica.setIdTipoCaracteristica(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.TipoCaracteristica"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.tipoCaracteristica = new TipoCaracteristica();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}