package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;

public class OrganismoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  static Logger log = Logger.getLogger(OrganismoBeanBusiness.class.getName());

  public void addOrganismo(Organismo organismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Organismo organismoNew = 
      (Organismo)BeanUtils.cloneBean(
      organismo);

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (organismoNew.getCiudad() != null) {
      organismoNew.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        organismoNew.getCiudad().getIdCiudad()));
    }
    pm.makePersistent(organismoNew);
  }

  public void updateOrganismo(Organismo organismo) throws Exception
  {
    Organismo organismoModify = 
      findOrganismoById(organismo.getIdOrganismo());

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (organismo.getCiudad() != null) {
      organismo.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        organismo.getCiudad().getIdCiudad()));
    }

    BeanUtils.copyProperties(organismoModify, organismo);
  }

  public void deleteOrganismo(Organismo organismo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Organismo organismoDelete = 
      findOrganismoById(organismo.getIdOrganismo());
    pm.deletePersistent(organismoDelete);
  }

  public Organismo findOrganismoById(long idOrganismo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idOrganismo == pIdOrganismo";
    Query query = pm.newQuery(Organismo.class, filter);

    query.declareParameters("long pIdOrganismo");

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colOrganismo.iterator();
    return (Organismo)iterator.next();
  }

  public Collection findOrganismoAll() throws Exception
  {
    Collection collection = null;
    try {
      PersistenceManager pm = PMThread.getPM();
      Extent organismoExtent = pm.getExtent(
        Organismo.class, true);
      Query query = pm.newQuery(organismoExtent);
      query.setOrdering("codOrganismo ascending");
      collection = new ArrayList((Collection)query.execute());
    }
    catch (Exception e) {
      log.error("Excepcion Obetniendo la Coleccion de Organismos", e);
    }
    return collection;
  }

  public Collection findByCodOrganismo(String codOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codOrganismo == pCodOrganismo";

    Query query = pm.newQuery(Organismo.class, filter);

    query.declareParameters("java.lang.String pCodOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodOrganismo", new String(codOrganismo));

    query.setOrdering("codOrganismo ascending");

    Collection colOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colOrganismo);

    return colOrganismo;
  }

  public Collection findByNombreOrganismo(String nombreOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombreOrganismo.startsWith(pNombreOrganismo)";

    Query query = pm.newQuery(Organismo.class, filter);

    query.declareParameters("java.lang.String pNombreOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombreOrganismo", new String(nombreOrganismo));

    query.setOrdering("codOrganismo ascending");

    Collection colOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colOrganismo);

    return colOrganismo;
  }
}