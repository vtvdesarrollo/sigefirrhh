package sigefirrhh.base.estructura;

import java.io.Serializable;

public class RegionPK
  implements Serializable
{
  public long idRegion;

  public RegionPK()
  {
  }

  public RegionPK(long idRegion)
  {
    this.idRegion = idRegion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RegionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RegionPK thatPK)
  {
    return 
      this.idRegion == thatPK.idRegion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRegion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRegion);
  }
}