package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;

public class OrganismoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(OrganismoForm.class.getName());
  private Organismo organismo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showOrganismoByCodOrganismo;
  private boolean showOrganismoByNombreOrganismo;
  private String findCodOrganismo;
  private String findNombreOrganismo;
  private Collection colPaisForCiudad;
  private Collection colEstadoForCiudad;
  private Collection colCiudad;
  private String selectPaisForCiudad;
  private String selectEstadoForCiudad;
  private String selectCiudad;
  private Object stateResultOrganismoByCodOrganismo = null;

  private Object stateResultOrganismoByNombreOrganismo = null;

  public String getFindCodOrganismo()
  {
    return this.findCodOrganismo;
  }
  public void setFindCodOrganismo(String findCodOrganismo) {
    this.findCodOrganismo = findCodOrganismo;
  }
  public String getFindNombreOrganismo() {
    return this.findNombreOrganismo;
  }
  public void setFindNombreOrganismo(String findNombreOrganismo) {
    this.findNombreOrganismo = findNombreOrganismo;
  }

  public String getSelectPaisForCiudad()
  {
    return this.selectPaisForCiudad;
  }
  public void setSelectPaisForCiudad(String valPaisForCiudad) {
    this.selectPaisForCiudad = valPaisForCiudad;
  }
  public void changePaisForCiudad(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      this.colEstadoForCiudad = null;
      if (idPais > 0L) {
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
      } else {
        this.selectCiudad = null;
        this.organismo.setCiudad(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudad = null;
      this.organismo.setCiudad(
        null);
    }
  }

  public boolean isShowPaisForCiudad() { return this.colPaisForCiudad != null; }

  public String getSelectEstadoForCiudad() {
    return this.selectEstadoForCiudad;
  }
  public void setSelectEstadoForCiudad(String valEstadoForCiudad) {
    this.selectEstadoForCiudad = valEstadoForCiudad;
  }
  public void changeEstadoForCiudad(ValueChangeEvent event) {
    long idEstado = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      if (idEstado > 0L) {
        this.colCiudad = 
          this.ubicacionFacade.findCiudadByEstado(
          idEstado);
      } else {
        this.selectCiudad = null;
        this.organismo.setCiudad(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudad = null;
      this.organismo.setCiudad(
        null);
    }
  }

  public boolean isShowEstadoForCiudad() { return this.colEstadoForCiudad != null; }

  public String getSelectCiudad() {
    return this.selectCiudad;
  }
  public void setSelectCiudad(String valCiudad) {
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    this.organismo.setCiudad(null);
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      if (String.valueOf(ciudad.getIdCiudad()).equals(
        valCiudad)) {
        this.organismo.setCiudad(
          ciudad);
        break;
      }
    }
    this.selectCiudad = valCiudad;
  }
  public boolean isShowCiudad() {
    return this.colCiudad != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public Organismo getOrganismo() {
    if (this.organismo == null) {
      this.organismo = new Organismo();
    }
    return this.organismo;
  }

  public OrganismoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListOrganoRector()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Organismo.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAprobacionMpd() {
    Collection col = new ArrayList();

    Iterator iterEntry = Organismo.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListOrganismoAdscrito() {
    Collection col = new ArrayList();

    Iterator iterEntry = Organismo.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPaisForCiudad() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForCiudad.iterator();
    Pais paisForCiudad = null;
    while (iterator.hasNext()) {
      paisForCiudad = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForCiudad.getIdPais()), 
        paisForCiudad.toString()));
    }
    return col;
  }

  public Collection getColEstadoForCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForCiudad.iterator();
    Estado estadoForCiudad = null;
    while (iterator.hasNext()) {
      estadoForCiudad = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estadoForCiudad.getIdEstado()), 
        estadoForCiudad.toString()));
    }
    return col;
  }

  public Collection getColCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ciudad.getIdCiudad()), 
        ciudad.toString()));
    }
    return col;
  }

  public Collection getListActualizaExpediente() {
    Collection col = new ArrayList();

    Iterator iterEntry = Organismo.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colPaisForCiudad = 
        this.ubicacionFacade.findAllPais();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findOrganismoByCodOrganismo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findOrganismoByCodOrganismo(this.findCodOrganismo);
      this.showOrganismoByCodOrganismo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showOrganismoByCodOrganismo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodOrganismo = null;
    this.findNombreOrganismo = null;

    return null;
  }

  public String findOrganismoByNombreOrganismo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findOrganismoByNombreOrganismo(this.findNombreOrganismo);
      this.showOrganismoByNombreOrganismo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showOrganismoByNombreOrganismo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodOrganismo = null;
    this.findNombreOrganismo = null;

    return null;
  }

  public boolean isShowOrganismoByCodOrganismo() {
    return this.showOrganismoByCodOrganismo;
  }
  public boolean isShowOrganismoByNombreOrganismo() {
    return this.showOrganismoByNombreOrganismo;
  }

  public String selectOrganismo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectCiudad = null;
    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    long idOrganismo = 
      Long.parseLong((String)requestParameterMap.get("idOrganismo"));
    try
    {
      this.organismo = 
        this.estructuraFacade.findOrganismoById(
        idOrganismo);
      if (this.organismo.getCiudad() != null) {
        this.selectCiudad = 
          String.valueOf(this.organismo.getCiudad().getIdCiudad());
      }

      Ciudad ciudad = null;
      Estado estadoForCiudad = null;
      Pais paisForCiudad = null;

      if (this.organismo.getCiudad() != null) {
        long idCiudad = 
          this.organismo.getCiudad().getIdCiudad();
        this.selectCiudad = String.valueOf(idCiudad);
        ciudad = this.ubicacionFacade.findCiudadById(
          idCiudad);
        this.colCiudad = this.ubicacionFacade.findCiudadByEstado(
          ciudad.getEstado().getIdEstado());

        long idEstadoForCiudad = 
          this.organismo.getCiudad().getEstado().getIdEstado();
        this.selectEstadoForCiudad = String.valueOf(idEstadoForCiudad);
        estadoForCiudad = 
          this.ubicacionFacade.findEstadoById(
          idEstadoForCiudad);
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          estadoForCiudad.getPais().getIdPais());

        long idPaisForCiudad = 
          estadoForCiudad.getPais().getIdPais();
        this.selectPaisForCiudad = String.valueOf(idPaisForCiudad);
        paisForCiudad = 
          this.ubicacionFacade.findPaisById(
          idPaisForCiudad);
        this.colPaisForCiudad = 
          this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.organismo = null;
    this.showOrganismoByCodOrganismo = false;
    this.showOrganismoByNombreOrganismo = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addOrganismo(
          this.organismo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateOrganismo(
          this.organismo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteOrganismo(
        this.organismo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.organismo = new Organismo();

    this.selectCiudad = null;

    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.organismo.setIdOrganismo(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.Organismo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.organismo = new Organismo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}