package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class RegionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRegion(Region region)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Region regionNew = 
      (Region)BeanUtils.cloneBean(
      region);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (regionNew.getOrganismo() != null) {
      regionNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        regionNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(regionNew);
  }

  public void updateRegion(Region region) throws Exception
  {
    Region regionModify = 
      findRegionById(region.getIdRegion());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (region.getOrganismo() != null) {
      region.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        region.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(regionModify, region);
  }

  public void deleteRegion(Region region) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Region regionDelete = 
      findRegionById(region.getIdRegion());
    pm.deletePersistent(regionDelete);
  }

  public Region findRegionById(long idRegion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRegion == pIdRegion";
    Query query = pm.newQuery(Region.class, filter);

    query.declareParameters("long pIdRegion");

    parameters.put("pIdRegion", new Long(idRegion));

    Collection colRegion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRegion.iterator();
    return (Region)iterator.next();
  }

  public Collection findRegionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent regionExtent = pm.getExtent(
      Region.class, true);
    Query query = pm.newQuery(regionExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodRegion(String codRegion, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codRegion == pCodRegion &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Region.class, filter);

    query.declareParameters("java.lang.String pCodRegion, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodRegion", new String(codRegion));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colRegion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegion);

    return colRegion;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Region.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colRegion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegion);

    return colRegion;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Region.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colRegion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegion);

    return colRegion;
  }
}