package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ClasificacionDependenciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addClasificacionDependencia(ClasificacionDependencia clasificacionDependencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ClasificacionDependencia clasificacionDependenciaNew = 
      (ClasificacionDependencia)BeanUtils.cloneBean(
      clasificacionDependencia);

    CaracteristicaDependenciaBeanBusiness caracteristicaDependenciaBeanBusiness = new CaracteristicaDependenciaBeanBusiness();

    if (clasificacionDependenciaNew.getCaracteristicaDependencia() != null) {
      clasificacionDependenciaNew.setCaracteristicaDependencia(
        caracteristicaDependenciaBeanBusiness.findCaracteristicaDependenciaById(
        clasificacionDependenciaNew.getCaracteristicaDependencia().getIdCaracteristicaDependencia()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (clasificacionDependenciaNew.getDependencia() != null) {
      clasificacionDependenciaNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        clasificacionDependenciaNew.getDependencia().getIdDependencia()));
    }
    pm.makePersistent(clasificacionDependenciaNew);
  }

  public void updateClasificacionDependencia(ClasificacionDependencia clasificacionDependencia) throws Exception
  {
    ClasificacionDependencia clasificacionDependenciaModify = 
      findClasificacionDependenciaById(clasificacionDependencia.getIdClasificacionDependencia());

    CaracteristicaDependenciaBeanBusiness caracteristicaDependenciaBeanBusiness = new CaracteristicaDependenciaBeanBusiness();

    if (clasificacionDependencia.getCaracteristicaDependencia() != null) {
      clasificacionDependencia.setCaracteristicaDependencia(
        caracteristicaDependenciaBeanBusiness.findCaracteristicaDependenciaById(
        clasificacionDependencia.getCaracteristicaDependencia().getIdCaracteristicaDependencia()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (clasificacionDependencia.getDependencia() != null) {
      clasificacionDependencia.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        clasificacionDependencia.getDependencia().getIdDependencia()));
    }

    BeanUtils.copyProperties(clasificacionDependenciaModify, clasificacionDependencia);
  }

  public void deleteClasificacionDependencia(ClasificacionDependencia clasificacionDependencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ClasificacionDependencia clasificacionDependenciaDelete = 
      findClasificacionDependenciaById(clasificacionDependencia.getIdClasificacionDependencia());
    pm.deletePersistent(clasificacionDependenciaDelete);
  }

  public ClasificacionDependencia findClasificacionDependenciaById(long idClasificacionDependencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idClasificacionDependencia == pIdClasificacionDependencia";
    Query query = pm.newQuery(ClasificacionDependencia.class, filter);

    query.declareParameters("long pIdClasificacionDependencia");

    parameters.put("pIdClasificacionDependencia", new Long(idClasificacionDependencia));

    Collection colClasificacionDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colClasificacionDependencia.iterator();
    return (ClasificacionDependencia)iterator.next();
  }

  public Collection findClasificacionDependenciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent clasificacionDependenciaExtent = pm.getExtent(
      ClasificacionDependencia.class, true);
    Query query = pm.newQuery(clasificacionDependenciaExtent);
    query.setOrdering("caracteristicaDependencia.codigo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCaracteristicaDependencia(long idCaracteristicaDependencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "caracteristicaDependencia.idCaracteristicaDependencia == pIdCaracteristicaDependencia";

    Query query = pm.newQuery(ClasificacionDependencia.class, filter);

    query.declareParameters("long pIdCaracteristicaDependencia");
    HashMap parameters = new HashMap();

    parameters.put("pIdCaracteristicaDependencia", new Long(idCaracteristicaDependencia));

    query.setOrdering("caracteristicaDependencia.codigo ascending");

    Collection colClasificacionDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colClasificacionDependencia);

    return colClasificacionDependencia;
  }

  public Collection findByDependencia(long idDependencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "dependencia.idDependencia == pIdDependencia";

    Query query = pm.newQuery(ClasificacionDependencia.class, filter);

    query.declareParameters("long pIdDependencia");
    HashMap parameters = new HashMap();

    parameters.put("pIdDependencia", new Long(idDependencia));

    query.setOrdering("caracteristicaDependencia.codigo ascending");

    Collection colClasificacionDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colClasificacionDependencia);

    return colClasificacionDependencia;
  }
}