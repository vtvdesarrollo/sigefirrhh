package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.ubicacion.Ciudad;

public class Organismo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idOrganismo;
  private String codOrganismo;
  private String nombreOrganismo;
  private String nombreCorto;
  private String rif;
  private String nit;
  private String direccion;
  private String zonaPostal;
  private String telefono;
  private String nombreAgenteRetencion;
  private String cedulaAgenteRetencion;
  private String rifAgenteRetencion;
  private String codigoAnteriorMpd;
  private String codSigecof;
  private String codUel;
  private String nombreRrhh;
  private int cedulaRrhh;
  private String telefonoRrhh;
  private String gacetaRrhh;
  private String nombreInformatica;
  private int cedulaInformatica;
  private String telefonoInformatica;
  private String gacetaInformatica;
  private String nombreMaximaAutoridad;
  private int cedulaMaximaAutoridad;
  private String telefonoMaximaAutoridad;
  private String gacetaMaximaAutoridad;
  private String organoRector;
  private String aprobacionMpd;
  private String organismoAdscrito;
  private Ciudad ciudad;
  private String actualizaExpediente;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "actualizaExpediente", "aprobacionMpd", "cedulaAgenteRetencion", "cedulaInformatica", "cedulaMaximaAutoridad", "cedulaRrhh", "ciudad", "codOrganismo", "codSigecof", "codUel", "codigoAnteriorMpd", "direccion", "gacetaInformatica", "gacetaMaximaAutoridad", "gacetaRrhh", "idOrganismo", "nit", "nombreAgenteRetencion", "nombreCorto", "nombreInformatica", "nombreMaximaAutoridad", "nombreOrganismo", "nombreRrhh", "organismoAdscrito", "organoRector", "rif", "rifAgenteRetencion", "telefono", "telefonoInformatica", "telefonoMaximaAutoridad", "telefonoRrhh", "zonaPostal" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Organismo());

    LISTA_SI_NO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public Organismo()
  {
    jdoSetorganoRector(this, "N");

    jdoSetaprobacionMpd(this, "S");

    jdoSetorganismoAdscrito(this, "N");

    jdoSetactualizaExpediente(this, "N");
  }

  public String toString() {
    return jdoGetcodOrganismo(this) + " " + 
      jdoGetnombreOrganismo(this);
  }

  public String getAprobacionMpd()
  {
    return jdoGetaprobacionMpd(this);
  }

  public String getCedulaAgenteRetencion()
  {
    return jdoGetcedulaAgenteRetencion(this);
  }

  public Ciudad getCiudad()
  {
    return jdoGetciudad(this);
  }

  public String getCodOrganismo()
  {
    return jdoGetcodOrganismo(this);
  }

  public String getCodSigecof()
  {
    return jdoGetcodSigecof(this);
  }

  public String getCodUel()
  {
    return jdoGetcodUel(this);
  }

  public String getDireccion()
  {
    return jdoGetdireccion(this);
  }

  public long getIdOrganismo()
  {
    return jdoGetidOrganismo(this);
  }

  public String getNit()
  {
    return jdoGetnit(this);
  }

  public String getNombreAgenteRetencion()
  {
    return jdoGetnombreAgenteRetencion(this);
  }

  public String getOrganoRector()
  {
    return jdoGetorganoRector(this);
  }

  public String getRif()
  {
    return jdoGetrif(this);
  }

  public String getRifAgenteRetencion()
  {
    return jdoGetrifAgenteRetencion(this);
  }

  public String getTelefono()
  {
    return jdoGettelefono(this);
  }

  public String getZonaPostal()
  {
    return jdoGetzonaPostal(this);
  }

  public void setAprobacionMpd(String string)
  {
    jdoSetaprobacionMpd(this, string);
  }

  public void setCedulaAgenteRetencion(String string)
  {
    jdoSetcedulaAgenteRetencion(this, string);
  }

  public void setCiudad(Ciudad ciudad)
  {
    jdoSetciudad(this, ciudad);
  }

  public void setCodOrganismo(String string)
  {
    jdoSetcodOrganismo(this, string);
  }

  public void setCodSigecof(String string)
  {
    jdoSetcodSigecof(this, string);
  }

  public void setCodUel(String string)
  {
    jdoSetcodUel(this, string);
  }

  public void setDireccion(String string)
  {
    jdoSetdireccion(this, string);
  }

  public void setIdOrganismo(long l)
  {
    jdoSetidOrganismo(this, l);
  }

  public void setNit(String string)
  {
    jdoSetnit(this, string);
  }

  public void setNombreAgenteRetencion(String string)
  {
    jdoSetnombreAgenteRetencion(this, string);
  }

  public void setOrganoRector(String string)
  {
    jdoSetorganoRector(this, string);
  }

  public void setRif(String string)
  {
    jdoSetrif(this, string);
  }

  public void setRifAgenteRetencion(String string)
  {
    jdoSetrifAgenteRetencion(this, string);
  }

  public void setTelefono(String string)
  {
    jdoSettelefono(this, string);
  }

  public void setZonaPostal(String string)
  {
    jdoSetzonaPostal(this, string);
  }

  public String getNombreOrganismo() {
    return jdoGetnombreOrganismo(this);
  }

  public void setNombreOrganismo(String string) {
    jdoSetnombreOrganismo(this, string);
  }

  public String getOrganismoAdscrito() {
    return jdoGetorganismoAdscrito(this);
  }

  public void setOrganismoAdscrito(String string) {
    jdoSetorganismoAdscrito(this, string);
  }

  public int getCedulaInformatica()
  {
    return jdoGetcedulaInformatica(this);
  }

  public int getCedulaMaximaAutoridad()
  {
    return jdoGetcedulaMaximaAutoridad(this);
  }

  public int getCedulaRrhh()
  {
    return jdoGetcedulaRrhh(this);
  }

  public String getNombreInformatica()
  {
    return jdoGetnombreInformatica(this);
  }

  public String getNombreMaximaAutoridad()
  {
    return jdoGetnombreMaximaAutoridad(this);
  }

  public String getNombreRrhh()
  {
    return jdoGetnombreRrhh(this);
  }

  public void setCedulaInformatica(int i)
  {
    jdoSetcedulaInformatica(this, i);
  }

  public void setCedulaMaximaAutoridad(int i)
  {
    jdoSetcedulaMaximaAutoridad(this, i);
  }

  public void setCedulaRrhh(int i)
  {
    jdoSetcedulaRrhh(this, i);
  }

  public void setNombreInformatica(String string)
  {
    jdoSetnombreInformatica(this, string);
  }

  public void setNombreMaximaAutoridad(String string)
  {
    jdoSetnombreMaximaAutoridad(this, string);
  }

  public void setNombreRrhh(String string)
  {
    jdoSetnombreRrhh(this, string);
  }

  public String getCodigoAnteriorMpd()
  {
    return jdoGetcodigoAnteriorMpd(this);
  }

  public void setCodigoAnteriorMpd(String string)
  {
    jdoSetcodigoAnteriorMpd(this, string);
  }

  public String getActualizaExpediente()
  {
    return jdoGetactualizaExpediente(this);
  }

  public void setActualizaExpediente(String string)
  {
    jdoSetactualizaExpediente(this, string);
  }

  public String getTelefonoInformatica()
  {
    return jdoGettelefonoInformatica(this);
  }

  public String getTelefonoMaximaAutoridad()
  {
    return jdoGettelefonoMaximaAutoridad(this);
  }

  public void setTelefonoInformatica(String string)
  {
    jdoSettelefonoInformatica(this, string);
  }

  public void setTelefonoMaximaAutoridad(String string)
  {
    jdoSettelefonoMaximaAutoridad(this, string);
  }

  public String getGacetaInformatica()
  {
    return jdoGetgacetaInformatica(this);
  }

  public String getGacetaMaximaAutoridad()
  {
    return jdoGetgacetaMaximaAutoridad(this);
  }

  public String getGacetaRrhh()
  {
    return jdoGetgacetaRrhh(this);
  }

  public void setGacetaInformatica(String string)
  {
    jdoSetgacetaInformatica(this, string);
  }

  public void setGacetaMaximaAutoridad(String string)
  {
    jdoSetgacetaMaximaAutoridad(this, string);
  }

  public void setGacetaRrhh(String string)
  {
    jdoSetgacetaRrhh(this, string);
  }

  public String getTelefonoRrhh()
  {
    return jdoGettelefonoRrhh(this);
  }

  public void setTelefonoRrhh(String string)
  {
    jdoSettelefonoRrhh(this, string);
  }

  public String getNombreCorto() {
    return jdoGetnombreCorto(this);
  }
  public void setNombreCorto(String nombreCorto) {
    jdoSetnombreCorto(this, nombreCorto);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 32;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Organismo localOrganismo = new Organismo();
    localOrganismo.jdoFlags = 1;
    localOrganismo.jdoStateManager = paramStateManager;
    return localOrganismo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Organismo localOrganismo = new Organismo();
    localOrganismo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localOrganismo.jdoFlags = 1;
    localOrganismo.jdoStateManager = paramStateManager;
    return localOrganismo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.actualizaExpediente);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cedulaAgenteRetencion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaInformatica);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaMaximaAutoridad);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaRrhh);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codOrganismo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSigecof);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codUel);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoAnteriorMpd);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.direccion);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gacetaInformatica);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gacetaMaximaAutoridad);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gacetaRrhh);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idOrganismo);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nit);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreAgenteRetencion);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCorto);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreInformatica);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreMaximaAutoridad);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreOrganismo);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreRrhh);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.organismoAdscrito);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.organoRector);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.rif);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.rifAgenteRetencion);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefonoInformatica);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefonoMaximaAutoridad);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefonoRrhh);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.zonaPostal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.actualizaExpediente = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaAgenteRetencion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaInformatica = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaMaximaAutoridad = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaRrhh = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSigecof = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codUel = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoAnteriorMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.direccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gacetaInformatica = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gacetaMaximaAutoridad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gacetaRrhh = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idOrganismo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nit = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreAgenteRetencion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCorto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreInformatica = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreMaximaAutoridad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreRrhh = localStateManager.replacingStringField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismoAdscrito = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organoRector = localStateManager.replacingStringField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.rif = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.rifAgenteRetencion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono = localStateManager.replacingStringField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefonoInformatica = localStateManager.replacingStringField(this, paramInt);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefonoMaximaAutoridad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefonoRrhh = localStateManager.replacingStringField(this, paramInt);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.zonaPostal = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Organismo paramOrganismo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.actualizaExpediente = paramOrganismo.actualizaExpediente;
      return;
    case 1:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramOrganismo.aprobacionMpd;
      return;
    case 2:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaAgenteRetencion = paramOrganismo.cedulaAgenteRetencion;
      return;
    case 3:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaInformatica = paramOrganismo.cedulaInformatica;
      return;
    case 4:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaMaximaAutoridad = paramOrganismo.cedulaMaximaAutoridad;
      return;
    case 5:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaRrhh = paramOrganismo.cedulaRrhh;
      return;
    case 6:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramOrganismo.ciudad;
      return;
    case 7:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codOrganismo = paramOrganismo.codOrganismo;
      return;
    case 8:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codSigecof = paramOrganismo.codSigecof;
      return;
    case 9:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codUel = paramOrganismo.codUel;
      return;
    case 10:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codigoAnteriorMpd = paramOrganismo.codigoAnteriorMpd;
      return;
    case 11:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.direccion = paramOrganismo.direccion;
      return;
    case 12:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.gacetaInformatica = paramOrganismo.gacetaInformatica;
      return;
    case 13:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.gacetaMaximaAutoridad = paramOrganismo.gacetaMaximaAutoridad;
      return;
    case 14:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.gacetaRrhh = paramOrganismo.gacetaRrhh;
      return;
    case 15:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.idOrganismo = paramOrganismo.idOrganismo;
      return;
    case 16:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nit = paramOrganismo.nit;
      return;
    case 17:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreAgenteRetencion = paramOrganismo.nombreAgenteRetencion;
      return;
    case 18:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCorto = paramOrganismo.nombreCorto;
      return;
    case 19:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreInformatica = paramOrganismo.nombreInformatica;
      return;
    case 20:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreMaximaAutoridad = paramOrganismo.nombreMaximaAutoridad;
      return;
    case 21:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreOrganismo = paramOrganismo.nombreOrganismo;
      return;
    case 22:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreRrhh = paramOrganismo.nombreRrhh;
      return;
    case 23:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.organismoAdscrito = paramOrganismo.organismoAdscrito;
      return;
    case 24:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.organoRector = paramOrganismo.organoRector;
      return;
    case 25:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.rif = paramOrganismo.rif;
      return;
    case 26:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.rifAgenteRetencion = paramOrganismo.rifAgenteRetencion;
      return;
    case 27:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.telefono = paramOrganismo.telefono;
      return;
    case 28:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.telefonoInformatica = paramOrganismo.telefonoInformatica;
      return;
    case 29:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.telefonoMaximaAutoridad = paramOrganismo.telefonoMaximaAutoridad;
      return;
    case 30:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.telefonoRrhh = paramOrganismo.telefonoRrhh;
      return;
    case 31:
      if (paramOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.zonaPostal = paramOrganismo.zonaPostal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Organismo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Organismo localOrganismo = (Organismo)paramObject;
    if (localOrganismo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localOrganismo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new OrganismoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new OrganismoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof OrganismoPK))
      throw new IllegalArgumentException("arg1");
    OrganismoPK localOrganismoPK = (OrganismoPK)paramObject;
    localOrganismoPK.idOrganismo = this.idOrganismo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof OrganismoPK))
      throw new IllegalArgumentException("arg1");
    OrganismoPK localOrganismoPK = (OrganismoPK)paramObject;
    this.idOrganismo = localOrganismoPK.idOrganismo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof OrganismoPK))
      throw new IllegalArgumentException("arg2");
    OrganismoPK localOrganismoPK = (OrganismoPK)paramObject;
    localOrganismoPK.idOrganismo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 15);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof OrganismoPK))
      throw new IllegalArgumentException("arg2");
    OrganismoPK localOrganismoPK = (OrganismoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 15, localOrganismoPK.idOrganismo);
  }

  private static final String jdoGetactualizaExpediente(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.actualizaExpediente;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.actualizaExpediente;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 0))
      return paramOrganismo.actualizaExpediente;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 0, paramOrganismo.actualizaExpediente);
  }

  private static final void jdoSetactualizaExpediente(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.actualizaExpediente = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.actualizaExpediente = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 0, paramOrganismo.actualizaExpediente, paramString);
  }

  private static final String jdoGetaprobacionMpd(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.aprobacionMpd;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.aprobacionMpd;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 1))
      return paramOrganismo.aprobacionMpd;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 1, paramOrganismo.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 1, paramOrganismo.aprobacionMpd, paramString);
  }

  private static final String jdoGetcedulaAgenteRetencion(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.cedulaAgenteRetencion;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.cedulaAgenteRetencion;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 2))
      return paramOrganismo.cedulaAgenteRetencion;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 2, paramOrganismo.cedulaAgenteRetencion);
  }

  private static final void jdoSetcedulaAgenteRetencion(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.cedulaAgenteRetencion = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.cedulaAgenteRetencion = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 2, paramOrganismo.cedulaAgenteRetencion, paramString);
  }

  private static final int jdoGetcedulaInformatica(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.cedulaInformatica;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.cedulaInformatica;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 3))
      return paramOrganismo.cedulaInformatica;
    return localStateManager.getIntField(paramOrganismo, jdoInheritedFieldCount + 3, paramOrganismo.cedulaInformatica);
  }

  private static final void jdoSetcedulaInformatica(Organismo paramOrganismo, int paramInt)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.cedulaInformatica = paramInt;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.cedulaInformatica = paramInt;
      return;
    }
    localStateManager.setIntField(paramOrganismo, jdoInheritedFieldCount + 3, paramOrganismo.cedulaInformatica, paramInt);
  }

  private static final int jdoGetcedulaMaximaAutoridad(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.cedulaMaximaAutoridad;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.cedulaMaximaAutoridad;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 4))
      return paramOrganismo.cedulaMaximaAutoridad;
    return localStateManager.getIntField(paramOrganismo, jdoInheritedFieldCount + 4, paramOrganismo.cedulaMaximaAutoridad);
  }

  private static final void jdoSetcedulaMaximaAutoridad(Organismo paramOrganismo, int paramInt)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.cedulaMaximaAutoridad = paramInt;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.cedulaMaximaAutoridad = paramInt;
      return;
    }
    localStateManager.setIntField(paramOrganismo, jdoInheritedFieldCount + 4, paramOrganismo.cedulaMaximaAutoridad, paramInt);
  }

  private static final int jdoGetcedulaRrhh(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.cedulaRrhh;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.cedulaRrhh;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 5))
      return paramOrganismo.cedulaRrhh;
    return localStateManager.getIntField(paramOrganismo, jdoInheritedFieldCount + 5, paramOrganismo.cedulaRrhh);
  }

  private static final void jdoSetcedulaRrhh(Organismo paramOrganismo, int paramInt)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.cedulaRrhh = paramInt;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.cedulaRrhh = paramInt;
      return;
    }
    localStateManager.setIntField(paramOrganismo, jdoInheritedFieldCount + 5, paramOrganismo.cedulaRrhh, paramInt);
  }

  private static final Ciudad jdoGetciudad(Organismo paramOrganismo)
  {
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.ciudad;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 6))
      return paramOrganismo.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramOrganismo, jdoInheritedFieldCount + 6, paramOrganismo.ciudad);
  }

  private static final void jdoSetciudad(Organismo paramOrganismo, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramOrganismo, jdoInheritedFieldCount + 6, paramOrganismo.ciudad, paramCiudad);
  }

  private static final String jdoGetcodOrganismo(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.codOrganismo;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.codOrganismo;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 7))
      return paramOrganismo.codOrganismo;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 7, paramOrganismo.codOrganismo);
  }

  private static final void jdoSetcodOrganismo(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.codOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.codOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 7, paramOrganismo.codOrganismo, paramString);
  }

  private static final String jdoGetcodSigecof(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.codSigecof;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.codSigecof;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 8))
      return paramOrganismo.codSigecof;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 8, paramOrganismo.codSigecof);
  }

  private static final void jdoSetcodSigecof(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.codSigecof = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.codSigecof = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 8, paramOrganismo.codSigecof, paramString);
  }

  private static final String jdoGetcodUel(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.codUel;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.codUel;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 9))
      return paramOrganismo.codUel;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 9, paramOrganismo.codUel);
  }

  private static final void jdoSetcodUel(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.codUel = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.codUel = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 9, paramOrganismo.codUel, paramString);
  }

  private static final String jdoGetcodigoAnteriorMpd(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.codigoAnteriorMpd;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.codigoAnteriorMpd;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 10))
      return paramOrganismo.codigoAnteriorMpd;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 10, paramOrganismo.codigoAnteriorMpd);
  }

  private static final void jdoSetcodigoAnteriorMpd(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.codigoAnteriorMpd = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.codigoAnteriorMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 10, paramOrganismo.codigoAnteriorMpd, paramString);
  }

  private static final String jdoGetdireccion(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.direccion;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.direccion;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 11))
      return paramOrganismo.direccion;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 11, paramOrganismo.direccion);
  }

  private static final void jdoSetdireccion(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.direccion = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.direccion = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 11, paramOrganismo.direccion, paramString);
  }

  private static final String jdoGetgacetaInformatica(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.gacetaInformatica;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.gacetaInformatica;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 12))
      return paramOrganismo.gacetaInformatica;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 12, paramOrganismo.gacetaInformatica);
  }

  private static final void jdoSetgacetaInformatica(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.gacetaInformatica = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.gacetaInformatica = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 12, paramOrganismo.gacetaInformatica, paramString);
  }

  private static final String jdoGetgacetaMaximaAutoridad(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.gacetaMaximaAutoridad;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.gacetaMaximaAutoridad;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 13))
      return paramOrganismo.gacetaMaximaAutoridad;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 13, paramOrganismo.gacetaMaximaAutoridad);
  }

  private static final void jdoSetgacetaMaximaAutoridad(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.gacetaMaximaAutoridad = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.gacetaMaximaAutoridad = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 13, paramOrganismo.gacetaMaximaAutoridad, paramString);
  }

  private static final String jdoGetgacetaRrhh(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.gacetaRrhh;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.gacetaRrhh;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 14))
      return paramOrganismo.gacetaRrhh;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 14, paramOrganismo.gacetaRrhh);
  }

  private static final void jdoSetgacetaRrhh(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.gacetaRrhh = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.gacetaRrhh = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 14, paramOrganismo.gacetaRrhh, paramString);
  }

  private static final long jdoGetidOrganismo(Organismo paramOrganismo)
  {
    return paramOrganismo.idOrganismo;
  }

  private static final void jdoSetidOrganismo(Organismo paramOrganismo, long paramLong)
  {
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.idOrganismo = paramLong;
      return;
    }
    localStateManager.setLongField(paramOrganismo, jdoInheritedFieldCount + 15, paramOrganismo.idOrganismo, paramLong);
  }

  private static final String jdoGetnit(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.nit;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.nit;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 16))
      return paramOrganismo.nit;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 16, paramOrganismo.nit);
  }

  private static final void jdoSetnit(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.nit = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.nit = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 16, paramOrganismo.nit, paramString);
  }

  private static final String jdoGetnombreAgenteRetencion(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.nombreAgenteRetencion;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.nombreAgenteRetencion;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 17))
      return paramOrganismo.nombreAgenteRetencion;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 17, paramOrganismo.nombreAgenteRetencion);
  }

  private static final void jdoSetnombreAgenteRetencion(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.nombreAgenteRetencion = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.nombreAgenteRetencion = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 17, paramOrganismo.nombreAgenteRetencion, paramString);
  }

  private static final String jdoGetnombreCorto(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.nombreCorto;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.nombreCorto;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 18))
      return paramOrganismo.nombreCorto;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 18, paramOrganismo.nombreCorto);
  }

  private static final void jdoSetnombreCorto(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.nombreCorto = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.nombreCorto = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 18, paramOrganismo.nombreCorto, paramString);
  }

  private static final String jdoGetnombreInformatica(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.nombreInformatica;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.nombreInformatica;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 19))
      return paramOrganismo.nombreInformatica;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 19, paramOrganismo.nombreInformatica);
  }

  private static final void jdoSetnombreInformatica(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.nombreInformatica = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.nombreInformatica = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 19, paramOrganismo.nombreInformatica, paramString);
  }

  private static final String jdoGetnombreMaximaAutoridad(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.nombreMaximaAutoridad;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.nombreMaximaAutoridad;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 20))
      return paramOrganismo.nombreMaximaAutoridad;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 20, paramOrganismo.nombreMaximaAutoridad);
  }

  private static final void jdoSetnombreMaximaAutoridad(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.nombreMaximaAutoridad = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.nombreMaximaAutoridad = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 20, paramOrganismo.nombreMaximaAutoridad, paramString);
  }

  private static final String jdoGetnombreOrganismo(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.nombreOrganismo;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.nombreOrganismo;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 21))
      return paramOrganismo.nombreOrganismo;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 21, paramOrganismo.nombreOrganismo);
  }

  private static final void jdoSetnombreOrganismo(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.nombreOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.nombreOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 21, paramOrganismo.nombreOrganismo, paramString);
  }

  private static final String jdoGetnombreRrhh(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.nombreRrhh;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.nombreRrhh;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 22))
      return paramOrganismo.nombreRrhh;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 22, paramOrganismo.nombreRrhh);
  }

  private static final void jdoSetnombreRrhh(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.nombreRrhh = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.nombreRrhh = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 22, paramOrganismo.nombreRrhh, paramString);
  }

  private static final String jdoGetorganismoAdscrito(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.organismoAdscrito;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.organismoAdscrito;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 23))
      return paramOrganismo.organismoAdscrito;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 23, paramOrganismo.organismoAdscrito);
  }

  private static final void jdoSetorganismoAdscrito(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.organismoAdscrito = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.organismoAdscrito = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 23, paramOrganismo.organismoAdscrito, paramString);
  }

  private static final String jdoGetorganoRector(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.organoRector;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.organoRector;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 24))
      return paramOrganismo.organoRector;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 24, paramOrganismo.organoRector);
  }

  private static final void jdoSetorganoRector(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.organoRector = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.organoRector = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 24, paramOrganismo.organoRector, paramString);
  }

  private static final String jdoGetrif(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.rif;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.rif;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 25))
      return paramOrganismo.rif;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 25, paramOrganismo.rif);
  }

  private static final void jdoSetrif(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.rif = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.rif = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 25, paramOrganismo.rif, paramString);
  }

  private static final String jdoGetrifAgenteRetencion(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.rifAgenteRetencion;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.rifAgenteRetencion;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 26))
      return paramOrganismo.rifAgenteRetencion;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 26, paramOrganismo.rifAgenteRetencion);
  }

  private static final void jdoSetrifAgenteRetencion(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.rifAgenteRetencion = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.rifAgenteRetencion = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 26, paramOrganismo.rifAgenteRetencion, paramString);
  }

  private static final String jdoGettelefono(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.telefono;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.telefono;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 27))
      return paramOrganismo.telefono;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 27, paramOrganismo.telefono);
  }

  private static final void jdoSettelefono(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.telefono = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.telefono = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 27, paramOrganismo.telefono, paramString);
  }

  private static final String jdoGettelefonoInformatica(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.telefonoInformatica;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.telefonoInformatica;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 28))
      return paramOrganismo.telefonoInformatica;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 28, paramOrganismo.telefonoInformatica);
  }

  private static final void jdoSettelefonoInformatica(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.telefonoInformatica = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.telefonoInformatica = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 28, paramOrganismo.telefonoInformatica, paramString);
  }

  private static final String jdoGettelefonoMaximaAutoridad(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.telefonoMaximaAutoridad;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.telefonoMaximaAutoridad;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 29))
      return paramOrganismo.telefonoMaximaAutoridad;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 29, paramOrganismo.telefonoMaximaAutoridad);
  }

  private static final void jdoSettelefonoMaximaAutoridad(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.telefonoMaximaAutoridad = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.telefonoMaximaAutoridad = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 29, paramOrganismo.telefonoMaximaAutoridad, paramString);
  }

  private static final String jdoGettelefonoRrhh(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.telefonoRrhh;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.telefonoRrhh;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 30))
      return paramOrganismo.telefonoRrhh;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 30, paramOrganismo.telefonoRrhh);
  }

  private static final void jdoSettelefonoRrhh(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.telefonoRrhh = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.telefonoRrhh = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 30, paramOrganismo.telefonoRrhh, paramString);
  }

  private static final String jdoGetzonaPostal(Organismo paramOrganismo)
  {
    if (paramOrganismo.jdoFlags <= 0)
      return paramOrganismo.zonaPostal;
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramOrganismo.zonaPostal;
    if (localStateManager.isLoaded(paramOrganismo, jdoInheritedFieldCount + 31))
      return paramOrganismo.zonaPostal;
    return localStateManager.getStringField(paramOrganismo, jdoInheritedFieldCount + 31, paramOrganismo.zonaPostal);
  }

  private static final void jdoSetzonaPostal(Organismo paramOrganismo, String paramString)
  {
    if (paramOrganismo.jdoFlags == 0)
    {
      paramOrganismo.zonaPostal = paramString;
      return;
    }
    StateManager localStateManager = paramOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramOrganismo.zonaPostal = paramString;
      return;
    }
    localStateManager.setStringField(paramOrganismo, jdoInheritedFieldCount + 31, paramOrganismo.zonaPostal, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}