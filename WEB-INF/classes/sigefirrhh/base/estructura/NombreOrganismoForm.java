package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class NombreOrganismoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(NombreOrganismoForm.class.getName());
  private NombreOrganismo nombreOrganismo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showNombreOrganismoByNombre;
  private String findNombre;
  private Object stateResultNombreOrganismoByNombre = null;

  public String getFindNombre()
  {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public NombreOrganismo getNombreOrganismo() {
    if (this.nombreOrganismo == null) {
      this.nombreOrganismo = new NombreOrganismo();
    }
    return this.nombreOrganismo;
  }

  public NombreOrganismoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findNombreOrganismoByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findNombreOrganismoByNombre(this.findNombre, idOrganismo);
      this.showNombreOrganismoByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showNombreOrganismoByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findNombre = null;

    return null;
  }

  public boolean isShowNombreOrganismoByNombre() {
    return this.showNombreOrganismoByNombre;
  }

  public String selectNombreOrganismo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idNombreOrganismo = 
      Long.parseLong((String)requestParameterMap.get("idNombreOrganismo"));
    try
    {
      this.nombreOrganismo = 
        this.estructuraFacade.findNombreOrganismoById(
        idNombreOrganismo);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.nombreOrganismo = null;
    this.showNombreOrganismoByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.nombreOrganismo.getFechaVigencia() != null) && 
      (this.nombreOrganismo.getFechaVigencia().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Vigencia no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addNombreOrganismo(
          this.nombreOrganismo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateNombreOrganismo(
          this.nombreOrganismo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteNombreOrganismo(
        this.nombreOrganismo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.nombreOrganismo = new NombreOrganismo();

    this.nombreOrganismo.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.nombreOrganismo.setIdNombreOrganismo(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.NombreOrganismo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.nombreOrganismo = new NombreOrganismo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}