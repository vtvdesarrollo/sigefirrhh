package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Estructura
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idEstructura;
  private String nombre;
  private String aprobacionMpd;
  private int anio;
  private Date fechaVigencia;
  private String vigente;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "aprobacionMpd", "fechaVigencia", "idEstructura", "nombre", "organismo", "vigente" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.Estructura"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Estructura());

    LISTA_SI_NO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public Estructura()
  {
    jdoSetaprobacionMpd(this, "N");

    jdoSetanio(this, 0);

    jdoSetvigente(this, "S");
  }

  public String toString()
  {
    return jdoGetnombre(this) + " - " + jdoGetanio(this);
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }

  public String getAprobacionMpd() {
    return jdoGetaprobacionMpd(this);
  }

  public void setAprobacionMpd(String aprobacionMpd) {
    jdoSetaprobacionMpd(this, aprobacionMpd);
  }

  public Date getFechaVigencia() {
    return jdoGetfechaVigencia(this);
  }

  public void setFechaVigencia(Date fechaVigencia) {
    jdoSetfechaVigencia(this, fechaVigencia);
  }

  public long getIdEstructura() {
    return jdoGetidEstructura(this);
  }

  public void setIdEstructura(long idEstructura) {
    jdoSetidEstructura(this, idEstructura);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }

  public String getVigente() {
    return jdoGetvigente(this);
  }

  public void setVigente(String vigente) {
    jdoSetvigente(this, vigente);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Estructura localEstructura = new Estructura();
    localEstructura.jdoFlags = 1;
    localEstructura.jdoStateManager = paramStateManager;
    return localEstructura;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Estructura localEstructura = new Estructura();
    localEstructura.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEstructura.jdoFlags = 1;
    localEstructura.jdoStateManager = paramStateManager;
    return localEstructura;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEstructura);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigente);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEstructura = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigente = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Estructura paramEstructura, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEstructura == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramEstructura.anio;
      return;
    case 1:
      if (paramEstructura == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramEstructura.aprobacionMpd;
      return;
    case 2:
      if (paramEstructura == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramEstructura.fechaVigencia;
      return;
    case 3:
      if (paramEstructura == null)
        throw new IllegalArgumentException("arg1");
      this.idEstructura = paramEstructura.idEstructura;
      return;
    case 4:
      if (paramEstructura == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramEstructura.nombre;
      return;
    case 5:
      if (paramEstructura == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramEstructura.organismo;
      return;
    case 6:
      if (paramEstructura == null)
        throw new IllegalArgumentException("arg1");
      this.vigente = paramEstructura.vigente;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Estructura))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Estructura localEstructura = (Estructura)paramObject;
    if (localEstructura.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEstructura, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EstructuraPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EstructuraPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EstructuraPK))
      throw new IllegalArgumentException("arg1");
    EstructuraPK localEstructuraPK = (EstructuraPK)paramObject;
    localEstructuraPK.idEstructura = this.idEstructura;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EstructuraPK))
      throw new IllegalArgumentException("arg1");
    EstructuraPK localEstructuraPK = (EstructuraPK)paramObject;
    this.idEstructura = localEstructuraPK.idEstructura;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EstructuraPK))
      throw new IllegalArgumentException("arg2");
    EstructuraPK localEstructuraPK = (EstructuraPK)paramObject;
    localEstructuraPK.idEstructura = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EstructuraPK))
      throw new IllegalArgumentException("arg2");
    EstructuraPK localEstructuraPK = (EstructuraPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localEstructuraPK.idEstructura);
  }

  private static final int jdoGetanio(Estructura paramEstructura)
  {
    if (paramEstructura.jdoFlags <= 0)
      return paramEstructura.anio;
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
      return paramEstructura.anio;
    if (localStateManager.isLoaded(paramEstructura, jdoInheritedFieldCount + 0))
      return paramEstructura.anio;
    return localStateManager.getIntField(paramEstructura, jdoInheritedFieldCount + 0, paramEstructura.anio);
  }

  private static final void jdoSetanio(Estructura paramEstructura, int paramInt)
  {
    if (paramEstructura.jdoFlags == 0)
    {
      paramEstructura.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstructura.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramEstructura, jdoInheritedFieldCount + 0, paramEstructura.anio, paramInt);
  }

  private static final String jdoGetaprobacionMpd(Estructura paramEstructura)
  {
    if (paramEstructura.jdoFlags <= 0)
      return paramEstructura.aprobacionMpd;
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
      return paramEstructura.aprobacionMpd;
    if (localStateManager.isLoaded(paramEstructura, jdoInheritedFieldCount + 1))
      return paramEstructura.aprobacionMpd;
    return localStateManager.getStringField(paramEstructura, jdoInheritedFieldCount + 1, paramEstructura.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(Estructura paramEstructura, String paramString)
  {
    if (paramEstructura.jdoFlags == 0)
    {
      paramEstructura.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstructura.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramEstructura, jdoInheritedFieldCount + 1, paramEstructura.aprobacionMpd, paramString);
  }

  private static final Date jdoGetfechaVigencia(Estructura paramEstructura)
  {
    if (paramEstructura.jdoFlags <= 0)
      return paramEstructura.fechaVigencia;
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
      return paramEstructura.fechaVigencia;
    if (localStateManager.isLoaded(paramEstructura, jdoInheritedFieldCount + 2))
      return paramEstructura.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramEstructura, jdoInheritedFieldCount + 2, paramEstructura.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(Estructura paramEstructura, Date paramDate)
  {
    if (paramEstructura.jdoFlags == 0)
    {
      paramEstructura.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstructura.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramEstructura, jdoInheritedFieldCount + 2, paramEstructura.fechaVigencia, paramDate);
  }

  private static final long jdoGetidEstructura(Estructura paramEstructura)
  {
    return paramEstructura.idEstructura;
  }

  private static final void jdoSetidEstructura(Estructura paramEstructura, long paramLong)
  {
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstructura.idEstructura = paramLong;
      return;
    }
    localStateManager.setLongField(paramEstructura, jdoInheritedFieldCount + 3, paramEstructura.idEstructura, paramLong);
  }

  private static final String jdoGetnombre(Estructura paramEstructura)
  {
    if (paramEstructura.jdoFlags <= 0)
      return paramEstructura.nombre;
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
      return paramEstructura.nombre;
    if (localStateManager.isLoaded(paramEstructura, jdoInheritedFieldCount + 4))
      return paramEstructura.nombre;
    return localStateManager.getStringField(paramEstructura, jdoInheritedFieldCount + 4, paramEstructura.nombre);
  }

  private static final void jdoSetnombre(Estructura paramEstructura, String paramString)
  {
    if (paramEstructura.jdoFlags == 0)
    {
      paramEstructura.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstructura.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramEstructura, jdoInheritedFieldCount + 4, paramEstructura.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(Estructura paramEstructura)
  {
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
      return paramEstructura.organismo;
    if (localStateManager.isLoaded(paramEstructura, jdoInheritedFieldCount + 5))
      return paramEstructura.organismo;
    return (Organismo)localStateManager.getObjectField(paramEstructura, jdoInheritedFieldCount + 5, paramEstructura.organismo);
  }

  private static final void jdoSetorganismo(Estructura paramEstructura, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstructura.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramEstructura, jdoInheritedFieldCount + 5, paramEstructura.organismo, paramOrganismo);
  }

  private static final String jdoGetvigente(Estructura paramEstructura)
  {
    if (paramEstructura.jdoFlags <= 0)
      return paramEstructura.vigente;
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
      return paramEstructura.vigente;
    if (localStateManager.isLoaded(paramEstructura, jdoInheritedFieldCount + 6))
      return paramEstructura.vigente;
    return localStateManager.getStringField(paramEstructura, jdoInheritedFieldCount + 6, paramEstructura.vigente);
  }

  private static final void jdoSetvigente(Estructura paramEstructura, String paramString)
  {
    if (paramEstructura.jdoFlags == 0)
    {
      paramEstructura.vigente = paramString;
      return;
    }
    StateManager localStateManager = paramEstructura.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstructura.vigente = paramString;
      return;
    }
    localStateManager.setStringField(paramEstructura, jdoInheritedFieldCount + 6, paramEstructura.vigente, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}