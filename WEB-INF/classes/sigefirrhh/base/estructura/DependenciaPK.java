package sigefirrhh.base.estructura;

import java.io.Serializable;

public class DependenciaPK
  implements Serializable
{
  public long idDependencia;

  public DependenciaPK()
  {
  }

  public DependenciaPK(long idDependencia)
  {
    this.idDependencia = idDependencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DependenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DependenciaPK thatPK)
  {
    return 
      this.idDependencia == thatPK.idDependencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDependencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDependencia);
  }
}