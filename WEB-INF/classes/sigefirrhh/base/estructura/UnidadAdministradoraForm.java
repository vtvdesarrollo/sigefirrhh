package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class UnidadAdministradoraForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(UnidadAdministradoraForm.class.getName());
  private UnidadAdministradora unidadAdministradora;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showUnidadAdministradoraByCodUnidadAdminist;
  private boolean showUnidadAdministradoraByNombre;
  private String findCodUnidadAdminist;
  private String findNombre;
  private Object stateResultUnidadAdministradoraByCodUnidadAdminist = null;

  private Object stateResultUnidadAdministradoraByNombre = null;

  public String getFindCodUnidadAdminist()
  {
    return this.findCodUnidadAdminist;
  }
  public void setFindCodUnidadAdminist(String findCodUnidadAdminist) {
    this.findCodUnidadAdminist = findCodUnidadAdminist;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public UnidadAdministradora getUnidadAdministradora() {
    if (this.unidadAdministradora == null) {
      this.unidadAdministradora = new UnidadAdministradora();
    }
    return this.unidadAdministradora;
  }

  public UnidadAdministradoraForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListTipoUnidad()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = UnidadAdministradora.LISTA_TIPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListVigente() {
    Collection col = new ArrayList();

    Iterator iterEntry = UnidadAdministradora.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findUnidadAdministradoraByCodUnidadAdminist()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findUnidadAdministradoraByCodUnidadAdminist(this.findCodUnidadAdminist, idOrganismo);
      this.showUnidadAdministradoraByCodUnidadAdminist = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUnidadAdministradoraByCodUnidadAdminist)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodUnidadAdminist = null;
    this.findNombre = null;

    return null;
  }

  public String findUnidadAdministradoraByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findUnidadAdministradoraByNombre(this.findNombre, idOrganismo);
      this.showUnidadAdministradoraByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUnidadAdministradoraByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodUnidadAdminist = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowUnidadAdministradoraByCodUnidadAdminist() {
    return this.showUnidadAdministradoraByCodUnidadAdminist;
  }
  public boolean isShowUnidadAdministradoraByNombre() {
    return this.showUnidadAdministradoraByNombre;
  }

  public String selectUnidadAdministradora()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idUnidadAdministradora = 
      Long.parseLong((String)requestParameterMap.get("idUnidadAdministradora"));
    try
    {
      this.unidadAdministradora = 
        this.estructuraFacade.findUnidadAdministradoraById(
        idUnidadAdministradora);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.unidadAdministradora = null;
    this.showUnidadAdministradoraByCodUnidadAdminist = false;
    this.showUnidadAdministradoraByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addUnidadAdministradora(
          this.unidadAdministradora);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateUnidadAdministradora(
          this.unidadAdministradora);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteUnidadAdministradora(
        this.unidadAdministradora);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.unidadAdministradora = new UnidadAdministradora();

    this.unidadAdministradora.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.unidadAdministradora.setIdUnidadAdministradora(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.UnidadAdministradora"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.unidadAdministradora = new UnidadAdministradora();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}