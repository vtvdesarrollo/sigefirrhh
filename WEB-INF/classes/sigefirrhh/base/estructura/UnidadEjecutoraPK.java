package sigefirrhh.base.estructura;

import java.io.Serializable;

public class UnidadEjecutoraPK
  implements Serializable
{
  public long idUnidadEjecutora;

  public UnidadEjecutoraPK()
  {
  }

  public UnidadEjecutoraPK(long idUnidadEjecutora)
  {
    this.idUnidadEjecutora = idUnidadEjecutora;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UnidadEjecutoraPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UnidadEjecutoraPK thatPK)
  {
    return 
      this.idUnidadEjecutora == thatPK.idUnidadEjecutora;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUnidadEjecutora)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUnidadEjecutora);
  }
}