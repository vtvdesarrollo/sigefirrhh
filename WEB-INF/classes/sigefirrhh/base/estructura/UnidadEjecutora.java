package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class UnidadEjecutora
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idUnidadEjecutora;
  private String codUnidadEjecutora;
  private String nombre;
  private Programa programa;
  private String vigente;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codUnidadEjecutora", "idUnidadEjecutora", "nombre", "programa", "vigente" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Programa"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.UnidadEjecutora"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UnidadEjecutora());

    LISTA_SI_NO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public UnidadEjecutora()
  {
    jdoSetvigente(this, "S");
  }

  public String toString() {
    return jdoGetcodUnidadEjecutora(this) + " - " + jdoGetnombre(this);
  }

  public String getCodUnidadEjecutora()
  {
    return jdoGetcodUnidadEjecutora(this);
  }

  public void setCodUnidadEjecutora(String codUnidadEjecutora)
  {
    jdoSetcodUnidadEjecutora(this, codUnidadEjecutora);
  }

  public long getIdUnidadEjecutora()
  {
    return jdoGetidUnidadEjecutora(this);
  }

  public void setIdUnidadEjecutora(long idUnidadEjecutora)
  {
    jdoSetidUnidadEjecutora(this, idUnidadEjecutora);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public Programa getPrograma()
  {
    return jdoGetprograma(this);
  }

  public void setPrograma(Programa programa)
  {
    jdoSetprograma(this, programa);
  }

  public String getVigente()
  {
    return jdoGetvigente(this);
  }

  public void setVigente(String vigente)
  {
    jdoSetvigente(this, vigente);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UnidadEjecutora localUnidadEjecutora = new UnidadEjecutora();
    localUnidadEjecutora.jdoFlags = 1;
    localUnidadEjecutora.jdoStateManager = paramStateManager;
    return localUnidadEjecutora;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UnidadEjecutora localUnidadEjecutora = new UnidadEjecutora();
    localUnidadEjecutora.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUnidadEjecutora.jdoFlags = 1;
    localUnidadEjecutora.jdoStateManager = paramStateManager;
    return localUnidadEjecutora;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codUnidadEjecutora);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUnidadEjecutora);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.programa);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigente);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codUnidadEjecutora = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUnidadEjecutora = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.programa = ((Programa)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigente = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UnidadEjecutora paramUnidadEjecutora, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUnidadEjecutora == null)
        throw new IllegalArgumentException("arg1");
      this.codUnidadEjecutora = paramUnidadEjecutora.codUnidadEjecutora;
      return;
    case 1:
      if (paramUnidadEjecutora == null)
        throw new IllegalArgumentException("arg1");
      this.idUnidadEjecutora = paramUnidadEjecutora.idUnidadEjecutora;
      return;
    case 2:
      if (paramUnidadEjecutora == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramUnidadEjecutora.nombre;
      return;
    case 3:
      if (paramUnidadEjecutora == null)
        throw new IllegalArgumentException("arg1");
      this.programa = paramUnidadEjecutora.programa;
      return;
    case 4:
      if (paramUnidadEjecutora == null)
        throw new IllegalArgumentException("arg1");
      this.vigente = paramUnidadEjecutora.vigente;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UnidadEjecutora))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UnidadEjecutora localUnidadEjecutora = (UnidadEjecutora)paramObject;
    if (localUnidadEjecutora.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUnidadEjecutora, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UnidadEjecutoraPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UnidadEjecutoraPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UnidadEjecutoraPK))
      throw new IllegalArgumentException("arg1");
    UnidadEjecutoraPK localUnidadEjecutoraPK = (UnidadEjecutoraPK)paramObject;
    localUnidadEjecutoraPK.idUnidadEjecutora = this.idUnidadEjecutora;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UnidadEjecutoraPK))
      throw new IllegalArgumentException("arg1");
    UnidadEjecutoraPK localUnidadEjecutoraPK = (UnidadEjecutoraPK)paramObject;
    this.idUnidadEjecutora = localUnidadEjecutoraPK.idUnidadEjecutora;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UnidadEjecutoraPK))
      throw new IllegalArgumentException("arg2");
    UnidadEjecutoraPK localUnidadEjecutoraPK = (UnidadEjecutoraPK)paramObject;
    localUnidadEjecutoraPK.idUnidadEjecutora = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UnidadEjecutoraPK))
      throw new IllegalArgumentException("arg2");
    UnidadEjecutoraPK localUnidadEjecutoraPK = (UnidadEjecutoraPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localUnidadEjecutoraPK.idUnidadEjecutora);
  }

  private static final String jdoGetcodUnidadEjecutora(UnidadEjecutora paramUnidadEjecutora)
  {
    if (paramUnidadEjecutora.jdoFlags <= 0)
      return paramUnidadEjecutora.codUnidadEjecutora;
    StateManager localStateManager = paramUnidadEjecutora.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadEjecutora.codUnidadEjecutora;
    if (localStateManager.isLoaded(paramUnidadEjecutora, jdoInheritedFieldCount + 0))
      return paramUnidadEjecutora.codUnidadEjecutora;
    return localStateManager.getStringField(paramUnidadEjecutora, jdoInheritedFieldCount + 0, paramUnidadEjecutora.codUnidadEjecutora);
  }

  private static final void jdoSetcodUnidadEjecutora(UnidadEjecutora paramUnidadEjecutora, String paramString)
  {
    if (paramUnidadEjecutora.jdoFlags == 0)
    {
      paramUnidadEjecutora.codUnidadEjecutora = paramString;
      return;
    }
    StateManager localStateManager = paramUnidadEjecutora.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadEjecutora.codUnidadEjecutora = paramString;
      return;
    }
    localStateManager.setStringField(paramUnidadEjecutora, jdoInheritedFieldCount + 0, paramUnidadEjecutora.codUnidadEjecutora, paramString);
  }

  private static final long jdoGetidUnidadEjecutora(UnidadEjecutora paramUnidadEjecutora)
  {
    return paramUnidadEjecutora.idUnidadEjecutora;
  }

  private static final void jdoSetidUnidadEjecutora(UnidadEjecutora paramUnidadEjecutora, long paramLong)
  {
    StateManager localStateManager = paramUnidadEjecutora.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadEjecutora.idUnidadEjecutora = paramLong;
      return;
    }
    localStateManager.setLongField(paramUnidadEjecutora, jdoInheritedFieldCount + 1, paramUnidadEjecutora.idUnidadEjecutora, paramLong);
  }

  private static final String jdoGetnombre(UnidadEjecutora paramUnidadEjecutora)
  {
    if (paramUnidadEjecutora.jdoFlags <= 0)
      return paramUnidadEjecutora.nombre;
    StateManager localStateManager = paramUnidadEjecutora.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadEjecutora.nombre;
    if (localStateManager.isLoaded(paramUnidadEjecutora, jdoInheritedFieldCount + 2))
      return paramUnidadEjecutora.nombre;
    return localStateManager.getStringField(paramUnidadEjecutora, jdoInheritedFieldCount + 2, paramUnidadEjecutora.nombre);
  }

  private static final void jdoSetnombre(UnidadEjecutora paramUnidadEjecutora, String paramString)
  {
    if (paramUnidadEjecutora.jdoFlags == 0)
    {
      paramUnidadEjecutora.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramUnidadEjecutora.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadEjecutora.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramUnidadEjecutora, jdoInheritedFieldCount + 2, paramUnidadEjecutora.nombre, paramString);
  }

  private static final Programa jdoGetprograma(UnidadEjecutora paramUnidadEjecutora)
  {
    StateManager localStateManager = paramUnidadEjecutora.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadEjecutora.programa;
    if (localStateManager.isLoaded(paramUnidadEjecutora, jdoInheritedFieldCount + 3))
      return paramUnidadEjecutora.programa;
    return (Programa)localStateManager.getObjectField(paramUnidadEjecutora, jdoInheritedFieldCount + 3, paramUnidadEjecutora.programa);
  }

  private static final void jdoSetprograma(UnidadEjecutora paramUnidadEjecutora, Programa paramPrograma)
  {
    StateManager localStateManager = paramUnidadEjecutora.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadEjecutora.programa = paramPrograma;
      return;
    }
    localStateManager.setObjectField(paramUnidadEjecutora, jdoInheritedFieldCount + 3, paramUnidadEjecutora.programa, paramPrograma);
  }

  private static final String jdoGetvigente(UnidadEjecutora paramUnidadEjecutora)
  {
    if (paramUnidadEjecutora.jdoFlags <= 0)
      return paramUnidadEjecutora.vigente;
    StateManager localStateManager = paramUnidadEjecutora.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadEjecutora.vigente;
    if (localStateManager.isLoaded(paramUnidadEjecutora, jdoInheritedFieldCount + 4))
      return paramUnidadEjecutora.vigente;
    return localStateManager.getStringField(paramUnidadEjecutora, jdoInheritedFieldCount + 4, paramUnidadEjecutora.vigente);
  }

  private static final void jdoSetvigente(UnidadEjecutora paramUnidadEjecutora, String paramString)
  {
    if (paramUnidadEjecutora.jdoFlags == 0)
    {
      paramUnidadEjecutora.vigente = paramString;
      return;
    }
    StateManager localStateManager = paramUnidadEjecutora.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadEjecutora.vigente = paramString;
      return;
    }
    localStateManager.setStringField(paramUnidadEjecutora, jdoInheritedFieldCount + 4, paramUnidadEjecutora.vigente, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}