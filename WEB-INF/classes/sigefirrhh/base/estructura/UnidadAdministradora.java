package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class UnidadAdministradora
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_TIPO;
  private long idUnidadAdministradora;
  private String codUnidadAdminist;
  private String nombre;
  private String tipoUnidad;
  private Organismo organismo;
  private String vigente;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codUnidadAdminist", "idUnidadAdministradora", "nombre", "organismo", "tipoUnidad", "vigente" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 26, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.UnidadAdministradora"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UnidadAdministradora());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_TIPO.put("O", "ORDENADORA");
    LISTA_TIPO.put("S", "SOLICITANTE");
    LISTA_TIPO.put("A", "AMBAS");
  }

  public UnidadAdministradora()
  {
    jdoSettipoUnidad(this, "A");

    jdoSetvigente(this, "S");
  }
  public String toString() {
    return jdoGetcodUnidadAdminist(this) + " - " + 
      jdoGetnombre(this);
  }

  public String getCodUnidadAdminist()
  {
    return jdoGetcodUnidadAdminist(this);
  }

  public void setCodUnidadAdminist(String codUnidadAdminist) {
    jdoSetcodUnidadAdminist(this, codUnidadAdminist);
  }

  public long getIdUnidadAdministradora() {
    return jdoGetidUnidadAdministradora(this);
  }

  public void setIdUnidadAdministradora(long idUnidadAdministradora) {
    jdoSetidUnidadAdministradora(this, idUnidadAdministradora);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }

  public String getTipoUnidad() {
    return jdoGettipoUnidad(this);
  }

  public void setTipoUnidad(String tipoUnidad) {
    jdoSettipoUnidad(this, tipoUnidad);
  }

  public String getVigente() {
    return jdoGetvigente(this);
  }

  public void setVigente(String vigente) {
    jdoSetvigente(this, vigente);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UnidadAdministradora localUnidadAdministradora = new UnidadAdministradora();
    localUnidadAdministradora.jdoFlags = 1;
    localUnidadAdministradora.jdoStateManager = paramStateManager;
    return localUnidadAdministradora;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UnidadAdministradora localUnidadAdministradora = new UnidadAdministradora();
    localUnidadAdministradora.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUnidadAdministradora.jdoFlags = 1;
    localUnidadAdministradora.jdoStateManager = paramStateManager;
    return localUnidadAdministradora;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codUnidadAdminist);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUnidadAdministradora);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoUnidad);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigente);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codUnidadAdminist = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUnidadAdministradora = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoUnidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigente = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UnidadAdministradora paramUnidadAdministradora, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUnidadAdministradora == null)
        throw new IllegalArgumentException("arg1");
      this.codUnidadAdminist = paramUnidadAdministradora.codUnidadAdminist;
      return;
    case 1:
      if (paramUnidadAdministradora == null)
        throw new IllegalArgumentException("arg1");
      this.idUnidadAdministradora = paramUnidadAdministradora.idUnidadAdministradora;
      return;
    case 2:
      if (paramUnidadAdministradora == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramUnidadAdministradora.nombre;
      return;
    case 3:
      if (paramUnidadAdministradora == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramUnidadAdministradora.organismo;
      return;
    case 4:
      if (paramUnidadAdministradora == null)
        throw new IllegalArgumentException("arg1");
      this.tipoUnidad = paramUnidadAdministradora.tipoUnidad;
      return;
    case 5:
      if (paramUnidadAdministradora == null)
        throw new IllegalArgumentException("arg1");
      this.vigente = paramUnidadAdministradora.vigente;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UnidadAdministradora))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UnidadAdministradora localUnidadAdministradora = (UnidadAdministradora)paramObject;
    if (localUnidadAdministradora.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUnidadAdministradora, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UnidadAdministradoraPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UnidadAdministradoraPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UnidadAdministradoraPK))
      throw new IllegalArgumentException("arg1");
    UnidadAdministradoraPK localUnidadAdministradoraPK = (UnidadAdministradoraPK)paramObject;
    localUnidadAdministradoraPK.idUnidadAdministradora = this.idUnidadAdministradora;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UnidadAdministradoraPK))
      throw new IllegalArgumentException("arg1");
    UnidadAdministradoraPK localUnidadAdministradoraPK = (UnidadAdministradoraPK)paramObject;
    this.idUnidadAdministradora = localUnidadAdministradoraPK.idUnidadAdministradora;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UnidadAdministradoraPK))
      throw new IllegalArgumentException("arg2");
    UnidadAdministradoraPK localUnidadAdministradoraPK = (UnidadAdministradoraPK)paramObject;
    localUnidadAdministradoraPK.idUnidadAdministradora = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UnidadAdministradoraPK))
      throw new IllegalArgumentException("arg2");
    UnidadAdministradoraPK localUnidadAdministradoraPK = (UnidadAdministradoraPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localUnidadAdministradoraPK.idUnidadAdministradora);
  }

  private static final String jdoGetcodUnidadAdminist(UnidadAdministradora paramUnidadAdministradora)
  {
    if (paramUnidadAdministradora.jdoFlags <= 0)
      return paramUnidadAdministradora.codUnidadAdminist;
    StateManager localStateManager = paramUnidadAdministradora.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadAdministradora.codUnidadAdminist;
    if (localStateManager.isLoaded(paramUnidadAdministradora, jdoInheritedFieldCount + 0))
      return paramUnidadAdministradora.codUnidadAdminist;
    return localStateManager.getStringField(paramUnidadAdministradora, jdoInheritedFieldCount + 0, paramUnidadAdministradora.codUnidadAdminist);
  }

  private static final void jdoSetcodUnidadAdminist(UnidadAdministradora paramUnidadAdministradora, String paramString)
  {
    if (paramUnidadAdministradora.jdoFlags == 0)
    {
      paramUnidadAdministradora.codUnidadAdminist = paramString;
      return;
    }
    StateManager localStateManager = paramUnidadAdministradora.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadAdministradora.codUnidadAdminist = paramString;
      return;
    }
    localStateManager.setStringField(paramUnidadAdministradora, jdoInheritedFieldCount + 0, paramUnidadAdministradora.codUnidadAdminist, paramString);
  }

  private static final long jdoGetidUnidadAdministradora(UnidadAdministradora paramUnidadAdministradora)
  {
    return paramUnidadAdministradora.idUnidadAdministradora;
  }

  private static final void jdoSetidUnidadAdministradora(UnidadAdministradora paramUnidadAdministradora, long paramLong)
  {
    StateManager localStateManager = paramUnidadAdministradora.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadAdministradora.idUnidadAdministradora = paramLong;
      return;
    }
    localStateManager.setLongField(paramUnidadAdministradora, jdoInheritedFieldCount + 1, paramUnidadAdministradora.idUnidadAdministradora, paramLong);
  }

  private static final String jdoGetnombre(UnidadAdministradora paramUnidadAdministradora)
  {
    if (paramUnidadAdministradora.jdoFlags <= 0)
      return paramUnidadAdministradora.nombre;
    StateManager localStateManager = paramUnidadAdministradora.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadAdministradora.nombre;
    if (localStateManager.isLoaded(paramUnidadAdministradora, jdoInheritedFieldCount + 2))
      return paramUnidadAdministradora.nombre;
    return localStateManager.getStringField(paramUnidadAdministradora, jdoInheritedFieldCount + 2, paramUnidadAdministradora.nombre);
  }

  private static final void jdoSetnombre(UnidadAdministradora paramUnidadAdministradora, String paramString)
  {
    if (paramUnidadAdministradora.jdoFlags == 0)
    {
      paramUnidadAdministradora.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramUnidadAdministradora.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadAdministradora.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramUnidadAdministradora, jdoInheritedFieldCount + 2, paramUnidadAdministradora.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(UnidadAdministradora paramUnidadAdministradora)
  {
    StateManager localStateManager = paramUnidadAdministradora.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadAdministradora.organismo;
    if (localStateManager.isLoaded(paramUnidadAdministradora, jdoInheritedFieldCount + 3))
      return paramUnidadAdministradora.organismo;
    return (Organismo)localStateManager.getObjectField(paramUnidadAdministradora, jdoInheritedFieldCount + 3, paramUnidadAdministradora.organismo);
  }

  private static final void jdoSetorganismo(UnidadAdministradora paramUnidadAdministradora, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramUnidadAdministradora.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadAdministradora.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramUnidadAdministradora, jdoInheritedFieldCount + 3, paramUnidadAdministradora.organismo, paramOrganismo);
  }

  private static final String jdoGettipoUnidad(UnidadAdministradora paramUnidadAdministradora)
  {
    if (paramUnidadAdministradora.jdoFlags <= 0)
      return paramUnidadAdministradora.tipoUnidad;
    StateManager localStateManager = paramUnidadAdministradora.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadAdministradora.tipoUnidad;
    if (localStateManager.isLoaded(paramUnidadAdministradora, jdoInheritedFieldCount + 4))
      return paramUnidadAdministradora.tipoUnidad;
    return localStateManager.getStringField(paramUnidadAdministradora, jdoInheritedFieldCount + 4, paramUnidadAdministradora.tipoUnidad);
  }

  private static final void jdoSettipoUnidad(UnidadAdministradora paramUnidadAdministradora, String paramString)
  {
    if (paramUnidadAdministradora.jdoFlags == 0)
    {
      paramUnidadAdministradora.tipoUnidad = paramString;
      return;
    }
    StateManager localStateManager = paramUnidadAdministradora.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadAdministradora.tipoUnidad = paramString;
      return;
    }
    localStateManager.setStringField(paramUnidadAdministradora, jdoInheritedFieldCount + 4, paramUnidadAdministradora.tipoUnidad, paramString);
  }

  private static final String jdoGetvigente(UnidadAdministradora paramUnidadAdministradora)
  {
    if (paramUnidadAdministradora.jdoFlags <= 0)
      return paramUnidadAdministradora.vigente;
    StateManager localStateManager = paramUnidadAdministradora.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadAdministradora.vigente;
    if (localStateManager.isLoaded(paramUnidadAdministradora, jdoInheritedFieldCount + 5))
      return paramUnidadAdministradora.vigente;
    return localStateManager.getStringField(paramUnidadAdministradora, jdoInheritedFieldCount + 5, paramUnidadAdministradora.vigente);
  }

  private static final void jdoSetvigente(UnidadAdministradora paramUnidadAdministradora, String paramString)
  {
    if (paramUnidadAdministradora.jdoFlags == 0)
    {
      paramUnidadAdministradora.vigente = paramString;
      return;
    }
    StateManager localStateManager = paramUnidadAdministradora.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadAdministradora.vigente = paramString;
      return;
    }
    localStateManager.setStringField(paramUnidadAdministradora, jdoInheritedFieldCount + 5, paramUnidadAdministradora.vigente, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}