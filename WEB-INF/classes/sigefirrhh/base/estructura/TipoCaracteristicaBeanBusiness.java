package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoCaracteristicaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoCaracteristica(TipoCaracteristica tipoCaracteristica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoCaracteristica tipoCaracteristicaNew = 
      (TipoCaracteristica)BeanUtils.cloneBean(
      tipoCaracteristica);

    pm.makePersistent(tipoCaracteristicaNew);
  }

  public void updateTipoCaracteristica(TipoCaracteristica tipoCaracteristica) throws Exception
  {
    TipoCaracteristica tipoCaracteristicaModify = 
      findTipoCaracteristicaById(tipoCaracteristica.getIdTipoCaracteristica());

    BeanUtils.copyProperties(tipoCaracteristicaModify, tipoCaracteristica);
  }

  public void deleteTipoCaracteristica(TipoCaracteristica tipoCaracteristica) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoCaracteristica tipoCaracteristicaDelete = 
      findTipoCaracteristicaById(tipoCaracteristica.getIdTipoCaracteristica());
    pm.deletePersistent(tipoCaracteristicaDelete);
  }

  public TipoCaracteristica findTipoCaracteristicaById(long idTipoCaracteristica) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoCaracteristica == pIdTipoCaracteristica";
    Query query = pm.newQuery(TipoCaracteristica.class, filter);

    query.declareParameters("long pIdTipoCaracteristica");

    parameters.put("pIdTipoCaracteristica", new Long(idTipoCaracteristica));

    Collection colTipoCaracteristica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoCaracteristica.iterator();
    return (TipoCaracteristica)iterator.next();
  }

  public Collection findTipoCaracteristicaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoCaracteristicaExtent = pm.getExtent(
      TipoCaracteristica.class, true);
    Query query = pm.newQuery(tipoCaracteristicaExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoCaracteristica(String codTipoCaracteristica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoCaracteristica == pCodTipoCaracteristica";

    Query query = pm.newQuery(TipoCaracteristica.class, filter);

    query.declareParameters("java.lang.String pCodTipoCaracteristica");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoCaracteristica", new String(codTipoCaracteristica));

    query.setOrdering("nombre ascending");

    Collection colTipoCaracteristica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoCaracteristica);

    return colTipoCaracteristica;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(TipoCaracteristica.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colTipoCaracteristica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoCaracteristica);

    return colTipoCaracteristica;
  }
}