package sigefirrhh.base.estructura;

import java.io.Serializable;

public class CaracteristicaDependenciaPK
  implements Serializable
{
  public long idCaracteristicaDependencia;

  public CaracteristicaDependenciaPK()
  {
  }

  public CaracteristicaDependenciaPK(long idCaracteristicaDependencia)
  {
    this.idCaracteristicaDependencia = idCaracteristicaDependencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CaracteristicaDependenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CaracteristicaDependenciaPK thatPK)
  {
    return 
      this.idCaracteristicaDependencia == thatPK.idCaracteristicaDependencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCaracteristicaDependencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCaracteristicaDependencia);
  }
}