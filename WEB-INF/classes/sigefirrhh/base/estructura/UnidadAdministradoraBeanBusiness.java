package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class UnidadAdministradoraBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addUnidadAdministradora(UnidadAdministradora unidadAdministradora)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    UnidadAdministradora unidadAdministradoraNew = 
      (UnidadAdministradora)BeanUtils.cloneBean(
      unidadAdministradora);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (unidadAdministradoraNew.getOrganismo() != null) {
      unidadAdministradoraNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        unidadAdministradoraNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(unidadAdministradoraNew);
  }

  public void updateUnidadAdministradora(UnidadAdministradora unidadAdministradora) throws Exception
  {
    UnidadAdministradora unidadAdministradoraModify = 
      findUnidadAdministradoraById(unidadAdministradora.getIdUnidadAdministradora());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (unidadAdministradora.getOrganismo() != null) {
      unidadAdministradora.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        unidadAdministradora.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(unidadAdministradoraModify, unidadAdministradora);
  }

  public void deleteUnidadAdministradora(UnidadAdministradora unidadAdministradora) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    UnidadAdministradora unidadAdministradoraDelete = 
      findUnidadAdministradoraById(unidadAdministradora.getIdUnidadAdministradora());
    pm.deletePersistent(unidadAdministradoraDelete);
  }

  public UnidadAdministradora findUnidadAdministradoraById(long idUnidadAdministradora) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idUnidadAdministradora == pIdUnidadAdministradora";
    Query query = pm.newQuery(UnidadAdministradora.class, filter);

    query.declareParameters("long pIdUnidadAdministradora");

    parameters.put("pIdUnidadAdministradora", new Long(idUnidadAdministradora));

    Collection colUnidadAdministradora = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colUnidadAdministradora.iterator();
    return (UnidadAdministradora)iterator.next();
  }

  public Collection findUnidadAdministradoraAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent unidadAdministradoraExtent = pm.getExtent(
      UnidadAdministradora.class, true);
    Query query = pm.newQuery(unidadAdministradoraExtent);
    query.setOrdering("codUnidadAdminist ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodUnidadAdminist(String codUnidadAdminist, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codUnidadAdminist == pCodUnidadAdminist &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(UnidadAdministradora.class, filter);

    query.declareParameters("java.lang.String pCodUnidadAdminist, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodUnidadAdminist", new String(codUnidadAdminist));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codUnidadAdminist ascending");

    Collection colUnidadAdministradora = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUnidadAdministradora);

    return colUnidadAdministradora;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(UnidadAdministradora.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codUnidadAdminist ascending");

    Collection colUnidadAdministradora = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUnidadAdministradora);

    return colUnidadAdministradora;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(UnidadAdministradora.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codUnidadAdminist ascending");

    Collection colUnidadAdministradora = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUnidadAdministradora);

    return colUnidadAdministradora;
  }
}