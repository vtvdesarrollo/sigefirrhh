package sigefirrhh.base.estructura;

import java.io.Serializable;

public class PuebloIndigenaPK
  implements Serializable
{
  public long idPuebloIndigena;

  public PuebloIndigenaPK()
  {
  }

  public PuebloIndigenaPK(long idPuebloIndigena)
  {
    this.idPuebloIndigena = idPuebloIndigena;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PuebloIndigenaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PuebloIndigenaPK thatPK)
  {
    return 
      this.idPuebloIndigena == thatPK.idPuebloIndigena;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPuebloIndigena)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPuebloIndigena);
  }
}