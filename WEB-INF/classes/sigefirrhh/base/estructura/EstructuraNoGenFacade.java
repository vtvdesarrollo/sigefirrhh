package sigefirrhh.base.estructura;

import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class EstructuraNoGenFacade extends EstructuraFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraNoGenBusiness estructuraNoGenBusiness = new EstructuraNoGenBusiness();

  public Collection findDependenciaByIdOrganismo(long idOrganismo)
    throws Exception
  {
    return this.estructuraNoGenBusiness.findDependenciaByIdOrganismo(idOrganismo);
  }

  public Collection findDependenciaByPersonal(long idPersonal, String estatus)
    throws Exception
  {
    return this.estructuraNoGenBusiness.findDependenciaByPersonal(idPersonal, estatus);
  }

  public Dependencia findDependenciaByCodigoAndRegion(String codDependencia, long idRegion) throws Exception
  {
    try {
      this.txn.open();
      Dependencia dependencia = 
        this.estructuraNoGenBusiness.findDependenciaByCodigoAndRegion(codDependencia, idRegion);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(dependencia);
      return dependencia;
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}