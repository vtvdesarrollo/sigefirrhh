package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.ubicacion.Ciudad;

public class PuebloIndigena
  implements Serializable, PersistenceCapable
{
  private long idPuebloIndigena;
  private String codPuebloIndigena;
  private String descripcion;
  private Ciudad ciudad;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "ciudad", "codPuebloIndigena", "descripcion", "idPuebloIndigena" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + " - " + 
      jdoGetcodPuebloIndigena(this) + " - ";
  }

  public Ciudad getCiudad() {
    return jdoGetciudad(this);
  }

  public void setCiudad(Ciudad ciudad) {
    jdoSetciudad(this, ciudad);
  }

  public String getCodPuebloIndigena() {
    return jdoGetcodPuebloIndigena(this);
  }

  public void setCodPuebloIndigena(String codPuebloIndigena) {
    jdoSetcodPuebloIndigena(this, codPuebloIndigena);
  }

  public String getDescripcion() {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion) {
    jdoSetdescripcion(this, descripcion);
  }

  public long getIdPuebloIndigena() {
    return jdoGetidPuebloIndigena(this);
  }

  public void setIdPuebloIndigena(long idPuebloIndigena) {
    jdoSetidPuebloIndigena(this, idPuebloIndigena);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.PuebloIndigena"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PuebloIndigena());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PuebloIndigena localPuebloIndigena = new PuebloIndigena();
    localPuebloIndigena.jdoFlags = 1;
    localPuebloIndigena.jdoStateManager = paramStateManager;
    return localPuebloIndigena;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PuebloIndigena localPuebloIndigena = new PuebloIndigena();
    localPuebloIndigena.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPuebloIndigena.jdoFlags = 1;
    localPuebloIndigena.jdoStateManager = paramStateManager;
    return localPuebloIndigena;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codPuebloIndigena);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPuebloIndigena);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codPuebloIndigena = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPuebloIndigena = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PuebloIndigena paramPuebloIndigena, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPuebloIndigena == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramPuebloIndigena.ciudad;
      return;
    case 1:
      if (paramPuebloIndigena == null)
        throw new IllegalArgumentException("arg1");
      this.codPuebloIndigena = paramPuebloIndigena.codPuebloIndigena;
      return;
    case 2:
      if (paramPuebloIndigena == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramPuebloIndigena.descripcion;
      return;
    case 3:
      if (paramPuebloIndigena == null)
        throw new IllegalArgumentException("arg1");
      this.idPuebloIndigena = paramPuebloIndigena.idPuebloIndigena;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PuebloIndigena))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PuebloIndigena localPuebloIndigena = (PuebloIndigena)paramObject;
    if (localPuebloIndigena.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPuebloIndigena, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PuebloIndigenaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PuebloIndigenaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PuebloIndigenaPK))
      throw new IllegalArgumentException("arg1");
    PuebloIndigenaPK localPuebloIndigenaPK = (PuebloIndigenaPK)paramObject;
    localPuebloIndigenaPK.idPuebloIndigena = this.idPuebloIndigena;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PuebloIndigenaPK))
      throw new IllegalArgumentException("arg1");
    PuebloIndigenaPK localPuebloIndigenaPK = (PuebloIndigenaPK)paramObject;
    this.idPuebloIndigena = localPuebloIndigenaPK.idPuebloIndigena;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PuebloIndigenaPK))
      throw new IllegalArgumentException("arg2");
    PuebloIndigenaPK localPuebloIndigenaPK = (PuebloIndigenaPK)paramObject;
    localPuebloIndigenaPK.idPuebloIndigena = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PuebloIndigenaPK))
      throw new IllegalArgumentException("arg2");
    PuebloIndigenaPK localPuebloIndigenaPK = (PuebloIndigenaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localPuebloIndigenaPK.idPuebloIndigena);
  }

  private static final Ciudad jdoGetciudad(PuebloIndigena paramPuebloIndigena)
  {
    StateManager localStateManager = paramPuebloIndigena.jdoStateManager;
    if (localStateManager == null)
      return paramPuebloIndigena.ciudad;
    if (localStateManager.isLoaded(paramPuebloIndigena, jdoInheritedFieldCount + 0))
      return paramPuebloIndigena.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramPuebloIndigena, jdoInheritedFieldCount + 0, paramPuebloIndigena.ciudad);
  }

  private static final void jdoSetciudad(PuebloIndigena paramPuebloIndigena, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramPuebloIndigena.jdoStateManager;
    if (localStateManager == null)
    {
      paramPuebloIndigena.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramPuebloIndigena, jdoInheritedFieldCount + 0, paramPuebloIndigena.ciudad, paramCiudad);
  }

  private static final String jdoGetcodPuebloIndigena(PuebloIndigena paramPuebloIndigena)
  {
    if (paramPuebloIndigena.jdoFlags <= 0)
      return paramPuebloIndigena.codPuebloIndigena;
    StateManager localStateManager = paramPuebloIndigena.jdoStateManager;
    if (localStateManager == null)
      return paramPuebloIndigena.codPuebloIndigena;
    if (localStateManager.isLoaded(paramPuebloIndigena, jdoInheritedFieldCount + 1))
      return paramPuebloIndigena.codPuebloIndigena;
    return localStateManager.getStringField(paramPuebloIndigena, jdoInheritedFieldCount + 1, paramPuebloIndigena.codPuebloIndigena);
  }

  private static final void jdoSetcodPuebloIndigena(PuebloIndigena paramPuebloIndigena, String paramString)
  {
    if (paramPuebloIndigena.jdoFlags == 0)
    {
      paramPuebloIndigena.codPuebloIndigena = paramString;
      return;
    }
    StateManager localStateManager = paramPuebloIndigena.jdoStateManager;
    if (localStateManager == null)
    {
      paramPuebloIndigena.codPuebloIndigena = paramString;
      return;
    }
    localStateManager.setStringField(paramPuebloIndigena, jdoInheritedFieldCount + 1, paramPuebloIndigena.codPuebloIndigena, paramString);
  }

  private static final String jdoGetdescripcion(PuebloIndigena paramPuebloIndigena)
  {
    if (paramPuebloIndigena.jdoFlags <= 0)
      return paramPuebloIndigena.descripcion;
    StateManager localStateManager = paramPuebloIndigena.jdoStateManager;
    if (localStateManager == null)
      return paramPuebloIndigena.descripcion;
    if (localStateManager.isLoaded(paramPuebloIndigena, jdoInheritedFieldCount + 2))
      return paramPuebloIndigena.descripcion;
    return localStateManager.getStringField(paramPuebloIndigena, jdoInheritedFieldCount + 2, paramPuebloIndigena.descripcion);
  }

  private static final void jdoSetdescripcion(PuebloIndigena paramPuebloIndigena, String paramString)
  {
    if (paramPuebloIndigena.jdoFlags == 0)
    {
      paramPuebloIndigena.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramPuebloIndigena.jdoStateManager;
    if (localStateManager == null)
    {
      paramPuebloIndigena.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramPuebloIndigena, jdoInheritedFieldCount + 2, paramPuebloIndigena.descripcion, paramString);
  }

  private static final long jdoGetidPuebloIndigena(PuebloIndigena paramPuebloIndigena)
  {
    return paramPuebloIndigena.idPuebloIndigena;
  }

  private static final void jdoSetidPuebloIndigena(PuebloIndigena paramPuebloIndigena, long paramLong)
  {
    StateManager localStateManager = paramPuebloIndigena.jdoStateManager;
    if (localStateManager == null)
    {
      paramPuebloIndigena.idPuebloIndigena = paramLong;
      return;
    }
    localStateManager.setLongField(paramPuebloIndigena, jdoInheritedFieldCount + 3, paramPuebloIndigena.idPuebloIndigena, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}