package sigefirrhh.base.estructura;

import java.io.Serializable;

public class SedePK
  implements Serializable
{
  public long idSede;

  public SedePK()
  {
  }

  public SedePK(long idSede)
  {
    this.idSede = idSede;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SedePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SedePK thatPK)
  {
    return 
      this.idSede == thatPK.idSede;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSede)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSede);
  }
}