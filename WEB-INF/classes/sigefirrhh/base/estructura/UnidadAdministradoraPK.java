package sigefirrhh.base.estructura;

import java.io.Serializable;

public class UnidadAdministradoraPK
  implements Serializable
{
  public long idUnidadAdministradora;

  public UnidadAdministradoraPK()
  {
  }

  public UnidadAdministradoraPK(long idUnidadAdministradora)
  {
    this.idUnidadAdministradora = idUnidadAdministradora;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UnidadAdministradoraPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UnidadAdministradoraPK thatPK)
  {
    return 
      this.idUnidadAdministradora == thatPK.idUnidadAdministradora;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUnidadAdministradora)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUnidadAdministradora);
  }
}