package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Programa
  implements Serializable, PersistenceCapable
{
  private long idPrograma;
  private String codPrograma;
  private String nombre;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codPrograma", "idPrograma", "nombre", "organismo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + jdoGetcodPrograma(this);
  }

  public String getCodPrograma()
  {
    return jdoGetcodPrograma(this);
  }

  public void setCodPrograma(String codPrograma)
  {
    jdoSetcodPrograma(this, codPrograma);
  }

  public long getIdPrograma()
  {
    return jdoGetidPrograma(this);
  }

  public void setIdPrograma(long idPrograma)
  {
    jdoSetidPrograma(this, idPrograma);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.Programa"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Programa());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Programa localPrograma = new Programa();
    localPrograma.jdoFlags = 1;
    localPrograma.jdoStateManager = paramStateManager;
    return localPrograma;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Programa localPrograma = new Programa();
    localPrograma.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPrograma.jdoFlags = 1;
    localPrograma.jdoStateManager = paramStateManager;
    return localPrograma;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codPrograma);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPrograma);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codPrograma = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPrograma = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Programa paramPrograma, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPrograma == null)
        throw new IllegalArgumentException("arg1");
      this.codPrograma = paramPrograma.codPrograma;
      return;
    case 1:
      if (paramPrograma == null)
        throw new IllegalArgumentException("arg1");
      this.idPrograma = paramPrograma.idPrograma;
      return;
    case 2:
      if (paramPrograma == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramPrograma.nombre;
      return;
    case 3:
      if (paramPrograma == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramPrograma.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Programa))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Programa localPrograma = (Programa)paramObject;
    if (localPrograma.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPrograma, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ProgramaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ProgramaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProgramaPK))
      throw new IllegalArgumentException("arg1");
    ProgramaPK localProgramaPK = (ProgramaPK)paramObject;
    localProgramaPK.idPrograma = this.idPrograma;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProgramaPK))
      throw new IllegalArgumentException("arg1");
    ProgramaPK localProgramaPK = (ProgramaPK)paramObject;
    this.idPrograma = localProgramaPK.idPrograma;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProgramaPK))
      throw new IllegalArgumentException("arg2");
    ProgramaPK localProgramaPK = (ProgramaPK)paramObject;
    localProgramaPK.idPrograma = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProgramaPK))
      throw new IllegalArgumentException("arg2");
    ProgramaPK localProgramaPK = (ProgramaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localProgramaPK.idPrograma);
  }

  private static final String jdoGetcodPrograma(Programa paramPrograma)
  {
    if (paramPrograma.jdoFlags <= 0)
      return paramPrograma.codPrograma;
    StateManager localStateManager = paramPrograma.jdoStateManager;
    if (localStateManager == null)
      return paramPrograma.codPrograma;
    if (localStateManager.isLoaded(paramPrograma, jdoInheritedFieldCount + 0))
      return paramPrograma.codPrograma;
    return localStateManager.getStringField(paramPrograma, jdoInheritedFieldCount + 0, paramPrograma.codPrograma);
  }

  private static final void jdoSetcodPrograma(Programa paramPrograma, String paramString)
  {
    if (paramPrograma.jdoFlags == 0)
    {
      paramPrograma.codPrograma = paramString;
      return;
    }
    StateManager localStateManager = paramPrograma.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrograma.codPrograma = paramString;
      return;
    }
    localStateManager.setStringField(paramPrograma, jdoInheritedFieldCount + 0, paramPrograma.codPrograma, paramString);
  }

  private static final long jdoGetidPrograma(Programa paramPrograma)
  {
    return paramPrograma.idPrograma;
  }

  private static final void jdoSetidPrograma(Programa paramPrograma, long paramLong)
  {
    StateManager localStateManager = paramPrograma.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrograma.idPrograma = paramLong;
      return;
    }
    localStateManager.setLongField(paramPrograma, jdoInheritedFieldCount + 1, paramPrograma.idPrograma, paramLong);
  }

  private static final String jdoGetnombre(Programa paramPrograma)
  {
    if (paramPrograma.jdoFlags <= 0)
      return paramPrograma.nombre;
    StateManager localStateManager = paramPrograma.jdoStateManager;
    if (localStateManager == null)
      return paramPrograma.nombre;
    if (localStateManager.isLoaded(paramPrograma, jdoInheritedFieldCount + 2))
      return paramPrograma.nombre;
    return localStateManager.getStringField(paramPrograma, jdoInheritedFieldCount + 2, paramPrograma.nombre);
  }

  private static final void jdoSetnombre(Programa paramPrograma, String paramString)
  {
    if (paramPrograma.jdoFlags == 0)
    {
      paramPrograma.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramPrograma.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrograma.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramPrograma, jdoInheritedFieldCount + 2, paramPrograma.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(Programa paramPrograma)
  {
    StateManager localStateManager = paramPrograma.jdoStateManager;
    if (localStateManager == null)
      return paramPrograma.organismo;
    if (localStateManager.isLoaded(paramPrograma, jdoInheritedFieldCount + 3))
      return paramPrograma.organismo;
    return (Organismo)localStateManager.getObjectField(paramPrograma, jdoInheritedFieldCount + 3, paramPrograma.organismo);
  }

  private static final void jdoSetorganismo(Programa paramPrograma, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramPrograma.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrograma.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramPrograma, jdoInheritedFieldCount + 3, paramPrograma.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}