package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;

public class LugarPagoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addLugarPago(LugarPago lugarPago)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    LugarPago lugarPagoNew = 
      (LugarPago)BeanUtils.cloneBean(
      lugarPago);

    SedeBeanBusiness sedeBeanBusiness = new SedeBeanBusiness();

    if (lugarPagoNew.getSede() != null) {
      lugarPagoNew.setSede(
        sedeBeanBusiness.findSedeById(
        lugarPagoNew.getSede().getIdSede()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (lugarPagoNew.getCiudad() != null) {
      lugarPagoNew.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        lugarPagoNew.getCiudad().getIdCiudad()));
    }
    pm.makePersistent(lugarPagoNew);
  }

  public void updateLugarPago(LugarPago lugarPago) throws Exception
  {
    LugarPago lugarPagoModify = 
      findLugarPagoById(lugarPago.getIdLugarPago());

    SedeBeanBusiness sedeBeanBusiness = new SedeBeanBusiness();

    if (lugarPago.getSede() != null) {
      lugarPago.setSede(
        sedeBeanBusiness.findSedeById(
        lugarPago.getSede().getIdSede()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (lugarPago.getCiudad() != null) {
      lugarPago.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        lugarPago.getCiudad().getIdCiudad()));
    }

    BeanUtils.copyProperties(lugarPagoModify, lugarPago);
  }

  public void deleteLugarPago(LugarPago lugarPago) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    LugarPago lugarPagoDelete = 
      findLugarPagoById(lugarPago.getIdLugarPago());
    pm.deletePersistent(lugarPagoDelete);
  }

  public LugarPago findLugarPagoById(long idLugarPago) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idLugarPago == pIdLugarPago";
    Query query = pm.newQuery(LugarPago.class, filter);

    query.declareParameters("long pIdLugarPago");

    parameters.put("pIdLugarPago", new Long(idLugarPago));

    Collection colLugarPago = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colLugarPago.iterator();
    return (LugarPago)iterator.next();
  }

  public Collection findLugarPagoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent lugarPagoExtent = pm.getExtent(
      LugarPago.class, true);
    Query query = pm.newQuery(lugarPagoExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodLugarPago(String codLugarPago)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codLugarPago == pCodLugarPago";

    Query query = pm.newQuery(LugarPago.class, filter);

    query.declareParameters("java.lang.String pCodLugarPago");
    HashMap parameters = new HashMap();

    parameters.put("pCodLugarPago", new String(codLugarPago));

    query.setOrdering("nombre ascending");

    Collection colLugarPago = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colLugarPago);

    return colLugarPago;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(LugarPago.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colLugarPago = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colLugarPago);

    return colLugarPago;
  }
}