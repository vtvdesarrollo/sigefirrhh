package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.ubicacion.Ciudad;

public class LugarPago
  implements Serializable, PersistenceCapable
{
  private long idLugarPago;
  private String codLugarPago;
  private String nombre;
  private String direccion;
  private String codCesta;
  private Sede sede;
  private Ciudad ciudad;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "ciudad", "codCesta", "codLugarPago", "direccion", "idLugarPago", "nombre", "sede" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Sede") };
  private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + 
      jdoGetcodLugarPago(this) + " - " + 
      jdoGetsede(this).getNombre();
  }

  public Ciudad getCiudad() {
    return jdoGetciudad(this);
  }

  public void setCiudad(Ciudad ciudad) {
    jdoSetciudad(this, ciudad);
  }

  public String getCodCesta() {
    return jdoGetcodCesta(this);
  }

  public void setCodCesta(String codCesta) {
    jdoSetcodCesta(this, codCesta);
  }

  public String getCodLugarPago() {
    return jdoGetcodLugarPago(this);
  }

  public void setCodLugarPago(String codLugarPago) {
    jdoSetcodLugarPago(this, codLugarPago);
  }

  public String getDireccion() {
    return jdoGetdireccion(this);
  }

  public void setDireccion(String direccion) {
    jdoSetdireccion(this, direccion);
  }

  public long getIdLugarPago() {
    return jdoGetidLugarPago(this);
  }

  public void setIdLugarPago(long idLugarPago) {
    jdoSetidLugarPago(this, idLugarPago);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public Sede getSede() {
    return jdoGetsede(this);
  }

  public void setSede(Sede sede) {
    jdoSetsede(this, sede);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.LugarPago"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new LugarPago());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    LugarPago localLugarPago = new LugarPago();
    localLugarPago.jdoFlags = 1;
    localLugarPago.jdoStateManager = paramStateManager;
    return localLugarPago;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    LugarPago localLugarPago = new LugarPago();
    localLugarPago.jdoCopyKeyFieldsFromObjectId(paramObject);
    localLugarPago.jdoFlags = 1;
    localLugarPago.jdoStateManager = paramStateManager;
    return localLugarPago;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCesta);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codLugarPago);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.direccion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idLugarPago);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.sede);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCesta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codLugarPago = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.direccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idLugarPago = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sede = ((Sede)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(LugarPago paramLugarPago, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramLugarPago == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramLugarPago.ciudad;
      return;
    case 1:
      if (paramLugarPago == null)
        throw new IllegalArgumentException("arg1");
      this.codCesta = paramLugarPago.codCesta;
      return;
    case 2:
      if (paramLugarPago == null)
        throw new IllegalArgumentException("arg1");
      this.codLugarPago = paramLugarPago.codLugarPago;
      return;
    case 3:
      if (paramLugarPago == null)
        throw new IllegalArgumentException("arg1");
      this.direccion = paramLugarPago.direccion;
      return;
    case 4:
      if (paramLugarPago == null)
        throw new IllegalArgumentException("arg1");
      this.idLugarPago = paramLugarPago.idLugarPago;
      return;
    case 5:
      if (paramLugarPago == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramLugarPago.nombre;
      return;
    case 6:
      if (paramLugarPago == null)
        throw new IllegalArgumentException("arg1");
      this.sede = paramLugarPago.sede;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof LugarPago))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    LugarPago localLugarPago = (LugarPago)paramObject;
    if (localLugarPago.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localLugarPago, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new LugarPagoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new LugarPagoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof LugarPagoPK))
      throw new IllegalArgumentException("arg1");
    LugarPagoPK localLugarPagoPK = (LugarPagoPK)paramObject;
    localLugarPagoPK.idLugarPago = this.idLugarPago;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof LugarPagoPK))
      throw new IllegalArgumentException("arg1");
    LugarPagoPK localLugarPagoPK = (LugarPagoPK)paramObject;
    this.idLugarPago = localLugarPagoPK.idLugarPago;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof LugarPagoPK))
      throw new IllegalArgumentException("arg2");
    LugarPagoPK localLugarPagoPK = (LugarPagoPK)paramObject;
    localLugarPagoPK.idLugarPago = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof LugarPagoPK))
      throw new IllegalArgumentException("arg2");
    LugarPagoPK localLugarPagoPK = (LugarPagoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localLugarPagoPK.idLugarPago);
  }

  private static final Ciudad jdoGetciudad(LugarPago paramLugarPago)
  {
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
      return paramLugarPago.ciudad;
    if (localStateManager.isLoaded(paramLugarPago, jdoInheritedFieldCount + 0))
      return paramLugarPago.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramLugarPago, jdoInheritedFieldCount + 0, paramLugarPago.ciudad);
  }

  private static final void jdoSetciudad(LugarPago paramLugarPago, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramLugarPago.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramLugarPago, jdoInheritedFieldCount + 0, paramLugarPago.ciudad, paramCiudad);
  }

  private static final String jdoGetcodCesta(LugarPago paramLugarPago)
  {
    if (paramLugarPago.jdoFlags <= 0)
      return paramLugarPago.codCesta;
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
      return paramLugarPago.codCesta;
    if (localStateManager.isLoaded(paramLugarPago, jdoInheritedFieldCount + 1))
      return paramLugarPago.codCesta;
    return localStateManager.getStringField(paramLugarPago, jdoInheritedFieldCount + 1, paramLugarPago.codCesta);
  }

  private static final void jdoSetcodCesta(LugarPago paramLugarPago, String paramString)
  {
    if (paramLugarPago.jdoFlags == 0)
    {
      paramLugarPago.codCesta = paramString;
      return;
    }
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramLugarPago.codCesta = paramString;
      return;
    }
    localStateManager.setStringField(paramLugarPago, jdoInheritedFieldCount + 1, paramLugarPago.codCesta, paramString);
  }

  private static final String jdoGetcodLugarPago(LugarPago paramLugarPago)
  {
    if (paramLugarPago.jdoFlags <= 0)
      return paramLugarPago.codLugarPago;
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
      return paramLugarPago.codLugarPago;
    if (localStateManager.isLoaded(paramLugarPago, jdoInheritedFieldCount + 2))
      return paramLugarPago.codLugarPago;
    return localStateManager.getStringField(paramLugarPago, jdoInheritedFieldCount + 2, paramLugarPago.codLugarPago);
  }

  private static final void jdoSetcodLugarPago(LugarPago paramLugarPago, String paramString)
  {
    if (paramLugarPago.jdoFlags == 0)
    {
      paramLugarPago.codLugarPago = paramString;
      return;
    }
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramLugarPago.codLugarPago = paramString;
      return;
    }
    localStateManager.setStringField(paramLugarPago, jdoInheritedFieldCount + 2, paramLugarPago.codLugarPago, paramString);
  }

  private static final String jdoGetdireccion(LugarPago paramLugarPago)
  {
    if (paramLugarPago.jdoFlags <= 0)
      return paramLugarPago.direccion;
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
      return paramLugarPago.direccion;
    if (localStateManager.isLoaded(paramLugarPago, jdoInheritedFieldCount + 3))
      return paramLugarPago.direccion;
    return localStateManager.getStringField(paramLugarPago, jdoInheritedFieldCount + 3, paramLugarPago.direccion);
  }

  private static final void jdoSetdireccion(LugarPago paramLugarPago, String paramString)
  {
    if (paramLugarPago.jdoFlags == 0)
    {
      paramLugarPago.direccion = paramString;
      return;
    }
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramLugarPago.direccion = paramString;
      return;
    }
    localStateManager.setStringField(paramLugarPago, jdoInheritedFieldCount + 3, paramLugarPago.direccion, paramString);
  }

  private static final long jdoGetidLugarPago(LugarPago paramLugarPago)
  {
    return paramLugarPago.idLugarPago;
  }

  private static final void jdoSetidLugarPago(LugarPago paramLugarPago, long paramLong)
  {
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramLugarPago.idLugarPago = paramLong;
      return;
    }
    localStateManager.setLongField(paramLugarPago, jdoInheritedFieldCount + 4, paramLugarPago.idLugarPago, paramLong);
  }

  private static final String jdoGetnombre(LugarPago paramLugarPago)
  {
    if (paramLugarPago.jdoFlags <= 0)
      return paramLugarPago.nombre;
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
      return paramLugarPago.nombre;
    if (localStateManager.isLoaded(paramLugarPago, jdoInheritedFieldCount + 5))
      return paramLugarPago.nombre;
    return localStateManager.getStringField(paramLugarPago, jdoInheritedFieldCount + 5, paramLugarPago.nombre);
  }

  private static final void jdoSetnombre(LugarPago paramLugarPago, String paramString)
  {
    if (paramLugarPago.jdoFlags == 0)
    {
      paramLugarPago.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramLugarPago.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramLugarPago, jdoInheritedFieldCount + 5, paramLugarPago.nombre, paramString);
  }

  private static final Sede jdoGetsede(LugarPago paramLugarPago)
  {
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
      return paramLugarPago.sede;
    if (localStateManager.isLoaded(paramLugarPago, jdoInheritedFieldCount + 6))
      return paramLugarPago.sede;
    return (Sede)localStateManager.getObjectField(paramLugarPago, jdoInheritedFieldCount + 6, paramLugarPago.sede);
  }

  private static final void jdoSetsede(LugarPago paramLugarPago, Sede paramSede)
  {
    StateManager localStateManager = paramLugarPago.jdoStateManager;
    if (localStateManager == null)
    {
      paramLugarPago.sede = paramSede;
      return;
    }
    localStateManager.setObjectField(paramLugarPago, jdoInheritedFieldCount + 6, paramLugarPago.sede, paramSede);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}