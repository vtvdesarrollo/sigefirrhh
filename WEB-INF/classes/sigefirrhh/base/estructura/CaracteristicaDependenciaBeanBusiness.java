package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CaracteristicaDependenciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCaracteristicaDependencia(CaracteristicaDependencia caracteristicaDependencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CaracteristicaDependencia caracteristicaDependenciaNew = 
      (CaracteristicaDependencia)BeanUtils.cloneBean(
      caracteristicaDependencia);

    TipoCaracteristicaBeanBusiness tipoCaracteristicaBeanBusiness = new TipoCaracteristicaBeanBusiness();

    if (caracteristicaDependenciaNew.getTipoCaracteristica() != null) {
      caracteristicaDependenciaNew.setTipoCaracteristica(
        tipoCaracteristicaBeanBusiness.findTipoCaracteristicaById(
        caracteristicaDependenciaNew.getTipoCaracteristica().getIdTipoCaracteristica()));
    }
    pm.makePersistent(caracteristicaDependenciaNew);
  }

  public void updateCaracteristicaDependencia(CaracteristicaDependencia caracteristicaDependencia) throws Exception
  {
    CaracteristicaDependencia caracteristicaDependenciaModify = 
      findCaracteristicaDependenciaById(caracteristicaDependencia.getIdCaracteristicaDependencia());

    TipoCaracteristicaBeanBusiness tipoCaracteristicaBeanBusiness = new TipoCaracteristicaBeanBusiness();

    if (caracteristicaDependencia.getTipoCaracteristica() != null) {
      caracteristicaDependencia.setTipoCaracteristica(
        tipoCaracteristicaBeanBusiness.findTipoCaracteristicaById(
        caracteristicaDependencia.getTipoCaracteristica().getIdTipoCaracteristica()));
    }

    BeanUtils.copyProperties(caracteristicaDependenciaModify, caracteristicaDependencia);
  }

  public void deleteCaracteristicaDependencia(CaracteristicaDependencia caracteristicaDependencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CaracteristicaDependencia caracteristicaDependenciaDelete = 
      findCaracteristicaDependenciaById(caracteristicaDependencia.getIdCaracteristicaDependencia());
    pm.deletePersistent(caracteristicaDependenciaDelete);
  }

  public CaracteristicaDependencia findCaracteristicaDependenciaById(long idCaracteristicaDependencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCaracteristicaDependencia == pIdCaracteristicaDependencia";
    Query query = pm.newQuery(CaracteristicaDependencia.class, filter);

    query.declareParameters("long pIdCaracteristicaDependencia");

    parameters.put("pIdCaracteristicaDependencia", new Long(idCaracteristicaDependencia));

    Collection colCaracteristicaDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCaracteristicaDependencia.iterator();
    return (CaracteristicaDependencia)iterator.next();
  }

  public Collection findCaracteristicaDependenciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent caracteristicaDependenciaExtent = pm.getExtent(
      CaracteristicaDependencia.class, true);
    Query query = pm.newQuery(caracteristicaDependenciaExtent);
    query.setOrdering("tipoCaracteristica.codTipoCaracteristica ascending, codigo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoCaracteristica(long idTipoCaracteristica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoCaracteristica.idTipoCaracteristica == pIdTipoCaracteristica";

    Query query = pm.newQuery(CaracteristicaDependencia.class, filter);

    query.declareParameters("long pIdTipoCaracteristica");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoCaracteristica", new Long(idTipoCaracteristica));

    query.setOrdering("tipoCaracteristica.codTipoCaracteristica ascending, codigo ascending");

    Collection colCaracteristicaDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCaracteristicaDependencia);

    return colCaracteristicaDependencia;
  }
}