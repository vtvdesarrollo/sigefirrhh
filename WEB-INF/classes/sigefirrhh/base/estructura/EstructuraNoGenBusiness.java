package sigefirrhh.base.estructura;

import java.io.Serializable;
import java.util.Collection;

public class EstructuraNoGenBusiness extends EstructuraBusiness
  implements Serializable
{
  private DependenciaNoGenBeanBusiness dependenciaNoGenBeanBusiness = new DependenciaNoGenBeanBusiness();

  public Collection findDependenciaByIdOrganismo(long idOrganismo)
    throws Exception
  {
    return this.dependenciaNoGenBeanBusiness.findByIdOrganismo(idOrganismo);
  }

  public Collection findDependenciaByPersonal(long idPersonal, String estatus) throws Exception {
    return this.dependenciaNoGenBeanBusiness.findByPersonal(idPersonal, estatus);
  }

  public Dependencia findDependenciaByCodigoAndRegion(String codDependencia, long idRegion) throws Exception {
    return this.dependenciaNoGenBeanBusiness.findByCodigoAndRegion(codDependencia, idRegion);
  }
}