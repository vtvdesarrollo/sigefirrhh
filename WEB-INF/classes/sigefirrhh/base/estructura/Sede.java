package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.ubicacion.Ciudad;

public class Sede
  implements Serializable, PersistenceCapable
{
  private long idSede;
  private Region region;
  private String codSede;
  private String nombre;
  private String direccion;
  private Ciudad ciudad;
  private String codigoPatronal;
  private String codCesta;
  private String regimen;
  private String riesgo;
  private LugarPago lugarPago;
  private Turno turno;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "ciudad", "codCesta", "codSede", "codigoPatronal", "direccion", "idSede", "lugarPago", "nombre", "organismo", "regimen", "region", "riesgo", "turno" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.LugarPago"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Region"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.Turno") };
  private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 21, 24, 26, 21, 26, 21, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Sede()
  {
    jdoSetregimen(this, "I");

    jdoSetriesgo(this, "1");
  }

  public String toString()
  {
    return jdoGetnombre(this) + " - " + jdoGetcodSede(this);
  }

  public Ciudad getCiudad()
  {
    return jdoGetciudad(this);
  }

  public void setCiudad(Ciudad ciudad)
  {
    jdoSetciudad(this, ciudad);
  }

  public String getCodCesta()
  {
    return jdoGetcodCesta(this);
  }

  public void setCodCesta(String codCesta)
  {
    jdoSetcodCesta(this, codCesta);
  }

  public String getCodigoPatronal()
  {
    return jdoGetcodigoPatronal(this);
  }

  public void setCodigoPatronal(String codigoPatronal)
  {
    jdoSetcodigoPatronal(this, codigoPatronal);
  }

  public String getCodSede()
  {
    return jdoGetcodSede(this);
  }

  public void setCodSede(String codSede)
  {
    jdoSetcodSede(this, codSede);
  }

  public String getDireccion()
  {
    return jdoGetdireccion(this);
  }

  public void setDireccion(String direccion)
  {
    jdoSetdireccion(this, direccion);
  }

  public long getIdSede()
  {
    return jdoGetidSede(this);
  }

  public void setIdSede(long idSede)
  {
    jdoSetidSede(this, idSede);
  }

  public LugarPago getLugarPago()
  {
    return jdoGetlugarPago(this);
  }

  public void setLugarPago(LugarPago lugarPago)
  {
    jdoSetlugarPago(this, lugarPago);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public String getRegimen()
  {
    return jdoGetregimen(this);
  }

  public void setRegimen(String regimen)
  {
    jdoSetregimen(this, regimen);
  }

  public Region getRegion()
  {
    return jdoGetregion(this);
  }

  public void setRegion(Region region)
  {
    jdoSetregion(this, region);
  }

  public String getRiesgo()
  {
    return jdoGetriesgo(this);
  }

  public void setRiesgo(String riesgo)
  {
    jdoSetriesgo(this, riesgo);
  }

  public Turno getTurno()
  {
    return jdoGetturno(this);
  }

  public void setTurno(Turno turno)
  {
    jdoSetturno(this, turno);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.Sede"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Sede());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Sede localSede = new Sede();
    localSede.jdoFlags = 1;
    localSede.jdoStateManager = paramStateManager;
    return localSede;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Sede localSede = new Sede();
    localSede.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSede.jdoFlags = 1;
    localSede.jdoStateManager = paramStateManager;
    return localSede;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCesta);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSede);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoPatronal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.direccion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSede);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.lugarPago);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.regimen);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.region);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.riesgo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.turno);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCesta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoPatronal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.direccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSede = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lugarPago = ((LugarPago)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.regimen = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.region = ((Region)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.riesgo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.turno = ((Turno)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Sede paramSede, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramSede.ciudad;
      return;
    case 1:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.codCesta = paramSede.codCesta;
      return;
    case 2:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.codSede = paramSede.codSede;
      return;
    case 3:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.codigoPatronal = paramSede.codigoPatronal;
      return;
    case 4:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.direccion = paramSede.direccion;
      return;
    case 5:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.idSede = paramSede.idSede;
      return;
    case 6:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.lugarPago = paramSede.lugarPago;
      return;
    case 7:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramSede.nombre;
      return;
    case 8:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramSede.organismo;
      return;
    case 9:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.regimen = paramSede.regimen;
      return;
    case 10:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.region = paramSede.region;
      return;
    case 11:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.riesgo = paramSede.riesgo;
      return;
    case 12:
      if (paramSede == null)
        throw new IllegalArgumentException("arg1");
      this.turno = paramSede.turno;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Sede))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Sede localSede = (Sede)paramObject;
    if (localSede.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSede, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SedePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SedePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SedePK))
      throw new IllegalArgumentException("arg1");
    SedePK localSedePK = (SedePK)paramObject;
    localSedePK.idSede = this.idSede;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SedePK))
      throw new IllegalArgumentException("arg1");
    SedePK localSedePK = (SedePK)paramObject;
    this.idSede = localSedePK.idSede;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SedePK))
      throw new IllegalArgumentException("arg2");
    SedePK localSedePK = (SedePK)paramObject;
    localSedePK.idSede = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SedePK))
      throw new IllegalArgumentException("arg2");
    SedePK localSedePK = (SedePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localSedePK.idSede);
  }

  private static final Ciudad jdoGetciudad(Sede paramSede)
  {
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.ciudad;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 0))
      return paramSede.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramSede, jdoInheritedFieldCount + 0, paramSede.ciudad);
  }

  private static final void jdoSetciudad(Sede paramSede, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramSede, jdoInheritedFieldCount + 0, paramSede.ciudad, paramCiudad);
  }

  private static final String jdoGetcodCesta(Sede paramSede)
  {
    if (paramSede.jdoFlags <= 0)
      return paramSede.codCesta;
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.codCesta;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 1))
      return paramSede.codCesta;
    return localStateManager.getStringField(paramSede, jdoInheritedFieldCount + 1, paramSede.codCesta);
  }

  private static final void jdoSetcodCesta(Sede paramSede, String paramString)
  {
    if (paramSede.jdoFlags == 0)
    {
      paramSede.codCesta = paramString;
      return;
    }
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.codCesta = paramString;
      return;
    }
    localStateManager.setStringField(paramSede, jdoInheritedFieldCount + 1, paramSede.codCesta, paramString);
  }

  private static final String jdoGetcodSede(Sede paramSede)
  {
    if (paramSede.jdoFlags <= 0)
      return paramSede.codSede;
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.codSede;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 2))
      return paramSede.codSede;
    return localStateManager.getStringField(paramSede, jdoInheritedFieldCount + 2, paramSede.codSede);
  }

  private static final void jdoSetcodSede(Sede paramSede, String paramString)
  {
    if (paramSede.jdoFlags == 0)
    {
      paramSede.codSede = paramString;
      return;
    }
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.codSede = paramString;
      return;
    }
    localStateManager.setStringField(paramSede, jdoInheritedFieldCount + 2, paramSede.codSede, paramString);
  }

  private static final String jdoGetcodigoPatronal(Sede paramSede)
  {
    if (paramSede.jdoFlags <= 0)
      return paramSede.codigoPatronal;
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.codigoPatronal;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 3))
      return paramSede.codigoPatronal;
    return localStateManager.getStringField(paramSede, jdoInheritedFieldCount + 3, paramSede.codigoPatronal);
  }

  private static final void jdoSetcodigoPatronal(Sede paramSede, String paramString)
  {
    if (paramSede.jdoFlags == 0)
    {
      paramSede.codigoPatronal = paramString;
      return;
    }
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.codigoPatronal = paramString;
      return;
    }
    localStateManager.setStringField(paramSede, jdoInheritedFieldCount + 3, paramSede.codigoPatronal, paramString);
  }

  private static final String jdoGetdireccion(Sede paramSede)
  {
    if (paramSede.jdoFlags <= 0)
      return paramSede.direccion;
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.direccion;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 4))
      return paramSede.direccion;
    return localStateManager.getStringField(paramSede, jdoInheritedFieldCount + 4, paramSede.direccion);
  }

  private static final void jdoSetdireccion(Sede paramSede, String paramString)
  {
    if (paramSede.jdoFlags == 0)
    {
      paramSede.direccion = paramString;
      return;
    }
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.direccion = paramString;
      return;
    }
    localStateManager.setStringField(paramSede, jdoInheritedFieldCount + 4, paramSede.direccion, paramString);
  }

  private static final long jdoGetidSede(Sede paramSede)
  {
    return paramSede.idSede;
  }

  private static final void jdoSetidSede(Sede paramSede, long paramLong)
  {
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.idSede = paramLong;
      return;
    }
    localStateManager.setLongField(paramSede, jdoInheritedFieldCount + 5, paramSede.idSede, paramLong);
  }

  private static final LugarPago jdoGetlugarPago(Sede paramSede)
  {
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.lugarPago;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 6))
      return paramSede.lugarPago;
    return (LugarPago)localStateManager.getObjectField(paramSede, jdoInheritedFieldCount + 6, paramSede.lugarPago);
  }

  private static final void jdoSetlugarPago(Sede paramSede, LugarPago paramLugarPago)
  {
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.lugarPago = paramLugarPago;
      return;
    }
    localStateManager.setObjectField(paramSede, jdoInheritedFieldCount + 6, paramSede.lugarPago, paramLugarPago);
  }

  private static final String jdoGetnombre(Sede paramSede)
  {
    if (paramSede.jdoFlags <= 0)
      return paramSede.nombre;
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.nombre;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 7))
      return paramSede.nombre;
    return localStateManager.getStringField(paramSede, jdoInheritedFieldCount + 7, paramSede.nombre);
  }

  private static final void jdoSetnombre(Sede paramSede, String paramString)
  {
    if (paramSede.jdoFlags == 0)
    {
      paramSede.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramSede, jdoInheritedFieldCount + 7, paramSede.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(Sede paramSede)
  {
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.organismo;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 8))
      return paramSede.organismo;
    return (Organismo)localStateManager.getObjectField(paramSede, jdoInheritedFieldCount + 8, paramSede.organismo);
  }

  private static final void jdoSetorganismo(Sede paramSede, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramSede, jdoInheritedFieldCount + 8, paramSede.organismo, paramOrganismo);
  }

  private static final String jdoGetregimen(Sede paramSede)
  {
    if (paramSede.jdoFlags <= 0)
      return paramSede.regimen;
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.regimen;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 9))
      return paramSede.regimen;
    return localStateManager.getStringField(paramSede, jdoInheritedFieldCount + 9, paramSede.regimen);
  }

  private static final void jdoSetregimen(Sede paramSede, String paramString)
  {
    if (paramSede.jdoFlags == 0)
    {
      paramSede.regimen = paramString;
      return;
    }
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.regimen = paramString;
      return;
    }
    localStateManager.setStringField(paramSede, jdoInheritedFieldCount + 9, paramSede.regimen, paramString);
  }

  private static final Region jdoGetregion(Sede paramSede)
  {
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.region;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 10))
      return paramSede.region;
    return (Region)localStateManager.getObjectField(paramSede, jdoInheritedFieldCount + 10, paramSede.region);
  }

  private static final void jdoSetregion(Sede paramSede, Region paramRegion)
  {
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.region = paramRegion;
      return;
    }
    localStateManager.setObjectField(paramSede, jdoInheritedFieldCount + 10, paramSede.region, paramRegion);
  }

  private static final String jdoGetriesgo(Sede paramSede)
  {
    if (paramSede.jdoFlags <= 0)
      return paramSede.riesgo;
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.riesgo;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 11))
      return paramSede.riesgo;
    return localStateManager.getStringField(paramSede, jdoInheritedFieldCount + 11, paramSede.riesgo);
  }

  private static final void jdoSetriesgo(Sede paramSede, String paramString)
  {
    if (paramSede.jdoFlags == 0)
    {
      paramSede.riesgo = paramString;
      return;
    }
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.riesgo = paramString;
      return;
    }
    localStateManager.setStringField(paramSede, jdoInheritedFieldCount + 11, paramSede.riesgo, paramString);
  }

  private static final Turno jdoGetturno(Sede paramSede)
  {
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
      return paramSede.turno;
    if (localStateManager.isLoaded(paramSede, jdoInheritedFieldCount + 12))
      return paramSede.turno;
    return (Turno)localStateManager.getObjectField(paramSede, jdoInheritedFieldCount + 12, paramSede.turno);
  }

  private static final void jdoSetturno(Sede paramSede, Turno paramTurno)
  {
    StateManager localStateManager = paramSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramSede.turno = paramTurno;
      return;
    }
    localStateManager.setObjectField(paramSede, jdoInheritedFieldCount + 12, paramSede.turno, paramTurno);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}