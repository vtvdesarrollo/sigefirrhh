package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class UnidadFuncionalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(UnidadFuncionalForm.class.getName());
  private UnidadFuncional unidadFuncional;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showUnidadFuncionalByCodUnidadFuncional;
  private boolean showUnidadFuncionalByNombre;
  private String findCodUnidadFuncional;
  private String findNombre;
  private Object stateResultUnidadFuncionalByCodUnidadFuncional = null;

  private Object stateResultUnidadFuncionalByNombre = null;

  public String getFindCodUnidadFuncional()
  {
    return this.findCodUnidadFuncional;
  }
  public void setFindCodUnidadFuncional(String findCodUnidadFuncional) {
    this.findCodUnidadFuncional = findCodUnidadFuncional;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public UnidadFuncional getUnidadFuncional() {
    if (this.unidadFuncional == null) {
      this.unidadFuncional = new UnidadFuncional();
    }
    return this.unidadFuncional;
  }

  public UnidadFuncionalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findUnidadFuncionalByCodUnidadFuncional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findUnidadFuncionalByCodUnidadFuncional(this.findCodUnidadFuncional, idOrganismo);
      this.showUnidadFuncionalByCodUnidadFuncional = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUnidadFuncionalByCodUnidadFuncional)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodUnidadFuncional = null;
    this.findNombre = null;

    return null;
  }

  public String findUnidadFuncionalByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findUnidadFuncionalByNombre(this.findNombre, idOrganismo);
      this.showUnidadFuncionalByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUnidadFuncionalByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodUnidadFuncional = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowUnidadFuncionalByCodUnidadFuncional() {
    return this.showUnidadFuncionalByCodUnidadFuncional;
  }
  public boolean isShowUnidadFuncionalByNombre() {
    return this.showUnidadFuncionalByNombre;
  }

  public String selectUnidadFuncional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idUnidadFuncional = 
      Long.parseLong((String)requestParameterMap.get("idUnidadFuncional"));
    try
    {
      this.unidadFuncional = 
        this.estructuraFacade.findUnidadFuncionalById(
        idUnidadFuncional);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.unidadFuncional = null;
    this.showUnidadFuncionalByCodUnidadFuncional = false;
    this.showUnidadFuncionalByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addUnidadFuncional(
          this.unidadFuncional);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateUnidadFuncional(
          this.unidadFuncional);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteUnidadFuncional(
        this.unidadFuncional);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.unidadFuncional = new UnidadFuncional();

    this.unidadFuncional.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.unidadFuncional.setIdUnidadFuncional(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.UnidadFuncional"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.unidadFuncional = new UnidadFuncional();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}