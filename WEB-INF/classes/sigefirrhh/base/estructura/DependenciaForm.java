package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class DependenciaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(DependenciaForm.class.getName());
  private Dependencia dependencia;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showDependenciaByCodDependencia;
  private boolean showDependenciaByNombre;
  private boolean showDependenciaByEstructura;
  private boolean showDependenciaByAdministradoraUel;
  private boolean showDependenciaByRegion;
  private String findCodDependencia;
  private String findNombre;
  private String findSelectEstructura;
  private String findSelectAdministradoraUel;
  private String findSelectRegion;
  private Collection findColEstructura;
  private Collection findColAdministradoraUel;
  private Collection findColRegion;
  private Collection colEstructura;
  private Collection colAdministradoraUel;
  private Collection colRegion;
  private Collection colSede;
  private Collection colUnidadFuncional;
  private Collection colTipoDependencia;
  private Collection colGrupoOrganismo;
  private Collection colDependenciaAnterior;
  private String selectEstructura;
  private String selectAdministradoraUel;
  private String selectRegion;
  private String selectSede;
  private String selectUnidadFuncional;
  private String selectTipoDependencia;
  private String selectGrupoOrganismo;
  private String selectDependenciaAnterior;
  private Object stateResultDependenciaByCodDependencia = null;

  private Object stateResultDependenciaByNombre = null;

  private Object stateResultDependenciaByEstructura = null;

  private Object stateResultDependenciaByAdministradoraUel = null;

  private Object stateResultDependenciaByRegion = null;

  public String getFindCodDependencia()
  {
    return this.findCodDependencia;
  }
  public void setFindCodDependencia(String findCodDependencia) {
    this.findCodDependencia = findCodDependencia;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }
  public String getFindSelectEstructura() {
    return this.findSelectEstructura;
  }
  public void setFindSelectEstructura(String valEstructura) {
    this.findSelectEstructura = valEstructura;
  }

  public Collection getFindColEstructura() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColEstructura.iterator();
    Estructura estructura = null;
    while (iterator.hasNext()) {
      estructura = (Estructura)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estructura.getIdEstructura()), 
        estructura.toString()));
    }
    return col;
  }
  public String getFindSelectAdministradoraUel() {
    return this.findSelectAdministradoraUel;
  }
  public void setFindSelectAdministradoraUel(String valAdministradoraUel) {
    this.findSelectAdministradoraUel = valAdministradoraUel;
  }

  public Collection getFindColAdministradoraUel() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColAdministradoraUel.iterator();
    AdministradoraUel administradoraUel = null;
    while (iterator.hasNext()) {
      administradoraUel = (AdministradoraUel)iterator.next();
      col.add(new SelectItem(
        String.valueOf(administradoraUel.getIdAdministradoraUel()), 
        administradoraUel.toString()));
    }
    return col;
  }
  public String getFindSelectRegion() {
    return this.findSelectRegion;
  }
  public void setFindSelectRegion(String valRegion) {
    this.findSelectRegion = valRegion;
  }

  public Collection getFindColRegion() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public String getSelectEstructura()
  {
    return this.selectEstructura;
  }
  public void setSelectEstructura(String valEstructura) {
    Iterator iterator = this.colEstructura.iterator();
    Estructura estructura = null;
    this.dependencia.setEstructura(null);
    while (iterator.hasNext()) {
      estructura = (Estructura)iterator.next();
      if (String.valueOf(estructura.getIdEstructura()).equals(
        valEstructura)) {
        this.dependencia.setEstructura(
          estructura);
        break;
      }
    }
    this.selectEstructura = valEstructura;
  }
  public String getSelectAdministradoraUel() {
    return this.selectAdministradoraUel;
  }
  public void setSelectAdministradoraUel(String valAdministradoraUel) {
    Iterator iterator = this.colAdministradoraUel.iterator();
    AdministradoraUel administradoraUel = null;
    this.dependencia.setAdministradoraUel(null);
    while (iterator.hasNext()) {
      administradoraUel = (AdministradoraUel)iterator.next();
      if (String.valueOf(administradoraUel.getIdAdministradoraUel()).equals(
        valAdministradoraUel)) {
        this.dependencia.setAdministradoraUel(
          administradoraUel);
        break;
      }
    }
    this.selectAdministradoraUel = valAdministradoraUel;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }
  public void setSelectRegion(String valRegion) {
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    this.dependencia.setRegion(null);
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      if (String.valueOf(region.getIdRegion()).equals(
        valRegion)) {
        this.dependencia.setRegion(
          region);
        break;
      }
    }
    this.selectRegion = valRegion;
  }
  public String getSelectSede() {
    return this.selectSede;
  }
  public void setSelectSede(String valSede) {
    Iterator iterator = this.colSede.iterator();
    Sede sede = null;
    this.dependencia.setSede(null);
    while (iterator.hasNext()) {
      sede = (Sede)iterator.next();
      if (String.valueOf(sede.getIdSede()).equals(
        valSede)) {
        this.dependencia.setSede(
          sede);
        break;
      }
    }
    this.selectSede = valSede;
  }
  public String getSelectUnidadFuncional() {
    return this.selectUnidadFuncional;
  }
  public void setSelectUnidadFuncional(String valUnidadFuncional) {
    Iterator iterator = this.colUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    this.dependencia.setUnidadFuncional(null);
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      if (String.valueOf(unidadFuncional.getIdUnidadFuncional()).equals(
        valUnidadFuncional)) {
        this.dependencia.setUnidadFuncional(
          unidadFuncional);
        break;
      }
    }
    this.selectUnidadFuncional = valUnidadFuncional;
  }
  public String getSelectTipoDependencia() {
    return this.selectTipoDependencia;
  }
  public void setSelectTipoDependencia(String valTipoDependencia) {
    Iterator iterator = this.colTipoDependencia.iterator();
    TipoDependencia tipoDependencia = null;
    this.dependencia.setTipoDependencia(null);
    while (iterator.hasNext()) {
      tipoDependencia = (TipoDependencia)iterator.next();
      if (String.valueOf(tipoDependencia.getIdTipoDependencia()).equals(
        valTipoDependencia)) {
        this.dependencia.setTipoDependencia(
          tipoDependencia);
        break;
      }
    }
    this.selectTipoDependencia = valTipoDependencia;
  }
  public String getSelectGrupoOrganismo() {
    return this.selectGrupoOrganismo;
  }
  public void setSelectGrupoOrganismo(String valGrupoOrganismo) {
    Iterator iterator = this.colGrupoOrganismo.iterator();
    GrupoOrganismo grupoOrganismo = null;
    this.dependencia.setGrupoOrganismo(null);
    while (iterator.hasNext()) {
      grupoOrganismo = (GrupoOrganismo)iterator.next();
      if (String.valueOf(grupoOrganismo.getIdGrupoOrganismo()).equals(
        valGrupoOrganismo)) {
        this.dependencia.setGrupoOrganismo(
          grupoOrganismo);
        break;
      }
    }
    this.selectGrupoOrganismo = valGrupoOrganismo;
  }
  public String getSelectDependenciaAnterior() {
    return this.selectDependenciaAnterior;
  }
  public void setSelectDependenciaAnterior(String valDependenciaAnterior) {
    Iterator iterator = this.colDependenciaAnterior.iterator();
    Dependencia dependenciaAnterior = null;
    this.dependencia.setDependenciaAnterior(null);
    while (iterator.hasNext()) {
      dependenciaAnterior = (Dependencia)iterator.next();
      if (String.valueOf(dependenciaAnterior.getIdDependencia()).equals(
        valDependenciaAnterior)) {
        this.dependencia.setDependenciaAnterior(
          dependenciaAnterior);
        break;
      }
    }
    this.selectDependenciaAnterior = valDependenciaAnterior;
  }
  public Collection getResult() {
    return this.result;
  }

  public Dependencia getDependencia() {
    if (this.dependencia == null) {
      this.dependencia = new Dependencia();
    }
    return this.dependencia;
  }

  public DependenciaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColEstructura()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstructura.iterator();
    Estructura estructura = null;
    while (iterator.hasNext()) {
      estructura = (Estructura)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estructura.getIdEstructura()), 
        estructura.toString()));
    }
    return col;
  }

  public Collection getColAdministradoraUel()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAdministradoraUel.iterator();
    AdministradoraUel administradoraUel = null;
    while (iterator.hasNext()) {
      administradoraUel = (AdministradoraUel)iterator.next();
      col.add(new SelectItem(
        String.valueOf(administradoraUel.getIdAdministradoraUel()), 
        administradoraUel.toString()));
    }
    return col;
  }

  public Collection getColRegion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public Collection getColSede()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colSede.iterator();
    Sede sede = null;
    while (iterator.hasNext()) {
      sede = (Sede)iterator.next();
      col.add(new SelectItem(
        String.valueOf(sede.getIdSede()), 
        sede.toString()));
    }
    return col;
  }

  public Collection getColUnidadFuncional()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadFuncional.getIdUnidadFuncional()), 
        unidadFuncional.toString()));
    }
    return col;
  }

  public Collection getColTipoDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoDependencia.iterator();
    TipoDependencia tipoDependencia = null;
    while (iterator.hasNext()) {
      tipoDependencia = (TipoDependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoDependencia.getIdTipoDependencia()), 
        tipoDependencia.toString()));
    }
    return col;
  }

  public Collection getListLocalidad() {
    Collection col = new ArrayList();

    Iterator iterEntry = Dependencia.LISTA_LOCALIDAD.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColGrupoOrganismo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoOrganismo.iterator();
    GrupoOrganismo grupoOrganismo = null;
    while (iterator.hasNext()) {
      grupoOrganismo = (GrupoOrganismo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoOrganismo.getIdGrupoOrganismo()), 
        grupoOrganismo.toString()));
    }
    return col;
  }

  public Collection getListVigente() {
    Collection col = new ArrayList();

    Iterator iterEntry = Dependencia.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColDependenciaAnterior()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependenciaAnterior.iterator();
    Dependencia dependenciaAnterior = null;
    while (iterator.hasNext()) {
      dependenciaAnterior = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependenciaAnterior.getIdDependencia()), 
        dependenciaAnterior.toString()));
    }
    return col;
  }

  public Collection getListDependenciaStaff() {
    Collection col = new ArrayList();

    Iterator iterEntry = Dependencia.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAprobacionMpd() {
    Collection col = new ArrayList();

    Iterator iterEntry = Dependencia.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListSedeDiplomatica() {
    Collection col = new ArrayList();

    Iterator iterEntry = Dependencia.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColEstructura = 
        this.estructuraFacade.findEstructuraByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColAdministradoraUel = 
        this.estructuraFacade.findAllAdministradoraUel();
      this.findColRegion = 
        this.estructuraFacade.findRegionByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colEstructura = 
        this.estructuraFacade.findEstructuraByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colAdministradoraUel = 
        this.estructuraFacade.findAllAdministradoraUel();
      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colSede = 
        this.estructuraFacade.findSedeByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colUnidadFuncional = 
        this.estructuraFacade.findUnidadFuncionalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colTipoDependencia = 
        this.estructuraFacade.findAllTipoDependencia();
      this.colGrupoOrganismo = 
        this.estructuraFacade.findGrupoOrganismoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colDependenciaAnterior = 
        this.estructuraFacade.findDependenciaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findDependenciaByCodDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findDependenciaByCodDependencia(this.findCodDependencia, idOrganismo);
      this.showDependenciaByCodDependencia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDependenciaByCodDependencia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodDependencia = null;
    this.findNombre = null;
    this.findSelectEstructura = null;
    this.findSelectAdministradoraUel = null;
    this.findSelectRegion = null;

    return null;
  }

  public String findDependenciaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findDependenciaByNombre(this.findNombre, idOrganismo);
      this.showDependenciaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDependenciaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodDependencia = null;
    this.findNombre = null;
    this.findSelectEstructura = null;
    this.findSelectAdministradoraUel = null;
    this.findSelectRegion = null;

    return null;
  }

  public String findDependenciaByEstructura()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findDependenciaByEstructura(Long.valueOf(this.findSelectEstructura).longValue(), idOrganismo);
      this.showDependenciaByEstructura = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDependenciaByEstructura)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodDependencia = null;
    this.findNombre = null;
    this.findSelectEstructura = null;
    this.findSelectAdministradoraUel = null;
    this.findSelectRegion = null;

    return null;
  }

  public String findDependenciaByAdministradoraUel()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findDependenciaByAdministradoraUel(Long.valueOf(this.findSelectAdministradoraUel).longValue(), idOrganismo);
      this.showDependenciaByAdministradoraUel = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDependenciaByAdministradoraUel)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodDependencia = null;
    this.findNombre = null;
    this.findSelectEstructura = null;
    this.findSelectAdministradoraUel = null;
    this.findSelectRegion = null;

    return null;
  }

  public String findDependenciaByRegion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findDependenciaByRegion(Long.valueOf(this.findSelectRegion).longValue(), idOrganismo);
      this.showDependenciaByRegion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDependenciaByRegion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodDependencia = null;
    this.findNombre = null;
    this.findSelectEstructura = null;
    this.findSelectAdministradoraUel = null;
    this.findSelectRegion = null;

    return null;
  }

  public boolean isShowDependenciaByCodDependencia() {
    return this.showDependenciaByCodDependencia;
  }
  public boolean isShowDependenciaByNombre() {
    return this.showDependenciaByNombre;
  }
  public boolean isShowDependenciaByEstructura() {
    return this.showDependenciaByEstructura;
  }
  public boolean isShowDependenciaByAdministradoraUel() {
    return this.showDependenciaByAdministradoraUel;
  }
  public boolean isShowDependenciaByRegion() {
    return this.showDependenciaByRegion;
  }

  public String selectDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectEstructura = null;
    this.selectAdministradoraUel = null;
    this.selectRegion = null;
    this.selectSede = null;
    this.selectUnidadFuncional = null;
    this.selectTipoDependencia = null;
    this.selectGrupoOrganismo = null;
    this.selectDependenciaAnterior = null;

    long idDependencia = 
      Long.parseLong((String)requestParameterMap.get("idDependencia"));
    try
    {
      this.dependencia = 
        this.estructuraFacade.findDependenciaById(
        idDependencia);
      if (this.dependencia.getEstructura() != null) {
        this.selectEstructura = 
          String.valueOf(this.dependencia.getEstructura().getIdEstructura());
      }
      if (this.dependencia.getAdministradoraUel() != null) {
        this.selectAdministradoraUel = 
          String.valueOf(this.dependencia.getAdministradoraUel().getIdAdministradoraUel());
      }
      if (this.dependencia.getRegion() != null) {
        this.selectRegion = 
          String.valueOf(this.dependencia.getRegion().getIdRegion());
      }
      if (this.dependencia.getSede() != null) {
        this.selectSede = 
          String.valueOf(this.dependencia.getSede().getIdSede());
      }
      if (this.dependencia.getUnidadFuncional() != null) {
        this.selectUnidadFuncional = 
          String.valueOf(this.dependencia.getUnidadFuncional().getIdUnidadFuncional());
      }
      if (this.dependencia.getTipoDependencia() != null) {
        this.selectTipoDependencia = 
          String.valueOf(this.dependencia.getTipoDependencia().getIdTipoDependencia());
      }
      if (this.dependencia.getGrupoOrganismo() != null) {
        this.selectGrupoOrganismo = 
          String.valueOf(this.dependencia.getGrupoOrganismo().getIdGrupoOrganismo());
      }
      if (this.dependencia.getDependenciaAnterior() != null) {
        this.selectDependenciaAnterior = 
          String.valueOf(this.dependencia.getDependenciaAnterior().getIdDependencia());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.dependencia = null;
    this.showDependenciaByCodDependencia = false;
    this.showDependenciaByNombre = false;
    this.showDependenciaByEstructura = false;
    this.showDependenciaByAdministradoraUel = false;
    this.showDependenciaByRegion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.dependencia.getFechaVigencia() != null) && 
      (this.dependencia.getFechaVigencia().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Vigencia no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.dependencia.getFechaFin() != null) && 
      (this.dependencia.getFechaFin().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Eliminación no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addDependencia(
          this.dependencia);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateDependencia(
          this.dependencia);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteDependencia(
        this.dependencia);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.dependencia = new Dependencia();

    this.selectEstructura = null;

    this.selectAdministradoraUel = null;

    this.selectRegion = null;

    this.selectSede = null;

    this.selectUnidadFuncional = null;

    this.selectTipoDependencia = null;

    this.selectGrupoOrganismo = null;

    this.selectDependenciaAnterior = null;

    this.dependencia.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.dependencia.setIdDependencia(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.Dependencia"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.dependencia = new Dependencia();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}