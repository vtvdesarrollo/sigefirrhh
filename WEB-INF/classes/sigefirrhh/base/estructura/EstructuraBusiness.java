package sigefirrhh.base.estructura;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class EstructuraBusiness extends AbstractBusiness
  implements Serializable
{
  private TipoCaracteristicaBeanBusiness tipoCaracteristicaBeanBusiness = new TipoCaracteristicaBeanBusiness();

  private ClasificacionDependenciaBeanBusiness clasificacionDependenciaBeanBusiness = new ClasificacionDependenciaBeanBusiness();

  private CaracteristicaDependenciaBeanBusiness caracteristicaDependenciaBeanBusiness = new CaracteristicaDependenciaBeanBusiness();

  private AdministradoraUelBeanBusiness administradoraUelBeanBusiness = new AdministradoraUelBeanBusiness();

  private DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

  private GrupoOrganismoBeanBusiness grupoOrganismoBeanBusiness = new GrupoOrganismoBeanBusiness();

  private LugarPagoBeanBusiness lugarPagoBeanBusiness = new LugarPagoBeanBusiness();

  private NombreOrganismoBeanBusiness nombreOrganismoBeanBusiness = new NombreOrganismoBeanBusiness();

  private OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

  private ProgramaBeanBusiness programaBeanBusiness = new ProgramaBeanBusiness();

  private RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

  private SedeBeanBusiness sedeBeanBusiness = new SedeBeanBusiness();

  private TipoDependenciaBeanBusiness tipoDependenciaBeanBusiness = new TipoDependenciaBeanBusiness();

  private UnidadAdministradoraBeanBusiness unidadAdministradoraBeanBusiness = new UnidadAdministradoraBeanBusiness();

  private UnidadEjecutoraBeanBusiness unidadEjecutoraBeanBusiness = new UnidadEjecutoraBeanBusiness();

  private UnidadFuncionalBeanBusiness unidadFuncionalBeanBusiness = new UnidadFuncionalBeanBusiness();

  private EstructuraBeanBusiness estructuraBeanBusiness = new EstructuraBeanBusiness();

  public void addTipoCaracteristica(TipoCaracteristica tipoCaracteristica)
    throws Exception
  {
    this.tipoCaracteristicaBeanBusiness.addTipoCaracteristica(tipoCaracteristica);
  }

  public void updateTipoCaracteristica(TipoCaracteristica tipoCaracteristica) throws Exception {
    this.tipoCaracteristicaBeanBusiness.updateTipoCaracteristica(tipoCaracteristica);
  }

  public void deleteTipoCaracteristica(TipoCaracteristica tipoCaracteristica) throws Exception {
    this.tipoCaracteristicaBeanBusiness.deleteTipoCaracteristica(tipoCaracteristica);
  }

  public TipoCaracteristica findTipoCaracteristicaById(long tipoCaracteristicaId) throws Exception {
    return this.tipoCaracteristicaBeanBusiness.findTipoCaracteristicaById(tipoCaracteristicaId);
  }

  public Collection findAllTipoCaracteristica() throws Exception {
    return this.tipoCaracteristicaBeanBusiness.findTipoCaracteristicaAll();
  }

  public Collection findTipoCaracteristicaByCodTipoCaracteristica(String codTipoCaracteristica)
    throws Exception
  {
    return this.tipoCaracteristicaBeanBusiness.findByCodTipoCaracteristica(codTipoCaracteristica);
  }

  public Collection findTipoCaracteristicaByNombre(String nombre)
    throws Exception
  {
    return this.tipoCaracteristicaBeanBusiness.findByNombre(nombre);
  }

  public void addClasificacionDependencia(ClasificacionDependencia clasificacionDependencia)
    throws Exception
  {
    this.clasificacionDependenciaBeanBusiness.addClasificacionDependencia(clasificacionDependencia);
  }

  public void updateClasificacionDependencia(ClasificacionDependencia clasificacionDependencia) throws Exception {
    this.clasificacionDependenciaBeanBusiness.updateClasificacionDependencia(clasificacionDependencia);
  }

  public void deleteClasificacionDependencia(ClasificacionDependencia clasificacionDependencia) throws Exception {
    this.clasificacionDependenciaBeanBusiness.deleteClasificacionDependencia(clasificacionDependencia);
  }

  public ClasificacionDependencia findClasificacionDependenciaById(long clasificacionDependenciaId) throws Exception {
    return this.clasificacionDependenciaBeanBusiness.findClasificacionDependenciaById(clasificacionDependenciaId);
  }

  public Collection findAllClasificacionDependencia() throws Exception {
    return this.clasificacionDependenciaBeanBusiness.findClasificacionDependenciaAll();
  }

  public Collection findClasificacionDependenciaByCaracteristicaDependencia(long idCaracteristicaDependencia)
    throws Exception
  {
    return this.clasificacionDependenciaBeanBusiness.findByCaracteristicaDependencia(idCaracteristicaDependencia);
  }

  public Collection findClasificacionDependenciaByDependencia(long idDependencia)
    throws Exception
  {
    return this.clasificacionDependenciaBeanBusiness.findByDependencia(idDependencia);
  }

  public void addCaracteristicaDependencia(CaracteristicaDependencia caracteristicaDependencia)
    throws Exception
  {
    this.caracteristicaDependenciaBeanBusiness.addCaracteristicaDependencia(caracteristicaDependencia);
  }

  public void updateCaracteristicaDependencia(CaracteristicaDependencia caracteristicaDependencia) throws Exception {
    this.caracteristicaDependenciaBeanBusiness.updateCaracteristicaDependencia(caracteristicaDependencia);
  }

  public void deleteCaracteristicaDependencia(CaracteristicaDependencia caracteristicaDependencia) throws Exception {
    this.caracteristicaDependenciaBeanBusiness.deleteCaracteristicaDependencia(caracteristicaDependencia);
  }

  public CaracteristicaDependencia findCaracteristicaDependenciaById(long caracteristicaDependenciaId) throws Exception {
    return this.caracteristicaDependenciaBeanBusiness.findCaracteristicaDependenciaById(caracteristicaDependenciaId);
  }

  public Collection findAllCaracteristicaDependencia() throws Exception {
    return this.caracteristicaDependenciaBeanBusiness.findCaracteristicaDependenciaAll();
  }

  public Collection findCaracteristicaDependenciaByTipoCaracteristica(long idTipoCaracteristica)
    throws Exception
  {
    return this.caracteristicaDependenciaBeanBusiness.findByTipoCaracteristica(idTipoCaracteristica);
  }

  public void addAdministradoraUel(AdministradoraUel administradoraUel)
    throws Exception
  {
    this.administradoraUelBeanBusiness.addAdministradoraUel(administradoraUel);
  }

  public void updateAdministradoraUel(AdministradoraUel administradoraUel) throws Exception {
    this.administradoraUelBeanBusiness.updateAdministradoraUel(administradoraUel);
  }

  public void deleteAdministradoraUel(AdministradoraUel administradoraUel) throws Exception {
    this.administradoraUelBeanBusiness.deleteAdministradoraUel(administradoraUel);
  }

  public AdministradoraUel findAdministradoraUelById(long administradoraUelId) throws Exception {
    return this.administradoraUelBeanBusiness.findAdministradoraUelById(administradoraUelId);
  }

  public Collection findAllAdministradoraUel() throws Exception {
    return this.administradoraUelBeanBusiness.findAdministradoraUelAll();
  }

  public Collection findAdministradoraUelByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    return this.administradoraUelBeanBusiness.findByUnidadAdministradora(idUnidadAdministradora);
  }

  public void addDependencia(Dependencia dependencia)
    throws Exception
  {
    this.dependenciaBeanBusiness.addDependencia(dependencia);
  }

  public void updateDependencia(Dependencia dependencia) throws Exception {
    this.dependenciaBeanBusiness.updateDependencia(dependencia);
  }

  public void deleteDependencia(Dependencia dependencia) throws Exception {
    this.dependenciaBeanBusiness.deleteDependencia(dependencia);
  }

  public Dependencia findDependenciaById(long dependenciaId) throws Exception {
    return this.dependenciaBeanBusiness.findDependenciaById(dependenciaId);
  }

  public Collection findAllDependencia() throws Exception {
    return this.dependenciaBeanBusiness.findDependenciaAll();
  }

  public Collection findDependenciaByCodDependencia(String codDependencia, long idOrganismo)
    throws Exception
  {
    return this.dependenciaBeanBusiness.findByCodDependencia(codDependencia, idOrganismo);
  }

  public Collection findDependenciaByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.dependenciaBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findDependenciaByEstructura(long idEstructura, long idOrganismo)
    throws Exception
  {
    return this.dependenciaBeanBusiness.findByEstructura(idEstructura, idOrganismo);
  }

  public Collection findDependenciaByAdministradoraUel(long idAdministradoraUel, long idOrganismo)
    throws Exception
  {
    return this.dependenciaBeanBusiness.findByAdministradoraUel(idAdministradoraUel, idOrganismo);
  }

  public Collection findDependenciaByRegion(long idRegion, long idOrganismo)
    throws Exception
  {
    return this.dependenciaBeanBusiness.findByRegion(idRegion, idOrganismo);
  }

  public Collection findDependenciaBySede(long idSede, long idOrganismo)
    throws Exception
  {
    return this.dependenciaBeanBusiness.findBySede(idSede, idOrganismo);
  }

  public Collection findDependenciaByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.dependenciaBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addGrupoOrganismo(GrupoOrganismo grupoOrganismo)
    throws Exception
  {
    this.grupoOrganismoBeanBusiness.addGrupoOrganismo(grupoOrganismo);
  }

  public void updateGrupoOrganismo(GrupoOrganismo grupoOrganismo) throws Exception {
    this.grupoOrganismoBeanBusiness.updateGrupoOrganismo(grupoOrganismo);
  }

  public void deleteGrupoOrganismo(GrupoOrganismo grupoOrganismo) throws Exception {
    this.grupoOrganismoBeanBusiness.deleteGrupoOrganismo(grupoOrganismo);
  }

  public GrupoOrganismo findGrupoOrganismoById(long grupoOrganismoId) throws Exception {
    return this.grupoOrganismoBeanBusiness.findGrupoOrganismoById(grupoOrganismoId);
  }

  public Collection findAllGrupoOrganismo() throws Exception {
    return this.grupoOrganismoBeanBusiness.findGrupoOrganismoAll();
  }

  public Collection findGrupoOrganismoByCodGrupoOrganismo(String codGrupoOrganismo, long idOrganismo)
    throws Exception
  {
    return this.grupoOrganismoBeanBusiness.findByCodGrupoOrganismo(codGrupoOrganismo, idOrganismo);
  }

  public Collection findGrupoOrganismoByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.grupoOrganismoBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findGrupoOrganismoByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.grupoOrganismoBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addLugarPago(LugarPago lugarPago)
    throws Exception
  {
    this.lugarPagoBeanBusiness.addLugarPago(lugarPago);
  }

  public void updateLugarPago(LugarPago lugarPago) throws Exception {
    this.lugarPagoBeanBusiness.updateLugarPago(lugarPago);
  }

  public void deleteLugarPago(LugarPago lugarPago) throws Exception {
    this.lugarPagoBeanBusiness.deleteLugarPago(lugarPago);
  }

  public LugarPago findLugarPagoById(long lugarPagoId) throws Exception {
    return this.lugarPagoBeanBusiness.findLugarPagoById(lugarPagoId);
  }

  public Collection findAllLugarPago() throws Exception {
    return this.lugarPagoBeanBusiness.findLugarPagoAll();
  }

  public Collection findLugarPagoByCodLugarPago(String codLugarPago) throws Exception
  {
    return this.lugarPagoBeanBusiness.findByCodLugarPago(codLugarPago);
  }

  public Collection findLugarPagoByNombre(String nombre)
    throws Exception
  {
    return this.lugarPagoBeanBusiness.findByNombre(nombre);
  }

  public void addNombreOrganismo(NombreOrganismo nombreOrganismo)
    throws Exception
  {
    this.nombreOrganismoBeanBusiness.addNombreOrganismo(nombreOrganismo);
  }

  public void updateNombreOrganismo(NombreOrganismo nombreOrganismo) throws Exception {
    this.nombreOrganismoBeanBusiness.updateNombreOrganismo(nombreOrganismo);
  }

  public void deleteNombreOrganismo(NombreOrganismo nombreOrganismo) throws Exception {
    this.nombreOrganismoBeanBusiness.deleteNombreOrganismo(nombreOrganismo);
  }

  public NombreOrganismo findNombreOrganismoById(long nombreOrganismoId) throws Exception {
    return this.nombreOrganismoBeanBusiness.findNombreOrganismoById(nombreOrganismoId);
  }

  public Collection findAllNombreOrganismo() throws Exception {
    return this.nombreOrganismoBeanBusiness.findNombreOrganismoAll();
  }

  public Collection findNombreOrganismoByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.nombreOrganismoBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findNombreOrganismoByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.nombreOrganismoBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addOrganismo(Organismo organismo)
    throws Exception
  {
    this.organismoBeanBusiness.addOrganismo(organismo);
  }

  public void updateOrganismo(Organismo organismo) throws Exception {
    this.organismoBeanBusiness.updateOrganismo(organismo);
  }

  public void deleteOrganismo(Organismo organismo) throws Exception {
    this.organismoBeanBusiness.deleteOrganismo(organismo);
  }

  public Organismo findOrganismoById(long organismoId) throws Exception {
    return this.organismoBeanBusiness.findOrganismoById(organismoId);
  }

  public Collection findAllOrganismo() throws Exception {
    return this.organismoBeanBusiness.findOrganismoAll();
  }

  public Collection findOrganismoByCodOrganismo(String codOrganismo)
    throws Exception
  {
    return this.organismoBeanBusiness.findByCodOrganismo(codOrganismo);
  }

  public Collection findOrganismoByNombreOrganismo(String nombreOrganismo)
    throws Exception
  {
    return this.organismoBeanBusiness.findByNombreOrganismo(nombreOrganismo);
  }

  public void addPrograma(Programa programa)
    throws Exception
  {
    this.programaBeanBusiness.addPrograma(programa);
  }

  public void updatePrograma(Programa programa) throws Exception {
    this.programaBeanBusiness.updatePrograma(programa);
  }

  public void deletePrograma(Programa programa) throws Exception {
    this.programaBeanBusiness.deletePrograma(programa);
  }

  public Programa findProgramaById(long programaId) throws Exception {
    return this.programaBeanBusiness.findProgramaById(programaId);
  }

  public Collection findAllPrograma() throws Exception {
    return this.programaBeanBusiness.findProgramaAll();
  }

  public Collection findProgramaByCodPrograma(String codPrograma, long idOrganismo)
    throws Exception
  {
    return this.programaBeanBusiness.findByCodPrograma(codPrograma, idOrganismo);
  }

  public Collection findProgramaByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.programaBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findProgramaByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.programaBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addRegion(Region region)
    throws Exception
  {
    this.regionBeanBusiness.addRegion(region);
  }

  public void updateRegion(Region region) throws Exception {
    this.regionBeanBusiness.updateRegion(region);
  }

  public void deleteRegion(Region region) throws Exception {
    this.regionBeanBusiness.deleteRegion(region);
  }

  public Region findRegionById(long regionId) throws Exception {
    return this.regionBeanBusiness.findRegionById(regionId);
  }

  public Collection findAllRegion() throws Exception {
    return this.regionBeanBusiness.findRegionAll();
  }

  public Collection findRegionByCodRegion(String codRegion, long idOrganismo)
    throws Exception
  {
    return this.regionBeanBusiness.findByCodRegion(codRegion, idOrganismo);
  }

  public Collection findRegionByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.regionBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findRegionByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.regionBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addSede(Sede sede)
    throws Exception
  {
    this.sedeBeanBusiness.addSede(sede);
  }

  public void updateSede(Sede sede) throws Exception {
    this.sedeBeanBusiness.updateSede(sede);
  }

  public void deleteSede(Sede sede) throws Exception {
    this.sedeBeanBusiness.deleteSede(sede);
  }

  public Sede findSedeById(long sedeId) throws Exception {
    return this.sedeBeanBusiness.findSedeById(sedeId);
  }

  public Collection findAllSede() throws Exception {
    return this.sedeBeanBusiness.findSedeAll();
  }

  public Collection findSedeByRegion(long idRegion, long idOrganismo)
    throws Exception
  {
    return this.sedeBeanBusiness.findByRegion(idRegion, idOrganismo);
  }

  public Collection findSedeByCodSede(String codSede, long idOrganismo)
    throws Exception
  {
    return this.sedeBeanBusiness.findByCodSede(codSede, idOrganismo);
  }

  public Collection findSedeByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.sedeBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findSedeByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.sedeBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addTipoDependencia(TipoDependencia tipoDependencia)
    throws Exception
  {
    this.tipoDependenciaBeanBusiness.addTipoDependencia(tipoDependencia);
  }

  public void updateTipoDependencia(TipoDependencia tipoDependencia) throws Exception {
    this.tipoDependenciaBeanBusiness.updateTipoDependencia(tipoDependencia);
  }

  public void deleteTipoDependencia(TipoDependencia tipoDependencia) throws Exception {
    this.tipoDependenciaBeanBusiness.deleteTipoDependencia(tipoDependencia);
  }

  public TipoDependencia findTipoDependenciaById(long tipoDependenciaId) throws Exception {
    return this.tipoDependenciaBeanBusiness.findTipoDependenciaById(tipoDependenciaId);
  }

  public Collection findAllTipoDependencia() throws Exception {
    return this.tipoDependenciaBeanBusiness.findTipoDependenciaAll();
  }

  public Collection findTipoDependenciaByCodTipoDependencia(String codTipoDependencia)
    throws Exception
  {
    return this.tipoDependenciaBeanBusiness.findByCodTipoDependencia(codTipoDependencia);
  }

  public Collection findTipoDependenciaByNombre(String nombre)
    throws Exception
  {
    return this.tipoDependenciaBeanBusiness.findByNombre(nombre);
  }

  public void addUnidadAdministradora(UnidadAdministradora unidadAdministradora)
    throws Exception
  {
    this.unidadAdministradoraBeanBusiness.addUnidadAdministradora(unidadAdministradora);
  }

  public void updateUnidadAdministradora(UnidadAdministradora unidadAdministradora) throws Exception {
    this.unidadAdministradoraBeanBusiness.updateUnidadAdministradora(unidadAdministradora);
  }

  public void deleteUnidadAdministradora(UnidadAdministradora unidadAdministradora) throws Exception {
    this.unidadAdministradoraBeanBusiness.deleteUnidadAdministradora(unidadAdministradora);
  }

  public UnidadAdministradora findUnidadAdministradoraById(long unidadAdministradoraId) throws Exception {
    return this.unidadAdministradoraBeanBusiness.findUnidadAdministradoraById(unidadAdministradoraId);
  }

  public Collection findAllUnidadAdministradora() throws Exception {
    return this.unidadAdministradoraBeanBusiness.findUnidadAdministradoraAll();
  }

  public Collection findUnidadAdministradoraByCodUnidadAdminist(String codUnidadAdminist, long idOrganismo)
    throws Exception
  {
    return this.unidadAdministradoraBeanBusiness.findByCodUnidadAdminist(codUnidadAdminist, idOrganismo);
  }

  public Collection findUnidadAdministradoraByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.unidadAdministradoraBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findUnidadAdministradoraByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.unidadAdministradoraBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addUnidadEjecutora(UnidadEjecutora unidadEjecutora)
    throws Exception
  {
    this.unidadEjecutoraBeanBusiness.addUnidadEjecutora(unidadEjecutora);
  }

  public void updateUnidadEjecutora(UnidadEjecutora unidadEjecutora) throws Exception {
    this.unidadEjecutoraBeanBusiness.updateUnidadEjecutora(unidadEjecutora);
  }

  public void deleteUnidadEjecutora(UnidadEjecutora unidadEjecutora) throws Exception {
    this.unidadEjecutoraBeanBusiness.deleteUnidadEjecutora(unidadEjecutora);
  }

  public UnidadEjecutora findUnidadEjecutoraById(long unidadEjecutoraId) throws Exception {
    return this.unidadEjecutoraBeanBusiness.findUnidadEjecutoraById(unidadEjecutoraId);
  }

  public Collection findAllUnidadEjecutora() throws Exception {
    return this.unidadEjecutoraBeanBusiness.findUnidadEjecutoraAll();
  }

  public Collection findUnidadEjecutoraByCodUnidadEjecutora(String codUnidadEjecutora)
    throws Exception
  {
    return this.unidadEjecutoraBeanBusiness.findByCodUnidadEjecutora(codUnidadEjecutora);
  }

  public Collection findUnidadEjecutoraByNombre(String nombre)
    throws Exception
  {
    return this.unidadEjecutoraBeanBusiness.findByNombre(nombre);
  }

  public void addUnidadFuncional(UnidadFuncional unidadFuncional)
    throws Exception
  {
    this.unidadFuncionalBeanBusiness.addUnidadFuncional(unidadFuncional);
  }

  public void updateUnidadFuncional(UnidadFuncional unidadFuncional) throws Exception {
    this.unidadFuncionalBeanBusiness.updateUnidadFuncional(unidadFuncional);
  }

  public void deleteUnidadFuncional(UnidadFuncional unidadFuncional) throws Exception {
    this.unidadFuncionalBeanBusiness.deleteUnidadFuncional(unidadFuncional);
  }

  public UnidadFuncional findUnidadFuncionalById(long unidadFuncionalId) throws Exception {
    return this.unidadFuncionalBeanBusiness.findUnidadFuncionalById(unidadFuncionalId);
  }

  public Collection findAllUnidadFuncional() throws Exception {
    return this.unidadFuncionalBeanBusiness.findUnidadFuncionalAll();
  }

  public Collection findUnidadFuncionalByCodUnidadFuncional(String codUnidadFuncional, long idOrganismo)
    throws Exception
  {
    return this.unidadFuncionalBeanBusiness.findByCodUnidadFuncional(codUnidadFuncional, idOrganismo);
  }

  public Collection findUnidadFuncionalByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.unidadFuncionalBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findUnidadFuncionalByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.unidadFuncionalBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addEstructura(Estructura estructura)
    throws Exception
  {
    this.estructuraBeanBusiness.addEstructura(estructura);
  }

  public void updateEstructura(Estructura estructura) throws Exception {
    this.estructuraBeanBusiness.updateEstructura(estructura);
  }

  public void deleteEstructura(Estructura estructura) throws Exception {
    this.estructuraBeanBusiness.deleteEstructura(estructura);
  }

  public Estructura findEstructuraById(long estructuraId) throws Exception {
    return this.estructuraBeanBusiness.findEstructuraById(estructuraId);
  }

  public Collection findAllEstructura() throws Exception {
    return this.estructuraBeanBusiness.findEstructuraAll();
  }

  public Collection findEstructuraByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.estructuraBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findEstructuraByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.estructuraBeanBusiness.findByOrganismo(idOrganismo);
  }
}