package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoCaracteristica
  implements Serializable, PersistenceCapable
{
  private long idTipoCaracteristica;
  private String codTipoCaracteristica;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoCaracteristica", "idTipoCaracteristica", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + 
      jdoGetcodTipoCaracteristica(this);
  }

  public String getCodTipoCaracteristica()
  {
    return jdoGetcodTipoCaracteristica(this);
  }
  public void setCodTipoCaracteristica(String codTipoCaracteristica) {
    jdoSetcodTipoCaracteristica(this, codTipoCaracteristica);
  }
  public long getIdTipoCaracteristica() {
    return jdoGetidTipoCaracteristica(this);
  }
  public void setIdTipoCaracteristica(long idTipoCaracteristica) {
    jdoSetidTipoCaracteristica(this, idTipoCaracteristica);
  }
  public String getNombre() {
    return jdoGetnombre(this);
  }
  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.TipoCaracteristica"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoCaracteristica());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoCaracteristica localTipoCaracteristica = new TipoCaracteristica();
    localTipoCaracteristica.jdoFlags = 1;
    localTipoCaracteristica.jdoStateManager = paramStateManager;
    return localTipoCaracteristica;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoCaracteristica localTipoCaracteristica = new TipoCaracteristica();
    localTipoCaracteristica.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoCaracteristica.jdoFlags = 1;
    localTipoCaracteristica.jdoStateManager = paramStateManager;
    return localTipoCaracteristica;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoCaracteristica);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoCaracteristica);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoCaracteristica = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoCaracteristica = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoCaracteristica paramTipoCaracteristica, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoCaracteristica == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoCaracteristica = paramTipoCaracteristica.codTipoCaracteristica;
      return;
    case 1:
      if (paramTipoCaracteristica == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoCaracteristica = paramTipoCaracteristica.idTipoCaracteristica;
      return;
    case 2:
      if (paramTipoCaracteristica == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramTipoCaracteristica.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoCaracteristica))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoCaracteristica localTipoCaracteristica = (TipoCaracteristica)paramObject;
    if (localTipoCaracteristica.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoCaracteristica, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoCaracteristicaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoCaracteristicaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoCaracteristicaPK))
      throw new IllegalArgumentException("arg1");
    TipoCaracteristicaPK localTipoCaracteristicaPK = (TipoCaracteristicaPK)paramObject;
    localTipoCaracteristicaPK.idTipoCaracteristica = this.idTipoCaracteristica;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoCaracteristicaPK))
      throw new IllegalArgumentException("arg1");
    TipoCaracteristicaPK localTipoCaracteristicaPK = (TipoCaracteristicaPK)paramObject;
    this.idTipoCaracteristica = localTipoCaracteristicaPK.idTipoCaracteristica;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoCaracteristicaPK))
      throw new IllegalArgumentException("arg2");
    TipoCaracteristicaPK localTipoCaracteristicaPK = (TipoCaracteristicaPK)paramObject;
    localTipoCaracteristicaPK.idTipoCaracteristica = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoCaracteristicaPK))
      throw new IllegalArgumentException("arg2");
    TipoCaracteristicaPK localTipoCaracteristicaPK = (TipoCaracteristicaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localTipoCaracteristicaPK.idTipoCaracteristica);
  }

  private static final String jdoGetcodTipoCaracteristica(TipoCaracteristica paramTipoCaracteristica)
  {
    if (paramTipoCaracteristica.jdoFlags <= 0)
      return paramTipoCaracteristica.codTipoCaracteristica;
    StateManager localStateManager = paramTipoCaracteristica.jdoStateManager;
    if (localStateManager == null)
      return paramTipoCaracteristica.codTipoCaracteristica;
    if (localStateManager.isLoaded(paramTipoCaracteristica, jdoInheritedFieldCount + 0))
      return paramTipoCaracteristica.codTipoCaracteristica;
    return localStateManager.getStringField(paramTipoCaracteristica, jdoInheritedFieldCount + 0, paramTipoCaracteristica.codTipoCaracteristica);
  }

  private static final void jdoSetcodTipoCaracteristica(TipoCaracteristica paramTipoCaracteristica, String paramString)
  {
    if (paramTipoCaracteristica.jdoFlags == 0)
    {
      paramTipoCaracteristica.codTipoCaracteristica = paramString;
      return;
    }
    StateManager localStateManager = paramTipoCaracteristica.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoCaracteristica.codTipoCaracteristica = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoCaracteristica, jdoInheritedFieldCount + 0, paramTipoCaracteristica.codTipoCaracteristica, paramString);
  }

  private static final long jdoGetidTipoCaracteristica(TipoCaracteristica paramTipoCaracteristica)
  {
    return paramTipoCaracteristica.idTipoCaracteristica;
  }

  private static final void jdoSetidTipoCaracteristica(TipoCaracteristica paramTipoCaracteristica, long paramLong)
  {
    StateManager localStateManager = paramTipoCaracteristica.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoCaracteristica.idTipoCaracteristica = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoCaracteristica, jdoInheritedFieldCount + 1, paramTipoCaracteristica.idTipoCaracteristica, paramLong);
  }

  private static final String jdoGetnombre(TipoCaracteristica paramTipoCaracteristica)
  {
    if (paramTipoCaracteristica.jdoFlags <= 0)
      return paramTipoCaracteristica.nombre;
    StateManager localStateManager = paramTipoCaracteristica.jdoStateManager;
    if (localStateManager == null)
      return paramTipoCaracteristica.nombre;
    if (localStateManager.isLoaded(paramTipoCaracteristica, jdoInheritedFieldCount + 2))
      return paramTipoCaracteristica.nombre;
    return localStateManager.getStringField(paramTipoCaracteristica, jdoInheritedFieldCount + 2, paramTipoCaracteristica.nombre);
  }

  private static final void jdoSetnombre(TipoCaracteristica paramTipoCaracteristica, String paramString)
  {
    if (paramTipoCaracteristica.jdoFlags == 0)
    {
      paramTipoCaracteristica.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramTipoCaracteristica.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoCaracteristica.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoCaracteristica, jdoInheritedFieldCount + 2, paramTipoCaracteristica.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}