package sigefirrhh.base.estructura;

import java.io.Serializable;

public class TipoDependenciaPK
  implements Serializable
{
  public long idTipoDependencia;

  public TipoDependenciaPK()
  {
  }

  public TipoDependenciaPK(long idTipoDependencia)
  {
    this.idTipoDependencia = idTipoDependencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoDependenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoDependenciaPK thatPK)
  {
    return 
      this.idTipoDependencia == thatPK.idTipoDependencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoDependencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoDependencia);
  }
}