package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;

public class SedeForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SedeForm.class.getName());
  private Sede sede;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showSedeByRegion;
  private boolean showSedeByCodSede;
  private boolean showSedeByNombre;
  private String findSelectRegion;
  private String findCodSede;
  private String findNombre;
  private Collection findColRegion;
  private Collection colRegion;
  private Collection colPaisForCiudad;
  private Collection colEstadoForCiudad;
  private Collection colCiudad;
  private Collection colLugarPago;
  private Collection colTurno;
  private String selectRegion;
  private String selectPaisForCiudad;
  private String selectEstadoForCiudad;
  private String selectCiudad;
  private String selectLugarPago;
  private String selectTurno;
  private Object stateResultSedeByRegion = null;

  private Object stateResultSedeByCodSede = null;

  private Object stateResultSedeByNombre = null;

  public String getFindSelectRegion()
  {
    return this.findSelectRegion;
  }
  public void setFindSelectRegion(String valRegion) {
    this.findSelectRegion = valRegion;
  }

  public Collection getFindColRegion() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }
  public String getFindCodSede() {
    return this.findCodSede;
  }
  public void setFindCodSede(String findCodSede) {
    this.findCodSede = findCodSede;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectRegion()
  {
    return this.selectRegion;
  }
  public void setSelectRegion(String valRegion) {
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    this.sede.setRegion(null);
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      if (String.valueOf(region.getIdRegion()).equals(
        valRegion)) {
        this.sede.setRegion(
          region);
        break;
      }
    }
    this.selectRegion = valRegion;
  }
  public String getSelectPaisForCiudad() {
    return this.selectPaisForCiudad;
  }
  public void setSelectPaisForCiudad(String valPaisForCiudad) {
    this.selectPaisForCiudad = valPaisForCiudad;
  }
  public void changePaisForCiudad(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      this.colEstadoForCiudad = null;
      if (idPais > 0L) {
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
      } else {
        this.selectCiudad = null;
        this.sede.setCiudad(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudad = null;
      this.sede.setCiudad(
        null);
    }
  }

  public boolean isShowPaisForCiudad() { return this.colPaisForCiudad != null; }

  public String getSelectEstadoForCiudad() {
    return this.selectEstadoForCiudad;
  }
  public void setSelectEstadoForCiudad(String valEstadoForCiudad) {
    this.selectEstadoForCiudad = valEstadoForCiudad;
  }
  public void changeEstadoForCiudad(ValueChangeEvent event) {
    long idEstado = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      if (idEstado > 0L) {
        this.colCiudad = 
          this.ubicacionFacade.findCiudadByEstado(
          idEstado);
      } else {
        this.selectCiudad = null;
        this.sede.setCiudad(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudad = null;
      this.sede.setCiudad(
        null);
    }
  }

  public boolean isShowEstadoForCiudad() { return this.colEstadoForCiudad != null; }

  public String getSelectCiudad() {
    return this.selectCiudad;
  }
  public void setSelectCiudad(String valCiudad) {
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    this.sede.setCiudad(null);
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      if (String.valueOf(ciudad.getIdCiudad()).equals(
        valCiudad)) {
        this.sede.setCiudad(
          ciudad);
        break;
      }
    }
    this.selectCiudad = valCiudad;
  }
  public boolean isShowCiudad() {
    return this.colCiudad != null;
  }
  public String getSelectLugarPago() {
    return this.selectLugarPago;
  }
  public void setSelectLugarPago(String valLugarPago) {
    Iterator iterator = this.colLugarPago.iterator();
    LugarPago lugarPago = null;
    this.sede.setLugarPago(null);
    while (iterator.hasNext()) {
      lugarPago = (LugarPago)iterator.next();
      if (String.valueOf(lugarPago.getIdLugarPago()).equals(
        valLugarPago)) {
        this.sede.setLugarPago(
          lugarPago);
        break;
      }
    }
    this.selectLugarPago = valLugarPago;
  }
  public String getSelectTurno() {
    return this.selectTurno;
  }
  public void setSelectTurno(String valTurno) {
    Iterator iterator = this.colTurno.iterator();
    Turno turno = null;
    this.sede.setTurno(null);
    while (iterator.hasNext()) {
      turno = (Turno)iterator.next();
      if (String.valueOf(turno.getIdTurno()).equals(
        valTurno)) {
        this.sede.setTurno(
          turno);
        break;
      }
    }
    this.selectTurno = valTurno;
  }
  public Collection getResult() {
    return this.result;
  }

  public Sede getSede() {
    if (this.sede == null) {
      this.sede = new Sede();
    }
    return this.sede;
  }

  public SedeForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRegion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public Collection getColPaisForCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForCiudad.iterator();
    Pais paisForCiudad = null;
    while (iterator.hasNext()) {
      paisForCiudad = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForCiudad.getIdPais()), 
        paisForCiudad.toString()));
    }
    return col;
  }

  public Collection getColEstadoForCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForCiudad.iterator();
    Estado estadoForCiudad = null;
    while (iterator.hasNext()) {
      estadoForCiudad = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estadoForCiudad.getIdEstado()), 
        estadoForCiudad.toString()));
    }
    return col;
  }

  public Collection getColCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ciudad.getIdCiudad()), 
        ciudad.toString()));
    }
    return col;
  }

  public Collection getColLugarPago()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colLugarPago.iterator();
    LugarPago lugarPago = null;
    while (iterator.hasNext()) {
      lugarPago = (LugarPago)iterator.next();
      col.add(new SelectItem(
        String.valueOf(lugarPago.getIdLugarPago()), 
        lugarPago.toString()));
    }
    return col;
  }

  public Collection getColTurno()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTurno.iterator();
    Turno turno = null;
    while (iterator.hasNext()) {
      turno = (Turno)iterator.next();
      col.add(new SelectItem(
        String.valueOf(turno.getIdTurno()), 
        turno.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColRegion = 
        this.estructuraFacade.findRegionByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colPaisForCiudad = 
        this.ubicacionFacade.findAllPais();
      this.colLugarPago = 
        this.estructuraFacade.findAllLugarPago();
      this.colTurno = 
        this.definicionesFacade.findTurnoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSedeByRegion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findSedeByRegion(Long.valueOf(this.findSelectRegion).longValue(), idOrganismo);
      this.showSedeByRegion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSedeByRegion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRegion = null;
    this.findCodSede = null;
    this.findNombre = null;

    return null;
  }

  public String findSedeByCodSede()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findSedeByCodSede(this.findCodSede, idOrganismo);
      this.showSedeByCodSede = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSedeByCodSede)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRegion = null;
    this.findCodSede = null;
    this.findNombre = null;

    return null;
  }

  public String findSedeByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findSedeByNombre(this.findNombre, idOrganismo);
      this.showSedeByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSedeByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRegion = null;
    this.findCodSede = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowSedeByRegion() {
    return this.showSedeByRegion;
  }
  public boolean isShowSedeByCodSede() {
    return this.showSedeByCodSede;
  }
  public boolean isShowSedeByNombre() {
    return this.showSedeByNombre;
  }

  public String selectSede()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRegion = null;
    this.selectCiudad = null;
    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    this.selectLugarPago = null;
    this.selectTurno = null;

    long idSede = 
      Long.parseLong((String)requestParameterMap.get("idSede"));
    try
    {
      this.sede = 
        this.estructuraFacade.findSedeById(
        idSede);
      if (this.sede.getRegion() != null) {
        this.selectRegion = 
          String.valueOf(this.sede.getRegion().getIdRegion());
      }
      if (this.sede.getCiudad() != null) {
        this.selectCiudad = 
          String.valueOf(this.sede.getCiudad().getIdCiudad());
      }
      if (this.sede.getLugarPago() != null) {
        this.selectLugarPago = 
          String.valueOf(this.sede.getLugarPago().getIdLugarPago());
      }
      if (this.sede.getTurno() != null) {
        this.selectTurno = 
          String.valueOf(this.sede.getTurno().getIdTurno());
      }

      Ciudad ciudad = null;
      Estado estadoForCiudad = null;
      Pais paisForCiudad = null;

      if (this.sede.getCiudad() != null) {
        long idCiudad = 
          this.sede.getCiudad().getIdCiudad();
        this.selectCiudad = String.valueOf(idCiudad);
        ciudad = this.ubicacionFacade.findCiudadById(
          idCiudad);
        this.colCiudad = this.ubicacionFacade.findCiudadByEstado(
          ciudad.getEstado().getIdEstado());

        long idEstadoForCiudad = 
          this.sede.getCiudad().getEstado().getIdEstado();
        this.selectEstadoForCiudad = String.valueOf(idEstadoForCiudad);
        estadoForCiudad = 
          this.ubicacionFacade.findEstadoById(
          idEstadoForCiudad);
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          estadoForCiudad.getPais().getIdPais());

        long idPaisForCiudad = 
          estadoForCiudad.getPais().getIdPais();
        this.selectPaisForCiudad = String.valueOf(idPaisForCiudad);
        paisForCiudad = 
          this.ubicacionFacade.findPaisById(
          idPaisForCiudad);
        this.colPaisForCiudad = 
          this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.sede = null;
    this.showSedeByRegion = false;
    this.showSedeByCodSede = false;
    this.showSedeByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addSede(
          this.sede);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateSede(
          this.sede);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteSede(
        this.sede);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.sede = new Sede();

    this.selectRegion = null;

    this.selectCiudad = null;

    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    this.selectLugarPago = null;

    this.selectTurno = null;

    this.sede.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.sede.setIdSede(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.Sede"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.sede = new Sede();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}