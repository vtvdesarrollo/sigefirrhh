package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ProgramaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPrograma(Programa programa)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Programa programaNew = 
      (Programa)BeanUtils.cloneBean(
      programa);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (programaNew.getOrganismo() != null) {
      programaNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        programaNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(programaNew);
  }

  public void updatePrograma(Programa programa) throws Exception
  {
    Programa programaModify = 
      findProgramaById(programa.getIdPrograma());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (programa.getOrganismo() != null) {
      programa.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        programa.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(programaModify, programa);
  }

  public void deletePrograma(Programa programa) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Programa programaDelete = 
      findProgramaById(programa.getIdPrograma());
    pm.deletePersistent(programaDelete);
  }

  public Programa findProgramaById(long idPrograma) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPrograma == pIdPrograma";
    Query query = pm.newQuery(Programa.class, filter);

    query.declareParameters("long pIdPrograma");

    parameters.put("pIdPrograma", new Long(idPrograma));

    Collection colPrograma = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPrograma.iterator();
    return (Programa)iterator.next();
  }

  public Collection findProgramaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent programaExtent = pm.getExtent(
      Programa.class, true);
    Query query = pm.newQuery(programaExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodPrograma(String codPrograma, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codPrograma == pCodPrograma &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Programa.class, filter);

    query.declareParameters("java.lang.String pCodPrograma, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodPrograma", new String(codPrograma));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colPrograma = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrograma);

    return colPrograma;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Programa.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colPrograma = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrograma);

    return colPrograma;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Programa.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colPrograma = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrograma);

    return colPrograma;
  }
}