package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class NombreOrganismo
  implements Serializable, PersistenceCapable
{
  private long idNombreOrganismo;
  private String nombre;
  private String nombreCorto;
  private Date fechaVigencia;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "fechaVigencia", "idNombreOrganismo", "nombre", "nombreCorto", "organismo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + 
      jdoGetorganismo(this).getCodOrganismo() + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaVigencia(this));
  }

  public Date getFechaVigencia()
  {
    return jdoGetfechaVigencia(this);
  }

  public long getIdNombreOrganismo()
  {
    return jdoGetidNombreOrganismo(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public String getNombreCorto()
  {
    return jdoGetnombreCorto(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setFechaVigencia(Date date)
  {
    jdoSetfechaVigencia(this, date);
  }

  public void setIdNombreOrganismo(long l)
  {
    jdoSetidNombreOrganismo(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setNombreCorto(String string)
  {
    jdoSetnombreCorto(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.NombreOrganismo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new NombreOrganismo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    NombreOrganismo localNombreOrganismo = new NombreOrganismo();
    localNombreOrganismo.jdoFlags = 1;
    localNombreOrganismo.jdoStateManager = paramStateManager;
    return localNombreOrganismo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    NombreOrganismo localNombreOrganismo = new NombreOrganismo();
    localNombreOrganismo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localNombreOrganismo.jdoFlags = 1;
    localNombreOrganismo.jdoStateManager = paramStateManager;
    return localNombreOrganismo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idNombreOrganismo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCorto);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idNombreOrganismo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCorto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(NombreOrganismo paramNombreOrganismo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramNombreOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramNombreOrganismo.fechaVigencia;
      return;
    case 1:
      if (paramNombreOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.idNombreOrganismo = paramNombreOrganismo.idNombreOrganismo;
      return;
    case 2:
      if (paramNombreOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramNombreOrganismo.nombre;
      return;
    case 3:
      if (paramNombreOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCorto = paramNombreOrganismo.nombreCorto;
      return;
    case 4:
      if (paramNombreOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramNombreOrganismo.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof NombreOrganismo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    NombreOrganismo localNombreOrganismo = (NombreOrganismo)paramObject;
    if (localNombreOrganismo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localNombreOrganismo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new NombreOrganismoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new NombreOrganismoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NombreOrganismoPK))
      throw new IllegalArgumentException("arg1");
    NombreOrganismoPK localNombreOrganismoPK = (NombreOrganismoPK)paramObject;
    localNombreOrganismoPK.idNombreOrganismo = this.idNombreOrganismo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NombreOrganismoPK))
      throw new IllegalArgumentException("arg1");
    NombreOrganismoPK localNombreOrganismoPK = (NombreOrganismoPK)paramObject;
    this.idNombreOrganismo = localNombreOrganismoPK.idNombreOrganismo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NombreOrganismoPK))
      throw new IllegalArgumentException("arg2");
    NombreOrganismoPK localNombreOrganismoPK = (NombreOrganismoPK)paramObject;
    localNombreOrganismoPK.idNombreOrganismo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NombreOrganismoPK))
      throw new IllegalArgumentException("arg2");
    NombreOrganismoPK localNombreOrganismoPK = (NombreOrganismoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localNombreOrganismoPK.idNombreOrganismo);
  }

  private static final Date jdoGetfechaVigencia(NombreOrganismo paramNombreOrganismo)
  {
    if (paramNombreOrganismo.jdoFlags <= 0)
      return paramNombreOrganismo.fechaVigencia;
    StateManager localStateManager = paramNombreOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramNombreOrganismo.fechaVigencia;
    if (localStateManager.isLoaded(paramNombreOrganismo, jdoInheritedFieldCount + 0))
      return paramNombreOrganismo.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramNombreOrganismo, jdoInheritedFieldCount + 0, paramNombreOrganismo.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(NombreOrganismo paramNombreOrganismo, Date paramDate)
  {
    if (paramNombreOrganismo.jdoFlags == 0)
    {
      paramNombreOrganismo.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramNombreOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramNombreOrganismo.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramNombreOrganismo, jdoInheritedFieldCount + 0, paramNombreOrganismo.fechaVigencia, paramDate);
  }

  private static final long jdoGetidNombreOrganismo(NombreOrganismo paramNombreOrganismo)
  {
    return paramNombreOrganismo.idNombreOrganismo;
  }

  private static final void jdoSetidNombreOrganismo(NombreOrganismo paramNombreOrganismo, long paramLong)
  {
    StateManager localStateManager = paramNombreOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramNombreOrganismo.idNombreOrganismo = paramLong;
      return;
    }
    localStateManager.setLongField(paramNombreOrganismo, jdoInheritedFieldCount + 1, paramNombreOrganismo.idNombreOrganismo, paramLong);
  }

  private static final String jdoGetnombre(NombreOrganismo paramNombreOrganismo)
  {
    if (paramNombreOrganismo.jdoFlags <= 0)
      return paramNombreOrganismo.nombre;
    StateManager localStateManager = paramNombreOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramNombreOrganismo.nombre;
    if (localStateManager.isLoaded(paramNombreOrganismo, jdoInheritedFieldCount + 2))
      return paramNombreOrganismo.nombre;
    return localStateManager.getStringField(paramNombreOrganismo, jdoInheritedFieldCount + 2, paramNombreOrganismo.nombre);
  }

  private static final void jdoSetnombre(NombreOrganismo paramNombreOrganismo, String paramString)
  {
    if (paramNombreOrganismo.jdoFlags == 0)
    {
      paramNombreOrganismo.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramNombreOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramNombreOrganismo.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramNombreOrganismo, jdoInheritedFieldCount + 2, paramNombreOrganismo.nombre, paramString);
  }

  private static final String jdoGetnombreCorto(NombreOrganismo paramNombreOrganismo)
  {
    if (paramNombreOrganismo.jdoFlags <= 0)
      return paramNombreOrganismo.nombreCorto;
    StateManager localStateManager = paramNombreOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramNombreOrganismo.nombreCorto;
    if (localStateManager.isLoaded(paramNombreOrganismo, jdoInheritedFieldCount + 3))
      return paramNombreOrganismo.nombreCorto;
    return localStateManager.getStringField(paramNombreOrganismo, jdoInheritedFieldCount + 3, paramNombreOrganismo.nombreCorto);
  }

  private static final void jdoSetnombreCorto(NombreOrganismo paramNombreOrganismo, String paramString)
  {
    if (paramNombreOrganismo.jdoFlags == 0)
    {
      paramNombreOrganismo.nombreCorto = paramString;
      return;
    }
    StateManager localStateManager = paramNombreOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramNombreOrganismo.nombreCorto = paramString;
      return;
    }
    localStateManager.setStringField(paramNombreOrganismo, jdoInheritedFieldCount + 3, paramNombreOrganismo.nombreCorto, paramString);
  }

  private static final Organismo jdoGetorganismo(NombreOrganismo paramNombreOrganismo)
  {
    StateManager localStateManager = paramNombreOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramNombreOrganismo.organismo;
    if (localStateManager.isLoaded(paramNombreOrganismo, jdoInheritedFieldCount + 4))
      return paramNombreOrganismo.organismo;
    return (Organismo)localStateManager.getObjectField(paramNombreOrganismo, jdoInheritedFieldCount + 4, paramNombreOrganismo.organismo);
  }

  private static final void jdoSetorganismo(NombreOrganismo paramNombreOrganismo, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramNombreOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramNombreOrganismo.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramNombreOrganismo, jdoInheritedFieldCount + 4, paramNombreOrganismo.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}