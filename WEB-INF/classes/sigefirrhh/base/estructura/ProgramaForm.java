package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class ProgramaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ProgramaForm.class.getName());
  private Programa programa;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showProgramaByCodPrograma;
  private boolean showProgramaByNombre;
  private String findCodPrograma;
  private String findNombre;
  private Object stateResultProgramaByCodPrograma = null;

  private Object stateResultProgramaByNombre = null;

  public String getFindCodPrograma()
  {
    return this.findCodPrograma;
  }
  public void setFindCodPrograma(String findCodPrograma) {
    this.findCodPrograma = findCodPrograma;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Programa getPrograma() {
    if (this.programa == null) {
      this.programa = new Programa();
    }
    return this.programa;
  }

  public ProgramaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findProgramaByCodPrograma()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findProgramaByCodPrograma(this.findCodPrograma, idOrganismo);
      this.showProgramaByCodPrograma = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showProgramaByCodPrograma)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodPrograma = null;
    this.findNombre = null;

    return null;
  }

  public String findProgramaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findProgramaByNombre(this.findNombre, idOrganismo);
      this.showProgramaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showProgramaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodPrograma = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowProgramaByCodPrograma() {
    return this.showProgramaByCodPrograma;
  }
  public boolean isShowProgramaByNombre() {
    return this.showProgramaByNombre;
  }

  public String selectPrograma()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPrograma = 
      Long.parseLong((String)requestParameterMap.get("idPrograma"));
    try
    {
      this.programa = 
        this.estructuraFacade.findProgramaById(
        idPrograma);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.programa = null;
    this.showProgramaByCodPrograma = false;
    this.showProgramaByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addPrograma(
          this.programa);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updatePrograma(
          this.programa);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deletePrograma(
        this.programa);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.programa = new Programa();

    this.programa.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.programa.setIdPrograma(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.Programa"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.programa = new Programa();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}