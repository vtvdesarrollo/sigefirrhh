package sigefirrhh.base.estructura;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class EstructuraFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();

  public void addTipoCaracteristica(TipoCaracteristica tipoCaracteristica)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addTipoCaracteristica(tipoCaracteristica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoCaracteristica(TipoCaracteristica tipoCaracteristica) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateTipoCaracteristica(tipoCaracteristica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoCaracteristica(TipoCaracteristica tipoCaracteristica) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteTipoCaracteristica(tipoCaracteristica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoCaracteristica findTipoCaracteristicaById(long tipoCaracteristicaId) throws Exception
  {
    try { this.txn.open();
      TipoCaracteristica tipoCaracteristica = 
        this.estructuraBusiness.findTipoCaracteristicaById(tipoCaracteristicaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoCaracteristica);
      return tipoCaracteristica;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoCaracteristica() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllTipoCaracteristica();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoCaracteristicaByCodTipoCaracteristica(String codTipoCaracteristica)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findTipoCaracteristicaByCodTipoCaracteristica(codTipoCaracteristica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoCaracteristicaByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findTipoCaracteristicaByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addClasificacionDependencia(ClasificacionDependencia clasificacionDependencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addClasificacionDependencia(clasificacionDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateClasificacionDependencia(ClasificacionDependencia clasificacionDependencia) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateClasificacionDependencia(clasificacionDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteClasificacionDependencia(ClasificacionDependencia clasificacionDependencia) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteClasificacionDependencia(clasificacionDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ClasificacionDependencia findClasificacionDependenciaById(long clasificacionDependenciaId) throws Exception
  {
    try { this.txn.open();
      ClasificacionDependencia clasificacionDependencia = 
        this.estructuraBusiness.findClasificacionDependenciaById(clasificacionDependenciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(clasificacionDependencia);
      return clasificacionDependencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllClasificacionDependencia() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllClasificacionDependencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findClasificacionDependenciaByCaracteristicaDependencia(long idCaracteristicaDependencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findClasificacionDependenciaByCaracteristicaDependencia(idCaracteristicaDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findClasificacionDependenciaByDependencia(long idDependencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findClasificacionDependenciaByDependencia(idDependencia);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCaracteristicaDependencia(CaracteristicaDependencia caracteristicaDependencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addCaracteristicaDependencia(caracteristicaDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCaracteristicaDependencia(CaracteristicaDependencia caracteristicaDependencia) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateCaracteristicaDependencia(caracteristicaDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCaracteristicaDependencia(CaracteristicaDependencia caracteristicaDependencia) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteCaracteristicaDependencia(caracteristicaDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CaracteristicaDependencia findCaracteristicaDependenciaById(long caracteristicaDependenciaId) throws Exception
  {
    try { this.txn.open();
      CaracteristicaDependencia caracteristicaDependencia = 
        this.estructuraBusiness.findCaracteristicaDependenciaById(caracteristicaDependenciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(caracteristicaDependencia);
      return caracteristicaDependencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCaracteristicaDependencia() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllCaracteristicaDependencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCaracteristicaDependenciaByTipoCaracteristica(long idTipoCaracteristica)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findCaracteristicaDependenciaByTipoCaracteristica(idTipoCaracteristica);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAdministradoraUel(AdministradoraUel administradoraUel)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addAdministradoraUel(administradoraUel);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAdministradoraUel(AdministradoraUel administradoraUel) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateAdministradoraUel(administradoraUel);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAdministradoraUel(AdministradoraUel administradoraUel) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteAdministradoraUel(administradoraUel);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public AdministradoraUel findAdministradoraUelById(long administradoraUelId) throws Exception
  {
    try { this.txn.open();
      AdministradoraUel administradoraUel = 
        this.estructuraBusiness.findAdministradoraUelById(administradoraUelId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(administradoraUel);
      return administradoraUel;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAdministradoraUel() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllAdministradoraUel();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAdministradoraUelByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findAdministradoraUelByUnidadAdministradora(idUnidadAdministradora);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDependencia(Dependencia dependencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addDependencia(dependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDependencia(Dependencia dependencia) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateDependencia(dependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDependencia(Dependencia dependencia) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteDependencia(dependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Dependencia findDependenciaById(long dependenciaId) throws Exception
  {
    try { this.txn.open();
      Dependencia dependencia = 
        this.estructuraBusiness.findDependenciaById(dependenciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(dependencia);
      return dependencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDependencia() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllDependencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDependenciaByCodDependencia(String codDependencia, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findDependenciaByCodDependencia(codDependencia, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDependenciaByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findDependenciaByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDependenciaByEstructura(long idEstructura, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findDependenciaByEstructura(idEstructura, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDependenciaByAdministradoraUel(long idAdministradoraUel, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findDependenciaByAdministradoraUel(idAdministradoraUel, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDependenciaByRegion(long idRegion, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findDependenciaByRegion(idRegion, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDependenciaBySede(long idSede, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findDependenciaBySede(idSede, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDependenciaByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findDependenciaByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addGrupoOrganismo(GrupoOrganismo grupoOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addGrupoOrganismo(grupoOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateGrupoOrganismo(GrupoOrganismo grupoOrganismo) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateGrupoOrganismo(grupoOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteGrupoOrganismo(GrupoOrganismo grupoOrganismo) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteGrupoOrganismo(grupoOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public GrupoOrganismo findGrupoOrganismoById(long grupoOrganismoId) throws Exception
  {
    try { this.txn.open();
      GrupoOrganismo grupoOrganismo = 
        this.estructuraBusiness.findGrupoOrganismoById(grupoOrganismoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(grupoOrganismo);
      return grupoOrganismo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllGrupoOrganismo() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllGrupoOrganismo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoOrganismoByCodGrupoOrganismo(String codGrupoOrganismo, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findGrupoOrganismoByCodGrupoOrganismo(codGrupoOrganismo, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoOrganismoByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findGrupoOrganismoByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoOrganismoByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findGrupoOrganismoByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addLugarPago(LugarPago lugarPago)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addLugarPago(lugarPago);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateLugarPago(LugarPago lugarPago) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateLugarPago(lugarPago);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteLugarPago(LugarPago lugarPago) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteLugarPago(lugarPago);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public LugarPago findLugarPagoById(long lugarPagoId) throws Exception
  {
    try { this.txn.open();
      LugarPago lugarPago = 
        this.estructuraBusiness.findLugarPagoById(lugarPagoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(lugarPago);
      return lugarPago;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllLugarPago() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllLugarPago();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findLugarPagoByCodLugarPago(String codLugarPago) throws Exception
  {
    try {
      this.txn.open();
      return this.estructuraBusiness.findLugarPagoByCodLugarPago(codLugarPago);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findLugarPagoByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findLugarPagoByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addNombreOrganismo(NombreOrganismo nombreOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addNombreOrganismo(nombreOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateNombreOrganismo(NombreOrganismo nombreOrganismo) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateNombreOrganismo(nombreOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteNombreOrganismo(NombreOrganismo nombreOrganismo) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteNombreOrganismo(nombreOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public NombreOrganismo findNombreOrganismoById(long nombreOrganismoId) throws Exception
  {
    try { this.txn.open();
      NombreOrganismo nombreOrganismo = 
        this.estructuraBusiness.findNombreOrganismoById(nombreOrganismoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(nombreOrganismo);
      return nombreOrganismo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllNombreOrganismo() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllNombreOrganismo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNombreOrganismoByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findNombreOrganismoByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNombreOrganismoByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findNombreOrganismoByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addOrganismo(Organismo organismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addOrganismo(organismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateOrganismo(Organismo organismo) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateOrganismo(organismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteOrganismo(Organismo organismo) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteOrganismo(organismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Organismo findOrganismoById(long organismoId) throws Exception
  {
    try { this.txn.open();
      Organismo organismo = 
        this.estructuraBusiness.findOrganismoById(organismoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(organismo);
      return organismo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllOrganismo() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllOrganismo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findOrganismoByCodOrganismo(String codOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findOrganismoByCodOrganismo(codOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findOrganismoByNombreOrganismo(String nombreOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findOrganismoByNombreOrganismo(nombreOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPrograma(Programa programa)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addPrograma(programa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePrograma(Programa programa) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updatePrograma(programa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePrograma(Programa programa) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deletePrograma(programa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Programa findProgramaById(long programaId) throws Exception
  {
    try { this.txn.open();
      Programa programa = 
        this.estructuraBusiness.findProgramaById(programaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(programa);
      return programa;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPrograma() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllPrograma();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProgramaByCodPrograma(String codPrograma, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findProgramaByCodPrograma(codPrograma, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProgramaByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findProgramaByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProgramaByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findProgramaByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRegion(Region region)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addRegion(region);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRegion(Region region) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateRegion(region);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRegion(Region region) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteRegion(region);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Region findRegionById(long regionId) throws Exception
  {
    try { this.txn.open();
      Region region = 
        this.estructuraBusiness.findRegionById(regionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(region);
      return region;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRegion() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllRegion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegionByCodRegion(String codRegion, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findRegionByCodRegion(codRegion, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegionByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findRegionByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegionByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findRegionByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSede(Sede sede)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addSede(sede);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSede(Sede sede) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateSede(sede);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSede(Sede sede) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteSede(sede);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Sede findSedeById(long sedeId) throws Exception
  {
    try { this.txn.open();
      Sede sede = 
        this.estructuraBusiness.findSedeById(sedeId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(sede);
      return sede;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSede() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllSede();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSedeByRegion(long idRegion, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findSedeByRegion(idRegion, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSedeByCodSede(String codSede, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findSedeByCodSede(codSede, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSedeByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findSedeByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSedeByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findSedeByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoDependencia(TipoDependencia tipoDependencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addTipoDependencia(tipoDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoDependencia(TipoDependencia tipoDependencia) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateTipoDependencia(tipoDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoDependencia(TipoDependencia tipoDependencia) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteTipoDependencia(tipoDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoDependencia findTipoDependenciaById(long tipoDependenciaId) throws Exception
  {
    try { this.txn.open();
      TipoDependencia tipoDependencia = 
        this.estructuraBusiness.findTipoDependenciaById(tipoDependenciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoDependencia);
      return tipoDependencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoDependencia() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllTipoDependencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoDependenciaByCodTipoDependencia(String codTipoDependencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findTipoDependenciaByCodTipoDependencia(codTipoDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoDependenciaByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findTipoDependenciaByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addUnidadAdministradora(UnidadAdministradora unidadAdministradora)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addUnidadAdministradora(unidadAdministradora);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateUnidadAdministradora(UnidadAdministradora unidadAdministradora) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateUnidadAdministradora(unidadAdministradora);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteUnidadAdministradora(UnidadAdministradora unidadAdministradora) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteUnidadAdministradora(unidadAdministradora);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public UnidadAdministradora findUnidadAdministradoraById(long unidadAdministradoraId) throws Exception
  {
    try { this.txn.open();
      UnidadAdministradora unidadAdministradora = 
        this.estructuraBusiness.findUnidadAdministradoraById(unidadAdministradoraId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(unidadAdministradora);
      return unidadAdministradora;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllUnidadAdministradora() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllUnidadAdministradora();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUnidadAdministradoraByCodUnidadAdminist(String codUnidadAdminist, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findUnidadAdministradoraByCodUnidadAdminist(codUnidadAdminist, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUnidadAdministradoraByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findUnidadAdministradoraByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUnidadAdministradoraByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findUnidadAdministradoraByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addUnidadEjecutora(UnidadEjecutora unidadEjecutora)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addUnidadEjecutora(unidadEjecutora);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateUnidadEjecutora(UnidadEjecutora unidadEjecutora) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateUnidadEjecutora(unidadEjecutora);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteUnidadEjecutora(UnidadEjecutora unidadEjecutora) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteUnidadEjecutora(unidadEjecutora);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public UnidadEjecutora findUnidadEjecutoraById(long unidadEjecutoraId) throws Exception
  {
    try { this.txn.open();
      UnidadEjecutora unidadEjecutora = 
        this.estructuraBusiness.findUnidadEjecutoraById(unidadEjecutoraId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(unidadEjecutora);
      return unidadEjecutora;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllUnidadEjecutora() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllUnidadEjecutora();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUnidadEjecutoraByCodUnidadEjecutora(String codUnidadEjecutora)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findUnidadEjecutoraByCodUnidadEjecutora(codUnidadEjecutora);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUnidadEjecutoraByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findUnidadEjecutoraByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addUnidadFuncional(UnidadFuncional unidadFuncional)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addUnidadFuncional(unidadFuncional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateUnidadFuncional(UnidadFuncional unidadFuncional) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateUnidadFuncional(unidadFuncional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteUnidadFuncional(UnidadFuncional unidadFuncional) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteUnidadFuncional(unidadFuncional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public UnidadFuncional findUnidadFuncionalById(long unidadFuncionalId) throws Exception
  {
    try { this.txn.open();
      UnidadFuncional unidadFuncional = 
        this.estructuraBusiness.findUnidadFuncionalById(unidadFuncionalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(unidadFuncional);
      return unidadFuncional;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllUnidadFuncional() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllUnidadFuncional();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUnidadFuncionalByCodUnidadFuncional(String codUnidadFuncional, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findUnidadFuncionalByCodUnidadFuncional(codUnidadFuncional, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUnidadFuncionalByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findUnidadFuncionalByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUnidadFuncionalByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findUnidadFuncionalByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEstructura(Estructura estructura)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.estructuraBusiness.addEstructura(estructura);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEstructura(Estructura estructura) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.updateEstructura(estructura);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEstructura(Estructura estructura) throws Exception
  {
    try { this.txn.open();
      this.estructuraBusiness.deleteEstructura(estructura);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Estructura findEstructuraById(long estructuraId) throws Exception
  {
    try { this.txn.open();
      Estructura estructura = 
        this.estructuraBusiness.findEstructuraById(estructuraId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(estructura);
      return estructura;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEstructura() throws Exception
  {
    try { this.txn.open();
      return this.estructuraBusiness.findAllEstructura();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEstructuraByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findEstructuraByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEstructuraByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.estructuraBusiness.findEstructuraByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}