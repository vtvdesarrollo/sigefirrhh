package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class CaracteristicaDependencia
  implements Serializable, PersistenceCapable
{
  private long idCaracteristicaDependencia;
  private TipoCaracteristica tipoCaracteristica;
  private String codigo;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codigo", "idCaracteristicaDependencia", "nombre", "tipoCaracteristica" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.TipoCaracteristica") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGettipoCaracteristica(this).getNombre() + " - " + jdoGetnombre(this) + " - " + 
      jdoGetcodigo(this);
  }

  public String getCodigo()
  {
    return jdoGetcodigo(this);
  }
  public void setCodigo(String codigo) {
    jdoSetcodigo(this, codigo);
  }
  public long getIdCaracteristicaDependencia() {
    return jdoGetidCaracteristicaDependencia(this);
  }
  public void setIdCaracteristicaDependencia(long idCaracteristicaDependencia) {
    jdoSetidCaracteristicaDependencia(this, idCaracteristicaDependencia);
  }
  public String getNombre() {
    return jdoGetnombre(this);
  }
  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }
  public TipoCaracteristica getTipoCaracteristica() {
    return jdoGettipoCaracteristica(this);
  }
  public void setTipoCaracteristica(TipoCaracteristica tipoCaracteristica) {
    jdoSettipoCaracteristica(this, tipoCaracteristica);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.CaracteristicaDependencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CaracteristicaDependencia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CaracteristicaDependencia localCaracteristicaDependencia = new CaracteristicaDependencia();
    localCaracteristicaDependencia.jdoFlags = 1;
    localCaracteristicaDependencia.jdoStateManager = paramStateManager;
    return localCaracteristicaDependencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CaracteristicaDependencia localCaracteristicaDependencia = new CaracteristicaDependencia();
    localCaracteristicaDependencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCaracteristicaDependencia.jdoFlags = 1;
    localCaracteristicaDependencia.jdoStateManager = paramStateManager;
    return localCaracteristicaDependencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCaracteristicaDependencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoCaracteristica);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCaracteristicaDependencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCaracteristica = ((TipoCaracteristica)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CaracteristicaDependencia paramCaracteristicaDependencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCaracteristicaDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.codigo = paramCaracteristicaDependencia.codigo;
      return;
    case 1:
      if (paramCaracteristicaDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.idCaracteristicaDependencia = paramCaracteristicaDependencia.idCaracteristicaDependencia;
      return;
    case 2:
      if (paramCaracteristicaDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramCaracteristicaDependencia.nombre;
      return;
    case 3:
      if (paramCaracteristicaDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCaracteristica = paramCaracteristicaDependencia.tipoCaracteristica;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CaracteristicaDependencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CaracteristicaDependencia localCaracteristicaDependencia = (CaracteristicaDependencia)paramObject;
    if (localCaracteristicaDependencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCaracteristicaDependencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CaracteristicaDependenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CaracteristicaDependenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CaracteristicaDependenciaPK))
      throw new IllegalArgumentException("arg1");
    CaracteristicaDependenciaPK localCaracteristicaDependenciaPK = (CaracteristicaDependenciaPK)paramObject;
    localCaracteristicaDependenciaPK.idCaracteristicaDependencia = this.idCaracteristicaDependencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CaracteristicaDependenciaPK))
      throw new IllegalArgumentException("arg1");
    CaracteristicaDependenciaPK localCaracteristicaDependenciaPK = (CaracteristicaDependenciaPK)paramObject;
    this.idCaracteristicaDependencia = localCaracteristicaDependenciaPK.idCaracteristicaDependencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CaracteristicaDependenciaPK))
      throw new IllegalArgumentException("arg2");
    CaracteristicaDependenciaPK localCaracteristicaDependenciaPK = (CaracteristicaDependenciaPK)paramObject;
    localCaracteristicaDependenciaPK.idCaracteristicaDependencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CaracteristicaDependenciaPK))
      throw new IllegalArgumentException("arg2");
    CaracteristicaDependenciaPK localCaracteristicaDependenciaPK = (CaracteristicaDependenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localCaracteristicaDependenciaPK.idCaracteristicaDependencia);
  }

  private static final String jdoGetcodigo(CaracteristicaDependencia paramCaracteristicaDependencia)
  {
    if (paramCaracteristicaDependencia.jdoFlags <= 0)
      return paramCaracteristicaDependencia.codigo;
    StateManager localStateManager = paramCaracteristicaDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramCaracteristicaDependencia.codigo;
    if (localStateManager.isLoaded(paramCaracteristicaDependencia, jdoInheritedFieldCount + 0))
      return paramCaracteristicaDependencia.codigo;
    return localStateManager.getStringField(paramCaracteristicaDependencia, jdoInheritedFieldCount + 0, paramCaracteristicaDependencia.codigo);
  }

  private static final void jdoSetcodigo(CaracteristicaDependencia paramCaracteristicaDependencia, String paramString)
  {
    if (paramCaracteristicaDependencia.jdoFlags == 0)
    {
      paramCaracteristicaDependencia.codigo = paramString;
      return;
    }
    StateManager localStateManager = paramCaracteristicaDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramCaracteristicaDependencia.codigo = paramString;
      return;
    }
    localStateManager.setStringField(paramCaracteristicaDependencia, jdoInheritedFieldCount + 0, paramCaracteristicaDependencia.codigo, paramString);
  }

  private static final long jdoGetidCaracteristicaDependencia(CaracteristicaDependencia paramCaracteristicaDependencia)
  {
    return paramCaracteristicaDependencia.idCaracteristicaDependencia;
  }

  private static final void jdoSetidCaracteristicaDependencia(CaracteristicaDependencia paramCaracteristicaDependencia, long paramLong)
  {
    StateManager localStateManager = paramCaracteristicaDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramCaracteristicaDependencia.idCaracteristicaDependencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramCaracteristicaDependencia, jdoInheritedFieldCount + 1, paramCaracteristicaDependencia.idCaracteristicaDependencia, paramLong);
  }

  private static final String jdoGetnombre(CaracteristicaDependencia paramCaracteristicaDependencia)
  {
    if (paramCaracteristicaDependencia.jdoFlags <= 0)
      return paramCaracteristicaDependencia.nombre;
    StateManager localStateManager = paramCaracteristicaDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramCaracteristicaDependencia.nombre;
    if (localStateManager.isLoaded(paramCaracteristicaDependencia, jdoInheritedFieldCount + 2))
      return paramCaracteristicaDependencia.nombre;
    return localStateManager.getStringField(paramCaracteristicaDependencia, jdoInheritedFieldCount + 2, paramCaracteristicaDependencia.nombre);
  }

  private static final void jdoSetnombre(CaracteristicaDependencia paramCaracteristicaDependencia, String paramString)
  {
    if (paramCaracteristicaDependencia.jdoFlags == 0)
    {
      paramCaracteristicaDependencia.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramCaracteristicaDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramCaracteristicaDependencia.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramCaracteristicaDependencia, jdoInheritedFieldCount + 2, paramCaracteristicaDependencia.nombre, paramString);
  }

  private static final TipoCaracteristica jdoGettipoCaracteristica(CaracteristicaDependencia paramCaracteristicaDependencia)
  {
    StateManager localStateManager = paramCaracteristicaDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramCaracteristicaDependencia.tipoCaracteristica;
    if (localStateManager.isLoaded(paramCaracteristicaDependencia, jdoInheritedFieldCount + 3))
      return paramCaracteristicaDependencia.tipoCaracteristica;
    return (TipoCaracteristica)localStateManager.getObjectField(paramCaracteristicaDependencia, jdoInheritedFieldCount + 3, paramCaracteristicaDependencia.tipoCaracteristica);
  }

  private static final void jdoSettipoCaracteristica(CaracteristicaDependencia paramCaracteristicaDependencia, TipoCaracteristica paramTipoCaracteristica)
  {
    StateManager localStateManager = paramCaracteristicaDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramCaracteristicaDependencia.tipoCaracteristica = paramTipoCaracteristica;
      return;
    }
    localStateManager.setObjectField(paramCaracteristicaDependencia, jdoInheritedFieldCount + 3, paramCaracteristicaDependencia.tipoCaracteristica, paramTipoCaracteristica);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}