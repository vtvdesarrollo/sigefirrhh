package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class DependenciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDependencia(Dependencia dependencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Dependencia dependenciaNew = 
      (Dependencia)BeanUtils.cloneBean(
      dependencia);

    EstructuraBeanBusiness estructuraBeanBusiness = new EstructuraBeanBusiness();

    if (dependenciaNew.getEstructura() != null) {
      dependenciaNew.setEstructura(
        estructuraBeanBusiness.findEstructuraById(
        dependenciaNew.getEstructura().getIdEstructura()));
    }

    AdministradoraUelBeanBusiness administradoraUelBeanBusiness = new AdministradoraUelBeanBusiness();

    if (dependenciaNew.getAdministradoraUel() != null) {
      dependenciaNew.setAdministradoraUel(
        administradoraUelBeanBusiness.findAdministradoraUelById(
        dependenciaNew.getAdministradoraUel().getIdAdministradoraUel()));
    }

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (dependenciaNew.getRegion() != null) {
      dependenciaNew.setRegion(
        regionBeanBusiness.findRegionById(
        dependenciaNew.getRegion().getIdRegion()));
    }

    SedeBeanBusiness sedeBeanBusiness = new SedeBeanBusiness();

    if (dependenciaNew.getSede() != null) {
      dependenciaNew.setSede(
        sedeBeanBusiness.findSedeById(
        dependenciaNew.getSede().getIdSede()));
    }

    UnidadFuncionalBeanBusiness unidadFuncionalBeanBusiness = new UnidadFuncionalBeanBusiness();

    if (dependenciaNew.getUnidadFuncional() != null) {
      dependenciaNew.setUnidadFuncional(
        unidadFuncionalBeanBusiness.findUnidadFuncionalById(
        dependenciaNew.getUnidadFuncional().getIdUnidadFuncional()));
    }

    TipoDependenciaBeanBusiness tipoDependenciaBeanBusiness = new TipoDependenciaBeanBusiness();

    if (dependenciaNew.getTipoDependencia() != null) {
      dependenciaNew.setTipoDependencia(
        tipoDependenciaBeanBusiness.findTipoDependenciaById(
        dependenciaNew.getTipoDependencia().getIdTipoDependencia()));
    }

    GrupoOrganismoBeanBusiness grupoOrganismoBeanBusiness = new GrupoOrganismoBeanBusiness();

    if (dependenciaNew.getGrupoOrganismo() != null) {
      dependenciaNew.setGrupoOrganismo(
        grupoOrganismoBeanBusiness.findGrupoOrganismoById(
        dependenciaNew.getGrupoOrganismo().getIdGrupoOrganismo()));
    }

    DependenciaBeanBusiness dependenciaAnteriorBeanBusiness = new DependenciaBeanBusiness();

    if (dependenciaNew.getDependenciaAnterior() != null) {
      dependenciaNew.setDependenciaAnterior(
        dependenciaAnteriorBeanBusiness.findDependenciaById(
        dependenciaNew.getDependenciaAnterior().getIdDependencia()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (dependenciaNew.getOrganismo() != null) {
      dependenciaNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        dependenciaNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(dependenciaNew);
  }

  public void updateDependencia(Dependencia dependencia) throws Exception
  {
    Dependencia dependenciaModify = 
      findDependenciaById(dependencia.getIdDependencia());

    EstructuraBeanBusiness estructuraBeanBusiness = new EstructuraBeanBusiness();

    if (dependencia.getEstructura() != null) {
      dependencia.setEstructura(
        estructuraBeanBusiness.findEstructuraById(
        dependencia.getEstructura().getIdEstructura()));
    }

    AdministradoraUelBeanBusiness administradoraUelBeanBusiness = new AdministradoraUelBeanBusiness();

    if (dependencia.getAdministradoraUel() != null) {
      dependencia.setAdministradoraUel(
        administradoraUelBeanBusiness.findAdministradoraUelById(
        dependencia.getAdministradoraUel().getIdAdministradoraUel()));
    }

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (dependencia.getRegion() != null) {
      dependencia.setRegion(
        regionBeanBusiness.findRegionById(
        dependencia.getRegion().getIdRegion()));
    }

    SedeBeanBusiness sedeBeanBusiness = new SedeBeanBusiness();

    if (dependencia.getSede() != null) {
      dependencia.setSede(
        sedeBeanBusiness.findSedeById(
        dependencia.getSede().getIdSede()));
    }

    UnidadFuncionalBeanBusiness unidadFuncionalBeanBusiness = new UnidadFuncionalBeanBusiness();

    if (dependencia.getUnidadFuncional() != null) {
      dependencia.setUnidadFuncional(
        unidadFuncionalBeanBusiness.findUnidadFuncionalById(
        dependencia.getUnidadFuncional().getIdUnidadFuncional()));
    }

    TipoDependenciaBeanBusiness tipoDependenciaBeanBusiness = new TipoDependenciaBeanBusiness();

    if (dependencia.getTipoDependencia() != null) {
      dependencia.setTipoDependencia(
        tipoDependenciaBeanBusiness.findTipoDependenciaById(
        dependencia.getTipoDependencia().getIdTipoDependencia()));
    }

    GrupoOrganismoBeanBusiness grupoOrganismoBeanBusiness = new GrupoOrganismoBeanBusiness();

    if (dependencia.getGrupoOrganismo() != null) {
      dependencia.setGrupoOrganismo(
        grupoOrganismoBeanBusiness.findGrupoOrganismoById(
        dependencia.getGrupoOrganismo().getIdGrupoOrganismo()));
    }

    DependenciaBeanBusiness dependenciaAnteriorBeanBusiness = new DependenciaBeanBusiness();

    if (dependencia.getDependenciaAnterior() != null) {
      dependencia.setDependenciaAnterior(
        dependenciaAnteriorBeanBusiness.findDependenciaById(
        dependencia.getDependenciaAnterior().getIdDependencia()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (dependencia.getOrganismo() != null) {
      dependencia.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        dependencia.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(dependenciaModify, dependencia);
  }

  public void deleteDependencia(Dependencia dependencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Dependencia dependenciaDelete = 
      findDependenciaById(dependencia.getIdDependencia());
    pm.deletePersistent(dependenciaDelete);
  }

  public Dependencia findDependenciaById(long idDependencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDependencia == pIdDependencia";
    Query query = pm.newQuery(Dependencia.class, filter);

    query.declareParameters("long pIdDependencia");

    parameters.put("pIdDependencia", new Long(idDependencia));

    Collection colDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDependencia.iterator();
    return (Dependencia)iterator.next();
  }

  public Collection findDependenciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent dependenciaExtent = pm.getExtent(
      Dependencia.class, true);
    Query query = pm.newQuery(dependenciaExtent);
    query.setOrdering("codDependencia ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodDependencia(String codDependencia, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codDependencia == pCodDependencia &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Dependencia.class, filter);

    query.declareParameters("java.lang.String pCodDependencia, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodDependencia", new String(codDependencia));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDependencia ascending");

    Collection colDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDependencia);

    return colDependencia;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Dependencia.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDependencia ascending");

    Collection colDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDependencia);

    return colDependencia;
  }

  public Collection findByEstructura(long idEstructura, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "estructura.idEstructura == pIdEstructura &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Dependencia.class, filter);

    query.declareParameters("long pIdEstructura, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdEstructura", new Long(idEstructura));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDependencia ascending");

    Collection colDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDependencia);

    return colDependencia;
  }

  public Collection findByAdministradoraUel(long idAdministradoraUel, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "administradoraUel.idAdministradoraUel == pIdAdministradoraUel &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Dependencia.class, filter);

    query.declareParameters("long pIdAdministradoraUel, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdAdministradoraUel", new Long(idAdministradoraUel));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDependencia ascending");

    Collection colDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDependencia);

    return colDependencia;
  }

  public Collection findByRegion(long idRegion, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "region.idRegion == pIdRegion &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Dependencia.class, filter);

    query.declareParameters("long pIdRegion, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegion", new Long(idRegion));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDependencia ascending");

    Collection colDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDependencia);

    return colDependencia;
  }

  public Collection findBySede(long idSede, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "sede.idSede == pIdSede &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Dependencia.class, filter);

    query.declareParameters("long pIdSede, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdSede", new Long(idSede));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDependencia ascending");

    Collection colDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDependencia);

    return colDependencia;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Dependencia.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codDependencia ascending");

    Collection colDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDependencia);

    return colDependencia;
  }
}