package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ClasificacionDependenciaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ClasificacionDependenciaForm.class.getName());
  private ClasificacionDependencia clasificacionDependencia;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraNoGenFacade estructuraFacade = new EstructuraNoGenFacade();
  private boolean showClasificacionDependenciaByCaracteristicaDependencia;
  private boolean showClasificacionDependenciaByDependencia;
  private String findSelectCaracteristicaDependencia;
  private String findSelectDependencia;
  private Collection findColCaracteristicaDependencia;
  private Collection findColDependencia;
  private Collection colCaracteristicaDependencia;
  private Collection colDependencia;
  private String selectCaracteristicaDependencia;
  private String selectDependencia;
  private Dependencia dependencia;
  private long findIdRegion;
  private String findCodDependencia;
  private Collection findColRegion;
  private Object stateResultClasificacionDependenciaByCaracteristicaDependencia = null;

  private Object stateResultClasificacionDependenciaByDependencia = null;

  public boolean isShowFindDependencia()
  {
    return this.dependencia != null;
  }
  public Collection getFindColRegion() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }
  public String findDependenciaByCodigoAndRegion() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.dependencia = null;
      this.dependencia = this.estructuraFacade.findDependenciaByCodigoAndRegion(this.findCodDependencia, this.findIdRegion);
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Dependencia no existe", ""));
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String getFindSelectCaracteristicaDependencia() {
    return this.findSelectCaracteristicaDependencia;
  }
  public void setFindSelectCaracteristicaDependencia(String valCaracteristicaDependencia) {
    this.findSelectCaracteristicaDependencia = valCaracteristicaDependencia;
  }

  public Collection getFindColCaracteristicaDependencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCaracteristicaDependencia.iterator();
    CaracteristicaDependencia caracteristicaDependencia = null;
    while (iterator.hasNext()) {
      caracteristicaDependencia = (CaracteristicaDependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(caracteristicaDependencia.getIdCaracteristicaDependencia()), 
        caracteristicaDependencia.toString()));
    }
    return col;
  }
  public String getFindSelectDependencia() {
    return this.findSelectDependencia;
  }
  public void setFindSelectDependencia(String valDependencia) {
    this.findSelectDependencia = valDependencia;
  }

  public Collection getFindColDependencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }

  public String getSelectCaracteristicaDependencia()
  {
    return this.selectCaracteristicaDependencia;
  }
  public void setSelectCaracteristicaDependencia(String valCaracteristicaDependencia) {
    Iterator iterator = this.colCaracteristicaDependencia.iterator();
    CaracteristicaDependencia caracteristicaDependencia = null;
    this.clasificacionDependencia.setCaracteristicaDependencia(null);
    while (iterator.hasNext()) {
      caracteristicaDependencia = (CaracteristicaDependencia)iterator.next();
      if (String.valueOf(caracteristicaDependencia.getIdCaracteristicaDependencia()).equals(
        valCaracteristicaDependencia)) {
        this.clasificacionDependencia.setCaracteristicaDependencia(
          caracteristicaDependencia);
        break;
      }
    }
    this.selectCaracteristicaDependencia = valCaracteristicaDependencia;
  }
  public String getSelectDependencia() {
    return this.selectDependencia;
  }
  public void setSelectDependencia(String valDependencia) {
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    this.clasificacionDependencia.setDependencia(null);
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      if (String.valueOf(dependencia.getIdDependencia()).equals(
        valDependencia)) {
        this.clasificacionDependencia.setDependencia(
          dependencia);
        break;
      }
    }
    this.selectDependencia = valDependencia;
  }
  public Collection getResult() {
    return this.result;
  }

  public ClasificacionDependencia getClasificacionDependencia() {
    if (this.clasificacionDependencia == null) {
      this.clasificacionDependencia = new ClasificacionDependencia();
    }
    return this.clasificacionDependencia;
  }

  public ClasificacionDependenciaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColCaracteristicaDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCaracteristicaDependencia.iterator();
    CaracteristicaDependencia caracteristicaDependencia = null;
    while (iterator.hasNext()) {
      caracteristicaDependencia = (CaracteristicaDependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(caracteristicaDependencia.getIdCaracteristicaDependencia()), 
        caracteristicaDependencia.toString()));
    }
    return col;
  }

  public Collection getColDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColCaracteristicaDependencia = 
        this.estructuraFacade.findAllCaracteristicaDependencia();

      this.findColRegion = 
        this.estructuraFacade.findAllRegion();

      this.colCaracteristicaDependencia = 
        this.estructuraFacade.findAllCaracteristicaDependencia();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findClasificacionDependenciaByCaracteristicaDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findClasificacionDependenciaByCaracteristicaDependencia(Long.valueOf(this.findSelectCaracteristicaDependencia).longValue());
      this.showClasificacionDependenciaByCaracteristicaDependencia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showClasificacionDependenciaByCaracteristicaDependencia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectCaracteristicaDependencia = null;
    this.findSelectDependencia = null;

    return null;
  }

  public String findClasificacionDependenciaByDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findClasificacionDependenciaByDependencia(this.dependencia.getIdDependencia());
      log.error("SIZE......... " + this.result.size());

      this.showClasificacionDependenciaByDependencia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showClasificacionDependenciaByDependencia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectCaracteristicaDependencia = null;
    this.findSelectDependencia = null;

    return null;
  }

  public boolean isShowClasificacionDependenciaByCaracteristicaDependencia() {
    return this.showClasificacionDependenciaByCaracteristicaDependencia;
  }
  public boolean isShowClasificacionDependenciaByDependencia() {
    return this.showClasificacionDependenciaByDependencia;
  }

  public String selectClasificacionDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectCaracteristicaDependencia = null;
    this.selectDependencia = null;

    long idClasificacionDependencia = 
      Long.parseLong((String)requestParameterMap.get("idClasificacionDependencia"));
    try
    {
      this.clasificacionDependencia = 
        this.estructuraFacade.findClasificacionDependenciaById(
        idClasificacionDependencia);
      if (this.clasificacionDependencia.getCaracteristicaDependencia() != null) {
        this.selectCaracteristicaDependencia = 
          String.valueOf(this.clasificacionDependencia.getCaracteristicaDependencia().getIdCaracteristicaDependencia());
      }

      this.dependencia = this.clasificacionDependencia.getDependencia();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.clasificacionDependencia = null;
    this.showClasificacionDependenciaByCaracteristicaDependencia = false;
    this.showClasificacionDependenciaByDependencia = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        if (this.dependencia == null) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe Asignar una Entidad", ""));
          return "cancel";
        }
        this.clasificacionDependencia.setDependencia(this.dependencia);
        this.estructuraFacade.addClasificacionDependencia(
          this.clasificacionDependencia);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.clasificacionDependencia);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateClasificacionDependencia(
          this.clasificacionDependencia);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.clasificacionDependencia);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteClasificacionDependencia(
        this.clasificacionDependencia);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.clasificacionDependencia);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.clasificacionDependencia = new ClasificacionDependencia();

    this.selectCaracteristicaDependencia = null;

    this.selectDependencia = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.clasificacionDependencia.setIdClasificacionDependencia(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.ClasificacionDependencia"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.clasificacionDependencia = new ClasificacionDependencia();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public Dependencia getDependencia()
  {
    return this.dependencia;
  }
  public void setDependencia(Dependencia dependencia) {
    this.dependencia = dependencia;
  }

  public long getFindIdRegion() {
    return this.findIdRegion;
  }
  public void setFindIdRegion(long findIdRegion) {
    this.findIdRegion = findIdRegion;
  }
  public String getFindCodDependencia() {
    return this.findCodDependencia;
  }
  public void setFindCodDependencia(String findCodDependencia) {
    this.findCodDependencia = findCodDependencia;
  }
}