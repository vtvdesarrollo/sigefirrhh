package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class AdministradoraUel
  implements Serializable, PersistenceCapable
{
  private long idAdministradoraUel;
  private UnidadAdministradora unidadAdministradora;
  private UnidadEjecutora unidadEjecutora;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idAdministradoraUel", "unidadAdministradora", "unidadEjecutora" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.UnidadAdministradora"), sunjdo$classForName$("sigefirrhh.base.estructura.UnidadEjecutora") };
  private static final byte[] jdoFieldFlags = { 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return "U.ADM: " + jdoGetunidadAdministradora(this).getCodUnidadAdminist() + 
      " - UEL: " + jdoGetunidadEjecutora(this).getCodUnidadEjecutora() + " " + 
      jdoGetunidadEjecutora(this).getNombre();
  }

  public long getIdAdministradoraUel() {
    return jdoGetidAdministradoraUel(this);
  }

  public void setIdAdministradoraUel(long idAdministradoraUel) {
    jdoSetidAdministradoraUel(this, idAdministradoraUel);
  }

  public UnidadAdministradora getUnidadAdministradora() {
    return jdoGetunidadAdministradora(this);
  }

  public void setUnidadAdministradora(UnidadAdministradora unidadAdministradora) {
    jdoSetunidadAdministradora(this, unidadAdministradora);
  }

  public UnidadEjecutora getUnidadEjecutora() {
    return jdoGetunidadEjecutora(this);
  }

  public void setUnidadEjecutora(UnidadEjecutora unidadEjecutora) {
    jdoSetunidadEjecutora(this, unidadEjecutora);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.AdministradoraUel"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AdministradoraUel());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AdministradoraUel localAdministradoraUel = new AdministradoraUel();
    localAdministradoraUel.jdoFlags = 1;
    localAdministradoraUel.jdoStateManager = paramStateManager;
    return localAdministradoraUel;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AdministradoraUel localAdministradoraUel = new AdministradoraUel();
    localAdministradoraUel.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAdministradoraUel.jdoFlags = 1;
    localAdministradoraUel.jdoStateManager = paramStateManager;
    return localAdministradoraUel;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAdministradoraUel);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadAdministradora);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadEjecutora);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAdministradoraUel = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadAdministradora = ((UnidadAdministradora)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadEjecutora = ((UnidadEjecutora)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AdministradoraUel paramAdministradoraUel, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAdministradoraUel == null)
        throw new IllegalArgumentException("arg1");
      this.idAdministradoraUel = paramAdministradoraUel.idAdministradoraUel;
      return;
    case 1:
      if (paramAdministradoraUel == null)
        throw new IllegalArgumentException("arg1");
      this.unidadAdministradora = paramAdministradoraUel.unidadAdministradora;
      return;
    case 2:
      if (paramAdministradoraUel == null)
        throw new IllegalArgumentException("arg1");
      this.unidadEjecutora = paramAdministradoraUel.unidadEjecutora;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AdministradoraUel))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AdministradoraUel localAdministradoraUel = (AdministradoraUel)paramObject;
    if (localAdministradoraUel.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAdministradoraUel, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AdministradoraUelPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AdministradoraUelPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AdministradoraUelPK))
      throw new IllegalArgumentException("arg1");
    AdministradoraUelPK localAdministradoraUelPK = (AdministradoraUelPK)paramObject;
    localAdministradoraUelPK.idAdministradoraUel = this.idAdministradoraUel;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AdministradoraUelPK))
      throw new IllegalArgumentException("arg1");
    AdministradoraUelPK localAdministradoraUelPK = (AdministradoraUelPK)paramObject;
    this.idAdministradoraUel = localAdministradoraUelPK.idAdministradoraUel;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AdministradoraUelPK))
      throw new IllegalArgumentException("arg2");
    AdministradoraUelPK localAdministradoraUelPK = (AdministradoraUelPK)paramObject;
    localAdministradoraUelPK.idAdministradoraUel = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AdministradoraUelPK))
      throw new IllegalArgumentException("arg2");
    AdministradoraUelPK localAdministradoraUelPK = (AdministradoraUelPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localAdministradoraUelPK.idAdministradoraUel);
  }

  private static final long jdoGetidAdministradoraUel(AdministradoraUel paramAdministradoraUel)
  {
    return paramAdministradoraUel.idAdministradoraUel;
  }

  private static final void jdoSetidAdministradoraUel(AdministradoraUel paramAdministradoraUel, long paramLong)
  {
    StateManager localStateManager = paramAdministradoraUel.jdoStateManager;
    if (localStateManager == null)
    {
      paramAdministradoraUel.idAdministradoraUel = paramLong;
      return;
    }
    localStateManager.setLongField(paramAdministradoraUel, jdoInheritedFieldCount + 0, paramAdministradoraUel.idAdministradoraUel, paramLong);
  }

  private static final UnidadAdministradora jdoGetunidadAdministradora(AdministradoraUel paramAdministradoraUel)
  {
    StateManager localStateManager = paramAdministradoraUel.jdoStateManager;
    if (localStateManager == null)
      return paramAdministradoraUel.unidadAdministradora;
    if (localStateManager.isLoaded(paramAdministradoraUel, jdoInheritedFieldCount + 1))
      return paramAdministradoraUel.unidadAdministradora;
    return (UnidadAdministradora)localStateManager.getObjectField(paramAdministradoraUel, jdoInheritedFieldCount + 1, paramAdministradoraUel.unidadAdministradora);
  }

  private static final void jdoSetunidadAdministradora(AdministradoraUel paramAdministradoraUel, UnidadAdministradora paramUnidadAdministradora)
  {
    StateManager localStateManager = paramAdministradoraUel.jdoStateManager;
    if (localStateManager == null)
    {
      paramAdministradoraUel.unidadAdministradora = paramUnidadAdministradora;
      return;
    }
    localStateManager.setObjectField(paramAdministradoraUel, jdoInheritedFieldCount + 1, paramAdministradoraUel.unidadAdministradora, paramUnidadAdministradora);
  }

  private static final UnidadEjecutora jdoGetunidadEjecutora(AdministradoraUel paramAdministradoraUel)
  {
    StateManager localStateManager = paramAdministradoraUel.jdoStateManager;
    if (localStateManager == null)
      return paramAdministradoraUel.unidadEjecutora;
    if (localStateManager.isLoaded(paramAdministradoraUel, jdoInheritedFieldCount + 2))
      return paramAdministradoraUel.unidadEjecutora;
    return (UnidadEjecutora)localStateManager.getObjectField(paramAdministradoraUel, jdoInheritedFieldCount + 2, paramAdministradoraUel.unidadEjecutora);
  }

  private static final void jdoSetunidadEjecutora(AdministradoraUel paramAdministradoraUel, UnidadEjecutora paramUnidadEjecutora)
  {
    StateManager localStateManager = paramAdministradoraUel.jdoStateManager;
    if (localStateManager == null)
    {
      paramAdministradoraUel.unidadEjecutora = paramUnidadEjecutora;
      return;
    }
    localStateManager.setObjectField(paramAdministradoraUel, jdoInheritedFieldCount + 2, paramAdministradoraUel.unidadEjecutora, paramUnidadEjecutora);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}