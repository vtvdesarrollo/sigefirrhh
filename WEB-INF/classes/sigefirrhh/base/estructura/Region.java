package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Region
  implements Serializable, PersistenceCapable
{
  private long idRegion;
  private String codRegion;
  private String nombre;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codRegion", "idRegion", "nombre", "organismo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + jdoGetcodRegion(this);
  }

  public String getCodRegion()
  {
    return jdoGetcodRegion(this);
  }

  public void setCodRegion(String codRegion)
  {
    jdoSetcodRegion(this, codRegion);
  }

  public long getIdRegion()
  {
    return jdoGetidRegion(this);
  }

  public void setIdRegion(long idRegion)
  {
    jdoSetidRegion(this, idRegion);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.Region"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Region());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Region localRegion = new Region();
    localRegion.jdoFlags = 1;
    localRegion.jdoStateManager = paramStateManager;
    return localRegion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Region localRegion = new Region();
    localRegion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRegion.jdoFlags = 1;
    localRegion.jdoStateManager = paramStateManager;
    return localRegion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codRegion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRegion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRegion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Region paramRegion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRegion == null)
        throw new IllegalArgumentException("arg1");
      this.codRegion = paramRegion.codRegion;
      return;
    case 1:
      if (paramRegion == null)
        throw new IllegalArgumentException("arg1");
      this.idRegion = paramRegion.idRegion;
      return;
    case 2:
      if (paramRegion == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramRegion.nombre;
      return;
    case 3:
      if (paramRegion == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramRegion.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Region))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Region localRegion = (Region)paramObject;
    if (localRegion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRegion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RegionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RegionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegionPK))
      throw new IllegalArgumentException("arg1");
    RegionPK localRegionPK = (RegionPK)paramObject;
    localRegionPK.idRegion = this.idRegion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegionPK))
      throw new IllegalArgumentException("arg1");
    RegionPK localRegionPK = (RegionPK)paramObject;
    this.idRegion = localRegionPK.idRegion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegionPK))
      throw new IllegalArgumentException("arg2");
    RegionPK localRegionPK = (RegionPK)paramObject;
    localRegionPK.idRegion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegionPK))
      throw new IllegalArgumentException("arg2");
    RegionPK localRegionPK = (RegionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localRegionPK.idRegion);
  }

  private static final String jdoGetcodRegion(Region paramRegion)
  {
    if (paramRegion.jdoFlags <= 0)
      return paramRegion.codRegion;
    StateManager localStateManager = paramRegion.jdoStateManager;
    if (localStateManager == null)
      return paramRegion.codRegion;
    if (localStateManager.isLoaded(paramRegion, jdoInheritedFieldCount + 0))
      return paramRegion.codRegion;
    return localStateManager.getStringField(paramRegion, jdoInheritedFieldCount + 0, paramRegion.codRegion);
  }

  private static final void jdoSetcodRegion(Region paramRegion, String paramString)
  {
    if (paramRegion.jdoFlags == 0)
    {
      paramRegion.codRegion = paramString;
      return;
    }
    StateManager localStateManager = paramRegion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegion.codRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegion, jdoInheritedFieldCount + 0, paramRegion.codRegion, paramString);
  }

  private static final long jdoGetidRegion(Region paramRegion)
  {
    return paramRegion.idRegion;
  }

  private static final void jdoSetidRegion(Region paramRegion, long paramLong)
  {
    StateManager localStateManager = paramRegion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegion.idRegion = paramLong;
      return;
    }
    localStateManager.setLongField(paramRegion, jdoInheritedFieldCount + 1, paramRegion.idRegion, paramLong);
  }

  private static final String jdoGetnombre(Region paramRegion)
  {
    if (paramRegion.jdoFlags <= 0)
      return paramRegion.nombre;
    StateManager localStateManager = paramRegion.jdoStateManager;
    if (localStateManager == null)
      return paramRegion.nombre;
    if (localStateManager.isLoaded(paramRegion, jdoInheritedFieldCount + 2))
      return paramRegion.nombre;
    return localStateManager.getStringField(paramRegion, jdoInheritedFieldCount + 2, paramRegion.nombre);
  }

  private static final void jdoSetnombre(Region paramRegion, String paramString)
  {
    if (paramRegion.jdoFlags == 0)
    {
      paramRegion.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramRegion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegion.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramRegion, jdoInheritedFieldCount + 2, paramRegion.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(Region paramRegion)
  {
    StateManager localStateManager = paramRegion.jdoStateManager;
    if (localStateManager == null)
      return paramRegion.organismo;
    if (localStateManager.isLoaded(paramRegion, jdoInheritedFieldCount + 3))
      return paramRegion.organismo;
    return (Organismo)localStateManager.getObjectField(paramRegion, jdoInheritedFieldCount + 3, paramRegion.organismo);
  }

  private static final void jdoSetorganismo(Region paramRegion, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramRegion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegion.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramRegion, jdoInheritedFieldCount + 3, paramRegion.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}