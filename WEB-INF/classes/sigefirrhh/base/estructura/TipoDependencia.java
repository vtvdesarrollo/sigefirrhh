package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoDependencia
  implements Serializable, PersistenceCapable
{
  private long idTipoDependencia;
  private String codTipoDependencia;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoDependencia", "idTipoDependencia", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + 
      jdoGetcodTipoDependencia(this);
  }

  public String getCodTipoDependencia()
  {
    return jdoGetcodTipoDependencia(this);
  }

  public long getIdTipoDependencia()
  {
    return jdoGetidTipoDependencia(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodTipoDependencia(String string)
  {
    jdoSetcodTipoDependencia(this, string);
  }

  public void setIdTipoDependencia(long l)
  {
    jdoSetidTipoDependencia(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.TipoDependencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoDependencia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoDependencia localTipoDependencia = new TipoDependencia();
    localTipoDependencia.jdoFlags = 1;
    localTipoDependencia.jdoStateManager = paramStateManager;
    return localTipoDependencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoDependencia localTipoDependencia = new TipoDependencia();
    localTipoDependencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoDependencia.jdoFlags = 1;
    localTipoDependencia.jdoStateManager = paramStateManager;
    return localTipoDependencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoDependencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoDependencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoDependencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoDependencia paramTipoDependencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoDependencia = paramTipoDependencia.codTipoDependencia;
      return;
    case 1:
      if (paramTipoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoDependencia = paramTipoDependencia.idTipoDependencia;
      return;
    case 2:
      if (paramTipoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramTipoDependencia.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoDependencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoDependencia localTipoDependencia = (TipoDependencia)paramObject;
    if (localTipoDependencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoDependencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoDependenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoDependenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoDependenciaPK))
      throw new IllegalArgumentException("arg1");
    TipoDependenciaPK localTipoDependenciaPK = (TipoDependenciaPK)paramObject;
    localTipoDependenciaPK.idTipoDependencia = this.idTipoDependencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoDependenciaPK))
      throw new IllegalArgumentException("arg1");
    TipoDependenciaPK localTipoDependenciaPK = (TipoDependenciaPK)paramObject;
    this.idTipoDependencia = localTipoDependenciaPK.idTipoDependencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoDependenciaPK))
      throw new IllegalArgumentException("arg2");
    TipoDependenciaPK localTipoDependenciaPK = (TipoDependenciaPK)paramObject;
    localTipoDependenciaPK.idTipoDependencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoDependenciaPK))
      throw new IllegalArgumentException("arg2");
    TipoDependenciaPK localTipoDependenciaPK = (TipoDependenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localTipoDependenciaPK.idTipoDependencia);
  }

  private static final String jdoGetcodTipoDependencia(TipoDependencia paramTipoDependencia)
  {
    if (paramTipoDependencia.jdoFlags <= 0)
      return paramTipoDependencia.codTipoDependencia;
    StateManager localStateManager = paramTipoDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramTipoDependencia.codTipoDependencia;
    if (localStateManager.isLoaded(paramTipoDependencia, jdoInheritedFieldCount + 0))
      return paramTipoDependencia.codTipoDependencia;
    return localStateManager.getStringField(paramTipoDependencia, jdoInheritedFieldCount + 0, paramTipoDependencia.codTipoDependencia);
  }

  private static final void jdoSetcodTipoDependencia(TipoDependencia paramTipoDependencia, String paramString)
  {
    if (paramTipoDependencia.jdoFlags == 0)
    {
      paramTipoDependencia.codTipoDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramTipoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoDependencia.codTipoDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoDependencia, jdoInheritedFieldCount + 0, paramTipoDependencia.codTipoDependencia, paramString);
  }

  private static final long jdoGetidTipoDependencia(TipoDependencia paramTipoDependencia)
  {
    return paramTipoDependencia.idTipoDependencia;
  }

  private static final void jdoSetidTipoDependencia(TipoDependencia paramTipoDependencia, long paramLong)
  {
    StateManager localStateManager = paramTipoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoDependencia.idTipoDependencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoDependencia, jdoInheritedFieldCount + 1, paramTipoDependencia.idTipoDependencia, paramLong);
  }

  private static final String jdoGetnombre(TipoDependencia paramTipoDependencia)
  {
    if (paramTipoDependencia.jdoFlags <= 0)
      return paramTipoDependencia.nombre;
    StateManager localStateManager = paramTipoDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramTipoDependencia.nombre;
    if (localStateManager.isLoaded(paramTipoDependencia, jdoInheritedFieldCount + 2))
      return paramTipoDependencia.nombre;
    return localStateManager.getStringField(paramTipoDependencia, jdoInheritedFieldCount + 2, paramTipoDependencia.nombre);
  }

  private static final void jdoSetnombre(TipoDependencia paramTipoDependencia, String paramString)
  {
    if (paramTipoDependencia.jdoFlags == 0)
    {
      paramTipoDependencia.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramTipoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoDependencia.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoDependencia, jdoInheritedFieldCount + 2, paramTipoDependencia.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}