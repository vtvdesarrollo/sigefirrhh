package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class UnidadFuncionalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addUnidadFuncional(UnidadFuncional unidadFuncional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    UnidadFuncional unidadFuncionalNew = 
      (UnidadFuncional)BeanUtils.cloneBean(
      unidadFuncional);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (unidadFuncionalNew.getOrganismo() != null) {
      unidadFuncionalNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        unidadFuncionalNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(unidadFuncionalNew);
  }

  public void updateUnidadFuncional(UnidadFuncional unidadFuncional) throws Exception
  {
    UnidadFuncional unidadFuncionalModify = 
      findUnidadFuncionalById(unidadFuncional.getIdUnidadFuncional());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (unidadFuncional.getOrganismo() != null) {
      unidadFuncional.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        unidadFuncional.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(unidadFuncionalModify, unidadFuncional);
  }

  public void deleteUnidadFuncional(UnidadFuncional unidadFuncional) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    UnidadFuncional unidadFuncionalDelete = 
      findUnidadFuncionalById(unidadFuncional.getIdUnidadFuncional());
    pm.deletePersistent(unidadFuncionalDelete);
  }

  public UnidadFuncional findUnidadFuncionalById(long idUnidadFuncional) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idUnidadFuncional == pIdUnidadFuncional";
    Query query = pm.newQuery(UnidadFuncional.class, filter);

    query.declareParameters("long pIdUnidadFuncional");

    parameters.put("pIdUnidadFuncional", new Long(idUnidadFuncional));

    Collection colUnidadFuncional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colUnidadFuncional.iterator();
    return (UnidadFuncional)iterator.next();
  }

  public Collection findUnidadFuncionalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent unidadFuncionalExtent = pm.getExtent(
      UnidadFuncional.class, true);
    Query query = pm.newQuery(unidadFuncionalExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodUnidadFuncional(String codUnidadFuncional, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codUnidadFuncional == pCodUnidadFuncional &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(UnidadFuncional.class, filter);

    query.declareParameters("java.lang.String pCodUnidadFuncional, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodUnidadFuncional", new String(codUnidadFuncional));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colUnidadFuncional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUnidadFuncional);

    return colUnidadFuncional;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(UnidadFuncional.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colUnidadFuncional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUnidadFuncional);

    return colUnidadFuncional;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(UnidadFuncional.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colUnidadFuncional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUnidadFuncional);

    return colUnidadFuncional;
  }
}