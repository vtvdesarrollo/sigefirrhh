package sigefirrhh.base.estructura;

import java.io.Serializable;

public class UnidadFuncionalPK
  implements Serializable
{
  public long idUnidadFuncional;

  public UnidadFuncionalPK()
  {
  }

  public UnidadFuncionalPK(long idUnidadFuncional)
  {
    this.idUnidadFuncional = idUnidadFuncional;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UnidadFuncionalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UnidadFuncionalPK thatPK)
  {
    return 
      this.idUnidadFuncional == thatPK.idUnidadFuncional;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUnidadFuncional)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUnidadFuncional);
  }
}