package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class EstructuraBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEstructura(Estructura estructura)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Estructura estructuraNew = 
      (Estructura)BeanUtils.cloneBean(
      estructura);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (estructuraNew.getOrganismo() != null) {
      estructuraNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        estructuraNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(estructuraNew);
  }

  public void updateEstructura(Estructura estructura) throws Exception
  {
    Estructura estructuraModify = 
      findEstructuraById(estructura.getIdEstructura());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (estructura.getOrganismo() != null) {
      estructura.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        estructura.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(estructuraModify, estructura);
  }

  public void deleteEstructura(Estructura estructura) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Estructura estructuraDelete = 
      findEstructuraById(estructura.getIdEstructura());
    pm.deletePersistent(estructuraDelete);
  }

  public Estructura findEstructuraById(long idEstructura) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEstructura == pIdEstructura";
    Query query = pm.newQuery(Estructura.class, filter);

    query.declareParameters("long pIdEstructura");

    parameters.put("pIdEstructura", new Long(idEstructura));

    Collection colEstructura = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEstructura.iterator();
    return (Estructura)iterator.next();
  }

  public Collection findEstructuraAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent estructuraExtent = pm.getExtent(
      Estructura.class, true);
    Query query = pm.newQuery(estructuraExtent);
    query.setOrdering("fechaVigencia ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Estructura.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaVigencia ascending");

    Collection colEstructura = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEstructura);

    return colEstructura;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Estructura.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaVigencia ascending");

    Collection colEstructura = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEstructura);

    return colEstructura;
  }
}