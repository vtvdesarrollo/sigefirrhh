package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class UnidadEjecutoraForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(UnidadEjecutoraForm.class.getName());
  private UnidadEjecutora unidadEjecutora;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showUnidadEjecutoraByCodUnidadEjecutora;
  private boolean showUnidadEjecutoraByNombre;
  private String findCodUnidadEjecutora;
  private String findNombre;
  private Collection colPrograma;
  private String selectPrograma;
  private Object stateResultUnidadEjecutoraByCodUnidadEjecutora = null;

  private Object stateResultUnidadEjecutoraByNombre = null;

  public String getFindCodUnidadEjecutora()
  {
    return this.findCodUnidadEjecutora;
  }
  public void setFindCodUnidadEjecutora(String findCodUnidadEjecutora) {
    this.findCodUnidadEjecutora = findCodUnidadEjecutora;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectPrograma()
  {
    return this.selectPrograma;
  }
  public void setSelectPrograma(String valPrograma) {
    Iterator iterator = this.colPrograma.iterator();
    Programa programa = null;
    this.unidadEjecutora.setPrograma(null);
    while (iterator.hasNext()) {
      programa = (Programa)iterator.next();
      if (String.valueOf(programa.getIdPrograma()).equals(
        valPrograma)) {
        this.unidadEjecutora.setPrograma(
          programa);
        break;
      }
    }
    this.selectPrograma = valPrograma;
  }
  public Collection getResult() {
    return this.result;
  }

  public UnidadEjecutora getUnidadEjecutora() {
    if (this.unidadEjecutora == null) {
      this.unidadEjecutora = new UnidadEjecutora();
    }
    return this.unidadEjecutora;
  }

  public UnidadEjecutoraForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPrograma()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPrograma.iterator();
    Programa programa = null;
    while (iterator.hasNext()) {
      programa = (Programa)iterator.next();
      col.add(new SelectItem(
        String.valueOf(programa.getIdPrograma()), 
        programa.toString()));
    }
    return col;
  }

  public Collection getListVigente() {
    Collection col = new ArrayList();

    Iterator iterEntry = UnidadEjecutora.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colPrograma = 
        this.estructuraFacade.findProgramaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findUnidadEjecutoraByCodUnidadEjecutora()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findUnidadEjecutoraByCodUnidadEjecutora(this.findCodUnidadEjecutora);
      this.showUnidadEjecutoraByCodUnidadEjecutora = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUnidadEjecutoraByCodUnidadEjecutora)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodUnidadEjecutora = null;
    this.findNombre = null;

    return null;
  }

  public String findUnidadEjecutoraByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findUnidadEjecutoraByNombre(this.findNombre);
      this.showUnidadEjecutoraByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUnidadEjecutoraByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodUnidadEjecutora = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowUnidadEjecutoraByCodUnidadEjecutora() {
    return this.showUnidadEjecutoraByCodUnidadEjecutora;
  }
  public boolean isShowUnidadEjecutoraByNombre() {
    return this.showUnidadEjecutoraByNombre;
  }

  public String selectUnidadEjecutora()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPrograma = null;

    long idUnidadEjecutora = 
      Long.parseLong((String)requestParameterMap.get("idUnidadEjecutora"));
    try
    {
      this.unidadEjecutora = 
        this.estructuraFacade.findUnidadEjecutoraById(
        idUnidadEjecutora);
      if (this.unidadEjecutora.getPrograma() != null) {
        this.selectPrograma = 
          String.valueOf(this.unidadEjecutora.getPrograma().getIdPrograma());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.unidadEjecutora = null;
    this.showUnidadEjecutoraByCodUnidadEjecutora = false;
    this.showUnidadEjecutoraByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addUnidadEjecutora(
          this.unidadEjecutora);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateUnidadEjecutora(
          this.unidadEjecutora);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteUnidadEjecutora(
        this.unidadEjecutora);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.unidadEjecutora = new UnidadEjecutora();

    this.selectPrograma = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.unidadEjecutora.setIdUnidadEjecutora(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.UnidadEjecutora"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.unidadEjecutora = new UnidadEjecutora();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}