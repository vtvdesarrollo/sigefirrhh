package sigefirrhh.base.estructura;

import java.io.Serializable;

public class AdministradoraUelPK
  implements Serializable
{
  public long idAdministradoraUel;

  public AdministradoraUelPK()
  {
  }

  public AdministradoraUelPK(long idAdministradoraUel)
  {
    this.idAdministradoraUel = idAdministradoraUel;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AdministradoraUelPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AdministradoraUelPK thatPK)
  {
    return 
      this.idAdministradoraUel == thatPK.idAdministradoraUel;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAdministradoraUel)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAdministradoraUel);
  }
}