package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class UnidadEjecutoraBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addUnidadEjecutora(UnidadEjecutora unidadEjecutora)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    UnidadEjecutora unidadEjecutoraNew = 
      (UnidadEjecutora)BeanUtils.cloneBean(
      unidadEjecutora);

    ProgramaBeanBusiness programaBeanBusiness = new ProgramaBeanBusiness();

    if (unidadEjecutoraNew.getPrograma() != null) {
      unidadEjecutoraNew.setPrograma(
        programaBeanBusiness.findProgramaById(
        unidadEjecutoraNew.getPrograma().getIdPrograma()));
    }
    pm.makePersistent(unidadEjecutoraNew);
  }

  public void updateUnidadEjecutora(UnidadEjecutora unidadEjecutora) throws Exception
  {
    UnidadEjecutora unidadEjecutoraModify = 
      findUnidadEjecutoraById(unidadEjecutora.getIdUnidadEjecutora());

    ProgramaBeanBusiness programaBeanBusiness = new ProgramaBeanBusiness();

    if (unidadEjecutora.getPrograma() != null) {
      unidadEjecutora.setPrograma(
        programaBeanBusiness.findProgramaById(
        unidadEjecutora.getPrograma().getIdPrograma()));
    }

    BeanUtils.copyProperties(unidadEjecutoraModify, unidadEjecutora);
  }

  public void deleteUnidadEjecutora(UnidadEjecutora unidadEjecutora) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    UnidadEjecutora unidadEjecutoraDelete = 
      findUnidadEjecutoraById(unidadEjecutora.getIdUnidadEjecutora());
    pm.deletePersistent(unidadEjecutoraDelete);
  }

  public UnidadEjecutora findUnidadEjecutoraById(long idUnidadEjecutora) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idUnidadEjecutora == pIdUnidadEjecutora";
    Query query = pm.newQuery(UnidadEjecutora.class, filter);

    query.declareParameters("long pIdUnidadEjecutora");

    parameters.put("pIdUnidadEjecutora", new Long(idUnidadEjecutora));

    Collection colUnidadEjecutora = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colUnidadEjecutora.iterator();
    return (UnidadEjecutora)iterator.next();
  }

  public Collection findUnidadEjecutoraAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent unidadEjecutoraExtent = pm.getExtent(
      UnidadEjecutora.class, true);
    Query query = pm.newQuery(unidadEjecutoraExtent);
    query.setOrdering("codUnidadEjecutora ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodUnidadEjecutora(String codUnidadEjecutora)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codUnidadEjecutora == pCodUnidadEjecutora";

    Query query = pm.newQuery(UnidadEjecutora.class, filter);

    query.declareParameters("java.lang.String pCodUnidadEjecutora");
    HashMap parameters = new HashMap();

    parameters.put("pCodUnidadEjecutora", new String(codUnidadEjecutora));

    query.setOrdering("codUnidadEjecutora ascending");

    Collection colUnidadEjecutora = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUnidadEjecutora);

    return colUnidadEjecutora;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(UnidadEjecutora.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codUnidadEjecutora ascending");

    Collection colUnidadEjecutora = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUnidadEjecutora);

    return colUnidadEjecutora;
  }
}