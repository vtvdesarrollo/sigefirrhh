package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class CaracteristicaDependenciaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CaracteristicaDependenciaForm.class.getName());
  private CaracteristicaDependencia caracteristicaDependencia;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showCaracteristicaDependenciaByTipoCaracteristica;
  private String findSelectTipoCaracteristica;
  private Collection findColTipoCaracteristica;
  private Collection colTipoCaracteristica;
  private String selectTipoCaracteristica;
  private Object stateResultCaracteristicaDependenciaByTipoCaracteristica = null;

  public String getFindSelectTipoCaracteristica()
  {
    return this.findSelectTipoCaracteristica;
  }
  public void setFindSelectTipoCaracteristica(String valTipoCaracteristica) {
    this.findSelectTipoCaracteristica = valTipoCaracteristica;
  }

  public Collection getFindColTipoCaracteristica() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoCaracteristica.iterator();
    TipoCaracteristica tipoCaracteristica = null;
    while (iterator.hasNext()) {
      tipoCaracteristica = (TipoCaracteristica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCaracteristica.getIdTipoCaracteristica()), 
        tipoCaracteristica.toString()));
    }
    return col;
  }

  public String getSelectTipoCaracteristica()
  {
    return this.selectTipoCaracteristica;
  }
  public void setSelectTipoCaracteristica(String valTipoCaracteristica) {
    Iterator iterator = this.colTipoCaracteristica.iterator();
    TipoCaracteristica tipoCaracteristica = null;
    this.caracteristicaDependencia.setTipoCaracteristica(null);
    while (iterator.hasNext()) {
      tipoCaracteristica = (TipoCaracteristica)iterator.next();
      if (String.valueOf(tipoCaracteristica.getIdTipoCaracteristica()).equals(
        valTipoCaracteristica)) {
        this.caracteristicaDependencia.setTipoCaracteristica(
          tipoCaracteristica);
        break;
      }
    }
    this.selectTipoCaracteristica = valTipoCaracteristica;
  }
  public Collection getResult() {
    return this.result;
  }

  public CaracteristicaDependencia getCaracteristicaDependencia() {
    if (this.caracteristicaDependencia == null) {
      this.caracteristicaDependencia = new CaracteristicaDependencia();
    }
    return this.caracteristicaDependencia;
  }

  public CaracteristicaDependenciaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoCaracteristica()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoCaracteristica.iterator();
    TipoCaracteristica tipoCaracteristica = null;
    while (iterator.hasNext()) {
      tipoCaracteristica = (TipoCaracteristica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCaracteristica.getIdTipoCaracteristica()), 
        tipoCaracteristica.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoCaracteristica = 
        this.estructuraFacade.findAllTipoCaracteristica();

      this.colTipoCaracteristica = 
        this.estructuraFacade.findAllTipoCaracteristica();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findCaracteristicaDependenciaByTipoCaracteristica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.estructuraFacade.findCaracteristicaDependenciaByTipoCaracteristica(Long.valueOf(this.findSelectTipoCaracteristica).longValue());
      this.showCaracteristicaDependenciaByTipoCaracteristica = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCaracteristicaDependenciaByTipoCaracteristica)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoCaracteristica = null;

    return null;
  }

  public boolean isShowCaracteristicaDependenciaByTipoCaracteristica() {
    return this.showCaracteristicaDependenciaByTipoCaracteristica;
  }

  public String selectCaracteristicaDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoCaracteristica = null;

    long idCaracteristicaDependencia = 
      Long.parseLong((String)requestParameterMap.get("idCaracteristicaDependencia"));
    try
    {
      this.caracteristicaDependencia = 
        this.estructuraFacade.findCaracteristicaDependenciaById(
        idCaracteristicaDependencia);
      if (this.caracteristicaDependencia.getTipoCaracteristica() != null) {
        this.selectTipoCaracteristica = 
          String.valueOf(this.caracteristicaDependencia.getTipoCaracteristica().getIdTipoCaracteristica());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.caracteristicaDependencia = null;
    this.showCaracteristicaDependenciaByTipoCaracteristica = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addCaracteristicaDependencia(
          this.caracteristicaDependencia);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateCaracteristicaDependencia(
          this.caracteristicaDependencia);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteCaracteristicaDependencia(
        this.caracteristicaDependencia);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.caracteristicaDependencia = new CaracteristicaDependencia();

    this.selectTipoCaracteristica = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.caracteristicaDependencia.setIdCaracteristicaDependencia(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.CaracteristicaDependencia"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.caracteristicaDependencia = new CaracteristicaDependencia();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}