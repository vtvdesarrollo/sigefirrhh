package sigefirrhh.base.estructura;

import java.io.Serializable;

public class EstructuraPK
  implements Serializable
{
  public long idEstructura;

  public EstructuraPK()
  {
  }

  public EstructuraPK(long idEstructura)
  {
    this.idEstructura = idEstructura;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EstructuraPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EstructuraPK thatPK)
  {
    return 
      this.idEstructura == thatPK.idEstructura;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEstructura)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEstructura);
  }
}