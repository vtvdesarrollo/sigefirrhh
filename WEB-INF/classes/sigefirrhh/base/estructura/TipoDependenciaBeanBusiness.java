package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoDependenciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoDependencia(TipoDependencia tipoDependencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoDependencia tipoDependenciaNew = 
      (TipoDependencia)BeanUtils.cloneBean(
      tipoDependencia);

    pm.makePersistent(tipoDependenciaNew);
  }

  public void updateTipoDependencia(TipoDependencia tipoDependencia) throws Exception
  {
    TipoDependencia tipoDependenciaModify = 
      findTipoDependenciaById(tipoDependencia.getIdTipoDependencia());

    BeanUtils.copyProperties(tipoDependenciaModify, tipoDependencia);
  }

  public void deleteTipoDependencia(TipoDependencia tipoDependencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoDependencia tipoDependenciaDelete = 
      findTipoDependenciaById(tipoDependencia.getIdTipoDependencia());
    pm.deletePersistent(tipoDependenciaDelete);
  }

  public TipoDependencia findTipoDependenciaById(long idTipoDependencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoDependencia == pIdTipoDependencia";
    Query query = pm.newQuery(TipoDependencia.class, filter);

    query.declareParameters("long pIdTipoDependencia");

    parameters.put("pIdTipoDependencia", new Long(idTipoDependencia));

    Collection colTipoDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoDependencia.iterator();
    return (TipoDependencia)iterator.next();
  }

  public Collection findTipoDependenciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoDependenciaExtent = pm.getExtent(
      TipoDependencia.class, true);
    Query query = pm.newQuery(tipoDependenciaExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoDependencia(String codTipoDependencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoDependencia == pCodTipoDependencia";

    Query query = pm.newQuery(TipoDependencia.class, filter);

    query.declareParameters("java.lang.String pCodTipoDependencia");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoDependencia", new String(codTipoDependencia));

    query.setOrdering("nombre ascending");

    Collection colTipoDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoDependencia);

    return colTipoDependencia;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(TipoDependencia.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colTipoDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoDependencia);

    return colTipoDependencia;
  }
}