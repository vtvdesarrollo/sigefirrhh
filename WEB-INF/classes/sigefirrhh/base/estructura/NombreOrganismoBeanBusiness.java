package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class NombreOrganismoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addNombreOrganismo(NombreOrganismo nombreOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    NombreOrganismo nombreOrganismoNew = 
      (NombreOrganismo)BeanUtils.cloneBean(
      nombreOrganismo);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (nombreOrganismoNew.getOrganismo() != null) {
      nombreOrganismoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        nombreOrganismoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(nombreOrganismoNew);
  }

  public void updateNombreOrganismo(NombreOrganismo nombreOrganismo) throws Exception
  {
    NombreOrganismo nombreOrganismoModify = 
      findNombreOrganismoById(nombreOrganismo.getIdNombreOrganismo());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (nombreOrganismo.getOrganismo() != null) {
      nombreOrganismo.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        nombreOrganismo.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(nombreOrganismoModify, nombreOrganismo);
  }

  public void deleteNombreOrganismo(NombreOrganismo nombreOrganismo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    NombreOrganismo nombreOrganismoDelete = 
      findNombreOrganismoById(nombreOrganismo.getIdNombreOrganismo());
    pm.deletePersistent(nombreOrganismoDelete);
  }

  public NombreOrganismo findNombreOrganismoById(long idNombreOrganismo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idNombreOrganismo == pIdNombreOrganismo";
    Query query = pm.newQuery(NombreOrganismo.class, filter);

    query.declareParameters("long pIdNombreOrganismo");

    parameters.put("pIdNombreOrganismo", new Long(idNombreOrganismo));

    Collection colNombreOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colNombreOrganismo.iterator();
    return (NombreOrganismo)iterator.next();
  }

  public Collection findNombreOrganismoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent nombreOrganismoExtent = pm.getExtent(
      NombreOrganismo.class, true);
    Query query = pm.newQuery(nombreOrganismoExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(NombreOrganismo.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colNombreOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNombreOrganismo);

    return colNombreOrganismo;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(NombreOrganismo.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colNombreOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNombreOrganismo);

    return colNombreOrganismo;
  }
}