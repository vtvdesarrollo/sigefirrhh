package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ClasificacionDependencia
  implements Serializable, PersistenceCapable
{
  private long idClasificacionDependencia;
  private CaracteristicaDependencia caracteristicaDependencia;
  private Dependencia dependencia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "caracteristicaDependencia", "dependencia", "idClasificacionDependencia" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.estructura.CaracteristicaDependencia"), sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 26, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdependencia(this) + " - Tipo: " + 
      jdoGetcaracteristicaDependencia(this);
  }

  public Dependencia getDependencia()
  {
    return jdoGetdependencia(this);
  }
  public void setDependencia(Dependencia dependencia) {
    jdoSetdependencia(this, dependencia);
  }
  public CaracteristicaDependencia getCaracteristicaDependencia() {
    return jdoGetcaracteristicaDependencia(this);
  }

  public void setCaracteristicaDependencia(CaracteristicaDependencia caracteristicaDependencia) {
    jdoSetcaracteristicaDependencia(this, caracteristicaDependencia);
  }

  public long getIdClasificacionDependencia() {
    return jdoGetidClasificacionDependencia(this);
  }
  public void setIdClasificacionDependencia(long idClasificacionDependencia) {
    jdoSetidClasificacionDependencia(this, idClasificacionDependencia);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.ClasificacionDependencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ClasificacionDependencia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ClasificacionDependencia localClasificacionDependencia = new ClasificacionDependencia();
    localClasificacionDependencia.jdoFlags = 1;
    localClasificacionDependencia.jdoStateManager = paramStateManager;
    return localClasificacionDependencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ClasificacionDependencia localClasificacionDependencia = new ClasificacionDependencia();
    localClasificacionDependencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localClasificacionDependencia.jdoFlags = 1;
    localClasificacionDependencia.jdoStateManager = paramStateManager;
    return localClasificacionDependencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.caracteristicaDependencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idClasificacionDependencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.caracteristicaDependencia = ((CaracteristicaDependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idClasificacionDependencia = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ClasificacionDependencia paramClasificacionDependencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramClasificacionDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.caracteristicaDependencia = paramClasificacionDependencia.caracteristicaDependencia;
      return;
    case 1:
      if (paramClasificacionDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramClasificacionDependencia.dependencia;
      return;
    case 2:
      if (paramClasificacionDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.idClasificacionDependencia = paramClasificacionDependencia.idClasificacionDependencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ClasificacionDependencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ClasificacionDependencia localClasificacionDependencia = (ClasificacionDependencia)paramObject;
    if (localClasificacionDependencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localClasificacionDependencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ClasificacionDependenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ClasificacionDependenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ClasificacionDependenciaPK))
      throw new IllegalArgumentException("arg1");
    ClasificacionDependenciaPK localClasificacionDependenciaPK = (ClasificacionDependenciaPK)paramObject;
    localClasificacionDependenciaPK.idClasificacionDependencia = this.idClasificacionDependencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ClasificacionDependenciaPK))
      throw new IllegalArgumentException("arg1");
    ClasificacionDependenciaPK localClasificacionDependenciaPK = (ClasificacionDependenciaPK)paramObject;
    this.idClasificacionDependencia = localClasificacionDependenciaPK.idClasificacionDependencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ClasificacionDependenciaPK))
      throw new IllegalArgumentException("arg2");
    ClasificacionDependenciaPK localClasificacionDependenciaPK = (ClasificacionDependenciaPK)paramObject;
    localClasificacionDependenciaPK.idClasificacionDependencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ClasificacionDependenciaPK))
      throw new IllegalArgumentException("arg2");
    ClasificacionDependenciaPK localClasificacionDependenciaPK = (ClasificacionDependenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localClasificacionDependenciaPK.idClasificacionDependencia);
  }

  private static final CaracteristicaDependencia jdoGetcaracteristicaDependencia(ClasificacionDependencia paramClasificacionDependencia)
  {
    StateManager localStateManager = paramClasificacionDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramClasificacionDependencia.caracteristicaDependencia;
    if (localStateManager.isLoaded(paramClasificacionDependencia, jdoInheritedFieldCount + 0))
      return paramClasificacionDependencia.caracteristicaDependencia;
    return (CaracteristicaDependencia)localStateManager.getObjectField(paramClasificacionDependencia, jdoInheritedFieldCount + 0, paramClasificacionDependencia.caracteristicaDependencia);
  }

  private static final void jdoSetcaracteristicaDependencia(ClasificacionDependencia paramClasificacionDependencia, CaracteristicaDependencia paramCaracteristicaDependencia)
  {
    StateManager localStateManager = paramClasificacionDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacionDependencia.caracteristicaDependencia = paramCaracteristicaDependencia;
      return;
    }
    localStateManager.setObjectField(paramClasificacionDependencia, jdoInheritedFieldCount + 0, paramClasificacionDependencia.caracteristicaDependencia, paramCaracteristicaDependencia);
  }

  private static final Dependencia jdoGetdependencia(ClasificacionDependencia paramClasificacionDependencia)
  {
    StateManager localStateManager = paramClasificacionDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramClasificacionDependencia.dependencia;
    if (localStateManager.isLoaded(paramClasificacionDependencia, jdoInheritedFieldCount + 1))
      return paramClasificacionDependencia.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramClasificacionDependencia, jdoInheritedFieldCount + 1, paramClasificacionDependencia.dependencia);
  }

  private static final void jdoSetdependencia(ClasificacionDependencia paramClasificacionDependencia, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramClasificacionDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacionDependencia.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramClasificacionDependencia, jdoInheritedFieldCount + 1, paramClasificacionDependencia.dependencia, paramDependencia);
  }

  private static final long jdoGetidClasificacionDependencia(ClasificacionDependencia paramClasificacionDependencia)
  {
    return paramClasificacionDependencia.idClasificacionDependencia;
  }

  private static final void jdoSetidClasificacionDependencia(ClasificacionDependencia paramClasificacionDependencia, long paramLong)
  {
    StateManager localStateManager = paramClasificacionDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacionDependencia.idClasificacionDependencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramClasificacionDependencia, jdoInheritedFieldCount + 2, paramClasificacionDependencia.idClasificacionDependencia, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}