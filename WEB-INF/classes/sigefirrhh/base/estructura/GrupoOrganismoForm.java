package sigefirrhh.base.estructura;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class GrupoOrganismoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GrupoOrganismoForm.class.getName());
  private GrupoOrganismo grupoOrganismo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showGrupoOrganismoByCodGrupoOrganismo;
  private boolean showGrupoOrganismoByNombre;
  private String findCodGrupoOrganismo;
  private String findNombre;
  private Object stateResultGrupoOrganismoByCodGrupoOrganismo = null;

  private Object stateResultGrupoOrganismoByNombre = null;

  public String getFindCodGrupoOrganismo()
  {
    return this.findCodGrupoOrganismo;
  }
  public void setFindCodGrupoOrganismo(String findCodGrupoOrganismo) {
    this.findCodGrupoOrganismo = findCodGrupoOrganismo;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public GrupoOrganismo getGrupoOrganismo() {
    if (this.grupoOrganismo == null) {
      this.grupoOrganismo = new GrupoOrganismo();
    }
    return this.grupoOrganismo;
  }

  public GrupoOrganismoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findGrupoOrganismoByCodGrupoOrganismo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findGrupoOrganismoByCodGrupoOrganismo(this.findCodGrupoOrganismo, idOrganismo);
      this.showGrupoOrganismoByCodGrupoOrganismo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGrupoOrganismoByCodGrupoOrganismo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodGrupoOrganismo = null;
    this.findNombre = null;

    return null;
  }

  public String findGrupoOrganismoByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.estructuraFacade.findGrupoOrganismoByNombre(this.findNombre, idOrganismo);
      this.showGrupoOrganismoByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGrupoOrganismoByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodGrupoOrganismo = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowGrupoOrganismoByCodGrupoOrganismo() {
    return this.showGrupoOrganismoByCodGrupoOrganismo;
  }
  public boolean isShowGrupoOrganismoByNombre() {
    return this.showGrupoOrganismoByNombre;
  }

  public String selectGrupoOrganismo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idGrupoOrganismo = 
      Long.parseLong((String)requestParameterMap.get("idGrupoOrganismo"));
    try
    {
      this.grupoOrganismo = 
        this.estructuraFacade.findGrupoOrganismoById(
        idGrupoOrganismo);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.grupoOrganismo = null;
    this.showGrupoOrganismoByCodGrupoOrganismo = false;
    this.showGrupoOrganismoByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.estructuraFacade.addGrupoOrganismo(
          this.grupoOrganismo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.estructuraFacade.updateGrupoOrganismo(
          this.grupoOrganismo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.estructuraFacade.deleteGrupoOrganismo(
        this.grupoOrganismo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.grupoOrganismo = new GrupoOrganismo();

    this.grupoOrganismo.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.grupoOrganismo.setIdGrupoOrganismo(identityGenerator.getNextSequenceNumber("sigefirrhh.base.estructura.GrupoOrganismo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.grupoOrganismo = new GrupoOrganismo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}