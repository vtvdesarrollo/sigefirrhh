package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class UnidadFuncional
  implements Serializable, PersistenceCapable
{
  private long idUnidadFuncional;
  private String codUnidadFuncional;
  private String nombre;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codUnidadFuncional", "idUnidadFuncional", "nombre", "organismo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " - " + 
      jdoGetcodUnidadFuncional(this);
  }

  public String getCodUnidadFuncional() {
    return jdoGetcodUnidadFuncional(this);
  }

  public void setCodUnidadFuncional(String codUnidadFuncional) {
    jdoSetcodUnidadFuncional(this, codUnidadFuncional);
  }

  public long getIdUnidadFuncional() {
    return jdoGetidUnidadFuncional(this);
  }

  public void setIdUnidadFuncional(long idUnidadFuncional) {
    jdoSetidUnidadFuncional(this, idUnidadFuncional);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.UnidadFuncional"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UnidadFuncional());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UnidadFuncional localUnidadFuncional = new UnidadFuncional();
    localUnidadFuncional.jdoFlags = 1;
    localUnidadFuncional.jdoStateManager = paramStateManager;
    return localUnidadFuncional;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UnidadFuncional localUnidadFuncional = new UnidadFuncional();
    localUnidadFuncional.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUnidadFuncional.jdoFlags = 1;
    localUnidadFuncional.jdoStateManager = paramStateManager;
    return localUnidadFuncional;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codUnidadFuncional);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUnidadFuncional);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codUnidadFuncional = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUnidadFuncional = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UnidadFuncional paramUnidadFuncional, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUnidadFuncional == null)
        throw new IllegalArgumentException("arg1");
      this.codUnidadFuncional = paramUnidadFuncional.codUnidadFuncional;
      return;
    case 1:
      if (paramUnidadFuncional == null)
        throw new IllegalArgumentException("arg1");
      this.idUnidadFuncional = paramUnidadFuncional.idUnidadFuncional;
      return;
    case 2:
      if (paramUnidadFuncional == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramUnidadFuncional.nombre;
      return;
    case 3:
      if (paramUnidadFuncional == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramUnidadFuncional.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UnidadFuncional))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UnidadFuncional localUnidadFuncional = (UnidadFuncional)paramObject;
    if (localUnidadFuncional.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUnidadFuncional, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UnidadFuncionalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UnidadFuncionalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UnidadFuncionalPK))
      throw new IllegalArgumentException("arg1");
    UnidadFuncionalPK localUnidadFuncionalPK = (UnidadFuncionalPK)paramObject;
    localUnidadFuncionalPK.idUnidadFuncional = this.idUnidadFuncional;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UnidadFuncionalPK))
      throw new IllegalArgumentException("arg1");
    UnidadFuncionalPK localUnidadFuncionalPK = (UnidadFuncionalPK)paramObject;
    this.idUnidadFuncional = localUnidadFuncionalPK.idUnidadFuncional;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UnidadFuncionalPK))
      throw new IllegalArgumentException("arg2");
    UnidadFuncionalPK localUnidadFuncionalPK = (UnidadFuncionalPK)paramObject;
    localUnidadFuncionalPK.idUnidadFuncional = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UnidadFuncionalPK))
      throw new IllegalArgumentException("arg2");
    UnidadFuncionalPK localUnidadFuncionalPK = (UnidadFuncionalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localUnidadFuncionalPK.idUnidadFuncional);
  }

  private static final String jdoGetcodUnidadFuncional(UnidadFuncional paramUnidadFuncional)
  {
    if (paramUnidadFuncional.jdoFlags <= 0)
      return paramUnidadFuncional.codUnidadFuncional;
    StateManager localStateManager = paramUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadFuncional.codUnidadFuncional;
    if (localStateManager.isLoaded(paramUnidadFuncional, jdoInheritedFieldCount + 0))
      return paramUnidadFuncional.codUnidadFuncional;
    return localStateManager.getStringField(paramUnidadFuncional, jdoInheritedFieldCount + 0, paramUnidadFuncional.codUnidadFuncional);
  }

  private static final void jdoSetcodUnidadFuncional(UnidadFuncional paramUnidadFuncional, String paramString)
  {
    if (paramUnidadFuncional.jdoFlags == 0)
    {
      paramUnidadFuncional.codUnidadFuncional = paramString;
      return;
    }
    StateManager localStateManager = paramUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadFuncional.codUnidadFuncional = paramString;
      return;
    }
    localStateManager.setStringField(paramUnidadFuncional, jdoInheritedFieldCount + 0, paramUnidadFuncional.codUnidadFuncional, paramString);
  }

  private static final long jdoGetidUnidadFuncional(UnidadFuncional paramUnidadFuncional)
  {
    return paramUnidadFuncional.idUnidadFuncional;
  }

  private static final void jdoSetidUnidadFuncional(UnidadFuncional paramUnidadFuncional, long paramLong)
  {
    StateManager localStateManager = paramUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadFuncional.idUnidadFuncional = paramLong;
      return;
    }
    localStateManager.setLongField(paramUnidadFuncional, jdoInheritedFieldCount + 1, paramUnidadFuncional.idUnidadFuncional, paramLong);
  }

  private static final String jdoGetnombre(UnidadFuncional paramUnidadFuncional)
  {
    if (paramUnidadFuncional.jdoFlags <= 0)
      return paramUnidadFuncional.nombre;
    StateManager localStateManager = paramUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadFuncional.nombre;
    if (localStateManager.isLoaded(paramUnidadFuncional, jdoInheritedFieldCount + 2))
      return paramUnidadFuncional.nombre;
    return localStateManager.getStringField(paramUnidadFuncional, jdoInheritedFieldCount + 2, paramUnidadFuncional.nombre);
  }

  private static final void jdoSetnombre(UnidadFuncional paramUnidadFuncional, String paramString)
  {
    if (paramUnidadFuncional.jdoFlags == 0)
    {
      paramUnidadFuncional.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadFuncional.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramUnidadFuncional, jdoInheritedFieldCount + 2, paramUnidadFuncional.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(UnidadFuncional paramUnidadFuncional)
  {
    StateManager localStateManager = paramUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadFuncional.organismo;
    if (localStateManager.isLoaded(paramUnidadFuncional, jdoInheritedFieldCount + 3))
      return paramUnidadFuncional.organismo;
    return (Organismo)localStateManager.getObjectField(paramUnidadFuncional, jdoInheritedFieldCount + 3, paramUnidadFuncional.organismo);
  }

  private static final void jdoSetorganismo(UnidadFuncional paramUnidadFuncional, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadFuncional.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramUnidadFuncional, jdoInheritedFieldCount + 3, paramUnidadFuncional.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}