package sigefirrhh.base.estructura;

import java.io.Serializable;

public class OrganismoPK
  implements Serializable
{
  public long idOrganismo;

  public OrganismoPK()
  {
  }

  public OrganismoPK(long idOrganismo)
  {
    this.idOrganismo = idOrganismo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((OrganismoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(OrganismoPK thatPK)
  {
    return 
      this.idOrganismo == thatPK.idOrganismo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idOrganismo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idOrganismo);
  }
}