package sigefirrhh.base.estructura;

import java.io.Serializable;

public class TipoCaracteristicaPK
  implements Serializable
{
  public long idTipoCaracteristica;

  public TipoCaracteristicaPK()
  {
  }

  public TipoCaracteristicaPK(long idTipoCaracteristica)
  {
    this.idTipoCaracteristica = idTipoCaracteristica;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoCaracteristicaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoCaracteristicaPK thatPK)
  {
    return 
      this.idTipoCaracteristica == thatPK.idTipoCaracteristica;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoCaracteristica)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoCaracteristica);
  }
}