package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.definiciones.TurnoBeanBusiness;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;

public class SedeBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSede(Sede sede)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Sede sedeNew = 
      (Sede)BeanUtils.cloneBean(
      sede);

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (sedeNew.getRegion() != null) {
      sedeNew.setRegion(
        regionBeanBusiness.findRegionById(
        sedeNew.getRegion().getIdRegion()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (sedeNew.getCiudad() != null) {
      sedeNew.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        sedeNew.getCiudad().getIdCiudad()));
    }

    LugarPagoBeanBusiness lugarPagoBeanBusiness = new LugarPagoBeanBusiness();

    if (sedeNew.getLugarPago() != null) {
      sedeNew.setLugarPago(
        lugarPagoBeanBusiness.findLugarPagoById(
        sedeNew.getLugarPago().getIdLugarPago()));
    }

    TurnoBeanBusiness turnoBeanBusiness = new TurnoBeanBusiness();

    if (sedeNew.getTurno() != null) {
      sedeNew.setTurno(
        turnoBeanBusiness.findTurnoById(
        sedeNew.getTurno().getIdTurno()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (sedeNew.getOrganismo() != null) {
      sedeNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        sedeNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(sedeNew);
  }

  public void updateSede(Sede sede) throws Exception
  {
    Sede sedeModify = 
      findSedeById(sede.getIdSede());

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (sede.getRegion() != null) {
      sede.setRegion(
        regionBeanBusiness.findRegionById(
        sede.getRegion().getIdRegion()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (sede.getCiudad() != null) {
      sede.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        sede.getCiudad().getIdCiudad()));
    }

    LugarPagoBeanBusiness lugarPagoBeanBusiness = new LugarPagoBeanBusiness();

    if (sede.getLugarPago() != null) {
      sede.setLugarPago(
        lugarPagoBeanBusiness.findLugarPagoById(
        sede.getLugarPago().getIdLugarPago()));
    }

    TurnoBeanBusiness turnoBeanBusiness = new TurnoBeanBusiness();

    if (sede.getTurno() != null) {
      sede.setTurno(
        turnoBeanBusiness.findTurnoById(
        sede.getTurno().getIdTurno()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (sede.getOrganismo() != null) {
      sede.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        sede.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(sedeModify, sede);
  }

  public void deleteSede(Sede sede) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Sede sedeDelete = 
      findSedeById(sede.getIdSede());
    pm.deletePersistent(sedeDelete);
  }

  public Sede findSedeById(long idSede) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSede == pIdSede";
    Query query = pm.newQuery(Sede.class, filter);

    query.declareParameters("long pIdSede");

    parameters.put("pIdSede", new Long(idSede));

    Collection colSede = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSede.iterator();
    return (Sede)iterator.next();
  }

  public Collection findSedeAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent sedeExtent = pm.getExtent(
      Sede.class, true);
    Query query = pm.newQuery(sedeExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByRegion(long idRegion, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "region.idRegion == pIdRegion &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Sede.class, filter);

    query.declareParameters("long pIdRegion, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegion", new Long(idRegion));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colSede = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSede);

    return colSede;
  }

  public Collection findByCodSede(String codSede, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codSede == pCodSede &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Sede.class, filter);

    query.declareParameters("java.lang.String pCodSede, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodSede", new String(codSede));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colSede = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSede);

    return colSede;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Sede.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colSede = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSede);

    return colSede;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Sede.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("nombre ascending");

    Collection colSede = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSede);

    return colSede;
  }
}