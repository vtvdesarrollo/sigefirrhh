package sigefirrhh.base.estructura;

import java.io.Serializable;

public class ClasificacionDependenciaPK
  implements Serializable
{
  public long idClasificacionDependencia;

  public ClasificacionDependenciaPK()
  {
  }

  public ClasificacionDependenciaPK(long idClasificacionDependencia)
  {
    this.idClasificacionDependencia = idClasificacionDependencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ClasificacionDependenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ClasificacionDependenciaPK thatPK)
  {
    return 
      this.idClasificacionDependencia == thatPK.idClasificacionDependencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idClasificacionDependencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idClasificacionDependencia);
  }
}