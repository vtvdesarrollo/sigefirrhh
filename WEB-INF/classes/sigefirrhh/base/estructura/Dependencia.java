package sigefirrhh.base.estructura;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Dependencia
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_LOCALIDAD;
  private long idDependencia;
  private String codDependencia;
  private String nombre;
  private Estructura estructura;
  private AdministradoraUel administradoraUel;
  private Region region;
  private Sede sede;
  private UnidadFuncional unidadFuncional;
  private TipoDependencia tipoDependencia;
  private String localidad;
  private GrupoOrganismo grupoOrganismo;
  private String vigente;
  private Date fechaVigencia;
  private Date fechaFin;
  private int nivelEstructura;
  private Dependencia dependenciaAnterior;
  private String dependenciaStaff;
  private String aprobacionMpd;
  private String sedeDiplomatica;
  private String codCesta;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "administradoraUel", "aprobacionMpd", "codCesta", "codDependencia", "dependenciaAnterior", "dependenciaStaff", "estructura", "fechaFin", "fechaVigencia", "grupoOrganismo", "idDependencia", "localidad", "nivelEstructura", "nombre", "organismo", "region", "sede", "sedeDiplomatica", "tipoDependencia", "unidadFuncional", "vigente" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.estructura.AdministradoraUel"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Estructura"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.estructura.GrupoOrganismo"), Long.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("sigefirrhh.base.estructura.Region"), sunjdo$classForName$("sigefirrhh.base.estructura.Sede"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.TipoDependencia"), sunjdo$classForName$("sigefirrhh.base.estructura.UnidadFuncional"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 26, 21, 26, 21, 21, 26, 24, 21, 21, 21, 26, 26, 26, 21, 26, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Dependencia());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_LOCALIDAD = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_LOCALIDAD.put("C", "NIVEL CENTRAL");
    LISTA_LOCALIDAD.put("R", "NIVEL REGIONAL");
  }

  public Dependencia()
  {
    jdoSetlocalidad(this, "C");

    jdoSetvigente(this, "S");

    jdoSetnivelEstructura(this, 1);

    jdoSetdependenciaStaff(this, "N");

    jdoSetaprobacionMpd(this, "S");

    jdoSetsedeDiplomatica(this, "N");
  }

  public String toString()
  {
    return 
      jdoGetcodDependencia(this) + " - " + jdoGetnombre(this);
  }

  public AdministradoraUel getAdministradoraUel() {
    return jdoGetadministradoraUel(this);
  }

  public void setAdministradoraUel(AdministradoraUel administradoraUel) {
    jdoSetadministradoraUel(this, administradoraUel);
  }

  public String getAprobacionMpd() {
    return jdoGetaprobacionMpd(this);
  }

  public void setAprobacionMpd(String aprobacionMpd) {
    jdoSetaprobacionMpd(this, aprobacionMpd);
  }

  public String getCodCesta() {
    return jdoGetcodCesta(this);
  }

  public void setCodCesta(String codCesta) {
    jdoSetcodCesta(this, codCesta);
  }

  public String getCodDependencia() {
    return jdoGetcodDependencia(this);
  }

  public void setCodDependencia(String codDependencia) {
    jdoSetcodDependencia(this, codDependencia);
  }

  public Dependencia getDependenciaAnterior() {
    return jdoGetdependenciaAnterior(this);
  }

  public void setDependenciaAnterior(Dependencia dependenciaAnterior) {
    jdoSetdependenciaAnterior(this, dependenciaAnterior);
  }

  public String getDependenciaStaff() {
    return jdoGetdependenciaStaff(this);
  }

  public void setDependenciaStaff(String dependenciaStaff) {
    jdoSetdependenciaStaff(this, dependenciaStaff);
  }

  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }

  public void setFechaFin(Date fechaFin) {
    jdoSetfechaFin(this, fechaFin);
  }

  public Date getFechaVigencia() {
    return jdoGetfechaVigencia(this);
  }

  public void setFechaVigencia(Date fechaVigencia) {
    jdoSetfechaVigencia(this, fechaVigencia);
  }

  public GrupoOrganismo getGrupoOrganismo() {
    return jdoGetgrupoOrganismo(this);
  }

  public void setGrupoOrganismo(GrupoOrganismo grupoOrganismo) {
    jdoSetgrupoOrganismo(this, grupoOrganismo);
  }

  public long getIdDependencia() {
    return jdoGetidDependencia(this);
  }

  public void setIdDependencia(long idDependencia) {
    jdoSetidDependencia(this, idDependencia);
  }

  public String getLocalidad() {
    return jdoGetlocalidad(this);
  }

  public void setLocalidad(String localidad) {
    jdoSetlocalidad(this, localidad);
  }

  public int getNivelEstructura() {
    return jdoGetnivelEstructura(this);
  }

  public void setNivelEstructura(int nivelEstructura) {
    jdoSetnivelEstructura(this, nivelEstructura);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }

  public Region getRegion() {
    return jdoGetregion(this);
  }

  public void setRegion(Region region) {
    jdoSetregion(this, region);
  }

  public Sede getSede() {
    return jdoGetsede(this);
  }

  public void setSede(Sede sede) {
    jdoSetsede(this, sede);
  }

  public String getSedeDiplomatica() {
    return jdoGetsedeDiplomatica(this);
  }

  public void setSedeDiplomatica(String sedeDiplomatica) {
    jdoSetsedeDiplomatica(this, sedeDiplomatica);
  }

  public TipoDependencia getTipoDependencia() {
    return jdoGettipoDependencia(this);
  }

  public void setTipoDependencia(TipoDependencia tipoDependencia) {
    jdoSettipoDependencia(this, tipoDependencia);
  }

  public UnidadFuncional getUnidadFuncional() {
    return jdoGetunidadFuncional(this);
  }

  public void setUnidadFuncional(UnidadFuncional unidadFuncional) {
    jdoSetunidadFuncional(this, unidadFuncional);
  }

  public String getVigente() {
    return jdoGetvigente(this);
  }

  public void setVigente(String vigente) {
    jdoSetvigente(this, vigente);
  }

  public Estructura getEstructura() {
    return jdoGetestructura(this);
  }

  public void setEstructura(Estructura estructura) {
    jdoSetestructura(this, estructura);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 21;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Dependencia localDependencia = new Dependencia();
    localDependencia.jdoFlags = 1;
    localDependencia.jdoStateManager = paramStateManager;
    return localDependencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Dependencia localDependencia = new Dependencia();
    localDependencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDependencia.jdoFlags = 1;
    localDependencia.jdoStateManager = paramStateManager;
    return localDependencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.administradoraUel);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCesta);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codDependencia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependenciaAnterior);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.dependenciaStaff);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.estructura);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoOrganismo);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDependencia);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.localidad);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.nivelEstructura);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.region);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.sede);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sedeDiplomatica);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoDependencia);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadFuncional);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigente);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.administradoraUel = ((AdministradoraUel)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCesta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependenciaAnterior = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependenciaStaff = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estructura = ((Estructura)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoOrganismo = ((GrupoOrganismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDependencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.localidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEstructura = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.region = ((Region)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sede = ((Sede)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sedeDiplomatica = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoDependencia = ((TipoDependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadFuncional = ((UnidadFuncional)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigente = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Dependencia paramDependencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.administradoraUel = paramDependencia.administradoraUel;
      return;
    case 1:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramDependencia.aprobacionMpd;
      return;
    case 2:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.codCesta = paramDependencia.codCesta;
      return;
    case 3:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.codDependencia = paramDependencia.codDependencia;
      return;
    case 4:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.dependenciaAnterior = paramDependencia.dependenciaAnterior;
      return;
    case 5:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.dependenciaStaff = paramDependencia.dependenciaStaff;
      return;
    case 6:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.estructura = paramDependencia.estructura;
      return;
    case 7:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramDependencia.fechaFin;
      return;
    case 8:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramDependencia.fechaVigencia;
      return;
    case 9:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.grupoOrganismo = paramDependencia.grupoOrganismo;
      return;
    case 10:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.idDependencia = paramDependencia.idDependencia;
      return;
    case 11:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.localidad = paramDependencia.localidad;
      return;
    case 12:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEstructura = paramDependencia.nivelEstructura;
      return;
    case 13:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramDependencia.nombre;
      return;
    case 14:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramDependencia.organismo;
      return;
    case 15:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.region = paramDependencia.region;
      return;
    case 16:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.sede = paramDependencia.sede;
      return;
    case 17:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.sedeDiplomatica = paramDependencia.sedeDiplomatica;
      return;
    case 18:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.tipoDependencia = paramDependencia.tipoDependencia;
      return;
    case 19:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.unidadFuncional = paramDependencia.unidadFuncional;
      return;
    case 20:
      if (paramDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.vigente = paramDependencia.vigente;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Dependencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Dependencia localDependencia = (Dependencia)paramObject;
    if (localDependencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDependencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DependenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DependenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DependenciaPK))
      throw new IllegalArgumentException("arg1");
    DependenciaPK localDependenciaPK = (DependenciaPK)paramObject;
    localDependenciaPK.idDependencia = this.idDependencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DependenciaPK))
      throw new IllegalArgumentException("arg1");
    DependenciaPK localDependenciaPK = (DependenciaPK)paramObject;
    this.idDependencia = localDependenciaPK.idDependencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DependenciaPK))
      throw new IllegalArgumentException("arg2");
    DependenciaPK localDependenciaPK = (DependenciaPK)paramObject;
    localDependenciaPK.idDependencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 10);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DependenciaPK))
      throw new IllegalArgumentException("arg2");
    DependenciaPK localDependenciaPK = (DependenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 10, localDependenciaPK.idDependencia);
  }

  private static final AdministradoraUel jdoGetadministradoraUel(Dependencia paramDependencia)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.administradoraUel;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 0))
      return paramDependencia.administradoraUel;
    return (AdministradoraUel)localStateManager.getObjectField(paramDependencia, jdoInheritedFieldCount + 0, paramDependencia.administradoraUel);
  }

  private static final void jdoSetadministradoraUel(Dependencia paramDependencia, AdministradoraUel paramAdministradoraUel)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.administradoraUel = paramAdministradoraUel;
      return;
    }
    localStateManager.setObjectField(paramDependencia, jdoInheritedFieldCount + 0, paramDependencia.administradoraUel, paramAdministradoraUel);
  }

  private static final String jdoGetaprobacionMpd(Dependencia paramDependencia)
  {
    if (paramDependencia.jdoFlags <= 0)
      return paramDependencia.aprobacionMpd;
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.aprobacionMpd;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 1))
      return paramDependencia.aprobacionMpd;
    return localStateManager.getStringField(paramDependencia, jdoInheritedFieldCount + 1, paramDependencia.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(Dependencia paramDependencia, String paramString)
  {
    if (paramDependencia.jdoFlags == 0)
    {
      paramDependencia.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramDependencia, jdoInheritedFieldCount + 1, paramDependencia.aprobacionMpd, paramString);
  }

  private static final String jdoGetcodCesta(Dependencia paramDependencia)
  {
    if (paramDependencia.jdoFlags <= 0)
      return paramDependencia.codCesta;
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.codCesta;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 2))
      return paramDependencia.codCesta;
    return localStateManager.getStringField(paramDependencia, jdoInheritedFieldCount + 2, paramDependencia.codCesta);
  }

  private static final void jdoSetcodCesta(Dependencia paramDependencia, String paramString)
  {
    if (paramDependencia.jdoFlags == 0)
    {
      paramDependencia.codCesta = paramString;
      return;
    }
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.codCesta = paramString;
      return;
    }
    localStateManager.setStringField(paramDependencia, jdoInheritedFieldCount + 2, paramDependencia.codCesta, paramString);
  }

  private static final String jdoGetcodDependencia(Dependencia paramDependencia)
  {
    if (paramDependencia.jdoFlags <= 0)
      return paramDependencia.codDependencia;
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.codDependencia;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 3))
      return paramDependencia.codDependencia;
    return localStateManager.getStringField(paramDependencia, jdoInheritedFieldCount + 3, paramDependencia.codDependencia);
  }

  private static final void jdoSetcodDependencia(Dependencia paramDependencia, String paramString)
  {
    if (paramDependencia.jdoFlags == 0)
    {
      paramDependencia.codDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.codDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramDependencia, jdoInheritedFieldCount + 3, paramDependencia.codDependencia, paramString);
  }

  private static final Dependencia jdoGetdependenciaAnterior(Dependencia paramDependencia)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.dependenciaAnterior;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 4))
      return paramDependencia.dependenciaAnterior;
    return (Dependencia)localStateManager.getObjectField(paramDependencia, jdoInheritedFieldCount + 4, paramDependencia.dependenciaAnterior);
  }

  private static final void jdoSetdependenciaAnterior(Dependencia paramDependencia1, Dependencia paramDependencia2)
  {
    StateManager localStateManager = paramDependencia1.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia1.dependenciaAnterior = paramDependencia2;
      return;
    }
    localStateManager.setObjectField(paramDependencia1, jdoInheritedFieldCount + 4, paramDependencia1.dependenciaAnterior, paramDependencia2);
  }

  private static final String jdoGetdependenciaStaff(Dependencia paramDependencia)
  {
    if (paramDependencia.jdoFlags <= 0)
      return paramDependencia.dependenciaStaff;
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.dependenciaStaff;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 5))
      return paramDependencia.dependenciaStaff;
    return localStateManager.getStringField(paramDependencia, jdoInheritedFieldCount + 5, paramDependencia.dependenciaStaff);
  }

  private static final void jdoSetdependenciaStaff(Dependencia paramDependencia, String paramString)
  {
    if (paramDependencia.jdoFlags == 0)
    {
      paramDependencia.dependenciaStaff = paramString;
      return;
    }
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.dependenciaStaff = paramString;
      return;
    }
    localStateManager.setStringField(paramDependencia, jdoInheritedFieldCount + 5, paramDependencia.dependenciaStaff, paramString);
  }

  private static final Estructura jdoGetestructura(Dependencia paramDependencia)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.estructura;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 6))
      return paramDependencia.estructura;
    return (Estructura)localStateManager.getObjectField(paramDependencia, jdoInheritedFieldCount + 6, paramDependencia.estructura);
  }

  private static final void jdoSetestructura(Dependencia paramDependencia, Estructura paramEstructura)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.estructura = paramEstructura;
      return;
    }
    localStateManager.setObjectField(paramDependencia, jdoInheritedFieldCount + 6, paramDependencia.estructura, paramEstructura);
  }

  private static final Date jdoGetfechaFin(Dependencia paramDependencia)
  {
    if (paramDependencia.jdoFlags <= 0)
      return paramDependencia.fechaFin;
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.fechaFin;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 7))
      return paramDependencia.fechaFin;
    return (Date)localStateManager.getObjectField(paramDependencia, jdoInheritedFieldCount + 7, paramDependencia.fechaFin);
  }

  private static final void jdoSetfechaFin(Dependencia paramDependencia, Date paramDate)
  {
    if (paramDependencia.jdoFlags == 0)
    {
      paramDependencia.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramDependencia, jdoInheritedFieldCount + 7, paramDependencia.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaVigencia(Dependencia paramDependencia)
  {
    if (paramDependencia.jdoFlags <= 0)
      return paramDependencia.fechaVigencia;
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.fechaVigencia;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 8))
      return paramDependencia.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramDependencia, jdoInheritedFieldCount + 8, paramDependencia.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(Dependencia paramDependencia, Date paramDate)
  {
    if (paramDependencia.jdoFlags == 0)
    {
      paramDependencia.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramDependencia, jdoInheritedFieldCount + 8, paramDependencia.fechaVigencia, paramDate);
  }

  private static final GrupoOrganismo jdoGetgrupoOrganismo(Dependencia paramDependencia)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.grupoOrganismo;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 9))
      return paramDependencia.grupoOrganismo;
    return (GrupoOrganismo)localStateManager.getObjectField(paramDependencia, jdoInheritedFieldCount + 9, paramDependencia.grupoOrganismo);
  }

  private static final void jdoSetgrupoOrganismo(Dependencia paramDependencia, GrupoOrganismo paramGrupoOrganismo)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.grupoOrganismo = paramGrupoOrganismo;
      return;
    }
    localStateManager.setObjectField(paramDependencia, jdoInheritedFieldCount + 9, paramDependencia.grupoOrganismo, paramGrupoOrganismo);
  }

  private static final long jdoGetidDependencia(Dependencia paramDependencia)
  {
    return paramDependencia.idDependencia;
  }

  private static final void jdoSetidDependencia(Dependencia paramDependencia, long paramLong)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.idDependencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramDependencia, jdoInheritedFieldCount + 10, paramDependencia.idDependencia, paramLong);
  }

  private static final String jdoGetlocalidad(Dependencia paramDependencia)
  {
    if (paramDependencia.jdoFlags <= 0)
      return paramDependencia.localidad;
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.localidad;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 11))
      return paramDependencia.localidad;
    return localStateManager.getStringField(paramDependencia, jdoInheritedFieldCount + 11, paramDependencia.localidad);
  }

  private static final void jdoSetlocalidad(Dependencia paramDependencia, String paramString)
  {
    if (paramDependencia.jdoFlags == 0)
    {
      paramDependencia.localidad = paramString;
      return;
    }
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.localidad = paramString;
      return;
    }
    localStateManager.setStringField(paramDependencia, jdoInheritedFieldCount + 11, paramDependencia.localidad, paramString);
  }

  private static final int jdoGetnivelEstructura(Dependencia paramDependencia)
  {
    if (paramDependencia.jdoFlags <= 0)
      return paramDependencia.nivelEstructura;
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.nivelEstructura;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 12))
      return paramDependencia.nivelEstructura;
    return localStateManager.getIntField(paramDependencia, jdoInheritedFieldCount + 12, paramDependencia.nivelEstructura);
  }

  private static final void jdoSetnivelEstructura(Dependencia paramDependencia, int paramInt)
  {
    if (paramDependencia.jdoFlags == 0)
    {
      paramDependencia.nivelEstructura = paramInt;
      return;
    }
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.nivelEstructura = paramInt;
      return;
    }
    localStateManager.setIntField(paramDependencia, jdoInheritedFieldCount + 12, paramDependencia.nivelEstructura, paramInt);
  }

  private static final String jdoGetnombre(Dependencia paramDependencia)
  {
    if (paramDependencia.jdoFlags <= 0)
      return paramDependencia.nombre;
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.nombre;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 13))
      return paramDependencia.nombre;
    return localStateManager.getStringField(paramDependencia, jdoInheritedFieldCount + 13, paramDependencia.nombre);
  }

  private static final void jdoSetnombre(Dependencia paramDependencia, String paramString)
  {
    if (paramDependencia.jdoFlags == 0)
    {
      paramDependencia.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramDependencia, jdoInheritedFieldCount + 13, paramDependencia.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(Dependencia paramDependencia)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.organismo;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 14))
      return paramDependencia.organismo;
    return (Organismo)localStateManager.getObjectField(paramDependencia, jdoInheritedFieldCount + 14, paramDependencia.organismo);
  }

  private static final void jdoSetorganismo(Dependencia paramDependencia, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramDependencia, jdoInheritedFieldCount + 14, paramDependencia.organismo, paramOrganismo);
  }

  private static final Region jdoGetregion(Dependencia paramDependencia)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.region;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 15))
      return paramDependencia.region;
    return (Region)localStateManager.getObjectField(paramDependencia, jdoInheritedFieldCount + 15, paramDependencia.region);
  }

  private static final void jdoSetregion(Dependencia paramDependencia, Region paramRegion)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.region = paramRegion;
      return;
    }
    localStateManager.setObjectField(paramDependencia, jdoInheritedFieldCount + 15, paramDependencia.region, paramRegion);
  }

  private static final Sede jdoGetsede(Dependencia paramDependencia)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.sede;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 16))
      return paramDependencia.sede;
    return (Sede)localStateManager.getObjectField(paramDependencia, jdoInheritedFieldCount + 16, paramDependencia.sede);
  }

  private static final void jdoSetsede(Dependencia paramDependencia, Sede paramSede)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.sede = paramSede;
      return;
    }
    localStateManager.setObjectField(paramDependencia, jdoInheritedFieldCount + 16, paramDependencia.sede, paramSede);
  }

  private static final String jdoGetsedeDiplomatica(Dependencia paramDependencia)
  {
    if (paramDependencia.jdoFlags <= 0)
      return paramDependencia.sedeDiplomatica;
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.sedeDiplomatica;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 17))
      return paramDependencia.sedeDiplomatica;
    return localStateManager.getStringField(paramDependencia, jdoInheritedFieldCount + 17, paramDependencia.sedeDiplomatica);
  }

  private static final void jdoSetsedeDiplomatica(Dependencia paramDependencia, String paramString)
  {
    if (paramDependencia.jdoFlags == 0)
    {
      paramDependencia.sedeDiplomatica = paramString;
      return;
    }
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.sedeDiplomatica = paramString;
      return;
    }
    localStateManager.setStringField(paramDependencia, jdoInheritedFieldCount + 17, paramDependencia.sedeDiplomatica, paramString);
  }

  private static final TipoDependencia jdoGettipoDependencia(Dependencia paramDependencia)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.tipoDependencia;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 18))
      return paramDependencia.tipoDependencia;
    return (TipoDependencia)localStateManager.getObjectField(paramDependencia, jdoInheritedFieldCount + 18, paramDependencia.tipoDependencia);
  }

  private static final void jdoSettipoDependencia(Dependencia paramDependencia, TipoDependencia paramTipoDependencia)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.tipoDependencia = paramTipoDependencia;
      return;
    }
    localStateManager.setObjectField(paramDependencia, jdoInheritedFieldCount + 18, paramDependencia.tipoDependencia, paramTipoDependencia);
  }

  private static final UnidadFuncional jdoGetunidadFuncional(Dependencia paramDependencia)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.unidadFuncional;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 19))
      return paramDependencia.unidadFuncional;
    return (UnidadFuncional)localStateManager.getObjectField(paramDependencia, jdoInheritedFieldCount + 19, paramDependencia.unidadFuncional);
  }

  private static final void jdoSetunidadFuncional(Dependencia paramDependencia, UnidadFuncional paramUnidadFuncional)
  {
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.unidadFuncional = paramUnidadFuncional;
      return;
    }
    localStateManager.setObjectField(paramDependencia, jdoInheritedFieldCount + 19, paramDependencia.unidadFuncional, paramUnidadFuncional);
  }

  private static final String jdoGetvigente(Dependencia paramDependencia)
  {
    if (paramDependencia.jdoFlags <= 0)
      return paramDependencia.vigente;
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramDependencia.vigente;
    if (localStateManager.isLoaded(paramDependencia, jdoInheritedFieldCount + 20))
      return paramDependencia.vigente;
    return localStateManager.getStringField(paramDependencia, jdoInheritedFieldCount + 20, paramDependencia.vigente);
  }

  private static final void jdoSetvigente(Dependencia paramDependencia, String paramString)
  {
    if (paramDependencia.jdoFlags == 0)
    {
      paramDependencia.vigente = paramString;
      return;
    }
    StateManager localStateManager = paramDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependencia.vigente = paramString;
      return;
    }
    localStateManager.setStringField(paramDependencia, jdoInheritedFieldCount + 20, paramDependencia.vigente, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}