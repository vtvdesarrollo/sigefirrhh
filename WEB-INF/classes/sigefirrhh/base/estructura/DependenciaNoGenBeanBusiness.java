package sigefirrhh.base.estructura;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import sigefirrhh.general.Lista;

public class DependenciaNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByIdOrganismo(long id)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsData = null;
    PreparedStatement stData = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select a.id_dependencia as id, (a.nombre || ' - ' || a.cod_dependencia)  as descripcion  ");
      sql.append(" from dependencia a");
      sql.append(" where a.id_organismo = ?");
      sql.append(" order by a.nombre");

      stData = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stData.setLong(1, id);
      rsData = stData.executeQuery();

      Lista lista = new Lista();
      while (rsData.next()) {
        lista = new Lista();
        lista.setId(rsData.getLong("id"));
        lista.setNombre(rsData.getString("descripcion"));
        col.add(lista);
      }

      return col;
    } finally {
      if (rsData != null) try {
          rsData.close();
        } catch (Exception localException3) {
        } if (stData != null) try {
          stData.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findByPersonal(long idPersonal, String estatus) throws Exception { Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select a.id_dependencia as id, (a.nombre || ' - ' || a.cod_dependencia)  as descripcion  ");
      sql.append(" from dependencia a");
      sql.append(" where id_dependencia in (");
      sql.append(" select id_dependencia from trabajador");
      sql.append(" where id_personal = ?");
      sql.append(" and estatus = ?)");
      sql.append(" order by a.nombre");
      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idPersonal);
      stRegistros.setString(2, estatus);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5) {
        } 
    } } 
  public Dependencia findByCodigoAndRegion(String codDependencia, long idRegion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "codDependencia == pCodDependencia && region.idRegion == pIdRegion";
    Query query = pm.newQuery(Dependencia.class, filter);

    query.declareParameters("String pCodDependencia, long pIdRegion");

    parameters.put("pCodDependencia", codDependencia);
    parameters.put("pIdRegion", new Long(idRegion));

    Collection colDependencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDependencia.iterator();
    return (Dependencia)iterator.next();
  }
}