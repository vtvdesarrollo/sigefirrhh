package sigefirrhh.base.ubicacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class CiudadForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CiudadForm.class.getName());
  private Ciudad ciudad;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private boolean showCiudadByCodCiudad;
  private boolean showCiudadByNombre;
  private boolean showCiudadByEstado;
  private String findCodCiudad;
  private String findNombre;
  private String findSelectPaisForEstado;
  private String findSelectEstado;
  private Collection findColPaisForEstado;
  private Collection findColEstado;
  private Collection colPaisForEstado;
  private Collection colEstado;
  private String selectPaisForEstado;
  private String selectEstado;
  private Object stateResultCiudadByCodCiudad = null;

  private Object stateResultCiudadByNombre = null;

  private Object stateResultCiudadByEstado = null;

  public String getFindCodCiudad()
  {
    return this.findCodCiudad;
  }
  public void setFindCodCiudad(String findCodCiudad) {
    this.findCodCiudad = findCodCiudad;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }
  public Collection getFindColPaisForEstado() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPaisForEstado.iterator();
    Pais paisForEstado = null;
    while (iterator.hasNext()) {
      paisForEstado = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForEstado.getIdPais()), 
        paisForEstado.toString()));
    }
    return col;
  }
  public String getFindSelectPaisForEstado() {
    return this.findSelectPaisForEstado;
  }
  public void setFindSelectPaisForEstado(String valPaisForEstado) {
    this.findSelectPaisForEstado = valPaisForEstado;
  }
  public void findChangePaisForEstado(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColEstado = null;
      if (idPais > 0L)
        this.findColEstado = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowPaisForEstado() { return this.findColPaisForEstado != null; }

  public String getFindSelectEstado() {
    return this.findSelectEstado;
  }
  public void setFindSelectEstado(String valEstado) {
    this.findSelectEstado = valEstado;
  }

  public Collection getFindColEstado() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColEstado.iterator();
    Estado estado = null;
    while (iterator.hasNext()) {
      estado = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estado.getIdEstado()), 
        estado.toString()));
    }
    return col;
  }
  public boolean isFindShowEstado() {
    return this.findColEstado != null;
  }

  public String getSelectPaisForEstado()
  {
    return this.selectPaisForEstado;
  }
  public void setSelectPaisForEstado(String valPaisForEstado) {
    this.selectPaisForEstado = valPaisForEstado;
  }
  public void changePaisForEstado(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colEstado = null;
      if (idPais > 0L) {
        this.colEstado = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
      } else {
        this.selectEstado = null;
        this.ciudad.setEstado(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectEstado = null;
      this.ciudad.setEstado(
        null);
    }
  }

  public boolean isShowPaisForEstado() { return this.colPaisForEstado != null; }

  public String getSelectEstado() {
    return this.selectEstado;
  }
  public void setSelectEstado(String valEstado) {
    Iterator iterator = this.colEstado.iterator();
    Estado estado = null;
    this.ciudad.setEstado(null);
    while (iterator.hasNext()) {
      estado = (Estado)iterator.next();
      if (String.valueOf(estado.getIdEstado()).equals(
        valEstado)) {
        this.ciudad.setEstado(
          estado);
        break;
      }
    }
    this.selectEstado = valEstado;
  }
  public boolean isShowEstado() {
    return this.colEstado != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public Ciudad getCiudad() {
    if (this.ciudad == null) {
      this.ciudad = new Ciudad();
    }
    return this.ciudad;
  }

  public CiudadForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPaisForEstado()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForEstado.iterator();
    Pais paisForEstado = null;
    while (iterator.hasNext()) {
      paisForEstado = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForEstado.getIdPais()), 
        paisForEstado.toString()));
    }
    return col;
  }

  public Collection getColEstado()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstado.iterator();
    Estado estado = null;
    while (iterator.hasNext()) {
      estado = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estado.getIdEstado()), 
        estado.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColPaisForEstado = 
        this.ubicacionFacade.findAllPais();

      this.colPaisForEstado = 
        this.ubicacionFacade.findAllPais();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findCiudadByCodCiudad()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findCiudadByCodCiudad(this.findCodCiudad);
      this.showCiudadByCodCiudad = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCiudadByCodCiudad)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodCiudad = null;
    this.findNombre = null;
    this.findSelectPaisForEstado = null;
    this.findSelectEstado = null;

    return null;
  }

  public String findCiudadByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findCiudadByNombre(this.findNombre);
      this.showCiudadByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCiudadByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodCiudad = null;
    this.findNombre = null;
    this.findSelectPaisForEstado = null;
    this.findSelectEstado = null;

    return null;
  }

  public String findCiudadByEstado()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findCiudadByEstado(Long.valueOf(this.findSelectEstado).longValue());
      this.showCiudadByEstado = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCiudadByEstado)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodCiudad = null;
    this.findNombre = null;
    this.findSelectPaisForEstado = null;
    this.findSelectEstado = null;

    return null;
  }

  public boolean isShowCiudadByCodCiudad() {
    return this.showCiudadByCodCiudad;
  }
  public boolean isShowCiudadByNombre() {
    return this.showCiudadByNombre;
  }
  public boolean isShowCiudadByEstado() {
    return this.showCiudadByEstado;
  }

  public String selectCiudad()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectEstado = null;
    this.selectPaisForEstado = null;

    long idCiudad = 
      Long.parseLong((String)requestParameterMap.get("idCiudad"));
    try
    {
      this.ciudad = 
        this.ubicacionFacade.findCiudadById(
        idCiudad);
      if (this.ciudad.getEstado() != null) {
        this.selectEstado = 
          String.valueOf(this.ciudad.getEstado().getIdEstado());
      }

      Estado estado = null;
      Pais paisForEstado = null;

      if (this.ciudad.getEstado() != null) {
        long idEstado = 
          this.ciudad.getEstado().getIdEstado();
        this.selectEstado = String.valueOf(idEstado);
        estado = this.ubicacionFacade.findEstadoById(
          idEstado);
        this.colEstado = this.ubicacionFacade.findEstadoByPais(
          estado.getPais().getIdPais());

        long idPaisForEstado = 
          this.ciudad.getEstado().getPais().getIdPais();
        this.selectPaisForEstado = String.valueOf(idPaisForEstado);
        paisForEstado = 
          this.ubicacionFacade.findPaisById(
          idPaisForEstado);
        this.colPaisForEstado = 
          this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.ciudad = null;
    this.showCiudadByCodCiudad = false;
    this.showCiudadByNombre = false;
    this.showCiudadByEstado = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.ubicacionFacade.addCiudad(
          this.ciudad);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.ubicacionFacade.updateCiudad(
          this.ciudad);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.ubicacionFacade.deleteCiudad(
        this.ciudad);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.ciudad = new Ciudad();

    this.selectEstado = null;

    this.selectPaisForEstado = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.ciudad.setIdCiudad(identityGenerator.getNextSequenceNumber("sigefirrhh.base.ubicacion.Ciudad"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.ciudad = new Ciudad();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}