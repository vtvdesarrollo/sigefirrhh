package sigefirrhh.base.ubicacion;

import java.io.Serializable;

public class PaisPK
  implements Serializable
{
  public long idPais;

  public PaisPK()
  {
  }

  public PaisPK(long idPais)
  {
    this.idPais = idPais;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PaisPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PaisPK thatPK)
  {
    return 
      this.idPais == thatPK.idPais;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPais)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPais);
  }
}