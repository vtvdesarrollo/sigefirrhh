package sigefirrhh.base.ubicacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Ciudad
  implements Serializable, PersistenceCapable
{
  private long idCiudad;
  private String codCiudad;
  private String nombre;
  private String abreviatura;
  private Estado estado;
  private double multiplicador;
  private double fluctuacion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "abreviatura", "codCiudad", "estado", "fluctuacion", "idCiudad", "multiplicador", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Estado"), Double.TYPE, Long.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 26, 21, 24, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " " + 
      jdoGetcodCiudad(this) + " " + 
      jdoGetestado(this).getNombre();
  }

  public String getAbreviatura() {
    return jdoGetabreviatura(this);
  }

  public void setAbreviatura(String abreviatura) {
    jdoSetabreviatura(this, abreviatura);
  }

  public String getCodCiudad() {
    return jdoGetcodCiudad(this);
  }

  public void setCodCiudad(String codCiudad) {
    jdoSetcodCiudad(this, codCiudad);
  }

  public Estado getEstado() {
    return jdoGetestado(this);
  }

  public void setEstado(Estado estado) {
    jdoSetestado(this, estado);
  }

  public double getFluctuacion() {
    return jdoGetfluctuacion(this);
  }

  public void setFluctuacion(double fluctuacion) {
    jdoSetfluctuacion(this, fluctuacion);
  }

  public long getIdCiudad() {
    return jdoGetidCiudad(this);
  }

  public void setIdCiudad(long idCiudad) {
    jdoSetidCiudad(this, idCiudad);
  }

  public double getMultiplicador() {
    return jdoGetmultiplicador(this);
  }

  public void setMultiplicador(double multiplicador) {
    jdoSetmultiplicador(this, multiplicador);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Ciudad());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Ciudad localCiudad = new Ciudad();
    localCiudad.jdoFlags = 1;
    localCiudad.jdoStateManager = paramStateManager;
    return localCiudad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Ciudad localCiudad = new Ciudad();
    localCiudad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCiudad.jdoFlags = 1;
    localCiudad.jdoStateManager = paramStateManager;
    return localCiudad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.abreviatura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCiudad);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.estado);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.fluctuacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCiudad);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.multiplicador);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.abreviatura = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCiudad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estado = ((Estado)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fluctuacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCiudad = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.multiplicador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Ciudad paramCiudad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCiudad == null)
        throw new IllegalArgumentException("arg1");
      this.abreviatura = paramCiudad.abreviatura;
      return;
    case 1:
      if (paramCiudad == null)
        throw new IllegalArgumentException("arg1");
      this.codCiudad = paramCiudad.codCiudad;
      return;
    case 2:
      if (paramCiudad == null)
        throw new IllegalArgumentException("arg1");
      this.estado = paramCiudad.estado;
      return;
    case 3:
      if (paramCiudad == null)
        throw new IllegalArgumentException("arg1");
      this.fluctuacion = paramCiudad.fluctuacion;
      return;
    case 4:
      if (paramCiudad == null)
        throw new IllegalArgumentException("arg1");
      this.idCiudad = paramCiudad.idCiudad;
      return;
    case 5:
      if (paramCiudad == null)
        throw new IllegalArgumentException("arg1");
      this.multiplicador = paramCiudad.multiplicador;
      return;
    case 6:
      if (paramCiudad == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramCiudad.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Ciudad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Ciudad localCiudad = (Ciudad)paramObject;
    if (localCiudad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCiudad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CiudadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CiudadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CiudadPK))
      throw new IllegalArgumentException("arg1");
    CiudadPK localCiudadPK = (CiudadPK)paramObject;
    localCiudadPK.idCiudad = this.idCiudad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CiudadPK))
      throw new IllegalArgumentException("arg1");
    CiudadPK localCiudadPK = (CiudadPK)paramObject;
    this.idCiudad = localCiudadPK.idCiudad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CiudadPK))
      throw new IllegalArgumentException("arg2");
    CiudadPK localCiudadPK = (CiudadPK)paramObject;
    localCiudadPK.idCiudad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CiudadPK))
      throw new IllegalArgumentException("arg2");
    CiudadPK localCiudadPK = (CiudadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localCiudadPK.idCiudad);
  }

  private static final String jdoGetabreviatura(Ciudad paramCiudad)
  {
    if (paramCiudad.jdoFlags <= 0)
      return paramCiudad.abreviatura;
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
      return paramCiudad.abreviatura;
    if (localStateManager.isLoaded(paramCiudad, jdoInheritedFieldCount + 0))
      return paramCiudad.abreviatura;
    return localStateManager.getStringField(paramCiudad, jdoInheritedFieldCount + 0, paramCiudad.abreviatura);
  }

  private static final void jdoSetabreviatura(Ciudad paramCiudad, String paramString)
  {
    if (paramCiudad.jdoFlags == 0)
    {
      paramCiudad.abreviatura = paramString;
      return;
    }
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCiudad.abreviatura = paramString;
      return;
    }
    localStateManager.setStringField(paramCiudad, jdoInheritedFieldCount + 0, paramCiudad.abreviatura, paramString);
  }

  private static final String jdoGetcodCiudad(Ciudad paramCiudad)
  {
    if (paramCiudad.jdoFlags <= 0)
      return paramCiudad.codCiudad;
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
      return paramCiudad.codCiudad;
    if (localStateManager.isLoaded(paramCiudad, jdoInheritedFieldCount + 1))
      return paramCiudad.codCiudad;
    return localStateManager.getStringField(paramCiudad, jdoInheritedFieldCount + 1, paramCiudad.codCiudad);
  }

  private static final void jdoSetcodCiudad(Ciudad paramCiudad, String paramString)
  {
    if (paramCiudad.jdoFlags == 0)
    {
      paramCiudad.codCiudad = paramString;
      return;
    }
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCiudad.codCiudad = paramString;
      return;
    }
    localStateManager.setStringField(paramCiudad, jdoInheritedFieldCount + 1, paramCiudad.codCiudad, paramString);
  }

  private static final Estado jdoGetestado(Ciudad paramCiudad)
  {
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
      return paramCiudad.estado;
    if (localStateManager.isLoaded(paramCiudad, jdoInheritedFieldCount + 2))
      return paramCiudad.estado;
    return (Estado)localStateManager.getObjectField(paramCiudad, jdoInheritedFieldCount + 2, paramCiudad.estado);
  }

  private static final void jdoSetestado(Ciudad paramCiudad, Estado paramEstado)
  {
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCiudad.estado = paramEstado;
      return;
    }
    localStateManager.setObjectField(paramCiudad, jdoInheritedFieldCount + 2, paramCiudad.estado, paramEstado);
  }

  private static final double jdoGetfluctuacion(Ciudad paramCiudad)
  {
    if (paramCiudad.jdoFlags <= 0)
      return paramCiudad.fluctuacion;
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
      return paramCiudad.fluctuacion;
    if (localStateManager.isLoaded(paramCiudad, jdoInheritedFieldCount + 3))
      return paramCiudad.fluctuacion;
    return localStateManager.getDoubleField(paramCiudad, jdoInheritedFieldCount + 3, paramCiudad.fluctuacion);
  }

  private static final void jdoSetfluctuacion(Ciudad paramCiudad, double paramDouble)
  {
    if (paramCiudad.jdoFlags == 0)
    {
      paramCiudad.fluctuacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCiudad.fluctuacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCiudad, jdoInheritedFieldCount + 3, paramCiudad.fluctuacion, paramDouble);
  }

  private static final long jdoGetidCiudad(Ciudad paramCiudad)
  {
    return paramCiudad.idCiudad;
  }

  private static final void jdoSetidCiudad(Ciudad paramCiudad, long paramLong)
  {
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCiudad.idCiudad = paramLong;
      return;
    }
    localStateManager.setLongField(paramCiudad, jdoInheritedFieldCount + 4, paramCiudad.idCiudad, paramLong);
  }

  private static final double jdoGetmultiplicador(Ciudad paramCiudad)
  {
    if (paramCiudad.jdoFlags <= 0)
      return paramCiudad.multiplicador;
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
      return paramCiudad.multiplicador;
    if (localStateManager.isLoaded(paramCiudad, jdoInheritedFieldCount + 5))
      return paramCiudad.multiplicador;
    return localStateManager.getDoubleField(paramCiudad, jdoInheritedFieldCount + 5, paramCiudad.multiplicador);
  }

  private static final void jdoSetmultiplicador(Ciudad paramCiudad, double paramDouble)
  {
    if (paramCiudad.jdoFlags == 0)
    {
      paramCiudad.multiplicador = paramDouble;
      return;
    }
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCiudad.multiplicador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCiudad, jdoInheritedFieldCount + 5, paramCiudad.multiplicador, paramDouble);
  }

  private static final String jdoGetnombre(Ciudad paramCiudad)
  {
    if (paramCiudad.jdoFlags <= 0)
      return paramCiudad.nombre;
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
      return paramCiudad.nombre;
    if (localStateManager.isLoaded(paramCiudad, jdoInheritedFieldCount + 6))
      return paramCiudad.nombre;
    return localStateManager.getStringField(paramCiudad, jdoInheritedFieldCount + 6, paramCiudad.nombre);
  }

  private static final void jdoSetnombre(Ciudad paramCiudad, String paramString)
  {
    if (paramCiudad.jdoFlags == 0)
    {
      paramCiudad.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramCiudad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCiudad.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramCiudad, jdoInheritedFieldCount + 6, paramCiudad.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}