package sigefirrhh.base.ubicacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class RegionContinenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RegionContinenteForm.class.getName());
  private RegionContinente regionContinente;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private boolean showRegionContinenteByCodRegionContinente;
  private boolean showRegionContinenteByNombre;
  private boolean showRegionContinenteByContinente;
  private String findCodRegionContinente;
  private String findNombre;
  private String findSelectContinente;
  private Collection findColContinente;
  private Collection colContinente;
  private String selectContinente;
  private Object stateResultRegionContinenteByCodRegionContinente = null;

  private Object stateResultRegionContinenteByNombre = null;

  private Object stateResultRegionContinenteByContinente = null;

  public String getFindCodRegionContinente()
  {
    return this.findCodRegionContinente;
  }
  public void setFindCodRegionContinente(String findCodRegionContinente) {
    this.findCodRegionContinente = findCodRegionContinente;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }
  public String getFindSelectContinente() {
    return this.findSelectContinente;
  }
  public void setFindSelectContinente(String valContinente) {
    this.findSelectContinente = valContinente;
  }

  public Collection getFindColContinente() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColContinente.iterator();
    Continente continente = null;
    while (iterator.hasNext()) {
      continente = (Continente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(continente.getIdContinente()), 
        continente.toString()));
    }
    return col;
  }

  public String getSelectContinente()
  {
    return this.selectContinente;
  }
  public void setSelectContinente(String valContinente) {
    Iterator iterator = this.colContinente.iterator();
    Continente continente = null;
    this.regionContinente.setContinente(null);
    while (iterator.hasNext()) {
      continente = (Continente)iterator.next();
      if (String.valueOf(continente.getIdContinente()).equals(
        valContinente)) {
        this.regionContinente.setContinente(
          continente);
        break;
      }
    }
    this.selectContinente = valContinente;
  }
  public Collection getResult() {
    return this.result;
  }

  public RegionContinente getRegionContinente() {
    if (this.regionContinente == null) {
      this.regionContinente = new RegionContinente();
    }
    return this.regionContinente;
  }

  public RegionContinenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColContinente()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colContinente.iterator();
    Continente continente = null;
    while (iterator.hasNext()) {
      continente = (Continente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(continente.getIdContinente()), 
        continente.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColContinente = 
        this.ubicacionFacade.findAllContinente();

      this.colContinente = 
        this.ubicacionFacade.findAllContinente();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findRegionContinenteByCodRegionContinente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findRegionContinenteByCodRegionContinente(this.findCodRegionContinente);
      this.showRegionContinenteByCodRegionContinente = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRegionContinenteByCodRegionContinente)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodRegionContinente = null;
    this.findNombre = null;
    this.findSelectContinente = null;

    return null;
  }

  public String findRegionContinenteByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findRegionContinenteByNombre(this.findNombre);
      this.showRegionContinenteByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRegionContinenteByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodRegionContinente = null;
    this.findNombre = null;
    this.findSelectContinente = null;

    return null;
  }

  public String findRegionContinenteByContinente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findRegionContinenteByContinente(Long.valueOf(this.findSelectContinente).longValue());
      this.showRegionContinenteByContinente = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRegionContinenteByContinente)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodRegionContinente = null;
    this.findNombre = null;
    this.findSelectContinente = null;

    return null;
  }

  public boolean isShowRegionContinenteByCodRegionContinente() {
    return this.showRegionContinenteByCodRegionContinente;
  }
  public boolean isShowRegionContinenteByNombre() {
    return this.showRegionContinenteByNombre;
  }
  public boolean isShowRegionContinenteByContinente() {
    return this.showRegionContinenteByContinente;
  }

  public String selectRegionContinente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectContinente = null;

    long idRegionContinente = 
      Long.parseLong((String)requestParameterMap.get("idRegionContinente"));
    try
    {
      this.regionContinente = 
        this.ubicacionFacade.findRegionContinenteById(
        idRegionContinente);
      if (this.regionContinente.getContinente() != null) {
        this.selectContinente = 
          String.valueOf(this.regionContinente.getContinente().getIdContinente());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.regionContinente = null;
    this.showRegionContinenteByCodRegionContinente = false;
    this.showRegionContinenteByNombre = false;
    this.showRegionContinenteByContinente = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.ubicacionFacade.addRegionContinente(
          this.regionContinente);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.ubicacionFacade.updateRegionContinente(
          this.regionContinente);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.ubicacionFacade.deleteRegionContinente(
        this.regionContinente);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.regionContinente = new RegionContinente();

    this.selectContinente = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.regionContinente.setIdRegionContinente(identityGenerator.getNextSequenceNumber("sigefirrhh.base.ubicacion.RegionContinente"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.regionContinente = new RegionContinente();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}