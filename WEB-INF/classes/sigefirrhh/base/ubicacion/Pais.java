package sigefirrhh.base.ubicacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Pais
  implements Serializable, PersistenceCapable
{
  private long idPais;
  private String codPais;
  private String nombre;
  private String abreviatura;
  private String moneda;
  private String monedaSing;
  private String monedaPlur;
  private String simbolo;
  private String fraccion;
  private RegionContinente regionContinente;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "abreviatura", "codPais", "fraccion", "idPais", "moneda", "monedaPlur", "monedaSing", "nombre", "regionContinente", "simbolo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.RegionContinente"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " " + jdoGetcodPais(this);
  }

  public String getAbreviatura()
  {
    return jdoGetabreviatura(this);
  }

  public String getCodPais()
  {
    return jdoGetcodPais(this);
  }

  public long getIdPais()
  {
    return jdoGetidPais(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setAbreviatura(String string)
  {
    jdoSetabreviatura(this, string);
  }

  public void setCodPais(String string)
  {
    jdoSetcodPais(this, string);
  }

  public void setIdPais(long l)
  {
    jdoSetidPais(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public String getFraccion()
  {
    return jdoGetfraccion(this);
  }

  public String getMoneda()
  {
    return jdoGetmoneda(this);
  }

  public String getMonedaPlur()
  {
    return jdoGetmonedaPlur(this);
  }

  public String getMonedaSing()
  {
    return jdoGetmonedaSing(this);
  }

  public String getSimbolo()
  {
    return jdoGetsimbolo(this);
  }

  public void setFraccion(String string)
  {
    jdoSetfraccion(this, string);
  }

  public void setMoneda(String string)
  {
    jdoSetmoneda(this, string);
  }

  public void setMonedaPlur(String string)
  {
    jdoSetmonedaPlur(this, string);
  }

  public void setMonedaSing(String string)
  {
    jdoSetmonedaSing(this, string);
  }

  public void setSimbolo(String string)
  {
    jdoSetsimbolo(this, string);
  }

  public RegionContinente getRegionContinente()
  {
    return jdoGetregionContinente(this);
  }

  public void setRegionContinente(RegionContinente continente)
  {
    jdoSetregionContinente(this, continente);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.ubicacion.Pais"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Pais());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Pais localPais = new Pais();
    localPais.jdoFlags = 1;
    localPais.jdoStateManager = paramStateManager;
    return localPais;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Pais localPais = new Pais();
    localPais.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPais.jdoFlags = 1;
    localPais.jdoStateManager = paramStateManager;
    return localPais;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.abreviatura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codPais);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.fraccion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPais);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.moneda);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.monedaPlur);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.monedaSing);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.regionContinente);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.simbolo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.abreviatura = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codPais = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fraccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPais = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.moneda = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monedaPlur = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monedaSing = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.regionContinente = ((RegionContinente)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.simbolo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Pais paramPais, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPais == null)
        throw new IllegalArgumentException("arg1");
      this.abreviatura = paramPais.abreviatura;
      return;
    case 1:
      if (paramPais == null)
        throw new IllegalArgumentException("arg1");
      this.codPais = paramPais.codPais;
      return;
    case 2:
      if (paramPais == null)
        throw new IllegalArgumentException("arg1");
      this.fraccion = paramPais.fraccion;
      return;
    case 3:
      if (paramPais == null)
        throw new IllegalArgumentException("arg1");
      this.idPais = paramPais.idPais;
      return;
    case 4:
      if (paramPais == null)
        throw new IllegalArgumentException("arg1");
      this.moneda = paramPais.moneda;
      return;
    case 5:
      if (paramPais == null)
        throw new IllegalArgumentException("arg1");
      this.monedaPlur = paramPais.monedaPlur;
      return;
    case 6:
      if (paramPais == null)
        throw new IllegalArgumentException("arg1");
      this.monedaSing = paramPais.monedaSing;
      return;
    case 7:
      if (paramPais == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramPais.nombre;
      return;
    case 8:
      if (paramPais == null)
        throw new IllegalArgumentException("arg1");
      this.regionContinente = paramPais.regionContinente;
      return;
    case 9:
      if (paramPais == null)
        throw new IllegalArgumentException("arg1");
      this.simbolo = paramPais.simbolo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Pais))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Pais localPais = (Pais)paramObject;
    if (localPais.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPais, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PaisPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PaisPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PaisPK))
      throw new IllegalArgumentException("arg1");
    PaisPK localPaisPK = (PaisPK)paramObject;
    localPaisPK.idPais = this.idPais;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PaisPK))
      throw new IllegalArgumentException("arg1");
    PaisPK localPaisPK = (PaisPK)paramObject;
    this.idPais = localPaisPK.idPais;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PaisPK))
      throw new IllegalArgumentException("arg2");
    PaisPK localPaisPK = (PaisPK)paramObject;
    localPaisPK.idPais = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PaisPK))
      throw new IllegalArgumentException("arg2");
    PaisPK localPaisPK = (PaisPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localPaisPK.idPais);
  }

  private static final String jdoGetabreviatura(Pais paramPais)
  {
    if (paramPais.jdoFlags <= 0)
      return paramPais.abreviatura;
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
      return paramPais.abreviatura;
    if (localStateManager.isLoaded(paramPais, jdoInheritedFieldCount + 0))
      return paramPais.abreviatura;
    return localStateManager.getStringField(paramPais, jdoInheritedFieldCount + 0, paramPais.abreviatura);
  }

  private static final void jdoSetabreviatura(Pais paramPais, String paramString)
  {
    if (paramPais.jdoFlags == 0)
    {
      paramPais.abreviatura = paramString;
      return;
    }
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
    {
      paramPais.abreviatura = paramString;
      return;
    }
    localStateManager.setStringField(paramPais, jdoInheritedFieldCount + 0, paramPais.abreviatura, paramString);
  }

  private static final String jdoGetcodPais(Pais paramPais)
  {
    if (paramPais.jdoFlags <= 0)
      return paramPais.codPais;
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
      return paramPais.codPais;
    if (localStateManager.isLoaded(paramPais, jdoInheritedFieldCount + 1))
      return paramPais.codPais;
    return localStateManager.getStringField(paramPais, jdoInheritedFieldCount + 1, paramPais.codPais);
  }

  private static final void jdoSetcodPais(Pais paramPais, String paramString)
  {
    if (paramPais.jdoFlags == 0)
    {
      paramPais.codPais = paramString;
      return;
    }
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
    {
      paramPais.codPais = paramString;
      return;
    }
    localStateManager.setStringField(paramPais, jdoInheritedFieldCount + 1, paramPais.codPais, paramString);
  }

  private static final String jdoGetfraccion(Pais paramPais)
  {
    if (paramPais.jdoFlags <= 0)
      return paramPais.fraccion;
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
      return paramPais.fraccion;
    if (localStateManager.isLoaded(paramPais, jdoInheritedFieldCount + 2))
      return paramPais.fraccion;
    return localStateManager.getStringField(paramPais, jdoInheritedFieldCount + 2, paramPais.fraccion);
  }

  private static final void jdoSetfraccion(Pais paramPais, String paramString)
  {
    if (paramPais.jdoFlags == 0)
    {
      paramPais.fraccion = paramString;
      return;
    }
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
    {
      paramPais.fraccion = paramString;
      return;
    }
    localStateManager.setStringField(paramPais, jdoInheritedFieldCount + 2, paramPais.fraccion, paramString);
  }

  private static final long jdoGetidPais(Pais paramPais)
  {
    return paramPais.idPais;
  }

  private static final void jdoSetidPais(Pais paramPais, long paramLong)
  {
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
    {
      paramPais.idPais = paramLong;
      return;
    }
    localStateManager.setLongField(paramPais, jdoInheritedFieldCount + 3, paramPais.idPais, paramLong);
  }

  private static final String jdoGetmoneda(Pais paramPais)
  {
    if (paramPais.jdoFlags <= 0)
      return paramPais.moneda;
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
      return paramPais.moneda;
    if (localStateManager.isLoaded(paramPais, jdoInheritedFieldCount + 4))
      return paramPais.moneda;
    return localStateManager.getStringField(paramPais, jdoInheritedFieldCount + 4, paramPais.moneda);
  }

  private static final void jdoSetmoneda(Pais paramPais, String paramString)
  {
    if (paramPais.jdoFlags == 0)
    {
      paramPais.moneda = paramString;
      return;
    }
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
    {
      paramPais.moneda = paramString;
      return;
    }
    localStateManager.setStringField(paramPais, jdoInheritedFieldCount + 4, paramPais.moneda, paramString);
  }

  private static final String jdoGetmonedaPlur(Pais paramPais)
  {
    if (paramPais.jdoFlags <= 0)
      return paramPais.monedaPlur;
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
      return paramPais.monedaPlur;
    if (localStateManager.isLoaded(paramPais, jdoInheritedFieldCount + 5))
      return paramPais.monedaPlur;
    return localStateManager.getStringField(paramPais, jdoInheritedFieldCount + 5, paramPais.monedaPlur);
  }

  private static final void jdoSetmonedaPlur(Pais paramPais, String paramString)
  {
    if (paramPais.jdoFlags == 0)
    {
      paramPais.monedaPlur = paramString;
      return;
    }
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
    {
      paramPais.monedaPlur = paramString;
      return;
    }
    localStateManager.setStringField(paramPais, jdoInheritedFieldCount + 5, paramPais.monedaPlur, paramString);
  }

  private static final String jdoGetmonedaSing(Pais paramPais)
  {
    if (paramPais.jdoFlags <= 0)
      return paramPais.monedaSing;
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
      return paramPais.monedaSing;
    if (localStateManager.isLoaded(paramPais, jdoInheritedFieldCount + 6))
      return paramPais.monedaSing;
    return localStateManager.getStringField(paramPais, jdoInheritedFieldCount + 6, paramPais.monedaSing);
  }

  private static final void jdoSetmonedaSing(Pais paramPais, String paramString)
  {
    if (paramPais.jdoFlags == 0)
    {
      paramPais.monedaSing = paramString;
      return;
    }
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
    {
      paramPais.monedaSing = paramString;
      return;
    }
    localStateManager.setStringField(paramPais, jdoInheritedFieldCount + 6, paramPais.monedaSing, paramString);
  }

  private static final String jdoGetnombre(Pais paramPais)
  {
    if (paramPais.jdoFlags <= 0)
      return paramPais.nombre;
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
      return paramPais.nombre;
    if (localStateManager.isLoaded(paramPais, jdoInheritedFieldCount + 7))
      return paramPais.nombre;
    return localStateManager.getStringField(paramPais, jdoInheritedFieldCount + 7, paramPais.nombre);
  }

  private static final void jdoSetnombre(Pais paramPais, String paramString)
  {
    if (paramPais.jdoFlags == 0)
    {
      paramPais.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
    {
      paramPais.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramPais, jdoInheritedFieldCount + 7, paramPais.nombre, paramString);
  }

  private static final RegionContinente jdoGetregionContinente(Pais paramPais)
  {
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
      return paramPais.regionContinente;
    if (localStateManager.isLoaded(paramPais, jdoInheritedFieldCount + 8))
      return paramPais.regionContinente;
    return (RegionContinente)localStateManager.getObjectField(paramPais, jdoInheritedFieldCount + 8, paramPais.regionContinente);
  }

  private static final void jdoSetregionContinente(Pais paramPais, RegionContinente paramRegionContinente)
  {
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
    {
      paramPais.regionContinente = paramRegionContinente;
      return;
    }
    localStateManager.setObjectField(paramPais, jdoInheritedFieldCount + 8, paramPais.regionContinente, paramRegionContinente);
  }

  private static final String jdoGetsimbolo(Pais paramPais)
  {
    if (paramPais.jdoFlags <= 0)
      return paramPais.simbolo;
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
      return paramPais.simbolo;
    if (localStateManager.isLoaded(paramPais, jdoInheritedFieldCount + 9))
      return paramPais.simbolo;
    return localStateManager.getStringField(paramPais, jdoInheritedFieldCount + 9, paramPais.simbolo);
  }

  private static final void jdoSetsimbolo(Pais paramPais, String paramString)
  {
    if (paramPais.jdoFlags == 0)
    {
      paramPais.simbolo = paramString;
      return;
    }
    StateManager localStateManager = paramPais.jdoStateManager;
    if (localStateManager == null)
    {
      paramPais.simbolo = paramString;
      return;
    }
    localStateManager.setStringField(paramPais, jdoInheritedFieldCount + 9, paramPais.simbolo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}