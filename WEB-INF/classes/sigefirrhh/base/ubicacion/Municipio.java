package sigefirrhh.base.ubicacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Municipio
  implements Serializable, PersistenceCapable
{
  private long idMunicipio;
  private String codMunicipio;
  private String nombre;
  private String abreviatura;
  private Estado estado;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "abreviatura", "codMunicipio", "estado", "idMunicipio", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Estado"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 26, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " " + 
      jdoGetcodMunicipio(this) + " " + 
      jdoGetestado(this).getNombre();
  }

  public String getAbreviatura()
  {
    return jdoGetabreviatura(this);
  }

  public String getCodMunicipio()
  {
    return jdoGetcodMunicipio(this);
  }

  public Estado getEstado()
  {
    return jdoGetestado(this);
  }

  public long getIdMunicipio()
  {
    return jdoGetidMunicipio(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setAbreviatura(String string)
  {
    jdoSetabreviatura(this, string);
  }

  public void setCodMunicipio(String string)
  {
    jdoSetcodMunicipio(this, string);
  }

  public void setEstado(Estado estado)
  {
    jdoSetestado(this, estado);
  }

  public void setIdMunicipio(long l)
  {
    jdoSetidMunicipio(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.ubicacion.Municipio"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Municipio());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Municipio localMunicipio = new Municipio();
    localMunicipio.jdoFlags = 1;
    localMunicipio.jdoStateManager = paramStateManager;
    return localMunicipio;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Municipio localMunicipio = new Municipio();
    localMunicipio.jdoCopyKeyFieldsFromObjectId(paramObject);
    localMunicipio.jdoFlags = 1;
    localMunicipio.jdoStateManager = paramStateManager;
    return localMunicipio;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.abreviatura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codMunicipio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.estado);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idMunicipio);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.abreviatura = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codMunicipio = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estado = ((Estado)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idMunicipio = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Municipio paramMunicipio, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramMunicipio == null)
        throw new IllegalArgumentException("arg1");
      this.abreviatura = paramMunicipio.abreviatura;
      return;
    case 1:
      if (paramMunicipio == null)
        throw new IllegalArgumentException("arg1");
      this.codMunicipio = paramMunicipio.codMunicipio;
      return;
    case 2:
      if (paramMunicipio == null)
        throw new IllegalArgumentException("arg1");
      this.estado = paramMunicipio.estado;
      return;
    case 3:
      if (paramMunicipio == null)
        throw new IllegalArgumentException("arg1");
      this.idMunicipio = paramMunicipio.idMunicipio;
      return;
    case 4:
      if (paramMunicipio == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramMunicipio.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Municipio))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Municipio localMunicipio = (Municipio)paramObject;
    if (localMunicipio.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localMunicipio, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new MunicipioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new MunicipioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MunicipioPK))
      throw new IllegalArgumentException("arg1");
    MunicipioPK localMunicipioPK = (MunicipioPK)paramObject;
    localMunicipioPK.idMunicipio = this.idMunicipio;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MunicipioPK))
      throw new IllegalArgumentException("arg1");
    MunicipioPK localMunicipioPK = (MunicipioPK)paramObject;
    this.idMunicipio = localMunicipioPK.idMunicipio;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MunicipioPK))
      throw new IllegalArgumentException("arg2");
    MunicipioPK localMunicipioPK = (MunicipioPK)paramObject;
    localMunicipioPK.idMunicipio = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MunicipioPK))
      throw new IllegalArgumentException("arg2");
    MunicipioPK localMunicipioPK = (MunicipioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localMunicipioPK.idMunicipio);
  }

  private static final String jdoGetabreviatura(Municipio paramMunicipio)
  {
    if (paramMunicipio.jdoFlags <= 0)
      return paramMunicipio.abreviatura;
    StateManager localStateManager = paramMunicipio.jdoStateManager;
    if (localStateManager == null)
      return paramMunicipio.abreviatura;
    if (localStateManager.isLoaded(paramMunicipio, jdoInheritedFieldCount + 0))
      return paramMunicipio.abreviatura;
    return localStateManager.getStringField(paramMunicipio, jdoInheritedFieldCount + 0, paramMunicipio.abreviatura);
  }

  private static final void jdoSetabreviatura(Municipio paramMunicipio, String paramString)
  {
    if (paramMunicipio.jdoFlags == 0)
    {
      paramMunicipio.abreviatura = paramString;
      return;
    }
    StateManager localStateManager = paramMunicipio.jdoStateManager;
    if (localStateManager == null)
    {
      paramMunicipio.abreviatura = paramString;
      return;
    }
    localStateManager.setStringField(paramMunicipio, jdoInheritedFieldCount + 0, paramMunicipio.abreviatura, paramString);
  }

  private static final String jdoGetcodMunicipio(Municipio paramMunicipio)
  {
    if (paramMunicipio.jdoFlags <= 0)
      return paramMunicipio.codMunicipio;
    StateManager localStateManager = paramMunicipio.jdoStateManager;
    if (localStateManager == null)
      return paramMunicipio.codMunicipio;
    if (localStateManager.isLoaded(paramMunicipio, jdoInheritedFieldCount + 1))
      return paramMunicipio.codMunicipio;
    return localStateManager.getStringField(paramMunicipio, jdoInheritedFieldCount + 1, paramMunicipio.codMunicipio);
  }

  private static final void jdoSetcodMunicipio(Municipio paramMunicipio, String paramString)
  {
    if (paramMunicipio.jdoFlags == 0)
    {
      paramMunicipio.codMunicipio = paramString;
      return;
    }
    StateManager localStateManager = paramMunicipio.jdoStateManager;
    if (localStateManager == null)
    {
      paramMunicipio.codMunicipio = paramString;
      return;
    }
    localStateManager.setStringField(paramMunicipio, jdoInheritedFieldCount + 1, paramMunicipio.codMunicipio, paramString);
  }

  private static final Estado jdoGetestado(Municipio paramMunicipio)
  {
    StateManager localStateManager = paramMunicipio.jdoStateManager;
    if (localStateManager == null)
      return paramMunicipio.estado;
    if (localStateManager.isLoaded(paramMunicipio, jdoInheritedFieldCount + 2))
      return paramMunicipio.estado;
    return (Estado)localStateManager.getObjectField(paramMunicipio, jdoInheritedFieldCount + 2, paramMunicipio.estado);
  }

  private static final void jdoSetestado(Municipio paramMunicipio, Estado paramEstado)
  {
    StateManager localStateManager = paramMunicipio.jdoStateManager;
    if (localStateManager == null)
    {
      paramMunicipio.estado = paramEstado;
      return;
    }
    localStateManager.setObjectField(paramMunicipio, jdoInheritedFieldCount + 2, paramMunicipio.estado, paramEstado);
  }

  private static final long jdoGetidMunicipio(Municipio paramMunicipio)
  {
    return paramMunicipio.idMunicipio;
  }

  private static final void jdoSetidMunicipio(Municipio paramMunicipio, long paramLong)
  {
    StateManager localStateManager = paramMunicipio.jdoStateManager;
    if (localStateManager == null)
    {
      paramMunicipio.idMunicipio = paramLong;
      return;
    }
    localStateManager.setLongField(paramMunicipio, jdoInheritedFieldCount + 3, paramMunicipio.idMunicipio, paramLong);
  }

  private static final String jdoGetnombre(Municipio paramMunicipio)
  {
    if (paramMunicipio.jdoFlags <= 0)
      return paramMunicipio.nombre;
    StateManager localStateManager = paramMunicipio.jdoStateManager;
    if (localStateManager == null)
      return paramMunicipio.nombre;
    if (localStateManager.isLoaded(paramMunicipio, jdoInheritedFieldCount + 4))
      return paramMunicipio.nombre;
    return localStateManager.getStringField(paramMunicipio, jdoInheritedFieldCount + 4, paramMunicipio.nombre);
  }

  private static final void jdoSetnombre(Municipio paramMunicipio, String paramString)
  {
    if (paramMunicipio.jdoFlags == 0)
    {
      paramMunicipio.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramMunicipio.jdoStateManager;
    if (localStateManager == null)
    {
      paramMunicipio.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramMunicipio, jdoInheritedFieldCount + 4, paramMunicipio.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}