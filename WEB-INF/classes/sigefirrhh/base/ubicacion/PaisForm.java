package sigefirrhh.base.ubicacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class PaisForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PaisForm.class.getName());
  private Pais pais;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private boolean showPaisByCodPais;
  private boolean showPaisByNombre;
  private String findCodPais;
  private String findNombre;
  private Collection colRegionContinente;
  private String selectRegionContinente;
  private Object stateResultPaisByCodPais = null;

  private Object stateResultPaisByNombre = null;

  public String getFindCodPais()
  {
    return this.findCodPais;
  }
  public void setFindCodPais(String findCodPais) {
    this.findCodPais = findCodPais;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectRegionContinente()
  {
    return this.selectRegionContinente;
  }
  public void setSelectRegionContinente(String valRegionContinente) {
    Iterator iterator = this.colRegionContinente.iterator();
    RegionContinente regionContinente = null;
    this.pais.setRegionContinente(null);
    while (iterator.hasNext()) {
      regionContinente = (RegionContinente)iterator.next();
      if (String.valueOf(regionContinente.getIdRegionContinente()).equals(
        valRegionContinente)) {
        this.pais.setRegionContinente(
          regionContinente);
        break;
      }
    }
    this.selectRegionContinente = valRegionContinente;
  }
  public Collection getResult() {
    return this.result;
  }

  public Pais getPais() {
    if (this.pais == null) {
      this.pais = new Pais();
    }
    return this.pais;
  }

  public PaisForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRegionContinente()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegionContinente.iterator();
    RegionContinente regionContinente = null;
    while (iterator.hasNext()) {
      regionContinente = (RegionContinente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(regionContinente.getIdRegionContinente()), 
        regionContinente.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colRegionContinente = 
        this.ubicacionFacade.findAllRegionContinente();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPaisByCodPais()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findPaisByCodPais(this.findCodPais);
      this.showPaisByCodPais = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPaisByCodPais)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodPais = null;
    this.findNombre = null;

    return null;
  }

  public String findPaisByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findPaisByNombre(this.findNombre);
      this.showPaisByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPaisByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodPais = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowPaisByCodPais() {
    return this.showPaisByCodPais;
  }
  public boolean isShowPaisByNombre() {
    return this.showPaisByNombre;
  }

  public String selectPais()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRegionContinente = null;

    long idPais = 
      Long.parseLong((String)requestParameterMap.get("idPais"));
    try
    {
      this.pais = 
        this.ubicacionFacade.findPaisById(
        idPais);
      if (this.pais.getRegionContinente() != null) {
        this.selectRegionContinente = 
          String.valueOf(this.pais.getRegionContinente().getIdRegionContinente());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.pais = null;
    this.showPaisByCodPais = false;
    this.showPaisByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.ubicacionFacade.addPais(
          this.pais);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.ubicacionFacade.updatePais(
          this.pais);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.ubicacionFacade.deletePais(
        this.pais);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.pais = new Pais();

    this.selectRegionContinente = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.pais.setIdPais(identityGenerator.getNextSequenceNumber("sigefirrhh.base.ubicacion.Pais"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.pais = new Pais();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}