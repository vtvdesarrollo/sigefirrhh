package sigefirrhh.base.ubicacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CiudadBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCiudad(Ciudad ciudad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Ciudad ciudadNew = 
      (Ciudad)BeanUtils.cloneBean(
      ciudad);

    EstadoBeanBusiness estadoBeanBusiness = new EstadoBeanBusiness();

    if (ciudadNew.getEstado() != null) {
      ciudadNew.setEstado(
        estadoBeanBusiness.findEstadoById(
        ciudadNew.getEstado().getIdEstado()));
    }
    pm.makePersistent(ciudadNew);
  }

  public void updateCiudad(Ciudad ciudad) throws Exception
  {
    Ciudad ciudadModify = 
      findCiudadById(ciudad.getIdCiudad());

    EstadoBeanBusiness estadoBeanBusiness = new EstadoBeanBusiness();

    if (ciudad.getEstado() != null) {
      ciudad.setEstado(
        estadoBeanBusiness.findEstadoById(
        ciudad.getEstado().getIdEstado()));
    }

    BeanUtils.copyProperties(ciudadModify, ciudad);
  }

  public void deleteCiudad(Ciudad ciudad) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Ciudad ciudadDelete = 
      findCiudadById(ciudad.getIdCiudad());
    pm.deletePersistent(ciudadDelete);
  }

  public Ciudad findCiudadById(long idCiudad) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCiudad == pIdCiudad";
    Query query = pm.newQuery(Ciudad.class, filter);

    query.declareParameters("long pIdCiudad");

    parameters.put("pIdCiudad", new Long(idCiudad));

    Collection colCiudad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCiudad.iterator();
    return (Ciudad)iterator.next();
  }

  public Collection findCiudadAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent ciudadExtent = pm.getExtent(
      Ciudad.class, true);
    Query query = pm.newQuery(ciudadExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodCiudad(String codCiudad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCiudad == pCodCiudad";

    Query query = pm.newQuery(Ciudad.class, filter);

    query.declareParameters("java.lang.String pCodCiudad");
    HashMap parameters = new HashMap();

    parameters.put("pCodCiudad", new String(codCiudad));

    query.setOrdering("nombre ascending");

    Collection colCiudad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCiudad);

    return colCiudad;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Ciudad.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colCiudad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCiudad);

    return colCiudad;
  }

  public Collection findByEstado(long idEstado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "estado.idEstado == pIdEstado";

    Query query = pm.newQuery(Ciudad.class, filter);

    query.declareParameters("long pIdEstado");
    HashMap parameters = new HashMap();

    parameters.put("pIdEstado", new Long(idEstado));

    query.setOrdering("nombre ascending");

    Collection colCiudad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCiudad);

    return colCiudad;
  }
}