package sigefirrhh.base.ubicacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class RegionContinenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRegionContinente(RegionContinente regionContinente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RegionContinente regionContinenteNew = 
      (RegionContinente)BeanUtils.cloneBean(
      regionContinente);

    ContinenteBeanBusiness continenteBeanBusiness = new ContinenteBeanBusiness();

    if (regionContinenteNew.getContinente() != null) {
      regionContinenteNew.setContinente(
        continenteBeanBusiness.findContinenteById(
        regionContinenteNew.getContinente().getIdContinente()));
    }
    pm.makePersistent(regionContinenteNew);
  }

  public void updateRegionContinente(RegionContinente regionContinente) throws Exception
  {
    RegionContinente regionContinenteModify = 
      findRegionContinenteById(regionContinente.getIdRegionContinente());

    ContinenteBeanBusiness continenteBeanBusiness = new ContinenteBeanBusiness();

    if (regionContinente.getContinente() != null) {
      regionContinente.setContinente(
        continenteBeanBusiness.findContinenteById(
        regionContinente.getContinente().getIdContinente()));
    }

    BeanUtils.copyProperties(regionContinenteModify, regionContinente);
  }

  public void deleteRegionContinente(RegionContinente regionContinente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RegionContinente regionContinenteDelete = 
      findRegionContinenteById(regionContinente.getIdRegionContinente());
    pm.deletePersistent(regionContinenteDelete);
  }

  public RegionContinente findRegionContinenteById(long idRegionContinente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRegionContinente == pIdRegionContinente";
    Query query = pm.newQuery(RegionContinente.class, filter);

    query.declareParameters("long pIdRegionContinente");

    parameters.put("pIdRegionContinente", new Long(idRegionContinente));

    Collection colRegionContinente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRegionContinente.iterator();
    return (RegionContinente)iterator.next();
  }

  public Collection findRegionContinenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent regionContinenteExtent = pm.getExtent(
      RegionContinente.class, true);
    Query query = pm.newQuery(regionContinenteExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodRegionContinente(String codRegionContinente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codRegionContinente == pCodRegionContinente";

    Query query = pm.newQuery(RegionContinente.class, filter);

    query.declareParameters("java.lang.String pCodRegionContinente");
    HashMap parameters = new HashMap();

    parameters.put("pCodRegionContinente", new String(codRegionContinente));

    query.setOrdering("nombre ascending");

    Collection colRegionContinente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegionContinente);

    return colRegionContinente;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(RegionContinente.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colRegionContinente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegionContinente);

    return colRegionContinente;
  }

  public Collection findByContinente(long idContinente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "continente.idContinente == pIdContinente";

    Query query = pm.newQuery(RegionContinente.class, filter);

    query.declareParameters("long pIdContinente");
    HashMap parameters = new HashMap();

    parameters.put("pIdContinente", new Long(idContinente));

    query.setOrdering("nombre ascending");

    Collection colRegionContinente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegionContinente);

    return colRegionContinente;
  }
}