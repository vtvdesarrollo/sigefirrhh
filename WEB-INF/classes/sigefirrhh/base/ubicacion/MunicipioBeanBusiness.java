package sigefirrhh.base.ubicacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class MunicipioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addMunicipio(Municipio municipio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Municipio municipioNew = 
      (Municipio)BeanUtils.cloneBean(
      municipio);

    EstadoBeanBusiness estadoBeanBusiness = new EstadoBeanBusiness();

    if (municipioNew.getEstado() != null) {
      municipioNew.setEstado(
        estadoBeanBusiness.findEstadoById(
        municipioNew.getEstado().getIdEstado()));
    }
    pm.makePersistent(municipioNew);
  }

  public void updateMunicipio(Municipio municipio) throws Exception
  {
    Municipio municipioModify = 
      findMunicipioById(municipio.getIdMunicipio());

    EstadoBeanBusiness estadoBeanBusiness = new EstadoBeanBusiness();

    if (municipio.getEstado() != null) {
      municipio.setEstado(
        estadoBeanBusiness.findEstadoById(
        municipio.getEstado().getIdEstado()));
    }

    BeanUtils.copyProperties(municipioModify, municipio);
  }

  public void deleteMunicipio(Municipio municipio) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Municipio municipioDelete = 
      findMunicipioById(municipio.getIdMunicipio());
    pm.deletePersistent(municipioDelete);
  }

  public Municipio findMunicipioById(long idMunicipio) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idMunicipio == pIdMunicipio";
    Query query = pm.newQuery(Municipio.class, filter);

    query.declareParameters("long pIdMunicipio");

    parameters.put("pIdMunicipio", new Long(idMunicipio));

    Collection colMunicipio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colMunicipio.iterator();
    return (Municipio)iterator.next();
  }

  public Collection findMunicipioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent municipioExtent = pm.getExtent(
      Municipio.class, true);
    Query query = pm.newQuery(municipioExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodMunicipio(String codMunicipio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codMunicipio == pCodMunicipio";

    Query query = pm.newQuery(Municipio.class, filter);

    query.declareParameters("java.lang.String pCodMunicipio");
    HashMap parameters = new HashMap();

    parameters.put("pCodMunicipio", new String(codMunicipio));

    query.setOrdering("nombre ascending");

    Collection colMunicipio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMunicipio);

    return colMunicipio;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Municipio.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colMunicipio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMunicipio);

    return colMunicipio;
  }

  public Collection findByEstado(long idEstado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "estado.idEstado == pIdEstado";

    Query query = pm.newQuery(Municipio.class, filter);

    query.declareParameters("long pIdEstado");
    HashMap parameters = new HashMap();

    parameters.put("pIdEstado", new Long(idEstado));

    query.setOrdering("nombre ascending");

    Collection colMunicipio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMunicipio);

    return colMunicipio;
  }
}