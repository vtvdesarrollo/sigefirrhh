package sigefirrhh.base.ubicacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ParroquiaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParroquia(Parroquia parroquia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Parroquia parroquiaNew = 
      (Parroquia)BeanUtils.cloneBean(
      parroquia);

    MunicipioBeanBusiness municipioBeanBusiness = new MunicipioBeanBusiness();

    if (parroquiaNew.getMunicipio() != null) {
      parroquiaNew.setMunicipio(
        municipioBeanBusiness.findMunicipioById(
        parroquiaNew.getMunicipio().getIdMunicipio()));
    }
    pm.makePersistent(parroquiaNew);
  }

  public void updateParroquia(Parroquia parroquia) throws Exception
  {
    Parroquia parroquiaModify = 
      findParroquiaById(parroquia.getIdParroquia());

    MunicipioBeanBusiness municipioBeanBusiness = new MunicipioBeanBusiness();

    if (parroquia.getMunicipio() != null) {
      parroquia.setMunicipio(
        municipioBeanBusiness.findMunicipioById(
        parroquia.getMunicipio().getIdMunicipio()));
    }

    BeanUtils.copyProperties(parroquiaModify, parroquia);
  }

  public void deleteParroquia(Parroquia parroquia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Parroquia parroquiaDelete = 
      findParroquiaById(parroquia.getIdParroquia());
    pm.deletePersistent(parroquiaDelete);
  }

  public Parroquia findParroquiaById(long idParroquia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParroquia == pIdParroquia";
    Query query = pm.newQuery(Parroquia.class, filter);

    query.declareParameters("long pIdParroquia");

    parameters.put("pIdParroquia", new Long(idParroquia));

    Collection colParroquia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParroquia.iterator();
    return (Parroquia)iterator.next();
  }

  public Collection findParroquiaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent parroquiaExtent = pm.getExtent(
      Parroquia.class, true);
    Query query = pm.newQuery(parroquiaExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodParroquia(String codParroquia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codParroquia == pCodParroquia";

    Query query = pm.newQuery(Parroquia.class, filter);

    query.declareParameters("java.lang.String pCodParroquia");
    HashMap parameters = new HashMap();

    parameters.put("pCodParroquia", new String(codParroquia));

    query.setOrdering("nombre ascending");

    Collection colParroquia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParroquia);

    return colParroquia;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Parroquia.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colParroquia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParroquia);

    return colParroquia;
  }

  public Collection findByMunicipio(long idMunicipio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "municipio.idMunicipio == pIdMunicipio";

    Query query = pm.newQuery(Parroquia.class, filter);

    query.declareParameters("long pIdMunicipio");
    HashMap parameters = new HashMap();

    parameters.put("pIdMunicipio", new Long(idMunicipio));

    query.setOrdering("nombre ascending");

    Collection colParroquia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParroquia);

    return colParroquia;
  }
}