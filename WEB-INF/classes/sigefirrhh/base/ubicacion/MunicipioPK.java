package sigefirrhh.base.ubicacion;

import java.io.Serializable;

public class MunicipioPK
  implements Serializable
{
  public long idMunicipio;

  public MunicipioPK()
  {
  }

  public MunicipioPK(long idMunicipio)
  {
    this.idMunicipio = idMunicipio;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((MunicipioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(MunicipioPK thatPK)
  {
    return 
      this.idMunicipio == thatPK.idMunicipio;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idMunicipio)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idMunicipio);
  }
}