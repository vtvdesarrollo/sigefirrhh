package sigefirrhh.base.ubicacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ContinenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addContinente(Continente continente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Continente continenteNew = 
      (Continente)BeanUtils.cloneBean(
      continente);

    pm.makePersistent(continenteNew);
  }

  public void updateContinente(Continente continente) throws Exception
  {
    Continente continenteModify = 
      findContinenteById(continente.getIdContinente());

    BeanUtils.copyProperties(continenteModify, continente);
  }

  public void deleteContinente(Continente continente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Continente continenteDelete = 
      findContinenteById(continente.getIdContinente());
    pm.deletePersistent(continenteDelete);
  }

  public Continente findContinenteById(long idContinente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idContinente == pIdContinente";
    Query query = pm.newQuery(Continente.class, filter);

    query.declareParameters("long pIdContinente");

    parameters.put("pIdContinente", new Long(idContinente));

    Collection colContinente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colContinente.iterator();
    return (Continente)iterator.next();
  }

  public Collection findContinenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent continenteExtent = pm.getExtent(
      Continente.class, true);
    Query query = pm.newQuery(continenteExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodContinente(String codContinente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codContinente == pCodContinente";

    Query query = pm.newQuery(Continente.class, filter);

    query.declareParameters("java.lang.String pCodContinente");
    HashMap parameters = new HashMap();

    parameters.put("pCodContinente", new String(codContinente));

    query.setOrdering("nombre ascending");

    Collection colContinente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colContinente);

    return colContinente;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Continente.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colContinente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colContinente);

    return colContinente;
  }
}