package sigefirrhh.base.ubicacion;

import java.io.Serializable;

public class CiudadPK
  implements Serializable
{
  public long idCiudad;

  public CiudadPK()
  {
  }

  public CiudadPK(long idCiudad)
  {
    this.idCiudad = idCiudad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CiudadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CiudadPK thatPK)
  {
    return 
      this.idCiudad == thatPK.idCiudad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCiudad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCiudad);
  }
}