package sigefirrhh.base.ubicacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class MunicipioForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(MunicipioForm.class.getName());
  private Municipio municipio;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private boolean showMunicipioByCodMunicipio;
  private boolean showMunicipioByNombre;
  private boolean showMunicipioByEstado;
  private String findCodMunicipio;
  private String findNombre;
  private String findSelectPaisForEstado;
  private String findSelectEstado;
  private Collection findColPaisForEstado;
  private Collection findColEstado;
  private Collection colPaisForEstado;
  private Collection colEstado;
  private String selectPaisForEstado;
  private String selectEstado;
  private Object stateResultMunicipioByCodMunicipio = null;

  private Object stateResultMunicipioByNombre = null;

  private Object stateResultMunicipioByEstado = null;

  public String getFindCodMunicipio()
  {
    return this.findCodMunicipio;
  }
  public void setFindCodMunicipio(String findCodMunicipio) {
    this.findCodMunicipio = findCodMunicipio;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }
  public Collection getFindColPaisForEstado() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPaisForEstado.iterator();
    Pais paisForEstado = null;
    while (iterator.hasNext()) {
      paisForEstado = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForEstado.getIdPais()), 
        paisForEstado.toString()));
    }
    return col;
  }
  public String getFindSelectPaisForEstado() {
    return this.findSelectPaisForEstado;
  }
  public void setFindSelectPaisForEstado(String valPaisForEstado) {
    this.findSelectPaisForEstado = valPaisForEstado;
  }
  public void findChangePaisForEstado(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColEstado = null;
      if (idPais > 0L)
        this.findColEstado = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowPaisForEstado() { return this.findColPaisForEstado != null; }

  public String getFindSelectEstado() {
    return this.findSelectEstado;
  }
  public void setFindSelectEstado(String valEstado) {
    this.findSelectEstado = valEstado;
  }

  public Collection getFindColEstado() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColEstado.iterator();
    Estado estado = null;
    while (iterator.hasNext()) {
      estado = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estado.getIdEstado()), 
        estado.toString()));
    }
    return col;
  }
  public boolean isFindShowEstado() {
    return this.findColEstado != null;
  }

  public String getSelectPaisForEstado()
  {
    return this.selectPaisForEstado;
  }
  public void setSelectPaisForEstado(String valPaisForEstado) {
    this.selectPaisForEstado = valPaisForEstado;
  }
  public void changePaisForEstado(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colEstado = null;
      if (idPais > 0L) {
        this.colEstado = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
      } else {
        this.selectEstado = null;
        this.municipio.setEstado(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectEstado = null;
      this.municipio.setEstado(
        null);
    }
  }

  public boolean isShowPaisForEstado() { return this.colPaisForEstado != null; }

  public String getSelectEstado() {
    return this.selectEstado;
  }
  public void setSelectEstado(String valEstado) {
    Iterator iterator = this.colEstado.iterator();
    Estado estado = null;
    this.municipio.setEstado(null);
    while (iterator.hasNext()) {
      estado = (Estado)iterator.next();
      if (String.valueOf(estado.getIdEstado()).equals(
        valEstado)) {
        this.municipio.setEstado(
          estado);
        break;
      }
    }
    this.selectEstado = valEstado;
  }
  public boolean isShowEstado() {
    return this.colEstado != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public Municipio getMunicipio() {
    if (this.municipio == null) {
      this.municipio = new Municipio();
    }
    return this.municipio;
  }

  public MunicipioForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPaisForEstado()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForEstado.iterator();
    Pais paisForEstado = null;
    while (iterator.hasNext()) {
      paisForEstado = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForEstado.getIdPais()), 
        paisForEstado.toString()));
    }
    return col;
  }

  public Collection getColEstado()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstado.iterator();
    Estado estado = null;
    while (iterator.hasNext()) {
      estado = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estado.getIdEstado()), 
        estado.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColPaisForEstado = 
        this.ubicacionFacade.findAllPais();

      this.colPaisForEstado = 
        this.ubicacionFacade.findAllPais();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findMunicipioByCodMunicipio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findMunicipioByCodMunicipio(this.findCodMunicipio);
      this.showMunicipioByCodMunicipio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showMunicipioByCodMunicipio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodMunicipio = null;
    this.findNombre = null;
    this.findSelectPaisForEstado = null;
    this.findSelectEstado = null;

    return null;
  }

  public String findMunicipioByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findMunicipioByNombre(this.findNombre);
      this.showMunicipioByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showMunicipioByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodMunicipio = null;
    this.findNombre = null;
    this.findSelectPaisForEstado = null;
    this.findSelectEstado = null;

    return null;
  }

  public String findMunicipioByEstado()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findMunicipioByEstado(Long.valueOf(this.findSelectEstado).longValue());
      this.showMunicipioByEstado = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showMunicipioByEstado)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodMunicipio = null;
    this.findNombre = null;
    this.findSelectPaisForEstado = null;
    this.findSelectEstado = null;

    return null;
  }

  public boolean isShowMunicipioByCodMunicipio() {
    return this.showMunicipioByCodMunicipio;
  }
  public boolean isShowMunicipioByNombre() {
    return this.showMunicipioByNombre;
  }
  public boolean isShowMunicipioByEstado() {
    return this.showMunicipioByEstado;
  }

  public String selectMunicipio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectEstado = null;
    this.selectPaisForEstado = null;

    long idMunicipio = 
      Long.parseLong((String)requestParameterMap.get("idMunicipio"));
    try
    {
      this.municipio = 
        this.ubicacionFacade.findMunicipioById(
        idMunicipio);
      if (this.municipio.getEstado() != null) {
        this.selectEstado = 
          String.valueOf(this.municipio.getEstado().getIdEstado());
      }

      Estado estado = null;
      Pais paisForEstado = null;

      if (this.municipio.getEstado() != null) {
        long idEstado = 
          this.municipio.getEstado().getIdEstado();
        this.selectEstado = String.valueOf(idEstado);
        estado = this.ubicacionFacade.findEstadoById(
          idEstado);
        this.colEstado = this.ubicacionFacade.findEstadoByPais(
          estado.getPais().getIdPais());

        long idPaisForEstado = 
          this.municipio.getEstado().getPais().getIdPais();
        this.selectPaisForEstado = String.valueOf(idPaisForEstado);
        paisForEstado = 
          this.ubicacionFacade.findPaisById(
          idPaisForEstado);
        this.colPaisForEstado = 
          this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.municipio = null;
    this.showMunicipioByCodMunicipio = false;
    this.showMunicipioByNombre = false;
    this.showMunicipioByEstado = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.ubicacionFacade.addMunicipio(
          this.municipio);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.ubicacionFacade.updateMunicipio(
          this.municipio);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.ubicacionFacade.deleteMunicipio(
        this.municipio);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.municipio = new Municipio();

    this.selectEstado = null;

    this.selectPaisForEstado = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.municipio.setIdMunicipio(identityGenerator.getNextSequenceNumber("sigefirrhh.base.ubicacion.Municipio"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.municipio = new Municipio();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}