package sigefirrhh.base.ubicacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PaisBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPais(Pais pais)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Pais paisNew = 
      (Pais)BeanUtils.cloneBean(
      pais);

    RegionContinenteBeanBusiness regionContinenteBeanBusiness = new RegionContinenteBeanBusiness();

    if (paisNew.getRegionContinente() != null) {
      paisNew.setRegionContinente(
        regionContinenteBeanBusiness.findRegionContinenteById(
        paisNew.getRegionContinente().getIdRegionContinente()));
    }
    pm.makePersistent(paisNew);
  }

  public void updatePais(Pais pais) throws Exception
  {
    Pais paisModify = 
      findPaisById(pais.getIdPais());

    RegionContinenteBeanBusiness regionContinenteBeanBusiness = new RegionContinenteBeanBusiness();

    if (pais.getRegionContinente() != null) {
      pais.setRegionContinente(
        regionContinenteBeanBusiness.findRegionContinenteById(
        pais.getRegionContinente().getIdRegionContinente()));
    }

    BeanUtils.copyProperties(paisModify, pais);
  }

  public void deletePais(Pais pais) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Pais paisDelete = 
      findPaisById(pais.getIdPais());
    pm.deletePersistent(paisDelete);
  }

  public Pais findPaisById(long idPais) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPais == pIdPais";
    Query query = pm.newQuery(Pais.class, filter);

    query.declareParameters("long pIdPais");

    parameters.put("pIdPais", new Long(idPais));

    Collection colPais = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPais.iterator();
    return (Pais)iterator.next();
  }

  public Collection findPaisAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent paisExtent = pm.getExtent(
      Pais.class, true);
    Query query = pm.newQuery(paisExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodPais(String codPais)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codPais == pCodPais";

    Query query = pm.newQuery(Pais.class, filter);

    query.declareParameters("java.lang.String pCodPais");
    HashMap parameters = new HashMap();

    parameters.put("pCodPais", new String(codPais));

    query.setOrdering("nombre ascending");

    Collection colPais = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPais);

    return colPais;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Pais.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colPais = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPais);

    return colPais;
  }
}