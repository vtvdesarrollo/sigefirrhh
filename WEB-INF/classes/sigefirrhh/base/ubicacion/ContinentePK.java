package sigefirrhh.base.ubicacion;

import java.io.Serializable;

public class ContinentePK
  implements Serializable
{
  public long idContinente;

  public ContinentePK()
  {
  }

  public ContinentePK(long idContinente)
  {
    this.idContinente = idContinente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ContinentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ContinentePK thatPK)
  {
    return 
      this.idContinente == thatPK.idContinente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idContinente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idContinente);
  }
}