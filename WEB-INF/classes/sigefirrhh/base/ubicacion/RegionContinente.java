package sigefirrhh.base.ubicacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class RegionContinente
  implements Serializable, PersistenceCapable
{
  private long idRegionContinente;
  private String codRegionContinente;
  private String nombre;
  private String abreviatura;
  private Continente continente;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "abreviatura", "codRegionContinente", "continente", "idRegionContinente", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Continente"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 26, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " " + 
      jdoGetcodRegionContinente(this);
  }

  public String getAbreviatura()
  {
    return jdoGetabreviatura(this);
  }

  public String getCodRegionContinente()
  {
    return jdoGetcodRegionContinente(this);
  }

  public Continente getContinente()
  {
    return jdoGetcontinente(this);
  }

  public long getIdRegionContinente()
  {
    return jdoGetidRegionContinente(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setAbreviatura(String string)
  {
    jdoSetabreviatura(this, string);
  }

  public void setCodRegionContinente(String string)
  {
    jdoSetcodRegionContinente(this, string);
  }

  public void setContinente(Continente continente)
  {
    jdoSetcontinente(this, continente);
  }

  public void setIdRegionContinente(long l)
  {
    jdoSetidRegionContinente(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.ubicacion.RegionContinente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RegionContinente());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RegionContinente localRegionContinente = new RegionContinente();
    localRegionContinente.jdoFlags = 1;
    localRegionContinente.jdoStateManager = paramStateManager;
    return localRegionContinente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RegionContinente localRegionContinente = new RegionContinente();
    localRegionContinente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRegionContinente.jdoFlags = 1;
    localRegionContinente.jdoStateManager = paramStateManager;
    return localRegionContinente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.abreviatura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codRegionContinente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.continente);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRegionContinente);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.abreviatura = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRegionContinente = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.continente = ((Continente)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRegionContinente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RegionContinente paramRegionContinente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRegionContinente == null)
        throw new IllegalArgumentException("arg1");
      this.abreviatura = paramRegionContinente.abreviatura;
      return;
    case 1:
      if (paramRegionContinente == null)
        throw new IllegalArgumentException("arg1");
      this.codRegionContinente = paramRegionContinente.codRegionContinente;
      return;
    case 2:
      if (paramRegionContinente == null)
        throw new IllegalArgumentException("arg1");
      this.continente = paramRegionContinente.continente;
      return;
    case 3:
      if (paramRegionContinente == null)
        throw new IllegalArgumentException("arg1");
      this.idRegionContinente = paramRegionContinente.idRegionContinente;
      return;
    case 4:
      if (paramRegionContinente == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramRegionContinente.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RegionContinente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RegionContinente localRegionContinente = (RegionContinente)paramObject;
    if (localRegionContinente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRegionContinente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RegionContinentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RegionContinentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegionContinentePK))
      throw new IllegalArgumentException("arg1");
    RegionContinentePK localRegionContinentePK = (RegionContinentePK)paramObject;
    localRegionContinentePK.idRegionContinente = this.idRegionContinente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegionContinentePK))
      throw new IllegalArgumentException("arg1");
    RegionContinentePK localRegionContinentePK = (RegionContinentePK)paramObject;
    this.idRegionContinente = localRegionContinentePK.idRegionContinente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegionContinentePK))
      throw new IllegalArgumentException("arg2");
    RegionContinentePK localRegionContinentePK = (RegionContinentePK)paramObject;
    localRegionContinentePK.idRegionContinente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegionContinentePK))
      throw new IllegalArgumentException("arg2");
    RegionContinentePK localRegionContinentePK = (RegionContinentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localRegionContinentePK.idRegionContinente);
  }

  private static final String jdoGetabreviatura(RegionContinente paramRegionContinente)
  {
    if (paramRegionContinente.jdoFlags <= 0)
      return paramRegionContinente.abreviatura;
    StateManager localStateManager = paramRegionContinente.jdoStateManager;
    if (localStateManager == null)
      return paramRegionContinente.abreviatura;
    if (localStateManager.isLoaded(paramRegionContinente, jdoInheritedFieldCount + 0))
      return paramRegionContinente.abreviatura;
    return localStateManager.getStringField(paramRegionContinente, jdoInheritedFieldCount + 0, paramRegionContinente.abreviatura);
  }

  private static final void jdoSetabreviatura(RegionContinente paramRegionContinente, String paramString)
  {
    if (paramRegionContinente.jdoFlags == 0)
    {
      paramRegionContinente.abreviatura = paramString;
      return;
    }
    StateManager localStateManager = paramRegionContinente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegionContinente.abreviatura = paramString;
      return;
    }
    localStateManager.setStringField(paramRegionContinente, jdoInheritedFieldCount + 0, paramRegionContinente.abreviatura, paramString);
  }

  private static final String jdoGetcodRegionContinente(RegionContinente paramRegionContinente)
  {
    if (paramRegionContinente.jdoFlags <= 0)
      return paramRegionContinente.codRegionContinente;
    StateManager localStateManager = paramRegionContinente.jdoStateManager;
    if (localStateManager == null)
      return paramRegionContinente.codRegionContinente;
    if (localStateManager.isLoaded(paramRegionContinente, jdoInheritedFieldCount + 1))
      return paramRegionContinente.codRegionContinente;
    return localStateManager.getStringField(paramRegionContinente, jdoInheritedFieldCount + 1, paramRegionContinente.codRegionContinente);
  }

  private static final void jdoSetcodRegionContinente(RegionContinente paramRegionContinente, String paramString)
  {
    if (paramRegionContinente.jdoFlags == 0)
    {
      paramRegionContinente.codRegionContinente = paramString;
      return;
    }
    StateManager localStateManager = paramRegionContinente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegionContinente.codRegionContinente = paramString;
      return;
    }
    localStateManager.setStringField(paramRegionContinente, jdoInheritedFieldCount + 1, paramRegionContinente.codRegionContinente, paramString);
  }

  private static final Continente jdoGetcontinente(RegionContinente paramRegionContinente)
  {
    StateManager localStateManager = paramRegionContinente.jdoStateManager;
    if (localStateManager == null)
      return paramRegionContinente.continente;
    if (localStateManager.isLoaded(paramRegionContinente, jdoInheritedFieldCount + 2))
      return paramRegionContinente.continente;
    return (Continente)localStateManager.getObjectField(paramRegionContinente, jdoInheritedFieldCount + 2, paramRegionContinente.continente);
  }

  private static final void jdoSetcontinente(RegionContinente paramRegionContinente, Continente paramContinente)
  {
    StateManager localStateManager = paramRegionContinente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegionContinente.continente = paramContinente;
      return;
    }
    localStateManager.setObjectField(paramRegionContinente, jdoInheritedFieldCount + 2, paramRegionContinente.continente, paramContinente);
  }

  private static final long jdoGetidRegionContinente(RegionContinente paramRegionContinente)
  {
    return paramRegionContinente.idRegionContinente;
  }

  private static final void jdoSetidRegionContinente(RegionContinente paramRegionContinente, long paramLong)
  {
    StateManager localStateManager = paramRegionContinente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegionContinente.idRegionContinente = paramLong;
      return;
    }
    localStateManager.setLongField(paramRegionContinente, jdoInheritedFieldCount + 3, paramRegionContinente.idRegionContinente, paramLong);
  }

  private static final String jdoGetnombre(RegionContinente paramRegionContinente)
  {
    if (paramRegionContinente.jdoFlags <= 0)
      return paramRegionContinente.nombre;
    StateManager localStateManager = paramRegionContinente.jdoStateManager;
    if (localStateManager == null)
      return paramRegionContinente.nombre;
    if (localStateManager.isLoaded(paramRegionContinente, jdoInheritedFieldCount + 4))
      return paramRegionContinente.nombre;
    return localStateManager.getStringField(paramRegionContinente, jdoInheritedFieldCount + 4, paramRegionContinente.nombre);
  }

  private static final void jdoSetnombre(RegionContinente paramRegionContinente, String paramString)
  {
    if (paramRegionContinente.jdoFlags == 0)
    {
      paramRegionContinente.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramRegionContinente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegionContinente.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramRegionContinente, jdoInheritedFieldCount + 4, paramRegionContinente.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}