package sigefirrhh.base.ubicacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class EstadoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEstado(Estado estado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Estado estadoNew = 
      (Estado)BeanUtils.cloneBean(
      estado);

    PaisBeanBusiness paisBeanBusiness = new PaisBeanBusiness();

    if (estadoNew.getPais() != null) {
      estadoNew.setPais(
        paisBeanBusiness.findPaisById(
        estadoNew.getPais().getIdPais()));
    }
    pm.makePersistent(estadoNew);
  }

  public void updateEstado(Estado estado) throws Exception
  {
    Estado estadoModify = 
      findEstadoById(estado.getIdEstado());

    PaisBeanBusiness paisBeanBusiness = new PaisBeanBusiness();

    if (estado.getPais() != null) {
      estado.setPais(
        paisBeanBusiness.findPaisById(
        estado.getPais().getIdPais()));
    }

    BeanUtils.copyProperties(estadoModify, estado);
  }

  public void deleteEstado(Estado estado) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Estado estadoDelete = 
      findEstadoById(estado.getIdEstado());
    pm.deletePersistent(estadoDelete);
  }

  public Estado findEstadoById(long idEstado) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEstado == pIdEstado";
    Query query = pm.newQuery(Estado.class, filter);

    query.declareParameters("long pIdEstado");

    parameters.put("pIdEstado", new Long(idEstado));

    Collection colEstado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEstado.iterator();
    return (Estado)iterator.next();
  }

  public Collection findEstadoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent estadoExtent = pm.getExtent(
      Estado.class, true);
    Query query = pm.newQuery(estadoExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodEstado(String codEstado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codEstado == pCodEstado";

    Query query = pm.newQuery(Estado.class, filter);

    query.declareParameters("java.lang.String pCodEstado");
    HashMap parameters = new HashMap();

    parameters.put("pCodEstado", new String(codEstado));

    query.setOrdering("nombre ascending");

    Collection colEstado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEstado);

    return colEstado;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Estado.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colEstado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEstado);

    return colEstado;
  }

  public Collection findByPais(long idPais)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "pais.idPais == pIdPais";

    Query query = pm.newQuery(Estado.class, filter);

    query.declareParameters("long pIdPais");
    HashMap parameters = new HashMap();

    parameters.put("pIdPais", new Long(idPais));

    query.setOrdering("nombre ascending");

    Collection colEstado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEstado);

    return colEstado;
  }
}