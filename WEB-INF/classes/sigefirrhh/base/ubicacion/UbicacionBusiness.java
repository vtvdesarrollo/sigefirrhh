package sigefirrhh.base.ubicacion;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class UbicacionBusiness extends AbstractBusiness
  implements Serializable
{
  private CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

  private ContinenteBeanBusiness continenteBeanBusiness = new ContinenteBeanBusiness();

  private EstadoBeanBusiness estadoBeanBusiness = new EstadoBeanBusiness();

  private MunicipioBeanBusiness municipioBeanBusiness = new MunicipioBeanBusiness();

  private PaisBeanBusiness paisBeanBusiness = new PaisBeanBusiness();

  private ParroquiaBeanBusiness parroquiaBeanBusiness = new ParroquiaBeanBusiness();

  private RegionContinenteBeanBusiness regionContinenteBeanBusiness = new RegionContinenteBeanBusiness();

  public void addCiudad(Ciudad ciudad)
    throws Exception
  {
    this.ciudadBeanBusiness.addCiudad(ciudad);
  }

  public void updateCiudad(Ciudad ciudad) throws Exception {
    this.ciudadBeanBusiness.updateCiudad(ciudad);
  }

  public void deleteCiudad(Ciudad ciudad) throws Exception {
    this.ciudadBeanBusiness.deleteCiudad(ciudad);
  }

  public Ciudad findCiudadById(long ciudadId) throws Exception {
    return this.ciudadBeanBusiness.findCiudadById(ciudadId);
  }

  public Collection findAllCiudad() throws Exception {
    return this.ciudadBeanBusiness.findCiudadAll();
  }

  public Collection findCiudadByCodCiudad(String codCiudad)
    throws Exception
  {
    return this.ciudadBeanBusiness.findByCodCiudad(codCiudad);
  }

  public Collection findCiudadByNombre(String nombre)
    throws Exception
  {
    return this.ciudadBeanBusiness.findByNombre(nombre);
  }

  public Collection findCiudadByEstado(long idEstado)
    throws Exception
  {
    return this.ciudadBeanBusiness.findByEstado(idEstado);
  }

  public void addContinente(Continente continente)
    throws Exception
  {
    this.continenteBeanBusiness.addContinente(continente);
  }

  public void updateContinente(Continente continente) throws Exception {
    this.continenteBeanBusiness.updateContinente(continente);
  }

  public void deleteContinente(Continente continente) throws Exception {
    this.continenteBeanBusiness.deleteContinente(continente);
  }

  public Continente findContinenteById(long continenteId) throws Exception {
    return this.continenteBeanBusiness.findContinenteById(continenteId);
  }

  public Collection findAllContinente() throws Exception {
    return this.continenteBeanBusiness.findContinenteAll();
  }

  public Collection findContinenteByCodContinente(String codContinente)
    throws Exception
  {
    return this.continenteBeanBusiness.findByCodContinente(codContinente);
  }

  public Collection findContinenteByNombre(String nombre)
    throws Exception
  {
    return this.continenteBeanBusiness.findByNombre(nombre);
  }

  public void addEstado(Estado estado)
    throws Exception
  {
    this.estadoBeanBusiness.addEstado(estado);
  }

  public void updateEstado(Estado estado) throws Exception {
    this.estadoBeanBusiness.updateEstado(estado);
  }

  public void deleteEstado(Estado estado) throws Exception {
    this.estadoBeanBusiness.deleteEstado(estado);
  }

  public Estado findEstadoById(long estadoId) throws Exception {
    return this.estadoBeanBusiness.findEstadoById(estadoId);
  }

  public Collection findAllEstado() throws Exception {
    return this.estadoBeanBusiness.findEstadoAll();
  }

  public Collection findEstadoByCodEstado(String codEstado)
    throws Exception
  {
    return this.estadoBeanBusiness.findByCodEstado(codEstado);
  }

  public Collection findEstadoByNombre(String nombre)
    throws Exception
  {
    return this.estadoBeanBusiness.findByNombre(nombre);
  }

  public Collection findEstadoByPais(long idPais)
    throws Exception
  {
    return this.estadoBeanBusiness.findByPais(idPais);
  }

  public void addMunicipio(Municipio municipio)
    throws Exception
  {
    this.municipioBeanBusiness.addMunicipio(municipio);
  }

  public void updateMunicipio(Municipio municipio) throws Exception {
    this.municipioBeanBusiness.updateMunicipio(municipio);
  }

  public void deleteMunicipio(Municipio municipio) throws Exception {
    this.municipioBeanBusiness.deleteMunicipio(municipio);
  }

  public Municipio findMunicipioById(long municipioId) throws Exception {
    return this.municipioBeanBusiness.findMunicipioById(municipioId);
  }

  public Collection findAllMunicipio() throws Exception {
    return this.municipioBeanBusiness.findMunicipioAll();
  }

  public Collection findMunicipioByCodMunicipio(String codMunicipio)
    throws Exception
  {
    return this.municipioBeanBusiness.findByCodMunicipio(codMunicipio);
  }

  public Collection findMunicipioByNombre(String nombre)
    throws Exception
  {
    return this.municipioBeanBusiness.findByNombre(nombre);
  }

  public Collection findMunicipioByEstado(long idEstado)
    throws Exception
  {
    return this.municipioBeanBusiness.findByEstado(idEstado);
  }

  public void addPais(Pais pais)
    throws Exception
  {
    this.paisBeanBusiness.addPais(pais);
  }

  public void updatePais(Pais pais) throws Exception {
    this.paisBeanBusiness.updatePais(pais);
  }

  public void deletePais(Pais pais) throws Exception {
    this.paisBeanBusiness.deletePais(pais);
  }

  public Pais findPaisById(long paisId) throws Exception {
    return this.paisBeanBusiness.findPaisById(paisId);
  }

  public Collection findAllPais() throws Exception {
    return this.paisBeanBusiness.findPaisAll();
  }

  public Collection findPaisByCodPais(String codPais)
    throws Exception
  {
    return this.paisBeanBusiness.findByCodPais(codPais);
  }

  public Collection findPaisByNombre(String nombre)
    throws Exception
  {
    return this.paisBeanBusiness.findByNombre(nombre);
  }

  public void addParroquia(Parroquia parroquia)
    throws Exception
  {
    this.parroquiaBeanBusiness.addParroquia(parroquia);
  }

  public void updateParroquia(Parroquia parroquia) throws Exception {
    this.parroquiaBeanBusiness.updateParroquia(parroquia);
  }

  public void deleteParroquia(Parroquia parroquia) throws Exception {
    this.parroquiaBeanBusiness.deleteParroquia(parroquia);
  }

  public Parroquia findParroquiaById(long parroquiaId) throws Exception {
    return this.parroquiaBeanBusiness.findParroquiaById(parroquiaId);
  }

  public Collection findAllParroquia() throws Exception {
    return this.parroquiaBeanBusiness.findParroquiaAll();
  }

  public Collection findParroquiaByCodParroquia(String codParroquia)
    throws Exception
  {
    return this.parroquiaBeanBusiness.findByCodParroquia(codParroquia);
  }

  public Collection findParroquiaByNombre(String nombre)
    throws Exception
  {
    return this.parroquiaBeanBusiness.findByNombre(nombre);
  }

  public Collection findParroquiaByMunicipio(long idMunicipio)
    throws Exception
  {
    return this.parroquiaBeanBusiness.findByMunicipio(idMunicipio);
  }

  public void addRegionContinente(RegionContinente regionContinente)
    throws Exception
  {
    this.regionContinenteBeanBusiness.addRegionContinente(regionContinente);
  }

  public void updateRegionContinente(RegionContinente regionContinente) throws Exception {
    this.regionContinenteBeanBusiness.updateRegionContinente(regionContinente);
  }

  public void deleteRegionContinente(RegionContinente regionContinente) throws Exception {
    this.regionContinenteBeanBusiness.deleteRegionContinente(regionContinente);
  }

  public RegionContinente findRegionContinenteById(long regionContinenteId) throws Exception {
    return this.regionContinenteBeanBusiness.findRegionContinenteById(regionContinenteId);
  }

  public Collection findAllRegionContinente() throws Exception {
    return this.regionContinenteBeanBusiness.findRegionContinenteAll();
  }

  public Collection findRegionContinenteByCodRegionContinente(String codRegionContinente)
    throws Exception
  {
    return this.regionContinenteBeanBusiness.findByCodRegionContinente(codRegionContinente);
  }

  public Collection findRegionContinenteByNombre(String nombre)
    throws Exception
  {
    return this.regionContinenteBeanBusiness.findByNombre(nombre);
  }

  public Collection findRegionContinenteByContinente(long idContinente)
    throws Exception
  {
    return this.regionContinenteBeanBusiness.findByContinente(idContinente);
  }
}