package sigefirrhh.base.ubicacion;

import java.io.Serializable;

public class EstadoPK
  implements Serializable
{
  public long idEstado;

  public EstadoPK()
  {
  }

  public EstadoPK(long idEstado)
  {
    this.idEstado = idEstado;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EstadoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EstadoPK thatPK)
  {
    return 
      this.idEstado == thatPK.idEstado;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEstado)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEstado);
  }
}