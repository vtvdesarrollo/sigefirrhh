package sigefirrhh.base.ubicacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class EstadoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(EstadoForm.class.getName());
  private Estado estado;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private boolean showEstadoByCodEstado;
  private boolean showEstadoByNombre;
  private boolean showEstadoByPais;
  private String findCodEstado;
  private String findNombre;
  private String findSelectPais;
  private Collection findColPais;
  private Collection colPais;
  private String selectPais;
  private Object stateResultEstadoByCodEstado = null;

  private Object stateResultEstadoByNombre = null;

  private Object stateResultEstadoByPais = null;

  public String getFindCodEstado()
  {
    return this.findCodEstado;
  }
  public void setFindCodEstado(String findCodEstado) {
    this.findCodEstado = findCodEstado;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }
  public String getFindSelectPais() {
    return this.findSelectPais;
  }
  public void setFindSelectPais(String valPais) {
    this.findSelectPais = valPais;
  }

  public Collection getFindColPais() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPais.iterator();
    Pais pais = null;
    while (iterator.hasNext()) {
      pais = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(pais.getIdPais()), 
        pais.toString()));
    }
    return col;
  }

  public String getSelectPais()
  {
    return this.selectPais;
  }
  public void setSelectPais(String valPais) {
    Iterator iterator = this.colPais.iterator();
    Pais pais = null;
    this.estado.setPais(null);
    while (iterator.hasNext()) {
      pais = (Pais)iterator.next();
      if (String.valueOf(pais.getIdPais()).equals(
        valPais)) {
        this.estado.setPais(
          pais);
        break;
      }
    }
    this.selectPais = valPais;
  }
  public Collection getResult() {
    return this.result;
  }

  public Estado getEstado() {
    if (this.estado == null) {
      this.estado = new Estado();
    }
    return this.estado;
  }

  public EstadoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPais()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPais.iterator();
    Pais pais = null;
    while (iterator.hasNext()) {
      pais = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(pais.getIdPais()), 
        pais.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColPais = 
        this.ubicacionFacade.findAllPais();

      this.colPais = 
        this.ubicacionFacade.findAllPais();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findEstadoByCodEstado()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findEstadoByCodEstado(this.findCodEstado);
      this.showEstadoByCodEstado = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEstadoByCodEstado)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodEstado = null;
    this.findNombre = null;
    this.findSelectPais = null;

    return null;
  }

  public String findEstadoByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findEstadoByNombre(this.findNombre);
      this.showEstadoByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEstadoByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodEstado = null;
    this.findNombre = null;
    this.findSelectPais = null;

    return null;
  }

  public String findEstadoByPais()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findEstadoByPais(Long.valueOf(this.findSelectPais).longValue());
      this.showEstadoByPais = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEstadoByPais)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodEstado = null;
    this.findNombre = null;
    this.findSelectPais = null;

    return null;
  }

  public boolean isShowEstadoByCodEstado() {
    return this.showEstadoByCodEstado;
  }
  public boolean isShowEstadoByNombre() {
    return this.showEstadoByNombre;
  }
  public boolean isShowEstadoByPais() {
    return this.showEstadoByPais;
  }

  public String selectEstado()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPais = null;

    long idEstado = 
      Long.parseLong((String)requestParameterMap.get("idEstado"));
    try
    {
      this.estado = 
        this.ubicacionFacade.findEstadoById(
        idEstado);
      if (this.estado.getPais() != null) {
        this.selectPais = 
          String.valueOf(this.estado.getPais().getIdPais());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.estado = null;
    this.showEstadoByCodEstado = false;
    this.showEstadoByNombre = false;
    this.showEstadoByPais = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.ubicacionFacade.addEstado(
          this.estado);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.ubicacionFacade.updateEstado(
          this.estado);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.ubicacionFacade.deleteEstado(
        this.estado);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.estado = new Estado();

    this.selectPais = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.estado.setIdEstado(identityGenerator.getNextSequenceNumber("sigefirrhh.base.ubicacion.Estado"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.estado = new Estado();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}