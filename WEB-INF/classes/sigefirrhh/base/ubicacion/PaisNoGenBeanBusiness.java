package sigefirrhh.base.ubicacion;

import eforserver.business.AbstractBusiness;
import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import sigefirrhh.general.Lista;

public class PaisNoGenBeanBusiness extends AbstractBusiness
  implements Serializable
{
  public Collection findAll()
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsData = null;
    PreparedStatement stData = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select a.id_pais as id, (a.nombre || ' - ' || a.cod_pais)  as descripcion  ");
      sql.append(" from pais a");
      sql.append(" order by a.nombre");

      stData = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      rsData = stData.executeQuery();

      Lista lista = new Lista();
      while (rsData.next()) {
        lista = new Lista();
        lista.setId(rsData.getLong("id"));
        lista.setNombre(rsData.getString("descripcion"));
        col.add(lista);
      }
      return col;
    } finally {
      if (rsData != null) try {
          rsData.close();
        } catch (Exception localException3) {
        } if (stData != null) try {
          stData.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}