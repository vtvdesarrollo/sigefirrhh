package sigefirrhh.base.ubicacion;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class UbicacionNoGenFacade extends UbicacionFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private UbicacionNoGenBusiness ubicacionNoGenBusiness = new UbicacionNoGenBusiness();

  public Collection findCiudadByIdEstado(long idEstado)
    throws Exception
  {
    return this.ubicacionNoGenBusiness.findCiudadByIdEstado(idEstado);
  }

  public Collection findEstadoByIdPais(long idPais)
    throws Exception
  {
    return this.ubicacionNoGenBusiness.findEstadoByIdPais(idPais);
  }

  public Collection findMunicipioByIdEstado(long idEstado) throws Exception
  {
    try {
      this.txn.open();
      return this.ubicacionNoGenBusiness.findMunicipioByIdEstado(idEstado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPaisAll()
    throws Exception
  {
    return this.ubicacionNoGenBusiness.findPaisAll();
  }

  public Collection findParroquiaByIdMunicipio(long idMunicipio)
    throws Exception
  {
    return this.ubicacionNoGenBusiness.findParroquiaByIdMunicipio(idMunicipio);
  }
}