package sigefirrhh.base.ubicacion;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class UbicacionFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private UbicacionBusiness ubicacionBusiness = new UbicacionBusiness();

  public void addCiudad(Ciudad ciudad)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ubicacionBusiness.addCiudad(ciudad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCiudad(Ciudad ciudad) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.updateCiudad(ciudad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCiudad(Ciudad ciudad) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.deleteCiudad(ciudad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Ciudad findCiudadById(long ciudadId) throws Exception
  {
    try { this.txn.open();
      Ciudad ciudad = 
        this.ubicacionBusiness.findCiudadById(ciudadId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(ciudad);
      return ciudad;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCiudad() throws Exception
  {
    try { this.txn.open();
      return this.ubicacionBusiness.findAllCiudad();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCiudadByCodCiudad(String codCiudad)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findCiudadByCodCiudad(codCiudad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCiudadByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findCiudadByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCiudadByEstado(long idEstado)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findCiudadByEstado(idEstado);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addContinente(Continente continente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ubicacionBusiness.addContinente(continente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateContinente(Continente continente) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.updateContinente(continente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteContinente(Continente continente) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.deleteContinente(continente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Continente findContinenteById(long continenteId) throws Exception
  {
    try { this.txn.open();
      Continente continente = 
        this.ubicacionBusiness.findContinenteById(continenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(continente);
      return continente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllContinente() throws Exception
  {
    try { this.txn.open();
      return this.ubicacionBusiness.findAllContinente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findContinenteByCodContinente(String codContinente)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findContinenteByCodContinente(codContinente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findContinenteByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findContinenteByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEstado(Estado estado)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ubicacionBusiness.addEstado(estado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEstado(Estado estado) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.updateEstado(estado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEstado(Estado estado) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.deleteEstado(estado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Estado findEstadoById(long estadoId) throws Exception
  {
    try { this.txn.open();
      Estado estado = 
        this.ubicacionBusiness.findEstadoById(estadoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(estado);
      return estado;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEstado() throws Exception
  {
    try { this.txn.open();
      return this.ubicacionBusiness.findAllEstado();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEstadoByCodEstado(String codEstado)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findEstadoByCodEstado(codEstado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEstadoByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findEstadoByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEstadoByPais(long idPais)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findEstadoByPais(idPais);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addMunicipio(Municipio municipio)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ubicacionBusiness.addMunicipio(municipio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateMunicipio(Municipio municipio) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.updateMunicipio(municipio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteMunicipio(Municipio municipio) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.deleteMunicipio(municipio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Municipio findMunicipioById(long municipioId) throws Exception
  {
    try { this.txn.open();
      Municipio municipio = 
        this.ubicacionBusiness.findMunicipioById(municipioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(municipio);
      return municipio;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllMunicipio() throws Exception
  {
    try { this.txn.open();
      return this.ubicacionBusiness.findAllMunicipio();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMunicipioByCodMunicipio(String codMunicipio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findMunicipioByCodMunicipio(codMunicipio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMunicipioByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findMunicipioByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMunicipioByEstado(long idEstado)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findMunicipioByEstado(idEstado);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPais(Pais pais)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ubicacionBusiness.addPais(pais);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePais(Pais pais) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.updatePais(pais);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePais(Pais pais) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.deletePais(pais);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Pais findPaisById(long paisId) throws Exception
  {
    try { this.txn.open();
      Pais pais = 
        this.ubicacionBusiness.findPaisById(paisId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(pais);
      return pais;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPais() throws Exception
  {
    try { this.txn.open();
      return this.ubicacionBusiness.findAllPais();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPaisByCodPais(String codPais)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findPaisByCodPais(codPais);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPaisByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findPaisByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addParroquia(Parroquia parroquia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ubicacionBusiness.addParroquia(parroquia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParroquia(Parroquia parroquia) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.updateParroquia(parroquia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParroquia(Parroquia parroquia) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.deleteParroquia(parroquia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Parroquia findParroquiaById(long parroquiaId) throws Exception
  {
    try { this.txn.open();
      Parroquia parroquia = 
        this.ubicacionBusiness.findParroquiaById(parroquiaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(parroquia);
      return parroquia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParroquia() throws Exception
  {
    try { this.txn.open();
      return this.ubicacionBusiness.findAllParroquia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParroquiaByCodParroquia(String codParroquia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findParroquiaByCodParroquia(codParroquia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParroquiaByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findParroquiaByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParroquiaByMunicipio(long idMunicipio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findParroquiaByMunicipio(idMunicipio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRegionContinente(RegionContinente regionContinente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ubicacionBusiness.addRegionContinente(regionContinente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRegionContinente(RegionContinente regionContinente) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.updateRegionContinente(regionContinente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRegionContinente(RegionContinente regionContinente) throws Exception
  {
    try { this.txn.open();
      this.ubicacionBusiness.deleteRegionContinente(regionContinente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RegionContinente findRegionContinenteById(long regionContinenteId) throws Exception
  {
    try { this.txn.open();
      RegionContinente regionContinente = 
        this.ubicacionBusiness.findRegionContinenteById(regionContinenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(regionContinente);
      return regionContinente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRegionContinente() throws Exception
  {
    try { this.txn.open();
      return this.ubicacionBusiness.findAllRegionContinente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegionContinenteByCodRegionContinente(String codRegionContinente)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findRegionContinenteByCodRegionContinente(codRegionContinente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegionContinenteByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findRegionContinenteByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegionContinenteByContinente(long idContinente)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ubicacionBusiness.findRegionContinenteByContinente(idContinente);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}