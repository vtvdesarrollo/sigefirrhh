package sigefirrhh.base.ubicacion;

import java.io.Serializable;

public class ParroquiaPK
  implements Serializable
{
  public long idParroquia;

  public ParroquiaPK()
  {
  }

  public ParroquiaPK(long idParroquia)
  {
    this.idParroquia = idParroquia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParroquiaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParroquiaPK thatPK)
  {
    return 
      this.idParroquia == thatPK.idParroquia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParroquia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParroquia);
  }
}