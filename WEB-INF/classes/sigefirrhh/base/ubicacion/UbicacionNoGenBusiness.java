package sigefirrhh.base.ubicacion;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class UbicacionNoGenBusiness extends AbstractBusiness
  implements Serializable
{
  private CiudadNoGenBeanBusiness ciudadNoGenBeanBusiness = new CiudadNoGenBeanBusiness();

  private EstadoNoGenBeanBusiness estadoNoGenBeanBusiness = new EstadoNoGenBeanBusiness();

  private MunicipioNoGenBeanBusiness municipioNoGenBeanBusiness = new MunicipioNoGenBeanBusiness();

  private ParroquiaNoGenBeanBusiness parroquiaNoGenBeanBusiness = new ParroquiaNoGenBeanBusiness();

  private PaisNoGenBeanBusiness paisNoGenBeanBusiness = new PaisNoGenBeanBusiness();

  public Collection findCiudadByIdEstado(long idEstado)
    throws Exception
  {
    return this.ciudadNoGenBeanBusiness.findByIdEstado(idEstado);
  }

  public Collection findEstadoByIdPais(long idPais)
    throws Exception
  {
    return this.estadoNoGenBeanBusiness.findByIdPais(idPais);
  }

  public Collection findMunicipioByIdEstado(long idEstado)
    throws Exception
  {
    return this.municipioNoGenBeanBusiness.findByIdEstado(idEstado);
  }

  public Collection findPaisAll()
    throws Exception
  {
    return this.paisNoGenBeanBusiness.findAll();
  }

  public Collection findParroquiaByIdMunicipio(long idMunicipio)
    throws Exception
  {
    return this.parroquiaNoGenBeanBusiness.findByIdMunicipio(idMunicipio);
  }
}