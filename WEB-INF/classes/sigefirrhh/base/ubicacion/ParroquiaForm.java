package sigefirrhh.base.ubicacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class ParroquiaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ParroquiaForm.class.getName());
  private Parroquia parroquia;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private boolean showParroquiaByCodParroquia;
  private boolean showParroquiaByNombre;
  private boolean showParroquiaByMunicipio;
  private String findCodParroquia;
  private String findNombre;
  private String findSelectPaisForMunicipio;
  private String findSelectEstadoForMunicipio;
  private String findSelectMunicipio;
  private Collection findColPaisForMunicipio;
  private Collection findColEstadoForMunicipio;
  private Collection findColMunicipio;
  private Collection colPaisForMunicipio;
  private Collection colEstadoForMunicipio;
  private Collection colMunicipio;
  private String selectPaisForMunicipio;
  private String selectEstadoForMunicipio;
  private String selectMunicipio;
  private Object stateResultParroquiaByCodParroquia = null;

  private Object stateResultParroquiaByNombre = null;

  private Object stateResultParroquiaByMunicipio = null;

  public String getFindCodParroquia()
  {
    return this.findCodParroquia;
  }
  public void setFindCodParroquia(String findCodParroquia) {
    this.findCodParroquia = findCodParroquia;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }
  public Collection getFindColPaisForMunicipio() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPaisForMunicipio.iterator();
    Pais paisForMunicipio = null;
    while (iterator.hasNext()) {
      paisForMunicipio = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForMunicipio.getIdPais()), 
        paisForMunicipio.toString()));
    }
    return col;
  }
  public String getFindSelectPaisForMunicipio() {
    return this.findSelectPaisForMunicipio;
  }
  public void setFindSelectPaisForMunicipio(String valPaisForMunicipio) {
    this.findSelectPaisForMunicipio = valPaisForMunicipio;
  }
  public void findChangePaisForMunicipio(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColMunicipio = null;
      this.findColEstadoForMunicipio = null;
      if (idPais > 0L)
        this.findColEstadoForMunicipio = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowPaisForMunicipio() { return this.findColPaisForMunicipio != null; }

  public Collection getFindColEstadoForMunicipio() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColEstadoForMunicipio.iterator();
    Estado estadoForMunicipio = null;
    while (iterator.hasNext()) {
      estadoForMunicipio = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estadoForMunicipio.getIdEstado()), 
        estadoForMunicipio.toString()));
    }
    return col;
  }
  public String getFindSelectEstadoForMunicipio() {
    return this.findSelectEstadoForMunicipio;
  }
  public void setFindSelectEstadoForMunicipio(String valEstadoForMunicipio) {
    this.findSelectEstadoForMunicipio = valEstadoForMunicipio;
  }
  public void findChangeEstadoForMunicipio(ValueChangeEvent event) {
    long idEstado = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColMunicipio = null;
      if (idEstado > 0L)
        this.findColMunicipio = 
          this.ubicacionFacade.findMunicipioByEstado(
          idEstado);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowEstadoForMunicipio() { return this.findColEstadoForMunicipio != null; }

  public String getFindSelectMunicipio() {
    return this.findSelectMunicipio;
  }
  public void setFindSelectMunicipio(String valMunicipio) {
    this.findSelectMunicipio = valMunicipio;
  }

  public Collection getFindColMunicipio() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColMunicipio.iterator();
    Municipio municipio = null;
    while (iterator.hasNext()) {
      municipio = (Municipio)iterator.next();
      col.add(new SelectItem(
        String.valueOf(municipio.getIdMunicipio()), 
        municipio.toString()));
    }
    return col;
  }
  public boolean isFindShowMunicipio() {
    return this.findColMunicipio != null;
  }

  public String getSelectPaisForMunicipio()
  {
    return this.selectPaisForMunicipio;
  }
  public void setSelectPaisForMunicipio(String valPaisForMunicipio) {
    this.selectPaisForMunicipio = valPaisForMunicipio;
  }
  public void changePaisForMunicipio(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colMunicipio = null;
      this.colEstadoForMunicipio = null;
      if (idPais > 0L) {
        this.colEstadoForMunicipio = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
      } else {
        this.selectMunicipio = null;
        this.parroquia.setMunicipio(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectMunicipio = null;
      this.parroquia.setMunicipio(
        null);
    }
  }

  public boolean isShowPaisForMunicipio() { return this.colPaisForMunicipio != null; }

  public String getSelectEstadoForMunicipio() {
    return this.selectEstadoForMunicipio;
  }
  public void setSelectEstadoForMunicipio(String valEstadoForMunicipio) {
    this.selectEstadoForMunicipio = valEstadoForMunicipio;
  }
  public void changeEstadoForMunicipio(ValueChangeEvent event) {
    long idEstado = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colMunicipio = null;
      if (idEstado > 0L) {
        this.colMunicipio = 
          this.ubicacionFacade.findMunicipioByEstado(
          idEstado);
      } else {
        this.selectMunicipio = null;
        this.parroquia.setMunicipio(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectMunicipio = null;
      this.parroquia.setMunicipio(
        null);
    }
  }

  public boolean isShowEstadoForMunicipio() { return this.colEstadoForMunicipio != null; }

  public String getSelectMunicipio() {
    return this.selectMunicipio;
  }
  public void setSelectMunicipio(String valMunicipio) {
    Iterator iterator = this.colMunicipio.iterator();
    Municipio municipio = null;
    this.parroquia.setMunicipio(null);
    while (iterator.hasNext()) {
      municipio = (Municipio)iterator.next();
      if (String.valueOf(municipio.getIdMunicipio()).equals(
        valMunicipio)) {
        this.parroquia.setMunicipio(
          municipio);
        break;
      }
    }
    this.selectMunicipio = valMunicipio;
  }
  public boolean isShowMunicipio() {
    return this.colMunicipio != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public Parroquia getParroquia() {
    if (this.parroquia == null) {
      this.parroquia = new Parroquia();
    }
    return this.parroquia;
  }

  public ParroquiaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPaisForMunicipio()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForMunicipio.iterator();
    Pais paisForMunicipio = null;
    while (iterator.hasNext()) {
      paisForMunicipio = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForMunicipio.getIdPais()), 
        paisForMunicipio.toString()));
    }
    return col;
  }

  public Collection getColEstadoForMunicipio()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForMunicipio.iterator();
    Estado estadoForMunicipio = null;
    while (iterator.hasNext()) {
      estadoForMunicipio = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estadoForMunicipio.getIdEstado()), 
        estadoForMunicipio.toString()));
    }
    return col;
  }

  public Collection getColMunicipio()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colMunicipio.iterator();
    Municipio municipio = null;
    while (iterator.hasNext()) {
      municipio = (Municipio)iterator.next();
      col.add(new SelectItem(
        String.valueOf(municipio.getIdMunicipio()), 
        municipio.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColPaisForMunicipio = 
        this.ubicacionFacade.findAllPais();

      this.colPaisForMunicipio = 
        this.ubicacionFacade.findAllPais();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findParroquiaByCodParroquia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findParroquiaByCodParroquia(this.findCodParroquia);
      this.showParroquiaByCodParroquia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showParroquiaByCodParroquia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodParroquia = null;
    this.findNombre = null;
    this.findSelectPaisForMunicipio = null;
    this.findSelectEstadoForMunicipio = null;
    this.findSelectMunicipio = null;

    return null;
  }

  public String findParroquiaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findParroquiaByNombre(this.findNombre);
      this.showParroquiaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showParroquiaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodParroquia = null;
    this.findNombre = null;
    this.findSelectPaisForMunicipio = null;
    this.findSelectEstadoForMunicipio = null;
    this.findSelectMunicipio = null;

    return null;
  }

  public String findParroquiaByMunicipio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findParroquiaByMunicipio(Long.valueOf(this.findSelectMunicipio).longValue());
      this.showParroquiaByMunicipio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showParroquiaByMunicipio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodParroquia = null;
    this.findNombre = null;
    this.findSelectPaisForMunicipio = null;
    this.findSelectEstadoForMunicipio = null;
    this.findSelectMunicipio = null;

    return null;
  }

  public boolean isShowParroquiaByCodParroquia() {
    return this.showParroquiaByCodParroquia;
  }
  public boolean isShowParroquiaByNombre() {
    return this.showParroquiaByNombre;
  }
  public boolean isShowParroquiaByMunicipio() {
    return this.showParroquiaByMunicipio;
  }

  public String selectParroquia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectMunicipio = null;
    this.selectPaisForMunicipio = null;

    this.selectEstadoForMunicipio = null;

    long idParroquia = 
      Long.parseLong((String)requestParameterMap.get("idParroquia"));
    try
    {
      this.parroquia = 
        this.ubicacionFacade.findParroquiaById(
        idParroquia);
      if (this.parroquia.getMunicipio() != null) {
        this.selectMunicipio = 
          String.valueOf(this.parroquia.getMunicipio().getIdMunicipio());
      }

      Municipio municipio = null;
      Estado estadoForMunicipio = null;
      Pais paisForMunicipio = null;

      if (this.parroquia.getMunicipio() != null) {
        long idMunicipio = 
          this.parroquia.getMunicipio().getIdMunicipio();
        this.selectMunicipio = String.valueOf(idMunicipio);
        municipio = this.ubicacionFacade.findMunicipioById(
          idMunicipio);
        this.colMunicipio = this.ubicacionFacade.findMunicipioByEstado(
          municipio.getEstado().getIdEstado());

        long idEstadoForMunicipio = 
          this.parroquia.getMunicipio().getEstado().getIdEstado();
        this.selectEstadoForMunicipio = String.valueOf(idEstadoForMunicipio);
        estadoForMunicipio = 
          this.ubicacionFacade.findEstadoById(
          idEstadoForMunicipio);
        this.colEstadoForMunicipio = 
          this.ubicacionFacade.findEstadoByPais(
          estadoForMunicipio.getPais().getIdPais());

        long idPaisForMunicipio = 
          estadoForMunicipio.getPais().getIdPais();
        this.selectPaisForMunicipio = String.valueOf(idPaisForMunicipio);
        paisForMunicipio = 
          this.ubicacionFacade.findPaisById(
          idPaisForMunicipio);
        this.colPaisForMunicipio = 
          this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.parroquia = null;
    this.showParroquiaByCodParroquia = false;
    this.showParroquiaByNombre = false;
    this.showParroquiaByMunicipio = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.ubicacionFacade.addParroquia(
          this.parroquia);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.ubicacionFacade.updateParroquia(
          this.parroquia);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.ubicacionFacade.deleteParroquia(
        this.parroquia);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.parroquia = new Parroquia();

    this.selectMunicipio = null;

    this.selectPaisForMunicipio = null;

    this.selectEstadoForMunicipio = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.parroquia.setIdParroquia(identityGenerator.getNextSequenceNumber("sigefirrhh.base.ubicacion.Parroquia"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.parroquia = new Parroquia();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}