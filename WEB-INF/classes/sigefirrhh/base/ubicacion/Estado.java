package sigefirrhh.base.ubicacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Estado
  implements Serializable, PersistenceCapable
{
  private long idEstado;
  private String codEstado;
  private String nombre;
  private String abreviatura;
  private Pais pais;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "abreviatura", "codEstado", "idEstado", "nombre", "pais" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Pais") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " " + 
      jdoGetcodEstado(this) + " " + 
      jdoGetpais(this).getNombre();
  }

  public String getAbreviatura()
  {
    return jdoGetabreviatura(this);
  }

  public String getCodEstado()
  {
    return jdoGetcodEstado(this);
  }

  public long getIdEstado()
  {
    return jdoGetidEstado(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public Pais getPais()
  {
    return jdoGetpais(this);
  }

  public void setAbreviatura(String string)
  {
    jdoSetabreviatura(this, string);
  }

  public void setCodEstado(String string)
  {
    jdoSetcodEstado(this, string);
  }

  public void setIdEstado(long l)
  {
    jdoSetidEstado(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setPais(Pais pais)
  {
    jdoSetpais(this, pais);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.ubicacion.Estado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Estado());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Estado localEstado = new Estado();
    localEstado.jdoFlags = 1;
    localEstado.jdoStateManager = paramStateManager;
    return localEstado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Estado localEstado = new Estado();
    localEstado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEstado.jdoFlags = 1;
    localEstado.jdoStateManager = paramStateManager;
    return localEstado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.abreviatura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codEstado);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEstado);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.pais);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.abreviatura = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codEstado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEstado = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pais = ((Pais)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Estado paramEstado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEstado == null)
        throw new IllegalArgumentException("arg1");
      this.abreviatura = paramEstado.abreviatura;
      return;
    case 1:
      if (paramEstado == null)
        throw new IllegalArgumentException("arg1");
      this.codEstado = paramEstado.codEstado;
      return;
    case 2:
      if (paramEstado == null)
        throw new IllegalArgumentException("arg1");
      this.idEstado = paramEstado.idEstado;
      return;
    case 3:
      if (paramEstado == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramEstado.nombre;
      return;
    case 4:
      if (paramEstado == null)
        throw new IllegalArgumentException("arg1");
      this.pais = paramEstado.pais;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Estado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Estado localEstado = (Estado)paramObject;
    if (localEstado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEstado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EstadoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EstadoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EstadoPK))
      throw new IllegalArgumentException("arg1");
    EstadoPK localEstadoPK = (EstadoPK)paramObject;
    localEstadoPK.idEstado = this.idEstado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EstadoPK))
      throw new IllegalArgumentException("arg1");
    EstadoPK localEstadoPK = (EstadoPK)paramObject;
    this.idEstado = localEstadoPK.idEstado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EstadoPK))
      throw new IllegalArgumentException("arg2");
    EstadoPK localEstadoPK = (EstadoPK)paramObject;
    localEstadoPK.idEstado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EstadoPK))
      throw new IllegalArgumentException("arg2");
    EstadoPK localEstadoPK = (EstadoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localEstadoPK.idEstado);
  }

  private static final String jdoGetabreviatura(Estado paramEstado)
  {
    if (paramEstado.jdoFlags <= 0)
      return paramEstado.abreviatura;
    StateManager localStateManager = paramEstado.jdoStateManager;
    if (localStateManager == null)
      return paramEstado.abreviatura;
    if (localStateManager.isLoaded(paramEstado, jdoInheritedFieldCount + 0))
      return paramEstado.abreviatura;
    return localStateManager.getStringField(paramEstado, jdoInheritedFieldCount + 0, paramEstado.abreviatura);
  }

  private static final void jdoSetabreviatura(Estado paramEstado, String paramString)
  {
    if (paramEstado.jdoFlags == 0)
    {
      paramEstado.abreviatura = paramString;
      return;
    }
    StateManager localStateManager = paramEstado.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstado.abreviatura = paramString;
      return;
    }
    localStateManager.setStringField(paramEstado, jdoInheritedFieldCount + 0, paramEstado.abreviatura, paramString);
  }

  private static final String jdoGetcodEstado(Estado paramEstado)
  {
    if (paramEstado.jdoFlags <= 0)
      return paramEstado.codEstado;
    StateManager localStateManager = paramEstado.jdoStateManager;
    if (localStateManager == null)
      return paramEstado.codEstado;
    if (localStateManager.isLoaded(paramEstado, jdoInheritedFieldCount + 1))
      return paramEstado.codEstado;
    return localStateManager.getStringField(paramEstado, jdoInheritedFieldCount + 1, paramEstado.codEstado);
  }

  private static final void jdoSetcodEstado(Estado paramEstado, String paramString)
  {
    if (paramEstado.jdoFlags == 0)
    {
      paramEstado.codEstado = paramString;
      return;
    }
    StateManager localStateManager = paramEstado.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstado.codEstado = paramString;
      return;
    }
    localStateManager.setStringField(paramEstado, jdoInheritedFieldCount + 1, paramEstado.codEstado, paramString);
  }

  private static final long jdoGetidEstado(Estado paramEstado)
  {
    return paramEstado.idEstado;
  }

  private static final void jdoSetidEstado(Estado paramEstado, long paramLong)
  {
    StateManager localStateManager = paramEstado.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstado.idEstado = paramLong;
      return;
    }
    localStateManager.setLongField(paramEstado, jdoInheritedFieldCount + 2, paramEstado.idEstado, paramLong);
  }

  private static final String jdoGetnombre(Estado paramEstado)
  {
    if (paramEstado.jdoFlags <= 0)
      return paramEstado.nombre;
    StateManager localStateManager = paramEstado.jdoStateManager;
    if (localStateManager == null)
      return paramEstado.nombre;
    if (localStateManager.isLoaded(paramEstado, jdoInheritedFieldCount + 3))
      return paramEstado.nombre;
    return localStateManager.getStringField(paramEstado, jdoInheritedFieldCount + 3, paramEstado.nombre);
  }

  private static final void jdoSetnombre(Estado paramEstado, String paramString)
  {
    if (paramEstado.jdoFlags == 0)
    {
      paramEstado.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramEstado.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstado.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramEstado, jdoInheritedFieldCount + 3, paramEstado.nombre, paramString);
  }

  private static final Pais jdoGetpais(Estado paramEstado)
  {
    StateManager localStateManager = paramEstado.jdoStateManager;
    if (localStateManager == null)
      return paramEstado.pais;
    if (localStateManager.isLoaded(paramEstado, jdoInheritedFieldCount + 4))
      return paramEstado.pais;
    return (Pais)localStateManager.getObjectField(paramEstado, jdoInheritedFieldCount + 4, paramEstado.pais);
  }

  private static final void jdoSetpais(Estado paramEstado, Pais paramPais)
  {
    StateManager localStateManager = paramEstado.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstado.pais = paramPais;
      return;
    }
    localStateManager.setObjectField(paramEstado, jdoInheritedFieldCount + 4, paramEstado.pais, paramPais);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}