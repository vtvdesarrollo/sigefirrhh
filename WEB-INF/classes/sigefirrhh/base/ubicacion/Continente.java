package sigefirrhh.base.ubicacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Continente
  implements Serializable, PersistenceCapable
{
  private long idContinente;
  private String codContinente;
  private String nombre;
  private String abreviatura;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "abreviatura", "codContinente", "idContinente", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " " + jdoGetcodContinente(this);
  }

  public String getAbreviatura()
  {
    return jdoGetabreviatura(this);
  }

  public String getCodContinente()
  {
    return jdoGetcodContinente(this);
  }

  public long getIdContinente()
  {
    return jdoGetidContinente(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setAbreviatura(String string)
  {
    jdoSetabreviatura(this, string);
  }

  public void setCodContinente(String string)
  {
    jdoSetcodContinente(this, string);
  }

  public void setIdContinente(long l)
  {
    jdoSetidContinente(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.ubicacion.Continente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Continente());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Continente localContinente = new Continente();
    localContinente.jdoFlags = 1;
    localContinente.jdoStateManager = paramStateManager;
    return localContinente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Continente localContinente = new Continente();
    localContinente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localContinente.jdoFlags = 1;
    localContinente.jdoStateManager = paramStateManager;
    return localContinente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.abreviatura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codContinente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idContinente);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.abreviatura = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codContinente = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idContinente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Continente paramContinente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramContinente == null)
        throw new IllegalArgumentException("arg1");
      this.abreviatura = paramContinente.abreviatura;
      return;
    case 1:
      if (paramContinente == null)
        throw new IllegalArgumentException("arg1");
      this.codContinente = paramContinente.codContinente;
      return;
    case 2:
      if (paramContinente == null)
        throw new IllegalArgumentException("arg1");
      this.idContinente = paramContinente.idContinente;
      return;
    case 3:
      if (paramContinente == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramContinente.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Continente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Continente localContinente = (Continente)paramObject;
    if (localContinente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localContinente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ContinentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ContinentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ContinentePK))
      throw new IllegalArgumentException("arg1");
    ContinentePK localContinentePK = (ContinentePK)paramObject;
    localContinentePK.idContinente = this.idContinente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ContinentePK))
      throw new IllegalArgumentException("arg1");
    ContinentePK localContinentePK = (ContinentePK)paramObject;
    this.idContinente = localContinentePK.idContinente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ContinentePK))
      throw new IllegalArgumentException("arg2");
    ContinentePK localContinentePK = (ContinentePK)paramObject;
    localContinentePK.idContinente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ContinentePK))
      throw new IllegalArgumentException("arg2");
    ContinentePK localContinentePK = (ContinentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localContinentePK.idContinente);
  }

  private static final String jdoGetabreviatura(Continente paramContinente)
  {
    if (paramContinente.jdoFlags <= 0)
      return paramContinente.abreviatura;
    StateManager localStateManager = paramContinente.jdoStateManager;
    if (localStateManager == null)
      return paramContinente.abreviatura;
    if (localStateManager.isLoaded(paramContinente, jdoInheritedFieldCount + 0))
      return paramContinente.abreviatura;
    return localStateManager.getStringField(paramContinente, jdoInheritedFieldCount + 0, paramContinente.abreviatura);
  }

  private static final void jdoSetabreviatura(Continente paramContinente, String paramString)
  {
    if (paramContinente.jdoFlags == 0)
    {
      paramContinente.abreviatura = paramString;
      return;
    }
    StateManager localStateManager = paramContinente.jdoStateManager;
    if (localStateManager == null)
    {
      paramContinente.abreviatura = paramString;
      return;
    }
    localStateManager.setStringField(paramContinente, jdoInheritedFieldCount + 0, paramContinente.abreviatura, paramString);
  }

  private static final String jdoGetcodContinente(Continente paramContinente)
  {
    if (paramContinente.jdoFlags <= 0)
      return paramContinente.codContinente;
    StateManager localStateManager = paramContinente.jdoStateManager;
    if (localStateManager == null)
      return paramContinente.codContinente;
    if (localStateManager.isLoaded(paramContinente, jdoInheritedFieldCount + 1))
      return paramContinente.codContinente;
    return localStateManager.getStringField(paramContinente, jdoInheritedFieldCount + 1, paramContinente.codContinente);
  }

  private static final void jdoSetcodContinente(Continente paramContinente, String paramString)
  {
    if (paramContinente.jdoFlags == 0)
    {
      paramContinente.codContinente = paramString;
      return;
    }
    StateManager localStateManager = paramContinente.jdoStateManager;
    if (localStateManager == null)
    {
      paramContinente.codContinente = paramString;
      return;
    }
    localStateManager.setStringField(paramContinente, jdoInheritedFieldCount + 1, paramContinente.codContinente, paramString);
  }

  private static final long jdoGetidContinente(Continente paramContinente)
  {
    return paramContinente.idContinente;
  }

  private static final void jdoSetidContinente(Continente paramContinente, long paramLong)
  {
    StateManager localStateManager = paramContinente.jdoStateManager;
    if (localStateManager == null)
    {
      paramContinente.idContinente = paramLong;
      return;
    }
    localStateManager.setLongField(paramContinente, jdoInheritedFieldCount + 2, paramContinente.idContinente, paramLong);
  }

  private static final String jdoGetnombre(Continente paramContinente)
  {
    if (paramContinente.jdoFlags <= 0)
      return paramContinente.nombre;
    StateManager localStateManager = paramContinente.jdoStateManager;
    if (localStateManager == null)
      return paramContinente.nombre;
    if (localStateManager.isLoaded(paramContinente, jdoInheritedFieldCount + 3))
      return paramContinente.nombre;
    return localStateManager.getStringField(paramContinente, jdoInheritedFieldCount + 3, paramContinente.nombre);
  }

  private static final void jdoSetnombre(Continente paramContinente, String paramString)
  {
    if (paramContinente.jdoFlags == 0)
    {
      paramContinente.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramContinente.jdoStateManager;
    if (localStateManager == null)
    {
      paramContinente.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramContinente, jdoInheritedFieldCount + 3, paramContinente.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}