package sigefirrhh.base.ubicacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class ContinenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ContinenteForm.class.getName());
  private Continente continente;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private boolean showContinenteByCodContinente;
  private boolean showContinenteByNombre;
  private String findCodContinente;
  private String findNombre;
  private Object stateResultContinenteByCodContinente = null;

  private Object stateResultContinenteByNombre = null;

  public String getFindCodContinente()
  {
    return this.findCodContinente;
  }
  public void setFindCodContinente(String findCodContinente) {
    this.findCodContinente = findCodContinente;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Continente getContinente() {
    if (this.continente == null) {
      this.continente = new Continente();
    }
    return this.continente;
  }

  public ContinenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findContinenteByCodContinente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findContinenteByCodContinente(this.findCodContinente);
      this.showContinenteByCodContinente = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showContinenteByCodContinente)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodContinente = null;
    this.findNombre = null;

    return null;
  }

  public String findContinenteByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ubicacionFacade.findContinenteByNombre(this.findNombre);
      this.showContinenteByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showContinenteByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodContinente = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowContinenteByCodContinente() {
    return this.showContinenteByCodContinente;
  }
  public boolean isShowContinenteByNombre() {
    return this.showContinenteByNombre;
  }

  public String selectContinente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idContinente = 
      Long.parseLong((String)requestParameterMap.get("idContinente"));
    try
    {
      this.continente = 
        this.ubicacionFacade.findContinenteById(
        idContinente);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.continente = null;
    this.showContinenteByCodContinente = false;
    this.showContinenteByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.ubicacionFacade.addContinente(
          this.continente);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.ubicacionFacade.updateContinente(
          this.continente);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.ubicacionFacade.deleteContinente(
        this.continente);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.continente = new Continente();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.continente.setIdContinente(identityGenerator.getNextSequenceNumber("sigefirrhh.base.ubicacion.Continente"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.continente = new Continente();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}