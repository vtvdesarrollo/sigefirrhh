package sigefirrhh.base.ubicacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Parroquia
  implements Serializable, PersistenceCapable
{
  private long idParroquia;
  private String codParroquia;
  private String nombre;
  private String abreviatura;
  private Municipio municipio;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "abreviatura", "codParroquia", "idParroquia", "municipio", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.ubicacion.Municipio"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " " + 
      jdoGetcodParroquia(this) + " " + 
      jdoGetmunicipio(this).getNombre() + " ";
  }

  public String getAbreviatura()
  {
    return jdoGetabreviatura(this);
  }

  public String getCodParroquia()
  {
    return jdoGetcodParroquia(this);
  }

  public long getIdParroquia()
  {
    return jdoGetidParroquia(this);
  }

  public Municipio getMunicipio()
  {
    return jdoGetmunicipio(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setAbreviatura(String string)
  {
    jdoSetabreviatura(this, string);
  }

  public void setCodParroquia(String string)
  {
    jdoSetcodParroquia(this, string);
  }

  public void setIdParroquia(long l)
  {
    jdoSetidParroquia(this, l);
  }

  public void setMunicipio(Municipio municipio)
  {
    jdoSetmunicipio(this, municipio);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.ubicacion.Parroquia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Parroquia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Parroquia localParroquia = new Parroquia();
    localParroquia.jdoFlags = 1;
    localParroquia.jdoStateManager = paramStateManager;
    return localParroquia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Parroquia localParroquia = new Parroquia();
    localParroquia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParroquia.jdoFlags = 1;
    localParroquia.jdoStateManager = paramStateManager;
    return localParroquia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.abreviatura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codParroquia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParroquia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.municipio);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.abreviatura = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codParroquia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParroquia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.municipio = ((Municipio)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Parroquia paramParroquia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParroquia == null)
        throw new IllegalArgumentException("arg1");
      this.abreviatura = paramParroquia.abreviatura;
      return;
    case 1:
      if (paramParroquia == null)
        throw new IllegalArgumentException("arg1");
      this.codParroquia = paramParroquia.codParroquia;
      return;
    case 2:
      if (paramParroquia == null)
        throw new IllegalArgumentException("arg1");
      this.idParroquia = paramParroquia.idParroquia;
      return;
    case 3:
      if (paramParroquia == null)
        throw new IllegalArgumentException("arg1");
      this.municipio = paramParroquia.municipio;
      return;
    case 4:
      if (paramParroquia == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramParroquia.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Parroquia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Parroquia localParroquia = (Parroquia)paramObject;
    if (localParroquia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParroquia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParroquiaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParroquiaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParroquiaPK))
      throw new IllegalArgumentException("arg1");
    ParroquiaPK localParroquiaPK = (ParroquiaPK)paramObject;
    localParroquiaPK.idParroquia = this.idParroquia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParroquiaPK))
      throw new IllegalArgumentException("arg1");
    ParroquiaPK localParroquiaPK = (ParroquiaPK)paramObject;
    this.idParroquia = localParroquiaPK.idParroquia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParroquiaPK))
      throw new IllegalArgumentException("arg2");
    ParroquiaPK localParroquiaPK = (ParroquiaPK)paramObject;
    localParroquiaPK.idParroquia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParroquiaPK))
      throw new IllegalArgumentException("arg2");
    ParroquiaPK localParroquiaPK = (ParroquiaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localParroquiaPK.idParroquia);
  }

  private static final String jdoGetabreviatura(Parroquia paramParroquia)
  {
    if (paramParroquia.jdoFlags <= 0)
      return paramParroquia.abreviatura;
    StateManager localStateManager = paramParroquia.jdoStateManager;
    if (localStateManager == null)
      return paramParroquia.abreviatura;
    if (localStateManager.isLoaded(paramParroquia, jdoInheritedFieldCount + 0))
      return paramParroquia.abreviatura;
    return localStateManager.getStringField(paramParroquia, jdoInheritedFieldCount + 0, paramParroquia.abreviatura);
  }

  private static final void jdoSetabreviatura(Parroquia paramParroquia, String paramString)
  {
    if (paramParroquia.jdoFlags == 0)
    {
      paramParroquia.abreviatura = paramString;
      return;
    }
    StateManager localStateManager = paramParroquia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParroquia.abreviatura = paramString;
      return;
    }
    localStateManager.setStringField(paramParroquia, jdoInheritedFieldCount + 0, paramParroquia.abreviatura, paramString);
  }

  private static final String jdoGetcodParroquia(Parroquia paramParroquia)
  {
    if (paramParroquia.jdoFlags <= 0)
      return paramParroquia.codParroquia;
    StateManager localStateManager = paramParroquia.jdoStateManager;
    if (localStateManager == null)
      return paramParroquia.codParroquia;
    if (localStateManager.isLoaded(paramParroquia, jdoInheritedFieldCount + 1))
      return paramParroquia.codParroquia;
    return localStateManager.getStringField(paramParroquia, jdoInheritedFieldCount + 1, paramParroquia.codParroquia);
  }

  private static final void jdoSetcodParroquia(Parroquia paramParroquia, String paramString)
  {
    if (paramParroquia.jdoFlags == 0)
    {
      paramParroquia.codParroquia = paramString;
      return;
    }
    StateManager localStateManager = paramParroquia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParroquia.codParroquia = paramString;
      return;
    }
    localStateManager.setStringField(paramParroquia, jdoInheritedFieldCount + 1, paramParroquia.codParroquia, paramString);
  }

  private static final long jdoGetidParroquia(Parroquia paramParroquia)
  {
    return paramParroquia.idParroquia;
  }

  private static final void jdoSetidParroquia(Parroquia paramParroquia, long paramLong)
  {
    StateManager localStateManager = paramParroquia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParroquia.idParroquia = paramLong;
      return;
    }
    localStateManager.setLongField(paramParroquia, jdoInheritedFieldCount + 2, paramParroquia.idParroquia, paramLong);
  }

  private static final Municipio jdoGetmunicipio(Parroquia paramParroquia)
  {
    StateManager localStateManager = paramParroquia.jdoStateManager;
    if (localStateManager == null)
      return paramParroquia.municipio;
    if (localStateManager.isLoaded(paramParroquia, jdoInheritedFieldCount + 3))
      return paramParroquia.municipio;
    return (Municipio)localStateManager.getObjectField(paramParroquia, jdoInheritedFieldCount + 3, paramParroquia.municipio);
  }

  private static final void jdoSetmunicipio(Parroquia paramParroquia, Municipio paramMunicipio)
  {
    StateManager localStateManager = paramParroquia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParroquia.municipio = paramMunicipio;
      return;
    }
    localStateManager.setObjectField(paramParroquia, jdoInheritedFieldCount + 3, paramParroquia.municipio, paramMunicipio);
  }

  private static final String jdoGetnombre(Parroquia paramParroquia)
  {
    if (paramParroquia.jdoFlags <= 0)
      return paramParroquia.nombre;
    StateManager localStateManager = paramParroquia.jdoStateManager;
    if (localStateManager == null)
      return paramParroquia.nombre;
    if (localStateManager.isLoaded(paramParroquia, jdoInheritedFieldCount + 4))
      return paramParroquia.nombre;
    return localStateManager.getStringField(paramParroquia, jdoInheritedFieldCount + 4, paramParroquia.nombre);
  }

  private static final void jdoSetnombre(Parroquia paramParroquia, String paramString)
  {
    if (paramParroquia.jdoFlags == 0)
    {
      paramParroquia.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramParroquia.jdoStateManager;
    if (localStateManager == null)
    {
      paramParroquia.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramParroquia, jdoInheritedFieldCount + 4, paramParroquia.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}