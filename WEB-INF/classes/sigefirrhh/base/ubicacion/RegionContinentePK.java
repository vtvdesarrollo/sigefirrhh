package sigefirrhh.base.ubicacion;

import java.io.Serializable;

public class RegionContinentePK
  implements Serializable
{
  public long idRegionContinente;

  public RegionContinentePK()
  {
  }

  public RegionContinentePK(long idRegionContinente)
  {
    this.idRegionContinente = idRegionContinente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RegionContinentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RegionContinentePK thatPK)
  {
    return 
      this.idRegionContinente == thatPK.idRegionContinente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRegionContinente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRegionContinente);
  }
}