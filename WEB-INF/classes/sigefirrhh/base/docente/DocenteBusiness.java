package sigefirrhh.base.docente;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class DocenteBusiness extends AbstractBusiness
  implements Serializable
{
  private AsignaturaBeanBusiness asignaturaBeanBusiness = new AsignaturaBeanBusiness();

  private AreaGeograficaBeanBusiness areaGeograficaBeanBusiness = new AreaGeograficaBeanBusiness();

  private CategoriaDocenteBeanBusiness categoriaDocenteBeanBusiness = new CategoriaDocenteBeanBusiness();

  private DedicacionDocenteBeanBusiness dedicacionDocenteBeanBusiness = new DedicacionDocenteBeanBusiness();

  private SeguridadDocenteBeanBusiness seguridadDocenteBeanBusiness = new SeguridadDocenteBeanBusiness();

  private GradoDocenteBeanBusiness gradoDocenteBeanBusiness = new GradoDocenteBeanBusiness();

  private JerarquiaDocenteBeanBusiness jerarquiaDocenteBeanBusiness = new JerarquiaDocenteBeanBusiness();

  private NivelDocenteBeanBusiness nivelDocenteBeanBusiness = new NivelDocenteBeanBusiness();

  private TurnoDocenteBeanBusiness turnoDocenteBeanBusiness = new TurnoDocenteBeanBusiness();

  private GradoNivelDocenteBeanBusiness gradoNivelDocenteBeanBusiness = new GradoNivelDocenteBeanBusiness();

  private JerarquiaCategoriaDocenteBeanBusiness jerarquiaCategoriaDocenteBeanBusiness = new JerarquiaCategoriaDocenteBeanBusiness();

  private CausaDocenteBeanBusiness causaDocenteBeanBusiness = new CausaDocenteBeanBusiness();

  public void addAsignatura(Asignatura asignatura)
    throws Exception
  {
    this.asignaturaBeanBusiness.addAsignatura(asignatura);
  }

  public void updateAsignatura(Asignatura asignatura) throws Exception {
    this.asignaturaBeanBusiness.updateAsignatura(asignatura);
  }

  public void deleteAsignatura(Asignatura asignatura) throws Exception {
    this.asignaturaBeanBusiness.deleteAsignatura(asignatura);
  }

  public Asignatura findAsignaturaById(long asignaturaId) throws Exception {
    return this.asignaturaBeanBusiness.findAsignaturaById(asignaturaId);
  }

  public Collection findAllAsignatura() throws Exception {
    return this.asignaturaBeanBusiness.findAsignaturaAll();
  }

  public Collection findAsignaturaByCodAsignatura(String codAsignatura)
    throws Exception
  {
    return this.asignaturaBeanBusiness.findByCodAsignatura(codAsignatura);
  }

  public Collection findAsignaturaByNombre(String nombre)
    throws Exception
  {
    return this.asignaturaBeanBusiness.findByNombre(nombre);
  }

  public void addAreaGeografica(AreaGeografica areaGeografica)
    throws Exception
  {
    this.areaGeograficaBeanBusiness.addAreaGeografica(areaGeografica);
  }

  public void updateAreaGeografica(AreaGeografica areaGeografica) throws Exception {
    this.areaGeograficaBeanBusiness.updateAreaGeografica(areaGeografica);
  }

  public void deleteAreaGeografica(AreaGeografica areaGeografica) throws Exception {
    this.areaGeograficaBeanBusiness.deleteAreaGeografica(areaGeografica);
  }

  public AreaGeografica findAreaGeograficaById(long areaGeograficaId) throws Exception {
    return this.areaGeograficaBeanBusiness.findAreaGeograficaById(areaGeograficaId);
  }

  public Collection findAllAreaGeografica() throws Exception {
    return this.areaGeograficaBeanBusiness.findAreaGeograficaAll();
  }

  public Collection findAreaGeograficaByCodAreaGeografica(String codAreaGeografica)
    throws Exception
  {
    return this.areaGeograficaBeanBusiness.findByCodAreaGeografica(codAreaGeografica);
  }

  public Collection findAreaGeograficaByNombre(String nombre)
    throws Exception
  {
    return this.areaGeograficaBeanBusiness.findByNombre(nombre);
  }

  public void addCategoriaDocente(CategoriaDocente categoriaDocente)
    throws Exception
  {
    this.categoriaDocenteBeanBusiness.addCategoriaDocente(categoriaDocente);
  }

  public void updateCategoriaDocente(CategoriaDocente categoriaDocente) throws Exception {
    this.categoriaDocenteBeanBusiness.updateCategoriaDocente(categoriaDocente);
  }

  public void deleteCategoriaDocente(CategoriaDocente categoriaDocente) throws Exception {
    this.categoriaDocenteBeanBusiness.deleteCategoriaDocente(categoriaDocente);
  }

  public CategoriaDocente findCategoriaDocenteById(long categoriaDocenteId) throws Exception {
    return this.categoriaDocenteBeanBusiness.findCategoriaDocenteById(categoriaDocenteId);
  }

  public Collection findAllCategoriaDocente() throws Exception {
    return this.categoriaDocenteBeanBusiness.findCategoriaDocenteAll();
  }

  public Collection findCategoriaDocenteByDigitoCategoria(String digitoCategoria)
    throws Exception
  {
    return this.categoriaDocenteBeanBusiness.findByDigitoCategoria(digitoCategoria);
  }

  public Collection findCategoriaDocenteByNombre(String nombre)
    throws Exception
  {
    return this.categoriaDocenteBeanBusiness.findByNombre(nombre);
  }

  public void addDedicacionDocente(DedicacionDocente dedicacionDocente)
    throws Exception
  {
    this.dedicacionDocenteBeanBusiness.addDedicacionDocente(dedicacionDocente);
  }

  public void updateDedicacionDocente(DedicacionDocente dedicacionDocente) throws Exception {
    this.dedicacionDocenteBeanBusiness.updateDedicacionDocente(dedicacionDocente);
  }

  public void deleteDedicacionDocente(DedicacionDocente dedicacionDocente) throws Exception {
    this.dedicacionDocenteBeanBusiness.deleteDedicacionDocente(dedicacionDocente);
  }

  public DedicacionDocente findDedicacionDocenteById(long dedicacionDocenteId) throws Exception {
    return this.dedicacionDocenteBeanBusiness.findDedicacionDocenteById(dedicacionDocenteId);
  }

  public Collection findAllDedicacionDocente() throws Exception {
    return this.dedicacionDocenteBeanBusiness.findDedicacionDocenteAll();
  }

  public Collection findDedicacionDocenteByDigitoDedicacion(String digitoDedicacion)
    throws Exception
  {
    return this.dedicacionDocenteBeanBusiness.findByDigitoDedicacion(digitoDedicacion);
  }

  public Collection findDedicacionDocenteByNombre(String nombre)
    throws Exception
  {
    return this.dedicacionDocenteBeanBusiness.findByNombre(nombre);
  }

  public void addSeguridadDocente(SeguridadDocente seguridadDocente)
    throws Exception
  {
    this.seguridadDocenteBeanBusiness.addSeguridadDocente(seguridadDocente);
  }

  public void updateSeguridadDocente(SeguridadDocente seguridadDocente) throws Exception {
    this.seguridadDocenteBeanBusiness.updateSeguridadDocente(seguridadDocente);
  }

  public void deleteSeguridadDocente(SeguridadDocente seguridadDocente) throws Exception {
    this.seguridadDocenteBeanBusiness.deleteSeguridadDocente(seguridadDocente);
  }

  public SeguridadDocente findSeguridadDocenteById(long seguridadDocenteId) throws Exception {
    return this.seguridadDocenteBeanBusiness.findSeguridadDocenteById(seguridadDocenteId);
  }

  public Collection findAllSeguridadDocente() throws Exception {
    return this.seguridadDocenteBeanBusiness.findSeguridadDocenteAll();
  }

  public Collection findSeguridadDocenteByAnio(int anio)
    throws Exception
  {
    return this.seguridadDocenteBeanBusiness.findByAnio(anio);
  }

  public Collection findSeguridadDocenteByCerrado(String cerrado)
    throws Exception
  {
    return this.seguridadDocenteBeanBusiness.findByCerrado(cerrado);
  }

  public Collection findSeguridadDocenteByFechaCierre(Date fechaCierre)
    throws Exception
  {
    return this.seguridadDocenteBeanBusiness.findByFechaCierre(fechaCierre);
  }

  public void addGradoDocente(GradoDocente gradoDocente)
    throws Exception
  {
    this.gradoDocenteBeanBusiness.addGradoDocente(gradoDocente);
  }

  public void updateGradoDocente(GradoDocente gradoDocente) throws Exception {
    this.gradoDocenteBeanBusiness.updateGradoDocente(gradoDocente);
  }

  public void deleteGradoDocente(GradoDocente gradoDocente) throws Exception {
    this.gradoDocenteBeanBusiness.deleteGradoDocente(gradoDocente);
  }

  public GradoDocente findGradoDocenteById(long gradoDocenteId) throws Exception {
    return this.gradoDocenteBeanBusiness.findGradoDocenteById(gradoDocenteId);
  }

  public Collection findAllGradoDocente() throws Exception {
    return this.gradoDocenteBeanBusiness.findGradoDocenteAll();
  }

  public Collection findGradoDocenteByDigitoGrado(String digitoGrado)
    throws Exception
  {
    return this.gradoDocenteBeanBusiness.findByDigitoGrado(digitoGrado);
  }

  public Collection findGradoDocenteByNombre(String nombre)
    throws Exception
  {
    return this.gradoDocenteBeanBusiness.findByNombre(nombre);
  }

  public void addJerarquiaDocente(JerarquiaDocente jerarquiaDocente)
    throws Exception
  {
    this.jerarquiaDocenteBeanBusiness.addJerarquiaDocente(jerarquiaDocente);
  }

  public void updateJerarquiaDocente(JerarquiaDocente jerarquiaDocente) throws Exception {
    this.jerarquiaDocenteBeanBusiness.updateJerarquiaDocente(jerarquiaDocente);
  }

  public void deleteJerarquiaDocente(JerarquiaDocente jerarquiaDocente) throws Exception {
    this.jerarquiaDocenteBeanBusiness.deleteJerarquiaDocente(jerarquiaDocente);
  }

  public JerarquiaDocente findJerarquiaDocenteById(long jerarquiaDocenteId) throws Exception {
    return this.jerarquiaDocenteBeanBusiness.findJerarquiaDocenteById(jerarquiaDocenteId);
  }

  public Collection findAllJerarquiaDocente() throws Exception {
    return this.jerarquiaDocenteBeanBusiness.findJerarquiaDocenteAll();
  }

  public Collection findJerarquiaDocenteByDigitoJerarquia(String digitoJerarquia)
    throws Exception
  {
    return this.jerarquiaDocenteBeanBusiness.findByDigitoJerarquia(digitoJerarquia);
  }

  public Collection findJerarquiaDocenteByNombre(String nombre)
    throws Exception
  {
    return this.jerarquiaDocenteBeanBusiness.findByNombre(nombre);
  }

  public void addNivelDocente(NivelDocente nivelDocente)
    throws Exception
  {
    this.nivelDocenteBeanBusiness.addNivelDocente(nivelDocente);
  }

  public void updateNivelDocente(NivelDocente nivelDocente) throws Exception {
    this.nivelDocenteBeanBusiness.updateNivelDocente(nivelDocente);
  }

  public void deleteNivelDocente(NivelDocente nivelDocente) throws Exception {
    this.nivelDocenteBeanBusiness.deleteNivelDocente(nivelDocente);
  }

  public NivelDocente findNivelDocenteById(long nivelDocenteId) throws Exception {
    return this.nivelDocenteBeanBusiness.findNivelDocenteById(nivelDocenteId);
  }

  public Collection findAllNivelDocente() throws Exception {
    return this.nivelDocenteBeanBusiness.findNivelDocenteAll();
  }

  public Collection findNivelDocenteByDigitoNivel(String digitoNivel)
    throws Exception
  {
    return this.nivelDocenteBeanBusiness.findByDigitoNivel(digitoNivel);
  }

  public Collection findNivelDocenteByNombre(String nombre)
    throws Exception
  {
    return this.nivelDocenteBeanBusiness.findByNombre(nombre);
  }

  public void addTurnoDocente(TurnoDocente turnoDocente)
    throws Exception
  {
    this.turnoDocenteBeanBusiness.addTurnoDocente(turnoDocente);
  }

  public void updateTurnoDocente(TurnoDocente turnoDocente) throws Exception {
    this.turnoDocenteBeanBusiness.updateTurnoDocente(turnoDocente);
  }

  public void deleteTurnoDocente(TurnoDocente turnoDocente) throws Exception {
    this.turnoDocenteBeanBusiness.deleteTurnoDocente(turnoDocente);
  }

  public TurnoDocente findTurnoDocenteById(long turnoDocenteId) throws Exception {
    return this.turnoDocenteBeanBusiness.findTurnoDocenteById(turnoDocenteId);
  }

  public Collection findAllTurnoDocente() throws Exception {
    return this.turnoDocenteBeanBusiness.findTurnoDocenteAll();
  }

  public Collection findTurnoDocenteByDigitoTurno(String digitoTurno)
    throws Exception
  {
    return this.turnoDocenteBeanBusiness.findByDigitoTurno(digitoTurno);
  }

  public Collection findTurnoDocenteByNombre(String nombre)
    throws Exception
  {
    return this.turnoDocenteBeanBusiness.findByNombre(nombre);
  }

  public void addGradoNivelDocente(GradoNivelDocente gradoNivelDocente)
    throws Exception
  {
    this.gradoNivelDocenteBeanBusiness.addGradoNivelDocente(gradoNivelDocente);
  }

  public void updateGradoNivelDocente(GradoNivelDocente gradoNivelDocente) throws Exception {
    this.gradoNivelDocenteBeanBusiness.updateGradoNivelDocente(gradoNivelDocente);
  }

  public void deleteGradoNivelDocente(GradoNivelDocente gradoNivelDocente) throws Exception {
    this.gradoNivelDocenteBeanBusiness.deleteGradoNivelDocente(gradoNivelDocente);
  }

  public GradoNivelDocente findGradoNivelDocenteById(long gradoNivelDocenteId) throws Exception {
    return this.gradoNivelDocenteBeanBusiness.findGradoNivelDocenteById(gradoNivelDocenteId);
  }

  public Collection findAllGradoNivelDocente() throws Exception {
    return this.gradoNivelDocenteBeanBusiness.findGradoNivelDocenteAll();
  }

  public Collection findGradoNivelDocenteByGradoDocente(long idGradoDocente)
    throws Exception
  {
    return this.gradoNivelDocenteBeanBusiness.findByGradoDocente(idGradoDocente);
  }

  public void addJerarquiaCategoriaDocente(JerarquiaCategoriaDocente jerarquiaCategoriaDocente)
    throws Exception
  {
    this.jerarquiaCategoriaDocenteBeanBusiness.addJerarquiaCategoriaDocente(jerarquiaCategoriaDocente);
  }

  public void updateJerarquiaCategoriaDocente(JerarquiaCategoriaDocente jerarquiaCategoriaDocente) throws Exception {
    this.jerarquiaCategoriaDocenteBeanBusiness.updateJerarquiaCategoriaDocente(jerarquiaCategoriaDocente);
  }

  public void deleteJerarquiaCategoriaDocente(JerarquiaCategoriaDocente jerarquiaCategoriaDocente) throws Exception {
    this.jerarquiaCategoriaDocenteBeanBusiness.deleteJerarquiaCategoriaDocente(jerarquiaCategoriaDocente);
  }

  public JerarquiaCategoriaDocente findJerarquiaCategoriaDocenteById(long jerarquiaCategoriaDocenteId) throws Exception {
    return this.jerarquiaCategoriaDocenteBeanBusiness.findJerarquiaCategoriaDocenteById(jerarquiaCategoriaDocenteId);
  }

  public Collection findAllJerarquiaCategoriaDocente() throws Exception {
    return this.jerarquiaCategoriaDocenteBeanBusiness.findJerarquiaCategoriaDocenteAll();
  }

  public Collection findJerarquiaCategoriaDocenteByJerarquiaDocente(long idJerarquiaDocente)
    throws Exception
  {
    return this.jerarquiaCategoriaDocenteBeanBusiness.findByJerarquiaDocente(idJerarquiaDocente);
  }

  public void addCausaDocente(CausaDocente causaDocente)
    throws Exception
  {
    this.causaDocenteBeanBusiness.addCausaDocente(causaDocente);
  }

  public void updateCausaDocente(CausaDocente causaDocente) throws Exception {
    this.causaDocenteBeanBusiness.updateCausaDocente(causaDocente);
  }

  public void deleteCausaDocente(CausaDocente causaDocente) throws Exception {
    this.causaDocenteBeanBusiness.deleteCausaDocente(causaDocente);
  }

  public CausaDocente findCausaDocenteById(long causaDocenteId) throws Exception {
    return this.causaDocenteBeanBusiness.findCausaDocenteById(causaDocenteId);
  }

  public Collection findAllCausaDocente() throws Exception {
    return this.causaDocenteBeanBusiness.findCausaDocenteAll();
  }

  public Collection findCausaDocenteByCodCausaDocente(String codCausaDocente)
    throws Exception
  {
    return this.causaDocenteBeanBusiness.findByCodCausaDocente(codCausaDocente);
  }

  public Collection findCausaDocenteByNombre(String nombre)
    throws Exception
  {
    return this.causaDocenteBeanBusiness.findByNombre(nombre);
  }

  public Collection findCausaDocenteByTipo(String tipo)
    throws Exception
  {
    return this.causaDocenteBeanBusiness.findByTipo(tipo);
  }
}