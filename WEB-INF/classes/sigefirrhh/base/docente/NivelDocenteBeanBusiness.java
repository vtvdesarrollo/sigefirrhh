package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class NivelDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addNivelDocente(NivelDocente nivelDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    NivelDocente nivelDocenteNew = 
      (NivelDocente)BeanUtils.cloneBean(
      nivelDocente);

    pm.makePersistent(nivelDocenteNew);
  }

  public void updateNivelDocente(NivelDocente nivelDocente) throws Exception
  {
    NivelDocente nivelDocenteModify = 
      findNivelDocenteById(nivelDocente.getIdNivelDocente());

    BeanUtils.copyProperties(nivelDocenteModify, nivelDocente);
  }

  public void deleteNivelDocente(NivelDocente nivelDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    NivelDocente nivelDocenteDelete = 
      findNivelDocenteById(nivelDocente.getIdNivelDocente());
    pm.deletePersistent(nivelDocenteDelete);
  }

  public NivelDocente findNivelDocenteById(long idNivelDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idNivelDocente == pIdNivelDocente";
    Query query = pm.newQuery(NivelDocente.class, filter);

    query.declareParameters("long pIdNivelDocente");

    parameters.put("pIdNivelDocente", new Long(idNivelDocente));

    Collection colNivelDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colNivelDocente.iterator();
    return (NivelDocente)iterator.next();
  }

  public Collection findNivelDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent nivelDocenteExtent = pm.getExtent(
      NivelDocente.class, true);
    Query query = pm.newQuery(nivelDocenteExtent);
    query.setOrdering("digitoNivel ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByDigitoNivel(String digitoNivel)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "digitoNivel == pDigitoNivel";

    Query query = pm.newQuery(NivelDocente.class, filter);

    query.declareParameters("java.lang.String pDigitoNivel");
    HashMap parameters = new HashMap();

    parameters.put("pDigitoNivel", new String(digitoNivel));

    query.setOrdering("digitoNivel ascending");

    Collection colNivelDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNivelDocente);

    return colNivelDocente;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(NivelDocente.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("digitoNivel ascending");

    Collection colNivelDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNivelDocente);

    return colNivelDocente;
  }
}