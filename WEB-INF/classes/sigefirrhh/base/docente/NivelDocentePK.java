package sigefirrhh.base.docente;

import java.io.Serializable;

public class NivelDocentePK
  implements Serializable
{
  public long idNivelDocente;

  public NivelDocentePK()
  {
  }

  public NivelDocentePK(long idNivelDocente)
  {
    this.idNivelDocente = idNivelDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((NivelDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(NivelDocentePK thatPK)
  {
    return 
      this.idNivelDocente == thatPK.idNivelDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idNivelDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idNivelDocente);
  }
}