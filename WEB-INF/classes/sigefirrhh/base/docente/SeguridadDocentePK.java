package sigefirrhh.base.docente;

import java.io.Serializable;

public class SeguridadDocentePK
  implements Serializable
{
  public long idSeguridadDocente;

  public SeguridadDocentePK()
  {
  }

  public SeguridadDocentePK(long idSeguridadDocente)
  {
    this.idSeguridadDocente = idSeguridadDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SeguridadDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SeguridadDocentePK thatPK)
  {
    return 
      this.idSeguridadDocente == thatPK.idSeguridadDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSeguridadDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSeguridadDocente);
  }
}