package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CausaDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCausaDocente(CausaDocente causaDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CausaDocente causaDocenteNew = 
      (CausaDocente)BeanUtils.cloneBean(
      causaDocente);

    pm.makePersistent(causaDocenteNew);
  }

  public void updateCausaDocente(CausaDocente causaDocente) throws Exception
  {
    CausaDocente causaDocenteModify = 
      findCausaDocenteById(causaDocente.getIdCausaDocente());

    BeanUtils.copyProperties(causaDocenteModify, causaDocente);
  }

  public void deleteCausaDocente(CausaDocente causaDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CausaDocente causaDocenteDelete = 
      findCausaDocenteById(causaDocente.getIdCausaDocente());
    pm.deletePersistent(causaDocenteDelete);
  }

  public CausaDocente findCausaDocenteById(long idCausaDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCausaDocente == pIdCausaDocente";
    Query query = pm.newQuery(CausaDocente.class, filter);

    query.declareParameters("long pIdCausaDocente");

    parameters.put("pIdCausaDocente", new Long(idCausaDocente));

    Collection colCausaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCausaDocente.iterator();
    return (CausaDocente)iterator.next();
  }

  public Collection findCausaDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent causaDocenteExtent = pm.getExtent(
      CausaDocente.class, true);
    Query query = pm.newQuery(causaDocenteExtent);
    query.setOrdering("codCausaDocente ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodCausaDocente(String codCausaDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCausaDocente == pCodCausaDocente";

    Query query = pm.newQuery(CausaDocente.class, filter);

    query.declareParameters("java.lang.String pCodCausaDocente");
    HashMap parameters = new HashMap();

    parameters.put("pCodCausaDocente", new String(codCausaDocente));

    query.setOrdering("codCausaDocente ascending");

    Collection colCausaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCausaDocente);

    return colCausaDocente;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(CausaDocente.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codCausaDocente ascending");

    Collection colCausaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCausaDocente);

    return colCausaDocente;
  }

  public Collection findByTipo(String tipo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipo == pTipo";

    Query query = pm.newQuery(CausaDocente.class, filter);

    query.declareParameters("java.lang.String pTipo");
    HashMap parameters = new HashMap();

    parameters.put("pTipo", new String(tipo));

    query.setOrdering("codCausaDocente ascending");

    Collection colCausaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCausaDocente);

    return colCausaDocente;
  }
}