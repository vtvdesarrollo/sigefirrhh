package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TurnoDependencia
  implements Serializable, PersistenceCapable
{
  private long idTurnoDependencia;
  private int codTurnoDependencia;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTurnoDependencia", "idTurnoDependencia", "nombre" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getCodTurnoDependencia()
  {
    return jdoGetcodTurnoDependencia(this);
  }

  public long getIdTurnoDependencia()
  {
    return jdoGetidTurnoDependencia(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodTurnoDependencia(int i)
  {
    jdoSetcodTurnoDependencia(this, i);
  }

  public void setIdTurnoDependencia(long l)
  {
    jdoSetidTurnoDependencia(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.TurnoDependencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TurnoDependencia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TurnoDependencia localTurnoDependencia = new TurnoDependencia();
    localTurnoDependencia.jdoFlags = 1;
    localTurnoDependencia.jdoStateManager = paramStateManager;
    return localTurnoDependencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TurnoDependencia localTurnoDependencia = new TurnoDependencia();
    localTurnoDependencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTurnoDependencia.jdoFlags = 1;
    localTurnoDependencia.jdoStateManager = paramStateManager;
    return localTurnoDependencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codTurnoDependencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTurnoDependencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTurnoDependencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTurnoDependencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TurnoDependencia paramTurnoDependencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTurnoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.codTurnoDependencia = paramTurnoDependencia.codTurnoDependencia;
      return;
    case 1:
      if (paramTurnoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.idTurnoDependencia = paramTurnoDependencia.idTurnoDependencia;
      return;
    case 2:
      if (paramTurnoDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramTurnoDependencia.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TurnoDependencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TurnoDependencia localTurnoDependencia = (TurnoDependencia)paramObject;
    if (localTurnoDependencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTurnoDependencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TurnoDependenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TurnoDependenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TurnoDependenciaPK))
      throw new IllegalArgumentException("arg1");
    TurnoDependenciaPK localTurnoDependenciaPK = (TurnoDependenciaPK)paramObject;
    localTurnoDependenciaPK.idTurnoDependencia = this.idTurnoDependencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TurnoDependenciaPK))
      throw new IllegalArgumentException("arg1");
    TurnoDependenciaPK localTurnoDependenciaPK = (TurnoDependenciaPK)paramObject;
    this.idTurnoDependencia = localTurnoDependenciaPK.idTurnoDependencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TurnoDependenciaPK))
      throw new IllegalArgumentException("arg2");
    TurnoDependenciaPK localTurnoDependenciaPK = (TurnoDependenciaPK)paramObject;
    localTurnoDependenciaPK.idTurnoDependencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TurnoDependenciaPK))
      throw new IllegalArgumentException("arg2");
    TurnoDependenciaPK localTurnoDependenciaPK = (TurnoDependenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localTurnoDependenciaPK.idTurnoDependencia);
  }

  private static final int jdoGetcodTurnoDependencia(TurnoDependencia paramTurnoDependencia)
  {
    if (paramTurnoDependencia.jdoFlags <= 0)
      return paramTurnoDependencia.codTurnoDependencia;
    StateManager localStateManager = paramTurnoDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramTurnoDependencia.codTurnoDependencia;
    if (localStateManager.isLoaded(paramTurnoDependencia, jdoInheritedFieldCount + 0))
      return paramTurnoDependencia.codTurnoDependencia;
    return localStateManager.getIntField(paramTurnoDependencia, jdoInheritedFieldCount + 0, paramTurnoDependencia.codTurnoDependencia);
  }

  private static final void jdoSetcodTurnoDependencia(TurnoDependencia paramTurnoDependencia, int paramInt)
  {
    if (paramTurnoDependencia.jdoFlags == 0)
    {
      paramTurnoDependencia.codTurnoDependencia = paramInt;
      return;
    }
    StateManager localStateManager = paramTurnoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurnoDependencia.codTurnoDependencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramTurnoDependencia, jdoInheritedFieldCount + 0, paramTurnoDependencia.codTurnoDependencia, paramInt);
  }

  private static final long jdoGetidTurnoDependencia(TurnoDependencia paramTurnoDependencia)
  {
    return paramTurnoDependencia.idTurnoDependencia;
  }

  private static final void jdoSetidTurnoDependencia(TurnoDependencia paramTurnoDependencia, long paramLong)
  {
    StateManager localStateManager = paramTurnoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurnoDependencia.idTurnoDependencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramTurnoDependencia, jdoInheritedFieldCount + 1, paramTurnoDependencia.idTurnoDependencia, paramLong);
  }

  private static final String jdoGetnombre(TurnoDependencia paramTurnoDependencia)
  {
    if (paramTurnoDependencia.jdoFlags <= 0)
      return paramTurnoDependencia.nombre;
    StateManager localStateManager = paramTurnoDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramTurnoDependencia.nombre;
    if (localStateManager.isLoaded(paramTurnoDependencia, jdoInheritedFieldCount + 2))
      return paramTurnoDependencia.nombre;
    return localStateManager.getStringField(paramTurnoDependencia, jdoInheritedFieldCount + 2, paramTurnoDependencia.nombre);
  }

  private static final void jdoSetnombre(TurnoDependencia paramTurnoDependencia, String paramString)
  {
    if (paramTurnoDependencia.jdoFlags == 0)
    {
      paramTurnoDependencia.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramTurnoDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurnoDependencia.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTurnoDependencia, jdoInheritedFieldCount + 2, paramTurnoDependencia.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}