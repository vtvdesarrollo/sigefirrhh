package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PlantelActual
  implements Serializable, PersistenceCapable
{
  private long idPlantelActual;
  private String codPlantelActual;
  private String nombre;
  private String direccion;
  private NivelPlantelActual nivelPlantelActual;
  private ZonaEducativa zonaEducativa;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codPlantelActual", "direccion", "idPlantelActual", "nivelPlantelActual", "nombre", "zonaEducativa" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.docente.NivelPlantelActual"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.docente.ZonaEducativa") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String getCodPlantelActual()
  {
    return jdoGetcodPlantelActual(this);
  }

  public String getDireccion()
  {
    return jdoGetdireccion(this);
  }

  public long getIdPlantelActual()
  {
    return jdoGetidPlantelActual(this);
  }

  public NivelPlantelActual getNivelPlantelActual()
  {
    return jdoGetnivelPlantelActual(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public ZonaEducativa getZonaEducativa()
  {
    return jdoGetzonaEducativa(this);
  }

  public void setCodPlantelActual(String string)
  {
    jdoSetcodPlantelActual(this, string);
  }

  public void setDireccion(String string)
  {
    jdoSetdireccion(this, string);
  }

  public void setIdPlantelActual(long l)
  {
    jdoSetidPlantelActual(this, l);
  }

  public void setNivelPlantelActual(NivelPlantelActual actual)
  {
    jdoSetnivelPlantelActual(this, actual);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setZonaEducativa(ZonaEducativa educativa)
  {
    jdoSetzonaEducativa(this, educativa);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.PlantelActual"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PlantelActual());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PlantelActual localPlantelActual = new PlantelActual();
    localPlantelActual.jdoFlags = 1;
    localPlantelActual.jdoStateManager = paramStateManager;
    return localPlantelActual;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PlantelActual localPlantelActual = new PlantelActual();
    localPlantelActual.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPlantelActual.jdoFlags = 1;
    localPlantelActual.jdoStateManager = paramStateManager;
    return localPlantelActual;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codPlantelActual);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.direccion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPlantelActual);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nivelPlantelActual);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.zonaEducativa);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codPlantelActual = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.direccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPlantelActual = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelPlantelActual = ((NivelPlantelActual)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.zonaEducativa = ((ZonaEducativa)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PlantelActual paramPlantelActual, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPlantelActual == null)
        throw new IllegalArgumentException("arg1");
      this.codPlantelActual = paramPlantelActual.codPlantelActual;
      return;
    case 1:
      if (paramPlantelActual == null)
        throw new IllegalArgumentException("arg1");
      this.direccion = paramPlantelActual.direccion;
      return;
    case 2:
      if (paramPlantelActual == null)
        throw new IllegalArgumentException("arg1");
      this.idPlantelActual = paramPlantelActual.idPlantelActual;
      return;
    case 3:
      if (paramPlantelActual == null)
        throw new IllegalArgumentException("arg1");
      this.nivelPlantelActual = paramPlantelActual.nivelPlantelActual;
      return;
    case 4:
      if (paramPlantelActual == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramPlantelActual.nombre;
      return;
    case 5:
      if (paramPlantelActual == null)
        throw new IllegalArgumentException("arg1");
      this.zonaEducativa = paramPlantelActual.zonaEducativa;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PlantelActual))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PlantelActual localPlantelActual = (PlantelActual)paramObject;
    if (localPlantelActual.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPlantelActual, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PlantelActualPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PlantelActualPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlantelActualPK))
      throw new IllegalArgumentException("arg1");
    PlantelActualPK localPlantelActualPK = (PlantelActualPK)paramObject;
    localPlantelActualPK.idPlantelActual = this.idPlantelActual;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlantelActualPK))
      throw new IllegalArgumentException("arg1");
    PlantelActualPK localPlantelActualPK = (PlantelActualPK)paramObject;
    this.idPlantelActual = localPlantelActualPK.idPlantelActual;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlantelActualPK))
      throw new IllegalArgumentException("arg2");
    PlantelActualPK localPlantelActualPK = (PlantelActualPK)paramObject;
    localPlantelActualPK.idPlantelActual = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlantelActualPK))
      throw new IllegalArgumentException("arg2");
    PlantelActualPK localPlantelActualPK = (PlantelActualPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localPlantelActualPK.idPlantelActual);
  }

  private static final String jdoGetcodPlantelActual(PlantelActual paramPlantelActual)
  {
    if (paramPlantelActual.jdoFlags <= 0)
      return paramPlantelActual.codPlantelActual;
    StateManager localStateManager = paramPlantelActual.jdoStateManager;
    if (localStateManager == null)
      return paramPlantelActual.codPlantelActual;
    if (localStateManager.isLoaded(paramPlantelActual, jdoInheritedFieldCount + 0))
      return paramPlantelActual.codPlantelActual;
    return localStateManager.getStringField(paramPlantelActual, jdoInheritedFieldCount + 0, paramPlantelActual.codPlantelActual);
  }

  private static final void jdoSetcodPlantelActual(PlantelActual paramPlantelActual, String paramString)
  {
    if (paramPlantelActual.jdoFlags == 0)
    {
      paramPlantelActual.codPlantelActual = paramString;
      return;
    }
    StateManager localStateManager = paramPlantelActual.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlantelActual.codPlantelActual = paramString;
      return;
    }
    localStateManager.setStringField(paramPlantelActual, jdoInheritedFieldCount + 0, paramPlantelActual.codPlantelActual, paramString);
  }

  private static final String jdoGetdireccion(PlantelActual paramPlantelActual)
  {
    if (paramPlantelActual.jdoFlags <= 0)
      return paramPlantelActual.direccion;
    StateManager localStateManager = paramPlantelActual.jdoStateManager;
    if (localStateManager == null)
      return paramPlantelActual.direccion;
    if (localStateManager.isLoaded(paramPlantelActual, jdoInheritedFieldCount + 1))
      return paramPlantelActual.direccion;
    return localStateManager.getStringField(paramPlantelActual, jdoInheritedFieldCount + 1, paramPlantelActual.direccion);
  }

  private static final void jdoSetdireccion(PlantelActual paramPlantelActual, String paramString)
  {
    if (paramPlantelActual.jdoFlags == 0)
    {
      paramPlantelActual.direccion = paramString;
      return;
    }
    StateManager localStateManager = paramPlantelActual.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlantelActual.direccion = paramString;
      return;
    }
    localStateManager.setStringField(paramPlantelActual, jdoInheritedFieldCount + 1, paramPlantelActual.direccion, paramString);
  }

  private static final long jdoGetidPlantelActual(PlantelActual paramPlantelActual)
  {
    return paramPlantelActual.idPlantelActual;
  }

  private static final void jdoSetidPlantelActual(PlantelActual paramPlantelActual, long paramLong)
  {
    StateManager localStateManager = paramPlantelActual.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlantelActual.idPlantelActual = paramLong;
      return;
    }
    localStateManager.setLongField(paramPlantelActual, jdoInheritedFieldCount + 2, paramPlantelActual.idPlantelActual, paramLong);
  }

  private static final NivelPlantelActual jdoGetnivelPlantelActual(PlantelActual paramPlantelActual)
  {
    StateManager localStateManager = paramPlantelActual.jdoStateManager;
    if (localStateManager == null)
      return paramPlantelActual.nivelPlantelActual;
    if (localStateManager.isLoaded(paramPlantelActual, jdoInheritedFieldCount + 3))
      return paramPlantelActual.nivelPlantelActual;
    return (NivelPlantelActual)localStateManager.getObjectField(paramPlantelActual, jdoInheritedFieldCount + 3, paramPlantelActual.nivelPlantelActual);
  }

  private static final void jdoSetnivelPlantelActual(PlantelActual paramPlantelActual, NivelPlantelActual paramNivelPlantelActual)
  {
    StateManager localStateManager = paramPlantelActual.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlantelActual.nivelPlantelActual = paramNivelPlantelActual;
      return;
    }
    localStateManager.setObjectField(paramPlantelActual, jdoInheritedFieldCount + 3, paramPlantelActual.nivelPlantelActual, paramNivelPlantelActual);
  }

  private static final String jdoGetnombre(PlantelActual paramPlantelActual)
  {
    if (paramPlantelActual.jdoFlags <= 0)
      return paramPlantelActual.nombre;
    StateManager localStateManager = paramPlantelActual.jdoStateManager;
    if (localStateManager == null)
      return paramPlantelActual.nombre;
    if (localStateManager.isLoaded(paramPlantelActual, jdoInheritedFieldCount + 4))
      return paramPlantelActual.nombre;
    return localStateManager.getStringField(paramPlantelActual, jdoInheritedFieldCount + 4, paramPlantelActual.nombre);
  }

  private static final void jdoSetnombre(PlantelActual paramPlantelActual, String paramString)
  {
    if (paramPlantelActual.jdoFlags == 0)
    {
      paramPlantelActual.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramPlantelActual.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlantelActual.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramPlantelActual, jdoInheritedFieldCount + 4, paramPlantelActual.nombre, paramString);
  }

  private static final ZonaEducativa jdoGetzonaEducativa(PlantelActual paramPlantelActual)
  {
    StateManager localStateManager = paramPlantelActual.jdoStateManager;
    if (localStateManager == null)
      return paramPlantelActual.zonaEducativa;
    if (localStateManager.isLoaded(paramPlantelActual, jdoInheritedFieldCount + 5))
      return paramPlantelActual.zonaEducativa;
    return (ZonaEducativa)localStateManager.getObjectField(paramPlantelActual, jdoInheritedFieldCount + 5, paramPlantelActual.zonaEducativa);
  }

  private static final void jdoSetzonaEducativa(PlantelActual paramPlantelActual, ZonaEducativa paramZonaEducativa)
  {
    StateManager localStateManager = paramPlantelActual.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlantelActual.zonaEducativa = paramZonaEducativa;
      return;
    }
    localStateManager.setObjectField(paramPlantelActual, jdoInheritedFieldCount + 5, paramPlantelActual.zonaEducativa, paramZonaEducativa);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}