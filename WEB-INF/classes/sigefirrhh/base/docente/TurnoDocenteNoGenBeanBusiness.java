package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class TurnoDocenteNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByTurnoAndCondicion(String turno, String condicion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "turno == pTurno && condicion == pCondicion";

    Query query = pm.newQuery(TurnoDocente.class, filter);

    query.declareParameters("String pTurno, String pCondicion");
    HashMap parameters = new HashMap();

    parameters.put("pTurno", new String(turno));
    parameters.put("pCondicion", new String(condicion));

    query.setOrdering("digitoTurno ascending");

    Collection colTurnoDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTurnoDocente);

    return colTurnoDocente;
  }
}