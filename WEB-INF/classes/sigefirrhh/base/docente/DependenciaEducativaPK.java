package sigefirrhh.base.docente;

import java.io.Serializable;

public class DependenciaEducativaPK
  implements Serializable
{
  public long idDependenciaEducativa;

  public DependenciaEducativaPK()
  {
  }

  public DependenciaEducativaPK(long idDependenciaEducativa)
  {
    this.idDependenciaEducativa = idDependenciaEducativa;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DependenciaEducativaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DependenciaEducativaPK thatPK)
  {
    return 
      this.idDependenciaEducativa == thatPK.idDependenciaEducativa;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDependenciaEducativa)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDependenciaEducativa);
  }
}