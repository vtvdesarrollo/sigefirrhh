package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class NivelDocente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idNivelDocente;
  private String digitoNivel;
  private String nombre;
  private String vigencia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "digitoNivel", "idNivelDocente", "nombre", "vigencia" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.NivelDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new NivelDocente());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public NivelDocente()
  {
    jdoSetvigencia(this, "S");
  }
  public String toString() {
    return jdoGetdigitoNivel(this) + " - " + 
      jdoGetnombre(this);
  }

  public String getDigitoNivel()
  {
    return jdoGetdigitoNivel(this);
  }

  public long getIdNivelDocente()
  {
    return jdoGetidNivelDocente(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setDigitoNivel(String string)
  {
    jdoSetdigitoNivel(this, string);
  }

  public void setIdNivelDocente(long l)
  {
    jdoSetidNivelDocente(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public String getVigencia() {
    return jdoGetvigencia(this);
  }
  public void setVigencia(String vigencia) {
    jdoSetvigencia(this, vigencia);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    NivelDocente localNivelDocente = new NivelDocente();
    localNivelDocente.jdoFlags = 1;
    localNivelDocente.jdoStateManager = paramStateManager;
    return localNivelDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    NivelDocente localNivelDocente = new NivelDocente();
    localNivelDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localNivelDocente.jdoFlags = 1;
    localNivelDocente.jdoStateManager = paramStateManager;
    return localNivelDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.digitoNivel);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idNivelDocente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.digitoNivel = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idNivelDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigencia = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(NivelDocente paramNivelDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramNivelDocente == null)
        throw new IllegalArgumentException("arg1");
      this.digitoNivel = paramNivelDocente.digitoNivel;
      return;
    case 1:
      if (paramNivelDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idNivelDocente = paramNivelDocente.idNivelDocente;
      return;
    case 2:
      if (paramNivelDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramNivelDocente.nombre;
      return;
    case 3:
      if (paramNivelDocente == null)
        throw new IllegalArgumentException("arg1");
      this.vigencia = paramNivelDocente.vigencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof NivelDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    NivelDocente localNivelDocente = (NivelDocente)paramObject;
    if (localNivelDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localNivelDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new NivelDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new NivelDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NivelDocentePK))
      throw new IllegalArgumentException("arg1");
    NivelDocentePK localNivelDocentePK = (NivelDocentePK)paramObject;
    localNivelDocentePK.idNivelDocente = this.idNivelDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NivelDocentePK))
      throw new IllegalArgumentException("arg1");
    NivelDocentePK localNivelDocentePK = (NivelDocentePK)paramObject;
    this.idNivelDocente = localNivelDocentePK.idNivelDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NivelDocentePK))
      throw new IllegalArgumentException("arg2");
    NivelDocentePK localNivelDocentePK = (NivelDocentePK)paramObject;
    localNivelDocentePK.idNivelDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NivelDocentePK))
      throw new IllegalArgumentException("arg2");
    NivelDocentePK localNivelDocentePK = (NivelDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localNivelDocentePK.idNivelDocente);
  }

  private static final String jdoGetdigitoNivel(NivelDocente paramNivelDocente)
  {
    if (paramNivelDocente.jdoFlags <= 0)
      return paramNivelDocente.digitoNivel;
    StateManager localStateManager = paramNivelDocente.jdoStateManager;
    if (localStateManager == null)
      return paramNivelDocente.digitoNivel;
    if (localStateManager.isLoaded(paramNivelDocente, jdoInheritedFieldCount + 0))
      return paramNivelDocente.digitoNivel;
    return localStateManager.getStringField(paramNivelDocente, jdoInheritedFieldCount + 0, paramNivelDocente.digitoNivel);
  }

  private static final void jdoSetdigitoNivel(NivelDocente paramNivelDocente, String paramString)
  {
    if (paramNivelDocente.jdoFlags == 0)
    {
      paramNivelDocente.digitoNivel = paramString;
      return;
    }
    StateManager localStateManager = paramNivelDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelDocente.digitoNivel = paramString;
      return;
    }
    localStateManager.setStringField(paramNivelDocente, jdoInheritedFieldCount + 0, paramNivelDocente.digitoNivel, paramString);
  }

  private static final long jdoGetidNivelDocente(NivelDocente paramNivelDocente)
  {
    return paramNivelDocente.idNivelDocente;
  }

  private static final void jdoSetidNivelDocente(NivelDocente paramNivelDocente, long paramLong)
  {
    StateManager localStateManager = paramNivelDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelDocente.idNivelDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramNivelDocente, jdoInheritedFieldCount + 1, paramNivelDocente.idNivelDocente, paramLong);
  }

  private static final String jdoGetnombre(NivelDocente paramNivelDocente)
  {
    if (paramNivelDocente.jdoFlags <= 0)
      return paramNivelDocente.nombre;
    StateManager localStateManager = paramNivelDocente.jdoStateManager;
    if (localStateManager == null)
      return paramNivelDocente.nombre;
    if (localStateManager.isLoaded(paramNivelDocente, jdoInheritedFieldCount + 2))
      return paramNivelDocente.nombre;
    return localStateManager.getStringField(paramNivelDocente, jdoInheritedFieldCount + 2, paramNivelDocente.nombre);
  }

  private static final void jdoSetnombre(NivelDocente paramNivelDocente, String paramString)
  {
    if (paramNivelDocente.jdoFlags == 0)
    {
      paramNivelDocente.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramNivelDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelDocente.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramNivelDocente, jdoInheritedFieldCount + 2, paramNivelDocente.nombre, paramString);
  }

  private static final String jdoGetvigencia(NivelDocente paramNivelDocente)
  {
    if (paramNivelDocente.jdoFlags <= 0)
      return paramNivelDocente.vigencia;
    StateManager localStateManager = paramNivelDocente.jdoStateManager;
    if (localStateManager == null)
      return paramNivelDocente.vigencia;
    if (localStateManager.isLoaded(paramNivelDocente, jdoInheritedFieldCount + 3))
      return paramNivelDocente.vigencia;
    return localStateManager.getStringField(paramNivelDocente, jdoInheritedFieldCount + 3, paramNivelDocente.vigencia);
  }

  private static final void jdoSetvigencia(NivelDocente paramNivelDocente, String paramString)
  {
    if (paramNivelDocente.jdoFlags == 0)
    {
      paramNivelDocente.vigencia = paramString;
      return;
    }
    StateManager localStateManager = paramNivelDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelDocente.vigencia = paramString;
      return;
    }
    localStateManager.setStringField(paramNivelDocente, jdoInheritedFieldCount + 3, paramNivelDocente.vigencia, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}