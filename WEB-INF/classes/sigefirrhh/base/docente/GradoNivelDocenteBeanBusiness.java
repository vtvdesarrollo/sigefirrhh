package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class GradoNivelDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addGradoNivelDocente(GradoNivelDocente gradoNivelDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    GradoNivelDocente gradoNivelDocenteNew = 
      (GradoNivelDocente)BeanUtils.cloneBean(
      gradoNivelDocente);

    GradoDocenteBeanBusiness gradoDocenteBeanBusiness = new GradoDocenteBeanBusiness();

    if (gradoNivelDocenteNew.getGradoDocente() != null) {
      gradoNivelDocenteNew.setGradoDocente(
        gradoDocenteBeanBusiness.findGradoDocenteById(
        gradoNivelDocenteNew.getGradoDocente().getIdGradoDocente()));
    }

    NivelDocenteBeanBusiness nivelDocenteBeanBusiness = new NivelDocenteBeanBusiness();

    if (gradoNivelDocenteNew.getNivelDocente() != null) {
      gradoNivelDocenteNew.setNivelDocente(
        nivelDocenteBeanBusiness.findNivelDocenteById(
        gradoNivelDocenteNew.getNivelDocente().getIdNivelDocente()));
    }
    pm.makePersistent(gradoNivelDocenteNew);
  }

  public void updateGradoNivelDocente(GradoNivelDocente gradoNivelDocente) throws Exception
  {
    GradoNivelDocente gradoNivelDocenteModify = 
      findGradoNivelDocenteById(gradoNivelDocente.getIdGradoNivelDocente());

    GradoDocenteBeanBusiness gradoDocenteBeanBusiness = new GradoDocenteBeanBusiness();

    if (gradoNivelDocente.getGradoDocente() != null) {
      gradoNivelDocente.setGradoDocente(
        gradoDocenteBeanBusiness.findGradoDocenteById(
        gradoNivelDocente.getGradoDocente().getIdGradoDocente()));
    }

    NivelDocenteBeanBusiness nivelDocenteBeanBusiness = new NivelDocenteBeanBusiness();

    if (gradoNivelDocente.getNivelDocente() != null) {
      gradoNivelDocente.setNivelDocente(
        nivelDocenteBeanBusiness.findNivelDocenteById(
        gradoNivelDocente.getNivelDocente().getIdNivelDocente()));
    }

    BeanUtils.copyProperties(gradoNivelDocenteModify, gradoNivelDocente);
  }

  public void deleteGradoNivelDocente(GradoNivelDocente gradoNivelDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    GradoNivelDocente gradoNivelDocenteDelete = 
      findGradoNivelDocenteById(gradoNivelDocente.getIdGradoNivelDocente());
    pm.deletePersistent(gradoNivelDocenteDelete);
  }

  public GradoNivelDocente findGradoNivelDocenteById(long idGradoNivelDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idGradoNivelDocente == pIdGradoNivelDocente";
    Query query = pm.newQuery(GradoNivelDocente.class, filter);

    query.declareParameters("long pIdGradoNivelDocente");

    parameters.put("pIdGradoNivelDocente", new Long(idGradoNivelDocente));

    Collection colGradoNivelDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colGradoNivelDocente.iterator();
    return (GradoNivelDocente)iterator.next();
  }

  public Collection findGradoNivelDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent gradoNivelDocenteExtent = pm.getExtent(
      GradoNivelDocente.class, true);
    Query query = pm.newQuery(gradoNivelDocenteExtent);
    query.setOrdering("gradoDocente.digitoGrado ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByGradoDocente(long idGradoDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "gradoDocente.idGradoDocente == pIdGradoDocente";

    Query query = pm.newQuery(GradoNivelDocente.class, filter);

    query.declareParameters("long pIdGradoDocente");
    HashMap parameters = new HashMap();

    parameters.put("pIdGradoDocente", new Long(idGradoDocente));

    query.setOrdering("gradoDocente.digitoGrado ascending");

    Collection colGradoNivelDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGradoNivelDocente);

    return colGradoNivelDocente;
  }
}