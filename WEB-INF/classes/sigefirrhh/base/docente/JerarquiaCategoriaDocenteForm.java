package sigefirrhh.base.docente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class JerarquiaCategoriaDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(JerarquiaCategoriaDocenteForm.class.getName());
  private JerarquiaCategoriaDocente jerarquiaCategoriaDocente;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DocenteFacade docenteFacade = new DocenteFacade();
  private boolean showJerarquiaCategoriaDocenteByJerarquiaDocente;
  private String findSelectJerarquiaDocente;
  private Collection findColJerarquiaDocente;
  private Collection colJerarquiaDocente;
  private Collection colCategoriaDocente;
  private String selectJerarquiaDocente;
  private String selectCategoriaDocente;
  private Object stateResultJerarquiaCategoriaDocenteByJerarquiaDocente = null;

  public String getFindSelectJerarquiaDocente()
  {
    return this.findSelectJerarquiaDocente;
  }
  public void setFindSelectJerarquiaDocente(String valJerarquiaDocente) {
    this.findSelectJerarquiaDocente = valJerarquiaDocente;
  }

  public Collection getFindColJerarquiaDocente() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColJerarquiaDocente.iterator();
    JerarquiaDocente jerarquiaDocente = null;
    while (iterator.hasNext()) {
      jerarquiaDocente = (JerarquiaDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(jerarquiaDocente.getIdJerarquiaDocente()), 
        jerarquiaDocente.toString()));
    }
    return col;
  }

  public String getSelectJerarquiaDocente()
  {
    return this.selectJerarquiaDocente;
  }
  public void setSelectJerarquiaDocente(String valJerarquiaDocente) {
    Iterator iterator = this.colJerarquiaDocente.iterator();
    JerarquiaDocente jerarquiaDocente = null;
    this.jerarquiaCategoriaDocente.setJerarquiaDocente(null);
    while (iterator.hasNext()) {
      jerarquiaDocente = (JerarquiaDocente)iterator.next();
      if (String.valueOf(jerarquiaDocente.getIdJerarquiaDocente()).equals(
        valJerarquiaDocente)) {
        this.jerarquiaCategoriaDocente.setJerarquiaDocente(
          jerarquiaDocente);
        break;
      }
    }
    this.selectJerarquiaDocente = valJerarquiaDocente;
  }
  public String getSelectCategoriaDocente() {
    return this.selectCategoriaDocente;
  }
  public void setSelectCategoriaDocente(String valCategoriaDocente) {
    Iterator iterator = this.colCategoriaDocente.iterator();
    CategoriaDocente categoriaDocente = null;
    this.jerarquiaCategoriaDocente.setCategoriaDocente(null);
    while (iterator.hasNext()) {
      categoriaDocente = (CategoriaDocente)iterator.next();
      if (String.valueOf(categoriaDocente.getIdCategoriaDocente()).equals(
        valCategoriaDocente)) {
        this.jerarquiaCategoriaDocente.setCategoriaDocente(
          categoriaDocente);
        break;
      }
    }
    this.selectCategoriaDocente = valCategoriaDocente;
  }
  public Collection getResult() {
    return this.result;
  }

  public JerarquiaCategoriaDocente getJerarquiaCategoriaDocente() {
    if (this.jerarquiaCategoriaDocente == null) {
      this.jerarquiaCategoriaDocente = new JerarquiaCategoriaDocente();
    }
    return this.jerarquiaCategoriaDocente;
  }

  public JerarquiaCategoriaDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColJerarquiaDocente()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colJerarquiaDocente.iterator();
    JerarquiaDocente jerarquiaDocente = null;
    while (iterator.hasNext()) {
      jerarquiaDocente = (JerarquiaDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(jerarquiaDocente.getIdJerarquiaDocente()), 
        jerarquiaDocente.toString()));
    }
    return col;
  }

  public Collection getColCategoriaDocente()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCategoriaDocente.iterator();
    CategoriaDocente categoriaDocente = null;
    while (iterator.hasNext()) {
      categoriaDocente = (CategoriaDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(categoriaDocente.getIdCategoriaDocente()), 
        categoriaDocente.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColJerarquiaDocente = 
        this.docenteFacade.findAllJerarquiaDocente();

      this.colJerarquiaDocente = 
        this.docenteFacade.findAllJerarquiaDocente();
      this.colCategoriaDocente = 
        this.docenteFacade.findAllCategoriaDocente();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findJerarquiaCategoriaDocenteByJerarquiaDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findJerarquiaCategoriaDocenteByJerarquiaDocente(Long.valueOf(this.findSelectJerarquiaDocente).longValue());
      this.showJerarquiaCategoriaDocenteByJerarquiaDocente = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showJerarquiaCategoriaDocenteByJerarquiaDocente)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectJerarquiaDocente = null;

    return null;
  }

  public boolean isShowJerarquiaCategoriaDocenteByJerarquiaDocente() {
    return this.showJerarquiaCategoriaDocenteByJerarquiaDocente;
  }

  public String selectJerarquiaCategoriaDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectJerarquiaDocente = null;
    this.selectCategoriaDocente = null;

    long idJerarquiaCategoriaDocente = 
      Long.parseLong((String)requestParameterMap.get("idJerarquiaCategoriaDocente"));
    try
    {
      this.jerarquiaCategoriaDocente = 
        this.docenteFacade.findJerarquiaCategoriaDocenteById(
        idJerarquiaCategoriaDocente);
      if (this.jerarquiaCategoriaDocente.getJerarquiaDocente() != null) {
        this.selectJerarquiaDocente = 
          String.valueOf(this.jerarquiaCategoriaDocente.getJerarquiaDocente().getIdJerarquiaDocente());
      }
      if (this.jerarquiaCategoriaDocente.getCategoriaDocente() != null) {
        this.selectCategoriaDocente = 
          String.valueOf(this.jerarquiaCategoriaDocente.getCategoriaDocente().getIdCategoriaDocente());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.jerarquiaCategoriaDocente = null;
    this.showJerarquiaCategoriaDocenteByJerarquiaDocente = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.docenteFacade.addJerarquiaCategoriaDocente(
          this.jerarquiaCategoriaDocente);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.docenteFacade.updateJerarquiaCategoriaDocente(
          this.jerarquiaCategoriaDocente);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.docenteFacade.deleteJerarquiaCategoriaDocente(
        this.jerarquiaCategoriaDocente);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.jerarquiaCategoriaDocente = new JerarquiaCategoriaDocente();

    this.selectJerarquiaDocente = null;

    this.selectCategoriaDocente = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.jerarquiaCategoriaDocente.setIdJerarquiaCategoriaDocente(identityGenerator.getNextSequenceNumber("sigefirrhh.base.docente.JerarquiaCategoriaDocente"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.jerarquiaCategoriaDocente = new JerarquiaCategoriaDocente();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}