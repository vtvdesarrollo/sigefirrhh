package sigefirrhh.base.docente;

import java.io.Serializable;

public class GradoNivelDocentePK
  implements Serializable
{
  public long idGradoNivelDocente;

  public GradoNivelDocentePK()
  {
  }

  public GradoNivelDocentePK(long idGradoNivelDocente)
  {
    this.idGradoNivelDocente = idGradoNivelDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((GradoNivelDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(GradoNivelDocentePK thatPK)
  {
    return 
      this.idGradoNivelDocente == thatPK.idGradoNivelDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idGradoNivelDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idGradoNivelDocente);
  }
}