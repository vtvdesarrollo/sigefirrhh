package sigefirrhh.base.docente;

import java.io.Serializable;

public class JerarquiaDocentePK
  implements Serializable
{
  public long idJerarquiaDocente;

  public JerarquiaDocentePK()
  {
  }

  public JerarquiaDocentePK(long idJerarquiaDocente)
  {
    this.idJerarquiaDocente = idJerarquiaDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((JerarquiaDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(JerarquiaDocentePK thatPK)
  {
    return 
      this.idJerarquiaDocente == thatPK.idJerarquiaDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idJerarquiaDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idJerarquiaDocente);
  }
}