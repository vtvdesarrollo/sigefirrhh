package sigefirrhh.base.docente;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.jdo.PersistenceManager;

public class DocenteFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DocenteBusiness docenteBusiness = new DocenteBusiness();

  public void addAsignatura(Asignatura asignatura)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addAsignatura(asignatura);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAsignatura(Asignatura asignatura) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateAsignatura(asignatura);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAsignatura(Asignatura asignatura) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteAsignatura(asignatura);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Asignatura findAsignaturaById(long asignaturaId) throws Exception
  {
    try { this.txn.open();
      Asignatura asignatura = 
        this.docenteBusiness.findAsignaturaById(asignaturaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(asignatura);
      return asignatura;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAsignatura() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllAsignatura();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAsignaturaByCodAsignatura(String codAsignatura)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findAsignaturaByCodAsignatura(codAsignatura);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAsignaturaByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findAsignaturaByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAreaGeografica(AreaGeografica areaGeografica)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addAreaGeografica(areaGeografica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAreaGeografica(AreaGeografica areaGeografica) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateAreaGeografica(areaGeografica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAreaGeografica(AreaGeografica areaGeografica) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteAreaGeografica(areaGeografica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public AreaGeografica findAreaGeograficaById(long areaGeograficaId) throws Exception
  {
    try { this.txn.open();
      AreaGeografica areaGeografica = 
        this.docenteBusiness.findAreaGeograficaById(areaGeograficaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(areaGeografica);
      return areaGeografica;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAreaGeografica() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllAreaGeografica();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAreaGeograficaByCodAreaGeografica(String codAreaGeografica)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findAreaGeograficaByCodAreaGeografica(codAreaGeografica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAreaGeograficaByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findAreaGeograficaByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCategoriaDocente(CategoriaDocente categoriaDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addCategoriaDocente(categoriaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCategoriaDocente(CategoriaDocente categoriaDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateCategoriaDocente(categoriaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCategoriaDocente(CategoriaDocente categoriaDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteCategoriaDocente(categoriaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CategoriaDocente findCategoriaDocenteById(long categoriaDocenteId) throws Exception
  {
    try { this.txn.open();
      CategoriaDocente categoriaDocente = 
        this.docenteBusiness.findCategoriaDocenteById(categoriaDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(categoriaDocente);
      return categoriaDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCategoriaDocente() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllCategoriaDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCategoriaDocenteByDigitoCategoria(String digitoCategoria)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findCategoriaDocenteByDigitoCategoria(digitoCategoria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCategoriaDocenteByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findCategoriaDocenteByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDedicacionDocente(DedicacionDocente dedicacionDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addDedicacionDocente(dedicacionDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDedicacionDocente(DedicacionDocente dedicacionDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateDedicacionDocente(dedicacionDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDedicacionDocente(DedicacionDocente dedicacionDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteDedicacionDocente(dedicacionDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DedicacionDocente findDedicacionDocenteById(long dedicacionDocenteId) throws Exception
  {
    try { this.txn.open();
      DedicacionDocente dedicacionDocente = 
        this.docenteBusiness.findDedicacionDocenteById(dedicacionDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(dedicacionDocente);
      return dedicacionDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDedicacionDocente() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllDedicacionDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDedicacionDocenteByDigitoDedicacion(String digitoDedicacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findDedicacionDocenteByDigitoDedicacion(digitoDedicacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDedicacionDocenteByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findDedicacionDocenteByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSeguridadDocente(SeguridadDocente seguridadDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addSeguridadDocente(seguridadDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSeguridadDocente(SeguridadDocente seguridadDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateSeguridadDocente(seguridadDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSeguridadDocente(SeguridadDocente seguridadDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteSeguridadDocente(seguridadDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SeguridadDocente findSeguridadDocenteById(long seguridadDocenteId) throws Exception
  {
    try { this.txn.open();
      SeguridadDocente seguridadDocente = 
        this.docenteBusiness.findSeguridadDocenteById(seguridadDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadDocente);
      return seguridadDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSeguridadDocente() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllSeguridadDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadDocenteByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findSeguridadDocenteByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadDocenteByCerrado(String cerrado)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findSeguridadDocenteByCerrado(cerrado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadDocenteByFechaCierre(Date fechaCierre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findSeguridadDocenteByFechaCierre(fechaCierre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addGradoDocente(GradoDocente gradoDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addGradoDocente(gradoDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateGradoDocente(GradoDocente gradoDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateGradoDocente(gradoDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteGradoDocente(GradoDocente gradoDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteGradoDocente(gradoDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public GradoDocente findGradoDocenteById(long gradoDocenteId) throws Exception
  {
    try { this.txn.open();
      GradoDocente gradoDocente = 
        this.docenteBusiness.findGradoDocenteById(gradoDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(gradoDocente);
      return gradoDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllGradoDocente() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllGradoDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGradoDocenteByDigitoGrado(String digitoGrado)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findGradoDocenteByDigitoGrado(digitoGrado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGradoDocenteByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findGradoDocenteByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addJerarquiaDocente(JerarquiaDocente jerarquiaDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addJerarquiaDocente(jerarquiaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateJerarquiaDocente(JerarquiaDocente jerarquiaDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateJerarquiaDocente(jerarquiaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteJerarquiaDocente(JerarquiaDocente jerarquiaDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteJerarquiaDocente(jerarquiaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public JerarquiaDocente findJerarquiaDocenteById(long jerarquiaDocenteId) throws Exception
  {
    try { this.txn.open();
      JerarquiaDocente jerarquiaDocente = 
        this.docenteBusiness.findJerarquiaDocenteById(jerarquiaDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(jerarquiaDocente);
      return jerarquiaDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllJerarquiaDocente() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllJerarquiaDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findJerarquiaDocenteByDigitoJerarquia(String digitoJerarquia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findJerarquiaDocenteByDigitoJerarquia(digitoJerarquia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findJerarquiaDocenteByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findJerarquiaDocenteByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addNivelDocente(NivelDocente nivelDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addNivelDocente(nivelDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateNivelDocente(NivelDocente nivelDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateNivelDocente(nivelDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteNivelDocente(NivelDocente nivelDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteNivelDocente(nivelDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public NivelDocente findNivelDocenteById(long nivelDocenteId) throws Exception
  {
    try { this.txn.open();
      NivelDocente nivelDocente = 
        this.docenteBusiness.findNivelDocenteById(nivelDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(nivelDocente);
      return nivelDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllNivelDocente() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllNivelDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNivelDocenteByDigitoNivel(String digitoNivel)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findNivelDocenteByDigitoNivel(digitoNivel);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNivelDocenteByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findNivelDocenteByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTurnoDocente(TurnoDocente turnoDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addTurnoDocente(turnoDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTurnoDocente(TurnoDocente turnoDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateTurnoDocente(turnoDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTurnoDocente(TurnoDocente turnoDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteTurnoDocente(turnoDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TurnoDocente findTurnoDocenteById(long turnoDocenteId) throws Exception
  {
    try { this.txn.open();
      TurnoDocente turnoDocente = 
        this.docenteBusiness.findTurnoDocenteById(turnoDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(turnoDocente);
      return turnoDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTurnoDocente() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllTurnoDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTurnoDocenteByDigitoTurno(String digitoTurno)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findTurnoDocenteByDigitoTurno(digitoTurno);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTurnoDocenteByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findTurnoDocenteByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addGradoNivelDocente(GradoNivelDocente gradoNivelDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addGradoNivelDocente(gradoNivelDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateGradoNivelDocente(GradoNivelDocente gradoNivelDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateGradoNivelDocente(gradoNivelDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteGradoNivelDocente(GradoNivelDocente gradoNivelDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteGradoNivelDocente(gradoNivelDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public GradoNivelDocente findGradoNivelDocenteById(long gradoNivelDocenteId) throws Exception
  {
    try { this.txn.open();
      GradoNivelDocente gradoNivelDocente = 
        this.docenteBusiness.findGradoNivelDocenteById(gradoNivelDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(gradoNivelDocente);
      return gradoNivelDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllGradoNivelDocente() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllGradoNivelDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGradoNivelDocenteByGradoDocente(long idGradoDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findGradoNivelDocenteByGradoDocente(idGradoDocente);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addJerarquiaCategoriaDocente(JerarquiaCategoriaDocente jerarquiaCategoriaDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addJerarquiaCategoriaDocente(jerarquiaCategoriaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateJerarquiaCategoriaDocente(JerarquiaCategoriaDocente jerarquiaCategoriaDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateJerarquiaCategoriaDocente(jerarquiaCategoriaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteJerarquiaCategoriaDocente(JerarquiaCategoriaDocente jerarquiaCategoriaDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteJerarquiaCategoriaDocente(jerarquiaCategoriaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public JerarquiaCategoriaDocente findJerarquiaCategoriaDocenteById(long jerarquiaCategoriaDocenteId) throws Exception
  {
    try { this.txn.open();
      JerarquiaCategoriaDocente jerarquiaCategoriaDocente = 
        this.docenteBusiness.findJerarquiaCategoriaDocenteById(jerarquiaCategoriaDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(jerarquiaCategoriaDocente);
      return jerarquiaCategoriaDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllJerarquiaCategoriaDocente() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllJerarquiaCategoriaDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findJerarquiaCategoriaDocenteByJerarquiaDocente(long idJerarquiaDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findJerarquiaCategoriaDocenteByJerarquiaDocente(idJerarquiaDocente);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCausaDocente(CausaDocente causaDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docenteBusiness.addCausaDocente(causaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCausaDocente(CausaDocente causaDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.updateCausaDocente(causaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCausaDocente(CausaDocente causaDocente) throws Exception
  {
    try { this.txn.open();
      this.docenteBusiness.deleteCausaDocente(causaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CausaDocente findCausaDocenteById(long causaDocenteId) throws Exception
  {
    try { this.txn.open();
      CausaDocente causaDocente = 
        this.docenteBusiness.findCausaDocenteById(causaDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(causaDocente);
      return causaDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCausaDocente() throws Exception
  {
    try { this.txn.open();
      return this.docenteBusiness.findAllCausaDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCausaDocenteByCodCausaDocente(String codCausaDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findCausaDocenteByCodCausaDocente(codCausaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCausaDocenteByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findCausaDocenteByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCausaDocenteByTipo(String tipo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docenteBusiness.findCausaDocenteByTipo(tipo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}