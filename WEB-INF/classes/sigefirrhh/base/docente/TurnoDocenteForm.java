package sigefirrhh.base.docente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class TurnoDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TurnoDocenteForm.class.getName());
  private TurnoDocente turnoDocente;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DocenteFacade docenteFacade = new DocenteFacade();
  private boolean showTurnoDocenteByDigitoTurno;
  private boolean showTurnoDocenteByNombre;
  private String findDigitoTurno;
  private String findNombre;
  private Object stateResultTurnoDocenteByDigitoTurno = null;

  private Object stateResultTurnoDocenteByNombre = null;

  public String getFindDigitoTurno()
  {
    return this.findDigitoTurno;
  }
  public void setFindDigitoTurno(String findDigitoTurno) {
    this.findDigitoTurno = findDigitoTurno;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public TurnoDocente getTurnoDocente() {
    if (this.turnoDocente == null) {
      this.turnoDocente = new TurnoDocente();
    }
    return this.turnoDocente;
  }

  public TurnoDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListCondicion()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = TurnoDocente.LISTA_CONDICION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTurno() {
    Collection col = new ArrayList();

    Iterator iterEntry = TurnoDocente.LISTA_TURNO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListVigencia() {
    Collection col = new ArrayList();

    Iterator iterEntry = TurnoDocente.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findTurnoDocenteByDigitoTurno()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findTurnoDocenteByDigitoTurno(this.findDigitoTurno);
      this.showTurnoDocenteByDigitoTurno = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTurnoDocenteByDigitoTurno)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findDigitoTurno = null;
    this.findNombre = null;

    return null;
  }

  public String findTurnoDocenteByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findTurnoDocenteByNombre(this.findNombre);
      this.showTurnoDocenteByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTurnoDocenteByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findDigitoTurno = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowTurnoDocenteByDigitoTurno() {
    return this.showTurnoDocenteByDigitoTurno;
  }
  public boolean isShowTurnoDocenteByNombre() {
    return this.showTurnoDocenteByNombre;
  }

  public String selectTurnoDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTurnoDocente = 
      Long.parseLong((String)requestParameterMap.get("idTurnoDocente"));
    try
    {
      this.turnoDocente = 
        this.docenteFacade.findTurnoDocenteById(
        idTurnoDocente);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.turnoDocente = null;
    this.showTurnoDocenteByDigitoTurno = false;
    this.showTurnoDocenteByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.docenteFacade.addTurnoDocente(
          this.turnoDocente);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.docenteFacade.updateTurnoDocente(
          this.turnoDocente);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.docenteFacade.deleteTurnoDocente(
        this.turnoDocente);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.turnoDocente = new TurnoDocente();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.turnoDocente.setIdTurnoDocente(identityGenerator.getNextSequenceNumber("sigefirrhh.base.docente.TurnoDocente"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.turnoDocente = new TurnoDocente();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}