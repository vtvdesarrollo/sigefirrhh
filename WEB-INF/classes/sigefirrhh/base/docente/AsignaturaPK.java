package sigefirrhh.base.docente;

import java.io.Serializable;

public class AsignaturaPK
  implements Serializable
{
  public long idAsignatura;

  public AsignaturaPK()
  {
  }

  public AsignaturaPK(long idAsignatura)
  {
    this.idAsignatura = idAsignatura;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AsignaturaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AsignaturaPK thatPK)
  {
    return 
      this.idAsignatura == thatPK.idAsignatura;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAsignatura)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAsignatura);
  }
}