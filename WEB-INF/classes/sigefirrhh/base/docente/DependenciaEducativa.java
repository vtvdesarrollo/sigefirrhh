package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.TipoDependencia;
import sigefirrhh.base.ubicacion.Parroquia;

public class DependenciaEducativa
  implements Serializable, PersistenceCapable
{
  private long idDependenciaEducativa;
  private String nombre;
  private int correlativoParroquia;
  private UbicacionGeopolitica ubicacionGeopolitica;
  private TipoDependencia tipoDependencia;
  private NivelDependencia nivelDependencia;
  private TurnoDependencia turnoDependencia;
  private ZonaEducativa zonaEducativa;
  private AreaGeografica areaGeografica;
  private Parroquia parroquia;
  private PlantelActual plantelActual;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "areaGeografica", "correlativoParroquia", "idDependenciaEducativa", "nivelDependencia", "nombre", "parroquia", "plantelActual", "tipoDependencia", "turnoDependencia", "ubicacionGeopolitica", "zonaEducativa" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.docente.AreaGeografica"), Integer.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.base.docente.NivelDependencia"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Parroquia"), sunjdo$classForName$("sigefirrhh.base.docente.PlantelActual"), sunjdo$classForName$("sigefirrhh.base.estructura.TipoDependencia"), sunjdo$classForName$("sigefirrhh.base.docente.TurnoDependencia"), sunjdo$classForName$("sigefirrhh.base.docente.UbicacionGeopolitica"), sunjdo$classForName$("sigefirrhh.base.docente.ZonaEducativa") };
  private static final byte[] jdoFieldFlags = { 26, 21, 24, 26, 21, 26, 26, 26, 26, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public AreaGeografica getAreaGeografica()
  {
    return jdoGetareaGeografica(this);
  }

  public int getCorrelativoParroquia()
  {
    return jdoGetcorrelativoParroquia(this);
  }

  public long getIdDependenciaEducativa()
  {
    return jdoGetidDependenciaEducativa(this);
  }

  public NivelDependencia getNivelDependencia()
  {
    return jdoGetnivelDependencia(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public Parroquia getParroquia()
  {
    return jdoGetparroquia(this);
  }

  public PlantelActual getPlantelActual()
  {
    return jdoGetplantelActual(this);
  }

  public TipoDependencia getTipoDependencia()
  {
    return jdoGettipoDependencia(this);
  }

  public TurnoDependencia getTurnoDependencia()
  {
    return jdoGetturnoDependencia(this);
  }

  public UbicacionGeopolitica getUbicacionGeopolitica()
  {
    return jdoGetubicacionGeopolitica(this);
  }

  public ZonaEducativa getZonaEducativa()
  {
    return jdoGetzonaEducativa(this);
  }

  public void setAreaGeografica(AreaGeografica geografica)
  {
    jdoSetareaGeografica(this, geografica);
  }

  public void setCorrelativoParroquia(int i)
  {
    jdoSetcorrelativoParroquia(this, i);
  }

  public void setIdDependenciaEducativa(long l)
  {
    jdoSetidDependenciaEducativa(this, l);
  }

  public void setNivelDependencia(NivelDependencia dependencia)
  {
    jdoSetnivelDependencia(this, dependencia);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setParroquia(Parroquia parroquia)
  {
    jdoSetparroquia(this, parroquia);
  }

  public void setPlantelActual(PlantelActual actual)
  {
    jdoSetplantelActual(this, actual);
  }

  public void setTipoDependencia(TipoDependencia dependencia)
  {
    jdoSettipoDependencia(this, dependencia);
  }

  public void setTurnoDependencia(TurnoDependencia dependencia)
  {
    jdoSetturnoDependencia(this, dependencia);
  }

  public void setUbicacionGeopolitica(UbicacionGeopolitica geopolitica)
  {
    jdoSetubicacionGeopolitica(this, geopolitica);
  }

  public void setZonaEducativa(ZonaEducativa educativa)
  {
    jdoSetzonaEducativa(this, educativa);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.DependenciaEducativa"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DependenciaEducativa());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DependenciaEducativa localDependenciaEducativa = new DependenciaEducativa();
    localDependenciaEducativa.jdoFlags = 1;
    localDependenciaEducativa.jdoStateManager = paramStateManager;
    return localDependenciaEducativa;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DependenciaEducativa localDependenciaEducativa = new DependenciaEducativa();
    localDependenciaEducativa.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDependenciaEducativa.jdoFlags = 1;
    localDependenciaEducativa.jdoStateManager = paramStateManager;
    return localDependenciaEducativa;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.areaGeografica);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.correlativoParroquia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDependenciaEducativa);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nivelDependencia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.parroquia);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.plantelActual);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoDependencia);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.turnoDependencia);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ubicacionGeopolitica);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.zonaEducativa);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.areaGeografica = ((AreaGeografica)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.correlativoParroquia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDependenciaEducativa = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelDependencia = ((NivelDependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.parroquia = ((Parroquia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.plantelActual = ((PlantelActual)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoDependencia = ((TipoDependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.turnoDependencia = ((TurnoDependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ubicacionGeopolitica = ((UbicacionGeopolitica)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.zonaEducativa = ((ZonaEducativa)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DependenciaEducativa paramDependenciaEducativa, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDependenciaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.areaGeografica = paramDependenciaEducativa.areaGeografica;
      return;
    case 1:
      if (paramDependenciaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.correlativoParroquia = paramDependenciaEducativa.correlativoParroquia;
      return;
    case 2:
      if (paramDependenciaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.idDependenciaEducativa = paramDependenciaEducativa.idDependenciaEducativa;
      return;
    case 3:
      if (paramDependenciaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.nivelDependencia = paramDependenciaEducativa.nivelDependencia;
      return;
    case 4:
      if (paramDependenciaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramDependenciaEducativa.nombre;
      return;
    case 5:
      if (paramDependenciaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.parroquia = paramDependenciaEducativa.parroquia;
      return;
    case 6:
      if (paramDependenciaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.plantelActual = paramDependenciaEducativa.plantelActual;
      return;
    case 7:
      if (paramDependenciaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.tipoDependencia = paramDependenciaEducativa.tipoDependencia;
      return;
    case 8:
      if (paramDependenciaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.turnoDependencia = paramDependenciaEducativa.turnoDependencia;
      return;
    case 9:
      if (paramDependenciaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.ubicacionGeopolitica = paramDependenciaEducativa.ubicacionGeopolitica;
      return;
    case 10:
      if (paramDependenciaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.zonaEducativa = paramDependenciaEducativa.zonaEducativa;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DependenciaEducativa))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DependenciaEducativa localDependenciaEducativa = (DependenciaEducativa)paramObject;
    if (localDependenciaEducativa.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDependenciaEducativa, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DependenciaEducativaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DependenciaEducativaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DependenciaEducativaPK))
      throw new IllegalArgumentException("arg1");
    DependenciaEducativaPK localDependenciaEducativaPK = (DependenciaEducativaPK)paramObject;
    localDependenciaEducativaPK.idDependenciaEducativa = this.idDependenciaEducativa;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DependenciaEducativaPK))
      throw new IllegalArgumentException("arg1");
    DependenciaEducativaPK localDependenciaEducativaPK = (DependenciaEducativaPK)paramObject;
    this.idDependenciaEducativa = localDependenciaEducativaPK.idDependenciaEducativa;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DependenciaEducativaPK))
      throw new IllegalArgumentException("arg2");
    DependenciaEducativaPK localDependenciaEducativaPK = (DependenciaEducativaPK)paramObject;
    localDependenciaEducativaPK.idDependenciaEducativa = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DependenciaEducativaPK))
      throw new IllegalArgumentException("arg2");
    DependenciaEducativaPK localDependenciaEducativaPK = (DependenciaEducativaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localDependenciaEducativaPK.idDependenciaEducativa);
  }

  private static final AreaGeografica jdoGetareaGeografica(DependenciaEducativa paramDependenciaEducativa)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaEducativa.areaGeografica;
    if (localStateManager.isLoaded(paramDependenciaEducativa, jdoInheritedFieldCount + 0))
      return paramDependenciaEducativa.areaGeografica;
    return (AreaGeografica)localStateManager.getObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 0, paramDependenciaEducativa.areaGeografica);
  }

  private static final void jdoSetareaGeografica(DependenciaEducativa paramDependenciaEducativa, AreaGeografica paramAreaGeografica)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaEducativa.areaGeografica = paramAreaGeografica;
      return;
    }
    localStateManager.setObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 0, paramDependenciaEducativa.areaGeografica, paramAreaGeografica);
  }

  private static final int jdoGetcorrelativoParroquia(DependenciaEducativa paramDependenciaEducativa)
  {
    if (paramDependenciaEducativa.jdoFlags <= 0)
      return paramDependenciaEducativa.correlativoParroquia;
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaEducativa.correlativoParroquia;
    if (localStateManager.isLoaded(paramDependenciaEducativa, jdoInheritedFieldCount + 1))
      return paramDependenciaEducativa.correlativoParroquia;
    return localStateManager.getIntField(paramDependenciaEducativa, jdoInheritedFieldCount + 1, paramDependenciaEducativa.correlativoParroquia);
  }

  private static final void jdoSetcorrelativoParroquia(DependenciaEducativa paramDependenciaEducativa, int paramInt)
  {
    if (paramDependenciaEducativa.jdoFlags == 0)
    {
      paramDependenciaEducativa.correlativoParroquia = paramInt;
      return;
    }
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaEducativa.correlativoParroquia = paramInt;
      return;
    }
    localStateManager.setIntField(paramDependenciaEducativa, jdoInheritedFieldCount + 1, paramDependenciaEducativa.correlativoParroquia, paramInt);
  }

  private static final long jdoGetidDependenciaEducativa(DependenciaEducativa paramDependenciaEducativa)
  {
    return paramDependenciaEducativa.idDependenciaEducativa;
  }

  private static final void jdoSetidDependenciaEducativa(DependenciaEducativa paramDependenciaEducativa, long paramLong)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaEducativa.idDependenciaEducativa = paramLong;
      return;
    }
    localStateManager.setLongField(paramDependenciaEducativa, jdoInheritedFieldCount + 2, paramDependenciaEducativa.idDependenciaEducativa, paramLong);
  }

  private static final NivelDependencia jdoGetnivelDependencia(DependenciaEducativa paramDependenciaEducativa)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaEducativa.nivelDependencia;
    if (localStateManager.isLoaded(paramDependenciaEducativa, jdoInheritedFieldCount + 3))
      return paramDependenciaEducativa.nivelDependencia;
    return (NivelDependencia)localStateManager.getObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 3, paramDependenciaEducativa.nivelDependencia);
  }

  private static final void jdoSetnivelDependencia(DependenciaEducativa paramDependenciaEducativa, NivelDependencia paramNivelDependencia)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaEducativa.nivelDependencia = paramNivelDependencia;
      return;
    }
    localStateManager.setObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 3, paramDependenciaEducativa.nivelDependencia, paramNivelDependencia);
  }

  private static final String jdoGetnombre(DependenciaEducativa paramDependenciaEducativa)
  {
    if (paramDependenciaEducativa.jdoFlags <= 0)
      return paramDependenciaEducativa.nombre;
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaEducativa.nombre;
    if (localStateManager.isLoaded(paramDependenciaEducativa, jdoInheritedFieldCount + 4))
      return paramDependenciaEducativa.nombre;
    return localStateManager.getStringField(paramDependenciaEducativa, jdoInheritedFieldCount + 4, paramDependenciaEducativa.nombre);
  }

  private static final void jdoSetnombre(DependenciaEducativa paramDependenciaEducativa, String paramString)
  {
    if (paramDependenciaEducativa.jdoFlags == 0)
    {
      paramDependenciaEducativa.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaEducativa.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramDependenciaEducativa, jdoInheritedFieldCount + 4, paramDependenciaEducativa.nombre, paramString);
  }

  private static final Parroquia jdoGetparroquia(DependenciaEducativa paramDependenciaEducativa)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaEducativa.parroquia;
    if (localStateManager.isLoaded(paramDependenciaEducativa, jdoInheritedFieldCount + 5))
      return paramDependenciaEducativa.parroquia;
    return (Parroquia)localStateManager.getObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 5, paramDependenciaEducativa.parroquia);
  }

  private static final void jdoSetparroquia(DependenciaEducativa paramDependenciaEducativa, Parroquia paramParroquia)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaEducativa.parroquia = paramParroquia;
      return;
    }
    localStateManager.setObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 5, paramDependenciaEducativa.parroquia, paramParroquia);
  }

  private static final PlantelActual jdoGetplantelActual(DependenciaEducativa paramDependenciaEducativa)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaEducativa.plantelActual;
    if (localStateManager.isLoaded(paramDependenciaEducativa, jdoInheritedFieldCount + 6))
      return paramDependenciaEducativa.plantelActual;
    return (PlantelActual)localStateManager.getObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 6, paramDependenciaEducativa.plantelActual);
  }

  private static final void jdoSetplantelActual(DependenciaEducativa paramDependenciaEducativa, PlantelActual paramPlantelActual)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaEducativa.plantelActual = paramPlantelActual;
      return;
    }
    localStateManager.setObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 6, paramDependenciaEducativa.plantelActual, paramPlantelActual);
  }

  private static final TipoDependencia jdoGettipoDependencia(DependenciaEducativa paramDependenciaEducativa)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaEducativa.tipoDependencia;
    if (localStateManager.isLoaded(paramDependenciaEducativa, jdoInheritedFieldCount + 7))
      return paramDependenciaEducativa.tipoDependencia;
    return (TipoDependencia)localStateManager.getObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 7, paramDependenciaEducativa.tipoDependencia);
  }

  private static final void jdoSettipoDependencia(DependenciaEducativa paramDependenciaEducativa, TipoDependencia paramTipoDependencia)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaEducativa.tipoDependencia = paramTipoDependencia;
      return;
    }
    localStateManager.setObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 7, paramDependenciaEducativa.tipoDependencia, paramTipoDependencia);
  }

  private static final TurnoDependencia jdoGetturnoDependencia(DependenciaEducativa paramDependenciaEducativa)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaEducativa.turnoDependencia;
    if (localStateManager.isLoaded(paramDependenciaEducativa, jdoInheritedFieldCount + 8))
      return paramDependenciaEducativa.turnoDependencia;
    return (TurnoDependencia)localStateManager.getObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 8, paramDependenciaEducativa.turnoDependencia);
  }

  private static final void jdoSetturnoDependencia(DependenciaEducativa paramDependenciaEducativa, TurnoDependencia paramTurnoDependencia)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaEducativa.turnoDependencia = paramTurnoDependencia;
      return;
    }
    localStateManager.setObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 8, paramDependenciaEducativa.turnoDependencia, paramTurnoDependencia);
  }

  private static final UbicacionGeopolitica jdoGetubicacionGeopolitica(DependenciaEducativa paramDependenciaEducativa)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaEducativa.ubicacionGeopolitica;
    if (localStateManager.isLoaded(paramDependenciaEducativa, jdoInheritedFieldCount + 9))
      return paramDependenciaEducativa.ubicacionGeopolitica;
    return (UbicacionGeopolitica)localStateManager.getObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 9, paramDependenciaEducativa.ubicacionGeopolitica);
  }

  private static final void jdoSetubicacionGeopolitica(DependenciaEducativa paramDependenciaEducativa, UbicacionGeopolitica paramUbicacionGeopolitica)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaEducativa.ubicacionGeopolitica = paramUbicacionGeopolitica;
      return;
    }
    localStateManager.setObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 9, paramDependenciaEducativa.ubicacionGeopolitica, paramUbicacionGeopolitica);
  }

  private static final ZonaEducativa jdoGetzonaEducativa(DependenciaEducativa paramDependenciaEducativa)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaEducativa.zonaEducativa;
    if (localStateManager.isLoaded(paramDependenciaEducativa, jdoInheritedFieldCount + 10))
      return paramDependenciaEducativa.zonaEducativa;
    return (ZonaEducativa)localStateManager.getObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 10, paramDependenciaEducativa.zonaEducativa);
  }

  private static final void jdoSetzonaEducativa(DependenciaEducativa paramDependenciaEducativa, ZonaEducativa paramZonaEducativa)
  {
    StateManager localStateManager = paramDependenciaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaEducativa.zonaEducativa = paramZonaEducativa;
      return;
    }
    localStateManager.setObjectField(paramDependenciaEducativa, jdoInheritedFieldCount + 10, paramDependenciaEducativa.zonaEducativa, paramZonaEducativa);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}