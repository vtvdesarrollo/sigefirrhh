package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.ubicacion.Municipio;

public class ZonaEducativa
  implements Serializable, PersistenceCapable
{
  private long idZonaEducativa;
  private String codigoZona;
  private String nombre;
  private Municipio municipio;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codigoZona", "idZonaEducativa", "municipio", "nombre", "organismo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.ubicacion.Municipio"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 21, 24, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String getCodigoZona()
  {
    return jdoGetcodigoZona(this);
  }

  public long getIdZonaEducativa()
  {
    return jdoGetidZonaEducativa(this);
  }

  public Municipio getMunicipio()
  {
    return jdoGetmunicipio(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setCodigoZona(String string)
  {
    jdoSetcodigoZona(this, string);
  }

  public void setIdZonaEducativa(long l)
  {
    jdoSetidZonaEducativa(this, l);
  }

  public void setMunicipio(Municipio municipio)
  {
    jdoSetmunicipio(this, municipio);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.ZonaEducativa"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ZonaEducativa());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ZonaEducativa localZonaEducativa = new ZonaEducativa();
    localZonaEducativa.jdoFlags = 1;
    localZonaEducativa.jdoStateManager = paramStateManager;
    return localZonaEducativa;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ZonaEducativa localZonaEducativa = new ZonaEducativa();
    localZonaEducativa.jdoCopyKeyFieldsFromObjectId(paramObject);
    localZonaEducativa.jdoFlags = 1;
    localZonaEducativa.jdoStateManager = paramStateManager;
    return localZonaEducativa;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoZona);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idZonaEducativa);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.municipio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoZona = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idZonaEducativa = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.municipio = ((Municipio)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ZonaEducativa paramZonaEducativa, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramZonaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.codigoZona = paramZonaEducativa.codigoZona;
      return;
    case 1:
      if (paramZonaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.idZonaEducativa = paramZonaEducativa.idZonaEducativa;
      return;
    case 2:
      if (paramZonaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.municipio = paramZonaEducativa.municipio;
      return;
    case 3:
      if (paramZonaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramZonaEducativa.nombre;
      return;
    case 4:
      if (paramZonaEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramZonaEducativa.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ZonaEducativa))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ZonaEducativa localZonaEducativa = (ZonaEducativa)paramObject;
    if (localZonaEducativa.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localZonaEducativa, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ZonaEducativaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ZonaEducativaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ZonaEducativaPK))
      throw new IllegalArgumentException("arg1");
    ZonaEducativaPK localZonaEducativaPK = (ZonaEducativaPK)paramObject;
    localZonaEducativaPK.idZonaEducativa = this.idZonaEducativa;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ZonaEducativaPK))
      throw new IllegalArgumentException("arg1");
    ZonaEducativaPK localZonaEducativaPK = (ZonaEducativaPK)paramObject;
    this.idZonaEducativa = localZonaEducativaPK.idZonaEducativa;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ZonaEducativaPK))
      throw new IllegalArgumentException("arg2");
    ZonaEducativaPK localZonaEducativaPK = (ZonaEducativaPK)paramObject;
    localZonaEducativaPK.idZonaEducativa = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ZonaEducativaPK))
      throw new IllegalArgumentException("arg2");
    ZonaEducativaPK localZonaEducativaPK = (ZonaEducativaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localZonaEducativaPK.idZonaEducativa);
  }

  private static final String jdoGetcodigoZona(ZonaEducativa paramZonaEducativa)
  {
    if (paramZonaEducativa.jdoFlags <= 0)
      return paramZonaEducativa.codigoZona;
    StateManager localStateManager = paramZonaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramZonaEducativa.codigoZona;
    if (localStateManager.isLoaded(paramZonaEducativa, jdoInheritedFieldCount + 0))
      return paramZonaEducativa.codigoZona;
    return localStateManager.getStringField(paramZonaEducativa, jdoInheritedFieldCount + 0, paramZonaEducativa.codigoZona);
  }

  private static final void jdoSetcodigoZona(ZonaEducativa paramZonaEducativa, String paramString)
  {
    if (paramZonaEducativa.jdoFlags == 0)
    {
      paramZonaEducativa.codigoZona = paramString;
      return;
    }
    StateManager localStateManager = paramZonaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramZonaEducativa.codigoZona = paramString;
      return;
    }
    localStateManager.setStringField(paramZonaEducativa, jdoInheritedFieldCount + 0, paramZonaEducativa.codigoZona, paramString);
  }

  private static final long jdoGetidZonaEducativa(ZonaEducativa paramZonaEducativa)
  {
    return paramZonaEducativa.idZonaEducativa;
  }

  private static final void jdoSetidZonaEducativa(ZonaEducativa paramZonaEducativa, long paramLong)
  {
    StateManager localStateManager = paramZonaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramZonaEducativa.idZonaEducativa = paramLong;
      return;
    }
    localStateManager.setLongField(paramZonaEducativa, jdoInheritedFieldCount + 1, paramZonaEducativa.idZonaEducativa, paramLong);
  }

  private static final Municipio jdoGetmunicipio(ZonaEducativa paramZonaEducativa)
  {
    StateManager localStateManager = paramZonaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramZonaEducativa.municipio;
    if (localStateManager.isLoaded(paramZonaEducativa, jdoInheritedFieldCount + 2))
      return paramZonaEducativa.municipio;
    return (Municipio)localStateManager.getObjectField(paramZonaEducativa, jdoInheritedFieldCount + 2, paramZonaEducativa.municipio);
  }

  private static final void jdoSetmunicipio(ZonaEducativa paramZonaEducativa, Municipio paramMunicipio)
  {
    StateManager localStateManager = paramZonaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramZonaEducativa.municipio = paramMunicipio;
      return;
    }
    localStateManager.setObjectField(paramZonaEducativa, jdoInheritedFieldCount + 2, paramZonaEducativa.municipio, paramMunicipio);
  }

  private static final String jdoGetnombre(ZonaEducativa paramZonaEducativa)
  {
    if (paramZonaEducativa.jdoFlags <= 0)
      return paramZonaEducativa.nombre;
    StateManager localStateManager = paramZonaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramZonaEducativa.nombre;
    if (localStateManager.isLoaded(paramZonaEducativa, jdoInheritedFieldCount + 3))
      return paramZonaEducativa.nombre;
    return localStateManager.getStringField(paramZonaEducativa, jdoInheritedFieldCount + 3, paramZonaEducativa.nombre);
  }

  private static final void jdoSetnombre(ZonaEducativa paramZonaEducativa, String paramString)
  {
    if (paramZonaEducativa.jdoFlags == 0)
    {
      paramZonaEducativa.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramZonaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramZonaEducativa.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramZonaEducativa, jdoInheritedFieldCount + 3, paramZonaEducativa.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(ZonaEducativa paramZonaEducativa)
  {
    StateManager localStateManager = paramZonaEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramZonaEducativa.organismo;
    if (localStateManager.isLoaded(paramZonaEducativa, jdoInheritedFieldCount + 4))
      return paramZonaEducativa.organismo;
    return (Organismo)localStateManager.getObjectField(paramZonaEducativa, jdoInheritedFieldCount + 4, paramZonaEducativa.organismo);
  }

  private static final void jdoSetorganismo(ZonaEducativa paramZonaEducativa, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramZonaEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramZonaEducativa.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramZonaEducativa, jdoInheritedFieldCount + 4, paramZonaEducativa.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}