package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class AreaGeograficaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAreaGeografica(AreaGeografica areaGeografica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    AreaGeografica areaGeograficaNew = 
      (AreaGeografica)BeanUtils.cloneBean(
      areaGeografica);

    pm.makePersistent(areaGeograficaNew);
  }

  public void updateAreaGeografica(AreaGeografica areaGeografica) throws Exception
  {
    AreaGeografica areaGeograficaModify = 
      findAreaGeograficaById(areaGeografica.getIdAreaGeografica());

    BeanUtils.copyProperties(areaGeograficaModify, areaGeografica);
  }

  public void deleteAreaGeografica(AreaGeografica areaGeografica) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    AreaGeografica areaGeograficaDelete = 
      findAreaGeograficaById(areaGeografica.getIdAreaGeografica());
    pm.deletePersistent(areaGeograficaDelete);
  }

  public AreaGeografica findAreaGeograficaById(long idAreaGeografica) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAreaGeografica == pIdAreaGeografica";
    Query query = pm.newQuery(AreaGeografica.class, filter);

    query.declareParameters("long pIdAreaGeografica");

    parameters.put("pIdAreaGeografica", new Long(idAreaGeografica));

    Collection colAreaGeografica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAreaGeografica.iterator();
    return (AreaGeografica)iterator.next();
  }

  public Collection findAreaGeograficaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent areaGeograficaExtent = pm.getExtent(
      AreaGeografica.class, true);
    Query query = pm.newQuery(areaGeograficaExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodAreaGeografica(String codAreaGeografica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codAreaGeografica == pCodAreaGeografica";

    Query query = pm.newQuery(AreaGeografica.class, filter);

    query.declareParameters("java.lang.String pCodAreaGeografica");
    HashMap parameters = new HashMap();

    parameters.put("pCodAreaGeografica", new String(codAreaGeografica));

    Collection colAreaGeografica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAreaGeografica);

    return colAreaGeografica;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(AreaGeografica.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    Collection colAreaGeografica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAreaGeografica);

    return colAreaGeografica;
  }
}