package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class JerarquiaCategoriaDocente
  implements Serializable, PersistenceCapable
{
  private long idJerarquiaCategoriaDocente;
  private JerarquiaDocente jerarquiaDocente;
  private CategoriaDocente categoriaDocente;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "categoriaDocente", "idJerarquiaCategoriaDocente", "jerarquiaDocente" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.docente.CategoriaDocente"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.docente.JerarquiaDocente") };
  private static final byte[] jdoFieldFlags = { 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcategoriaDocente(this).toString();
  }

  public CategoriaDocente getCategoriaDocente()
  {
    return jdoGetcategoriaDocente(this);
  }
  public void setCategoriaDocente(CategoriaDocente categoriaDocente) {
    jdoSetcategoriaDocente(this, categoriaDocente);
  }
  public long getIdJerarquiaCategoriaDocente() {
    return jdoGetidJerarquiaCategoriaDocente(this);
  }
  public void setIdJerarquiaCategoriaDocente(long idJerarquiaCategoriaDocente) {
    jdoSetidJerarquiaCategoriaDocente(this, idJerarquiaCategoriaDocente);
  }
  public JerarquiaDocente getJerarquiaDocente() {
    return jdoGetjerarquiaDocente(this);
  }
  public void setJerarquiaDocente(JerarquiaDocente jerarquiaDocente) {
    jdoSetjerarquiaDocente(this, jerarquiaDocente);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.JerarquiaCategoriaDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new JerarquiaCategoriaDocente());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    JerarquiaCategoriaDocente localJerarquiaCategoriaDocente = new JerarquiaCategoriaDocente();
    localJerarquiaCategoriaDocente.jdoFlags = 1;
    localJerarquiaCategoriaDocente.jdoStateManager = paramStateManager;
    return localJerarquiaCategoriaDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    JerarquiaCategoriaDocente localJerarquiaCategoriaDocente = new JerarquiaCategoriaDocente();
    localJerarquiaCategoriaDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localJerarquiaCategoriaDocente.jdoFlags = 1;
    localJerarquiaCategoriaDocente.jdoStateManager = paramStateManager;
    return localJerarquiaCategoriaDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.categoriaDocente);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idJerarquiaCategoriaDocente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.jerarquiaDocente);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.categoriaDocente = ((CategoriaDocente)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idJerarquiaCategoriaDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.jerarquiaDocente = ((JerarquiaDocente)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(JerarquiaCategoriaDocente paramJerarquiaCategoriaDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramJerarquiaCategoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.categoriaDocente = paramJerarquiaCategoriaDocente.categoriaDocente;
      return;
    case 1:
      if (paramJerarquiaCategoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idJerarquiaCategoriaDocente = paramJerarquiaCategoriaDocente.idJerarquiaCategoriaDocente;
      return;
    case 2:
      if (paramJerarquiaCategoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.jerarquiaDocente = paramJerarquiaCategoriaDocente.jerarquiaDocente;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof JerarquiaCategoriaDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    JerarquiaCategoriaDocente localJerarquiaCategoriaDocente = (JerarquiaCategoriaDocente)paramObject;
    if (localJerarquiaCategoriaDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localJerarquiaCategoriaDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new JerarquiaCategoriaDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new JerarquiaCategoriaDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof JerarquiaCategoriaDocentePK))
      throw new IllegalArgumentException("arg1");
    JerarquiaCategoriaDocentePK localJerarquiaCategoriaDocentePK = (JerarquiaCategoriaDocentePK)paramObject;
    localJerarquiaCategoriaDocentePK.idJerarquiaCategoriaDocente = this.idJerarquiaCategoriaDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof JerarquiaCategoriaDocentePK))
      throw new IllegalArgumentException("arg1");
    JerarquiaCategoriaDocentePK localJerarquiaCategoriaDocentePK = (JerarquiaCategoriaDocentePK)paramObject;
    this.idJerarquiaCategoriaDocente = localJerarquiaCategoriaDocentePK.idJerarquiaCategoriaDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof JerarquiaCategoriaDocentePK))
      throw new IllegalArgumentException("arg2");
    JerarquiaCategoriaDocentePK localJerarquiaCategoriaDocentePK = (JerarquiaCategoriaDocentePK)paramObject;
    localJerarquiaCategoriaDocentePK.idJerarquiaCategoriaDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof JerarquiaCategoriaDocentePK))
      throw new IllegalArgumentException("arg2");
    JerarquiaCategoriaDocentePK localJerarquiaCategoriaDocentePK = (JerarquiaCategoriaDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localJerarquiaCategoriaDocentePK.idJerarquiaCategoriaDocente);
  }

  private static final CategoriaDocente jdoGetcategoriaDocente(JerarquiaCategoriaDocente paramJerarquiaCategoriaDocente)
  {
    StateManager localStateManager = paramJerarquiaCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramJerarquiaCategoriaDocente.categoriaDocente;
    if (localStateManager.isLoaded(paramJerarquiaCategoriaDocente, jdoInheritedFieldCount + 0))
      return paramJerarquiaCategoriaDocente.categoriaDocente;
    return (CategoriaDocente)localStateManager.getObjectField(paramJerarquiaCategoriaDocente, jdoInheritedFieldCount + 0, paramJerarquiaCategoriaDocente.categoriaDocente);
  }

  private static final void jdoSetcategoriaDocente(JerarquiaCategoriaDocente paramJerarquiaCategoriaDocente, CategoriaDocente paramCategoriaDocente)
  {
    StateManager localStateManager = paramJerarquiaCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramJerarquiaCategoriaDocente.categoriaDocente = paramCategoriaDocente;
      return;
    }
    localStateManager.setObjectField(paramJerarquiaCategoriaDocente, jdoInheritedFieldCount + 0, paramJerarquiaCategoriaDocente.categoriaDocente, paramCategoriaDocente);
  }

  private static final long jdoGetidJerarquiaCategoriaDocente(JerarquiaCategoriaDocente paramJerarquiaCategoriaDocente)
  {
    return paramJerarquiaCategoriaDocente.idJerarquiaCategoriaDocente;
  }

  private static final void jdoSetidJerarquiaCategoriaDocente(JerarquiaCategoriaDocente paramJerarquiaCategoriaDocente, long paramLong)
  {
    StateManager localStateManager = paramJerarquiaCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramJerarquiaCategoriaDocente.idJerarquiaCategoriaDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramJerarquiaCategoriaDocente, jdoInheritedFieldCount + 1, paramJerarquiaCategoriaDocente.idJerarquiaCategoriaDocente, paramLong);
  }

  private static final JerarquiaDocente jdoGetjerarquiaDocente(JerarquiaCategoriaDocente paramJerarquiaCategoriaDocente)
  {
    StateManager localStateManager = paramJerarquiaCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramJerarquiaCategoriaDocente.jerarquiaDocente;
    if (localStateManager.isLoaded(paramJerarquiaCategoriaDocente, jdoInheritedFieldCount + 2))
      return paramJerarquiaCategoriaDocente.jerarquiaDocente;
    return (JerarquiaDocente)localStateManager.getObjectField(paramJerarquiaCategoriaDocente, jdoInheritedFieldCount + 2, paramJerarquiaCategoriaDocente.jerarquiaDocente);
  }

  private static final void jdoSetjerarquiaDocente(JerarquiaCategoriaDocente paramJerarquiaCategoriaDocente, JerarquiaDocente paramJerarquiaDocente)
  {
    StateManager localStateManager = paramJerarquiaCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramJerarquiaCategoriaDocente.jerarquiaDocente = paramJerarquiaDocente;
      return;
    }
    localStateManager.setObjectField(paramJerarquiaCategoriaDocente, jdoInheritedFieldCount + 2, paramJerarquiaCategoriaDocente.jerarquiaDocente, paramJerarquiaDocente);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}