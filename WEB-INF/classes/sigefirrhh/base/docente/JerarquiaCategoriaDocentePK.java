package sigefirrhh.base.docente;

import java.io.Serializable;

public class JerarquiaCategoriaDocentePK
  implements Serializable
{
  public long idJerarquiaCategoriaDocente;

  public JerarquiaCategoriaDocentePK()
  {
  }

  public JerarquiaCategoriaDocentePK(long idJerarquiaCategoriaDocente)
  {
    this.idJerarquiaCategoriaDocente = idJerarquiaCategoriaDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((JerarquiaCategoriaDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(JerarquiaCategoriaDocentePK thatPK)
  {
    return 
      this.idJerarquiaCategoriaDocente == thatPK.idJerarquiaCategoriaDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idJerarquiaCategoriaDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idJerarquiaCategoriaDocente);
  }
}