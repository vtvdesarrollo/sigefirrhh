package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TurnoDocente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  protected static final Map LISTA_CONDICION;
  protected static final Map LISTA_TURNO;
  private long idTurnoDocente;
  private String digitoTurno;
  private String nombre;
  private double topeCargaSemanal;
  private String condicion;
  private String turno;
  private String vigencia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "condicion", "digitoTurno", "idTurnoDocente", "nombre", "topeCargaSemanal", "turno", "vigencia" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.TurnoDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TurnoDocente());

    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_CONDICION = 
      new LinkedHashMap();
    LISTA_TURNO = 
      new LinkedHashMap();

    LISTA_CONDICION.put("1", "INTERINO");
    LISTA_CONDICION.put("2", "TITULAR");
    LISTA_TURNO.put("1", "DIURNO");
    LISTA_TURNO.put("2", "NOCTURNO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public TurnoDocente()
  {
    jdoSettopeCargaSemanal(this, 0.0D);

    jdoSetcondicion(this, "1");

    jdoSetturno(this, "1");

    jdoSetvigencia(this, "S");
  }

  public String toString() {
    return jdoGetdigitoTurno(this) + " - " + 
      jdoGetnombre(this);
  }

  public String getDigitoTurno()
  {
    return jdoGetdigitoTurno(this);
  }
  public long getIdTurnoDocente() {
    return jdoGetidTurnoDocente(this);
  }
  public String getNombre() {
    return jdoGetnombre(this);
  }
  public void setDigitoTurno(String string) {
    jdoSetdigitoTurno(this, string);
  }
  public void setIdTurnoDocente(long l) {
    jdoSetidTurnoDocente(this, l);
  }
  public void setNombre(String string) {
    jdoSetnombre(this, string);
  }

  public double getTopeCargaSemanal() {
    return jdoGettopeCargaSemanal(this);
  }
  public void setTopeCargaSemanal(double topeCargaSemanal) {
    jdoSettopeCargaSemanal(this, topeCargaSemanal);
  }
  public String getCondicion() {
    return jdoGetcondicion(this);
  }
  public void setCondicion(String condicion) {
    jdoSetcondicion(this, condicion);
  }
  public String getTurno() {
    return jdoGetturno(this);
  }
  public void setTurno(String turno) {
    jdoSetturno(this, turno);
  }
  public String getVigencia() {
    return jdoGetvigencia(this);
  }
  public void setVigencia(String vigencia) {
    jdoSetvigencia(this, vigencia);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TurnoDocente localTurnoDocente = new TurnoDocente();
    localTurnoDocente.jdoFlags = 1;
    localTurnoDocente.jdoStateManager = paramStateManager;
    return localTurnoDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TurnoDocente localTurnoDocente = new TurnoDocente();
    localTurnoDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTurnoDocente.jdoFlags = 1;
    localTurnoDocente.jdoStateManager = paramStateManager;
    return localTurnoDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.condicion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.digitoTurno);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTurnoDocente);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.topeCargaSemanal);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.turno);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.condicion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.digitoTurno = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTurnoDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.topeCargaSemanal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.turno = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigencia = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TurnoDocente paramTurnoDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTurnoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.condicion = paramTurnoDocente.condicion;
      return;
    case 1:
      if (paramTurnoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.digitoTurno = paramTurnoDocente.digitoTurno;
      return;
    case 2:
      if (paramTurnoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idTurnoDocente = paramTurnoDocente.idTurnoDocente;
      return;
    case 3:
      if (paramTurnoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramTurnoDocente.nombre;
      return;
    case 4:
      if (paramTurnoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.topeCargaSemanal = paramTurnoDocente.topeCargaSemanal;
      return;
    case 5:
      if (paramTurnoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.turno = paramTurnoDocente.turno;
      return;
    case 6:
      if (paramTurnoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.vigencia = paramTurnoDocente.vigencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TurnoDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TurnoDocente localTurnoDocente = (TurnoDocente)paramObject;
    if (localTurnoDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTurnoDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TurnoDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TurnoDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TurnoDocentePK))
      throw new IllegalArgumentException("arg1");
    TurnoDocentePK localTurnoDocentePK = (TurnoDocentePK)paramObject;
    localTurnoDocentePK.idTurnoDocente = this.idTurnoDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TurnoDocentePK))
      throw new IllegalArgumentException("arg1");
    TurnoDocentePK localTurnoDocentePK = (TurnoDocentePK)paramObject;
    this.idTurnoDocente = localTurnoDocentePK.idTurnoDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TurnoDocentePK))
      throw new IllegalArgumentException("arg2");
    TurnoDocentePK localTurnoDocentePK = (TurnoDocentePK)paramObject;
    localTurnoDocentePK.idTurnoDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TurnoDocentePK))
      throw new IllegalArgumentException("arg2");
    TurnoDocentePK localTurnoDocentePK = (TurnoDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localTurnoDocentePK.idTurnoDocente);
  }

  private static final String jdoGetcondicion(TurnoDocente paramTurnoDocente)
  {
    if (paramTurnoDocente.jdoFlags <= 0)
      return paramTurnoDocente.condicion;
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTurnoDocente.condicion;
    if (localStateManager.isLoaded(paramTurnoDocente, jdoInheritedFieldCount + 0))
      return paramTurnoDocente.condicion;
    return localStateManager.getStringField(paramTurnoDocente, jdoInheritedFieldCount + 0, paramTurnoDocente.condicion);
  }

  private static final void jdoSetcondicion(TurnoDocente paramTurnoDocente, String paramString)
  {
    if (paramTurnoDocente.jdoFlags == 0)
    {
      paramTurnoDocente.condicion = paramString;
      return;
    }
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurnoDocente.condicion = paramString;
      return;
    }
    localStateManager.setStringField(paramTurnoDocente, jdoInheritedFieldCount + 0, paramTurnoDocente.condicion, paramString);
  }

  private static final String jdoGetdigitoTurno(TurnoDocente paramTurnoDocente)
  {
    if (paramTurnoDocente.jdoFlags <= 0)
      return paramTurnoDocente.digitoTurno;
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTurnoDocente.digitoTurno;
    if (localStateManager.isLoaded(paramTurnoDocente, jdoInheritedFieldCount + 1))
      return paramTurnoDocente.digitoTurno;
    return localStateManager.getStringField(paramTurnoDocente, jdoInheritedFieldCount + 1, paramTurnoDocente.digitoTurno);
  }

  private static final void jdoSetdigitoTurno(TurnoDocente paramTurnoDocente, String paramString)
  {
    if (paramTurnoDocente.jdoFlags == 0)
    {
      paramTurnoDocente.digitoTurno = paramString;
      return;
    }
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurnoDocente.digitoTurno = paramString;
      return;
    }
    localStateManager.setStringField(paramTurnoDocente, jdoInheritedFieldCount + 1, paramTurnoDocente.digitoTurno, paramString);
  }

  private static final long jdoGetidTurnoDocente(TurnoDocente paramTurnoDocente)
  {
    return paramTurnoDocente.idTurnoDocente;
  }

  private static final void jdoSetidTurnoDocente(TurnoDocente paramTurnoDocente, long paramLong)
  {
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurnoDocente.idTurnoDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramTurnoDocente, jdoInheritedFieldCount + 2, paramTurnoDocente.idTurnoDocente, paramLong);
  }

  private static final String jdoGetnombre(TurnoDocente paramTurnoDocente)
  {
    if (paramTurnoDocente.jdoFlags <= 0)
      return paramTurnoDocente.nombre;
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTurnoDocente.nombre;
    if (localStateManager.isLoaded(paramTurnoDocente, jdoInheritedFieldCount + 3))
      return paramTurnoDocente.nombre;
    return localStateManager.getStringField(paramTurnoDocente, jdoInheritedFieldCount + 3, paramTurnoDocente.nombre);
  }

  private static final void jdoSetnombre(TurnoDocente paramTurnoDocente, String paramString)
  {
    if (paramTurnoDocente.jdoFlags == 0)
    {
      paramTurnoDocente.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurnoDocente.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTurnoDocente, jdoInheritedFieldCount + 3, paramTurnoDocente.nombre, paramString);
  }

  private static final double jdoGettopeCargaSemanal(TurnoDocente paramTurnoDocente)
  {
    if (paramTurnoDocente.jdoFlags <= 0)
      return paramTurnoDocente.topeCargaSemanal;
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTurnoDocente.topeCargaSemanal;
    if (localStateManager.isLoaded(paramTurnoDocente, jdoInheritedFieldCount + 4))
      return paramTurnoDocente.topeCargaSemanal;
    return localStateManager.getDoubleField(paramTurnoDocente, jdoInheritedFieldCount + 4, paramTurnoDocente.topeCargaSemanal);
  }

  private static final void jdoSettopeCargaSemanal(TurnoDocente paramTurnoDocente, double paramDouble)
  {
    if (paramTurnoDocente.jdoFlags == 0)
    {
      paramTurnoDocente.topeCargaSemanal = paramDouble;
      return;
    }
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurnoDocente.topeCargaSemanal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTurnoDocente, jdoInheritedFieldCount + 4, paramTurnoDocente.topeCargaSemanal, paramDouble);
  }

  private static final String jdoGetturno(TurnoDocente paramTurnoDocente)
  {
    if (paramTurnoDocente.jdoFlags <= 0)
      return paramTurnoDocente.turno;
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTurnoDocente.turno;
    if (localStateManager.isLoaded(paramTurnoDocente, jdoInheritedFieldCount + 5))
      return paramTurnoDocente.turno;
    return localStateManager.getStringField(paramTurnoDocente, jdoInheritedFieldCount + 5, paramTurnoDocente.turno);
  }

  private static final void jdoSetturno(TurnoDocente paramTurnoDocente, String paramString)
  {
    if (paramTurnoDocente.jdoFlags == 0)
    {
      paramTurnoDocente.turno = paramString;
      return;
    }
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurnoDocente.turno = paramString;
      return;
    }
    localStateManager.setStringField(paramTurnoDocente, jdoInheritedFieldCount + 5, paramTurnoDocente.turno, paramString);
  }

  private static final String jdoGetvigencia(TurnoDocente paramTurnoDocente)
  {
    if (paramTurnoDocente.jdoFlags <= 0)
      return paramTurnoDocente.vigencia;
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTurnoDocente.vigencia;
    if (localStateManager.isLoaded(paramTurnoDocente, jdoInheritedFieldCount + 6))
      return paramTurnoDocente.vigencia;
    return localStateManager.getStringField(paramTurnoDocente, jdoInheritedFieldCount + 6, paramTurnoDocente.vigencia);
  }

  private static final void jdoSetvigencia(TurnoDocente paramTurnoDocente, String paramString)
  {
    if (paramTurnoDocente.jdoFlags == 0)
    {
      paramTurnoDocente.vigencia = paramString;
      return;
    }
    StateManager localStateManager = paramTurnoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTurnoDocente.vigencia = paramString;
      return;
    }
    localStateManager.setStringField(paramTurnoDocente, jdoInheritedFieldCount + 6, paramTurnoDocente.vigencia, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}