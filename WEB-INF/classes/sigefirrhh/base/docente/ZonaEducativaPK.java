package sigefirrhh.base.docente;

import java.io.Serializable;

public class ZonaEducativaPK
  implements Serializable
{
  public long idZonaEducativa;

  public ZonaEducativaPK()
  {
  }

  public ZonaEducativaPK(long idZonaEducativa)
  {
    this.idZonaEducativa = idZonaEducativa;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ZonaEducativaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ZonaEducativaPK thatPK)
  {
    return 
      this.idZonaEducativa == thatPK.idZonaEducativa;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idZonaEducativa)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idZonaEducativa);
  }
}