package sigefirrhh.base.docente;

import java.io.Serializable;

public class NivelPlantelActualPK
  implements Serializable
{
  public long idNivelPlantelActual;

  public NivelPlantelActualPK()
  {
  }

  public NivelPlantelActualPK(long idNivelPlantelActual)
  {
    this.idNivelPlantelActual = idNivelPlantelActual;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((NivelPlantelActualPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(NivelPlantelActualPK thatPK)
  {
    return 
      this.idNivelPlantelActual == thatPK.idNivelPlantelActual;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idNivelPlantelActual)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idNivelPlantelActual);
  }
}