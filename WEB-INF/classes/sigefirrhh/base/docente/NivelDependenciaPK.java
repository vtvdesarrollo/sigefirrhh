package sigefirrhh.base.docente;

import java.io.Serializable;

public class NivelDependenciaPK
  implements Serializable
{
  public long idNivelDependencia;

  public NivelDependenciaPK()
  {
  }

  public NivelDependenciaPK(long idNivelDependencia)
  {
    this.idNivelDependencia = idNivelDependencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((NivelDependenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(NivelDependenciaPK thatPK)
  {
    return 
      this.idNivelDependencia == thatPK.idNivelDependencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idNivelDependencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idNivelDependencia);
  }
}