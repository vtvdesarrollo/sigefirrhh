package sigefirrhh.base.docente;

import java.io.Serializable;

public class TurnoDependenciaPK
  implements Serializable
{
  public long idTurnoDependencia;

  public TurnoDependenciaPK()
  {
  }

  public TurnoDependenciaPK(long idTurnoDependencia)
  {
    this.idTurnoDependencia = idTurnoDependencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TurnoDependenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TurnoDependenciaPK thatPK)
  {
    return 
      this.idTurnoDependencia == thatPK.idTurnoDependencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTurnoDependencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTurnoDependencia);
  }
}