package sigefirrhh.base.docente;

import java.io.Serializable;

public class TurnoDocentePK
  implements Serializable
{
  public long idTurnoDocente;

  public TurnoDocentePK()
  {
  }

  public TurnoDocentePK(long idTurnoDocente)
  {
    this.idTurnoDocente = idTurnoDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TurnoDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TurnoDocentePK thatPK)
  {
    return 
      this.idTurnoDocente == thatPK.idTurnoDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTurnoDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTurnoDocente);
  }
}