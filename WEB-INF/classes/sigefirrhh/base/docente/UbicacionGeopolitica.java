package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class UbicacionGeopolitica
  implements Serializable, PersistenceCapable
{
  private long idUbicacionGeopolitica;
  private int codUbicacionGeop;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codUbicacionGeop", "idUbicacionGeopolitica", "nombre" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getCodUbicacionGeop()
  {
    return jdoGetcodUbicacionGeop(this);
  }

  public long getIdUbicacionGeopolitica()
  {
    return jdoGetidUbicacionGeopolitica(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodUbicacionGeop(int i)
  {
    jdoSetcodUbicacionGeop(this, i);
  }

  public void setIdUbicacionGeopolitica(long l)
  {
    jdoSetidUbicacionGeopolitica(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.UbicacionGeopolitica"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UbicacionGeopolitica());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UbicacionGeopolitica localUbicacionGeopolitica = new UbicacionGeopolitica();
    localUbicacionGeopolitica.jdoFlags = 1;
    localUbicacionGeopolitica.jdoStateManager = paramStateManager;
    return localUbicacionGeopolitica;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UbicacionGeopolitica localUbicacionGeopolitica = new UbicacionGeopolitica();
    localUbicacionGeopolitica.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUbicacionGeopolitica.jdoFlags = 1;
    localUbicacionGeopolitica.jdoStateManager = paramStateManager;
    return localUbicacionGeopolitica;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codUbicacionGeop);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUbicacionGeopolitica);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codUbicacionGeop = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUbicacionGeopolitica = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UbicacionGeopolitica paramUbicacionGeopolitica, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUbicacionGeopolitica == null)
        throw new IllegalArgumentException("arg1");
      this.codUbicacionGeop = paramUbicacionGeopolitica.codUbicacionGeop;
      return;
    case 1:
      if (paramUbicacionGeopolitica == null)
        throw new IllegalArgumentException("arg1");
      this.idUbicacionGeopolitica = paramUbicacionGeopolitica.idUbicacionGeopolitica;
      return;
    case 2:
      if (paramUbicacionGeopolitica == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramUbicacionGeopolitica.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UbicacionGeopolitica))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UbicacionGeopolitica localUbicacionGeopolitica = (UbicacionGeopolitica)paramObject;
    if (localUbicacionGeopolitica.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUbicacionGeopolitica, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UbicacionGeopoliticaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UbicacionGeopoliticaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UbicacionGeopoliticaPK))
      throw new IllegalArgumentException("arg1");
    UbicacionGeopoliticaPK localUbicacionGeopoliticaPK = (UbicacionGeopoliticaPK)paramObject;
    localUbicacionGeopoliticaPK.idUbicacionGeopolitica = this.idUbicacionGeopolitica;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UbicacionGeopoliticaPK))
      throw new IllegalArgumentException("arg1");
    UbicacionGeopoliticaPK localUbicacionGeopoliticaPK = (UbicacionGeopoliticaPK)paramObject;
    this.idUbicacionGeopolitica = localUbicacionGeopoliticaPK.idUbicacionGeopolitica;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UbicacionGeopoliticaPK))
      throw new IllegalArgumentException("arg2");
    UbicacionGeopoliticaPK localUbicacionGeopoliticaPK = (UbicacionGeopoliticaPK)paramObject;
    localUbicacionGeopoliticaPK.idUbicacionGeopolitica = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UbicacionGeopoliticaPK))
      throw new IllegalArgumentException("arg2");
    UbicacionGeopoliticaPK localUbicacionGeopoliticaPK = (UbicacionGeopoliticaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localUbicacionGeopoliticaPK.idUbicacionGeopolitica);
  }

  private static final int jdoGetcodUbicacionGeop(UbicacionGeopolitica paramUbicacionGeopolitica)
  {
    if (paramUbicacionGeopolitica.jdoFlags <= 0)
      return paramUbicacionGeopolitica.codUbicacionGeop;
    StateManager localStateManager = paramUbicacionGeopolitica.jdoStateManager;
    if (localStateManager == null)
      return paramUbicacionGeopolitica.codUbicacionGeop;
    if (localStateManager.isLoaded(paramUbicacionGeopolitica, jdoInheritedFieldCount + 0))
      return paramUbicacionGeopolitica.codUbicacionGeop;
    return localStateManager.getIntField(paramUbicacionGeopolitica, jdoInheritedFieldCount + 0, paramUbicacionGeopolitica.codUbicacionGeop);
  }

  private static final void jdoSetcodUbicacionGeop(UbicacionGeopolitica paramUbicacionGeopolitica, int paramInt)
  {
    if (paramUbicacionGeopolitica.jdoFlags == 0)
    {
      paramUbicacionGeopolitica.codUbicacionGeop = paramInt;
      return;
    }
    StateManager localStateManager = paramUbicacionGeopolitica.jdoStateManager;
    if (localStateManager == null)
    {
      paramUbicacionGeopolitica.codUbicacionGeop = paramInt;
      return;
    }
    localStateManager.setIntField(paramUbicacionGeopolitica, jdoInheritedFieldCount + 0, paramUbicacionGeopolitica.codUbicacionGeop, paramInt);
  }

  private static final long jdoGetidUbicacionGeopolitica(UbicacionGeopolitica paramUbicacionGeopolitica)
  {
    return paramUbicacionGeopolitica.idUbicacionGeopolitica;
  }

  private static final void jdoSetidUbicacionGeopolitica(UbicacionGeopolitica paramUbicacionGeopolitica, long paramLong)
  {
    StateManager localStateManager = paramUbicacionGeopolitica.jdoStateManager;
    if (localStateManager == null)
    {
      paramUbicacionGeopolitica.idUbicacionGeopolitica = paramLong;
      return;
    }
    localStateManager.setLongField(paramUbicacionGeopolitica, jdoInheritedFieldCount + 1, paramUbicacionGeopolitica.idUbicacionGeopolitica, paramLong);
  }

  private static final String jdoGetnombre(UbicacionGeopolitica paramUbicacionGeopolitica)
  {
    if (paramUbicacionGeopolitica.jdoFlags <= 0)
      return paramUbicacionGeopolitica.nombre;
    StateManager localStateManager = paramUbicacionGeopolitica.jdoStateManager;
    if (localStateManager == null)
      return paramUbicacionGeopolitica.nombre;
    if (localStateManager.isLoaded(paramUbicacionGeopolitica, jdoInheritedFieldCount + 2))
      return paramUbicacionGeopolitica.nombre;
    return localStateManager.getStringField(paramUbicacionGeopolitica, jdoInheritedFieldCount + 2, paramUbicacionGeopolitica.nombre);
  }

  private static final void jdoSetnombre(UbicacionGeopolitica paramUbicacionGeopolitica, String paramString)
  {
    if (paramUbicacionGeopolitica.jdoFlags == 0)
    {
      paramUbicacionGeopolitica.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramUbicacionGeopolitica.jdoStateManager;
    if (localStateManager == null)
    {
      paramUbicacionGeopolitica.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramUbicacionGeopolitica, jdoInheritedFieldCount + 2, paramUbicacionGeopolitica.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}