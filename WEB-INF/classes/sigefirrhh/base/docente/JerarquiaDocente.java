package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class JerarquiaDocente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idJerarquiaDocente;
  private String digitoJerarquia;
  private String nombre;
  private String vigencia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "digitoJerarquia", "idJerarquiaDocente", "nombre", "vigencia" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.JerarquiaDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new JerarquiaDocente());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public JerarquiaDocente()
  {
    jdoSetvigencia(this, "S");
  }
  public String toString() { return jdoGetdigitoJerarquia(this) + " - " + 
      jdoGetnombre(this);
  }

  public String getDigitoJerarquia()
  {
    return jdoGetdigitoJerarquia(this);
  }

  public long getIdJerarquiaDocente()
  {
    return jdoGetidJerarquiaDocente(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setDigitoJerarquia(String string)
  {
    jdoSetdigitoJerarquia(this, string);
  }

  public void setIdJerarquiaDocente(long l)
  {
    jdoSetidJerarquiaDocente(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public String getVigencia() {
    return jdoGetvigencia(this);
  }
  public void setVigencia(String vigencia) {
    jdoSetvigencia(this, vigencia);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    JerarquiaDocente localJerarquiaDocente = new JerarquiaDocente();
    localJerarquiaDocente.jdoFlags = 1;
    localJerarquiaDocente.jdoStateManager = paramStateManager;
    return localJerarquiaDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    JerarquiaDocente localJerarquiaDocente = new JerarquiaDocente();
    localJerarquiaDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localJerarquiaDocente.jdoFlags = 1;
    localJerarquiaDocente.jdoStateManager = paramStateManager;
    return localJerarquiaDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.digitoJerarquia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idJerarquiaDocente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.digitoJerarquia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idJerarquiaDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigencia = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(JerarquiaDocente paramJerarquiaDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramJerarquiaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.digitoJerarquia = paramJerarquiaDocente.digitoJerarquia;
      return;
    case 1:
      if (paramJerarquiaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idJerarquiaDocente = paramJerarquiaDocente.idJerarquiaDocente;
      return;
    case 2:
      if (paramJerarquiaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramJerarquiaDocente.nombre;
      return;
    case 3:
      if (paramJerarquiaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.vigencia = paramJerarquiaDocente.vigencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof JerarquiaDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    JerarquiaDocente localJerarquiaDocente = (JerarquiaDocente)paramObject;
    if (localJerarquiaDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localJerarquiaDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new JerarquiaDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new JerarquiaDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof JerarquiaDocentePK))
      throw new IllegalArgumentException("arg1");
    JerarquiaDocentePK localJerarquiaDocentePK = (JerarquiaDocentePK)paramObject;
    localJerarquiaDocentePK.idJerarquiaDocente = this.idJerarquiaDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof JerarquiaDocentePK))
      throw new IllegalArgumentException("arg1");
    JerarquiaDocentePK localJerarquiaDocentePK = (JerarquiaDocentePK)paramObject;
    this.idJerarquiaDocente = localJerarquiaDocentePK.idJerarquiaDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof JerarquiaDocentePK))
      throw new IllegalArgumentException("arg2");
    JerarquiaDocentePK localJerarquiaDocentePK = (JerarquiaDocentePK)paramObject;
    localJerarquiaDocentePK.idJerarquiaDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof JerarquiaDocentePK))
      throw new IllegalArgumentException("arg2");
    JerarquiaDocentePK localJerarquiaDocentePK = (JerarquiaDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localJerarquiaDocentePK.idJerarquiaDocente);
  }

  private static final String jdoGetdigitoJerarquia(JerarquiaDocente paramJerarquiaDocente)
  {
    if (paramJerarquiaDocente.jdoFlags <= 0)
      return paramJerarquiaDocente.digitoJerarquia;
    StateManager localStateManager = paramJerarquiaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramJerarquiaDocente.digitoJerarquia;
    if (localStateManager.isLoaded(paramJerarquiaDocente, jdoInheritedFieldCount + 0))
      return paramJerarquiaDocente.digitoJerarquia;
    return localStateManager.getStringField(paramJerarquiaDocente, jdoInheritedFieldCount + 0, paramJerarquiaDocente.digitoJerarquia);
  }

  private static final void jdoSetdigitoJerarquia(JerarquiaDocente paramJerarquiaDocente, String paramString)
  {
    if (paramJerarquiaDocente.jdoFlags == 0)
    {
      paramJerarquiaDocente.digitoJerarquia = paramString;
      return;
    }
    StateManager localStateManager = paramJerarquiaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramJerarquiaDocente.digitoJerarquia = paramString;
      return;
    }
    localStateManager.setStringField(paramJerarquiaDocente, jdoInheritedFieldCount + 0, paramJerarquiaDocente.digitoJerarquia, paramString);
  }

  private static final long jdoGetidJerarquiaDocente(JerarquiaDocente paramJerarquiaDocente)
  {
    return paramJerarquiaDocente.idJerarquiaDocente;
  }

  private static final void jdoSetidJerarquiaDocente(JerarquiaDocente paramJerarquiaDocente, long paramLong)
  {
    StateManager localStateManager = paramJerarquiaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramJerarquiaDocente.idJerarquiaDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramJerarquiaDocente, jdoInheritedFieldCount + 1, paramJerarquiaDocente.idJerarquiaDocente, paramLong);
  }

  private static final String jdoGetnombre(JerarquiaDocente paramJerarquiaDocente)
  {
    if (paramJerarquiaDocente.jdoFlags <= 0)
      return paramJerarquiaDocente.nombre;
    StateManager localStateManager = paramJerarquiaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramJerarquiaDocente.nombre;
    if (localStateManager.isLoaded(paramJerarquiaDocente, jdoInheritedFieldCount + 2))
      return paramJerarquiaDocente.nombre;
    return localStateManager.getStringField(paramJerarquiaDocente, jdoInheritedFieldCount + 2, paramJerarquiaDocente.nombre);
  }

  private static final void jdoSetnombre(JerarquiaDocente paramJerarquiaDocente, String paramString)
  {
    if (paramJerarquiaDocente.jdoFlags == 0)
    {
      paramJerarquiaDocente.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramJerarquiaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramJerarquiaDocente.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramJerarquiaDocente, jdoInheritedFieldCount + 2, paramJerarquiaDocente.nombre, paramString);
  }

  private static final String jdoGetvigencia(JerarquiaDocente paramJerarquiaDocente)
  {
    if (paramJerarquiaDocente.jdoFlags <= 0)
      return paramJerarquiaDocente.vigencia;
    StateManager localStateManager = paramJerarquiaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramJerarquiaDocente.vigencia;
    if (localStateManager.isLoaded(paramJerarquiaDocente, jdoInheritedFieldCount + 3))
      return paramJerarquiaDocente.vigencia;
    return localStateManager.getStringField(paramJerarquiaDocente, jdoInheritedFieldCount + 3, paramJerarquiaDocente.vigencia);
  }

  private static final void jdoSetvigencia(JerarquiaDocente paramJerarquiaDocente, String paramString)
  {
    if (paramJerarquiaDocente.jdoFlags == 0)
    {
      paramJerarquiaDocente.vigencia = paramString;
      return;
    }
    StateManager localStateManager = paramJerarquiaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramJerarquiaDocente.vigencia = paramString;
      return;
    }
    localStateManager.setStringField(paramJerarquiaDocente, jdoInheritedFieldCount + 3, paramJerarquiaDocente.vigencia, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}