package sigefirrhh.base.docente;

import java.io.Serializable;

public class AreaGeograficaPK
  implements Serializable
{
  public long idAreaGeografica;

  public AreaGeograficaPK()
  {
  }

  public AreaGeograficaPK(long idAreaGeografica)
  {
    this.idAreaGeografica = idAreaGeografica;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AreaGeograficaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AreaGeograficaPK thatPK)
  {
    return 
      this.idAreaGeografica == thatPK.idAreaGeografica;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAreaGeografica)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAreaGeografica);
  }
}