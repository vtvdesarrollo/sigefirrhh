package sigefirrhh.base.docente;

import java.io.Serializable;

public class PlantelActualPK
  implements Serializable
{
  public long idPlantelActual;

  public PlantelActualPK()
  {
  }

  public PlantelActualPK(long idPlantelActual)
  {
    this.idPlantelActual = idPlantelActual;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PlantelActualPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PlantelActualPK thatPK)
  {
    return 
      this.idPlantelActual == thatPK.idPlantelActual;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPlantelActual)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPlantelActual);
  }
}