package sigefirrhh.base.docente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class AreaGeograficaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(AreaGeograficaForm.class.getName());
  private AreaGeografica areaGeografica;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DocenteFacade docenteFacade = new DocenteFacade();
  private boolean showAreaGeograficaByCodAreaGeografica;
  private boolean showAreaGeograficaByNombre;
  private String findCodAreaGeografica;
  private String findNombre;
  private Object stateResultAreaGeograficaByCodAreaGeografica = null;

  private Object stateResultAreaGeograficaByNombre = null;

  public String getFindCodAreaGeografica()
  {
    return this.findCodAreaGeografica;
  }
  public void setFindCodAreaGeografica(String findCodAreaGeografica) {
    this.findCodAreaGeografica = findCodAreaGeografica;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public AreaGeografica getAreaGeografica() {
    if (this.areaGeografica == null) {
      this.areaGeografica = new AreaGeografica();
    }
    return this.areaGeografica;
  }

  public AreaGeograficaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findAreaGeograficaByCodAreaGeografica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findAreaGeograficaByCodAreaGeografica(this.findCodAreaGeografica);
      this.showAreaGeograficaByCodAreaGeografica = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAreaGeograficaByCodAreaGeografica)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodAreaGeografica = null;
    this.findNombre = null;

    return null;
  }

  public String findAreaGeograficaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findAreaGeograficaByNombre(this.findNombre);
      this.showAreaGeograficaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAreaGeograficaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodAreaGeografica = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowAreaGeograficaByCodAreaGeografica() {
    return this.showAreaGeograficaByCodAreaGeografica;
  }
  public boolean isShowAreaGeograficaByNombre() {
    return this.showAreaGeograficaByNombre;
  }

  public String selectAreaGeografica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idAreaGeografica = 
      Long.parseLong((String)requestParameterMap.get("idAreaGeografica"));
    try
    {
      this.areaGeografica = 
        this.docenteFacade.findAreaGeograficaById(
        idAreaGeografica);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.areaGeografica = null;
    this.showAreaGeograficaByCodAreaGeografica = false;
    this.showAreaGeograficaByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.docenteFacade.addAreaGeografica(
          this.areaGeografica);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.docenteFacade.updateAreaGeografica(
          this.areaGeografica);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.docenteFacade.deleteAreaGeografica(
        this.areaGeografica);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.areaGeografica = new AreaGeografica();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.areaGeografica.setIdAreaGeografica(identityGenerator.getNextSequenceNumber("sigefirrhh.base.docente.AreaGeografica"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.areaGeografica = new AreaGeografica();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}