package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class SeguridadDocente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idSeguridadDocente;
  private int mes;
  private int anio;
  private String cerrado;
  private Date fechaCierre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "cerrado", "fechaCierre", "idSeguridadDocente", "mes" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.SeguridadDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SeguridadDocente());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public String toString()
  {
    return jdoGetmes(this) + " - " + LISTA_SINO.get(jdoGetcerrado(this));
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }

  public String getCerrado() {
    return jdoGetcerrado(this);
  }

  public void setCerrado(String cerrado) {
    jdoSetcerrado(this, cerrado);
  }

  public Date getFechaCierre() {
    return jdoGetfechaCierre(this);
  }

  public void setFechaCierre(Date fechaCierre) {
    jdoSetfechaCierre(this, fechaCierre);
  }

  public long getIdSeguridadDocente()
  {
    return jdoGetidSeguridadDocente(this);
  }

  public void setIdSeguridadDocente(long idSeguridadDocente) {
    jdoSetidSeguridadDocente(this, idSeguridadDocente);
  }

  public int getMes() {
    return jdoGetmes(this);
  }

  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SeguridadDocente localSeguridadDocente = new SeguridadDocente();
    localSeguridadDocente.jdoFlags = 1;
    localSeguridadDocente.jdoStateManager = paramStateManager;
    return localSeguridadDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SeguridadDocente localSeguridadDocente = new SeguridadDocente();
    localSeguridadDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSeguridadDocente.jdoFlags = 1;
    localSeguridadDocente.jdoStateManager = paramStateManager;
    return localSeguridadDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cerrado);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCierre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSeguridadDocente);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cerrado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCierre = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSeguridadDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SeguridadDocente paramSeguridadDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSeguridadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramSeguridadDocente.anio;
      return;
    case 1:
      if (paramSeguridadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.cerrado = paramSeguridadDocente.cerrado;
      return;
    case 2:
      if (paramSeguridadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCierre = paramSeguridadDocente.fechaCierre;
      return;
    case 3:
      if (paramSeguridadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idSeguridadDocente = paramSeguridadDocente.idSeguridadDocente;
      return;
    case 4:
      if (paramSeguridadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramSeguridadDocente.mes;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SeguridadDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SeguridadDocente localSeguridadDocente = (SeguridadDocente)paramObject;
    if (localSeguridadDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSeguridadDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SeguridadDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SeguridadDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadDocentePK))
      throw new IllegalArgumentException("arg1");
    SeguridadDocentePK localSeguridadDocentePK = (SeguridadDocentePK)paramObject;
    localSeguridadDocentePK.idSeguridadDocente = this.idSeguridadDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadDocentePK))
      throw new IllegalArgumentException("arg1");
    SeguridadDocentePK localSeguridadDocentePK = (SeguridadDocentePK)paramObject;
    this.idSeguridadDocente = localSeguridadDocentePK.idSeguridadDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadDocentePK))
      throw new IllegalArgumentException("arg2");
    SeguridadDocentePK localSeguridadDocentePK = (SeguridadDocentePK)paramObject;
    localSeguridadDocentePK.idSeguridadDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadDocentePK))
      throw new IllegalArgumentException("arg2");
    SeguridadDocentePK localSeguridadDocentePK = (SeguridadDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localSeguridadDocentePK.idSeguridadDocente);
  }

  private static final int jdoGetanio(SeguridadDocente paramSeguridadDocente)
  {
    if (paramSeguridadDocente.jdoFlags <= 0)
      return paramSeguridadDocente.anio;
    StateManager localStateManager = paramSeguridadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadDocente.anio;
    if (localStateManager.isLoaded(paramSeguridadDocente, jdoInheritedFieldCount + 0))
      return paramSeguridadDocente.anio;
    return localStateManager.getIntField(paramSeguridadDocente, jdoInheritedFieldCount + 0, paramSeguridadDocente.anio);
  }

  private static final void jdoSetanio(SeguridadDocente paramSeguridadDocente, int paramInt)
  {
    if (paramSeguridadDocente.jdoFlags == 0)
    {
      paramSeguridadDocente.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadDocente.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadDocente, jdoInheritedFieldCount + 0, paramSeguridadDocente.anio, paramInt);
  }

  private static final String jdoGetcerrado(SeguridadDocente paramSeguridadDocente)
  {
    if (paramSeguridadDocente.jdoFlags <= 0)
      return paramSeguridadDocente.cerrado;
    StateManager localStateManager = paramSeguridadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadDocente.cerrado;
    if (localStateManager.isLoaded(paramSeguridadDocente, jdoInheritedFieldCount + 1))
      return paramSeguridadDocente.cerrado;
    return localStateManager.getStringField(paramSeguridadDocente, jdoInheritedFieldCount + 1, paramSeguridadDocente.cerrado);
  }

  private static final void jdoSetcerrado(SeguridadDocente paramSeguridadDocente, String paramString)
  {
    if (paramSeguridadDocente.jdoFlags == 0)
    {
      paramSeguridadDocente.cerrado = paramString;
      return;
    }
    StateManager localStateManager = paramSeguridadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadDocente.cerrado = paramString;
      return;
    }
    localStateManager.setStringField(paramSeguridadDocente, jdoInheritedFieldCount + 1, paramSeguridadDocente.cerrado, paramString);
  }

  private static final Date jdoGetfechaCierre(SeguridadDocente paramSeguridadDocente)
  {
    if (paramSeguridadDocente.jdoFlags <= 0)
      return paramSeguridadDocente.fechaCierre;
    StateManager localStateManager = paramSeguridadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadDocente.fechaCierre;
    if (localStateManager.isLoaded(paramSeguridadDocente, jdoInheritedFieldCount + 2))
      return paramSeguridadDocente.fechaCierre;
    return (Date)localStateManager.getObjectField(paramSeguridadDocente, jdoInheritedFieldCount + 2, paramSeguridadDocente.fechaCierre);
  }

  private static final void jdoSetfechaCierre(SeguridadDocente paramSeguridadDocente, Date paramDate)
  {
    if (paramSeguridadDocente.jdoFlags == 0)
    {
      paramSeguridadDocente.fechaCierre = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadDocente.fechaCierre = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadDocente, jdoInheritedFieldCount + 2, paramSeguridadDocente.fechaCierre, paramDate);
  }

  private static final long jdoGetidSeguridadDocente(SeguridadDocente paramSeguridadDocente)
  {
    return paramSeguridadDocente.idSeguridadDocente;
  }

  private static final void jdoSetidSeguridadDocente(SeguridadDocente paramSeguridadDocente, long paramLong)
  {
    StateManager localStateManager = paramSeguridadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadDocente.idSeguridadDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramSeguridadDocente, jdoInheritedFieldCount + 3, paramSeguridadDocente.idSeguridadDocente, paramLong);
  }

  private static final int jdoGetmes(SeguridadDocente paramSeguridadDocente)
  {
    if (paramSeguridadDocente.jdoFlags <= 0)
      return paramSeguridadDocente.mes;
    StateManager localStateManager = paramSeguridadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadDocente.mes;
    if (localStateManager.isLoaded(paramSeguridadDocente, jdoInheritedFieldCount + 4))
      return paramSeguridadDocente.mes;
    return localStateManager.getIntField(paramSeguridadDocente, jdoInheritedFieldCount + 4, paramSeguridadDocente.mes);
  }

  private static final void jdoSetmes(SeguridadDocente paramSeguridadDocente, int paramInt)
  {
    if (paramSeguridadDocente.jdoFlags == 0)
    {
      paramSeguridadDocente.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadDocente.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadDocente, jdoInheritedFieldCount + 4, paramSeguridadDocente.mes, paramInt);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}