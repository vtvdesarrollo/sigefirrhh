package sigefirrhh.base.docente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class GradoNivelDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GradoNivelDocenteForm.class.getName());
  private GradoNivelDocente gradoNivelDocente;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DocenteFacade docenteFacade = new DocenteFacade();
  private boolean showGradoNivelDocenteByGradoDocente;
  private String findSelectGradoDocente;
  private Collection findColGradoDocente;
  private Collection colGradoDocente;
  private Collection colNivelDocente;
  private String selectGradoDocente;
  private String selectNivelDocente;
  private Object stateResultGradoNivelDocenteByGradoDocente = null;

  public String getFindSelectGradoDocente()
  {
    return this.findSelectGradoDocente;
  }
  public void setFindSelectGradoDocente(String valGradoDocente) {
    this.findSelectGradoDocente = valGradoDocente;
  }

  public Collection getFindColGradoDocente() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColGradoDocente.iterator();
    GradoDocente gradoDocente = null;
    while (iterator.hasNext()) {
      gradoDocente = (GradoDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(gradoDocente.getIdGradoDocente()), 
        gradoDocente.toString()));
    }
    return col;
  }

  public String getSelectGradoDocente()
  {
    return this.selectGradoDocente;
  }
  public void setSelectGradoDocente(String valGradoDocente) {
    Iterator iterator = this.colGradoDocente.iterator();
    GradoDocente gradoDocente = null;
    this.gradoNivelDocente.setGradoDocente(null);
    while (iterator.hasNext()) {
      gradoDocente = (GradoDocente)iterator.next();
      if (String.valueOf(gradoDocente.getIdGradoDocente()).equals(
        valGradoDocente)) {
        this.gradoNivelDocente.setGradoDocente(
          gradoDocente);
        break;
      }
    }
    this.selectGradoDocente = valGradoDocente;
  }
  public String getSelectNivelDocente() {
    return this.selectNivelDocente;
  }
  public void setSelectNivelDocente(String valNivelDocente) {
    Iterator iterator = this.colNivelDocente.iterator();
    NivelDocente nivelDocente = null;
    this.gradoNivelDocente.setNivelDocente(null);
    while (iterator.hasNext()) {
      nivelDocente = (NivelDocente)iterator.next();
      if (String.valueOf(nivelDocente.getIdNivelDocente()).equals(
        valNivelDocente)) {
        this.gradoNivelDocente.setNivelDocente(
          nivelDocente);
        break;
      }
    }
    this.selectNivelDocente = valNivelDocente;
  }
  public Collection getResult() {
    return this.result;
  }

  public GradoNivelDocente getGradoNivelDocente() {
    if (this.gradoNivelDocente == null) {
      this.gradoNivelDocente = new GradoNivelDocente();
    }
    return this.gradoNivelDocente;
  }

  public GradoNivelDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColGradoDocente()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGradoDocente.iterator();
    GradoDocente gradoDocente = null;
    while (iterator.hasNext()) {
      gradoDocente = (GradoDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(gradoDocente.getIdGradoDocente()), 
        gradoDocente.toString()));
    }
    return col;
  }

  public Collection getColNivelDocente()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colNivelDocente.iterator();
    NivelDocente nivelDocente = null;
    while (iterator.hasNext()) {
      nivelDocente = (NivelDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nivelDocente.getIdNivelDocente()), 
        nivelDocente.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColGradoDocente = 
        this.docenteFacade.findAllGradoDocente();

      this.colGradoDocente = 
        this.docenteFacade.findAllGradoDocente();
      this.colNivelDocente = 
        this.docenteFacade.findAllNivelDocente();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findGradoNivelDocenteByGradoDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findGradoNivelDocenteByGradoDocente(Long.valueOf(this.findSelectGradoDocente).longValue());
      this.showGradoNivelDocenteByGradoDocente = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGradoNivelDocenteByGradoDocente)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGradoDocente = null;

    return null;
  }

  public boolean isShowGradoNivelDocenteByGradoDocente() {
    return this.showGradoNivelDocenteByGradoDocente;
  }

  public String selectGradoNivelDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectGradoDocente = null;
    this.selectNivelDocente = null;

    long idGradoNivelDocente = 
      Long.parseLong((String)requestParameterMap.get("idGradoNivelDocente"));
    try
    {
      this.gradoNivelDocente = 
        this.docenteFacade.findGradoNivelDocenteById(
        idGradoNivelDocente);
      if (this.gradoNivelDocente.getGradoDocente() != null) {
        this.selectGradoDocente = 
          String.valueOf(this.gradoNivelDocente.getGradoDocente().getIdGradoDocente());
      }
      if (this.gradoNivelDocente.getNivelDocente() != null) {
        this.selectNivelDocente = 
          String.valueOf(this.gradoNivelDocente.getNivelDocente().getIdNivelDocente());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.gradoNivelDocente = null;
    this.showGradoNivelDocenteByGradoDocente = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.docenteFacade.addGradoNivelDocente(
          this.gradoNivelDocente);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.docenteFacade.updateGradoNivelDocente(
          this.gradoNivelDocente);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.docenteFacade.deleteGradoNivelDocente(
        this.gradoNivelDocente);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.gradoNivelDocente = new GradoNivelDocente();

    this.selectGradoDocente = null;

    this.selectNivelDocente = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.gradoNivelDocente.setIdGradoNivelDocente(identityGenerator.getNextSequenceNumber("sigefirrhh.base.docente.GradoNivelDocente"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.gradoNivelDocente = new GradoNivelDocente();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}