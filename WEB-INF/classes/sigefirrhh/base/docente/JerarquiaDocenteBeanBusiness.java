package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class JerarquiaDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addJerarquiaDocente(JerarquiaDocente jerarquiaDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    JerarquiaDocente jerarquiaDocenteNew = 
      (JerarquiaDocente)BeanUtils.cloneBean(
      jerarquiaDocente);

    pm.makePersistent(jerarquiaDocenteNew);
  }

  public void updateJerarquiaDocente(JerarquiaDocente jerarquiaDocente) throws Exception
  {
    JerarquiaDocente jerarquiaDocenteModify = 
      findJerarquiaDocenteById(jerarquiaDocente.getIdJerarquiaDocente());

    BeanUtils.copyProperties(jerarquiaDocenteModify, jerarquiaDocente);
  }

  public void deleteJerarquiaDocente(JerarquiaDocente jerarquiaDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    JerarquiaDocente jerarquiaDocenteDelete = 
      findJerarquiaDocenteById(jerarquiaDocente.getIdJerarquiaDocente());
    pm.deletePersistent(jerarquiaDocenteDelete);
  }

  public JerarquiaDocente findJerarquiaDocenteById(long idJerarquiaDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idJerarquiaDocente == pIdJerarquiaDocente";
    Query query = pm.newQuery(JerarquiaDocente.class, filter);

    query.declareParameters("long pIdJerarquiaDocente");

    parameters.put("pIdJerarquiaDocente", new Long(idJerarquiaDocente));

    Collection colJerarquiaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colJerarquiaDocente.iterator();
    return (JerarquiaDocente)iterator.next();
  }

  public Collection findJerarquiaDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent jerarquiaDocenteExtent = pm.getExtent(
      JerarquiaDocente.class, true);
    Query query = pm.newQuery(jerarquiaDocenteExtent);
    query.setOrdering("digitoJerarquia ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByDigitoJerarquia(String digitoJerarquia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "digitoJerarquia == pDigitoJerarquia";

    Query query = pm.newQuery(JerarquiaDocente.class, filter);

    query.declareParameters("java.lang.String pDigitoJerarquia");
    HashMap parameters = new HashMap();

    parameters.put("pDigitoJerarquia", new String(digitoJerarquia));

    query.setOrdering("digitoJerarquia ascending");

    Collection colJerarquiaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colJerarquiaDocente);

    return colJerarquiaDocente;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(JerarquiaDocente.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("digitoJerarquia ascending");

    Collection colJerarquiaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colJerarquiaDocente);

    return colJerarquiaDocente;
  }
}