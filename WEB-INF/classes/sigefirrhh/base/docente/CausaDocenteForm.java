package sigefirrhh.base.docente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class CausaDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CausaDocenteForm.class.getName());
  private CausaDocente causaDocente;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DocenteFacade docenteFacade = new DocenteFacade();
  private boolean showCausaDocenteByCodCausaDocente;
  private boolean showCausaDocenteByNombre;
  private String findCodCausaDocente;
  private String findNombre;
  private Object stateResultCausaDocenteByCodCausaDocente = null;

  private Object stateResultCausaDocenteByNombre = null;

  public String getFindCodCausaDocente()
  {
    return this.findCodCausaDocente;
  }
  public void setFindCodCausaDocente(String findCodCausaDocente) {
    this.findCodCausaDocente = findCodCausaDocente;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public CausaDocente getCausaDocente() {
    if (this.causaDocente == null) {
      this.causaDocente = new CausaDocente();
    }
    return this.causaDocente;
  }

  public CausaDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListTipo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = CausaDocente.LISTA_TIPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findCausaDocenteByCodCausaDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findCausaDocenteByCodCausaDocente(this.findCodCausaDocente);
      this.showCausaDocenteByCodCausaDocente = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCausaDocenteByCodCausaDocente)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodCausaDocente = null;
    this.findNombre = null;

    return null;
  }

  public String findCausaDocenteByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findCausaDocenteByNombre(this.findNombre);
      this.showCausaDocenteByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCausaDocenteByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodCausaDocente = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowCausaDocenteByCodCausaDocente() {
    return this.showCausaDocenteByCodCausaDocente;
  }
  public boolean isShowCausaDocenteByNombre() {
    return this.showCausaDocenteByNombre;
  }

  public String selectCausaDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idCausaDocente = 
      Long.parseLong((String)requestParameterMap.get("idCausaDocente"));
    try
    {
      this.causaDocente = 
        this.docenteFacade.findCausaDocenteById(
        idCausaDocente);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.causaDocente = null;
    this.showCausaDocenteByCodCausaDocente = false;
    this.showCausaDocenteByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.docenteFacade.addCausaDocente(
          this.causaDocente);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.docenteFacade.updateCausaDocente(
          this.causaDocente);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.docenteFacade.deleteCausaDocente(
        this.causaDocente);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.causaDocente = new CausaDocente();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.causaDocente.setIdCausaDocente(identityGenerator.getNextSequenceNumber("sigefirrhh.base.docente.CausaDocente"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.causaDocente = new CausaDocente();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}