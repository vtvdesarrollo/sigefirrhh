package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class GradoNivelDocente
  implements Serializable, PersistenceCapable
{
  private long idGradoNivelDocente;
  private GradoDocente gradoDocente;
  private NivelDocente nivelDocente;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "gradoDocente", "idGradoNivelDocente", "nivelDocente" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.docente.GradoDocente"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.docente.NivelDocente") };
  private static final byte[] jdoFieldFlags = { 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnivelDocente(this).toString();
  }

  public GradoDocente getGradoDocente()
  {
    return jdoGetgradoDocente(this);
  }
  public void setGradoDocente(GradoDocente gradoDocente) {
    jdoSetgradoDocente(this, gradoDocente);
  }
  public long getIdGradoNivelDocente() {
    return jdoGetidGradoNivelDocente(this);
  }
  public void setIdGradoNivelDocente(long idGradoNivelDocente) {
    jdoSetidGradoNivelDocente(this, idGradoNivelDocente);
  }
  public NivelDocente getNivelDocente() {
    return jdoGetnivelDocente(this);
  }
  public void setNivelDocente(NivelDocente nivelDocente) {
    jdoSetnivelDocente(this, nivelDocente);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.GradoNivelDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new GradoNivelDocente());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    GradoNivelDocente localGradoNivelDocente = new GradoNivelDocente();
    localGradoNivelDocente.jdoFlags = 1;
    localGradoNivelDocente.jdoStateManager = paramStateManager;
    return localGradoNivelDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    GradoNivelDocente localGradoNivelDocente = new GradoNivelDocente();
    localGradoNivelDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localGradoNivelDocente.jdoFlags = 1;
    localGradoNivelDocente.jdoStateManager = paramStateManager;
    return localGradoNivelDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.gradoDocente);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idGradoNivelDocente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nivelDocente);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gradoDocente = ((GradoDocente)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idGradoNivelDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelDocente = ((NivelDocente)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(GradoNivelDocente paramGradoNivelDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramGradoNivelDocente == null)
        throw new IllegalArgumentException("arg1");
      this.gradoDocente = paramGradoNivelDocente.gradoDocente;
      return;
    case 1:
      if (paramGradoNivelDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idGradoNivelDocente = paramGradoNivelDocente.idGradoNivelDocente;
      return;
    case 2:
      if (paramGradoNivelDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nivelDocente = paramGradoNivelDocente.nivelDocente;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof GradoNivelDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    GradoNivelDocente localGradoNivelDocente = (GradoNivelDocente)paramObject;
    if (localGradoNivelDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localGradoNivelDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new GradoNivelDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new GradoNivelDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GradoNivelDocentePK))
      throw new IllegalArgumentException("arg1");
    GradoNivelDocentePK localGradoNivelDocentePK = (GradoNivelDocentePK)paramObject;
    localGradoNivelDocentePK.idGradoNivelDocente = this.idGradoNivelDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GradoNivelDocentePK))
      throw new IllegalArgumentException("arg1");
    GradoNivelDocentePK localGradoNivelDocentePK = (GradoNivelDocentePK)paramObject;
    this.idGradoNivelDocente = localGradoNivelDocentePK.idGradoNivelDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GradoNivelDocentePK))
      throw new IllegalArgumentException("arg2");
    GradoNivelDocentePK localGradoNivelDocentePK = (GradoNivelDocentePK)paramObject;
    localGradoNivelDocentePK.idGradoNivelDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GradoNivelDocentePK))
      throw new IllegalArgumentException("arg2");
    GradoNivelDocentePK localGradoNivelDocentePK = (GradoNivelDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localGradoNivelDocentePK.idGradoNivelDocente);
  }

  private static final GradoDocente jdoGetgradoDocente(GradoNivelDocente paramGradoNivelDocente)
  {
    StateManager localStateManager = paramGradoNivelDocente.jdoStateManager;
    if (localStateManager == null)
      return paramGradoNivelDocente.gradoDocente;
    if (localStateManager.isLoaded(paramGradoNivelDocente, jdoInheritedFieldCount + 0))
      return paramGradoNivelDocente.gradoDocente;
    return (GradoDocente)localStateManager.getObjectField(paramGradoNivelDocente, jdoInheritedFieldCount + 0, paramGradoNivelDocente.gradoDocente);
  }

  private static final void jdoSetgradoDocente(GradoNivelDocente paramGradoNivelDocente, GradoDocente paramGradoDocente)
  {
    StateManager localStateManager = paramGradoNivelDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramGradoNivelDocente.gradoDocente = paramGradoDocente;
      return;
    }
    localStateManager.setObjectField(paramGradoNivelDocente, jdoInheritedFieldCount + 0, paramGradoNivelDocente.gradoDocente, paramGradoDocente);
  }

  private static final long jdoGetidGradoNivelDocente(GradoNivelDocente paramGradoNivelDocente)
  {
    return paramGradoNivelDocente.idGradoNivelDocente;
  }

  private static final void jdoSetidGradoNivelDocente(GradoNivelDocente paramGradoNivelDocente, long paramLong)
  {
    StateManager localStateManager = paramGradoNivelDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramGradoNivelDocente.idGradoNivelDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramGradoNivelDocente, jdoInheritedFieldCount + 1, paramGradoNivelDocente.idGradoNivelDocente, paramLong);
  }

  private static final NivelDocente jdoGetnivelDocente(GradoNivelDocente paramGradoNivelDocente)
  {
    StateManager localStateManager = paramGradoNivelDocente.jdoStateManager;
    if (localStateManager == null)
      return paramGradoNivelDocente.nivelDocente;
    if (localStateManager.isLoaded(paramGradoNivelDocente, jdoInheritedFieldCount + 2))
      return paramGradoNivelDocente.nivelDocente;
    return (NivelDocente)localStateManager.getObjectField(paramGradoNivelDocente, jdoInheritedFieldCount + 2, paramGradoNivelDocente.nivelDocente);
  }

  private static final void jdoSetnivelDocente(GradoNivelDocente paramGradoNivelDocente, NivelDocente paramNivelDocente)
  {
    StateManager localStateManager = paramGradoNivelDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramGradoNivelDocente.nivelDocente = paramNivelDocente;
      return;
    }
    localStateManager.setObjectField(paramGradoNivelDocente, jdoInheritedFieldCount + 2, paramGradoNivelDocente.nivelDocente, paramNivelDocente);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}