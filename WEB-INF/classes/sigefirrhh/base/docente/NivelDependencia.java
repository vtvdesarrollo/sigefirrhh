package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class NivelDependencia
  implements Serializable, PersistenceCapable
{
  private long idNivelDependencia;
  private int codNivelDependencia;
  private String nombre;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codNivelDependencia", "idNivelDependencia", "nombre", "organismo" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getCodNivelDependencia()
  {
    return jdoGetcodNivelDependencia(this);
  }

  public long getIdNivelDependencia()
  {
    return jdoGetidNivelDependencia(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setCodNivelDependencia(int i)
  {
    jdoSetcodNivelDependencia(this, i);
  }

  public void setIdNivelDependencia(long l)
  {
    jdoSetidNivelDependencia(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.NivelDependencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new NivelDependencia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    NivelDependencia localNivelDependencia = new NivelDependencia();
    localNivelDependencia.jdoFlags = 1;
    localNivelDependencia.jdoStateManager = paramStateManager;
    return localNivelDependencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    NivelDependencia localNivelDependencia = new NivelDependencia();
    localNivelDependencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localNivelDependencia.jdoFlags = 1;
    localNivelDependencia.jdoStateManager = paramStateManager;
    return localNivelDependencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codNivelDependencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idNivelDependencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codNivelDependencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idNivelDependencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(NivelDependencia paramNivelDependencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramNivelDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.codNivelDependencia = paramNivelDependencia.codNivelDependencia;
      return;
    case 1:
      if (paramNivelDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.idNivelDependencia = paramNivelDependencia.idNivelDependencia;
      return;
    case 2:
      if (paramNivelDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramNivelDependencia.nombre;
      return;
    case 3:
      if (paramNivelDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramNivelDependencia.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof NivelDependencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    NivelDependencia localNivelDependencia = (NivelDependencia)paramObject;
    if (localNivelDependencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localNivelDependencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new NivelDependenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new NivelDependenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NivelDependenciaPK))
      throw new IllegalArgumentException("arg1");
    NivelDependenciaPK localNivelDependenciaPK = (NivelDependenciaPK)paramObject;
    localNivelDependenciaPK.idNivelDependencia = this.idNivelDependencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NivelDependenciaPK))
      throw new IllegalArgumentException("arg1");
    NivelDependenciaPK localNivelDependenciaPK = (NivelDependenciaPK)paramObject;
    this.idNivelDependencia = localNivelDependenciaPK.idNivelDependencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NivelDependenciaPK))
      throw new IllegalArgumentException("arg2");
    NivelDependenciaPK localNivelDependenciaPK = (NivelDependenciaPK)paramObject;
    localNivelDependenciaPK.idNivelDependencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NivelDependenciaPK))
      throw new IllegalArgumentException("arg2");
    NivelDependenciaPK localNivelDependenciaPK = (NivelDependenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localNivelDependenciaPK.idNivelDependencia);
  }

  private static final int jdoGetcodNivelDependencia(NivelDependencia paramNivelDependencia)
  {
    if (paramNivelDependencia.jdoFlags <= 0)
      return paramNivelDependencia.codNivelDependencia;
    StateManager localStateManager = paramNivelDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramNivelDependencia.codNivelDependencia;
    if (localStateManager.isLoaded(paramNivelDependencia, jdoInheritedFieldCount + 0))
      return paramNivelDependencia.codNivelDependencia;
    return localStateManager.getIntField(paramNivelDependencia, jdoInheritedFieldCount + 0, paramNivelDependencia.codNivelDependencia);
  }

  private static final void jdoSetcodNivelDependencia(NivelDependencia paramNivelDependencia, int paramInt)
  {
    if (paramNivelDependencia.jdoFlags == 0)
    {
      paramNivelDependencia.codNivelDependencia = paramInt;
      return;
    }
    StateManager localStateManager = paramNivelDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelDependencia.codNivelDependencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramNivelDependencia, jdoInheritedFieldCount + 0, paramNivelDependencia.codNivelDependencia, paramInt);
  }

  private static final long jdoGetidNivelDependencia(NivelDependencia paramNivelDependencia)
  {
    return paramNivelDependencia.idNivelDependencia;
  }

  private static final void jdoSetidNivelDependencia(NivelDependencia paramNivelDependencia, long paramLong)
  {
    StateManager localStateManager = paramNivelDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelDependencia.idNivelDependencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramNivelDependencia, jdoInheritedFieldCount + 1, paramNivelDependencia.idNivelDependencia, paramLong);
  }

  private static final String jdoGetnombre(NivelDependencia paramNivelDependencia)
  {
    if (paramNivelDependencia.jdoFlags <= 0)
      return paramNivelDependencia.nombre;
    StateManager localStateManager = paramNivelDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramNivelDependencia.nombre;
    if (localStateManager.isLoaded(paramNivelDependencia, jdoInheritedFieldCount + 2))
      return paramNivelDependencia.nombre;
    return localStateManager.getStringField(paramNivelDependencia, jdoInheritedFieldCount + 2, paramNivelDependencia.nombre);
  }

  private static final void jdoSetnombre(NivelDependencia paramNivelDependencia, String paramString)
  {
    if (paramNivelDependencia.jdoFlags == 0)
    {
      paramNivelDependencia.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramNivelDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelDependencia.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramNivelDependencia, jdoInheritedFieldCount + 2, paramNivelDependencia.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(NivelDependencia paramNivelDependencia)
  {
    StateManager localStateManager = paramNivelDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramNivelDependencia.organismo;
    if (localStateManager.isLoaded(paramNivelDependencia, jdoInheritedFieldCount + 3))
      return paramNivelDependencia.organismo;
    return (Organismo)localStateManager.getObjectField(paramNivelDependencia, jdoInheritedFieldCount + 3, paramNivelDependencia.organismo);
  }

  private static final void jdoSetorganismo(NivelDependencia paramNivelDependencia, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramNivelDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelDependencia.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramNivelDependencia, jdoInheritedFieldCount + 3, paramNivelDependencia.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}