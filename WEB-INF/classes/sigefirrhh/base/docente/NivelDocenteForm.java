package sigefirrhh.base.docente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class NivelDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(NivelDocenteForm.class.getName());
  private NivelDocente nivelDocente;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DocenteFacade docenteFacade = new DocenteFacade();
  private boolean showNivelDocenteByDigitoNivel;
  private boolean showNivelDocenteByNombre;
  private String findDigitoNivel;
  private String findNombre;
  private Object stateResultNivelDocenteByDigitoNivel = null;

  private Object stateResultNivelDocenteByNombre = null;

  public String getFindDigitoNivel()
  {
    return this.findDigitoNivel;
  }
  public void setFindDigitoNivel(String findDigitoNivel) {
    this.findDigitoNivel = findDigitoNivel;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public NivelDocente getNivelDocente() {
    if (this.nivelDocente == null) {
      this.nivelDocente = new NivelDocente();
    }
    return this.nivelDocente;
  }

  public NivelDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListVigencia()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = NivelDocente.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findNivelDocenteByDigitoNivel()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findNivelDocenteByDigitoNivel(this.findDigitoNivel);
      this.showNivelDocenteByDigitoNivel = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showNivelDocenteByDigitoNivel)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findDigitoNivel = null;
    this.findNombre = null;

    return null;
  }

  public String findNivelDocenteByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findNivelDocenteByNombre(this.findNombre);
      this.showNivelDocenteByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showNivelDocenteByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findDigitoNivel = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowNivelDocenteByDigitoNivel() {
    return this.showNivelDocenteByDigitoNivel;
  }
  public boolean isShowNivelDocenteByNombre() {
    return this.showNivelDocenteByNombre;
  }

  public String selectNivelDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idNivelDocente = 
      Long.parseLong((String)requestParameterMap.get("idNivelDocente"));
    try
    {
      this.nivelDocente = 
        this.docenteFacade.findNivelDocenteById(
        idNivelDocente);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.nivelDocente = null;
    this.showNivelDocenteByDigitoNivel = false;
    this.showNivelDocenteByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.docenteFacade.addNivelDocente(
          this.nivelDocente);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.docenteFacade.updateNivelDocente(
          this.nivelDocente);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.docenteFacade.deleteNivelDocente(
        this.nivelDocente);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.nivelDocente = new NivelDocente();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.nivelDocente.setIdNivelDocente(identityGenerator.getNextSequenceNumber("sigefirrhh.base.docente.NivelDocente"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.nivelDocente = new NivelDocente();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}