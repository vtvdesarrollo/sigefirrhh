package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class GradoDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addGradoDocente(GradoDocente gradoDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    GradoDocente gradoDocenteNew = 
      (GradoDocente)BeanUtils.cloneBean(
      gradoDocente);

    pm.makePersistent(gradoDocenteNew);
  }

  public void updateGradoDocente(GradoDocente gradoDocente) throws Exception
  {
    GradoDocente gradoDocenteModify = 
      findGradoDocenteById(gradoDocente.getIdGradoDocente());

    BeanUtils.copyProperties(gradoDocenteModify, gradoDocente);
  }

  public void deleteGradoDocente(GradoDocente gradoDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    GradoDocente gradoDocenteDelete = 
      findGradoDocenteById(gradoDocente.getIdGradoDocente());
    pm.deletePersistent(gradoDocenteDelete);
  }

  public GradoDocente findGradoDocenteById(long idGradoDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idGradoDocente == pIdGradoDocente";
    Query query = pm.newQuery(GradoDocente.class, filter);

    query.declareParameters("long pIdGradoDocente");

    parameters.put("pIdGradoDocente", new Long(idGradoDocente));

    Collection colGradoDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colGradoDocente.iterator();
    return (GradoDocente)iterator.next();
  }

  public Collection findGradoDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent gradoDocenteExtent = pm.getExtent(
      GradoDocente.class, true);
    Query query = pm.newQuery(gradoDocenteExtent);
    query.setOrdering("digitoGrado ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByDigitoGrado(String digitoGrado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "digitoGrado == pDigitoGrado";

    Query query = pm.newQuery(GradoDocente.class, filter);

    query.declareParameters("java.lang.String pDigitoGrado");
    HashMap parameters = new HashMap();

    parameters.put("pDigitoGrado", new String(digitoGrado));

    query.setOrdering("digitoGrado ascending");

    Collection colGradoDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGradoDocente);

    return colGradoDocente;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(GradoDocente.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("digitoGrado ascending");

    Collection colGradoDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGradoDocente);

    return colGradoDocente;
  }
}