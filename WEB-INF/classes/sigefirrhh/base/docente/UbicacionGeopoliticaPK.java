package sigefirrhh.base.docente;

import java.io.Serializable;

public class UbicacionGeopoliticaPK
  implements Serializable
{
  public long idUbicacionGeopolitica;

  public UbicacionGeopoliticaPK()
  {
  }

  public UbicacionGeopoliticaPK(long idUbicacionGeopolitica)
  {
    this.idUbicacionGeopolitica = idUbicacionGeopolitica;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UbicacionGeopoliticaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UbicacionGeopoliticaPK thatPK)
  {
    return 
      this.idUbicacionGeopolitica == thatPK.idUbicacionGeopolitica;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUbicacionGeopolitica)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUbicacionGeopolitica);
  }
}