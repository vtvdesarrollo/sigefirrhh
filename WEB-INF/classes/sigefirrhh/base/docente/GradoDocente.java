package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class GradoDocente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idGradoDocente;
  private String digitoGrado;
  private String nombre;
  private String vigencia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "digitoGrado", "idGradoDocente", "nombre", "vigencia" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.GradoDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new GradoDocente());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public GradoDocente()
  {
    jdoSetvigencia(this, "S");
  }
  public String toString() { return jdoGetdigitoGrado(this) + " - " + 
      jdoGetnombre(this);
  }

  public String getDigitoGrado()
  {
    return jdoGetdigitoGrado(this);
  }

  public long getIdGradoDocente()
  {
    return jdoGetidGradoDocente(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setDigitoGrado(String string)
  {
    jdoSetdigitoGrado(this, string);
  }

  public void setIdGradoDocente(long l)
  {
    jdoSetidGradoDocente(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public String getVigencia() {
    return jdoGetvigencia(this);
  }
  public void setVigencia(String vigencia) {
    jdoSetvigencia(this, vigencia);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    GradoDocente localGradoDocente = new GradoDocente();
    localGradoDocente.jdoFlags = 1;
    localGradoDocente.jdoStateManager = paramStateManager;
    return localGradoDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    GradoDocente localGradoDocente = new GradoDocente();
    localGradoDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localGradoDocente.jdoFlags = 1;
    localGradoDocente.jdoStateManager = paramStateManager;
    return localGradoDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.digitoGrado);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idGradoDocente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.digitoGrado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idGradoDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigencia = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(GradoDocente paramGradoDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramGradoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.digitoGrado = paramGradoDocente.digitoGrado;
      return;
    case 1:
      if (paramGradoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idGradoDocente = paramGradoDocente.idGradoDocente;
      return;
    case 2:
      if (paramGradoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramGradoDocente.nombre;
      return;
    case 3:
      if (paramGradoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.vigencia = paramGradoDocente.vigencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof GradoDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    GradoDocente localGradoDocente = (GradoDocente)paramObject;
    if (localGradoDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localGradoDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new GradoDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new GradoDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GradoDocentePK))
      throw new IllegalArgumentException("arg1");
    GradoDocentePK localGradoDocentePK = (GradoDocentePK)paramObject;
    localGradoDocentePK.idGradoDocente = this.idGradoDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GradoDocentePK))
      throw new IllegalArgumentException("arg1");
    GradoDocentePK localGradoDocentePK = (GradoDocentePK)paramObject;
    this.idGradoDocente = localGradoDocentePK.idGradoDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GradoDocentePK))
      throw new IllegalArgumentException("arg2");
    GradoDocentePK localGradoDocentePK = (GradoDocentePK)paramObject;
    localGradoDocentePK.idGradoDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GradoDocentePK))
      throw new IllegalArgumentException("arg2");
    GradoDocentePK localGradoDocentePK = (GradoDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localGradoDocentePK.idGradoDocente);
  }

  private static final String jdoGetdigitoGrado(GradoDocente paramGradoDocente)
  {
    if (paramGradoDocente.jdoFlags <= 0)
      return paramGradoDocente.digitoGrado;
    StateManager localStateManager = paramGradoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramGradoDocente.digitoGrado;
    if (localStateManager.isLoaded(paramGradoDocente, jdoInheritedFieldCount + 0))
      return paramGradoDocente.digitoGrado;
    return localStateManager.getStringField(paramGradoDocente, jdoInheritedFieldCount + 0, paramGradoDocente.digitoGrado);
  }

  private static final void jdoSetdigitoGrado(GradoDocente paramGradoDocente, String paramString)
  {
    if (paramGradoDocente.jdoFlags == 0)
    {
      paramGradoDocente.digitoGrado = paramString;
      return;
    }
    StateManager localStateManager = paramGradoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramGradoDocente.digitoGrado = paramString;
      return;
    }
    localStateManager.setStringField(paramGradoDocente, jdoInheritedFieldCount + 0, paramGradoDocente.digitoGrado, paramString);
  }

  private static final long jdoGetidGradoDocente(GradoDocente paramGradoDocente)
  {
    return paramGradoDocente.idGradoDocente;
  }

  private static final void jdoSetidGradoDocente(GradoDocente paramGradoDocente, long paramLong)
  {
    StateManager localStateManager = paramGradoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramGradoDocente.idGradoDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramGradoDocente, jdoInheritedFieldCount + 1, paramGradoDocente.idGradoDocente, paramLong);
  }

  private static final String jdoGetnombre(GradoDocente paramGradoDocente)
  {
    if (paramGradoDocente.jdoFlags <= 0)
      return paramGradoDocente.nombre;
    StateManager localStateManager = paramGradoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramGradoDocente.nombre;
    if (localStateManager.isLoaded(paramGradoDocente, jdoInheritedFieldCount + 2))
      return paramGradoDocente.nombre;
    return localStateManager.getStringField(paramGradoDocente, jdoInheritedFieldCount + 2, paramGradoDocente.nombre);
  }

  private static final void jdoSetnombre(GradoDocente paramGradoDocente, String paramString)
  {
    if (paramGradoDocente.jdoFlags == 0)
    {
      paramGradoDocente.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramGradoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramGradoDocente.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramGradoDocente, jdoInheritedFieldCount + 2, paramGradoDocente.nombre, paramString);
  }

  private static final String jdoGetvigencia(GradoDocente paramGradoDocente)
  {
    if (paramGradoDocente.jdoFlags <= 0)
      return paramGradoDocente.vigencia;
    StateManager localStateManager = paramGradoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramGradoDocente.vigencia;
    if (localStateManager.isLoaded(paramGradoDocente, jdoInheritedFieldCount + 3))
      return paramGradoDocente.vigencia;
    return localStateManager.getStringField(paramGradoDocente, jdoInheritedFieldCount + 3, paramGradoDocente.vigencia);
  }

  private static final void jdoSetvigencia(GradoDocente paramGradoDocente, String paramString)
  {
    if (paramGradoDocente.jdoFlags == 0)
    {
      paramGradoDocente.vigencia = paramString;
      return;
    }
    StateManager localStateManager = paramGradoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramGradoDocente.vigencia = paramString;
      return;
    }
    localStateManager.setStringField(paramGradoDocente, jdoInheritedFieldCount + 3, paramGradoDocente.vigencia, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}