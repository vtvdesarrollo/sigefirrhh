package sigefirrhh.base.docente;

import java.io.Serializable;

public class GradoDocentePK
  implements Serializable
{
  public long idGradoDocente;

  public GradoDocentePK()
  {
  }

  public GradoDocentePK(long idGradoDocente)
  {
    this.idGradoDocente = idGradoDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((GradoDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(GradoDocentePK thatPK)
  {
    return 
      this.idGradoDocente == thatPK.idGradoDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idGradoDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idGradoDocente);
  }
}