package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CategoriaDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCategoriaDocente(CategoriaDocente categoriaDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CategoriaDocente categoriaDocenteNew = 
      (CategoriaDocente)BeanUtils.cloneBean(
      categoriaDocente);

    pm.makePersistent(categoriaDocenteNew);
  }

  public void updateCategoriaDocente(CategoriaDocente categoriaDocente) throws Exception
  {
    CategoriaDocente categoriaDocenteModify = 
      findCategoriaDocenteById(categoriaDocente.getIdCategoriaDocente());

    BeanUtils.copyProperties(categoriaDocenteModify, categoriaDocente);
  }

  public void deleteCategoriaDocente(CategoriaDocente categoriaDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CategoriaDocente categoriaDocenteDelete = 
      findCategoriaDocenteById(categoriaDocente.getIdCategoriaDocente());
    pm.deletePersistent(categoriaDocenteDelete);
  }

  public CategoriaDocente findCategoriaDocenteById(long idCategoriaDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCategoriaDocente == pIdCategoriaDocente";
    Query query = pm.newQuery(CategoriaDocente.class, filter);

    query.declareParameters("long pIdCategoriaDocente");

    parameters.put("pIdCategoriaDocente", new Long(idCategoriaDocente));

    Collection colCategoriaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCategoriaDocente.iterator();
    return (CategoriaDocente)iterator.next();
  }

  public Collection findCategoriaDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent categoriaDocenteExtent = pm.getExtent(
      CategoriaDocente.class, true);
    Query query = pm.newQuery(categoriaDocenteExtent);
    query.setOrdering("digitoCategoria ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByDigitoCategoria(String digitoCategoria)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "digitoCategoria == pDigitoCategoria";

    Query query = pm.newQuery(CategoriaDocente.class, filter);

    query.declareParameters("java.lang.String pDigitoCategoria");
    HashMap parameters = new HashMap();

    parameters.put("pDigitoCategoria", new String(digitoCategoria));

    query.setOrdering("digitoCategoria ascending");

    Collection colCategoriaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCategoriaDocente);

    return colCategoriaDocente;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(CategoriaDocente.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("digitoCategoria ascending");

    Collection colCategoriaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCategoriaDocente);

    return colCategoriaDocente;
  }
}