package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class NivelPlantelActual
  implements Serializable, PersistenceCapable
{
  private long idNivelPlantelActual;
  private int digitoNivel;
  private String nombre;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "digitoNivel", "idNivelPlantelActual", "nombre", "organismo" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getDigitoNivel()
  {
    return jdoGetdigitoNivel(this);
  }

  public long getIdNivelPlantelActual()
  {
    return jdoGetidNivelPlantelActual(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setDigitoNivel(int i)
  {
    jdoSetdigitoNivel(this, i);
  }

  public void setIdNivelPlantelActual(long l)
  {
    jdoSetidNivelPlantelActual(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.NivelPlantelActual"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new NivelPlantelActual());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    NivelPlantelActual localNivelPlantelActual = new NivelPlantelActual();
    localNivelPlantelActual.jdoFlags = 1;
    localNivelPlantelActual.jdoStateManager = paramStateManager;
    return localNivelPlantelActual;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    NivelPlantelActual localNivelPlantelActual = new NivelPlantelActual();
    localNivelPlantelActual.jdoCopyKeyFieldsFromObjectId(paramObject);
    localNivelPlantelActual.jdoFlags = 1;
    localNivelPlantelActual.jdoStateManager = paramStateManager;
    return localNivelPlantelActual;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.digitoNivel);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idNivelPlantelActual);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.digitoNivel = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idNivelPlantelActual = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(NivelPlantelActual paramNivelPlantelActual, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramNivelPlantelActual == null)
        throw new IllegalArgumentException("arg1");
      this.digitoNivel = paramNivelPlantelActual.digitoNivel;
      return;
    case 1:
      if (paramNivelPlantelActual == null)
        throw new IllegalArgumentException("arg1");
      this.idNivelPlantelActual = paramNivelPlantelActual.idNivelPlantelActual;
      return;
    case 2:
      if (paramNivelPlantelActual == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramNivelPlantelActual.nombre;
      return;
    case 3:
      if (paramNivelPlantelActual == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramNivelPlantelActual.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof NivelPlantelActual))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    NivelPlantelActual localNivelPlantelActual = (NivelPlantelActual)paramObject;
    if (localNivelPlantelActual.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localNivelPlantelActual, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new NivelPlantelActualPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new NivelPlantelActualPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NivelPlantelActualPK))
      throw new IllegalArgumentException("arg1");
    NivelPlantelActualPK localNivelPlantelActualPK = (NivelPlantelActualPK)paramObject;
    localNivelPlantelActualPK.idNivelPlantelActual = this.idNivelPlantelActual;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NivelPlantelActualPK))
      throw new IllegalArgumentException("arg1");
    NivelPlantelActualPK localNivelPlantelActualPK = (NivelPlantelActualPK)paramObject;
    this.idNivelPlantelActual = localNivelPlantelActualPK.idNivelPlantelActual;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NivelPlantelActualPK))
      throw new IllegalArgumentException("arg2");
    NivelPlantelActualPK localNivelPlantelActualPK = (NivelPlantelActualPK)paramObject;
    localNivelPlantelActualPK.idNivelPlantelActual = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NivelPlantelActualPK))
      throw new IllegalArgumentException("arg2");
    NivelPlantelActualPK localNivelPlantelActualPK = (NivelPlantelActualPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localNivelPlantelActualPK.idNivelPlantelActual);
  }

  private static final int jdoGetdigitoNivel(NivelPlantelActual paramNivelPlantelActual)
  {
    if (paramNivelPlantelActual.jdoFlags <= 0)
      return paramNivelPlantelActual.digitoNivel;
    StateManager localStateManager = paramNivelPlantelActual.jdoStateManager;
    if (localStateManager == null)
      return paramNivelPlantelActual.digitoNivel;
    if (localStateManager.isLoaded(paramNivelPlantelActual, jdoInheritedFieldCount + 0))
      return paramNivelPlantelActual.digitoNivel;
    return localStateManager.getIntField(paramNivelPlantelActual, jdoInheritedFieldCount + 0, paramNivelPlantelActual.digitoNivel);
  }

  private static final void jdoSetdigitoNivel(NivelPlantelActual paramNivelPlantelActual, int paramInt)
  {
    if (paramNivelPlantelActual.jdoFlags == 0)
    {
      paramNivelPlantelActual.digitoNivel = paramInt;
      return;
    }
    StateManager localStateManager = paramNivelPlantelActual.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelPlantelActual.digitoNivel = paramInt;
      return;
    }
    localStateManager.setIntField(paramNivelPlantelActual, jdoInheritedFieldCount + 0, paramNivelPlantelActual.digitoNivel, paramInt);
  }

  private static final long jdoGetidNivelPlantelActual(NivelPlantelActual paramNivelPlantelActual)
  {
    return paramNivelPlantelActual.idNivelPlantelActual;
  }

  private static final void jdoSetidNivelPlantelActual(NivelPlantelActual paramNivelPlantelActual, long paramLong)
  {
    StateManager localStateManager = paramNivelPlantelActual.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelPlantelActual.idNivelPlantelActual = paramLong;
      return;
    }
    localStateManager.setLongField(paramNivelPlantelActual, jdoInheritedFieldCount + 1, paramNivelPlantelActual.idNivelPlantelActual, paramLong);
  }

  private static final String jdoGetnombre(NivelPlantelActual paramNivelPlantelActual)
  {
    if (paramNivelPlantelActual.jdoFlags <= 0)
      return paramNivelPlantelActual.nombre;
    StateManager localStateManager = paramNivelPlantelActual.jdoStateManager;
    if (localStateManager == null)
      return paramNivelPlantelActual.nombre;
    if (localStateManager.isLoaded(paramNivelPlantelActual, jdoInheritedFieldCount + 2))
      return paramNivelPlantelActual.nombre;
    return localStateManager.getStringField(paramNivelPlantelActual, jdoInheritedFieldCount + 2, paramNivelPlantelActual.nombre);
  }

  private static final void jdoSetnombre(NivelPlantelActual paramNivelPlantelActual, String paramString)
  {
    if (paramNivelPlantelActual.jdoFlags == 0)
    {
      paramNivelPlantelActual.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramNivelPlantelActual.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelPlantelActual.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramNivelPlantelActual, jdoInheritedFieldCount + 2, paramNivelPlantelActual.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(NivelPlantelActual paramNivelPlantelActual)
  {
    StateManager localStateManager = paramNivelPlantelActual.jdoStateManager;
    if (localStateManager == null)
      return paramNivelPlantelActual.organismo;
    if (localStateManager.isLoaded(paramNivelPlantelActual, jdoInheritedFieldCount + 3))
      return paramNivelPlantelActual.organismo;
    return (Organismo)localStateManager.getObjectField(paramNivelPlantelActual, jdoInheritedFieldCount + 3, paramNivelPlantelActual.organismo);
  }

  private static final void jdoSetorganismo(NivelPlantelActual paramNivelPlantelActual, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramNivelPlantelActual.jdoStateManager;
    if (localStateManager == null)
    {
      paramNivelPlantelActual.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramNivelPlantelActual, jdoInheritedFieldCount + 3, paramNivelPlantelActual.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}