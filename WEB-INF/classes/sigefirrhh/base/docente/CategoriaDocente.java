package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class CategoriaDocente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idCategoriaDocente;
  private String digitoCategoria;
  private String nombre;
  private String vigencia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "digitoCategoria", "idCategoriaDocente", "nombre", "vigencia" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.CategoriaDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CategoriaDocente());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public CategoriaDocente()
  {
    jdoSetvigencia(this, "S");
  }
  public String toString() { return jdoGetdigitoCategoria(this) + " - " + 
      jdoGetnombre(this);
  }

  public String getDigitoCategoria()
  {
    return jdoGetdigitoCategoria(this);
  }

  public long getIdCategoriaDocente()
  {
    return jdoGetidCategoriaDocente(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setDigitoCategoria(String string)
  {
    jdoSetdigitoCategoria(this, string);
  }

  public void setIdCategoriaDocente(long l)
  {
    jdoSetidCategoriaDocente(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public String getVigencia() {
    return jdoGetvigencia(this);
  }
  public void setVigencia(String vigencia) {
    jdoSetvigencia(this, vigencia);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CategoriaDocente localCategoriaDocente = new CategoriaDocente();
    localCategoriaDocente.jdoFlags = 1;
    localCategoriaDocente.jdoStateManager = paramStateManager;
    return localCategoriaDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CategoriaDocente localCategoriaDocente = new CategoriaDocente();
    localCategoriaDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCategoriaDocente.jdoFlags = 1;
    localCategoriaDocente.jdoStateManager = paramStateManager;
    return localCategoriaDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.digitoCategoria);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCategoriaDocente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.digitoCategoria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCategoriaDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigencia = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CategoriaDocente paramCategoriaDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCategoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.digitoCategoria = paramCategoriaDocente.digitoCategoria;
      return;
    case 1:
      if (paramCategoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idCategoriaDocente = paramCategoriaDocente.idCategoriaDocente;
      return;
    case 2:
      if (paramCategoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramCategoriaDocente.nombre;
      return;
    case 3:
      if (paramCategoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.vigencia = paramCategoriaDocente.vigencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CategoriaDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CategoriaDocente localCategoriaDocente = (CategoriaDocente)paramObject;
    if (localCategoriaDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCategoriaDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CategoriaDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CategoriaDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CategoriaDocentePK))
      throw new IllegalArgumentException("arg1");
    CategoriaDocentePK localCategoriaDocentePK = (CategoriaDocentePK)paramObject;
    localCategoriaDocentePK.idCategoriaDocente = this.idCategoriaDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CategoriaDocentePK))
      throw new IllegalArgumentException("arg1");
    CategoriaDocentePK localCategoriaDocentePK = (CategoriaDocentePK)paramObject;
    this.idCategoriaDocente = localCategoriaDocentePK.idCategoriaDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CategoriaDocentePK))
      throw new IllegalArgumentException("arg2");
    CategoriaDocentePK localCategoriaDocentePK = (CategoriaDocentePK)paramObject;
    localCategoriaDocentePK.idCategoriaDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CategoriaDocentePK))
      throw new IllegalArgumentException("arg2");
    CategoriaDocentePK localCategoriaDocentePK = (CategoriaDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localCategoriaDocentePK.idCategoriaDocente);
  }

  private static final String jdoGetdigitoCategoria(CategoriaDocente paramCategoriaDocente)
  {
    if (paramCategoriaDocente.jdoFlags <= 0)
      return paramCategoriaDocente.digitoCategoria;
    StateManager localStateManager = paramCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramCategoriaDocente.digitoCategoria;
    if (localStateManager.isLoaded(paramCategoriaDocente, jdoInheritedFieldCount + 0))
      return paramCategoriaDocente.digitoCategoria;
    return localStateManager.getStringField(paramCategoriaDocente, jdoInheritedFieldCount + 0, paramCategoriaDocente.digitoCategoria);
  }

  private static final void jdoSetdigitoCategoria(CategoriaDocente paramCategoriaDocente, String paramString)
  {
    if (paramCategoriaDocente.jdoFlags == 0)
    {
      paramCategoriaDocente.digitoCategoria = paramString;
      return;
    }
    StateManager localStateManager = paramCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramCategoriaDocente.digitoCategoria = paramString;
      return;
    }
    localStateManager.setStringField(paramCategoriaDocente, jdoInheritedFieldCount + 0, paramCategoriaDocente.digitoCategoria, paramString);
  }

  private static final long jdoGetidCategoriaDocente(CategoriaDocente paramCategoriaDocente)
  {
    return paramCategoriaDocente.idCategoriaDocente;
  }

  private static final void jdoSetidCategoriaDocente(CategoriaDocente paramCategoriaDocente, long paramLong)
  {
    StateManager localStateManager = paramCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramCategoriaDocente.idCategoriaDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramCategoriaDocente, jdoInheritedFieldCount + 1, paramCategoriaDocente.idCategoriaDocente, paramLong);
  }

  private static final String jdoGetnombre(CategoriaDocente paramCategoriaDocente)
  {
    if (paramCategoriaDocente.jdoFlags <= 0)
      return paramCategoriaDocente.nombre;
    StateManager localStateManager = paramCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramCategoriaDocente.nombre;
    if (localStateManager.isLoaded(paramCategoriaDocente, jdoInheritedFieldCount + 2))
      return paramCategoriaDocente.nombre;
    return localStateManager.getStringField(paramCategoriaDocente, jdoInheritedFieldCount + 2, paramCategoriaDocente.nombre);
  }

  private static final void jdoSetnombre(CategoriaDocente paramCategoriaDocente, String paramString)
  {
    if (paramCategoriaDocente.jdoFlags == 0)
    {
      paramCategoriaDocente.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramCategoriaDocente.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramCategoriaDocente, jdoInheritedFieldCount + 2, paramCategoriaDocente.nombre, paramString);
  }

  private static final String jdoGetvigencia(CategoriaDocente paramCategoriaDocente)
  {
    if (paramCategoriaDocente.jdoFlags <= 0)
      return paramCategoriaDocente.vigencia;
    StateManager localStateManager = paramCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramCategoriaDocente.vigencia;
    if (localStateManager.isLoaded(paramCategoriaDocente, jdoInheritedFieldCount + 3))
      return paramCategoriaDocente.vigencia;
    return localStateManager.getStringField(paramCategoriaDocente, jdoInheritedFieldCount + 3, paramCategoriaDocente.vigencia);
  }

  private static final void jdoSetvigencia(CategoriaDocente paramCategoriaDocente, String paramString)
  {
    if (paramCategoriaDocente.jdoFlags == 0)
    {
      paramCategoriaDocente.vigencia = paramString;
      return;
    }
    StateManager localStateManager = paramCategoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramCategoriaDocente.vigencia = paramString;
      return;
    }
    localStateManager.setStringField(paramCategoriaDocente, jdoInheritedFieldCount + 3, paramCategoriaDocente.vigencia, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}