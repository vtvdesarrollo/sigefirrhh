package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TurnoDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTurnoDocente(TurnoDocente turnoDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TurnoDocente turnoDocenteNew = 
      (TurnoDocente)BeanUtils.cloneBean(
      turnoDocente);

    pm.makePersistent(turnoDocenteNew);
  }

  public void updateTurnoDocente(TurnoDocente turnoDocente) throws Exception
  {
    TurnoDocente turnoDocenteModify = 
      findTurnoDocenteById(turnoDocente.getIdTurnoDocente());

    BeanUtils.copyProperties(turnoDocenteModify, turnoDocente);
  }

  public void deleteTurnoDocente(TurnoDocente turnoDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TurnoDocente turnoDocenteDelete = 
      findTurnoDocenteById(turnoDocente.getIdTurnoDocente());
    pm.deletePersistent(turnoDocenteDelete);
  }

  public TurnoDocente findTurnoDocenteById(long idTurnoDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTurnoDocente == pIdTurnoDocente";
    Query query = pm.newQuery(TurnoDocente.class, filter);

    query.declareParameters("long pIdTurnoDocente");

    parameters.put("pIdTurnoDocente", new Long(idTurnoDocente));

    Collection colTurnoDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTurnoDocente.iterator();
    return (TurnoDocente)iterator.next();
  }

  public Collection findTurnoDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent turnoDocenteExtent = pm.getExtent(
      TurnoDocente.class, true);
    Query query = pm.newQuery(turnoDocenteExtent);
    query.setOrdering("digitoTurno ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByDigitoTurno(String digitoTurno)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "digitoTurno == pDigitoTurno";

    Query query = pm.newQuery(TurnoDocente.class, filter);

    query.declareParameters("java.lang.String pDigitoTurno");
    HashMap parameters = new HashMap();

    parameters.put("pDigitoTurno", new String(digitoTurno));

    query.setOrdering("digitoTurno ascending");

    Collection colTurnoDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTurnoDocente);

    return colTurnoDocente;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(TurnoDocente.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("digitoTurno ascending");

    Collection colTurnoDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTurnoDocente);

    return colTurnoDocente;
  }
}