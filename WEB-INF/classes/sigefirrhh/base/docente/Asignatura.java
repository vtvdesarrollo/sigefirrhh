package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Asignatura
  implements Serializable, PersistenceCapable
{
  private long idAsignatura;
  private String codAsignatura;
  private String nombre;
  private double topeHoras;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codAsignatura", "idAsignatura", "nombre", "topeHoras" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcodAsignatura(this) + " - " + jdoGetnombre(this);
  }

  public String getCodAsignatura() {
    return jdoGetcodAsignatura(this);
  }
  public void setCodAsignatura(String codAsignatura) {
    jdoSetcodAsignatura(this, codAsignatura);
  }
  public long getIdAsignatura() {
    return jdoGetidAsignatura(this);
  }
  public void setIdAsignatura(long idAsignatura) {
    jdoSetidAsignatura(this, idAsignatura);
  }
  public String getNombre() {
    return jdoGetnombre(this);
  }
  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public double getTopeHoras() {
    return jdoGettopeHoras(this);
  }

  public void setTopeHoras(double topeHoras) {
    jdoSettopeHoras(this, topeHoras);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.Asignatura"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Asignatura());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Asignatura localAsignatura = new Asignatura();
    localAsignatura.jdoFlags = 1;
    localAsignatura.jdoStateManager = paramStateManager;
    return localAsignatura;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Asignatura localAsignatura = new Asignatura();
    localAsignatura.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAsignatura.jdoFlags = 1;
    localAsignatura.jdoStateManager = paramStateManager;
    return localAsignatura;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codAsignatura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAsignatura);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.topeHoras);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codAsignatura = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAsignatura = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.topeHoras = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Asignatura paramAsignatura, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAsignatura == null)
        throw new IllegalArgumentException("arg1");
      this.codAsignatura = paramAsignatura.codAsignatura;
      return;
    case 1:
      if (paramAsignatura == null)
        throw new IllegalArgumentException("arg1");
      this.idAsignatura = paramAsignatura.idAsignatura;
      return;
    case 2:
      if (paramAsignatura == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramAsignatura.nombre;
      return;
    case 3:
      if (paramAsignatura == null)
        throw new IllegalArgumentException("arg1");
      this.topeHoras = paramAsignatura.topeHoras;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Asignatura))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Asignatura localAsignatura = (Asignatura)paramObject;
    if (localAsignatura.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAsignatura, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AsignaturaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AsignaturaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AsignaturaPK))
      throw new IllegalArgumentException("arg1");
    AsignaturaPK localAsignaturaPK = (AsignaturaPK)paramObject;
    localAsignaturaPK.idAsignatura = this.idAsignatura;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AsignaturaPK))
      throw new IllegalArgumentException("arg1");
    AsignaturaPK localAsignaturaPK = (AsignaturaPK)paramObject;
    this.idAsignatura = localAsignaturaPK.idAsignatura;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AsignaturaPK))
      throw new IllegalArgumentException("arg2");
    AsignaturaPK localAsignaturaPK = (AsignaturaPK)paramObject;
    localAsignaturaPK.idAsignatura = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AsignaturaPK))
      throw new IllegalArgumentException("arg2");
    AsignaturaPK localAsignaturaPK = (AsignaturaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localAsignaturaPK.idAsignatura);
  }

  private static final String jdoGetcodAsignatura(Asignatura paramAsignatura)
  {
    if (paramAsignatura.jdoFlags <= 0)
      return paramAsignatura.codAsignatura;
    StateManager localStateManager = paramAsignatura.jdoStateManager;
    if (localStateManager == null)
      return paramAsignatura.codAsignatura;
    if (localStateManager.isLoaded(paramAsignatura, jdoInheritedFieldCount + 0))
      return paramAsignatura.codAsignatura;
    return localStateManager.getStringField(paramAsignatura, jdoInheritedFieldCount + 0, paramAsignatura.codAsignatura);
  }

  private static final void jdoSetcodAsignatura(Asignatura paramAsignatura, String paramString)
  {
    if (paramAsignatura.jdoFlags == 0)
    {
      paramAsignatura.codAsignatura = paramString;
      return;
    }
    StateManager localStateManager = paramAsignatura.jdoStateManager;
    if (localStateManager == null)
    {
      paramAsignatura.codAsignatura = paramString;
      return;
    }
    localStateManager.setStringField(paramAsignatura, jdoInheritedFieldCount + 0, paramAsignatura.codAsignatura, paramString);
  }

  private static final long jdoGetidAsignatura(Asignatura paramAsignatura)
  {
    return paramAsignatura.idAsignatura;
  }

  private static final void jdoSetidAsignatura(Asignatura paramAsignatura, long paramLong)
  {
    StateManager localStateManager = paramAsignatura.jdoStateManager;
    if (localStateManager == null)
    {
      paramAsignatura.idAsignatura = paramLong;
      return;
    }
    localStateManager.setLongField(paramAsignatura, jdoInheritedFieldCount + 1, paramAsignatura.idAsignatura, paramLong);
  }

  private static final String jdoGetnombre(Asignatura paramAsignatura)
  {
    if (paramAsignatura.jdoFlags <= 0)
      return paramAsignatura.nombre;
    StateManager localStateManager = paramAsignatura.jdoStateManager;
    if (localStateManager == null)
      return paramAsignatura.nombre;
    if (localStateManager.isLoaded(paramAsignatura, jdoInheritedFieldCount + 2))
      return paramAsignatura.nombre;
    return localStateManager.getStringField(paramAsignatura, jdoInheritedFieldCount + 2, paramAsignatura.nombre);
  }

  private static final void jdoSetnombre(Asignatura paramAsignatura, String paramString)
  {
    if (paramAsignatura.jdoFlags == 0)
    {
      paramAsignatura.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramAsignatura.jdoStateManager;
    if (localStateManager == null)
    {
      paramAsignatura.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramAsignatura, jdoInheritedFieldCount + 2, paramAsignatura.nombre, paramString);
  }

  private static final double jdoGettopeHoras(Asignatura paramAsignatura)
  {
    if (paramAsignatura.jdoFlags <= 0)
      return paramAsignatura.topeHoras;
    StateManager localStateManager = paramAsignatura.jdoStateManager;
    if (localStateManager == null)
      return paramAsignatura.topeHoras;
    if (localStateManager.isLoaded(paramAsignatura, jdoInheritedFieldCount + 3))
      return paramAsignatura.topeHoras;
    return localStateManager.getDoubleField(paramAsignatura, jdoInheritedFieldCount + 3, paramAsignatura.topeHoras);
  }

  private static final void jdoSettopeHoras(Asignatura paramAsignatura, double paramDouble)
  {
    if (paramAsignatura.jdoFlags == 0)
    {
      paramAsignatura.topeHoras = paramDouble;
      return;
    }
    StateManager localStateManager = paramAsignatura.jdoStateManager;
    if (localStateManager == null)
    {
      paramAsignatura.topeHoras = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAsignatura, jdoInheritedFieldCount + 3, paramAsignatura.topeHoras, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}