package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class DedicacionDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDedicacionDocente(DedicacionDocente dedicacionDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DedicacionDocente dedicacionDocenteNew = 
      (DedicacionDocente)BeanUtils.cloneBean(
      dedicacionDocente);

    pm.makePersistent(dedicacionDocenteNew);
  }

  public void updateDedicacionDocente(DedicacionDocente dedicacionDocente) throws Exception
  {
    DedicacionDocente dedicacionDocenteModify = 
      findDedicacionDocenteById(dedicacionDocente.getIdDedicacionDocente());

    BeanUtils.copyProperties(dedicacionDocenteModify, dedicacionDocente);
  }

  public void deleteDedicacionDocente(DedicacionDocente dedicacionDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DedicacionDocente dedicacionDocenteDelete = 
      findDedicacionDocenteById(dedicacionDocente.getIdDedicacionDocente());
    pm.deletePersistent(dedicacionDocenteDelete);
  }

  public DedicacionDocente findDedicacionDocenteById(long idDedicacionDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDedicacionDocente == pIdDedicacionDocente";
    Query query = pm.newQuery(DedicacionDocente.class, filter);

    query.declareParameters("long pIdDedicacionDocente");

    parameters.put("pIdDedicacionDocente", new Long(idDedicacionDocente));

    Collection colDedicacionDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDedicacionDocente.iterator();
    return (DedicacionDocente)iterator.next();
  }

  public Collection findDedicacionDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent dedicacionDocenteExtent = pm.getExtent(
      DedicacionDocente.class, true);
    Query query = pm.newQuery(dedicacionDocenteExtent);
    query.setOrdering("digitoDedicacion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByDigitoDedicacion(String digitoDedicacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "digitoDedicacion == pDigitoDedicacion";

    Query query = pm.newQuery(DedicacionDocente.class, filter);

    query.declareParameters("java.lang.String pDigitoDedicacion");
    HashMap parameters = new HashMap();

    parameters.put("pDigitoDedicacion", new String(digitoDedicacion));

    query.setOrdering("digitoDedicacion ascending");

    Collection colDedicacionDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDedicacionDocente);

    return colDedicacionDocente;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(DedicacionDocente.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("digitoDedicacion ascending");

    Collection colDedicacionDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDedicacionDocente);

    return colDedicacionDocente;
  }
}