package sigefirrhh.base.docente;

import java.io.Serializable;

public class DedicacionDocentePK
  implements Serializable
{
  public long idDedicacionDocente;

  public DedicacionDocentePK()
  {
  }

  public DedicacionDocentePK(long idDedicacionDocente)
  {
    this.idDedicacionDocente = idDedicacionDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DedicacionDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DedicacionDocentePK thatPK)
  {
    return 
      this.idDedicacionDocente == thatPK.idDedicacionDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDedicacionDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDedicacionDocente);
  }
}