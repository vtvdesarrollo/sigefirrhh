package sigefirrhh.base.docente;

import java.io.Serializable;
import java.util.Collection;

public class DocenteNoGenBusiness extends DocenteBusiness
  implements Serializable
{
  private TurnoDocenteNoGenBeanBusiness turnoDocenteNoGenBeanBusiness = new TurnoDocenteNoGenBeanBusiness();

  public Collection findTurnoDocenteByTurnoAndCondicion(String turno, String condicion)
    throws Exception
  {
    return this.turnoDocenteNoGenBeanBusiness.findByTurnoAndCondicion(turno, condicion);
  }
}