package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SeguridadDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSeguridadDocente(SeguridadDocente seguridadDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SeguridadDocente seguridadDocenteNew = 
      (SeguridadDocente)BeanUtils.cloneBean(
      seguridadDocente);

    pm.makePersistent(seguridadDocenteNew);
  }

  public void updateSeguridadDocente(SeguridadDocente seguridadDocente) throws Exception
  {
    SeguridadDocente seguridadDocenteModify = 
      findSeguridadDocenteById(seguridadDocente.getIdSeguridadDocente());

    BeanUtils.copyProperties(seguridadDocenteModify, seguridadDocente);
  }

  public void deleteSeguridadDocente(SeguridadDocente seguridadDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SeguridadDocente seguridadDocenteDelete = 
      findSeguridadDocenteById(seguridadDocente.getIdSeguridadDocente());
    pm.deletePersistent(seguridadDocenteDelete);
  }

  public SeguridadDocente findSeguridadDocenteById(long idSeguridadDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSeguridadDocente == pIdSeguridadDocente";
    Query query = pm.newQuery(SeguridadDocente.class, filter);

    query.declareParameters("long pIdSeguridadDocente");

    parameters.put("pIdSeguridadDocente", new Long(idSeguridadDocente));

    Collection colSeguridadDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSeguridadDocente.iterator();
    return (SeguridadDocente)iterator.next();
  }

  public Collection findSeguridadDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent seguridadDocenteExtent = pm.getExtent(
      SeguridadDocente.class, true);
    Query query = pm.newQuery(seguridadDocenteExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(SeguridadDocente.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    Collection colSeguridadDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadDocente);

    return colSeguridadDocente;
  }

  public Collection findByCerrado(String cerrado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cerrado == pCerrado";

    Query query = pm.newQuery(SeguridadDocente.class, filter);

    query.declareParameters("java.lang.String pCerrado");
    HashMap parameters = new HashMap();

    parameters.put("pCerrado", new String(cerrado));

    Collection colSeguridadDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadDocente);

    return colSeguridadDocente;
  }

  public Collection findByFechaCierre(Date fechaCierre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "fechaCierre == pFechaCierre";

    Query query = pm.newQuery(SeguridadDocente.class, filter);

    query.declareParameters("java.util.Date pFechaCierre");
    HashMap parameters = new HashMap();

    parameters.put("pFechaCierre", fechaCierre);

    Collection colSeguridadDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadDocente);

    return colSeguridadDocente;
  }
}