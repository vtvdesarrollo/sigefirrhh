package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class DedicacionDocente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idDedicacionDocente;
  private String digitoDedicacion;
  private String nombre;
  private String vigencia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "digitoDedicacion", "idDedicacionDocente", "nombre", "vigencia" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.DedicacionDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DedicacionDocente());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public DedicacionDocente()
  {
    jdoSetvigencia(this, "S");
  }
  public String toString() {
    return jdoGetdigitoDedicacion(this) + " - " + 
      jdoGetnombre(this);
  }

  public String getDigitoDedicacion()
  {
    return jdoGetdigitoDedicacion(this);
  }

  public long getIdDedicacionDocente()
  {
    return jdoGetidDedicacionDocente(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setDigitoDedicacion(String string)
  {
    jdoSetdigitoDedicacion(this, string);
  }

  public void setIdDedicacionDocente(long l)
  {
    jdoSetidDedicacionDocente(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public String getVigencia() {
    return jdoGetvigencia(this);
  }
  public void setVigencia(String vigencia) {
    jdoSetvigencia(this, vigencia);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DedicacionDocente localDedicacionDocente = new DedicacionDocente();
    localDedicacionDocente.jdoFlags = 1;
    localDedicacionDocente.jdoStateManager = paramStateManager;
    return localDedicacionDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DedicacionDocente localDedicacionDocente = new DedicacionDocente();
    localDedicacionDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDedicacionDocente.jdoFlags = 1;
    localDedicacionDocente.jdoStateManager = paramStateManager;
    return localDedicacionDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.digitoDedicacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDedicacionDocente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.digitoDedicacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDedicacionDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigencia = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DedicacionDocente paramDedicacionDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDedicacionDocente == null)
        throw new IllegalArgumentException("arg1");
      this.digitoDedicacion = paramDedicacionDocente.digitoDedicacion;
      return;
    case 1:
      if (paramDedicacionDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idDedicacionDocente = paramDedicacionDocente.idDedicacionDocente;
      return;
    case 2:
      if (paramDedicacionDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramDedicacionDocente.nombre;
      return;
    case 3:
      if (paramDedicacionDocente == null)
        throw new IllegalArgumentException("arg1");
      this.vigencia = paramDedicacionDocente.vigencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DedicacionDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DedicacionDocente localDedicacionDocente = (DedicacionDocente)paramObject;
    if (localDedicacionDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDedicacionDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DedicacionDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DedicacionDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DedicacionDocentePK))
      throw new IllegalArgumentException("arg1");
    DedicacionDocentePK localDedicacionDocentePK = (DedicacionDocentePK)paramObject;
    localDedicacionDocentePK.idDedicacionDocente = this.idDedicacionDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DedicacionDocentePK))
      throw new IllegalArgumentException("arg1");
    DedicacionDocentePK localDedicacionDocentePK = (DedicacionDocentePK)paramObject;
    this.idDedicacionDocente = localDedicacionDocentePK.idDedicacionDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DedicacionDocentePK))
      throw new IllegalArgumentException("arg2");
    DedicacionDocentePK localDedicacionDocentePK = (DedicacionDocentePK)paramObject;
    localDedicacionDocentePK.idDedicacionDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DedicacionDocentePK))
      throw new IllegalArgumentException("arg2");
    DedicacionDocentePK localDedicacionDocentePK = (DedicacionDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localDedicacionDocentePK.idDedicacionDocente);
  }

  private static final String jdoGetdigitoDedicacion(DedicacionDocente paramDedicacionDocente)
  {
    if (paramDedicacionDocente.jdoFlags <= 0)
      return paramDedicacionDocente.digitoDedicacion;
    StateManager localStateManager = paramDedicacionDocente.jdoStateManager;
    if (localStateManager == null)
      return paramDedicacionDocente.digitoDedicacion;
    if (localStateManager.isLoaded(paramDedicacionDocente, jdoInheritedFieldCount + 0))
      return paramDedicacionDocente.digitoDedicacion;
    return localStateManager.getStringField(paramDedicacionDocente, jdoInheritedFieldCount + 0, paramDedicacionDocente.digitoDedicacion);
  }

  private static final void jdoSetdigitoDedicacion(DedicacionDocente paramDedicacionDocente, String paramString)
  {
    if (paramDedicacionDocente.jdoFlags == 0)
    {
      paramDedicacionDocente.digitoDedicacion = paramString;
      return;
    }
    StateManager localStateManager = paramDedicacionDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramDedicacionDocente.digitoDedicacion = paramString;
      return;
    }
    localStateManager.setStringField(paramDedicacionDocente, jdoInheritedFieldCount + 0, paramDedicacionDocente.digitoDedicacion, paramString);
  }

  private static final long jdoGetidDedicacionDocente(DedicacionDocente paramDedicacionDocente)
  {
    return paramDedicacionDocente.idDedicacionDocente;
  }

  private static final void jdoSetidDedicacionDocente(DedicacionDocente paramDedicacionDocente, long paramLong)
  {
    StateManager localStateManager = paramDedicacionDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramDedicacionDocente.idDedicacionDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramDedicacionDocente, jdoInheritedFieldCount + 1, paramDedicacionDocente.idDedicacionDocente, paramLong);
  }

  private static final String jdoGetnombre(DedicacionDocente paramDedicacionDocente)
  {
    if (paramDedicacionDocente.jdoFlags <= 0)
      return paramDedicacionDocente.nombre;
    StateManager localStateManager = paramDedicacionDocente.jdoStateManager;
    if (localStateManager == null)
      return paramDedicacionDocente.nombre;
    if (localStateManager.isLoaded(paramDedicacionDocente, jdoInheritedFieldCount + 2))
      return paramDedicacionDocente.nombre;
    return localStateManager.getStringField(paramDedicacionDocente, jdoInheritedFieldCount + 2, paramDedicacionDocente.nombre);
  }

  private static final void jdoSetnombre(DedicacionDocente paramDedicacionDocente, String paramString)
  {
    if (paramDedicacionDocente.jdoFlags == 0)
    {
      paramDedicacionDocente.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramDedicacionDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramDedicacionDocente.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramDedicacionDocente, jdoInheritedFieldCount + 2, paramDedicacionDocente.nombre, paramString);
  }

  private static final String jdoGetvigencia(DedicacionDocente paramDedicacionDocente)
  {
    if (paramDedicacionDocente.jdoFlags <= 0)
      return paramDedicacionDocente.vigencia;
    StateManager localStateManager = paramDedicacionDocente.jdoStateManager;
    if (localStateManager == null)
      return paramDedicacionDocente.vigencia;
    if (localStateManager.isLoaded(paramDedicacionDocente, jdoInheritedFieldCount + 3))
      return paramDedicacionDocente.vigencia;
    return localStateManager.getStringField(paramDedicacionDocente, jdoInheritedFieldCount + 3, paramDedicacionDocente.vigencia);
  }

  private static final void jdoSetvigencia(DedicacionDocente paramDedicacionDocente, String paramString)
  {
    if (paramDedicacionDocente.jdoFlags == 0)
    {
      paramDedicacionDocente.vigencia = paramString;
      return;
    }
    StateManager localStateManager = paramDedicacionDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramDedicacionDocente.vigencia = paramString;
      return;
    }
    localStateManager.setStringField(paramDedicacionDocente, jdoInheritedFieldCount + 3, paramDedicacionDocente.vigencia, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}