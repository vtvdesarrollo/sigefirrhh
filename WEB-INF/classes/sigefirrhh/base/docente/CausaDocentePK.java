package sigefirrhh.base.docente;

import java.io.Serializable;

public class CausaDocentePK
  implements Serializable
{
  public long idCausaDocente;

  public CausaDocentePK()
  {
  }

  public CausaDocentePK(long idCausaDocente)
  {
    this.idCausaDocente = idCausaDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CausaDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CausaDocentePK thatPK)
  {
    return 
      this.idCausaDocente == thatPK.idCausaDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCausaDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCausaDocente);
  }
}