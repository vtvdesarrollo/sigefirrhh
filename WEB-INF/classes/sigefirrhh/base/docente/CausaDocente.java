package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class CausaDocente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  private long idCausaDocente;
  private String codCausaDocente;
  private String nombre;
  private String tipo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codCausaDocente", "idCausaDocente", "nombre", "tipo" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.CausaDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CausaDocente());

    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_TIPO.put("1", "EGRESO");
    LISTA_TIPO.put("2", "REINGRESO");
    LISTA_TIPO.put("3", "OTROS");
  }

  public String toString()
  {
    return jdoGetcodCausaDocente(this) + " - " + 
      jdoGetnombre(this);
  }

  public String getCodCausaDocente() {
    return jdoGetcodCausaDocente(this);
  }

  public void setCodCausaDocente(String codCausaDocente) {
    jdoSetcodCausaDocente(this, codCausaDocente);
  }

  public long getIdCausaDocente() {
    return jdoGetidCausaDocente(this);
  }

  public void setIdCausaDocente(long idCausaDocente) {
    jdoSetidCausaDocente(this, idCausaDocente);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public String getTipo() {
    return jdoGettipo(this);
  }

  public void setTipo(String tipo) {
    jdoSettipo(this, tipo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CausaDocente localCausaDocente = new CausaDocente();
    localCausaDocente.jdoFlags = 1;
    localCausaDocente.jdoStateManager = paramStateManager;
    return localCausaDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CausaDocente localCausaDocente = new CausaDocente();
    localCausaDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCausaDocente.jdoFlags = 1;
    localCausaDocente.jdoStateManager = paramStateManager;
    return localCausaDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCausaDocente);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCausaDocente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCausaDocente = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCausaDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CausaDocente paramCausaDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCausaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.codCausaDocente = paramCausaDocente.codCausaDocente;
      return;
    case 1:
      if (paramCausaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idCausaDocente = paramCausaDocente.idCausaDocente;
      return;
    case 2:
      if (paramCausaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramCausaDocente.nombre;
      return;
    case 3:
      if (paramCausaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramCausaDocente.tipo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CausaDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CausaDocente localCausaDocente = (CausaDocente)paramObject;
    if (localCausaDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCausaDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CausaDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CausaDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CausaDocentePK))
      throw new IllegalArgumentException("arg1");
    CausaDocentePK localCausaDocentePK = (CausaDocentePK)paramObject;
    localCausaDocentePK.idCausaDocente = this.idCausaDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CausaDocentePK))
      throw new IllegalArgumentException("arg1");
    CausaDocentePK localCausaDocentePK = (CausaDocentePK)paramObject;
    this.idCausaDocente = localCausaDocentePK.idCausaDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CausaDocentePK))
      throw new IllegalArgumentException("arg2");
    CausaDocentePK localCausaDocentePK = (CausaDocentePK)paramObject;
    localCausaDocentePK.idCausaDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CausaDocentePK))
      throw new IllegalArgumentException("arg2");
    CausaDocentePK localCausaDocentePK = (CausaDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localCausaDocentePK.idCausaDocente);
  }

  private static final String jdoGetcodCausaDocente(CausaDocente paramCausaDocente)
  {
    if (paramCausaDocente.jdoFlags <= 0)
      return paramCausaDocente.codCausaDocente;
    StateManager localStateManager = paramCausaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramCausaDocente.codCausaDocente;
    if (localStateManager.isLoaded(paramCausaDocente, jdoInheritedFieldCount + 0))
      return paramCausaDocente.codCausaDocente;
    return localStateManager.getStringField(paramCausaDocente, jdoInheritedFieldCount + 0, paramCausaDocente.codCausaDocente);
  }

  private static final void jdoSetcodCausaDocente(CausaDocente paramCausaDocente, String paramString)
  {
    if (paramCausaDocente.jdoFlags == 0)
    {
      paramCausaDocente.codCausaDocente = paramString;
      return;
    }
    StateManager localStateManager = paramCausaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaDocente.codCausaDocente = paramString;
      return;
    }
    localStateManager.setStringField(paramCausaDocente, jdoInheritedFieldCount + 0, paramCausaDocente.codCausaDocente, paramString);
  }

  private static final long jdoGetidCausaDocente(CausaDocente paramCausaDocente)
  {
    return paramCausaDocente.idCausaDocente;
  }

  private static final void jdoSetidCausaDocente(CausaDocente paramCausaDocente, long paramLong)
  {
    StateManager localStateManager = paramCausaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaDocente.idCausaDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramCausaDocente, jdoInheritedFieldCount + 1, paramCausaDocente.idCausaDocente, paramLong);
  }

  private static final String jdoGetnombre(CausaDocente paramCausaDocente)
  {
    if (paramCausaDocente.jdoFlags <= 0)
      return paramCausaDocente.nombre;
    StateManager localStateManager = paramCausaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramCausaDocente.nombre;
    if (localStateManager.isLoaded(paramCausaDocente, jdoInheritedFieldCount + 2))
      return paramCausaDocente.nombre;
    return localStateManager.getStringField(paramCausaDocente, jdoInheritedFieldCount + 2, paramCausaDocente.nombre);
  }

  private static final void jdoSetnombre(CausaDocente paramCausaDocente, String paramString)
  {
    if (paramCausaDocente.jdoFlags == 0)
    {
      paramCausaDocente.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramCausaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaDocente.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramCausaDocente, jdoInheritedFieldCount + 2, paramCausaDocente.nombre, paramString);
  }

  private static final String jdoGettipo(CausaDocente paramCausaDocente)
  {
    if (paramCausaDocente.jdoFlags <= 0)
      return paramCausaDocente.tipo;
    StateManager localStateManager = paramCausaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramCausaDocente.tipo;
    if (localStateManager.isLoaded(paramCausaDocente, jdoInheritedFieldCount + 3))
      return paramCausaDocente.tipo;
    return localStateManager.getStringField(paramCausaDocente, jdoInheritedFieldCount + 3, paramCausaDocente.tipo);
  }

  private static final void jdoSettipo(CausaDocente paramCausaDocente, String paramString)
  {
    if (paramCausaDocente.jdoFlags == 0)
    {
      paramCausaDocente.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramCausaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramCausaDocente.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramCausaDocente, jdoInheritedFieldCount + 3, paramCausaDocente.tipo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}