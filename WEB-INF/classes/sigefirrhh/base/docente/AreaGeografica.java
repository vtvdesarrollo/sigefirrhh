package sigefirrhh.base.docente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class AreaGeografica
  implements Serializable, PersistenceCapable
{
  private long idAreaGeografica;
  private String codAreaGeografica;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codAreaGeografica", "idAreaGeografica", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String getCodAreaGeografica()
  {
    return jdoGetcodAreaGeografica(this);
  }

  public long getIdAreaGeografica()
  {
    return jdoGetidAreaGeografica(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodAreaGeografica(String string)
  {
    jdoSetcodAreaGeografica(this, string);
  }

  public void setIdAreaGeografica(long l)
  {
    jdoSetidAreaGeografica(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.docente.AreaGeografica"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AreaGeografica());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AreaGeografica localAreaGeografica = new AreaGeografica();
    localAreaGeografica.jdoFlags = 1;
    localAreaGeografica.jdoStateManager = paramStateManager;
    return localAreaGeografica;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AreaGeografica localAreaGeografica = new AreaGeografica();
    localAreaGeografica.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAreaGeografica.jdoFlags = 1;
    localAreaGeografica.jdoStateManager = paramStateManager;
    return localAreaGeografica;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codAreaGeografica);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAreaGeografica);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codAreaGeografica = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAreaGeografica = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AreaGeografica paramAreaGeografica, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAreaGeografica == null)
        throw new IllegalArgumentException("arg1");
      this.codAreaGeografica = paramAreaGeografica.codAreaGeografica;
      return;
    case 1:
      if (paramAreaGeografica == null)
        throw new IllegalArgumentException("arg1");
      this.idAreaGeografica = paramAreaGeografica.idAreaGeografica;
      return;
    case 2:
      if (paramAreaGeografica == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramAreaGeografica.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AreaGeografica))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AreaGeografica localAreaGeografica = (AreaGeografica)paramObject;
    if (localAreaGeografica.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAreaGeografica, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AreaGeograficaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AreaGeograficaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AreaGeograficaPK))
      throw new IllegalArgumentException("arg1");
    AreaGeograficaPK localAreaGeograficaPK = (AreaGeograficaPK)paramObject;
    localAreaGeograficaPK.idAreaGeografica = this.idAreaGeografica;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AreaGeograficaPK))
      throw new IllegalArgumentException("arg1");
    AreaGeograficaPK localAreaGeograficaPK = (AreaGeograficaPK)paramObject;
    this.idAreaGeografica = localAreaGeograficaPK.idAreaGeografica;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AreaGeograficaPK))
      throw new IllegalArgumentException("arg2");
    AreaGeograficaPK localAreaGeograficaPK = (AreaGeograficaPK)paramObject;
    localAreaGeograficaPK.idAreaGeografica = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AreaGeograficaPK))
      throw new IllegalArgumentException("arg2");
    AreaGeograficaPK localAreaGeograficaPK = (AreaGeograficaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localAreaGeograficaPK.idAreaGeografica);
  }

  private static final String jdoGetcodAreaGeografica(AreaGeografica paramAreaGeografica)
  {
    if (paramAreaGeografica.jdoFlags <= 0)
      return paramAreaGeografica.codAreaGeografica;
    StateManager localStateManager = paramAreaGeografica.jdoStateManager;
    if (localStateManager == null)
      return paramAreaGeografica.codAreaGeografica;
    if (localStateManager.isLoaded(paramAreaGeografica, jdoInheritedFieldCount + 0))
      return paramAreaGeografica.codAreaGeografica;
    return localStateManager.getStringField(paramAreaGeografica, jdoInheritedFieldCount + 0, paramAreaGeografica.codAreaGeografica);
  }

  private static final void jdoSetcodAreaGeografica(AreaGeografica paramAreaGeografica, String paramString)
  {
    if (paramAreaGeografica.jdoFlags == 0)
    {
      paramAreaGeografica.codAreaGeografica = paramString;
      return;
    }
    StateManager localStateManager = paramAreaGeografica.jdoStateManager;
    if (localStateManager == null)
    {
      paramAreaGeografica.codAreaGeografica = paramString;
      return;
    }
    localStateManager.setStringField(paramAreaGeografica, jdoInheritedFieldCount + 0, paramAreaGeografica.codAreaGeografica, paramString);
  }

  private static final long jdoGetidAreaGeografica(AreaGeografica paramAreaGeografica)
  {
    return paramAreaGeografica.idAreaGeografica;
  }

  private static final void jdoSetidAreaGeografica(AreaGeografica paramAreaGeografica, long paramLong)
  {
    StateManager localStateManager = paramAreaGeografica.jdoStateManager;
    if (localStateManager == null)
    {
      paramAreaGeografica.idAreaGeografica = paramLong;
      return;
    }
    localStateManager.setLongField(paramAreaGeografica, jdoInheritedFieldCount + 1, paramAreaGeografica.idAreaGeografica, paramLong);
  }

  private static final String jdoGetnombre(AreaGeografica paramAreaGeografica)
  {
    if (paramAreaGeografica.jdoFlags <= 0)
      return paramAreaGeografica.nombre;
    StateManager localStateManager = paramAreaGeografica.jdoStateManager;
    if (localStateManager == null)
      return paramAreaGeografica.nombre;
    if (localStateManager.isLoaded(paramAreaGeografica, jdoInheritedFieldCount + 2))
      return paramAreaGeografica.nombre;
    return localStateManager.getStringField(paramAreaGeografica, jdoInheritedFieldCount + 2, paramAreaGeografica.nombre);
  }

  private static final void jdoSetnombre(AreaGeografica paramAreaGeografica, String paramString)
  {
    if (paramAreaGeografica.jdoFlags == 0)
    {
      paramAreaGeografica.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramAreaGeografica.jdoStateManager;
    if (localStateManager == null)
    {
      paramAreaGeografica.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramAreaGeografica, jdoInheritedFieldCount + 2, paramAreaGeografica.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}