package sigefirrhh.base.docente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class CategoriaDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CategoriaDocenteForm.class.getName());
  private CategoriaDocente categoriaDocente;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DocenteFacade docenteFacade = new DocenteFacade();
  private boolean showCategoriaDocenteByDigitoCategoria;
  private boolean showCategoriaDocenteByNombre;
  private String findDigitoCategoria;
  private String findNombre;
  private Object stateResultCategoriaDocenteByDigitoCategoria = null;

  private Object stateResultCategoriaDocenteByNombre = null;

  public String getFindDigitoCategoria()
  {
    return this.findDigitoCategoria;
  }
  public void setFindDigitoCategoria(String findDigitoCategoria) {
    this.findDigitoCategoria = findDigitoCategoria;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public CategoriaDocente getCategoriaDocente() {
    if (this.categoriaDocente == null) {
      this.categoriaDocente = new CategoriaDocente();
    }
    return this.categoriaDocente;
  }

  public CategoriaDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListVigencia()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = CategoriaDocente.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findCategoriaDocenteByDigitoCategoria()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findCategoriaDocenteByDigitoCategoria(this.findDigitoCategoria);
      this.showCategoriaDocenteByDigitoCategoria = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCategoriaDocenteByDigitoCategoria)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findDigitoCategoria = null;
    this.findNombre = null;

    return null;
  }

  public String findCategoriaDocenteByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findCategoriaDocenteByNombre(this.findNombre);
      this.showCategoriaDocenteByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCategoriaDocenteByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findDigitoCategoria = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowCategoriaDocenteByDigitoCategoria() {
    return this.showCategoriaDocenteByDigitoCategoria;
  }
  public boolean isShowCategoriaDocenteByNombre() {
    return this.showCategoriaDocenteByNombre;
  }

  public String selectCategoriaDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idCategoriaDocente = 
      Long.parseLong((String)requestParameterMap.get("idCategoriaDocente"));
    try
    {
      this.categoriaDocente = 
        this.docenteFacade.findCategoriaDocenteById(
        idCategoriaDocente);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.categoriaDocente = null;
    this.showCategoriaDocenteByDigitoCategoria = false;
    this.showCategoriaDocenteByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.docenteFacade.addCategoriaDocente(
          this.categoriaDocente);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.docenteFacade.updateCategoriaDocente(
          this.categoriaDocente);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.docenteFacade.deleteCategoriaDocente(
        this.categoriaDocente);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.categoriaDocente = new CategoriaDocente();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.categoriaDocente.setIdCategoriaDocente(identityGenerator.getNextSequenceNumber("sigefirrhh.base.docente.CategoriaDocente"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.categoriaDocente = new CategoriaDocente();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}