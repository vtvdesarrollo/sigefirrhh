package sigefirrhh.base.docente;

import java.io.Serializable;

public class CategoriaDocentePK
  implements Serializable
{
  public long idCategoriaDocente;

  public CategoriaDocentePK()
  {
  }

  public CategoriaDocentePK(long idCategoriaDocente)
  {
    this.idCategoriaDocente = idCategoriaDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CategoriaDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CategoriaDocentePK thatPK)
  {
    return 
      this.idCategoriaDocente == thatPK.idCategoriaDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCategoriaDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCategoriaDocente);
  }
}