package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class JerarquiaCategoriaDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addJerarquiaCategoriaDocente(JerarquiaCategoriaDocente jerarquiaCategoriaDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    JerarquiaCategoriaDocente jerarquiaCategoriaDocenteNew = 
      (JerarquiaCategoriaDocente)BeanUtils.cloneBean(
      jerarquiaCategoriaDocente);

    JerarquiaDocenteBeanBusiness jerarquiaDocenteBeanBusiness = new JerarquiaDocenteBeanBusiness();

    if (jerarquiaCategoriaDocenteNew.getJerarquiaDocente() != null) {
      jerarquiaCategoriaDocenteNew.setJerarquiaDocente(
        jerarquiaDocenteBeanBusiness.findJerarquiaDocenteById(
        jerarquiaCategoriaDocenteNew.getJerarquiaDocente().getIdJerarquiaDocente()));
    }

    CategoriaDocenteBeanBusiness categoriaDocenteBeanBusiness = new CategoriaDocenteBeanBusiness();

    if (jerarquiaCategoriaDocenteNew.getCategoriaDocente() != null) {
      jerarquiaCategoriaDocenteNew.setCategoriaDocente(
        categoriaDocenteBeanBusiness.findCategoriaDocenteById(
        jerarquiaCategoriaDocenteNew.getCategoriaDocente().getIdCategoriaDocente()));
    }
    pm.makePersistent(jerarquiaCategoriaDocenteNew);
  }

  public void updateJerarquiaCategoriaDocente(JerarquiaCategoriaDocente jerarquiaCategoriaDocente) throws Exception
  {
    JerarquiaCategoriaDocente jerarquiaCategoriaDocenteModify = 
      findJerarquiaCategoriaDocenteById(jerarquiaCategoriaDocente.getIdJerarquiaCategoriaDocente());

    JerarquiaDocenteBeanBusiness jerarquiaDocenteBeanBusiness = new JerarquiaDocenteBeanBusiness();

    if (jerarquiaCategoriaDocente.getJerarquiaDocente() != null) {
      jerarquiaCategoriaDocente.setJerarquiaDocente(
        jerarquiaDocenteBeanBusiness.findJerarquiaDocenteById(
        jerarquiaCategoriaDocente.getJerarquiaDocente().getIdJerarquiaDocente()));
    }

    CategoriaDocenteBeanBusiness categoriaDocenteBeanBusiness = new CategoriaDocenteBeanBusiness();

    if (jerarquiaCategoriaDocente.getCategoriaDocente() != null) {
      jerarquiaCategoriaDocente.setCategoriaDocente(
        categoriaDocenteBeanBusiness.findCategoriaDocenteById(
        jerarquiaCategoriaDocente.getCategoriaDocente().getIdCategoriaDocente()));
    }

    BeanUtils.copyProperties(jerarquiaCategoriaDocenteModify, jerarquiaCategoriaDocente);
  }

  public void deleteJerarquiaCategoriaDocente(JerarquiaCategoriaDocente jerarquiaCategoriaDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    JerarquiaCategoriaDocente jerarquiaCategoriaDocenteDelete = 
      findJerarquiaCategoriaDocenteById(jerarquiaCategoriaDocente.getIdJerarquiaCategoriaDocente());
    pm.deletePersistent(jerarquiaCategoriaDocenteDelete);
  }

  public JerarquiaCategoriaDocente findJerarquiaCategoriaDocenteById(long idJerarquiaCategoriaDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idJerarquiaCategoriaDocente == pIdJerarquiaCategoriaDocente";
    Query query = pm.newQuery(JerarquiaCategoriaDocente.class, filter);

    query.declareParameters("long pIdJerarquiaCategoriaDocente");

    parameters.put("pIdJerarquiaCategoriaDocente", new Long(idJerarquiaCategoriaDocente));

    Collection colJerarquiaCategoriaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colJerarquiaCategoriaDocente.iterator();
    return (JerarquiaCategoriaDocente)iterator.next();
  }

  public Collection findJerarquiaCategoriaDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent jerarquiaCategoriaDocenteExtent = pm.getExtent(
      JerarquiaCategoriaDocente.class, true);
    Query query = pm.newQuery(jerarquiaCategoriaDocenteExtent);
    query.setOrdering("jerarquiaDocente.digitoJerarquia ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByJerarquiaDocente(long idJerarquiaDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "jerarquiaDocente.idJerarquiaDocente == pIdJerarquiaDocente";

    Query query = pm.newQuery(JerarquiaCategoriaDocente.class, filter);

    query.declareParameters("long pIdJerarquiaDocente");
    HashMap parameters = new HashMap();

    parameters.put("pIdJerarquiaDocente", new Long(idJerarquiaDocente));

    query.setOrdering("jerarquiaDocente.digitoJerarquia ascending");

    Collection colJerarquiaCategoriaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colJerarquiaCategoriaDocente);

    return colJerarquiaCategoriaDocente;
  }
}