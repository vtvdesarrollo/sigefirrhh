package sigefirrhh.base.docente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class JerarquiaDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(JerarquiaDocenteForm.class.getName());
  private JerarquiaDocente jerarquiaDocente;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DocenteFacade docenteFacade = new DocenteFacade();
  private boolean showJerarquiaDocenteByDigitoJerarquia;
  private boolean showJerarquiaDocenteByNombre;
  private String findDigitoJerarquia;
  private String findNombre;
  private Object stateResultJerarquiaDocenteByDigitoJerarquia = null;

  private Object stateResultJerarquiaDocenteByNombre = null;

  public String getFindDigitoJerarquia()
  {
    return this.findDigitoJerarquia;
  }
  public void setFindDigitoJerarquia(String findDigitoJerarquia) {
    this.findDigitoJerarquia = findDigitoJerarquia;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public JerarquiaDocente getJerarquiaDocente() {
    if (this.jerarquiaDocente == null) {
      this.jerarquiaDocente = new JerarquiaDocente();
    }
    return this.jerarquiaDocente;
  }

  public JerarquiaDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListVigencia()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = JerarquiaDocente.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findJerarquiaDocenteByDigitoJerarquia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findJerarquiaDocenteByDigitoJerarquia(this.findDigitoJerarquia);
      this.showJerarquiaDocenteByDigitoJerarquia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showJerarquiaDocenteByDigitoJerarquia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findDigitoJerarquia = null;
    this.findNombre = null;

    return null;
  }

  public String findJerarquiaDocenteByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findJerarquiaDocenteByNombre(this.findNombre);
      this.showJerarquiaDocenteByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showJerarquiaDocenteByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findDigitoJerarquia = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowJerarquiaDocenteByDigitoJerarquia() {
    return this.showJerarquiaDocenteByDigitoJerarquia;
  }
  public boolean isShowJerarquiaDocenteByNombre() {
    return this.showJerarquiaDocenteByNombre;
  }

  public String selectJerarquiaDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idJerarquiaDocente = 
      Long.parseLong((String)requestParameterMap.get("idJerarquiaDocente"));
    try
    {
      this.jerarquiaDocente = 
        this.docenteFacade.findJerarquiaDocenteById(
        idJerarquiaDocente);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.jerarquiaDocente = null;
    this.showJerarquiaDocenteByDigitoJerarquia = false;
    this.showJerarquiaDocenteByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.docenteFacade.addJerarquiaDocente(
          this.jerarquiaDocente);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.docenteFacade.updateJerarquiaDocente(
          this.jerarquiaDocente);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.docenteFacade.deleteJerarquiaDocente(
        this.jerarquiaDocente);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.jerarquiaDocente = new JerarquiaDocente();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.jerarquiaDocente.setIdJerarquiaDocente(identityGenerator.getNextSequenceNumber("sigefirrhh.base.docente.JerarquiaDocente"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.jerarquiaDocente = new JerarquiaDocente();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}