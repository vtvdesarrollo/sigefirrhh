package sigefirrhh.base.docente;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class DocenteNoGenFacade extends DocenteFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DocenteNoGenBusiness docenteNoGenBusiness = new DocenteNoGenBusiness();

  public Collection findTurnoDocenteByTurnoAndCondicion(String turno, String condicion) throws Exception
  {
    try {
      this.txn.open();
      return this.docenteNoGenBusiness.findTurnoDocenteByTurnoAndCondicion(turno, condicion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}