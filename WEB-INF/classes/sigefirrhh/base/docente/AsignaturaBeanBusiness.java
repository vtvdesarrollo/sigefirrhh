package sigefirrhh.base.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class AsignaturaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAsignatura(Asignatura asignatura)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Asignatura asignaturaNew = 
      (Asignatura)BeanUtils.cloneBean(
      asignatura);

    pm.makePersistent(asignaturaNew);
  }

  public void updateAsignatura(Asignatura asignatura) throws Exception
  {
    Asignatura asignaturaModify = 
      findAsignaturaById(asignatura.getIdAsignatura());

    BeanUtils.copyProperties(asignaturaModify, asignatura);
  }

  public void deleteAsignatura(Asignatura asignatura) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Asignatura asignaturaDelete = 
      findAsignaturaById(asignatura.getIdAsignatura());
    pm.deletePersistent(asignaturaDelete);
  }

  public Asignatura findAsignaturaById(long idAsignatura) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAsignatura == pIdAsignatura";
    Query query = pm.newQuery(Asignatura.class, filter);

    query.declareParameters("long pIdAsignatura");

    parameters.put("pIdAsignatura", new Long(idAsignatura));

    Collection colAsignatura = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAsignatura.iterator();
    return (Asignatura)iterator.next();
  }

  public Collection findAsignaturaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent asignaturaExtent = pm.getExtent(
      Asignatura.class, true);
    Query query = pm.newQuery(asignaturaExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodAsignatura(String codAsignatura)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codAsignatura == pCodAsignatura";

    Query query = pm.newQuery(Asignatura.class, filter);

    query.declareParameters("java.lang.String pCodAsignatura");
    HashMap parameters = new HashMap();

    parameters.put("pCodAsignatura", new String(codAsignatura));

    Collection colAsignatura = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAsignatura);

    return colAsignatura;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Asignatura.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    Collection colAsignatura = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAsignatura);

    return colAsignatura;
  }
}