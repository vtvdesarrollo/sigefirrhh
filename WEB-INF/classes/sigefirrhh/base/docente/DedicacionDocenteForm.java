package sigefirrhh.base.docente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class DedicacionDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(DedicacionDocenteForm.class.getName());
  private DedicacionDocente dedicacionDocente;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DocenteFacade docenteFacade = new DocenteFacade();
  private boolean showDedicacionDocenteByDigitoDedicacion;
  private boolean showDedicacionDocenteByNombre;
  private String findDigitoDedicacion;
  private String findNombre;
  private Object stateResultDedicacionDocenteByDigitoDedicacion = null;

  private Object stateResultDedicacionDocenteByNombre = null;

  public String getFindDigitoDedicacion()
  {
    return this.findDigitoDedicacion;
  }
  public void setFindDigitoDedicacion(String findDigitoDedicacion) {
    this.findDigitoDedicacion = findDigitoDedicacion;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public DedicacionDocente getDedicacionDocente() {
    if (this.dedicacionDocente == null) {
      this.dedicacionDocente = new DedicacionDocente();
    }
    return this.dedicacionDocente;
  }

  public DedicacionDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListVigencia()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = DedicacionDocente.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findDedicacionDocenteByDigitoDedicacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findDedicacionDocenteByDigitoDedicacion(this.findDigitoDedicacion);
      this.showDedicacionDocenteByDigitoDedicacion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDedicacionDocenteByDigitoDedicacion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findDigitoDedicacion = null;
    this.findNombre = null;

    return null;
  }

  public String findDedicacionDocenteByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.docenteFacade.findDedicacionDocenteByNombre(this.findNombre);
      this.showDedicacionDocenteByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDedicacionDocenteByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findDigitoDedicacion = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowDedicacionDocenteByDigitoDedicacion() {
    return this.showDedicacionDocenteByDigitoDedicacion;
  }
  public boolean isShowDedicacionDocenteByNombre() {
    return this.showDedicacionDocenteByNombre;
  }

  public String selectDedicacionDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idDedicacionDocente = 
      Long.parseLong((String)requestParameterMap.get("idDedicacionDocente"));
    try
    {
      this.dedicacionDocente = 
        this.docenteFacade.findDedicacionDocenteById(
        idDedicacionDocente);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.dedicacionDocente = null;
    this.showDedicacionDocenteByDigitoDedicacion = false;
    this.showDedicacionDocenteByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.docenteFacade.addDedicacionDocente(
          this.dedicacionDocente);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.docenteFacade.updateDedicacionDocente(
          this.dedicacionDocente);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.docenteFacade.deleteDedicacionDocente(
        this.dedicacionDocente);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.dedicacionDocente = new DedicacionDocente();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.dedicacionDocente.setIdDedicacionDocente(identityGenerator.getNextSequenceNumber("sigefirrhh.base.docente.DedicacionDocente"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.dedicacionDocente = new DedicacionDocente();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}