package sigefirrhh.base.judicial;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Materia
  implements Serializable, PersistenceCapable
{
  private long idMateria;
  private Clasificacion clasificacion;
  private String codMateria;
  private String nombre;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "clasificacion", "codMateria", "idMateria", "idSitp", "nombre", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.judicial.Clasificacion"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 26, 21, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcodMateria(this) + "  -  " + 
      jdoGetnombre(this);
  }

  public Clasificacion getClasificacion()
  {
    return jdoGetclasificacion(this);
  }

  public void setClasificacion(Clasificacion clasificacion)
  {
    jdoSetclasificacion(this, clasificacion);
  }

  public String getCodMateria()
  {
    return jdoGetcodMateria(this);
  }

  public void setCodMateria(String codMateria)
  {
    jdoSetcodMateria(this, codMateria);
  }

  public long getIdMateria()
  {
    return jdoGetidMateria(this);
  }

  public void setIdMateria(long idMateria)
  {
    jdoSetidMateria(this, idMateria);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.judicial.Materia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Materia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Materia localMateria = new Materia();
    localMateria.jdoFlags = 1;
    localMateria.jdoStateManager = paramStateManager;
    return localMateria;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Materia localMateria = new Materia();
    localMateria.jdoCopyKeyFieldsFromObjectId(paramObject);
    localMateria.jdoFlags = 1;
    localMateria.jdoStateManager = paramStateManager;
    return localMateria;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.clasificacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codMateria);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idMateria);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.clasificacion = ((Clasificacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codMateria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idMateria = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Materia paramMateria, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramMateria == null)
        throw new IllegalArgumentException("arg1");
      this.clasificacion = paramMateria.clasificacion;
      return;
    case 1:
      if (paramMateria == null)
        throw new IllegalArgumentException("arg1");
      this.codMateria = paramMateria.codMateria;
      return;
    case 2:
      if (paramMateria == null)
        throw new IllegalArgumentException("arg1");
      this.idMateria = paramMateria.idMateria;
      return;
    case 3:
      if (paramMateria == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramMateria.idSitp;
      return;
    case 4:
      if (paramMateria == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramMateria.nombre;
      return;
    case 5:
      if (paramMateria == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramMateria.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Materia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Materia localMateria = (Materia)paramObject;
    if (localMateria.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localMateria, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new MateriaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new MateriaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MateriaPK))
      throw new IllegalArgumentException("arg1");
    MateriaPK localMateriaPK = (MateriaPK)paramObject;
    localMateriaPK.idMateria = this.idMateria;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MateriaPK))
      throw new IllegalArgumentException("arg1");
    MateriaPK localMateriaPK = (MateriaPK)paramObject;
    this.idMateria = localMateriaPK.idMateria;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MateriaPK))
      throw new IllegalArgumentException("arg2");
    MateriaPK localMateriaPK = (MateriaPK)paramObject;
    localMateriaPK.idMateria = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MateriaPK))
      throw new IllegalArgumentException("arg2");
    MateriaPK localMateriaPK = (MateriaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localMateriaPK.idMateria);
  }

  private static final Clasificacion jdoGetclasificacion(Materia paramMateria)
  {
    StateManager localStateManager = paramMateria.jdoStateManager;
    if (localStateManager == null)
      return paramMateria.clasificacion;
    if (localStateManager.isLoaded(paramMateria, jdoInheritedFieldCount + 0))
      return paramMateria.clasificacion;
    return (Clasificacion)localStateManager.getObjectField(paramMateria, jdoInheritedFieldCount + 0, paramMateria.clasificacion);
  }

  private static final void jdoSetclasificacion(Materia paramMateria, Clasificacion paramClasificacion)
  {
    StateManager localStateManager = paramMateria.jdoStateManager;
    if (localStateManager == null)
    {
      paramMateria.clasificacion = paramClasificacion;
      return;
    }
    localStateManager.setObjectField(paramMateria, jdoInheritedFieldCount + 0, paramMateria.clasificacion, paramClasificacion);
  }

  private static final String jdoGetcodMateria(Materia paramMateria)
  {
    if (paramMateria.jdoFlags <= 0)
      return paramMateria.codMateria;
    StateManager localStateManager = paramMateria.jdoStateManager;
    if (localStateManager == null)
      return paramMateria.codMateria;
    if (localStateManager.isLoaded(paramMateria, jdoInheritedFieldCount + 1))
      return paramMateria.codMateria;
    return localStateManager.getStringField(paramMateria, jdoInheritedFieldCount + 1, paramMateria.codMateria);
  }

  private static final void jdoSetcodMateria(Materia paramMateria, String paramString)
  {
    if (paramMateria.jdoFlags == 0)
    {
      paramMateria.codMateria = paramString;
      return;
    }
    StateManager localStateManager = paramMateria.jdoStateManager;
    if (localStateManager == null)
    {
      paramMateria.codMateria = paramString;
      return;
    }
    localStateManager.setStringField(paramMateria, jdoInheritedFieldCount + 1, paramMateria.codMateria, paramString);
  }

  private static final long jdoGetidMateria(Materia paramMateria)
  {
    return paramMateria.idMateria;
  }

  private static final void jdoSetidMateria(Materia paramMateria, long paramLong)
  {
    StateManager localStateManager = paramMateria.jdoStateManager;
    if (localStateManager == null)
    {
      paramMateria.idMateria = paramLong;
      return;
    }
    localStateManager.setLongField(paramMateria, jdoInheritedFieldCount + 2, paramMateria.idMateria, paramLong);
  }

  private static final int jdoGetidSitp(Materia paramMateria)
  {
    if (paramMateria.jdoFlags <= 0)
      return paramMateria.idSitp;
    StateManager localStateManager = paramMateria.jdoStateManager;
    if (localStateManager == null)
      return paramMateria.idSitp;
    if (localStateManager.isLoaded(paramMateria, jdoInheritedFieldCount + 3))
      return paramMateria.idSitp;
    return localStateManager.getIntField(paramMateria, jdoInheritedFieldCount + 3, paramMateria.idSitp);
  }

  private static final void jdoSetidSitp(Materia paramMateria, int paramInt)
  {
    if (paramMateria.jdoFlags == 0)
    {
      paramMateria.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramMateria.jdoStateManager;
    if (localStateManager == null)
    {
      paramMateria.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramMateria, jdoInheritedFieldCount + 3, paramMateria.idSitp, paramInt);
  }

  private static final String jdoGetnombre(Materia paramMateria)
  {
    if (paramMateria.jdoFlags <= 0)
      return paramMateria.nombre;
    StateManager localStateManager = paramMateria.jdoStateManager;
    if (localStateManager == null)
      return paramMateria.nombre;
    if (localStateManager.isLoaded(paramMateria, jdoInheritedFieldCount + 4))
      return paramMateria.nombre;
    return localStateManager.getStringField(paramMateria, jdoInheritedFieldCount + 4, paramMateria.nombre);
  }

  private static final void jdoSetnombre(Materia paramMateria, String paramString)
  {
    if (paramMateria.jdoFlags == 0)
    {
      paramMateria.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramMateria.jdoStateManager;
    if (localStateManager == null)
    {
      paramMateria.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramMateria, jdoInheritedFieldCount + 4, paramMateria.nombre, paramString);
  }

  private static final Date jdoGettiempoSitp(Materia paramMateria)
  {
    if (paramMateria.jdoFlags <= 0)
      return paramMateria.tiempoSitp;
    StateManager localStateManager = paramMateria.jdoStateManager;
    if (localStateManager == null)
      return paramMateria.tiempoSitp;
    if (localStateManager.isLoaded(paramMateria, jdoInheritedFieldCount + 5))
      return paramMateria.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramMateria, jdoInheritedFieldCount + 5, paramMateria.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Materia paramMateria, Date paramDate)
  {
    if (paramMateria.jdoFlags == 0)
    {
      paramMateria.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramMateria.jdoStateManager;
    if (localStateManager == null)
    {
      paramMateria.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramMateria, jdoInheritedFieldCount + 5, paramMateria.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}