package sigefirrhh.base.judicial;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Instancia
  implements Serializable, PersistenceCapable
{
  private long idInstancia;
  private Clasificacion clasificacion;
  private String codInstancia;
  private String nombre;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "clasificacion", "codInstancia", "idInstancia", "idSitp", "nombre", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.judicial.Clasificacion"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 26, 21, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcodInstancia(this) + "  -  " + 
      jdoGetnombre(this);
  }

  public Clasificacion getClasificacion()
  {
    return jdoGetclasificacion(this);
  }

  public void setClasificacion(Clasificacion clasificacion)
  {
    jdoSetclasificacion(this, clasificacion);
  }

  public String getCodInstancia()
  {
    return jdoGetcodInstancia(this);
  }

  public void setCodInstancia(String codInstancia)
  {
    jdoSetcodInstancia(this, codInstancia);
  }

  public long getIdInstancia()
  {
    return jdoGetidInstancia(this);
  }

  public void setIdInstancia(long idInstancia)
  {
    jdoSetidInstancia(this, idInstancia);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.judicial.Instancia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Instancia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Instancia localInstancia = new Instancia();
    localInstancia.jdoFlags = 1;
    localInstancia.jdoStateManager = paramStateManager;
    return localInstancia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Instancia localInstancia = new Instancia();
    localInstancia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localInstancia.jdoFlags = 1;
    localInstancia.jdoStateManager = paramStateManager;
    return localInstancia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.clasificacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codInstancia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idInstancia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.clasificacion = ((Clasificacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codInstancia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idInstancia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Instancia paramInstancia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramInstancia == null)
        throw new IllegalArgumentException("arg1");
      this.clasificacion = paramInstancia.clasificacion;
      return;
    case 1:
      if (paramInstancia == null)
        throw new IllegalArgumentException("arg1");
      this.codInstancia = paramInstancia.codInstancia;
      return;
    case 2:
      if (paramInstancia == null)
        throw new IllegalArgumentException("arg1");
      this.idInstancia = paramInstancia.idInstancia;
      return;
    case 3:
      if (paramInstancia == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramInstancia.idSitp;
      return;
    case 4:
      if (paramInstancia == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramInstancia.nombre;
      return;
    case 5:
      if (paramInstancia == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramInstancia.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Instancia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Instancia localInstancia = (Instancia)paramObject;
    if (localInstancia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localInstancia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new InstanciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new InstanciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof InstanciaPK))
      throw new IllegalArgumentException("arg1");
    InstanciaPK localInstanciaPK = (InstanciaPK)paramObject;
    localInstanciaPK.idInstancia = this.idInstancia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof InstanciaPK))
      throw new IllegalArgumentException("arg1");
    InstanciaPK localInstanciaPK = (InstanciaPK)paramObject;
    this.idInstancia = localInstanciaPK.idInstancia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof InstanciaPK))
      throw new IllegalArgumentException("arg2");
    InstanciaPK localInstanciaPK = (InstanciaPK)paramObject;
    localInstanciaPK.idInstancia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof InstanciaPK))
      throw new IllegalArgumentException("arg2");
    InstanciaPK localInstanciaPK = (InstanciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localInstanciaPK.idInstancia);
  }

  private static final Clasificacion jdoGetclasificacion(Instancia paramInstancia)
  {
    StateManager localStateManager = paramInstancia.jdoStateManager;
    if (localStateManager == null)
      return paramInstancia.clasificacion;
    if (localStateManager.isLoaded(paramInstancia, jdoInheritedFieldCount + 0))
      return paramInstancia.clasificacion;
    return (Clasificacion)localStateManager.getObjectField(paramInstancia, jdoInheritedFieldCount + 0, paramInstancia.clasificacion);
  }

  private static final void jdoSetclasificacion(Instancia paramInstancia, Clasificacion paramClasificacion)
  {
    StateManager localStateManager = paramInstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstancia.clasificacion = paramClasificacion;
      return;
    }
    localStateManager.setObjectField(paramInstancia, jdoInheritedFieldCount + 0, paramInstancia.clasificacion, paramClasificacion);
  }

  private static final String jdoGetcodInstancia(Instancia paramInstancia)
  {
    if (paramInstancia.jdoFlags <= 0)
      return paramInstancia.codInstancia;
    StateManager localStateManager = paramInstancia.jdoStateManager;
    if (localStateManager == null)
      return paramInstancia.codInstancia;
    if (localStateManager.isLoaded(paramInstancia, jdoInheritedFieldCount + 1))
      return paramInstancia.codInstancia;
    return localStateManager.getStringField(paramInstancia, jdoInheritedFieldCount + 1, paramInstancia.codInstancia);
  }

  private static final void jdoSetcodInstancia(Instancia paramInstancia, String paramString)
  {
    if (paramInstancia.jdoFlags == 0)
    {
      paramInstancia.codInstancia = paramString;
      return;
    }
    StateManager localStateManager = paramInstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstancia.codInstancia = paramString;
      return;
    }
    localStateManager.setStringField(paramInstancia, jdoInheritedFieldCount + 1, paramInstancia.codInstancia, paramString);
  }

  private static final long jdoGetidInstancia(Instancia paramInstancia)
  {
    return paramInstancia.idInstancia;
  }

  private static final void jdoSetidInstancia(Instancia paramInstancia, long paramLong)
  {
    StateManager localStateManager = paramInstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstancia.idInstancia = paramLong;
      return;
    }
    localStateManager.setLongField(paramInstancia, jdoInheritedFieldCount + 2, paramInstancia.idInstancia, paramLong);
  }

  private static final int jdoGetidSitp(Instancia paramInstancia)
  {
    if (paramInstancia.jdoFlags <= 0)
      return paramInstancia.idSitp;
    StateManager localStateManager = paramInstancia.jdoStateManager;
    if (localStateManager == null)
      return paramInstancia.idSitp;
    if (localStateManager.isLoaded(paramInstancia, jdoInheritedFieldCount + 3))
      return paramInstancia.idSitp;
    return localStateManager.getIntField(paramInstancia, jdoInheritedFieldCount + 3, paramInstancia.idSitp);
  }

  private static final void jdoSetidSitp(Instancia paramInstancia, int paramInt)
  {
    if (paramInstancia.jdoFlags == 0)
    {
      paramInstancia.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramInstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstancia.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramInstancia, jdoInheritedFieldCount + 3, paramInstancia.idSitp, paramInt);
  }

  private static final String jdoGetnombre(Instancia paramInstancia)
  {
    if (paramInstancia.jdoFlags <= 0)
      return paramInstancia.nombre;
    StateManager localStateManager = paramInstancia.jdoStateManager;
    if (localStateManager == null)
      return paramInstancia.nombre;
    if (localStateManager.isLoaded(paramInstancia, jdoInheritedFieldCount + 4))
      return paramInstancia.nombre;
    return localStateManager.getStringField(paramInstancia, jdoInheritedFieldCount + 4, paramInstancia.nombre);
  }

  private static final void jdoSetnombre(Instancia paramInstancia, String paramString)
  {
    if (paramInstancia.jdoFlags == 0)
    {
      paramInstancia.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramInstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstancia.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramInstancia, jdoInheritedFieldCount + 4, paramInstancia.nombre, paramString);
  }

  private static final Date jdoGettiempoSitp(Instancia paramInstancia)
  {
    if (paramInstancia.jdoFlags <= 0)
      return paramInstancia.tiempoSitp;
    StateManager localStateManager = paramInstancia.jdoStateManager;
    if (localStateManager == null)
      return paramInstancia.tiempoSitp;
    if (localStateManager.isLoaded(paramInstancia, jdoInheritedFieldCount + 5))
      return paramInstancia.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramInstancia, jdoInheritedFieldCount + 5, paramInstancia.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Instancia paramInstancia, Date paramDate)
  {
    if (paramInstancia.jdoFlags == 0)
    {
      paramInstancia.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramInstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramInstancia.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramInstancia, jdoInheritedFieldCount + 5, paramInstancia.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}