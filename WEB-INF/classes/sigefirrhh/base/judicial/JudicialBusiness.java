package sigefirrhh.base.judicial;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class JudicialBusiness extends AbstractBusiness
  implements Serializable
{
  private ClasificacionBeanBusiness clasificacionBeanBusiness = new ClasificacionBeanBusiness();

  private DependenciaJudicialBeanBusiness dependenciaJudicialBeanBusiness = new DependenciaJudicialBeanBusiness();

  private InstanciaBeanBusiness instanciaBeanBusiness = new InstanciaBeanBusiness();

  private MateriaBeanBusiness materiaBeanBusiness = new MateriaBeanBusiness();

  private OperacionBeanBusiness operacionBeanBusiness = new OperacionBeanBusiness();

  public void addClasificacion(Clasificacion clasificacion)
    throws Exception
  {
    this.clasificacionBeanBusiness.addClasificacion(clasificacion);
  }

  public void updateClasificacion(Clasificacion clasificacion) throws Exception {
    this.clasificacionBeanBusiness.updateClasificacion(clasificacion);
  }

  public void deleteClasificacion(Clasificacion clasificacion) throws Exception {
    this.clasificacionBeanBusiness.deleteClasificacion(clasificacion);
  }

  public Clasificacion findClasificacionById(long clasificacionId) throws Exception {
    return this.clasificacionBeanBusiness.findClasificacionById(clasificacionId);
  }

  public Collection findAllClasificacion() throws Exception {
    return this.clasificacionBeanBusiness.findClasificacionAll();
  }

  public Collection findClasificacionByCodClasificacion(String codClasificacion)
    throws Exception
  {
    return this.clasificacionBeanBusiness.findByCodClasificacion(codClasificacion);
  }

  public Collection findClasificacionByNombre(String nombre)
    throws Exception
  {
    return this.clasificacionBeanBusiness.findByNombre(nombre);
  }

  public void addDependenciaJudicial(DependenciaJudicial dependenciaJudicial)
    throws Exception
  {
    this.dependenciaJudicialBeanBusiness.addDependenciaJudicial(dependenciaJudicial);
  }

  public void updateDependenciaJudicial(DependenciaJudicial dependenciaJudicial) throws Exception {
    this.dependenciaJudicialBeanBusiness.updateDependenciaJudicial(dependenciaJudicial);
  }

  public void deleteDependenciaJudicial(DependenciaJudicial dependenciaJudicial) throws Exception {
    this.dependenciaJudicialBeanBusiness.deleteDependenciaJudicial(dependenciaJudicial);
  }

  public DependenciaJudicial findDependenciaJudicialById(long dependenciaJudicialId) throws Exception {
    return this.dependenciaJudicialBeanBusiness.findDependenciaJudicialById(dependenciaJudicialId);
  }

  public Collection findAllDependenciaJudicial() throws Exception {
    return this.dependenciaJudicialBeanBusiness.findDependenciaJudicialAll();
  }

  public Collection findDependenciaJudicialByDependencia(long idDependencia)
    throws Exception
  {
    return this.dependenciaJudicialBeanBusiness.findByDependencia(idDependencia);
  }

  public void addInstancia(Instancia instancia)
    throws Exception
  {
    this.instanciaBeanBusiness.addInstancia(instancia);
  }

  public void updateInstancia(Instancia instancia) throws Exception {
    this.instanciaBeanBusiness.updateInstancia(instancia);
  }

  public void deleteInstancia(Instancia instancia) throws Exception {
    this.instanciaBeanBusiness.deleteInstancia(instancia);
  }

  public Instancia findInstanciaById(long instanciaId) throws Exception {
    return this.instanciaBeanBusiness.findInstanciaById(instanciaId);
  }

  public Collection findAllInstancia() throws Exception {
    return this.instanciaBeanBusiness.findInstanciaAll();
  }

  public Collection findInstanciaByClasificacion(long idClasificacion)
    throws Exception
  {
    return this.instanciaBeanBusiness.findByClasificacion(idClasificacion);
  }

  public Collection findInstanciaByCodInstancia(String codInstancia)
    throws Exception
  {
    return this.instanciaBeanBusiness.findByCodInstancia(codInstancia);
  }

  public Collection findInstanciaByNombre(String nombre)
    throws Exception
  {
    return this.instanciaBeanBusiness.findByNombre(nombre);
  }

  public void addMateria(Materia materia)
    throws Exception
  {
    this.materiaBeanBusiness.addMateria(materia);
  }

  public void updateMateria(Materia materia) throws Exception {
    this.materiaBeanBusiness.updateMateria(materia);
  }

  public void deleteMateria(Materia materia) throws Exception {
    this.materiaBeanBusiness.deleteMateria(materia);
  }

  public Materia findMateriaById(long materiaId) throws Exception {
    return this.materiaBeanBusiness.findMateriaById(materiaId);
  }

  public Collection findAllMateria() throws Exception {
    return this.materiaBeanBusiness.findMateriaAll();
  }

  public Collection findMateriaByClasificacion(long idClasificacion)
    throws Exception
  {
    return this.materiaBeanBusiness.findByClasificacion(idClasificacion);
  }

  public Collection findMateriaByCodMateria(String codMateria)
    throws Exception
  {
    return this.materiaBeanBusiness.findByCodMateria(codMateria);
  }

  public Collection findMateriaByNombre(String nombre)
    throws Exception
  {
    return this.materiaBeanBusiness.findByNombre(nombre);
  }

  public void addOperacion(Operacion operacion)
    throws Exception
  {
    this.operacionBeanBusiness.addOperacion(operacion);
  }

  public void updateOperacion(Operacion operacion) throws Exception {
    this.operacionBeanBusiness.updateOperacion(operacion);
  }

  public void deleteOperacion(Operacion operacion) throws Exception {
    this.operacionBeanBusiness.deleteOperacion(operacion);
  }

  public Operacion findOperacionById(long operacionId) throws Exception {
    return this.operacionBeanBusiness.findOperacionById(operacionId);
  }

  public Collection findAllOperacion() throws Exception {
    return this.operacionBeanBusiness.findOperacionAll();
  }

  public Collection findOperacionByCodOperacion(String codOperacion)
    throws Exception
  {
    return this.operacionBeanBusiness.findByCodOperacion(codOperacion);
  }

  public Collection findOperacionByNombre(String nombre)
    throws Exception
  {
    return this.operacionBeanBusiness.findByNombre(nombre);
  }
}