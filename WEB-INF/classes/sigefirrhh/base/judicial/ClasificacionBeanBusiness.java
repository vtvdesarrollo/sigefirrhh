package sigefirrhh.base.judicial;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ClasificacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addClasificacion(Clasificacion clasificacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Clasificacion clasificacionNew = 
      (Clasificacion)BeanUtils.cloneBean(
      clasificacion);

    pm.makePersistent(clasificacionNew);
  }

  public void updateClasificacion(Clasificacion clasificacion) throws Exception
  {
    Clasificacion clasificacionModify = 
      findClasificacionById(clasificacion.getIdClasificacion());

    BeanUtils.copyProperties(clasificacionModify, clasificacion);
  }

  public void deleteClasificacion(Clasificacion clasificacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Clasificacion clasificacionDelete = 
      findClasificacionById(clasificacion.getIdClasificacion());
    pm.deletePersistent(clasificacionDelete);
  }

  public Clasificacion findClasificacionById(long idClasificacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idClasificacion == pIdClasificacion";
    Query query = pm.newQuery(Clasificacion.class, filter);

    query.declareParameters("long pIdClasificacion");

    parameters.put("pIdClasificacion", new Long(idClasificacion));

    Collection colClasificacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colClasificacion.iterator();
    return (Clasificacion)iterator.next();
  }

  public Collection findClasificacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent clasificacionExtent = pm.getExtent(
      Clasificacion.class, true);
    Query query = pm.newQuery(clasificacionExtent);
    query.setOrdering("codClasificacion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodClasificacion(String codClasificacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codClasificacion == pCodClasificacion";

    Query query = pm.newQuery(Clasificacion.class, filter);

    query.declareParameters("java.lang.String pCodClasificacion");
    HashMap parameters = new HashMap();

    parameters.put("pCodClasificacion", new String(codClasificacion));

    query.setOrdering("codClasificacion ascending");

    Collection colClasificacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colClasificacion);

    return colClasificacion;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Clasificacion.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codClasificacion ascending");

    Collection colClasificacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colClasificacion);

    return colClasificacion;
  }
}