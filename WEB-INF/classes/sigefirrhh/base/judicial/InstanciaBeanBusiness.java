package sigefirrhh.base.judicial;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class InstanciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addInstancia(Instancia instancia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Instancia instanciaNew = 
      (Instancia)BeanUtils.cloneBean(
      instancia);

    ClasificacionBeanBusiness clasificacionBeanBusiness = new ClasificacionBeanBusiness();

    if (instanciaNew.getClasificacion() != null) {
      instanciaNew.setClasificacion(
        clasificacionBeanBusiness.findClasificacionById(
        instanciaNew.getClasificacion().getIdClasificacion()));
    }
    pm.makePersistent(instanciaNew);
  }

  public void updateInstancia(Instancia instancia) throws Exception
  {
    Instancia instanciaModify = 
      findInstanciaById(instancia.getIdInstancia());

    ClasificacionBeanBusiness clasificacionBeanBusiness = new ClasificacionBeanBusiness();

    if (instancia.getClasificacion() != null) {
      instancia.setClasificacion(
        clasificacionBeanBusiness.findClasificacionById(
        instancia.getClasificacion().getIdClasificacion()));
    }

    BeanUtils.copyProperties(instanciaModify, instancia);
  }

  public void deleteInstancia(Instancia instancia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Instancia instanciaDelete = 
      findInstanciaById(instancia.getIdInstancia());
    pm.deletePersistent(instanciaDelete);
  }

  public Instancia findInstanciaById(long idInstancia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idInstancia == pIdInstancia";
    Query query = pm.newQuery(Instancia.class, filter);

    query.declareParameters("long pIdInstancia");

    parameters.put("pIdInstancia", new Long(idInstancia));

    Collection colInstancia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colInstancia.iterator();
    return (Instancia)iterator.next();
  }

  public Collection findInstanciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent instanciaExtent = pm.getExtent(
      Instancia.class, true);
    Query query = pm.newQuery(instanciaExtent);
    query.setOrdering("codInstancia ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByClasificacion(long idClasificacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "clasificacion.idClasificacion == pIdClasificacion";

    Query query = pm.newQuery(Instancia.class, filter);

    query.declareParameters("long pIdClasificacion");
    HashMap parameters = new HashMap();

    parameters.put("pIdClasificacion", new Long(idClasificacion));

    query.setOrdering("codInstancia ascending");

    Collection colInstancia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colInstancia);

    return colInstancia;
  }

  public Collection findByCodInstancia(String codInstancia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codInstancia == pCodInstancia";

    Query query = pm.newQuery(Instancia.class, filter);

    query.declareParameters("java.lang.String pCodInstancia");
    HashMap parameters = new HashMap();

    parameters.put("pCodInstancia", new String(codInstancia));

    query.setOrdering("codInstancia ascending");

    Collection colInstancia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colInstancia);

    return colInstancia;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Instancia.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codInstancia ascending");

    Collection colInstancia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colInstancia);

    return colInstancia;
  }
}