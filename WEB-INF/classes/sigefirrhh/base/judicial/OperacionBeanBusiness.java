package sigefirrhh.base.judicial;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class OperacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addOperacion(Operacion operacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Operacion operacionNew = 
      (Operacion)BeanUtils.cloneBean(
      operacion);

    pm.makePersistent(operacionNew);
  }

  public void updateOperacion(Operacion operacion) throws Exception
  {
    Operacion operacionModify = 
      findOperacionById(operacion.getIdOperacion());

    BeanUtils.copyProperties(operacionModify, operacion);
  }

  public void deleteOperacion(Operacion operacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Operacion operacionDelete = 
      findOperacionById(operacion.getIdOperacion());
    pm.deletePersistent(operacionDelete);
  }

  public Operacion findOperacionById(long idOperacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idOperacion == pIdOperacion";
    Query query = pm.newQuery(Operacion.class, filter);

    query.declareParameters("long pIdOperacion");

    parameters.put("pIdOperacion", new Long(idOperacion));

    Collection colOperacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colOperacion.iterator();
    return (Operacion)iterator.next();
  }

  public Collection findOperacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent operacionExtent = pm.getExtent(
      Operacion.class, true);
    Query query = pm.newQuery(operacionExtent);
    query.setOrdering("codOperacion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodOperacion(String codOperacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codOperacion == pCodOperacion";

    Query query = pm.newQuery(Operacion.class, filter);

    query.declareParameters("java.lang.String pCodOperacion");
    HashMap parameters = new HashMap();

    parameters.put("pCodOperacion", new String(codOperacion));

    query.setOrdering("codOperacion ascending");

    Collection colOperacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colOperacion);

    return colOperacion;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Operacion.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codOperacion ascending");

    Collection colOperacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colOperacion);

    return colOperacion;
  }
}