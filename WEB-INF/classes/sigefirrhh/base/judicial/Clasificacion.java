package sigefirrhh.base.judicial;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Clasificacion
  implements Serializable, PersistenceCapable
{
  private long idClasificacion;
  private String codClasificacion;
  private String nombre;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codClasificacion", "idClasificacion", "idSitp", "nombre", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcodClasificacion(this) + "  -  " + 
      jdoGetnombre(this);
  }

  public String getCodClasificacion()
  {
    return jdoGetcodClasificacion(this);
  }

  public void setCodClasificacion(String codClasificacion)
  {
    jdoSetcodClasificacion(this, codClasificacion);
  }

  public long getIdClasificacion()
  {
    return jdoGetidClasificacion(this);
  }

  public void setIdClasificacion(long idClasificacion)
  {
    jdoSetidClasificacion(this, idClasificacion);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.judicial.Clasificacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Clasificacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Clasificacion localClasificacion = new Clasificacion();
    localClasificacion.jdoFlags = 1;
    localClasificacion.jdoStateManager = paramStateManager;
    return localClasificacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Clasificacion localClasificacion = new Clasificacion();
    localClasificacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localClasificacion.jdoFlags = 1;
    localClasificacion.jdoStateManager = paramStateManager;
    return localClasificacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codClasificacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idClasificacion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codClasificacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idClasificacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Clasificacion paramClasificacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramClasificacion == null)
        throw new IllegalArgumentException("arg1");
      this.codClasificacion = paramClasificacion.codClasificacion;
      return;
    case 1:
      if (paramClasificacion == null)
        throw new IllegalArgumentException("arg1");
      this.idClasificacion = paramClasificacion.idClasificacion;
      return;
    case 2:
      if (paramClasificacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramClasificacion.idSitp;
      return;
    case 3:
      if (paramClasificacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramClasificacion.nombre;
      return;
    case 4:
      if (paramClasificacion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramClasificacion.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Clasificacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Clasificacion localClasificacion = (Clasificacion)paramObject;
    if (localClasificacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localClasificacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ClasificacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ClasificacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ClasificacionPK))
      throw new IllegalArgumentException("arg1");
    ClasificacionPK localClasificacionPK = (ClasificacionPK)paramObject;
    localClasificacionPK.idClasificacion = this.idClasificacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ClasificacionPK))
      throw new IllegalArgumentException("arg1");
    ClasificacionPK localClasificacionPK = (ClasificacionPK)paramObject;
    this.idClasificacion = localClasificacionPK.idClasificacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ClasificacionPK))
      throw new IllegalArgumentException("arg2");
    ClasificacionPK localClasificacionPK = (ClasificacionPK)paramObject;
    localClasificacionPK.idClasificacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ClasificacionPK))
      throw new IllegalArgumentException("arg2");
    ClasificacionPK localClasificacionPK = (ClasificacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localClasificacionPK.idClasificacion);
  }

  private static final String jdoGetcodClasificacion(Clasificacion paramClasificacion)
  {
    if (paramClasificacion.jdoFlags <= 0)
      return paramClasificacion.codClasificacion;
    StateManager localStateManager = paramClasificacion.jdoStateManager;
    if (localStateManager == null)
      return paramClasificacion.codClasificacion;
    if (localStateManager.isLoaded(paramClasificacion, jdoInheritedFieldCount + 0))
      return paramClasificacion.codClasificacion;
    return localStateManager.getStringField(paramClasificacion, jdoInheritedFieldCount + 0, paramClasificacion.codClasificacion);
  }

  private static final void jdoSetcodClasificacion(Clasificacion paramClasificacion, String paramString)
  {
    if (paramClasificacion.jdoFlags == 0)
    {
      paramClasificacion.codClasificacion = paramString;
      return;
    }
    StateManager localStateManager = paramClasificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacion.codClasificacion = paramString;
      return;
    }
    localStateManager.setStringField(paramClasificacion, jdoInheritedFieldCount + 0, paramClasificacion.codClasificacion, paramString);
  }

  private static final long jdoGetidClasificacion(Clasificacion paramClasificacion)
  {
    return paramClasificacion.idClasificacion;
  }

  private static final void jdoSetidClasificacion(Clasificacion paramClasificacion, long paramLong)
  {
    StateManager localStateManager = paramClasificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacion.idClasificacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramClasificacion, jdoInheritedFieldCount + 1, paramClasificacion.idClasificacion, paramLong);
  }

  private static final int jdoGetidSitp(Clasificacion paramClasificacion)
  {
    if (paramClasificacion.jdoFlags <= 0)
      return paramClasificacion.idSitp;
    StateManager localStateManager = paramClasificacion.jdoStateManager;
    if (localStateManager == null)
      return paramClasificacion.idSitp;
    if (localStateManager.isLoaded(paramClasificacion, jdoInheritedFieldCount + 2))
      return paramClasificacion.idSitp;
    return localStateManager.getIntField(paramClasificacion, jdoInheritedFieldCount + 2, paramClasificacion.idSitp);
  }

  private static final void jdoSetidSitp(Clasificacion paramClasificacion, int paramInt)
  {
    if (paramClasificacion.jdoFlags == 0)
    {
      paramClasificacion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramClasificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramClasificacion, jdoInheritedFieldCount + 2, paramClasificacion.idSitp, paramInt);
  }

  private static final String jdoGetnombre(Clasificacion paramClasificacion)
  {
    if (paramClasificacion.jdoFlags <= 0)
      return paramClasificacion.nombre;
    StateManager localStateManager = paramClasificacion.jdoStateManager;
    if (localStateManager == null)
      return paramClasificacion.nombre;
    if (localStateManager.isLoaded(paramClasificacion, jdoInheritedFieldCount + 3))
      return paramClasificacion.nombre;
    return localStateManager.getStringField(paramClasificacion, jdoInheritedFieldCount + 3, paramClasificacion.nombre);
  }

  private static final void jdoSetnombre(Clasificacion paramClasificacion, String paramString)
  {
    if (paramClasificacion.jdoFlags == 0)
    {
      paramClasificacion.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramClasificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacion.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramClasificacion, jdoInheritedFieldCount + 3, paramClasificacion.nombre, paramString);
  }

  private static final Date jdoGettiempoSitp(Clasificacion paramClasificacion)
  {
    if (paramClasificacion.jdoFlags <= 0)
      return paramClasificacion.tiempoSitp;
    StateManager localStateManager = paramClasificacion.jdoStateManager;
    if (localStateManager == null)
      return paramClasificacion.tiempoSitp;
    if (localStateManager.isLoaded(paramClasificacion, jdoInheritedFieldCount + 4))
      return paramClasificacion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramClasificacion, jdoInheritedFieldCount + 4, paramClasificacion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Clasificacion paramClasificacion, Date paramDate)
  {
    if (paramClasificacion.jdoFlags == 0)
    {
      paramClasificacion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramClasificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramClasificacion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramClasificacion, jdoInheritedFieldCount + 4, paramClasificacion.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}