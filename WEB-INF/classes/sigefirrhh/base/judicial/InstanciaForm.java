package sigefirrhh.base.judicial;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class InstanciaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(InstanciaForm.class.getName());
  private Instancia instancia;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private JudicialFacade judicialFacade = new JudicialFacade();
  private boolean showInstanciaByClasificacion;
  private boolean showInstanciaByCodInstancia;
  private boolean showInstanciaByNombre;
  private String findSelectClasificacion;
  private String findCodInstancia;
  private String findNombre;
  private Collection findColClasificacion;
  private Collection colClasificacion;
  private String selectClasificacion;
  private Object stateResultInstanciaByClasificacion = null;

  private Object stateResultInstanciaByCodInstancia = null;

  private Object stateResultInstanciaByNombre = null;

  public String getFindSelectClasificacion()
  {
    return this.findSelectClasificacion;
  }
  public void setFindSelectClasificacion(String valClasificacion) {
    this.findSelectClasificacion = valClasificacion;
  }

  public Collection getFindColClasificacion() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColClasificacion.iterator();
    Clasificacion clasificacion = null;
    while (iterator.hasNext()) {
      clasificacion = (Clasificacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(clasificacion.getIdClasificacion()), 
        clasificacion.toString()));
    }
    return col;
  }
  public String getFindCodInstancia() {
    return this.findCodInstancia;
  }
  public void setFindCodInstancia(String findCodInstancia) {
    this.findCodInstancia = findCodInstancia;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectClasificacion()
  {
    return this.selectClasificacion;
  }
  public void setSelectClasificacion(String valClasificacion) {
    Iterator iterator = this.colClasificacion.iterator();
    Clasificacion clasificacion = null;
    this.instancia.setClasificacion(null);
    while (iterator.hasNext()) {
      clasificacion = (Clasificacion)iterator.next();
      if (String.valueOf(clasificacion.getIdClasificacion()).equals(
        valClasificacion)) {
        this.instancia.setClasificacion(
          clasificacion);
        break;
      }
    }
    this.selectClasificacion = valClasificacion;
  }
  public Collection getResult() {
    return this.result;
  }

  public Instancia getInstancia() {
    if (this.instancia == null) {
      this.instancia = new Instancia();
    }
    return this.instancia;
  }

  public InstanciaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColClasificacion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colClasificacion.iterator();
    Clasificacion clasificacion = null;
    while (iterator.hasNext()) {
      clasificacion = (Clasificacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(clasificacion.getIdClasificacion()), 
        clasificacion.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColClasificacion = 
        this.judicialFacade.findAllClasificacion();

      this.colClasificacion = 
        this.judicialFacade.findAllClasificacion();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findInstanciaByClasificacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.judicialFacade.findInstanciaByClasificacion(Long.valueOf(this.findSelectClasificacion).longValue());
      this.showInstanciaByClasificacion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showInstanciaByClasificacion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectClasificacion = null;
    this.findCodInstancia = null;
    this.findNombre = null;

    return null;
  }

  public String findInstanciaByCodInstancia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.judicialFacade.findInstanciaByCodInstancia(this.findCodInstancia);
      this.showInstanciaByCodInstancia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showInstanciaByCodInstancia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectClasificacion = null;
    this.findCodInstancia = null;
    this.findNombre = null;

    return null;
  }

  public String findInstanciaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.judicialFacade.findInstanciaByNombre(this.findNombre);
      this.showInstanciaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showInstanciaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectClasificacion = null;
    this.findCodInstancia = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowInstanciaByClasificacion() {
    return this.showInstanciaByClasificacion;
  }
  public boolean isShowInstanciaByCodInstancia() {
    return this.showInstanciaByCodInstancia;
  }
  public boolean isShowInstanciaByNombre() {
    return this.showInstanciaByNombre;
  }

  public String selectInstancia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectClasificacion = null;

    long idInstancia = 
      Long.parseLong((String)requestParameterMap.get("idInstancia"));
    try
    {
      this.instancia = 
        this.judicialFacade.findInstanciaById(
        idInstancia);
      if (this.instancia.getClasificacion() != null) {
        this.selectClasificacion = 
          String.valueOf(this.instancia.getClasificacion().getIdClasificacion());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.instancia = null;
    this.showInstanciaByClasificacion = false;
    this.showInstanciaByCodInstancia = false;
    this.showInstanciaByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.instancia.getTiempoSitp() != null) && 
      (this.instancia.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.judicialFacade.addInstancia(
          this.instancia);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.judicialFacade.updateInstancia(
          this.instancia);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.judicialFacade.deleteInstancia(
        this.instancia);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.instancia = new Instancia();

    this.selectClasificacion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.instancia.setIdInstancia(identityGenerator.getNextSequenceNumber("sigefirrhh.base.judicial.Instancia"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.instancia = new Instancia();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}