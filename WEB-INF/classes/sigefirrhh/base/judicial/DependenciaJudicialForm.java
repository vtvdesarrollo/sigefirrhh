package sigefirrhh.base.judicial;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class DependenciaJudicialForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(DependenciaJudicialForm.class.getName());
  private DependenciaJudicial dependenciaJudicial;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private JudicialFacade judicialFacade = new JudicialFacade();
  private boolean showDependenciaJudicialByDependencia;
  private String findSelectDependencia;
  private Collection findColDependencia;
  private Collection colDependencia;
  private Collection colOperacion;
  private Collection colClasificacionForMateria;
  private Collection colMateria;
  private Collection colClasificacionForInstancia;
  private Collection colInstancia;
  private String selectDependencia;
  private String selectOperacion;
  private String selectClasificacionForMateria;
  private String selectMateria;
  private String selectClasificacionForInstancia;
  private String selectInstancia;
  private Object stateResultDependenciaJudicialByDependencia = null;

  public String getFindSelectDependencia()
  {
    return this.findSelectDependencia;
  }
  public void setFindSelectDependencia(String valDependencia) {
    this.findSelectDependencia = valDependencia;
  }

  public Collection getFindColDependencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }

  public String getSelectDependencia()
  {
    return this.selectDependencia;
  }
  public void setSelectDependencia(String valDependencia) {
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    this.dependenciaJudicial.setDependencia(null);
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      if (String.valueOf(dependencia.getIdDependencia()).equals(
        valDependencia)) {
        this.dependenciaJudicial.setDependencia(
          dependencia);
        break;
      }
    }
    this.selectDependencia = valDependencia;
  }
  public String getSelectOperacion() {
    return this.selectOperacion;
  }
  public void setSelectOperacion(String valOperacion) {
    Iterator iterator = this.colOperacion.iterator();
    Operacion operacion = null;
    this.dependenciaJudicial.setOperacion(null);
    while (iterator.hasNext()) {
      operacion = (Operacion)iterator.next();
      if (String.valueOf(operacion.getIdOperacion()).equals(
        valOperacion)) {
        this.dependenciaJudicial.setOperacion(
          operacion);
        break;
      }
    }
    this.selectOperacion = valOperacion;
  }
  public String getSelectClasificacionForMateria() {
    return this.selectClasificacionForMateria;
  }
  public void setSelectClasificacionForMateria(String valClasificacionForMateria) {
    this.selectClasificacionForMateria = valClasificacionForMateria;
  }
  public void changeClasificacionForMateria(ValueChangeEvent event) {
    long idClasificacion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colMateria = null;
      if (idClasificacion > 0L) {
        this.colMateria = 
          this.judicialFacade.findMateriaByClasificacion(
          idClasificacion);
      } else {
        this.selectMateria = null;
        this.dependenciaJudicial.setMateria(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectMateria = null;
      this.dependenciaJudicial.setMateria(
        null);
    }
  }

  public boolean isShowClasificacionForMateria() { return this.colClasificacionForMateria != null; }

  public String getSelectMateria() {
    return this.selectMateria;
  }
  public void setSelectMateria(String valMateria) {
    Iterator iterator = this.colMateria.iterator();
    Materia materia = null;
    this.dependenciaJudicial.setMateria(null);
    while (iterator.hasNext()) {
      materia = (Materia)iterator.next();
      if (String.valueOf(materia.getIdMateria()).equals(
        valMateria)) {
        this.dependenciaJudicial.setMateria(
          materia);
        break;
      }
    }
    this.selectMateria = valMateria;
  }
  public boolean isShowMateria() {
    return this.colMateria != null;
  }
  public String getSelectClasificacionForInstancia() {
    return this.selectClasificacionForInstancia;
  }
  public void setSelectClasificacionForInstancia(String valClasificacionForInstancia) {
    this.selectClasificacionForInstancia = valClasificacionForInstancia;
  }
  public void changeClasificacionForInstancia(ValueChangeEvent event) {
    long idClasificacion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colInstancia = null;
      if (idClasificacion > 0L) {
        this.colInstancia = 
          this.judicialFacade.findInstanciaByClasificacion(
          idClasificacion);
      } else {
        this.selectInstancia = null;
        this.dependenciaJudicial.setInstancia(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectInstancia = null;
      this.dependenciaJudicial.setInstancia(
        null);
    }
  }

  public boolean isShowClasificacionForInstancia() { return this.colClasificacionForInstancia != null; }

  public String getSelectInstancia() {
    return this.selectInstancia;
  }
  public void setSelectInstancia(String valInstancia) {
    Iterator iterator = this.colInstancia.iterator();
    Instancia instancia = null;
    this.dependenciaJudicial.setInstancia(null);
    while (iterator.hasNext()) {
      instancia = (Instancia)iterator.next();
      if (String.valueOf(instancia.getIdInstancia()).equals(
        valInstancia)) {
        this.dependenciaJudicial.setInstancia(
          instancia);
        break;
      }
    }
    this.selectInstancia = valInstancia;
  }
  public boolean isShowInstancia() {
    return this.colInstancia != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public DependenciaJudicial getDependenciaJudicial() {
    if (this.dependenciaJudicial == null) {
      this.dependenciaJudicial = new DependenciaJudicial();
    }
    return this.dependenciaJudicial;
  }

  public DependenciaJudicialForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }

  public Collection getColOperacion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colOperacion.iterator();
    Operacion operacion = null;
    while (iterator.hasNext()) {
      operacion = (Operacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(operacion.getIdOperacion()), 
        operacion.toString()));
    }
    return col;
  }

  public Collection getColClasificacionForMateria() {
    Collection col = new ArrayList();
    Iterator iterator = this.colClasificacionForMateria.iterator();
    Clasificacion clasificacionForMateria = null;
    while (iterator.hasNext()) {
      clasificacionForMateria = (Clasificacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(clasificacionForMateria.getIdClasificacion()), 
        clasificacionForMateria.toString()));
    }
    return col;
  }

  public Collection getColMateria()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colMateria.iterator();
    Materia materia = null;
    while (iterator.hasNext()) {
      materia = (Materia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(materia.getIdMateria()), 
        materia.toString()));
    }
    return col;
  }

  public Collection getColClasificacionForInstancia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colClasificacionForInstancia.iterator();
    Clasificacion clasificacionForInstancia = null;
    while (iterator.hasNext()) {
      clasificacionForInstancia = (Clasificacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(clasificacionForInstancia.getIdClasificacion()), 
        clasificacionForInstancia.toString()));
    }
    return col;
  }

  public Collection getColInstancia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colInstancia.iterator();
    Instancia instancia = null;
    while (iterator.hasNext()) {
      instancia = (Instancia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(instancia.getIdInstancia()), 
        instancia.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColDependencia = 
        this.estructuraFacade.findDependenciaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colDependencia = 
        this.estructuraFacade.findDependenciaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colOperacion = 
        this.judicialFacade.findAllOperacion();
      this.colClasificacionForMateria = 
        this.judicialFacade.findAllClasificacion();
      this.colClasificacionForInstancia = 
        this.judicialFacade.findAllClasificacion();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findDependenciaJudicialByDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.judicialFacade.findDependenciaJudicialByDependencia(Long.valueOf(this.findSelectDependencia).longValue());
      this.showDependenciaJudicialByDependencia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDependenciaJudicialByDependencia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectDependencia = null;

    return null;
  }

  public boolean isShowDependenciaJudicialByDependencia() {
    return this.showDependenciaJudicialByDependencia;
  }

  public String selectDependenciaJudicial()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectDependencia = null;
    this.selectOperacion = null;
    this.selectMateria = null;
    this.selectClasificacionForMateria = null;

    this.selectInstancia = null;
    this.selectClasificacionForInstancia = null;

    long idDependenciaJudicial = 
      Long.parseLong((String)requestParameterMap.get("idDependenciaJudicial"));
    try
    {
      this.dependenciaJudicial = 
        this.judicialFacade.findDependenciaJudicialById(
        idDependenciaJudicial);
      if (this.dependenciaJudicial.getDependencia() != null) {
        this.selectDependencia = 
          String.valueOf(this.dependenciaJudicial.getDependencia().getIdDependencia());
      }
      if (this.dependenciaJudicial.getOperacion() != null) {
        this.selectOperacion = 
          String.valueOf(this.dependenciaJudicial.getOperacion().getIdOperacion());
      }
      if (this.dependenciaJudicial.getMateria() != null) {
        this.selectMateria = 
          String.valueOf(this.dependenciaJudicial.getMateria().getIdMateria());
      }
      if (this.dependenciaJudicial.getInstancia() != null) {
        this.selectInstancia = 
          String.valueOf(this.dependenciaJudicial.getInstancia().getIdInstancia());
      }

      Materia materia = null;
      Clasificacion clasificacionForMateria = null;
      Instancia instancia = null;
      Clasificacion clasificacionForInstancia = null;

      if (this.dependenciaJudicial.getMateria() != null) {
        long idMateria = 
          this.dependenciaJudicial.getMateria().getIdMateria();
        this.selectMateria = String.valueOf(idMateria);
        materia = this.judicialFacade.findMateriaById(
          idMateria);
        this.colMateria = this.judicialFacade.findMateriaByClasificacion(
          materia.getClasificacion().getIdClasificacion());

        long idClasificacionForMateria = 
          this.dependenciaJudicial.getMateria().getClasificacion().getIdClasificacion();
        this.selectClasificacionForMateria = String.valueOf(idClasificacionForMateria);
        clasificacionForMateria = 
          this.judicialFacade.findClasificacionById(
          idClasificacionForMateria);
        this.colClasificacionForMateria = 
          this.judicialFacade.findAllClasificacion();
      }
      if (this.dependenciaJudicial.getInstancia() != null) {
        long idInstancia = 
          this.dependenciaJudicial.getInstancia().getIdInstancia();
        this.selectInstancia = String.valueOf(idInstancia);
        instancia = this.judicialFacade.findInstanciaById(
          idInstancia);
        this.colInstancia = this.judicialFacade.findInstanciaByClasificacion(
          instancia.getClasificacion().getIdClasificacion());

        long idClasificacionForInstancia = 
          this.dependenciaJudicial.getInstancia().getClasificacion().getIdClasificacion();
        this.selectClasificacionForInstancia = String.valueOf(idClasificacionForInstancia);
        clasificacionForInstancia = 
          this.judicialFacade.findClasificacionById(
          idClasificacionForInstancia);
        this.colClasificacionForInstancia = 
          this.judicialFacade.findAllClasificacion();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.dependenciaJudicial = null;
    this.showDependenciaJudicialByDependencia = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.dependenciaJudicial.getTiempoSitp() != null) && 
      (this.dependenciaJudicial.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.judicialFacade.addDependenciaJudicial(
          this.dependenciaJudicial);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.judicialFacade.updateDependenciaJudicial(
          this.dependenciaJudicial);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.judicialFacade.deleteDependenciaJudicial(
        this.dependenciaJudicial);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.dependenciaJudicial = new DependenciaJudicial();

    this.selectDependencia = null;

    this.selectOperacion = null;

    this.selectMateria = null;

    this.selectClasificacionForMateria = null;

    this.selectInstancia = null;

    this.selectClasificacionForInstancia = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.dependenciaJudicial.setIdDependenciaJudicial(identityGenerator.getNextSequenceNumber("sigefirrhh.base.judicial.DependenciaJudicial"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.dependenciaJudicial = new DependenciaJudicial();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}