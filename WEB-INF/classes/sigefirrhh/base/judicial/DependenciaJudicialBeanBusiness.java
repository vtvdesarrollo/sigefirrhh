package sigefirrhh.base.judicial;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.DependenciaBeanBusiness;

public class DependenciaJudicialBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDependenciaJudicial(DependenciaJudicial dependenciaJudicial)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DependenciaJudicial dependenciaJudicialNew = 
      (DependenciaJudicial)BeanUtils.cloneBean(
      dependenciaJudicial);

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (dependenciaJudicialNew.getDependencia() != null) {
      dependenciaJudicialNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        dependenciaJudicialNew.getDependencia().getIdDependencia()));
    }

    OperacionBeanBusiness operacionBeanBusiness = new OperacionBeanBusiness();

    if (dependenciaJudicialNew.getOperacion() != null) {
      dependenciaJudicialNew.setOperacion(
        operacionBeanBusiness.findOperacionById(
        dependenciaJudicialNew.getOperacion().getIdOperacion()));
    }

    MateriaBeanBusiness materiaBeanBusiness = new MateriaBeanBusiness();

    if (dependenciaJudicialNew.getMateria() != null) {
      dependenciaJudicialNew.setMateria(
        materiaBeanBusiness.findMateriaById(
        dependenciaJudicialNew.getMateria().getIdMateria()));
    }

    InstanciaBeanBusiness instanciaBeanBusiness = new InstanciaBeanBusiness();

    if (dependenciaJudicialNew.getInstancia() != null) {
      dependenciaJudicialNew.setInstancia(
        instanciaBeanBusiness.findInstanciaById(
        dependenciaJudicialNew.getInstancia().getIdInstancia()));
    }
    pm.makePersistent(dependenciaJudicialNew);
  }

  public void updateDependenciaJudicial(DependenciaJudicial dependenciaJudicial) throws Exception
  {
    DependenciaJudicial dependenciaJudicialModify = 
      findDependenciaJudicialById(dependenciaJudicial.getIdDependenciaJudicial());

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (dependenciaJudicial.getDependencia() != null) {
      dependenciaJudicial.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        dependenciaJudicial.getDependencia().getIdDependencia()));
    }

    OperacionBeanBusiness operacionBeanBusiness = new OperacionBeanBusiness();

    if (dependenciaJudicial.getOperacion() != null) {
      dependenciaJudicial.setOperacion(
        operacionBeanBusiness.findOperacionById(
        dependenciaJudicial.getOperacion().getIdOperacion()));
    }

    MateriaBeanBusiness materiaBeanBusiness = new MateriaBeanBusiness();

    if (dependenciaJudicial.getMateria() != null) {
      dependenciaJudicial.setMateria(
        materiaBeanBusiness.findMateriaById(
        dependenciaJudicial.getMateria().getIdMateria()));
    }

    InstanciaBeanBusiness instanciaBeanBusiness = new InstanciaBeanBusiness();

    if (dependenciaJudicial.getInstancia() != null) {
      dependenciaJudicial.setInstancia(
        instanciaBeanBusiness.findInstanciaById(
        dependenciaJudicial.getInstancia().getIdInstancia()));
    }

    BeanUtils.copyProperties(dependenciaJudicialModify, dependenciaJudicial);
  }

  public void deleteDependenciaJudicial(DependenciaJudicial dependenciaJudicial) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DependenciaJudicial dependenciaJudicialDelete = 
      findDependenciaJudicialById(dependenciaJudicial.getIdDependenciaJudicial());
    pm.deletePersistent(dependenciaJudicialDelete);
  }

  public DependenciaJudicial findDependenciaJudicialById(long idDependenciaJudicial) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDependenciaJudicial == pIdDependenciaJudicial";
    Query query = pm.newQuery(DependenciaJudicial.class, filter);

    query.declareParameters("long pIdDependenciaJudicial");

    parameters.put("pIdDependenciaJudicial", new Long(idDependenciaJudicial));

    Collection colDependenciaJudicial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDependenciaJudicial.iterator();
    return (DependenciaJudicial)iterator.next();
  }

  public Collection findDependenciaJudicialAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent dependenciaJudicialExtent = pm.getExtent(
      DependenciaJudicial.class, true);
    Query query = pm.newQuery(dependenciaJudicialExtent);
    query.setOrdering("dependencia.codDependencia ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByDependencia(long idDependencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "dependencia.idDependencia == pIdDependencia";

    Query query = pm.newQuery(DependenciaJudicial.class, filter);

    query.declareParameters("long pIdDependencia");
    HashMap parameters = new HashMap();

    parameters.put("pIdDependencia", new Long(idDependencia));

    query.setOrdering("dependencia.codDependencia ascending");

    Collection colDependenciaJudicial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDependenciaJudicial);

    return colDependenciaJudicial;
  }
}