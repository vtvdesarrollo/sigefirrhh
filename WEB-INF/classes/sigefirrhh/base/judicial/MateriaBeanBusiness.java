package sigefirrhh.base.judicial;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class MateriaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addMateria(Materia materia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Materia materiaNew = 
      (Materia)BeanUtils.cloneBean(
      materia);

    ClasificacionBeanBusiness clasificacionBeanBusiness = new ClasificacionBeanBusiness();

    if (materiaNew.getClasificacion() != null) {
      materiaNew.setClasificacion(
        clasificacionBeanBusiness.findClasificacionById(
        materiaNew.getClasificacion().getIdClasificacion()));
    }
    pm.makePersistent(materiaNew);
  }

  public void updateMateria(Materia materia) throws Exception
  {
    Materia materiaModify = 
      findMateriaById(materia.getIdMateria());

    ClasificacionBeanBusiness clasificacionBeanBusiness = new ClasificacionBeanBusiness();

    if (materia.getClasificacion() != null) {
      materia.setClasificacion(
        clasificacionBeanBusiness.findClasificacionById(
        materia.getClasificacion().getIdClasificacion()));
    }

    BeanUtils.copyProperties(materiaModify, materia);
  }

  public void deleteMateria(Materia materia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Materia materiaDelete = 
      findMateriaById(materia.getIdMateria());
    pm.deletePersistent(materiaDelete);
  }

  public Materia findMateriaById(long idMateria) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idMateria == pIdMateria";
    Query query = pm.newQuery(Materia.class, filter);

    query.declareParameters("long pIdMateria");

    parameters.put("pIdMateria", new Long(idMateria));

    Collection colMateria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colMateria.iterator();
    return (Materia)iterator.next();
  }

  public Collection findMateriaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent materiaExtent = pm.getExtent(
      Materia.class, true);
    Query query = pm.newQuery(materiaExtent);
    query.setOrdering("codMateria ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByClasificacion(long idClasificacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "clasificacion.idClasificacion == pIdClasificacion";

    Query query = pm.newQuery(Materia.class, filter);

    query.declareParameters("long pIdClasificacion");
    HashMap parameters = new HashMap();

    parameters.put("pIdClasificacion", new Long(idClasificacion));

    query.setOrdering("codMateria ascending");

    Collection colMateria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMateria);

    return colMateria;
  }

  public Collection findByCodMateria(String codMateria)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codMateria == pCodMateria";

    Query query = pm.newQuery(Materia.class, filter);

    query.declareParameters("java.lang.String pCodMateria");
    HashMap parameters = new HashMap();

    parameters.put("pCodMateria", new String(codMateria));

    query.setOrdering("codMateria ascending");

    Collection colMateria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMateria);

    return colMateria;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Materia.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codMateria ascending");

    Collection colMateria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMateria);

    return colMateria;
  }
}