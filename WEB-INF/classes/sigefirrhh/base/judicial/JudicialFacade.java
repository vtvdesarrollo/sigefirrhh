package sigefirrhh.base.judicial;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class JudicialFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private JudicialBusiness judicialBusiness = new JudicialBusiness();

  public void addClasificacion(Clasificacion clasificacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.judicialBusiness.addClasificacion(clasificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateClasificacion(Clasificacion clasificacion) throws Exception
  {
    try { this.txn.open();
      this.judicialBusiness.updateClasificacion(clasificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteClasificacion(Clasificacion clasificacion) throws Exception
  {
    try { this.txn.open();
      this.judicialBusiness.deleteClasificacion(clasificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Clasificacion findClasificacionById(long clasificacionId) throws Exception
  {
    try { this.txn.open();
      Clasificacion clasificacion = 
        this.judicialBusiness.findClasificacionById(clasificacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(clasificacion);
      return clasificacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllClasificacion() throws Exception
  {
    try { this.txn.open();
      return this.judicialBusiness.findAllClasificacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findClasificacionByCodClasificacion(String codClasificacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.judicialBusiness.findClasificacionByCodClasificacion(codClasificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findClasificacionByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.judicialBusiness.findClasificacionByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDependenciaJudicial(DependenciaJudicial dependenciaJudicial)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.judicialBusiness.addDependenciaJudicial(dependenciaJudicial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDependenciaJudicial(DependenciaJudicial dependenciaJudicial) throws Exception
  {
    try { this.txn.open();
      this.judicialBusiness.updateDependenciaJudicial(dependenciaJudicial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDependenciaJudicial(DependenciaJudicial dependenciaJudicial) throws Exception
  {
    try { this.txn.open();
      this.judicialBusiness.deleteDependenciaJudicial(dependenciaJudicial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DependenciaJudicial findDependenciaJudicialById(long dependenciaJudicialId) throws Exception
  {
    try { this.txn.open();
      DependenciaJudicial dependenciaJudicial = 
        this.judicialBusiness.findDependenciaJudicialById(dependenciaJudicialId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(dependenciaJudicial);
      return dependenciaJudicial;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDependenciaJudicial() throws Exception
  {
    try { this.txn.open();
      return this.judicialBusiness.findAllDependenciaJudicial();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDependenciaJudicialByDependencia(long idDependencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.judicialBusiness.findDependenciaJudicialByDependencia(idDependencia);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addInstancia(Instancia instancia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.judicialBusiness.addInstancia(instancia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateInstancia(Instancia instancia) throws Exception
  {
    try { this.txn.open();
      this.judicialBusiness.updateInstancia(instancia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteInstancia(Instancia instancia) throws Exception
  {
    try { this.txn.open();
      this.judicialBusiness.deleteInstancia(instancia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Instancia findInstanciaById(long instanciaId) throws Exception
  {
    try { this.txn.open();
      Instancia instancia = 
        this.judicialBusiness.findInstanciaById(instanciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(instancia);
      return instancia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllInstancia() throws Exception
  {
    try { this.txn.open();
      return this.judicialBusiness.findAllInstancia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findInstanciaByClasificacion(long idClasificacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.judicialBusiness.findInstanciaByClasificacion(idClasificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findInstanciaByCodInstancia(String codInstancia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.judicialBusiness.findInstanciaByCodInstancia(codInstancia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findInstanciaByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.judicialBusiness.findInstanciaByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addMateria(Materia materia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.judicialBusiness.addMateria(materia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateMateria(Materia materia) throws Exception
  {
    try { this.txn.open();
      this.judicialBusiness.updateMateria(materia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteMateria(Materia materia) throws Exception
  {
    try { this.txn.open();
      this.judicialBusiness.deleteMateria(materia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Materia findMateriaById(long materiaId) throws Exception
  {
    try { this.txn.open();
      Materia materia = 
        this.judicialBusiness.findMateriaById(materiaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(materia);
      return materia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllMateria() throws Exception
  {
    try { this.txn.open();
      return this.judicialBusiness.findAllMateria();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMateriaByClasificacion(long idClasificacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.judicialBusiness.findMateriaByClasificacion(idClasificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMateriaByCodMateria(String codMateria)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.judicialBusiness.findMateriaByCodMateria(codMateria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMateriaByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.judicialBusiness.findMateriaByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addOperacion(Operacion operacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.judicialBusiness.addOperacion(operacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateOperacion(Operacion operacion) throws Exception
  {
    try { this.txn.open();
      this.judicialBusiness.updateOperacion(operacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteOperacion(Operacion operacion) throws Exception
  {
    try { this.txn.open();
      this.judicialBusiness.deleteOperacion(operacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Operacion findOperacionById(long operacionId) throws Exception
  {
    try { this.txn.open();
      Operacion operacion = 
        this.judicialBusiness.findOperacionById(operacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(operacion);
      return operacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllOperacion() throws Exception
  {
    try { this.txn.open();
      return this.judicialBusiness.findAllOperacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findOperacionByCodOperacion(String codOperacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.judicialBusiness.findOperacionByCodOperacion(codOperacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findOperacionByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.judicialBusiness.findOperacionByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}