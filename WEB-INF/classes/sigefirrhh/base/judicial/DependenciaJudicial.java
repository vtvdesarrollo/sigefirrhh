package sigefirrhh.base.judicial;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Dependencia;

public class DependenciaJudicial
  implements Serializable, PersistenceCapable
{
  private long idDependenciaJudicial;
  private Dependencia dependencia;
  private Operacion operacion;
  private Materia materia;
  private Instancia instancia;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "dependencia", "idDependenciaJudicial", "idSitp", "instancia", "materia", "operacion", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.judicial.Instancia"), sunjdo$classForName$("sigefirrhh.base.judicial.Materia"), sunjdo$classForName$("sigefirrhh.base.judicial.Operacion"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 26, 24, 21, 26, 26, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdependencia(this).getCodDependencia() + "  -  " + 
      jdoGetdependencia(this).getNombre();
  }

  public Dependencia getDependencia()
  {
    return jdoGetdependencia(this);
  }

  public void setDependencia(Dependencia dependencia)
  {
    jdoSetdependencia(this, dependencia);
  }

  public long getIdDependenciaJudicial()
  {
    return jdoGetidDependenciaJudicial(this);
  }

  public void setIdDependenciaJudicial(long idDependenciaJudicial)
  {
    jdoSetidDependenciaJudicial(this, idDependenciaJudicial);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public Instancia getInstancia()
  {
    return jdoGetinstancia(this);
  }

  public void setInstancia(Instancia instancia)
  {
    jdoSetinstancia(this, instancia);
  }

  public Materia getMateria()
  {
    return jdoGetmateria(this);
  }

  public void setMateria(Materia materia)
  {
    jdoSetmateria(this, materia);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public Operacion getOperacion()
  {
    return jdoGetoperacion(this);
  }

  public void setOperacion(Operacion operacion)
  {
    jdoSetoperacion(this, operacion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.judicial.DependenciaJudicial"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DependenciaJudicial());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DependenciaJudicial localDependenciaJudicial = new DependenciaJudicial();
    localDependenciaJudicial.jdoFlags = 1;
    localDependenciaJudicial.jdoStateManager = paramStateManager;
    return localDependenciaJudicial;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DependenciaJudicial localDependenciaJudicial = new DependenciaJudicial();
    localDependenciaJudicial.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDependenciaJudicial.jdoFlags = 1;
    localDependenciaJudicial.jdoStateManager = paramStateManager;
    return localDependenciaJudicial;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDependenciaJudicial);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.instancia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.materia);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.operacion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDependenciaJudicial = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.instancia = ((Instancia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.materia = ((Materia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.operacion = ((Operacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DependenciaJudicial paramDependenciaJudicial, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDependenciaJudicial == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramDependenciaJudicial.dependencia;
      return;
    case 1:
      if (paramDependenciaJudicial == null)
        throw new IllegalArgumentException("arg1");
      this.idDependenciaJudicial = paramDependenciaJudicial.idDependenciaJudicial;
      return;
    case 2:
      if (paramDependenciaJudicial == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramDependenciaJudicial.idSitp;
      return;
    case 3:
      if (paramDependenciaJudicial == null)
        throw new IllegalArgumentException("arg1");
      this.instancia = paramDependenciaJudicial.instancia;
      return;
    case 4:
      if (paramDependenciaJudicial == null)
        throw new IllegalArgumentException("arg1");
      this.materia = paramDependenciaJudicial.materia;
      return;
    case 5:
      if (paramDependenciaJudicial == null)
        throw new IllegalArgumentException("arg1");
      this.operacion = paramDependenciaJudicial.operacion;
      return;
    case 6:
      if (paramDependenciaJudicial == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramDependenciaJudicial.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DependenciaJudicial))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DependenciaJudicial localDependenciaJudicial = (DependenciaJudicial)paramObject;
    if (localDependenciaJudicial.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDependenciaJudicial, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DependenciaJudicialPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DependenciaJudicialPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DependenciaJudicialPK))
      throw new IllegalArgumentException("arg1");
    DependenciaJudicialPK localDependenciaJudicialPK = (DependenciaJudicialPK)paramObject;
    localDependenciaJudicialPK.idDependenciaJudicial = this.idDependenciaJudicial;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DependenciaJudicialPK))
      throw new IllegalArgumentException("arg1");
    DependenciaJudicialPK localDependenciaJudicialPK = (DependenciaJudicialPK)paramObject;
    this.idDependenciaJudicial = localDependenciaJudicialPK.idDependenciaJudicial;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DependenciaJudicialPK))
      throw new IllegalArgumentException("arg2");
    DependenciaJudicialPK localDependenciaJudicialPK = (DependenciaJudicialPK)paramObject;
    localDependenciaJudicialPK.idDependenciaJudicial = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DependenciaJudicialPK))
      throw new IllegalArgumentException("arg2");
    DependenciaJudicialPK localDependenciaJudicialPK = (DependenciaJudicialPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localDependenciaJudicialPK.idDependenciaJudicial);
  }

  private static final Dependencia jdoGetdependencia(DependenciaJudicial paramDependenciaJudicial)
  {
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaJudicial.dependencia;
    if (localStateManager.isLoaded(paramDependenciaJudicial, jdoInheritedFieldCount + 0))
      return paramDependenciaJudicial.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramDependenciaJudicial, jdoInheritedFieldCount + 0, paramDependenciaJudicial.dependencia);
  }

  private static final void jdoSetdependencia(DependenciaJudicial paramDependenciaJudicial, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaJudicial.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramDependenciaJudicial, jdoInheritedFieldCount + 0, paramDependenciaJudicial.dependencia, paramDependencia);
  }

  private static final long jdoGetidDependenciaJudicial(DependenciaJudicial paramDependenciaJudicial)
  {
    return paramDependenciaJudicial.idDependenciaJudicial;
  }

  private static final void jdoSetidDependenciaJudicial(DependenciaJudicial paramDependenciaJudicial, long paramLong)
  {
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaJudicial.idDependenciaJudicial = paramLong;
      return;
    }
    localStateManager.setLongField(paramDependenciaJudicial, jdoInheritedFieldCount + 1, paramDependenciaJudicial.idDependenciaJudicial, paramLong);
  }

  private static final int jdoGetidSitp(DependenciaJudicial paramDependenciaJudicial)
  {
    if (paramDependenciaJudicial.jdoFlags <= 0)
      return paramDependenciaJudicial.idSitp;
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaJudicial.idSitp;
    if (localStateManager.isLoaded(paramDependenciaJudicial, jdoInheritedFieldCount + 2))
      return paramDependenciaJudicial.idSitp;
    return localStateManager.getIntField(paramDependenciaJudicial, jdoInheritedFieldCount + 2, paramDependenciaJudicial.idSitp);
  }

  private static final void jdoSetidSitp(DependenciaJudicial paramDependenciaJudicial, int paramInt)
  {
    if (paramDependenciaJudicial.jdoFlags == 0)
    {
      paramDependenciaJudicial.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaJudicial.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramDependenciaJudicial, jdoInheritedFieldCount + 2, paramDependenciaJudicial.idSitp, paramInt);
  }

  private static final Instancia jdoGetinstancia(DependenciaJudicial paramDependenciaJudicial)
  {
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaJudicial.instancia;
    if (localStateManager.isLoaded(paramDependenciaJudicial, jdoInheritedFieldCount + 3))
      return paramDependenciaJudicial.instancia;
    return (Instancia)localStateManager.getObjectField(paramDependenciaJudicial, jdoInheritedFieldCount + 3, paramDependenciaJudicial.instancia);
  }

  private static final void jdoSetinstancia(DependenciaJudicial paramDependenciaJudicial, Instancia paramInstancia)
  {
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaJudicial.instancia = paramInstancia;
      return;
    }
    localStateManager.setObjectField(paramDependenciaJudicial, jdoInheritedFieldCount + 3, paramDependenciaJudicial.instancia, paramInstancia);
  }

  private static final Materia jdoGetmateria(DependenciaJudicial paramDependenciaJudicial)
  {
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaJudicial.materia;
    if (localStateManager.isLoaded(paramDependenciaJudicial, jdoInheritedFieldCount + 4))
      return paramDependenciaJudicial.materia;
    return (Materia)localStateManager.getObjectField(paramDependenciaJudicial, jdoInheritedFieldCount + 4, paramDependenciaJudicial.materia);
  }

  private static final void jdoSetmateria(DependenciaJudicial paramDependenciaJudicial, Materia paramMateria)
  {
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaJudicial.materia = paramMateria;
      return;
    }
    localStateManager.setObjectField(paramDependenciaJudicial, jdoInheritedFieldCount + 4, paramDependenciaJudicial.materia, paramMateria);
  }

  private static final Operacion jdoGetoperacion(DependenciaJudicial paramDependenciaJudicial)
  {
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaJudicial.operacion;
    if (localStateManager.isLoaded(paramDependenciaJudicial, jdoInheritedFieldCount + 5))
      return paramDependenciaJudicial.operacion;
    return (Operacion)localStateManager.getObjectField(paramDependenciaJudicial, jdoInheritedFieldCount + 5, paramDependenciaJudicial.operacion);
  }

  private static final void jdoSetoperacion(DependenciaJudicial paramDependenciaJudicial, Operacion paramOperacion)
  {
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaJudicial.operacion = paramOperacion;
      return;
    }
    localStateManager.setObjectField(paramDependenciaJudicial, jdoInheritedFieldCount + 5, paramDependenciaJudicial.operacion, paramOperacion);
  }

  private static final Date jdoGettiempoSitp(DependenciaJudicial paramDependenciaJudicial)
  {
    if (paramDependenciaJudicial.jdoFlags <= 0)
      return paramDependenciaJudicial.tiempoSitp;
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
      return paramDependenciaJudicial.tiempoSitp;
    if (localStateManager.isLoaded(paramDependenciaJudicial, jdoInheritedFieldCount + 6))
      return paramDependenciaJudicial.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramDependenciaJudicial, jdoInheritedFieldCount + 6, paramDependenciaJudicial.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(DependenciaJudicial paramDependenciaJudicial, Date paramDate)
  {
    if (paramDependenciaJudicial.jdoFlags == 0)
    {
      paramDependenciaJudicial.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramDependenciaJudicial.jdoStateManager;
    if (localStateManager == null)
    {
      paramDependenciaJudicial.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramDependenciaJudicial, jdoInheritedFieldCount + 6, paramDependenciaJudicial.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}