package sigefirrhh.base.judicial;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Operacion
  implements Serializable, PersistenceCapable
{
  private long idOperacion;
  private String codOperacion;
  private String nombre;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codOperacion", "idOperacion", "idSitp", "nombre", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcodOperacion(this) + "  -  " + 
      jdoGetnombre(this);
  }

  public String getCodOperacion()
  {
    return jdoGetcodOperacion(this);
  }

  public void setCodOperacion(String codOperacion)
  {
    jdoSetcodOperacion(this, codOperacion);
  }

  public long getIdOperacion()
  {
    return jdoGetidOperacion(this);
  }

  public void setIdOperacion(long idOperacion)
  {
    jdoSetidOperacion(this, idOperacion);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.judicial.Operacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Operacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Operacion localOperacion = new Operacion();
    localOperacion.jdoFlags = 1;
    localOperacion.jdoStateManager = paramStateManager;
    return localOperacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Operacion localOperacion = new Operacion();
    localOperacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localOperacion.jdoFlags = 1;
    localOperacion.jdoStateManager = paramStateManager;
    return localOperacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codOperacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idOperacion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codOperacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idOperacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Operacion paramOperacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramOperacion == null)
        throw new IllegalArgumentException("arg1");
      this.codOperacion = paramOperacion.codOperacion;
      return;
    case 1:
      if (paramOperacion == null)
        throw new IllegalArgumentException("arg1");
      this.idOperacion = paramOperacion.idOperacion;
      return;
    case 2:
      if (paramOperacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramOperacion.idSitp;
      return;
    case 3:
      if (paramOperacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramOperacion.nombre;
      return;
    case 4:
      if (paramOperacion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramOperacion.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Operacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Operacion localOperacion = (Operacion)paramObject;
    if (localOperacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localOperacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new OperacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new OperacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof OperacionPK))
      throw new IllegalArgumentException("arg1");
    OperacionPK localOperacionPK = (OperacionPK)paramObject;
    localOperacionPK.idOperacion = this.idOperacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof OperacionPK))
      throw new IllegalArgumentException("arg1");
    OperacionPK localOperacionPK = (OperacionPK)paramObject;
    this.idOperacion = localOperacionPK.idOperacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof OperacionPK))
      throw new IllegalArgumentException("arg2");
    OperacionPK localOperacionPK = (OperacionPK)paramObject;
    localOperacionPK.idOperacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof OperacionPK))
      throw new IllegalArgumentException("arg2");
    OperacionPK localOperacionPK = (OperacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localOperacionPK.idOperacion);
  }

  private static final String jdoGetcodOperacion(Operacion paramOperacion)
  {
    if (paramOperacion.jdoFlags <= 0)
      return paramOperacion.codOperacion;
    StateManager localStateManager = paramOperacion.jdoStateManager;
    if (localStateManager == null)
      return paramOperacion.codOperacion;
    if (localStateManager.isLoaded(paramOperacion, jdoInheritedFieldCount + 0))
      return paramOperacion.codOperacion;
    return localStateManager.getStringField(paramOperacion, jdoInheritedFieldCount + 0, paramOperacion.codOperacion);
  }

  private static final void jdoSetcodOperacion(Operacion paramOperacion, String paramString)
  {
    if (paramOperacion.jdoFlags == 0)
    {
      paramOperacion.codOperacion = paramString;
      return;
    }
    StateManager localStateManager = paramOperacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramOperacion.codOperacion = paramString;
      return;
    }
    localStateManager.setStringField(paramOperacion, jdoInheritedFieldCount + 0, paramOperacion.codOperacion, paramString);
  }

  private static final long jdoGetidOperacion(Operacion paramOperacion)
  {
    return paramOperacion.idOperacion;
  }

  private static final void jdoSetidOperacion(Operacion paramOperacion, long paramLong)
  {
    StateManager localStateManager = paramOperacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramOperacion.idOperacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramOperacion, jdoInheritedFieldCount + 1, paramOperacion.idOperacion, paramLong);
  }

  private static final int jdoGetidSitp(Operacion paramOperacion)
  {
    if (paramOperacion.jdoFlags <= 0)
      return paramOperacion.idSitp;
    StateManager localStateManager = paramOperacion.jdoStateManager;
    if (localStateManager == null)
      return paramOperacion.idSitp;
    if (localStateManager.isLoaded(paramOperacion, jdoInheritedFieldCount + 2))
      return paramOperacion.idSitp;
    return localStateManager.getIntField(paramOperacion, jdoInheritedFieldCount + 2, paramOperacion.idSitp);
  }

  private static final void jdoSetidSitp(Operacion paramOperacion, int paramInt)
  {
    if (paramOperacion.jdoFlags == 0)
    {
      paramOperacion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramOperacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramOperacion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramOperacion, jdoInheritedFieldCount + 2, paramOperacion.idSitp, paramInt);
  }

  private static final String jdoGetnombre(Operacion paramOperacion)
  {
    if (paramOperacion.jdoFlags <= 0)
      return paramOperacion.nombre;
    StateManager localStateManager = paramOperacion.jdoStateManager;
    if (localStateManager == null)
      return paramOperacion.nombre;
    if (localStateManager.isLoaded(paramOperacion, jdoInheritedFieldCount + 3))
      return paramOperacion.nombre;
    return localStateManager.getStringField(paramOperacion, jdoInheritedFieldCount + 3, paramOperacion.nombre);
  }

  private static final void jdoSetnombre(Operacion paramOperacion, String paramString)
  {
    if (paramOperacion.jdoFlags == 0)
    {
      paramOperacion.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramOperacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramOperacion.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramOperacion, jdoInheritedFieldCount + 3, paramOperacion.nombre, paramString);
  }

  private static final Date jdoGettiempoSitp(Operacion paramOperacion)
  {
    if (paramOperacion.jdoFlags <= 0)
      return paramOperacion.tiempoSitp;
    StateManager localStateManager = paramOperacion.jdoStateManager;
    if (localStateManager == null)
      return paramOperacion.tiempoSitp;
    if (localStateManager.isLoaded(paramOperacion, jdoInheritedFieldCount + 4))
      return paramOperacion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramOperacion, jdoInheritedFieldCount + 4, paramOperacion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Operacion paramOperacion, Date paramDate)
  {
    if (paramOperacion.jdoFlags == 0)
    {
      paramOperacion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramOperacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramOperacion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramOperacion, jdoInheritedFieldCount + 4, paramOperacion.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}