package sigefirrhh.base.judicial;

import java.io.Serializable;

public class ClasificacionPK
  implements Serializable
{
  public long idClasificacion;

  public ClasificacionPK()
  {
  }

  public ClasificacionPK(long idClasificacion)
  {
    this.idClasificacion = idClasificacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ClasificacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ClasificacionPK thatPK)
  {
    return 
      this.idClasificacion == thatPK.idClasificacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idClasificacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idClasificacion);
  }
}