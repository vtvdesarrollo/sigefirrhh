package sigefirrhh.base.judicial;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class MateriaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(MateriaForm.class.getName());
  private Materia materia;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private JudicialFacade judicialFacade = new JudicialFacade();
  private boolean showMateriaByClasificacion;
  private boolean showMateriaByCodMateria;
  private boolean showMateriaByNombre;
  private String findSelectClasificacion;
  private String findCodMateria;
  private String findNombre;
  private Collection findColClasificacion;
  private Collection colClasificacion;
  private String selectClasificacion;
  private Object stateResultMateriaByClasificacion = null;

  private Object stateResultMateriaByCodMateria = null;

  private Object stateResultMateriaByNombre = null;

  public String getFindSelectClasificacion()
  {
    return this.findSelectClasificacion;
  }
  public void setFindSelectClasificacion(String valClasificacion) {
    this.findSelectClasificacion = valClasificacion;
  }

  public Collection getFindColClasificacion() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColClasificacion.iterator();
    Clasificacion clasificacion = null;
    while (iterator.hasNext()) {
      clasificacion = (Clasificacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(clasificacion.getIdClasificacion()), 
        clasificacion.toString()));
    }
    return col;
  }
  public String getFindCodMateria() {
    return this.findCodMateria;
  }
  public void setFindCodMateria(String findCodMateria) {
    this.findCodMateria = findCodMateria;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectClasificacion()
  {
    return this.selectClasificacion;
  }
  public void setSelectClasificacion(String valClasificacion) {
    Iterator iterator = this.colClasificacion.iterator();
    Clasificacion clasificacion = null;
    this.materia.setClasificacion(null);
    while (iterator.hasNext()) {
      clasificacion = (Clasificacion)iterator.next();
      if (String.valueOf(clasificacion.getIdClasificacion()).equals(
        valClasificacion)) {
        this.materia.setClasificacion(
          clasificacion);
        break;
      }
    }
    this.selectClasificacion = valClasificacion;
  }
  public Collection getResult() {
    return this.result;
  }

  public Materia getMateria() {
    if (this.materia == null) {
      this.materia = new Materia();
    }
    return this.materia;
  }

  public MateriaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColClasificacion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colClasificacion.iterator();
    Clasificacion clasificacion = null;
    while (iterator.hasNext()) {
      clasificacion = (Clasificacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(clasificacion.getIdClasificacion()), 
        clasificacion.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColClasificacion = 
        this.judicialFacade.findAllClasificacion();

      this.colClasificacion = 
        this.judicialFacade.findAllClasificacion();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findMateriaByClasificacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.judicialFacade.findMateriaByClasificacion(Long.valueOf(this.findSelectClasificacion).longValue());
      this.showMateriaByClasificacion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showMateriaByClasificacion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectClasificacion = null;
    this.findCodMateria = null;
    this.findNombre = null;

    return null;
  }

  public String findMateriaByCodMateria()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.judicialFacade.findMateriaByCodMateria(this.findCodMateria);
      this.showMateriaByCodMateria = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showMateriaByCodMateria)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectClasificacion = null;
    this.findCodMateria = null;
    this.findNombre = null;

    return null;
  }

  public String findMateriaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.judicialFacade.findMateriaByNombre(this.findNombre);
      this.showMateriaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showMateriaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectClasificacion = null;
    this.findCodMateria = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowMateriaByClasificacion() {
    return this.showMateriaByClasificacion;
  }
  public boolean isShowMateriaByCodMateria() {
    return this.showMateriaByCodMateria;
  }
  public boolean isShowMateriaByNombre() {
    return this.showMateriaByNombre;
  }

  public String selectMateria()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectClasificacion = null;

    long idMateria = 
      Long.parseLong((String)requestParameterMap.get("idMateria"));
    try
    {
      this.materia = 
        this.judicialFacade.findMateriaById(
        idMateria);
      if (this.materia.getClasificacion() != null) {
        this.selectClasificacion = 
          String.valueOf(this.materia.getClasificacion().getIdClasificacion());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.materia = null;
    this.showMateriaByClasificacion = false;
    this.showMateriaByCodMateria = false;
    this.showMateriaByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.materia.getTiempoSitp() != null) && 
      (this.materia.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.judicialFacade.addMateria(
          this.materia);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.judicialFacade.updateMateria(
          this.materia);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.judicialFacade.deleteMateria(
        this.materia);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.materia = new Materia();

    this.selectClasificacion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.materia.setIdMateria(identityGenerator.getNextSequenceNumber("sigefirrhh.base.judicial.Materia"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.materia = new Materia();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}