package sigefirrhh.base.judicial;

import java.io.Serializable;

public class MateriaPK
  implements Serializable
{
  public long idMateria;

  public MateriaPK()
  {
  }

  public MateriaPK(long idMateria)
  {
    this.idMateria = idMateria;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((MateriaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(MateriaPK thatPK)
  {
    return 
      this.idMateria == thatPK.idMateria;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idMateria)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idMateria);
  }
}