package sigefirrhh.base.judicial;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class OperacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(OperacionForm.class.getName());
  private Operacion operacion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private JudicialFacade judicialFacade = new JudicialFacade();
  private boolean showOperacionByCodOperacion;
  private boolean showOperacionByNombre;
  private String findCodOperacion;
  private String findNombre;
  private Object stateResultOperacionByCodOperacion = null;

  private Object stateResultOperacionByNombre = null;

  public String getFindCodOperacion()
  {
    return this.findCodOperacion;
  }
  public void setFindCodOperacion(String findCodOperacion) {
    this.findCodOperacion = findCodOperacion;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Operacion getOperacion() {
    if (this.operacion == null) {
      this.operacion = new Operacion();
    }
    return this.operacion;
  }

  public OperacionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findOperacionByCodOperacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.judicialFacade.findOperacionByCodOperacion(this.findCodOperacion);
      this.showOperacionByCodOperacion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showOperacionByCodOperacion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodOperacion = null;
    this.findNombre = null;

    return null;
  }

  public String findOperacionByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.judicialFacade.findOperacionByNombre(this.findNombre);
      this.showOperacionByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showOperacionByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodOperacion = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowOperacionByCodOperacion() {
    return this.showOperacionByCodOperacion;
  }
  public boolean isShowOperacionByNombre() {
    return this.showOperacionByNombre;
  }

  public String selectOperacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idOperacion = 
      Long.parseLong((String)requestParameterMap.get("idOperacion"));
    try
    {
      this.operacion = 
        this.judicialFacade.findOperacionById(
        idOperacion);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.operacion = null;
    this.showOperacionByCodOperacion = false;
    this.showOperacionByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.operacion.getTiempoSitp() != null) && 
      (this.operacion.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.judicialFacade.addOperacion(
          this.operacion);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.judicialFacade.updateOperacion(
          this.operacion);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.judicialFacade.deleteOperacion(
        this.operacion);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.operacion = new Operacion();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.operacion.setIdOperacion(identityGenerator.getNextSequenceNumber("sigefirrhh.base.judicial.Operacion"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.operacion = new Operacion();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}