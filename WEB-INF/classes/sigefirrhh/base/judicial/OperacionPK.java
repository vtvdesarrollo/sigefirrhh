package sigefirrhh.base.judicial;

import java.io.Serializable;

public class OperacionPK
  implements Serializable
{
  public long idOperacion;

  public OperacionPK()
  {
  }

  public OperacionPK(long idOperacion)
  {
    this.idOperacion = idOperacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((OperacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(OperacionPK thatPK)
  {
    return 
      this.idOperacion == thatPK.idOperacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idOperacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idOperacion);
  }
}