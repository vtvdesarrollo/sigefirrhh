package sigefirrhh.base.judicial;

import java.io.Serializable;

public class DependenciaJudicialPK
  implements Serializable
{
  public long idDependenciaJudicial;

  public DependenciaJudicialPK()
  {
  }

  public DependenciaJudicialPK(long idDependenciaJudicial)
  {
    this.idDependenciaJudicial = idDependenciaJudicial;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DependenciaJudicialPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DependenciaJudicialPK thatPK)
  {
    return 
      this.idDependenciaJudicial == thatPK.idDependenciaJudicial;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDependenciaJudicial)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDependenciaJudicial);
  }
}