package sigefirrhh.base.judicial;

import java.io.Serializable;

public class InstanciaPK
  implements Serializable
{
  public long idInstancia;

  public InstanciaPK()
  {
  }

  public InstanciaPK(long idInstancia)
  {
    this.idInstancia = idInstancia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((InstanciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(InstanciaPK thatPK)
  {
    return 
      this.idInstancia == thatPK.idInstancia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idInstancia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idInstancia);
  }
}