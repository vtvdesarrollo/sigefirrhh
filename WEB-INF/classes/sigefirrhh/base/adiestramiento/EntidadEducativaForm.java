package sigefirrhh.base.adiestramiento;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;

public class EntidadEducativaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(EntidadEducativaForm.class.getName());
  private EntidadEducativa entidadEducativa;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private AdiestramientoFacade adiestramientoFacade = new AdiestramientoFacade();
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private boolean showEntidadEducativaByCodEntidadEducativa;
  private boolean showEntidadEducativaByNombre;
  private boolean showEntidadEducativaByTipoEntidad;
  private String findCodEntidadEducativa;
  private String findNombre;
  private String findSelectTipoEntidad;
  private Collection findColTipoEntidad;
  private Collection colTipoEntidad;
  private Collection colPaisForEstado;
  private Collection colEstado;
  private String selectTipoEntidad;
  private String selectPaisForEstado;
  private String selectEstado;
  private Object stateResultEntidadEducativaByCodEntidadEducativa = null;

  private Object stateResultEntidadEducativaByNombre = null;

  private Object stateResultEntidadEducativaByTipoEntidad = null;

  public String getFindCodEntidadEducativa()
  {
    return this.findCodEntidadEducativa;
  }
  public void setFindCodEntidadEducativa(String findCodEntidadEducativa) {
    this.findCodEntidadEducativa = findCodEntidadEducativa;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }
  public String getFindSelectTipoEntidad() {
    return this.findSelectTipoEntidad;
  }
  public void setFindSelectTipoEntidad(String valTipoEntidad) {
    this.findSelectTipoEntidad = valTipoEntidad;
  }

  public Collection getFindColTipoEntidad() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoEntidad.iterator();
    TipoEntidad tipoEntidad = null;
    while (iterator.hasNext()) {
      tipoEntidad = (TipoEntidad)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoEntidad.getIdTipoEntidad()), 
        tipoEntidad.toString()));
    }
    return col;
  }

  public String getSelectTipoEntidad()
  {
    return this.selectTipoEntidad;
  }
  public void setSelectTipoEntidad(String valTipoEntidad) {
    Iterator iterator = this.colTipoEntidad.iterator();
    TipoEntidad tipoEntidad = null;
    this.entidadEducativa.setTipoEntidad(null);
    while (iterator.hasNext()) {
      tipoEntidad = (TipoEntidad)iterator.next();
      if (String.valueOf(tipoEntidad.getIdTipoEntidad()).equals(
        valTipoEntidad)) {
        this.entidadEducativa.setTipoEntidad(
          tipoEntidad);
        break;
      }
    }
    this.selectTipoEntidad = valTipoEntidad;
  }
  public String getSelectPaisForEstado() {
    return this.selectPaisForEstado;
  }
  public void setSelectPaisForEstado(String valPaisForEstado) {
    this.selectPaisForEstado = valPaisForEstado;
  }
  public void changePaisForEstado(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colEstado = null;
      if (idPais > 0L) {
        this.colEstado = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
      } else {
        this.selectEstado = null;
        this.entidadEducativa.setEstado(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectEstado = null;
      this.entidadEducativa.setEstado(
        null);
    }
  }

  public boolean isShowPaisForEstado() { return this.colPaisForEstado != null; }

  public String getSelectEstado() {
    return this.selectEstado;
  }
  public void setSelectEstado(String valEstado) {
    Iterator iterator = this.colEstado.iterator();
    Estado estado = null;
    this.entidadEducativa.setEstado(null);
    while (iterator.hasNext()) {
      estado = (Estado)iterator.next();
      if (String.valueOf(estado.getIdEstado()).equals(
        valEstado)) {
        this.entidadEducativa.setEstado(
          estado);
        break;
      }
    }
    this.selectEstado = valEstado;
  }
  public boolean isShowEstado() {
    return this.colEstado != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public EntidadEducativa getEntidadEducativa() {
    if (this.entidadEducativa == null) {
      this.entidadEducativa = new EntidadEducativa();
    }
    return this.entidadEducativa;
  }

  public EntidadEducativaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListSector()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = EntidadEducativa.LISTA_SECTOR.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColTipoEntidad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoEntidad.iterator();
    TipoEntidad tipoEntidad = null;
    while (iterator.hasNext()) {
      tipoEntidad = (TipoEntidad)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoEntidad.getIdTipoEntidad()), 
        tipoEntidad.toString()));
    }
    return col;
  }

  public Collection getColPaisForEstado() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForEstado.iterator();
    Pais paisForEstado = null;
    while (iterator.hasNext()) {
      paisForEstado = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForEstado.getIdPais()), 
        paisForEstado.toString()));
    }
    return col;
  }

  public Collection getColEstado()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstado.iterator();
    Estado estado = null;
    while (iterator.hasNext()) {
      estado = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estado.getIdEstado()), 
        estado.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoEntidad = 
        this.adiestramientoFacade.findAllTipoEntidad();

      this.colTipoEntidad = 
        this.adiestramientoFacade.findAllTipoEntidad();
      this.colPaisForEstado = 
        this.ubicacionFacade.findAllPais();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findEntidadEducativaByCodEntidadEducativa()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findEntidadEducativaByCodEntidadEducativa(this.findCodEntidadEducativa);
      this.showEntidadEducativaByCodEntidadEducativa = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEntidadEducativaByCodEntidadEducativa)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodEntidadEducativa = null;
    this.findNombre = null;
    this.findSelectTipoEntidad = null;

    return null;
  }

  public String findEntidadEducativaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findEntidadEducativaByNombre(this.findNombre);
      this.showEntidadEducativaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEntidadEducativaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodEntidadEducativa = null;
    this.findNombre = null;
    this.findSelectTipoEntidad = null;

    return null;
  }

  public String findEntidadEducativaByTipoEntidad()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findEntidadEducativaByTipoEntidad(Long.valueOf(this.findSelectTipoEntidad).longValue());
      this.showEntidadEducativaByTipoEntidad = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEntidadEducativaByTipoEntidad)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodEntidadEducativa = null;
    this.findNombre = null;
    this.findSelectTipoEntidad = null;

    return null;
  }

  public boolean isShowEntidadEducativaByCodEntidadEducativa() {
    return this.showEntidadEducativaByCodEntidadEducativa;
  }
  public boolean isShowEntidadEducativaByNombre() {
    return this.showEntidadEducativaByNombre;
  }
  public boolean isShowEntidadEducativaByTipoEntidad() {
    return this.showEntidadEducativaByTipoEntidad;
  }

  public String selectEntidadEducativa()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoEntidad = null;
    this.selectEstado = null;
    this.selectPaisForEstado = null;

    long idEntidadEducativa = 
      Long.parseLong((String)requestParameterMap.get("idEntidadEducativa"));
    try
    {
      this.entidadEducativa = 
        this.adiestramientoFacade.findEntidadEducativaById(
        idEntidadEducativa);
      if (this.entidadEducativa.getTipoEntidad() != null) {
        this.selectTipoEntidad = 
          String.valueOf(this.entidadEducativa.getTipoEntidad().getIdTipoEntidad());
      }
      if (this.entidadEducativa.getEstado() != null) {
        this.selectEstado = 
          String.valueOf(this.entidadEducativa.getEstado().getIdEstado());
      }

      Estado estado = null;
      Pais paisForEstado = null;

      if (this.entidadEducativa.getEstado() != null) {
        long idEstado = 
          this.entidadEducativa.getEstado().getIdEstado();
        this.selectEstado = String.valueOf(idEstado);
        estado = this.ubicacionFacade.findEstadoById(
          idEstado);
        this.colEstado = this.ubicacionFacade.findEstadoByPais(
          estado.getPais().getIdPais());

        long idPaisForEstado = 
          this.entidadEducativa.getEstado().getPais().getIdPais();
        this.selectPaisForEstado = String.valueOf(idPaisForEstado);
        paisForEstado = 
          this.ubicacionFacade.findPaisById(
          idPaisForEstado);
        this.colPaisForEstado = 
          this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.entidadEducativa = null;
    this.showEntidadEducativaByCodEntidadEducativa = false;
    this.showEntidadEducativaByNombre = false;
    this.showEntidadEducativaByTipoEntidad = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.adiestramientoFacade.addEntidadEducativa(
          this.entidadEducativa);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.adiestramientoFacade.updateEntidadEducativa(
          this.entidadEducativa);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
      log.error("Excepcion controlada:", e);
    }

    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.adiestramientoFacade.deleteEntidadEducativa(
        this.entidadEducativa);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.entidadEducativa = new EntidadEducativa();

    this.selectTipoEntidad = null;

    this.selectEstado = null;

    this.selectPaisForEstado = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.entidadEducativa.setIdEntidadEducativa(identityGenerator.getNextSequenceNumber("sigefirrhh.base.adiestramiento.EntidadEducativa"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.entidadEducativa = new EntidadEducativa();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}