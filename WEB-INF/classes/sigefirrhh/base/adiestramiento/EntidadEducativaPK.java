package sigefirrhh.base.adiestramiento;

import java.io.Serializable;

public class EntidadEducativaPK
  implements Serializable
{
  public long idEntidadEducativa;

  public EntidadEducativaPK()
  {
  }

  public EntidadEducativaPK(long idEntidadEducativa)
  {
    this.idEntidadEducativa = idEntidadEducativa;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EntidadEducativaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EntidadEducativaPK thatPK)
  {
    return 
      this.idEntidadEducativa == thatPK.idEntidadEducativa;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEntidadEducativa)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEntidadEducativa);
  }
}