package sigefirrhh.base.adiestramiento;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class AreaConocimientoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAreaConocimiento(AreaConocimiento areaConocimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    AreaConocimiento areaConocimientoNew = 
      (AreaConocimiento)BeanUtils.cloneBean(
      areaConocimiento);

    pm.makePersistent(areaConocimientoNew);
  }

  public void updateAreaConocimiento(AreaConocimiento areaConocimiento) throws Exception
  {
    AreaConocimiento areaConocimientoModify = 
      findAreaConocimientoById(areaConocimiento.getIdAreaConocimiento());

    BeanUtils.copyProperties(areaConocimientoModify, areaConocimiento);
  }

  public void deleteAreaConocimiento(AreaConocimiento areaConocimiento) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    AreaConocimiento areaConocimientoDelete = 
      findAreaConocimientoById(areaConocimiento.getIdAreaConocimiento());
    pm.deletePersistent(areaConocimientoDelete);
  }

  public AreaConocimiento findAreaConocimientoById(long idAreaConocimiento) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAreaConocimiento == pIdAreaConocimiento";
    Query query = pm.newQuery(AreaConocimiento.class, filter);

    query.declareParameters("long pIdAreaConocimiento");

    parameters.put("pIdAreaConocimiento", new Long(idAreaConocimiento));

    Collection colAreaConocimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAreaConocimiento.iterator();
    return (AreaConocimiento)iterator.next();
  }

  public Collection findAreaConocimientoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent areaConocimientoExtent = pm.getExtent(
      AreaConocimiento.class, true);
    Query query = pm.newQuery(areaConocimientoExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodAreaConocimiento(String codAreaConocimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codAreaConocimiento == pCodAreaConocimiento";

    Query query = pm.newQuery(AreaConocimiento.class, filter);

    query.declareParameters("java.lang.String pCodAreaConocimiento");
    HashMap parameters = new HashMap();

    parameters.put("pCodAreaConocimiento", new String(codAreaConocimiento));

    query.setOrdering("descripcion ascending");

    Collection colAreaConocimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAreaConocimiento);

    return colAreaConocimiento;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(AreaConocimiento.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colAreaConocimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAreaConocimiento);

    return colAreaConocimiento;
  }
}