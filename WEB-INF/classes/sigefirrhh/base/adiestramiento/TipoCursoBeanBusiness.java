package sigefirrhh.base.adiestramiento;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoCursoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoCurso(TipoCurso tipoCurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoCurso tipoCursoNew = 
      (TipoCurso)BeanUtils.cloneBean(
      tipoCurso);

    pm.makePersistent(tipoCursoNew);
  }

  public void updateTipoCurso(TipoCurso tipoCurso) throws Exception
  {
    TipoCurso tipoCursoModify = 
      findTipoCursoById(tipoCurso.getIdTipoCurso());

    BeanUtils.copyProperties(tipoCursoModify, tipoCurso);
  }

  public void deleteTipoCurso(TipoCurso tipoCurso) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoCurso tipoCursoDelete = 
      findTipoCursoById(tipoCurso.getIdTipoCurso());
    pm.deletePersistent(tipoCursoDelete);
  }

  public TipoCurso findTipoCursoById(long idTipoCurso) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoCurso == pIdTipoCurso";
    Query query = pm.newQuery(TipoCurso.class, filter);

    query.declareParameters("long pIdTipoCurso");

    parameters.put("pIdTipoCurso", new Long(idTipoCurso));

    Collection colTipoCurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoCurso.iterator();
    return (TipoCurso)iterator.next();
  }

  public Collection findTipoCursoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoCursoExtent = pm.getExtent(
      TipoCurso.class, true);
    Query query = pm.newQuery(tipoCursoExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoCurso(String codTipoCurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoCurso == pCodTipoCurso";

    Query query = pm.newQuery(TipoCurso.class, filter);

    query.declareParameters("java.lang.String pCodTipoCurso");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoCurso", new String(codTipoCurso));

    query.setOrdering("descripcion ascending");

    Collection colTipoCurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoCurso);

    return colTipoCurso;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(TipoCurso.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTipoCurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoCurso);

    return colTipoCurso;
  }
}