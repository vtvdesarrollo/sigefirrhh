package sigefirrhh.base.adiestramiento;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class TipoCursoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TipoCursoForm.class.getName());
  private TipoCurso tipoCurso;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private AdiestramientoFacade adiestramientoFacade = new AdiestramientoFacade();
  private boolean showTipoCursoByCodTipoCurso;
  private boolean showTipoCursoByDescripcion;
  private String findCodTipoCurso;
  private String findDescripcion;
  private Object stateResultTipoCursoByCodTipoCurso = null;

  private Object stateResultTipoCursoByDescripcion = null;

  public String getFindCodTipoCurso()
  {
    return this.findCodTipoCurso;
  }
  public void setFindCodTipoCurso(String findCodTipoCurso) {
    this.findCodTipoCurso = findCodTipoCurso;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public TipoCurso getTipoCurso() {
    if (this.tipoCurso == null) {
      this.tipoCurso = new TipoCurso();
    }
    return this.tipoCurso;
  }

  public TipoCursoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findTipoCursoByCodTipoCurso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findTipoCursoByCodTipoCurso(this.findCodTipoCurso);
      this.showTipoCursoByCodTipoCurso = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoCursoByCodTipoCurso)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoCurso = null;
    this.findDescripcion = null;

    return null;
  }

  public String findTipoCursoByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findTipoCursoByDescripcion(this.findDescripcion);
      this.showTipoCursoByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showTipoCursoByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodTipoCurso = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowTipoCursoByCodTipoCurso() {
    return this.showTipoCursoByCodTipoCurso;
  }
  public boolean isShowTipoCursoByDescripcion() {
    return this.showTipoCursoByDescripcion;
  }

  public String selectTipoCurso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTipoCurso = 
      Long.parseLong((String)requestParameterMap.get("idTipoCurso"));
    try
    {
      this.tipoCurso = 
        this.adiestramientoFacade.findTipoCursoById(
        idTipoCurso);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.tipoCurso = null;
    this.showTipoCursoByCodTipoCurso = false;
    this.showTipoCursoByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.adiestramientoFacade.addTipoCurso(
          this.tipoCurso);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.adiestramientoFacade.updateTipoCurso(
          this.tipoCurso);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.adiestramientoFacade.deleteTipoCurso(
        this.tipoCurso);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.tipoCurso = new TipoCurso();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.tipoCurso.setIdTipoCurso(identityGenerator.getNextSequenceNumber("sigefirrhh.base.adiestramiento.TipoCurso"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.tipoCurso = new TipoCurso();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}