package sigefirrhh.base.adiestramiento;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoCurso
  implements Serializable, PersistenceCapable
{
  private long idTipoCurso;
  private String codTipoCurso;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoCurso", "descripcion", "idTipoCurso" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + "  -  " + jdoGetcodTipoCurso(this);
  }

  public String getCodTipoCurso()
  {
    return jdoGetcodTipoCurso(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdTipoCurso()
  {
    return jdoGetidTipoCurso(this);
  }

  public void setCodTipoCurso(String string)
  {
    jdoSetcodTipoCurso(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdTipoCurso(long l)
  {
    jdoSetidTipoCurso(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.adiestramiento.TipoCurso"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoCurso());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoCurso localTipoCurso = new TipoCurso();
    localTipoCurso.jdoFlags = 1;
    localTipoCurso.jdoStateManager = paramStateManager;
    return localTipoCurso;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoCurso localTipoCurso = new TipoCurso();
    localTipoCurso.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoCurso.jdoFlags = 1;
    localTipoCurso.jdoStateManager = paramStateManager;
    return localTipoCurso;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoCurso);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoCurso);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoCurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoCurso = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoCurso paramTipoCurso, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoCurso == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoCurso = paramTipoCurso.codTipoCurso;
      return;
    case 1:
      if (paramTipoCurso == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTipoCurso.descripcion;
      return;
    case 2:
      if (paramTipoCurso == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoCurso = paramTipoCurso.idTipoCurso;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoCurso))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoCurso localTipoCurso = (TipoCurso)paramObject;
    if (localTipoCurso.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoCurso, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoCursoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoCursoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoCursoPK))
      throw new IllegalArgumentException("arg1");
    TipoCursoPK localTipoCursoPK = (TipoCursoPK)paramObject;
    localTipoCursoPK.idTipoCurso = this.idTipoCurso;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoCursoPK))
      throw new IllegalArgumentException("arg1");
    TipoCursoPK localTipoCursoPK = (TipoCursoPK)paramObject;
    this.idTipoCurso = localTipoCursoPK.idTipoCurso;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoCursoPK))
      throw new IllegalArgumentException("arg2");
    TipoCursoPK localTipoCursoPK = (TipoCursoPK)paramObject;
    localTipoCursoPK.idTipoCurso = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoCursoPK))
      throw new IllegalArgumentException("arg2");
    TipoCursoPK localTipoCursoPK = (TipoCursoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localTipoCursoPK.idTipoCurso);
  }

  private static final String jdoGetcodTipoCurso(TipoCurso paramTipoCurso)
  {
    if (paramTipoCurso.jdoFlags <= 0)
      return paramTipoCurso.codTipoCurso;
    StateManager localStateManager = paramTipoCurso.jdoStateManager;
    if (localStateManager == null)
      return paramTipoCurso.codTipoCurso;
    if (localStateManager.isLoaded(paramTipoCurso, jdoInheritedFieldCount + 0))
      return paramTipoCurso.codTipoCurso;
    return localStateManager.getStringField(paramTipoCurso, jdoInheritedFieldCount + 0, paramTipoCurso.codTipoCurso);
  }

  private static final void jdoSetcodTipoCurso(TipoCurso paramTipoCurso, String paramString)
  {
    if (paramTipoCurso.jdoFlags == 0)
    {
      paramTipoCurso.codTipoCurso = paramString;
      return;
    }
    StateManager localStateManager = paramTipoCurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoCurso.codTipoCurso = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoCurso, jdoInheritedFieldCount + 0, paramTipoCurso.codTipoCurso, paramString);
  }

  private static final String jdoGetdescripcion(TipoCurso paramTipoCurso)
  {
    if (paramTipoCurso.jdoFlags <= 0)
      return paramTipoCurso.descripcion;
    StateManager localStateManager = paramTipoCurso.jdoStateManager;
    if (localStateManager == null)
      return paramTipoCurso.descripcion;
    if (localStateManager.isLoaded(paramTipoCurso, jdoInheritedFieldCount + 1))
      return paramTipoCurso.descripcion;
    return localStateManager.getStringField(paramTipoCurso, jdoInheritedFieldCount + 1, paramTipoCurso.descripcion);
  }

  private static final void jdoSetdescripcion(TipoCurso paramTipoCurso, String paramString)
  {
    if (paramTipoCurso.jdoFlags == 0)
    {
      paramTipoCurso.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoCurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoCurso.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoCurso, jdoInheritedFieldCount + 1, paramTipoCurso.descripcion, paramString);
  }

  private static final long jdoGetidTipoCurso(TipoCurso paramTipoCurso)
  {
    return paramTipoCurso.idTipoCurso;
  }

  private static final void jdoSetidTipoCurso(TipoCurso paramTipoCurso, long paramLong)
  {
    StateManager localStateManager = paramTipoCurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoCurso.idTipoCurso = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoCurso, jdoInheritedFieldCount + 2, paramTipoCurso.idTipoCurso, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}