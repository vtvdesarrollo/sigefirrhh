package sigefirrhh.base.adiestramiento;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CursoEntidadBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCursoEntidad(CursoEntidad cursoEntidad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CursoEntidad cursoEntidadNew = 
      (CursoEntidad)BeanUtils.cloneBean(
      cursoEntidad);

    EntidadEducativaBeanBusiness entidadEducativaBeanBusiness = new EntidadEducativaBeanBusiness();

    if (cursoEntidadNew.getEntidadEducativa() != null) {
      cursoEntidadNew.setEntidadEducativa(
        entidadEducativaBeanBusiness.findEntidadEducativaById(
        cursoEntidadNew.getEntidadEducativa().getIdEntidadEducativa()));
    }

    CursoBeanBusiness cursoBeanBusiness = new CursoBeanBusiness();

    if (cursoEntidadNew.getCurso() != null) {
      cursoEntidadNew.setCurso(
        cursoBeanBusiness.findCursoById(
        cursoEntidadNew.getCurso().getIdCurso()));
    }
    pm.makePersistent(cursoEntidadNew);
  }

  public void updateCursoEntidad(CursoEntidad cursoEntidad) throws Exception
  {
    CursoEntidad cursoEntidadModify = 
      findCursoEntidadById(cursoEntidad.getIdCursoEntidad());

    EntidadEducativaBeanBusiness entidadEducativaBeanBusiness = new EntidadEducativaBeanBusiness();

    if (cursoEntidad.getEntidadEducativa() != null) {
      cursoEntidad.setEntidadEducativa(
        entidadEducativaBeanBusiness.findEntidadEducativaById(
        cursoEntidad.getEntidadEducativa().getIdEntidadEducativa()));
    }

    CursoBeanBusiness cursoBeanBusiness = new CursoBeanBusiness();

    if (cursoEntidad.getCurso() != null) {
      cursoEntidad.setCurso(
        cursoBeanBusiness.findCursoById(
        cursoEntidad.getCurso().getIdCurso()));
    }

    BeanUtils.copyProperties(cursoEntidadModify, cursoEntidad);
  }

  public void deleteCursoEntidad(CursoEntidad cursoEntidad) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CursoEntidad cursoEntidadDelete = 
      findCursoEntidadById(cursoEntidad.getIdCursoEntidad());
    pm.deletePersistent(cursoEntidadDelete);
  }

  public CursoEntidad findCursoEntidadById(long idCursoEntidad) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCursoEntidad == pIdCursoEntidad";
    Query query = pm.newQuery(CursoEntidad.class, filter);

    query.declareParameters("long pIdCursoEntidad");

    parameters.put("pIdCursoEntidad", new Long(idCursoEntidad));

    Collection colCursoEntidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCursoEntidad.iterator();
    return (CursoEntidad)iterator.next();
  }

  public Collection findCursoEntidadAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent cursoEntidadExtent = pm.getExtent(
      CursoEntidad.class, true);
    Query query = pm.newQuery(cursoEntidadExtent);
    query.setOrdering("curso.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByEntidadEducativa(long idEntidadEducativa)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "entidadEducativa.idEntidadEducativa == pIdEntidadEducativa";

    Query query = pm.newQuery(CursoEntidad.class, filter);

    query.declareParameters("long pIdEntidadEducativa");
    HashMap parameters = new HashMap();

    parameters.put("pIdEntidadEducativa", new Long(idEntidadEducativa));

    query.setOrdering("curso.descripcion ascending");

    Collection colCursoEntidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCursoEntidad);

    return colCursoEntidad;
  }

  public Collection findByCurso(long idCurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "curso.idCurso == pIdCurso";

    Query query = pm.newQuery(CursoEntidad.class, filter);

    query.declareParameters("long pIdCurso");
    HashMap parameters = new HashMap();

    parameters.put("pIdCurso", new Long(idCurso));

    query.setOrdering("curso.descripcion ascending");

    Collection colCursoEntidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCursoEntidad);

    return colCursoEntidad;
  }
}