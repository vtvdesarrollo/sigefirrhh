package sigefirrhh.base.adiestramiento;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.NivelEducativo;
import sigefirrhh.base.personal.NivelEducativoBeanBusiness;

public class EntidadNivelBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEntidadNivel(EntidadNivel entidadNivel)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    EntidadNivel entidadNivelNew = 
      (EntidadNivel)BeanUtils.cloneBean(
      entidadNivel);

    EntidadEducativaBeanBusiness entidadEducativaBeanBusiness = new EntidadEducativaBeanBusiness();

    if (entidadNivelNew.getEntidadEducativa() != null) {
      entidadNivelNew.setEntidadEducativa(
        entidadEducativaBeanBusiness.findEntidadEducativaById(
        entidadNivelNew.getEntidadEducativa().getIdEntidadEducativa()));
    }

    NivelEducativoBeanBusiness nivelEducativoBeanBusiness = new NivelEducativoBeanBusiness();

    if (entidadNivelNew.getNivelEducativo() != null) {
      entidadNivelNew.setNivelEducativo(
        nivelEducativoBeanBusiness.findNivelEducativoById(
        entidadNivelNew.getNivelEducativo().getIdNivelEducativo()));
    }
    pm.makePersistent(entidadNivelNew);
  }

  public void updateEntidadNivel(EntidadNivel entidadNivel) throws Exception
  {
    EntidadNivel entidadNivelModify = 
      findEntidadNivelById(entidadNivel.getIdEntidadNivel());

    EntidadEducativaBeanBusiness entidadEducativaBeanBusiness = new EntidadEducativaBeanBusiness();

    if (entidadNivel.getEntidadEducativa() != null) {
      entidadNivel.setEntidadEducativa(
        entidadEducativaBeanBusiness.findEntidadEducativaById(
        entidadNivel.getEntidadEducativa().getIdEntidadEducativa()));
    }

    NivelEducativoBeanBusiness nivelEducativoBeanBusiness = new NivelEducativoBeanBusiness();

    if (entidadNivel.getNivelEducativo() != null) {
      entidadNivel.setNivelEducativo(
        nivelEducativoBeanBusiness.findNivelEducativoById(
        entidadNivel.getNivelEducativo().getIdNivelEducativo()));
    }

    BeanUtils.copyProperties(entidadNivelModify, entidadNivel);
  }

  public void deleteEntidadNivel(EntidadNivel entidadNivel) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    EntidadNivel entidadNivelDelete = 
      findEntidadNivelById(entidadNivel.getIdEntidadNivel());
    pm.deletePersistent(entidadNivelDelete);
  }

  public EntidadNivel findEntidadNivelById(long idEntidadNivel) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEntidadNivel == pIdEntidadNivel";
    Query query = pm.newQuery(EntidadNivel.class, filter);

    query.declareParameters("long pIdEntidadNivel");

    parameters.put("pIdEntidadNivel", new Long(idEntidadNivel));

    Collection colEntidadNivel = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEntidadNivel.iterator();
    return (EntidadNivel)iterator.next();
  }

  public Collection findEntidadNivelAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent entidadNivelExtent = pm.getExtent(
      EntidadNivel.class, true);
    Query query = pm.newQuery(entidadNivelExtent);
    query.setOrdering("entidadEducativa.nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByEntidadEducativa(long idEntidadEducativa)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "entidadEducativa.idEntidadEducativa == pIdEntidadEducativa";

    Query query = pm.newQuery(EntidadNivel.class, filter);

    query.declareParameters("long pIdEntidadEducativa");
    HashMap parameters = new HashMap();

    parameters.put("pIdEntidadEducativa", new Long(idEntidadEducativa));

    query.setOrdering("entidadEducativa.nombre ascending");

    Collection colEntidadNivel = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEntidadNivel);

    return colEntidadNivel;
  }

  public Collection findByNivelEducativo(long idNivelEducativo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nivelEducativo.idNivelEducativo == pIdNivelEducativo";

    Query query = pm.newQuery(EntidadNivel.class, filter);

    query.declareParameters("long pIdNivelEducativo");
    HashMap parameters = new HashMap();

    parameters.put("pIdNivelEducativo", new Long(idNivelEducativo));

    query.setOrdering("entidadEducativa.nombre ascending");

    Collection colEntidadNivel = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEntidadNivel);

    return colEntidadNivel;
  }
}