package sigefirrhh.base.adiestramiento;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoEntidad
  implements Serializable, PersistenceCapable
{
  private long idTipoEntidad;
  private String codTipoEntidad;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoEntidad", "descripcion", "idTipoEntidad" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + "  -  " + jdoGetcodTipoEntidad(this);
  }

  public String getCodTipoEntidad()
  {
    return jdoGetcodTipoEntidad(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdTipoEntidad()
  {
    return jdoGetidTipoEntidad(this);
  }

  public void setCodTipoEntidad(String string)
  {
    jdoSetcodTipoEntidad(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdTipoEntidad(long l)
  {
    jdoSetidTipoEntidad(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.adiestramiento.TipoEntidad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoEntidad());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoEntidad localTipoEntidad = new TipoEntidad();
    localTipoEntidad.jdoFlags = 1;
    localTipoEntidad.jdoStateManager = paramStateManager;
    return localTipoEntidad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoEntidad localTipoEntidad = new TipoEntidad();
    localTipoEntidad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoEntidad.jdoFlags = 1;
    localTipoEntidad.jdoStateManager = paramStateManager;
    return localTipoEntidad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoEntidad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoEntidad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoEntidad = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoEntidad paramTipoEntidad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoEntidad == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoEntidad = paramTipoEntidad.codTipoEntidad;
      return;
    case 1:
      if (paramTipoEntidad == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTipoEntidad.descripcion;
      return;
    case 2:
      if (paramTipoEntidad == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoEntidad = paramTipoEntidad.idTipoEntidad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoEntidad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoEntidad localTipoEntidad = (TipoEntidad)paramObject;
    if (localTipoEntidad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoEntidad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoEntidadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoEntidadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoEntidadPK))
      throw new IllegalArgumentException("arg1");
    TipoEntidadPK localTipoEntidadPK = (TipoEntidadPK)paramObject;
    localTipoEntidadPK.idTipoEntidad = this.idTipoEntidad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoEntidadPK))
      throw new IllegalArgumentException("arg1");
    TipoEntidadPK localTipoEntidadPK = (TipoEntidadPK)paramObject;
    this.idTipoEntidad = localTipoEntidadPK.idTipoEntidad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoEntidadPK))
      throw new IllegalArgumentException("arg2");
    TipoEntidadPK localTipoEntidadPK = (TipoEntidadPK)paramObject;
    localTipoEntidadPK.idTipoEntidad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoEntidadPK))
      throw new IllegalArgumentException("arg2");
    TipoEntidadPK localTipoEntidadPK = (TipoEntidadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localTipoEntidadPK.idTipoEntidad);
  }

  private static final String jdoGetcodTipoEntidad(TipoEntidad paramTipoEntidad)
  {
    if (paramTipoEntidad.jdoFlags <= 0)
      return paramTipoEntidad.codTipoEntidad;
    StateManager localStateManager = paramTipoEntidad.jdoStateManager;
    if (localStateManager == null)
      return paramTipoEntidad.codTipoEntidad;
    if (localStateManager.isLoaded(paramTipoEntidad, jdoInheritedFieldCount + 0))
      return paramTipoEntidad.codTipoEntidad;
    return localStateManager.getStringField(paramTipoEntidad, jdoInheritedFieldCount + 0, paramTipoEntidad.codTipoEntidad);
  }

  private static final void jdoSetcodTipoEntidad(TipoEntidad paramTipoEntidad, String paramString)
  {
    if (paramTipoEntidad.jdoFlags == 0)
    {
      paramTipoEntidad.codTipoEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramTipoEntidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoEntidad.codTipoEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoEntidad, jdoInheritedFieldCount + 0, paramTipoEntidad.codTipoEntidad, paramString);
  }

  private static final String jdoGetdescripcion(TipoEntidad paramTipoEntidad)
  {
    if (paramTipoEntidad.jdoFlags <= 0)
      return paramTipoEntidad.descripcion;
    StateManager localStateManager = paramTipoEntidad.jdoStateManager;
    if (localStateManager == null)
      return paramTipoEntidad.descripcion;
    if (localStateManager.isLoaded(paramTipoEntidad, jdoInheritedFieldCount + 1))
      return paramTipoEntidad.descripcion;
    return localStateManager.getStringField(paramTipoEntidad, jdoInheritedFieldCount + 1, paramTipoEntidad.descripcion);
  }

  private static final void jdoSetdescripcion(TipoEntidad paramTipoEntidad, String paramString)
  {
    if (paramTipoEntidad.jdoFlags == 0)
    {
      paramTipoEntidad.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoEntidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoEntidad.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoEntidad, jdoInheritedFieldCount + 1, paramTipoEntidad.descripcion, paramString);
  }

  private static final long jdoGetidTipoEntidad(TipoEntidad paramTipoEntidad)
  {
    return paramTipoEntidad.idTipoEntidad;
  }

  private static final void jdoSetidTipoEntidad(TipoEntidad paramTipoEntidad, long paramLong)
  {
    StateManager localStateManager = paramTipoEntidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoEntidad.idTipoEntidad = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoEntidad, jdoInheritedFieldCount + 2, paramTipoEntidad.idTipoEntidad, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}