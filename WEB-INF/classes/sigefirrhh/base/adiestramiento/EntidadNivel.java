package sigefirrhh.base.adiestramiento;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.NivelEducativo;

public class EntidadNivel
  implements Serializable, PersistenceCapable
{
  private long idEntidadNivel;
  private EntidadEducativa entidadEducativa;
  private NivelEducativo nivelEducativo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "entidadEducativa", "idEntidadNivel", "nivelEducativo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.adiestramiento.EntidadEducativa"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.personal.NivelEducativo") };
  private static final byte[] jdoFieldFlags = { 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetentidadEducativa(this).getNombre() + "  -  " + 
      jdoGetnivelEducativo(this).getDescripcion();
  }

  public EntidadEducativa getEntidadEducativa()
  {
    return jdoGetentidadEducativa(this);
  }

  public long getIdEntidadNivel() {
    return jdoGetidEntidadNivel(this);
  }

  public NivelEducativo getNivelEducativo() {
    return jdoGetnivelEducativo(this);
  }

  public void setEntidadEducativa(EntidadEducativa educativa) {
    jdoSetentidadEducativa(this, educativa);
  }

  public void setIdEntidadNivel(long l) {
    jdoSetidEntidadNivel(this, l);
  }

  public void setNivelEducativo(NivelEducativo educativo) {
    jdoSetnivelEducativo(this, educativo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.adiestramiento.EntidadNivel"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new EntidadNivel());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    EntidadNivel localEntidadNivel = new EntidadNivel();
    localEntidadNivel.jdoFlags = 1;
    localEntidadNivel.jdoStateManager = paramStateManager;
    return localEntidadNivel;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    EntidadNivel localEntidadNivel = new EntidadNivel();
    localEntidadNivel.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEntidadNivel.jdoFlags = 1;
    localEntidadNivel.jdoStateManager = paramStateManager;
    return localEntidadNivel;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.entidadEducativa);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEntidadNivel);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nivelEducativo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entidadEducativa = ((EntidadEducativa)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEntidadNivel = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = ((NivelEducativo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(EntidadNivel paramEntidadNivel, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEntidadNivel == null)
        throw new IllegalArgumentException("arg1");
      this.entidadEducativa = paramEntidadNivel.entidadEducativa;
      return;
    case 1:
      if (paramEntidadNivel == null)
        throw new IllegalArgumentException("arg1");
      this.idEntidadNivel = paramEntidadNivel.idEntidadNivel;
      return;
    case 2:
      if (paramEntidadNivel == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramEntidadNivel.nivelEducativo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof EntidadNivel))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    EntidadNivel localEntidadNivel = (EntidadNivel)paramObject;
    if (localEntidadNivel.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEntidadNivel, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EntidadNivelPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EntidadNivelPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EntidadNivelPK))
      throw new IllegalArgumentException("arg1");
    EntidadNivelPK localEntidadNivelPK = (EntidadNivelPK)paramObject;
    localEntidadNivelPK.idEntidadNivel = this.idEntidadNivel;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EntidadNivelPK))
      throw new IllegalArgumentException("arg1");
    EntidadNivelPK localEntidadNivelPK = (EntidadNivelPK)paramObject;
    this.idEntidadNivel = localEntidadNivelPK.idEntidadNivel;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EntidadNivelPK))
      throw new IllegalArgumentException("arg2");
    EntidadNivelPK localEntidadNivelPK = (EntidadNivelPK)paramObject;
    localEntidadNivelPK.idEntidadNivel = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EntidadNivelPK))
      throw new IllegalArgumentException("arg2");
    EntidadNivelPK localEntidadNivelPK = (EntidadNivelPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localEntidadNivelPK.idEntidadNivel);
  }

  private static final EntidadEducativa jdoGetentidadEducativa(EntidadNivel paramEntidadNivel)
  {
    StateManager localStateManager = paramEntidadNivel.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadNivel.entidadEducativa;
    if (localStateManager.isLoaded(paramEntidadNivel, jdoInheritedFieldCount + 0))
      return paramEntidadNivel.entidadEducativa;
    return (EntidadEducativa)localStateManager.getObjectField(paramEntidadNivel, jdoInheritedFieldCount + 0, paramEntidadNivel.entidadEducativa);
  }

  private static final void jdoSetentidadEducativa(EntidadNivel paramEntidadNivel, EntidadEducativa paramEntidadEducativa)
  {
    StateManager localStateManager = paramEntidadNivel.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadNivel.entidadEducativa = paramEntidadEducativa;
      return;
    }
    localStateManager.setObjectField(paramEntidadNivel, jdoInheritedFieldCount + 0, paramEntidadNivel.entidadEducativa, paramEntidadEducativa);
  }

  private static final long jdoGetidEntidadNivel(EntidadNivel paramEntidadNivel)
  {
    return paramEntidadNivel.idEntidadNivel;
  }

  private static final void jdoSetidEntidadNivel(EntidadNivel paramEntidadNivel, long paramLong)
  {
    StateManager localStateManager = paramEntidadNivel.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadNivel.idEntidadNivel = paramLong;
      return;
    }
    localStateManager.setLongField(paramEntidadNivel, jdoInheritedFieldCount + 1, paramEntidadNivel.idEntidadNivel, paramLong);
  }

  private static final NivelEducativo jdoGetnivelEducativo(EntidadNivel paramEntidadNivel)
  {
    StateManager localStateManager = paramEntidadNivel.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadNivel.nivelEducativo;
    if (localStateManager.isLoaded(paramEntidadNivel, jdoInheritedFieldCount + 2))
      return paramEntidadNivel.nivelEducativo;
    return (NivelEducativo)localStateManager.getObjectField(paramEntidadNivel, jdoInheritedFieldCount + 2, paramEntidadNivel.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(EntidadNivel paramEntidadNivel, NivelEducativo paramNivelEducativo)
  {
    StateManager localStateManager = paramEntidadNivel.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadNivel.nivelEducativo = paramNivelEducativo;
      return;
    }
    localStateManager.setObjectField(paramEntidadNivel, jdoInheritedFieldCount + 2, paramEntidadNivel.nivelEducativo, paramNivelEducativo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}