package sigefirrhh.base.adiestramiento;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class AdiestramientoBusiness extends AbstractBusiness
  implements Serializable
{
  private AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

  private CursoBeanBusiness cursoBeanBusiness = new CursoBeanBusiness();

  private CursoEntidadBeanBusiness cursoEntidadBeanBusiness = new CursoEntidadBeanBusiness();

  private EntidadEducativaBeanBusiness entidadEducativaBeanBusiness = new EntidadEducativaBeanBusiness();

  private EntidadNivelBeanBusiness entidadNivelBeanBusiness = new EntidadNivelBeanBusiness();

  private TipoCursoBeanBusiness tipoCursoBeanBusiness = new TipoCursoBeanBusiness();

  private TipoEntidadBeanBusiness tipoEntidadBeanBusiness = new TipoEntidadBeanBusiness();

  public void addAreaConocimiento(AreaConocimiento areaConocimiento)
    throws Exception
  {
    this.areaConocimientoBeanBusiness.addAreaConocimiento(areaConocimiento);
  }

  public void updateAreaConocimiento(AreaConocimiento areaConocimiento) throws Exception {
    this.areaConocimientoBeanBusiness.updateAreaConocimiento(areaConocimiento);
  }

  public void deleteAreaConocimiento(AreaConocimiento areaConocimiento) throws Exception {
    this.areaConocimientoBeanBusiness.deleteAreaConocimiento(areaConocimiento);
  }

  public AreaConocimiento findAreaConocimientoById(long areaConocimientoId) throws Exception {
    return this.areaConocimientoBeanBusiness.findAreaConocimientoById(areaConocimientoId);
  }

  public Collection findAllAreaConocimiento() throws Exception {
    return this.areaConocimientoBeanBusiness.findAreaConocimientoAll();
  }

  public Collection findAreaConocimientoByCodAreaConocimiento(String codAreaConocimiento)
    throws Exception
  {
    return this.areaConocimientoBeanBusiness.findByCodAreaConocimiento(codAreaConocimiento);
  }

  public Collection findAreaConocimientoByDescripcion(String descripcion)
    throws Exception
  {
    return this.areaConocimientoBeanBusiness.findByDescripcion(descripcion);
  }

  public void addCurso(Curso curso)
    throws Exception
  {
    this.cursoBeanBusiness.addCurso(curso);
  }

  public void updateCurso(Curso curso) throws Exception {
    this.cursoBeanBusiness.updateCurso(curso);
  }

  public void deleteCurso(Curso curso) throws Exception {
    this.cursoBeanBusiness.deleteCurso(curso);
  }

  public Curso findCursoById(long cursoId) throws Exception {
    return this.cursoBeanBusiness.findCursoById(cursoId);
  }

  public Collection findAllCurso() throws Exception {
    return this.cursoBeanBusiness.findCursoAll();
  }

  public Collection findCursoByCodCurso(String codCurso)
    throws Exception
  {
    return this.cursoBeanBusiness.findByCodCurso(codCurso);
  }

  public Collection findCursoByDescripcion(String descripcion)
    throws Exception
  {
    return this.cursoBeanBusiness.findByDescripcion(descripcion);
  }

  public Collection findCursoByTipoCurso(long idTipoCurso)
    throws Exception
  {
    return this.cursoBeanBusiness.findByTipoCurso(idTipoCurso);
  }

  public Collection findCursoByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    return this.cursoBeanBusiness.findByAreaConocimiento(idAreaConocimiento);
  }

  public void addCursoEntidad(CursoEntidad cursoEntidad)
    throws Exception
  {
    this.cursoEntidadBeanBusiness.addCursoEntidad(cursoEntidad);
  }

  public void updateCursoEntidad(CursoEntidad cursoEntidad) throws Exception {
    this.cursoEntidadBeanBusiness.updateCursoEntidad(cursoEntidad);
  }

  public void deleteCursoEntidad(CursoEntidad cursoEntidad) throws Exception {
    this.cursoEntidadBeanBusiness.deleteCursoEntidad(cursoEntidad);
  }

  public CursoEntidad findCursoEntidadById(long cursoEntidadId) throws Exception {
    return this.cursoEntidadBeanBusiness.findCursoEntidadById(cursoEntidadId);
  }

  public Collection findAllCursoEntidad() throws Exception {
    return this.cursoEntidadBeanBusiness.findCursoEntidadAll();
  }

  public Collection findCursoEntidadByEntidadEducativa(long idEntidadEducativa)
    throws Exception
  {
    return this.cursoEntidadBeanBusiness.findByEntidadEducativa(idEntidadEducativa);
  }

  public Collection findCursoEntidadByCurso(long idCurso)
    throws Exception
  {
    return this.cursoEntidadBeanBusiness.findByCurso(idCurso);
  }

  public void addEntidadEducativa(EntidadEducativa entidadEducativa)
    throws Exception
  {
    this.entidadEducativaBeanBusiness.addEntidadEducativa(entidadEducativa);
  }

  public void updateEntidadEducativa(EntidadEducativa entidadEducativa) throws Exception {
    this.entidadEducativaBeanBusiness.updateEntidadEducativa(entidadEducativa);
  }

  public void deleteEntidadEducativa(EntidadEducativa entidadEducativa) throws Exception {
    this.entidadEducativaBeanBusiness.deleteEntidadEducativa(entidadEducativa);
  }

  public EntidadEducativa findEntidadEducativaById(long entidadEducativaId) throws Exception {
    return this.entidadEducativaBeanBusiness.findEntidadEducativaById(entidadEducativaId);
  }

  public Collection findAllEntidadEducativa() throws Exception {
    return this.entidadEducativaBeanBusiness.findEntidadEducativaAll();
  }

  public Collection findEntidadEducativaByCodEntidadEducativa(String codEntidadEducativa)
    throws Exception
  {
    return this.entidadEducativaBeanBusiness.findByCodEntidadEducativa(codEntidadEducativa);
  }

  public Collection findEntidadEducativaByNombre(String nombre)
    throws Exception
  {
    return this.entidadEducativaBeanBusiness.findByNombre(nombre);
  }

  public Collection findEntidadEducativaByTipoEntidad(long idTipoEntidad)
    throws Exception
  {
    return this.entidadEducativaBeanBusiness.findByTipoEntidad(idTipoEntidad);
  }

  public void addEntidadNivel(EntidadNivel entidadNivel)
    throws Exception
  {
    this.entidadNivelBeanBusiness.addEntidadNivel(entidadNivel);
  }

  public void updateEntidadNivel(EntidadNivel entidadNivel) throws Exception {
    this.entidadNivelBeanBusiness.updateEntidadNivel(entidadNivel);
  }

  public void deleteEntidadNivel(EntidadNivel entidadNivel) throws Exception {
    this.entidadNivelBeanBusiness.deleteEntidadNivel(entidadNivel);
  }

  public EntidadNivel findEntidadNivelById(long entidadNivelId) throws Exception {
    return this.entidadNivelBeanBusiness.findEntidadNivelById(entidadNivelId);
  }

  public Collection findAllEntidadNivel() throws Exception {
    return this.entidadNivelBeanBusiness.findEntidadNivelAll();
  }

  public Collection findEntidadNivelByEntidadEducativa(long idEntidadEducativa)
    throws Exception
  {
    return this.entidadNivelBeanBusiness.findByEntidadEducativa(idEntidadEducativa);
  }

  public Collection findEntidadNivelByNivelEducativo(long idNivelEducativo)
    throws Exception
  {
    return this.entidadNivelBeanBusiness.findByNivelEducativo(idNivelEducativo);
  }

  public void addTipoCurso(TipoCurso tipoCurso)
    throws Exception
  {
    this.tipoCursoBeanBusiness.addTipoCurso(tipoCurso);
  }

  public void updateTipoCurso(TipoCurso tipoCurso) throws Exception {
    this.tipoCursoBeanBusiness.updateTipoCurso(tipoCurso);
  }

  public void deleteTipoCurso(TipoCurso tipoCurso) throws Exception {
    this.tipoCursoBeanBusiness.deleteTipoCurso(tipoCurso);
  }

  public TipoCurso findTipoCursoById(long tipoCursoId) throws Exception {
    return this.tipoCursoBeanBusiness.findTipoCursoById(tipoCursoId);
  }

  public Collection findAllTipoCurso() throws Exception {
    return this.tipoCursoBeanBusiness.findTipoCursoAll();
  }

  public Collection findTipoCursoByCodTipoCurso(String codTipoCurso)
    throws Exception
  {
    return this.tipoCursoBeanBusiness.findByCodTipoCurso(codTipoCurso);
  }

  public Collection findTipoCursoByDescripcion(String descripcion)
    throws Exception
  {
    return this.tipoCursoBeanBusiness.findByDescripcion(descripcion);
  }

  public void addTipoEntidad(TipoEntidad tipoEntidad)
    throws Exception
  {
    this.tipoEntidadBeanBusiness.addTipoEntidad(tipoEntidad);
  }

  public void updateTipoEntidad(TipoEntidad tipoEntidad) throws Exception {
    this.tipoEntidadBeanBusiness.updateTipoEntidad(tipoEntidad);
  }

  public void deleteTipoEntidad(TipoEntidad tipoEntidad) throws Exception {
    this.tipoEntidadBeanBusiness.deleteTipoEntidad(tipoEntidad);
  }

  public TipoEntidad findTipoEntidadById(long tipoEntidadId) throws Exception {
    return this.tipoEntidadBeanBusiness.findTipoEntidadById(tipoEntidadId);
  }

  public Collection findAllTipoEntidad() throws Exception {
    return this.tipoEntidadBeanBusiness.findTipoEntidadAll();
  }

  public Collection findTipoEntidadByCodTipoEntidad(String codTipoEntidad)
    throws Exception
  {
    return this.tipoEntidadBeanBusiness.findByCodTipoEntidad(codTipoEntidad);
  }

  public Collection findTipoEntidadByDescripcion(String descripcion)
    throws Exception
  {
    return this.tipoEntidadBeanBusiness.findByDescripcion(descripcion);
  }
}