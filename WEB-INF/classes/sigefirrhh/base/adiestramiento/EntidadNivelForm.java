package sigefirrhh.base.adiestramiento;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.personal.NivelEducativo;
import sigefirrhh.base.personal.PersonalFacade;
import sigefirrhh.login.LoginSession;

public class EntidadNivelForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(EntidadNivelForm.class.getName());
  private EntidadNivel entidadNivel;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private AdiestramientoFacade adiestramientoFacade = new AdiestramientoFacade();
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showEntidadNivelByEntidadEducativa;
  private boolean showEntidadNivelByNivelEducativo;
  private String findSelectEntidadEducativa;
  private String findSelectNivelEducativo;
  private Collection findColEntidadEducativa;
  private Collection findColNivelEducativo;
  private Collection colEntidadEducativa;
  private Collection colNivelEducativo;
  private String selectEntidadEducativa;
  private String selectNivelEducativo;
  private Object stateResultEntidadNivelByEntidadEducativa = null;

  private Object stateResultEntidadNivelByNivelEducativo = null;

  public String getFindSelectEntidadEducativa()
  {
    return this.findSelectEntidadEducativa;
  }
  public void setFindSelectEntidadEducativa(String valEntidadEducativa) {
    this.findSelectEntidadEducativa = valEntidadEducativa;
  }

  public Collection getFindColEntidadEducativa() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColEntidadEducativa.iterator();
    EntidadEducativa entidadEducativa = null;
    while (iterator.hasNext()) {
      entidadEducativa = (EntidadEducativa)iterator.next();
      col.add(new SelectItem(
        String.valueOf(entidadEducativa.getIdEntidadEducativa()), 
        entidadEducativa.toString()));
    }
    return col;
  }
  public String getFindSelectNivelEducativo() {
    return this.findSelectNivelEducativo;
  }
  public void setFindSelectNivelEducativo(String valNivelEducativo) {
    this.findSelectNivelEducativo = valNivelEducativo;
  }

  public Collection getFindColNivelEducativo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColNivelEducativo.iterator();
    NivelEducativo nivelEducativo = null;
    while (iterator.hasNext()) {
      nivelEducativo = (NivelEducativo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nivelEducativo.getIdNivelEducativo()), 
        nivelEducativo.toString()));
    }
    return col;
  }

  public String getSelectEntidadEducativa()
  {
    return this.selectEntidadEducativa;
  }
  public void setSelectEntidadEducativa(String valEntidadEducativa) {
    Iterator iterator = this.colEntidadEducativa.iterator();
    EntidadEducativa entidadEducativa = null;
    this.entidadNivel.setEntidadEducativa(null);
    while (iterator.hasNext()) {
      entidadEducativa = (EntidadEducativa)iterator.next();
      if (String.valueOf(entidadEducativa.getIdEntidadEducativa()).equals(
        valEntidadEducativa)) {
        this.entidadNivel.setEntidadEducativa(
          entidadEducativa);
        break;
      }
    }
    this.selectEntidadEducativa = valEntidadEducativa;
  }
  public String getSelectNivelEducativo() {
    return this.selectNivelEducativo;
  }
  public void setSelectNivelEducativo(String valNivelEducativo) {
    Iterator iterator = this.colNivelEducativo.iterator();
    NivelEducativo nivelEducativo = null;
    this.entidadNivel.setNivelEducativo(null);
    while (iterator.hasNext()) {
      nivelEducativo = (NivelEducativo)iterator.next();
      if (String.valueOf(nivelEducativo.getIdNivelEducativo()).equals(
        valNivelEducativo)) {
        this.entidadNivel.setNivelEducativo(
          nivelEducativo);
        break;
      }
    }
    this.selectNivelEducativo = valNivelEducativo;
  }
  public Collection getResult() {
    return this.result;
  }

  public EntidadNivel getEntidadNivel() {
    if (this.entidadNivel == null) {
      this.entidadNivel = new EntidadNivel();
    }
    return this.entidadNivel;
  }

  public EntidadNivelForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColEntidadEducativa()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEntidadEducativa.iterator();
    EntidadEducativa entidadEducativa = null;
    while (iterator.hasNext()) {
      entidadEducativa = (EntidadEducativa)iterator.next();
      col.add(new SelectItem(
        String.valueOf(entidadEducativa.getIdEntidadEducativa()), 
        entidadEducativa.toString()));
    }
    return col;
  }

  public Collection getColNivelEducativo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colNivelEducativo.iterator();
    NivelEducativo nivelEducativo = null;
    while (iterator.hasNext()) {
      nivelEducativo = (NivelEducativo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nivelEducativo.getIdNivelEducativo()), 
        nivelEducativo.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColEntidadEducativa = 
        this.adiestramientoFacade.findAllEntidadEducativa();
      this.findColNivelEducativo = 
        this.personalFacade.findAllNivelEducativo();

      this.colEntidadEducativa = 
        this.adiestramientoFacade.findAllEntidadEducativa();
      this.colNivelEducativo = 
        this.personalFacade.findAllNivelEducativo();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findEntidadNivelByEntidadEducativa()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findEntidadNivelByEntidadEducativa(Long.valueOf(this.findSelectEntidadEducativa).longValue());
      this.showEntidadNivelByEntidadEducativa = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEntidadNivelByEntidadEducativa)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectEntidadEducativa = null;
    this.findSelectNivelEducativo = null;

    return null;
  }

  public String findEntidadNivelByNivelEducativo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findEntidadNivelByNivelEducativo(Long.valueOf(this.findSelectNivelEducativo).longValue());
      this.showEntidadNivelByNivelEducativo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEntidadNivelByNivelEducativo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectEntidadEducativa = null;
    this.findSelectNivelEducativo = null;

    return null;
  }

  public boolean isShowEntidadNivelByEntidadEducativa() {
    return this.showEntidadNivelByEntidadEducativa;
  }
  public boolean isShowEntidadNivelByNivelEducativo() {
    return this.showEntidadNivelByNivelEducativo;
  }

  public String selectEntidadNivel()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectEntidadEducativa = null;
    this.selectNivelEducativo = null;

    long idEntidadNivel = 
      Long.parseLong((String)requestParameterMap.get("idEntidadNivel"));
    try
    {
      this.entidadNivel = 
        this.adiestramientoFacade.findEntidadNivelById(
        idEntidadNivel);
      if (this.entidadNivel.getEntidadEducativa() != null) {
        this.selectEntidadEducativa = 
          String.valueOf(this.entidadNivel.getEntidadEducativa().getIdEntidadEducativa());
      }
      if (this.entidadNivel.getNivelEducativo() != null) {
        this.selectNivelEducativo = 
          String.valueOf(this.entidadNivel.getNivelEducativo().getIdNivelEducativo());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.entidadNivel = null;
    this.showEntidadNivelByEntidadEducativa = false;
    this.showEntidadNivelByNivelEducativo = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.adiestramientoFacade.addEntidadNivel(
          this.entidadNivel);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.adiestramientoFacade.updateEntidadNivel(
          this.entidadNivel);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
      log.error("Excepcion controlada:", e);
    }

    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.adiestramientoFacade.deleteEntidadNivel(
        this.entidadNivel);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.entidadNivel = new EntidadNivel();

    this.selectEntidadEducativa = null;

    this.selectNivelEducativo = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.entidadNivel.setIdEntidadNivel(identityGenerator.getNextSequenceNumber("sigefirrhh.base.adiestramiento.EntidadNivel"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.entidadNivel = new EntidadNivel();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}