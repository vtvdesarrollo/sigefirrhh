package sigefirrhh.base.adiestramiento;

import java.io.Serializable;

public class CursoEntidadPK
  implements Serializable
{
  public long idCursoEntidad;

  public CursoEntidadPK()
  {
  }

  public CursoEntidadPK(long idCursoEntidad)
  {
    this.idCursoEntidad = idCursoEntidad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CursoEntidadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CursoEntidadPK thatPK)
  {
    return 
      this.idCursoEntidad == thatPK.idCursoEntidad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCursoEntidad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCursoEntidad);
  }
}