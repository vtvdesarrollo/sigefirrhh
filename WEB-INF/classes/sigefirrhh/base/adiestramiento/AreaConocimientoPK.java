package sigefirrhh.base.adiestramiento;

import java.io.Serializable;

public class AreaConocimientoPK
  implements Serializable
{
  public long idAreaConocimiento;

  public AreaConocimientoPK()
  {
  }

  public AreaConocimientoPK(long idAreaConocimiento)
  {
    this.idAreaConocimiento = idAreaConocimiento;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AreaConocimientoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AreaConocimientoPK thatPK)
  {
    return 
      this.idAreaConocimiento == thatPK.idAreaConocimiento;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAreaConocimiento)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAreaConocimiento);
  }
}