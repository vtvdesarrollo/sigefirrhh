package sigefirrhh.base.adiestramiento;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Curso
  implements Serializable, PersistenceCapable
{
  private long idCurso;
  private String codCurso;
  private String descripcion;
  private TipoCurso tipoCurso;
  private AreaConocimiento areaConocimiento;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "areaConocimiento", "codCurso", "descripcion", "idCurso", "tipoCurso" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.adiestramiento.AreaConocimiento"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.adiestramiento.TipoCurso") };
  private static final byte[] jdoFieldFlags = { 26, 21, 21, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + "  -  " + jdoGetcodCurso(this);
  }

  public String getCodCurso()
  {
    return jdoGetcodCurso(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdCurso()
  {
    return jdoGetidCurso(this);
  }

  public TipoCurso getTipoCurso()
  {
    return jdoGettipoCurso(this);
  }

  public void setCodCurso(String string)
  {
    jdoSetcodCurso(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdCurso(long l)
  {
    jdoSetidCurso(this, l);
  }

  public void setTipoCurso(TipoCurso curso)
  {
    jdoSettipoCurso(this, curso);
  }

  public AreaConocimiento getAreaConocimiento()
  {
    return jdoGetareaConocimiento(this);
  }

  public void setAreaConocimiento(AreaConocimiento conocimiento)
  {
    jdoSetareaConocimiento(this, conocimiento);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.adiestramiento.Curso"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Curso());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Curso localCurso = new Curso();
    localCurso.jdoFlags = 1;
    localCurso.jdoStateManager = paramStateManager;
    return localCurso;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Curso localCurso = new Curso();
    localCurso.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCurso.jdoFlags = 1;
    localCurso.jdoStateManager = paramStateManager;
    return localCurso;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.areaConocimiento);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCurso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCurso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoCurso);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.areaConocimiento = ((AreaConocimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCurso = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCurso = ((TipoCurso)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Curso paramCurso, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCurso == null)
        throw new IllegalArgumentException("arg1");
      this.areaConocimiento = paramCurso.areaConocimiento;
      return;
    case 1:
      if (paramCurso == null)
        throw new IllegalArgumentException("arg1");
      this.codCurso = paramCurso.codCurso;
      return;
    case 2:
      if (paramCurso == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramCurso.descripcion;
      return;
    case 3:
      if (paramCurso == null)
        throw new IllegalArgumentException("arg1");
      this.idCurso = paramCurso.idCurso;
      return;
    case 4:
      if (paramCurso == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCurso = paramCurso.tipoCurso;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Curso))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Curso localCurso = (Curso)paramObject;
    if (localCurso.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCurso, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CursoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CursoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CursoPK))
      throw new IllegalArgumentException("arg1");
    CursoPK localCursoPK = (CursoPK)paramObject;
    localCursoPK.idCurso = this.idCurso;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CursoPK))
      throw new IllegalArgumentException("arg1");
    CursoPK localCursoPK = (CursoPK)paramObject;
    this.idCurso = localCursoPK.idCurso;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CursoPK))
      throw new IllegalArgumentException("arg2");
    CursoPK localCursoPK = (CursoPK)paramObject;
    localCursoPK.idCurso = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CursoPK))
      throw new IllegalArgumentException("arg2");
    CursoPK localCursoPK = (CursoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localCursoPK.idCurso);
  }

  private static final AreaConocimiento jdoGetareaConocimiento(Curso paramCurso)
  {
    StateManager localStateManager = paramCurso.jdoStateManager;
    if (localStateManager == null)
      return paramCurso.areaConocimiento;
    if (localStateManager.isLoaded(paramCurso, jdoInheritedFieldCount + 0))
      return paramCurso.areaConocimiento;
    return (AreaConocimiento)localStateManager.getObjectField(paramCurso, jdoInheritedFieldCount + 0, paramCurso.areaConocimiento);
  }

  private static final void jdoSetareaConocimiento(Curso paramCurso, AreaConocimiento paramAreaConocimiento)
  {
    StateManager localStateManager = paramCurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramCurso.areaConocimiento = paramAreaConocimiento;
      return;
    }
    localStateManager.setObjectField(paramCurso, jdoInheritedFieldCount + 0, paramCurso.areaConocimiento, paramAreaConocimiento);
  }

  private static final String jdoGetcodCurso(Curso paramCurso)
  {
    if (paramCurso.jdoFlags <= 0)
      return paramCurso.codCurso;
    StateManager localStateManager = paramCurso.jdoStateManager;
    if (localStateManager == null)
      return paramCurso.codCurso;
    if (localStateManager.isLoaded(paramCurso, jdoInheritedFieldCount + 1))
      return paramCurso.codCurso;
    return localStateManager.getStringField(paramCurso, jdoInheritedFieldCount + 1, paramCurso.codCurso);
  }

  private static final void jdoSetcodCurso(Curso paramCurso, String paramString)
  {
    if (paramCurso.jdoFlags == 0)
    {
      paramCurso.codCurso = paramString;
      return;
    }
    StateManager localStateManager = paramCurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramCurso.codCurso = paramString;
      return;
    }
    localStateManager.setStringField(paramCurso, jdoInheritedFieldCount + 1, paramCurso.codCurso, paramString);
  }

  private static final String jdoGetdescripcion(Curso paramCurso)
  {
    if (paramCurso.jdoFlags <= 0)
      return paramCurso.descripcion;
    StateManager localStateManager = paramCurso.jdoStateManager;
    if (localStateManager == null)
      return paramCurso.descripcion;
    if (localStateManager.isLoaded(paramCurso, jdoInheritedFieldCount + 2))
      return paramCurso.descripcion;
    return localStateManager.getStringField(paramCurso, jdoInheritedFieldCount + 2, paramCurso.descripcion);
  }

  private static final void jdoSetdescripcion(Curso paramCurso, String paramString)
  {
    if (paramCurso.jdoFlags == 0)
    {
      paramCurso.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramCurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramCurso.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramCurso, jdoInheritedFieldCount + 2, paramCurso.descripcion, paramString);
  }

  private static final long jdoGetidCurso(Curso paramCurso)
  {
    return paramCurso.idCurso;
  }

  private static final void jdoSetidCurso(Curso paramCurso, long paramLong)
  {
    StateManager localStateManager = paramCurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramCurso.idCurso = paramLong;
      return;
    }
    localStateManager.setLongField(paramCurso, jdoInheritedFieldCount + 3, paramCurso.idCurso, paramLong);
  }

  private static final TipoCurso jdoGettipoCurso(Curso paramCurso)
  {
    StateManager localStateManager = paramCurso.jdoStateManager;
    if (localStateManager == null)
      return paramCurso.tipoCurso;
    if (localStateManager.isLoaded(paramCurso, jdoInheritedFieldCount + 4))
      return paramCurso.tipoCurso;
    return (TipoCurso)localStateManager.getObjectField(paramCurso, jdoInheritedFieldCount + 4, paramCurso.tipoCurso);
  }

  private static final void jdoSettipoCurso(Curso paramCurso, TipoCurso paramTipoCurso)
  {
    StateManager localStateManager = paramCurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramCurso.tipoCurso = paramTipoCurso;
      return;
    }
    localStateManager.setObjectField(paramCurso, jdoInheritedFieldCount + 4, paramCurso.tipoCurso, paramTipoCurso);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}