package sigefirrhh.base.adiestramiento;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CursoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCurso(Curso curso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Curso cursoNew = 
      (Curso)BeanUtils.cloneBean(
      curso);

    TipoCursoBeanBusiness tipoCursoBeanBusiness = new TipoCursoBeanBusiness();

    if (cursoNew.getTipoCurso() != null) {
      cursoNew.setTipoCurso(
        tipoCursoBeanBusiness.findTipoCursoById(
        cursoNew.getTipoCurso().getIdTipoCurso()));
    }

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (cursoNew.getAreaConocimiento() != null) {
      cursoNew.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        cursoNew.getAreaConocimiento().getIdAreaConocimiento()));
    }
    pm.makePersistent(cursoNew);
  }

  public void updateCurso(Curso curso) throws Exception
  {
    Curso cursoModify = 
      findCursoById(curso.getIdCurso());

    TipoCursoBeanBusiness tipoCursoBeanBusiness = new TipoCursoBeanBusiness();

    if (curso.getTipoCurso() != null) {
      curso.setTipoCurso(
        tipoCursoBeanBusiness.findTipoCursoById(
        curso.getTipoCurso().getIdTipoCurso()));
    }

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (curso.getAreaConocimiento() != null) {
      curso.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        curso.getAreaConocimiento().getIdAreaConocimiento()));
    }

    BeanUtils.copyProperties(cursoModify, curso);
  }

  public void deleteCurso(Curso curso) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Curso cursoDelete = 
      findCursoById(curso.getIdCurso());
    pm.deletePersistent(cursoDelete);
  }

  public Curso findCursoById(long idCurso) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCurso == pIdCurso";
    Query query = pm.newQuery(Curso.class, filter);

    query.declareParameters("long pIdCurso");

    parameters.put("pIdCurso", new Long(idCurso));

    Collection colCurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCurso.iterator();
    return (Curso)iterator.next();
  }

  public Collection findCursoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent cursoExtent = pm.getExtent(
      Curso.class, true);
    Query query = pm.newQuery(cursoExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodCurso(String codCurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCurso == pCodCurso";

    Query query = pm.newQuery(Curso.class, filter);

    query.declareParameters("java.lang.String pCodCurso");
    HashMap parameters = new HashMap();

    parameters.put("pCodCurso", new String(codCurso));

    query.setOrdering("descripcion ascending");

    Collection colCurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCurso);

    return colCurso;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(Curso.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colCurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCurso);

    return colCurso;
  }

  public Collection findByTipoCurso(long idTipoCurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoCurso.idTipoCurso == pIdTipoCurso";

    Query query = pm.newQuery(Curso.class, filter);

    query.declareParameters("long pIdTipoCurso");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoCurso", new Long(idTipoCurso));

    query.setOrdering("descripcion ascending");

    Collection colCurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCurso);

    return colCurso;
  }

  public Collection findByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "areaConocimiento.idAreaConocimiento == pIdAreaConocimiento";

    Query query = pm.newQuery(Curso.class, filter);

    query.declareParameters("long pIdAreaConocimiento");
    HashMap parameters = new HashMap();

    parameters.put("pIdAreaConocimiento", new Long(idAreaConocimiento));

    query.setOrdering("descripcion ascending");

    Collection colCurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCurso);

    return colCurso;
  }
}