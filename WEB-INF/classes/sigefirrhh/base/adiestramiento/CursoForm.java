package sigefirrhh.base.adiestramiento;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class CursoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CursoForm.class.getName());
  private Curso curso;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private AdiestramientoFacade adiestramientoFacade = new AdiestramientoFacade();
  private boolean showCursoByCodCurso;
  private boolean showCursoByDescripcion;
  private boolean showCursoByTipoCurso;
  private boolean showCursoByAreaConocimiento;
  private String findCodCurso;
  private String findDescripcion;
  private String findSelectTipoCurso;
  private String findSelectAreaConocimiento;
  private Collection findColTipoCurso;
  private Collection findColAreaConocimiento;
  private Collection colTipoCurso;
  private Collection colAreaConocimiento;
  private String selectTipoCurso;
  private String selectAreaConocimiento;
  private Object stateResultCursoByCodCurso = null;

  private Object stateResultCursoByDescripcion = null;

  private Object stateResultCursoByTipoCurso = null;

  private Object stateResultCursoByAreaConocimiento = null;

  public String getFindCodCurso()
  {
    return this.findCodCurso;
  }
  public void setFindCodCurso(String findCodCurso) {
    this.findCodCurso = findCodCurso;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }
  public String getFindSelectTipoCurso() {
    return this.findSelectTipoCurso;
  }
  public void setFindSelectTipoCurso(String valTipoCurso) {
    this.findSelectTipoCurso = valTipoCurso;
  }

  public Collection getFindColTipoCurso() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoCurso.iterator();
    TipoCurso tipoCurso = null;
    while (iterator.hasNext()) {
      tipoCurso = (TipoCurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCurso.getIdTipoCurso()), 
        tipoCurso.toString()));
    }
    return col;
  }
  public String getFindSelectAreaConocimiento() {
    return this.findSelectAreaConocimiento;
  }
  public void setFindSelectAreaConocimiento(String valAreaConocimiento) {
    this.findSelectAreaConocimiento = valAreaConocimiento;
  }

  public Collection getFindColAreaConocimiento() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(areaConocimiento.getIdAreaConocimiento()), 
        areaConocimiento.toString()));
    }
    return col;
  }

  public String getSelectTipoCurso()
  {
    return this.selectTipoCurso;
  }
  public void setSelectTipoCurso(String valTipoCurso) {
    Iterator iterator = this.colTipoCurso.iterator();
    TipoCurso tipoCurso = null;
    this.curso.setTipoCurso(null);
    while (iterator.hasNext()) {
      tipoCurso = (TipoCurso)iterator.next();
      if (String.valueOf(tipoCurso.getIdTipoCurso()).equals(
        valTipoCurso)) {
        this.curso.setTipoCurso(
          tipoCurso);
        break;
      }
    }
    this.selectTipoCurso = valTipoCurso;
  }
  public String getSelectAreaConocimiento() {
    return this.selectAreaConocimiento;
  }
  public void setSelectAreaConocimiento(String valAreaConocimiento) {
    Iterator iterator = this.colAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    this.curso.setAreaConocimiento(null);
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      if (String.valueOf(areaConocimiento.getIdAreaConocimiento()).equals(
        valAreaConocimiento)) {
        this.curso.setAreaConocimiento(
          areaConocimiento);
        break;
      }
    }
    this.selectAreaConocimiento = valAreaConocimiento;
  }
  public Collection getResult() {
    return this.result;
  }

  public Curso getCurso() {
    if (this.curso == null) {
      this.curso = new Curso();
    }
    return this.curso;
  }

  public CursoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoCurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoCurso.iterator();
    TipoCurso tipoCurso = null;
    while (iterator.hasNext()) {
      tipoCurso = (TipoCurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCurso.getIdTipoCurso()), 
        tipoCurso.toString()));
    }
    return col;
  }

  public Collection getColAreaConocimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(areaConocimiento.getIdAreaConocimiento()), 
        areaConocimiento.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoCurso = 
        this.adiestramientoFacade.findAllTipoCurso();
      this.findColAreaConocimiento = 
        this.adiestramientoFacade.findAllAreaConocimiento();

      this.colTipoCurso = 
        this.adiestramientoFacade.findAllTipoCurso();
      this.colAreaConocimiento = 
        this.adiestramientoFacade.findAllAreaConocimiento();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findCursoByCodCurso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findCursoByCodCurso(this.findCodCurso);
      this.showCursoByCodCurso = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCursoByCodCurso)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodCurso = null;
    this.findDescripcion = null;
    this.findSelectTipoCurso = null;
    this.findSelectAreaConocimiento = null;

    return null;
  }

  public String findCursoByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findCursoByDescripcion(this.findDescripcion);
      this.showCursoByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCursoByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodCurso = null;
    this.findDescripcion = null;
    this.findSelectTipoCurso = null;
    this.findSelectAreaConocimiento = null;

    return null;
  }

  public String findCursoByTipoCurso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findCursoByTipoCurso(Long.valueOf(this.findSelectTipoCurso).longValue());
      this.showCursoByTipoCurso = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCursoByTipoCurso)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodCurso = null;
    this.findDescripcion = null;
    this.findSelectTipoCurso = null;
    this.findSelectAreaConocimiento = null;

    return null;
  }

  public String findCursoByAreaConocimiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findCursoByAreaConocimiento(Long.valueOf(this.findSelectAreaConocimiento).longValue());
      this.showCursoByAreaConocimiento = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCursoByAreaConocimiento)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodCurso = null;
    this.findDescripcion = null;
    this.findSelectTipoCurso = null;
    this.findSelectAreaConocimiento = null;

    return null;
  }

  public boolean isShowCursoByCodCurso() {
    return this.showCursoByCodCurso;
  }
  public boolean isShowCursoByDescripcion() {
    return this.showCursoByDescripcion;
  }
  public boolean isShowCursoByTipoCurso() {
    return this.showCursoByTipoCurso;
  }
  public boolean isShowCursoByAreaConocimiento() {
    return this.showCursoByAreaConocimiento;
  }

  public String selectCurso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoCurso = null;
    this.selectAreaConocimiento = null;

    long idCurso = 
      Long.parseLong((String)requestParameterMap.get("idCurso"));
    try
    {
      this.curso = 
        this.adiestramientoFacade.findCursoById(
        idCurso);
      if (this.curso.getTipoCurso() != null) {
        this.selectTipoCurso = 
          String.valueOf(this.curso.getTipoCurso().getIdTipoCurso());
      }
      if (this.curso.getAreaConocimiento() != null) {
        this.selectAreaConocimiento = 
          String.valueOf(this.curso.getAreaConocimiento().getIdAreaConocimiento());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.curso = null;
    this.showCursoByCodCurso = false;
    this.showCursoByDescripcion = false;
    this.showCursoByTipoCurso = false;
    this.showCursoByAreaConocimiento = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.adiestramientoFacade.addCurso(
          this.curso);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.adiestramientoFacade.updateCurso(
          this.curso);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
      log.error("Excepcion controlada:", e);
    }

    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.adiestramientoFacade.deleteCurso(
        this.curso);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.curso = new Curso();

    this.selectTipoCurso = null;

    this.selectAreaConocimiento = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.curso.setIdCurso(identityGenerator.getNextSequenceNumber("sigefirrhh.base.adiestramiento.Curso"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.curso = new Curso();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}