package sigefirrhh.base.adiestramiento;

import java.io.Serializable;

public class CursoPK
  implements Serializable
{
  public long idCurso;

  public CursoPK()
  {
  }

  public CursoPK(long idCurso)
  {
    this.idCurso = idCurso;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CursoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CursoPK thatPK)
  {
    return 
      this.idCurso == thatPK.idCurso;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCurso)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCurso);
  }
}