package sigefirrhh.base.adiestramiento;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class AreaConocimientoForm extends Form
  implements Serializable
{
  private AreaConocimiento areaConocimiento;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private AdiestramientoFacade adiestramientoFacade = new AdiestramientoFacade();
  private boolean showAreaConocimientoByCodAreaConocimiento;
  private boolean showAreaConocimientoByDescripcion;
  private String findCodAreaConocimiento;
  private String findDescripcion;
  static Logger log = Logger.getLogger(AreaConocimientoForm.class.getName());

  private Object stateResultAreaConocimientoByCodAreaConocimiento = null;

  private Object stateResultAreaConocimientoByDescripcion = null;

  public String getFindCodAreaConocimiento()
  {
    return this.findCodAreaConocimiento;
  }
  public void setFindCodAreaConocimiento(String findCodAreaConocimiento) {
    this.findCodAreaConocimiento = findCodAreaConocimiento;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public AreaConocimiento getAreaConocimiento() {
    if (this.areaConocimiento == null) {
      this.areaConocimiento = new AreaConocimiento();
    }
    return this.areaConocimiento;
  }

  public AreaConocimientoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findAreaConocimientoByCodAreaConocimiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findAreaConocimientoByCodAreaConocimiento(this.findCodAreaConocimiento);
      this.showAreaConocimientoByCodAreaConocimiento = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAreaConocimientoByCodAreaConocimiento)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodAreaConocimiento = null;
    this.findDescripcion = null;

    return null;
  }

  public String findAreaConocimientoByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findAreaConocimientoByDescripcion(this.findDescripcion);
      this.showAreaConocimientoByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAreaConocimientoByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodAreaConocimiento = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowAreaConocimientoByCodAreaConocimiento() {
    return this.showAreaConocimientoByCodAreaConocimiento;
  }
  public boolean isShowAreaConocimientoByDescripcion() {
    return this.showAreaConocimientoByDescripcion;
  }

  public String selectAreaConocimiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idAreaConocimiento = 
      Long.parseLong((String)requestParameterMap.get("idAreaConocimiento"));
    try
    {
      this.areaConocimiento = 
        this.adiestramientoFacade.findAreaConocimientoById(
        idAreaConocimiento);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.areaConocimiento = null;
    this.showAreaConocimientoByCodAreaConocimiento = false;
    this.showAreaConocimientoByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.adiestramientoFacade.addAreaConocimiento(
          this.areaConocimiento);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.adiestramientoFacade.updateAreaConocimiento(
          this.areaConocimiento);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
      log.error("Excepcion controlada:", e);
    }

    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.adiestramientoFacade.deleteAreaConocimiento(
        this.areaConocimiento);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.areaConocimiento = new AreaConocimiento();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.areaConocimiento.setIdAreaConocimiento(identityGenerator.getNextSequenceNumber("sigefirrhh.base.adiestramiento.AreaConocimiento"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.areaConocimiento = new AreaConocimiento();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}