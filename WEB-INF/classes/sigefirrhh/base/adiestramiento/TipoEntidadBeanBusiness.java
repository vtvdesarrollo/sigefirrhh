package sigefirrhh.base.adiestramiento;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoEntidadBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoEntidad(TipoEntidad tipoEntidad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoEntidad tipoEntidadNew = 
      (TipoEntidad)BeanUtils.cloneBean(
      tipoEntidad);

    pm.makePersistent(tipoEntidadNew);
  }

  public void updateTipoEntidad(TipoEntidad tipoEntidad) throws Exception
  {
    TipoEntidad tipoEntidadModify = 
      findTipoEntidadById(tipoEntidad.getIdTipoEntidad());

    BeanUtils.copyProperties(tipoEntidadModify, tipoEntidad);
  }

  public void deleteTipoEntidad(TipoEntidad tipoEntidad) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoEntidad tipoEntidadDelete = 
      findTipoEntidadById(tipoEntidad.getIdTipoEntidad());
    pm.deletePersistent(tipoEntidadDelete);
  }

  public TipoEntidad findTipoEntidadById(long idTipoEntidad) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoEntidad == pIdTipoEntidad";
    Query query = pm.newQuery(TipoEntidad.class, filter);

    query.declareParameters("long pIdTipoEntidad");

    parameters.put("pIdTipoEntidad", new Long(idTipoEntidad));

    Collection colTipoEntidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoEntidad.iterator();
    return (TipoEntidad)iterator.next();
  }

  public Collection findTipoEntidadAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoEntidadExtent = pm.getExtent(
      TipoEntidad.class, true);
    Query query = pm.newQuery(tipoEntidadExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoEntidad(String codTipoEntidad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoEntidad == pCodTipoEntidad";

    Query query = pm.newQuery(TipoEntidad.class, filter);

    query.declareParameters("java.lang.String pCodTipoEntidad");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoEntidad", new String(codTipoEntidad));

    query.setOrdering("descripcion ascending");

    Collection colTipoEntidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoEntidad);

    return colTipoEntidad;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(TipoEntidad.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTipoEntidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoEntidad);

    return colTipoEntidad;
  }
}