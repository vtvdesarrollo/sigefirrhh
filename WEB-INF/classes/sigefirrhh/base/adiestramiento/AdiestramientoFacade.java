package sigefirrhh.base.adiestramiento;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class AdiestramientoFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private AdiestramientoBusiness adiestramientoBusiness = new AdiestramientoBusiness();

  public void addAreaConocimiento(AreaConocimiento areaConocimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.adiestramientoBusiness.addAreaConocimiento(areaConocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAreaConocimiento(AreaConocimiento areaConocimiento) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.updateAreaConocimiento(areaConocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAreaConocimiento(AreaConocimiento areaConocimiento) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.deleteAreaConocimiento(areaConocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public AreaConocimiento findAreaConocimientoById(long areaConocimientoId) throws Exception
  {
    try { this.txn.open();
      AreaConocimiento areaConocimiento = 
        this.adiestramientoBusiness.findAreaConocimientoById(areaConocimientoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(areaConocimiento);
      return areaConocimiento;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAreaConocimiento() throws Exception
  {
    try { this.txn.open();
      return this.adiestramientoBusiness.findAllAreaConocimiento();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAreaConocimientoByCodAreaConocimiento(String codAreaConocimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findAreaConocimientoByCodAreaConocimiento(codAreaConocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAreaConocimientoByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findAreaConocimientoByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCurso(Curso curso)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.adiestramientoBusiness.addCurso(curso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCurso(Curso curso) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.updateCurso(curso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCurso(Curso curso) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.deleteCurso(curso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Curso findCursoById(long cursoId) throws Exception
  {
    try { this.txn.open();
      Curso curso = 
        this.adiestramientoBusiness.findCursoById(cursoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(curso);
      return curso;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCurso() throws Exception
  {
    try { this.txn.open();
      return this.adiestramientoBusiness.findAllCurso();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCursoByCodCurso(String codCurso)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findCursoByCodCurso(codCurso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCursoByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findCursoByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCursoByTipoCurso(long idTipoCurso)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findCursoByTipoCurso(idTipoCurso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCursoByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findCursoByAreaConocimiento(idAreaConocimiento);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCursoEntidad(CursoEntidad cursoEntidad)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.adiestramientoBusiness.addCursoEntidad(cursoEntidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCursoEntidad(CursoEntidad cursoEntidad) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.updateCursoEntidad(cursoEntidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCursoEntidad(CursoEntidad cursoEntidad) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.deleteCursoEntidad(cursoEntidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CursoEntidad findCursoEntidadById(long cursoEntidadId) throws Exception
  {
    try { this.txn.open();
      CursoEntidad cursoEntidad = 
        this.adiestramientoBusiness.findCursoEntidadById(cursoEntidadId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(cursoEntidad);
      return cursoEntidad;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCursoEntidad() throws Exception
  {
    try { this.txn.open();
      return this.adiestramientoBusiness.findAllCursoEntidad();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCursoEntidadByEntidadEducativa(long idEntidadEducativa)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findCursoEntidadByEntidadEducativa(idEntidadEducativa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCursoEntidadByCurso(long idCurso)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findCursoEntidadByCurso(idCurso);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEntidadEducativa(EntidadEducativa entidadEducativa)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.adiestramientoBusiness.addEntidadEducativa(entidadEducativa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEntidadEducativa(EntidadEducativa entidadEducativa) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.updateEntidadEducativa(entidadEducativa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEntidadEducativa(EntidadEducativa entidadEducativa) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.deleteEntidadEducativa(entidadEducativa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public EntidadEducativa findEntidadEducativaById(long entidadEducativaId) throws Exception
  {
    try { this.txn.open();
      EntidadEducativa entidadEducativa = 
        this.adiestramientoBusiness.findEntidadEducativaById(entidadEducativaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(entidadEducativa);
      return entidadEducativa;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEntidadEducativa() throws Exception
  {
    try { this.txn.open();
      return this.adiestramientoBusiness.findAllEntidadEducativa();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEntidadEducativaByCodEntidadEducativa(String codEntidadEducativa)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findEntidadEducativaByCodEntidadEducativa(codEntidadEducativa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEntidadEducativaByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findEntidadEducativaByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEntidadEducativaByTipoEntidad(long idTipoEntidad)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findEntidadEducativaByTipoEntidad(idTipoEntidad);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEntidadNivel(EntidadNivel entidadNivel)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.adiestramientoBusiness.addEntidadNivel(entidadNivel);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEntidadNivel(EntidadNivel entidadNivel) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.updateEntidadNivel(entidadNivel);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEntidadNivel(EntidadNivel entidadNivel) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.deleteEntidadNivel(entidadNivel);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public EntidadNivel findEntidadNivelById(long entidadNivelId) throws Exception
  {
    try { this.txn.open();
      EntidadNivel entidadNivel = 
        this.adiestramientoBusiness.findEntidadNivelById(entidadNivelId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(entidadNivel);
      return entidadNivel;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEntidadNivel() throws Exception
  {
    try { this.txn.open();
      return this.adiestramientoBusiness.findAllEntidadNivel();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEntidadNivelByEntidadEducativa(long idEntidadEducativa)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findEntidadNivelByEntidadEducativa(idEntidadEducativa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEntidadNivelByNivelEducativo(long idNivelEducativo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findEntidadNivelByNivelEducativo(idNivelEducativo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoCurso(TipoCurso tipoCurso)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.adiestramientoBusiness.addTipoCurso(tipoCurso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoCurso(TipoCurso tipoCurso) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.updateTipoCurso(tipoCurso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoCurso(TipoCurso tipoCurso) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.deleteTipoCurso(tipoCurso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoCurso findTipoCursoById(long tipoCursoId) throws Exception
  {
    try { this.txn.open();
      TipoCurso tipoCurso = 
        this.adiestramientoBusiness.findTipoCursoById(tipoCursoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoCurso);
      return tipoCurso;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoCurso() throws Exception
  {
    try { this.txn.open();
      return this.adiestramientoBusiness.findAllTipoCurso();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoCursoByCodTipoCurso(String codTipoCurso)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findTipoCursoByCodTipoCurso(codTipoCurso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoCursoByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findTipoCursoByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoEntidad(TipoEntidad tipoEntidad)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.adiestramientoBusiness.addTipoEntidad(tipoEntidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoEntidad(TipoEntidad tipoEntidad) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.updateTipoEntidad(tipoEntidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoEntidad(TipoEntidad tipoEntidad) throws Exception
  {
    try { this.txn.open();
      this.adiestramientoBusiness.deleteTipoEntidad(tipoEntidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoEntidad findTipoEntidadById(long tipoEntidadId) throws Exception
  {
    try { this.txn.open();
      TipoEntidad tipoEntidad = 
        this.adiestramientoBusiness.findTipoEntidadById(tipoEntidadId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoEntidad);
      return tipoEntidad;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoEntidad() throws Exception
  {
    try { this.txn.open();
      return this.adiestramientoBusiness.findAllTipoEntidad();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoEntidadByCodTipoEntidad(String codTipoEntidad)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findTipoEntidadByCodTipoEntidad(codTipoEntidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoEntidadByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.adiestramientoBusiness.findTipoEntidadByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}