package sigefirrhh.base.adiestramiento;

import java.io.Serializable;

public class EntidadNivelPK
  implements Serializable
{
  public long idEntidadNivel;

  public EntidadNivelPK()
  {
  }

  public EntidadNivelPK(long idEntidadNivel)
  {
    this.idEntidadNivel = idEntidadNivel;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EntidadNivelPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EntidadNivelPK thatPK)
  {
    return 
      this.idEntidadNivel == thatPK.idEntidadNivel;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEntidadNivel)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEntidadNivel);
  }
}