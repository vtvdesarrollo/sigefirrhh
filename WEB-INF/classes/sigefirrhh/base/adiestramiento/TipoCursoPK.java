package sigefirrhh.base.adiestramiento;

import java.io.Serializable;

public class TipoCursoPK
  implements Serializable
{
  public long idTipoCurso;

  public TipoCursoPK()
  {
  }

  public TipoCursoPK(long idTipoCurso)
  {
    this.idTipoCurso = idTipoCurso;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoCursoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoCursoPK thatPK)
  {
    return 
      this.idTipoCurso == thatPK.idTipoCurso;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoCurso)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoCurso);
  }
}