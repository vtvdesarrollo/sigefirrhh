package sigefirrhh.base.adiestramiento;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class CursoEntidadForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CursoEntidadForm.class.getName());
  private CursoEntidad cursoEntidad;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private AdiestramientoFacade adiestramientoFacade = new AdiestramientoFacade();
  private boolean showCursoEntidadByEntidadEducativa;
  private boolean showCursoEntidadByCurso;
  private String findSelectEntidadEducativa;
  private String findSelectCurso;
  private Collection findColEntidadEducativa;
  private Collection findColCurso;
  private Collection colEntidadEducativa;
  private Collection colCurso;
  private String selectEntidadEducativa;
  private String selectCurso;
  private Object stateResultCursoEntidadByEntidadEducativa = null;

  private Object stateResultCursoEntidadByCurso = null;

  public String getFindSelectEntidadEducativa()
  {
    return this.findSelectEntidadEducativa;
  }
  public void setFindSelectEntidadEducativa(String valEntidadEducativa) {
    this.findSelectEntidadEducativa = valEntidadEducativa;
  }

  public Collection getFindColEntidadEducativa() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColEntidadEducativa.iterator();
    EntidadEducativa entidadEducativa = null;
    while (iterator.hasNext()) {
      entidadEducativa = (EntidadEducativa)iterator.next();
      col.add(new SelectItem(
        String.valueOf(entidadEducativa.getIdEntidadEducativa()), 
        entidadEducativa.toString()));
    }
    return col;
  }
  public String getFindSelectCurso() {
    return this.findSelectCurso;
  }
  public void setFindSelectCurso(String valCurso) {
    this.findSelectCurso = valCurso;
  }

  public Collection getFindColCurso() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCurso.iterator();
    Curso curso = null;
    while (iterator.hasNext()) {
      curso = (Curso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(curso.getIdCurso()), 
        curso.toString()));
    }
    return col;
  }

  public String getSelectEntidadEducativa()
  {
    return this.selectEntidadEducativa;
  }
  public void setSelectEntidadEducativa(String valEntidadEducativa) {
    Iterator iterator = this.colEntidadEducativa.iterator();
    EntidadEducativa entidadEducativa = null;
    this.cursoEntidad.setEntidadEducativa(null);
    while (iterator.hasNext()) {
      entidadEducativa = (EntidadEducativa)iterator.next();
      if (String.valueOf(entidadEducativa.getIdEntidadEducativa()).equals(
        valEntidadEducativa)) {
        this.cursoEntidad.setEntidadEducativa(
          entidadEducativa);
        break;
      }
    }
    this.selectEntidadEducativa = valEntidadEducativa;
  }
  public String getSelectCurso() {
    return this.selectCurso;
  }
  public void setSelectCurso(String valCurso) {
    Iterator iterator = this.colCurso.iterator();
    Curso curso = null;
    this.cursoEntidad.setCurso(null);
    while (iterator.hasNext()) {
      curso = (Curso)iterator.next();
      if (String.valueOf(curso.getIdCurso()).equals(
        valCurso)) {
        this.cursoEntidad.setCurso(
          curso);
        break;
      }
    }
    this.selectCurso = valCurso;
  }
  public Collection getResult() {
    return this.result;
  }

  public CursoEntidad getCursoEntidad() {
    if (this.cursoEntidad == null) {
      this.cursoEntidad = new CursoEntidad();
    }
    return this.cursoEntidad;
  }

  public CursoEntidadForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColEntidadEducativa()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEntidadEducativa.iterator();
    EntidadEducativa entidadEducativa = null;
    while (iterator.hasNext()) {
      entidadEducativa = (EntidadEducativa)iterator.next();
      col.add(new SelectItem(
        String.valueOf(entidadEducativa.getIdEntidadEducativa()), 
        entidadEducativa.toString()));
    }
    return col;
  }

  public Collection getColCurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCurso.iterator();
    Curso curso = null;
    while (iterator.hasNext()) {
      curso = (Curso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(curso.getIdCurso()), 
        curso.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColEntidadEducativa = 
        this.adiestramientoFacade.findAllEntidadEducativa();
      this.findColCurso = 
        this.adiestramientoFacade.findAllCurso();

      this.colEntidadEducativa = 
        this.adiestramientoFacade.findAllEntidadEducativa();
      this.colCurso = 
        this.adiestramientoFacade.findAllCurso();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findCursoEntidadByEntidadEducativa()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findCursoEntidadByEntidadEducativa(Long.valueOf(this.findSelectEntidadEducativa).longValue());
      this.showCursoEntidadByEntidadEducativa = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCursoEntidadByEntidadEducativa)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectEntidadEducativa = null;
    this.findSelectCurso = null;

    return null;
  }

  public String findCursoEntidadByCurso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.adiestramientoFacade.findCursoEntidadByCurso(Long.valueOf(this.findSelectCurso).longValue());
      this.showCursoEntidadByCurso = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCursoEntidadByCurso)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectEntidadEducativa = null;
    this.findSelectCurso = null;

    return null;
  }

  public boolean isShowCursoEntidadByEntidadEducativa() {
    return this.showCursoEntidadByEntidadEducativa;
  }
  public boolean isShowCursoEntidadByCurso() {
    return this.showCursoEntidadByCurso;
  }

  public String selectCursoEntidad()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectEntidadEducativa = null;
    this.selectCurso = null;

    long idCursoEntidad = 
      Long.parseLong((String)requestParameterMap.get("idCursoEntidad"));
    try
    {
      this.cursoEntidad = 
        this.adiestramientoFacade.findCursoEntidadById(
        idCursoEntidad);
      if (this.cursoEntidad.getEntidadEducativa() != null) {
        this.selectEntidadEducativa = 
          String.valueOf(this.cursoEntidad.getEntidadEducativa().getIdEntidadEducativa());
      }
      if (this.cursoEntidad.getCurso() != null) {
        this.selectCurso = 
          String.valueOf(this.cursoEntidad.getCurso().getIdCurso());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.cursoEntidad = null;
    this.showCursoEntidadByEntidadEducativa = false;
    this.showCursoEntidadByCurso = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.adiestramientoFacade.addCursoEntidad(
          this.cursoEntidad);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.adiestramientoFacade.updateCursoEntidad(
          this.cursoEntidad);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
      log.error("Excepcion controlada:", e);
    }

    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.adiestramientoFacade.deleteCursoEntidad(
        this.cursoEntidad);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.cursoEntidad = new CursoEntidad();

    this.selectEntidadEducativa = null;

    this.selectCurso = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.cursoEntidad.setIdCursoEntidad(identityGenerator.getNextSequenceNumber("sigefirrhh.base.adiestramiento.CursoEntidad"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.cursoEntidad = new CursoEntidad();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}