package sigefirrhh.base.adiestramiento;

import java.io.Serializable;

public class TipoEntidadPK
  implements Serializable
{
  public long idTipoEntidad;

  public TipoEntidadPK()
  {
  }

  public TipoEntidadPK(long idTipoEntidad)
  {
    this.idTipoEntidad = idTipoEntidad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoEntidadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoEntidadPK thatPK)
  {
    return 
      this.idTipoEntidad == thatPK.idTipoEntidad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoEntidad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoEntidad);
  }
}