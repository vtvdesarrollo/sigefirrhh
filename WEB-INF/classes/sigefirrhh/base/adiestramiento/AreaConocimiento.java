package sigefirrhh.base.adiestramiento;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class AreaConocimiento
  implements Serializable, PersistenceCapable
{
  private long idAreaConocimiento;
  private String codAreaConocimiento;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codAreaConocimiento", "descripcion", "idAreaConocimiento" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + "  -  " + jdoGetcodAreaConocimiento(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public long getIdAreaConocimiento()
  {
    return jdoGetidAreaConocimiento(this);
  }

  public void setIdAreaConocimiento(long l)
  {
    jdoSetidAreaConocimiento(this, l);
  }

  public String getCodAreaConocimiento()
  {
    return jdoGetcodAreaConocimiento(this);
  }

  public void setCodAreaConocimiento(String string)
  {
    jdoSetcodAreaConocimiento(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.adiestramiento.AreaConocimiento"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AreaConocimiento());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AreaConocimiento localAreaConocimiento = new AreaConocimiento();
    localAreaConocimiento.jdoFlags = 1;
    localAreaConocimiento.jdoStateManager = paramStateManager;
    return localAreaConocimiento;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AreaConocimiento localAreaConocimiento = new AreaConocimiento();
    localAreaConocimiento.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAreaConocimiento.jdoFlags = 1;
    localAreaConocimiento.jdoStateManager = paramStateManager;
    return localAreaConocimiento;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codAreaConocimiento);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAreaConocimiento);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codAreaConocimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAreaConocimiento = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AreaConocimiento paramAreaConocimiento, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAreaConocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.codAreaConocimiento = paramAreaConocimiento.codAreaConocimiento;
      return;
    case 1:
      if (paramAreaConocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramAreaConocimiento.descripcion;
      return;
    case 2:
      if (paramAreaConocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.idAreaConocimiento = paramAreaConocimiento.idAreaConocimiento;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AreaConocimiento))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AreaConocimiento localAreaConocimiento = (AreaConocimiento)paramObject;
    if (localAreaConocimiento.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAreaConocimiento, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AreaConocimientoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AreaConocimientoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AreaConocimientoPK))
      throw new IllegalArgumentException("arg1");
    AreaConocimientoPK localAreaConocimientoPK = (AreaConocimientoPK)paramObject;
    localAreaConocimientoPK.idAreaConocimiento = this.idAreaConocimiento;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AreaConocimientoPK))
      throw new IllegalArgumentException("arg1");
    AreaConocimientoPK localAreaConocimientoPK = (AreaConocimientoPK)paramObject;
    this.idAreaConocimiento = localAreaConocimientoPK.idAreaConocimiento;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AreaConocimientoPK))
      throw new IllegalArgumentException("arg2");
    AreaConocimientoPK localAreaConocimientoPK = (AreaConocimientoPK)paramObject;
    localAreaConocimientoPK.idAreaConocimiento = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AreaConocimientoPK))
      throw new IllegalArgumentException("arg2");
    AreaConocimientoPK localAreaConocimientoPK = (AreaConocimientoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localAreaConocimientoPK.idAreaConocimiento);
  }

  private static final String jdoGetcodAreaConocimiento(AreaConocimiento paramAreaConocimiento)
  {
    if (paramAreaConocimiento.jdoFlags <= 0)
      return paramAreaConocimiento.codAreaConocimiento;
    StateManager localStateManager = paramAreaConocimiento.jdoStateManager;
    if (localStateManager == null)
      return paramAreaConocimiento.codAreaConocimiento;
    if (localStateManager.isLoaded(paramAreaConocimiento, jdoInheritedFieldCount + 0))
      return paramAreaConocimiento.codAreaConocimiento;
    return localStateManager.getStringField(paramAreaConocimiento, jdoInheritedFieldCount + 0, paramAreaConocimiento.codAreaConocimiento);
  }

  private static final void jdoSetcodAreaConocimiento(AreaConocimiento paramAreaConocimiento, String paramString)
  {
    if (paramAreaConocimiento.jdoFlags == 0)
    {
      paramAreaConocimiento.codAreaConocimiento = paramString;
      return;
    }
    StateManager localStateManager = paramAreaConocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramAreaConocimiento.codAreaConocimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramAreaConocimiento, jdoInheritedFieldCount + 0, paramAreaConocimiento.codAreaConocimiento, paramString);
  }

  private static final String jdoGetdescripcion(AreaConocimiento paramAreaConocimiento)
  {
    if (paramAreaConocimiento.jdoFlags <= 0)
      return paramAreaConocimiento.descripcion;
    StateManager localStateManager = paramAreaConocimiento.jdoStateManager;
    if (localStateManager == null)
      return paramAreaConocimiento.descripcion;
    if (localStateManager.isLoaded(paramAreaConocimiento, jdoInheritedFieldCount + 1))
      return paramAreaConocimiento.descripcion;
    return localStateManager.getStringField(paramAreaConocimiento, jdoInheritedFieldCount + 1, paramAreaConocimiento.descripcion);
  }

  private static final void jdoSetdescripcion(AreaConocimiento paramAreaConocimiento, String paramString)
  {
    if (paramAreaConocimiento.jdoFlags == 0)
    {
      paramAreaConocimiento.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramAreaConocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramAreaConocimiento.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramAreaConocimiento, jdoInheritedFieldCount + 1, paramAreaConocimiento.descripcion, paramString);
  }

  private static final long jdoGetidAreaConocimiento(AreaConocimiento paramAreaConocimiento)
  {
    return paramAreaConocimiento.idAreaConocimiento;
  }

  private static final void jdoSetidAreaConocimiento(AreaConocimiento paramAreaConocimiento, long paramLong)
  {
    StateManager localStateManager = paramAreaConocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramAreaConocimiento.idAreaConocimiento = paramLong;
      return;
    }
    localStateManager.setLongField(paramAreaConocimiento, jdoInheritedFieldCount + 2, paramAreaConocimiento.idAreaConocimiento, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}