package sigefirrhh.base.adiestramiento;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class CursoEntidad
  implements Serializable, PersistenceCapable
{
  private long idCursoEntidad;
  private EntidadEducativa entidadEducativa;
  private Curso curso;
  private double costoParticipante;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "costoParticipante", "curso", "entidadEducativa", "idCursoEntidad" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, sunjdo$classForName$("sigefirrhh.base.adiestramiento.Curso"), sunjdo$classForName$("sigefirrhh.base.adiestramiento.EntidadEducativa"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 26, 26, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcurso(this).getDescripcion() + "  -  " + jdoGetentidadEducativa(this).getNombre();
  }

  public double getCostoParticipante()
  {
    return jdoGetcostoParticipante(this);
  }

  public Curso getCurso()
  {
    return jdoGetcurso(this);
  }

  public EntidadEducativa getEntidadEducativa()
  {
    return jdoGetentidadEducativa(this);
  }

  public long getIdCursoEntidad()
  {
    return jdoGetidCursoEntidad(this);
  }

  public void setCostoParticipante(double d)
  {
    jdoSetcostoParticipante(this, d);
  }

  public void setCurso(Curso curso)
  {
    jdoSetcurso(this, curso);
  }

  public void setEntidadEducativa(EntidadEducativa educativa)
  {
    jdoSetentidadEducativa(this, educativa);
  }

  public void setIdCursoEntidad(long l)
  {
    jdoSetidCursoEntidad(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.adiestramiento.CursoEntidad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CursoEntidad());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CursoEntidad localCursoEntidad = new CursoEntidad();
    localCursoEntidad.jdoFlags = 1;
    localCursoEntidad.jdoStateManager = paramStateManager;
    return localCursoEntidad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CursoEntidad localCursoEntidad = new CursoEntidad();
    localCursoEntidad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCursoEntidad.jdoFlags = 1;
    localCursoEntidad.jdoStateManager = paramStateManager;
    return localCursoEntidad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costoParticipante);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.curso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.entidadEducativa);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCursoEntidad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costoParticipante = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.curso = ((Curso)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entidadEducativa = ((EntidadEducativa)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCursoEntidad = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CursoEntidad paramCursoEntidad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCursoEntidad == null)
        throw new IllegalArgumentException("arg1");
      this.costoParticipante = paramCursoEntidad.costoParticipante;
      return;
    case 1:
      if (paramCursoEntidad == null)
        throw new IllegalArgumentException("arg1");
      this.curso = paramCursoEntidad.curso;
      return;
    case 2:
      if (paramCursoEntidad == null)
        throw new IllegalArgumentException("arg1");
      this.entidadEducativa = paramCursoEntidad.entidadEducativa;
      return;
    case 3:
      if (paramCursoEntidad == null)
        throw new IllegalArgumentException("arg1");
      this.idCursoEntidad = paramCursoEntidad.idCursoEntidad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CursoEntidad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CursoEntidad localCursoEntidad = (CursoEntidad)paramObject;
    if (localCursoEntidad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCursoEntidad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CursoEntidadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CursoEntidadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CursoEntidadPK))
      throw new IllegalArgumentException("arg1");
    CursoEntidadPK localCursoEntidadPK = (CursoEntidadPK)paramObject;
    localCursoEntidadPK.idCursoEntidad = this.idCursoEntidad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CursoEntidadPK))
      throw new IllegalArgumentException("arg1");
    CursoEntidadPK localCursoEntidadPK = (CursoEntidadPK)paramObject;
    this.idCursoEntidad = localCursoEntidadPK.idCursoEntidad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CursoEntidadPK))
      throw new IllegalArgumentException("arg2");
    CursoEntidadPK localCursoEntidadPK = (CursoEntidadPK)paramObject;
    localCursoEntidadPK.idCursoEntidad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CursoEntidadPK))
      throw new IllegalArgumentException("arg2");
    CursoEntidadPK localCursoEntidadPK = (CursoEntidadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localCursoEntidadPK.idCursoEntidad);
  }

  private static final double jdoGetcostoParticipante(CursoEntidad paramCursoEntidad)
  {
    if (paramCursoEntidad.jdoFlags <= 0)
      return paramCursoEntidad.costoParticipante;
    StateManager localStateManager = paramCursoEntidad.jdoStateManager;
    if (localStateManager == null)
      return paramCursoEntidad.costoParticipante;
    if (localStateManager.isLoaded(paramCursoEntidad, jdoInheritedFieldCount + 0))
      return paramCursoEntidad.costoParticipante;
    return localStateManager.getDoubleField(paramCursoEntidad, jdoInheritedFieldCount + 0, paramCursoEntidad.costoParticipante);
  }

  private static final void jdoSetcostoParticipante(CursoEntidad paramCursoEntidad, double paramDouble)
  {
    if (paramCursoEntidad.jdoFlags == 0)
    {
      paramCursoEntidad.costoParticipante = paramDouble;
      return;
    }
    StateManager localStateManager = paramCursoEntidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCursoEntidad.costoParticipante = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCursoEntidad, jdoInheritedFieldCount + 0, paramCursoEntidad.costoParticipante, paramDouble);
  }

  private static final Curso jdoGetcurso(CursoEntidad paramCursoEntidad)
  {
    StateManager localStateManager = paramCursoEntidad.jdoStateManager;
    if (localStateManager == null)
      return paramCursoEntidad.curso;
    if (localStateManager.isLoaded(paramCursoEntidad, jdoInheritedFieldCount + 1))
      return paramCursoEntidad.curso;
    return (Curso)localStateManager.getObjectField(paramCursoEntidad, jdoInheritedFieldCount + 1, paramCursoEntidad.curso);
  }

  private static final void jdoSetcurso(CursoEntidad paramCursoEntidad, Curso paramCurso)
  {
    StateManager localStateManager = paramCursoEntidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCursoEntidad.curso = paramCurso;
      return;
    }
    localStateManager.setObjectField(paramCursoEntidad, jdoInheritedFieldCount + 1, paramCursoEntidad.curso, paramCurso);
  }

  private static final EntidadEducativa jdoGetentidadEducativa(CursoEntidad paramCursoEntidad)
  {
    StateManager localStateManager = paramCursoEntidad.jdoStateManager;
    if (localStateManager == null)
      return paramCursoEntidad.entidadEducativa;
    if (localStateManager.isLoaded(paramCursoEntidad, jdoInheritedFieldCount + 2))
      return paramCursoEntidad.entidadEducativa;
    return (EntidadEducativa)localStateManager.getObjectField(paramCursoEntidad, jdoInheritedFieldCount + 2, paramCursoEntidad.entidadEducativa);
  }

  private static final void jdoSetentidadEducativa(CursoEntidad paramCursoEntidad, EntidadEducativa paramEntidadEducativa)
  {
    StateManager localStateManager = paramCursoEntidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCursoEntidad.entidadEducativa = paramEntidadEducativa;
      return;
    }
    localStateManager.setObjectField(paramCursoEntidad, jdoInheritedFieldCount + 2, paramCursoEntidad.entidadEducativa, paramEntidadEducativa);
  }

  private static final long jdoGetidCursoEntidad(CursoEntidad paramCursoEntidad)
  {
    return paramCursoEntidad.idCursoEntidad;
  }

  private static final void jdoSetidCursoEntidad(CursoEntidad paramCursoEntidad, long paramLong)
  {
    StateManager localStateManager = paramCursoEntidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCursoEntidad.idCursoEntidad = paramLong;
      return;
    }
    localStateManager.setLongField(paramCursoEntidad, jdoInheritedFieldCount + 3, paramCursoEntidad.idCursoEntidad, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}