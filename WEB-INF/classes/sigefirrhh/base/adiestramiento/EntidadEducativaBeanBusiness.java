package sigefirrhh.base.adiestramiento;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.EstadoBeanBusiness;

public class EntidadEducativaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEntidadEducativa(EntidadEducativa entidadEducativa)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    EntidadEducativa entidadEducativaNew = 
      (EntidadEducativa)BeanUtils.cloneBean(
      entidadEducativa);

    TipoEntidadBeanBusiness tipoEntidadBeanBusiness = new TipoEntidadBeanBusiness();

    if (entidadEducativaNew.getTipoEntidad() != null) {
      entidadEducativaNew.setTipoEntidad(
        tipoEntidadBeanBusiness.findTipoEntidadById(
        entidadEducativaNew.getTipoEntidad().getIdTipoEntidad()));
    }

    EstadoBeanBusiness estadoBeanBusiness = new EstadoBeanBusiness();

    if (entidadEducativaNew.getEstado() != null) {
      entidadEducativaNew.setEstado(
        estadoBeanBusiness.findEstadoById(
        entidadEducativaNew.getEstado().getIdEstado()));
    }
    pm.makePersistent(entidadEducativaNew);
  }

  public void updateEntidadEducativa(EntidadEducativa entidadEducativa) throws Exception
  {
    EntidadEducativa entidadEducativaModify = 
      findEntidadEducativaById(entidadEducativa.getIdEntidadEducativa());

    TipoEntidadBeanBusiness tipoEntidadBeanBusiness = new TipoEntidadBeanBusiness();

    if (entidadEducativa.getTipoEntidad() != null) {
      entidadEducativa.setTipoEntidad(
        tipoEntidadBeanBusiness.findTipoEntidadById(
        entidadEducativa.getTipoEntidad().getIdTipoEntidad()));
    }

    EstadoBeanBusiness estadoBeanBusiness = new EstadoBeanBusiness();

    if (entidadEducativa.getEstado() != null) {
      entidadEducativa.setEstado(
        estadoBeanBusiness.findEstadoById(
        entidadEducativa.getEstado().getIdEstado()));
    }

    BeanUtils.copyProperties(entidadEducativaModify, entidadEducativa);
  }

  public void deleteEntidadEducativa(EntidadEducativa entidadEducativa) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    EntidadEducativa entidadEducativaDelete = 
      findEntidadEducativaById(entidadEducativa.getIdEntidadEducativa());
    pm.deletePersistent(entidadEducativaDelete);
  }

  public EntidadEducativa findEntidadEducativaById(long idEntidadEducativa) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEntidadEducativa == pIdEntidadEducativa";
    Query query = pm.newQuery(EntidadEducativa.class, filter);

    query.declareParameters("long pIdEntidadEducativa");

    parameters.put("pIdEntidadEducativa", new Long(idEntidadEducativa));

    Collection colEntidadEducativa = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEntidadEducativa.iterator();
    return (EntidadEducativa)iterator.next();
  }

  public Collection findEntidadEducativaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent entidadEducativaExtent = pm.getExtent(
      EntidadEducativa.class, true);
    Query query = pm.newQuery(entidadEducativaExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodEntidadEducativa(String codEntidadEducativa)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codEntidadEducativa.startsWith(pCodEntidadEducativa)";

    Query query = pm.newQuery(EntidadEducativa.class, filter);

    query.declareParameters("java.lang.String pCodEntidadEducativa");
    HashMap parameters = new HashMap();

    parameters.put("pCodEntidadEducativa", new String(codEntidadEducativa));

    query.setOrdering("nombre ascending");

    Collection colEntidadEducativa = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEntidadEducativa);

    return colEntidadEducativa;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(EntidadEducativa.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colEntidadEducativa = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEntidadEducativa);

    return colEntidadEducativa;
  }

  public Collection findByTipoEntidad(long idTipoEntidad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoEntidad.idTipoEntidad == pIdTipoEntidad";

    Query query = pm.newQuery(EntidadEducativa.class, filter);

    query.declareParameters("long pIdTipoEntidad");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoEntidad", new Long(idTipoEntidad));

    query.setOrdering("nombre ascending");

    Collection colEntidadEducativa = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEntidadEducativa);

    return colEntidadEducativa;
  }
}