package sigefirrhh.base.adiestramiento;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.ubicacion.Estado;

public class EntidadEducativa
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SECTOR;
  private long idEntidadEducativa;
  private String codEntidadEducativa;
  private String nombre;
  private String direccion;
  private String telefono1;
  private String telefono2;
  private String personaContacto;
  private String codInce;
  private String numeroRif;
  private String numeroMed;
  private String sector;
  private TipoEntidad tipoEntidad;
  private Estado estado;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codEntidadEducativa", "codInce", "direccion", "estado", "idEntidadEducativa", "nombre", "numeroMed", "numeroRif", "personaContacto", "sector", "telefono1", "telefono2", "tipoEntidad" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Estado"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.adiestramiento.TipoEntidad") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 24, 21, 21, 21, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.adiestramiento.EntidadEducativa"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new EntidadEducativa());

    LISTA_SECTOR = 
      new LinkedHashMap();

    LISTA_SECTOR.put("P", "PRIVADO");
    LISTA_SECTOR.put("U", "PUBLICO");
  }

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + jdoGetcodEntidadEducativa(this);
  }

  public String getCodEntidadEducativa()
  {
    return jdoGetcodEntidadEducativa(this);
  }

  public String getCodInce()
  {
    return jdoGetcodInce(this);
  }

  public String getDireccion()
  {
    return jdoGetdireccion(this);
  }

  public Estado getEstado()
  {
    return jdoGetestado(this);
  }

  public long getIdEntidadEducativa()
  {
    return jdoGetidEntidadEducativa(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public String getNumeroMed()
  {
    return jdoGetnumeroMed(this);
  }

  public String getNumeroRif()
  {
    return jdoGetnumeroRif(this);
  }

  public String getPersonaContacto()
  {
    return jdoGetpersonaContacto(this);
  }

  public String getSector()
  {
    return jdoGetsector(this);
  }

  public String getTelefono1()
  {
    return jdoGettelefono1(this);
  }

  public String getTelefono2()
  {
    return jdoGettelefono2(this);
  }

  public TipoEntidad getTipoEntidad()
  {
    return jdoGettipoEntidad(this);
  }

  public void setCodEntidadEducativa(String string)
  {
    jdoSetcodEntidadEducativa(this, string);
  }

  public void setCodInce(String string)
  {
    jdoSetcodInce(this, string);
  }

  public void setDireccion(String string)
  {
    jdoSetdireccion(this, string);
  }

  public void setEstado(Estado estado)
  {
    jdoSetestado(this, estado);
  }

  public void setIdEntidadEducativa(long l)
  {
    jdoSetidEntidadEducativa(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setNumeroMed(String string)
  {
    jdoSetnumeroMed(this, string);
  }

  public void setNumeroRif(String string)
  {
    jdoSetnumeroRif(this, string);
  }

  public void setPersonaContacto(String string)
  {
    jdoSetpersonaContacto(this, string);
  }

  public void setSector(String string)
  {
    jdoSetsector(this, string);
  }

  public void setTelefono1(String string)
  {
    jdoSettelefono1(this, string);
  }

  public void setTelefono2(String string)
  {
    jdoSettelefono2(this, string);
  }

  public void setTipoEntidad(TipoEntidad entidad)
  {
    jdoSettipoEntidad(this, entidad);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    EntidadEducativa localEntidadEducativa = new EntidadEducativa();
    localEntidadEducativa.jdoFlags = 1;
    localEntidadEducativa.jdoStateManager = paramStateManager;
    return localEntidadEducativa;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    EntidadEducativa localEntidadEducativa = new EntidadEducativa();
    localEntidadEducativa.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEntidadEducativa.jdoFlags = 1;
    localEntidadEducativa.jdoStateManager = paramStateManager;
    return localEntidadEducativa;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codEntidadEducativa);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codInce);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.direccion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.estado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEntidadEducativa);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroMed);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroRif);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.personaContacto);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sector);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono1);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono2);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoEntidad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codEntidadEducativa = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codInce = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.direccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estado = ((Estado)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEntidadEducativa = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMed = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroRif = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personaContacto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sector = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono1 = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono2 = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoEntidad = ((TipoEntidad)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(EntidadEducativa paramEntidadEducativa, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.codEntidadEducativa = paramEntidadEducativa.codEntidadEducativa;
      return;
    case 1:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.codInce = paramEntidadEducativa.codInce;
      return;
    case 2:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.direccion = paramEntidadEducativa.direccion;
      return;
    case 3:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.estado = paramEntidadEducativa.estado;
      return;
    case 4:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.idEntidadEducativa = paramEntidadEducativa.idEntidadEducativa;
      return;
    case 5:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramEntidadEducativa.nombre;
      return;
    case 6:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMed = paramEntidadEducativa.numeroMed;
      return;
    case 7:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.numeroRif = paramEntidadEducativa.numeroRif;
      return;
    case 8:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.personaContacto = paramEntidadEducativa.personaContacto;
      return;
    case 9:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.sector = paramEntidadEducativa.sector;
      return;
    case 10:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.telefono1 = paramEntidadEducativa.telefono1;
      return;
    case 11:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.telefono2 = paramEntidadEducativa.telefono2;
      return;
    case 12:
      if (paramEntidadEducativa == null)
        throw new IllegalArgumentException("arg1");
      this.tipoEntidad = paramEntidadEducativa.tipoEntidad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof EntidadEducativa))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    EntidadEducativa localEntidadEducativa = (EntidadEducativa)paramObject;
    if (localEntidadEducativa.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEntidadEducativa, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EntidadEducativaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EntidadEducativaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EntidadEducativaPK))
      throw new IllegalArgumentException("arg1");
    EntidadEducativaPK localEntidadEducativaPK = (EntidadEducativaPK)paramObject;
    localEntidadEducativaPK.idEntidadEducativa = this.idEntidadEducativa;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EntidadEducativaPK))
      throw new IllegalArgumentException("arg1");
    EntidadEducativaPK localEntidadEducativaPK = (EntidadEducativaPK)paramObject;
    this.idEntidadEducativa = localEntidadEducativaPK.idEntidadEducativa;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EntidadEducativaPK))
      throw new IllegalArgumentException("arg2");
    EntidadEducativaPK localEntidadEducativaPK = (EntidadEducativaPK)paramObject;
    localEntidadEducativaPK.idEntidadEducativa = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EntidadEducativaPK))
      throw new IllegalArgumentException("arg2");
    EntidadEducativaPK localEntidadEducativaPK = (EntidadEducativaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localEntidadEducativaPK.idEntidadEducativa);
  }

  private static final String jdoGetcodEntidadEducativa(EntidadEducativa paramEntidadEducativa)
  {
    if (paramEntidadEducativa.jdoFlags <= 0)
      return paramEntidadEducativa.codEntidadEducativa;
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.codEntidadEducativa;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 0))
      return paramEntidadEducativa.codEntidadEducativa;
    return localStateManager.getStringField(paramEntidadEducativa, jdoInheritedFieldCount + 0, paramEntidadEducativa.codEntidadEducativa);
  }

  private static final void jdoSetcodEntidadEducativa(EntidadEducativa paramEntidadEducativa, String paramString)
  {
    if (paramEntidadEducativa.jdoFlags == 0)
    {
      paramEntidadEducativa.codEntidadEducativa = paramString;
      return;
    }
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.codEntidadEducativa = paramString;
      return;
    }
    localStateManager.setStringField(paramEntidadEducativa, jdoInheritedFieldCount + 0, paramEntidadEducativa.codEntidadEducativa, paramString);
  }

  private static final String jdoGetcodInce(EntidadEducativa paramEntidadEducativa)
  {
    if (paramEntidadEducativa.jdoFlags <= 0)
      return paramEntidadEducativa.codInce;
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.codInce;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 1))
      return paramEntidadEducativa.codInce;
    return localStateManager.getStringField(paramEntidadEducativa, jdoInheritedFieldCount + 1, paramEntidadEducativa.codInce);
  }

  private static final void jdoSetcodInce(EntidadEducativa paramEntidadEducativa, String paramString)
  {
    if (paramEntidadEducativa.jdoFlags == 0)
    {
      paramEntidadEducativa.codInce = paramString;
      return;
    }
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.codInce = paramString;
      return;
    }
    localStateManager.setStringField(paramEntidadEducativa, jdoInheritedFieldCount + 1, paramEntidadEducativa.codInce, paramString);
  }

  private static final String jdoGetdireccion(EntidadEducativa paramEntidadEducativa)
  {
    if (paramEntidadEducativa.jdoFlags <= 0)
      return paramEntidadEducativa.direccion;
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.direccion;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 2))
      return paramEntidadEducativa.direccion;
    return localStateManager.getStringField(paramEntidadEducativa, jdoInheritedFieldCount + 2, paramEntidadEducativa.direccion);
  }

  private static final void jdoSetdireccion(EntidadEducativa paramEntidadEducativa, String paramString)
  {
    if (paramEntidadEducativa.jdoFlags == 0)
    {
      paramEntidadEducativa.direccion = paramString;
      return;
    }
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.direccion = paramString;
      return;
    }
    localStateManager.setStringField(paramEntidadEducativa, jdoInheritedFieldCount + 2, paramEntidadEducativa.direccion, paramString);
  }

  private static final Estado jdoGetestado(EntidadEducativa paramEntidadEducativa)
  {
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.estado;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 3))
      return paramEntidadEducativa.estado;
    return (Estado)localStateManager.getObjectField(paramEntidadEducativa, jdoInheritedFieldCount + 3, paramEntidadEducativa.estado);
  }

  private static final void jdoSetestado(EntidadEducativa paramEntidadEducativa, Estado paramEstado)
  {
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.estado = paramEstado;
      return;
    }
    localStateManager.setObjectField(paramEntidadEducativa, jdoInheritedFieldCount + 3, paramEntidadEducativa.estado, paramEstado);
  }

  private static final long jdoGetidEntidadEducativa(EntidadEducativa paramEntidadEducativa)
  {
    return paramEntidadEducativa.idEntidadEducativa;
  }

  private static final void jdoSetidEntidadEducativa(EntidadEducativa paramEntidadEducativa, long paramLong)
  {
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.idEntidadEducativa = paramLong;
      return;
    }
    localStateManager.setLongField(paramEntidadEducativa, jdoInheritedFieldCount + 4, paramEntidadEducativa.idEntidadEducativa, paramLong);
  }

  private static final String jdoGetnombre(EntidadEducativa paramEntidadEducativa)
  {
    if (paramEntidadEducativa.jdoFlags <= 0)
      return paramEntidadEducativa.nombre;
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.nombre;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 5))
      return paramEntidadEducativa.nombre;
    return localStateManager.getStringField(paramEntidadEducativa, jdoInheritedFieldCount + 5, paramEntidadEducativa.nombre);
  }

  private static final void jdoSetnombre(EntidadEducativa paramEntidadEducativa, String paramString)
  {
    if (paramEntidadEducativa.jdoFlags == 0)
    {
      paramEntidadEducativa.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramEntidadEducativa, jdoInheritedFieldCount + 5, paramEntidadEducativa.nombre, paramString);
  }

  private static final String jdoGetnumeroMed(EntidadEducativa paramEntidadEducativa)
  {
    if (paramEntidadEducativa.jdoFlags <= 0)
      return paramEntidadEducativa.numeroMed;
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.numeroMed;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 6))
      return paramEntidadEducativa.numeroMed;
    return localStateManager.getStringField(paramEntidadEducativa, jdoInheritedFieldCount + 6, paramEntidadEducativa.numeroMed);
  }

  private static final void jdoSetnumeroMed(EntidadEducativa paramEntidadEducativa, String paramString)
  {
    if (paramEntidadEducativa.jdoFlags == 0)
    {
      paramEntidadEducativa.numeroMed = paramString;
      return;
    }
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.numeroMed = paramString;
      return;
    }
    localStateManager.setStringField(paramEntidadEducativa, jdoInheritedFieldCount + 6, paramEntidadEducativa.numeroMed, paramString);
  }

  private static final String jdoGetnumeroRif(EntidadEducativa paramEntidadEducativa)
  {
    if (paramEntidadEducativa.jdoFlags <= 0)
      return paramEntidadEducativa.numeroRif;
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.numeroRif;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 7))
      return paramEntidadEducativa.numeroRif;
    return localStateManager.getStringField(paramEntidadEducativa, jdoInheritedFieldCount + 7, paramEntidadEducativa.numeroRif);
  }

  private static final void jdoSetnumeroRif(EntidadEducativa paramEntidadEducativa, String paramString)
  {
    if (paramEntidadEducativa.jdoFlags == 0)
    {
      paramEntidadEducativa.numeroRif = paramString;
      return;
    }
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.numeroRif = paramString;
      return;
    }
    localStateManager.setStringField(paramEntidadEducativa, jdoInheritedFieldCount + 7, paramEntidadEducativa.numeroRif, paramString);
  }

  private static final String jdoGetpersonaContacto(EntidadEducativa paramEntidadEducativa)
  {
    if (paramEntidadEducativa.jdoFlags <= 0)
      return paramEntidadEducativa.personaContacto;
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.personaContacto;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 8))
      return paramEntidadEducativa.personaContacto;
    return localStateManager.getStringField(paramEntidadEducativa, jdoInheritedFieldCount + 8, paramEntidadEducativa.personaContacto);
  }

  private static final void jdoSetpersonaContacto(EntidadEducativa paramEntidadEducativa, String paramString)
  {
    if (paramEntidadEducativa.jdoFlags == 0)
    {
      paramEntidadEducativa.personaContacto = paramString;
      return;
    }
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.personaContacto = paramString;
      return;
    }
    localStateManager.setStringField(paramEntidadEducativa, jdoInheritedFieldCount + 8, paramEntidadEducativa.personaContacto, paramString);
  }

  private static final String jdoGetsector(EntidadEducativa paramEntidadEducativa)
  {
    if (paramEntidadEducativa.jdoFlags <= 0)
      return paramEntidadEducativa.sector;
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.sector;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 9))
      return paramEntidadEducativa.sector;
    return localStateManager.getStringField(paramEntidadEducativa, jdoInheritedFieldCount + 9, paramEntidadEducativa.sector);
  }

  private static final void jdoSetsector(EntidadEducativa paramEntidadEducativa, String paramString)
  {
    if (paramEntidadEducativa.jdoFlags == 0)
    {
      paramEntidadEducativa.sector = paramString;
      return;
    }
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.sector = paramString;
      return;
    }
    localStateManager.setStringField(paramEntidadEducativa, jdoInheritedFieldCount + 9, paramEntidadEducativa.sector, paramString);
  }

  private static final String jdoGettelefono1(EntidadEducativa paramEntidadEducativa)
  {
    if (paramEntidadEducativa.jdoFlags <= 0)
      return paramEntidadEducativa.telefono1;
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.telefono1;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 10))
      return paramEntidadEducativa.telefono1;
    return localStateManager.getStringField(paramEntidadEducativa, jdoInheritedFieldCount + 10, paramEntidadEducativa.telefono1);
  }

  private static final void jdoSettelefono1(EntidadEducativa paramEntidadEducativa, String paramString)
  {
    if (paramEntidadEducativa.jdoFlags == 0)
    {
      paramEntidadEducativa.telefono1 = paramString;
      return;
    }
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.telefono1 = paramString;
      return;
    }
    localStateManager.setStringField(paramEntidadEducativa, jdoInheritedFieldCount + 10, paramEntidadEducativa.telefono1, paramString);
  }

  private static final String jdoGettelefono2(EntidadEducativa paramEntidadEducativa)
  {
    if (paramEntidadEducativa.jdoFlags <= 0)
      return paramEntidadEducativa.telefono2;
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.telefono2;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 11))
      return paramEntidadEducativa.telefono2;
    return localStateManager.getStringField(paramEntidadEducativa, jdoInheritedFieldCount + 11, paramEntidadEducativa.telefono2);
  }

  private static final void jdoSettelefono2(EntidadEducativa paramEntidadEducativa, String paramString)
  {
    if (paramEntidadEducativa.jdoFlags == 0)
    {
      paramEntidadEducativa.telefono2 = paramString;
      return;
    }
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.telefono2 = paramString;
      return;
    }
    localStateManager.setStringField(paramEntidadEducativa, jdoInheritedFieldCount + 11, paramEntidadEducativa.telefono2, paramString);
  }

  private static final TipoEntidad jdoGettipoEntidad(EntidadEducativa paramEntidadEducativa)
  {
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
      return paramEntidadEducativa.tipoEntidad;
    if (localStateManager.isLoaded(paramEntidadEducativa, jdoInheritedFieldCount + 12))
      return paramEntidadEducativa.tipoEntidad;
    return (TipoEntidad)localStateManager.getObjectField(paramEntidadEducativa, jdoInheritedFieldCount + 12, paramEntidadEducativa.tipoEntidad);
  }

  private static final void jdoSettipoEntidad(EntidadEducativa paramEntidadEducativa, TipoEntidad paramTipoEntidad)
  {
    StateManager localStateManager = paramEntidadEducativa.jdoStateManager;
    if (localStateManager == null)
    {
      paramEntidadEducativa.tipoEntidad = paramTipoEntidad;
      return;
    }
    localStateManager.setObjectField(paramEntidadEducativa, jdoInheritedFieldCount + 12, paramEntidadEducativa.tipoEntidad, paramTipoEntidad);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}