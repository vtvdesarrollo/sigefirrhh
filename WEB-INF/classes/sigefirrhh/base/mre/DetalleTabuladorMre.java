package sigefirrhh.base.mre;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.ubicacion.Ciudad;

public class DetalleTabuladorMre
  implements Serializable, PersistenceCapable
{
  private long idDetalleTabuladorMre;
  private Tabulador tabulador;
  private int nivel;
  private double asignacionMensual;
  private double fluctuacionMensual;
  private Ciudad ciudad;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "asignacionMensual", "ciudad", "fluctuacionMensual", "idDetalleTabuladorMre", "nivel", "tabulador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), Double.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Tabulador") };
  private static final byte[] jdoFieldFlags = { 21, 26, 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public DetalleTabuladorMre()
  {
    jdoSetnivel(this, 1);

    jdoSetasignacionMensual(this, 0.0D);

    jdoSetfluctuacionMensual(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetasignacionMensual(this));
    String c = b.format(jdoGetfluctuacionMensual(this));

    return jdoGetnivel(this) + " - " + jdoGetciudad(this).getNombre() + " - " + a + " - " + c;
  }

  public double getAsignacionMensual() {
    return jdoGetasignacionMensual(this);
  }

  public void setAsignacionMensual(double asignacionMensual) {
    jdoSetasignacionMensual(this, asignacionMensual);
  }

  public Ciudad getCiudad() {
    return jdoGetciudad(this);
  }

  public void setCiudad(Ciudad ciudad) {
    jdoSetciudad(this, ciudad);
  }

  public double getFluctuacionMensual() {
    return jdoGetfluctuacionMensual(this);
  }

  public void setFluctuacionMensual(double fluctuacionMensual) {
    jdoSetfluctuacionMensual(this, fluctuacionMensual);
  }

  public long getIdDetalleTabuladorMre() {
    return jdoGetidDetalleTabuladorMre(this);
  }

  public void setIdDetalleTabuladorMre(long idDetalleTabuladorMre) {
    jdoSetidDetalleTabuladorMre(this, idDetalleTabuladorMre);
  }

  public int getNivel() {
    return jdoGetnivel(this);
  }

  public void setNivel(int nivel) {
    jdoSetnivel(this, nivel);
  }

  public Tabulador getTabulador() {
    return jdoGettabulador(this);
  }

  public void setTabulador(Tabulador tabulador) {
    jdoSettabulador(this, tabulador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.mre.DetalleTabuladorMre"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DetalleTabuladorMre());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DetalleTabuladorMre localDetalleTabuladorMre = new DetalleTabuladorMre();
    localDetalleTabuladorMre.jdoFlags = 1;
    localDetalleTabuladorMre.jdoStateManager = paramStateManager;
    return localDetalleTabuladorMre;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DetalleTabuladorMre localDetalleTabuladorMre = new DetalleTabuladorMre();
    localDetalleTabuladorMre.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDetalleTabuladorMre.jdoFlags = 1;
    localDetalleTabuladorMre.jdoStateManager = paramStateManager;
    return localDetalleTabuladorMre;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.asignacionMensual);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.fluctuacionMensual);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDetalleTabuladorMre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.nivel);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tabulador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asignacionMensual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fluctuacionMensual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDetalleTabuladorMre = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivel = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tabulador = ((Tabulador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DetalleTabuladorMre paramDetalleTabuladorMre, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDetalleTabuladorMre == null)
        throw new IllegalArgumentException("arg1");
      this.asignacionMensual = paramDetalleTabuladorMre.asignacionMensual;
      return;
    case 1:
      if (paramDetalleTabuladorMre == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramDetalleTabuladorMre.ciudad;
      return;
    case 2:
      if (paramDetalleTabuladorMre == null)
        throw new IllegalArgumentException("arg1");
      this.fluctuacionMensual = paramDetalleTabuladorMre.fluctuacionMensual;
      return;
    case 3:
      if (paramDetalleTabuladorMre == null)
        throw new IllegalArgumentException("arg1");
      this.idDetalleTabuladorMre = paramDetalleTabuladorMre.idDetalleTabuladorMre;
      return;
    case 4:
      if (paramDetalleTabuladorMre == null)
        throw new IllegalArgumentException("arg1");
      this.nivel = paramDetalleTabuladorMre.nivel;
      return;
    case 5:
      if (paramDetalleTabuladorMre == null)
        throw new IllegalArgumentException("arg1");
      this.tabulador = paramDetalleTabuladorMre.tabulador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DetalleTabuladorMre))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DetalleTabuladorMre localDetalleTabuladorMre = (DetalleTabuladorMre)paramObject;
    if (localDetalleTabuladorMre.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDetalleTabuladorMre, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DetalleTabuladorMrePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DetalleTabuladorMrePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DetalleTabuladorMrePK))
      throw new IllegalArgumentException("arg1");
    DetalleTabuladorMrePK localDetalleTabuladorMrePK = (DetalleTabuladorMrePK)paramObject;
    localDetalleTabuladorMrePK.idDetalleTabuladorMre = this.idDetalleTabuladorMre;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DetalleTabuladorMrePK))
      throw new IllegalArgumentException("arg1");
    DetalleTabuladorMrePK localDetalleTabuladorMrePK = (DetalleTabuladorMrePK)paramObject;
    this.idDetalleTabuladorMre = localDetalleTabuladorMrePK.idDetalleTabuladorMre;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DetalleTabuladorMrePK))
      throw new IllegalArgumentException("arg2");
    DetalleTabuladorMrePK localDetalleTabuladorMrePK = (DetalleTabuladorMrePK)paramObject;
    localDetalleTabuladorMrePK.idDetalleTabuladorMre = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DetalleTabuladorMrePK))
      throw new IllegalArgumentException("arg2");
    DetalleTabuladorMrePK localDetalleTabuladorMrePK = (DetalleTabuladorMrePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localDetalleTabuladorMrePK.idDetalleTabuladorMre);
  }

  private static final double jdoGetasignacionMensual(DetalleTabuladorMre paramDetalleTabuladorMre)
  {
    if (paramDetalleTabuladorMre.jdoFlags <= 0)
      return paramDetalleTabuladorMre.asignacionMensual;
    StateManager localStateManager = paramDetalleTabuladorMre.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorMre.asignacionMensual;
    if (localStateManager.isLoaded(paramDetalleTabuladorMre, jdoInheritedFieldCount + 0))
      return paramDetalleTabuladorMre.asignacionMensual;
    return localStateManager.getDoubleField(paramDetalleTabuladorMre, jdoInheritedFieldCount + 0, paramDetalleTabuladorMre.asignacionMensual);
  }

  private static final void jdoSetasignacionMensual(DetalleTabuladorMre paramDetalleTabuladorMre, double paramDouble)
  {
    if (paramDetalleTabuladorMre.jdoFlags == 0)
    {
      paramDetalleTabuladorMre.asignacionMensual = paramDouble;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorMre.asignacionMensual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramDetalleTabuladorMre, jdoInheritedFieldCount + 0, paramDetalleTabuladorMre.asignacionMensual, paramDouble);
  }

  private static final Ciudad jdoGetciudad(DetalleTabuladorMre paramDetalleTabuladorMre)
  {
    StateManager localStateManager = paramDetalleTabuladorMre.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorMre.ciudad;
    if (localStateManager.isLoaded(paramDetalleTabuladorMre, jdoInheritedFieldCount + 1))
      return paramDetalleTabuladorMre.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramDetalleTabuladorMre, jdoInheritedFieldCount + 1, paramDetalleTabuladorMre.ciudad);
  }

  private static final void jdoSetciudad(DetalleTabuladorMre paramDetalleTabuladorMre, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramDetalleTabuladorMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorMre.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramDetalleTabuladorMre, jdoInheritedFieldCount + 1, paramDetalleTabuladorMre.ciudad, paramCiudad);
  }

  private static final double jdoGetfluctuacionMensual(DetalleTabuladorMre paramDetalleTabuladorMre)
  {
    if (paramDetalleTabuladorMre.jdoFlags <= 0)
      return paramDetalleTabuladorMre.fluctuacionMensual;
    StateManager localStateManager = paramDetalleTabuladorMre.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorMre.fluctuacionMensual;
    if (localStateManager.isLoaded(paramDetalleTabuladorMre, jdoInheritedFieldCount + 2))
      return paramDetalleTabuladorMre.fluctuacionMensual;
    return localStateManager.getDoubleField(paramDetalleTabuladorMre, jdoInheritedFieldCount + 2, paramDetalleTabuladorMre.fluctuacionMensual);
  }

  private static final void jdoSetfluctuacionMensual(DetalleTabuladorMre paramDetalleTabuladorMre, double paramDouble)
  {
    if (paramDetalleTabuladorMre.jdoFlags == 0)
    {
      paramDetalleTabuladorMre.fluctuacionMensual = paramDouble;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorMre.fluctuacionMensual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramDetalleTabuladorMre, jdoInheritedFieldCount + 2, paramDetalleTabuladorMre.fluctuacionMensual, paramDouble);
  }

  private static final long jdoGetidDetalleTabuladorMre(DetalleTabuladorMre paramDetalleTabuladorMre)
  {
    return paramDetalleTabuladorMre.idDetalleTabuladorMre;
  }

  private static final void jdoSetidDetalleTabuladorMre(DetalleTabuladorMre paramDetalleTabuladorMre, long paramLong)
  {
    StateManager localStateManager = paramDetalleTabuladorMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorMre.idDetalleTabuladorMre = paramLong;
      return;
    }
    localStateManager.setLongField(paramDetalleTabuladorMre, jdoInheritedFieldCount + 3, paramDetalleTabuladorMre.idDetalleTabuladorMre, paramLong);
  }

  private static final int jdoGetnivel(DetalleTabuladorMre paramDetalleTabuladorMre)
  {
    if (paramDetalleTabuladorMre.jdoFlags <= 0)
      return paramDetalleTabuladorMre.nivel;
    StateManager localStateManager = paramDetalleTabuladorMre.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorMre.nivel;
    if (localStateManager.isLoaded(paramDetalleTabuladorMre, jdoInheritedFieldCount + 4))
      return paramDetalleTabuladorMre.nivel;
    return localStateManager.getIntField(paramDetalleTabuladorMre, jdoInheritedFieldCount + 4, paramDetalleTabuladorMre.nivel);
  }

  private static final void jdoSetnivel(DetalleTabuladorMre paramDetalleTabuladorMre, int paramInt)
  {
    if (paramDetalleTabuladorMre.jdoFlags == 0)
    {
      paramDetalleTabuladorMre.nivel = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorMre.nivel = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleTabuladorMre, jdoInheritedFieldCount + 4, paramDetalleTabuladorMre.nivel, paramInt);
  }

  private static final Tabulador jdoGettabulador(DetalleTabuladorMre paramDetalleTabuladorMre)
  {
    StateManager localStateManager = paramDetalleTabuladorMre.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorMre.tabulador;
    if (localStateManager.isLoaded(paramDetalleTabuladorMre, jdoInheritedFieldCount + 5))
      return paramDetalleTabuladorMre.tabulador;
    return (Tabulador)localStateManager.getObjectField(paramDetalleTabuladorMre, jdoInheritedFieldCount + 5, paramDetalleTabuladorMre.tabulador);
  }

  private static final void jdoSettabulador(DetalleTabuladorMre paramDetalleTabuladorMre, Tabulador paramTabulador)
  {
    StateManager localStateManager = paramDetalleTabuladorMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorMre.tabulador = paramTabulador;
      return;
    }
    localStateManager.setObjectField(paramDetalleTabuladorMre, jdoInheritedFieldCount + 5, paramDetalleTabuladorMre.tabulador, paramTabulador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}