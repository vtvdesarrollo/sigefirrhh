package sigefirrhh.base.mre;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.cargo.TabuladorBeanBusiness;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;

public class DetalleTabuladorMreBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDetalleTabuladorMre(DetalleTabuladorMre detalleTabuladorMre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DetalleTabuladorMre detalleTabuladorMreNew = 
      (DetalleTabuladorMre)BeanUtils.cloneBean(
      detalleTabuladorMre);

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (detalleTabuladorMreNew.getTabulador() != null) {
      detalleTabuladorMreNew.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        detalleTabuladorMreNew.getTabulador().getIdTabulador()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (detalleTabuladorMreNew.getCiudad() != null) {
      detalleTabuladorMreNew.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        detalleTabuladorMreNew.getCiudad().getIdCiudad()));
    }
    pm.makePersistent(detalleTabuladorMreNew);
  }

  public void updateDetalleTabuladorMre(DetalleTabuladorMre detalleTabuladorMre) throws Exception
  {
    DetalleTabuladorMre detalleTabuladorMreModify = 
      findDetalleTabuladorMreById(detalleTabuladorMre.getIdDetalleTabuladorMre());

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (detalleTabuladorMre.getTabulador() != null) {
      detalleTabuladorMre.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        detalleTabuladorMre.getTabulador().getIdTabulador()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (detalleTabuladorMre.getCiudad() != null) {
      detalleTabuladorMre.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        detalleTabuladorMre.getCiudad().getIdCiudad()));
    }

    BeanUtils.copyProperties(detalleTabuladorMreModify, detalleTabuladorMre);
  }

  public void deleteDetalleTabuladorMre(DetalleTabuladorMre detalleTabuladorMre) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DetalleTabuladorMre detalleTabuladorMreDelete = 
      findDetalleTabuladorMreById(detalleTabuladorMre.getIdDetalleTabuladorMre());
    pm.deletePersistent(detalleTabuladorMreDelete);
  }

  public DetalleTabuladorMre findDetalleTabuladorMreById(long idDetalleTabuladorMre) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDetalleTabuladorMre == pIdDetalleTabuladorMre";
    Query query = pm.newQuery(DetalleTabuladorMre.class, filter);

    query.declareParameters("long pIdDetalleTabuladorMre");

    parameters.put("pIdDetalleTabuladorMre", new Long(idDetalleTabuladorMre));

    Collection colDetalleTabuladorMre = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDetalleTabuladorMre.iterator();
    return (DetalleTabuladorMre)iterator.next();
  }

  public Collection findDetalleTabuladorMreAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent detalleTabuladorMreExtent = pm.getExtent(
      DetalleTabuladorMre.class, true);
    Query query = pm.newQuery(detalleTabuladorMreExtent);
    query.setOrdering("nivel ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTabulador(long idTabulador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tabulador.idTabulador == pIdTabulador";

    Query query = pm.newQuery(DetalleTabuladorMre.class, filter);

    query.declareParameters("long pIdTabulador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTabulador", new Long(idTabulador));

    query.setOrdering("nivel ascending");

    Collection colDetalleTabuladorMre = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDetalleTabuladorMre);

    return colDetalleTabuladorMre;
  }
}