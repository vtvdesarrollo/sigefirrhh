package sigefirrhh.base.mre;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class MreFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private MreBusiness mreBusiness = new MreBusiness();

  public void addAjusteTabuladorOnu(AjusteTabuladorOnu ajusteTabuladorOnu)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.mreBusiness.addAjusteTabuladorOnu(ajusteTabuladorOnu);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAjusteTabuladorOnu(AjusteTabuladorOnu ajusteTabuladorOnu) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.updateAjusteTabuladorOnu(ajusteTabuladorOnu);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAjusteTabuladorOnu(AjusteTabuladorOnu ajusteTabuladorOnu) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.deleteAjusteTabuladorOnu(ajusteTabuladorOnu);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public AjusteTabuladorOnu findAjusteTabuladorOnuById(long ajusteTabuladorOnuId) throws Exception
  {
    try { this.txn.open();
      AjusteTabuladorOnu ajusteTabuladorOnu = 
        this.mreBusiness.findAjusteTabuladorOnuById(ajusteTabuladorOnuId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(ajusteTabuladorOnu);
      return ajusteTabuladorOnu;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAjusteTabuladorOnu() throws Exception
  {
    try { this.txn.open();
      return this.mreBusiness.findAllAjusteTabuladorOnu();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAjusteTabuladorOnuByTabulador(long idTabulador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.mreBusiness.findAjusteTabuladorOnuByTabulador(idTabulador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDetalleTabuladorMre(DetalleTabuladorMre detalleTabuladorMre)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.mreBusiness.addDetalleTabuladorMre(detalleTabuladorMre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDetalleTabuladorMre(DetalleTabuladorMre detalleTabuladorMre) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.updateDetalleTabuladorMre(detalleTabuladorMre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDetalleTabuladorMre(DetalleTabuladorMre detalleTabuladorMre) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.deleteDetalleTabuladorMre(detalleTabuladorMre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DetalleTabuladorMre findDetalleTabuladorMreById(long detalleTabuladorMreId) throws Exception
  {
    try { this.txn.open();
      DetalleTabuladorMre detalleTabuladorMre = 
        this.mreBusiness.findDetalleTabuladorMreById(detalleTabuladorMreId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(detalleTabuladorMre);
      return detalleTabuladorMre;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDetalleTabuladorMre() throws Exception
  {
    try { this.txn.open();
      return this.mreBusiness.findAllDetalleTabuladorMre();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDetalleTabuladorMreByTabulador(long idTabulador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.mreBusiness.findDetalleTabuladorMreByTabulador(idTabulador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDetalleTabuladorOnu(DetalleTabuladorOnu detalleTabuladorOnu)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.mreBusiness.addDetalleTabuladorOnu(detalleTabuladorOnu);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDetalleTabuladorOnu(DetalleTabuladorOnu detalleTabuladorOnu) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.updateDetalleTabuladorOnu(detalleTabuladorOnu);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDetalleTabuladorOnu(DetalleTabuladorOnu detalleTabuladorOnu) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.deleteDetalleTabuladorOnu(detalleTabuladorOnu);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DetalleTabuladorOnu findDetalleTabuladorOnuById(long detalleTabuladorOnuId) throws Exception
  {
    try { this.txn.open();
      DetalleTabuladorOnu detalleTabuladorOnu = 
        this.mreBusiness.findDetalleTabuladorOnuById(detalleTabuladorOnuId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(detalleTabuladorOnu);
      return detalleTabuladorOnu;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDetalleTabuladorOnu() throws Exception
  {
    try { this.txn.open();
      return this.mreBusiness.findAllDetalleTabuladorOnu();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDetalleTabuladorOnuByTabulador(long idTabulador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.mreBusiness.findDetalleTabuladorOnuByTabulador(idTabulador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSedeDiplomatica(SedeDiplomatica sedeDiplomatica)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.mreBusiness.addSedeDiplomatica(sedeDiplomatica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSedeDiplomatica(SedeDiplomatica sedeDiplomatica) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.updateSedeDiplomatica(sedeDiplomatica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSedeDiplomatica(SedeDiplomatica sedeDiplomatica) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.deleteSedeDiplomatica(sedeDiplomatica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SedeDiplomatica findSedeDiplomaticaById(long sedeDiplomaticaId) throws Exception
  {
    try { this.txn.open();
      SedeDiplomatica sedeDiplomatica = 
        this.mreBusiness.findSedeDiplomaticaById(sedeDiplomaticaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(sedeDiplomatica);
      return sedeDiplomatica;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSedeDiplomatica() throws Exception
  {
    try { this.txn.open();
      return this.mreBusiness.findAllSedeDiplomatica();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSedeDiplomaticaByCodSede(String codSede)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.mreBusiness.findSedeDiplomaticaByCodSede(codSede);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSedeDiplomaticaByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.mreBusiness.findSedeDiplomaticaByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTipoSede(TipoSede tipoSede)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.mreBusiness.addTipoSede(tipoSede);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTipoSede(TipoSede tipoSede) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.updateTipoSede(tipoSede);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTipoSede(TipoSede tipoSede) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.deleteTipoSede(tipoSede);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TipoSede findTipoSedeById(long tipoSedeId) throws Exception
  {
    try { this.txn.open();
      TipoSede tipoSede = 
        this.mreBusiness.findTipoSedeById(tipoSedeId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tipoSede);
      return tipoSede;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTipoSede() throws Exception
  {
    try { this.txn.open();
      return this.mreBusiness.findAllTipoSede();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoSedeByCodTipoSede(String codTipoSede)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.mreBusiness.findTipoSedeByCodTipoSede(codTipoSede);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTipoSedeByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.mreBusiness.findTipoSedeByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}