package sigefirrhh.base.mre;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;

public class SedeDiplomaticaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSedeDiplomatica(SedeDiplomatica sedeDiplomatica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SedeDiplomatica sedeDiplomaticaNew = 
      (SedeDiplomatica)BeanUtils.cloneBean(
      sedeDiplomatica);

    TipoSedeBeanBusiness tipoSedeBeanBusiness = new TipoSedeBeanBusiness();

    if (sedeDiplomaticaNew.getTipoSede() != null) {
      sedeDiplomaticaNew.setTipoSede(
        tipoSedeBeanBusiness.findTipoSedeById(
        sedeDiplomaticaNew.getTipoSede().getIdTipoSede()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (sedeDiplomaticaNew.getCiudad() != null) {
      sedeDiplomaticaNew.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        sedeDiplomaticaNew.getCiudad().getIdCiudad()));
    }
    pm.makePersistent(sedeDiplomaticaNew);
  }

  public void updateSedeDiplomatica(SedeDiplomatica sedeDiplomatica) throws Exception
  {
    SedeDiplomatica sedeDiplomaticaModify = 
      findSedeDiplomaticaById(sedeDiplomatica.getIdSedeDiplomatica());

    TipoSedeBeanBusiness tipoSedeBeanBusiness = new TipoSedeBeanBusiness();

    if (sedeDiplomatica.getTipoSede() != null) {
      sedeDiplomatica.setTipoSede(
        tipoSedeBeanBusiness.findTipoSedeById(
        sedeDiplomatica.getTipoSede().getIdTipoSede()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (sedeDiplomatica.getCiudad() != null) {
      sedeDiplomatica.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        sedeDiplomatica.getCiudad().getIdCiudad()));
    }

    BeanUtils.copyProperties(sedeDiplomaticaModify, sedeDiplomatica);
  }

  public void deleteSedeDiplomatica(SedeDiplomatica sedeDiplomatica) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SedeDiplomatica sedeDiplomaticaDelete = 
      findSedeDiplomaticaById(sedeDiplomatica.getIdSedeDiplomatica());
    pm.deletePersistent(sedeDiplomaticaDelete);
  }

  public SedeDiplomatica findSedeDiplomaticaById(long idSedeDiplomatica) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSedeDiplomatica == pIdSedeDiplomatica";
    Query query = pm.newQuery(SedeDiplomatica.class, filter);

    query.declareParameters("long pIdSedeDiplomatica");

    parameters.put("pIdSedeDiplomatica", new Long(idSedeDiplomatica));

    Collection colSedeDiplomatica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSedeDiplomatica.iterator();
    return (SedeDiplomatica)iterator.next();
  }

  public Collection findSedeDiplomaticaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent sedeDiplomaticaExtent = pm.getExtent(
      SedeDiplomatica.class, true);
    Query query = pm.newQuery(sedeDiplomaticaExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodSede(String codSede)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codSede == pCodSede";

    Query query = pm.newQuery(SedeDiplomatica.class, filter);

    query.declareParameters("java.lang.String pCodSede");
    HashMap parameters = new HashMap();

    parameters.put("pCodSede", new String(codSede));

    query.setOrdering("nombre ascending");

    Collection colSedeDiplomatica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSedeDiplomatica);

    return colSedeDiplomatica;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(SedeDiplomatica.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("nombre ascending");

    Collection colSedeDiplomatica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSedeDiplomatica);

    return colSedeDiplomatica;
  }
}