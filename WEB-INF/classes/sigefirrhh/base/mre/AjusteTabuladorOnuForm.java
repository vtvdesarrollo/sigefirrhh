package sigefirrhh.base.mre;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class AjusteTabuladorOnuForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(AjusteTabuladorOnuForm.class.getName());
  private AjusteTabuladorOnu ajusteTabuladorOnu;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoFacade cargoFacade = new CargoFacade();
  private MreFacade mreFacade = new MreFacade();
  private boolean showAjusteTabuladorOnuByTabulador;
  private String findSelectTabulador;
  private Collection findColTabulador;
  private Collection colTabulador;
  private String selectTabulador;
  private Object stateResultAjusteTabuladorOnuByTabulador = null;

  public String getFindSelectTabulador()
  {
    return this.findSelectTabulador;
  }
  public void setFindSelectTabulador(String valTabulador) {
    this.findSelectTabulador = valTabulador;
  }

  public Collection getFindColTabulador() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTabulador.iterator();
    Tabulador tabulador = null;
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tabulador.getIdTabulador()), 
        tabulador.toString()));
    }
    return col;
  }

  public String getSelectTabulador()
  {
    return this.selectTabulador;
  }
  public void setSelectTabulador(String valTabulador) {
    Iterator iterator = this.colTabulador.iterator();
    Tabulador tabulador = null;
    this.ajusteTabuladorOnu.setTabulador(null);
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      if (String.valueOf(tabulador.getIdTabulador()).equals(
        valTabulador)) {
        this.ajusteTabuladorOnu.setTabulador(
          tabulador);
        break;
      }
    }
    this.selectTabulador = valTabulador;
  }
  public Collection getResult() {
    return this.result;
  }

  public AjusteTabuladorOnu getAjusteTabuladorOnu() {
    if (this.ajusteTabuladorOnu == null) {
      this.ajusteTabuladorOnu = new AjusteTabuladorOnu();
    }
    return this.ajusteTabuladorOnu;
  }

  public AjusteTabuladorOnuForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTabulador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTabulador.iterator();
    Tabulador tabulador = null;
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tabulador.getIdTabulador()), 
        tabulador.toString()));
    }
    return col;
  }

  public Collection getListEstadoCivil()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = AjusteTabuladorOnu.LISTA_ESTADO_CIVIL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTabulador = 
        this.cargoFacade.findTabuladorByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTabulador = 
        this.cargoFacade.findTabuladorByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findAjusteTabuladorOnuByTabulador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.mreFacade.findAjusteTabuladorOnuByTabulador(Long.valueOf(this.findSelectTabulador).longValue());
      this.showAjusteTabuladorOnuByTabulador = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAjusteTabuladorOnuByTabulador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTabulador = null;

    return null;
  }

  public boolean isShowAjusteTabuladorOnuByTabulador() {
    return this.showAjusteTabuladorOnuByTabulador;
  }

  public String selectAjusteTabuladorOnu()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTabulador = null;

    long idAjusteTabuladorOnu = 
      Long.parseLong((String)requestParameterMap.get("idAjusteTabuladorOnu"));
    try
    {
      this.ajusteTabuladorOnu = 
        this.mreFacade.findAjusteTabuladorOnuById(
        idAjusteTabuladorOnu);
      if (this.ajusteTabuladorOnu.getTabulador() != null) {
        this.selectTabulador = 
          String.valueOf(this.ajusteTabuladorOnu.getTabulador().getIdTabulador());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.ajusteTabuladorOnu = null;
    this.showAjusteTabuladorOnuByTabulador = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.mreFacade.addAjusteTabuladorOnu(
          this.ajusteTabuladorOnu);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.mreFacade.updateAjusteTabuladorOnu(
          this.ajusteTabuladorOnu);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.mreFacade.deleteAjusteTabuladorOnu(
        this.ajusteTabuladorOnu);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.ajusteTabuladorOnu = new AjusteTabuladorOnu();

    this.selectTabulador = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.ajusteTabuladorOnu.setIdAjusteTabuladorOnu(identityGenerator.getNextSequenceNumber("sigefirrhh.base.mre.AjusteTabuladorOnu"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.ajusteTabuladorOnu = new AjusteTabuladorOnu();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}