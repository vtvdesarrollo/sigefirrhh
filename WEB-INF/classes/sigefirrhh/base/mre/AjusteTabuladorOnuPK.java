package sigefirrhh.base.mre;

import java.io.Serializable;

public class AjusteTabuladorOnuPK
  implements Serializable
{
  public long idAjusteTabuladorOnu;

  public AjusteTabuladorOnuPK()
  {
  }

  public AjusteTabuladorOnuPK(long idAjusteTabuladorOnu)
  {
    this.idAjusteTabuladorOnu = idAjusteTabuladorOnu;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AjusteTabuladorOnuPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AjusteTabuladorOnuPK thatPK)
  {
    return 
      this.idAjusteTabuladorOnu == thatPK.idAjusteTabuladorOnu;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAjusteTabuladorOnu)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAjusteTabuladorOnu);
  }
}