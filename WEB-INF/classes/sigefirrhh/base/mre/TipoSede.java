package sigefirrhh.base.mre;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TipoSede
  implements Serializable, PersistenceCapable
{
  private long idTipoSede;
  private String codTipoSede;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTipoSede", "descripcion", "idTipoSede" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + "  -  " + jdoGetcodTipoSede(this);
  }

  public String getCodTipoSede()
  {
    return jdoGetcodTipoSede(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdTipoSede()
  {
    return jdoGetidTipoSede(this);
  }

  public void setCodTipoSede(String string)
  {
    jdoSetcodTipoSede(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdTipoSede(long l)
  {
    jdoSetidTipoSede(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.mre.TipoSede"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TipoSede());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TipoSede localTipoSede = new TipoSede();
    localTipoSede.jdoFlags = 1;
    localTipoSede.jdoStateManager = paramStateManager;
    return localTipoSede;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TipoSede localTipoSede = new TipoSede();
    localTipoSede.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTipoSede.jdoFlags = 1;
    localTipoSede.jdoStateManager = paramStateManager;
    return localTipoSede;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoSede);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTipoSede);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTipoSede = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TipoSede paramTipoSede, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTipoSede == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoSede = paramTipoSede.codTipoSede;
      return;
    case 1:
      if (paramTipoSede == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramTipoSede.descripcion;
      return;
    case 2:
      if (paramTipoSede == null)
        throw new IllegalArgumentException("arg1");
      this.idTipoSede = paramTipoSede.idTipoSede;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TipoSede))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TipoSede localTipoSede = (TipoSede)paramObject;
    if (localTipoSede.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTipoSede, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TipoSedePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TipoSedePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoSedePK))
      throw new IllegalArgumentException("arg1");
    TipoSedePK localTipoSedePK = (TipoSedePK)paramObject;
    localTipoSedePK.idTipoSede = this.idTipoSede;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TipoSedePK))
      throw new IllegalArgumentException("arg1");
    TipoSedePK localTipoSedePK = (TipoSedePK)paramObject;
    this.idTipoSede = localTipoSedePK.idTipoSede;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoSedePK))
      throw new IllegalArgumentException("arg2");
    TipoSedePK localTipoSedePK = (TipoSedePK)paramObject;
    localTipoSedePK.idTipoSede = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TipoSedePK))
      throw new IllegalArgumentException("arg2");
    TipoSedePK localTipoSedePK = (TipoSedePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localTipoSedePK.idTipoSede);
  }

  private static final String jdoGetcodTipoSede(TipoSede paramTipoSede)
  {
    if (paramTipoSede.jdoFlags <= 0)
      return paramTipoSede.codTipoSede;
    StateManager localStateManager = paramTipoSede.jdoStateManager;
    if (localStateManager == null)
      return paramTipoSede.codTipoSede;
    if (localStateManager.isLoaded(paramTipoSede, jdoInheritedFieldCount + 0))
      return paramTipoSede.codTipoSede;
    return localStateManager.getStringField(paramTipoSede, jdoInheritedFieldCount + 0, paramTipoSede.codTipoSede);
  }

  private static final void jdoSetcodTipoSede(TipoSede paramTipoSede, String paramString)
  {
    if (paramTipoSede.jdoFlags == 0)
    {
      paramTipoSede.codTipoSede = paramString;
      return;
    }
    StateManager localStateManager = paramTipoSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoSede.codTipoSede = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoSede, jdoInheritedFieldCount + 0, paramTipoSede.codTipoSede, paramString);
  }

  private static final String jdoGetdescripcion(TipoSede paramTipoSede)
  {
    if (paramTipoSede.jdoFlags <= 0)
      return paramTipoSede.descripcion;
    StateManager localStateManager = paramTipoSede.jdoStateManager;
    if (localStateManager == null)
      return paramTipoSede.descripcion;
    if (localStateManager.isLoaded(paramTipoSede, jdoInheritedFieldCount + 1))
      return paramTipoSede.descripcion;
    return localStateManager.getStringField(paramTipoSede, jdoInheritedFieldCount + 1, paramTipoSede.descripcion);
  }

  private static final void jdoSetdescripcion(TipoSede paramTipoSede, String paramString)
  {
    if (paramTipoSede.jdoFlags == 0)
    {
      paramTipoSede.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramTipoSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoSede.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramTipoSede, jdoInheritedFieldCount + 1, paramTipoSede.descripcion, paramString);
  }

  private static final long jdoGetidTipoSede(TipoSede paramTipoSede)
  {
    return paramTipoSede.idTipoSede;
  }

  private static final void jdoSetidTipoSede(TipoSede paramTipoSede, long paramLong)
  {
    StateManager localStateManager = paramTipoSede.jdoStateManager;
    if (localStateManager == null)
    {
      paramTipoSede.idTipoSede = paramLong;
      return;
    }
    localStateManager.setLongField(paramTipoSede, jdoInheritedFieldCount + 2, paramTipoSede.idTipoSede, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}