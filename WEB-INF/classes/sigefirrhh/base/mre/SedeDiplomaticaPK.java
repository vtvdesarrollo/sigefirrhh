package sigefirrhh.base.mre;

import java.io.Serializable;

public class SedeDiplomaticaPK
  implements Serializable
{
  public long idSedeDiplomatica;

  public SedeDiplomaticaPK()
  {
  }

  public SedeDiplomaticaPK(long idSedeDiplomatica)
  {
    this.idSedeDiplomatica = idSedeDiplomatica;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SedeDiplomaticaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SedeDiplomaticaPK thatPK)
  {
    return 
      this.idSedeDiplomatica == thatPK.idSedeDiplomatica;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSedeDiplomatica)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSedeDiplomatica);
  }
}