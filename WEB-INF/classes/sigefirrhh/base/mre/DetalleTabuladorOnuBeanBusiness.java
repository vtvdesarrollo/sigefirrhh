package sigefirrhh.base.mre;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.cargo.TabuladorBeanBusiness;

public class DetalleTabuladorOnuBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDetalleTabuladorOnu(DetalleTabuladorOnu detalleTabuladorOnu)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DetalleTabuladorOnu detalleTabuladorOnuNew = 
      (DetalleTabuladorOnu)BeanUtils.cloneBean(
      detalleTabuladorOnu);

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (detalleTabuladorOnuNew.getTabulador() != null) {
      detalleTabuladorOnuNew.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        detalleTabuladorOnuNew.getTabulador().getIdTabulador()));
    }
    pm.makePersistent(detalleTabuladorOnuNew);
  }

  public void updateDetalleTabuladorOnu(DetalleTabuladorOnu detalleTabuladorOnu) throws Exception
  {
    DetalleTabuladorOnu detalleTabuladorOnuModify = 
      findDetalleTabuladorOnuById(detalleTabuladorOnu.getIdDetalleTabuladorOnu());

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (detalleTabuladorOnu.getTabulador() != null) {
      detalleTabuladorOnu.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        detalleTabuladorOnu.getTabulador().getIdTabulador()));
    }

    BeanUtils.copyProperties(detalleTabuladorOnuModify, detalleTabuladorOnu);
  }

  public void deleteDetalleTabuladorOnu(DetalleTabuladorOnu detalleTabuladorOnu) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DetalleTabuladorOnu detalleTabuladorOnuDelete = 
      findDetalleTabuladorOnuById(detalleTabuladorOnu.getIdDetalleTabuladorOnu());
    pm.deletePersistent(detalleTabuladorOnuDelete);
  }

  public DetalleTabuladorOnu findDetalleTabuladorOnuById(long idDetalleTabuladorOnu) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDetalleTabuladorOnu == pIdDetalleTabuladorOnu";
    Query query = pm.newQuery(DetalleTabuladorOnu.class, filter);

    query.declareParameters("long pIdDetalleTabuladorOnu");

    parameters.put("pIdDetalleTabuladorOnu", new Long(idDetalleTabuladorOnu));

    Collection colDetalleTabuladorOnu = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDetalleTabuladorOnu.iterator();
    return (DetalleTabuladorOnu)iterator.next();
  }

  public Collection findDetalleTabuladorOnuAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent detalleTabuladorOnuExtent = pm.getExtent(
      DetalleTabuladorOnu.class, true);
    Query query = pm.newQuery(detalleTabuladorOnuExtent);
    query.setOrdering("nivel ascending, aniosServicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTabulador(long idTabulador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tabulador.idTabulador == pIdTabulador";

    Query query = pm.newQuery(DetalleTabuladorOnu.class, filter);

    query.declareParameters("long pIdTabulador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTabulador", new Long(idTabulador));

    query.setOrdering("nivel ascending, aniosServicio ascending");

    Collection colDetalleTabuladorOnu = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDetalleTabuladorOnu);

    return colDetalleTabuladorOnu;
  }
}