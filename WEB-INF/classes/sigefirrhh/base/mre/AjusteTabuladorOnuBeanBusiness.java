package sigefirrhh.base.mre;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.cargo.TabuladorBeanBusiness;

public class AjusteTabuladorOnuBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAjusteTabuladorOnu(AjusteTabuladorOnu ajusteTabuladorOnu)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    AjusteTabuladorOnu ajusteTabuladorOnuNew = 
      (AjusteTabuladorOnu)BeanUtils.cloneBean(
      ajusteTabuladorOnu);

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (ajusteTabuladorOnuNew.getTabulador() != null) {
      ajusteTabuladorOnuNew.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        ajusteTabuladorOnuNew.getTabulador().getIdTabulador()));
    }
    pm.makePersistent(ajusteTabuladorOnuNew);
  }

  public void updateAjusteTabuladorOnu(AjusteTabuladorOnu ajusteTabuladorOnu) throws Exception
  {
    AjusteTabuladorOnu ajusteTabuladorOnuModify = 
      findAjusteTabuladorOnuById(ajusteTabuladorOnu.getIdAjusteTabuladorOnu());

    TabuladorBeanBusiness tabuladorBeanBusiness = new TabuladorBeanBusiness();

    if (ajusteTabuladorOnu.getTabulador() != null) {
      ajusteTabuladorOnu.setTabulador(
        tabuladorBeanBusiness.findTabuladorById(
        ajusteTabuladorOnu.getTabulador().getIdTabulador()));
    }

    BeanUtils.copyProperties(ajusteTabuladorOnuModify, ajusteTabuladorOnu);
  }

  public void deleteAjusteTabuladorOnu(AjusteTabuladorOnu ajusteTabuladorOnu) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    AjusteTabuladorOnu ajusteTabuladorOnuDelete = 
      findAjusteTabuladorOnuById(ajusteTabuladorOnu.getIdAjusteTabuladorOnu());
    pm.deletePersistent(ajusteTabuladorOnuDelete);
  }

  public AjusteTabuladorOnu findAjusteTabuladorOnuById(long idAjusteTabuladorOnu) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAjusteTabuladorOnu == pIdAjusteTabuladorOnu";
    Query query = pm.newQuery(AjusteTabuladorOnu.class, filter);

    query.declareParameters("long pIdAjusteTabuladorOnu");

    parameters.put("pIdAjusteTabuladorOnu", new Long(idAjusteTabuladorOnu));

    Collection colAjusteTabuladorOnu = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAjusteTabuladorOnu.iterator();
    return (AjusteTabuladorOnu)iterator.next();
  }

  public Collection findAjusteTabuladorOnuAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent ajusteTabuladorOnuExtent = pm.getExtent(
      AjusteTabuladorOnu.class, true);
    Query query = pm.newQuery(ajusteTabuladorOnuExtent);
    query.setOrdering("nivel ascending, aniosServicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTabulador(long idTabulador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tabulador.idTabulador == pIdTabulador";

    Query query = pm.newQuery(AjusteTabuladorOnu.class, filter);

    query.declareParameters("long pIdTabulador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTabulador", new Long(idTabulador));

    query.setOrdering("nivel ascending, aniosServicio ascending");

    Collection colAjusteTabuladorOnu = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAjusteTabuladorOnu);

    return colAjusteTabuladorOnu;
  }
}