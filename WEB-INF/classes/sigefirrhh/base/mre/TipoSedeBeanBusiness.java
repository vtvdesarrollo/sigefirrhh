package sigefirrhh.base.mre;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class TipoSedeBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTipoSede(TipoSede tipoSede)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TipoSede tipoSedeNew = 
      (TipoSede)BeanUtils.cloneBean(
      tipoSede);

    pm.makePersistent(tipoSedeNew);
  }

  public void updateTipoSede(TipoSede tipoSede) throws Exception
  {
    TipoSede tipoSedeModify = 
      findTipoSedeById(tipoSede.getIdTipoSede());

    BeanUtils.copyProperties(tipoSedeModify, tipoSede);
  }

  public void deleteTipoSede(TipoSede tipoSede) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TipoSede tipoSedeDelete = 
      findTipoSedeById(tipoSede.getIdTipoSede());
    pm.deletePersistent(tipoSedeDelete);
  }

  public TipoSede findTipoSedeById(long idTipoSede) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTipoSede == pIdTipoSede";
    Query query = pm.newQuery(TipoSede.class, filter);

    query.declareParameters("long pIdTipoSede");

    parameters.put("pIdTipoSede", new Long(idTipoSede));

    Collection colTipoSede = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTipoSede.iterator();
    return (TipoSede)iterator.next();
  }

  public Collection findTipoSedeAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent tipoSedeExtent = pm.getExtent(
      TipoSede.class, true);
    Query query = pm.newQuery(tipoSedeExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodTipoSede(String codTipoSede)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codTipoSede == pCodTipoSede";

    Query query = pm.newQuery(TipoSede.class, filter);

    query.declareParameters("java.lang.String pCodTipoSede");
    HashMap parameters = new HashMap();

    parameters.put("pCodTipoSede", new String(codTipoSede));

    query.setOrdering("descripcion ascending");

    Collection colTipoSede = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoSede);

    return colTipoSede;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(TipoSede.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("descripcion ascending");

    Collection colTipoSede = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTipoSede);

    return colTipoSede;
  }
}