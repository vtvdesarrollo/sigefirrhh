package sigefirrhh.base.mre;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;

public class DetalleTabuladorMreForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(DetalleTabuladorMreForm.class.getName());
  private DetalleTabuladorMre detalleTabuladorMre;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoFacade cargoFacade = new CargoFacade();
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private MreFacade mreFacade = new MreFacade();
  private boolean showDetalleTabuladorMreByTabulador;
  private String findSelectTabulador;
  private Collection findColTabulador;
  private Collection colTabulador;
  private Collection colPaisForCiudad;
  private Collection colEstadoForCiudad;
  private Collection colCiudad;
  private String selectTabulador;
  private String selectPaisForCiudad;
  private String selectEstadoForCiudad;
  private String selectCiudad;
  private Object stateResultDetalleTabuladorMreByTabulador = null;

  public String getFindSelectTabulador()
  {
    return this.findSelectTabulador;
  }
  public void setFindSelectTabulador(String valTabulador) {
    this.findSelectTabulador = valTabulador;
  }

  public Collection getFindColTabulador() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTabulador.iterator();
    Tabulador tabulador = null;
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tabulador.getIdTabulador()), 
        tabulador.toString()));
    }
    return col;
  }

  public String getSelectTabulador()
  {
    return this.selectTabulador;
  }
  public void setSelectTabulador(String valTabulador) {
    Iterator iterator = this.colTabulador.iterator();
    Tabulador tabulador = null;
    this.detalleTabuladorMre.setTabulador(null);
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      if (String.valueOf(tabulador.getIdTabulador()).equals(
        valTabulador)) {
        this.detalleTabuladorMre.setTabulador(
          tabulador);
        break;
      }
    }
    this.selectTabulador = valTabulador;
  }
  public String getSelectPaisForCiudad() {
    return this.selectPaisForCiudad;
  }
  public void setSelectPaisForCiudad(String valPaisForCiudad) {
    this.selectPaisForCiudad = valPaisForCiudad;
  }
  public void changePaisForCiudad(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      this.colEstadoForCiudad = null;
      if (idPais > 0L) {
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
      } else {
        this.selectCiudad = null;
        this.detalleTabuladorMre.setCiudad(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudad = null;
      this.detalleTabuladorMre.setCiudad(
        null);
    }
  }

  public boolean isShowPaisForCiudad() { return this.colPaisForCiudad != null; }

  public String getSelectEstadoForCiudad() {
    return this.selectEstadoForCiudad;
  }
  public void setSelectEstadoForCiudad(String valEstadoForCiudad) {
    this.selectEstadoForCiudad = valEstadoForCiudad;
  }
  public void changeEstadoForCiudad(ValueChangeEvent event) {
    long idEstado = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      if (idEstado > 0L) {
        this.colCiudad = 
          this.ubicacionFacade.findCiudadByEstado(
          idEstado);
      } else {
        this.selectCiudad = null;
        this.detalleTabuladorMre.setCiudad(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudad = null;
      this.detalleTabuladorMre.setCiudad(
        null);
    }
  }

  public boolean isShowEstadoForCiudad() { return this.colEstadoForCiudad != null; }

  public String getSelectCiudad() {
    return this.selectCiudad;
  }
  public void setSelectCiudad(String valCiudad) {
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    this.detalleTabuladorMre.setCiudad(null);
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      if (String.valueOf(ciudad.getIdCiudad()).equals(
        valCiudad)) {
        this.detalleTabuladorMre.setCiudad(
          ciudad);
        break;
      }
    }
    this.selectCiudad = valCiudad;
  }
  public boolean isShowCiudad() {
    return this.colCiudad != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public DetalleTabuladorMre getDetalleTabuladorMre() {
    if (this.detalleTabuladorMre == null) {
      this.detalleTabuladorMre = new DetalleTabuladorMre();
    }
    return this.detalleTabuladorMre;
  }

  public DetalleTabuladorMreForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTabulador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTabulador.iterator();
    Tabulador tabulador = null;
    while (iterator.hasNext()) {
      tabulador = (Tabulador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tabulador.getIdTabulador()), 
        tabulador.toString()));
    }
    return col;
  }

  public Collection getColPaisForCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForCiudad.iterator();
    Pais paisForCiudad = null;
    while (iterator.hasNext()) {
      paisForCiudad = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForCiudad.getIdPais()), 
        paisForCiudad.toString()));
    }
    return col;
  }

  public Collection getColEstadoForCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForCiudad.iterator();
    Estado estadoForCiudad = null;
    while (iterator.hasNext()) {
      estadoForCiudad = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estadoForCiudad.getIdEstado()), 
        estadoForCiudad.toString()));
    }
    return col;
  }

  public Collection getColCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ciudad.getIdCiudad()), 
        ciudad.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTabulador = 
        this.cargoFacade.findTabuladorByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTabulador = 
        this.cargoFacade.findTabuladorByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colPaisForCiudad = 
        this.ubicacionFacade.findAllPais();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findDetalleTabuladorMreByTabulador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.mreFacade.findDetalleTabuladorMreByTabulador(Long.valueOf(this.findSelectTabulador).longValue());
      this.showDetalleTabuladorMreByTabulador = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDetalleTabuladorMreByTabulador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTabulador = null;

    return null;
  }

  public boolean isShowDetalleTabuladorMreByTabulador() {
    return this.showDetalleTabuladorMreByTabulador;
  }

  public String selectDetalleTabuladorMre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTabulador = null;
    this.selectCiudad = null;
    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    long idDetalleTabuladorMre = 
      Long.parseLong((String)requestParameterMap.get("idDetalleTabuladorMre"));
    try
    {
      this.detalleTabuladorMre = 
        this.mreFacade.findDetalleTabuladorMreById(
        idDetalleTabuladorMre);
      if (this.detalleTabuladorMre.getTabulador() != null) {
        this.selectTabulador = 
          String.valueOf(this.detalleTabuladorMre.getTabulador().getIdTabulador());
      }
      if (this.detalleTabuladorMre.getCiudad() != null) {
        this.selectCiudad = 
          String.valueOf(this.detalleTabuladorMre.getCiudad().getIdCiudad());
      }

      Ciudad ciudad = null;
      Estado estadoForCiudad = null;
      Pais paisForCiudad = null;

      if (this.detalleTabuladorMre.getCiudad() != null) {
        long idCiudad = 
          this.detalleTabuladorMre.getCiudad().getIdCiudad();
        this.selectCiudad = String.valueOf(idCiudad);
        ciudad = this.ubicacionFacade.findCiudadById(
          idCiudad);
        this.colCiudad = this.ubicacionFacade.findCiudadByEstado(
          ciudad.getEstado().getIdEstado());

        long idEstadoForCiudad = 
          this.detalleTabuladorMre.getCiudad().getEstado().getIdEstado();
        this.selectEstadoForCiudad = String.valueOf(idEstadoForCiudad);
        estadoForCiudad = 
          this.ubicacionFacade.findEstadoById(
          idEstadoForCiudad);
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          estadoForCiudad.getPais().getIdPais());

        long idPaisForCiudad = 
          estadoForCiudad.getPais().getIdPais();
        this.selectPaisForCiudad = String.valueOf(idPaisForCiudad);
        paisForCiudad = 
          this.ubicacionFacade.findPaisById(
          idPaisForCiudad);
        this.colPaisForCiudad = 
          this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.detalleTabuladorMre = null;
    this.showDetalleTabuladorMreByTabulador = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.mreFacade.addDetalleTabuladorMre(
          this.detalleTabuladorMre);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.mreFacade.updateDetalleTabuladorMre(
          this.detalleTabuladorMre);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.mreFacade.deleteDetalleTabuladorMre(
        this.detalleTabuladorMre);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.detalleTabuladorMre = new DetalleTabuladorMre();

    this.selectTabulador = null;

    this.selectCiudad = null;

    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.detalleTabuladorMre.setIdDetalleTabuladorMre(identityGenerator.getNextSequenceNumber("sigefirrhh.base.mre.DetalleTabuladorMre"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.detalleTabuladorMre = new DetalleTabuladorMre();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}