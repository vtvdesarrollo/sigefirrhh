package sigefirrhh.base.mre;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;

public class SedeDiplomaticaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SedeDiplomaticaForm.class.getName());
  private SedeDiplomatica sedeDiplomatica;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private MreFacade mreFacade = new MreFacade();
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private boolean showSedeDiplomaticaByCodSede;
  private boolean showSedeDiplomaticaByNombre;
  private String findCodSede;
  private String findNombre;
  private Collection colTipoSede;
  private Collection colPaisForCiudad;
  private Collection colEstadoForCiudad;
  private Collection colCiudad;
  private String selectTipoSede;
  private String selectPaisForCiudad;
  private String selectEstadoForCiudad;
  private String selectCiudad;
  private Object stateResultSedeDiplomaticaByCodSede = null;

  private Object stateResultSedeDiplomaticaByNombre = null;

  public String getFindCodSede()
  {
    return this.findCodSede;
  }
  public void setFindCodSede(String findCodSede) {
    this.findCodSede = findCodSede;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public String getSelectTipoSede()
  {
    return this.selectTipoSede;
  }
  public void setSelectTipoSede(String valTipoSede) {
    Iterator iterator = this.colTipoSede.iterator();
    TipoSede tipoSede = null;
    this.sedeDiplomatica.setTipoSede(null);
    while (iterator.hasNext()) {
      tipoSede = (TipoSede)iterator.next();
      if (String.valueOf(tipoSede.getIdTipoSede()).equals(
        valTipoSede)) {
        this.sedeDiplomatica.setTipoSede(
          tipoSede);
        break;
      }
    }
    this.selectTipoSede = valTipoSede;
  }
  public String getSelectPaisForCiudad() {
    return this.selectPaisForCiudad;
  }
  public void setSelectPaisForCiudad(String valPaisForCiudad) {
    this.selectPaisForCiudad = valPaisForCiudad;
  }
  public void changePaisForCiudad(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      this.colEstadoForCiudad = null;
      if (idPais > 0L) {
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
      } else {
        this.selectCiudad = null;
        this.sedeDiplomatica.setCiudad(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudad = null;
      this.sedeDiplomatica.setCiudad(
        null);
    }
  }

  public boolean isShowPaisForCiudad() { return this.colPaisForCiudad != null; }

  public String getSelectEstadoForCiudad() {
    return this.selectEstadoForCiudad;
  }
  public void setSelectEstadoForCiudad(String valEstadoForCiudad) {
    this.selectEstadoForCiudad = valEstadoForCiudad;
  }
  public void changeEstadoForCiudad(ValueChangeEvent event) {
    long idEstado = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      if (idEstado > 0L) {
        this.colCiudad = 
          this.ubicacionFacade.findCiudadByEstado(
          idEstado);
      } else {
        this.selectCiudad = null;
        this.sedeDiplomatica.setCiudad(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudad = null;
      this.sedeDiplomatica.setCiudad(
        null);
    }
  }

  public boolean isShowEstadoForCiudad() { return this.colEstadoForCiudad != null; }

  public String getSelectCiudad() {
    return this.selectCiudad;
  }
  public void setSelectCiudad(String valCiudad) {
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    this.sedeDiplomatica.setCiudad(null);
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      if (String.valueOf(ciudad.getIdCiudad()).equals(
        valCiudad)) {
        this.sedeDiplomatica.setCiudad(
          ciudad);
        break;
      }
    }
    this.selectCiudad = valCiudad;
  }
  public boolean isShowCiudad() {
    return this.colCiudad != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public SedeDiplomatica getSedeDiplomatica() {
    if (this.sedeDiplomatica == null) {
      this.sedeDiplomatica = new SedeDiplomatica();
    }
    return this.sedeDiplomatica;
  }

  public SedeDiplomaticaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoSede()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoSede.iterator();
    TipoSede tipoSede = null;
    while (iterator.hasNext()) {
      tipoSede = (TipoSede)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoSede.getIdTipoSede()), 
        tipoSede.toString()));
    }
    return col;
  }

  public Collection getColPaisForCiudad() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForCiudad.iterator();
    Pais paisForCiudad = null;
    while (iterator.hasNext()) {
      paisForCiudad = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForCiudad.getIdPais()), 
        paisForCiudad.toString()));
    }
    return col;
  }

  public Collection getColEstadoForCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForCiudad.iterator();
    Estado estadoForCiudad = null;
    while (iterator.hasNext()) {
      estadoForCiudad = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estadoForCiudad.getIdEstado()), 
        estadoForCiudad.toString()));
    }
    return col;
  }

  public Collection getColCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ciudad.getIdCiudad()), 
        ciudad.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colTipoSede = 
        this.mreFacade.findAllTipoSede();
      this.colPaisForCiudad = 
        this.ubicacionFacade.findAllPais();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSedeDiplomaticaByCodSede()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.mreFacade.findSedeDiplomaticaByCodSede(this.findCodSede);
      this.showSedeDiplomaticaByCodSede = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSedeDiplomaticaByCodSede)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodSede = null;
    this.findNombre = null;

    return null;
  }

  public String findSedeDiplomaticaByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.mreFacade.findSedeDiplomaticaByNombre(this.findNombre);
      this.showSedeDiplomaticaByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSedeDiplomaticaByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodSede = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowSedeDiplomaticaByCodSede() {
    return this.showSedeDiplomaticaByCodSede;
  }
  public boolean isShowSedeDiplomaticaByNombre() {
    return this.showSedeDiplomaticaByNombre;
  }

  public String selectSedeDiplomatica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoSede = null;
    this.selectCiudad = null;
    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    long idSedeDiplomatica = 
      Long.parseLong((String)requestParameterMap.get("idSedeDiplomatica"));
    try
    {
      this.sedeDiplomatica = 
        this.mreFacade.findSedeDiplomaticaById(
        idSedeDiplomatica);
      if (this.sedeDiplomatica.getTipoSede() != null) {
        this.selectTipoSede = 
          String.valueOf(this.sedeDiplomatica.getTipoSede().getIdTipoSede());
      }
      if (this.sedeDiplomatica.getCiudad() != null) {
        this.selectCiudad = 
          String.valueOf(this.sedeDiplomatica.getCiudad().getIdCiudad());
      }

      Ciudad ciudad = null;
      Estado estadoForCiudad = null;
      Pais paisForCiudad = null;

      if (this.sedeDiplomatica.getCiudad() != null) {
        long idCiudad = 
          this.sedeDiplomatica.getCiudad().getIdCiudad();
        this.selectCiudad = String.valueOf(idCiudad);
        ciudad = this.ubicacionFacade.findCiudadById(
          idCiudad);
        this.colCiudad = this.ubicacionFacade.findCiudadByEstado(
          ciudad.getEstado().getIdEstado());

        long idEstadoForCiudad = 
          this.sedeDiplomatica.getCiudad().getEstado().getIdEstado();
        this.selectEstadoForCiudad = String.valueOf(idEstadoForCiudad);
        estadoForCiudad = 
          this.ubicacionFacade.findEstadoById(
          idEstadoForCiudad);
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          estadoForCiudad.getPais().getIdPais());

        long idPaisForCiudad = 
          estadoForCiudad.getPais().getIdPais();
        this.selectPaisForCiudad = String.valueOf(idPaisForCiudad);
        paisForCiudad = 
          this.ubicacionFacade.findPaisById(
          idPaisForCiudad);
        this.colPaisForCiudad = 
          this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.sedeDiplomatica = null;
    this.showSedeDiplomaticaByCodSede = false;
    this.showSedeDiplomaticaByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.mreFacade.addSedeDiplomatica(
          this.sedeDiplomatica);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.mreFacade.updateSedeDiplomatica(
          this.sedeDiplomatica);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.mreFacade.deleteSedeDiplomatica(
        this.sedeDiplomatica);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.sedeDiplomatica = new SedeDiplomatica();

    this.selectTipoSede = null;

    this.selectCiudad = null;

    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.sedeDiplomatica.setIdSedeDiplomatica(identityGenerator.getNextSequenceNumber("sigefirrhh.base.mre.SedeDiplomatica"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.sedeDiplomatica = new SedeDiplomatica();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}