package sigefirrhh.base.mre;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class MreBusiness extends AbstractBusiness
  implements Serializable
{
  private AjusteTabuladorOnuBeanBusiness ajusteTabuladorOnuBeanBusiness = new AjusteTabuladorOnuBeanBusiness();

  private DetalleTabuladorMreBeanBusiness detalleTabuladorMreBeanBusiness = new DetalleTabuladorMreBeanBusiness();

  private DetalleTabuladorOnuBeanBusiness detalleTabuladorOnuBeanBusiness = new DetalleTabuladorOnuBeanBusiness();

  private SedeDiplomaticaBeanBusiness sedeDiplomaticaBeanBusiness = new SedeDiplomaticaBeanBusiness();

  private TipoSedeBeanBusiness tipoSedeBeanBusiness = new TipoSedeBeanBusiness();

  public void addAjusteTabuladorOnu(AjusteTabuladorOnu ajusteTabuladorOnu)
    throws Exception
  {
    this.ajusteTabuladorOnuBeanBusiness.addAjusteTabuladorOnu(ajusteTabuladorOnu);
  }

  public void updateAjusteTabuladorOnu(AjusteTabuladorOnu ajusteTabuladorOnu) throws Exception {
    this.ajusteTabuladorOnuBeanBusiness.updateAjusteTabuladorOnu(ajusteTabuladorOnu);
  }

  public void deleteAjusteTabuladorOnu(AjusteTabuladorOnu ajusteTabuladorOnu) throws Exception {
    this.ajusteTabuladorOnuBeanBusiness.deleteAjusteTabuladorOnu(ajusteTabuladorOnu);
  }

  public AjusteTabuladorOnu findAjusteTabuladorOnuById(long ajusteTabuladorOnuId) throws Exception {
    return this.ajusteTabuladorOnuBeanBusiness.findAjusteTabuladorOnuById(ajusteTabuladorOnuId);
  }

  public Collection findAllAjusteTabuladorOnu() throws Exception {
    return this.ajusteTabuladorOnuBeanBusiness.findAjusteTabuladorOnuAll();
  }

  public Collection findAjusteTabuladorOnuByTabulador(long idTabulador)
    throws Exception
  {
    return this.ajusteTabuladorOnuBeanBusiness.findByTabulador(idTabulador);
  }

  public void addDetalleTabuladorMre(DetalleTabuladorMre detalleTabuladorMre)
    throws Exception
  {
    this.detalleTabuladorMreBeanBusiness.addDetalleTabuladorMre(detalleTabuladorMre);
  }

  public void updateDetalleTabuladorMre(DetalleTabuladorMre detalleTabuladorMre) throws Exception {
    this.detalleTabuladorMreBeanBusiness.updateDetalleTabuladorMre(detalleTabuladorMre);
  }

  public void deleteDetalleTabuladorMre(DetalleTabuladorMre detalleTabuladorMre) throws Exception {
    this.detalleTabuladorMreBeanBusiness.deleteDetalleTabuladorMre(detalleTabuladorMre);
  }

  public DetalleTabuladorMre findDetalleTabuladorMreById(long detalleTabuladorMreId) throws Exception {
    return this.detalleTabuladorMreBeanBusiness.findDetalleTabuladorMreById(detalleTabuladorMreId);
  }

  public Collection findAllDetalleTabuladorMre() throws Exception {
    return this.detalleTabuladorMreBeanBusiness.findDetalleTabuladorMreAll();
  }

  public Collection findDetalleTabuladorMreByTabulador(long idTabulador)
    throws Exception
  {
    return this.detalleTabuladorMreBeanBusiness.findByTabulador(idTabulador);
  }

  public void addDetalleTabuladorOnu(DetalleTabuladorOnu detalleTabuladorOnu)
    throws Exception
  {
    this.detalleTabuladorOnuBeanBusiness.addDetalleTabuladorOnu(detalleTabuladorOnu);
  }

  public void updateDetalleTabuladorOnu(DetalleTabuladorOnu detalleTabuladorOnu) throws Exception {
    this.detalleTabuladorOnuBeanBusiness.updateDetalleTabuladorOnu(detalleTabuladorOnu);
  }

  public void deleteDetalleTabuladorOnu(DetalleTabuladorOnu detalleTabuladorOnu) throws Exception {
    this.detalleTabuladorOnuBeanBusiness.deleteDetalleTabuladorOnu(detalleTabuladorOnu);
  }

  public DetalleTabuladorOnu findDetalleTabuladorOnuById(long detalleTabuladorOnuId) throws Exception {
    return this.detalleTabuladorOnuBeanBusiness.findDetalleTabuladorOnuById(detalleTabuladorOnuId);
  }

  public Collection findAllDetalleTabuladorOnu() throws Exception {
    return this.detalleTabuladorOnuBeanBusiness.findDetalleTabuladorOnuAll();
  }

  public Collection findDetalleTabuladorOnuByTabulador(long idTabulador)
    throws Exception
  {
    return this.detalleTabuladorOnuBeanBusiness.findByTabulador(idTabulador);
  }

  public void addSedeDiplomatica(SedeDiplomatica sedeDiplomatica)
    throws Exception
  {
    this.sedeDiplomaticaBeanBusiness.addSedeDiplomatica(sedeDiplomatica);
  }

  public void updateSedeDiplomatica(SedeDiplomatica sedeDiplomatica) throws Exception {
    this.sedeDiplomaticaBeanBusiness.updateSedeDiplomatica(sedeDiplomatica);
  }

  public void deleteSedeDiplomatica(SedeDiplomatica sedeDiplomatica) throws Exception {
    this.sedeDiplomaticaBeanBusiness.deleteSedeDiplomatica(sedeDiplomatica);
  }

  public SedeDiplomatica findSedeDiplomaticaById(long sedeDiplomaticaId) throws Exception {
    return this.sedeDiplomaticaBeanBusiness.findSedeDiplomaticaById(sedeDiplomaticaId);
  }

  public Collection findAllSedeDiplomatica() throws Exception {
    return this.sedeDiplomaticaBeanBusiness.findSedeDiplomaticaAll();
  }

  public Collection findSedeDiplomaticaByCodSede(String codSede)
    throws Exception
  {
    return this.sedeDiplomaticaBeanBusiness.findByCodSede(codSede);
  }

  public Collection findSedeDiplomaticaByNombre(String nombre)
    throws Exception
  {
    return this.sedeDiplomaticaBeanBusiness.findByNombre(nombre);
  }

  public void addTipoSede(TipoSede tipoSede)
    throws Exception
  {
    this.tipoSedeBeanBusiness.addTipoSede(tipoSede);
  }

  public void updateTipoSede(TipoSede tipoSede) throws Exception {
    this.tipoSedeBeanBusiness.updateTipoSede(tipoSede);
  }

  public void deleteTipoSede(TipoSede tipoSede) throws Exception {
    this.tipoSedeBeanBusiness.deleteTipoSede(tipoSede);
  }

  public TipoSede findTipoSedeById(long tipoSedeId) throws Exception {
    return this.tipoSedeBeanBusiness.findTipoSedeById(tipoSedeId);
  }

  public Collection findAllTipoSede() throws Exception {
    return this.tipoSedeBeanBusiness.findTipoSedeAll();
  }

  public Collection findTipoSedeByCodTipoSede(String codTipoSede)
    throws Exception
  {
    return this.tipoSedeBeanBusiness.findByCodTipoSede(codTipoSede);
  }

  public Collection findTipoSedeByDescripcion(String descripcion)
    throws Exception
  {
    return this.tipoSedeBeanBusiness.findByDescripcion(descripcion);
  }
}