package sigefirrhh.base.mre;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Tabulador;

public class DetalleTabuladorOnu
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTADO_CIVIL;
  private long idDetalleTabuladorOnu;
  private Tabulador tabulador;
  private int nivel;
  private String estadoCivil;
  private int aniosServicio;
  private double asignacionAnual;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aniosServicio", "asignacionAnual", "estadoCivil", "idDetalleTabuladorOnu", "nivel", "tabulador" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Tabulador") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.mre.DetalleTabuladorOnu"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DetalleTabuladorOnu());

    LISTA_ESTADO_CIVIL = 
      new LinkedHashMap();

    LISTA_ESTADO_CIVIL.put("S", "SOLTERO(A)");
    LISTA_ESTADO_CIVIL.put("C", "CASADO(A)");
  }

  public DetalleTabuladorOnu()
  {
    jdoSetnivel(this, 1);

    jdoSetaniosServicio(this, 0);

    jdoSetasignacionAnual(this, 0.0D);
  }

  public String toString() {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetasignacionAnual(this));

    return jdoGetnivel(this) + " - " + jdoGetaniosServicio(this) + " - " + 
      jdoGetestadoCivil(this) + " - " + a;
  }

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }

  public void setAniosServicio(int aniosServicio) {
    jdoSetaniosServicio(this, aniosServicio);
  }

  public double getAsignacionAnual() {
    return jdoGetasignacionAnual(this);
  }

  public void setAsignacionAnual(double asignacionAnual) {
    jdoSetasignacionAnual(this, asignacionAnual);
  }

  public String getEstadoCivil() {
    return jdoGetestadoCivil(this);
  }

  public void setEstadoCivil(String estadoCivil) {
    jdoSetestadoCivil(this, estadoCivil);
  }

  public long getIdDetalleTabuladorOnu() {
    return jdoGetidDetalleTabuladorOnu(this);
  }

  public void setIdDetalleTabuladorOnu(long idDetalleTabuladorOnu) {
    jdoSetidDetalleTabuladorOnu(this, idDetalleTabuladorOnu);
  }

  public int getNivel() {
    return jdoGetnivel(this);
  }

  public void setNivel(int nivel) {
    jdoSetnivel(this, nivel);
  }

  public Tabulador getTabulador() {
    return jdoGettabulador(this);
  }

  public void setTabulador(Tabulador tabulador) {
    jdoSettabulador(this, tabulador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DetalleTabuladorOnu localDetalleTabuladorOnu = new DetalleTabuladorOnu();
    localDetalleTabuladorOnu.jdoFlags = 1;
    localDetalleTabuladorOnu.jdoStateManager = paramStateManager;
    return localDetalleTabuladorOnu;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DetalleTabuladorOnu localDetalleTabuladorOnu = new DetalleTabuladorOnu();
    localDetalleTabuladorOnu.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDetalleTabuladorOnu.jdoFlags = 1;
    localDetalleTabuladorOnu.jdoStateManager = paramStateManager;
    return localDetalleTabuladorOnu;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.asignacionAnual);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estadoCivil);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDetalleTabuladorOnu);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.nivel);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tabulador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asignacionAnual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estadoCivil = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDetalleTabuladorOnu = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivel = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tabulador = ((Tabulador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DetalleTabuladorOnu paramDetalleTabuladorOnu, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDetalleTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramDetalleTabuladorOnu.aniosServicio;
      return;
    case 1:
      if (paramDetalleTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.asignacionAnual = paramDetalleTabuladorOnu.asignacionAnual;
      return;
    case 2:
      if (paramDetalleTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.estadoCivil = paramDetalleTabuladorOnu.estadoCivil;
      return;
    case 3:
      if (paramDetalleTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.idDetalleTabuladorOnu = paramDetalleTabuladorOnu.idDetalleTabuladorOnu;
      return;
    case 4:
      if (paramDetalleTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.nivel = paramDetalleTabuladorOnu.nivel;
      return;
    case 5:
      if (paramDetalleTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.tabulador = paramDetalleTabuladorOnu.tabulador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DetalleTabuladorOnu))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DetalleTabuladorOnu localDetalleTabuladorOnu = (DetalleTabuladorOnu)paramObject;
    if (localDetalleTabuladorOnu.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDetalleTabuladorOnu, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DetalleTabuladorOnuPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DetalleTabuladorOnuPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DetalleTabuladorOnuPK))
      throw new IllegalArgumentException("arg1");
    DetalleTabuladorOnuPK localDetalleTabuladorOnuPK = (DetalleTabuladorOnuPK)paramObject;
    localDetalleTabuladorOnuPK.idDetalleTabuladorOnu = this.idDetalleTabuladorOnu;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DetalleTabuladorOnuPK))
      throw new IllegalArgumentException("arg1");
    DetalleTabuladorOnuPK localDetalleTabuladorOnuPK = (DetalleTabuladorOnuPK)paramObject;
    this.idDetalleTabuladorOnu = localDetalleTabuladorOnuPK.idDetalleTabuladorOnu;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DetalleTabuladorOnuPK))
      throw new IllegalArgumentException("arg2");
    DetalleTabuladorOnuPK localDetalleTabuladorOnuPK = (DetalleTabuladorOnuPK)paramObject;
    localDetalleTabuladorOnuPK.idDetalleTabuladorOnu = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DetalleTabuladorOnuPK))
      throw new IllegalArgumentException("arg2");
    DetalleTabuladorOnuPK localDetalleTabuladorOnuPK = (DetalleTabuladorOnuPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localDetalleTabuladorOnuPK.idDetalleTabuladorOnu);
  }

  private static final int jdoGetaniosServicio(DetalleTabuladorOnu paramDetalleTabuladorOnu)
  {
    if (paramDetalleTabuladorOnu.jdoFlags <= 0)
      return paramDetalleTabuladorOnu.aniosServicio;
    StateManager localStateManager = paramDetalleTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorOnu.aniosServicio;
    if (localStateManager.isLoaded(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 0))
      return paramDetalleTabuladorOnu.aniosServicio;
    return localStateManager.getIntField(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 0, paramDetalleTabuladorOnu.aniosServicio);
  }

  private static final void jdoSetaniosServicio(DetalleTabuladorOnu paramDetalleTabuladorOnu, int paramInt)
  {
    if (paramDetalleTabuladorOnu.jdoFlags == 0)
    {
      paramDetalleTabuladorOnu.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorOnu.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 0, paramDetalleTabuladorOnu.aniosServicio, paramInt);
  }

  private static final double jdoGetasignacionAnual(DetalleTabuladorOnu paramDetalleTabuladorOnu)
  {
    if (paramDetalleTabuladorOnu.jdoFlags <= 0)
      return paramDetalleTabuladorOnu.asignacionAnual;
    StateManager localStateManager = paramDetalleTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorOnu.asignacionAnual;
    if (localStateManager.isLoaded(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 1))
      return paramDetalleTabuladorOnu.asignacionAnual;
    return localStateManager.getDoubleField(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 1, paramDetalleTabuladorOnu.asignacionAnual);
  }

  private static final void jdoSetasignacionAnual(DetalleTabuladorOnu paramDetalleTabuladorOnu, double paramDouble)
  {
    if (paramDetalleTabuladorOnu.jdoFlags == 0)
    {
      paramDetalleTabuladorOnu.asignacionAnual = paramDouble;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorOnu.asignacionAnual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 1, paramDetalleTabuladorOnu.asignacionAnual, paramDouble);
  }

  private static final String jdoGetestadoCivil(DetalleTabuladorOnu paramDetalleTabuladorOnu)
  {
    if (paramDetalleTabuladorOnu.jdoFlags <= 0)
      return paramDetalleTabuladorOnu.estadoCivil;
    StateManager localStateManager = paramDetalleTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorOnu.estadoCivil;
    if (localStateManager.isLoaded(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 2))
      return paramDetalleTabuladorOnu.estadoCivil;
    return localStateManager.getStringField(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 2, paramDetalleTabuladorOnu.estadoCivil);
  }

  private static final void jdoSetestadoCivil(DetalleTabuladorOnu paramDetalleTabuladorOnu, String paramString)
  {
    if (paramDetalleTabuladorOnu.jdoFlags == 0)
    {
      paramDetalleTabuladorOnu.estadoCivil = paramString;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorOnu.estadoCivil = paramString;
      return;
    }
    localStateManager.setStringField(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 2, paramDetalleTabuladorOnu.estadoCivil, paramString);
  }

  private static final long jdoGetidDetalleTabuladorOnu(DetalleTabuladorOnu paramDetalleTabuladorOnu)
  {
    return paramDetalleTabuladorOnu.idDetalleTabuladorOnu;
  }

  private static final void jdoSetidDetalleTabuladorOnu(DetalleTabuladorOnu paramDetalleTabuladorOnu, long paramLong)
  {
    StateManager localStateManager = paramDetalleTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorOnu.idDetalleTabuladorOnu = paramLong;
      return;
    }
    localStateManager.setLongField(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 3, paramDetalleTabuladorOnu.idDetalleTabuladorOnu, paramLong);
  }

  private static final int jdoGetnivel(DetalleTabuladorOnu paramDetalleTabuladorOnu)
  {
    if (paramDetalleTabuladorOnu.jdoFlags <= 0)
      return paramDetalleTabuladorOnu.nivel;
    StateManager localStateManager = paramDetalleTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorOnu.nivel;
    if (localStateManager.isLoaded(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 4))
      return paramDetalleTabuladorOnu.nivel;
    return localStateManager.getIntField(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 4, paramDetalleTabuladorOnu.nivel);
  }

  private static final void jdoSetnivel(DetalleTabuladorOnu paramDetalleTabuladorOnu, int paramInt)
  {
    if (paramDetalleTabuladorOnu.jdoFlags == 0)
    {
      paramDetalleTabuladorOnu.nivel = paramInt;
      return;
    }
    StateManager localStateManager = paramDetalleTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorOnu.nivel = paramInt;
      return;
    }
    localStateManager.setIntField(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 4, paramDetalleTabuladorOnu.nivel, paramInt);
  }

  private static final Tabulador jdoGettabulador(DetalleTabuladorOnu paramDetalleTabuladorOnu)
  {
    StateManager localStateManager = paramDetalleTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
      return paramDetalleTabuladorOnu.tabulador;
    if (localStateManager.isLoaded(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 5))
      return paramDetalleTabuladorOnu.tabulador;
    return (Tabulador)localStateManager.getObjectField(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 5, paramDetalleTabuladorOnu.tabulador);
  }

  private static final void jdoSettabulador(DetalleTabuladorOnu paramDetalleTabuladorOnu, Tabulador paramTabulador)
  {
    StateManager localStateManager = paramDetalleTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramDetalleTabuladorOnu.tabulador = paramTabulador;
      return;
    }
    localStateManager.setObjectField(paramDetalleTabuladorOnu, jdoInheritedFieldCount + 5, paramDetalleTabuladorOnu.tabulador, paramTabulador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}