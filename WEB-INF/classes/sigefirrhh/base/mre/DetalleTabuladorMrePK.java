package sigefirrhh.base.mre;

import java.io.Serializable;

public class DetalleTabuladorMrePK
  implements Serializable
{
  public long idDetalleTabuladorMre;

  public DetalleTabuladorMrePK()
  {
  }

  public DetalleTabuladorMrePK(long idDetalleTabuladorMre)
  {
    this.idDetalleTabuladorMre = idDetalleTabuladorMre;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DetalleTabuladorMrePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DetalleTabuladorMrePK thatPK)
  {
    return 
      this.idDetalleTabuladorMre == thatPK.idDetalleTabuladorMre;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDetalleTabuladorMre)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDetalleTabuladorMre);
  }
}