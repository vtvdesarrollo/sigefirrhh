package sigefirrhh.base.mre;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Tabulador;

public class AjusteTabuladorOnu
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTADO_CIVIL;
  private long idAjusteTabuladorOnu;
  private Tabulador tabulador;
  private int nivel;
  private String estadoCivil;
  private int aniosServicio;
  private double ajusteAnual;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "ajusteAnual", "aniosServicio", "estadoCivil", "idAjusteTabuladorOnu", "nivel", "tabulador" }; private static final Class[] jdoFieldTypes = { Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Tabulador") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.mre.AjusteTabuladorOnu"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AjusteTabuladorOnu());

    LISTA_ESTADO_CIVIL = 
      new LinkedHashMap();

    LISTA_ESTADO_CIVIL.put("S", "SOLTERO(A)");
    LISTA_ESTADO_CIVIL.put("C", "CASADO(A)");
  }

  public AjusteTabuladorOnu()
  {
    jdoSetnivel(this, 1);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetajusteAnual(this));

    return jdoGetnivel(this) + " - " + jdoGetaniosServicio(this) + " - " + 
      jdoGetestadoCivil(this) + " - " + a;
  }

  public double getAjusteAnual()
  {
    return jdoGetajusteAnual(this);
  }

  public void setAjusteAnual(double ajusteAnual)
  {
    jdoSetajusteAnual(this, ajusteAnual);
  }

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }

  public void setAniosServicio(int aniosServicio)
  {
    jdoSetaniosServicio(this, aniosServicio);
  }

  public String getEstadoCivil()
  {
    return jdoGetestadoCivil(this);
  }

  public void setEstadoCivil(String estadoCivil)
  {
    jdoSetestadoCivil(this, estadoCivil);
  }

  public long getIdAjusteTabuladorOnu()
  {
    return jdoGetidAjusteTabuladorOnu(this);
  }

  public void setIdAjusteTabuladorOnu(long idAjusteTabuladorOnu)
  {
    jdoSetidAjusteTabuladorOnu(this, idAjusteTabuladorOnu);
  }

  public int getNivel()
  {
    return jdoGetnivel(this);
  }

  public void setNivel(int nivel)
  {
    jdoSetnivel(this, nivel);
  }

  public Tabulador getTabulador()
  {
    return jdoGettabulador(this);
  }

  public void setTabulador(Tabulador tabulador)
  {
    jdoSettabulador(this, tabulador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AjusteTabuladorOnu localAjusteTabuladorOnu = new AjusteTabuladorOnu();
    localAjusteTabuladorOnu.jdoFlags = 1;
    localAjusteTabuladorOnu.jdoStateManager = paramStateManager;
    return localAjusteTabuladorOnu;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AjusteTabuladorOnu localAjusteTabuladorOnu = new AjusteTabuladorOnu();
    localAjusteTabuladorOnu.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAjusteTabuladorOnu.jdoFlags = 1;
    localAjusteTabuladorOnu.jdoStateManager = paramStateManager;
    return localAjusteTabuladorOnu;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.ajusteAnual);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estadoCivil);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAjusteTabuladorOnu);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.nivel);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tabulador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ajusteAnual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estadoCivil = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAjusteTabuladorOnu = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivel = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tabulador = ((Tabulador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AjusteTabuladorOnu paramAjusteTabuladorOnu, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAjusteTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.ajusteAnual = paramAjusteTabuladorOnu.ajusteAnual;
      return;
    case 1:
      if (paramAjusteTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramAjusteTabuladorOnu.aniosServicio;
      return;
    case 2:
      if (paramAjusteTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.estadoCivil = paramAjusteTabuladorOnu.estadoCivil;
      return;
    case 3:
      if (paramAjusteTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.idAjusteTabuladorOnu = paramAjusteTabuladorOnu.idAjusteTabuladorOnu;
      return;
    case 4:
      if (paramAjusteTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.nivel = paramAjusteTabuladorOnu.nivel;
      return;
    case 5:
      if (paramAjusteTabuladorOnu == null)
        throw new IllegalArgumentException("arg1");
      this.tabulador = paramAjusteTabuladorOnu.tabulador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AjusteTabuladorOnu))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AjusteTabuladorOnu localAjusteTabuladorOnu = (AjusteTabuladorOnu)paramObject;
    if (localAjusteTabuladorOnu.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAjusteTabuladorOnu, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AjusteTabuladorOnuPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AjusteTabuladorOnuPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AjusteTabuladorOnuPK))
      throw new IllegalArgumentException("arg1");
    AjusteTabuladorOnuPK localAjusteTabuladorOnuPK = (AjusteTabuladorOnuPK)paramObject;
    localAjusteTabuladorOnuPK.idAjusteTabuladorOnu = this.idAjusteTabuladorOnu;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AjusteTabuladorOnuPK))
      throw new IllegalArgumentException("arg1");
    AjusteTabuladorOnuPK localAjusteTabuladorOnuPK = (AjusteTabuladorOnuPK)paramObject;
    this.idAjusteTabuladorOnu = localAjusteTabuladorOnuPK.idAjusteTabuladorOnu;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AjusteTabuladorOnuPK))
      throw new IllegalArgumentException("arg2");
    AjusteTabuladorOnuPK localAjusteTabuladorOnuPK = (AjusteTabuladorOnuPK)paramObject;
    localAjusteTabuladorOnuPK.idAjusteTabuladorOnu = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AjusteTabuladorOnuPK))
      throw new IllegalArgumentException("arg2");
    AjusteTabuladorOnuPK localAjusteTabuladorOnuPK = (AjusteTabuladorOnuPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localAjusteTabuladorOnuPK.idAjusteTabuladorOnu);
  }

  private static final double jdoGetajusteAnual(AjusteTabuladorOnu paramAjusteTabuladorOnu)
  {
    if (paramAjusteTabuladorOnu.jdoFlags <= 0)
      return paramAjusteTabuladorOnu.ajusteAnual;
    StateManager localStateManager = paramAjusteTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteTabuladorOnu.ajusteAnual;
    if (localStateManager.isLoaded(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 0))
      return paramAjusteTabuladorOnu.ajusteAnual;
    return localStateManager.getDoubleField(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 0, paramAjusteTabuladorOnu.ajusteAnual);
  }

  private static final void jdoSetajusteAnual(AjusteTabuladorOnu paramAjusteTabuladorOnu, double paramDouble)
  {
    if (paramAjusteTabuladorOnu.jdoFlags == 0)
    {
      paramAjusteTabuladorOnu.ajusteAnual = paramDouble;
      return;
    }
    StateManager localStateManager = paramAjusteTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteTabuladorOnu.ajusteAnual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 0, paramAjusteTabuladorOnu.ajusteAnual, paramDouble);
  }

  private static final int jdoGetaniosServicio(AjusteTabuladorOnu paramAjusteTabuladorOnu)
  {
    if (paramAjusteTabuladorOnu.jdoFlags <= 0)
      return paramAjusteTabuladorOnu.aniosServicio;
    StateManager localStateManager = paramAjusteTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteTabuladorOnu.aniosServicio;
    if (localStateManager.isLoaded(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 1))
      return paramAjusteTabuladorOnu.aniosServicio;
    return localStateManager.getIntField(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 1, paramAjusteTabuladorOnu.aniosServicio);
  }

  private static final void jdoSetaniosServicio(AjusteTabuladorOnu paramAjusteTabuladorOnu, int paramInt)
  {
    if (paramAjusteTabuladorOnu.jdoFlags == 0)
    {
      paramAjusteTabuladorOnu.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramAjusteTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteTabuladorOnu.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 1, paramAjusteTabuladorOnu.aniosServicio, paramInt);
  }

  private static final String jdoGetestadoCivil(AjusteTabuladorOnu paramAjusteTabuladorOnu)
  {
    if (paramAjusteTabuladorOnu.jdoFlags <= 0)
      return paramAjusteTabuladorOnu.estadoCivil;
    StateManager localStateManager = paramAjusteTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteTabuladorOnu.estadoCivil;
    if (localStateManager.isLoaded(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 2))
      return paramAjusteTabuladorOnu.estadoCivil;
    return localStateManager.getStringField(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 2, paramAjusteTabuladorOnu.estadoCivil);
  }

  private static final void jdoSetestadoCivil(AjusteTabuladorOnu paramAjusteTabuladorOnu, String paramString)
  {
    if (paramAjusteTabuladorOnu.jdoFlags == 0)
    {
      paramAjusteTabuladorOnu.estadoCivil = paramString;
      return;
    }
    StateManager localStateManager = paramAjusteTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteTabuladorOnu.estadoCivil = paramString;
      return;
    }
    localStateManager.setStringField(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 2, paramAjusteTabuladorOnu.estadoCivil, paramString);
  }

  private static final long jdoGetidAjusteTabuladorOnu(AjusteTabuladorOnu paramAjusteTabuladorOnu)
  {
    return paramAjusteTabuladorOnu.idAjusteTabuladorOnu;
  }

  private static final void jdoSetidAjusteTabuladorOnu(AjusteTabuladorOnu paramAjusteTabuladorOnu, long paramLong)
  {
    StateManager localStateManager = paramAjusteTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteTabuladorOnu.idAjusteTabuladorOnu = paramLong;
      return;
    }
    localStateManager.setLongField(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 3, paramAjusteTabuladorOnu.idAjusteTabuladorOnu, paramLong);
  }

  private static final int jdoGetnivel(AjusteTabuladorOnu paramAjusteTabuladorOnu)
  {
    if (paramAjusteTabuladorOnu.jdoFlags <= 0)
      return paramAjusteTabuladorOnu.nivel;
    StateManager localStateManager = paramAjusteTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteTabuladorOnu.nivel;
    if (localStateManager.isLoaded(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 4))
      return paramAjusteTabuladorOnu.nivel;
    return localStateManager.getIntField(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 4, paramAjusteTabuladorOnu.nivel);
  }

  private static final void jdoSetnivel(AjusteTabuladorOnu paramAjusteTabuladorOnu, int paramInt)
  {
    if (paramAjusteTabuladorOnu.jdoFlags == 0)
    {
      paramAjusteTabuladorOnu.nivel = paramInt;
      return;
    }
    StateManager localStateManager = paramAjusteTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteTabuladorOnu.nivel = paramInt;
      return;
    }
    localStateManager.setIntField(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 4, paramAjusteTabuladorOnu.nivel, paramInt);
  }

  private static final Tabulador jdoGettabulador(AjusteTabuladorOnu paramAjusteTabuladorOnu)
  {
    StateManager localStateManager = paramAjusteTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteTabuladorOnu.tabulador;
    if (localStateManager.isLoaded(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 5))
      return paramAjusteTabuladorOnu.tabulador;
    return (Tabulador)localStateManager.getObjectField(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 5, paramAjusteTabuladorOnu.tabulador);
  }

  private static final void jdoSettabulador(AjusteTabuladorOnu paramAjusteTabuladorOnu, Tabulador paramTabulador)
  {
    StateManager localStateManager = paramAjusteTabuladorOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteTabuladorOnu.tabulador = paramTabulador;
      return;
    }
    localStateManager.setObjectField(paramAjusteTabuladorOnu, jdoInheritedFieldCount + 5, paramAjusteTabuladorOnu.tabulador, paramTabulador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}