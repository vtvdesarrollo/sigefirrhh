package sigefirrhh.base.mre;

import java.io.Serializable;

public class DetalleTabuladorOnuPK
  implements Serializable
{
  public long idDetalleTabuladorOnu;

  public DetalleTabuladorOnuPK()
  {
  }

  public DetalleTabuladorOnuPK(long idDetalleTabuladorOnu)
  {
    this.idDetalleTabuladorOnu = idDetalleTabuladorOnu;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DetalleTabuladorOnuPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DetalleTabuladorOnuPK thatPK)
  {
    return 
      this.idDetalleTabuladorOnu == thatPK.idDetalleTabuladorOnu;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDetalleTabuladorOnu)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDetalleTabuladorOnu);
  }
}