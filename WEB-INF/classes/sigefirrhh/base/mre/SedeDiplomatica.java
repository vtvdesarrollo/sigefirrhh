package sigefirrhh.base.mre;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.ubicacion.Ciudad;

public class SedeDiplomatica
  implements Serializable, PersistenceCapable
{
  private long idSedeDiplomatica;
  private String codSede;
  private String nombre;
  private TipoSede tipoSede;
  private Ciudad ciudad;
  private String codViejo;
  private String codBiblio;
  private String direccion;
  private String telefono1;
  private String telefono2;
  private String fax;
  private String email;
  private String observacion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "ciudad", "codBiblio", "codSede", "codViejo", "direccion", "email", "fax", "idSedeDiplomatica", "nombre", "observacion", "telefono1", "telefono2", "tipoSede" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.mre.TipoSede") };
  private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this) + " " + 
      jdoGettipoSede(this).getDescripcion();
  }

  public Ciudad getCiudad()
  {
    return jdoGetciudad(this);
  }

  public String getCodBiblio()
  {
    return jdoGetcodBiblio(this);
  }

  public String getCodSede()
  {
    return jdoGetcodSede(this);
  }

  public String getCodViejo()
  {
    return jdoGetcodViejo(this);
  }

  public String getDireccion()
  {
    return jdoGetdireccion(this);
  }

  public String getEmail()
  {
    return jdoGetemail(this);
  }

  public String getFax()
  {
    return jdoGetfax(this);
  }

  public long getIdSedeDiplomatica()
  {
    return jdoGetidSedeDiplomatica(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public String getObservacion()
  {
    return jdoGetobservacion(this);
  }

  public String getTelefono1()
  {
    return jdoGettelefono1(this);
  }

  public String getTelefono2()
  {
    return jdoGettelefono2(this);
  }

  public TipoSede getTipoSede()
  {
    return jdoGettipoSede(this);
  }

  public void setCiudad(Ciudad ciudad)
  {
    jdoSetciudad(this, ciudad);
  }

  public void setCodBiblio(String string)
  {
    jdoSetcodBiblio(this, string);
  }

  public void setCodSede(String string)
  {
    jdoSetcodSede(this, string);
  }

  public void setCodViejo(String string)
  {
    jdoSetcodViejo(this, string);
  }

  public void setDireccion(String string)
  {
    jdoSetdireccion(this, string);
  }

  public void setEmail(String string)
  {
    jdoSetemail(this, string);
  }

  public void setFax(String string)
  {
    jdoSetfax(this, string);
  }

  public void setIdSedeDiplomatica(long l)
  {
    jdoSetidSedeDiplomatica(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setObservacion(String string)
  {
    jdoSetobservacion(this, string);
  }

  public void setTelefono1(String string)
  {
    jdoSettelefono1(this, string);
  }

  public void setTelefono2(String string)
  {
    jdoSettelefono2(this, string);
  }

  public void setTipoSede(TipoSede sede)
  {
    jdoSettipoSede(this, sede);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.base.mre.SedeDiplomatica"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SedeDiplomatica());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SedeDiplomatica localSedeDiplomatica = new SedeDiplomatica();
    localSedeDiplomatica.jdoFlags = 1;
    localSedeDiplomatica.jdoStateManager = paramStateManager;
    return localSedeDiplomatica;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SedeDiplomatica localSedeDiplomatica = new SedeDiplomatica();
    localSedeDiplomatica.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSedeDiplomatica.jdoFlags = 1;
    localSedeDiplomatica.jdoStateManager = paramStateManager;
    return localSedeDiplomatica;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codBiblio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSede);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codViejo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.direccion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.email);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.fax);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSedeDiplomatica);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observacion);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono1);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono2);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoSede);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codBiblio = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codViejo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.direccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.email = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fax = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSedeDiplomatica = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono1 = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono2 = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoSede = ((TipoSede)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SedeDiplomatica paramSedeDiplomatica, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramSedeDiplomatica.ciudad;
      return;
    case 1:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.codBiblio = paramSedeDiplomatica.codBiblio;
      return;
    case 2:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.codSede = paramSedeDiplomatica.codSede;
      return;
    case 3:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.codViejo = paramSedeDiplomatica.codViejo;
      return;
    case 4:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.direccion = paramSedeDiplomatica.direccion;
      return;
    case 5:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.email = paramSedeDiplomatica.email;
      return;
    case 6:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.fax = paramSedeDiplomatica.fax;
      return;
    case 7:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.idSedeDiplomatica = paramSedeDiplomatica.idSedeDiplomatica;
      return;
    case 8:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramSedeDiplomatica.nombre;
      return;
    case 9:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.observacion = paramSedeDiplomatica.observacion;
      return;
    case 10:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.telefono1 = paramSedeDiplomatica.telefono1;
      return;
    case 11:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.telefono2 = paramSedeDiplomatica.telefono2;
      return;
    case 12:
      if (paramSedeDiplomatica == null)
        throw new IllegalArgumentException("arg1");
      this.tipoSede = paramSedeDiplomatica.tipoSede;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SedeDiplomatica))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SedeDiplomatica localSedeDiplomatica = (SedeDiplomatica)paramObject;
    if (localSedeDiplomatica.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSedeDiplomatica, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SedeDiplomaticaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SedeDiplomaticaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SedeDiplomaticaPK))
      throw new IllegalArgumentException("arg1");
    SedeDiplomaticaPK localSedeDiplomaticaPK = (SedeDiplomaticaPK)paramObject;
    localSedeDiplomaticaPK.idSedeDiplomatica = this.idSedeDiplomatica;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SedeDiplomaticaPK))
      throw new IllegalArgumentException("arg1");
    SedeDiplomaticaPK localSedeDiplomaticaPK = (SedeDiplomaticaPK)paramObject;
    this.idSedeDiplomatica = localSedeDiplomaticaPK.idSedeDiplomatica;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SedeDiplomaticaPK))
      throw new IllegalArgumentException("arg2");
    SedeDiplomaticaPK localSedeDiplomaticaPK = (SedeDiplomaticaPK)paramObject;
    localSedeDiplomaticaPK.idSedeDiplomatica = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SedeDiplomaticaPK))
      throw new IllegalArgumentException("arg2");
    SedeDiplomaticaPK localSedeDiplomaticaPK = (SedeDiplomaticaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localSedeDiplomaticaPK.idSedeDiplomatica);
  }

  private static final Ciudad jdoGetciudad(SedeDiplomatica paramSedeDiplomatica)
  {
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.ciudad;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 0))
      return paramSedeDiplomatica.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramSedeDiplomatica, jdoInheritedFieldCount + 0, paramSedeDiplomatica.ciudad);
  }

  private static final void jdoSetciudad(SedeDiplomatica paramSedeDiplomatica, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramSedeDiplomatica, jdoInheritedFieldCount + 0, paramSedeDiplomatica.ciudad, paramCiudad);
  }

  private static final String jdoGetcodBiblio(SedeDiplomatica paramSedeDiplomatica)
  {
    if (paramSedeDiplomatica.jdoFlags <= 0)
      return paramSedeDiplomatica.codBiblio;
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.codBiblio;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 1))
      return paramSedeDiplomatica.codBiblio;
    return localStateManager.getStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 1, paramSedeDiplomatica.codBiblio);
  }

  private static final void jdoSetcodBiblio(SedeDiplomatica paramSedeDiplomatica, String paramString)
  {
    if (paramSedeDiplomatica.jdoFlags == 0)
    {
      paramSedeDiplomatica.codBiblio = paramString;
      return;
    }
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.codBiblio = paramString;
      return;
    }
    localStateManager.setStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 1, paramSedeDiplomatica.codBiblio, paramString);
  }

  private static final String jdoGetcodSede(SedeDiplomatica paramSedeDiplomatica)
  {
    if (paramSedeDiplomatica.jdoFlags <= 0)
      return paramSedeDiplomatica.codSede;
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.codSede;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 2))
      return paramSedeDiplomatica.codSede;
    return localStateManager.getStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 2, paramSedeDiplomatica.codSede);
  }

  private static final void jdoSetcodSede(SedeDiplomatica paramSedeDiplomatica, String paramString)
  {
    if (paramSedeDiplomatica.jdoFlags == 0)
    {
      paramSedeDiplomatica.codSede = paramString;
      return;
    }
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.codSede = paramString;
      return;
    }
    localStateManager.setStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 2, paramSedeDiplomatica.codSede, paramString);
  }

  private static final String jdoGetcodViejo(SedeDiplomatica paramSedeDiplomatica)
  {
    if (paramSedeDiplomatica.jdoFlags <= 0)
      return paramSedeDiplomatica.codViejo;
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.codViejo;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 3))
      return paramSedeDiplomatica.codViejo;
    return localStateManager.getStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 3, paramSedeDiplomatica.codViejo);
  }

  private static final void jdoSetcodViejo(SedeDiplomatica paramSedeDiplomatica, String paramString)
  {
    if (paramSedeDiplomatica.jdoFlags == 0)
    {
      paramSedeDiplomatica.codViejo = paramString;
      return;
    }
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.codViejo = paramString;
      return;
    }
    localStateManager.setStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 3, paramSedeDiplomatica.codViejo, paramString);
  }

  private static final String jdoGetdireccion(SedeDiplomatica paramSedeDiplomatica)
  {
    if (paramSedeDiplomatica.jdoFlags <= 0)
      return paramSedeDiplomatica.direccion;
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.direccion;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 4))
      return paramSedeDiplomatica.direccion;
    return localStateManager.getStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 4, paramSedeDiplomatica.direccion);
  }

  private static final void jdoSetdireccion(SedeDiplomatica paramSedeDiplomatica, String paramString)
  {
    if (paramSedeDiplomatica.jdoFlags == 0)
    {
      paramSedeDiplomatica.direccion = paramString;
      return;
    }
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.direccion = paramString;
      return;
    }
    localStateManager.setStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 4, paramSedeDiplomatica.direccion, paramString);
  }

  private static final String jdoGetemail(SedeDiplomatica paramSedeDiplomatica)
  {
    if (paramSedeDiplomatica.jdoFlags <= 0)
      return paramSedeDiplomatica.email;
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.email;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 5))
      return paramSedeDiplomatica.email;
    return localStateManager.getStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 5, paramSedeDiplomatica.email);
  }

  private static final void jdoSetemail(SedeDiplomatica paramSedeDiplomatica, String paramString)
  {
    if (paramSedeDiplomatica.jdoFlags == 0)
    {
      paramSedeDiplomatica.email = paramString;
      return;
    }
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.email = paramString;
      return;
    }
    localStateManager.setStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 5, paramSedeDiplomatica.email, paramString);
  }

  private static final String jdoGetfax(SedeDiplomatica paramSedeDiplomatica)
  {
    if (paramSedeDiplomatica.jdoFlags <= 0)
      return paramSedeDiplomatica.fax;
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.fax;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 6))
      return paramSedeDiplomatica.fax;
    return localStateManager.getStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 6, paramSedeDiplomatica.fax);
  }

  private static final void jdoSetfax(SedeDiplomatica paramSedeDiplomatica, String paramString)
  {
    if (paramSedeDiplomatica.jdoFlags == 0)
    {
      paramSedeDiplomatica.fax = paramString;
      return;
    }
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.fax = paramString;
      return;
    }
    localStateManager.setStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 6, paramSedeDiplomatica.fax, paramString);
  }

  private static final long jdoGetidSedeDiplomatica(SedeDiplomatica paramSedeDiplomatica)
  {
    return paramSedeDiplomatica.idSedeDiplomatica;
  }

  private static final void jdoSetidSedeDiplomatica(SedeDiplomatica paramSedeDiplomatica, long paramLong)
  {
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.idSedeDiplomatica = paramLong;
      return;
    }
    localStateManager.setLongField(paramSedeDiplomatica, jdoInheritedFieldCount + 7, paramSedeDiplomatica.idSedeDiplomatica, paramLong);
  }

  private static final String jdoGetnombre(SedeDiplomatica paramSedeDiplomatica)
  {
    if (paramSedeDiplomatica.jdoFlags <= 0)
      return paramSedeDiplomatica.nombre;
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.nombre;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 8))
      return paramSedeDiplomatica.nombre;
    return localStateManager.getStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 8, paramSedeDiplomatica.nombre);
  }

  private static final void jdoSetnombre(SedeDiplomatica paramSedeDiplomatica, String paramString)
  {
    if (paramSedeDiplomatica.jdoFlags == 0)
    {
      paramSedeDiplomatica.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 8, paramSedeDiplomatica.nombre, paramString);
  }

  private static final String jdoGetobservacion(SedeDiplomatica paramSedeDiplomatica)
  {
    if (paramSedeDiplomatica.jdoFlags <= 0)
      return paramSedeDiplomatica.observacion;
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.observacion;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 9))
      return paramSedeDiplomatica.observacion;
    return localStateManager.getStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 9, paramSedeDiplomatica.observacion);
  }

  private static final void jdoSetobservacion(SedeDiplomatica paramSedeDiplomatica, String paramString)
  {
    if (paramSedeDiplomatica.jdoFlags == 0)
    {
      paramSedeDiplomatica.observacion = paramString;
      return;
    }
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.observacion = paramString;
      return;
    }
    localStateManager.setStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 9, paramSedeDiplomatica.observacion, paramString);
  }

  private static final String jdoGettelefono1(SedeDiplomatica paramSedeDiplomatica)
  {
    if (paramSedeDiplomatica.jdoFlags <= 0)
      return paramSedeDiplomatica.telefono1;
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.telefono1;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 10))
      return paramSedeDiplomatica.telefono1;
    return localStateManager.getStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 10, paramSedeDiplomatica.telefono1);
  }

  private static final void jdoSettelefono1(SedeDiplomatica paramSedeDiplomatica, String paramString)
  {
    if (paramSedeDiplomatica.jdoFlags == 0)
    {
      paramSedeDiplomatica.telefono1 = paramString;
      return;
    }
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.telefono1 = paramString;
      return;
    }
    localStateManager.setStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 10, paramSedeDiplomatica.telefono1, paramString);
  }

  private static final String jdoGettelefono2(SedeDiplomatica paramSedeDiplomatica)
  {
    if (paramSedeDiplomatica.jdoFlags <= 0)
      return paramSedeDiplomatica.telefono2;
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.telefono2;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 11))
      return paramSedeDiplomatica.telefono2;
    return localStateManager.getStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 11, paramSedeDiplomatica.telefono2);
  }

  private static final void jdoSettelefono2(SedeDiplomatica paramSedeDiplomatica, String paramString)
  {
    if (paramSedeDiplomatica.jdoFlags == 0)
    {
      paramSedeDiplomatica.telefono2 = paramString;
      return;
    }
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.telefono2 = paramString;
      return;
    }
    localStateManager.setStringField(paramSedeDiplomatica, jdoInheritedFieldCount + 11, paramSedeDiplomatica.telefono2, paramString);
  }

  private static final TipoSede jdoGettipoSede(SedeDiplomatica paramSedeDiplomatica)
  {
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
      return paramSedeDiplomatica.tipoSede;
    if (localStateManager.isLoaded(paramSedeDiplomatica, jdoInheritedFieldCount + 12))
      return paramSedeDiplomatica.tipoSede;
    return (TipoSede)localStateManager.getObjectField(paramSedeDiplomatica, jdoInheritedFieldCount + 12, paramSedeDiplomatica.tipoSede);
  }

  private static final void jdoSettipoSede(SedeDiplomatica paramSedeDiplomatica, TipoSede paramTipoSede)
  {
    StateManager localStateManager = paramSedeDiplomatica.jdoStateManager;
    if (localStateManager == null)
    {
      paramSedeDiplomatica.tipoSede = paramTipoSede;
      return;
    }
    localStateManager.setObjectField(paramSedeDiplomatica, jdoInheritedFieldCount + 12, paramSedeDiplomatica.tipoSede, paramTipoSede);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}