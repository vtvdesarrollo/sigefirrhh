package sigefirrhh.base.mre;

import java.io.Serializable;

public class TipoSedePK
  implements Serializable
{
  public long idTipoSede;

  public TipoSedePK()
  {
  }

  public TipoSedePK(long idTipoSede)
  {
    this.idTipoSede = idTipoSede;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TipoSedePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TipoSedePK thatPK)
  {
    return 
      this.idTipoSede == thatPK.idTipoSede;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTipoSede)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTipoSede);
  }
}