package sigefirrhh.personal.liquidacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class PrestacionesVR
  implements Serializable, PersistenceCapable
{
  private long idPrestacionesVR;
  private Date inicioLapso;
  private Date finLapso;
  private double lapso;
  private int diasCalculo;
  private double baseCalculo;
  private double montoPrestaciones;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "baseCalculo", "diasCalculo", "finLapso", "idPrestacionesVR", "inicioLapso", "lapso", "montoPrestaciones", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.util.Date"), Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public double getBaseCalculo()
  {
    return jdoGetbaseCalculo(this);
  }

  public int getDiasCalculo()
  {
    return jdoGetdiasCalculo(this);
  }

  public Date getFinLapso()
  {
    return jdoGetfinLapso(this);
  }

  public long getIdPrestacionesVR()
  {
    return jdoGetidPrestacionesVR(this);
  }

  public Date getInicioLapso()
  {
    return jdoGetinicioLapso(this);
  }

  public double getLapso()
  {
    return jdoGetlapso(this);
  }

  public double getMontoPrestaciones()
  {
    return jdoGetmontoPrestaciones(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setBaseCalculo(double d)
  {
    jdoSetbaseCalculo(this, d);
  }

  public void setDiasCalculo(int i)
  {
    jdoSetdiasCalculo(this, i);
  }

  public void setFinLapso(Date date)
  {
    jdoSetfinLapso(this, date);
  }

  public void setIdPrestacionesVR(long l)
  {
    jdoSetidPrestacionesVR(this, l);
  }

  public void setInicioLapso(Date date)
  {
    jdoSetinicioLapso(this, date);
  }

  public void setLapso(double d)
  {
    jdoSetlapso(this, d);
  }

  public void setMontoPrestaciones(double d)
  {
    jdoSetmontoPrestaciones(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.liquidacion.PrestacionesVR"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PrestacionesVR());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PrestacionesVR localPrestacionesVR = new PrestacionesVR();
    localPrestacionesVR.jdoFlags = 1;
    localPrestacionesVR.jdoStateManager = paramStateManager;
    return localPrestacionesVR;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PrestacionesVR localPrestacionesVR = new PrestacionesVR();
    localPrestacionesVR.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPrestacionesVR.jdoFlags = 1;
    localPrestacionesVR.jdoStateManager = paramStateManager;
    return localPrestacionesVR;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseCalculo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasCalculo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finLapso);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPrestacionesVR);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.inicioLapso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.lapso);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPrestaciones);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseCalculo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasCalculo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finLapso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPrestacionesVR = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.inicioLapso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lapso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPrestaciones = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PrestacionesVR paramPrestacionesVR, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPrestacionesVR == null)
        throw new IllegalArgumentException("arg1");
      this.baseCalculo = paramPrestacionesVR.baseCalculo;
      return;
    case 1:
      if (paramPrestacionesVR == null)
        throw new IllegalArgumentException("arg1");
      this.diasCalculo = paramPrestacionesVR.diasCalculo;
      return;
    case 2:
      if (paramPrestacionesVR == null)
        throw new IllegalArgumentException("arg1");
      this.finLapso = paramPrestacionesVR.finLapso;
      return;
    case 3:
      if (paramPrestacionesVR == null)
        throw new IllegalArgumentException("arg1");
      this.idPrestacionesVR = paramPrestacionesVR.idPrestacionesVR;
      return;
    case 4:
      if (paramPrestacionesVR == null)
        throw new IllegalArgumentException("arg1");
      this.inicioLapso = paramPrestacionesVR.inicioLapso;
      return;
    case 5:
      if (paramPrestacionesVR == null)
        throw new IllegalArgumentException("arg1");
      this.lapso = paramPrestacionesVR.lapso;
      return;
    case 6:
      if (paramPrestacionesVR == null)
        throw new IllegalArgumentException("arg1");
      this.montoPrestaciones = paramPrestacionesVR.montoPrestaciones;
      return;
    case 7:
      if (paramPrestacionesVR == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramPrestacionesVR.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PrestacionesVR))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PrestacionesVR localPrestacionesVR = (PrestacionesVR)paramObject;
    if (localPrestacionesVR.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPrestacionesVR, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PrestacionesVRPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PrestacionesVRPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrestacionesVRPK))
      throw new IllegalArgumentException("arg1");
    PrestacionesVRPK localPrestacionesVRPK = (PrestacionesVRPK)paramObject;
    localPrestacionesVRPK.idPrestacionesVR = this.idPrestacionesVR;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrestacionesVRPK))
      throw new IllegalArgumentException("arg1");
    PrestacionesVRPK localPrestacionesVRPK = (PrestacionesVRPK)paramObject;
    this.idPrestacionesVR = localPrestacionesVRPK.idPrestacionesVR;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrestacionesVRPK))
      throw new IllegalArgumentException("arg2");
    PrestacionesVRPK localPrestacionesVRPK = (PrestacionesVRPK)paramObject;
    localPrestacionesVRPK.idPrestacionesVR = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrestacionesVRPK))
      throw new IllegalArgumentException("arg2");
    PrestacionesVRPK localPrestacionesVRPK = (PrestacionesVRPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localPrestacionesVRPK.idPrestacionesVR);
  }

  private static final double jdoGetbaseCalculo(PrestacionesVR paramPrestacionesVR)
  {
    if (paramPrestacionesVR.jdoFlags <= 0)
      return paramPrestacionesVR.baseCalculo;
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesVR.baseCalculo;
    if (localStateManager.isLoaded(paramPrestacionesVR, jdoInheritedFieldCount + 0))
      return paramPrestacionesVR.baseCalculo;
    return localStateManager.getDoubleField(paramPrestacionesVR, jdoInheritedFieldCount + 0, paramPrestacionesVR.baseCalculo);
  }

  private static final void jdoSetbaseCalculo(PrestacionesVR paramPrestacionesVR, double paramDouble)
  {
    if (paramPrestacionesVR.jdoFlags == 0)
    {
      paramPrestacionesVR.baseCalculo = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesVR.baseCalculo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesVR, jdoInheritedFieldCount + 0, paramPrestacionesVR.baseCalculo, paramDouble);
  }

  private static final int jdoGetdiasCalculo(PrestacionesVR paramPrestacionesVR)
  {
    if (paramPrestacionesVR.jdoFlags <= 0)
      return paramPrestacionesVR.diasCalculo;
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesVR.diasCalculo;
    if (localStateManager.isLoaded(paramPrestacionesVR, jdoInheritedFieldCount + 1))
      return paramPrestacionesVR.diasCalculo;
    return localStateManager.getIntField(paramPrestacionesVR, jdoInheritedFieldCount + 1, paramPrestacionesVR.diasCalculo);
  }

  private static final void jdoSetdiasCalculo(PrestacionesVR paramPrestacionesVR, int paramInt)
  {
    if (paramPrestacionesVR.jdoFlags == 0)
    {
      paramPrestacionesVR.diasCalculo = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesVR.diasCalculo = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesVR, jdoInheritedFieldCount + 1, paramPrestacionesVR.diasCalculo, paramInt);
  }

  private static final Date jdoGetfinLapso(PrestacionesVR paramPrestacionesVR)
  {
    if (paramPrestacionesVR.jdoFlags <= 0)
      return paramPrestacionesVR.finLapso;
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesVR.finLapso;
    if (localStateManager.isLoaded(paramPrestacionesVR, jdoInheritedFieldCount + 2))
      return paramPrestacionesVR.finLapso;
    return (Date)localStateManager.getObjectField(paramPrestacionesVR, jdoInheritedFieldCount + 2, paramPrestacionesVR.finLapso);
  }

  private static final void jdoSetfinLapso(PrestacionesVR paramPrestacionesVR, Date paramDate)
  {
    if (paramPrestacionesVR.jdoFlags == 0)
    {
      paramPrestacionesVR.finLapso = paramDate;
      return;
    }
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesVR.finLapso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesVR, jdoInheritedFieldCount + 2, paramPrestacionesVR.finLapso, paramDate);
  }

  private static final long jdoGetidPrestacionesVR(PrestacionesVR paramPrestacionesVR)
  {
    return paramPrestacionesVR.idPrestacionesVR;
  }

  private static final void jdoSetidPrestacionesVR(PrestacionesVR paramPrestacionesVR, long paramLong)
  {
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesVR.idPrestacionesVR = paramLong;
      return;
    }
    localStateManager.setLongField(paramPrestacionesVR, jdoInheritedFieldCount + 3, paramPrestacionesVR.idPrestacionesVR, paramLong);
  }

  private static final Date jdoGetinicioLapso(PrestacionesVR paramPrestacionesVR)
  {
    if (paramPrestacionesVR.jdoFlags <= 0)
      return paramPrestacionesVR.inicioLapso;
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesVR.inicioLapso;
    if (localStateManager.isLoaded(paramPrestacionesVR, jdoInheritedFieldCount + 4))
      return paramPrestacionesVR.inicioLapso;
    return (Date)localStateManager.getObjectField(paramPrestacionesVR, jdoInheritedFieldCount + 4, paramPrestacionesVR.inicioLapso);
  }

  private static final void jdoSetinicioLapso(PrestacionesVR paramPrestacionesVR, Date paramDate)
  {
    if (paramPrestacionesVR.jdoFlags == 0)
    {
      paramPrestacionesVR.inicioLapso = paramDate;
      return;
    }
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesVR.inicioLapso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesVR, jdoInheritedFieldCount + 4, paramPrestacionesVR.inicioLapso, paramDate);
  }

  private static final double jdoGetlapso(PrestacionesVR paramPrestacionesVR)
  {
    if (paramPrestacionesVR.jdoFlags <= 0)
      return paramPrestacionesVR.lapso;
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesVR.lapso;
    if (localStateManager.isLoaded(paramPrestacionesVR, jdoInheritedFieldCount + 5))
      return paramPrestacionesVR.lapso;
    return localStateManager.getDoubleField(paramPrestacionesVR, jdoInheritedFieldCount + 5, paramPrestacionesVR.lapso);
  }

  private static final void jdoSetlapso(PrestacionesVR paramPrestacionesVR, double paramDouble)
  {
    if (paramPrestacionesVR.jdoFlags == 0)
    {
      paramPrestacionesVR.lapso = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesVR.lapso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesVR, jdoInheritedFieldCount + 5, paramPrestacionesVR.lapso, paramDouble);
  }

  private static final double jdoGetmontoPrestaciones(PrestacionesVR paramPrestacionesVR)
  {
    if (paramPrestacionesVR.jdoFlags <= 0)
      return paramPrestacionesVR.montoPrestaciones;
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesVR.montoPrestaciones;
    if (localStateManager.isLoaded(paramPrestacionesVR, jdoInheritedFieldCount + 6))
      return paramPrestacionesVR.montoPrestaciones;
    return localStateManager.getDoubleField(paramPrestacionesVR, jdoInheritedFieldCount + 6, paramPrestacionesVR.montoPrestaciones);
  }

  private static final void jdoSetmontoPrestaciones(PrestacionesVR paramPrestacionesVR, double paramDouble)
  {
    if (paramPrestacionesVR.jdoFlags == 0)
    {
      paramPrestacionesVR.montoPrestaciones = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesVR.montoPrestaciones = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesVR, jdoInheritedFieldCount + 6, paramPrestacionesVR.montoPrestaciones, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(PrestacionesVR paramPrestacionesVR)
  {
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesVR.trabajador;
    if (localStateManager.isLoaded(paramPrestacionesVR, jdoInheritedFieldCount + 7))
      return paramPrestacionesVR.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramPrestacionesVR, jdoInheritedFieldCount + 7, paramPrestacionesVR.trabajador);
  }

  private static final void jdoSettrabajador(PrestacionesVR paramPrestacionesVR, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramPrestacionesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesVR.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesVR, jdoInheritedFieldCount + 7, paramPrestacionesVR.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}