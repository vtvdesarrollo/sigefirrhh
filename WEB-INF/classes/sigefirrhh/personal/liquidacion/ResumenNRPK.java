package sigefirrhh.personal.liquidacion;

import java.io.Serializable;

public class ResumenNRPK
  implements Serializable
{
  public long idResumenNR;

  public ResumenNRPK()
  {
  }

  public ResumenNRPK(long idResumenNR)
  {
    this.idResumenNR = idResumenNR;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ResumenNRPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ResumenNRPK thatPK)
  {
    return 
      this.idResumenNR == thatPK.idResumenNR;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idResumenNR)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idResumenNR);
  }
}