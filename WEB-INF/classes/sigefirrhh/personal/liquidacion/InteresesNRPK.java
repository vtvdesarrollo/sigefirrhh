package sigefirrhh.personal.liquidacion;

import java.io.Serializable;

public class InteresesNRPK
  implements Serializable
{
  public long idInteresesNR;

  public InteresesNRPK()
  {
  }

  public InteresesNRPK(long idInteresesNR)
  {
    this.idInteresesNR = idInteresesNR;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((InteresesNRPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(InteresesNRPK thatPK)
  {
    return 
      this.idInteresesNR == thatPK.idInteresesNR;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idInteresesNR)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idInteresesNR);
  }
}