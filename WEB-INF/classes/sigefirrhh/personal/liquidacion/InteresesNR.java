package sigefirrhh.personal.liquidacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class InteresesNR
  implements Serializable, PersistenceCapable
{
  private long idInteresesNR;
  private Date inicioLapso;
  private Date finLapso;
  private double lapso;
  private int diasCalculo;
  private double baseCalculo;
  private double tasaAplicada;
  private double interesMensual;
  private double interesesAcumulados;
  private double anticiposAcumulados;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anticiposAcumulados", "baseCalculo", "diasCalculo", "finLapso", "idInteresesNR", "inicioLapso", "interesMensual", "interesesAcumulados", "lapso", "tasaAplicada", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.util.Date"), Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public double getAnticiposAcumulados()
  {
    return jdoGetanticiposAcumulados(this);
  }

  public double getBaseCalculo()
  {
    return jdoGetbaseCalculo(this);
  }

  public int getDiasCalculo()
  {
    return jdoGetdiasCalculo(this);
  }

  public Date getFinLapso()
  {
    return jdoGetfinLapso(this);
  }

  public long getIdInteresesNR()
  {
    return jdoGetidInteresesNR(this);
  }

  public Date getInicioLapso()
  {
    return jdoGetinicioLapso(this);
  }

  public double getInteresesAcumulados()
  {
    return jdoGetinteresesAcumulados(this);
  }

  public double getInteresMensual()
  {
    return jdoGetinteresMensual(this);
  }

  public double getLapso()
  {
    return jdoGetlapso(this);
  }

  public double getTasaAplicada()
  {
    return jdoGettasaAplicada(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAnticiposAcumulados(double d)
  {
    jdoSetanticiposAcumulados(this, d);
  }

  public void setBaseCalculo(double d)
  {
    jdoSetbaseCalculo(this, d);
  }

  public void setDiasCalculo(int i)
  {
    jdoSetdiasCalculo(this, i);
  }

  public void setFinLapso(Date date)
  {
    jdoSetfinLapso(this, date);
  }

  public void setIdInteresesNR(long l)
  {
    jdoSetidInteresesNR(this, l);
  }

  public void setInicioLapso(Date date)
  {
    jdoSetinicioLapso(this, date);
  }

  public void setInteresesAcumulados(double d)
  {
    jdoSetinteresesAcumulados(this, d);
  }

  public void setInteresMensual(double d)
  {
    jdoSetinteresMensual(this, d);
  }

  public void setLapso(double d)
  {
    jdoSetlapso(this, d);
  }

  public void setTasaAplicada(double d)
  {
    jdoSettasaAplicada(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.liquidacion.InteresesNR"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new InteresesNR());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    InteresesNR localInteresesNR = new InteresesNR();
    localInteresesNR.jdoFlags = 1;
    localInteresesNR.jdoStateManager = paramStateManager;
    return localInteresesNR;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    InteresesNR localInteresesNR = new InteresesNR();
    localInteresesNR.jdoCopyKeyFieldsFromObjectId(paramObject);
    localInteresesNR.jdoFlags = 1;
    localInteresesNR.jdoStateManager = paramStateManager;
    return localInteresesNR;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anticiposAcumulados);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseCalculo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasCalculo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finLapso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idInteresesNR);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.inicioLapso);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.interesMensual);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.interesesAcumulados);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.lapso);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.tasaAplicada);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anticiposAcumulados = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseCalculo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasCalculo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finLapso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idInteresesNR = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.inicioLapso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.interesMensual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.interesesAcumulados = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lapso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tasaAplicada = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(InteresesNR paramInteresesNR, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramInteresesNR == null)
        throw new IllegalArgumentException("arg1");
      this.anticiposAcumulados = paramInteresesNR.anticiposAcumulados;
      return;
    case 1:
      if (paramInteresesNR == null)
        throw new IllegalArgumentException("arg1");
      this.baseCalculo = paramInteresesNR.baseCalculo;
      return;
    case 2:
      if (paramInteresesNR == null)
        throw new IllegalArgumentException("arg1");
      this.diasCalculo = paramInteresesNR.diasCalculo;
      return;
    case 3:
      if (paramInteresesNR == null)
        throw new IllegalArgumentException("arg1");
      this.finLapso = paramInteresesNR.finLapso;
      return;
    case 4:
      if (paramInteresesNR == null)
        throw new IllegalArgumentException("arg1");
      this.idInteresesNR = paramInteresesNR.idInteresesNR;
      return;
    case 5:
      if (paramInteresesNR == null)
        throw new IllegalArgumentException("arg1");
      this.inicioLapso = paramInteresesNR.inicioLapso;
      return;
    case 6:
      if (paramInteresesNR == null)
        throw new IllegalArgumentException("arg1");
      this.interesMensual = paramInteresesNR.interesMensual;
      return;
    case 7:
      if (paramInteresesNR == null)
        throw new IllegalArgumentException("arg1");
      this.interesesAcumulados = paramInteresesNR.interesesAcumulados;
      return;
    case 8:
      if (paramInteresesNR == null)
        throw new IllegalArgumentException("arg1");
      this.lapso = paramInteresesNR.lapso;
      return;
    case 9:
      if (paramInteresesNR == null)
        throw new IllegalArgumentException("arg1");
      this.tasaAplicada = paramInteresesNR.tasaAplicada;
      return;
    case 10:
      if (paramInteresesNR == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramInteresesNR.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof InteresesNR))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    InteresesNR localInteresesNR = (InteresesNR)paramObject;
    if (localInteresesNR.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localInteresesNR, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new InteresesNRPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new InteresesNRPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof InteresesNRPK))
      throw new IllegalArgumentException("arg1");
    InteresesNRPK localInteresesNRPK = (InteresesNRPK)paramObject;
    localInteresesNRPK.idInteresesNR = this.idInteresesNR;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof InteresesNRPK))
      throw new IllegalArgumentException("arg1");
    InteresesNRPK localInteresesNRPK = (InteresesNRPK)paramObject;
    this.idInteresesNR = localInteresesNRPK.idInteresesNR;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof InteresesNRPK))
      throw new IllegalArgumentException("arg2");
    InteresesNRPK localInteresesNRPK = (InteresesNRPK)paramObject;
    localInteresesNRPK.idInteresesNR = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof InteresesNRPK))
      throw new IllegalArgumentException("arg2");
    InteresesNRPK localInteresesNRPK = (InteresesNRPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localInteresesNRPK.idInteresesNR);
  }

  private static final double jdoGetanticiposAcumulados(InteresesNR paramInteresesNR)
  {
    if (paramInteresesNR.jdoFlags <= 0)
      return paramInteresesNR.anticiposAcumulados;
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesNR.anticiposAcumulados;
    if (localStateManager.isLoaded(paramInteresesNR, jdoInheritedFieldCount + 0))
      return paramInteresesNR.anticiposAcumulados;
    return localStateManager.getDoubleField(paramInteresesNR, jdoInheritedFieldCount + 0, paramInteresesNR.anticiposAcumulados);
  }

  private static final void jdoSetanticiposAcumulados(InteresesNR paramInteresesNR, double paramDouble)
  {
    if (paramInteresesNR.jdoFlags == 0)
    {
      paramInteresesNR.anticiposAcumulados = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesNR.anticiposAcumulados = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesNR, jdoInheritedFieldCount + 0, paramInteresesNR.anticiposAcumulados, paramDouble);
  }

  private static final double jdoGetbaseCalculo(InteresesNR paramInteresesNR)
  {
    if (paramInteresesNR.jdoFlags <= 0)
      return paramInteresesNR.baseCalculo;
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesNR.baseCalculo;
    if (localStateManager.isLoaded(paramInteresesNR, jdoInheritedFieldCount + 1))
      return paramInteresesNR.baseCalculo;
    return localStateManager.getDoubleField(paramInteresesNR, jdoInheritedFieldCount + 1, paramInteresesNR.baseCalculo);
  }

  private static final void jdoSetbaseCalculo(InteresesNR paramInteresesNR, double paramDouble)
  {
    if (paramInteresesNR.jdoFlags == 0)
    {
      paramInteresesNR.baseCalculo = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesNR.baseCalculo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesNR, jdoInheritedFieldCount + 1, paramInteresesNR.baseCalculo, paramDouble);
  }

  private static final int jdoGetdiasCalculo(InteresesNR paramInteresesNR)
  {
    if (paramInteresesNR.jdoFlags <= 0)
      return paramInteresesNR.diasCalculo;
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesNR.diasCalculo;
    if (localStateManager.isLoaded(paramInteresesNR, jdoInheritedFieldCount + 2))
      return paramInteresesNR.diasCalculo;
    return localStateManager.getIntField(paramInteresesNR, jdoInheritedFieldCount + 2, paramInteresesNR.diasCalculo);
  }

  private static final void jdoSetdiasCalculo(InteresesNR paramInteresesNR, int paramInt)
  {
    if (paramInteresesNR.jdoFlags == 0)
    {
      paramInteresesNR.diasCalculo = paramInt;
      return;
    }
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesNR.diasCalculo = paramInt;
      return;
    }
    localStateManager.setIntField(paramInteresesNR, jdoInheritedFieldCount + 2, paramInteresesNR.diasCalculo, paramInt);
  }

  private static final Date jdoGetfinLapso(InteresesNR paramInteresesNR)
  {
    if (paramInteresesNR.jdoFlags <= 0)
      return paramInteresesNR.finLapso;
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesNR.finLapso;
    if (localStateManager.isLoaded(paramInteresesNR, jdoInheritedFieldCount + 3))
      return paramInteresesNR.finLapso;
    return (Date)localStateManager.getObjectField(paramInteresesNR, jdoInheritedFieldCount + 3, paramInteresesNR.finLapso);
  }

  private static final void jdoSetfinLapso(InteresesNR paramInteresesNR, Date paramDate)
  {
    if (paramInteresesNR.jdoFlags == 0)
    {
      paramInteresesNR.finLapso = paramDate;
      return;
    }
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesNR.finLapso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramInteresesNR, jdoInheritedFieldCount + 3, paramInteresesNR.finLapso, paramDate);
  }

  private static final long jdoGetidInteresesNR(InteresesNR paramInteresesNR)
  {
    return paramInteresesNR.idInteresesNR;
  }

  private static final void jdoSetidInteresesNR(InteresesNR paramInteresesNR, long paramLong)
  {
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesNR.idInteresesNR = paramLong;
      return;
    }
    localStateManager.setLongField(paramInteresesNR, jdoInheritedFieldCount + 4, paramInteresesNR.idInteresesNR, paramLong);
  }

  private static final Date jdoGetinicioLapso(InteresesNR paramInteresesNR)
  {
    if (paramInteresesNR.jdoFlags <= 0)
      return paramInteresesNR.inicioLapso;
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesNR.inicioLapso;
    if (localStateManager.isLoaded(paramInteresesNR, jdoInheritedFieldCount + 5))
      return paramInteresesNR.inicioLapso;
    return (Date)localStateManager.getObjectField(paramInteresesNR, jdoInheritedFieldCount + 5, paramInteresesNR.inicioLapso);
  }

  private static final void jdoSetinicioLapso(InteresesNR paramInteresesNR, Date paramDate)
  {
    if (paramInteresesNR.jdoFlags == 0)
    {
      paramInteresesNR.inicioLapso = paramDate;
      return;
    }
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesNR.inicioLapso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramInteresesNR, jdoInheritedFieldCount + 5, paramInteresesNR.inicioLapso, paramDate);
  }

  private static final double jdoGetinteresMensual(InteresesNR paramInteresesNR)
  {
    if (paramInteresesNR.jdoFlags <= 0)
      return paramInteresesNR.interesMensual;
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesNR.interesMensual;
    if (localStateManager.isLoaded(paramInteresesNR, jdoInheritedFieldCount + 6))
      return paramInteresesNR.interesMensual;
    return localStateManager.getDoubleField(paramInteresesNR, jdoInheritedFieldCount + 6, paramInteresesNR.interesMensual);
  }

  private static final void jdoSetinteresMensual(InteresesNR paramInteresesNR, double paramDouble)
  {
    if (paramInteresesNR.jdoFlags == 0)
    {
      paramInteresesNR.interesMensual = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesNR.interesMensual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesNR, jdoInheritedFieldCount + 6, paramInteresesNR.interesMensual, paramDouble);
  }

  private static final double jdoGetinteresesAcumulados(InteresesNR paramInteresesNR)
  {
    if (paramInteresesNR.jdoFlags <= 0)
      return paramInteresesNR.interesesAcumulados;
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesNR.interesesAcumulados;
    if (localStateManager.isLoaded(paramInteresesNR, jdoInheritedFieldCount + 7))
      return paramInteresesNR.interesesAcumulados;
    return localStateManager.getDoubleField(paramInteresesNR, jdoInheritedFieldCount + 7, paramInteresesNR.interesesAcumulados);
  }

  private static final void jdoSetinteresesAcumulados(InteresesNR paramInteresesNR, double paramDouble)
  {
    if (paramInteresesNR.jdoFlags == 0)
    {
      paramInteresesNR.interesesAcumulados = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesNR.interesesAcumulados = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesNR, jdoInheritedFieldCount + 7, paramInteresesNR.interesesAcumulados, paramDouble);
  }

  private static final double jdoGetlapso(InteresesNR paramInteresesNR)
  {
    if (paramInteresesNR.jdoFlags <= 0)
      return paramInteresesNR.lapso;
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesNR.lapso;
    if (localStateManager.isLoaded(paramInteresesNR, jdoInheritedFieldCount + 8))
      return paramInteresesNR.lapso;
    return localStateManager.getDoubleField(paramInteresesNR, jdoInheritedFieldCount + 8, paramInteresesNR.lapso);
  }

  private static final void jdoSetlapso(InteresesNR paramInteresesNR, double paramDouble)
  {
    if (paramInteresesNR.jdoFlags == 0)
    {
      paramInteresesNR.lapso = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesNR.lapso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesNR, jdoInheritedFieldCount + 8, paramInteresesNR.lapso, paramDouble);
  }

  private static final double jdoGettasaAplicada(InteresesNR paramInteresesNR)
  {
    if (paramInteresesNR.jdoFlags <= 0)
      return paramInteresesNR.tasaAplicada;
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesNR.tasaAplicada;
    if (localStateManager.isLoaded(paramInteresesNR, jdoInheritedFieldCount + 9))
      return paramInteresesNR.tasaAplicada;
    return localStateManager.getDoubleField(paramInteresesNR, jdoInheritedFieldCount + 9, paramInteresesNR.tasaAplicada);
  }

  private static final void jdoSettasaAplicada(InteresesNR paramInteresesNR, double paramDouble)
  {
    if (paramInteresesNR.jdoFlags == 0)
    {
      paramInteresesNR.tasaAplicada = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesNR.tasaAplicada = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesNR, jdoInheritedFieldCount + 9, paramInteresesNR.tasaAplicada, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(InteresesNR paramInteresesNR)
  {
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesNR.trabajador;
    if (localStateManager.isLoaded(paramInteresesNR, jdoInheritedFieldCount + 10))
      return paramInteresesNR.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramInteresesNR, jdoInheritedFieldCount + 10, paramInteresesNR.trabajador);
  }

  private static final void jdoSettrabajador(InteresesNR paramInteresesNR, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramInteresesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesNR.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramInteresesNR, jdoInheritedFieldCount + 10, paramInteresesNR.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}