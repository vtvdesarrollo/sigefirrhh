package sigefirrhh.personal.liquidacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.personal.trabajador.Trabajador;

public class HistoricoDetalle
  implements Serializable, PersistenceCapable
{
  private long idHistoricoDetalle;
  private int anio;
  private int mes;
  private Date fechaConcepto;
  private double montoConcepto;
  private String tipoRegistro;
  private Concepto concepto;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "concepto", "fechaConcepto", "idHistoricoDetalle", "mes", "montoConcepto", "tipoRegistro", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 26, 21, 24, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public Concepto getConcepto()
  {
    return jdoGetconcepto(this);
  }

  public Date getFechaConcepto()
  {
    return jdoGetfechaConcepto(this);
  }

  public long getIdHistoricoDetalle()
  {
    return jdoGetidHistoricoDetalle(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public double getMontoConcepto()
  {
    return jdoGetmontoConcepto(this);
  }

  public String getTipoRegistro()
  {
    return jdoGettipoRegistro(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setConcepto(Concepto concepto)
  {
    jdoSetconcepto(this, concepto);
  }

  public void setFechaConcepto(Date date)
  {
    jdoSetfechaConcepto(this, date);
  }

  public void setIdHistoricoDetalle(long l)
  {
    jdoSetidHistoricoDetalle(this, l);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setMontoConcepto(double d)
  {
    jdoSetmontoConcepto(this, d);
  }

  public void setTipoRegistro(String string)
  {
    jdoSettipoRegistro(this, string);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.liquidacion.HistoricoDetalle"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HistoricoDetalle());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HistoricoDetalle localHistoricoDetalle = new HistoricoDetalle();
    localHistoricoDetalle.jdoFlags = 1;
    localHistoricoDetalle.jdoStateManager = paramStateManager;
    return localHistoricoDetalle;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HistoricoDetalle localHistoricoDetalle = new HistoricoDetalle();
    localHistoricoDetalle.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHistoricoDetalle.jdoFlags = 1;
    localHistoricoDetalle.jdoStateManager = paramStateManager;
    return localHistoricoDetalle;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.concepto);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaConcepto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHistoricoDetalle);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoConcepto);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoRegistro);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.concepto = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaConcepto = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHistoricoDetalle = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoConcepto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoRegistro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HistoricoDetalle paramHistoricoDetalle, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHistoricoDetalle == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramHistoricoDetalle.anio;
      return;
    case 1:
      if (paramHistoricoDetalle == null)
        throw new IllegalArgumentException("arg1");
      this.concepto = paramHistoricoDetalle.concepto;
      return;
    case 2:
      if (paramHistoricoDetalle == null)
        throw new IllegalArgumentException("arg1");
      this.fechaConcepto = paramHistoricoDetalle.fechaConcepto;
      return;
    case 3:
      if (paramHistoricoDetalle == null)
        throw new IllegalArgumentException("arg1");
      this.idHistoricoDetalle = paramHistoricoDetalle.idHistoricoDetalle;
      return;
    case 4:
      if (paramHistoricoDetalle == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramHistoricoDetalle.mes;
      return;
    case 5:
      if (paramHistoricoDetalle == null)
        throw new IllegalArgumentException("arg1");
      this.montoConcepto = paramHistoricoDetalle.montoConcepto;
      return;
    case 6:
      if (paramHistoricoDetalle == null)
        throw new IllegalArgumentException("arg1");
      this.tipoRegistro = paramHistoricoDetalle.tipoRegistro;
      return;
    case 7:
      if (paramHistoricoDetalle == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramHistoricoDetalle.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HistoricoDetalle))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HistoricoDetalle localHistoricoDetalle = (HistoricoDetalle)paramObject;
    if (localHistoricoDetalle.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHistoricoDetalle, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HistoricoDetallePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HistoricoDetallePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoDetallePK))
      throw new IllegalArgumentException("arg1");
    HistoricoDetallePK localHistoricoDetallePK = (HistoricoDetallePK)paramObject;
    localHistoricoDetallePK.idHistoricoDetalle = this.idHistoricoDetalle;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoDetallePK))
      throw new IllegalArgumentException("arg1");
    HistoricoDetallePK localHistoricoDetallePK = (HistoricoDetallePK)paramObject;
    this.idHistoricoDetalle = localHistoricoDetallePK.idHistoricoDetalle;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoDetallePK))
      throw new IllegalArgumentException("arg2");
    HistoricoDetallePK localHistoricoDetallePK = (HistoricoDetallePK)paramObject;
    localHistoricoDetallePK.idHistoricoDetalle = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoDetallePK))
      throw new IllegalArgumentException("arg2");
    HistoricoDetallePK localHistoricoDetallePK = (HistoricoDetallePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localHistoricoDetallePK.idHistoricoDetalle);
  }

  private static final int jdoGetanio(HistoricoDetalle paramHistoricoDetalle)
  {
    if (paramHistoricoDetalle.jdoFlags <= 0)
      return paramHistoricoDetalle.anio;
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDetalle.anio;
    if (localStateManager.isLoaded(paramHistoricoDetalle, jdoInheritedFieldCount + 0))
      return paramHistoricoDetalle.anio;
    return localStateManager.getIntField(paramHistoricoDetalle, jdoInheritedFieldCount + 0, paramHistoricoDetalle.anio);
  }

  private static final void jdoSetanio(HistoricoDetalle paramHistoricoDetalle, int paramInt)
  {
    if (paramHistoricoDetalle.jdoFlags == 0)
    {
      paramHistoricoDetalle.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDetalle.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoDetalle, jdoInheritedFieldCount + 0, paramHistoricoDetalle.anio, paramInt);
  }

  private static final Concepto jdoGetconcepto(HistoricoDetalle paramHistoricoDetalle)
  {
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDetalle.concepto;
    if (localStateManager.isLoaded(paramHistoricoDetalle, jdoInheritedFieldCount + 1))
      return paramHistoricoDetalle.concepto;
    return (Concepto)localStateManager.getObjectField(paramHistoricoDetalle, jdoInheritedFieldCount + 1, paramHistoricoDetalle.concepto);
  }

  private static final void jdoSetconcepto(HistoricoDetalle paramHistoricoDetalle, Concepto paramConcepto)
  {
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDetalle.concepto = paramConcepto;
      return;
    }
    localStateManager.setObjectField(paramHistoricoDetalle, jdoInheritedFieldCount + 1, paramHistoricoDetalle.concepto, paramConcepto);
  }

  private static final Date jdoGetfechaConcepto(HistoricoDetalle paramHistoricoDetalle)
  {
    if (paramHistoricoDetalle.jdoFlags <= 0)
      return paramHistoricoDetalle.fechaConcepto;
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDetalle.fechaConcepto;
    if (localStateManager.isLoaded(paramHistoricoDetalle, jdoInheritedFieldCount + 2))
      return paramHistoricoDetalle.fechaConcepto;
    return (Date)localStateManager.getObjectField(paramHistoricoDetalle, jdoInheritedFieldCount + 2, paramHistoricoDetalle.fechaConcepto);
  }

  private static final void jdoSetfechaConcepto(HistoricoDetalle paramHistoricoDetalle, Date paramDate)
  {
    if (paramHistoricoDetalle.jdoFlags == 0)
    {
      paramHistoricoDetalle.fechaConcepto = paramDate;
      return;
    }
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDetalle.fechaConcepto = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistoricoDetalle, jdoInheritedFieldCount + 2, paramHistoricoDetalle.fechaConcepto, paramDate);
  }

  private static final long jdoGetidHistoricoDetalle(HistoricoDetalle paramHistoricoDetalle)
  {
    return paramHistoricoDetalle.idHistoricoDetalle;
  }

  private static final void jdoSetidHistoricoDetalle(HistoricoDetalle paramHistoricoDetalle, long paramLong)
  {
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDetalle.idHistoricoDetalle = paramLong;
      return;
    }
    localStateManager.setLongField(paramHistoricoDetalle, jdoInheritedFieldCount + 3, paramHistoricoDetalle.idHistoricoDetalle, paramLong);
  }

  private static final int jdoGetmes(HistoricoDetalle paramHistoricoDetalle)
  {
    if (paramHistoricoDetalle.jdoFlags <= 0)
      return paramHistoricoDetalle.mes;
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDetalle.mes;
    if (localStateManager.isLoaded(paramHistoricoDetalle, jdoInheritedFieldCount + 4))
      return paramHistoricoDetalle.mes;
    return localStateManager.getIntField(paramHistoricoDetalle, jdoInheritedFieldCount + 4, paramHistoricoDetalle.mes);
  }

  private static final void jdoSetmes(HistoricoDetalle paramHistoricoDetalle, int paramInt)
  {
    if (paramHistoricoDetalle.jdoFlags == 0)
    {
      paramHistoricoDetalle.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDetalle.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoDetalle, jdoInheritedFieldCount + 4, paramHistoricoDetalle.mes, paramInt);
  }

  private static final double jdoGetmontoConcepto(HistoricoDetalle paramHistoricoDetalle)
  {
    if (paramHistoricoDetalle.jdoFlags <= 0)
      return paramHistoricoDetalle.montoConcepto;
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDetalle.montoConcepto;
    if (localStateManager.isLoaded(paramHistoricoDetalle, jdoInheritedFieldCount + 5))
      return paramHistoricoDetalle.montoConcepto;
    return localStateManager.getDoubleField(paramHistoricoDetalle, jdoInheritedFieldCount + 5, paramHistoricoDetalle.montoConcepto);
  }

  private static final void jdoSetmontoConcepto(HistoricoDetalle paramHistoricoDetalle, double paramDouble)
  {
    if (paramHistoricoDetalle.jdoFlags == 0)
    {
      paramHistoricoDetalle.montoConcepto = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDetalle.montoConcepto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoDetalle, jdoInheritedFieldCount + 5, paramHistoricoDetalle.montoConcepto, paramDouble);
  }

  private static final String jdoGettipoRegistro(HistoricoDetalle paramHistoricoDetalle)
  {
    if (paramHistoricoDetalle.jdoFlags <= 0)
      return paramHistoricoDetalle.tipoRegistro;
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDetalle.tipoRegistro;
    if (localStateManager.isLoaded(paramHistoricoDetalle, jdoInheritedFieldCount + 6))
      return paramHistoricoDetalle.tipoRegistro;
    return localStateManager.getStringField(paramHistoricoDetalle, jdoInheritedFieldCount + 6, paramHistoricoDetalle.tipoRegistro);
  }

  private static final void jdoSettipoRegistro(HistoricoDetalle paramHistoricoDetalle, String paramString)
  {
    if (paramHistoricoDetalle.jdoFlags == 0)
    {
      paramHistoricoDetalle.tipoRegistro = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDetalle.tipoRegistro = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoDetalle, jdoInheritedFieldCount + 6, paramHistoricoDetalle.tipoRegistro, paramString);
  }

  private static final Trabajador jdoGettrabajador(HistoricoDetalle paramHistoricoDetalle)
  {
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDetalle.trabajador;
    if (localStateManager.isLoaded(paramHistoricoDetalle, jdoInheritedFieldCount + 7))
      return paramHistoricoDetalle.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramHistoricoDetalle, jdoInheritedFieldCount + 7, paramHistoricoDetalle.trabajador);
  }

  private static final void jdoSettrabajador(HistoricoDetalle paramHistoricoDetalle, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramHistoricoDetalle.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDetalle.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramHistoricoDetalle, jdoInheritedFieldCount + 7, paramHistoricoDetalle.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}