package sigefirrhh.personal.liquidacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class PrestacionesNR
  implements Serializable, PersistenceCapable
{
  private long idPrestacionesNR;
  private int anio;
  private int mes;
  private Date finLapso;
  private double diasLapso;
  private int diasPrestaciones;
  private int numeroDiasAdicionales;
  private double baseCinco;
  private double montoCinco;
  private double baseAdicional;
  private double montoAdicional;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "baseAdicional", "baseCinco", "diasLapso", "diasPrestaciones", "finLapso", "idPrestacionesNR", "mes", "montoAdicional", "montoCinco", "numeroDiasAdicionales", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public double getBaseAdicional()
  {
    return jdoGetbaseAdicional(this);
  }

  public double getBaseCinco()
  {
    return jdoGetbaseCinco(this);
  }

  public double getDiasLapso()
  {
    return jdoGetdiasLapso(this);
  }

  public int getDiasPrestaciones()
  {
    return jdoGetdiasPrestaciones(this);
  }

  public Date getFinLapso()
  {
    return jdoGetfinLapso(this);
  }

  public long getIdPrestacionesNR()
  {
    return jdoGetidPrestacionesNR(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public double getMontoAdicional()
  {
    return jdoGetmontoAdicional(this);
  }

  public double getMontoCinco()
  {
    return jdoGetmontoCinco(this);
  }

  public int getNumeroDiasAdicionales()
  {
    return jdoGetnumeroDiasAdicionales(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setBaseAdicional(double d)
  {
    jdoSetbaseAdicional(this, d);
  }

  public void setBaseCinco(double d)
  {
    jdoSetbaseCinco(this, d);
  }

  public void setDiasLapso(double d)
  {
    jdoSetdiasLapso(this, d);
  }

  public void setDiasPrestaciones(int i)
  {
    jdoSetdiasPrestaciones(this, i);
  }

  public void setFinLapso(Date date)
  {
    jdoSetfinLapso(this, date);
  }

  public void setIdPrestacionesNR(long l)
  {
    jdoSetidPrestacionesNR(this, l);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setMontoAdicional(double d)
  {
    jdoSetmontoAdicional(this, d);
  }

  public void setMontoCinco(double d)
  {
    jdoSetmontoCinco(this, d);
  }

  public void setNumeroDiasAdicionales(int i)
  {
    jdoSetnumeroDiasAdicionales(this, i);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 12;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.liquidacion.PrestacionesNR"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PrestacionesNR());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PrestacionesNR localPrestacionesNR = new PrestacionesNR();
    localPrestacionesNR.jdoFlags = 1;
    localPrestacionesNR.jdoStateManager = paramStateManager;
    return localPrestacionesNR;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PrestacionesNR localPrestacionesNR = new PrestacionesNR();
    localPrestacionesNR.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPrestacionesNR.jdoFlags = 1;
    localPrestacionesNR.jdoStateManager = paramStateManager;
    return localPrestacionesNR;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseAdicional);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseCinco);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasLapso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasPrestaciones);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finLapso);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPrestacionesNR);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAdicional);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoCinco);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroDiasAdicionales);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseAdicional = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseCinco = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasLapso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasPrestaciones = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finLapso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPrestacionesNR = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAdicional = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoCinco = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroDiasAdicionales = localStateManager.replacingIntField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PrestacionesNR paramPrestacionesNR, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPrestacionesNR.anio;
      return;
    case 1:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.baseAdicional = paramPrestacionesNR.baseAdicional;
      return;
    case 2:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.baseCinco = paramPrestacionesNR.baseCinco;
      return;
    case 3:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.diasLapso = paramPrestacionesNR.diasLapso;
      return;
    case 4:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.diasPrestaciones = paramPrestacionesNR.diasPrestaciones;
      return;
    case 5:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.finLapso = paramPrestacionesNR.finLapso;
      return;
    case 6:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.idPrestacionesNR = paramPrestacionesNR.idPrestacionesNR;
      return;
    case 7:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramPrestacionesNR.mes;
      return;
    case 8:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoAdicional = paramPrestacionesNR.montoAdicional;
      return;
    case 9:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoCinco = paramPrestacionesNR.montoCinco;
      return;
    case 10:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.numeroDiasAdicionales = paramPrestacionesNR.numeroDiasAdicionales;
      return;
    case 11:
      if (paramPrestacionesNR == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramPrestacionesNR.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PrestacionesNR))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PrestacionesNR localPrestacionesNR = (PrestacionesNR)paramObject;
    if (localPrestacionesNR.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPrestacionesNR, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PrestacionesNRPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PrestacionesNRPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrestacionesNRPK))
      throw new IllegalArgumentException("arg1");
    PrestacionesNRPK localPrestacionesNRPK = (PrestacionesNRPK)paramObject;
    localPrestacionesNRPK.idPrestacionesNR = this.idPrestacionesNR;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrestacionesNRPK))
      throw new IllegalArgumentException("arg1");
    PrestacionesNRPK localPrestacionesNRPK = (PrestacionesNRPK)paramObject;
    this.idPrestacionesNR = localPrestacionesNRPK.idPrestacionesNR;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrestacionesNRPK))
      throw new IllegalArgumentException("arg2");
    PrestacionesNRPK localPrestacionesNRPK = (PrestacionesNRPK)paramObject;
    localPrestacionesNRPK.idPrestacionesNR = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrestacionesNRPK))
      throw new IllegalArgumentException("arg2");
    PrestacionesNRPK localPrestacionesNRPK = (PrestacionesNRPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localPrestacionesNRPK.idPrestacionesNR);
  }

  private static final int jdoGetanio(PrestacionesNR paramPrestacionesNR)
  {
    if (paramPrestacionesNR.jdoFlags <= 0)
      return paramPrestacionesNR.anio;
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesNR.anio;
    if (localStateManager.isLoaded(paramPrestacionesNR, jdoInheritedFieldCount + 0))
      return paramPrestacionesNR.anio;
    return localStateManager.getIntField(paramPrestacionesNR, jdoInheritedFieldCount + 0, paramPrestacionesNR.anio);
  }

  private static final void jdoSetanio(PrestacionesNR paramPrestacionesNR, int paramInt)
  {
    if (paramPrestacionesNR.jdoFlags == 0)
    {
      paramPrestacionesNR.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesNR, jdoInheritedFieldCount + 0, paramPrestacionesNR.anio, paramInt);
  }

  private static final double jdoGetbaseAdicional(PrestacionesNR paramPrestacionesNR)
  {
    if (paramPrestacionesNR.jdoFlags <= 0)
      return paramPrestacionesNR.baseAdicional;
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesNR.baseAdicional;
    if (localStateManager.isLoaded(paramPrestacionesNR, jdoInheritedFieldCount + 1))
      return paramPrestacionesNR.baseAdicional;
    return localStateManager.getDoubleField(paramPrestacionesNR, jdoInheritedFieldCount + 1, paramPrestacionesNR.baseAdicional);
  }

  private static final void jdoSetbaseAdicional(PrestacionesNR paramPrestacionesNR, double paramDouble)
  {
    if (paramPrestacionesNR.jdoFlags == 0)
    {
      paramPrestacionesNR.baseAdicional = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.baseAdicional = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesNR, jdoInheritedFieldCount + 1, paramPrestacionesNR.baseAdicional, paramDouble);
  }

  private static final double jdoGetbaseCinco(PrestacionesNR paramPrestacionesNR)
  {
    if (paramPrestacionesNR.jdoFlags <= 0)
      return paramPrestacionesNR.baseCinco;
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesNR.baseCinco;
    if (localStateManager.isLoaded(paramPrestacionesNR, jdoInheritedFieldCount + 2))
      return paramPrestacionesNR.baseCinco;
    return localStateManager.getDoubleField(paramPrestacionesNR, jdoInheritedFieldCount + 2, paramPrestacionesNR.baseCinco);
  }

  private static final void jdoSetbaseCinco(PrestacionesNR paramPrestacionesNR, double paramDouble)
  {
    if (paramPrestacionesNR.jdoFlags == 0)
    {
      paramPrestacionesNR.baseCinco = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.baseCinco = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesNR, jdoInheritedFieldCount + 2, paramPrestacionesNR.baseCinco, paramDouble);
  }

  private static final double jdoGetdiasLapso(PrestacionesNR paramPrestacionesNR)
  {
    if (paramPrestacionesNR.jdoFlags <= 0)
      return paramPrestacionesNR.diasLapso;
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesNR.diasLapso;
    if (localStateManager.isLoaded(paramPrestacionesNR, jdoInheritedFieldCount + 3))
      return paramPrestacionesNR.diasLapso;
    return localStateManager.getDoubleField(paramPrestacionesNR, jdoInheritedFieldCount + 3, paramPrestacionesNR.diasLapso);
  }

  private static final void jdoSetdiasLapso(PrestacionesNR paramPrestacionesNR, double paramDouble)
  {
    if (paramPrestacionesNR.jdoFlags == 0)
    {
      paramPrestacionesNR.diasLapso = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.diasLapso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesNR, jdoInheritedFieldCount + 3, paramPrestacionesNR.diasLapso, paramDouble);
  }

  private static final int jdoGetdiasPrestaciones(PrestacionesNR paramPrestacionesNR)
  {
    if (paramPrestacionesNR.jdoFlags <= 0)
      return paramPrestacionesNR.diasPrestaciones;
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesNR.diasPrestaciones;
    if (localStateManager.isLoaded(paramPrestacionesNR, jdoInheritedFieldCount + 4))
      return paramPrestacionesNR.diasPrestaciones;
    return localStateManager.getIntField(paramPrestacionesNR, jdoInheritedFieldCount + 4, paramPrestacionesNR.diasPrestaciones);
  }

  private static final void jdoSetdiasPrestaciones(PrestacionesNR paramPrestacionesNR, int paramInt)
  {
    if (paramPrestacionesNR.jdoFlags == 0)
    {
      paramPrestacionesNR.diasPrestaciones = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.diasPrestaciones = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesNR, jdoInheritedFieldCount + 4, paramPrestacionesNR.diasPrestaciones, paramInt);
  }

  private static final Date jdoGetfinLapso(PrestacionesNR paramPrestacionesNR)
  {
    if (paramPrestacionesNR.jdoFlags <= 0)
      return paramPrestacionesNR.finLapso;
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesNR.finLapso;
    if (localStateManager.isLoaded(paramPrestacionesNR, jdoInheritedFieldCount + 5))
      return paramPrestacionesNR.finLapso;
    return (Date)localStateManager.getObjectField(paramPrestacionesNR, jdoInheritedFieldCount + 5, paramPrestacionesNR.finLapso);
  }

  private static final void jdoSetfinLapso(PrestacionesNR paramPrestacionesNR, Date paramDate)
  {
    if (paramPrestacionesNR.jdoFlags == 0)
    {
      paramPrestacionesNR.finLapso = paramDate;
      return;
    }
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.finLapso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesNR, jdoInheritedFieldCount + 5, paramPrestacionesNR.finLapso, paramDate);
  }

  private static final long jdoGetidPrestacionesNR(PrestacionesNR paramPrestacionesNR)
  {
    return paramPrestacionesNR.idPrestacionesNR;
  }

  private static final void jdoSetidPrestacionesNR(PrestacionesNR paramPrestacionesNR, long paramLong)
  {
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.idPrestacionesNR = paramLong;
      return;
    }
    localStateManager.setLongField(paramPrestacionesNR, jdoInheritedFieldCount + 6, paramPrestacionesNR.idPrestacionesNR, paramLong);
  }

  private static final int jdoGetmes(PrestacionesNR paramPrestacionesNR)
  {
    if (paramPrestacionesNR.jdoFlags <= 0)
      return paramPrestacionesNR.mes;
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesNR.mes;
    if (localStateManager.isLoaded(paramPrestacionesNR, jdoInheritedFieldCount + 7))
      return paramPrestacionesNR.mes;
    return localStateManager.getIntField(paramPrestacionesNR, jdoInheritedFieldCount + 7, paramPrestacionesNR.mes);
  }

  private static final void jdoSetmes(PrestacionesNR paramPrestacionesNR, int paramInt)
  {
    if (paramPrestacionesNR.jdoFlags == 0)
    {
      paramPrestacionesNR.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesNR, jdoInheritedFieldCount + 7, paramPrestacionesNR.mes, paramInt);
  }

  private static final double jdoGetmontoAdicional(PrestacionesNR paramPrestacionesNR)
  {
    if (paramPrestacionesNR.jdoFlags <= 0)
      return paramPrestacionesNR.montoAdicional;
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesNR.montoAdicional;
    if (localStateManager.isLoaded(paramPrestacionesNR, jdoInheritedFieldCount + 8))
      return paramPrestacionesNR.montoAdicional;
    return localStateManager.getDoubleField(paramPrestacionesNR, jdoInheritedFieldCount + 8, paramPrestacionesNR.montoAdicional);
  }

  private static final void jdoSetmontoAdicional(PrestacionesNR paramPrestacionesNR, double paramDouble)
  {
    if (paramPrestacionesNR.jdoFlags == 0)
    {
      paramPrestacionesNR.montoAdicional = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.montoAdicional = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesNR, jdoInheritedFieldCount + 8, paramPrestacionesNR.montoAdicional, paramDouble);
  }

  private static final double jdoGetmontoCinco(PrestacionesNR paramPrestacionesNR)
  {
    if (paramPrestacionesNR.jdoFlags <= 0)
      return paramPrestacionesNR.montoCinco;
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesNR.montoCinco;
    if (localStateManager.isLoaded(paramPrestacionesNR, jdoInheritedFieldCount + 9))
      return paramPrestacionesNR.montoCinco;
    return localStateManager.getDoubleField(paramPrestacionesNR, jdoInheritedFieldCount + 9, paramPrestacionesNR.montoCinco);
  }

  private static final void jdoSetmontoCinco(PrestacionesNR paramPrestacionesNR, double paramDouble)
  {
    if (paramPrestacionesNR.jdoFlags == 0)
    {
      paramPrestacionesNR.montoCinco = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.montoCinco = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesNR, jdoInheritedFieldCount + 9, paramPrestacionesNR.montoCinco, paramDouble);
  }

  private static final int jdoGetnumeroDiasAdicionales(PrestacionesNR paramPrestacionesNR)
  {
    if (paramPrestacionesNR.jdoFlags <= 0)
      return paramPrestacionesNR.numeroDiasAdicionales;
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesNR.numeroDiasAdicionales;
    if (localStateManager.isLoaded(paramPrestacionesNR, jdoInheritedFieldCount + 10))
      return paramPrestacionesNR.numeroDiasAdicionales;
    return localStateManager.getIntField(paramPrestacionesNR, jdoInheritedFieldCount + 10, paramPrestacionesNR.numeroDiasAdicionales);
  }

  private static final void jdoSetnumeroDiasAdicionales(PrestacionesNR paramPrestacionesNR, int paramInt)
  {
    if (paramPrestacionesNR.jdoFlags == 0)
    {
      paramPrestacionesNR.numeroDiasAdicionales = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.numeroDiasAdicionales = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesNR, jdoInheritedFieldCount + 10, paramPrestacionesNR.numeroDiasAdicionales, paramInt);
  }

  private static final Trabajador jdoGettrabajador(PrestacionesNR paramPrestacionesNR)
  {
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesNR.trabajador;
    if (localStateManager.isLoaded(paramPrestacionesNR, jdoInheritedFieldCount + 11))
      return paramPrestacionesNR.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramPrestacionesNR, jdoInheritedFieldCount + 11, paramPrestacionesNR.trabajador);
  }

  private static final void jdoSettrabajador(PrestacionesNR paramPrestacionesNR, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramPrestacionesNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesNR.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesNR, jdoInheritedFieldCount + 11, paramPrestacionesNR.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}