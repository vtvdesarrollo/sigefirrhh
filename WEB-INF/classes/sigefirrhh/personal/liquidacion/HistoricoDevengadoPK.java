package sigefirrhh.personal.liquidacion;

import java.io.Serializable;

public class HistoricoDevengadoPK
  implements Serializable
{
  public long idHistoricoDevengado;

  public HistoricoDevengadoPK()
  {
  }

  public HistoricoDevengadoPK(long idHistoricoDevengado)
  {
    this.idHistoricoDevengado = idHistoricoDevengado;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HistoricoDevengadoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HistoricoDevengadoPK thatPK)
  {
    return 
      this.idHistoricoDevengado == thatPK.idHistoricoDevengado;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHistoricoDevengado)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHistoricoDevengado);
  }
}