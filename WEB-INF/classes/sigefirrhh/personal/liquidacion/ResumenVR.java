package sigefirrhh.personal.liquidacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class ResumenVR
  implements Serializable, PersistenceCapable
{
  private long idResumenVR;
  private double totalPrestaciones;
  private double totalAnticipos;
  private double totalIntereses;
  private double bonoTransferencia;
  private double alicuotaUtilidades;
  private double diasAlicuotaUtilidades;
  private double baseDiariaAlicuota;
  private double anticiposDespues0697;
  private double interesesDespues0697;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "alicuotaUtilidades", "anticiposDespues0697", "baseDiariaAlicuota", "bonoTransferencia", "diasAlicuotaUtilidades", "idResumenVR", "interesesDespues0697", "totalAnticipos", "totalIntereses", "totalPrestaciones", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Long.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public double getAlicuotaUtilidades()
  {
    return jdoGetalicuotaUtilidades(this);
  }

  public double getAnticiposDespues0697()
  {
    return jdoGetanticiposDespues0697(this);
  }

  public double getBaseDiariaAlicuota()
  {
    return jdoGetbaseDiariaAlicuota(this);
  }

  public double getBonoTransferencia()
  {
    return jdoGetbonoTransferencia(this);
  }

  public double getDiasAlicuotaUtilidades()
  {
    return jdoGetdiasAlicuotaUtilidades(this);
  }

  public long getIdResumenVR()
  {
    return jdoGetidResumenVR(this);
  }

  public double getInteresesDespues0697()
  {
    return jdoGetinteresesDespues0697(this);
  }

  public double getTotalAnticipos()
  {
    return jdoGettotalAnticipos(this);
  }

  public double getTotalIntereses()
  {
    return jdoGettotalIntereses(this);
  }

  public double getTotalPrestaciones()
  {
    return jdoGettotalPrestaciones(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAlicuotaUtilidades(double d)
  {
    jdoSetalicuotaUtilidades(this, d);
  }

  public void setAnticiposDespues0697(double d)
  {
    jdoSetanticiposDespues0697(this, d);
  }

  public void setBaseDiariaAlicuota(double d)
  {
    jdoSetbaseDiariaAlicuota(this, d);
  }

  public void setBonoTransferencia(double d)
  {
    jdoSetbonoTransferencia(this, d);
  }

  public void setDiasAlicuotaUtilidades(double d)
  {
    jdoSetdiasAlicuotaUtilidades(this, d);
  }

  public void setIdResumenVR(long l)
  {
    jdoSetidResumenVR(this, l);
  }

  public void setInteresesDespues0697(double d)
  {
    jdoSetinteresesDespues0697(this, d);
  }

  public void setTotalAnticipos(double d)
  {
    jdoSettotalAnticipos(this, d);
  }

  public void setTotalIntereses(double d)
  {
    jdoSettotalIntereses(this, d);
  }

  public void setTotalPrestaciones(double d)
  {
    jdoSettotalPrestaciones(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.liquidacion.ResumenVR"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ResumenVR());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ResumenVR localResumenVR = new ResumenVR();
    localResumenVR.jdoFlags = 1;
    localResumenVR.jdoStateManager = paramStateManager;
    return localResumenVR;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ResumenVR localResumenVR = new ResumenVR();
    localResumenVR.jdoCopyKeyFieldsFromObjectId(paramObject);
    localResumenVR.jdoFlags = 1;
    localResumenVR.jdoStateManager = paramStateManager;
    return localResumenVR;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.alicuotaUtilidades);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anticiposDespues0697);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseDiariaAlicuota);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.bonoTransferencia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasAlicuotaUtilidades);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idResumenVR);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.interesesDespues0697);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalAnticipos);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalIntereses);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalPrestaciones);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alicuotaUtilidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anticiposDespues0697 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseDiariaAlicuota = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bonoTransferencia = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasAlicuotaUtilidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idResumenVR = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.interesesDespues0697 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalAnticipos = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalIntereses = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalPrestaciones = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ResumenVR paramResumenVR, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramResumenVR == null)
        throw new IllegalArgumentException("arg1");
      this.alicuotaUtilidades = paramResumenVR.alicuotaUtilidades;
      return;
    case 1:
      if (paramResumenVR == null)
        throw new IllegalArgumentException("arg1");
      this.anticiposDespues0697 = paramResumenVR.anticiposDespues0697;
      return;
    case 2:
      if (paramResumenVR == null)
        throw new IllegalArgumentException("arg1");
      this.baseDiariaAlicuota = paramResumenVR.baseDiariaAlicuota;
      return;
    case 3:
      if (paramResumenVR == null)
        throw new IllegalArgumentException("arg1");
      this.bonoTransferencia = paramResumenVR.bonoTransferencia;
      return;
    case 4:
      if (paramResumenVR == null)
        throw new IllegalArgumentException("arg1");
      this.diasAlicuotaUtilidades = paramResumenVR.diasAlicuotaUtilidades;
      return;
    case 5:
      if (paramResumenVR == null)
        throw new IllegalArgumentException("arg1");
      this.idResumenVR = paramResumenVR.idResumenVR;
      return;
    case 6:
      if (paramResumenVR == null)
        throw new IllegalArgumentException("arg1");
      this.interesesDespues0697 = paramResumenVR.interesesDespues0697;
      return;
    case 7:
      if (paramResumenVR == null)
        throw new IllegalArgumentException("arg1");
      this.totalAnticipos = paramResumenVR.totalAnticipos;
      return;
    case 8:
      if (paramResumenVR == null)
        throw new IllegalArgumentException("arg1");
      this.totalIntereses = paramResumenVR.totalIntereses;
      return;
    case 9:
      if (paramResumenVR == null)
        throw new IllegalArgumentException("arg1");
      this.totalPrestaciones = paramResumenVR.totalPrestaciones;
      return;
    case 10:
      if (paramResumenVR == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramResumenVR.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ResumenVR))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ResumenVR localResumenVR = (ResumenVR)paramObject;
    if (localResumenVR.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localResumenVR, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ResumenVRPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ResumenVRPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ResumenVRPK))
      throw new IllegalArgumentException("arg1");
    ResumenVRPK localResumenVRPK = (ResumenVRPK)paramObject;
    localResumenVRPK.idResumenVR = this.idResumenVR;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ResumenVRPK))
      throw new IllegalArgumentException("arg1");
    ResumenVRPK localResumenVRPK = (ResumenVRPK)paramObject;
    this.idResumenVR = localResumenVRPK.idResumenVR;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ResumenVRPK))
      throw new IllegalArgumentException("arg2");
    ResumenVRPK localResumenVRPK = (ResumenVRPK)paramObject;
    localResumenVRPK.idResumenVR = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ResumenVRPK))
      throw new IllegalArgumentException("arg2");
    ResumenVRPK localResumenVRPK = (ResumenVRPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localResumenVRPK.idResumenVR);
  }

  private static final double jdoGetalicuotaUtilidades(ResumenVR paramResumenVR)
  {
    if (paramResumenVR.jdoFlags <= 0)
      return paramResumenVR.alicuotaUtilidades;
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenVR.alicuotaUtilidades;
    if (localStateManager.isLoaded(paramResumenVR, jdoInheritedFieldCount + 0))
      return paramResumenVR.alicuotaUtilidades;
    return localStateManager.getDoubleField(paramResumenVR, jdoInheritedFieldCount + 0, paramResumenVR.alicuotaUtilidades);
  }

  private static final void jdoSetalicuotaUtilidades(ResumenVR paramResumenVR, double paramDouble)
  {
    if (paramResumenVR.jdoFlags == 0)
    {
      paramResumenVR.alicuotaUtilidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenVR.alicuotaUtilidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenVR, jdoInheritedFieldCount + 0, paramResumenVR.alicuotaUtilidades, paramDouble);
  }

  private static final double jdoGetanticiposDespues0697(ResumenVR paramResumenVR)
  {
    if (paramResumenVR.jdoFlags <= 0)
      return paramResumenVR.anticiposDespues0697;
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenVR.anticiposDespues0697;
    if (localStateManager.isLoaded(paramResumenVR, jdoInheritedFieldCount + 1))
      return paramResumenVR.anticiposDespues0697;
    return localStateManager.getDoubleField(paramResumenVR, jdoInheritedFieldCount + 1, paramResumenVR.anticiposDespues0697);
  }

  private static final void jdoSetanticiposDespues0697(ResumenVR paramResumenVR, double paramDouble)
  {
    if (paramResumenVR.jdoFlags == 0)
    {
      paramResumenVR.anticiposDespues0697 = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenVR.anticiposDespues0697 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenVR, jdoInheritedFieldCount + 1, paramResumenVR.anticiposDespues0697, paramDouble);
  }

  private static final double jdoGetbaseDiariaAlicuota(ResumenVR paramResumenVR)
  {
    if (paramResumenVR.jdoFlags <= 0)
      return paramResumenVR.baseDiariaAlicuota;
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenVR.baseDiariaAlicuota;
    if (localStateManager.isLoaded(paramResumenVR, jdoInheritedFieldCount + 2))
      return paramResumenVR.baseDiariaAlicuota;
    return localStateManager.getDoubleField(paramResumenVR, jdoInheritedFieldCount + 2, paramResumenVR.baseDiariaAlicuota);
  }

  private static final void jdoSetbaseDiariaAlicuota(ResumenVR paramResumenVR, double paramDouble)
  {
    if (paramResumenVR.jdoFlags == 0)
    {
      paramResumenVR.baseDiariaAlicuota = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenVR.baseDiariaAlicuota = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenVR, jdoInheritedFieldCount + 2, paramResumenVR.baseDiariaAlicuota, paramDouble);
  }

  private static final double jdoGetbonoTransferencia(ResumenVR paramResumenVR)
  {
    if (paramResumenVR.jdoFlags <= 0)
      return paramResumenVR.bonoTransferencia;
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenVR.bonoTransferencia;
    if (localStateManager.isLoaded(paramResumenVR, jdoInheritedFieldCount + 3))
      return paramResumenVR.bonoTransferencia;
    return localStateManager.getDoubleField(paramResumenVR, jdoInheritedFieldCount + 3, paramResumenVR.bonoTransferencia);
  }

  private static final void jdoSetbonoTransferencia(ResumenVR paramResumenVR, double paramDouble)
  {
    if (paramResumenVR.jdoFlags == 0)
    {
      paramResumenVR.bonoTransferencia = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenVR.bonoTransferencia = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenVR, jdoInheritedFieldCount + 3, paramResumenVR.bonoTransferencia, paramDouble);
  }

  private static final double jdoGetdiasAlicuotaUtilidades(ResumenVR paramResumenVR)
  {
    if (paramResumenVR.jdoFlags <= 0)
      return paramResumenVR.diasAlicuotaUtilidades;
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenVR.diasAlicuotaUtilidades;
    if (localStateManager.isLoaded(paramResumenVR, jdoInheritedFieldCount + 4))
      return paramResumenVR.diasAlicuotaUtilidades;
    return localStateManager.getDoubleField(paramResumenVR, jdoInheritedFieldCount + 4, paramResumenVR.diasAlicuotaUtilidades);
  }

  private static final void jdoSetdiasAlicuotaUtilidades(ResumenVR paramResumenVR, double paramDouble)
  {
    if (paramResumenVR.jdoFlags == 0)
    {
      paramResumenVR.diasAlicuotaUtilidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenVR.diasAlicuotaUtilidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenVR, jdoInheritedFieldCount + 4, paramResumenVR.diasAlicuotaUtilidades, paramDouble);
  }

  private static final long jdoGetidResumenVR(ResumenVR paramResumenVR)
  {
    return paramResumenVR.idResumenVR;
  }

  private static final void jdoSetidResumenVR(ResumenVR paramResumenVR, long paramLong)
  {
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenVR.idResumenVR = paramLong;
      return;
    }
    localStateManager.setLongField(paramResumenVR, jdoInheritedFieldCount + 5, paramResumenVR.idResumenVR, paramLong);
  }

  private static final double jdoGetinteresesDespues0697(ResumenVR paramResumenVR)
  {
    if (paramResumenVR.jdoFlags <= 0)
      return paramResumenVR.interesesDespues0697;
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenVR.interesesDespues0697;
    if (localStateManager.isLoaded(paramResumenVR, jdoInheritedFieldCount + 6))
      return paramResumenVR.interesesDespues0697;
    return localStateManager.getDoubleField(paramResumenVR, jdoInheritedFieldCount + 6, paramResumenVR.interesesDespues0697);
  }

  private static final void jdoSetinteresesDespues0697(ResumenVR paramResumenVR, double paramDouble)
  {
    if (paramResumenVR.jdoFlags == 0)
    {
      paramResumenVR.interesesDespues0697 = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenVR.interesesDespues0697 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenVR, jdoInheritedFieldCount + 6, paramResumenVR.interesesDespues0697, paramDouble);
  }

  private static final double jdoGettotalAnticipos(ResumenVR paramResumenVR)
  {
    if (paramResumenVR.jdoFlags <= 0)
      return paramResumenVR.totalAnticipos;
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenVR.totalAnticipos;
    if (localStateManager.isLoaded(paramResumenVR, jdoInheritedFieldCount + 7))
      return paramResumenVR.totalAnticipos;
    return localStateManager.getDoubleField(paramResumenVR, jdoInheritedFieldCount + 7, paramResumenVR.totalAnticipos);
  }

  private static final void jdoSettotalAnticipos(ResumenVR paramResumenVR, double paramDouble)
  {
    if (paramResumenVR.jdoFlags == 0)
    {
      paramResumenVR.totalAnticipos = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenVR.totalAnticipos = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenVR, jdoInheritedFieldCount + 7, paramResumenVR.totalAnticipos, paramDouble);
  }

  private static final double jdoGettotalIntereses(ResumenVR paramResumenVR)
  {
    if (paramResumenVR.jdoFlags <= 0)
      return paramResumenVR.totalIntereses;
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenVR.totalIntereses;
    if (localStateManager.isLoaded(paramResumenVR, jdoInheritedFieldCount + 8))
      return paramResumenVR.totalIntereses;
    return localStateManager.getDoubleField(paramResumenVR, jdoInheritedFieldCount + 8, paramResumenVR.totalIntereses);
  }

  private static final void jdoSettotalIntereses(ResumenVR paramResumenVR, double paramDouble)
  {
    if (paramResumenVR.jdoFlags == 0)
    {
      paramResumenVR.totalIntereses = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenVR.totalIntereses = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenVR, jdoInheritedFieldCount + 8, paramResumenVR.totalIntereses, paramDouble);
  }

  private static final double jdoGettotalPrestaciones(ResumenVR paramResumenVR)
  {
    if (paramResumenVR.jdoFlags <= 0)
      return paramResumenVR.totalPrestaciones;
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenVR.totalPrestaciones;
    if (localStateManager.isLoaded(paramResumenVR, jdoInheritedFieldCount + 9))
      return paramResumenVR.totalPrestaciones;
    return localStateManager.getDoubleField(paramResumenVR, jdoInheritedFieldCount + 9, paramResumenVR.totalPrestaciones);
  }

  private static final void jdoSettotalPrestaciones(ResumenVR paramResumenVR, double paramDouble)
  {
    if (paramResumenVR.jdoFlags == 0)
    {
      paramResumenVR.totalPrestaciones = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenVR.totalPrestaciones = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenVR, jdoInheritedFieldCount + 9, paramResumenVR.totalPrestaciones, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(ResumenVR paramResumenVR)
  {
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenVR.trabajador;
    if (localStateManager.isLoaded(paramResumenVR, jdoInheritedFieldCount + 10))
      return paramResumenVR.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramResumenVR, jdoInheritedFieldCount + 10, paramResumenVR.trabajador);
  }

  private static final void jdoSettrabajador(ResumenVR paramResumenVR, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramResumenVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenVR.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramResumenVR, jdoInheritedFieldCount + 10, paramResumenVR.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}