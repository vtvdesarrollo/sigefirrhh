package sigefirrhh.personal.liquidacion;

import java.io.Serializable;

public class InteresesVRPK
  implements Serializable
{
  public long idInteresesVR;

  public InteresesVRPK()
  {
  }

  public InteresesVRPK(long idInteresesVR)
  {
    this.idInteresesVR = idInteresesVR;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((InteresesVRPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(InteresesVRPK thatPK)
  {
    return 
      this.idInteresesVR == thatPK.idInteresesVR;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idInteresesVR)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idInteresesVR);
  }
}