package sigefirrhh.personal.liquidacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class InteresesVR
  implements Serializable, PersistenceCapable
{
  private long idInteresesVR;
  private Date inicioLapso;
  private Date finLapso;
  private double lapso;
  private int diasCalculo;
  private double baseCalculo;
  private double tasaAplicada;
  private double interesMensual;
  private double interesesAcumulados;
  private double anticiposAcumulados;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anticiposAcumulados", "baseCalculo", "diasCalculo", "finLapso", "idInteresesVR", "inicioLapso", "interesMensual", "interesesAcumulados", "lapso", "tasaAplicada", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.util.Date"), Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public double getAnticiposAcumulados()
  {
    return jdoGetanticiposAcumulados(this);
  }

  public double getBaseCalculo()
  {
    return jdoGetbaseCalculo(this);
  }

  public int getDiasCalculo()
  {
    return jdoGetdiasCalculo(this);
  }

  public Date getFinLapso()
  {
    return jdoGetfinLapso(this);
  }

  public long getIdInteresesVR()
  {
    return jdoGetidInteresesVR(this);
  }

  public Date getInicioLapso()
  {
    return jdoGetinicioLapso(this);
  }

  public double getInteresesAcumulados()
  {
    return jdoGetinteresesAcumulados(this);
  }

  public double getInteresMensual()
  {
    return jdoGetinteresMensual(this);
  }

  public double getLapso()
  {
    return jdoGetlapso(this);
  }

  public double getTasaAplicada()
  {
    return jdoGettasaAplicada(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAnticiposAcumulados(double d)
  {
    jdoSetanticiposAcumulados(this, d);
  }

  public void setBaseCalculo(double d)
  {
    jdoSetbaseCalculo(this, d);
  }

  public void setDiasCalculo(int i)
  {
    jdoSetdiasCalculo(this, i);
  }

  public void setFinLapso(Date date)
  {
    jdoSetfinLapso(this, date);
  }

  public void setIdInteresesVR(long l)
  {
    jdoSetidInteresesVR(this, l);
  }

  public void setInicioLapso(Date date)
  {
    jdoSetinicioLapso(this, date);
  }

  public void setInteresesAcumulados(double d)
  {
    jdoSetinteresesAcumulados(this, d);
  }

  public void setInteresMensual(double d)
  {
    jdoSetinteresMensual(this, d);
  }

  public void setLapso(double d)
  {
    jdoSetlapso(this, d);
  }

  public void setTasaAplicada(double d)
  {
    jdoSettasaAplicada(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.liquidacion.InteresesVR"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new InteresesVR());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    InteresesVR localInteresesVR = new InteresesVR();
    localInteresesVR.jdoFlags = 1;
    localInteresesVR.jdoStateManager = paramStateManager;
    return localInteresesVR;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    InteresesVR localInteresesVR = new InteresesVR();
    localInteresesVR.jdoCopyKeyFieldsFromObjectId(paramObject);
    localInteresesVR.jdoFlags = 1;
    localInteresesVR.jdoStateManager = paramStateManager;
    return localInteresesVR;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anticiposAcumulados);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseCalculo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasCalculo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finLapso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idInteresesVR);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.inicioLapso);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.interesMensual);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.interesesAcumulados);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.lapso);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.tasaAplicada);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anticiposAcumulados = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseCalculo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasCalculo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finLapso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idInteresesVR = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.inicioLapso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.interesMensual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.interesesAcumulados = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lapso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tasaAplicada = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(InteresesVR paramInteresesVR, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramInteresesVR == null)
        throw new IllegalArgumentException("arg1");
      this.anticiposAcumulados = paramInteresesVR.anticiposAcumulados;
      return;
    case 1:
      if (paramInteresesVR == null)
        throw new IllegalArgumentException("arg1");
      this.baseCalculo = paramInteresesVR.baseCalculo;
      return;
    case 2:
      if (paramInteresesVR == null)
        throw new IllegalArgumentException("arg1");
      this.diasCalculo = paramInteresesVR.diasCalculo;
      return;
    case 3:
      if (paramInteresesVR == null)
        throw new IllegalArgumentException("arg1");
      this.finLapso = paramInteresesVR.finLapso;
      return;
    case 4:
      if (paramInteresesVR == null)
        throw new IllegalArgumentException("arg1");
      this.idInteresesVR = paramInteresesVR.idInteresesVR;
      return;
    case 5:
      if (paramInteresesVR == null)
        throw new IllegalArgumentException("arg1");
      this.inicioLapso = paramInteresesVR.inicioLapso;
      return;
    case 6:
      if (paramInteresesVR == null)
        throw new IllegalArgumentException("arg1");
      this.interesMensual = paramInteresesVR.interesMensual;
      return;
    case 7:
      if (paramInteresesVR == null)
        throw new IllegalArgumentException("arg1");
      this.interesesAcumulados = paramInteresesVR.interesesAcumulados;
      return;
    case 8:
      if (paramInteresesVR == null)
        throw new IllegalArgumentException("arg1");
      this.lapso = paramInteresesVR.lapso;
      return;
    case 9:
      if (paramInteresesVR == null)
        throw new IllegalArgumentException("arg1");
      this.tasaAplicada = paramInteresesVR.tasaAplicada;
      return;
    case 10:
      if (paramInteresesVR == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramInteresesVR.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof InteresesVR))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    InteresesVR localInteresesVR = (InteresesVR)paramObject;
    if (localInteresesVR.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localInteresesVR, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new InteresesVRPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new InteresesVRPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof InteresesVRPK))
      throw new IllegalArgumentException("arg1");
    InteresesVRPK localInteresesVRPK = (InteresesVRPK)paramObject;
    localInteresesVRPK.idInteresesVR = this.idInteresesVR;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof InteresesVRPK))
      throw new IllegalArgumentException("arg1");
    InteresesVRPK localInteresesVRPK = (InteresesVRPK)paramObject;
    this.idInteresesVR = localInteresesVRPK.idInteresesVR;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof InteresesVRPK))
      throw new IllegalArgumentException("arg2");
    InteresesVRPK localInteresesVRPK = (InteresesVRPK)paramObject;
    localInteresesVRPK.idInteresesVR = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof InteresesVRPK))
      throw new IllegalArgumentException("arg2");
    InteresesVRPK localInteresesVRPK = (InteresesVRPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localInteresesVRPK.idInteresesVR);
  }

  private static final double jdoGetanticiposAcumulados(InteresesVR paramInteresesVR)
  {
    if (paramInteresesVR.jdoFlags <= 0)
      return paramInteresesVR.anticiposAcumulados;
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesVR.anticiposAcumulados;
    if (localStateManager.isLoaded(paramInteresesVR, jdoInheritedFieldCount + 0))
      return paramInteresesVR.anticiposAcumulados;
    return localStateManager.getDoubleField(paramInteresesVR, jdoInheritedFieldCount + 0, paramInteresesVR.anticiposAcumulados);
  }

  private static final void jdoSetanticiposAcumulados(InteresesVR paramInteresesVR, double paramDouble)
  {
    if (paramInteresesVR.jdoFlags == 0)
    {
      paramInteresesVR.anticiposAcumulados = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesVR.anticiposAcumulados = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesVR, jdoInheritedFieldCount + 0, paramInteresesVR.anticiposAcumulados, paramDouble);
  }

  private static final double jdoGetbaseCalculo(InteresesVR paramInteresesVR)
  {
    if (paramInteresesVR.jdoFlags <= 0)
      return paramInteresesVR.baseCalculo;
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesVR.baseCalculo;
    if (localStateManager.isLoaded(paramInteresesVR, jdoInheritedFieldCount + 1))
      return paramInteresesVR.baseCalculo;
    return localStateManager.getDoubleField(paramInteresesVR, jdoInheritedFieldCount + 1, paramInteresesVR.baseCalculo);
  }

  private static final void jdoSetbaseCalculo(InteresesVR paramInteresesVR, double paramDouble)
  {
    if (paramInteresesVR.jdoFlags == 0)
    {
      paramInteresesVR.baseCalculo = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesVR.baseCalculo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesVR, jdoInheritedFieldCount + 1, paramInteresesVR.baseCalculo, paramDouble);
  }

  private static final int jdoGetdiasCalculo(InteresesVR paramInteresesVR)
  {
    if (paramInteresesVR.jdoFlags <= 0)
      return paramInteresesVR.diasCalculo;
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesVR.diasCalculo;
    if (localStateManager.isLoaded(paramInteresesVR, jdoInheritedFieldCount + 2))
      return paramInteresesVR.diasCalculo;
    return localStateManager.getIntField(paramInteresesVR, jdoInheritedFieldCount + 2, paramInteresesVR.diasCalculo);
  }

  private static final void jdoSetdiasCalculo(InteresesVR paramInteresesVR, int paramInt)
  {
    if (paramInteresesVR.jdoFlags == 0)
    {
      paramInteresesVR.diasCalculo = paramInt;
      return;
    }
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesVR.diasCalculo = paramInt;
      return;
    }
    localStateManager.setIntField(paramInteresesVR, jdoInheritedFieldCount + 2, paramInteresesVR.diasCalculo, paramInt);
  }

  private static final Date jdoGetfinLapso(InteresesVR paramInteresesVR)
  {
    if (paramInteresesVR.jdoFlags <= 0)
      return paramInteresesVR.finLapso;
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesVR.finLapso;
    if (localStateManager.isLoaded(paramInteresesVR, jdoInheritedFieldCount + 3))
      return paramInteresesVR.finLapso;
    return (Date)localStateManager.getObjectField(paramInteresesVR, jdoInheritedFieldCount + 3, paramInteresesVR.finLapso);
  }

  private static final void jdoSetfinLapso(InteresesVR paramInteresesVR, Date paramDate)
  {
    if (paramInteresesVR.jdoFlags == 0)
    {
      paramInteresesVR.finLapso = paramDate;
      return;
    }
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesVR.finLapso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramInteresesVR, jdoInheritedFieldCount + 3, paramInteresesVR.finLapso, paramDate);
  }

  private static final long jdoGetidInteresesVR(InteresesVR paramInteresesVR)
  {
    return paramInteresesVR.idInteresesVR;
  }

  private static final void jdoSetidInteresesVR(InteresesVR paramInteresesVR, long paramLong)
  {
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesVR.idInteresesVR = paramLong;
      return;
    }
    localStateManager.setLongField(paramInteresesVR, jdoInheritedFieldCount + 4, paramInteresesVR.idInteresesVR, paramLong);
  }

  private static final Date jdoGetinicioLapso(InteresesVR paramInteresesVR)
  {
    if (paramInteresesVR.jdoFlags <= 0)
      return paramInteresesVR.inicioLapso;
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesVR.inicioLapso;
    if (localStateManager.isLoaded(paramInteresesVR, jdoInheritedFieldCount + 5))
      return paramInteresesVR.inicioLapso;
    return (Date)localStateManager.getObjectField(paramInteresesVR, jdoInheritedFieldCount + 5, paramInteresesVR.inicioLapso);
  }

  private static final void jdoSetinicioLapso(InteresesVR paramInteresesVR, Date paramDate)
  {
    if (paramInteresesVR.jdoFlags == 0)
    {
      paramInteresesVR.inicioLapso = paramDate;
      return;
    }
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesVR.inicioLapso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramInteresesVR, jdoInheritedFieldCount + 5, paramInteresesVR.inicioLapso, paramDate);
  }

  private static final double jdoGetinteresMensual(InteresesVR paramInteresesVR)
  {
    if (paramInteresesVR.jdoFlags <= 0)
      return paramInteresesVR.interesMensual;
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesVR.interesMensual;
    if (localStateManager.isLoaded(paramInteresesVR, jdoInheritedFieldCount + 6))
      return paramInteresesVR.interesMensual;
    return localStateManager.getDoubleField(paramInteresesVR, jdoInheritedFieldCount + 6, paramInteresesVR.interesMensual);
  }

  private static final void jdoSetinteresMensual(InteresesVR paramInteresesVR, double paramDouble)
  {
    if (paramInteresesVR.jdoFlags == 0)
    {
      paramInteresesVR.interesMensual = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesVR.interesMensual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesVR, jdoInheritedFieldCount + 6, paramInteresesVR.interesMensual, paramDouble);
  }

  private static final double jdoGetinteresesAcumulados(InteresesVR paramInteresesVR)
  {
    if (paramInteresesVR.jdoFlags <= 0)
      return paramInteresesVR.interesesAcumulados;
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesVR.interesesAcumulados;
    if (localStateManager.isLoaded(paramInteresesVR, jdoInheritedFieldCount + 7))
      return paramInteresesVR.interesesAcumulados;
    return localStateManager.getDoubleField(paramInteresesVR, jdoInheritedFieldCount + 7, paramInteresesVR.interesesAcumulados);
  }

  private static final void jdoSetinteresesAcumulados(InteresesVR paramInteresesVR, double paramDouble)
  {
    if (paramInteresesVR.jdoFlags == 0)
    {
      paramInteresesVR.interesesAcumulados = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesVR.interesesAcumulados = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesVR, jdoInheritedFieldCount + 7, paramInteresesVR.interesesAcumulados, paramDouble);
  }

  private static final double jdoGetlapso(InteresesVR paramInteresesVR)
  {
    if (paramInteresesVR.jdoFlags <= 0)
      return paramInteresesVR.lapso;
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesVR.lapso;
    if (localStateManager.isLoaded(paramInteresesVR, jdoInheritedFieldCount + 8))
      return paramInteresesVR.lapso;
    return localStateManager.getDoubleField(paramInteresesVR, jdoInheritedFieldCount + 8, paramInteresesVR.lapso);
  }

  private static final void jdoSetlapso(InteresesVR paramInteresesVR, double paramDouble)
  {
    if (paramInteresesVR.jdoFlags == 0)
    {
      paramInteresesVR.lapso = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesVR.lapso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesVR, jdoInheritedFieldCount + 8, paramInteresesVR.lapso, paramDouble);
  }

  private static final double jdoGettasaAplicada(InteresesVR paramInteresesVR)
  {
    if (paramInteresesVR.jdoFlags <= 0)
      return paramInteresesVR.tasaAplicada;
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesVR.tasaAplicada;
    if (localStateManager.isLoaded(paramInteresesVR, jdoInheritedFieldCount + 9))
      return paramInteresesVR.tasaAplicada;
    return localStateManager.getDoubleField(paramInteresesVR, jdoInheritedFieldCount + 9, paramInteresesVR.tasaAplicada);
  }

  private static final void jdoSettasaAplicada(InteresesVR paramInteresesVR, double paramDouble)
  {
    if (paramInteresesVR.jdoFlags == 0)
    {
      paramInteresesVR.tasaAplicada = paramDouble;
      return;
    }
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesVR.tasaAplicada = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramInteresesVR, jdoInheritedFieldCount + 9, paramInteresesVR.tasaAplicada, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(InteresesVR paramInteresesVR)
  {
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
      return paramInteresesVR.trabajador;
    if (localStateManager.isLoaded(paramInteresesVR, jdoInheritedFieldCount + 10))
      return paramInteresesVR.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramInteresesVR, jdoInheritedFieldCount + 10, paramInteresesVR.trabajador);
  }

  private static final void jdoSettrabajador(InteresesVR paramInteresesVR, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramInteresesVR.jdoStateManager;
    if (localStateManager == null)
    {
      paramInteresesVR.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramInteresesVR, jdoInheritedFieldCount + 10, paramInteresesVR.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}