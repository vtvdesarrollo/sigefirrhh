package sigefirrhh.personal.liquidacion;

import java.io.Serializable;

public class ResumenVRPK
  implements Serializable
{
  public long idResumenVR;

  public ResumenVRPK()
  {
  }

  public ResumenVRPK(long idResumenVR)
  {
    this.idResumenVR = idResumenVR;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ResumenVRPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ResumenVRPK thatPK)
  {
    return 
      this.idResumenVR == thatPK.idResumenVR;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idResumenVR)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idResumenVR);
  }
}