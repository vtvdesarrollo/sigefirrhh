package sigefirrhh.personal.liquidacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.personal.trabajador.Trabajador;

public class HistoricoDevengado
  implements Serializable, PersistenceCapable
{
  private long idHistoricoDevengado;
  private int anio;
  private int mes;
  private Date fechaConcepto;
  private double montoConcepto;
  private String tipoRegistro;
  private Concepto concepto;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "concepto", "fechaConcepto", "idHistoricoDevengado", "mes", "montoConcepto", "tipoRegistro", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 26, 21, 24, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public Concepto getConcepto()
  {
    return jdoGetconcepto(this);
  }

  public Date getFechaConcepto()
  {
    return jdoGetfechaConcepto(this);
  }

  public long getIdHistoricoDevengado()
  {
    return jdoGetidHistoricoDevengado(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public double getMontoConcepto()
  {
    return jdoGetmontoConcepto(this);
  }

  public String getTipoRegistro()
  {
    return jdoGettipoRegistro(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setConcepto(Concepto concepto)
  {
    jdoSetconcepto(this, concepto);
  }

  public void setFechaConcepto(Date date)
  {
    jdoSetfechaConcepto(this, date);
  }

  public void setIdHistoricoDevengado(long l)
  {
    jdoSetidHistoricoDevengado(this, l);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setMontoConcepto(double d)
  {
    jdoSetmontoConcepto(this, d);
  }

  public void setTipoRegistro(String string)
  {
    jdoSettipoRegistro(this, string);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.liquidacion.HistoricoDevengado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HistoricoDevengado());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HistoricoDevengado localHistoricoDevengado = new HistoricoDevengado();
    localHistoricoDevengado.jdoFlags = 1;
    localHistoricoDevengado.jdoStateManager = paramStateManager;
    return localHistoricoDevengado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HistoricoDevengado localHistoricoDevengado = new HistoricoDevengado();
    localHistoricoDevengado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHistoricoDevengado.jdoFlags = 1;
    localHistoricoDevengado.jdoStateManager = paramStateManager;
    return localHistoricoDevengado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.concepto);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaConcepto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHistoricoDevengado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoConcepto);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoRegistro);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.concepto = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaConcepto = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHistoricoDevengado = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoConcepto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoRegistro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HistoricoDevengado paramHistoricoDevengado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHistoricoDevengado == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramHistoricoDevengado.anio;
      return;
    case 1:
      if (paramHistoricoDevengado == null)
        throw new IllegalArgumentException("arg1");
      this.concepto = paramHistoricoDevengado.concepto;
      return;
    case 2:
      if (paramHistoricoDevengado == null)
        throw new IllegalArgumentException("arg1");
      this.fechaConcepto = paramHistoricoDevengado.fechaConcepto;
      return;
    case 3:
      if (paramHistoricoDevengado == null)
        throw new IllegalArgumentException("arg1");
      this.idHistoricoDevengado = paramHistoricoDevengado.idHistoricoDevengado;
      return;
    case 4:
      if (paramHistoricoDevengado == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramHistoricoDevengado.mes;
      return;
    case 5:
      if (paramHistoricoDevengado == null)
        throw new IllegalArgumentException("arg1");
      this.montoConcepto = paramHistoricoDevengado.montoConcepto;
      return;
    case 6:
      if (paramHistoricoDevengado == null)
        throw new IllegalArgumentException("arg1");
      this.tipoRegistro = paramHistoricoDevengado.tipoRegistro;
      return;
    case 7:
      if (paramHistoricoDevengado == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramHistoricoDevengado.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HistoricoDevengado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HistoricoDevengado localHistoricoDevengado = (HistoricoDevengado)paramObject;
    if (localHistoricoDevengado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHistoricoDevengado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HistoricoDevengadoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HistoricoDevengadoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoDevengadoPK))
      throw new IllegalArgumentException("arg1");
    HistoricoDevengadoPK localHistoricoDevengadoPK = (HistoricoDevengadoPK)paramObject;
    localHistoricoDevengadoPK.idHistoricoDevengado = this.idHistoricoDevengado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoDevengadoPK))
      throw new IllegalArgumentException("arg1");
    HistoricoDevengadoPK localHistoricoDevengadoPK = (HistoricoDevengadoPK)paramObject;
    this.idHistoricoDevengado = localHistoricoDevengadoPK.idHistoricoDevengado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoDevengadoPK))
      throw new IllegalArgumentException("arg2");
    HistoricoDevengadoPK localHistoricoDevengadoPK = (HistoricoDevengadoPK)paramObject;
    localHistoricoDevengadoPK.idHistoricoDevengado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoDevengadoPK))
      throw new IllegalArgumentException("arg2");
    HistoricoDevengadoPK localHistoricoDevengadoPK = (HistoricoDevengadoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localHistoricoDevengadoPK.idHistoricoDevengado);
  }

  private static final int jdoGetanio(HistoricoDevengado paramHistoricoDevengado)
  {
    if (paramHistoricoDevengado.jdoFlags <= 0)
      return paramHistoricoDevengado.anio;
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDevengado.anio;
    if (localStateManager.isLoaded(paramHistoricoDevengado, jdoInheritedFieldCount + 0))
      return paramHistoricoDevengado.anio;
    return localStateManager.getIntField(paramHistoricoDevengado, jdoInheritedFieldCount + 0, paramHistoricoDevengado.anio);
  }

  private static final void jdoSetanio(HistoricoDevengado paramHistoricoDevengado, int paramInt)
  {
    if (paramHistoricoDevengado.jdoFlags == 0)
    {
      paramHistoricoDevengado.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengado.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoDevengado, jdoInheritedFieldCount + 0, paramHistoricoDevengado.anio, paramInt);
  }

  private static final Concepto jdoGetconcepto(HistoricoDevengado paramHistoricoDevengado)
  {
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDevengado.concepto;
    if (localStateManager.isLoaded(paramHistoricoDevengado, jdoInheritedFieldCount + 1))
      return paramHistoricoDevengado.concepto;
    return (Concepto)localStateManager.getObjectField(paramHistoricoDevengado, jdoInheritedFieldCount + 1, paramHistoricoDevengado.concepto);
  }

  private static final void jdoSetconcepto(HistoricoDevengado paramHistoricoDevengado, Concepto paramConcepto)
  {
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengado.concepto = paramConcepto;
      return;
    }
    localStateManager.setObjectField(paramHistoricoDevengado, jdoInheritedFieldCount + 1, paramHistoricoDevengado.concepto, paramConcepto);
  }

  private static final Date jdoGetfechaConcepto(HistoricoDevengado paramHistoricoDevengado)
  {
    if (paramHistoricoDevengado.jdoFlags <= 0)
      return paramHistoricoDevengado.fechaConcepto;
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDevengado.fechaConcepto;
    if (localStateManager.isLoaded(paramHistoricoDevengado, jdoInheritedFieldCount + 2))
      return paramHistoricoDevengado.fechaConcepto;
    return (Date)localStateManager.getObjectField(paramHistoricoDevengado, jdoInheritedFieldCount + 2, paramHistoricoDevengado.fechaConcepto);
  }

  private static final void jdoSetfechaConcepto(HistoricoDevengado paramHistoricoDevengado, Date paramDate)
  {
    if (paramHistoricoDevengado.jdoFlags == 0)
    {
      paramHistoricoDevengado.fechaConcepto = paramDate;
      return;
    }
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengado.fechaConcepto = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistoricoDevengado, jdoInheritedFieldCount + 2, paramHistoricoDevengado.fechaConcepto, paramDate);
  }

  private static final long jdoGetidHistoricoDevengado(HistoricoDevengado paramHistoricoDevengado)
  {
    return paramHistoricoDevengado.idHistoricoDevengado;
  }

  private static final void jdoSetidHistoricoDevengado(HistoricoDevengado paramHistoricoDevengado, long paramLong)
  {
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengado.idHistoricoDevengado = paramLong;
      return;
    }
    localStateManager.setLongField(paramHistoricoDevengado, jdoInheritedFieldCount + 3, paramHistoricoDevengado.idHistoricoDevengado, paramLong);
  }

  private static final int jdoGetmes(HistoricoDevengado paramHistoricoDevengado)
  {
    if (paramHistoricoDevengado.jdoFlags <= 0)
      return paramHistoricoDevengado.mes;
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDevengado.mes;
    if (localStateManager.isLoaded(paramHistoricoDevengado, jdoInheritedFieldCount + 4))
      return paramHistoricoDevengado.mes;
    return localStateManager.getIntField(paramHistoricoDevengado, jdoInheritedFieldCount + 4, paramHistoricoDevengado.mes);
  }

  private static final void jdoSetmes(HistoricoDevengado paramHistoricoDevengado, int paramInt)
  {
    if (paramHistoricoDevengado.jdoFlags == 0)
    {
      paramHistoricoDevengado.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengado.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoDevengado, jdoInheritedFieldCount + 4, paramHistoricoDevengado.mes, paramInt);
  }

  private static final double jdoGetmontoConcepto(HistoricoDevengado paramHistoricoDevengado)
  {
    if (paramHistoricoDevengado.jdoFlags <= 0)
      return paramHistoricoDevengado.montoConcepto;
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDevengado.montoConcepto;
    if (localStateManager.isLoaded(paramHistoricoDevengado, jdoInheritedFieldCount + 5))
      return paramHistoricoDevengado.montoConcepto;
    return localStateManager.getDoubleField(paramHistoricoDevengado, jdoInheritedFieldCount + 5, paramHistoricoDevengado.montoConcepto);
  }

  private static final void jdoSetmontoConcepto(HistoricoDevengado paramHistoricoDevengado, double paramDouble)
  {
    if (paramHistoricoDevengado.jdoFlags == 0)
    {
      paramHistoricoDevengado.montoConcepto = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengado.montoConcepto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoDevengado, jdoInheritedFieldCount + 5, paramHistoricoDevengado.montoConcepto, paramDouble);
  }

  private static final String jdoGettipoRegistro(HistoricoDevengado paramHistoricoDevengado)
  {
    if (paramHistoricoDevengado.jdoFlags <= 0)
      return paramHistoricoDevengado.tipoRegistro;
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDevengado.tipoRegistro;
    if (localStateManager.isLoaded(paramHistoricoDevengado, jdoInheritedFieldCount + 6))
      return paramHistoricoDevengado.tipoRegistro;
    return localStateManager.getStringField(paramHistoricoDevengado, jdoInheritedFieldCount + 6, paramHistoricoDevengado.tipoRegistro);
  }

  private static final void jdoSettipoRegistro(HistoricoDevengado paramHistoricoDevengado, String paramString)
  {
    if (paramHistoricoDevengado.jdoFlags == 0)
    {
      paramHistoricoDevengado.tipoRegistro = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengado.tipoRegistro = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoDevengado, jdoInheritedFieldCount + 6, paramHistoricoDevengado.tipoRegistro, paramString);
  }

  private static final Trabajador jdoGettrabajador(HistoricoDevengado paramHistoricoDevengado)
  {
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDevengado.trabajador;
    if (localStateManager.isLoaded(paramHistoricoDevengado, jdoInheritedFieldCount + 7))
      return paramHistoricoDevengado.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramHistoricoDevengado, jdoInheritedFieldCount + 7, paramHistoricoDevengado.trabajador);
  }

  private static final void jdoSettrabajador(HistoricoDevengado paramHistoricoDevengado, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramHistoricoDevengado.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengado.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramHistoricoDevengado, jdoInheritedFieldCount + 7, paramHistoricoDevengado.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}