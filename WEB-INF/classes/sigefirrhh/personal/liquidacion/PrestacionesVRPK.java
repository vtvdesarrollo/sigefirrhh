package sigefirrhh.personal.liquidacion;

import java.io.Serializable;

public class PrestacionesVRPK
  implements Serializable
{
  public long idPrestacionesVR;

  public PrestacionesVRPK()
  {
  }

  public PrestacionesVRPK(long idPrestacionesVR)
  {
    this.idPrestacionesVR = idPrestacionesVR;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PrestacionesVRPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PrestacionesVRPK thatPK)
  {
    return 
      this.idPrestacionesVR == thatPK.idPrestacionesVR;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPrestacionesVR)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPrestacionesVR);
  }
}