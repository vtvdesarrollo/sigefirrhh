package sigefirrhh.personal.liquidacion;

import java.io.Serializable;

public class PrestacionesNRPK
  implements Serializable
{
  public long idPrestacionesNR;

  public PrestacionesNRPK()
  {
  }

  public PrestacionesNRPK(long idPrestacionesNR)
  {
    this.idPrestacionesNR = idPrestacionesNR;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PrestacionesNRPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PrestacionesNRPK thatPK)
  {
    return 
      this.idPrestacionesNR == thatPK.idPrestacionesNR;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPrestacionesNR)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPrestacionesNR);
  }
}