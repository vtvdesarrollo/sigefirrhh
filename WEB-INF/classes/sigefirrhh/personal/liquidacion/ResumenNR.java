package sigefirrhh.personal.liquidacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class ResumenNR
  implements Serializable, PersistenceCapable
{
  private long idResumenNR;
  private double totalPrestaciones;
  private double totalAdicionales;
  private double totalAnticipos;
  private double totalIntereses;
  private double totalFideicomiso;
  private double diasAlicuotaUtilidades;
  private double fraccion108Lot;
  private double diasFraccion108Lot;
  private double diasAdicAbonar;
  private double montoAdicAbonar;
  private double diasArt365;
  private double montoArt365;
  private double montoArt107;
  private double montoArt125;
  private double montoArt110;
  private double diasVacPendiente;
  private double montoVacPendiente;
  private double diasVacFracc;
  private double montoVacFracc;
  private double diasBonVacFracc;
  private double montoBonVacFracc;
  private double diasBonFinFracc;
  private double montoBonFinFracc;
  private double montoRural;
  private double montoAdministrativo;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "diasAdicAbonar", "diasAlicuotaUtilidades", "diasArt365", "diasBonFinFracc", "diasBonVacFracc", "diasFraccion108Lot", "diasVacFracc", "diasVacPendiente", "fraccion108Lot", "idResumenNR", "montoAdicAbonar", "montoAdministrativo", "montoArt107", "montoArt110", "montoArt125", "montoArt365", "montoBonFinFracc", "montoBonVacFracc", "montoRural", "montoVacFracc", "montoVacPendiente", "totalAdicionales", "totalAnticipos", "totalFideicomiso", "totalIntereses", "totalPrestaciones", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Long.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public double getDiasAdicAbonar()
  {
    return jdoGetdiasAdicAbonar(this);
  }

  public double getDiasAlicuotaUtilidades()
  {
    return jdoGetdiasAlicuotaUtilidades(this);
  }

  public double getDiasArt365()
  {
    return jdoGetdiasArt365(this);
  }

  public double getDiasBonFinFracc()
  {
    return jdoGetdiasBonFinFracc(this);
  }

  public double getDiasBonVacFracc()
  {
    return jdoGetdiasBonVacFracc(this);
  }

  public double getDiasFraccion108Lot()
  {
    return jdoGetdiasFraccion108Lot(this);
  }

  public double getDiasVacFracc()
  {
    return jdoGetdiasVacFracc(this);
  }

  public double getDiasVacPendiente()
  {
    return jdoGetdiasVacPendiente(this);
  }

  public double getFraccion108Lot()
  {
    return jdoGetfraccion108Lot(this);
  }

  public long getIdResumenNR()
  {
    return jdoGetidResumenNR(this);
  }

  public double getMontoAdicAbonar()
  {
    return jdoGetmontoAdicAbonar(this);
  }

  public double getMontoAdministrativo()
  {
    return jdoGetmontoAdministrativo(this);
  }

  public double getMontoArt107()
  {
    return jdoGetmontoArt107(this);
  }

  public double getMontoArt110()
  {
    return jdoGetmontoArt110(this);
  }

  public double getMontoArt125()
  {
    return jdoGetmontoArt125(this);
  }

  public double getMontoArt365()
  {
    return jdoGetmontoArt365(this);
  }

  public double getMontoBonFinFracc()
  {
    return jdoGetmontoBonFinFracc(this);
  }

  public double getMontoBonVacFracc()
  {
    return jdoGetmontoBonVacFracc(this);
  }

  public double getMontoRural()
  {
    return jdoGetmontoRural(this);
  }

  public double getMontoVacFracc()
  {
    return jdoGetmontoVacFracc(this);
  }

  public double getMontoVacPendiente()
  {
    return jdoGetmontoVacPendiente(this);
  }

  public double getTotalAdicionales()
  {
    return jdoGettotalAdicionales(this);
  }

  public double getTotalAnticipos()
  {
    return jdoGettotalAnticipos(this);
  }

  public double getTotalFideicomiso()
  {
    return jdoGettotalFideicomiso(this);
  }

  public double getTotalIntereses()
  {
    return jdoGettotalIntereses(this);
  }

  public double getTotalPrestaciones()
  {
    return jdoGettotalPrestaciones(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setDiasAdicAbonar(double d)
  {
    jdoSetdiasAdicAbonar(this, d);
  }

  public void setDiasAlicuotaUtilidades(double d)
  {
    jdoSetdiasAlicuotaUtilidades(this, d);
  }

  public void setDiasArt365(double d)
  {
    jdoSetdiasArt365(this, d);
  }

  public void setDiasBonFinFracc(double d)
  {
    jdoSetdiasBonFinFracc(this, d);
  }

  public void setDiasBonVacFracc(double d)
  {
    jdoSetdiasBonVacFracc(this, d);
  }

  public void setDiasFraccion108Lot(double d)
  {
    jdoSetdiasFraccion108Lot(this, d);
  }

  public void setDiasVacFracc(double d)
  {
    jdoSetdiasVacFracc(this, d);
  }

  public void setDiasVacPendiente(double d)
  {
    jdoSetdiasVacPendiente(this, d);
  }

  public void setFraccion108Lot(double d)
  {
    jdoSetfraccion108Lot(this, d);
  }

  public void setIdResumenNR(long l)
  {
    jdoSetidResumenNR(this, l);
  }

  public void setMontoAdicAbonar(double d)
  {
    jdoSetmontoAdicAbonar(this, d);
  }

  public void setMontoAdministrativo(double d)
  {
    jdoSetmontoAdministrativo(this, d);
  }

  public void setMontoArt107(double d)
  {
    jdoSetmontoArt107(this, d);
  }

  public void setMontoArt110(double d)
  {
    jdoSetmontoArt110(this, d);
  }

  public void setMontoArt125(double d)
  {
    jdoSetmontoArt125(this, d);
  }

  public void setMontoArt365(double d)
  {
    jdoSetmontoArt365(this, d);
  }

  public void setMontoBonFinFracc(double d)
  {
    jdoSetmontoBonFinFracc(this, d);
  }

  public void setMontoBonVacFracc(double d)
  {
    jdoSetmontoBonVacFracc(this, d);
  }

  public void setMontoRural(double d)
  {
    jdoSetmontoRural(this, d);
  }

  public void setMontoVacFracc(double d)
  {
    jdoSetmontoVacFracc(this, d);
  }

  public void setMontoVacPendiente(double d)
  {
    jdoSetmontoVacPendiente(this, d);
  }

  public void setTotalAdicionales(double d)
  {
    jdoSettotalAdicionales(this, d);
  }

  public void setTotalAnticipos(double d)
  {
    jdoSettotalAnticipos(this, d);
  }

  public void setTotalFideicomiso(double d)
  {
    jdoSettotalFideicomiso(this, d);
  }

  public void setTotalIntereses(double d)
  {
    jdoSettotalIntereses(this, d);
  }

  public void setTotalPrestaciones(double d)
  {
    jdoSettotalPrestaciones(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 27;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.liquidacion.ResumenNR"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ResumenNR());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ResumenNR localResumenNR = new ResumenNR();
    localResumenNR.jdoFlags = 1;
    localResumenNR.jdoStateManager = paramStateManager;
    return localResumenNR;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ResumenNR localResumenNR = new ResumenNR();
    localResumenNR.jdoCopyKeyFieldsFromObjectId(paramObject);
    localResumenNR.jdoFlags = 1;
    localResumenNR.jdoStateManager = paramStateManager;
    return localResumenNR;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasAdicAbonar);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasAlicuotaUtilidades);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasArt365);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasBonFinFracc);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasBonVacFracc);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasFraccion108Lot);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasVacFracc);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasVacPendiente);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.fraccion108Lot);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idResumenNR);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAdicAbonar);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAdministrativo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoArt107);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoArt110);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoArt125);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoArt365);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoBonFinFracc);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoBonVacFracc);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoRural);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoVacFracc);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoVacPendiente);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalAdicionales);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalAnticipos);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalFideicomiso);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalIntereses);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalPrestaciones);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasAdicAbonar = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasAlicuotaUtilidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasArt365 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasBonFinFracc = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasBonVacFracc = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasFraccion108Lot = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasVacFracc = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasVacPendiente = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fraccion108Lot = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idResumenNR = localStateManager.replacingLongField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAdicAbonar = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAdministrativo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoArt107 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoArt110 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoArt125 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoArt365 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoBonFinFracc = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoBonVacFracc = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoRural = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoVacFracc = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoVacPendiente = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalAdicionales = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalAnticipos = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalFideicomiso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalIntereses = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalPrestaciones = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ResumenNR paramResumenNR, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.diasAdicAbonar = paramResumenNR.diasAdicAbonar;
      return;
    case 1:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.diasAlicuotaUtilidades = paramResumenNR.diasAlicuotaUtilidades;
      return;
    case 2:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.diasArt365 = paramResumenNR.diasArt365;
      return;
    case 3:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.diasBonFinFracc = paramResumenNR.diasBonFinFracc;
      return;
    case 4:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.diasBonVacFracc = paramResumenNR.diasBonVacFracc;
      return;
    case 5:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.diasFraccion108Lot = paramResumenNR.diasFraccion108Lot;
      return;
    case 6:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.diasVacFracc = paramResumenNR.diasVacFracc;
      return;
    case 7:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.diasVacPendiente = paramResumenNR.diasVacPendiente;
      return;
    case 8:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.fraccion108Lot = paramResumenNR.fraccion108Lot;
      return;
    case 9:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.idResumenNR = paramResumenNR.idResumenNR;
      return;
    case 10:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoAdicAbonar = paramResumenNR.montoAdicAbonar;
      return;
    case 11:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoAdministrativo = paramResumenNR.montoAdministrativo;
      return;
    case 12:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoArt107 = paramResumenNR.montoArt107;
      return;
    case 13:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoArt110 = paramResumenNR.montoArt110;
      return;
    case 14:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoArt125 = paramResumenNR.montoArt125;
      return;
    case 15:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoArt365 = paramResumenNR.montoArt365;
      return;
    case 16:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoBonFinFracc = paramResumenNR.montoBonFinFracc;
      return;
    case 17:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoBonVacFracc = paramResumenNR.montoBonVacFracc;
      return;
    case 18:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoRural = paramResumenNR.montoRural;
      return;
    case 19:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoVacFracc = paramResumenNR.montoVacFracc;
      return;
    case 20:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.montoVacPendiente = paramResumenNR.montoVacPendiente;
      return;
    case 21:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.totalAdicionales = paramResumenNR.totalAdicionales;
      return;
    case 22:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.totalAnticipos = paramResumenNR.totalAnticipos;
      return;
    case 23:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.totalFideicomiso = paramResumenNR.totalFideicomiso;
      return;
    case 24:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.totalIntereses = paramResumenNR.totalIntereses;
      return;
    case 25:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.totalPrestaciones = paramResumenNR.totalPrestaciones;
      return;
    case 26:
      if (paramResumenNR == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramResumenNR.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ResumenNR))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ResumenNR localResumenNR = (ResumenNR)paramObject;
    if (localResumenNR.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localResumenNR, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ResumenNRPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ResumenNRPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ResumenNRPK))
      throw new IllegalArgumentException("arg1");
    ResumenNRPK localResumenNRPK = (ResumenNRPK)paramObject;
    localResumenNRPK.idResumenNR = this.idResumenNR;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ResumenNRPK))
      throw new IllegalArgumentException("arg1");
    ResumenNRPK localResumenNRPK = (ResumenNRPK)paramObject;
    this.idResumenNR = localResumenNRPK.idResumenNR;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ResumenNRPK))
      throw new IllegalArgumentException("arg2");
    ResumenNRPK localResumenNRPK = (ResumenNRPK)paramObject;
    localResumenNRPK.idResumenNR = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 9);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ResumenNRPK))
      throw new IllegalArgumentException("arg2");
    ResumenNRPK localResumenNRPK = (ResumenNRPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 9, localResumenNRPK.idResumenNR);
  }

  private static final double jdoGetdiasAdicAbonar(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.diasAdicAbonar;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.diasAdicAbonar;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 0))
      return paramResumenNR.diasAdicAbonar;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 0, paramResumenNR.diasAdicAbonar);
  }

  private static final void jdoSetdiasAdicAbonar(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.diasAdicAbonar = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.diasAdicAbonar = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 0, paramResumenNR.diasAdicAbonar, paramDouble);
  }

  private static final double jdoGetdiasAlicuotaUtilidades(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.diasAlicuotaUtilidades;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.diasAlicuotaUtilidades;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 1))
      return paramResumenNR.diasAlicuotaUtilidades;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 1, paramResumenNR.diasAlicuotaUtilidades);
  }

  private static final void jdoSetdiasAlicuotaUtilidades(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.diasAlicuotaUtilidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.diasAlicuotaUtilidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 1, paramResumenNR.diasAlicuotaUtilidades, paramDouble);
  }

  private static final double jdoGetdiasArt365(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.diasArt365;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.diasArt365;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 2))
      return paramResumenNR.diasArt365;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 2, paramResumenNR.diasArt365);
  }

  private static final void jdoSetdiasArt365(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.diasArt365 = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.diasArt365 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 2, paramResumenNR.diasArt365, paramDouble);
  }

  private static final double jdoGetdiasBonFinFracc(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.diasBonFinFracc;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.diasBonFinFracc;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 3))
      return paramResumenNR.diasBonFinFracc;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 3, paramResumenNR.diasBonFinFracc);
  }

  private static final void jdoSetdiasBonFinFracc(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.diasBonFinFracc = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.diasBonFinFracc = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 3, paramResumenNR.diasBonFinFracc, paramDouble);
  }

  private static final double jdoGetdiasBonVacFracc(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.diasBonVacFracc;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.diasBonVacFracc;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 4))
      return paramResumenNR.diasBonVacFracc;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 4, paramResumenNR.diasBonVacFracc);
  }

  private static final void jdoSetdiasBonVacFracc(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.diasBonVacFracc = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.diasBonVacFracc = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 4, paramResumenNR.diasBonVacFracc, paramDouble);
  }

  private static final double jdoGetdiasFraccion108Lot(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.diasFraccion108Lot;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.diasFraccion108Lot;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 5))
      return paramResumenNR.diasFraccion108Lot;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 5, paramResumenNR.diasFraccion108Lot);
  }

  private static final void jdoSetdiasFraccion108Lot(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.diasFraccion108Lot = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.diasFraccion108Lot = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 5, paramResumenNR.diasFraccion108Lot, paramDouble);
  }

  private static final double jdoGetdiasVacFracc(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.diasVacFracc;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.diasVacFracc;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 6))
      return paramResumenNR.diasVacFracc;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 6, paramResumenNR.diasVacFracc);
  }

  private static final void jdoSetdiasVacFracc(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.diasVacFracc = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.diasVacFracc = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 6, paramResumenNR.diasVacFracc, paramDouble);
  }

  private static final double jdoGetdiasVacPendiente(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.diasVacPendiente;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.diasVacPendiente;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 7))
      return paramResumenNR.diasVacPendiente;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 7, paramResumenNR.diasVacPendiente);
  }

  private static final void jdoSetdiasVacPendiente(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.diasVacPendiente = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.diasVacPendiente = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 7, paramResumenNR.diasVacPendiente, paramDouble);
  }

  private static final double jdoGetfraccion108Lot(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.fraccion108Lot;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.fraccion108Lot;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 8))
      return paramResumenNR.fraccion108Lot;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 8, paramResumenNR.fraccion108Lot);
  }

  private static final void jdoSetfraccion108Lot(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.fraccion108Lot = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.fraccion108Lot = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 8, paramResumenNR.fraccion108Lot, paramDouble);
  }

  private static final long jdoGetidResumenNR(ResumenNR paramResumenNR)
  {
    return paramResumenNR.idResumenNR;
  }

  private static final void jdoSetidResumenNR(ResumenNR paramResumenNR, long paramLong)
  {
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.idResumenNR = paramLong;
      return;
    }
    localStateManager.setLongField(paramResumenNR, jdoInheritedFieldCount + 9, paramResumenNR.idResumenNR, paramLong);
  }

  private static final double jdoGetmontoAdicAbonar(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.montoAdicAbonar;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.montoAdicAbonar;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 10))
      return paramResumenNR.montoAdicAbonar;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 10, paramResumenNR.montoAdicAbonar);
  }

  private static final void jdoSetmontoAdicAbonar(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.montoAdicAbonar = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.montoAdicAbonar = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 10, paramResumenNR.montoAdicAbonar, paramDouble);
  }

  private static final double jdoGetmontoAdministrativo(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.montoAdministrativo;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.montoAdministrativo;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 11))
      return paramResumenNR.montoAdministrativo;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 11, paramResumenNR.montoAdministrativo);
  }

  private static final void jdoSetmontoAdministrativo(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.montoAdministrativo = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.montoAdministrativo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 11, paramResumenNR.montoAdministrativo, paramDouble);
  }

  private static final double jdoGetmontoArt107(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.montoArt107;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.montoArt107;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 12))
      return paramResumenNR.montoArt107;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 12, paramResumenNR.montoArt107);
  }

  private static final void jdoSetmontoArt107(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.montoArt107 = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.montoArt107 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 12, paramResumenNR.montoArt107, paramDouble);
  }

  private static final double jdoGetmontoArt110(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.montoArt110;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.montoArt110;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 13))
      return paramResumenNR.montoArt110;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 13, paramResumenNR.montoArt110);
  }

  private static final void jdoSetmontoArt110(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.montoArt110 = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.montoArt110 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 13, paramResumenNR.montoArt110, paramDouble);
  }

  private static final double jdoGetmontoArt125(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.montoArt125;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.montoArt125;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 14))
      return paramResumenNR.montoArt125;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 14, paramResumenNR.montoArt125);
  }

  private static final void jdoSetmontoArt125(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.montoArt125 = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.montoArt125 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 14, paramResumenNR.montoArt125, paramDouble);
  }

  private static final double jdoGetmontoArt365(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.montoArt365;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.montoArt365;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 15))
      return paramResumenNR.montoArt365;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 15, paramResumenNR.montoArt365);
  }

  private static final void jdoSetmontoArt365(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.montoArt365 = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.montoArt365 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 15, paramResumenNR.montoArt365, paramDouble);
  }

  private static final double jdoGetmontoBonFinFracc(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.montoBonFinFracc;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.montoBonFinFracc;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 16))
      return paramResumenNR.montoBonFinFracc;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 16, paramResumenNR.montoBonFinFracc);
  }

  private static final void jdoSetmontoBonFinFracc(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.montoBonFinFracc = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.montoBonFinFracc = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 16, paramResumenNR.montoBonFinFracc, paramDouble);
  }

  private static final double jdoGetmontoBonVacFracc(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.montoBonVacFracc;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.montoBonVacFracc;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 17))
      return paramResumenNR.montoBonVacFracc;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 17, paramResumenNR.montoBonVacFracc);
  }

  private static final void jdoSetmontoBonVacFracc(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.montoBonVacFracc = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.montoBonVacFracc = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 17, paramResumenNR.montoBonVacFracc, paramDouble);
  }

  private static final double jdoGetmontoRural(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.montoRural;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.montoRural;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 18))
      return paramResumenNR.montoRural;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 18, paramResumenNR.montoRural);
  }

  private static final void jdoSetmontoRural(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.montoRural = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.montoRural = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 18, paramResumenNR.montoRural, paramDouble);
  }

  private static final double jdoGetmontoVacFracc(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.montoVacFracc;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.montoVacFracc;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 19))
      return paramResumenNR.montoVacFracc;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 19, paramResumenNR.montoVacFracc);
  }

  private static final void jdoSetmontoVacFracc(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.montoVacFracc = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.montoVacFracc = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 19, paramResumenNR.montoVacFracc, paramDouble);
  }

  private static final double jdoGetmontoVacPendiente(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.montoVacPendiente;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.montoVacPendiente;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 20))
      return paramResumenNR.montoVacPendiente;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 20, paramResumenNR.montoVacPendiente);
  }

  private static final void jdoSetmontoVacPendiente(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.montoVacPendiente = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.montoVacPendiente = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 20, paramResumenNR.montoVacPendiente, paramDouble);
  }

  private static final double jdoGettotalAdicionales(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.totalAdicionales;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.totalAdicionales;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 21))
      return paramResumenNR.totalAdicionales;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 21, paramResumenNR.totalAdicionales);
  }

  private static final void jdoSettotalAdicionales(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.totalAdicionales = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.totalAdicionales = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 21, paramResumenNR.totalAdicionales, paramDouble);
  }

  private static final double jdoGettotalAnticipos(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.totalAnticipos;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.totalAnticipos;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 22))
      return paramResumenNR.totalAnticipos;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 22, paramResumenNR.totalAnticipos);
  }

  private static final void jdoSettotalAnticipos(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.totalAnticipos = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.totalAnticipos = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 22, paramResumenNR.totalAnticipos, paramDouble);
  }

  private static final double jdoGettotalFideicomiso(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.totalFideicomiso;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.totalFideicomiso;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 23))
      return paramResumenNR.totalFideicomiso;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 23, paramResumenNR.totalFideicomiso);
  }

  private static final void jdoSettotalFideicomiso(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.totalFideicomiso = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.totalFideicomiso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 23, paramResumenNR.totalFideicomiso, paramDouble);
  }

  private static final double jdoGettotalIntereses(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.totalIntereses;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.totalIntereses;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 24))
      return paramResumenNR.totalIntereses;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 24, paramResumenNR.totalIntereses);
  }

  private static final void jdoSettotalIntereses(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.totalIntereses = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.totalIntereses = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 24, paramResumenNR.totalIntereses, paramDouble);
  }

  private static final double jdoGettotalPrestaciones(ResumenNR paramResumenNR)
  {
    if (paramResumenNR.jdoFlags <= 0)
      return paramResumenNR.totalPrestaciones;
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.totalPrestaciones;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 25))
      return paramResumenNR.totalPrestaciones;
    return localStateManager.getDoubleField(paramResumenNR, jdoInheritedFieldCount + 25, paramResumenNR.totalPrestaciones);
  }

  private static final void jdoSettotalPrestaciones(ResumenNR paramResumenNR, double paramDouble)
  {
    if (paramResumenNR.jdoFlags == 0)
    {
      paramResumenNR.totalPrestaciones = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.totalPrestaciones = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenNR, jdoInheritedFieldCount + 25, paramResumenNR.totalPrestaciones, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(ResumenNR paramResumenNR)
  {
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
      return paramResumenNR.trabajador;
    if (localStateManager.isLoaded(paramResumenNR, jdoInheritedFieldCount + 26))
      return paramResumenNR.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramResumenNR, jdoInheritedFieldCount + 26, paramResumenNR.trabajador);
  }

  private static final void jdoSettrabajador(ResumenNR paramResumenNR, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramResumenNR.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenNR.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramResumenNR, jdoInheritedFieldCount + 26, paramResumenNR.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}