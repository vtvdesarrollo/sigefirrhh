package sigefirrhh.personal.liquidacion;

import java.io.Serializable;

public class HistoricoDetallePK
  implements Serializable
{
  public long idHistoricoDetalle;

  public HistoricoDetallePK()
  {
  }

  public HistoricoDetallePK(long idHistoricoDetalle)
  {
    this.idHistoricoDetalle = idHistoricoDetalle;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HistoricoDetallePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HistoricoDetallePK thatPK)
  {
    return 
      this.idHistoricoDetalle == thatPK.idHistoricoDetalle;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHistoricoDetalle)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHistoricoDetalle);
  }
}