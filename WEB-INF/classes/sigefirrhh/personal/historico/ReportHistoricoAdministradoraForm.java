package sigefirrhh.personal.historico;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportHistoricoAdministradoraForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportHistoricoAdministradoraForm.class.getName());
  private int reportId;
  private long idUnidadAdministradora;
  private String idTipoPersonal;
  private String reportName;
  private String orden;
  private int mes;
  private int anio;
  private String formato = "1";
  private String categoria = "S";
  private Collection listUnidadAdministradora;
  private Collection listTipoPersonal;
  private EstructuraFacade estructuraFacade;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private Collection colConceptoTipoPersonal;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private boolean showConcepto;
  private String idConceptoTipoPersonal;

  public boolean isShowConcepto()
  {
    return this.showConcepto;
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    this.showConcepto = false;
    try {
      if (idTipoPersonal > 0L) {
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
        this.colConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(idTipoPersonal);
        this.showConcepto = true;
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = null;
    }
  }

  public void changeConceptoTipoPersonal(ValueChangeEvent event) { long idConceptoTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.conceptoTipoPersonal = null;
      if (idConceptoTipoPersonal > 0L)
        this.conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(idConceptoTipoPersonal);
      else
        this.conceptoTipoPersonal = null;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.colConceptoTipoPersonal = null;
    } }

  public Collection getColConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }

    return col;
  }
  public ReportHistoricoAdministradoraForm() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "histsemmesuacatalf";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportHistoricoAdministradoraForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      Date fechaActual = new Date();
      this.anio = (fechaActual.getYear() + 1900);
      this.listUnidadAdministradora = this.estructuraFacade.findAllUnidadAdministradora();

      this.listTipoPersonal = this.definicionesFacade.findAllTipoPersonal();
    } catch (Exception e) {
      this.listUnidadAdministradora = new ArrayList();
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      if (this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"))
        this.reportName = "histsem";
      else {
        this.reportName = "histqui";
      }
      this.reportName += "mesua";

      if (this.idUnidadAdministradora != 0L) {
        this.reportName += "1";
      }
      if (this.categoria.equals("S")) {
        this.reportName += "cat";
      }

      if (this.orden.equals("A"))
        this.reportName += "alf";
      else if (this.orden.equals("C"))
        this.reportName += "ced";
      else {
        this.reportName += "cod";
      }

      if (this.formato.equals("2"))
        this.reportName = ("a_" + this.reportName);
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    try
    {
      if (this.tipoPersonal == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar un Tipo de Personal", ""));
        return null;
      }

      if (this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"))
        this.reportName = "histsem";
      else {
        this.reportName = "histqui";
      }
      this.reportName += "mesua";

      if (this.idUnidadAdministradora != 0L) {
        this.reportName += "1";
      }
      if (this.categoria.equals("S")) {
        this.reportName += "cat";
      }

      if (this.orden.equals("A"))
        this.reportName += "alf";
      else if (this.orden.equals("C"))
        this.reportName += "ced";
      else {
        this.reportName += "cod";
      }

      if (this.formato.equals("2")) {
        this.reportName = ("a_" + this.reportName);
      }

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));
      parameters.put("id_concepto_tipo_personal", new Long(this.conceptoTipoPersonal.getIdConceptoTipoPersonal()));

      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      if (this.idUnidadAdministradora != 0L) {
        parameters.put("id_unidad_administradora", new Long(this.idUnidadAdministradora));
      }

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/historico");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String orden) {
    this.orden = orden;
  }

  public long getIdUnidadAdministradora() {
    return this.idUnidadAdministradora;
  }

  public void setIdUnidadAdministradora(long idUnidadAdministradora) {
    this.idUnidadAdministradora = idUnidadAdministradora;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }

  public String getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(String idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
  public String getIdConceptoTipoPersonal() {
    return this.idConceptoTipoPersonal;
  }
  public void setIdConceptoTipoPersonal(String idConceptoTipoPersonal) {
    this.idConceptoTipoPersonal = idConceptoTipoPersonal;
  }

  public String getCategoria() {
    return this.categoria;
  }

  public void setCategoria(String categoria) {
    this.categoria = categoria;
  }
}