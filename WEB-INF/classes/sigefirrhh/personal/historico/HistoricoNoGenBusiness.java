package sigefirrhh.personal.historico;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class HistoricoNoGenBusiness extends HistoricoBusiness
  implements Serializable
{
  private HistoricoQuincenaNoGenBeanBusiness historicoQuincenaNoGenBeanBusiness = new HistoricoQuincenaNoGenBeanBusiness();
  private HistoricoSemanaNoGenBeanBusiness historicoSemanaNoGenBeanBusiness = new HistoricoSemanaNoGenBeanBusiness();

  public Collection findHistoricoQuincenaByConceptoTipoPersonalAndTrabajadorAndFecha(long idTipoPersonal, long idTrabajador, Date fechaInicio, Date fechaFin) throws Exception
  {
    return this.historicoQuincenaNoGenBeanBusiness.findByConceptoTipoPersonalAndTrabajadorAndFecha(idTipoPersonal, idTrabajador, fechaInicio, fechaFin);
  }

  public Collection findHistoricoQuincenaByConceptoTipoPersonalAndTrabajador(long idTipoPersonal, long idTrabajador, int anio, int mes, int semanaQuincena) throws Exception {
    return this.historicoQuincenaNoGenBeanBusiness.findByConceptoTipoPersonalAndTrabajador(idTipoPersonal, idTrabajador, anio, mes, semanaQuincena);
  }

  public Collection findHistoricoSemanaByConceptoTipoPersonalAndTrabajador(long idTipoPersonal, long idTrabajador, int anio, int mes, int semanaQuincena) throws Exception {
    return this.historicoSemanaNoGenBeanBusiness.findByConceptoTipoPersonalAndTrabajador(idTipoPersonal, idTrabajador, anio, mes, semanaQuincena);
  }

  public Collection findHistoricoQuincenaByTrabajadorAndNumeroNomina(long idTrabajador, int numeroNomina) throws Exception {
    return this.historicoQuincenaNoGenBeanBusiness.findByTrabajadorAndNumeroNomina(idTrabajador, numeroNomina);
  }

  public Collection findHistoricoSemanaByTrabajadorAndNumeroNomina(long idTrabajador, int numeroNomina) throws Exception {
    return this.historicoSemanaNoGenBeanBusiness.findByTrabajadorAndNumeroNomina(idTrabajador, numeroNomina);
  }
}