package sigefirrhh.personal.historico;

import java.io.Serializable;

public class HistoricoNominaPK
  implements Serializable
{
  public long idHistoricoNomina;

  public HistoricoNominaPK()
  {
  }

  public HistoricoNominaPK(long idHistoricoNomina)
  {
    this.idHistoricoNomina = idHistoricoNomina;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HistoricoNominaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HistoricoNominaPK thatPK)
  {
    return 
      this.idHistoricoNomina == thatPK.idHistoricoNomina;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHistoricoNomina)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHistoricoNomina);
  }
}