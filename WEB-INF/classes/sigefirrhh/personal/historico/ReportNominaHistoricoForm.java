package sigefirrhh.personal.historico;

import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.Semana;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.procesoNomina.ProcesoNominaNoGenFacade;

public class ReportNominaHistoricoForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportNominaHistoricoForm.class.getName());

  private int tipoReporte = 1;
  private int reportId;
  private String reportName;
  private String selectGrupoNomina;
  private String inicio;
  private String fin;
  private Calendar inicioAux = Calendar.getInstance();
  private Calendar finAux = Calendar.getInstance();
  private long idGrupoNomina;
  private Collection listGrupoNomina;
  private long idUnidadAdministradora;
  private Collection listUnidadAdministradora;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private boolean show;
  private boolean auxShow;
  private boolean showSemana = true;
  private boolean showNominaEspecial;
  private boolean showAnioMes;
  private boolean showQuincena = true;
  private boolean showPeriodo = true;
  private Collection listNominaEspecial;
  private GrupoNomina grupoNomina;
  private String tipoNomina = "O";
  private String selectNominaEspecial;
  private long idNominaEspecial;
  private NominaEspecial nominaEspecial;
  private boolean showGeneral;
  private int anio = 0;
  private int mes = 0;
  private int semana = 1;
  private int quincena = 1;
  private int numero_nomina = 0;
  private String estatus = "A";

  public ReportNominaHistoricoForm()
  {
    this.reportName = "nominarac";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportNominaHistoricoForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void changeNominaEspecial(ValueChangeEvent event)
  {
    this.idNominaEspecial = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.nominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialById(this.idNominaEspecial);
      this.inicioAux.setTime(this.nominaEspecial.getFechaInicio());
      this.finAux.setTime(this.nominaEspecial.getFechaFin());
      this.estatus = this.nominaEspecial.getPersonal();
      this.numero_nomina = this.nominaEspecial.getNumeroNomina();
      this.anio = this.nominaEspecial.getAnio();
      this.mes = this.nominaEspecial.getMes();
      this.auxShow = true;
      log.error("anio especial " + this.nominaEspecial.getAnio());
      log.error("mes especial " + this.nominaEspecial.getMes());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeGrupoNomina(ValueChangeEvent event) { this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.showGeneral = false;
      this.showSemana = false;
      this.showQuincena = false;

      if (this.idGrupoNomina != 0L) {
        this.showGeneral = true;

        this.grupoNomina = this.definicionesFacade.findGrupoNominaById(this.idGrupoNomina);
        if (this.grupoNomina.getPeriodicidad().equals("S"))
          this.showSemana = true;
        else
          this.showQuincena = true;
      }
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoNomina(ValueChangeEvent event)
  {
    String tipoNomina = (String)event.getNewValue();
    try {
      this.auxShow = false;
      this.showNominaEspecial = false;
      this.showSemana = false;
      this.showPeriodo = false;
      this.showQuincena = false;
      this.showGeneral = true;
      if (!tipoNomina.equals("O")) {
        this.showSemana = false;
        this.showPeriodo = false;
        this.showQuincena = false;
        llenarNominaEspecial(this.idGrupoNomina);
      } else {
        this.showPeriodo = true;
        if (this.grupoNomina.getPeriodicidad().equals("S"))
          this.showSemana = true;
        else
          this.showQuincena = true;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  private void llenarNominaEspecial(long idGrupoNomina) {
    try {
      this.listNominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialByGrupoNominaAndEstatus(idGrupoNomina, "P");

      this.showNominaEspecial = true;
    } catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  private void cambiarNombreAReporte() {
    if (this.tipoReporte == 1)
      this.reportName = "nominarac";
    else if (this.tipoReporte == 2)
      this.reportName = "nominaalf";
    else if (this.tipoReporte == 3)
      this.reportName = "nominauel";
    else if (this.tipoReporte == 4)
      this.reportName = "detconceptonoalf";
    else if (this.tipoReporte == 28)
      this.reportName = "detconceptonocod";
    else if (this.tipoReporte == 29)
      this.reportName = "detconceptonoced";
    else if (this.tipoReporte == 5)
      this.reportName = "resconceptono";
    else if (this.tipoReporte == 6)
      this.reportName = "resconceptonouel";
    else if (this.tipoReporte == 7)
      this.reportName = "depbannoalf";
    else if (this.tipoReporte == 8)
      this.reportName = "chequesno";
    else if (this.tipoReporte == 9)
      this.reportName = "aceppaguadm";
    else if (this.tipoReporte == 10)
      this.reportName = "aceppaglp";
    else if (this.tipoReporte == 15)
      this.reportName = "resaportesno";
    else if (this.tipoReporte == 16)
      this.reportName = "resaportesnouel";
    else if (this.tipoReporte == 20)
      this.reportName = "resconceptonouadm";
    else if (this.tipoReporte == 21)
      this.reportName = "aceppagdep";
    else if (this.tipoReporte == 22)
      this.reportName = "nominareg";
    else if (this.tipoReporte == 23)
      this.reportName = "nominadep";
    else if (this.tipoReporte == 24)
      this.reportName = "nominapr";
    else if (this.tipoReporte == 25)
      this.reportName = "depbannocod";
    else if (this.tipoReporte == 26)
      this.reportName = "depbannoced";
    else if (this.tipoReporte == 27)
      this.reportName = "depbannoua";
    else if (this.tipoReporte == 30)
      this.reportName = "resconceptonocatuel_hist_";
    else if (this.tipoReporte == 32)
      this.reportName = "netopagar";
    else if (this.tipoReporte == 100)
      this.reportName = "recibosalf";
    else if (this.tipoReporte == 101)
      this.reportName = "recibos2alfpptc";
    else if (this.tipoReporte == 102)
      this.reportName = "recibos3alfpptc";
    else if (this.tipoReporte == 103)
      this.reportName = "recibos4alfpptc";
    else if (this.tipoReporte == 104)
      this.reportName = "reciboscod";
    else if (this.tipoReporte == 105)
      this.reportName = "recibos2codpptc";
    else if (this.tipoReporte == 106)
      this.reportName = "recibos3codpptc";
    else if (this.tipoReporte == 107)
      this.reportName = "recibos4codpptc";
    else if (this.tipoReporte == 108)
      this.reportName = "recibosced";
    else if (this.tipoReporte == 109)
      this.reportName = "recibos2cedpptc";
    else if (this.tipoReporte == 110)
      this.reportName = "recibos3cedpptc";
    else if (this.tipoReporte == 111) {
      this.reportName = "recibos4cedpptc";
    }
    log.error("Tipo  reporte   " + this.tipoReporte);
    log.error("nombre reporte   " + this.reportName);

    if (this.idGrupoNomina != 0L) {
      if (this.grupoNomina.getPeriodicidad().equals("S"))
        this.reportName += "s";
      else {
        this.reportName += "q";
      }
      if (this.idUnidadAdministradora != 0L)
        this.reportName += "_ua";
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      this.listUnidadAdministradora = this.estructuraFacade.findUnidadAdministradoraByOrganismo(this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.listGrupoNomina = new ArrayList();
      this.listUnidadAdministradora = new ArrayList();
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      Map parameters = new Hashtable();

      if (this.tipoNomina.equals("O")) {
        if (this.grupoNomina.getPeriodicidad().equals("S")) {
          log.error("anio para semana   " + this.anio);
          log.error("semana para semana" + this.semana);

          Semana semana2 = this.definicionesFacade.findSemanaByAnioSemanaAnio(this.anio, this.semana);

          log.error("paso");

          this.inicioAux.setTime(semana2.getFechaInicio());
          this.finAux.setTime(semana2.getFechaFin());
        } else if (this.grupoNomina.getPeriodicidad().equals("Q")) {
          if (this.quincena == 1) {
            this.inicioAux.set(this.anio, this.mes - 1, 1);
            this.finAux.set(this.anio, this.mes - 1, 15);
          } else {
            this.inicioAux.set(this.anio, this.mes - 1, 1);
            this.finAux.setTime(this.inicioAux.getTime());
            log.error("1-fin " + this.finAux.getTime());
            this.finAux.add(2, 1);

            log.error("2-fin " + this.finAux.getTime());
            this.finAux.add(5, -1);
            this.inicioAux.set(this.anio, this.mes - 1, 16);
            log.error("3-fin " + this.finAux.getTime());
          }
        }
        else
        {
          this.inicioAux.set(this.anio, this.mes - 1, 1);
          this.finAux.setTime(this.inicioAux.getTime());
          log.error("1-fin M " + this.finAux.getTime());
          this.finAux.add(2, 1);
          log.error("2-fin M" + this.finAux.getTime());
          this.finAux.add(5, -1);
          log.error("3-fin M" + this.finAux.getTime());
        }
      }
      log.error("inicio  M" + this.inicioAux.getTime());
      log.error("0-fin  M" + this.finAux.getTime());
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));

      log.error("1 ");

      Date fec_ini = new Date(this.inicioAux.getTime().getYear(), this.inicioAux.getTime().getMonth(), this.inicioAux.getTime().getDate());
      Date fec_fin = new Date(this.finAux.getTime().getYear(), this.finAux.getTime().getMonth(), this.finAux.getTime().getDate());

      log.error("2 ");

      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));

      log.error("anio " + this.anio);
      log.error("mes " + this.mes);

      parameters.put("fec_ini", fec_ini);
      parameters.put("fec_fin", fec_fin);

      log.error("4 ");

      parameters.put("id_grupo_nomina", new Long(this.idGrupoNomina));
      if (this.tipoNomina.equals("O"))
      {
        log.error("6 " + this.idGrupoNomina);

        if (this.grupoNomina.getPeriodicidad().equals("S")) {
          log.error("7 ");

          parameters.put("periodo", new Integer(this.semana));
          log.error("8 " + this.semana);
        }
        else {
          parameters.put("periodo", new Integer(this.quincena));
          log.error("8 " + this.quincena);
        }
      }
      else
      {
        log.error("9 ");

        parameters.put("periodo", new Integer(1));
      }

      log.error("10 ");

      if (this.tipoNomina.equals("O")) {
        parameters.put("nomina_estatus", "A");
        parameters.put("nombre_nomina", "ORDINARIA");
        parameters.put("numero_nomina", new Integer(0));
        log.error("ordinaria ");
      } else {
        parameters.put("nomina_estatus", this.estatus);
        parameters.put("nombre_nomina", "ESPECIAL");
        parameters.put("numero_nomina", new Integer(this.numero_nomina));
        parameters.put("anio", new Integer(this.nominaEspecial.getAnio()));
        parameters.put("mes", new Integer(this.nominaEspecial.getMes()));
        log.error("espcial ");
      }

      if (this.tipoReporte > 99) {
        parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/historico");
      }

      if (this.tipoReporte == 1)
        this.reportName = "nominarac";
      else if (this.tipoReporte == 2)
        this.reportName = "nominaalf";
      else if (this.tipoReporte == 3)
        this.reportName = "nominauel";
      else if (this.tipoReporte == 4)
        this.reportName = "detconceptonoalf";
      else if (this.tipoReporte == 28)
        this.reportName = "detconceptonocod";
      else if (this.tipoReporte == 29)
        this.reportName = "detconceptonoced";
      else if (this.tipoReporte == 5)
        this.reportName = "resconceptono";
      else if (this.tipoReporte == 6)
        this.reportName = "resconceptonouel";
      else if (this.tipoReporte == 7)
        this.reportName = "depbannoalf";
      else if (this.tipoReporte == 8)
        this.reportName = "chequesno";
      else if (this.tipoReporte == 9)
        this.reportName = "aceppaguadm";
      else if (this.tipoReporte == 10)
        this.reportName = "aceppaglp";
      else if (this.tipoReporte == 15)
        this.reportName = "resaportesno";
      else if (this.tipoReporte == 16)
        this.reportName = "resaportesnouel";
      else if (this.tipoReporte == 20)
        this.reportName = "resconceptonouadm";
      else if (this.tipoReporte == 21)
        this.reportName = "aceppagdep";
      else if (this.tipoReporte == 22)
        this.reportName = "nominareg";
      else if (this.tipoReporte == 23)
        this.reportName = "nominadep";
      else if (this.tipoReporte == 24)
        this.reportName = "nominapr";
      else if (this.tipoReporte == 25)
        this.reportName = "depbannocod";
      else if (this.tipoReporte == 26)
        this.reportName = "depbannoced";
      else if (this.tipoReporte == 27)
        this.reportName = "depbannoua";
      else if (this.tipoReporte == 30)
        this.reportName = "resconceptonocatuel_hist_";
      else if (this.tipoReporte == 32)
        this.reportName = "netopagar";
      else if (this.tipoReporte == 100)
        this.reportName = "recibosalf";
      else if (this.tipoReporte == 101)
        this.reportName = "recibos2alfpptc";
      else if (this.tipoReporte == 102)
        this.reportName = "recibos3alfpptc";
      else if (this.tipoReporte == 103)
        this.reportName = "recibos4alfpptc";
      else if (this.tipoReporte == 104)
        this.reportName = "reciboscod";
      else if (this.tipoReporte == 105)
        this.reportName = "recibos2codpptc";
      else if (this.tipoReporte == 106)
        this.reportName = "recibos3codpptc";
      else if (this.tipoReporte == 107)
        this.reportName = "recibos4codpptc";
      else if (this.tipoReporte == 108)
        this.reportName = "recibosced";
      else if (this.tipoReporte == 109)
        this.reportName = "recibos2cedpptc";
      else if (this.tipoReporte == 110)
        this.reportName = "recibos3cedpptc";
      else if (this.tipoReporte == 111) {
        this.reportName = "recibos4cedpptc";
      }
      if (this.grupoNomina.getPeriodicidad().equals("S"))
        this.reportName += "s";
      else {
        this.reportName += "q";
      }

      if (this.idUnidadAdministradora != 0L) {
        this.reportName += "_ua";
        parameters.put("id_unidad_administradora", new Long(this.idUnidadAdministradora));
      }
      JasperForWeb report = new JasperForWeb();

      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/historico");

      report.start();
      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public Collection getListNominaEspecial() {
    if ((this.listNominaEspecial != null) && (!this.listNominaEspecial.isEmpty())) {
      return ListUtil.convertCollectionToSelectItemsWithId(
        this.listNominaEspecial, "sigefirrhh.personal.procesoNomina.NominaEspecial");
    }
    return null;
  }

  public Collection getListGrupoNomina() {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }

  public String getSelectGrupoNomina()
  {
    return this.selectGrupoNomina;
  }

  public void setSelectGrupoNomina(String string)
  {
    this.selectGrupoNomina = string;
  }

  public String getFin()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }

  public void setFin(String string)
  {
    this.fin = string;
  }

  public void setInicio(String string)
  {
    this.inicio = string;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public long getIdGrupoNomina()
  {
    return this.idGrupoNomina;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setIdGrupoNomina(long l)
  {
    this.idGrupoNomina = l;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public long getIdUnidadAdministradora()
  {
    return this.idUnidadAdministradora;
  }

  public void setIdUnidadAdministradora(long l)
  {
    this.idUnidadAdministradora = l;
  }

  public boolean isShowSemana()
  {
    return this.showSemana;
  }
  public String getTipoNomina() {
    return this.tipoNomina;
  }
  public void setTipoNomina(String tipoNomina) {
    this.tipoNomina = tipoNomina;
  }
  public boolean isShowAnioMes() {
    return this.showAnioMes;
  }
  public void setShowAnioMes(boolean showAnioMes) {
    this.showAnioMes = showAnioMes;
  }
  public boolean isShowNominaEspecial() {
    return this.showNominaEspecial;
  }
  public void setShowNominaEspecial(boolean showNominaEspecial) {
    this.showNominaEspecial = showNominaEspecial;
  }
  public boolean isShowQuincena() {
    return this.showQuincena;
  }

  public String getSelectNominaEspecial() {
    return this.selectNominaEspecial;
  }
  public void setSelectNominaEspecial(String selectNominaEspecial) {
    this.selectNominaEspecial = selectNominaEspecial;
  }
  public boolean isShowGeneral() {
    return this.showGeneral;
  }
  public void setShowGeneral(boolean showGeneral) {
    this.showGeneral = showGeneral;
  }
  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
  public int getSemana() {
    return this.semana;
  }
  public void setSemana(int semana) {
    this.semana = semana;
  }
  public int getQuincena() {
    return this.quincena;
  }
  public void setQuincena(int quincena) {
    this.quincena = quincena;
  }
  public String getEstatus() {
    return this.estatus;
  }
  public void setEstatus(String estatus) {
    this.estatus = estatus;
  }
  public int getNumero_nomina() {
    return this.numero_nomina;
  }
  public void setNumero_nomina(int numero_nomina) {
    this.numero_nomina = numero_nomina;
  }

  public boolean isShowPeriodo() {
    return this.showPeriodo;
  }
  public void setShowPeriodo(boolean showPeriodo) {
    this.showPeriodo = showPeriodo;
  }
}