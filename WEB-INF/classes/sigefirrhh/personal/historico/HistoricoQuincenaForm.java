package sigefirrhh.personal.historico;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.procesoNomina.ProcesoNominaFacade;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;

public class HistoricoQuincenaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(HistoricoQuincenaForm.class.getName());
  private HistoricoQuincena historicoQuincena;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private ProcesoNominaFacade procesoNominaFacade = new ProcesoNominaFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private HistoricoFacade historicoFacade = new HistoricoFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colTipoPersonal;
  private Collection colGrupoNomina;
  private Collection colConceptoTipoPersonal;
  private Collection colFrecuenciaTipoPersonal;
  private Collection colConceptoAporte;
  private Collection colNominaEspecial;
  private Collection colTrabajador;
  private Collection colHistoricoNomina;
  private String selectTipoPersonal;
  private String selectGrupoNomina;
  private String selectConceptoTipoPersonal;
  private String selectFrecuenciaTipoPersonal;
  private String selectConceptoAporte;
  private String selectNominaEspecial;
  private String selectTrabajador;
  private String selectHistoricoNomina;
  private Collection findColTipoPersonal;
  private Object stateResultHistoricoQuincenaByTrabajador = null;

  public Collection getFindColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.historicoQuincena.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.historicoQuincena.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String valGrupoNomina) {
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    this.historicoQuincena.setGrupoNomina(null);
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      if (String.valueOf(grupoNomina.getIdGrupoNomina()).equals(
        valGrupoNomina)) {
        this.historicoQuincena.setGrupoNomina(
          grupoNomina);
        break;
      }
    }
    this.selectGrupoNomina = valGrupoNomina;
  }
  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    this.historicoQuincena.setConceptoTipoPersonal(null);
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()).equals(
        valConceptoTipoPersonal)) {
        this.historicoQuincena.setConceptoTipoPersonal(
          conceptoTipoPersonal);
        break;
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public String getSelectFrecuenciaTipoPersonal() {
    return this.selectFrecuenciaTipoPersonal;
  }
  public void setSelectFrecuenciaTipoPersonal(String valFrecuenciaTipoPersonal) {
    Iterator iterator = this.colFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
    this.historicoQuincena.setFrecuenciaTipoPersonal(null);
    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      if (String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()).equals(
        valFrecuenciaTipoPersonal)) {
        this.historicoQuincena.setFrecuenciaTipoPersonal(
          frecuenciaTipoPersonal);
        break;
      }
    }
    this.selectFrecuenciaTipoPersonal = valFrecuenciaTipoPersonal;
  }
  public String getSelectConceptoAporte() {
    return this.selectConceptoAporte;
  }
  public void setSelectConceptoAporte(String valConceptoAporte) {
    Iterator iterator = this.colConceptoAporte.iterator();
    Concepto conceptoAporte = null;
    this.historicoQuincena.setConceptoAporte(null);
    while (iterator.hasNext()) {
      conceptoAporte = (Concepto)iterator.next();
      if (String.valueOf(conceptoAporte.getIdConcepto()).equals(
        valConceptoAporte)) {
        this.historicoQuincena.setConceptoAporte(
          conceptoAporte);
        break;
      }
    }
    this.selectConceptoAporte = valConceptoAporte;
  }
  public String getSelectNominaEspecial() {
    return this.selectNominaEspecial;
  }
  public void setSelectNominaEspecial(String valNominaEspecial) {
    Iterator iterator = this.colNominaEspecial.iterator();
    NominaEspecial nominaEspecial = null;
    this.historicoQuincena.setNominaEspecial(null);
    while (iterator.hasNext()) {
      nominaEspecial = (NominaEspecial)iterator.next();
      if (String.valueOf(nominaEspecial.getIdNominaEspecial()).equals(
        valNominaEspecial)) {
        this.historicoQuincena.setNominaEspecial(
          nominaEspecial);
        break;
      }
    }
    this.selectNominaEspecial = valNominaEspecial;
  }
  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    this.historicoQuincena.setTrabajador(null);
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      if (String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador)) {
        this.historicoQuincena.setTrabajador(
          trabajador);
        break;
      }
    }
    this.selectTrabajador = valTrabajador;
  }
  public String getSelectHistoricoNomina() {
    return this.selectHistoricoNomina;
  }
  public void setSelectHistoricoNomina(String valHistoricoNomina) {
    Iterator iterator = this.colHistoricoNomina.iterator();
    HistoricoNomina historicoNomina = null;
    this.historicoQuincena.setHistoricoNomina(null);
    while (iterator.hasNext()) {
      historicoNomina = (HistoricoNomina)iterator.next();
      if (String.valueOf(historicoNomina.getIdHistoricoNomina()).equals(
        valHistoricoNomina)) {
        this.historicoQuincena.setHistoricoNomina(
          historicoNomina);
        break;
      }
    }
    this.selectHistoricoNomina = valHistoricoNomina;
  }
  public Collection getResult() {
    return this.result;
  }

  public HistoricoQuincena getHistoricoQuincena() {
    if (this.historicoQuincena == null) {
      this.historicoQuincena = new HistoricoQuincena();
    }
    return this.historicoQuincena;
  }

  public HistoricoQuincenaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListOrigen()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = HistoricoQuincena.LISTA_ORIGEN.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoNomina.getIdGrupoNomina()), 
        grupoNomina.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColFrecuenciaTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()), 
        frecuenciaTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConceptoAporte()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoAporte.iterator();
    Concepto conceptoAporte = null;
    while (iterator.hasNext()) {
      conceptoAporte = (Concepto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoAporte.getIdConcepto()), 
        conceptoAporte.toString()));
    }
    return col;
  }

  public Collection getColNominaEspecial()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colNominaEspecial.iterator();
    NominaEspecial nominaEspecial = null;
    while (iterator.hasNext()) {
      nominaEspecial = (NominaEspecial)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nominaEspecial.getIdNominaEspecial()), 
        nominaEspecial.toString()));
    }
    return col;
  }

  public Collection getColTrabajador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public Collection getColHistoricoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colHistoricoNomina.iterator();
    HistoricoNomina historicoNomina = null;
    while (iterator.hasNext()) {
      historicoNomina = (HistoricoNomina)iterator.next();
      col.add(new SelectItem(
        String.valueOf(historicoNomina.getIdHistoricoNomina()), 
        historicoNomina.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        new DefinicionesFacade().findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colGrupoNomina = 
        this.definicionesFacade.findGrupoNominaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colConceptoTipoPersonal = 
        this.definicionesFacade.findAllConceptoTipoPersonal();
      this.colFrecuenciaTipoPersonal = 
        this.definicionesFacade.findAllFrecuenciaTipoPersonal();
      this.colConceptoAporte = 
        this.definicionesFacade.findConceptoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colNominaEspecial = 
        this.procesoNominaFacade.findAllNominaEspecial();

      this.colHistoricoNomina = 
        this.historicoFacade.findAllHistoricoNomina();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedula(this.findTrabajadorCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidos(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findHistoricoQuincenaByTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectTrabajador();
      if (!this.adding)
      {
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectHistoricoQuincena()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectGrupoNomina = null;
    this.selectConceptoTipoPersonal = null;
    this.selectFrecuenciaTipoPersonal = null;
    this.selectConceptoAporte = null;
    this.selectNominaEspecial = null;
    this.selectTrabajador = null;
    this.selectHistoricoNomina = null;

    long idHistoricoQuincena = 
      Long.parseLong((String)requestParameterMap.get("idHistoricoQuincena"));
    try
    {
      this.historicoQuincena = 
        this.historicoFacade.findHistoricoQuincenaById(
        idHistoricoQuincena);

      if (this.historicoQuincena.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.historicoQuincena.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.historicoQuincena.getGrupoNomina() != null) {
        this.selectGrupoNomina = 
          String.valueOf(this.historicoQuincena.getGrupoNomina().getIdGrupoNomina());
      }
      if (this.historicoQuincena.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.historicoQuincena.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }
      if (this.historicoQuincena.getFrecuenciaTipoPersonal() != null) {
        this.selectFrecuenciaTipoPersonal = 
          String.valueOf(this.historicoQuincena.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal());
      }
      if (this.historicoQuincena.getConceptoAporte() != null) {
        this.selectConceptoAporte = 
          String.valueOf(this.historicoQuincena.getConceptoAporte().getIdConcepto());
      }
      if (this.historicoQuincena.getNominaEspecial() != null) {
        this.selectNominaEspecial = 
          String.valueOf(this.historicoQuincena.getNominaEspecial().getIdNominaEspecial());
      }
      if (this.historicoQuincena.getTrabajador() != null) {
        this.selectTrabajador = 
          String.valueOf(this.historicoQuincena.getTrabajador().getIdTrabajador());
      }
      if (this.historicoQuincena.getHistoricoNomina() != null) {
        this.selectHistoricoNomina = 
          String.valueOf(this.historicoQuincena.getHistoricoNomina().getIdHistoricoNomina());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.historicoQuincena.getFecha() != null) && 
      (this.historicoQuincena.getFecha().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.historicoQuincena.setTrabajador(
          this.trabajador);
        this.historicoFacade.addHistoricoQuincena(
          this.historicoQuincena);
        context.addMessage("success_add", new FacesMessage("Se agregï¿½ con ï¿½xito"));
      } else {
        this.historicoFacade.updateHistoricoQuincena(
          this.historicoQuincena);
        context.addMessage("success_modify", new FacesMessage("Se modificï¿½ con ï¿½xito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e)
    {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.historicoFacade.deleteHistoricoQuincena(
        this.historicoQuincena);
      context.addMessage("success_delete", new FacesMessage("Se eliminï¿½ con ï¿½xito"));
      this.result = null;

      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedTrabajador = true;

    this.selectTipoPersonal = null;

    this.selectGrupoNomina = null;

    this.selectConceptoTipoPersonal = null;

    this.selectFrecuenciaTipoPersonal = null;

    this.selectConceptoAporte = null;

    this.selectNominaEspecial = null;

    this.selectTrabajador = null;

    this.selectHistoricoNomina = null;

    this.historicoQuincena = new HistoricoQuincena();

    this.historicoQuincena.setTrabajador(this.trabajador);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.historicoQuincena.setIdHistoricoQuincena(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.historico.HistoricoQuincena"));

    return null;
  }

  public boolean isShowNominaEspecialAux() {
    try {
      return this.historicoQuincena.getNumeroNomina() > 0; } catch (Exception e) {
    }
    return false;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.historicoQuincena = new HistoricoQuincena();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.historicoQuincena = new HistoricoQuincena();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedTrabajador));
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedTrabajador);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public LoginSession getLogin() {
    return this.login;
  }
}