package sigefirrhh.personal.historico;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.util.Collection;

public class HistoricoNoGenFacade extends HistoricoFacade
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private HistoricoNoGenBusiness historicoNoGenBusiness = new HistoricoNoGenBusiness();

  public Collection findHistoricoQuincenaByConceptoTipoPersonalAndTrabajador(long idConceptoTipoPersonal, long idTrabajador, int anio, int mes, int semanaQuincena) throws Exception
  {
    try {
      this.txn.open();
      return this.historicoNoGenBusiness.findHistoricoQuincenaByConceptoTipoPersonalAndTrabajador(idConceptoTipoPersonal, idTrabajador, anio, mes, semanaQuincena);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHistoricoSemanaByConceptoTipoPersonalAndTrabajador(long idConceptoTipoPersonal, long idTrabajador, int anio, int mes, int semanaQuincena) throws Exception
  {
    try { this.txn.open();
      return this.historicoNoGenBusiness.findHistoricoSemanaByConceptoTipoPersonalAndTrabajador(idConceptoTipoPersonal, idTrabajador, anio, mes, semanaQuincena);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHistoricoQuincenaByTrabajadorAndNumeroNomina(long idTrabajador, int numeroNomina) throws Exception
  {
    try { this.txn.open();
      return this.historicoNoGenBusiness.findHistoricoQuincenaByTrabajadorAndNumeroNomina(idTrabajador, numeroNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHistoricoSemanaByTrabajadorAndNumeroNomina(long idTrabajador, int numeroNomina) throws Exception
  {
    try { this.txn.open();
      return this.historicoNoGenBusiness.findHistoricoSemanaByTrabajadorAndNumeroNomina(idTrabajador, numeroNomina);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}