package sigefirrhh.personal.historico;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.trabajador.Trabajador;

public class HistoricoSemana
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ORIGEN;
  private long idHistoricoSemana;
  private int numeroNomina;
  private int anio;
  private int mes;
  private int semanaQuincena;
  private Date fecha;
  private double unidades;
  private double montoAsigna;
  private double montoDeduce;
  private String origen;
  private String documentoSoporte;
  private TipoPersonal tipoPersonal;
  private GrupoNomina grupoNomina;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private int idConcepto;
  private double montoAporte;
  private Concepto conceptoAporte;
  private NominaEspecial nominaEspecial;
  private Trabajador trabajador;
  private HistoricoNomina historicoNomina;
  private int mesSobretiempo;
  private int anioSobretiempo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "anioSobretiempo", "conceptoAporte", "conceptoTipoPersonal", "documentoSoporte", "fecha", "frecuenciaTipoPersonal", "grupoNomina", "historicoNomina", "idConcepto", "idHistoricoSemana", "mes", "mesSobretiempo", "montoAporte", "montoAsigna", "montoDeduce", "nominaEspecial", "numeroNomina", "origen", "semanaQuincena", "tipoPersonal", "trabajador", "unidades" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), sunjdo$classForName$("sigefirrhh.personal.historico.HistoricoNomina"), Integer.TYPE, Long.TYPE, Integer.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.procesoNomina.NominaEspecial"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 26, 26, 21, 21, 26, 26, 26, 21, 24, 21, 21, 21, 21, 21, 26, 21, 21, 21, 26, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.historico.HistoricoSemana"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HistoricoSemana());

    LISTA_ORIGEN = 
      new LinkedHashMap();
    LISTA_ORIGEN.put("F", "FIJO");
    LISTA_ORIGEN.put("V", "VARIABLE");
    LISTA_ORIGEN.put("P", "PRESTAMO");
    LISTA_ORIGEN.put("C", "CALCULADO");
  }

  public HistoricoSemana()
  {
    jdoSetnumeroNomina(this, 0);

    jdoSetanio(this, 0);

    jdoSetmes(this, 0);

    jdoSetsemanaQuincena(this, 0);

    jdoSetunidades(this, 0.0D);

    jdoSetmontoAsigna(this, 0.0D);

    jdoSetmontoDeduce(this, 0.0D);

    jdoSetmontoAporte(this, 0.0D);

    jdoSetmesSobretiempo(this, 0);

    jdoSetanioSobretiempo(this, 0);
  }
  public String toString() {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,##0.00");
    String c;
    String c;
    if (jdoGetmontoAsigna(this) == 0.0D)
      c = b.format(jdoGetmontoDeduce(this));
    else {
      c = b.format(jdoGetmontoAsigna(this));
    }

    String a = b.format(jdoGetunidades(this));

    return jdoGetmes(this) + " - " + 
      "S-" + jdoGetsemanaQuincena(this) + " - " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getCodConcepto() + " " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + 
      "UNID-" + a + " - " + 
      c + " - " + 
      "FREC-" + jdoGetfrecuenciaTipoPersonal(this).getFrecuenciaPago().getCodFrecuenciaPago();
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public int getIdConcepto()
  {
    return jdoGetidConcepto(this);
  }
  public void setIdConcepto(int idConcepto) {
    jdoSetidConcepto(this, idConcepto);
  }
  public Concepto getConceptoAporte() {
    return jdoGetconceptoAporte(this);
  }
  public void setConceptoAporte(Concepto conceptoAporte) {
    jdoSetconceptoAporte(this, conceptoAporte);
  }
  public double getMontoAporte() {
    return jdoGetmontoAporte(this);
  }
  public void setMontoAporte(double montoAporte) {
    jdoSetmontoAporte(this, montoAporte);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal()
  {
    return jdoGetfrecuenciaTipoPersonal(this);
  }

  public GrupoNomina getGrupoNomina()
  {
    return jdoGetgrupoNomina(this);
  }

  public HistoricoNomina getHistoricoNomina()
  {
    return jdoGethistoricoNomina(this);
  }

  public long getIdHistoricoSemana()
  {
    return jdoGetidHistoricoSemana(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public double getMontoAsigna()
  {
    return jdoGetmontoAsigna(this);
  }

  public double getMontoDeduce()
  {
    return jdoGetmontoDeduce(this);
  }

  public NominaEspecial getNominaEspecial()
  {
    return jdoGetnominaEspecial(this);
  }

  public int getNumeroNomina()
  {
    return jdoGetnumeroNomina(this);
  }

  public String getOrigen()
  {
    return jdoGetorigen(this);
  }

  public int getSemanaQuincena()
  {
    return jdoGetsemanaQuincena(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public double getUnidades()
  {
    return jdoGetunidades(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setFrecuenciaTipoPersonal(FrecuenciaTipoPersonal personal)
  {
    jdoSetfrecuenciaTipoPersonal(this, personal);
  }

  public void setGrupoNomina(GrupoNomina nomina)
  {
    jdoSetgrupoNomina(this, nomina);
  }

  public void setHistoricoNomina(HistoricoNomina nomina)
  {
    jdoSethistoricoNomina(this, nomina);
  }

  public void setIdHistoricoSemana(long l)
  {
    jdoSetidHistoricoSemana(this, l);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setMontoAsigna(double d)
  {
    jdoSetmontoAsigna(this, d);
  }

  public void setMontoDeduce(double d)
  {
    jdoSetmontoDeduce(this, d);
  }

  public void setNominaEspecial(NominaEspecial especial)
  {
    jdoSetnominaEspecial(this, especial);
  }

  public void setNumeroNomina(int i)
  {
    jdoSetnumeroNomina(this, i);
  }

  public void setOrigen(String string)
  {
    jdoSetorigen(this, string);
  }

  public void setSemanaQuincena(int i)
  {
    jdoSetsemanaQuincena(this, i);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setUnidades(double d)
  {
    jdoSetunidades(this, d);
  }

  public Date getFecha()
  {
    return jdoGetfecha(this);
  }

  public void setFecha(Date date)
  {
    jdoSetfecha(this, date);
  }

  public int getAnioSobretiempo() {
    return jdoGetanioSobretiempo(this);
  }
  public void setAnioSobretiempo(int anioSobretiempo) {
    jdoSetanioSobretiempo(this, anioSobretiempo);
  }
  public int getMesSobretiempo() {
    return jdoGetmesSobretiempo(this);
  }
  public void setMesSobretiempo(int mesSobretiempo) {
    jdoSetmesSobretiempo(this, mesSobretiempo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 23;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HistoricoSemana localHistoricoSemana = new HistoricoSemana();
    localHistoricoSemana.jdoFlags = 1;
    localHistoricoSemana.jdoStateManager = paramStateManager;
    return localHistoricoSemana;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HistoricoSemana localHistoricoSemana = new HistoricoSemana();
    localHistoricoSemana.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHistoricoSemana.jdoFlags = 1;
    localHistoricoSemana.jdoStateManager = paramStateManager;
    return localHistoricoSemana;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioSobretiempo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoAporte);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaTipoPersonal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.historicoNomina);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idConcepto);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHistoricoSemana);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesSobretiempo);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAporte);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAsigna);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoDeduce);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nominaEspecial);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroNomina);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origen);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanaQuincena);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioSobretiempo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoAporte = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaTipoPersonal = ((FrecuenciaTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.historicoNomina = ((HistoricoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConcepto = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHistoricoSemana = localStateManager.replacingLongField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesSobretiempo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAporte = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAsigna = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoDeduce = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nominaEspecial = ((NominaEspecial)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origen = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanaQuincena = localStateManager.replacingIntField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HistoricoSemana paramHistoricoSemana, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramHistoricoSemana.anio;
      return;
    case 1:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.anioSobretiempo = paramHistoricoSemana.anioSobretiempo;
      return;
    case 2:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoAporte = paramHistoricoSemana.conceptoAporte;
      return;
    case 3:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramHistoricoSemana.conceptoTipoPersonal;
      return;
    case 4:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramHistoricoSemana.documentoSoporte;
      return;
    case 5:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramHistoricoSemana.fecha;
      return;
    case 6:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaTipoPersonal = paramHistoricoSemana.frecuenciaTipoPersonal;
      return;
    case 7:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramHistoricoSemana.grupoNomina;
      return;
    case 8:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.historicoNomina = paramHistoricoSemana.historicoNomina;
      return;
    case 9:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.idConcepto = paramHistoricoSemana.idConcepto;
      return;
    case 10:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.idHistoricoSemana = paramHistoricoSemana.idHistoricoSemana;
      return;
    case 11:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramHistoricoSemana.mes;
      return;
    case 12:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.mesSobretiempo = paramHistoricoSemana.mesSobretiempo;
      return;
    case 13:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.montoAporte = paramHistoricoSemana.montoAporte;
      return;
    case 14:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.montoAsigna = paramHistoricoSemana.montoAsigna;
      return;
    case 15:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.montoDeduce = paramHistoricoSemana.montoDeduce;
      return;
    case 16:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.nominaEspecial = paramHistoricoSemana.nominaEspecial;
      return;
    case 17:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.numeroNomina = paramHistoricoSemana.numeroNomina;
      return;
    case 18:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.origen = paramHistoricoSemana.origen;
      return;
    case 19:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.semanaQuincena = paramHistoricoSemana.semanaQuincena;
      return;
    case 20:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramHistoricoSemana.tipoPersonal;
      return;
    case 21:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramHistoricoSemana.trabajador;
      return;
    case 22:
      if (paramHistoricoSemana == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramHistoricoSemana.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HistoricoSemana))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HistoricoSemana localHistoricoSemana = (HistoricoSemana)paramObject;
    if (localHistoricoSemana.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHistoricoSemana, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HistoricoSemanaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HistoricoSemanaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoSemanaPK))
      throw new IllegalArgumentException("arg1");
    HistoricoSemanaPK localHistoricoSemanaPK = (HistoricoSemanaPK)paramObject;
    localHistoricoSemanaPK.idHistoricoSemana = this.idHistoricoSemana;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoSemanaPK))
      throw new IllegalArgumentException("arg1");
    HistoricoSemanaPK localHistoricoSemanaPK = (HistoricoSemanaPK)paramObject;
    this.idHistoricoSemana = localHistoricoSemanaPK.idHistoricoSemana;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoSemanaPK))
      throw new IllegalArgumentException("arg2");
    HistoricoSemanaPK localHistoricoSemanaPK = (HistoricoSemanaPK)paramObject;
    localHistoricoSemanaPK.idHistoricoSemana = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 10);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoSemanaPK))
      throw new IllegalArgumentException("arg2");
    HistoricoSemanaPK localHistoricoSemanaPK = (HistoricoSemanaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 10, localHistoricoSemanaPK.idHistoricoSemana);
  }

  private static final int jdoGetanio(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.anio;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.anio;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 0))
      return paramHistoricoSemana.anio;
    return localStateManager.getIntField(paramHistoricoSemana, jdoInheritedFieldCount + 0, paramHistoricoSemana.anio);
  }

  private static final void jdoSetanio(HistoricoSemana paramHistoricoSemana, int paramInt)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoSemana, jdoInheritedFieldCount + 0, paramHistoricoSemana.anio, paramInt);
  }

  private static final int jdoGetanioSobretiempo(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.anioSobretiempo;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.anioSobretiempo;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 1))
      return paramHistoricoSemana.anioSobretiempo;
    return localStateManager.getIntField(paramHistoricoSemana, jdoInheritedFieldCount + 1, paramHistoricoSemana.anioSobretiempo);
  }

  private static final void jdoSetanioSobretiempo(HistoricoSemana paramHistoricoSemana, int paramInt)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.anioSobretiempo = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.anioSobretiempo = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoSemana, jdoInheritedFieldCount + 1, paramHistoricoSemana.anioSobretiempo, paramInt);
  }

  private static final Concepto jdoGetconceptoAporte(HistoricoSemana paramHistoricoSemana)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.conceptoAporte;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 2))
      return paramHistoricoSemana.conceptoAporte;
    return (Concepto)localStateManager.getObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 2, paramHistoricoSemana.conceptoAporte);
  }

  private static final void jdoSetconceptoAporte(HistoricoSemana paramHistoricoSemana, Concepto paramConcepto)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.conceptoAporte = paramConcepto;
      return;
    }
    localStateManager.setObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 2, paramHistoricoSemana.conceptoAporte, paramConcepto);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(HistoricoSemana paramHistoricoSemana)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 3))
      return paramHistoricoSemana.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 3, paramHistoricoSemana.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(HistoricoSemana paramHistoricoSemana, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 3, paramHistoricoSemana.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGetdocumentoSoporte(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.documentoSoporte;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.documentoSoporte;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 4))
      return paramHistoricoSemana.documentoSoporte;
    return localStateManager.getStringField(paramHistoricoSemana, jdoInheritedFieldCount + 4, paramHistoricoSemana.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(HistoricoSemana paramHistoricoSemana, String paramString)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoSemana, jdoInheritedFieldCount + 4, paramHistoricoSemana.documentoSoporte, paramString);
  }

  private static final Date jdoGetfecha(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.fecha;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.fecha;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 5))
      return paramHistoricoSemana.fecha;
    return (Date)localStateManager.getObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 5, paramHistoricoSemana.fecha);
  }

  private static final void jdoSetfecha(HistoricoSemana paramHistoricoSemana, Date paramDate)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 5, paramHistoricoSemana.fecha, paramDate);
  }

  private static final FrecuenciaTipoPersonal jdoGetfrecuenciaTipoPersonal(HistoricoSemana paramHistoricoSemana)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.frecuenciaTipoPersonal;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 6))
      return paramHistoricoSemana.frecuenciaTipoPersonal;
    return (FrecuenciaTipoPersonal)localStateManager.getObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 6, paramHistoricoSemana.frecuenciaTipoPersonal);
  }

  private static final void jdoSetfrecuenciaTipoPersonal(HistoricoSemana paramHistoricoSemana, FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.frecuenciaTipoPersonal = paramFrecuenciaTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 6, paramHistoricoSemana.frecuenciaTipoPersonal, paramFrecuenciaTipoPersonal);
  }

  private static final GrupoNomina jdoGetgrupoNomina(HistoricoSemana paramHistoricoSemana)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.grupoNomina;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 7))
      return paramHistoricoSemana.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 7, paramHistoricoSemana.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(HistoricoSemana paramHistoricoSemana, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 7, paramHistoricoSemana.grupoNomina, paramGrupoNomina);
  }

  private static final HistoricoNomina jdoGethistoricoNomina(HistoricoSemana paramHistoricoSemana)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.historicoNomina;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 8))
      return paramHistoricoSemana.historicoNomina;
    return (HistoricoNomina)localStateManager.getObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 8, paramHistoricoSemana.historicoNomina);
  }

  private static final void jdoSethistoricoNomina(HistoricoSemana paramHistoricoSemana, HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.historicoNomina = paramHistoricoNomina;
      return;
    }
    localStateManager.setObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 8, paramHistoricoSemana.historicoNomina, paramHistoricoNomina);
  }

  private static final int jdoGetidConcepto(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.idConcepto;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.idConcepto;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 9))
      return paramHistoricoSemana.idConcepto;
    return localStateManager.getIntField(paramHistoricoSemana, jdoInheritedFieldCount + 9, paramHistoricoSemana.idConcepto);
  }

  private static final void jdoSetidConcepto(HistoricoSemana paramHistoricoSemana, int paramInt)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.idConcepto = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.idConcepto = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoSemana, jdoInheritedFieldCount + 9, paramHistoricoSemana.idConcepto, paramInt);
  }

  private static final long jdoGetidHistoricoSemana(HistoricoSemana paramHistoricoSemana)
  {
    return paramHistoricoSemana.idHistoricoSemana;
  }

  private static final void jdoSetidHistoricoSemana(HistoricoSemana paramHistoricoSemana, long paramLong)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.idHistoricoSemana = paramLong;
      return;
    }
    localStateManager.setLongField(paramHistoricoSemana, jdoInheritedFieldCount + 10, paramHistoricoSemana.idHistoricoSemana, paramLong);
  }

  private static final int jdoGetmes(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.mes;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.mes;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 11))
      return paramHistoricoSemana.mes;
    return localStateManager.getIntField(paramHistoricoSemana, jdoInheritedFieldCount + 11, paramHistoricoSemana.mes);
  }

  private static final void jdoSetmes(HistoricoSemana paramHistoricoSemana, int paramInt)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoSemana, jdoInheritedFieldCount + 11, paramHistoricoSemana.mes, paramInt);
  }

  private static final int jdoGetmesSobretiempo(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.mesSobretiempo;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.mesSobretiempo;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 12))
      return paramHistoricoSemana.mesSobretiempo;
    return localStateManager.getIntField(paramHistoricoSemana, jdoInheritedFieldCount + 12, paramHistoricoSemana.mesSobretiempo);
  }

  private static final void jdoSetmesSobretiempo(HistoricoSemana paramHistoricoSemana, int paramInt)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.mesSobretiempo = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.mesSobretiempo = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoSemana, jdoInheritedFieldCount + 12, paramHistoricoSemana.mesSobretiempo, paramInt);
  }

  private static final double jdoGetmontoAporte(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.montoAporte;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.montoAporte;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 13))
      return paramHistoricoSemana.montoAporte;
    return localStateManager.getDoubleField(paramHistoricoSemana, jdoInheritedFieldCount + 13, paramHistoricoSemana.montoAporte);
  }

  private static final void jdoSetmontoAporte(HistoricoSemana paramHistoricoSemana, double paramDouble)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.montoAporte = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.montoAporte = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoSemana, jdoInheritedFieldCount + 13, paramHistoricoSemana.montoAporte, paramDouble);
  }

  private static final double jdoGetmontoAsigna(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.montoAsigna;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.montoAsigna;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 14))
      return paramHistoricoSemana.montoAsigna;
    return localStateManager.getDoubleField(paramHistoricoSemana, jdoInheritedFieldCount + 14, paramHistoricoSemana.montoAsigna);
  }

  private static final void jdoSetmontoAsigna(HistoricoSemana paramHistoricoSemana, double paramDouble)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.montoAsigna = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.montoAsigna = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoSemana, jdoInheritedFieldCount + 14, paramHistoricoSemana.montoAsigna, paramDouble);
  }

  private static final double jdoGetmontoDeduce(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.montoDeduce;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.montoDeduce;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 15))
      return paramHistoricoSemana.montoDeduce;
    return localStateManager.getDoubleField(paramHistoricoSemana, jdoInheritedFieldCount + 15, paramHistoricoSemana.montoDeduce);
  }

  private static final void jdoSetmontoDeduce(HistoricoSemana paramHistoricoSemana, double paramDouble)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.montoDeduce = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.montoDeduce = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoSemana, jdoInheritedFieldCount + 15, paramHistoricoSemana.montoDeduce, paramDouble);
  }

  private static final NominaEspecial jdoGetnominaEspecial(HistoricoSemana paramHistoricoSemana)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.nominaEspecial;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 16))
      return paramHistoricoSemana.nominaEspecial;
    return (NominaEspecial)localStateManager.getObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 16, paramHistoricoSemana.nominaEspecial);
  }

  private static final void jdoSetnominaEspecial(HistoricoSemana paramHistoricoSemana, NominaEspecial paramNominaEspecial)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.nominaEspecial = paramNominaEspecial;
      return;
    }
    localStateManager.setObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 16, paramHistoricoSemana.nominaEspecial, paramNominaEspecial);
  }

  private static final int jdoGetnumeroNomina(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.numeroNomina;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.numeroNomina;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 17))
      return paramHistoricoSemana.numeroNomina;
    return localStateManager.getIntField(paramHistoricoSemana, jdoInheritedFieldCount + 17, paramHistoricoSemana.numeroNomina);
  }

  private static final void jdoSetnumeroNomina(HistoricoSemana paramHistoricoSemana, int paramInt)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.numeroNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.numeroNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoSemana, jdoInheritedFieldCount + 17, paramHistoricoSemana.numeroNomina, paramInt);
  }

  private static final String jdoGetorigen(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.origen;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.origen;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 18))
      return paramHistoricoSemana.origen;
    return localStateManager.getStringField(paramHistoricoSemana, jdoInheritedFieldCount + 18, paramHistoricoSemana.origen);
  }

  private static final void jdoSetorigen(HistoricoSemana paramHistoricoSemana, String paramString)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.origen = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.origen = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoSemana, jdoInheritedFieldCount + 18, paramHistoricoSemana.origen, paramString);
  }

  private static final int jdoGetsemanaQuincena(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.semanaQuincena;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.semanaQuincena;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 19))
      return paramHistoricoSemana.semanaQuincena;
    return localStateManager.getIntField(paramHistoricoSemana, jdoInheritedFieldCount + 19, paramHistoricoSemana.semanaQuincena);
  }

  private static final void jdoSetsemanaQuincena(HistoricoSemana paramHistoricoSemana, int paramInt)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.semanaQuincena = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.semanaQuincena = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoSemana, jdoInheritedFieldCount + 19, paramHistoricoSemana.semanaQuincena, paramInt);
  }

  private static final TipoPersonal jdoGettipoPersonal(HistoricoSemana paramHistoricoSemana)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.tipoPersonal;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 20))
      return paramHistoricoSemana.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 20, paramHistoricoSemana.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(HistoricoSemana paramHistoricoSemana, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 20, paramHistoricoSemana.tipoPersonal, paramTipoPersonal);
  }

  private static final Trabajador jdoGettrabajador(HistoricoSemana paramHistoricoSemana)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.trabajador;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 21))
      return paramHistoricoSemana.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 21, paramHistoricoSemana.trabajador);
  }

  private static final void jdoSettrabajador(HistoricoSemana paramHistoricoSemana, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramHistoricoSemana, jdoInheritedFieldCount + 21, paramHistoricoSemana.trabajador, paramTrabajador);
  }

  private static final double jdoGetunidades(HistoricoSemana paramHistoricoSemana)
  {
    if (paramHistoricoSemana.jdoFlags <= 0)
      return paramHistoricoSemana.unidades;
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoSemana.unidades;
    if (localStateManager.isLoaded(paramHistoricoSemana, jdoInheritedFieldCount + 22))
      return paramHistoricoSemana.unidades;
    return localStateManager.getDoubleField(paramHistoricoSemana, jdoInheritedFieldCount + 22, paramHistoricoSemana.unidades);
  }

  private static final void jdoSetunidades(HistoricoSemana paramHistoricoSemana, double paramDouble)
  {
    if (paramHistoricoSemana.jdoFlags == 0)
    {
      paramHistoricoSemana.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoSemana.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoSemana.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoSemana, jdoInheritedFieldCount + 22, paramHistoricoSemana.unidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}