package sigefirrhh.personal.historico;

import java.io.Serializable;

public class MensajePrenominaPK
  implements Serializable
{
  public long idUltimaPrenomina;

  public MensajePrenominaPK()
  {
  }

  public MensajePrenominaPK(long idUltimaPrenomina)
  {
    this.idUltimaPrenomina = idUltimaPrenomina;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((MensajePrenominaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(MensajePrenominaPK thatPK)
  {
    return 
      this.idUltimaPrenomina == thatPK.idUltimaPrenomina;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUltimaPrenomina)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUltimaPrenomina);
  }
}