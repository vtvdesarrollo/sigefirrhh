package sigefirrhh.personal.historico;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.LugarPago;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.trabajador.Trabajador;

public class HistoricoNomina
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_SITUACION;
  protected static final Map LISTA_SINO;
  protected static final Map LISTA_PAGO;
  protected static final Map LISTA_CUENTA;
  private long idHistoricoNomina;
  private int numeroNomina;
  private int anio;
  private int mes;
  private int semanaQuincena;
  private Date fecha;
  private String estatus;
  private String situacion;
  private Dependencia dependencia;
  private String codDependencia;
  private Cargo cargo;
  private String codCargo;
  private int numeroRegistro;
  private int codigoNomina;
  private String formaPago;
  private Banco banco;
  private String tipoCtaNomina;
  private String cuentaNomina;
  private TipoPersonal tipoPersonal;
  private GrupoNomina grupoNomina;
  private NominaEspecial nominaEspecial;
  private LugarPago lugarPago;
  private Trabajador trabajador;
  private UnidadEjecutora unidadEjecutora;
  private String codUnidadEjecutora;
  private UnidadAdministradora unidadAdministradora;
  private String codUnidadAdminist;
  private Sede sede;
  private String codSede;
  private Region region;
  private String codRegion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "banco", "cargo", "codCargo", "codDependencia", "codRegion", "codSede", "codUnidadAdminist", "codUnidadEjecutora", "codigoNomina", "cuentaNomina", "dependencia", "estatus", "fecha", "formaPago", "grupoNomina", "idHistoricoNomina", "lugarPago", "mes", "nominaEspecial", "numeroNomina", "numeroRegistro", "region", "sede", "semanaQuincena", "situacion", "tipoCtaNomina", "tipoPersonal", "trabajador", "unidadAdministradora", "unidadEjecutora" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.LugarPago"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.procesoNomina.NominaEspecial"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Region"), sunjdo$classForName$("sigefirrhh.base.estructura.Sede"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), sunjdo$classForName$("sigefirrhh.base.estructura.UnidadAdministradora"), sunjdo$classForName$("sigefirrhh.base.estructura.UnidadEjecutora") }; private static final byte[] jdoFieldFlags = { 21, 26, 26, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 21, 26, 24, 26, 21, 26, 21, 21, 26, 26, 21, 21, 21, 26, 26, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.historico.HistoricoNomina"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HistoricoNomina());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_SITUACION = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_PAGO = 
      new LinkedHashMap();
    LISTA_CUENTA = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("A", "ACTIVO");
    LISTA_ESTATUS.put("E", "EGRESADO");
    LISTA_ESTATUS.put("S", "SUSPENDIDO");
    LISTA_SITUACION.put("1", "NORMAL");
    LISTA_SITUACION.put("2", "ENCARGADO");
    LISTA_SITUACION.put("3", "COMISION SERVICIO");
    LISTA_SITUACION.put("4", "VACACIONES");
    LISTA_SITUACION.put("5", "PROCESO JUBILACION");
    LISTA_SITUACION.put("6", "SUPLENTE");
    LISTA_SITUACION.put("7", "EN DISPONIBILIDAD");
    LISTA_SITUACION.put("8", "LICENCIA CON SUELDO");
    LISTA_SITUACION.put("9", "LICENCIA SIN SUELDO");
    LISTA_SITUACION.put("10", "SERVICIO EXTERIOR");
    LISTA_SITUACION.put("11", "SERVICIO INTERNO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
    LISTA_PAGO.put("1", "DEPOSITO");
    LISTA_PAGO.put("2", "CHEQUE");
    LISTA_PAGO.put("3", "EFECTIVO");
    LISTA_PAGO.put("T", "TRANSFERENCIA");
    LISTA_CUENTA.put("A", "AHORRO");
    LISTA_CUENTA.put("C", "CORRIENTE");
    LISTA_CUENTA.put("O", "OTRO");
    LISTA_CUENTA.put("N", "NO APLICA");
  }

  public HistoricoNomina()
  {
    jdoSetnumeroNomina(this, 0);

    jdoSetsemanaQuincena(this, 0);

    jdoSetestatus(this, "A");

    jdoSetsituacion(this, "1");
  }

  public String toString()
  {
    return jdoGettrabajador(this).getPersonal().getPrimerApellido() + " " + 
      jdoGettrabajador(this).getPersonal().getSegundoApellido() + ", " + 
      jdoGettrabajador(this).getPersonal().getPrimerNombre() + " - " + 
      jdoGettipoPersonal(this).getNombre();
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public void setAnio(int anio)
  {
    jdoSetanio(this, anio);
  }

  public Banco getBanco()
  {
    return jdoGetbanco(this);
  }

  public void setBanco(Banco banco)
  {
    jdoSetbanco(this, banco);
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public String getCodCargo()
  {
    return jdoGetcodCargo(this);
  }

  public void setCodCargo(String codCargo)
  {
    jdoSetcodCargo(this, codCargo);
  }

  public String getCodDependencia()
  {
    return jdoGetcodDependencia(this);
  }

  public void setCodDependencia(String codDependencia)
  {
    jdoSetcodDependencia(this, codDependencia);
  }

  public int getCodigoNomina()
  {
    return jdoGetcodigoNomina(this);
  }

  public void setCodigoNomina(int codigoNomina)
  {
    jdoSetcodigoNomina(this, codigoNomina);
  }

  public String getCodUnidadEjecutora()
  {
    return jdoGetcodUnidadEjecutora(this);
  }

  public void setCodUnidadEjecutora(String codUnidadEjecutora)
  {
    jdoSetcodUnidadEjecutora(this, codUnidadEjecutora);
  }

  public String getCuentaNomina()
  {
    return jdoGetcuentaNomina(this);
  }

  public void setCuentaNomina(String cuentaNomina)
  {
    jdoSetcuentaNomina(this, cuentaNomina);
  }

  public Dependencia getDependencia()
  {
    return jdoGetdependencia(this);
  }

  public void setDependencia(Dependencia dependencia)
  {
    jdoSetdependencia(this, dependencia);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String estatus)
  {
    jdoSetestatus(this, estatus);
  }

  public Date getFecha()
  {
    return jdoGetfecha(this);
  }

  public void setFecha(Date fecha)
  {
    jdoSetfecha(this, fecha);
  }

  public String getFormaPago()
  {
    return jdoGetformaPago(this);
  }

  public void setFormaPago(String formaPago)
  {
    jdoSetformaPago(this, formaPago);
  }

  public GrupoNomina getGrupoNomina()
  {
    return jdoGetgrupoNomina(this);
  }

  public void setGrupoNomina(GrupoNomina grupoNomina)
  {
    jdoSetgrupoNomina(this, grupoNomina);
  }

  public long getIdHistoricoNomina()
  {
    return jdoGetidHistoricoNomina(this);
  }

  public void setIdHistoricoNomina(long idHistoricoNomina)
  {
    jdoSetidHistoricoNomina(this, idHistoricoNomina);
  }

  public LugarPago getLugarPago()
  {
    return jdoGetlugarPago(this);
  }

  public void setLugarPago(LugarPago lugarPago)
  {
    jdoSetlugarPago(this, lugarPago);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public void setMes(int mes)
  {
    jdoSetmes(this, mes);
  }

  public NominaEspecial getNominaEspecial()
  {
    return jdoGetnominaEspecial(this);
  }

  public void setNominaEspecial(NominaEspecial nominaEspecial)
  {
    jdoSetnominaEspecial(this, nominaEspecial);
  }

  public int getNumeroNomina()
  {
    return jdoGetnumeroNomina(this);
  }

  public void setNumeroNomina(int numeroNomina)
  {
    jdoSetnumeroNomina(this, numeroNomina);
  }

  public int getNumeroRegistro()
  {
    return jdoGetnumeroRegistro(this);
  }

  public void setNumeroRegistro(int numeroRegistro)
  {
    jdoSetnumeroRegistro(this, numeroRegistro);
  }

  public int getSemanaQuincena()
  {
    return jdoGetsemanaQuincena(this);
  }

  public void setSemanaQuincena(int semanaQuincena)
  {
    jdoSetsemanaQuincena(this, semanaQuincena);
  }

  public String getSituacion()
  {
    return jdoGetsituacion(this);
  }

  public void setSituacion(String situacion)
  {
    jdoSetsituacion(this, situacion);
  }

  public String getTipoCtaNomina()
  {
    return jdoGettipoCtaNomina(this);
  }

  public void setTipoCtaNomina(String tipoCtaNomina)
  {
    jdoSettipoCtaNomina(this, tipoCtaNomina);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal)
  {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public UnidadAdministradora getUnidadAdministradora()
  {
    return jdoGetunidadAdministradora(this);
  }

  public void setUnidadAdministradora(UnidadAdministradora unidadAdministradora)
  {
    jdoSetunidadAdministradora(this, unidadAdministradora);
  }

  public UnidadEjecutora getUnidadEjecutora()
  {
    return jdoGetunidadEjecutora(this);
  }

  public void setUnidadEjecutora(UnidadEjecutora unidadEjecutora)
  {
    jdoSetunidadEjecutora(this, unidadEjecutora);
  }
  public String getCodRegion() {
    return jdoGetcodRegion(this);
  }
  public void setCodRegion(String codRegion) {
    jdoSetcodRegion(this, codRegion);
  }
  public String getCodSede() {
    return jdoGetcodSede(this);
  }
  public void setCodSede(String codSede) {
    jdoSetcodSede(this, codSede);
  }
  public String getCodUnidadAdminist() {
    return jdoGetcodUnidadAdminist(this);
  }
  public void setCodUnidadAdminist(String codUnidadAdminist) {
    jdoSetcodUnidadAdminist(this, codUnidadAdminist);
  }
  public Region getRegion() {
    return jdoGetregion(this);
  }
  public void setRegion(Region region) {
    jdoSetregion(this, region);
  }
  public Sede getSede() {
    return jdoGetsede(this);
  }
  public void setSede(Sede sede) {
    jdoSetsede(this, sede);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 31;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HistoricoNomina localHistoricoNomina = new HistoricoNomina();
    localHistoricoNomina.jdoFlags = 1;
    localHistoricoNomina.jdoStateManager = paramStateManager;
    return localHistoricoNomina;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HistoricoNomina localHistoricoNomina = new HistoricoNomina();
    localHistoricoNomina.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHistoricoNomina.jdoFlags = 1;
    localHistoricoNomina.jdoStateManager = paramStateManager;
    return localHistoricoNomina;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.banco);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codDependencia);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codRegion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSede);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codUnidadAdminist);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codUnidadEjecutora);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNomina);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cuentaNomina);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.formaPago);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHistoricoNomina);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.lugarPago);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nominaEspecial);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroNomina);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroRegistro);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.region);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.sede);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanaQuincena);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.situacion);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoCtaNomina);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadAdministradora);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadEjecutora);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.banco = ((Banco)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codUnidadAdminist = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codUnidadEjecutora = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuentaNomina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.formaPago = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHistoricoNomina = localStateManager.replacingLongField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lugarPago = ((LugarPago)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nominaEspecial = ((NominaEspecial)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroRegistro = localStateManager.replacingIntField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.region = ((Region)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sede = ((Sede)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanaQuincena = localStateManager.replacingIntField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.situacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCtaNomina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadAdministradora = ((UnidadAdministradora)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadEjecutora = ((UnidadEjecutora)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HistoricoNomina paramHistoricoNomina, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramHistoricoNomina.anio;
      return;
    case 1:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.banco = paramHistoricoNomina.banco;
      return;
    case 2:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramHistoricoNomina.cargo;
      return;
    case 3:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.codCargo = paramHistoricoNomina.codCargo;
      return;
    case 4:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.codDependencia = paramHistoricoNomina.codDependencia;
      return;
    case 5:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.codRegion = paramHistoricoNomina.codRegion;
      return;
    case 6:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.codSede = paramHistoricoNomina.codSede;
      return;
    case 7:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.codUnidadAdminist = paramHistoricoNomina.codUnidadAdminist;
      return;
    case 8:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.codUnidadEjecutora = paramHistoricoNomina.codUnidadEjecutora;
      return;
    case 9:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNomina = paramHistoricoNomina.codigoNomina;
      return;
    case 10:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.cuentaNomina = paramHistoricoNomina.cuentaNomina;
      return;
    case 11:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramHistoricoNomina.dependencia;
      return;
    case 12:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramHistoricoNomina.estatus;
      return;
    case 13:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramHistoricoNomina.fecha;
      return;
    case 14:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.formaPago = paramHistoricoNomina.formaPago;
      return;
    case 15:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramHistoricoNomina.grupoNomina;
      return;
    case 16:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.idHistoricoNomina = paramHistoricoNomina.idHistoricoNomina;
      return;
    case 17:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.lugarPago = paramHistoricoNomina.lugarPago;
      return;
    case 18:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramHistoricoNomina.mes;
      return;
    case 19:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.nominaEspecial = paramHistoricoNomina.nominaEspecial;
      return;
    case 20:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.numeroNomina = paramHistoricoNomina.numeroNomina;
      return;
    case 21:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.numeroRegistro = paramHistoricoNomina.numeroRegistro;
      return;
    case 22:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.region = paramHistoricoNomina.region;
      return;
    case 23:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.sede = paramHistoricoNomina.sede;
      return;
    case 24:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.semanaQuincena = paramHistoricoNomina.semanaQuincena;
      return;
    case 25:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.situacion = paramHistoricoNomina.situacion;
      return;
    case 26:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCtaNomina = paramHistoricoNomina.tipoCtaNomina;
      return;
    case 27:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramHistoricoNomina.tipoPersonal;
      return;
    case 28:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramHistoricoNomina.trabajador;
      return;
    case 29:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.unidadAdministradora = paramHistoricoNomina.unidadAdministradora;
      return;
    case 30:
      if (paramHistoricoNomina == null)
        throw new IllegalArgumentException("arg1");
      this.unidadEjecutora = paramHistoricoNomina.unidadEjecutora;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HistoricoNomina))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HistoricoNomina localHistoricoNomina = (HistoricoNomina)paramObject;
    if (localHistoricoNomina.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHistoricoNomina, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HistoricoNominaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HistoricoNominaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoNominaPK))
      throw new IllegalArgumentException("arg1");
    HistoricoNominaPK localHistoricoNominaPK = (HistoricoNominaPK)paramObject;
    localHistoricoNominaPK.idHistoricoNomina = this.idHistoricoNomina;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoNominaPK))
      throw new IllegalArgumentException("arg1");
    HistoricoNominaPK localHistoricoNominaPK = (HistoricoNominaPK)paramObject;
    this.idHistoricoNomina = localHistoricoNominaPK.idHistoricoNomina;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoNominaPK))
      throw new IllegalArgumentException("arg2");
    HistoricoNominaPK localHistoricoNominaPK = (HistoricoNominaPK)paramObject;
    localHistoricoNominaPK.idHistoricoNomina = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 16);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoNominaPK))
      throw new IllegalArgumentException("arg2");
    HistoricoNominaPK localHistoricoNominaPK = (HistoricoNominaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 16, localHistoricoNominaPK.idHistoricoNomina);
  }

  private static final int jdoGetanio(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.anio;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.anio;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 0))
      return paramHistoricoNomina.anio;
    return localStateManager.getIntField(paramHistoricoNomina, jdoInheritedFieldCount + 0, paramHistoricoNomina.anio);
  }

  private static final void jdoSetanio(HistoricoNomina paramHistoricoNomina, int paramInt)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoNomina, jdoInheritedFieldCount + 0, paramHistoricoNomina.anio, paramInt);
  }

  private static final Banco jdoGetbanco(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.banco;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 1))
      return paramHistoricoNomina.banco;
    return (Banco)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 1, paramHistoricoNomina.banco);
  }

  private static final void jdoSetbanco(HistoricoNomina paramHistoricoNomina, Banco paramBanco)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.banco = paramBanco;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 1, paramHistoricoNomina.banco, paramBanco);
  }

  private static final Cargo jdoGetcargo(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.cargo;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 2))
      return paramHistoricoNomina.cargo;
    return (Cargo)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 2, paramHistoricoNomina.cargo);
  }

  private static final void jdoSetcargo(HistoricoNomina paramHistoricoNomina, Cargo paramCargo)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 2, paramHistoricoNomina.cargo, paramCargo);
  }

  private static final String jdoGetcodCargo(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.codCargo;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.codCargo;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 3))
      return paramHistoricoNomina.codCargo;
    return localStateManager.getStringField(paramHistoricoNomina, jdoInheritedFieldCount + 3, paramHistoricoNomina.codCargo);
  }

  private static final void jdoSetcodCargo(HistoricoNomina paramHistoricoNomina, String paramString)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.codCargo = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.codCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoNomina, jdoInheritedFieldCount + 3, paramHistoricoNomina.codCargo, paramString);
  }

  private static final String jdoGetcodDependencia(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.codDependencia;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.codDependencia;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 4))
      return paramHistoricoNomina.codDependencia;
    return localStateManager.getStringField(paramHistoricoNomina, jdoInheritedFieldCount + 4, paramHistoricoNomina.codDependencia);
  }

  private static final void jdoSetcodDependencia(HistoricoNomina paramHistoricoNomina, String paramString)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.codDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.codDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoNomina, jdoInheritedFieldCount + 4, paramHistoricoNomina.codDependencia, paramString);
  }

  private static final String jdoGetcodRegion(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.codRegion;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.codRegion;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 5))
      return paramHistoricoNomina.codRegion;
    return localStateManager.getStringField(paramHistoricoNomina, jdoInheritedFieldCount + 5, paramHistoricoNomina.codRegion);
  }

  private static final void jdoSetcodRegion(HistoricoNomina paramHistoricoNomina, String paramString)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.codRegion = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.codRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoNomina, jdoInheritedFieldCount + 5, paramHistoricoNomina.codRegion, paramString);
  }

  private static final String jdoGetcodSede(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.codSede;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.codSede;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 6))
      return paramHistoricoNomina.codSede;
    return localStateManager.getStringField(paramHistoricoNomina, jdoInheritedFieldCount + 6, paramHistoricoNomina.codSede);
  }

  private static final void jdoSetcodSede(HistoricoNomina paramHistoricoNomina, String paramString)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.codSede = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.codSede = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoNomina, jdoInheritedFieldCount + 6, paramHistoricoNomina.codSede, paramString);
  }

  private static final String jdoGetcodUnidadAdminist(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.codUnidadAdminist;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.codUnidadAdminist;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 7))
      return paramHistoricoNomina.codUnidadAdminist;
    return localStateManager.getStringField(paramHistoricoNomina, jdoInheritedFieldCount + 7, paramHistoricoNomina.codUnidadAdminist);
  }

  private static final void jdoSetcodUnidadAdminist(HistoricoNomina paramHistoricoNomina, String paramString)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.codUnidadAdminist = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.codUnidadAdminist = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoNomina, jdoInheritedFieldCount + 7, paramHistoricoNomina.codUnidadAdminist, paramString);
  }

  private static final String jdoGetcodUnidadEjecutora(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.codUnidadEjecutora;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.codUnidadEjecutora;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 8))
      return paramHistoricoNomina.codUnidadEjecutora;
    return localStateManager.getStringField(paramHistoricoNomina, jdoInheritedFieldCount + 8, paramHistoricoNomina.codUnidadEjecutora);
  }

  private static final void jdoSetcodUnidadEjecutora(HistoricoNomina paramHistoricoNomina, String paramString)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.codUnidadEjecutora = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.codUnidadEjecutora = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoNomina, jdoInheritedFieldCount + 8, paramHistoricoNomina.codUnidadEjecutora, paramString);
  }

  private static final int jdoGetcodigoNomina(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.codigoNomina;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.codigoNomina;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 9))
      return paramHistoricoNomina.codigoNomina;
    return localStateManager.getIntField(paramHistoricoNomina, jdoInheritedFieldCount + 9, paramHistoricoNomina.codigoNomina);
  }

  private static final void jdoSetcodigoNomina(HistoricoNomina paramHistoricoNomina, int paramInt)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.codigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.codigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoNomina, jdoInheritedFieldCount + 9, paramHistoricoNomina.codigoNomina, paramInt);
  }

  private static final String jdoGetcuentaNomina(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.cuentaNomina;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.cuentaNomina;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 10))
      return paramHistoricoNomina.cuentaNomina;
    return localStateManager.getStringField(paramHistoricoNomina, jdoInheritedFieldCount + 10, paramHistoricoNomina.cuentaNomina);
  }

  private static final void jdoSetcuentaNomina(HistoricoNomina paramHistoricoNomina, String paramString)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.cuentaNomina = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.cuentaNomina = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoNomina, jdoInheritedFieldCount + 10, paramHistoricoNomina.cuentaNomina, paramString);
  }

  private static final Dependencia jdoGetdependencia(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.dependencia;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 11))
      return paramHistoricoNomina.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 11, paramHistoricoNomina.dependencia);
  }

  private static final void jdoSetdependencia(HistoricoNomina paramHistoricoNomina, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 11, paramHistoricoNomina.dependencia, paramDependencia);
  }

  private static final String jdoGetestatus(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.estatus;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.estatus;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 12))
      return paramHistoricoNomina.estatus;
    return localStateManager.getStringField(paramHistoricoNomina, jdoInheritedFieldCount + 12, paramHistoricoNomina.estatus);
  }

  private static final void jdoSetestatus(HistoricoNomina paramHistoricoNomina, String paramString)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoNomina, jdoInheritedFieldCount + 12, paramHistoricoNomina.estatus, paramString);
  }

  private static final Date jdoGetfecha(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.fecha;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.fecha;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 13))
      return paramHistoricoNomina.fecha;
    return (Date)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 13, paramHistoricoNomina.fecha);
  }

  private static final void jdoSetfecha(HistoricoNomina paramHistoricoNomina, Date paramDate)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 13, paramHistoricoNomina.fecha, paramDate);
  }

  private static final String jdoGetformaPago(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.formaPago;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.formaPago;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 14))
      return paramHistoricoNomina.formaPago;
    return localStateManager.getStringField(paramHistoricoNomina, jdoInheritedFieldCount + 14, paramHistoricoNomina.formaPago);
  }

  private static final void jdoSetformaPago(HistoricoNomina paramHistoricoNomina, String paramString)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.formaPago = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.formaPago = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoNomina, jdoInheritedFieldCount + 14, paramHistoricoNomina.formaPago, paramString);
  }

  private static final GrupoNomina jdoGetgrupoNomina(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.grupoNomina;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 15))
      return paramHistoricoNomina.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 15, paramHistoricoNomina.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(HistoricoNomina paramHistoricoNomina, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 15, paramHistoricoNomina.grupoNomina, paramGrupoNomina);
  }

  private static final long jdoGetidHistoricoNomina(HistoricoNomina paramHistoricoNomina)
  {
    return paramHistoricoNomina.idHistoricoNomina;
  }

  private static final void jdoSetidHistoricoNomina(HistoricoNomina paramHistoricoNomina, long paramLong)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.idHistoricoNomina = paramLong;
      return;
    }
    localStateManager.setLongField(paramHistoricoNomina, jdoInheritedFieldCount + 16, paramHistoricoNomina.idHistoricoNomina, paramLong);
  }

  private static final LugarPago jdoGetlugarPago(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.lugarPago;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 17))
      return paramHistoricoNomina.lugarPago;
    return (LugarPago)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 17, paramHistoricoNomina.lugarPago);
  }

  private static final void jdoSetlugarPago(HistoricoNomina paramHistoricoNomina, LugarPago paramLugarPago)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.lugarPago = paramLugarPago;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 17, paramHistoricoNomina.lugarPago, paramLugarPago);
  }

  private static final int jdoGetmes(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.mes;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.mes;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 18))
      return paramHistoricoNomina.mes;
    return localStateManager.getIntField(paramHistoricoNomina, jdoInheritedFieldCount + 18, paramHistoricoNomina.mes);
  }

  private static final void jdoSetmes(HistoricoNomina paramHistoricoNomina, int paramInt)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoNomina, jdoInheritedFieldCount + 18, paramHistoricoNomina.mes, paramInt);
  }

  private static final NominaEspecial jdoGetnominaEspecial(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.nominaEspecial;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 19))
      return paramHistoricoNomina.nominaEspecial;
    return (NominaEspecial)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 19, paramHistoricoNomina.nominaEspecial);
  }

  private static final void jdoSetnominaEspecial(HistoricoNomina paramHistoricoNomina, NominaEspecial paramNominaEspecial)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.nominaEspecial = paramNominaEspecial;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 19, paramHistoricoNomina.nominaEspecial, paramNominaEspecial);
  }

  private static final int jdoGetnumeroNomina(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.numeroNomina;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.numeroNomina;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 20))
      return paramHistoricoNomina.numeroNomina;
    return localStateManager.getIntField(paramHistoricoNomina, jdoInheritedFieldCount + 20, paramHistoricoNomina.numeroNomina);
  }

  private static final void jdoSetnumeroNomina(HistoricoNomina paramHistoricoNomina, int paramInt)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.numeroNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.numeroNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoNomina, jdoInheritedFieldCount + 20, paramHistoricoNomina.numeroNomina, paramInt);
  }

  private static final int jdoGetnumeroRegistro(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.numeroRegistro;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.numeroRegistro;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 21))
      return paramHistoricoNomina.numeroRegistro;
    return localStateManager.getIntField(paramHistoricoNomina, jdoInheritedFieldCount + 21, paramHistoricoNomina.numeroRegistro);
  }

  private static final void jdoSetnumeroRegistro(HistoricoNomina paramHistoricoNomina, int paramInt)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.numeroRegistro = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.numeroRegistro = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoNomina, jdoInheritedFieldCount + 21, paramHistoricoNomina.numeroRegistro, paramInt);
  }

  private static final Region jdoGetregion(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.region;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 22))
      return paramHistoricoNomina.region;
    return (Region)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 22, paramHistoricoNomina.region);
  }

  private static final void jdoSetregion(HistoricoNomina paramHistoricoNomina, Region paramRegion)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.region = paramRegion;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 22, paramHistoricoNomina.region, paramRegion);
  }

  private static final Sede jdoGetsede(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.sede;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 23))
      return paramHistoricoNomina.sede;
    return (Sede)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 23, paramHistoricoNomina.sede);
  }

  private static final void jdoSetsede(HistoricoNomina paramHistoricoNomina, Sede paramSede)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.sede = paramSede;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 23, paramHistoricoNomina.sede, paramSede);
  }

  private static final int jdoGetsemanaQuincena(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.semanaQuincena;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.semanaQuincena;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 24))
      return paramHistoricoNomina.semanaQuincena;
    return localStateManager.getIntField(paramHistoricoNomina, jdoInheritedFieldCount + 24, paramHistoricoNomina.semanaQuincena);
  }

  private static final void jdoSetsemanaQuincena(HistoricoNomina paramHistoricoNomina, int paramInt)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.semanaQuincena = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.semanaQuincena = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoNomina, jdoInheritedFieldCount + 24, paramHistoricoNomina.semanaQuincena, paramInt);
  }

  private static final String jdoGetsituacion(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.situacion;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.situacion;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 25))
      return paramHistoricoNomina.situacion;
    return localStateManager.getStringField(paramHistoricoNomina, jdoInheritedFieldCount + 25, paramHistoricoNomina.situacion);
  }

  private static final void jdoSetsituacion(HistoricoNomina paramHistoricoNomina, String paramString)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.situacion = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.situacion = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoNomina, jdoInheritedFieldCount + 25, paramHistoricoNomina.situacion, paramString);
  }

  private static final String jdoGettipoCtaNomina(HistoricoNomina paramHistoricoNomina)
  {
    if (paramHistoricoNomina.jdoFlags <= 0)
      return paramHistoricoNomina.tipoCtaNomina;
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.tipoCtaNomina;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 26))
      return paramHistoricoNomina.tipoCtaNomina;
    return localStateManager.getStringField(paramHistoricoNomina, jdoInheritedFieldCount + 26, paramHistoricoNomina.tipoCtaNomina);
  }

  private static final void jdoSettipoCtaNomina(HistoricoNomina paramHistoricoNomina, String paramString)
  {
    if (paramHistoricoNomina.jdoFlags == 0)
    {
      paramHistoricoNomina.tipoCtaNomina = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.tipoCtaNomina = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoNomina, jdoInheritedFieldCount + 26, paramHistoricoNomina.tipoCtaNomina, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.tipoPersonal;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 27))
      return paramHistoricoNomina.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 27, paramHistoricoNomina.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(HistoricoNomina paramHistoricoNomina, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 27, paramHistoricoNomina.tipoPersonal, paramTipoPersonal);
  }

  private static final Trabajador jdoGettrabajador(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.trabajador;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 28))
      return paramHistoricoNomina.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 28, paramHistoricoNomina.trabajador);
  }

  private static final void jdoSettrabajador(HistoricoNomina paramHistoricoNomina, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 28, paramHistoricoNomina.trabajador, paramTrabajador);
  }

  private static final UnidadAdministradora jdoGetunidadAdministradora(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.unidadAdministradora;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 29))
      return paramHistoricoNomina.unidadAdministradora;
    return (UnidadAdministradora)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 29, paramHistoricoNomina.unidadAdministradora);
  }

  private static final void jdoSetunidadAdministradora(HistoricoNomina paramHistoricoNomina, UnidadAdministradora paramUnidadAdministradora)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.unidadAdministradora = paramUnidadAdministradora;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 29, paramHistoricoNomina.unidadAdministradora, paramUnidadAdministradora);
  }

  private static final UnidadEjecutora jdoGetunidadEjecutora(HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNomina.unidadEjecutora;
    if (localStateManager.isLoaded(paramHistoricoNomina, jdoInheritedFieldCount + 30))
      return paramHistoricoNomina.unidadEjecutora;
    return (UnidadEjecutora)localStateManager.getObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 30, paramHistoricoNomina.unidadEjecutora);
  }

  private static final void jdoSetunidadEjecutora(HistoricoNomina paramHistoricoNomina, UnidadEjecutora paramUnidadEjecutora)
  {
    StateManager localStateManager = paramHistoricoNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNomina.unidadEjecutora = paramUnidadEjecutora;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNomina, jdoInheritedFieldCount + 30, paramHistoricoNomina.unidadEjecutora, paramUnidadEjecutora);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}