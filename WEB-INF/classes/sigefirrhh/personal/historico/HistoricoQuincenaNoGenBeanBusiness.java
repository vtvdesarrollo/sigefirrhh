package sigefirrhh.personal.historico;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class HistoricoQuincenaNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByConceptoTipoPersonalAndTrabajadorAndFecha(long idConceptoTipoPersonal, long idTrabajador, Date fechaInicio, Date fechaFin)
    throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal && fecha >= pFechaInicio && fecha <= pFechaFin";
    Query query = pm.newQuery(HistoricoQuincena.class, filter);

    query.declareParameters("long pIdHistoricoQuincena, long pIdTrabajador, Date pFechaInicio, Date pFechaFin");

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));
    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pFechaInicio", fechaInicio);
    parameters.put("pFechaFin", fechaFin);

    Collection colHistoricoQuincena = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colHistoricoQuincena;
  }

  public Collection findByConceptoTipoPersonalAndTrabajador(long idConceptoTipoPersonal, long idTrabajador, int anio, int mes, int semanaQuincena)
    throws Exception
  {
    HashMap parameters = new HashMap();
    StringBuffer filter = new StringBuffer();
    PersistenceManager pm = PMThread.getPM();

    filter.append("anio == pAnio && trabajador.idTrabajador == pIdTrabajador");
    if (idConceptoTipoPersonal != 0L) {
      filter.append(" && conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal");
    }
    if (mes != 0) {
      filter.append(" && mes == pMes");
    }
    if (semanaQuincena != 0) {
      filter.append(" && semanaQuincena == pSemanaQuincena");
    }

    Query query = pm.newQuery(HistoricoQuincena.class, filter.toString());

    query.declareParameters("long pIdTrabajador, long pIdConceptoTipoPersonal, int pAnio, int pMes, int pSemanaQuincena");

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));
    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pAnio", new Integer(anio));
    parameters.put("pMes", new Integer(mes));
    parameters.put("pSemanaQuincena", new Integer(semanaQuincena));

    query.setOrdering("mes ascending, semanaQuincena ascending, conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colHistoricoQuincena = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colHistoricoQuincena;
  }

  public Collection findByTrabajadorAndNumeroNomina(long idTrabajador, int numeroNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && numeroNomina == pNumeroNomina";

    Query query = pm.newQuery(HistoricoQuincena.class, filter);

    query.declareParameters("long pIdTrabajador, int pNumeroNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pNumeroNomina", new Integer(numeroNomina));

    query.setOrdering("anio descending, mes descending, semanaQuincena descending");

    Collection colHistoricoQuincena = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHistoricoQuincena);

    return colHistoricoQuincena;
  }
}