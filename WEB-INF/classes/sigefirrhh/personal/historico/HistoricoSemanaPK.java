package sigefirrhh.personal.historico;

import java.io.Serializable;

public class HistoricoSemanaPK
  implements Serializable
{
  public long idHistoricoSemana;

  public HistoricoSemanaPK()
  {
  }

  public HistoricoSemanaPK(long idHistoricoSemana)
  {
    this.idHistoricoSemana = idHistoricoSemana;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HistoricoSemanaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HistoricoSemanaPK thatPK)
  {
    return 
      this.idHistoricoSemana == thatPK.idHistoricoSemana;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHistoricoSemana)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHistoricoSemana);
  }
}