package sigefirrhh.personal.historico;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.LugarPago;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.procesoNomina.ProcesoNominaFacade;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;

public class HistoricoNominaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(HistoricoNominaForm.class.getName());
  private HistoricoNomina historicoNomina;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private CargoFacade cargoFacade = new CargoFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private ProcesoNominaFacade procesoNominaFacade = new ProcesoNominaFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private HistoricoFacade historicoFacade = new HistoricoFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colDependencia;
  private Collection colCargo;
  private Collection colBanco;
  private Collection colTipoPersonal;
  private Collection colGrupoNomina;
  private Collection colNominaEspecial;
  private Collection colLugarPago;
  private Collection colTrabajador;
  private Collection colUnidadEjecutora;
  private Collection colUnidadAdministradora;
  private Collection colSede;
  private Collection colRegion;
  private String selectDependencia;
  private String selectCargo;
  private String selectBanco;
  private String selectTipoPersonal;
  private String selectGrupoNomina;
  private String selectNominaEspecial;
  private String selectLugarPago;
  private String selectTrabajador;
  private String selectUnidadEjecutora;
  private String selectUnidadAdministradora;
  private String selectSede;
  private String selectRegion;
  private Collection findColTipoPersonal;
  private Object stateResultHistoricoNominaByTrabajador = null;

  public Collection getFindColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectDependencia()
  {
    return this.selectDependencia;
  }
  public void setSelectDependencia(String valDependencia) {
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    this.historicoNomina.setDependencia(null);
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      if (String.valueOf(dependencia.getIdDependencia()).equals(
        valDependencia)) {
        this.historicoNomina.setDependencia(
          dependencia);
        break;
      }
    }
    this.selectDependencia = valDependencia;
  }
  public String getSelectCargo() {
    return this.selectCargo;
  }
  public void setSelectCargo(String valCargo) {
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    this.historicoNomina.setCargo(null);
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        valCargo)) {
        this.historicoNomina.setCargo(
          cargo);
        break;
      }
    }
    this.selectCargo = valCargo;
  }
  public String getSelectBanco() {
    return this.selectBanco;
  }
  public void setSelectBanco(String valBanco) {
    Iterator iterator = this.colBanco.iterator();
    Banco banco = null;
    this.historicoNomina.setBanco(null);
    while (iterator.hasNext()) {
      banco = (Banco)iterator.next();
      if (String.valueOf(banco.getIdBanco()).equals(
        valBanco)) {
        this.historicoNomina.setBanco(
          banco);
        break;
      }
    }
    this.selectBanco = valBanco;
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.historicoNomina.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.historicoNomina.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String valGrupoNomina) {
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    this.historicoNomina.setGrupoNomina(null);
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      if (String.valueOf(grupoNomina.getIdGrupoNomina()).equals(
        valGrupoNomina)) {
        this.historicoNomina.setGrupoNomina(
          grupoNomina);
        break;
      }
    }
    this.selectGrupoNomina = valGrupoNomina;
  }
  public String getSelectNominaEspecial() {
    return this.selectNominaEspecial;
  }
  public void setSelectNominaEspecial(String valNominaEspecial) {
    Iterator iterator = this.colNominaEspecial.iterator();
    NominaEspecial nominaEspecial = null;
    this.historicoNomina.setNominaEspecial(null);
    while (iterator.hasNext()) {
      nominaEspecial = (NominaEspecial)iterator.next();
      if (String.valueOf(nominaEspecial.getIdNominaEspecial()).equals(
        valNominaEspecial)) {
        this.historicoNomina.setNominaEspecial(
          nominaEspecial);
        break;
      }
    }
    this.selectNominaEspecial = valNominaEspecial;
  }
  public String getSelectLugarPago() {
    return this.selectLugarPago;
  }
  public void setSelectLugarPago(String valLugarPago) {
    Iterator iterator = this.colLugarPago.iterator();
    LugarPago lugarPago = null;
    this.historicoNomina.setLugarPago(null);
    while (iterator.hasNext()) {
      lugarPago = (LugarPago)iterator.next();
      if (String.valueOf(lugarPago.getIdLugarPago()).equals(
        valLugarPago)) {
        this.historicoNomina.setLugarPago(
          lugarPago);
        break;
      }
    }
    this.selectLugarPago = valLugarPago;
  }
  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    this.historicoNomina.setTrabajador(null);
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      if (String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador)) {
        this.historicoNomina.setTrabajador(
          trabajador);
        break;
      }
    }
    this.selectTrabajador = valTrabajador;
  }
  public String getSelectUnidadEjecutora() {
    return this.selectUnidadEjecutora;
  }
  public void setSelectUnidadEjecutora(String valUnidadEjecutora) {
    Iterator iterator = this.colUnidadEjecutora.iterator();
    UnidadEjecutora unidadEjecutora = null;
    this.historicoNomina.setUnidadEjecutora(null);
    while (iterator.hasNext()) {
      unidadEjecutora = (UnidadEjecutora)iterator.next();
      if (String.valueOf(unidadEjecutora.getIdUnidadEjecutora()).equals(
        valUnidadEjecutora)) {
        this.historicoNomina.setUnidadEjecutora(
          unidadEjecutora);
        break;
      }
    }
    this.selectUnidadEjecutora = valUnidadEjecutora;
  }
  public String getSelectUnidadAdministradora() {
    return this.selectUnidadAdministradora;
  }
  public void setSelectUnidadAdministradora(String valUnidadAdministradora) {
    Iterator iterator = this.colUnidadAdministradora.iterator();
    UnidadAdministradora unidadAdministradora = null;
    this.historicoNomina.setUnidadAdministradora(null);
    while (iterator.hasNext()) {
      unidadAdministradora = (UnidadAdministradora)iterator.next();
      if (String.valueOf(unidadAdministradora.getIdUnidadAdministradora()).equals(
        valUnidadAdministradora)) {
        this.historicoNomina.setUnidadAdministradora(
          unidadAdministradora);
        break;
      }
    }
    this.selectUnidadAdministradora = valUnidadAdministradora;
  }
  public String getSelectSede() {
    return this.selectSede;
  }
  public void setSelectSede(String valSede) {
    Iterator iterator = this.colSede.iterator();
    Sede sede = null;
    this.historicoNomina.setSede(null);
    while (iterator.hasNext()) {
      sede = (Sede)iterator.next();
      if (String.valueOf(sede.getIdSede()).equals(
        valSede)) {
        this.historicoNomina.setSede(
          sede);
        break;
      }
    }
    this.selectSede = valSede;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }
  public void setSelectRegion(String valRegion) {
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    this.historicoNomina.setRegion(null);
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      if (String.valueOf(region.getIdRegion()).equals(
        valRegion)) {
        this.historicoNomina.setRegion(
          region);
        break;
      }
    }
    this.selectRegion = valRegion;
  }
  public Collection getResult() {
    return this.result;
  }

  public HistoricoNomina getHistoricoNomina() {
    if (this.historicoNomina == null) {
      this.historicoNomina = new HistoricoNomina();
    }
    return this.historicoNomina;
  }

  public HistoricoNominaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = HistoricoNomina.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListSituacion() {
    Collection col = new ArrayList();

    Iterator iterEntry = HistoricoNomina.LISTA_SITUACION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getListFormaPago()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = HistoricoNomina.LISTA_PAGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColBanco()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colBanco.iterator();
    Banco banco = null;
    while (iterator.hasNext()) {
      banco = (Banco)iterator.next();
      col.add(new SelectItem(
        String.valueOf(banco.getIdBanco()), 
        banco.toString()));
    }
    return col;
  }

  public Collection getListTipoCtaNomina() {
    Collection col = new ArrayList();

    Iterator iterEntry = HistoricoNomina.LISTA_CUENTA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoNomina.getIdGrupoNomina()), 
        grupoNomina.toString()));
    }
    return col;
  }

  public Collection getColNominaEspecial()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colNominaEspecial.iterator();
    NominaEspecial nominaEspecial = null;
    while (iterator.hasNext()) {
      nominaEspecial = (NominaEspecial)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nominaEspecial.getIdNominaEspecial()), 
        nominaEspecial.toString()));
    }
    return col;
  }

  public Collection getColLugarPago()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colLugarPago.iterator();
    LugarPago lugarPago = null;
    while (iterator.hasNext()) {
      lugarPago = (LugarPago)iterator.next();
      col.add(new SelectItem(
        String.valueOf(lugarPago.getIdLugarPago()), 
        lugarPago.toString()));
    }
    return col;
  }

  public Collection getColTrabajador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public Collection getColUnidadEjecutora()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadEjecutora.iterator();
    UnidadEjecutora unidadEjecutora = null;
    while (iterator.hasNext()) {
      unidadEjecutora = (UnidadEjecutora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadEjecutora.getIdUnidadEjecutora()), 
        unidadEjecutora.toString()));
    }
    return col;
  }

  public Collection getColUnidadAdministradora()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadAdministradora.iterator();
    UnidadAdministradora unidadAdministradora = null;
    while (iterator.hasNext()) {
      unidadAdministradora = (UnidadAdministradora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadAdministradora.getIdUnidadAdministradora()), 
        unidadAdministradora.toString()));
    }
    return col;
  }

  public Collection getColSede()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colSede.iterator();
    Sede sede = null;
    while (iterator.hasNext()) {
      sede = (Sede)iterator.next();
      col.add(new SelectItem(
        String.valueOf(sede.getIdSede()), 
        sede.toString()));
    }
    return col;
  }

  public Collection getColRegion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        new DefinicionesFacade().findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colDependencia = 
        this.estructuraFacade.findDependenciaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colCargo = 
        this.cargoFacade.findAllCargo();
      this.colBanco = 
        this.definicionesFacade.findAllBanco();
      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colGrupoNomina = 
        this.definicionesFacade.findGrupoNominaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colNominaEspecial = 
        this.procesoNominaFacade.findAllNominaEspecial();
      this.colLugarPago = 
        this.estructuraFacade.findAllLugarPago();

      this.colUnidadEjecutora = 
        this.estructuraFacade.findAllUnidadEjecutora();
      this.colUnidadAdministradora = 
        this.estructuraFacade.findUnidadAdministradoraByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colSede = 
        this.estructuraFacade.findSedeByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedula(this.findTrabajadorCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidos(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findHistoricoNominaByTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectTrabajador();
      if (!this.adding)
      {
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectHistoricoNomina()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectDependencia = null;
    this.selectCargo = null;
    this.selectBanco = null;
    this.selectTipoPersonal = null;
    this.selectGrupoNomina = null;
    this.selectNominaEspecial = null;
    this.selectLugarPago = null;
    this.selectTrabajador = null;
    this.selectUnidadEjecutora = null;
    this.selectUnidadAdministradora = null;
    this.selectSede = null;
    this.selectRegion = null;

    long idHistoricoNomina = 
      Long.parseLong((String)requestParameterMap.get("idHistoricoNomina"));
    try
    {
      this.historicoNomina = 
        this.historicoFacade.findHistoricoNominaById(
        idHistoricoNomina);

      if (this.historicoNomina.getDependencia() != null) {
        this.selectDependencia = 
          String.valueOf(this.historicoNomina.getDependencia().getIdDependencia());
      }
      if (this.historicoNomina.getCargo() != null) {
        this.selectCargo = 
          String.valueOf(this.historicoNomina.getCargo().getIdCargo());
      }
      if (this.historicoNomina.getBanco() != null) {
        this.selectBanco = 
          String.valueOf(this.historicoNomina.getBanco().getIdBanco());
      }
      if (this.historicoNomina.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.historicoNomina.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.historicoNomina.getGrupoNomina() != null) {
        this.selectGrupoNomina = 
          String.valueOf(this.historicoNomina.getGrupoNomina().getIdGrupoNomina());
      }
      if (this.historicoNomina.getNominaEspecial() != null) {
        this.selectNominaEspecial = 
          String.valueOf(this.historicoNomina.getNominaEspecial().getIdNominaEspecial());
      }
      if (this.historicoNomina.getLugarPago() != null) {
        this.selectLugarPago = 
          String.valueOf(this.historicoNomina.getLugarPago().getIdLugarPago());
      }
      if (this.historicoNomina.getTrabajador() != null) {
        this.selectTrabajador = 
          String.valueOf(this.historicoNomina.getTrabajador().getIdTrabajador());
      }
      if (this.historicoNomina.getUnidadEjecutora() != null) {
        this.selectUnidadEjecutora = 
          String.valueOf(this.historicoNomina.getUnidadEjecutora().getIdUnidadEjecutora());
      }
      if (this.historicoNomina.getUnidadAdministradora() != null) {
        this.selectUnidadAdministradora = 
          String.valueOf(this.historicoNomina.getUnidadAdministradora().getIdUnidadAdministradora());
      }
      if (this.historicoNomina.getSede() != null) {
        this.selectSede = 
          String.valueOf(this.historicoNomina.getSede().getIdSede());
      }
      if (this.historicoNomina.getRegion() != null) {
        this.selectRegion = 
          String.valueOf(this.historicoNomina.getRegion().getIdRegion());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.historicoNomina.getFecha() != null) && 
      (this.historicoNomina.getFecha().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.historicoNomina.setTrabajador(
          this.trabajador);
        this.historicoFacade.addHistoricoNomina(
          this.historicoNomina);
        context.addMessage("success_add", new FacesMessage("Se agregï¿½ con ï¿½xito"));
      } else {
        this.historicoFacade.updateHistoricoNomina(
          this.historicoNomina);
        context.addMessage("success_modify", new FacesMessage("Se modificï¿½ con ï¿½xito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e)
    {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.historicoFacade.deleteHistoricoNomina(
        this.historicoNomina);
      context.addMessage("success_delete", new FacesMessage("Se eliminï¿½ con ï¿½xito"));
      this.result = null;

      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedTrabajador = true;

    this.selectDependencia = null;

    this.selectCargo = null;

    this.selectBanco = null;

    this.selectTipoPersonal = null;

    this.selectGrupoNomina = null;

    this.selectNominaEspecial = null;

    this.selectLugarPago = null;

    this.selectTrabajador = null;

    this.selectUnidadEjecutora = null;

    this.selectUnidadAdministradora = null;

    this.selectSede = null;

    this.selectRegion = null;

    this.historicoNomina = new HistoricoNomina();

    this.historicoNomina.setTrabajador(this.trabajador);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.historicoNomina.setIdHistoricoNomina(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.historico.HistoricoNomina"));

    return null;
  }

  public boolean isShowBancoAux() {
    try {
      return this.historicoNomina.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowTipoCtaNominaAux() {
    try {
      return this.historicoNomina.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowCuentaNominaAux() {
    try {
      return this.historicoNomina.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.historicoNomina = new HistoricoNomina();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.historicoNomina = new HistoricoNomina();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedTrabajador));
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedTrabajador);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public LoginSession getLogin() {
    return this.login;
  }
}