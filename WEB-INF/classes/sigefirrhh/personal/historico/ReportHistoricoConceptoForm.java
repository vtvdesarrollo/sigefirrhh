package sigefirrhh.personal.historico;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportHistoricoConceptoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportHistoricoConceptoForm.class.getName());
  private int reportId;
  private long idRegion;
  private String idTipoPersonal;
  private String reportName;
  private String orden;
  private String agrupacion = "N";
  private int mes;
  private int anio;
  private int quincenaSemana = 0;
  private String formato = "1";
  private int preguntaQuincenaSemana = 0;
  private Collection listRegion;
  private Collection listTipoPersonal;
  private EstructuraFacade estructuraFacade;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private Collection colConceptoTipoPersonal;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private boolean showConcepto;
  private String idConceptoTipoPersonal;

  public boolean isShowQuincenaSemana()
  {
    return (this.mes != 0) && (this.preguntaQuincenaSemana != 0);
  }
  public boolean isShowConcepto() {
    return this.showConcepto;
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    this.showConcepto = false;
    try {
      if (idTipoPersonal > 0L) {
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
        this.colConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(idTipoPersonal);
        this.showConcepto = true;
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = null;
    }
  }

  public void changeConceptoTipoPersonal(ValueChangeEvent event) { long idConceptoTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.conceptoTipoPersonal = null;
      if (idConceptoTipoPersonal > 0L)
        this.conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(idConceptoTipoPersonal);
      else
        this.conceptoTipoPersonal = null;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.colConceptoTipoPersonal = null;
    } }

  public Collection getColConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }

    return col;
  }
  public ReportHistoricoConceptoForm() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "histsemalf";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportHistoricoConceptoForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      Date fechaActual = new Date();
      this.anio = (fechaActual.getYear() + 1900);
      this.listRegion = this.estructuraFacade.findAllRegion();

      this.listTipoPersonal = this.definicionesFacade.findAllTipoPersonal();
    } catch (Exception e) {
      this.listRegion = new ArrayList();
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      if (this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"))
        this.reportName = "histsem";
      else {
        this.reportName = "histqui";
      }

      if (this.agrupacion.equals("S")) {
        this.reportName += "dep";
      }

      if (this.mes != 0) {
        this.reportName += "mes";
      }

      if (this.idRegion != 0L) {
        this.reportName += "reg";
      }

      if (this.orden.equals("A"))
        this.reportName += "alf";
      else if (this.orden.equals("C"))
        this.reportName += "ced";
      else {
        this.reportName += "cod";
      }
      if (this.preguntaQuincenaSemana != 0) {
        this.reportName += "1";
      }
      if (this.formato.equals("2"))
        this.reportName = ("a_" + this.reportName);
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    try
    {
      if (this.tipoPersonal == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar un Tipo de Personal", ""));
        return null;
      }

      if (this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"))
        this.reportName = "histsem";
      else {
        this.reportName = "histqui";
      }

      if (this.agrupacion.equals("S")) {
        this.reportName += "dep";
      }

      if (this.mes != 0) {
        this.reportName += "mes";
      }

      if (this.idRegion != 0L) {
        this.reportName += "reg";
      }
      if (this.orden.equals("A"))
        this.reportName += "alf";
      else if (this.orden.equals("C"))
        this.reportName += "ced";
      else {
        this.reportName += "cod";
      }
      if (this.preguntaQuincenaSemana != 0) {
        this.reportName += "1";
      }
      if (this.formato.equals("2")) {
        this.reportName = ("a_" + this.reportName);
      }

      if (this.mes > 12) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El mes debe estar comprendido entre 1 y 12", ""));
        return null;
      }

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));
      parameters.put("id_concepto_tipo_personal", new Long(this.conceptoTipoPersonal.getIdConceptoTipoPersonal()));

      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      if (this.idRegion != 0L) {
        parameters.put("id_region", new Long(this.idRegion));
      }
      if (this.quincenaSemana != 0) {
        parameters.put("quincena", new Integer(this.quincenaSemana));
      }

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/historico");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String orden) {
    this.orden = orden;
  }

  public String getAgrupacion() {
    return this.agrupacion;
  }
  public void setAgrupacion(String agrupacion) {
    this.agrupacion = agrupacion;
  }

  public long getIdRegion() {
    return this.idRegion;
  }

  public void setIdRegion(long l) {
    this.idRegion = l;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }

  public String getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(String idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
  public String getIdConceptoTipoPersonal() {
    return this.idConceptoTipoPersonal;
  }
  public void setIdConceptoTipoPersonal(String idConceptoTipoPersonal) {
    this.idConceptoTipoPersonal = idConceptoTipoPersonal;
  }
  public int getQuincenaSemana() {
    return this.quincenaSemana;
  }
  public void setQuincenaSemana(int quincenaSemana) {
    this.quincenaSemana = quincenaSemana;
  }
  public int getPreguntaQuincenaSemana() {
    return this.preguntaQuincenaSemana;
  }
  public void setPreguntaQuincenaSemana(int preguntaQuincenaSemana) {
    this.preguntaQuincenaSemana = preguntaQuincenaSemana;
  }
}