package sigefirrhh.personal.historico;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoBeanBusiness;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.procesoNomina.NominaEspecialBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class HistoricoQuincenaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addHistoricoQuincena(HistoricoQuincena historicoQuincena)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    HistoricoQuincena historicoQuincenaNew = 
      (HistoricoQuincena)BeanUtils.cloneBean(
      historicoQuincena);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (historicoQuincenaNew.getTipoPersonal() != null) {
      historicoQuincenaNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        historicoQuincenaNew.getTipoPersonal().getIdTipoPersonal()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (historicoQuincenaNew.getGrupoNomina() != null) {
      historicoQuincenaNew.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        historicoQuincenaNew.getGrupoNomina().getIdGrupoNomina()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (historicoQuincenaNew.getConceptoTipoPersonal() != null) {
      historicoQuincenaNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        historicoQuincenaNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (historicoQuincenaNew.getFrecuenciaTipoPersonal() != null) {
      historicoQuincenaNew.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        historicoQuincenaNew.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    ConceptoBeanBusiness conceptoAporteBeanBusiness = new ConceptoBeanBusiness();

    if (historicoQuincenaNew.getConceptoAporte() != null) {
      historicoQuincenaNew.setConceptoAporte(
        conceptoAporteBeanBusiness.findConceptoById(
        historicoQuincenaNew.getConceptoAporte().getIdConcepto()));
    }

    NominaEspecialBeanBusiness nominaEspecialBeanBusiness = new NominaEspecialBeanBusiness();

    if (historicoQuincenaNew.getNominaEspecial() != null) {
      historicoQuincenaNew.setNominaEspecial(
        nominaEspecialBeanBusiness.findNominaEspecialById(
        historicoQuincenaNew.getNominaEspecial().getIdNominaEspecial()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (historicoQuincenaNew.getTrabajador() != null) {
      historicoQuincenaNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        historicoQuincenaNew.getTrabajador().getIdTrabajador()));
    }

    HistoricoNominaBeanBusiness historicoNominaBeanBusiness = new HistoricoNominaBeanBusiness();

    if (historicoQuincenaNew.getHistoricoNomina() != null) {
      historicoQuincenaNew.setHistoricoNomina(
        historicoNominaBeanBusiness.findHistoricoNominaById(
        historicoQuincenaNew.getHistoricoNomina().getIdHistoricoNomina()));
    }
    pm.makePersistent(historicoQuincenaNew);
  }

  public void updateHistoricoQuincena(HistoricoQuincena historicoQuincena) throws Exception
  {
    HistoricoQuincena historicoQuincenaModify = 
      findHistoricoQuincenaById(historicoQuincena.getIdHistoricoQuincena());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (historicoQuincena.getTipoPersonal() != null) {
      historicoQuincena.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        historicoQuincena.getTipoPersonal().getIdTipoPersonal()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (historicoQuincena.getGrupoNomina() != null) {
      historicoQuincena.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        historicoQuincena.getGrupoNomina().getIdGrupoNomina()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (historicoQuincena.getConceptoTipoPersonal() != null) {
      historicoQuincena.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        historicoQuincena.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (historicoQuincena.getFrecuenciaTipoPersonal() != null) {
      historicoQuincena.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        historicoQuincena.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    ConceptoBeanBusiness conceptoAporteBeanBusiness = new ConceptoBeanBusiness();

    if (historicoQuincena.getConceptoAporte() != null) {
      historicoQuincena.setConceptoAporte(
        conceptoAporteBeanBusiness.findConceptoById(
        historicoQuincena.getConceptoAporte().getIdConcepto()));
    }

    NominaEspecialBeanBusiness nominaEspecialBeanBusiness = new NominaEspecialBeanBusiness();

    if (historicoQuincena.getNominaEspecial() != null) {
      historicoQuincena.setNominaEspecial(
        nominaEspecialBeanBusiness.findNominaEspecialById(
        historicoQuincena.getNominaEspecial().getIdNominaEspecial()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (historicoQuincena.getTrabajador() != null) {
      historicoQuincena.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        historicoQuincena.getTrabajador().getIdTrabajador()));
    }

    HistoricoNominaBeanBusiness historicoNominaBeanBusiness = new HistoricoNominaBeanBusiness();

    if (historicoQuincena.getHistoricoNomina() != null) {
      historicoQuincena.setHistoricoNomina(
        historicoNominaBeanBusiness.findHistoricoNominaById(
        historicoQuincena.getHistoricoNomina().getIdHistoricoNomina()));
    }

    BeanUtils.copyProperties(historicoQuincenaModify, historicoQuincena);
  }

  public void deleteHistoricoQuincena(HistoricoQuincena historicoQuincena) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    HistoricoQuincena historicoQuincenaDelete = 
      findHistoricoQuincenaById(historicoQuincena.getIdHistoricoQuincena());
    pm.deletePersistent(historicoQuincenaDelete);
  }

  public HistoricoQuincena findHistoricoQuincenaById(long idHistoricoQuincena) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idHistoricoQuincena == pIdHistoricoQuincena";
    Query query = pm.newQuery(HistoricoQuincena.class, filter);

    query.declareParameters("long pIdHistoricoQuincena");

    parameters.put("pIdHistoricoQuincena", new Long(idHistoricoQuincena));

    Collection colHistoricoQuincena = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colHistoricoQuincena.iterator();
    return (HistoricoQuincena)iterator.next();
  }

  public Collection findHistoricoQuincenaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent historicoQuincenaExtent = pm.getExtent(
      HistoricoQuincena.class, true);
    Query query = pm.newQuery(historicoQuincenaExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}