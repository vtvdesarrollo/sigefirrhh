package sigefirrhh.personal.historico;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.trabajador.Trabajador;

public class HistoricoQuincena
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ORIGEN;
  private long idHistoricoQuincena;
  private int numeroNomina;
  private int anio;
  private int mes;
  private int semanaQuincena;
  private Date fecha;
  private double unidades;
  private double montoAsigna;
  private double montoDeduce;
  private String origen;
  private String documentoSoporte;
  private TipoPersonal tipoPersonal;
  private GrupoNomina grupoNomina;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private int idConcepto;
  private double montoAporte;
  private Concepto conceptoAporte;
  private NominaEspecial nominaEspecial;
  private Trabajador trabajador;
  private HistoricoNomina historicoNomina;
  private int mesSobretiempo;
  private int anioSobretiempo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "anioSobretiempo", "conceptoAporte", "conceptoTipoPersonal", "documentoSoporte", "fecha", "frecuenciaTipoPersonal", "grupoNomina", "historicoNomina", "idConcepto", "idHistoricoQuincena", "mes", "mesSobretiempo", "montoAporte", "montoAsigna", "montoDeduce", "nominaEspecial", "numeroNomina", "origen", "semanaQuincena", "tipoPersonal", "trabajador", "unidades" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), sunjdo$classForName$("sigefirrhh.personal.historico.HistoricoNomina"), Integer.TYPE, Long.TYPE, Integer.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.procesoNomina.NominaEspecial"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 26, 26, 21, 21, 26, 26, 26, 21, 24, 21, 21, 21, 21, 21, 26, 21, 21, 21, 26, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.historico.HistoricoQuincena"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HistoricoQuincena());

    LISTA_ORIGEN = 
      new LinkedHashMap();
    LISTA_ORIGEN.put("F", "FIJO");
    LISTA_ORIGEN.put("V", "VARIABLE");
    LISTA_ORIGEN.put("P", "PRESTAMO");
    LISTA_ORIGEN.put("C", "CALCULADO");
  }

  public HistoricoQuincena()
  {
    jdoSetnumeroNomina(this, 0);

    jdoSetanio(this, 0);

    jdoSetmes(this, 0);

    jdoSetsemanaQuincena(this, 0);

    jdoSetunidades(this, 0.0D);

    jdoSetmontoAsigna(this, 0.0D);

    jdoSetmontoDeduce(this, 0.0D);

    jdoSetmontoAporte(this, 0.0D);

    jdoSetmesSobretiempo(this, 0);

    jdoSetanioSobretiempo(this, 0);
  }

  public String toString() {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,##0.00");
    String c;
    String c;
    if (jdoGetmontoAsigna(this) == 0.0D)
      c = b.format(jdoGetmontoDeduce(this));
    else {
      c = b.format(jdoGetmontoAsigna(this));
    }

    String a = b.format(jdoGetunidades(this));

    return jdoGetmes(this) + " - " + 
      "Q-" + jdoGetsemanaQuincena(this) + " - " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getCodConcepto() + " " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + 
      "UNID-" + a + " - " + 
      c + " - " + 
      "FREC-" + jdoGetfrecuenciaTipoPersonal(this).getFrecuenciaPago().getCodFrecuenciaPago();
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public int getIdConcepto()
  {
    return jdoGetidConcepto(this);
  }
  public void setIdConcepto(int idConcepto) {
    jdoSetidConcepto(this, idConcepto);
  }
  public Concepto getConceptoAporte() {
    return jdoGetconceptoAporte(this);
  }
  public void setConceptoAporte(Concepto conceptoAporte) {
    jdoSetconceptoAporte(this, conceptoAporte);
  }
  public double getMontoAporte() {
    return jdoGetmontoAporte(this);
  }
  public void setMontoAporte(double montoAporte) {
    jdoSetmontoAporte(this, montoAporte);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal()
  {
    return jdoGetfrecuenciaTipoPersonal(this);
  }

  public GrupoNomina getGrupoNomina()
  {
    return jdoGetgrupoNomina(this);
  }

  public HistoricoNomina getHistoricoNomina()
  {
    return jdoGethistoricoNomina(this);
  }

  public long getIdHistoricoQuincena()
  {
    return jdoGetidHistoricoQuincena(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public double getMontoAsigna()
  {
    return jdoGetmontoAsigna(this);
  }

  public double getMontoDeduce()
  {
    return jdoGetmontoDeduce(this);
  }

  public NominaEspecial getNominaEspecial()
  {
    return jdoGetnominaEspecial(this);
  }

  public int getNumeroNomina()
  {
    return jdoGetnumeroNomina(this);
  }

  public String getOrigen()
  {
    return jdoGetorigen(this);
  }

  public int getSemanaQuincena()
  {
    return jdoGetsemanaQuincena(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public double getUnidades()
  {
    return jdoGetunidades(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setFrecuenciaTipoPersonal(FrecuenciaTipoPersonal personal)
  {
    jdoSetfrecuenciaTipoPersonal(this, personal);
  }

  public void setGrupoNomina(GrupoNomina nomina)
  {
    jdoSetgrupoNomina(this, nomina);
  }

  public void setHistoricoNomina(HistoricoNomina nomina)
  {
    jdoSethistoricoNomina(this, nomina);
  }

  public void setIdHistoricoQuincena(long l)
  {
    jdoSetidHistoricoQuincena(this, l);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setMontoAsigna(double d)
  {
    jdoSetmontoAsigna(this, d);
  }

  public void setMontoDeduce(double d)
  {
    jdoSetmontoDeduce(this, d);
  }

  public void setNominaEspecial(NominaEspecial especial)
  {
    jdoSetnominaEspecial(this, especial);
  }

  public void setNumeroNomina(int i)
  {
    jdoSetnumeroNomina(this, i);
  }

  public void setOrigen(String string)
  {
    jdoSetorigen(this, string);
  }

  public void setSemanaQuincena(int i)
  {
    jdoSetsemanaQuincena(this, i);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setUnidades(double d)
  {
    jdoSetunidades(this, d);
  }

  public Date getFecha()
  {
    return jdoGetfecha(this);
  }

  public void setFecha(Date date)
  {
    jdoSetfecha(this, date);
  }

  public int getAnioSobretiempo() {
    return jdoGetanioSobretiempo(this);
  }
  public void setAnioSobretiempo(int anioSobretiempo) {
    jdoSetanioSobretiempo(this, anioSobretiempo);
  }
  public int getMesSobretiempo() {
    return jdoGetmesSobretiempo(this);
  }
  public void setMesSobretiempo(int mesSobretiempo) {
    jdoSetmesSobretiempo(this, mesSobretiempo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 23;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HistoricoQuincena localHistoricoQuincena = new HistoricoQuincena();
    localHistoricoQuincena.jdoFlags = 1;
    localHistoricoQuincena.jdoStateManager = paramStateManager;
    return localHistoricoQuincena;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HistoricoQuincena localHistoricoQuincena = new HistoricoQuincena();
    localHistoricoQuincena.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHistoricoQuincena.jdoFlags = 1;
    localHistoricoQuincena.jdoStateManager = paramStateManager;
    return localHistoricoQuincena;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioSobretiempo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoAporte);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaTipoPersonal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.historicoNomina);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idConcepto);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHistoricoQuincena);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesSobretiempo);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAporte);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAsigna);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoDeduce);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nominaEspecial);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroNomina);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origen);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanaQuincena);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioSobretiempo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoAporte = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaTipoPersonal = ((FrecuenciaTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.historicoNomina = ((HistoricoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConcepto = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHistoricoQuincena = localStateManager.replacingLongField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesSobretiempo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAporte = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAsigna = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoDeduce = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nominaEspecial = ((NominaEspecial)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origen = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanaQuincena = localStateManager.replacingIntField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HistoricoQuincena paramHistoricoQuincena, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramHistoricoQuincena.anio;
      return;
    case 1:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.anioSobretiempo = paramHistoricoQuincena.anioSobretiempo;
      return;
    case 2:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoAporte = paramHistoricoQuincena.conceptoAporte;
      return;
    case 3:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramHistoricoQuincena.conceptoTipoPersonal;
      return;
    case 4:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramHistoricoQuincena.documentoSoporte;
      return;
    case 5:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramHistoricoQuincena.fecha;
      return;
    case 6:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaTipoPersonal = paramHistoricoQuincena.frecuenciaTipoPersonal;
      return;
    case 7:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramHistoricoQuincena.grupoNomina;
      return;
    case 8:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.historicoNomina = paramHistoricoQuincena.historicoNomina;
      return;
    case 9:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.idConcepto = paramHistoricoQuincena.idConcepto;
      return;
    case 10:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.idHistoricoQuincena = paramHistoricoQuincena.idHistoricoQuincena;
      return;
    case 11:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramHistoricoQuincena.mes;
      return;
    case 12:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.mesSobretiempo = paramHistoricoQuincena.mesSobretiempo;
      return;
    case 13:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.montoAporte = paramHistoricoQuincena.montoAporte;
      return;
    case 14:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.montoAsigna = paramHistoricoQuincena.montoAsigna;
      return;
    case 15:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.montoDeduce = paramHistoricoQuincena.montoDeduce;
      return;
    case 16:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.nominaEspecial = paramHistoricoQuincena.nominaEspecial;
      return;
    case 17:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.numeroNomina = paramHistoricoQuincena.numeroNomina;
      return;
    case 18:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.origen = paramHistoricoQuincena.origen;
      return;
    case 19:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.semanaQuincena = paramHistoricoQuincena.semanaQuincena;
      return;
    case 20:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramHistoricoQuincena.tipoPersonal;
      return;
    case 21:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramHistoricoQuincena.trabajador;
      return;
    case 22:
      if (paramHistoricoQuincena == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramHistoricoQuincena.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HistoricoQuincena))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HistoricoQuincena localHistoricoQuincena = (HistoricoQuincena)paramObject;
    if (localHistoricoQuincena.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHistoricoQuincena, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HistoricoQuincenaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HistoricoQuincenaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoQuincenaPK))
      throw new IllegalArgumentException("arg1");
    HistoricoQuincenaPK localHistoricoQuincenaPK = (HistoricoQuincenaPK)paramObject;
    localHistoricoQuincenaPK.idHistoricoQuincena = this.idHistoricoQuincena;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoQuincenaPK))
      throw new IllegalArgumentException("arg1");
    HistoricoQuincenaPK localHistoricoQuincenaPK = (HistoricoQuincenaPK)paramObject;
    this.idHistoricoQuincena = localHistoricoQuincenaPK.idHistoricoQuincena;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoQuincenaPK))
      throw new IllegalArgumentException("arg2");
    HistoricoQuincenaPK localHistoricoQuincenaPK = (HistoricoQuincenaPK)paramObject;
    localHistoricoQuincenaPK.idHistoricoQuincena = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 10);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoQuincenaPK))
      throw new IllegalArgumentException("arg2");
    HistoricoQuincenaPK localHistoricoQuincenaPK = (HistoricoQuincenaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 10, localHistoricoQuincenaPK.idHistoricoQuincena);
  }

  private static final int jdoGetanio(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.anio;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.anio;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 0))
      return paramHistoricoQuincena.anio;
    return localStateManager.getIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 0, paramHistoricoQuincena.anio);
  }

  private static final void jdoSetanio(HistoricoQuincena paramHistoricoQuincena, int paramInt)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 0, paramHistoricoQuincena.anio, paramInt);
  }

  private static final int jdoGetanioSobretiempo(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.anioSobretiempo;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.anioSobretiempo;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 1))
      return paramHistoricoQuincena.anioSobretiempo;
    return localStateManager.getIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 1, paramHistoricoQuincena.anioSobretiempo);
  }

  private static final void jdoSetanioSobretiempo(HistoricoQuincena paramHistoricoQuincena, int paramInt)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.anioSobretiempo = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.anioSobretiempo = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 1, paramHistoricoQuincena.anioSobretiempo, paramInt);
  }

  private static final Concepto jdoGetconceptoAporte(HistoricoQuincena paramHistoricoQuincena)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.conceptoAporte;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 2))
      return paramHistoricoQuincena.conceptoAporte;
    return (Concepto)localStateManager.getObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 2, paramHistoricoQuincena.conceptoAporte);
  }

  private static final void jdoSetconceptoAporte(HistoricoQuincena paramHistoricoQuincena, Concepto paramConcepto)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.conceptoAporte = paramConcepto;
      return;
    }
    localStateManager.setObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 2, paramHistoricoQuincena.conceptoAporte, paramConcepto);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(HistoricoQuincena paramHistoricoQuincena)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 3))
      return paramHistoricoQuincena.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 3, paramHistoricoQuincena.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(HistoricoQuincena paramHistoricoQuincena, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 3, paramHistoricoQuincena.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGetdocumentoSoporte(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.documentoSoporte;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.documentoSoporte;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 4))
      return paramHistoricoQuincena.documentoSoporte;
    return localStateManager.getStringField(paramHistoricoQuincena, jdoInheritedFieldCount + 4, paramHistoricoQuincena.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(HistoricoQuincena paramHistoricoQuincena, String paramString)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoQuincena, jdoInheritedFieldCount + 4, paramHistoricoQuincena.documentoSoporte, paramString);
  }

  private static final Date jdoGetfecha(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.fecha;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.fecha;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 5))
      return paramHistoricoQuincena.fecha;
    return (Date)localStateManager.getObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 5, paramHistoricoQuincena.fecha);
  }

  private static final void jdoSetfecha(HistoricoQuincena paramHistoricoQuincena, Date paramDate)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 5, paramHistoricoQuincena.fecha, paramDate);
  }

  private static final FrecuenciaTipoPersonal jdoGetfrecuenciaTipoPersonal(HistoricoQuincena paramHistoricoQuincena)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.frecuenciaTipoPersonal;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 6))
      return paramHistoricoQuincena.frecuenciaTipoPersonal;
    return (FrecuenciaTipoPersonal)localStateManager.getObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 6, paramHistoricoQuincena.frecuenciaTipoPersonal);
  }

  private static final void jdoSetfrecuenciaTipoPersonal(HistoricoQuincena paramHistoricoQuincena, FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.frecuenciaTipoPersonal = paramFrecuenciaTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 6, paramHistoricoQuincena.frecuenciaTipoPersonal, paramFrecuenciaTipoPersonal);
  }

  private static final GrupoNomina jdoGetgrupoNomina(HistoricoQuincena paramHistoricoQuincena)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.grupoNomina;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 7))
      return paramHistoricoQuincena.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 7, paramHistoricoQuincena.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(HistoricoQuincena paramHistoricoQuincena, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 7, paramHistoricoQuincena.grupoNomina, paramGrupoNomina);
  }

  private static final HistoricoNomina jdoGethistoricoNomina(HistoricoQuincena paramHistoricoQuincena)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.historicoNomina;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 8))
      return paramHistoricoQuincena.historicoNomina;
    return (HistoricoNomina)localStateManager.getObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 8, paramHistoricoQuincena.historicoNomina);
  }

  private static final void jdoSethistoricoNomina(HistoricoQuincena paramHistoricoQuincena, HistoricoNomina paramHistoricoNomina)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.historicoNomina = paramHistoricoNomina;
      return;
    }
    localStateManager.setObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 8, paramHistoricoQuincena.historicoNomina, paramHistoricoNomina);
  }

  private static final int jdoGetidConcepto(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.idConcepto;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.idConcepto;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 9))
      return paramHistoricoQuincena.idConcepto;
    return localStateManager.getIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 9, paramHistoricoQuincena.idConcepto);
  }

  private static final void jdoSetidConcepto(HistoricoQuincena paramHistoricoQuincena, int paramInt)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.idConcepto = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.idConcepto = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 9, paramHistoricoQuincena.idConcepto, paramInt);
  }

  private static final long jdoGetidHistoricoQuincena(HistoricoQuincena paramHistoricoQuincena)
  {
    return paramHistoricoQuincena.idHistoricoQuincena;
  }

  private static final void jdoSetidHistoricoQuincena(HistoricoQuincena paramHistoricoQuincena, long paramLong)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.idHistoricoQuincena = paramLong;
      return;
    }
    localStateManager.setLongField(paramHistoricoQuincena, jdoInheritedFieldCount + 10, paramHistoricoQuincena.idHistoricoQuincena, paramLong);
  }

  private static final int jdoGetmes(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.mes;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.mes;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 11))
      return paramHistoricoQuincena.mes;
    return localStateManager.getIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 11, paramHistoricoQuincena.mes);
  }

  private static final void jdoSetmes(HistoricoQuincena paramHistoricoQuincena, int paramInt)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 11, paramHistoricoQuincena.mes, paramInt);
  }

  private static final int jdoGetmesSobretiempo(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.mesSobretiempo;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.mesSobretiempo;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 12))
      return paramHistoricoQuincena.mesSobretiempo;
    return localStateManager.getIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 12, paramHistoricoQuincena.mesSobretiempo);
  }

  private static final void jdoSetmesSobretiempo(HistoricoQuincena paramHistoricoQuincena, int paramInt)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.mesSobretiempo = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.mesSobretiempo = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 12, paramHistoricoQuincena.mesSobretiempo, paramInt);
  }

  private static final double jdoGetmontoAporte(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.montoAporte;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.montoAporte;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 13))
      return paramHistoricoQuincena.montoAporte;
    return localStateManager.getDoubleField(paramHistoricoQuincena, jdoInheritedFieldCount + 13, paramHistoricoQuincena.montoAporte);
  }

  private static final void jdoSetmontoAporte(HistoricoQuincena paramHistoricoQuincena, double paramDouble)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.montoAporte = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.montoAporte = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoQuincena, jdoInheritedFieldCount + 13, paramHistoricoQuincena.montoAporte, paramDouble);
  }

  private static final double jdoGetmontoAsigna(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.montoAsigna;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.montoAsigna;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 14))
      return paramHistoricoQuincena.montoAsigna;
    return localStateManager.getDoubleField(paramHistoricoQuincena, jdoInheritedFieldCount + 14, paramHistoricoQuincena.montoAsigna);
  }

  private static final void jdoSetmontoAsigna(HistoricoQuincena paramHistoricoQuincena, double paramDouble)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.montoAsigna = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.montoAsigna = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoQuincena, jdoInheritedFieldCount + 14, paramHistoricoQuincena.montoAsigna, paramDouble);
  }

  private static final double jdoGetmontoDeduce(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.montoDeduce;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.montoDeduce;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 15))
      return paramHistoricoQuincena.montoDeduce;
    return localStateManager.getDoubleField(paramHistoricoQuincena, jdoInheritedFieldCount + 15, paramHistoricoQuincena.montoDeduce);
  }

  private static final void jdoSetmontoDeduce(HistoricoQuincena paramHistoricoQuincena, double paramDouble)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.montoDeduce = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.montoDeduce = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoQuincena, jdoInheritedFieldCount + 15, paramHistoricoQuincena.montoDeduce, paramDouble);
  }

  private static final NominaEspecial jdoGetnominaEspecial(HistoricoQuincena paramHistoricoQuincena)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.nominaEspecial;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 16))
      return paramHistoricoQuincena.nominaEspecial;
    return (NominaEspecial)localStateManager.getObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 16, paramHistoricoQuincena.nominaEspecial);
  }

  private static final void jdoSetnominaEspecial(HistoricoQuincena paramHistoricoQuincena, NominaEspecial paramNominaEspecial)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.nominaEspecial = paramNominaEspecial;
      return;
    }
    localStateManager.setObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 16, paramHistoricoQuincena.nominaEspecial, paramNominaEspecial);
  }

  private static final int jdoGetnumeroNomina(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.numeroNomina;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.numeroNomina;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 17))
      return paramHistoricoQuincena.numeroNomina;
    return localStateManager.getIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 17, paramHistoricoQuincena.numeroNomina);
  }

  private static final void jdoSetnumeroNomina(HistoricoQuincena paramHistoricoQuincena, int paramInt)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.numeroNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.numeroNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 17, paramHistoricoQuincena.numeroNomina, paramInt);
  }

  private static final String jdoGetorigen(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.origen;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.origen;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 18))
      return paramHistoricoQuincena.origen;
    return localStateManager.getStringField(paramHistoricoQuincena, jdoInheritedFieldCount + 18, paramHistoricoQuincena.origen);
  }

  private static final void jdoSetorigen(HistoricoQuincena paramHistoricoQuincena, String paramString)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.origen = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.origen = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoQuincena, jdoInheritedFieldCount + 18, paramHistoricoQuincena.origen, paramString);
  }

  private static final int jdoGetsemanaQuincena(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.semanaQuincena;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.semanaQuincena;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 19))
      return paramHistoricoQuincena.semanaQuincena;
    return localStateManager.getIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 19, paramHistoricoQuincena.semanaQuincena);
  }

  private static final void jdoSetsemanaQuincena(HistoricoQuincena paramHistoricoQuincena, int paramInt)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.semanaQuincena = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.semanaQuincena = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoQuincena, jdoInheritedFieldCount + 19, paramHistoricoQuincena.semanaQuincena, paramInt);
  }

  private static final TipoPersonal jdoGettipoPersonal(HistoricoQuincena paramHistoricoQuincena)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.tipoPersonal;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 20))
      return paramHistoricoQuincena.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 20, paramHistoricoQuincena.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(HistoricoQuincena paramHistoricoQuincena, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 20, paramHistoricoQuincena.tipoPersonal, paramTipoPersonal);
  }

  private static final Trabajador jdoGettrabajador(HistoricoQuincena paramHistoricoQuincena)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.trabajador;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 21))
      return paramHistoricoQuincena.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 21, paramHistoricoQuincena.trabajador);
  }

  private static final void jdoSettrabajador(HistoricoQuincena paramHistoricoQuincena, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramHistoricoQuincena, jdoInheritedFieldCount + 21, paramHistoricoQuincena.trabajador, paramTrabajador);
  }

  private static final double jdoGetunidades(HistoricoQuincena paramHistoricoQuincena)
  {
    if (paramHistoricoQuincena.jdoFlags <= 0)
      return paramHistoricoQuincena.unidades;
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoQuincena.unidades;
    if (localStateManager.isLoaded(paramHistoricoQuincena, jdoInheritedFieldCount + 22))
      return paramHistoricoQuincena.unidades;
    return localStateManager.getDoubleField(paramHistoricoQuincena, jdoInheritedFieldCount + 22, paramHistoricoQuincena.unidades);
  }

  private static final void jdoSetunidades(HistoricoQuincena paramHistoricoQuincena, double paramDouble)
  {
    if (paramHistoricoQuincena.jdoFlags == 0)
    {
      paramHistoricoQuincena.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoQuincena.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoQuincena.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoQuincena, jdoInheritedFieldCount + 22, paramHistoricoQuincena.unidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}