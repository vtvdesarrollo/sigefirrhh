package sigefirrhh.personal.historico;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class HistoricoBusiness extends AbstractBusiness
  implements Serializable
{
  private HistoricoNominaBeanBusiness historicoNominaBeanBusiness = new HistoricoNominaBeanBusiness();

  private HistoricoQuincenaBeanBusiness historicoQuincenaBeanBusiness = new HistoricoQuincenaBeanBusiness();

  private HistoricoSemanaBeanBusiness historicoSemanaBeanBusiness = new HistoricoSemanaBeanBusiness();

  public void addHistoricoNomina(HistoricoNomina historicoNomina)
    throws Exception
  {
    this.historicoNominaBeanBusiness.addHistoricoNomina(historicoNomina);
  }

  public void updateHistoricoNomina(HistoricoNomina historicoNomina) throws Exception {
    this.historicoNominaBeanBusiness.updateHistoricoNomina(historicoNomina);
  }

  public void deleteHistoricoNomina(HistoricoNomina historicoNomina) throws Exception {
    this.historicoNominaBeanBusiness.deleteHistoricoNomina(historicoNomina);
  }

  public HistoricoNomina findHistoricoNominaById(long historicoNominaId) throws Exception {
    return this.historicoNominaBeanBusiness.findHistoricoNominaById(historicoNominaId);
  }

  public Collection findAllHistoricoNomina() throws Exception {
    return this.historicoNominaBeanBusiness.findHistoricoNominaAll();
  }

  public void addHistoricoQuincena(HistoricoQuincena historicoQuincena)
    throws Exception
  {
    this.historicoQuincenaBeanBusiness.addHistoricoQuincena(historicoQuincena);
  }

  public void updateHistoricoQuincena(HistoricoQuincena historicoQuincena) throws Exception {
    this.historicoQuincenaBeanBusiness.updateHistoricoQuincena(historicoQuincena);
  }

  public void deleteHistoricoQuincena(HistoricoQuincena historicoQuincena) throws Exception {
    this.historicoQuincenaBeanBusiness.deleteHistoricoQuincena(historicoQuincena);
  }

  public HistoricoQuincena findHistoricoQuincenaById(long historicoQuincenaId) throws Exception {
    return this.historicoQuincenaBeanBusiness.findHistoricoQuincenaById(historicoQuincenaId);
  }

  public Collection findAllHistoricoQuincena() throws Exception {
    return this.historicoQuincenaBeanBusiness.findHistoricoQuincenaAll();
  }

  public void addHistoricoSemana(HistoricoSemana historicoSemana)
    throws Exception
  {
    this.historicoSemanaBeanBusiness.addHistoricoSemana(historicoSemana);
  }

  public void updateHistoricoSemana(HistoricoSemana historicoSemana) throws Exception {
    this.historicoSemanaBeanBusiness.updateHistoricoSemana(historicoSemana);
  }

  public void deleteHistoricoSemana(HistoricoSemana historicoSemana) throws Exception {
    this.historicoSemanaBeanBusiness.deleteHistoricoSemana(historicoSemana);
  }

  public HistoricoSemana findHistoricoSemanaById(long historicoSemanaId) throws Exception {
    return this.historicoSemanaBeanBusiness.findHistoricoSemanaById(historicoSemanaId);
  }

  public Collection findAllHistoricoSemana() throws Exception {
    return this.historicoSemanaBeanBusiness.findHistoricoSemanaAll();
  }
}