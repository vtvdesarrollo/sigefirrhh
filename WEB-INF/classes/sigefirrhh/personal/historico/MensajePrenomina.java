package sigefirrhh.personal.historico;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.procesoNomina.UltimaPrenomina;

public class MensajePrenomina
  implements Serializable, PersistenceCapable
{
  private long idUltimaPrenomina;
  private UltimaPrenomina ultimaPrenomina;
  private GrupoNomina grupoNomina;
  private NominaEspecial nominaEspecial;
  private String mensaje;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "grupoNomina", "idUltimaPrenomina", "mensaje", "nominaEspecial", "ultimaPrenomina" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.procesoNomina.NominaEspecial"), sunjdo$classForName$("sigefirrhh.personal.procesoNomina.UltimaPrenomina") };
  private static final byte[] jdoFieldFlags = { 26, 24, 21, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public UltimaPrenomina getUltimaPrenomina()
  {
    return jdoGetultimaPrenomina(this);
  }

  public void setUltimaPrenomina(UltimaPrenomina ultimaPrenomina) {
    jdoSetultimaPrenomina(this, ultimaPrenomina);
  }

  public GrupoNomina getGrupoNomina() {
    return jdoGetgrupoNomina(this);
  }

  public void setGrupoNomina(GrupoNomina grupoNomina) {
    jdoSetgrupoNomina(this, grupoNomina);
  }

  public NominaEspecial getNominaEspecial() {
    return jdoGetnominaEspecial(this);
  }

  public void setNominaEspecial(NominaEspecial nominaEspecial) {
    jdoSetnominaEspecial(this, nominaEspecial);
  }

  public String getMensaje() {
    return jdoGetmensaje(this);
  }

  public void setMensaje(String mensaje) {
    jdoSetmensaje(this, mensaje);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.historico.MensajePrenomina"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new MensajePrenomina());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    MensajePrenomina localMensajePrenomina = new MensajePrenomina();
    localMensajePrenomina.jdoFlags = 1;
    localMensajePrenomina.jdoStateManager = paramStateManager;
    return localMensajePrenomina;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    MensajePrenomina localMensajePrenomina = new MensajePrenomina();
    localMensajePrenomina.jdoCopyKeyFieldsFromObjectId(paramObject);
    localMensajePrenomina.jdoFlags = 1;
    localMensajePrenomina.jdoStateManager = paramStateManager;
    return localMensajePrenomina;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUltimaPrenomina);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mensaje);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nominaEspecial);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ultimaPrenomina);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUltimaPrenomina = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mensaje = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nominaEspecial = ((NominaEspecial)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ultimaPrenomina = ((UltimaPrenomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(MensajePrenomina paramMensajePrenomina, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramMensajePrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramMensajePrenomina.grupoNomina;
      return;
    case 1:
      if (paramMensajePrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.idUltimaPrenomina = paramMensajePrenomina.idUltimaPrenomina;
      return;
    case 2:
      if (paramMensajePrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.mensaje = paramMensajePrenomina.mensaje;
      return;
    case 3:
      if (paramMensajePrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.nominaEspecial = paramMensajePrenomina.nominaEspecial;
      return;
    case 4:
      if (paramMensajePrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.ultimaPrenomina = paramMensajePrenomina.ultimaPrenomina;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof MensajePrenomina))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    MensajePrenomina localMensajePrenomina = (MensajePrenomina)paramObject;
    if (localMensajePrenomina.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localMensajePrenomina, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new MensajePrenominaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new MensajePrenominaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MensajePrenominaPK))
      throw new IllegalArgumentException("arg1");
    MensajePrenominaPK localMensajePrenominaPK = (MensajePrenominaPK)paramObject;
    localMensajePrenominaPK.idUltimaPrenomina = this.idUltimaPrenomina;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MensajePrenominaPK))
      throw new IllegalArgumentException("arg1");
    MensajePrenominaPK localMensajePrenominaPK = (MensajePrenominaPK)paramObject;
    this.idUltimaPrenomina = localMensajePrenominaPK.idUltimaPrenomina;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MensajePrenominaPK))
      throw new IllegalArgumentException("arg2");
    MensajePrenominaPK localMensajePrenominaPK = (MensajePrenominaPK)paramObject;
    localMensajePrenominaPK.idUltimaPrenomina = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MensajePrenominaPK))
      throw new IllegalArgumentException("arg2");
    MensajePrenominaPK localMensajePrenominaPK = (MensajePrenominaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localMensajePrenominaPK.idUltimaPrenomina);
  }

  private static final GrupoNomina jdoGetgrupoNomina(MensajePrenomina paramMensajePrenomina)
  {
    StateManager localStateManager = paramMensajePrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramMensajePrenomina.grupoNomina;
    if (localStateManager.isLoaded(paramMensajePrenomina, jdoInheritedFieldCount + 0))
      return paramMensajePrenomina.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramMensajePrenomina, jdoInheritedFieldCount + 0, paramMensajePrenomina.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(MensajePrenomina paramMensajePrenomina, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramMensajePrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramMensajePrenomina.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramMensajePrenomina, jdoInheritedFieldCount + 0, paramMensajePrenomina.grupoNomina, paramGrupoNomina);
  }

  private static final long jdoGetidUltimaPrenomina(MensajePrenomina paramMensajePrenomina)
  {
    return paramMensajePrenomina.idUltimaPrenomina;
  }

  private static final void jdoSetidUltimaPrenomina(MensajePrenomina paramMensajePrenomina, long paramLong)
  {
    StateManager localStateManager = paramMensajePrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramMensajePrenomina.idUltimaPrenomina = paramLong;
      return;
    }
    localStateManager.setLongField(paramMensajePrenomina, jdoInheritedFieldCount + 1, paramMensajePrenomina.idUltimaPrenomina, paramLong);
  }

  private static final String jdoGetmensaje(MensajePrenomina paramMensajePrenomina)
  {
    if (paramMensajePrenomina.jdoFlags <= 0)
      return paramMensajePrenomina.mensaje;
    StateManager localStateManager = paramMensajePrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramMensajePrenomina.mensaje;
    if (localStateManager.isLoaded(paramMensajePrenomina, jdoInheritedFieldCount + 2))
      return paramMensajePrenomina.mensaje;
    return localStateManager.getStringField(paramMensajePrenomina, jdoInheritedFieldCount + 2, paramMensajePrenomina.mensaje);
  }

  private static final void jdoSetmensaje(MensajePrenomina paramMensajePrenomina, String paramString)
  {
    if (paramMensajePrenomina.jdoFlags == 0)
    {
      paramMensajePrenomina.mensaje = paramString;
      return;
    }
    StateManager localStateManager = paramMensajePrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramMensajePrenomina.mensaje = paramString;
      return;
    }
    localStateManager.setStringField(paramMensajePrenomina, jdoInheritedFieldCount + 2, paramMensajePrenomina.mensaje, paramString);
  }

  private static final NominaEspecial jdoGetnominaEspecial(MensajePrenomina paramMensajePrenomina)
  {
    StateManager localStateManager = paramMensajePrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramMensajePrenomina.nominaEspecial;
    if (localStateManager.isLoaded(paramMensajePrenomina, jdoInheritedFieldCount + 3))
      return paramMensajePrenomina.nominaEspecial;
    return (NominaEspecial)localStateManager.getObjectField(paramMensajePrenomina, jdoInheritedFieldCount + 3, paramMensajePrenomina.nominaEspecial);
  }

  private static final void jdoSetnominaEspecial(MensajePrenomina paramMensajePrenomina, NominaEspecial paramNominaEspecial)
  {
    StateManager localStateManager = paramMensajePrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramMensajePrenomina.nominaEspecial = paramNominaEspecial;
      return;
    }
    localStateManager.setObjectField(paramMensajePrenomina, jdoInheritedFieldCount + 3, paramMensajePrenomina.nominaEspecial, paramNominaEspecial);
  }

  private static final UltimaPrenomina jdoGetultimaPrenomina(MensajePrenomina paramMensajePrenomina)
  {
    StateManager localStateManager = paramMensajePrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramMensajePrenomina.ultimaPrenomina;
    if (localStateManager.isLoaded(paramMensajePrenomina, jdoInheritedFieldCount + 4))
      return paramMensajePrenomina.ultimaPrenomina;
    return (UltimaPrenomina)localStateManager.getObjectField(paramMensajePrenomina, jdoInheritedFieldCount + 4, paramMensajePrenomina.ultimaPrenomina);
  }

  private static final void jdoSetultimaPrenomina(MensajePrenomina paramMensajePrenomina, UltimaPrenomina paramUltimaPrenomina)
  {
    StateManager localStateManager = paramMensajePrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramMensajePrenomina.ultimaPrenomina = paramUltimaPrenomina;
      return;
    }
    localStateManager.setObjectField(paramMensajePrenomina, jdoInheritedFieldCount + 4, paramMensajePrenomina.ultimaPrenomina, paramUltimaPrenomina);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}