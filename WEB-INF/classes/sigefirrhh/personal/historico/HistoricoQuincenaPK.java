package sigefirrhh.personal.historico;

import java.io.Serializable;

public class HistoricoQuincenaPK
  implements Serializable
{
  public long idHistoricoQuincena;

  public HistoricoQuincenaPK()
  {
  }

  public HistoricoQuincenaPK(long idHistoricoQuincena)
  {
    this.idHistoricoQuincena = idHistoricoQuincena;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HistoricoQuincenaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HistoricoQuincenaPK thatPK)
  {
    return 
      this.idHistoricoQuincena == thatPK.idHistoricoQuincena;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHistoricoQuincena)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHistoricoQuincena);
  }
}