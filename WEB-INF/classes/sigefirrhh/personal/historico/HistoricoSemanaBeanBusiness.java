package sigefirrhh.personal.historico;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoBeanBusiness;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.procesoNomina.NominaEspecialBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class HistoricoSemanaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(HistoricoSemanaBeanBusiness.class.getName());

  public void addHistoricoSemana(HistoricoSemana historicoSemana)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    this.log.error("entro en agregar");
    HistoricoSemana historicoSemanaNew = 
      (HistoricoSemana)BeanUtils.cloneBean(
      historicoSemana);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (historicoSemanaNew.getTipoPersonal() != null) {
      historicoSemanaNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        historicoSemanaNew.getTipoPersonal().getIdTipoPersonal()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (historicoSemanaNew.getGrupoNomina() != null) {
      historicoSemanaNew.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        historicoSemanaNew.getGrupoNomina().getIdGrupoNomina()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (historicoSemanaNew.getConceptoTipoPersonal() != null) {
      historicoSemanaNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        historicoSemanaNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (historicoSemanaNew.getFrecuenciaTipoPersonal() != null) {
      historicoSemanaNew.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        historicoSemanaNew.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    ConceptoBeanBusiness conceptoAporteBeanBusiness = new ConceptoBeanBusiness();

    if (historicoSemanaNew.getConceptoAporte() != null) {
      historicoSemanaNew.setConceptoAporte(
        conceptoAporteBeanBusiness.findConceptoById(
        historicoSemanaNew.getConceptoAporte().getIdConcepto()));
    }

    NominaEspecialBeanBusiness nominaEspecialBeanBusiness = new NominaEspecialBeanBusiness();

    if (historicoSemanaNew.getNominaEspecial() != null) {
      historicoSemanaNew.setNominaEspecial(
        nominaEspecialBeanBusiness.findNominaEspecialById(
        historicoSemanaNew.getNominaEspecial().getIdNominaEspecial()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (historicoSemanaNew.getTrabajador() != null) {
      historicoSemanaNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        historicoSemanaNew.getTrabajador().getIdTrabajador()));
    }

    HistoricoNominaBeanBusiness historicoNominaBeanBusiness = new HistoricoNominaBeanBusiness();

    if (historicoSemanaNew.getHistoricoNomina() != null) {
      historicoSemanaNew.setHistoricoNomina(
        historicoNominaBeanBusiness.findHistoricoNominaById(
        historicoSemanaNew.getHistoricoNomina().getIdHistoricoNomina()));
    }
    pm.makePersistent(historicoSemanaNew);
  }

  public void updateHistoricoSemana(HistoricoSemana historicoSemana) throws Exception
  {
    HistoricoSemana historicoSemanaModify = 
      findHistoricoSemanaById(historicoSemana.getIdHistoricoSemana());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (historicoSemana.getTipoPersonal() != null) {
      historicoSemana.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        historicoSemana.getTipoPersonal().getIdTipoPersonal()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (historicoSemana.getGrupoNomina() != null) {
      historicoSemana.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        historicoSemana.getGrupoNomina().getIdGrupoNomina()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (historicoSemana.getConceptoTipoPersonal() != null) {
      historicoSemana.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        historicoSemana.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (historicoSemana.getFrecuenciaTipoPersonal() != null) {
      historicoSemana.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        historicoSemana.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    ConceptoBeanBusiness conceptoAporteBeanBusiness = new ConceptoBeanBusiness();

    if (historicoSemana.getConceptoAporte() != null) {
      historicoSemana.setConceptoAporte(
        conceptoAporteBeanBusiness.findConceptoById(
        historicoSemana.getConceptoAporte().getIdConcepto()));
    }

    NominaEspecialBeanBusiness nominaEspecialBeanBusiness = new NominaEspecialBeanBusiness();

    if (historicoSemana.getNominaEspecial() != null) {
      historicoSemana.setNominaEspecial(
        nominaEspecialBeanBusiness.findNominaEspecialById(
        historicoSemana.getNominaEspecial().getIdNominaEspecial()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (historicoSemana.getTrabajador() != null) {
      historicoSemana.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        historicoSemana.getTrabajador().getIdTrabajador()));
    }

    HistoricoNominaBeanBusiness historicoNominaBeanBusiness = new HistoricoNominaBeanBusiness();

    if (historicoSemana.getHistoricoNomina() != null) {
      historicoSemana.setHistoricoNomina(
        historicoNominaBeanBusiness.findHistoricoNominaById(
        historicoSemana.getHistoricoNomina().getIdHistoricoNomina()));
    }

    BeanUtils.copyProperties(historicoSemanaModify, historicoSemana);
  }

  public void deleteHistoricoSemana(HistoricoSemana historicoSemana) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    HistoricoSemana historicoSemanaDelete = 
      findHistoricoSemanaById(historicoSemana.getIdHistoricoSemana());
    pm.deletePersistent(historicoSemanaDelete);
  }

  public HistoricoSemana findHistoricoSemanaById(long idHistoricoSemana) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idHistoricoSemana == pIdHistoricoSemana";
    Query query = pm.newQuery(HistoricoSemana.class, filter);

    query.declareParameters("long pIdHistoricoSemana");

    parameters.put("pIdHistoricoSemana", new Long(idHistoricoSemana));

    Collection colHistoricoSemana = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colHistoricoSemana.iterator();
    return (HistoricoSemana)iterator.next();
  }

  public Collection findHistoricoSemanaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent historicoSemanaExtent = pm.getExtent(
      HistoricoSemana.class, true);
    Query query = pm.newQuery(historicoSemanaExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}