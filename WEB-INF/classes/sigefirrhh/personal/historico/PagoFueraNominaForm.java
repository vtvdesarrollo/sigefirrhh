package sigefirrhh.personal.historico;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.sistema.RegistrarAuditoria;

public class PagoFueraNominaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PagoFueraNominaForm.class.getName());
  private HistoricoQuincena historicoQuincena;
  private HistoricoSemana historicoSemana;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private DefinicionesFacadeExtend definicionesFacadeExtend = new DefinicionesFacadeExtend();

  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private HistoricoNoGenFacade historicoFacade = new HistoricoNoGenFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colConceptoTipoPersonal;
  private String selectConceptoTipoPersonal;
  private Collection findColTipoPersonal;
  private boolean showAsignacion;
  private boolean showDeduccion;
  private long idConcepto;
  private Object stateResultHistoricoQuincenaByTrabajador = null;

  public boolean isShowAsignacion()
  {
    return this.showAsignacion;
  }
  public boolean isShowDeduccion() {
    return this.showDeduccion;
  }

  public void changeConcepto(ValueChangeEvent event) {
    this.idConcepto = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.showAsignacion = false;
    this.showDeduccion = false;
    try {
      if (this.idConcepto != 0L) {
        ConceptoTipoPersonal conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(this.idConcepto);
        if (Integer.valueOf(conceptoTipoPersonal.getConcepto().getCodConcepto()).intValue() > 5000)
          this.showDeduccion = true;
        else {
          this.showAsignacion = true;
        }
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getFindColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectConceptoTipoPersonal()
  {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    if (this.trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S"))
      this.historicoSemana.setConceptoTipoPersonal(null);
    else {
      this.historicoQuincena.setConceptoTipoPersonal(null);
    }

    while (iterator.hasNext())
    {
      Long id = (Long)iterator.next();
      iterator.next();
      if (String.valueOf(id).equals(
        valConceptoTipoPersonal)) {
        try {
          if (this.trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S"))
            this.historicoSemana.setConceptoTipoPersonal(this.definicionesFacade.findConceptoTipoPersonalById(id.longValue()));
          else
            this.historicoQuincena.setConceptoTipoPersonal(this.definicionesFacade.findConceptoTipoPersonalById(id.longValue()));
        }
        catch (Exception e) {
          log.error("Excepcion controlada:", e);
        }
      }
    }

    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public HistoricoQuincena getHistoricoQuincena() {
    if (this.historicoQuincena == null) {
      this.historicoQuincena = new HistoricoQuincena();
    }
    return this.historicoQuincena;
  }

  public PagoFueraNominaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        new DefinicionesFacade().findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndIdTipoPersonal(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidosAndIdTipoPersonal(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());

        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findHistoricoByTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectTrabajador();
      if (!this.adding) {
        if (this.trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S"))
          this.result = 
            this.historicoFacade.findHistoricoSemanaByTrabajadorAndNumeroNomina(
            this.trabajador.getIdTrabajador(), 999);
        else {
          this.result = 
            this.historicoFacade.findHistoricoQuincenaByTrabajadorAndNumeroNomina(
            this.trabajador.getIdTrabajador(), 999);
        }

        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectHistoricoQuincena()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConceptoTipoPersonal = null;

    long idHistoricoQuincena = 
      Long.parseLong((String)requestParameterMap.get("idHistoricoQuincena"));
    try
    {
      this.showAsignacion = false;
      this.showDeduccion = false;

      this.historicoQuincena = 
        this.historicoFacade.findHistoricoQuincenaById(
        idHistoricoQuincena);
      if (Integer.valueOf(this.historicoQuincena.getConceptoTipoPersonal().getConcepto().getCodConcepto()).intValue() > 5000)
        this.showDeduccion = true;
      else {
        this.showAsignacion = true;
      }

      if (this.historicoQuincena.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.historicoQuincena.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectHistoricoSemana() {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConceptoTipoPersonal = null;

    long idHistoricoSemana = 
      Long.parseLong((String)requestParameterMap.get("idHistoricoSemana"));

    this.showAsignacion = false;
    this.showDeduccion = false;
    try
    {
      this.historicoSemana = 
        this.historicoFacade.findHistoricoSemanaById(
        idHistoricoSemana);

      if (Integer.valueOf(this.historicoSemana.getConceptoTipoPersonal().getConcepto().getCodConcepto()).intValue() > 5000)
        this.showDeduccion = true;
      else {
        this.showAsignacion = true;
      }

      if (this.historicoSemana.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.historicoSemana.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
      this.colConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(
        this.trabajador.getTipoPersonal().getIdTipoPersonal());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (this.trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S"))
    {
      if ((this.historicoSemana.getFecha() != null) && 
        (this.historicoSemana.getFecha().compareTo(new Date()) > 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha no puede ser mayor a la fecha de hoy", ""));
        error = true;
      }

    }
    else if ((this.historicoQuincena.getFecha() != null) && 
      (this.historicoQuincena.getFecha().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S")) {
        if (Integer.valueOf(this.historicoSemana.getConceptoTipoPersonal().getConcepto().getCodConcepto()).intValue() > 5000)
          this.historicoSemana.setMontoAsigna(0.0D);
        else {
          this.historicoSemana.setMontoDeduce(0.0D);
        }
        if (this.adding) {
          this.historicoSemana.setTrabajador(
            this.trabajador);
          this.historicoFacade.addHistoricoSemana(
            this.historicoSemana);
          RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.historicoSemana, this.trabajador.getPersonal());

          context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
        } else {
          this.historicoFacade.updateHistoricoSemana(
            this.historicoSemana);
          RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.historicoSemana, this.trabajador.getPersonal());

          context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
        }
      } else {
        if (Integer.valueOf(this.historicoQuincena.getConceptoTipoPersonal().getConcepto().getCodConcepto()).intValue() > 5000)
          this.historicoQuincena.setMontoAsigna(0.0D);
        else {
          this.historicoQuincena.setMontoDeduce(0.0D);
        }
        if (this.adding) {
          this.historicoQuincena.setTrabajador(
            this.trabajador);
          this.historicoFacade.addHistoricoQuincena(
            this.historicoQuincena);
          RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.historicoQuincena, this.trabajador.getPersonal());

          context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
        } else {
          this.historicoFacade.updateHistoricoQuincena(
            this.historicoQuincena);
          RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.historicoQuincena, this.trabajador.getPersonal());

          context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
        }
      }

      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e)
    {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S")) {
        this.historicoFacade.deleteHistoricoSemana(
          this.historicoSemana);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.historicoSemana, this.trabajador.getPersonal());
      }
      else {
        this.historicoFacade.deleteHistoricoQuincena(
          this.historicoQuincena);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.historicoQuincena, this.trabajador.getPersonal());
      }

      context.addMessage("success_delete", new FacesMessage("Se eliminï¿½ con ï¿½xito"));
      this.result = null;

      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedTrabajador = true;

    this.selectConceptoTipoPersonal = null;

    if (this.trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S")) {
      this.historicoSemana = new HistoricoSemana();
      log.error("Sema-1");
      this.historicoSemana.setTrabajador(this.trabajador);
      this.historicoSemana.setTipoPersonal(this.trabajador.getTipoPersonal());
      this.historicoSemana.setGrupoNomina(this.trabajador.getTipoPersonal().getGrupoNomina());
      this.historicoSemana.setNumeroNomina(999);
      this.historicoSemana.setOrigen("V");
      log.error("Sema-2");
      try {
        this.historicoSemana.setFrecuenciaTipoPersonal(this.definicionesFacadeExtend.findFrecuenciaTipoPersonalByTipoPersonal(999, this.trabajador.getTipoPersonal().getIdTipoPersonal()));
        log.error("Sema-3");
      } catch (Exception e) {
        log.error("Excepcion controlada:", e);
      }
      IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
      log.error("Sema-4");
      this.historicoSemana.setIdHistoricoSemana(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.historico.HistoricoSemana"));

      log.error("Sema-5");
    } else {
      log.error("Quim-0-entro");
      this.historicoQuincena = new HistoricoQuincena();
      log.error("Quim-1");
      this.historicoQuincena.setTrabajador(this.trabajador);
      this.historicoQuincena.setTipoPersonal(this.trabajador.getTipoPersonal());
      this.historicoQuincena.setGrupoNomina(this.trabajador.getTipoPersonal().getGrupoNomina());
      this.historicoQuincena.setNumeroNomina(999);
      this.historicoQuincena.setOrigen("V");
      log.error("Quim-2");
      try {
        this.historicoQuincena.setFrecuenciaTipoPersonal(this.definicionesFacadeExtend.findFrecuenciaTipoPersonalByTipoPersonal(999, this.trabajador.getTipoPersonal().getIdTipoPersonal()));
        log.error("Quim-3");
      } catch (Exception e) {
        log.error("Excepcion controlada:", e);
      }
      IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
      log.error("Quim-4");
      this.historicoQuincena.setIdHistoricoQuincena(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.historico.HistoricoQuincena"));
      log.error("Quim-5");
    }

    return null;
  }

  public boolean isShowNominaEspecialAux() {
    try {
      return this.historicoQuincena.getNumeroNomina() > 0; } catch (Exception e) {
    }
    return false;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.historicoQuincena = new HistoricoQuincena();
    this.historicoSemana = new HistoricoSemana();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.historicoQuincena = new HistoricoQuincena();
    this.historicoSemana = new HistoricoSemana();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedTrabajador));
  }
  public boolean isShowDataQuincena() {
    return ((this.selected) || ((this.adding) && (this.selectedTrabajador))) && (this.trabajador != null) && (!this.trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S"));
  }
  public boolean isShowDataSemana() {
    return ((this.selected) || ((this.adding) && (this.selectedTrabajador))) && (this.trabajador != null) && (this.trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S"));
  }
  public boolean isShowResultQuincena() {
    return (this.trabajador != null) && (!this.trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S"));
  }
  public boolean isShowResultSemana() {
    return (this.trabajador != null) && (this.trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S"));
  }
  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedTrabajador);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public LoginSession getLogin() {
    return this.login;
  }
  public HistoricoSemana getHistoricoSemana() {
    return this.historicoSemana;
  }
}