package sigefirrhh.personal.historico;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class HistoricoFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private HistoricoBusiness historicoBusiness = new HistoricoBusiness();

  public void addHistoricoNomina(HistoricoNomina historicoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.historicoBusiness.addHistoricoNomina(historicoNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateHistoricoNomina(HistoricoNomina historicoNomina) throws Exception
  {
    try { this.txn.open();
      this.historicoBusiness.updateHistoricoNomina(historicoNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteHistoricoNomina(HistoricoNomina historicoNomina) throws Exception
  {
    try { this.txn.open();
      this.historicoBusiness.deleteHistoricoNomina(historicoNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public HistoricoNomina findHistoricoNominaById(long historicoNominaId) throws Exception
  {
    try { this.txn.open();
      HistoricoNomina historicoNomina = 
        this.historicoBusiness.findHistoricoNominaById(historicoNominaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(historicoNomina);
      return historicoNomina;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllHistoricoNomina() throws Exception
  {
    try { this.txn.open();
      return this.historicoBusiness.findAllHistoricoNomina();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addHistoricoQuincena(HistoricoQuincena historicoQuincena)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.historicoBusiness.addHistoricoQuincena(historicoQuincena);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateHistoricoQuincena(HistoricoQuincena historicoQuincena) throws Exception
  {
    try { this.txn.open();
      this.historicoBusiness.updateHistoricoQuincena(historicoQuincena);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteHistoricoQuincena(HistoricoQuincena historicoQuincena) throws Exception
  {
    try { this.txn.open();
      this.historicoBusiness.deleteHistoricoQuincena(historicoQuincena);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public HistoricoQuincena findHistoricoQuincenaById(long historicoQuincenaId) throws Exception
  {
    try { this.txn.open();
      HistoricoQuincena historicoQuincena = 
        this.historicoBusiness.findHistoricoQuincenaById(historicoQuincenaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(historicoQuincena);
      return historicoQuincena;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllHistoricoQuincena() throws Exception
  {
    try { this.txn.open();
      return this.historicoBusiness.findAllHistoricoQuincena();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addHistoricoSemana(HistoricoSemana historicoSemana)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.historicoBusiness.addHistoricoSemana(historicoSemana);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateHistoricoSemana(HistoricoSemana historicoSemana) throws Exception
  {
    try { this.txn.open();
      this.historicoBusiness.updateHistoricoSemana(historicoSemana);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteHistoricoSemana(HistoricoSemana historicoSemana) throws Exception
  {
    try { this.txn.open();
      this.historicoBusiness.deleteHistoricoSemana(historicoSemana);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public HistoricoSemana findHistoricoSemanaById(long historicoSemanaId) throws Exception
  {
    try { this.txn.open();
      HistoricoSemana historicoSemana = 
        this.historicoBusiness.findHistoricoSemanaById(historicoSemanaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(historicoSemana);
      return historicoSemana;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllHistoricoSemana() throws Exception
  {
    try { this.txn.open();
      return this.historicoBusiness.findAllHistoricoSemana();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}