package sigefirrhh.personal.historico;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.BancoBeanBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.DependenciaBeanBusiness;
import sigefirrhh.base.estructura.LugarPago;
import sigefirrhh.base.estructura.LugarPagoBeanBusiness;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.RegionBeanBusiness;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.estructura.SedeBeanBusiness;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.base.estructura.UnidadAdministradoraBeanBusiness;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.base.estructura.UnidadEjecutoraBeanBusiness;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.procesoNomina.NominaEspecialBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class HistoricoNominaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addHistoricoNomina(HistoricoNomina historicoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    HistoricoNomina historicoNominaNew = 
      (HistoricoNomina)BeanUtils.cloneBean(
      historicoNomina);

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (historicoNominaNew.getDependencia() != null) {
      historicoNominaNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        historicoNominaNew.getDependencia().getIdDependencia()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (historicoNominaNew.getCargo() != null) {
      historicoNominaNew.setCargo(
        cargoBeanBusiness.findCargoById(
        historicoNominaNew.getCargo().getIdCargo()));
    }

    BancoBeanBusiness bancoBeanBusiness = new BancoBeanBusiness();

    if (historicoNominaNew.getBanco() != null) {
      historicoNominaNew.setBanco(
        bancoBeanBusiness.findBancoById(
        historicoNominaNew.getBanco().getIdBanco()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (historicoNominaNew.getTipoPersonal() != null) {
      historicoNominaNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        historicoNominaNew.getTipoPersonal().getIdTipoPersonal()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (historicoNominaNew.getGrupoNomina() != null) {
      historicoNominaNew.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        historicoNominaNew.getGrupoNomina().getIdGrupoNomina()));
    }

    NominaEspecialBeanBusiness nominaEspecialBeanBusiness = new NominaEspecialBeanBusiness();

    if (historicoNominaNew.getNominaEspecial() != null) {
      historicoNominaNew.setNominaEspecial(
        nominaEspecialBeanBusiness.findNominaEspecialById(
        historicoNominaNew.getNominaEspecial().getIdNominaEspecial()));
    }

    LugarPagoBeanBusiness lugarPagoBeanBusiness = new LugarPagoBeanBusiness();

    if (historicoNominaNew.getLugarPago() != null) {
      historicoNominaNew.setLugarPago(
        lugarPagoBeanBusiness.findLugarPagoById(
        historicoNominaNew.getLugarPago().getIdLugarPago()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (historicoNominaNew.getTrabajador() != null) {
      historicoNominaNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        historicoNominaNew.getTrabajador().getIdTrabajador()));
    }

    UnidadEjecutoraBeanBusiness unidadEjecutoraBeanBusiness = new UnidadEjecutoraBeanBusiness();

    if (historicoNominaNew.getUnidadEjecutora() != null) {
      historicoNominaNew.setUnidadEjecutora(
        unidadEjecutoraBeanBusiness.findUnidadEjecutoraById(
        historicoNominaNew.getUnidadEjecutora().getIdUnidadEjecutora()));
    }

    UnidadAdministradoraBeanBusiness unidadAdministradoraBeanBusiness = new UnidadAdministradoraBeanBusiness();

    if (historicoNominaNew.getUnidadAdministradora() != null) {
      historicoNominaNew.setUnidadAdministradora(
        unidadAdministradoraBeanBusiness.findUnidadAdministradoraById(
        historicoNominaNew.getUnidadAdministradora().getIdUnidadAdministradora()));
    }

    SedeBeanBusiness sedeBeanBusiness = new SedeBeanBusiness();

    if (historicoNominaNew.getSede() != null) {
      historicoNominaNew.setSede(
        sedeBeanBusiness.findSedeById(
        historicoNominaNew.getSede().getIdSede()));
    }

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (historicoNominaNew.getRegion() != null) {
      historicoNominaNew.setRegion(
        regionBeanBusiness.findRegionById(
        historicoNominaNew.getRegion().getIdRegion()));
    }
    pm.makePersistent(historicoNominaNew);
  }

  public void updateHistoricoNomina(HistoricoNomina historicoNomina) throws Exception
  {
    HistoricoNomina historicoNominaModify = 
      findHistoricoNominaById(historicoNomina.getIdHistoricoNomina());

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (historicoNomina.getDependencia() != null) {
      historicoNomina.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        historicoNomina.getDependencia().getIdDependencia()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (historicoNomina.getCargo() != null) {
      historicoNomina.setCargo(
        cargoBeanBusiness.findCargoById(
        historicoNomina.getCargo().getIdCargo()));
    }

    BancoBeanBusiness bancoBeanBusiness = new BancoBeanBusiness();

    if (historicoNomina.getBanco() != null) {
      historicoNomina.setBanco(
        bancoBeanBusiness.findBancoById(
        historicoNomina.getBanco().getIdBanco()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (historicoNomina.getTipoPersonal() != null) {
      historicoNomina.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        historicoNomina.getTipoPersonal().getIdTipoPersonal()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (historicoNomina.getGrupoNomina() != null) {
      historicoNomina.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        historicoNomina.getGrupoNomina().getIdGrupoNomina()));
    }

    NominaEspecialBeanBusiness nominaEspecialBeanBusiness = new NominaEspecialBeanBusiness();

    if (historicoNomina.getNominaEspecial() != null) {
      historicoNomina.setNominaEspecial(
        nominaEspecialBeanBusiness.findNominaEspecialById(
        historicoNomina.getNominaEspecial().getIdNominaEspecial()));
    }

    LugarPagoBeanBusiness lugarPagoBeanBusiness = new LugarPagoBeanBusiness();

    if (historicoNomina.getLugarPago() != null) {
      historicoNomina.setLugarPago(
        lugarPagoBeanBusiness.findLugarPagoById(
        historicoNomina.getLugarPago().getIdLugarPago()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (historicoNomina.getTrabajador() != null) {
      historicoNomina.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        historicoNomina.getTrabajador().getIdTrabajador()));
    }

    UnidadEjecutoraBeanBusiness unidadEjecutoraBeanBusiness = new UnidadEjecutoraBeanBusiness();

    if (historicoNomina.getUnidadEjecutora() != null) {
      historicoNomina.setUnidadEjecutora(
        unidadEjecutoraBeanBusiness.findUnidadEjecutoraById(
        historicoNomina.getUnidadEjecutora().getIdUnidadEjecutora()));
    }

    UnidadAdministradoraBeanBusiness unidadAdministradoraBeanBusiness = new UnidadAdministradoraBeanBusiness();

    if (historicoNomina.getUnidadAdministradora() != null) {
      historicoNomina.setUnidadAdministradora(
        unidadAdministradoraBeanBusiness.findUnidadAdministradoraById(
        historicoNomina.getUnidadAdministradora().getIdUnidadAdministradora()));
    }

    SedeBeanBusiness sedeBeanBusiness = new SedeBeanBusiness();

    if (historicoNomina.getSede() != null) {
      historicoNomina.setSede(
        sedeBeanBusiness.findSedeById(
        historicoNomina.getSede().getIdSede()));
    }

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (historicoNomina.getRegion() != null) {
      historicoNomina.setRegion(
        regionBeanBusiness.findRegionById(
        historicoNomina.getRegion().getIdRegion()));
    }

    BeanUtils.copyProperties(historicoNominaModify, historicoNomina);
  }

  public void deleteHistoricoNomina(HistoricoNomina historicoNomina) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    HistoricoNomina historicoNominaDelete = 
      findHistoricoNominaById(historicoNomina.getIdHistoricoNomina());
    pm.deletePersistent(historicoNominaDelete);
  }

  public HistoricoNomina findHistoricoNominaById(long idHistoricoNomina) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idHistoricoNomina == pIdHistoricoNomina";
    Query query = pm.newQuery(HistoricoNomina.class, filter);

    query.declareParameters("long pIdHistoricoNomina");

    parameters.put("pIdHistoricoNomina", new Long(idHistoricoNomina));

    Collection colHistoricoNomina = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colHistoricoNomina.iterator();
    return (HistoricoNomina)iterator.next();
  }

  public Collection findHistoricoNominaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent historicoNominaExtent = pm.getExtent(
      HistoricoNomina.class, true);
    Query query = pm.newQuery(historicoNominaExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}