package sigefirrhh.personal.docentes;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.docente.CausaDocente;
import sigefirrhh.base.docente.CausaDocenteBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class TrayectoriaDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTrayectoriaDocente(TrayectoriaDocente trayectoriaDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TrayectoriaDocente trayectoriaDocenteNew = 
      (TrayectoriaDocente)BeanUtils.cloneBean(
      trayectoriaDocente);

    CausaDocenteBeanBusiness causaDocenteBeanBusiness = new CausaDocenteBeanBusiness();

    if (trayectoriaDocenteNew.getCausaDocente() != null) {
      trayectoriaDocenteNew.setCausaDocente(
        causaDocenteBeanBusiness.findCausaDocenteById(
        trayectoriaDocenteNew.getCausaDocente().getIdCausaDocente()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (trayectoriaDocenteNew.getPersonal() != null) {
      trayectoriaDocenteNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        trayectoriaDocenteNew.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (trayectoriaDocenteNew.getOrganismo() != null) {
      trayectoriaDocenteNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        trayectoriaDocenteNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(trayectoriaDocenteNew);
  }

  public void updateTrayectoriaDocente(TrayectoriaDocente trayectoriaDocente) throws Exception
  {
    TrayectoriaDocente trayectoriaDocenteModify = 
      findTrayectoriaDocenteById(trayectoriaDocente.getIdTrayectoriaDocente());

    CausaDocenteBeanBusiness causaDocenteBeanBusiness = new CausaDocenteBeanBusiness();

    if (trayectoriaDocente.getCausaDocente() != null) {
      trayectoriaDocente.setCausaDocente(
        causaDocenteBeanBusiness.findCausaDocenteById(
        trayectoriaDocente.getCausaDocente().getIdCausaDocente()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (trayectoriaDocente.getPersonal() != null) {
      trayectoriaDocente.setPersonal(
        personalBeanBusiness.findPersonalById(
        trayectoriaDocente.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (trayectoriaDocente.getOrganismo() != null) {
      trayectoriaDocente.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        trayectoriaDocente.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(trayectoriaDocenteModify, trayectoriaDocente);
  }

  public void deleteTrayectoriaDocente(TrayectoriaDocente trayectoriaDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TrayectoriaDocente trayectoriaDocenteDelete = 
      findTrayectoriaDocenteById(trayectoriaDocente.getIdTrayectoriaDocente());
    pm.deletePersistent(trayectoriaDocenteDelete);
  }

  public TrayectoriaDocente findTrayectoriaDocenteById(long idTrayectoriaDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTrayectoriaDocente == pIdTrayectoriaDocente";
    Query query = pm.newQuery(TrayectoriaDocente.class, filter);

    query.declareParameters("long pIdTrayectoriaDocente");

    parameters.put("pIdTrayectoriaDocente", new Long(idTrayectoriaDocente));

    Collection colTrayectoriaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTrayectoriaDocente.iterator();
    return (TrayectoriaDocente)iterator.next();
  }

  public Collection findTrayectoriaDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent trayectoriaDocenteExtent = pm.getExtent(
      TrayectoriaDocente.class, true);
    Query query = pm.newQuery(trayectoriaDocenteExtent);
    query.setOrdering("fechaMovimiento ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCausaDocente(long idCausaDocente, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "causaDocente.idCausaDocente == pIdCausaDocente &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(TrayectoriaDocente.class, filter);

    query.declareParameters("long pIdCausaDocente, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdCausaDocente", new Long(idCausaDocente));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaMovimiento ascending");

    Collection colTrayectoriaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrayectoriaDocente);

    return colTrayectoriaDocente;
  }

  public Collection findByCodCausaDocente(String codCausaDocente, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCausaDocente == pCodCausaDocente &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(TrayectoriaDocente.class, filter);

    query.declareParameters("java.lang.String pCodCausaDocente, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodCausaDocente", new String(codCausaDocente));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaMovimiento ascending");

    Collection colTrayectoriaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrayectoriaDocente);

    return colTrayectoriaDocente;
  }

  public Collection findByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(TrayectoriaDocente.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaMovimiento ascending");

    Collection colTrayectoriaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrayectoriaDocente);

    return colTrayectoriaDocente;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(TrayectoriaDocente.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaMovimiento ascending");

    Collection colTrayectoriaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrayectoriaDocente);

    return colTrayectoriaDocente;
  }
}