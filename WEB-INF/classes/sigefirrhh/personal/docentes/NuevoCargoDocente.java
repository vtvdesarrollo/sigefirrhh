package sigefirrhh.personal.docentes;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.personal.registroCargos.RegistroDocente;
import sigefirrhh.personal.trabajador.Trabajador;

public class NuevoCargoDocente
  implements Serializable, PersistenceCapable
{
  private long idNuevoCargoDocente;
  private double sueldoActual;
  private double sueldoNuevo;
  private Trabajador trabajador;
  private RegistroDocente registroDocente;
  private Cargo cargoActual;
  private Cargo cargoNuevo;
  private Collection conceptosActuales;
  private Collection conceptosNuevos;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargoActual", "cargoNuevo", "conceptosActuales", "conceptosNuevos", "idNuevoCargoDocente", "registroDocente", "sueldoActual", "sueldoNuevo", "trabajador" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), sunjdo$classForName$("java.util.Collection"), sunjdo$classForName$("java.util.Collection"), Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.registroCargos.RegistroDocente"), Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 26, 26, 26, 26, 24, 26, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public NuevoCargoDocente()
  {
    jdoSetconceptosActuales(this, new ArrayList());
    jdoSetconceptosNuevos(this, new ArrayList());
  }
  public Cargo getCargoActual() {
    return jdoGetcargoActual(this);
  }
  public void setCargoActual(Cargo cargoActual) {
    jdoSetcargoActual(this, cargoActual);
  }
  public Cargo getCargoNuevo() {
    return jdoGetcargoNuevo(this);
  }
  public void setCargoNuevo(Cargo cargoNuevo) {
    jdoSetcargoNuevo(this, cargoNuevo);
  }
  public Collection getConceptosActuales() {
    return jdoGetconceptosActuales(this);
  }
  public void setConceptosActuales(Collection conceptosActuales) {
    jdoSetconceptosActuales(this, conceptosActuales);
  }
  public Collection getConceptosNuevos() {
    return jdoGetconceptosNuevos(this);
  }
  public void setConceptosNuevos(Collection conceptosNuevos) {
    jdoSetconceptosNuevos(this, conceptosNuevos);
  }
  public double getSueldoActual() {
    return jdoGetsueldoActual(this);
  }
  public void setSueldoActual(double sueldoActual) {
    jdoSetsueldoActual(this, sueldoActual);
  }
  public double getSueldoNuevo() {
    return jdoGetsueldoNuevo(this);
  }
  public void setSueldoNuevo(double sueldoNuevo) {
    jdoSetsueldoNuevo(this, sueldoNuevo);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }
  public RegistroDocente getRegistroDocente() {
    return jdoGetregistroDocente(this);
  }
  public void setRegistroDocente(RegistroDocente registroDocente) {
    jdoSetregistroDocente(this, registroDocente);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.docentes.NuevoCargoDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new NuevoCargoDocente());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    NuevoCargoDocente localNuevoCargoDocente = new NuevoCargoDocente();
    localNuevoCargoDocente.jdoFlags = 1;
    localNuevoCargoDocente.jdoStateManager = paramStateManager;
    return localNuevoCargoDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    NuevoCargoDocente localNuevoCargoDocente = new NuevoCargoDocente();
    localNuevoCargoDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localNuevoCargoDocente.jdoFlags = 1;
    localNuevoCargoDocente.jdoStateManager = paramStateManager;
    return localNuevoCargoDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargoActual);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargoNuevo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptosActuales);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptosNuevos);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idNuevoCargoDocente);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registroDocente);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoActual);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoNuevo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoActual = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoNuevo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptosActuales = ((Collection)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptosNuevos = ((Collection)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idNuevoCargoDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registroDocente = ((RegistroDocente)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoActual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoNuevo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(NuevoCargoDocente paramNuevoCargoDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramNuevoCargoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.cargoActual = paramNuevoCargoDocente.cargoActual;
      return;
    case 1:
      if (paramNuevoCargoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.cargoNuevo = paramNuevoCargoDocente.cargoNuevo;
      return;
    case 2:
      if (paramNuevoCargoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.conceptosActuales = paramNuevoCargoDocente.conceptosActuales;
      return;
    case 3:
      if (paramNuevoCargoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.conceptosNuevos = paramNuevoCargoDocente.conceptosNuevos;
      return;
    case 4:
      if (paramNuevoCargoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idNuevoCargoDocente = paramNuevoCargoDocente.idNuevoCargoDocente;
      return;
    case 5:
      if (paramNuevoCargoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.registroDocente = paramNuevoCargoDocente.registroDocente;
      return;
    case 6:
      if (paramNuevoCargoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoActual = paramNuevoCargoDocente.sueldoActual;
      return;
    case 7:
      if (paramNuevoCargoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoNuevo = paramNuevoCargoDocente.sueldoNuevo;
      return;
    case 8:
      if (paramNuevoCargoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramNuevoCargoDocente.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof NuevoCargoDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    NuevoCargoDocente localNuevoCargoDocente = (NuevoCargoDocente)paramObject;
    if (localNuevoCargoDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localNuevoCargoDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new NuevoCargoDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new NuevoCargoDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NuevoCargoDocentePK))
      throw new IllegalArgumentException("arg1");
    NuevoCargoDocentePK localNuevoCargoDocentePK = (NuevoCargoDocentePK)paramObject;
    localNuevoCargoDocentePK.idNuevoCargoDocente = this.idNuevoCargoDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NuevoCargoDocentePK))
      throw new IllegalArgumentException("arg1");
    NuevoCargoDocentePK localNuevoCargoDocentePK = (NuevoCargoDocentePK)paramObject;
    this.idNuevoCargoDocente = localNuevoCargoDocentePK.idNuevoCargoDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NuevoCargoDocentePK))
      throw new IllegalArgumentException("arg2");
    NuevoCargoDocentePK localNuevoCargoDocentePK = (NuevoCargoDocentePK)paramObject;
    localNuevoCargoDocentePK.idNuevoCargoDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NuevoCargoDocentePK))
      throw new IllegalArgumentException("arg2");
    NuevoCargoDocentePK localNuevoCargoDocentePK = (NuevoCargoDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localNuevoCargoDocentePK.idNuevoCargoDocente);
  }

  private static final Cargo jdoGetcargoActual(NuevoCargoDocente paramNuevoCargoDocente)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramNuevoCargoDocente.cargoActual;
    if (localStateManager.isLoaded(paramNuevoCargoDocente, jdoInheritedFieldCount + 0))
      return paramNuevoCargoDocente.cargoActual;
    return (Cargo)localStateManager.getObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 0, paramNuevoCargoDocente.cargoActual);
  }

  private static final void jdoSetcargoActual(NuevoCargoDocente paramNuevoCargoDocente, Cargo paramCargo)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNuevoCargoDocente.cargoActual = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 0, paramNuevoCargoDocente.cargoActual, paramCargo);
  }

  private static final Cargo jdoGetcargoNuevo(NuevoCargoDocente paramNuevoCargoDocente)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramNuevoCargoDocente.cargoNuevo;
    if (localStateManager.isLoaded(paramNuevoCargoDocente, jdoInheritedFieldCount + 1))
      return paramNuevoCargoDocente.cargoNuevo;
    return (Cargo)localStateManager.getObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 1, paramNuevoCargoDocente.cargoNuevo);
  }

  private static final void jdoSetcargoNuevo(NuevoCargoDocente paramNuevoCargoDocente, Cargo paramCargo)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNuevoCargoDocente.cargoNuevo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 1, paramNuevoCargoDocente.cargoNuevo, paramCargo);
  }

  private static final Collection jdoGetconceptosActuales(NuevoCargoDocente paramNuevoCargoDocente)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramNuevoCargoDocente.conceptosActuales;
    if (localStateManager.isLoaded(paramNuevoCargoDocente, jdoInheritedFieldCount + 2))
      return paramNuevoCargoDocente.conceptosActuales;
    return (Collection)localStateManager.getObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 2, paramNuevoCargoDocente.conceptosActuales);
  }

  private static final void jdoSetconceptosActuales(NuevoCargoDocente paramNuevoCargoDocente, Collection paramCollection)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNuevoCargoDocente.conceptosActuales = paramCollection;
      return;
    }
    localStateManager.setObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 2, paramNuevoCargoDocente.conceptosActuales, paramCollection);
  }

  private static final Collection jdoGetconceptosNuevos(NuevoCargoDocente paramNuevoCargoDocente)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramNuevoCargoDocente.conceptosNuevos;
    if (localStateManager.isLoaded(paramNuevoCargoDocente, jdoInheritedFieldCount + 3))
      return paramNuevoCargoDocente.conceptosNuevos;
    return (Collection)localStateManager.getObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 3, paramNuevoCargoDocente.conceptosNuevos);
  }

  private static final void jdoSetconceptosNuevos(NuevoCargoDocente paramNuevoCargoDocente, Collection paramCollection)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNuevoCargoDocente.conceptosNuevos = paramCollection;
      return;
    }
    localStateManager.setObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 3, paramNuevoCargoDocente.conceptosNuevos, paramCollection);
  }

  private static final long jdoGetidNuevoCargoDocente(NuevoCargoDocente paramNuevoCargoDocente)
  {
    return paramNuevoCargoDocente.idNuevoCargoDocente;
  }

  private static final void jdoSetidNuevoCargoDocente(NuevoCargoDocente paramNuevoCargoDocente, long paramLong)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNuevoCargoDocente.idNuevoCargoDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramNuevoCargoDocente, jdoInheritedFieldCount + 4, paramNuevoCargoDocente.idNuevoCargoDocente, paramLong);
  }

  private static final RegistroDocente jdoGetregistroDocente(NuevoCargoDocente paramNuevoCargoDocente)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramNuevoCargoDocente.registroDocente;
    if (localStateManager.isLoaded(paramNuevoCargoDocente, jdoInheritedFieldCount + 5))
      return paramNuevoCargoDocente.registroDocente;
    return (RegistroDocente)localStateManager.getObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 5, paramNuevoCargoDocente.registroDocente);
  }

  private static final void jdoSetregistroDocente(NuevoCargoDocente paramNuevoCargoDocente, RegistroDocente paramRegistroDocente)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNuevoCargoDocente.registroDocente = paramRegistroDocente;
      return;
    }
    localStateManager.setObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 5, paramNuevoCargoDocente.registroDocente, paramRegistroDocente);
  }

  private static final double jdoGetsueldoActual(NuevoCargoDocente paramNuevoCargoDocente)
  {
    if (paramNuevoCargoDocente.jdoFlags <= 0)
      return paramNuevoCargoDocente.sueldoActual;
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramNuevoCargoDocente.sueldoActual;
    if (localStateManager.isLoaded(paramNuevoCargoDocente, jdoInheritedFieldCount + 6))
      return paramNuevoCargoDocente.sueldoActual;
    return localStateManager.getDoubleField(paramNuevoCargoDocente, jdoInheritedFieldCount + 6, paramNuevoCargoDocente.sueldoActual);
  }

  private static final void jdoSetsueldoActual(NuevoCargoDocente paramNuevoCargoDocente, double paramDouble)
  {
    if (paramNuevoCargoDocente.jdoFlags == 0)
    {
      paramNuevoCargoDocente.sueldoActual = paramDouble;
      return;
    }
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNuevoCargoDocente.sueldoActual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNuevoCargoDocente, jdoInheritedFieldCount + 6, paramNuevoCargoDocente.sueldoActual, paramDouble);
  }

  private static final double jdoGetsueldoNuevo(NuevoCargoDocente paramNuevoCargoDocente)
  {
    if (paramNuevoCargoDocente.jdoFlags <= 0)
      return paramNuevoCargoDocente.sueldoNuevo;
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramNuevoCargoDocente.sueldoNuevo;
    if (localStateManager.isLoaded(paramNuevoCargoDocente, jdoInheritedFieldCount + 7))
      return paramNuevoCargoDocente.sueldoNuevo;
    return localStateManager.getDoubleField(paramNuevoCargoDocente, jdoInheritedFieldCount + 7, paramNuevoCargoDocente.sueldoNuevo);
  }

  private static final void jdoSetsueldoNuevo(NuevoCargoDocente paramNuevoCargoDocente, double paramDouble)
  {
    if (paramNuevoCargoDocente.jdoFlags == 0)
    {
      paramNuevoCargoDocente.sueldoNuevo = paramDouble;
      return;
    }
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNuevoCargoDocente.sueldoNuevo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNuevoCargoDocente, jdoInheritedFieldCount + 7, paramNuevoCargoDocente.sueldoNuevo, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(NuevoCargoDocente paramNuevoCargoDocente)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramNuevoCargoDocente.trabajador;
    if (localStateManager.isLoaded(paramNuevoCargoDocente, jdoInheritedFieldCount + 8))
      return paramNuevoCargoDocente.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 8, paramNuevoCargoDocente.trabajador);
  }

  private static final void jdoSettrabajador(NuevoCargoDocente paramNuevoCargoDocente, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramNuevoCargoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramNuevoCargoDocente.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramNuevoCargoDocente, jdoInheritedFieldCount + 8, paramNuevoCargoDocente.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}