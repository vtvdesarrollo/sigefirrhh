package sigefirrhh.personal.docentes;

import java.io.Serializable;

public class NuevoCargoDocentePK
  implements Serializable
{
  public long idNuevoCargoDocente;

  public NuevoCargoDocentePK()
  {
  }

  public NuevoCargoDocentePK(long idNuevoCargoDocente)
  {
    this.idNuevoCargoDocente = idNuevoCargoDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((NuevoCargoDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(NuevoCargoDocentePK thatPK)
  {
    return 
      this.idNuevoCargoDocente == thatPK.idNuevoCargoDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idNuevoCargoDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idNuevoCargoDocente);
  }
}