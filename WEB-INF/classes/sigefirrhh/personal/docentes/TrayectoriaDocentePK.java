package sigefirrhh.personal.docentes;

import java.io.Serializable;

public class TrayectoriaDocentePK
  implements Serializable
{
  public long idTrayectoriaDocente;

  public TrayectoriaDocentePK()
  {
  }

  public TrayectoriaDocentePK(long idTrayectoriaDocente)
  {
    this.idTrayectoriaDocente = idTrayectoriaDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TrayectoriaDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TrayectoriaDocentePK thatPK)
  {
    return 
      this.idTrayectoriaDocente == thatPK.idTrayectoriaDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTrayectoriaDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTrayectoriaDocente);
  }
}