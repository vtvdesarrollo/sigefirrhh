package sigefirrhh.personal.docentes;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import sigefirrhh.personal.expediente.TrayectoriaBeanBusiness;

public class TrayectoriaDocenteNoGenBeanBusiness extends TrayectoriaBeanBusiness
  implements Serializable
{
  public Collection findByCodCausaDocente(String codCausaDocente, long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCausaDocente == pCodCausaDocente &&  personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(TrayectoriaDocente.class, filter);

    query.declareParameters("String pCodCausaDocente, long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pCodCausaDocente", codCausaDocente);
    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaMovimiento descending");

    Collection colTrayectoriaDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrayectoriaDocente);

    return colTrayectoriaDocente;
  }
}