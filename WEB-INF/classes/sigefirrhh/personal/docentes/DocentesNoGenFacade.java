package sigefirrhh.personal.docentes;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import sigefirrhh.personal.docente.DocentesFacade;

public class DocentesNoGenFacade extends DocentesFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DocentesNoGenBusiness docentesNoGenBusiness = new DocentesNoGenBusiness();

  public int findLastNumeroTrayectoria()
    throws Exception
  {
    return this.docentesNoGenBusiness.findLastNumeroTrayectoria();
  }

  public Collection findTrayectoriaDocenteByCodCausaDocente(String codCausaDocente, long idPersonal) throws Exception {
    try {
      this.txn.open();
      return this.docentesNoGenBusiness.findTrayectoriaDocenteByCodCausaDocente(codCausaDocente, idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}