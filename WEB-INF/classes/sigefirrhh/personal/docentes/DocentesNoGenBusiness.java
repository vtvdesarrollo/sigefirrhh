package sigefirrhh.personal.docentes;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;

public class DocentesNoGenBusiness extends DocentesBusiness
  implements Serializable
{
  private TrayectoriaDocenteNoGenBeanBusiness trayectoriaDocenteNoGenBeanBusiness = new TrayectoriaDocenteNoGenBeanBusiness();

  public int findLastNumeroTrayectoria()
    throws Exception
  {
    Connection connection = Resource.getConnection();
    connection = Resource.getConnection();
    connection.setAutoCommit(true);
    StringBuffer sql = new StringBuffer();

    ResultSet rsRegistro = null;
    PreparedStatement stRegistro = null;

    sql.append("select max(numero_movimiento) as numero ");
    sql.append(" from trayectoriadocente");

    stRegistro = connection.prepareStatement(
      sql.toString(), 
      1003, 
      1007);

    rsRegistro = stRegistro.executeQuery();
    if (rsRegistro.next()) {
      return rsRegistro.getInt("numero");
    }
    return 0;
  }

  public Collection findTrayectoriaDocenteByCodCausaDocente(String codCausaDocente, long idPersonal)
    throws Exception
  {
    return this.trayectoriaDocenteNoGenBeanBusiness.findByCodCausaDocente(codCausaDocente, idPersonal);
  }
}