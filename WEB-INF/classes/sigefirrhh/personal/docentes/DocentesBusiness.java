package sigefirrhh.personal.docentes;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class DocentesBusiness extends AbstractBusiness
  implements Serializable
{
  private TrayectoriaDocenteBeanBusiness trayectoriaDocenteBeanBusiness = new TrayectoriaDocenteBeanBusiness();

  public void addTrayectoriaDocente(TrayectoriaDocente trayectoriaDocente)
    throws Exception
  {
    this.trayectoriaDocenteBeanBusiness.addTrayectoriaDocente(trayectoriaDocente);
  }

  public void updateTrayectoriaDocente(TrayectoriaDocente trayectoriaDocente) throws Exception {
    this.trayectoriaDocenteBeanBusiness.updateTrayectoriaDocente(trayectoriaDocente);
  }

  public void deleteTrayectoriaDocente(TrayectoriaDocente trayectoriaDocente) throws Exception {
    this.trayectoriaDocenteBeanBusiness.deleteTrayectoriaDocente(trayectoriaDocente);
  }

  public TrayectoriaDocente findTrayectoriaDocenteById(long trayectoriaDocenteId) throws Exception {
    return this.trayectoriaDocenteBeanBusiness.findTrayectoriaDocenteById(trayectoriaDocenteId);
  }

  public Collection findAllTrayectoriaDocente() throws Exception {
    return this.trayectoriaDocenteBeanBusiness.findTrayectoriaDocenteAll();
  }

  public Collection findTrayectoriaDocenteByCausaDocente(long idCausaDocente, long idOrganismo)
    throws Exception
  {
    return this.trayectoriaDocenteBeanBusiness.findByCausaDocente(idCausaDocente, idOrganismo);
  }

  public Collection findTrayectoriaDocenteByCodCausaDocente(String codCausaDocente, long idOrganismo)
    throws Exception
  {
    return this.trayectoriaDocenteBeanBusiness.findByCodCausaDocente(codCausaDocente, idOrganismo);
  }

  public Collection findTrayectoriaDocenteByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    return this.trayectoriaDocenteBeanBusiness.findByPersonal(idPersonal, idOrganismo);
  }

  public Collection findTrayectoriaDocenteByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.trayectoriaDocenteBeanBusiness.findByOrganismo(idOrganismo);
  }
}