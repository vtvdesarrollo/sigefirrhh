package sigefirrhh.personal.docentes;

import java.io.Serializable;

public class ConceptoDocentePK
  implements Serializable
{
  public long idConceptoDocente;

  public ConceptoDocentePK()
  {
  }

  public ConceptoDocentePK(long idConceptoDocente)
  {
    this.idConceptoDocente = idConceptoDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoDocentePK thatPK)
  {
    return 
      this.idConceptoDocente == thatPK.idConceptoDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoDocente);
  }
}