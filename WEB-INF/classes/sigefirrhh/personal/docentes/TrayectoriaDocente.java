package sigefirrhh.personal.docentes;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.docente.CausaDocente;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.personal.expediente.Personal;

public class TrayectoriaDocente
  implements Serializable, PersistenceCapable
{
  private long idTrayectoriaDocente;
  private int numeroMovimiento;
  private Date fechaMovimiento;
  private int anio;
  private int mes;
  private String estatus;
  private String codCargo;
  private String nombreCargo;
  private String codCargoAnterior;
  private String nombreCargoAnterior;
  private String nombreDespendencia;
  private String codDependencia;
  private String nombreDespendenciaAnterior;
  private String codDependenciaAnterior;
  private double horas;
  private CausaDocente causaDocente;
  private String codCausaDocente;
  private String usuarioEjecuta;
  private String usuarioAprueba;
  private String observaciones;
  private Personal personal;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "causaDocente", "codCargo", "codCargoAnterior", "codCausaDocente", "codDependencia", "codDependenciaAnterior", "estatus", "fechaMovimiento", "horas", "idTrayectoriaDocente", "mes", "nombreCargo", "nombreCargoAnterior", "nombreDespendencia", "nombreDespendenciaAnterior", "numeroMovimiento", "observaciones", "organismo", "personal", "usuarioAprueba", "usuarioEjecuta" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.docente.CausaDocente"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Double.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 26, 26, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public TrayectoriaDocente()
  {
    jdoSetnumeroMovimiento(this, 0);
  }

  public String toString()
  {
    return jdoGetcausaDocente(this).getCodCausaDocente() + " - " + jdoGetcausaDocente(this).getNombre();
  }
  public CausaDocente getCausaDocente() {
    return jdoGetcausaDocente(this);
  }
  public void setCausaDocente(CausaDocente causaDocente) {
    jdoSetcausaDocente(this, causaDocente);
  }
  public String getCodCargo() {
    return jdoGetcodCargo(this);
  }
  public void setCodCargo(String codCargo) {
    jdoSetcodCargo(this, codCargo);
  }
  public String getCodCausaDocente() {
    return jdoGetcodCausaDocente(this);
  }
  public void setCodCausaDocente(String codCausaDocente) {
    jdoSetcodCausaDocente(this, codCausaDocente);
  }
  public String getEstatus() {
    return jdoGetestatus(this);
  }
  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }
  public double getHoras() {
    return jdoGethoras(this);
  }
  public void setHoras(double horas) {
    jdoSethoras(this, horas);
  }
  public String getNombreCargo() {
    return jdoGetnombreCargo(this);
  }
  public void setNombreCargo(String nombreCargo) {
    jdoSetnombreCargo(this, nombreCargo);
  }
  public int getNumeroMovimiento() {
    return jdoGetnumeroMovimiento(this);
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    jdoSetnumeroMovimiento(this, numeroMovimiento);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }
  public String getUsuarioAprueba() {
    return jdoGetusuarioAprueba(this);
  }
  public void setUsuarioAprueba(String usuarioAprueba) {
    jdoSetusuarioAprueba(this, usuarioAprueba);
  }
  public String getUsuarioEjecuta() {
    return jdoGetusuarioEjecuta(this);
  }
  public void setUsuarioEjecuta(String usuarioEjecuta) {
    jdoSetusuarioEjecuta(this, usuarioEjecuta);
  }
  public String getCodDependencia() {
    return jdoGetcodDependencia(this);
  }
  public void setCodDependencia(String codDependencia) {
    jdoSetcodDependencia(this, codDependencia);
  }
  public String getNombreDespendencia() {
    return jdoGetnombreDespendencia(this);
  }
  public void setNombreDespendencia(String nombreDespendencia) {
    jdoSetnombreDespendencia(this, nombreDespendencia);
  }
  public Date getFechaMovimiento() {
    return jdoGetfechaMovimiento(this);
  }
  public void setFechaMovimiento(Date fechaMovimiento) {
    jdoSetfechaMovimiento(this, fechaMovimiento);
  }
  public String getCodCargoAnterior() {
    return jdoGetcodCargoAnterior(this);
  }
  public void setCodCargoAnterior(String codCargoAnterior) {
    jdoSetcodCargoAnterior(this, codCargoAnterior);
  }
  public String getCodDependenciaAnterior() {
    return jdoGetcodDependenciaAnterior(this);
  }
  public void setCodDependenciaAnterior(String codDependenciaAnterior) {
    jdoSetcodDependenciaAnterior(this, codDependenciaAnterior);
  }
  public String getNombreCargoAnterior() {
    return jdoGetnombreCargoAnterior(this);
  }
  public void setNombreCargoAnterior(String nombreCargoAnterior) {
    jdoSetnombreCargoAnterior(this, nombreCargoAnterior);
  }
  public String getNombreDespendenciaAnterior() {
    return jdoGetnombreDespendenciaAnterior(this);
  }
  public void setNombreDespendenciaAnterior(String nombreDespendenciaAnterior) {
    jdoSetnombreDespendenciaAnterior(this, nombreDespendenciaAnterior);
  }
  public long getIdTrayectoriaDocente() {
    return jdoGetidTrayectoriaDocente(this);
  }
  public void setIdTrayectoriaDocente(long idTrayectoriaDocente) {
    jdoSetidTrayectoriaDocente(this, idTrayectoriaDocente);
  }
  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }
  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 22;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.docentes.TrayectoriaDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TrayectoriaDocente());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TrayectoriaDocente localTrayectoriaDocente = new TrayectoriaDocente();
    localTrayectoriaDocente.jdoFlags = 1;
    localTrayectoriaDocente.jdoStateManager = paramStateManager;
    return localTrayectoriaDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TrayectoriaDocente localTrayectoriaDocente = new TrayectoriaDocente();
    localTrayectoriaDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTrayectoriaDocente.jdoFlags = 1;
    localTrayectoriaDocente.jdoStateManager = paramStateManager;
    return localTrayectoriaDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.causaDocente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargoAnterior);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCausaDocente);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codDependencia);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codDependenciaAnterior);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaMovimiento);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horas);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTrayectoriaDocente);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCargo);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCargoAnterior);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreDespendencia);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreDespendenciaAnterior);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroMovimiento);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuarioAprueba);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuarioEjecuta);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaDocente = ((CausaDocente)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargoAnterior = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCausaDocente = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codDependenciaAnterior = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaMovimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horas = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTrayectoriaDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCargoAnterior = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreDespendencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreDespendenciaAnterior = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMovimiento = localStateManager.replacingIntField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuarioAprueba = localStateManager.replacingStringField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuarioEjecuta = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TrayectoriaDocente paramTrayectoriaDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramTrayectoriaDocente.anio;
      return;
    case 1:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.causaDocente = paramTrayectoriaDocente.causaDocente;
      return;
    case 2:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.codCargo = paramTrayectoriaDocente.codCargo;
      return;
    case 3:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.codCargoAnterior = paramTrayectoriaDocente.codCargoAnterior;
      return;
    case 4:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.codCausaDocente = paramTrayectoriaDocente.codCausaDocente;
      return;
    case 5:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.codDependencia = paramTrayectoriaDocente.codDependencia;
      return;
    case 6:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.codDependenciaAnterior = paramTrayectoriaDocente.codDependenciaAnterior;
      return;
    case 7:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramTrayectoriaDocente.estatus;
      return;
    case 8:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.fechaMovimiento = paramTrayectoriaDocente.fechaMovimiento;
      return;
    case 9:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.horas = paramTrayectoriaDocente.horas;
      return;
    case 10:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idTrayectoriaDocente = paramTrayectoriaDocente.idTrayectoriaDocente;
      return;
    case 11:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramTrayectoriaDocente.mes;
      return;
    case 12:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCargo = paramTrayectoriaDocente.nombreCargo;
      return;
    case 13:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCargoAnterior = paramTrayectoriaDocente.nombreCargoAnterior;
      return;
    case 14:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombreDespendencia = paramTrayectoriaDocente.nombreDespendencia;
      return;
    case 15:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombreDespendenciaAnterior = paramTrayectoriaDocente.nombreDespendenciaAnterior;
      return;
    case 16:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMovimiento = paramTrayectoriaDocente.numeroMovimiento;
      return;
    case 17:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramTrayectoriaDocente.observaciones;
      return;
    case 18:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramTrayectoriaDocente.organismo;
      return;
    case 19:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramTrayectoriaDocente.personal;
      return;
    case 20:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.usuarioAprueba = paramTrayectoriaDocente.usuarioAprueba;
      return;
    case 21:
      if (paramTrayectoriaDocente == null)
        throw new IllegalArgumentException("arg1");
      this.usuarioEjecuta = paramTrayectoriaDocente.usuarioEjecuta;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TrayectoriaDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TrayectoriaDocente localTrayectoriaDocente = (TrayectoriaDocente)paramObject;
    if (localTrayectoriaDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTrayectoriaDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TrayectoriaDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TrayectoriaDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrayectoriaDocentePK))
      throw new IllegalArgumentException("arg1");
    TrayectoriaDocentePK localTrayectoriaDocentePK = (TrayectoriaDocentePK)paramObject;
    localTrayectoriaDocentePK.idTrayectoriaDocente = this.idTrayectoriaDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrayectoriaDocentePK))
      throw new IllegalArgumentException("arg1");
    TrayectoriaDocentePK localTrayectoriaDocentePK = (TrayectoriaDocentePK)paramObject;
    this.idTrayectoriaDocente = localTrayectoriaDocentePK.idTrayectoriaDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrayectoriaDocentePK))
      throw new IllegalArgumentException("arg2");
    TrayectoriaDocentePK localTrayectoriaDocentePK = (TrayectoriaDocentePK)paramObject;
    localTrayectoriaDocentePK.idTrayectoriaDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 10);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrayectoriaDocentePK))
      throw new IllegalArgumentException("arg2");
    TrayectoriaDocentePK localTrayectoriaDocentePK = (TrayectoriaDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 10, localTrayectoriaDocentePK.idTrayectoriaDocente);
  }

  private static final int jdoGetanio(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.anio;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.anio;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 0))
      return paramTrayectoriaDocente.anio;
    return localStateManager.getIntField(paramTrayectoriaDocente, jdoInheritedFieldCount + 0, paramTrayectoriaDocente.anio);
  }

  private static final void jdoSetanio(TrayectoriaDocente paramTrayectoriaDocente, int paramInt)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrayectoriaDocente, jdoInheritedFieldCount + 0, paramTrayectoriaDocente.anio, paramInt);
  }

  private static final CausaDocente jdoGetcausaDocente(TrayectoriaDocente paramTrayectoriaDocente)
  {
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.causaDocente;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 1))
      return paramTrayectoriaDocente.causaDocente;
    return (CausaDocente)localStateManager.getObjectField(paramTrayectoriaDocente, jdoInheritedFieldCount + 1, paramTrayectoriaDocente.causaDocente);
  }

  private static final void jdoSetcausaDocente(TrayectoriaDocente paramTrayectoriaDocente, CausaDocente paramCausaDocente)
  {
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.causaDocente = paramCausaDocente;
      return;
    }
    localStateManager.setObjectField(paramTrayectoriaDocente, jdoInheritedFieldCount + 1, paramTrayectoriaDocente.causaDocente, paramCausaDocente);
  }

  private static final String jdoGetcodCargo(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.codCargo;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.codCargo;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 2))
      return paramTrayectoriaDocente.codCargo;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 2, paramTrayectoriaDocente.codCargo);
  }

  private static final void jdoSetcodCargo(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.codCargo = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.codCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 2, paramTrayectoriaDocente.codCargo, paramString);
  }

  private static final String jdoGetcodCargoAnterior(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.codCargoAnterior;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.codCargoAnterior;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 3))
      return paramTrayectoriaDocente.codCargoAnterior;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 3, paramTrayectoriaDocente.codCargoAnterior);
  }

  private static final void jdoSetcodCargoAnterior(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.codCargoAnterior = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.codCargoAnterior = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 3, paramTrayectoriaDocente.codCargoAnterior, paramString);
  }

  private static final String jdoGetcodCausaDocente(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.codCausaDocente;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.codCausaDocente;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 4))
      return paramTrayectoriaDocente.codCausaDocente;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 4, paramTrayectoriaDocente.codCausaDocente);
  }

  private static final void jdoSetcodCausaDocente(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.codCausaDocente = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.codCausaDocente = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 4, paramTrayectoriaDocente.codCausaDocente, paramString);
  }

  private static final String jdoGetcodDependencia(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.codDependencia;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.codDependencia;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 5))
      return paramTrayectoriaDocente.codDependencia;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 5, paramTrayectoriaDocente.codDependencia);
  }

  private static final void jdoSetcodDependencia(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.codDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.codDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 5, paramTrayectoriaDocente.codDependencia, paramString);
  }

  private static final String jdoGetcodDependenciaAnterior(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.codDependenciaAnterior;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.codDependenciaAnterior;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 6))
      return paramTrayectoriaDocente.codDependenciaAnterior;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 6, paramTrayectoriaDocente.codDependenciaAnterior);
  }

  private static final void jdoSetcodDependenciaAnterior(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.codDependenciaAnterior = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.codDependenciaAnterior = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 6, paramTrayectoriaDocente.codDependenciaAnterior, paramString);
  }

  private static final String jdoGetestatus(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.estatus;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.estatus;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 7))
      return paramTrayectoriaDocente.estatus;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 7, paramTrayectoriaDocente.estatus);
  }

  private static final void jdoSetestatus(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 7, paramTrayectoriaDocente.estatus, paramString);
  }

  private static final Date jdoGetfechaMovimiento(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.fechaMovimiento;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.fechaMovimiento;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 8))
      return paramTrayectoriaDocente.fechaMovimiento;
    return (Date)localStateManager.getObjectField(paramTrayectoriaDocente, jdoInheritedFieldCount + 8, paramTrayectoriaDocente.fechaMovimiento);
  }

  private static final void jdoSetfechaMovimiento(TrayectoriaDocente paramTrayectoriaDocente, Date paramDate)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.fechaMovimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.fechaMovimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrayectoriaDocente, jdoInheritedFieldCount + 8, paramTrayectoriaDocente.fechaMovimiento, paramDate);
  }

  private static final double jdoGethoras(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.horas;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.horas;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 9))
      return paramTrayectoriaDocente.horas;
    return localStateManager.getDoubleField(paramTrayectoriaDocente, jdoInheritedFieldCount + 9, paramTrayectoriaDocente.horas);
  }

  private static final void jdoSethoras(TrayectoriaDocente paramTrayectoriaDocente, double paramDouble)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.horas = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.horas = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoriaDocente, jdoInheritedFieldCount + 9, paramTrayectoriaDocente.horas, paramDouble);
  }

  private static final long jdoGetidTrayectoriaDocente(TrayectoriaDocente paramTrayectoriaDocente)
  {
    return paramTrayectoriaDocente.idTrayectoriaDocente;
  }

  private static final void jdoSetidTrayectoriaDocente(TrayectoriaDocente paramTrayectoriaDocente, long paramLong)
  {
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.idTrayectoriaDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramTrayectoriaDocente, jdoInheritedFieldCount + 10, paramTrayectoriaDocente.idTrayectoriaDocente, paramLong);
  }

  private static final int jdoGetmes(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.mes;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.mes;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 11))
      return paramTrayectoriaDocente.mes;
    return localStateManager.getIntField(paramTrayectoriaDocente, jdoInheritedFieldCount + 11, paramTrayectoriaDocente.mes);
  }

  private static final void jdoSetmes(TrayectoriaDocente paramTrayectoriaDocente, int paramInt)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrayectoriaDocente, jdoInheritedFieldCount + 11, paramTrayectoriaDocente.mes, paramInt);
  }

  private static final String jdoGetnombreCargo(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.nombreCargo;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.nombreCargo;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 12))
      return paramTrayectoriaDocente.nombreCargo;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 12, paramTrayectoriaDocente.nombreCargo);
  }

  private static final void jdoSetnombreCargo(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.nombreCargo = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.nombreCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 12, paramTrayectoriaDocente.nombreCargo, paramString);
  }

  private static final String jdoGetnombreCargoAnterior(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.nombreCargoAnterior;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.nombreCargoAnterior;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 13))
      return paramTrayectoriaDocente.nombreCargoAnterior;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 13, paramTrayectoriaDocente.nombreCargoAnterior);
  }

  private static final void jdoSetnombreCargoAnterior(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.nombreCargoAnterior = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.nombreCargoAnterior = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 13, paramTrayectoriaDocente.nombreCargoAnterior, paramString);
  }

  private static final String jdoGetnombreDespendencia(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.nombreDespendencia;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.nombreDespendencia;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 14))
      return paramTrayectoriaDocente.nombreDespendencia;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 14, paramTrayectoriaDocente.nombreDespendencia);
  }

  private static final void jdoSetnombreDespendencia(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.nombreDespendencia = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.nombreDespendencia = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 14, paramTrayectoriaDocente.nombreDespendencia, paramString);
  }

  private static final String jdoGetnombreDespendenciaAnterior(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.nombreDespendenciaAnterior;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.nombreDespendenciaAnterior;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 15))
      return paramTrayectoriaDocente.nombreDespendenciaAnterior;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 15, paramTrayectoriaDocente.nombreDespendenciaAnterior);
  }

  private static final void jdoSetnombreDespendenciaAnterior(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.nombreDespendenciaAnterior = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.nombreDespendenciaAnterior = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 15, paramTrayectoriaDocente.nombreDespendenciaAnterior, paramString);
  }

  private static final int jdoGetnumeroMovimiento(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.numeroMovimiento;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.numeroMovimiento;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 16))
      return paramTrayectoriaDocente.numeroMovimiento;
    return localStateManager.getIntField(paramTrayectoriaDocente, jdoInheritedFieldCount + 16, paramTrayectoriaDocente.numeroMovimiento);
  }

  private static final void jdoSetnumeroMovimiento(TrayectoriaDocente paramTrayectoriaDocente, int paramInt)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.numeroMovimiento = paramInt;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.numeroMovimiento = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrayectoriaDocente, jdoInheritedFieldCount + 16, paramTrayectoriaDocente.numeroMovimiento, paramInt);
  }

  private static final String jdoGetobservaciones(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.observaciones;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.observaciones;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 17))
      return paramTrayectoriaDocente.observaciones;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 17, paramTrayectoriaDocente.observaciones);
  }

  private static final void jdoSetobservaciones(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 17, paramTrayectoriaDocente.observaciones, paramString);
  }

  private static final Organismo jdoGetorganismo(TrayectoriaDocente paramTrayectoriaDocente)
  {
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.organismo;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 18))
      return paramTrayectoriaDocente.organismo;
    return (Organismo)localStateManager.getObjectField(paramTrayectoriaDocente, jdoInheritedFieldCount + 18, paramTrayectoriaDocente.organismo);
  }

  private static final void jdoSetorganismo(TrayectoriaDocente paramTrayectoriaDocente, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramTrayectoriaDocente, jdoInheritedFieldCount + 18, paramTrayectoriaDocente.organismo, paramOrganismo);
  }

  private static final Personal jdoGetpersonal(TrayectoriaDocente paramTrayectoriaDocente)
  {
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.personal;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 19))
      return paramTrayectoriaDocente.personal;
    return (Personal)localStateManager.getObjectField(paramTrayectoriaDocente, jdoInheritedFieldCount + 19, paramTrayectoriaDocente.personal);
  }

  private static final void jdoSetpersonal(TrayectoriaDocente paramTrayectoriaDocente, Personal paramPersonal)
  {
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramTrayectoriaDocente, jdoInheritedFieldCount + 19, paramTrayectoriaDocente.personal, paramPersonal);
  }

  private static final String jdoGetusuarioAprueba(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.usuarioAprueba;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.usuarioAprueba;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 20))
      return paramTrayectoriaDocente.usuarioAprueba;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 20, paramTrayectoriaDocente.usuarioAprueba);
  }

  private static final void jdoSetusuarioAprueba(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.usuarioAprueba = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.usuarioAprueba = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 20, paramTrayectoriaDocente.usuarioAprueba, paramString);
  }

  private static final String jdoGetusuarioEjecuta(TrayectoriaDocente paramTrayectoriaDocente)
  {
    if (paramTrayectoriaDocente.jdoFlags <= 0)
      return paramTrayectoriaDocente.usuarioEjecuta;
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoriaDocente.usuarioEjecuta;
    if (localStateManager.isLoaded(paramTrayectoriaDocente, jdoInheritedFieldCount + 21))
      return paramTrayectoriaDocente.usuarioEjecuta;
    return localStateManager.getStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 21, paramTrayectoriaDocente.usuarioEjecuta);
  }

  private static final void jdoSetusuarioEjecuta(TrayectoriaDocente paramTrayectoriaDocente, String paramString)
  {
    if (paramTrayectoriaDocente.jdoFlags == 0)
    {
      paramTrayectoriaDocente.usuarioEjecuta = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoriaDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoriaDocente.usuarioEjecuta = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoriaDocente, jdoInheritedFieldCount + 21, paramTrayectoriaDocente.usuarioEjecuta, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}