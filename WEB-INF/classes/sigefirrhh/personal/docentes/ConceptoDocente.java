package sigefirrhh.personal.docentes;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;

public class ConceptoDocente
  implements Serializable, PersistenceCapable
{
  private long idConceptoDocente;
  private double unidades;
  private double monto;
  private Date fechaRegistro;
  private String variable;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private long idMovimiento;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "fechaRegistro", "frecuenciaTipoPersonal", "idConceptoDocente", "idMovimiento", "monto", "unidades", "variable" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), Long.TYPE, Long.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 26, 21, 26, 24, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmonto(this));

    return jdoGetconceptoTipoPersonal(this).getConcepto().getCodConcepto() + " - " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + 
      a + " - " + 
      jdoGetfrecuenciaTipoPersonal(this).getFrecuenciaPago().getCodFrecuenciaPago();
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal)
  {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public void setFechaRegistro(Date fechaRegistro)
  {
    jdoSetfechaRegistro(this, fechaRegistro);
  }

  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal()
  {
    return jdoGetfrecuenciaTipoPersonal(this);
  }

  public void setFrecuenciaTipoPersonal(FrecuenciaTipoPersonal frecuenciaTipoPersonal)
  {
    jdoSetfrecuenciaTipoPersonal(this, frecuenciaTipoPersonal);
  }

  public long getIdConceptoDocente()
  {
    return jdoGetidConceptoDocente(this);
  }

  public void setIdConceptoDocente(long idConceptoDocente)
  {
    jdoSetidConceptoDocente(this, idConceptoDocente);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public void setMonto(double monto)
  {
    jdoSetmonto(this, monto);
  }

  public double getUnidades()
  {
    return jdoGetunidades(this);
  }

  public void setUnidades(double unidades)
  {
    jdoSetunidades(this, unidades);
  }

  public String getVariable()
  {
    return jdoGetvariable(this);
  }

  public void setVariable(String variable)
  {
    jdoSetvariable(this, variable);
  }

  public long getIdMovimiento() {
    return jdoGetidMovimiento(this);
  }

  public void setIdMovimiento(long idMovimiento) {
    jdoSetidMovimiento(this, idMovimiento);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.docentes.ConceptoDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoDocente());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoDocente localConceptoDocente = new ConceptoDocente();
    localConceptoDocente.jdoFlags = 1;
    localConceptoDocente.jdoStateManager = paramStateManager;
    return localConceptoDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoDocente localConceptoDocente = new ConceptoDocente();
    localConceptoDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoDocente.jdoFlags = 1;
    localConceptoDocente.jdoStateManager = paramStateManager;
    return localConceptoDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaTipoPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoDocente);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idMovimiento);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.variable);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaTipoPersonal = ((FrecuenciaTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idMovimiento = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.variable = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoDocente paramConceptoDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoDocente.conceptoTipoPersonal;
      return;
    case 1:
      if (paramConceptoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramConceptoDocente.fechaRegistro;
      return;
    case 2:
      if (paramConceptoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaTipoPersonal = paramConceptoDocente.frecuenciaTipoPersonal;
      return;
    case 3:
      if (paramConceptoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoDocente = paramConceptoDocente.idConceptoDocente;
      return;
    case 4:
      if (paramConceptoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idMovimiento = paramConceptoDocente.idMovimiento;
      return;
    case 5:
      if (paramConceptoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramConceptoDocente.monto;
      return;
    case 6:
      if (paramConceptoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramConceptoDocente.unidades;
      return;
    case 7:
      if (paramConceptoDocente == null)
        throw new IllegalArgumentException("arg1");
      this.variable = paramConceptoDocente.variable;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoDocente localConceptoDocente = (ConceptoDocente)paramObject;
    if (localConceptoDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoDocentePK))
      throw new IllegalArgumentException("arg1");
    ConceptoDocentePK localConceptoDocentePK = (ConceptoDocentePK)paramObject;
    localConceptoDocentePK.idConceptoDocente = this.idConceptoDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoDocentePK))
      throw new IllegalArgumentException("arg1");
    ConceptoDocentePK localConceptoDocentePK = (ConceptoDocentePK)paramObject;
    this.idConceptoDocente = localConceptoDocentePK.idConceptoDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoDocentePK))
      throw new IllegalArgumentException("arg2");
    ConceptoDocentePK localConceptoDocentePK = (ConceptoDocentePK)paramObject;
    localConceptoDocentePK.idConceptoDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoDocentePK))
      throw new IllegalArgumentException("arg2");
    ConceptoDocentePK localConceptoDocentePK = (ConceptoDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localConceptoDocentePK.idConceptoDocente);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoDocente paramConceptoDocente)
  {
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDocente.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoDocente, jdoInheritedFieldCount + 0))
      return paramConceptoDocente.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoDocente, jdoInheritedFieldCount + 0, paramConceptoDocente.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoDocente paramConceptoDocente, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDocente.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoDocente, jdoInheritedFieldCount + 0, paramConceptoDocente.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final Date jdoGetfechaRegistro(ConceptoDocente paramConceptoDocente)
  {
    if (paramConceptoDocente.jdoFlags <= 0)
      return paramConceptoDocente.fechaRegistro;
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDocente.fechaRegistro;
    if (localStateManager.isLoaded(paramConceptoDocente, jdoInheritedFieldCount + 1))
      return paramConceptoDocente.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramConceptoDocente, jdoInheritedFieldCount + 1, paramConceptoDocente.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(ConceptoDocente paramConceptoDocente, Date paramDate)
  {
    if (paramConceptoDocente.jdoFlags == 0)
    {
      paramConceptoDocente.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDocente.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoDocente, jdoInheritedFieldCount + 1, paramConceptoDocente.fechaRegistro, paramDate);
  }

  private static final FrecuenciaTipoPersonal jdoGetfrecuenciaTipoPersonal(ConceptoDocente paramConceptoDocente)
  {
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDocente.frecuenciaTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoDocente, jdoInheritedFieldCount + 2))
      return paramConceptoDocente.frecuenciaTipoPersonal;
    return (FrecuenciaTipoPersonal)localStateManager.getObjectField(paramConceptoDocente, jdoInheritedFieldCount + 2, paramConceptoDocente.frecuenciaTipoPersonal);
  }

  private static final void jdoSetfrecuenciaTipoPersonal(ConceptoDocente paramConceptoDocente, FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDocente.frecuenciaTipoPersonal = paramFrecuenciaTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoDocente, jdoInheritedFieldCount + 2, paramConceptoDocente.frecuenciaTipoPersonal, paramFrecuenciaTipoPersonal);
  }

  private static final long jdoGetidConceptoDocente(ConceptoDocente paramConceptoDocente)
  {
    return paramConceptoDocente.idConceptoDocente;
  }

  private static final void jdoSetidConceptoDocente(ConceptoDocente paramConceptoDocente, long paramLong)
  {
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDocente.idConceptoDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoDocente, jdoInheritedFieldCount + 3, paramConceptoDocente.idConceptoDocente, paramLong);
  }

  private static final long jdoGetidMovimiento(ConceptoDocente paramConceptoDocente)
  {
    if (paramConceptoDocente.jdoFlags <= 0)
      return paramConceptoDocente.idMovimiento;
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDocente.idMovimiento;
    if (localStateManager.isLoaded(paramConceptoDocente, jdoInheritedFieldCount + 4))
      return paramConceptoDocente.idMovimiento;
    return localStateManager.getLongField(paramConceptoDocente, jdoInheritedFieldCount + 4, paramConceptoDocente.idMovimiento);
  }

  private static final void jdoSetidMovimiento(ConceptoDocente paramConceptoDocente, long paramLong)
  {
    if (paramConceptoDocente.jdoFlags == 0)
    {
      paramConceptoDocente.idMovimiento = paramLong;
      return;
    }
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDocente.idMovimiento = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoDocente, jdoInheritedFieldCount + 4, paramConceptoDocente.idMovimiento, paramLong);
  }

  private static final double jdoGetmonto(ConceptoDocente paramConceptoDocente)
  {
    if (paramConceptoDocente.jdoFlags <= 0)
      return paramConceptoDocente.monto;
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDocente.monto;
    if (localStateManager.isLoaded(paramConceptoDocente, jdoInheritedFieldCount + 5))
      return paramConceptoDocente.monto;
    return localStateManager.getDoubleField(paramConceptoDocente, jdoInheritedFieldCount + 5, paramConceptoDocente.monto);
  }

  private static final void jdoSetmonto(ConceptoDocente paramConceptoDocente, double paramDouble)
  {
    if (paramConceptoDocente.jdoFlags == 0)
    {
      paramConceptoDocente.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDocente.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoDocente, jdoInheritedFieldCount + 5, paramConceptoDocente.monto, paramDouble);
  }

  private static final double jdoGetunidades(ConceptoDocente paramConceptoDocente)
  {
    if (paramConceptoDocente.jdoFlags <= 0)
      return paramConceptoDocente.unidades;
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDocente.unidades;
    if (localStateManager.isLoaded(paramConceptoDocente, jdoInheritedFieldCount + 6))
      return paramConceptoDocente.unidades;
    return localStateManager.getDoubleField(paramConceptoDocente, jdoInheritedFieldCount + 6, paramConceptoDocente.unidades);
  }

  private static final void jdoSetunidades(ConceptoDocente paramConceptoDocente, double paramDouble)
  {
    if (paramConceptoDocente.jdoFlags == 0)
    {
      paramConceptoDocente.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDocente.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoDocente, jdoInheritedFieldCount + 6, paramConceptoDocente.unidades, paramDouble);
  }

  private static final String jdoGetvariable(ConceptoDocente paramConceptoDocente)
  {
    if (paramConceptoDocente.jdoFlags <= 0)
      return paramConceptoDocente.variable;
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDocente.variable;
    if (localStateManager.isLoaded(paramConceptoDocente, jdoInheritedFieldCount + 7))
      return paramConceptoDocente.variable;
    return localStateManager.getStringField(paramConceptoDocente, jdoInheritedFieldCount + 7, paramConceptoDocente.variable);
  }

  private static final void jdoSetvariable(ConceptoDocente paramConceptoDocente, String paramString)
  {
    if (paramConceptoDocente.jdoFlags == 0)
    {
      paramConceptoDocente.variable = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDocente.variable = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoDocente, jdoInheritedFieldCount + 7, paramConceptoDocente.variable, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}