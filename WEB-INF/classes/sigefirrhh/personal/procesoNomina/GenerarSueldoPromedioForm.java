package sigefirrhh.personal.procesoNomina;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class GenerarSueldoPromedioForm
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarSueldoPromedioForm.class.getName());
  private String selectGrupoNomina;
  private Collection listGrupoNomina;
  private GrupoNomina grupoNomina;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private boolean show;
  private boolean auxShow;
  private Integer semanaMes;
  private Integer lunesPrQuincena;
  private Integer lunesSeQuincena;
  private Boolean tieneSemana5;
  private Integer mesSemanal;
  private String periodicidad;
  private Calendar inicioAux;
  private Calendar finAux;
  private Integer semanaAnio;
  private Integer numeroSemanasMes;

  public GenerarSueldoPromedioForm()
  {
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void changeGrupoNomina(ValueChangeEvent event)
  {
    long idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.grupoNomina = this.definicionesFacade.findGrupoNominaById(idGrupoNomina);

      Collection colPeriodo = this.procesoNominaNoGenFacade.findFechaProximaNomina(idGrupoNomina);
      Iterator iteratorPeriodo = colPeriodo.iterator();
      if (iteratorPeriodo.hasNext()) {
        this.inicioAux = ((Calendar)iteratorPeriodo.next());
        this.finAux = ((Calendar)iteratorPeriodo.next());
        this.lunesPrQuincena = ((Integer)iteratorPeriodo.next());
        this.lunesSeQuincena = ((Integer)iteratorPeriodo.next());
        this.tieneSemana5 = ((Boolean)iteratorPeriodo.next());
        this.semanaMes = ((Integer)iteratorPeriodo.next());
        this.semanaAnio = ((Integer)iteratorPeriodo.next());
        this.periodicidad = ((String)iteratorPeriodo.next());
        this.numeroSemanasMes = ((Integer)iteratorPeriodo.next());
        this.mesSemanal = ((Integer)iteratorPeriodo.next());
        this.show = true;
      }

      this.auxShow = true;
    } catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listGrupoNomina = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if (this.grupoNomina.getPeriodicidad().equals("S"))
        this.procesoNominaNoGenFacade.generarSueldoPromedioSemanal(this.login.getIdOrganismo(), this.grupoNomina.getIdGrupoNomina(), this.periodicidad, "S", this.inicioAux.getTime(), this.finAux.getTime(), this.semanaMes, this.tieneSemana5, this.numeroSemanasMes);
      else {
        this.procesoNominaNoGenFacade.generarSueldoPromedioMensual(this.login.getIdOrganismo(), this.grupoNomina.getIdGrupoNomina(), this.periodicidad, "S", this.inicioAux.getTime(), this.finAux.getTime(), this.semanaMes, this.tieneSemana5, this.numeroSemanasMes);
      }
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.grupoNomina);

      context.addMessage("success_add", new FacesMessage("Se generó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getSelectGrupoNomina()
  {
    return this.selectGrupoNomina;
  }

  public void setSelectGrupoNomina(String string)
  {
    this.selectGrupoNomina = string;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }
}