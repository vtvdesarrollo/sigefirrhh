package sigefirrhh.personal.procesoNomina;

import java.util.Collection;
import java.util.Date;
import sigefirrhh.base.definiciones.GrupoNomina;

public class ProcesoNominaNoGenBusiness extends ProcesoNominaBusiness
{
  private SeguridadOrdinariaNoGenBeanBusiness seguridadOrdinariaNoGenBeanBusiness = new SeguridadOrdinariaNoGenBeanBusiness();

  private GenerarPrenominaBeanBusiness generarPrenominaBeanBusiness = new GenerarPrenominaBeanBusiness();

  private GenerarNominaBeanBusiness generarNominaBeanBusiness = new GenerarNominaBeanBusiness();

  private GenerarPrenominaEspecialBeanBusiness generarPrenominaEspecialBeanBusiness = new GenerarPrenominaEspecialBeanBusiness();

  private GenerarNominaEspecialBeanBusiness generarNominaEspecialBeanBusiness = new GenerarNominaEspecialBeanBusiness();

  private NominaEspecialNoGenBeanBusiness nominaEspecialNoGenBeanBusiness = new NominaEspecialNoGenBeanBusiness();

  private CalcularSueldosPromedioBeanBusiness calcularSueldosPromedioBeanBusiness = new CalcularSueldosPromedioBeanBusiness();

  private ReversarNominaBeanBusiness reversarNominaBeanBusiness = new ReversarNominaBeanBusiness();

  private AporteBeanBusiness aporteBeanBusiness = new AporteBeanBusiness();

  public void generarSueldoPromedioMensual(long idOrganismo, long idGrupoNomina, String periodicidad, String recalculo, Date fechaInicio, Date fechaFin, Integer semanaMes, Boolean tieneSemana5, Integer numeroSemanasMes)
    throws Exception
  {
    this.calcularSueldosPromedioBeanBusiness.calcularMensual(idOrganismo, 
      idGrupoNomina, 
      periodicidad, 
      recalculo, 
      fechaInicio, 
      fechaFin, 
      semanaMes, 
      tieneSemana5, 
      numeroSemanasMes);
  }

  public void generarSueldoPromedioSemanal(long idOrganismo, long idGrupoNomina, String periodicidad, String recalculo, Date fechaInicio, Date fechaFin, Integer semanaMes, Boolean tieneSemana5, Integer numeroSemanasMes)
    throws Exception
  {
    this.calcularSueldosPromedioBeanBusiness.calcularSemanal(idOrganismo, 
      idGrupoNomina, 
      periodicidad, 
      recalculo, 
      fechaInicio, 
      fechaFin, 
      semanaMes, 
      tieneSemana5, 
      numeroSemanasMes);
  }

  public Collection findFechaProximaNomina(long idGrupoNomina) throws Exception {
    return this.seguridadOrdinariaNoGenBeanBusiness.findFechaProximaNomina(idGrupoNomina);
  }

  public Collection findFechaUltimaNomina(long idGrupoNomina) throws Exception {
    return this.seguridadOrdinariaNoGenBeanBusiness.findFechaUltimaNomina(idGrupoNomina);
  }

  public boolean generarPrenomina(long idGrupoNomina, String periodicidad, String recalculo, Date fechaInicio, Date fechaFin, long idOrganismo, Integer lunesPrQuincena, Integer lunesSeQuincena, Boolean tieneSemana5, Integer semanaMes, Integer numeroSemanasMes) throws Exception {
    return this.generarPrenominaBeanBusiness.generar(idGrupoNomina, periodicidad, recalculo, fechaInicio, fechaFin, idOrganismo, lunesPrQuincena, lunesSeQuincena, tieneSemana5, semanaMes, numeroSemanasMes);
  }

  public boolean generarNomina(long idGrupoNomina, String periodicidad, String recalculo, Date fechaInicio, Date fechaFin, long idOrganismo, Integer lunesPrQuincena, Integer lunesSeQuincena, Boolean tieneSemana5, Integer semanaMes, String usuario, Integer semanaAnio, Integer numeroSemanasMes, Integer mesSemanal, Integer anioSemanal) throws Exception
  {
    return this.generarNominaBeanBusiness.generar(idGrupoNomina, periodicidad, recalculo, fechaInicio, fechaFin, idOrganismo, lunesPrQuincena, lunesSeQuincena, tieneSemana5, semanaMes, usuario, semanaAnio, numeroSemanasMes, mesSemanal, anioSemanal);
  }

  public boolean generarPrenominaEspecial(long idGrupoNomina, Date fechaInicio, Date fechaFin, long idOrganismo, long numeroNomina, long idFrecuenciaPago, long idNominaEspecial, String usuario, String estatus) throws Exception
  {
    return this.generarPrenominaEspecialBeanBusiness.generar(idGrupoNomina, fechaInicio, fechaFin, idOrganismo, numeroNomina, idFrecuenciaPago, idNominaEspecial, usuario, estatus);
  }

  public boolean generarNominaEspecial(long idGrupoNomina, Date fechaInicio, Date fechaFin, long idOrganismo, long numeroNomina, long idFrecuenciaPago, long idNominaEspecial, String usuario, String estatus, int anio, int mes) throws Exception {
    return this.generarNominaEspecialBeanBusiness.generar(idGrupoNomina, fechaInicio, fechaFin, idOrganismo, numeroNomina, idFrecuenciaPago, idNominaEspecial, usuario, estatus, anio, mes);
  }

  public int findLastNumeroNominaEspecial() throws Exception {
    return this.nominaEspecialNoGenBeanBusiness.findLastNumeroNomina();
  }

  public int findLastNumeroNominaEspecialByGrupoNominaAndAnio(long idGrupoNomina, int anio) throws Exception {
    return this.nominaEspecialNoGenBeanBusiness.findLastNumeroNominaEspecialByGrupoNominaAndAnio(idGrupoNomina, anio);
  }

  public Collection findNominaEspecialByGrupoNominaAndEstatus(long idGrupoNomina, String estatus) throws Exception {
    return this.nominaEspecialNoGenBeanBusiness.findByGrupoNominaAndEstatus(idGrupoNomina, estatus);
  }

  public Collection findNominaEspecialByGrupoNominaAndEstatusAndAnio(long idGrupoNomina, String estatus, int anio) throws Exception {
    return this.nominaEspecialNoGenBeanBusiness.findByGrupoNominaAndEstatusAndAnio(idGrupoNomina, estatus, anio);
  }

  public Collection findNominaEspecialByGrupoNominaAndEstatusAndMesAndAnio(long idGrupoNomina, String estatus, int mes, int anio) throws Exception {
    return this.nominaEspecialNoGenBeanBusiness.findByGrupoNominaAndEstatusAndMesAndAnio(idGrupoNomina, estatus, mes, anio);
  }

  public boolean reversarNomina(long idGrupoNomina, int numeroNomina, Date fechaInicio, String periodicidad, int anio, int mes, int semanaQuincena, boolean nominaEspecial)
    throws Exception
  {
    return this.reversarNominaBeanBusiness.reversar(
      idGrupoNomina, 
      numeroNomina, 
      fechaInicio, 
      periodicidad, 
      anio, 
      mes, 
      semanaQuincena, 
      nominaEspecial);
  }

  public void calcularPromedios(long idTipoPersonal, String periodicidad) throws Exception {
    this.calcularSueldosPromedioBeanBusiness.calcularPromedios(idTipoPersonal, periodicidad);
  }

  public Collection findNominaEspecialByGrupoNominaAndEstatus(long idGrupoNomina, String estatus, String pagada) throws Exception {
    return this.nominaEspecialNoGenBeanBusiness.findByGrupoNominaAndEstatus(idGrupoNomina, estatus, pagada);
  }

  public Collection findSeguridadOrdinariaByGrupoNominaDesc(long idGrupoNomina) throws Exception {
    return this.seguridadOrdinariaNoGenBeanBusiness.findByGrupoNominaDesc(idGrupoNomina);
  }

  public SeguridadOrdinaria findNominaMre(long idGrupoNomina) throws Exception {
    return this.seguridadOrdinariaNoGenBeanBusiness.findNominaMre(idGrupoNomina);
  }

  public void actualizarAportesPatronales(GrupoNomina grupoNomina, long anio, long mes) throws Exception {
    this.aporteBeanBusiness.actualizarAportesPatronales(grupoNomina, anio, mes);
  }

  public Collection getMensajesUltimaPrenomina(Long idNominaEspecial, Long idGrupoNomina)
    throws Exception
  {
    return this.generarPrenominaEspecialBeanBusiness.getMensajesUltimaPrenomina(idNominaEspecial, idGrupoNomina);
  }

  public Collection getMensajesUltimaPrenomina(Long idGrupoNomina) throws Exception
  {
    return this.generarPrenominaBeanBusiness.getMensajesUltimaPrenomina(idGrupoNomina);
  }
}