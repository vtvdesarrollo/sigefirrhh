package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;

public class SeguridadEspecialBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(SeguridadEspecialBeanBusiness.class.getName());

  public void addSeguridadEspecial(SeguridadEspecial seguridadEspecial)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SeguridadEspecial seguridadEspecialNew = 
      (SeguridadEspecial)BeanUtils.cloneBean(
      seguridadEspecial);

    NominaEspecialBeanBusiness nominaEspecialBeanBusiness = new NominaEspecialBeanBusiness();

    if (seguridadEspecialNew.getNominaEspecial() != null) {
      seguridadEspecialNew.setNominaEspecial(
        nominaEspecialBeanBusiness.findNominaEspecialById(
        seguridadEspecialNew.getNominaEspecial().getIdNominaEspecial()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (seguridadEspecialNew.getGrupoNomina() != null) {
      seguridadEspecialNew.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        seguridadEspecialNew.getGrupoNomina().getIdGrupoNomina()));
    }
    pm.makePersistent(seguridadEspecialNew);
  }

  public void updateSeguridadEspecial(SeguridadEspecial seguridadEspecial) throws Exception
  {
    SeguridadEspecial seguridadEspecialModify = 
      findSeguridadEspecialById(seguridadEspecial.getIdSeguridadEspecial());

    NominaEspecialBeanBusiness nominaEspecialBeanBusiness = new NominaEspecialBeanBusiness();

    if (seguridadEspecial.getNominaEspecial() != null) {
      seguridadEspecial.setNominaEspecial(
        nominaEspecialBeanBusiness.findNominaEspecialById(
        seguridadEspecial.getNominaEspecial().getIdNominaEspecial()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (seguridadEspecial.getGrupoNomina() != null) {
      seguridadEspecial.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        seguridadEspecial.getGrupoNomina().getIdGrupoNomina()));
    }

    BeanUtils.copyProperties(seguridadEspecialModify, seguridadEspecial);
  }

  public void deleteSeguridadEspecial(SeguridadEspecial seguridadEspecial) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SeguridadEspecial seguridadEspecialDelete = 
      findSeguridadEspecialById(seguridadEspecial.getIdSeguridadEspecial());
    pm.deletePersistent(seguridadEspecialDelete);
  }

  public SeguridadEspecial findSeguridadEspecialById(long idSeguridadEspecial) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSeguridadEspecial == pIdSeguridadEspecial";
    Query query = pm.newQuery(SeguridadEspecial.class, filter);

    query.declareParameters("long pIdSeguridadEspecial");

    parameters.put("pIdSeguridadEspecial", new Long(idSeguridadEspecial));

    Collection colSeguridadEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSeguridadEspecial.iterator();
    return (SeguridadEspecial)iterator.next();
  }

  public Collection findSeguridadEspecialAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent seguridadEspecialExtent = pm.getExtent(
      SeguridadEspecial.class, true);
    Query query = pm.newQuery(seguridadEspecialExtent);
    query.setOrdering("anio ascending, mes ascending, fechaInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    Collection colSeguridadEspecial = null;
    try
    {
      PersistenceManager pm = PMThread.getPM();

      String filter = "anio == pAnio";

      Query query = pm.newQuery(SeguridadEspecial.class, filter);

      query.declareParameters("int pAnio");
      HashMap parameters = new HashMap();

      parameters.put("pAnio", new Integer(anio));

      query.setOrdering("anio ascending, mes ascending, fechaInicio ascending");

      colSeguridadEspecial = 
        new ArrayList((Collection)query.executeWithMap(parameters));

      pm.makeTransientAll(colSeguridadEspecial);
    }
    catch (Exception er) {
      this.log.error("Error al buscar coleccion de SeguridadEspacial detalles:");
      er.printStackTrace();
      throw er;
    }

    return colSeguridadEspecial;
  }

  public Collection findByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina";

    Query query = pm.newQuery(SeguridadEspecial.class, filter);

    query.declareParameters("long pIdGrupoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));

    query.setOrdering("anio ascending, mes ascending, fechaInicio ascending");

    Collection colSeguridadEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadEspecial);

    return colSeguridadEspecial;
  }
}