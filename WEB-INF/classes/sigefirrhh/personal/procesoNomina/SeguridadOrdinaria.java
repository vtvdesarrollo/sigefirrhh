package sigefirrhh.personal.procesoNomina;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.GrupoNomina;

public class SeguridadOrdinaria
  implements Serializable, PersistenceCapable
{
  private long idSeguridadOrdinaria;
  private int anio;
  private int mes;
  private Date fechaInicio;
  private Date fechaFin;
  private int semanaQuincena;
  private Date fechaProceso;
  private String cierreDiplomatico;
  private String usuario;
  private GrupoNomina grupoNomina;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "cierreDiplomatico", "fechaFin", "fechaInicio", "fechaProceso", "grupoNomina", "idSeguridadOrdinaria", "mes", "semanaQuincena", "usuario" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), Long.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 26, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public SeguridadOrdinaria()
  {
    jdoSetcierreDiplomatico(this, "N");
  }

  public String toString()
  {
    return jdoGetanio(this) + "  -  " + 
      jdoGetmes(this) + "  -  " + jdoGetgrupoNomina(this).getNombre() + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this)) + "  -  " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaFin(this));
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public String getCierreDiplomatico() {
    return jdoGetcierreDiplomatico(this);
  }
  public void setCierreDiplomatico(String cierreDiplomatico) {
    jdoSetcierreDiplomatico(this, cierreDiplomatico);
  }
  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }
  public void setFechaFin(Date fechaFin) {
    jdoSetfechaFin(this, fechaFin);
  }
  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }
  public void setFechaInicio(Date fechaInicio) {
    jdoSetfechaInicio(this, fechaInicio);
  }
  public Date getFechaProceso() {
    return jdoGetfechaProceso(this);
  }
  public void setFechaProceso(Date fechaProceso) {
    jdoSetfechaProceso(this, fechaProceso);
  }
  public GrupoNomina getGrupoNomina() {
    return jdoGetgrupoNomina(this);
  }
  public void setGrupoNomina(GrupoNomina grupoNomina) {
    jdoSetgrupoNomina(this, grupoNomina);
  }
  public long getIdSeguridadOrdinaria() {
    return jdoGetidSeguridadOrdinaria(this);
  }
  public void setIdSeguridadOrdinaria(long idSeguridadOrdinaria) {
    jdoSetidSeguridadOrdinaria(this, idSeguridadOrdinaria);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }
  public int getSemanaQuincena() {
    return jdoGetsemanaQuincena(this);
  }
  public void setSemanaQuincena(int semanaQuincena) {
    jdoSetsemanaQuincena(this, semanaQuincena);
  }
  public String getUsuario() {
    return jdoGetusuario(this);
  }
  public void setUsuario(String usuario) {
    jdoSetusuario(this, usuario);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoNomina.SeguridadOrdinaria"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SeguridadOrdinaria());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SeguridadOrdinaria localSeguridadOrdinaria = new SeguridadOrdinaria();
    localSeguridadOrdinaria.jdoFlags = 1;
    localSeguridadOrdinaria.jdoStateManager = paramStateManager;
    return localSeguridadOrdinaria;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SeguridadOrdinaria localSeguridadOrdinaria = new SeguridadOrdinaria();
    localSeguridadOrdinaria.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSeguridadOrdinaria.jdoFlags = 1;
    localSeguridadOrdinaria.jdoStateManager = paramStateManager;
    return localSeguridadOrdinaria;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cierreDiplomatico);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSeguridadOrdinaria);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanaQuincena);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cierreDiplomatico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSeguridadOrdinaria = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanaQuincena = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SeguridadOrdinaria paramSeguridadOrdinaria, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSeguridadOrdinaria == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramSeguridadOrdinaria.anio;
      return;
    case 1:
      if (paramSeguridadOrdinaria == null)
        throw new IllegalArgumentException("arg1");
      this.cierreDiplomatico = paramSeguridadOrdinaria.cierreDiplomatico;
      return;
    case 2:
      if (paramSeguridadOrdinaria == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramSeguridadOrdinaria.fechaFin;
      return;
    case 3:
      if (paramSeguridadOrdinaria == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramSeguridadOrdinaria.fechaInicio;
      return;
    case 4:
      if (paramSeguridadOrdinaria == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramSeguridadOrdinaria.fechaProceso;
      return;
    case 5:
      if (paramSeguridadOrdinaria == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramSeguridadOrdinaria.grupoNomina;
      return;
    case 6:
      if (paramSeguridadOrdinaria == null)
        throw new IllegalArgumentException("arg1");
      this.idSeguridadOrdinaria = paramSeguridadOrdinaria.idSeguridadOrdinaria;
      return;
    case 7:
      if (paramSeguridadOrdinaria == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramSeguridadOrdinaria.mes;
      return;
    case 8:
      if (paramSeguridadOrdinaria == null)
        throw new IllegalArgumentException("arg1");
      this.semanaQuincena = paramSeguridadOrdinaria.semanaQuincena;
      return;
    case 9:
      if (paramSeguridadOrdinaria == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramSeguridadOrdinaria.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SeguridadOrdinaria))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SeguridadOrdinaria localSeguridadOrdinaria = (SeguridadOrdinaria)paramObject;
    if (localSeguridadOrdinaria.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSeguridadOrdinaria, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SeguridadOrdinariaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SeguridadOrdinariaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadOrdinariaPK))
      throw new IllegalArgumentException("arg1");
    SeguridadOrdinariaPK localSeguridadOrdinariaPK = (SeguridadOrdinariaPK)paramObject;
    localSeguridadOrdinariaPK.idSeguridadOrdinaria = this.idSeguridadOrdinaria;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadOrdinariaPK))
      throw new IllegalArgumentException("arg1");
    SeguridadOrdinariaPK localSeguridadOrdinariaPK = (SeguridadOrdinariaPK)paramObject;
    this.idSeguridadOrdinaria = localSeguridadOrdinariaPK.idSeguridadOrdinaria;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadOrdinariaPK))
      throw new IllegalArgumentException("arg2");
    SeguridadOrdinariaPK localSeguridadOrdinariaPK = (SeguridadOrdinariaPK)paramObject;
    localSeguridadOrdinariaPK.idSeguridadOrdinaria = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadOrdinariaPK))
      throw new IllegalArgumentException("arg2");
    SeguridadOrdinariaPK localSeguridadOrdinariaPK = (SeguridadOrdinariaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localSeguridadOrdinariaPK.idSeguridadOrdinaria);
  }

  private static final int jdoGetanio(SeguridadOrdinaria paramSeguridadOrdinaria)
  {
    if (paramSeguridadOrdinaria.jdoFlags <= 0)
      return paramSeguridadOrdinaria.anio;
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadOrdinaria.anio;
    if (localStateManager.isLoaded(paramSeguridadOrdinaria, jdoInheritedFieldCount + 0))
      return paramSeguridadOrdinaria.anio;
    return localStateManager.getIntField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 0, paramSeguridadOrdinaria.anio);
  }

  private static final void jdoSetanio(SeguridadOrdinaria paramSeguridadOrdinaria, int paramInt)
  {
    if (paramSeguridadOrdinaria.jdoFlags == 0)
    {
      paramSeguridadOrdinaria.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadOrdinaria.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 0, paramSeguridadOrdinaria.anio, paramInt);
  }

  private static final String jdoGetcierreDiplomatico(SeguridadOrdinaria paramSeguridadOrdinaria)
  {
    if (paramSeguridadOrdinaria.jdoFlags <= 0)
      return paramSeguridadOrdinaria.cierreDiplomatico;
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadOrdinaria.cierreDiplomatico;
    if (localStateManager.isLoaded(paramSeguridadOrdinaria, jdoInheritedFieldCount + 1))
      return paramSeguridadOrdinaria.cierreDiplomatico;
    return localStateManager.getStringField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 1, paramSeguridadOrdinaria.cierreDiplomatico);
  }

  private static final void jdoSetcierreDiplomatico(SeguridadOrdinaria paramSeguridadOrdinaria, String paramString)
  {
    if (paramSeguridadOrdinaria.jdoFlags == 0)
    {
      paramSeguridadOrdinaria.cierreDiplomatico = paramString;
      return;
    }
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadOrdinaria.cierreDiplomatico = paramString;
      return;
    }
    localStateManager.setStringField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 1, paramSeguridadOrdinaria.cierreDiplomatico, paramString);
  }

  private static final Date jdoGetfechaFin(SeguridadOrdinaria paramSeguridadOrdinaria)
  {
    if (paramSeguridadOrdinaria.jdoFlags <= 0)
      return paramSeguridadOrdinaria.fechaFin;
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadOrdinaria.fechaFin;
    if (localStateManager.isLoaded(paramSeguridadOrdinaria, jdoInheritedFieldCount + 2))
      return paramSeguridadOrdinaria.fechaFin;
    return (Date)localStateManager.getObjectField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 2, paramSeguridadOrdinaria.fechaFin);
  }

  private static final void jdoSetfechaFin(SeguridadOrdinaria paramSeguridadOrdinaria, Date paramDate)
  {
    if (paramSeguridadOrdinaria.jdoFlags == 0)
    {
      paramSeguridadOrdinaria.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadOrdinaria.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 2, paramSeguridadOrdinaria.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(SeguridadOrdinaria paramSeguridadOrdinaria)
  {
    if (paramSeguridadOrdinaria.jdoFlags <= 0)
      return paramSeguridadOrdinaria.fechaInicio;
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadOrdinaria.fechaInicio;
    if (localStateManager.isLoaded(paramSeguridadOrdinaria, jdoInheritedFieldCount + 3))
      return paramSeguridadOrdinaria.fechaInicio;
    return (Date)localStateManager.getObjectField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 3, paramSeguridadOrdinaria.fechaInicio);
  }

  private static final void jdoSetfechaInicio(SeguridadOrdinaria paramSeguridadOrdinaria, Date paramDate)
  {
    if (paramSeguridadOrdinaria.jdoFlags == 0)
    {
      paramSeguridadOrdinaria.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadOrdinaria.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 3, paramSeguridadOrdinaria.fechaInicio, paramDate);
  }

  private static final Date jdoGetfechaProceso(SeguridadOrdinaria paramSeguridadOrdinaria)
  {
    if (paramSeguridadOrdinaria.jdoFlags <= 0)
      return paramSeguridadOrdinaria.fechaProceso;
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadOrdinaria.fechaProceso;
    if (localStateManager.isLoaded(paramSeguridadOrdinaria, jdoInheritedFieldCount + 4))
      return paramSeguridadOrdinaria.fechaProceso;
    return (Date)localStateManager.getObjectField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 4, paramSeguridadOrdinaria.fechaProceso);
  }

  private static final void jdoSetfechaProceso(SeguridadOrdinaria paramSeguridadOrdinaria, Date paramDate)
  {
    if (paramSeguridadOrdinaria.jdoFlags == 0)
    {
      paramSeguridadOrdinaria.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadOrdinaria.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 4, paramSeguridadOrdinaria.fechaProceso, paramDate);
  }

  private static final GrupoNomina jdoGetgrupoNomina(SeguridadOrdinaria paramSeguridadOrdinaria)
  {
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadOrdinaria.grupoNomina;
    if (localStateManager.isLoaded(paramSeguridadOrdinaria, jdoInheritedFieldCount + 5))
      return paramSeguridadOrdinaria.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 5, paramSeguridadOrdinaria.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(SeguridadOrdinaria paramSeguridadOrdinaria, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadOrdinaria.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 5, paramSeguridadOrdinaria.grupoNomina, paramGrupoNomina);
  }

  private static final long jdoGetidSeguridadOrdinaria(SeguridadOrdinaria paramSeguridadOrdinaria)
  {
    return paramSeguridadOrdinaria.idSeguridadOrdinaria;
  }

  private static final void jdoSetidSeguridadOrdinaria(SeguridadOrdinaria paramSeguridadOrdinaria, long paramLong)
  {
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadOrdinaria.idSeguridadOrdinaria = paramLong;
      return;
    }
    localStateManager.setLongField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 6, paramSeguridadOrdinaria.idSeguridadOrdinaria, paramLong);
  }

  private static final int jdoGetmes(SeguridadOrdinaria paramSeguridadOrdinaria)
  {
    if (paramSeguridadOrdinaria.jdoFlags <= 0)
      return paramSeguridadOrdinaria.mes;
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadOrdinaria.mes;
    if (localStateManager.isLoaded(paramSeguridadOrdinaria, jdoInheritedFieldCount + 7))
      return paramSeguridadOrdinaria.mes;
    return localStateManager.getIntField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 7, paramSeguridadOrdinaria.mes);
  }

  private static final void jdoSetmes(SeguridadOrdinaria paramSeguridadOrdinaria, int paramInt)
  {
    if (paramSeguridadOrdinaria.jdoFlags == 0)
    {
      paramSeguridadOrdinaria.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadOrdinaria.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 7, paramSeguridadOrdinaria.mes, paramInt);
  }

  private static final int jdoGetsemanaQuincena(SeguridadOrdinaria paramSeguridadOrdinaria)
  {
    if (paramSeguridadOrdinaria.jdoFlags <= 0)
      return paramSeguridadOrdinaria.semanaQuincena;
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadOrdinaria.semanaQuincena;
    if (localStateManager.isLoaded(paramSeguridadOrdinaria, jdoInheritedFieldCount + 8))
      return paramSeguridadOrdinaria.semanaQuincena;
    return localStateManager.getIntField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 8, paramSeguridadOrdinaria.semanaQuincena);
  }

  private static final void jdoSetsemanaQuincena(SeguridadOrdinaria paramSeguridadOrdinaria, int paramInt)
  {
    if (paramSeguridadOrdinaria.jdoFlags == 0)
    {
      paramSeguridadOrdinaria.semanaQuincena = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadOrdinaria.semanaQuincena = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 8, paramSeguridadOrdinaria.semanaQuincena, paramInt);
  }

  private static final String jdoGetusuario(SeguridadOrdinaria paramSeguridadOrdinaria)
  {
    if (paramSeguridadOrdinaria.jdoFlags <= 0)
      return paramSeguridadOrdinaria.usuario;
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadOrdinaria.usuario;
    if (localStateManager.isLoaded(paramSeguridadOrdinaria, jdoInheritedFieldCount + 9))
      return paramSeguridadOrdinaria.usuario;
    return localStateManager.getStringField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 9, paramSeguridadOrdinaria.usuario);
  }

  private static final void jdoSetusuario(SeguridadOrdinaria paramSeguridadOrdinaria, String paramString)
  {
    if (paramSeguridadOrdinaria.jdoFlags == 0)
    {
      paramSeguridadOrdinaria.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramSeguridadOrdinaria.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadOrdinaria.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramSeguridadOrdinaria, jdoInheritedFieldCount + 9, paramSeguridadOrdinaria.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}