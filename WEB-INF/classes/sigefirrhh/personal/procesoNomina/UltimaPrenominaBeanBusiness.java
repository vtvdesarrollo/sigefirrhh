package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class UltimaPrenominaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addUltimaPrenomina(UltimaPrenomina ultimaPrenomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    UltimaPrenomina ultimaPrenominaNew = 
      (UltimaPrenomina)BeanUtils.cloneBean(
      ultimaPrenomina);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (ultimaPrenominaNew.getConceptoTipoPersonal() != null) {
      ultimaPrenominaNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        ultimaPrenominaNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (ultimaPrenominaNew.getFrecuenciaTipoPersonal() != null) {
      ultimaPrenominaNew.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        ultimaPrenominaNew.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (ultimaPrenominaNew.getTrabajador() != null) {
      ultimaPrenominaNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        ultimaPrenominaNew.getTrabajador().getIdTrabajador()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (ultimaPrenominaNew.getGrupoNomina() != null) {
      ultimaPrenominaNew.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        ultimaPrenominaNew.getGrupoNomina().getIdGrupoNomina()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (ultimaPrenominaNew.getTipoPersonal() != null) {
      ultimaPrenominaNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        ultimaPrenominaNew.getTipoPersonal().getIdTipoPersonal()));
    }

    NominaEspecialBeanBusiness nominaEspecialBeanBusiness = new NominaEspecialBeanBusiness();

    if (ultimaPrenominaNew.getNominaEspecial() != null) {
      ultimaPrenominaNew.setNominaEspecial(
        nominaEspecialBeanBusiness.findNominaEspecialById(
        ultimaPrenominaNew.getNominaEspecial().getIdNominaEspecial()));
    }
    pm.makePersistent(ultimaPrenominaNew);
  }

  public void updateUltimaPrenomina(UltimaPrenomina ultimaPrenomina) throws Exception
  {
    UltimaPrenomina ultimaPrenominaModify = 
      findUltimaPrenominaById(ultimaPrenomina.getIdUltimaPrenomina());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (ultimaPrenomina.getConceptoTipoPersonal() != null) {
      ultimaPrenomina.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        ultimaPrenomina.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (ultimaPrenomina.getFrecuenciaTipoPersonal() != null) {
      ultimaPrenomina.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        ultimaPrenomina.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (ultimaPrenomina.getTrabajador() != null) {
      ultimaPrenomina.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        ultimaPrenomina.getTrabajador().getIdTrabajador()));
    }

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (ultimaPrenomina.getGrupoNomina() != null) {
      ultimaPrenomina.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        ultimaPrenomina.getGrupoNomina().getIdGrupoNomina()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (ultimaPrenomina.getTipoPersonal() != null) {
      ultimaPrenomina.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        ultimaPrenomina.getTipoPersonal().getIdTipoPersonal()));
    }

    NominaEspecialBeanBusiness nominaEspecialBeanBusiness = new NominaEspecialBeanBusiness();

    if (ultimaPrenomina.getNominaEspecial() != null) {
      ultimaPrenomina.setNominaEspecial(
        nominaEspecialBeanBusiness.findNominaEspecialById(
        ultimaPrenomina.getNominaEspecial().getIdNominaEspecial()));
    }

    BeanUtils.copyProperties(ultimaPrenominaModify, ultimaPrenomina);
  }

  public void deleteUltimaPrenomina(UltimaPrenomina ultimaPrenomina) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    UltimaPrenomina ultimaPrenominaDelete = 
      findUltimaPrenominaById(ultimaPrenomina.getIdUltimaPrenomina());
    pm.deletePersistent(ultimaPrenominaDelete);
  }

  public UltimaPrenomina findUltimaPrenominaById(long idUltimaPrenomina) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idUltimaPrenomina == pIdUltimaPrenomina";
    Query query = pm.newQuery(UltimaPrenomina.class, filter);

    query.declareParameters("long pIdUltimaPrenomina");

    parameters.put("pIdUltimaPrenomina", new Long(idUltimaPrenomina));

    Collection colUltimaPrenomina = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colUltimaPrenomina.iterator();
    return (UltimaPrenomina)iterator.next();
  }

  public Collection findUltimaPrenominaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent ultimaPrenominaExtent = pm.getExtent(
      UltimaPrenomina.class, true);
    Query query = pm.newQuery(ultimaPrenominaExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}