package sigefirrhh.personal.procesoNomina;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class UltimaPrenomina
  implements Serializable, PersistenceCapable
{
  private long idUltimaPrenomina;
  private int numeroNomina;
  private double unidades;
  private double montoAsigna;
  private double montoDeduce;
  private String origen;
  private String documentoSoporte;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private Trabajador trabajador;
  private GrupoNomina grupoNomina;
  private TipoPersonal tipoPersonal;
  private NominaEspecial nominaEspecial;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "documentoSoporte", "frecuenciaTipoPersonal", "grupoNomina", "idUltimaPrenomina", "montoAsigna", "montoDeduce", "nominaEspecial", "numeroNomina", "origen", "tipoPersonal", "trabajador", "unidades" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), Long.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.procesoNomina.NominaEspecial"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), Double.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 21, 26, 26, 24, 21, 21, 26, 21, 21, 26, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal()
  {
    return jdoGetfrecuenciaTipoPersonal(this);
  }

  public GrupoNomina getGrupoNomina()
  {
    return jdoGetgrupoNomina(this);
  }

  public long getIdUltimaPrenomina()
  {
    return jdoGetidUltimaPrenomina(this);
  }

  public double getMontoAsigna()
  {
    return jdoGetmontoAsigna(this);
  }

  public double getMontoDeduce()
  {
    return jdoGetmontoDeduce(this);
  }

  public NominaEspecial getNominaEspecial()
  {
    return jdoGetnominaEspecial(this);
  }

  public int getNumeroNomina()
  {
    return jdoGetnumeroNomina(this);
  }

  public String getOrigen()
  {
    return jdoGetorigen(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public double getUnidades()
  {
    return jdoGetunidades(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setFrecuenciaTipoPersonal(FrecuenciaTipoPersonal personal)
  {
    jdoSetfrecuenciaTipoPersonal(this, personal);
  }

  public void setGrupoNomina(GrupoNomina nomina)
  {
    jdoSetgrupoNomina(this, nomina);
  }

  public void setIdUltimaPrenomina(long l)
  {
    jdoSetidUltimaPrenomina(this, l);
  }

  public void setMontoAsigna(double d)
  {
    jdoSetmontoAsigna(this, d);
  }

  public void setMontoDeduce(double d)
  {
    jdoSetmontoDeduce(this, d);
  }

  public void setNominaEspecial(NominaEspecial especial)
  {
    jdoSetnominaEspecial(this, especial);
  }

  public void setNumeroNomina(int i)
  {
    jdoSetnumeroNomina(this, i);
  }

  public void setOrigen(String string)
  {
    jdoSetorigen(this, string);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setUnidades(double d)
  {
    jdoSetunidades(this, d);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoNomina.UltimaPrenomina"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UltimaPrenomina());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UltimaPrenomina localUltimaPrenomina = new UltimaPrenomina();
    localUltimaPrenomina.jdoFlags = 1;
    localUltimaPrenomina.jdoStateManager = paramStateManager;
    return localUltimaPrenomina;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UltimaPrenomina localUltimaPrenomina = new UltimaPrenomina();
    localUltimaPrenomina.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUltimaPrenomina.jdoFlags = 1;
    localUltimaPrenomina.jdoStateManager = paramStateManager;
    return localUltimaPrenomina;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaTipoPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUltimaPrenomina);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAsigna);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoDeduce);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nominaEspecial);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroNomina);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origen);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaTipoPersonal = ((FrecuenciaTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUltimaPrenomina = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAsigna = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoDeduce = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nominaEspecial = ((NominaEspecial)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origen = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UltimaPrenomina paramUltimaPrenomina, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramUltimaPrenomina.conceptoTipoPersonal;
      return;
    case 1:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramUltimaPrenomina.documentoSoporte;
      return;
    case 2:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaTipoPersonal = paramUltimaPrenomina.frecuenciaTipoPersonal;
      return;
    case 3:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramUltimaPrenomina.grupoNomina;
      return;
    case 4:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.idUltimaPrenomina = paramUltimaPrenomina.idUltimaPrenomina;
      return;
    case 5:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.montoAsigna = paramUltimaPrenomina.montoAsigna;
      return;
    case 6:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.montoDeduce = paramUltimaPrenomina.montoDeduce;
      return;
    case 7:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.nominaEspecial = paramUltimaPrenomina.nominaEspecial;
      return;
    case 8:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.numeroNomina = paramUltimaPrenomina.numeroNomina;
      return;
    case 9:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.origen = paramUltimaPrenomina.origen;
      return;
    case 10:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramUltimaPrenomina.tipoPersonal;
      return;
    case 11:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramUltimaPrenomina.trabajador;
      return;
    case 12:
      if (paramUltimaPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramUltimaPrenomina.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UltimaPrenomina))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UltimaPrenomina localUltimaPrenomina = (UltimaPrenomina)paramObject;
    if (localUltimaPrenomina.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUltimaPrenomina, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UltimaPrenominaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UltimaPrenominaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UltimaPrenominaPK))
      throw new IllegalArgumentException("arg1");
    UltimaPrenominaPK localUltimaPrenominaPK = (UltimaPrenominaPK)paramObject;
    localUltimaPrenominaPK.idUltimaPrenomina = this.idUltimaPrenomina;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UltimaPrenominaPK))
      throw new IllegalArgumentException("arg1");
    UltimaPrenominaPK localUltimaPrenominaPK = (UltimaPrenominaPK)paramObject;
    this.idUltimaPrenomina = localUltimaPrenominaPK.idUltimaPrenomina;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UltimaPrenominaPK))
      throw new IllegalArgumentException("arg2");
    UltimaPrenominaPK localUltimaPrenominaPK = (UltimaPrenominaPK)paramObject;
    localUltimaPrenominaPK.idUltimaPrenomina = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UltimaPrenominaPK))
      throw new IllegalArgumentException("arg2");
    UltimaPrenominaPK localUltimaPrenominaPK = (UltimaPrenominaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localUltimaPrenominaPK.idUltimaPrenomina);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(UltimaPrenomina paramUltimaPrenomina)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 0))
      return paramUltimaPrenomina.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 0, paramUltimaPrenomina.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(UltimaPrenomina paramUltimaPrenomina, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 0, paramUltimaPrenomina.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGetdocumentoSoporte(UltimaPrenomina paramUltimaPrenomina)
  {
    if (paramUltimaPrenomina.jdoFlags <= 0)
      return paramUltimaPrenomina.documentoSoporte;
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.documentoSoporte;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 1))
      return paramUltimaPrenomina.documentoSoporte;
    return localStateManager.getStringField(paramUltimaPrenomina, jdoInheritedFieldCount + 1, paramUltimaPrenomina.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(UltimaPrenomina paramUltimaPrenomina, String paramString)
  {
    if (paramUltimaPrenomina.jdoFlags == 0)
    {
      paramUltimaPrenomina.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramUltimaPrenomina, jdoInheritedFieldCount + 1, paramUltimaPrenomina.documentoSoporte, paramString);
  }

  private static final FrecuenciaTipoPersonal jdoGetfrecuenciaTipoPersonal(UltimaPrenomina paramUltimaPrenomina)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.frecuenciaTipoPersonal;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 2))
      return paramUltimaPrenomina.frecuenciaTipoPersonal;
    return (FrecuenciaTipoPersonal)localStateManager.getObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 2, paramUltimaPrenomina.frecuenciaTipoPersonal);
  }

  private static final void jdoSetfrecuenciaTipoPersonal(UltimaPrenomina paramUltimaPrenomina, FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.frecuenciaTipoPersonal = paramFrecuenciaTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 2, paramUltimaPrenomina.frecuenciaTipoPersonal, paramFrecuenciaTipoPersonal);
  }

  private static final GrupoNomina jdoGetgrupoNomina(UltimaPrenomina paramUltimaPrenomina)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.grupoNomina;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 3))
      return paramUltimaPrenomina.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 3, paramUltimaPrenomina.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(UltimaPrenomina paramUltimaPrenomina, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 3, paramUltimaPrenomina.grupoNomina, paramGrupoNomina);
  }

  private static final long jdoGetidUltimaPrenomina(UltimaPrenomina paramUltimaPrenomina)
  {
    return paramUltimaPrenomina.idUltimaPrenomina;
  }

  private static final void jdoSetidUltimaPrenomina(UltimaPrenomina paramUltimaPrenomina, long paramLong)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.idUltimaPrenomina = paramLong;
      return;
    }
    localStateManager.setLongField(paramUltimaPrenomina, jdoInheritedFieldCount + 4, paramUltimaPrenomina.idUltimaPrenomina, paramLong);
  }

  private static final double jdoGetmontoAsigna(UltimaPrenomina paramUltimaPrenomina)
  {
    if (paramUltimaPrenomina.jdoFlags <= 0)
      return paramUltimaPrenomina.montoAsigna;
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.montoAsigna;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 5))
      return paramUltimaPrenomina.montoAsigna;
    return localStateManager.getDoubleField(paramUltimaPrenomina, jdoInheritedFieldCount + 5, paramUltimaPrenomina.montoAsigna);
  }

  private static final void jdoSetmontoAsigna(UltimaPrenomina paramUltimaPrenomina, double paramDouble)
  {
    if (paramUltimaPrenomina.jdoFlags == 0)
    {
      paramUltimaPrenomina.montoAsigna = paramDouble;
      return;
    }
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.montoAsigna = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramUltimaPrenomina, jdoInheritedFieldCount + 5, paramUltimaPrenomina.montoAsigna, paramDouble);
  }

  private static final double jdoGetmontoDeduce(UltimaPrenomina paramUltimaPrenomina)
  {
    if (paramUltimaPrenomina.jdoFlags <= 0)
      return paramUltimaPrenomina.montoDeduce;
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.montoDeduce;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 6))
      return paramUltimaPrenomina.montoDeduce;
    return localStateManager.getDoubleField(paramUltimaPrenomina, jdoInheritedFieldCount + 6, paramUltimaPrenomina.montoDeduce);
  }

  private static final void jdoSetmontoDeduce(UltimaPrenomina paramUltimaPrenomina, double paramDouble)
  {
    if (paramUltimaPrenomina.jdoFlags == 0)
    {
      paramUltimaPrenomina.montoDeduce = paramDouble;
      return;
    }
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.montoDeduce = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramUltimaPrenomina, jdoInheritedFieldCount + 6, paramUltimaPrenomina.montoDeduce, paramDouble);
  }

  private static final NominaEspecial jdoGetnominaEspecial(UltimaPrenomina paramUltimaPrenomina)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.nominaEspecial;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 7))
      return paramUltimaPrenomina.nominaEspecial;
    return (NominaEspecial)localStateManager.getObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 7, paramUltimaPrenomina.nominaEspecial);
  }

  private static final void jdoSetnominaEspecial(UltimaPrenomina paramUltimaPrenomina, NominaEspecial paramNominaEspecial)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.nominaEspecial = paramNominaEspecial;
      return;
    }
    localStateManager.setObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 7, paramUltimaPrenomina.nominaEspecial, paramNominaEspecial);
  }

  private static final int jdoGetnumeroNomina(UltimaPrenomina paramUltimaPrenomina)
  {
    if (paramUltimaPrenomina.jdoFlags <= 0)
      return paramUltimaPrenomina.numeroNomina;
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.numeroNomina;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 8))
      return paramUltimaPrenomina.numeroNomina;
    return localStateManager.getIntField(paramUltimaPrenomina, jdoInheritedFieldCount + 8, paramUltimaPrenomina.numeroNomina);
  }

  private static final void jdoSetnumeroNomina(UltimaPrenomina paramUltimaPrenomina, int paramInt)
  {
    if (paramUltimaPrenomina.jdoFlags == 0)
    {
      paramUltimaPrenomina.numeroNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.numeroNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramUltimaPrenomina, jdoInheritedFieldCount + 8, paramUltimaPrenomina.numeroNomina, paramInt);
  }

  private static final String jdoGetorigen(UltimaPrenomina paramUltimaPrenomina)
  {
    if (paramUltimaPrenomina.jdoFlags <= 0)
      return paramUltimaPrenomina.origen;
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.origen;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 9))
      return paramUltimaPrenomina.origen;
    return localStateManager.getStringField(paramUltimaPrenomina, jdoInheritedFieldCount + 9, paramUltimaPrenomina.origen);
  }

  private static final void jdoSetorigen(UltimaPrenomina paramUltimaPrenomina, String paramString)
  {
    if (paramUltimaPrenomina.jdoFlags == 0)
    {
      paramUltimaPrenomina.origen = paramString;
      return;
    }
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.origen = paramString;
      return;
    }
    localStateManager.setStringField(paramUltimaPrenomina, jdoInheritedFieldCount + 9, paramUltimaPrenomina.origen, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(UltimaPrenomina paramUltimaPrenomina)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.tipoPersonal;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 10))
      return paramUltimaPrenomina.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 10, paramUltimaPrenomina.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(UltimaPrenomina paramUltimaPrenomina, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 10, paramUltimaPrenomina.tipoPersonal, paramTipoPersonal);
  }

  private static final Trabajador jdoGettrabajador(UltimaPrenomina paramUltimaPrenomina)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.trabajador;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 11))
      return paramUltimaPrenomina.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 11, paramUltimaPrenomina.trabajador);
  }

  private static final void jdoSettrabajador(UltimaPrenomina paramUltimaPrenomina, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramUltimaPrenomina, jdoInheritedFieldCount + 11, paramUltimaPrenomina.trabajador, paramTrabajador);
  }

  private static final double jdoGetunidades(UltimaPrenomina paramUltimaPrenomina)
  {
    if (paramUltimaPrenomina.jdoFlags <= 0)
      return paramUltimaPrenomina.unidades;
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaPrenomina.unidades;
    if (localStateManager.isLoaded(paramUltimaPrenomina, jdoInheritedFieldCount + 12))
      return paramUltimaPrenomina.unidades;
    return localStateManager.getDoubleField(paramUltimaPrenomina, jdoInheritedFieldCount + 12, paramUltimaPrenomina.unidades);
  }

  private static final void jdoSetunidades(UltimaPrenomina paramUltimaPrenomina, double paramDouble)
  {
    if (paramUltimaPrenomina.jdoFlags == 0)
    {
      paramUltimaPrenomina.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramUltimaPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaPrenomina.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramUltimaPrenomina, jdoInheritedFieldCount + 12, paramUltimaPrenomina.unidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}