package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.sequence.IdentityGenerator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesBusinessExtend;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;
import sigefirrhh.base.definiciones.ParametroGobierno;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class GenerarNominaBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(GenerarNominaBeanBusiness.class.getName());

  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public boolean generar(long idGrupoNomina, String periodicidad, String recalculo, java.util.Date fechaInicio, java.util.Date fechaFin, long idOrganismo, Integer lunesPrQuincena, Integer lunesSeQuincena, Boolean tieneSemana5, Integer semanaMes, String usuario, Integer semanaAnio, Integer numeroSemanasMes, Integer mesSemanal, Integer anioSemanal)
    throws Exception
  {
    CalcularSueldosPromedioBeanBusiness calcularSueldosPromedio = 
      new CalcularSueldosPromedioBeanBusiness();

    if (periodicidad.equals("S"))
      calcularSueldosPromedio.calcularSemanal(
        idOrganismo, idGrupoNomina, periodicidad, recalculo, fechaInicio, fechaFin, semanaMes, tieneSemana5, numeroSemanasMes);
    else {
      calcularSueldosPromedio.calcularMensual(
        idOrganismo, idGrupoNomina, periodicidad, recalculo, fechaInicio, fechaFin, semanaMes, tieneSemana5, numeroSemanasMes);
    }
    return generarNomina(idGrupoNomina, periodicidad, recalculo, fechaInicio, fechaFin, idOrganismo, lunesPrQuincena, lunesSeQuincena, tieneSemana5, semanaMes, usuario, semanaAnio, mesSemanal, anioSemanal);
  }

  private boolean generarNomina(long idGrupoNomina, String periodicidad, String recalculo, java.util.Date fechaInicio, java.util.Date fechaFin, long idOrganismo, Integer lunesPrQuincena, Integer lunesSeQuincena, Boolean tieneSemana5, Integer semanaMes, String usuario, Integer semanaAnio, Integer mesSemanal, Integer anioSemanal)
    throws Exception
  {
    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      boolean sobregirados = false;
      double montoCalculado = 0.0D;
      this.log.error("VA INICIAR LA NUEVA NOMINA CON SP");

      GrupoNomina grupoNomina = new GrupoNomina();
      GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();
      grupoNomina = grupoNominaBeanBusiness.findGrupoNominaById(idGrupoNomina);
      int mes;
      int anio;
      int mes;
      if (grupoNomina.getPeriodicidad().equals("S")) {
        int anio = anioSemanal.intValue();
        mes = mesSemanal.intValue();
      } else {
        anio = fechaInicio.getYear() + 1900;
        mes = fechaInicio.getMonth() + 1;
      }
      long idTrabajador = 0L;
      long idHistoricoNomina = 0L;

      int semanaQuincena = 0;
      boolean primeraQuincena;
      boolean primeraQuincena;
      if (fechaInicio.getDate() == 1) {
        semanaQuincena = 1;
        primeraQuincena = true;
      } else {
        semanaQuincena = 2;
        primeraQuincena = false;
      }

      java.sql.Date fechaInicioSql = new java.sql.Date(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDate());
      java.sql.Date fechaFinSql = new java.sql.Date(fechaFin.getYear(), fechaFin.getMonth(), fechaFin.getDate());

      java.sql.Date fechaSql = new java.sql.Date(new java.util.Date().getYear(), new java.util.Date().getMonth(), new java.util.Date().getDate());

      EstructuraBusiness estructuraBusiness = 
        new EstructuraBusiness();

      GrupoOrganismo grupoOrganismo = null;

      long idGrupoOrganismo = 0L;

      Collection colGrupoOrganismo = 
        estructuraBusiness.findGrupoOrganismoByOrganismo(idOrganismo);
      grupoOrganismo = (GrupoOrganismo)colGrupoOrganismo.iterator().next();
      idGrupoOrganismo = grupoOrganismo.getIdGrupoOrganismo();

      DefinicionesBusinessExtend definicionesBusiness = 
        new DefinicionesBusinessExtend();

      Collection colParametroGobierno = 
        definicionesBusiness.findParametroGobiernoByGrupoOrganismo(
        idGrupoOrganismo);
      ParametroGobierno parametroGobierno = 
        (ParametroGobierno)colParametroGobierno.iterator().next();

      Calendar fechaTopeLphFemeninoInicio = Calendar.getInstance();
      fechaTopeLphFemeninoInicio.setTime(fechaInicio);
      fechaTopeLphFemeninoInicio.add(1, -parametroGobierno.getEdadfemLph());

      Calendar fechaTopeLphFemeninoFin = Calendar.getInstance();
      fechaTopeLphFemeninoFin.setTime(fechaFin);
      fechaTopeLphFemeninoFin.add(1, -parametroGobierno.getEdadfemLph());

      Calendar fechaTopeLphMasculinoInicio = Calendar.getInstance();
      fechaTopeLphMasculinoInicio.setTime(fechaInicio);
      fechaTopeLphMasculinoInicio.add(1, -parametroGobierno.getEdadmascLph());

      Calendar fechaTopeLphMasculinoFin = Calendar.getInstance();
      fechaTopeLphMasculinoFin.setTime(fechaFin);
      fechaTopeLphMasculinoFin.add(1, -parametroGobierno.getEdadmascLph());

      java.sql.Date fechaTopeLphFemeninoInicioSql = new java.sql.Date(fechaTopeLphFemeninoInicio.getTime().getYear(), fechaTopeLphFemeninoInicio.getTime().getMonth(), fechaTopeLphFemeninoInicio.getTime().getDate());
      java.sql.Date fechaTopeLphFemeninoFinSql = new java.sql.Date(fechaTopeLphFemeninoFin.getTime().getYear(), fechaTopeLphFemeninoFin.getTime().getMonth(), fechaTopeLphFemeninoFin.getTime().getDate());

      java.sql.Date fechaTopeLphMasculinoInicioSql = new java.sql.Date(fechaTopeLphMasculinoInicio.getTime().getYear(), fechaTopeLphMasculinoInicio.getTime().getMonth(), fechaTopeLphMasculinoInicio.getTime().getDate());
      java.sql.Date fechaTopeLphMasculinoFinSql = new java.sql.Date(fechaTopeLphMasculinoFin.getTime().getYear(), fechaTopeLphMasculinoFin.getTime().getMonth(), fechaTopeLphMasculinoFin.getTime().getDate());

      String frecuencia = null;
      int semanaMesMas4 = semanaMes.intValue() + 4;

      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      StringBuffer sql = new StringBuffer();
      sql.append("select generar_nomina(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

      this.log.error("1- false");
      this.log.error("2- true");
      this.log.error("3- " + idGrupoNomina);
      this.log.error("4- " + periodicidad);
      this.log.error("5- " + recalculo);
      this.log.error("6- " + fechaInicioSql);
      this.log.error("7- " + fechaFinSql);
      this.log.error("8- " + idOrganismo);
      this.log.error("9- " + lunesPrQuincena.intValue());
      this.log.error("10- " + lunesSeQuincena.intValue());
      this.log.error("11- " + tieneSemana5.booleanValue());
      this.log.error("12- " + semanaMes.intValue());
      this.log.error("13- " + usuario);
      this.log.error("14- " + semanaAnio.intValue());
      this.log.error("15- " + mesSemanal.intValue());
      this.log.error("16- " + primeraQuincena);
      this.log.error("17- " + semanaQuincena);
      this.log.error("18- 0");
      this.log.error("19- " + fechaSql);
      this.log.error("20- " + mes);
      this.log.error("21- " + anio);
      this.log.error("22- " + fechaTopeLphFemeninoInicioSql);
      this.log.error("23- " + fechaTopeLphFemeninoFinSql);
      this.log.error("24- " + fechaTopeLphMasculinoInicioSql);
      this.log.error("25- " + fechaTopeLphMasculinoFinSql);
      this.log.error("26- 0");
      this.log.error("27- 0");
      this.log.error("28- A");
      this.log.error("29- " + grupoNomina.getPagosNominaEgresados());
      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setBoolean(1, false);
      st.setBoolean(2, true);
      st.setLong(3, idGrupoNomina);
      st.setString(4, periodicidad);
      st.setString(5, recalculo);
      st.setDate(6, fechaInicioSql);
      st.setDate(7, fechaFinSql);
      st.setLong(8, idOrganismo);
      st.setInt(9, lunesPrQuincena.intValue());
      st.setInt(10, lunesSeQuincena.intValue());
      st.setBoolean(11, tieneSemana5.booleanValue());
      st.setInt(12, semanaMes.intValue());
      st.setString(13, usuario);
      st.setInt(14, semanaAnio.intValue());
      st.setInt(15, mesSemanal.intValue());
      st.setBoolean(16, primeraQuincena);
      st.setInt(17, semanaQuincena);
      st.setLong(18, 0L);
      st.setDate(19, fechaSql);
      st.setInt(20, mes);
      st.setInt(21, anio);
      st.setDate(22, fechaTopeLphFemeninoInicioSql);
      st.setDate(23, fechaTopeLphFemeninoFinSql);
      st.setDate(24, fechaTopeLphMasculinoInicioSql);
      st.setDate(25, fechaTopeLphMasculinoFinSql);
      st.setInt(26, 0);
      st.setInt(27, 0);
      st.setString(28, "A");
      st.setString(29, grupoNomina.getPagosNominaEgresados());

      int valor = 0;
      rs = st.executeQuery();
      rs.next();
      valor = rs.getInt(1);
      connection.commit();

      this.log.error("ejecutó la nomina");

      if (valor == 1) {
        this.log.error("HAY SOBREGIRADOS");
        return true;
      }
      int valor;
      StringBuffer sql;
      int semanaMesMas4;
      String frecuencia;
      java.sql.Date fechaTopeLphMasculinoFinSql;
      java.sql.Date fechaTopeLphMasculinoInicioSql;
      java.sql.Date fechaTopeLphFemeninoFinSql;
      java.sql.Date fechaTopeLphFemeninoInicioSql;
      Calendar fechaTopeLphMasculinoFin;
      Calendar fechaTopeLphMasculinoInicio;
      Calendar fechaTopeLphFemeninoFin;
      Calendar fechaTopeLphFemeninoInicio;
      ParametroGobierno parametroGobierno;
      Collection colParametroGobierno;
      DefinicionesBusinessExtend definicionesBusiness;
      Collection colGrupoOrganismo;
      long idGrupoOrganismo;
      GrupoOrganismo grupoOrganismo;
      EstructuraBusiness estructuraBusiness;
      java.sql.Date fechaSql;
      java.sql.Date fechaFinSql;
      java.sql.Date fechaInicioSql;
      int semanaQuincena;
      long idHistoricoNomina;
      long idTrabajador;
      int mes;
      int anio;
      GrupoNominaBeanBusiness grupoNominaBeanBusiness;
      GrupoNomina grupoNomina;
      double montoCalculado;
      boolean sobregirados;
      boolean primeraQuincena;
      if (valor == 100) {
        this.log.error("La integridad de los datos, impide grabar en los historicos");
        ErrorSistema error = new ErrorSistema();
        error.setDescription("La integridad de los datos, impide grabar en los historicos. Verifique con el reporte");
        throw error;
      }
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if ((connection != null) && (!connection.isClosed())) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
    if (rs != null) try {
        rs.close();
      } catch (Exception localException6) {
      } if (st != null) try {
        st.close();
      } catch (Exception localException7) {
      } if ((connection != null) && (!connection.isClosed())) try {
        connection.close(); connection = null;
      }
      catch (Exception localException8)
      {
      } return false;
  }
}