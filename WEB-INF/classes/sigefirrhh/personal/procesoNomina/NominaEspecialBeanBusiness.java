package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaPagoBeanBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;

public class NominaEspecialBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addNominaEspecial(NominaEspecial nominaEspecial)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    NominaEspecial nominaEspecialNew = 
      (NominaEspecial)BeanUtils.cloneBean(
      nominaEspecial);

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (nominaEspecialNew.getGrupoNomina() != null) {
      nominaEspecialNew.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        nominaEspecialNew.getGrupoNomina().getIdGrupoNomina()));
    }

    FrecuenciaPagoBeanBusiness frecuenciaPagoBeanBusiness = new FrecuenciaPagoBeanBusiness();

    if (nominaEspecialNew.getFrecuenciaPago() != null) {
      nominaEspecialNew.setFrecuenciaPago(
        frecuenciaPagoBeanBusiness.findFrecuenciaPagoById(
        nominaEspecialNew.getFrecuenciaPago().getIdFrecuenciaPago()));
    }
    pm.makePersistent(nominaEspecialNew);
  }

  public void updateNominaEspecial(NominaEspecial nominaEspecial) throws Exception
  {
    NominaEspecial nominaEspecialModify = 
      findNominaEspecialById(nominaEspecial.getIdNominaEspecial());

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (nominaEspecial.getGrupoNomina() != null) {
      nominaEspecial.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        nominaEspecial.getGrupoNomina().getIdGrupoNomina()));
    }

    FrecuenciaPagoBeanBusiness frecuenciaPagoBeanBusiness = new FrecuenciaPagoBeanBusiness();

    if (nominaEspecial.getFrecuenciaPago() != null) {
      nominaEspecial.setFrecuenciaPago(
        frecuenciaPagoBeanBusiness.findFrecuenciaPagoById(
        nominaEspecial.getFrecuenciaPago().getIdFrecuenciaPago()));
    }

    BeanUtils.copyProperties(nominaEspecialModify, nominaEspecial);
  }

  public void deleteNominaEspecial(NominaEspecial nominaEspecial) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    NominaEspecial nominaEspecialDelete = 
      findNominaEspecialById(nominaEspecial.getIdNominaEspecial());
    pm.deletePersistent(nominaEspecialDelete);
  }

  public NominaEspecial findNominaEspecialById(long idNominaEspecial) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idNominaEspecial == pIdNominaEspecial";
    Query query = pm.newQuery(NominaEspecial.class, filter);

    query.declareParameters("long pIdNominaEspecial");

    parameters.put("pIdNominaEspecial", new Long(idNominaEspecial));

    Collection colNominaEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colNominaEspecial.iterator();
    return (NominaEspecial)iterator.next();
  }

  public Collection findNominaEspecialAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent nominaEspecialExtent = pm.getExtent(
      NominaEspecial.class, true);
    Query query = pm.newQuery(nominaEspecialExtent);
    query.setOrdering("anio ascending, numeroNomina ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina";

    Query query = pm.newQuery(NominaEspecial.class, filter);

    query.declareParameters("long pIdGrupoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));

    query.setOrdering("anio ascending, numeroNomina ascending");

    Collection colNominaEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNominaEspecial);

    return colNominaEspecial;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(NominaEspecial.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, numeroNomina ascending");

    Collection colNominaEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNominaEspecial);

    return colNominaEspecial;
  }

  public Collection findByEstatus(String estatus)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "estatus == pEstatus";

    Query query = pm.newQuery(NominaEspecial.class, filter);

    query.declareParameters("java.lang.String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pEstatus", new String(estatus));

    query.setOrdering("anio ascending, numeroNomina ascending");

    Collection colNominaEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNominaEspecial);

    return colNominaEspecial;
  }

  public Collection findByFrecuenciaPago(long idFrecuenciaPago)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "frecuenciaPago.idFrecuenciaPago == pIdFrecuenciaPago";

    Query query = pm.newQuery(NominaEspecial.class, filter);

    query.declareParameters("long pIdFrecuenciaPago");
    HashMap parameters = new HashMap();

    parameters.put("pIdFrecuenciaPago", new Long(idFrecuenciaPago));

    query.setOrdering("anio ascending, numeroNomina ascending");

    Collection colNominaEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNominaEspecial);

    return colNominaEspecial;
  }
}