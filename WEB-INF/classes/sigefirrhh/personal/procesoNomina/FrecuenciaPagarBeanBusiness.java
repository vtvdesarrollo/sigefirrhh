package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaPagoBeanBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;

public class FrecuenciaPagarBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addFrecuenciaPagar(FrecuenciaPagar frecuenciaPagar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    FrecuenciaPagar frecuenciaPagarNew = 
      (FrecuenciaPagar)BeanUtils.cloneBean(
      frecuenciaPagar);

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (frecuenciaPagarNew.getGrupoNomina() != null) {
      frecuenciaPagarNew.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        frecuenciaPagarNew.getGrupoNomina().getIdGrupoNomina()));
    }

    FrecuenciaPagoBeanBusiness frecuenciaPagoBeanBusiness = new FrecuenciaPagoBeanBusiness();

    if (frecuenciaPagarNew.getFrecuenciaPago() != null) {
      frecuenciaPagarNew.setFrecuenciaPago(
        frecuenciaPagoBeanBusiness.findFrecuenciaPagoById(
        frecuenciaPagarNew.getFrecuenciaPago().getIdFrecuenciaPago()));
    }
    pm.makePersistent(frecuenciaPagarNew);
  }

  public void updateFrecuenciaPagar(FrecuenciaPagar frecuenciaPagar) throws Exception
  {
    FrecuenciaPagar frecuenciaPagarModify = 
      findFrecuenciaPagarById(frecuenciaPagar.getIdFrecuenciaPagar());

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (frecuenciaPagar.getGrupoNomina() != null) {
      frecuenciaPagar.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        frecuenciaPagar.getGrupoNomina().getIdGrupoNomina()));
    }

    FrecuenciaPagoBeanBusiness frecuenciaPagoBeanBusiness = new FrecuenciaPagoBeanBusiness();

    if (frecuenciaPagar.getFrecuenciaPago() != null) {
      frecuenciaPagar.setFrecuenciaPago(
        frecuenciaPagoBeanBusiness.findFrecuenciaPagoById(
        frecuenciaPagar.getFrecuenciaPago().getIdFrecuenciaPago()));
    }

    BeanUtils.copyProperties(frecuenciaPagarModify, frecuenciaPagar);
  }

  public void deleteFrecuenciaPagar(FrecuenciaPagar frecuenciaPagar) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    FrecuenciaPagar frecuenciaPagarDelete = 
      findFrecuenciaPagarById(frecuenciaPagar.getIdFrecuenciaPagar());
    pm.deletePersistent(frecuenciaPagarDelete);
  }

  public FrecuenciaPagar findFrecuenciaPagarById(long idFrecuenciaPagar) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idFrecuenciaPagar == pIdFrecuenciaPagar";
    Query query = pm.newQuery(FrecuenciaPagar.class, filter);

    query.declareParameters("long pIdFrecuenciaPagar");

    parameters.put("pIdFrecuenciaPagar", new Long(idFrecuenciaPagar));

    Collection colFrecuenciaPagar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colFrecuenciaPagar.iterator();
    return (FrecuenciaPagar)iterator.next();
  }

  public Collection findFrecuenciaPagarAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent frecuenciaPagarExtent = pm.getExtent(
      FrecuenciaPagar.class, true);
    Query query = pm.newQuery(frecuenciaPagarExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina";

    Query query = pm.newQuery(FrecuenciaPagar.class, filter);

    query.declareParameters("long pIdGrupoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));

    Collection colFrecuenciaPagar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFrecuenciaPagar);

    return colFrecuenciaPagar;
  }
}