package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class NominaEspecialNoGenBeanBusiness extends AbstractBeanBusiness
{
  public int findLastNumeroNominaEspecialByGrupoNominaAndAnio(long idGrupoNomina, int anio)
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina && anio == pAnio";

    Query query = pm.newQuery(NominaEspecial.class, filter);

    HashMap parameters = new HashMap();

    query.setOrdering("numeroNomina descending");
    query.declareParameters("long pIdGrupoNomina, int pAnio");

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pAnio", new Integer(anio));

    Collection colNominaEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));
    if (!colNominaEspecial.isEmpty()) {
      Iterator iterator = colNominaEspecial.iterator();
      NominaEspecial nominaEspecial = (NominaEspecial)iterator.next();
      return nominaEspecial.getNumeroNomina();
    }

    return 0;
  }

  public int findLastNumeroNomina()
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Query query = pm.newQuery(NominaEspecial.class);
    HashMap parameters = new HashMap();

    Collection colNominaEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));
    if (!colNominaEspecial.isEmpty()) {
      Iterator iterator = colNominaEspecial.iterator();
      NominaEspecial nominaEspecial = (NominaEspecial)iterator.next();
      return nominaEspecial.getNumeroNomina();
    }

    return 0;
  }

  public Collection findByGrupoNominaAndEstatus(long idGrupoNomina, String estatus)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina && estatus == pEstatus ";

    Query query = pm.newQuery(NominaEspecial.class, filter);

    query.declareParameters("long pIdGrupoNomina, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pEstatus", estatus);

    query.setOrdering("anio ascending, numeroNomina ascending");

    Collection colNominaEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNominaEspecial);

    return colNominaEspecial;
  }

  public Collection findByGrupoNominaAndEstatusAndAnio(long idGrupoNomina, String estatus, int anio) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina && estatus == pEstatus && anio == pAnio";

    Query query = pm.newQuery(NominaEspecial.class, filter);

    query.declareParameters("long pIdGrupoNomina, String pEstatus, int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pEstatus", estatus);
    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, numeroNomina ascending");

    Collection colNominaEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNominaEspecial);

    return colNominaEspecial;
  }

  public Collection findByGrupoNominaAndEstatusAndMesAndAnio(long idGrupoNomina, String estatus, int mes, int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina && estatus == pEstatus && anio == pAnio && mes == pMes";

    Query query = pm.newQuery(NominaEspecial.class, filter);

    query.declareParameters("long pIdGrupoNomina, String pEstatus, int pAnio, int pMes");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pEstatus", estatus);
    parameters.put("pAnio", new Integer(anio));
    parameters.put("pMes", new Integer(mes));

    query.setOrdering("anio ascending, numeroNomina ascending");

    Collection colNominaEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNominaEspecial);

    return colNominaEspecial;
  }

  public Collection findByGrupoNominaAndEstatus(long idGrupoNomina, String estatus, String pagada)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina && estatus == pEstatus && pagada == pPagada ";

    Query query = pm.newQuery(NominaEspecial.class, filter);

    query.declareParameters("long pIdGrupoNomina, String pEstatus, String pPagada");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pEstatus", estatus);
    parameters.put("pPagada", pagada);

    query.setOrdering("anio ascending, numeroNomina ascending");

    Collection colNominaEspecial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNominaEspecial);

    return colNominaEspecial;
  }
}