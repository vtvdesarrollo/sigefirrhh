package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FirmasReportes;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportNominaForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportNominaForm.class.getName());
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private Map parameters = null;
  private String selectGrupoNomina;
  private String inicio;
  private String fin;
  private Calendar inicioAux;
  private Calendar finAux;
  private long idGrupoNomina;
  private Collection listGrupoNomina;
  private long firma1;
  private Collection listFirma1;
  private long firma2;
  private Collection listFirma2;
  private long firma3;
  private Collection listFirma3;
  private long idUnidadAdministradora;
  private Collection listUnidadAdministradora;
  private String periodicidad;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private Integer semanaMes;
  private Integer lunesPrQuincena;
  private Integer lunesSeQuincena;
  private Boolean tieneSemana5;
  private Integer semanaAnio;
  private boolean show;
  private boolean auxShow;
  private boolean showSemana;
  private String conceptoDesde = "0000";
  private String conceptoHasta = "9999";
  private int agrupacionRecibos = 1;
  private int distribucionRecibos = 1;

  private String mensaje = "";

  public ReportNominaForm()
  {
    this.reportName = "nominarac";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.parameters = new Hashtable();

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportNominaForm.this.cambiarNombreAReporte(ReportNominaForm.this.parameters);
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public boolean isShowConcepto() {
    return (this.tipoReporte >= 50) && (this.tipoReporte <= 99);
  }
  public boolean isShowRecibo() {
    return this.tipoReporte >= 500;
  }
  private void cambiarNombreAReporte(Map parameters) {
    this.reportName = "nominarac";

    if (this.tipoReporte == 10)
      this.reportName = "nominarac";
    else if (this.tipoReporte == 15)
      this.reportName = "nominaalf";
    else if (this.tipoReporte == 20)
      this.reportName = "nominapr";
    else if (this.tipoReporte == 25)
      this.reportName = "nominauel";
    else if (this.tipoReporte == 30)
      this.reportName = "nominareg";
    else if (this.tipoReporte == 35)
      this.reportName = "nominadep";
    else if (this.tipoReporte == 50)
      this.reportName = "detconceptonoalf";
    else if (this.tipoReporte == 55)
      this.reportName = "detconceptonocod";
    else if (this.tipoReporte == 60)
      this.reportName = "detconceptonoced";
    else if (this.tipoReporte == 61)
      this.reportName = "detconceptonodep";
    else if (this.tipoReporte == 100)
      this.reportName = "resconceptono";
    else if (this.tipoReporte == 105)
      this.reportName = "resconceptonouel";
    else if (this.tipoReporte == 110)
      this.reportName = "resconceptonocat";
    else if (this.tipoReporte == 115)
      this.reportName = "resconceptonocatuel";
    else if (this.tipoReporte == 120)
      this.reportName = "resaportesno";
    else if (this.tipoReporte == 125)
      this.reportName = "resaportesnouel";
    else if (this.tipoReporte == 130)
      this.reportName = "resconceptonouadm";
    else if (this.tipoReporte == 150)
      this.reportName = "depbannoalf";
    else if (this.tipoReporte == 155)
      this.reportName = "depbannocod";
    else if (this.tipoReporte == 160)
      this.reportName = "depbannoced";
    else if (this.tipoReporte == 165)
      this.reportName = "depbannoua";
    else if (this.tipoReporte == 170)
      this.reportName = "depbannocedtipo";
    else if (this.tipoReporte == 200)
      this.reportName = "chequesno";
    else if (this.tipoReporte == 205)
      this.reportName = "chequesnoua";
    else if (this.tipoReporte == 210)
      this.reportName = "chequesnouadep";
    else if (this.tipoReporte == 250)
      this.reportName = "embargono";
    else if (this.tipoReporte == 400)
      this.reportName = "netopagar";
    else if (this.tipoReporte == 405)
      this.reportName = "netopagarsc";
    else if (this.tipoReporte == 300)
      this.reportName = "aceppaguadm";
    else if (this.tipoReporte == 305)
      this.reportName = "aceppaglp";
    else if (this.tipoReporte == 310)
      this.reportName = "aceppagdep";
    else if (this.tipoReporte == 311)
      this.reportName = "aceppagdepss";
    else if (this.tipoReporte == 315)
      this.reportName = "aceppagdep0";
    else if (this.tipoReporte == 320)
      this.reportName = "aceppaguel0";
    else if (this.tipoReporte == 500)
      this.reportName = "recibosalf";
    else if (this.tipoReporte == 505)
      this.reportName = "reciboscod";
    else if (this.tipoReporte == 510) {
      this.reportName = "recibosced";
    }
    if (this.tipoReporte >= 500) {
      if (this.distribucionRecibos == 1)
        this.reportName += "1pptc";
      else if (this.distribucionRecibos == 2)
        this.reportName += "2pptc";
      else if (this.distribucionRecibos == 3)
        this.reportName += "3pptc";
      else if (this.distribucionRecibos == 4) {
        this.reportName += "4pptc";
      }
      if (this.agrupacionRecibos == 2)
        this.reportName += "dep";
      else if (this.agrupacionRecibos == 3) {
        this.reportName += "uel";
      }
    }
    if (this.idUnidadAdministradora != 0L) {
      this.reportName += "_ua";
      parameters.put("id_unidad_administradora", new Long(this.idUnidadAdministradora));
    }
  }

  public void changeGrupoNomina(ValueChangeEvent event) {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.showSemana = false;
      Collection colPeriodo = this.procesoNominaNoGenFacade.findFechaUltimaNomina(this.idGrupoNomina);
      Iterator iteratorPeriodo = colPeriodo.iterator();
      if (iteratorPeriodo.hasNext()) {
        this.inicioAux = ((Calendar)iteratorPeriodo.next());
        this.finAux = ((Calendar)iteratorPeriodo.next());
        this.lunesPrQuincena = ((Integer)iteratorPeriodo.next());
        this.lunesSeQuincena = ((Integer)iteratorPeriodo.next());
        this.tieneSemana5 = ((Boolean)iteratorPeriodo.next());
        this.semanaMes = ((Integer)iteratorPeriodo.next());
        this.semanaAnio = ((Integer)iteratorPeriodo.next());
        this.periodicidad = ((String)iteratorPeriodo.next());
        this.auxShow = true;
        if (this.periodicidad.equals("S"))
          this.showSemana = true;
      }
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("ReportNominaForm / changeGrupoNomina(): Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      this.listUnidadAdministradora = this.estructuraFacade.findUnidadAdministradoraByOrganismo(this.login.getIdOrganismo());
      this.listFirma1 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());
      this.listFirma2 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());
      this.listFirma3 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("ReportNominaForm / refresh() : Excepcion controlada:", e);
      this.listGrupoNomina = new ArrayList();
      this.listUnidadAdministradora = new ArrayList();
      this.listFirma1 = new ArrayList();
      this.listFirma2 = new ArrayList();
      this.listFirma3 = new ArrayList();
    }
  }

  public String runReport() {
    this.parameters = new Hashtable();

    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      this.parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      this.parameters.put("numero_nomina", new Integer(0));

      Date fec_ini = new Date(this.inicioAux.getTime().getYear(), this.inicioAux.getTime().getMonth(), this.inicioAux.getTime().getDate());
      Date fec_fin = new Date(this.finAux.getTime().getYear(), this.finAux.getTime().getMonth(), this.finAux.getTime().getDate());

      this.parameters.put("fec_ini", fec_ini);
      this.parameters.put("fec_fin", fec_fin);
      this.parameters.put("anio", new Integer(fec_ini.getYear()));
      this.parameters.put("id_grupo_nomina", new Long(this.idGrupoNomina));
      this.parameters.put("semana_anio", this.semanaAnio);
      this.parameters.put("nomina_estatus", "A");
      this.parameters.put("nombre_nomina", "ORDINARIA");
      if (this.tipoReporte > 99) {
        this.parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/procesoNomina");
      }
      if ((this.tipoReporte > 99) && (this.mensaje != null)) {
        this.parameters.put("mensaje", this.mensaje);
      }

      if (this.firma1 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma1);
        this.parameters.put("nombre_uno", firma.getNombre());
        this.parameters.put("cargo_uno", firma.getCargo());
        if (firma.getNombramiento() == null)
          this.parameters.put("nombramiento_uno", "");
        else {
          this.parameters.put("nombramiento_uno", firma.getNombramiento());
        }
      }
      if (this.firma2 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma2);
        this.parameters.put("nombre_dos", firma.getNombre());
        this.parameters.put("cargo_dos", firma.getCargo());
        if (firma.getNombramiento() == null)
          this.parameters.put("nombramiento_dos", "");
        else {
          this.parameters.put("nombramiento_dos", firma.getNombramiento());
        }
      }
      if (this.firma3 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma3);
        this.parameters.put("nombre_tres", firma.getNombre());
        this.parameters.put("cargo_tres", firma.getCargo());
        if (firma.getNombramiento() == null)
          this.parameters.put("nombramiento_tres", "");
        else {
          this.parameters.put("nombramiento_tres", firma.getNombramiento());
        }
      }

      if ((this.tipoReporte >= 50) && (this.tipoReporte <= 99)) {
        this.parameters.put("cod_concepto_desde", this.conceptoDesde);
        this.parameters.put("cod_concepto_hasta", this.conceptoHasta);
        log.error("ReportNominaForm / runReport () : cod_concepto_desde" + this.conceptoDesde);
        log.error("ReportNominaForm / runReport () : cod_concepto_hasta" + this.conceptoHasta);
      }

      cambiarNombreAReporte(this.parameters);

      JasperForWeb report = new JasperForWeb();

      report.setReportName(this.reportName);
      report.setParameters(this.parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/procesoNomina");

      log.error("ReportNominaForm / runReport () : Nombre de reporte " + this.reportName + " Path " + report.getPath());

      report.start();
      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("ReportNominaForm / runReport () : Excepcion controlada:", e);
    }

    return null;
  }

  public Collection getListGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getListFirma1() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma1, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public Collection getListFirma2() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma2, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public Collection getListFirma3() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma3, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }

  public String getSelectGrupoNomina()
  {
    return this.selectGrupoNomina;
  }

  public void setSelectGrupoNomina(String string)
  {
    this.selectGrupoNomina = string;
  }

  public String getFin()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }

  public void setFin(String string)
  {
    this.fin = string;
  }

  public void setInicio(String string)
  {
    this.inicio = string;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public long getIdGrupoNomina()
  {
    return this.idGrupoNomina;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setIdGrupoNomina(long l)
  {
    this.idGrupoNomina = l;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public long getFirma1()
  {
    return this.firma1;
  }

  public long getFirma2()
  {
    return this.firma2;
  }

  public long getFirma3()
  {
    return this.firma3;
  }

  public void setFirma1(long l)
  {
    this.firma1 = l;
  }

  public void setFirma2(long l)
  {
    this.firma2 = l;
  }

  public void setFirma3(long l)
  {
    this.firma3 = l;
  }

  public long getIdUnidadAdministradora()
  {
    return this.idUnidadAdministradora;
  }

  public void setIdUnidadAdministradora(long l)
  {
    this.idUnidadAdministradora = l;
  }

  public String getMensaje() {
    return this.mensaje;
  }
  public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }
  public Integer getSemanaAnio() {
    return this.semanaAnio;
  }
  public void setSemanaAnio(Integer semanaAnio) {
    this.semanaAnio = semanaAnio;
  }
  public boolean isShowSemana() {
    return this.showSemana;
  }
  public int getAgrupacionRecibos() {
    return this.agrupacionRecibos;
  }
  public void setAgrupacionRecibos(int agrupacionRecibos) {
    this.agrupacionRecibos = agrupacionRecibos;
  }
  public String getConceptoDesde() {
    return this.conceptoDesde;
  }
  public void setConceptoDesde(String conceptoDesde) {
    this.conceptoDesde = conceptoDesde;
  }
  public String getConceptoHasta() {
    return this.conceptoHasta;
  }
  public void setConceptoHasta(String conceptoHasta) {
    this.conceptoHasta = conceptoHasta;
  }
  public int getDistribucionRecibos() {
    return this.distribucionRecibos;
  }
  public void setDistribucionRecibos(int distribucionRecibos) {
    this.distribucionRecibos = distribucionRecibos;
  }
}