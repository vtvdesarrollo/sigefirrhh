package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class SeguridadOrdinariaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SeguridadOrdinariaForm.class.getName());
  private SeguridadOrdinaria seguridadOrdinaria;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private ProcesoNominaFacade procesoNominaFacade = new ProcesoNominaFacade();
  private boolean showSeguridadOrdinariaByAnio;
  private boolean showSeguridadOrdinariaByGrupoNomina;
  private int findAnio;
  private String findSelectGrupoNomina;
  private Collection findColGrupoNomina;
  private Collection colGrupoNomina;
  private String selectGrupoNomina;
  private Object stateResultSeguridadOrdinariaByAnio = null;

  private Object stateResultSeguridadOrdinariaByGrupoNomina = null;

  public int getFindAnio()
  {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }
  public String getFindSelectGrupoNomina() {
    return this.findSelectGrupoNomina;
  }
  public void setFindSelectGrupoNomina(String valGrupoNomina) {
    this.findSelectGrupoNomina = valGrupoNomina;
  }

  public Collection getFindColGrupoNomina() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoNomina.getIdGrupoNomina()), 
        grupoNomina.toString()));
    }
    return col;
  }

  public String getSelectGrupoNomina()
  {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String valGrupoNomina) {
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    this.seguridadOrdinaria.setGrupoNomina(null);
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      if (String.valueOf(grupoNomina.getIdGrupoNomina()).equals(
        valGrupoNomina)) {
        this.seguridadOrdinaria.setGrupoNomina(
          grupoNomina);
        break;
      }
    }
    this.selectGrupoNomina = valGrupoNomina;
  }
  public Collection getResult() {
    return this.result;
  }

  public SeguridadOrdinaria getSeguridadOrdinaria() {
    if (this.seguridadOrdinaria == null) {
      this.seguridadOrdinaria = new SeguridadOrdinaria();
    }
    return this.seguridadOrdinaria;
  }

  public SeguridadOrdinariaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoNomina.getIdGrupoNomina()), 
        grupoNomina.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColGrupoNomina = 
        this.definicionesFacade.findGrupoNominaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colGrupoNomina = 
        this.definicionesFacade.findGrupoNominaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSeguridadOrdinariaByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.procesoNominaFacade.findSeguridadOrdinariaByAnio(this.findAnio);
      this.showSeguridadOrdinariaByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSeguridadOrdinariaByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findSelectGrupoNomina = null;

    return null;
  }

  public String findSeguridadOrdinariaByGrupoNomina()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.procesoNominaFacade.findSeguridadOrdinariaByGrupoNomina(Long.valueOf(this.findSelectGrupoNomina).longValue());
      this.showSeguridadOrdinariaByGrupoNomina = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSeguridadOrdinariaByGrupoNomina)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findSelectGrupoNomina = null;

    return null;
  }

  public boolean isShowSeguridadOrdinariaByAnio() {
    return this.showSeguridadOrdinariaByAnio;
  }
  public boolean isShowSeguridadOrdinariaByGrupoNomina() {
    return this.showSeguridadOrdinariaByGrupoNomina;
  }

  public String selectSeguridadOrdinaria()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectGrupoNomina = null;

    long idSeguridadOrdinaria = 
      Long.parseLong((String)requestParameterMap.get("idSeguridadOrdinaria"));
    try
    {
      this.seguridadOrdinaria = 
        this.procesoNominaFacade.findSeguridadOrdinariaById(
        idSeguridadOrdinaria);
      if (this.seguridadOrdinaria.getGrupoNomina() != null) {
        this.selectGrupoNomina = 
          String.valueOf(this.seguridadOrdinaria.getGrupoNomina().getIdGrupoNomina());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.seguridadOrdinaria = null;
    this.showSeguridadOrdinariaByAnio = false;
    this.showSeguridadOrdinariaByGrupoNomina = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.seguridadOrdinaria.getFechaInicio() != null) && 
      (this.seguridadOrdinaria.getFechaInicio().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Inicio período no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.seguridadOrdinaria.getFechaFin() != null) && 
      (this.seguridadOrdinaria.getFechaFin().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Fin período no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.seguridadOrdinaria.getFechaProceso() != null) && 
      (this.seguridadOrdinaria.getFechaProceso().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha de Proceso no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.procesoNominaFacade.addSeguridadOrdinaria(
          this.seguridadOrdinaria);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.procesoNominaFacade.updateSeguridadOrdinaria(
          this.seguridadOrdinaria);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.procesoNominaFacade.deleteSeguridadOrdinaria(
        this.seguridadOrdinaria);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.seguridadOrdinaria = new SeguridadOrdinaria();

    this.selectGrupoNomina = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.seguridadOrdinaria.setIdSeguridadOrdinaria(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.procesoNomina.SeguridadOrdinaria"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.seguridadOrdinaria = new SeguridadOrdinaria();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}