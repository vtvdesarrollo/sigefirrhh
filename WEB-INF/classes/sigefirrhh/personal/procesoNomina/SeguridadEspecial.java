package sigefirrhh.personal.procesoNomina;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.GrupoNomina;

public class SeguridadEspecial
  implements Serializable, PersistenceCapable
{
  private long idSeguridadEspecial;
  private int anio;
  private int mes;
  private Date fechaInicio;
  private Date fechaFin;
  private int semanaQuincena;
  private Date fechaProceso;
  private String usuario;
  private NominaEspecial nominaEspecial;
  private GrupoNomina grupoNomina;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "fechaFin", "fechaInicio", "fechaProceso", "grupoNomina", "idSeguridadEspecial", "mes", "nominaEspecial", "semanaQuincena", "usuario" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.procesoNomina.NominaEspecial"), Integer.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 26, 24, 21, 26, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetanio(this) + "  -  " + 
      jdoGetmes(this) + "  -  " + 
      jdoGetnominaEspecial(this).toString() + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this)) + "  -  " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaFin(this));
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public Date getFechaFin()
  {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio()
  {
    return jdoGetfechaInicio(this);
  }

  public Date getFechaProceso()
  {
    return jdoGetfechaProceso(this);
  }

  public GrupoNomina getGrupoNomina()
  {
    return jdoGetgrupoNomina(this);
  }

  public long getIdSeguridadEspecial()
  {
    return jdoGetidSeguridadEspecial(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public NominaEspecial getNominaEspecial()
  {
    return jdoGetnominaEspecial(this);
  }

  public int getSemanaQuincena()
  {
    return jdoGetsemanaQuincena(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setFechaFin(Date date)
  {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date)
  {
    jdoSetfechaInicio(this, date);
  }

  public void setFechaProceso(Date date)
  {
    jdoSetfechaProceso(this, date);
  }

  public void setGrupoNomina(GrupoNomina nomina)
  {
    jdoSetgrupoNomina(this, nomina);
  }

  public void setIdSeguridadEspecial(long l)
  {
    jdoSetidSeguridadEspecial(this, l);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setNominaEspecial(NominaEspecial especial)
  {
    jdoSetnominaEspecial(this, especial);
  }

  public void setSemanaQuincena(int i)
  {
    jdoSetsemanaQuincena(this, i);
  }

  public String getUsuario()
  {
    return jdoGetusuario(this);
  }
  public void setUsuario(String usuario) {
    jdoSetusuario(this, usuario);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoNomina.SeguridadEspecial"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SeguridadEspecial());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SeguridadEspecial localSeguridadEspecial = new SeguridadEspecial();
    localSeguridadEspecial.jdoFlags = 1;
    localSeguridadEspecial.jdoStateManager = paramStateManager;
    return localSeguridadEspecial;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SeguridadEspecial localSeguridadEspecial = new SeguridadEspecial();
    localSeguridadEspecial.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSeguridadEspecial.jdoFlags = 1;
    localSeguridadEspecial.jdoStateManager = paramStateManager;
    return localSeguridadEspecial;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSeguridadEspecial);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nominaEspecial);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanaQuincena);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSeguridadEspecial = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nominaEspecial = ((NominaEspecial)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanaQuincena = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SeguridadEspecial paramSeguridadEspecial, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSeguridadEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramSeguridadEspecial.anio;
      return;
    case 1:
      if (paramSeguridadEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramSeguridadEspecial.fechaFin;
      return;
    case 2:
      if (paramSeguridadEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramSeguridadEspecial.fechaInicio;
      return;
    case 3:
      if (paramSeguridadEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramSeguridadEspecial.fechaProceso;
      return;
    case 4:
      if (paramSeguridadEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramSeguridadEspecial.grupoNomina;
      return;
    case 5:
      if (paramSeguridadEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.idSeguridadEspecial = paramSeguridadEspecial.idSeguridadEspecial;
      return;
    case 6:
      if (paramSeguridadEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramSeguridadEspecial.mes;
      return;
    case 7:
      if (paramSeguridadEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.nominaEspecial = paramSeguridadEspecial.nominaEspecial;
      return;
    case 8:
      if (paramSeguridadEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.semanaQuincena = paramSeguridadEspecial.semanaQuincena;
      return;
    case 9:
      if (paramSeguridadEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramSeguridadEspecial.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SeguridadEspecial))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SeguridadEspecial localSeguridadEspecial = (SeguridadEspecial)paramObject;
    if (localSeguridadEspecial.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSeguridadEspecial, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SeguridadEspecialPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SeguridadEspecialPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadEspecialPK))
      throw new IllegalArgumentException("arg1");
    SeguridadEspecialPK localSeguridadEspecialPK = (SeguridadEspecialPK)paramObject;
    localSeguridadEspecialPK.idSeguridadEspecial = this.idSeguridadEspecial;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadEspecialPK))
      throw new IllegalArgumentException("arg1");
    SeguridadEspecialPK localSeguridadEspecialPK = (SeguridadEspecialPK)paramObject;
    this.idSeguridadEspecial = localSeguridadEspecialPK.idSeguridadEspecial;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadEspecialPK))
      throw new IllegalArgumentException("arg2");
    SeguridadEspecialPK localSeguridadEspecialPK = (SeguridadEspecialPK)paramObject;
    localSeguridadEspecialPK.idSeguridadEspecial = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadEspecialPK))
      throw new IllegalArgumentException("arg2");
    SeguridadEspecialPK localSeguridadEspecialPK = (SeguridadEspecialPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localSeguridadEspecialPK.idSeguridadEspecial);
  }

  private static final int jdoGetanio(SeguridadEspecial paramSeguridadEspecial)
  {
    if (paramSeguridadEspecial.jdoFlags <= 0)
      return paramSeguridadEspecial.anio;
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadEspecial.anio;
    if (localStateManager.isLoaded(paramSeguridadEspecial, jdoInheritedFieldCount + 0))
      return paramSeguridadEspecial.anio;
    return localStateManager.getIntField(paramSeguridadEspecial, jdoInheritedFieldCount + 0, paramSeguridadEspecial.anio);
  }

  private static final void jdoSetanio(SeguridadEspecial paramSeguridadEspecial, int paramInt)
  {
    if (paramSeguridadEspecial.jdoFlags == 0)
    {
      paramSeguridadEspecial.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadEspecial.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadEspecial, jdoInheritedFieldCount + 0, paramSeguridadEspecial.anio, paramInt);
  }

  private static final Date jdoGetfechaFin(SeguridadEspecial paramSeguridadEspecial)
  {
    if (paramSeguridadEspecial.jdoFlags <= 0)
      return paramSeguridadEspecial.fechaFin;
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadEspecial.fechaFin;
    if (localStateManager.isLoaded(paramSeguridadEspecial, jdoInheritedFieldCount + 1))
      return paramSeguridadEspecial.fechaFin;
    return (Date)localStateManager.getObjectField(paramSeguridadEspecial, jdoInheritedFieldCount + 1, paramSeguridadEspecial.fechaFin);
  }

  private static final void jdoSetfechaFin(SeguridadEspecial paramSeguridadEspecial, Date paramDate)
  {
    if (paramSeguridadEspecial.jdoFlags == 0)
    {
      paramSeguridadEspecial.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadEspecial.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadEspecial, jdoInheritedFieldCount + 1, paramSeguridadEspecial.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(SeguridadEspecial paramSeguridadEspecial)
  {
    if (paramSeguridadEspecial.jdoFlags <= 0)
      return paramSeguridadEspecial.fechaInicio;
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadEspecial.fechaInicio;
    if (localStateManager.isLoaded(paramSeguridadEspecial, jdoInheritedFieldCount + 2))
      return paramSeguridadEspecial.fechaInicio;
    return (Date)localStateManager.getObjectField(paramSeguridadEspecial, jdoInheritedFieldCount + 2, paramSeguridadEspecial.fechaInicio);
  }

  private static final void jdoSetfechaInicio(SeguridadEspecial paramSeguridadEspecial, Date paramDate)
  {
    if (paramSeguridadEspecial.jdoFlags == 0)
    {
      paramSeguridadEspecial.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadEspecial.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadEspecial, jdoInheritedFieldCount + 2, paramSeguridadEspecial.fechaInicio, paramDate);
  }

  private static final Date jdoGetfechaProceso(SeguridadEspecial paramSeguridadEspecial)
  {
    if (paramSeguridadEspecial.jdoFlags <= 0)
      return paramSeguridadEspecial.fechaProceso;
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadEspecial.fechaProceso;
    if (localStateManager.isLoaded(paramSeguridadEspecial, jdoInheritedFieldCount + 3))
      return paramSeguridadEspecial.fechaProceso;
    return (Date)localStateManager.getObjectField(paramSeguridadEspecial, jdoInheritedFieldCount + 3, paramSeguridadEspecial.fechaProceso);
  }

  private static final void jdoSetfechaProceso(SeguridadEspecial paramSeguridadEspecial, Date paramDate)
  {
    if (paramSeguridadEspecial.jdoFlags == 0)
    {
      paramSeguridadEspecial.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadEspecial.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadEspecial, jdoInheritedFieldCount + 3, paramSeguridadEspecial.fechaProceso, paramDate);
  }

  private static final GrupoNomina jdoGetgrupoNomina(SeguridadEspecial paramSeguridadEspecial)
  {
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadEspecial.grupoNomina;
    if (localStateManager.isLoaded(paramSeguridadEspecial, jdoInheritedFieldCount + 4))
      return paramSeguridadEspecial.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramSeguridadEspecial, jdoInheritedFieldCount + 4, paramSeguridadEspecial.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(SeguridadEspecial paramSeguridadEspecial, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadEspecial.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramSeguridadEspecial, jdoInheritedFieldCount + 4, paramSeguridadEspecial.grupoNomina, paramGrupoNomina);
  }

  private static final long jdoGetidSeguridadEspecial(SeguridadEspecial paramSeguridadEspecial)
  {
    return paramSeguridadEspecial.idSeguridadEspecial;
  }

  private static final void jdoSetidSeguridadEspecial(SeguridadEspecial paramSeguridadEspecial, long paramLong)
  {
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadEspecial.idSeguridadEspecial = paramLong;
      return;
    }
    localStateManager.setLongField(paramSeguridadEspecial, jdoInheritedFieldCount + 5, paramSeguridadEspecial.idSeguridadEspecial, paramLong);
  }

  private static final int jdoGetmes(SeguridadEspecial paramSeguridadEspecial)
  {
    if (paramSeguridadEspecial.jdoFlags <= 0)
      return paramSeguridadEspecial.mes;
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadEspecial.mes;
    if (localStateManager.isLoaded(paramSeguridadEspecial, jdoInheritedFieldCount + 6))
      return paramSeguridadEspecial.mes;
    return localStateManager.getIntField(paramSeguridadEspecial, jdoInheritedFieldCount + 6, paramSeguridadEspecial.mes);
  }

  private static final void jdoSetmes(SeguridadEspecial paramSeguridadEspecial, int paramInt)
  {
    if (paramSeguridadEspecial.jdoFlags == 0)
    {
      paramSeguridadEspecial.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadEspecial.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadEspecial, jdoInheritedFieldCount + 6, paramSeguridadEspecial.mes, paramInt);
  }

  private static final NominaEspecial jdoGetnominaEspecial(SeguridadEspecial paramSeguridadEspecial)
  {
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadEspecial.nominaEspecial;
    if (localStateManager.isLoaded(paramSeguridadEspecial, jdoInheritedFieldCount + 7))
      return paramSeguridadEspecial.nominaEspecial;
    return (NominaEspecial)localStateManager.getObjectField(paramSeguridadEspecial, jdoInheritedFieldCount + 7, paramSeguridadEspecial.nominaEspecial);
  }

  private static final void jdoSetnominaEspecial(SeguridadEspecial paramSeguridadEspecial, NominaEspecial paramNominaEspecial)
  {
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadEspecial.nominaEspecial = paramNominaEspecial;
      return;
    }
    localStateManager.setObjectField(paramSeguridadEspecial, jdoInheritedFieldCount + 7, paramSeguridadEspecial.nominaEspecial, paramNominaEspecial);
  }

  private static final int jdoGetsemanaQuincena(SeguridadEspecial paramSeguridadEspecial)
  {
    if (paramSeguridadEspecial.jdoFlags <= 0)
      return paramSeguridadEspecial.semanaQuincena;
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadEspecial.semanaQuincena;
    if (localStateManager.isLoaded(paramSeguridadEspecial, jdoInheritedFieldCount + 8))
      return paramSeguridadEspecial.semanaQuincena;
    return localStateManager.getIntField(paramSeguridadEspecial, jdoInheritedFieldCount + 8, paramSeguridadEspecial.semanaQuincena);
  }

  private static final void jdoSetsemanaQuincena(SeguridadEspecial paramSeguridadEspecial, int paramInt)
  {
    if (paramSeguridadEspecial.jdoFlags == 0)
    {
      paramSeguridadEspecial.semanaQuincena = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadEspecial.semanaQuincena = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadEspecial, jdoInheritedFieldCount + 8, paramSeguridadEspecial.semanaQuincena, paramInt);
  }

  private static final String jdoGetusuario(SeguridadEspecial paramSeguridadEspecial)
  {
    if (paramSeguridadEspecial.jdoFlags <= 0)
      return paramSeguridadEspecial.usuario;
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadEspecial.usuario;
    if (localStateManager.isLoaded(paramSeguridadEspecial, jdoInheritedFieldCount + 9))
      return paramSeguridadEspecial.usuario;
    return localStateManager.getStringField(paramSeguridadEspecial, jdoInheritedFieldCount + 9, paramSeguridadEspecial.usuario);
  }

  private static final void jdoSetusuario(SeguridadEspecial paramSeguridadEspecial, String paramString)
  {
    if (paramSeguridadEspecial.jdoFlags == 0)
    {
      paramSeguridadEspecial.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramSeguridadEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadEspecial.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramSeguridadEspecial, jdoInheritedFieldCount + 9, paramSeguridadEspecial.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}