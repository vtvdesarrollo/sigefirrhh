package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class GenerarPrenominaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarPrenominaForm.class.getName());
  private String selectGrupoNomina;
  private String inicio;
  private String fin;
  private Calendar inicioAux;
  private Calendar finAux;
  private long idGrupoNomina;
  private Collection listGrupoNomina;
  private String periodicidad;
  private String recalculo;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private Integer semanaMes;
  private Integer numeroSemanasMes;
  private Integer lunesPrQuincena;
  private Integer lunesSeQuincena;
  private Boolean tieneSemana5;
  private boolean show;
  private boolean auxShow;
  private boolean showSemana;
  private Integer semanaAnio;
  private Collection mensajesUltimaPrenomina;
  private boolean showMensajesPrenomina = false;

  public GenerarPrenominaForm()
  {
    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void changeGrupoNomina(ValueChangeEvent event)
  {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    this.showSemana = false;
    try {
      Collection colPeriodo = this.procesoNominaNoGenFacade.findFechaProximaNomina(this.idGrupoNomina);
      Iterator iteratorPeriodo = colPeriodo.iterator();
      if (iteratorPeriodo.hasNext()) {
        this.inicioAux = ((Calendar)iteratorPeriodo.next());
        this.finAux = ((Calendar)iteratorPeriodo.next());
        this.lunesPrQuincena = ((Integer)iteratorPeriodo.next());
        this.lunesSeQuincena = ((Integer)iteratorPeriodo.next());
        this.tieneSemana5 = ((Boolean)iteratorPeriodo.next());
        this.semanaMes = ((Integer)iteratorPeriodo.next());
        this.semanaAnio = ((Integer)iteratorPeriodo.next());
        this.periodicidad = ((String)iteratorPeriodo.next());
        this.numeroSemanasMes = ((Integer)iteratorPeriodo.next());

        this.auxShow = true;
        if (this.periodicidad.equals("S"))
          this.showSemana = true;
      }
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      log.error("Vacio:" + this.listGrupoNomina.isEmpty());
    }
    catch (Exception e) {
      log.error(e.getCause());
      this.listGrupoNomina = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    log.error("PASO !");
    log.error("idGrupoNomina " + this.idGrupoNomina);
    log.error("Desde " + this.inicio);
    log.error("Hasta " + this.fin);
    log.error("Periodicidad" + this.periodicidad);
    log.error("Recalculo" + this.recalculo);
    log.error("idorganimo " + this.login.getOrganismo().getIdOrganismo());

    GrupoNomina grupoNomina = this.definicionesFacade.findGrupoNominaById(this.idGrupoNomina);
    try {
      boolean estado = this.procesoNominaNoGenFacade.generarPrenomina(this.idGrupoNomina, this.periodicidad, this.recalculo, this.inicioAux.getTime(), this.finAux.getTime(), this.login.getOrganismo().getIdOrganismo(), this.lunesPrQuincena, this.lunesSeQuincena, this.tieneSemana5, this.semanaMes, this.numeroSemanasMes);
      this.mensajesUltimaPrenomina = this.procesoNominaNoGenFacade.getMensajesUltimaPrenomina(new Long(this.idGrupoNomina));
      if ((this.mensajesUltimaPrenomina != null) && (!this.mensajesUltimaPrenomina.isEmpty()))
      {
        setShowMensajesPrenomina(true);
      }
      else setShowMensajesPrenomina(false);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', grupoNomina);

      if (estado)
        context.addMessage("success_add", new FacesMessage("Se generó con éxito. Hay Trabajadores Sobregirados. Imprima el reporte pertinente"));
      else
        context.addMessage("success_add", new FacesMessage("Se generó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getRecalculo()
  {
    return this.recalculo;
  }
  public void setRecalculo(String recalculo) {
    this.recalculo = recalculo;
  }

  public String getSelectGrupoNomina()
  {
    return this.selectGrupoNomina;
  }

  public void setSelectGrupoNomina(String string)
  {
    this.selectGrupoNomina = string;
  }

  public String getFin()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }

  public void setFin(String string)
  {
    this.fin = string;
  }

  public void setInicio(String string)
  {
    this.inicio = string;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public Integer getSemanaAnio() {
    return this.semanaAnio;
  }
  public void setSemanaAnio(Integer semanaAnio) {
    this.semanaAnio = semanaAnio;
  }
  public boolean isShowSemana() {
    return this.showSemana;
  }

  public Collection getMensajesUltimaPrenomina() {
    return this.mensajesUltimaPrenomina;
  }

  public void setMensajesUltimaPrenomina(Collection mensajesUltimaPrenomina)
  {
    this.mensajesUltimaPrenomina = mensajesUltimaPrenomina;
  }

  public boolean isShowMensajesPrenomina()
  {
    return this.showMensajesPrenomina;
  }

  public void setShowMensajesPrenomina(boolean showMensajesPrenomina)
  {
    this.showMensajesPrenomina = showMensajesPrenomina;
  }
}