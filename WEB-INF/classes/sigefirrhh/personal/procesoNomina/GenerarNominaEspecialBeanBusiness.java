package sigefirrhh.personal.procesoNomina;

import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesBusiness;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaPagoBeanBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;
import sigefirrhh.personal.trabajador.ConceptoFijoNoGenBeanBusiness;
import sigefirrhh.personal.trabajador.ConceptoVariableNoGenBeanBusiness;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class GenerarNominaEspecialBeanBusiness
{
  Logger log = Logger.getLogger(GenerarNominaEspecialBeanBusiness.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DefinicionesBusiness definicionesBusiness = new DefinicionesBusiness();
  private ConceptoFijoNoGenBeanBusiness conceptoFijoNoGenBeanBusiness = new ConceptoFijoNoGenBeanBusiness();
  private ConceptoVariableNoGenBeanBusiness conceptoVariableNoGenBeanBusiness = new ConceptoVariableNoGenBeanBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public boolean generar(long idGrupoNomina, java.util.Date fechaInicio, java.util.Date fechaFin, long idOrganismo, long numeroNomina, long idFrecuenciaPago, long idNominaEspecial, String usuario, String estatus, int anio, int mes)
    throws Exception
  {
    java.sql.Date fechaSql = new java.sql.Date(new java.util.Date().getYear(), new java.util.Date().getMonth(), new java.util.Date().getDate());

    int semanaQuincena = 0;
    semanaQuincena = 1;

    java.sql.Date fechaInicioSql = new java.sql.Date(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDate());
    java.sql.Date fechaFinSql = new java.sql.Date(fechaFin.getYear(), fechaFin.getMonth(), fechaFin.getDate());

    boolean sobregirados = false;

    GrupoNomina grupoNomina = new GrupoNomina();
    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();
    grupoNomina = grupoNominaBeanBusiness.findGrupoNominaById(idGrupoNomina);

    String periodicidad = grupoNomina.getPeriodicidad();

    FrecuenciaPagoBeanBusiness frecuenciaPagoBeanBusiness = new FrecuenciaPagoBeanBusiness();

    FrecuenciaPago frecuenciaPago = frecuenciaPagoBeanBusiness.findFrecuenciaPagoById(idFrecuenciaPago);

    this.log.error("1- false");
    this.log.error("2- false");
    this.log.error("3- " + idGrupoNomina);
    this.log.error("4- " + periodicidad);
    this.log.error("5- N");
    this.log.error("6- " + fechaInicioSql);
    this.log.error("7- " + fechaFinSql);
    this.log.error("8- " + idOrganismo);
    this.log.error("9- 0");
    this.log.error("10- 0");
    this.log.error("11- false");
    this.log.error("12- 0");
    this.log.error("13- " + usuario);
    this.log.error("14- 0");
    this.log.error("15- 0");
    this.log.error("16- false");
    this.log.error("17- " + semanaQuincena);
    this.log.error("18- " + numeroNomina);
    this.log.error("19- " + fechaSql);
    this.log.error("20- " + mes);
    this.log.error("21- " + anio);
    this.log.error("22- null");
    this.log.error("23- null");
    this.log.error("24- null");
    this.log.error("25- null");
    this.log.error("26- " + idNominaEspecial);
    this.log.error("27- " + frecuenciaPago.getCodFrecuenciaPago());
    this.log.error("28- " + estatus);
    this.log.error("29- N");

    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      StringBuffer sql = new StringBuffer();
      sql.append("select generar_nomina(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setBoolean(1, false);
      st.setBoolean(2, false);
      st.setLong(3, idGrupoNomina);
      st.setString(4, periodicidad);
      st.setString(5, "N");
      st.setDate(6, fechaInicioSql);
      st.setDate(7, fechaFinSql);
      st.setLong(8, idOrganismo);
      st.setInt(9, 0);
      st.setInt(10, 0);
      st.setBoolean(11, false);
      st.setInt(12, 0);
      st.setString(13, usuario);
      st.setInt(14, 0);
      st.setInt(15, 0);
      st.setBoolean(16, false);
      st.setInt(17, semanaQuincena);
      st.setLong(18, numeroNomina);
      st.setDate(19, fechaSql);
      st.setInt(20, mes);
      st.setInt(21, anio);
      st.setDate(22, null);
      st.setDate(23, null);
      st.setDate(24, null);
      st.setDate(25, null);
      st.setLong(26, idNominaEspecial);
      st.setInt(27, frecuenciaPago.getCodFrecuenciaPago());
      st.setString(28, estatus);
      st.setString(29, "N");

      int valor = 0;
      rs = st.executeQuery();
      rs.next();
      valor = rs.getInt(1);
      connection.commit();

      this.log.error("ejecutó la nomina especial");

      if (valor == 1) {
        this.log.error("HAY SOBREGIRADOS");
        return true;
      }
      int valor;
      StringBuffer sql;
      if (valor == 100) {
        this.log.error("La integridad de los datos, impide grabar en los historicos");
        ErrorSistema error = new ErrorSistema();
        error.setDescription("La integridad de los datos, impide grabar en los historicos. Verifique con el reporte");
        throw error;
      }
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
    if (rs != null) try {
        rs.close();
      } catch (Exception localException6) {
      } if (st != null) try {
        st.close();
      } catch (Exception localException7) {
      } if (connection != null) try {
        connection.close(); connection = null;
      }
      catch (Exception localException8)
      {
      } return false;
  }
}