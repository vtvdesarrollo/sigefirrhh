package sigefirrhh.personal.procesoNomina;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class ConceptoExterno
  implements Serializable, PersistenceCapable
{
  private long idConceptoExterno;
  private double unidades;
  private double monto;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "idConceptoExterno", "monto", "trabajador", "unidades" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Long.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), Double.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 24, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public long getIdConceptoExterno()
  {
    return jdoGetidConceptoExterno(this);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public double getUnidades()
  {
    return jdoGetunidades(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setIdConceptoExterno(long l)
  {
    jdoSetidConceptoExterno(this, l);
  }

  public void setMonto(double d)
  {
    jdoSetmonto(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setUnidades(double d)
  {
    jdoSetunidades(this, d);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoNomina.ConceptoExterno"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoExterno());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoExterno localConceptoExterno = new ConceptoExterno();
    localConceptoExterno.jdoFlags = 1;
    localConceptoExterno.jdoStateManager = paramStateManager;
    return localConceptoExterno;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoExterno localConceptoExterno = new ConceptoExterno();
    localConceptoExterno.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoExterno.jdoFlags = 1;
    localConceptoExterno.jdoStateManager = paramStateManager;
    return localConceptoExterno;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoExterno);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoExterno = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoExterno paramConceptoExterno, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoExterno.conceptoTipoPersonal;
      return;
    case 1:
      if (paramConceptoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoExterno = paramConceptoExterno.idConceptoExterno;
      return;
    case 2:
      if (paramConceptoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramConceptoExterno.monto;
      return;
    case 3:
      if (paramConceptoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramConceptoExterno.trabajador;
      return;
    case 4:
      if (paramConceptoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramConceptoExterno.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoExterno))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoExterno localConceptoExterno = (ConceptoExterno)paramObject;
    if (localConceptoExterno.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoExterno, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoExternoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoExternoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoExternoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoExternoPK localConceptoExternoPK = (ConceptoExternoPK)paramObject;
    localConceptoExternoPK.idConceptoExterno = this.idConceptoExterno;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoExternoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoExternoPK localConceptoExternoPK = (ConceptoExternoPK)paramObject;
    this.idConceptoExterno = localConceptoExternoPK.idConceptoExterno;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoExternoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoExternoPK localConceptoExternoPK = (ConceptoExternoPK)paramObject;
    localConceptoExternoPK.idConceptoExterno = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoExternoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoExternoPK localConceptoExternoPK = (ConceptoExternoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localConceptoExternoPK.idConceptoExterno);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoExterno paramConceptoExterno)
  {
    StateManager localStateManager = paramConceptoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoExterno.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoExterno, jdoInheritedFieldCount + 0))
      return paramConceptoExterno.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoExterno, jdoInheritedFieldCount + 0, paramConceptoExterno.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoExterno paramConceptoExterno, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoExterno.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoExterno, jdoInheritedFieldCount + 0, paramConceptoExterno.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final long jdoGetidConceptoExterno(ConceptoExterno paramConceptoExterno)
  {
    return paramConceptoExterno.idConceptoExterno;
  }

  private static final void jdoSetidConceptoExterno(ConceptoExterno paramConceptoExterno, long paramLong)
  {
    StateManager localStateManager = paramConceptoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoExterno.idConceptoExterno = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoExterno, jdoInheritedFieldCount + 1, paramConceptoExterno.idConceptoExterno, paramLong);
  }

  private static final double jdoGetmonto(ConceptoExterno paramConceptoExterno)
  {
    if (paramConceptoExterno.jdoFlags <= 0)
      return paramConceptoExterno.monto;
    StateManager localStateManager = paramConceptoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoExterno.monto;
    if (localStateManager.isLoaded(paramConceptoExterno, jdoInheritedFieldCount + 2))
      return paramConceptoExterno.monto;
    return localStateManager.getDoubleField(paramConceptoExterno, jdoInheritedFieldCount + 2, paramConceptoExterno.monto);
  }

  private static final void jdoSetmonto(ConceptoExterno paramConceptoExterno, double paramDouble)
  {
    if (paramConceptoExterno.jdoFlags == 0)
    {
      paramConceptoExterno.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoExterno.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoExterno, jdoInheritedFieldCount + 2, paramConceptoExterno.monto, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(ConceptoExterno paramConceptoExterno)
  {
    StateManager localStateManager = paramConceptoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoExterno.trabajador;
    if (localStateManager.isLoaded(paramConceptoExterno, jdoInheritedFieldCount + 3))
      return paramConceptoExterno.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramConceptoExterno, jdoInheritedFieldCount + 3, paramConceptoExterno.trabajador);
  }

  private static final void jdoSettrabajador(ConceptoExterno paramConceptoExterno, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramConceptoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoExterno.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramConceptoExterno, jdoInheritedFieldCount + 3, paramConceptoExterno.trabajador, paramTrabajador);
  }

  private static final double jdoGetunidades(ConceptoExterno paramConceptoExterno)
  {
    if (paramConceptoExterno.jdoFlags <= 0)
      return paramConceptoExterno.unidades;
    StateManager localStateManager = paramConceptoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoExterno.unidades;
    if (localStateManager.isLoaded(paramConceptoExterno, jdoInheritedFieldCount + 4))
      return paramConceptoExterno.unidades;
    return localStateManager.getDoubleField(paramConceptoExterno, jdoInheritedFieldCount + 4, paramConceptoExterno.unidades);
  }

  private static final void jdoSetunidades(ConceptoExterno paramConceptoExterno, double paramDouble)
  {
    if (paramConceptoExterno.jdoFlags == 0)
    {
      paramConceptoExterno.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoExterno.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoExterno, jdoInheritedFieldCount + 4, paramConceptoExterno.unidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}