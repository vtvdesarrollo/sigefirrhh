package sigefirrhh.personal.procesoNomina;

import java.io.Serializable;

public class NominaEspecialPK
  implements Serializable
{
  public long idNominaEspecial;

  public NominaEspecialPK()
  {
  }

  public NominaEspecialPK(long idNominaEspecial)
  {
    this.idNominaEspecial = idNominaEspecial;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((NominaEspecialPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(NominaEspecialPK thatPK)
  {
    return 
      this.idNominaEspecial == thatPK.idNominaEspecial;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idNominaEspecial)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idNominaEspecial);
  }
}