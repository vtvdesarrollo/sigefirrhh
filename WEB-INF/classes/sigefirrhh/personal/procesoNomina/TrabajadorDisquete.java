package sigefirrhh.personal.procesoNomina;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Disquete;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.sistema.Usuario;

public class TrabajadorDisquete
  implements Serializable, PersistenceCapable
{
  private long idTrabajadorDisquete;
  private Trabajador trabajador;
  private Disquete disquete;
  private Usuario usuario;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "disquete", "idTrabajadorDisquete", "trabajador", "usuario" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.Disquete"), Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), sunjdo$classForName$("sigefirrhh.sistema.Usuario") };
  private static final byte[] jdoFieldFlags = { 26, 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Disquete getDisquete()
  {
    return jdoGetdisquete(this);
  }

  public long getIdTrabajadorDisquete()
  {
    return jdoGetidTrabajadorDisquete(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public Usuario getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setDisquete(Disquete disquete)
  {
    jdoSetdisquete(this, disquete);
  }

  public void setIdTrabajadorDisquete(long l)
  {
    jdoSetidTrabajadorDisquete(this, l);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setUsuario(Usuario usuario)
  {
    jdoSetusuario(this, usuario);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoNomina.TrabajadorDisquete"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TrabajadorDisquete());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TrabajadorDisquete localTrabajadorDisquete = new TrabajadorDisquete();
    localTrabajadorDisquete.jdoFlags = 1;
    localTrabajadorDisquete.jdoStateManager = paramStateManager;
    return localTrabajadorDisquete;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TrabajadorDisquete localTrabajadorDisquete = new TrabajadorDisquete();
    localTrabajadorDisquete.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTrabajadorDisquete.jdoFlags = 1;
    localTrabajadorDisquete.jdoStateManager = paramStateManager;
    return localTrabajadorDisquete;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.disquete);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTrabajadorDisquete);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.disquete = ((Disquete)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTrabajadorDisquete = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = ((Usuario)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TrabajadorDisquete paramTrabajadorDisquete, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTrabajadorDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.disquete = paramTrabajadorDisquete.disquete;
      return;
    case 1:
      if (paramTrabajadorDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.idTrabajadorDisquete = paramTrabajadorDisquete.idTrabajadorDisquete;
      return;
    case 2:
      if (paramTrabajadorDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramTrabajadorDisquete.trabajador;
      return;
    case 3:
      if (paramTrabajadorDisquete == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramTrabajadorDisquete.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TrabajadorDisquete))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TrabajadorDisquete localTrabajadorDisquete = (TrabajadorDisquete)paramObject;
    if (localTrabajadorDisquete.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTrabajadorDisquete, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TrabajadorDisquetePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TrabajadorDisquetePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrabajadorDisquetePK))
      throw new IllegalArgumentException("arg1");
    TrabajadorDisquetePK localTrabajadorDisquetePK = (TrabajadorDisquetePK)paramObject;
    localTrabajadorDisquetePK.idTrabajadorDisquete = this.idTrabajadorDisquete;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrabajadorDisquetePK))
      throw new IllegalArgumentException("arg1");
    TrabajadorDisquetePK localTrabajadorDisquetePK = (TrabajadorDisquetePK)paramObject;
    this.idTrabajadorDisquete = localTrabajadorDisquetePK.idTrabajadorDisquete;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrabajadorDisquetePK))
      throw new IllegalArgumentException("arg2");
    TrabajadorDisquetePK localTrabajadorDisquetePK = (TrabajadorDisquetePK)paramObject;
    localTrabajadorDisquetePK.idTrabajadorDisquete = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrabajadorDisquetePK))
      throw new IllegalArgumentException("arg2");
    TrabajadorDisquetePK localTrabajadorDisquetePK = (TrabajadorDisquetePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localTrabajadorDisquetePK.idTrabajadorDisquete);
  }

  private static final Disquete jdoGetdisquete(TrabajadorDisquete paramTrabajadorDisquete)
  {
    StateManager localStateManager = paramTrabajadorDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorDisquete.disquete;
    if (localStateManager.isLoaded(paramTrabajadorDisquete, jdoInheritedFieldCount + 0))
      return paramTrabajadorDisquete.disquete;
    return (Disquete)localStateManager.getObjectField(paramTrabajadorDisquete, jdoInheritedFieldCount + 0, paramTrabajadorDisquete.disquete);
  }

  private static final void jdoSetdisquete(TrabajadorDisquete paramTrabajadorDisquete, Disquete paramDisquete)
  {
    StateManager localStateManager = paramTrabajadorDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorDisquete.disquete = paramDisquete;
      return;
    }
    localStateManager.setObjectField(paramTrabajadorDisquete, jdoInheritedFieldCount + 0, paramTrabajadorDisquete.disquete, paramDisquete);
  }

  private static final long jdoGetidTrabajadorDisquete(TrabajadorDisquete paramTrabajadorDisquete)
  {
    return paramTrabajadorDisquete.idTrabajadorDisquete;
  }

  private static final void jdoSetidTrabajadorDisquete(TrabajadorDisquete paramTrabajadorDisquete, long paramLong)
  {
    StateManager localStateManager = paramTrabajadorDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorDisquete.idTrabajadorDisquete = paramLong;
      return;
    }
    localStateManager.setLongField(paramTrabajadorDisquete, jdoInheritedFieldCount + 1, paramTrabajadorDisquete.idTrabajadorDisquete, paramLong);
  }

  private static final Trabajador jdoGettrabajador(TrabajadorDisquete paramTrabajadorDisquete)
  {
    StateManager localStateManager = paramTrabajadorDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorDisquete.trabajador;
    if (localStateManager.isLoaded(paramTrabajadorDisquete, jdoInheritedFieldCount + 2))
      return paramTrabajadorDisquete.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramTrabajadorDisquete, jdoInheritedFieldCount + 2, paramTrabajadorDisquete.trabajador);
  }

  private static final void jdoSettrabajador(TrabajadorDisquete paramTrabajadorDisquete, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajadorDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorDisquete.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramTrabajadorDisquete, jdoInheritedFieldCount + 2, paramTrabajadorDisquete.trabajador, paramTrabajador);
  }

  private static final Usuario jdoGetusuario(TrabajadorDisquete paramTrabajadorDisquete)
  {
    StateManager localStateManager = paramTrabajadorDisquete.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorDisquete.usuario;
    if (localStateManager.isLoaded(paramTrabajadorDisquete, jdoInheritedFieldCount + 3))
      return paramTrabajadorDisquete.usuario;
    return (Usuario)localStateManager.getObjectField(paramTrabajadorDisquete, jdoInheritedFieldCount + 3, paramTrabajadorDisquete.usuario);
  }

  private static final void jdoSetusuario(TrabajadorDisquete paramTrabajadorDisquete, Usuario paramUsuario)
  {
    StateManager localStateManager = paramTrabajadorDisquete.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorDisquete.usuario = paramUsuario;
      return;
    }
    localStateManager.setObjectField(paramTrabajadorDisquete, jdoInheritedFieldCount + 3, paramTrabajadorDisquete.usuario, paramUsuario);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}