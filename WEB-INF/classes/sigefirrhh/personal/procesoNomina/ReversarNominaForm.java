package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ReversarNominaForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReversarNominaForm.class.getName());
  private NominaEspecial nominaEspecial;
  private long idNominaEspecial;
  private Collection listNominaEspecial;
  private String selectNominaEspecial;
  private String selectGrupoNomina;
  private String inicio;
  private String fin;
  private Calendar inicioAux = Calendar.getInstance();
  private Calendar finAux = Calendar.getInstance();
  private String numeroOrdenPago;
  private long idGrupoNomina;
  private int numeroNomina;
  private Collection listGrupoNomina;
  private String periodicidad;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private Integer semanaMes;
  private Integer lunesPrQuincena;
  private Integer lunesSeQuincena;
  private Boolean tieneSemana5;
  private Integer semanaAnio;
  private boolean show2 = false;
  private boolean auxShow = false;
  private boolean showNominaEspecial = false;
  private String tipoNomina = "O";
  private SeguridadOrdinaria seguridadOrdinaria;

  public ReversarNominaForm()
  {
    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void changeGrupoNomina(ValueChangeEvent event) {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    this.showNominaEspecial = false;
    this.auxShow = false;
    try {
      if (this.idGrupoNomina != 0L)
        if (this.tipoNomina.equals("O"))
          buscarNomina(this.idGrupoNomina);
        else
          llenarNominaEspecial(this.idGrupoNomina);
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  private void buscarNomina(long idGrupoNomina) {
    try {
      this.seguridadOrdinaria = ((SeguridadOrdinaria)this.procesoNominaNoGenFacade.findSeguridadOrdinariaByGrupoNominaDesc(this.idGrupoNomina).iterator().next());

      this.inicioAux.setTime(this.seguridadOrdinaria.getFechaInicio());
      this.finAux.setTime(this.seguridadOrdinaria.getFechaFin());
      this.auxShow = true;
    } catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  private void llenarNominaEspecial(long idGrupoNomina) {
    try { log.error("si pasa");
      this.listNominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialByGrupoNominaAndEstatus(idGrupoNomina, "P", "N");
      log.error("size NE" + this.listNominaEspecial.size());
      this.showNominaEspecial = true;
    }
    catch (Exception e)
    {
      this.showNominaEspecial = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeNominaEspecial(ValueChangeEvent event) {
    this.idNominaEspecial = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.nominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialById(this.idNominaEspecial);
      this.inicioAux.setTime(this.nominaEspecial.getFechaInicio());
      this.finAux.setTime(this.nominaEspecial.getFechaFin());
      this.auxShow = true;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoNomina(ValueChangeEvent event)
  {
    String tipoNomina = (String)event.getNewValue();
    try {
      this.auxShow = false;
      this.showNominaEspecial = false;
      if (tipoNomina.equals("O"))
        buscarNomina(this.idGrupoNomina);
      else {
        llenarNominaEspecial(this.idGrupoNomina);
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String preGenerate()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.show2 = true;

    return null;
  }
  public String abort() {
    this.auxShow = true;
    this.show2 = false;
    return null;
  }
  public void reversar() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      log.error("....................PASO 1");
      if (this.tipoNomina.equals("O")) {
        log.error("....................PASO 2");
        boolean estado = this.procesoNominaNoGenFacade.reversarNomina(this.seguridadOrdinaria.getGrupoNomina().getIdGrupoNomina(), 0, this.seguridadOrdinaria.getFechaInicio(), this.seguridadOrdinaria.getGrupoNomina().getPeriodicidad(), this.seguridadOrdinaria.getAnio(), this.seguridadOrdinaria.getMes(), this.seguridadOrdinaria.getSemanaQuincena(), false);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.seguridadOrdinaria.getGrupoNomina());
        log.error("....................PASO 2.1");
        if (estado) {
          context.addMessage("success_add", new FacesMessage("Se reversó con éxito"));
          this.auxShow = false;
          this.show2 = false;
          this.selectGrupoNomina = "0";
        } else {
          context.addMessage("success_add", new FacesMessage("Esta nómina ya se reversó anteriormente"));
          this.auxShow = false;
          this.show2 = false;
          this.selectGrupoNomina = "0";
        }
      }
      else {
        log.error("....................PASO 3");
        boolean estado = this.procesoNominaNoGenFacade.reversarNomina(this.nominaEspecial.getGrupoNomina().getIdGrupoNomina(), this.nominaEspecial.getNumeroNomina(), this.nominaEspecial.getFechaInicio(), this.nominaEspecial.getGrupoNomina().getPeriodicidad(), this.nominaEspecial.getAnio(), this.nominaEspecial.getMes(), 1, true);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.nominaEspecial.getGrupoNomina());
        if (estado) {
          context.addMessage("success_add", new FacesMessage("Se reversó con éxito"));
          this.auxShow = false;
          this.show2 = false;
          this.selectGrupoNomina = "0";
        } else {
          context.addMessage("success_add", new FacesMessage("Esta nómina ya se reversó anteriormente"));
          this.auxShow = false;
          this.show2 = false;
          this.selectGrupoNomina = "0";
        }
        log.error("....................PASO 3.1");
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.listGrupoNomina = new ArrayList();
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());

      log.error("vacio:" + this.listGrupoNomina.isEmpty());
    } catch (Exception e) {
      this.listGrupoNomina = new ArrayList();
    }
  }

  public Collection getListGrupoNomina() {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListNominaEspecial() {
    if ((this.listNominaEspecial != null) && (!this.listNominaEspecial.isEmpty())) {
      return ListUtil.convertCollectionToSelectItemsWithId(
        this.listNominaEspecial, "sigefirrhh.personal.procesoNomina.NominaEspecial");
    }
    return null;
  }

  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String string) {
    this.selectGrupoNomina = string;
  }
  public String getFin() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }
  public void setFin(String string) {
    this.fin = string;
  }
  public void setInicio(String string) {
    this.inicio = string;
  }

  public long getIdGrupoNomina()
  {
    return this.idGrupoNomina;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public void setIdGrupoNomina(long l) {
    this.idGrupoNomina = l;
  }

  public String getTipoNomina()
  {
    return this.tipoNomina;
  }
  public void setTipoNomina(String string) {
    this.tipoNomina = string;
  }

  public String getSelectNominaEspecial()
  {
    return this.selectNominaEspecial;
  }

  public void setSelectNominaEspecial(String string)
  {
    this.selectNominaEspecial = string;
  }

  public int getNumeroNomina() {
    return this.numeroNomina;
  }
  public void setNumeroNomina(int numeroNomina) {
    this.numeroNomina = numeroNomina;
  }
  public String getNumeroOrdenPago() {
    return this.numeroOrdenPago;
  }
  public void setNumeroOrdenPago(String numeroOrdenPago) {
    this.numeroOrdenPago = numeroOrdenPago;
  }
  public boolean isShow2() {
    return this.show2;
  }
  public boolean isShowNominaEspecial() {
    return this.showNominaEspecial;
  }
  public boolean isAuxShow() {
    return this.auxShow;
  }
  public void setShow2(boolean show2) {
    this.show2 = show2;
  }

  public void setaAuxShow(boolean auxShow) {
    this.auxShow = auxShow;
  }
  public void setShowNominaEspecial(boolean showNominaEspecial) {
    this.showNominaEspecial = showNominaEspecial;
  }
}