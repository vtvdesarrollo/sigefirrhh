package sigefirrhh.personal.procesoNomina;

import java.io.Serializable;

public class UltimaNominaPK
  implements Serializable
{
  public long idUltimaNomina;

  public UltimaNominaPK()
  {
  }

  public UltimaNominaPK(long idUltimaNomina)
  {
    this.idUltimaNomina = idUltimaNomina;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UltimaNominaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UltimaNominaPK thatPK)
  {
    return 
      this.idUltimaNomina == thatPK.idUltimaNomina;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUltimaNomina)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUltimaNomina);
  }
}