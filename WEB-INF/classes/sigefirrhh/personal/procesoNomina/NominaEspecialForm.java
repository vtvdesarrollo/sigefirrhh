package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class NominaEspecialForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(NominaEspecialForm.class.getName());
  private NominaEspecial nominaEspecial;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private ProcesoNominaNoGenFacade procesoNominaFacade = new ProcesoNominaNoGenFacade();
  private boolean showNominaEspecialByGrupoNomina;
  private boolean showNominaEspecialByAnio;
  private boolean showNominaEspecialByFrecuenciaPago;
  private String findSelectGrupoNomina;
  private int findAnio;
  private String findSelectFrecuenciaPago;
  private Collection findColGrupoNomina;
  private Collection findColFrecuenciaPago;
  private Collection colGrupoNomina;
  private Collection colFrecuenciaPago;
  private String selectGrupoNomina;
  private String selectFrecuenciaPago;
  private Object stateResultNominaEspecialByGrupoNomina = null;

  private Object stateResultNominaEspecialByAnio = null;

  private Object stateResultNominaEspecialByFrecuenciaPago = null;

  public String getFindSelectGrupoNomina()
  {
    return this.findSelectGrupoNomina;
  }
  public void setFindSelectGrupoNomina(String valGrupoNomina) {
    this.findSelectGrupoNomina = valGrupoNomina;
  }

  public Collection getFindColGrupoNomina() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoNomina.getIdGrupoNomina()), 
        grupoNomina.toString()));
    }
    return col;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }
  public String getFindSelectFrecuenciaPago() {
    return this.findSelectFrecuenciaPago;
  }
  public void setFindSelectFrecuenciaPago(String valFrecuenciaPago) {
    this.findSelectFrecuenciaPago = valFrecuenciaPago;
  }

  public Collection getFindColFrecuenciaPago() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColFrecuenciaPago.iterator();
    FrecuenciaPago frecuenciaPago = null;
    while (iterator.hasNext()) {
      frecuenciaPago = (FrecuenciaPago)iterator.next();
      col.add(new SelectItem(
        String.valueOf(frecuenciaPago.getIdFrecuenciaPago()), 
        frecuenciaPago.toString()));
    }
    return col;
  }

  public String getSelectGrupoNomina()
  {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String valGrupoNomina) {
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    this.nominaEspecial.setGrupoNomina(null);
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      if (String.valueOf(grupoNomina.getIdGrupoNomina()).equals(
        valGrupoNomina)) {
        this.nominaEspecial.setGrupoNomina(
          grupoNomina);
      }
    }
    this.selectGrupoNomina = valGrupoNomina;
  }
  public String getSelectFrecuenciaPago() {
    return this.selectFrecuenciaPago;
  }
  public void setSelectFrecuenciaPago(String valFrecuenciaPago) {
    Iterator iterator = this.colFrecuenciaPago.iterator();
    FrecuenciaPago frecuenciaPago = null;
    this.nominaEspecial.setFrecuenciaPago(null);
    while (iterator.hasNext()) {
      frecuenciaPago = (FrecuenciaPago)iterator.next();
      if (String.valueOf(frecuenciaPago.getIdFrecuenciaPago()).equals(
        valFrecuenciaPago)) {
        this.nominaEspecial.setFrecuenciaPago(
          frecuenciaPago);
      }
    }
    this.selectFrecuenciaPago = valFrecuenciaPago;
  }
  public Collection getResult() {
    return this.result;
  }

  public NominaEspecial getNominaEspecial() {
    if (this.nominaEspecial == null) {
      this.nominaEspecial = new NominaEspecial();
    }
    return this.nominaEspecial;
  }

  public NominaEspecialForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoNomina.getIdGrupoNomina()), 
        grupoNomina.toString()));
    }
    return col;
  }

  public Collection getListEstatus() {
    Collection col = new ArrayList();

    Iterator iterEntry = NominaEspecial.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListPersonal() {
    Collection col = new ArrayList();

    Iterator iterEntry = NominaEspecial.LISTA_PERSONAL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListPagada() {
    Collection col = new ArrayList();

    Iterator iterEntry = NominaEspecial.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getColFrecuenciaPago() {
    Collection col = new ArrayList();
    Iterator iterator = this.colFrecuenciaPago.iterator();
    FrecuenciaPago frecuenciaPago = null;
    while (iterator.hasNext()) {
      frecuenciaPago = (FrecuenciaPago)iterator.next();
      col.add(new SelectItem(
        String.valueOf(frecuenciaPago.getIdFrecuenciaPago()), 
        frecuenciaPago.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColGrupoNomina = 
        this.definicionesFacade.findGrupoNominaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColFrecuenciaPago = 
        this.definicionesFacade.findFrecuenciaPagoByOrganismoMayorA10(
        this.login.getOrganismo().getIdOrganismo());

      this.colGrupoNomina = 
        this.definicionesFacade.findGrupoNominaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colFrecuenciaPago = 
        this.definicionesFacade.findFrecuenciaPagoByOrganismoMayorA10(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findNominaEspecialByGrupoNomina()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.procesoNominaFacade.findNominaEspecialByGrupoNomina(Long.valueOf(this.findSelectGrupoNomina).longValue());
      this.showNominaEspecialByGrupoNomina = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showNominaEspecialByGrupoNomina)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoNomina = null;
    this.findAnio = 0;
    this.findSelectFrecuenciaPago = null;
    return null;
  }

  public String findNominaEspecialByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.procesoNominaFacade.findNominaEspecialByAnio(this.findAnio);
      this.showNominaEspecialByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showNominaEspecialByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoNomina = null;
    this.findAnio = 0;
    this.findSelectFrecuenciaPago = null;
    return null;
  }

  public String findNominaEspecialByFrecuenciaPago()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.procesoNominaFacade.findNominaEspecialByFrecuenciaPago(Long.valueOf(this.findSelectFrecuenciaPago).longValue());
      this.showNominaEspecialByFrecuenciaPago = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showNominaEspecialByFrecuenciaPago)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoNomina = null;
    this.findAnio = 0;
    this.findSelectFrecuenciaPago = null;
    return null;
  }

  public boolean isShowNominaEspecialByGrupoNomina() {
    return this.showNominaEspecialByGrupoNomina;
  }
  public boolean isShowNominaEspecialByAnio() {
    return this.showNominaEspecialByAnio;
  }
  public boolean isShowNominaEspecialByFrecuenciaPago() {
    return this.showNominaEspecialByFrecuenciaPago;
  }

  public String selectNominaEspecial()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectGrupoNomina = null;
    this.selectFrecuenciaPago = null;

    long idNominaEspecial = 
      Long.parseLong((String)requestParameterMap.get("idNominaEspecial"));
    try
    {
      this.nominaEspecial = 
        this.procesoNominaFacade.findNominaEspecialById(
        idNominaEspecial);
      if (this.nominaEspecial.getGrupoNomina() != null) {
        this.selectGrupoNomina = 
          String.valueOf(this.nominaEspecial.getGrupoNomina().getIdGrupoNomina());
      }
      if (this.nominaEspecial.getFrecuenciaPago() != null) {
        this.selectFrecuenciaPago = 
          String.valueOf(this.nominaEspecial.getFrecuenciaPago().getIdFrecuenciaPago());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.nominaEspecial = null;
    this.showNominaEspecialByGrupoNomina = false;
    this.showNominaEspecialByAnio = false;
    this.showNominaEspecialByFrecuenciaPago = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    if (this.nominaEspecial.getAnio() == 0) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El año no puede ser 0", ""));
      error = true;
    }
    if ((this.nominaEspecial.getMes() == 0) || (this.nominaEspecial.getMes() > 12)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El mes debe ser mayor a 0 y menor a 13", ""));
      error = true;
    }

    int anioFin = this.nominaEspecial.getFechaFin().getYear() + 1900;
    int mesFin = this.nominaEspecial.getFechaFin().getMonth() + 1;
    int anioInicio = this.nominaEspecial.getFechaInicio().getYear();
    int mesInicio = this.nominaEspecial.getFechaInicio().getMonth() + 1;

    if (this.nominaEspecial.getMes() != mesFin) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El mes de la fecha fin debe pertenecer al mes introducido", ""));
      error = true;
    }
    log.error("anio " + this.nominaEspecial.getAnio());
    log.error("anio fin " + anioFin);

    if (this.nominaEspecial.getAnio() != anioFin) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El año de la fecha fin debe pertenecer al año introducido", ""));
      error = true;
    }

    if (this.nominaEspecial.getFechaFin().compareTo(this.nominaEspecial.getFechaInicio()) < 0) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha fin debe ser mayor a la fecha inicio", ""));
      error = true;
    }

    if ((this.nominaEspecial.getFechaRegistro() != null) && 
      (this.nominaEspecial.getFechaRegistro().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Registro no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.nominaEspecial.setUsuario(this.login.getUsuario());
        this.nominaEspecial.setFechaRegistro(new Date());
        this.nominaEspecial.setNumeroNomina(this.procesoNominaFacade.findLastNumeroNominaEspecialByGrupoNominaAndAnio(Long.valueOf(this.selectGrupoNomina).longValue(), this.nominaEspecial.getAnio()) + 1);
        this.procesoNominaFacade.addNominaEspecial(
          this.nominaEspecial);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.nominaEspecial);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.procesoNominaFacade.updateNominaEspecial(
          this.nominaEspecial);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.nominaEspecial);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.procesoNominaFacade.deleteNominaEspecial(
        this.nominaEspecial);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.nominaEspecial);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.nominaEspecial = new NominaEspecial();

    this.selectGrupoNomina = null;

    this.selectFrecuenciaPago = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.nominaEspecial.setIdNominaEspecial(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.procesoNomina.NominaEspecial"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.nominaEspecial = new NominaEspecial();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}