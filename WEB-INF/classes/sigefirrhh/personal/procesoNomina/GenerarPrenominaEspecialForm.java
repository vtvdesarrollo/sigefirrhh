package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class GenerarPrenominaEspecialForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarPrenominaEspecialForm.class.getName());
  private String selectGrupoNomina;
  private String selectNominaEspecial;
  private String inicio;
  private String fin;
  private Calendar inicioAux;
  private Calendar finAux;
  private long idGrupoNomina;
  private long idNominaEspecial;
  private NominaEspecial nominaEspecial;
  private Collection listGrupoNomina;
  private Collection listNominaEspecial;
  private Collection mensajesUltimaPrenomina;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private ProcesoNominaFacade procesoNominaFacade = new ProcesoNominaFacade();
  private boolean show;
  private boolean show2;
  private boolean showMensajesPrenomina = false;

  public GenerarPrenominaEspecialForm()
  {
    this.inicioAux = Calendar.getInstance();
    this.finAux = Calendar.getInstance();
    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();

    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void changeGrupoNomina(ValueChangeEvent event)
  {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.show = false;
      this.listNominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialByGrupoNominaAndEstatus(this.idGrupoNomina, "A", "N");
      this.show = true;
    }
    catch (Exception e) {
      this.show = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeNominaEspecial(ValueChangeEvent event)
  {
    this.idNominaEspecial = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.show2 = false;
      this.nominaEspecial = this.procesoNominaFacade.findNominaEspecialById(this.idNominaEspecial);
      log.error("nomina especial" + this.nominaEspecial.getDescripcion());
      log.error("fecha inicio nomina especial " + this.nominaEspecial.getFechaInicio());
      this.inicioAux.setTime(this.nominaEspecial.getFechaInicio());
      this.finAux.setTime(this.nominaEspecial.getFechaFin());
      this.show2 = true;
    }
    catch (Exception e) {
      this.show2 = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      log.error("Vacio:" + this.listGrupoNomina.isEmpty());
    }
    catch (Exception e) {
      log.error(e.getCause());
      this.listGrupoNomina = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      setShowMensajesPrenomina(false);
      boolean estado = this.procesoNominaNoGenFacade.generarPrenominaEspecial(this.idGrupoNomina, this.inicioAux.getTime(), this.finAux.getTime(), this.login.getOrganismo().getIdOrganismo(), this.nominaEspecial.getNumeroNomina(), this.nominaEspecial.getFrecuenciaPago().getIdFrecuenciaPago(), this.nominaEspecial.getIdNominaEspecial(), this.login.getUsuario(), this.nominaEspecial.getPersonal());
      this.mensajesUltimaPrenomina = this.procesoNominaNoGenFacade.getMensajesUltimaPrenomina(new Long(this.nominaEspecial.getIdNominaEspecial()), new Long(this.idGrupoNomina));
      if ((this.mensajesUltimaPrenomina != null) && (!this.mensajesUltimaPrenomina.isEmpty()))
      {
        setShowMensajesPrenomina(true);
      }
      else setShowMensajesPrenomina(false);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.nominaEspecial);

      if (estado)
        context.addMessage("success_add", new FacesMessage("Hay Trabajadores Sobregirados. Imprima el reporte pertinente"));
      else
        context.addMessage("success_add", new FacesMessage("Se generó con éxito"));
    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public void setSelectNominaEspecial(String selectNominaEspecial)
  {
    this.selectNominaEspecial = selectNominaEspecial;
  }

  public Collection getListGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String string) {
    this.selectGrupoNomina = string;
  }
  public boolean isShow() {
    return this.show;
  }

  public Collection getListNominaEspecial() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listNominaEspecial, "sigefirrhh.personal.procesoNomina.NominaEspecial");
  }

  public Collection getMensajesUltimaPrenomina()
  {
    return this.mensajesUltimaPrenomina;
  }

  public String getSelectNominaEspecial()
  {
    return this.selectNominaEspecial;
  }

  public String getFin() {
    if (this.finAux == null) {
      return null;
    }
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio() {
    if (this.inicioAux == null) {
      return null;
    }
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }

  public boolean isShow2()
  {
    return this.show2;
  }

  public boolean isShowMensajesPrenomina() {
    return this.showMensajesPrenomina;
  }

  public void setShowMensajesPrenomina(boolean showMensajesPrenomina) {
    this.showMensajesPrenomina = showMensajesPrenomina;
  }

  public void setFin(String string) {
    this.fin = string;
  }
  public void setInicio(String string) {
    this.inicio = string;
  }
}