package sigefirrhh.personal.procesoNomina;

import java.io.Serializable;

public class ConceptoExternoPK
  implements Serializable
{
  public long idConceptoExterno;

  public ConceptoExternoPK()
  {
  }

  public ConceptoExternoPK(long idConceptoExterno)
  {
    this.idConceptoExterno = idConceptoExterno;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoExternoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoExternoPK thatPK)
  {
    return 
      this.idConceptoExterno == thatPK.idConceptoExterno;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoExterno)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoExterno);
  }
}