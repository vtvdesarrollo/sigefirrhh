package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FirmasReportes;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportAportesPatronalesForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportAportesPatronalesForm.class.getName());
  private int reportId;
  private long idConcepto = 0L;
  private long idTipoPersonal;
  private String reportName;
  private String banco = "N";
  private String codigoPatronal = "N";
  private String categoriaPresupuesto = "N";
  private String uel = "S";
  private int mes;
  private int anio;
  private boolean showConcepto = true;
  private boolean showAgrupacion = false;
  private boolean showReport = false;
  private boolean showBanco = false;
  private boolean showCodigoPatronal = false;
  private Collection listConcepto;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal concepto;
  private long firma1;
  private Collection listFirma1;
  private long firma2;
  private Collection listFirma2;
  private long firma3;
  private Collection listFirma3;

  public ReportAportesPatronalesForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "aportesquiuel";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportAportesPatronalesForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findAllTipoPersonal();
      this.listFirma1 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());
      this.listFirma2 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());
      this.listFirma3 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());
      this.banco = "N";
      this.codigoPatronal = "N";
      this.categoriaPresupuesto = "N";
      this.uel = "S";
      this.mes = 0;
      this.anio = 0;
      this.showConcepto = true;
      this.showAgrupacion = false;
      this.showReport = false;
      this.showBanco = false;
      this.showCodigoPatronal = false;
    }
    catch (Exception e)
    {
      this.listTipoPersonal = new ArrayList();
      this.listFirma1 = new ArrayList();
      this.listFirma2 = new ArrayList();
      this.listFirma3 = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try {
      if (this.tipoPersonal.getGrupoNomina().getPeriodicidad().equalsIgnoreCase("S")) {
        if (this.banco.equals("N")) {
          this.reportName = "aportessem";
          if (this.categoriaPresupuesto.equalsIgnoreCase("E"))
            this.reportName += "uel";
          else if (this.categoriaPresupuesto.equalsIgnoreCase("C"))
            this.reportName += "cat";
          else
            this.reportName += "catuel";
        }
        else if (this.banco.equalsIgnoreCase("S")) {
          this.reportName = "aportesbancossem";
        }
        if (getCodigoPatronal().equalsIgnoreCase("S"))
          this.reportName = "aportessemcp";
      }
      else {
        if (this.banco.equalsIgnoreCase("N")) {
          this.reportName = "aportesqui";
          if (this.categoriaPresupuesto.equalsIgnoreCase("E"))
            this.reportName += "uel";
          else if (this.categoriaPresupuesto.equalsIgnoreCase("C"))
            this.reportName += "cat";
          else
            this.reportName += "catuel";
        }
        else if (this.banco.equalsIgnoreCase("S")) {
          this.reportName = "aportesbancosqui";
        }
        if (getCodigoPatronal().equalsIgnoreCase("S"))
          this.reportName = "aportesquicp";
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      cambiarNombreAReporte();

      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      parameters.put("id_concepto_tipo_personal", new Long(this.idConcepto));
      parameters.put("titulo", this.concepto.getConcepto().getConceptoAporte().getDescripcion());

      if ((this.concepto.getConcepto().getConceptoRetroactivo() != null) && (this.concepto.getConcepto().getRetroactivo().equals("S"))) {
        parameters.put("id_concepto_retroactivo", new Long(this.concepto.getConcepto().getConceptoRetroactivo().getIdConcepto()));
        log.error("Cumple getConceptoRetroactivo()!= null &&  this.concepto.getConcepto().getRetroactivo().equals(S), id_concepto_retroactivo: " + this.concepto.getConcepto().getConceptoRetroactivo().getIdConcepto());
      } else {
        parameters.put("id_concepto_retroactivo", new Long(999999999L));
        log.error("id_concepto_retroactivo (this.idConcepto): " + this.idConcepto);
      }

      if (this.firma1 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma1);
        parameters.put("nombre_uno", firma.getNombre());
        parameters.put("cargo_uno", firma.getCargo());
        if (firma.getNombramiento() == null)
          parameters.put("nombramiento_uno", "");
        else {
          parameters.put("nombramiento_uno", firma.getNombramiento());
        }
      }
      if (this.firma2 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma2);
        parameters.put("nombre_dos", firma.getNombre());
        parameters.put("cargo_dos", firma.getCargo());
        if (firma.getNombramiento() == null)
          parameters.put("nombramiento_dos", "");
        else {
          parameters.put("nombramiento_dos", firma.getNombramiento());
        }
      }
      if (this.firma3 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma3);
        parameters.put("nombre_tres", firma.getNombre());
        parameters.put("cargo_tres", firma.getCargo());
        if (firma.getNombramiento() == null)
          parameters.put("nombramiento_tres", "");
        else {
          parameters.put("nombramiento_tres", firma.getNombramiento());
        }
      }
      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/procesoNomina");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    log.error("value " + String.valueOf(event.getNewValue()));
    long idTipoPersonal = Long.valueOf(String.valueOf(event.getNewValue())).longValue();
    this.idTipoPersonal = idTipoPersonal;
    this.tipoPersonal = null;
    this.listConcepto = null;
    try
    {
      if (idTipoPersonal > 0L) {
        this.showReport = false;
        this.showConcepto = false;
        this.showCodigoPatronal = false;
        this.showBanco = false;
        this.showAgrupacion = false;
        this.listConcepto = 
          this.definicionesFacade.findConceptoTipoPersonalForAportePatronal(
          idTipoPersonal, "S");
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
        this.showConcepto = true;
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeConcepto(ValueChangeEvent event)
  {
    long idConcepto = Long.valueOf(String.valueOf(event.getNewValue())).longValue();
    this.idConcepto = idConcepto;

    this.concepto = null;
    try
    {
      if (idConcepto > 0L) {
        this.showReport = false;
        this.showCodigoPatronal = false;
        this.showBanco = false;
        this.showAgrupacion = true;
        this.concepto = this.definicionesFacade.findConceptoTipoPersonalById(this.idConcepto);
        this.showReport = true;

        if (this.concepto.getCodConcepto().trim().equals("5003")) {
          this.showBanco = true;
        }

        if ((this.concepto.getCodConcepto().trim().equals("5001")) || (this.concepto.getCodConcepto().equals("5002"))) {
          this.showCodigoPatronal = true;
        }
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeCodigoPatronal(ValueChangeEvent event)
  {
    try
    {
      this.showAgrupacion = (!String.valueOf(event.getNewValue()).equalsIgnoreCase("S"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getListConcepto() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConcepto, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public Collection getListFirma1() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma1, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public Collection getListFirma2() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma2, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public Collection getListFirma3() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma3, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public int getAnio()
  {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }

  public boolean isShowConcepto() {
    return (this.showConcepto) && (this.listConcepto != null) && 
      (!this.listConcepto.isEmpty());
  }
  public boolean isShowReport() {
    return this.showReport;
  }
  public String getBanco() {
    return this.banco;
  }
  public void setBanco(String banco) {
    this.banco = banco;
  }
  public long getIdConcepto() {
    return this.idConcepto;
  }
  public void setIdConcepto(long idConcepto) {
    this.idConcepto = idConcepto;
  }
  public long getFirma1() {
    return this.firma1;
  }
  public void setFirma1(long firma1) {
    this.firma1 = firma1;
  }
  public long getFirma2() {
    return this.firma2;
  }
  public void setFirma2(long firma2) {
    this.firma2 = firma2;
  }
  public long getFirma3() {
    return this.firma3;
  }
  public void setFirma3(long firma3) {
    this.firma3 = firma3;
  }

  public String getCodigoPatronal()
  {
    return this.codigoPatronal;
  }

  public void setCodigoPatronal(String codigoPatronal)
  {
    this.codigoPatronal = codigoPatronal;
  }

  public boolean isShowBanco()
  {
    return this.showBanco;
  }

  public boolean isShowCodigoPatronal() {
    return this.showCodigoPatronal;
  }

  public String getCategoriaPresupuesto()
  {
    return this.categoriaPresupuesto;
  }

  public void setCategoriaPresupuesto(String categoriaPresupuesto)
  {
    this.categoriaPresupuesto = categoriaPresupuesto;
  }

  public String getUel()
  {
    return this.uel;
  }
  public void setUel(String uel) {
    this.uel = uel;
  }
  public boolean isShowAgrupacion() {
    return this.showAgrupacion;
  }
  public void setShowCodigoPatronal(boolean showCodigoPatronal) {
    this.showCodigoPatronal = showCodigoPatronal;
  }
  public void setShowReport(boolean showReport) {
    this.showReport = showReport;
  }
}