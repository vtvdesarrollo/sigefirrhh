package sigefirrhh.personal.procesoNomina;

import eforserver.common.Resource;
import eforserver.presentation.ListUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.Disquete;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ReportRetencionBanavihForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportRetencionBanavihForm.class
    .getName());
  private boolean generado;
  private String urlArchivo;
  private String urlArchivo2;
  private String reportName;
  private int reportId;
  private String selectTipoPersonal;
  private String selectGrupoNomina;
  private String selectDisquete;
  private String selectRegion;
  private String selectNominaEspecial;
  private String tipoNomina = "0";
  private String inicio;
  private String fin;
  private Date fechaProceso = new Date();
  private Date fechaAbono;
  private Disquete disquete;
  private Calendar inicioAux;
  private Calendar finAux;
  private String numeroOrdenPago;
  private long idTipoPersonal;
  private long idGrupoNomina;
  private String semanaQuincena;
  private int anio;
  private int mes;
  private int numero;
  private String prestamo;
  private long idRegion;
  private long idNominaEspecial;
  private Collection listTipoPersonal;
  private Collection listGrupoNomina;
  private Collection listDisquete;
  private Collection listRegion;
  private Collection listNominaEspecial;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private ProcesoNominaNoGenFacade nominaEspecialFacade;
  private LoginSession login;
  private boolean show;
  private boolean showEspecial = false;
  private boolean showOrdinaria = false;
  private boolean auxShow;
  private TipoPersonal tipoPersonal;
  private GrupoNomina grupoNomina;
  private NominaEspecial nominaEspecial;
  private long idUnidadAdministradora;
  private Collection listUnidadAdministradora;

  public ReportRetencionBanavihForm()
  {
    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.nominaEspecialFacade = new ProcesoNominaNoGenFacade();
    this.selectTipoPersonal = null;
    this.selectGrupoNomina = null;
    this.reportName = "retencionBanavih";
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication()
      .getVariableResolver().resolveVariable(context, "loginSession"));
    refresh();
  }

  public void changeGrupoNomina(ValueChangeEvent event) {
    this.idGrupoNomina = Long.valueOf((String)event.getNewValue()).longValue();
    this.auxShow = false;
    try {
      if (this.idGrupoNomina != 0L)
        this.grupoNomina = this.definicionesFacade
          .findGrupoNominaById(this.idGrupoNomina);
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    log.error("value " + String.valueOf(event.getNewValue()));
    System.out.println(" eventNewValue " + String.valueOf(event.getNewValue()));
    long idTipoPersonal = Long.valueOf(String.valueOf(event.getNewValue())).longValue();
    this.idTipoPersonal = idTipoPersonal;
    this.tipoPersonal = null;
    try {
      if (idTipoPersonal > 0L)
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String generar() throws Exception
  {
    Map parameters = new Hashtable();
    try
    {
      System.out.println(this.anio);
      System.out.println(this.mes);
      System.out.println("thisidtipopersonal:" + this.idTipoPersonal);
      System.out.println(this.tipoPersonal.getIdTipoPersonal());

      this.urlArchivo2 = genArchivoTxt(this.mes, this.anio, this.tipoPersonal.getIdTipoPersonal(), "1");
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      log.error("PASO 3");
    }
    return null;
  }

  private String genArchivoTxt(int mesProceso, int anioProceso, long tipoTrabajador, String fechaEgresos) {
    ResultSet rsBanavihResult = null;
    PreparedStatement stBanavihResult = null;
    Connection connection = Resource.getConnection();
    try {
      Random rmd = new Random();
      rmd.nextDouble();
      FacesContext context = FacesContext.getCurrentInstance();
      HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
      boolean success = new File(request.getRealPath("disquetes")).mkdir();
      File file = new File(request.getRealPath("disquetes") + "/" + rmd.nextDouble() + String.valueOf(tipoTrabajador) + ".txt");
      file.createNewFile();
      OutputStreamWriter writer = new OutputStreamWriter(
        new FileOutputStream(file), Charset.forName("ISO-8859-1"));
      StringBuffer sql = new StringBuffer();
      String fechaIngreso = getBuscar2(mesProceso, anioProceso);
      if (!"".equals(fechaEgresos)) {
        sql.append(" SELECT  dependencia,nacionalidad,cedula, apellido,apellido2, nombre, nombre2, ");
        sql.append(" idtp, tipopersonal,fingreso ,fegreso, unidadejecutora,sueldobasico,  bonos, ");
        sql.append(" alicuotaVacacional,alicuotaFinAnio, total FROM banavihactivos (?,?,?,?) ");
        sql.append(" ORDER BY apellido, nombre ");
      } else {
        sql.append("SELECT  dependencia,nacionalidad,cedula, apellido,apellido2, nombre, nombre2,");
        sql.append(" idtp, tipopersonal, fingreso,fegreso, unidadejecutora,sueldobasico,  bonos,");
        sql.append(" alicuotaVacacional,alicuotaFinAnio, total FROM banavihegresados(?,?,?,?,?) ");
        sql.append(" ORDER BY apellido, nombre ");
      }
      System.out.println(sql.toString());
      stBanavihResult = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      if (!"".equals(fechaEgresos)) {
        stBanavihResult.setInt(1, mesProceso);
        stBanavihResult.setInt(2, anioProceso);
        stBanavihResult.setLong(3, tipoTrabajador);
        stBanavihResult.setString(4, getBuscar2(mesProceso, anioProceso));
      }
      else {
        stBanavihResult.setInt(1, mesProceso);
        stBanavihResult.setInt(2, anioProceso);
        stBanavihResult.setLong(3, tipoTrabajador);
        stBanavihResult.setString(4, getBuscar3(mesProceso, anioProceso));
        stBanavihResult.setString(5, getBuscar2(mesProceso, anioProceso));
      }

      rsBanavihResult = stBanavihResult.executeQuery();
      rsBanavihResult.beforeFirst();
      while (rsBanavihResult.next()) {
        String salida = "";
        String nacionalidad = rsBanavihResult.getString("nacionalidad");
        if (nacionalidad.length() > 1) {
          nacionalidad = nacionalidad.substring(0, 1);
        }
        String cedula = String.valueOf(rsBanavihResult.getString("cedula"));
        String nombre = formatearStr(rsBanavihResult.getString("nombre"));
        if (nombre.length() > 25) {
          nombre = nombre.substring(0, 25);
        }
        String nombre2 = formatearStr(rsBanavihResult.getString("nombre2"));
        if (nombre2.length() > 25) {
          nombre2 = nombre2.substring(0, 25);
        }
        String apellido = formatearStr(rsBanavihResult.getString("apellido"));
        if (apellido.length() > 25) {
          apellido = apellido.substring(0, 25);
        }
        String apellido2 = formatearStr(rsBanavihResult.getString("apellido2"));
        if (apellido2.length() > 25) {
          apellido2 = apellido2.substring(0, 25);
        }
        String total = formatArch(String.valueOf(rsBanavihResult.getDouble("total")), 10);
        String fingreso = rsBanavihResult.getString("fingreso");
        String fEgreso = rsBanavihResult.getString("fegreso");
        if (fEgreso == null) {
          fEgreso = "";
        } else {
          String fec = String.valueOf(anioProceso) + "-" + String.valueOf(mesProceso);
          if (fEgreso.indexOf(fec) >= 0) {
            fEgreso = fEgreso.substring(8, 10) + "-" + fEgreso.substring(5, 7) + "-" + fEgreso.substring(0, 4);
            fEgreso = fEgreso.replaceAll("-", "");
          } else {
            fEgreso = "";
          }
        }
        fingreso = fingreso.substring(8, 10) + "-" + fingreso.substring(5, 7) + "-" + fingreso.substring(0, 4);
        fingreso = fingreso.replaceAll("-", "");

        salida = nacionalidad + "," + cedula + "," + nombre + "," + nombre2 + "," + apellido + "," + apellido2 + "," + total + "," + fingreso + "," + fEgreso + "\n";
        writer.write(salida);
      }
      writer.flush();
      writer.close();
      this.generado = true;
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');
      log.error("Archivo Banavih generado path:/sigefirrhh/disquetes/" + file.getName());
      return "/sigefirrhh/disquetes/" + file.getName();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return "";
  }

  public String formatArch(String abc, int leng) {
    if (abc != null)
      try {
        double value = Double.parseDouble(abc) * 100.0D;
        abc = String.valueOf(value);
        if (abc.indexOf(".") > 0) {
          abc = abc.substring(0, abc.indexOf("."));
        }
        for (int i = abc.length(); i < leng; i++)
          abc = "0" + abc;
      }
      catch (NumberFormatException e) {
        log.error("Excepcion Controlada: ", e);
      }
    else {
      abc = "0000000000";
    }
    return abc;
  }

  private String formatearStr(String nombre) {
    nombre = nombre.replace('Á', 'A');
    nombre = nombre.replace('É', 'E');
    nombre = nombre.replace('Í', 'I');
    nombre = nombre.replace('Ó', 'O');
    nombre = nombre.replace('Ú', 'U');
    nombre = nombre.replace('Ñ', 'N');
    return nombre;
  }

  public String getBuscar2(int mesProcess, int anoProcess)
  {
    String buscar2 = "";
    String anoBuscar = String.valueOf(anoProcess);
    try
    {
      int mes2 = mesProcess + 1;
      if (mes2 < 10) {
        buscar2 = anoBuscar + "/0" + mes2 + "/01";
      }
      else if (mes2 == 13) {
        int ano2 = Integer.parseInt(anoBuscar) + 1;
        buscar2 = ano2 + "/01/01";
      } else {
        buscar2 = anoBuscar + "/" + mes2 + "/01";
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return buscar2;
  }

  public String getBuscar3(int mesProcess, int anoProcess) {
    String buscar3 = "";

    String anoBuscar = String.valueOf(anoProcess);
    try {
      int mes2 = mesProcess;
      if (mes2 < 10) {
        buscar3 = anoBuscar + "/0" + mes2 + "/01";
      }
      else
        buscar3 = anoBuscar + "/" + mes2 + "/01";
    }
    catch (NumberFormatException localNumberFormatException) {
    }
    return buscar3;
  }

  public void refresh() {
    try {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalByConcepto(
        this.login.getIdUsuario(), this.login.getIdOrganismo(), 
        this.login.getAdministrador(), "5003");
      this.listDisquete = this.definicionesFacade.findDisqueteByTipoDisquete("A", 
        this.login.getIdOrganismo());
      this.listRegion = this.estructuraFacade.findAllRegion();
      this.listGrupoNomina = this.definicionesFacade
        .findGrupoNominaByOrganismo(this.login.getIdOrganismo());
      this.listUnidadAdministradora = this.estructuraFacade
        .findUnidadAdministradoraByOrganismo(this.login.getIdOrganismo());
      log.error("vacio (listUnidadAdministradora):" + 
        this.listUnidadAdministradora.isEmpty());
    } catch (Exception e) {
      log.error("Excepción controlada: ", e);
      this.listTipoPersonal = new ArrayList();
      this.listGrupoNomina = new ArrayList();
      this.listNominaEspecial = new ArrayList();
      this.listUnidadAdministradora = new ArrayList();
    }
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(String.valueOf(id), nombre));
    }
    return col;
  }

  public Collection getListGrupoNomina()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(this.listGrupoNomina, 
      "sigefirrhh.base.definiciones.GrupoNomina");
  }

  public Collection getListDisquete() {
    return ListUtil.convertCollectionToSelectItemsWithId(this.listDisquete, 
      "sigefirrhh.base.definiciones.Disquete");
  }

  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(this.listRegion, 
      "sigefirrhh.base.estructura.Region");
  }

  public Collection getListNominaEspecial() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listNominaEspecial, 
      "sigefirrhh.personal.procesoNomina.NominaEspecial");
  }

  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
  }

  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }

  public void setSelectGrupoNomina(String string) {
    this.selectGrupoNomina = string;
  }

  public String getFin() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }

  public void setFin(String string) {
    this.fin = string;
  }

  public void setInicio(String string) {
    this.inicio = string;
  }

  public boolean isShow() {
    return this.auxShow;
  }

  public boolean isShowEspecial() {
    return this.showEspecial;
  }

  public boolean isShowOrdinaria() {
    return this.showOrdinaria;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }

  public long getIdGrupoNomina() {
    return this.idGrupoNomina;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public void setIdTipoPersonal(long l) {
    this.idTipoPersonal = l;
  }

  public void setIdGrupoNomina(long l) {
    this.idGrupoNomina = l;
  }

  public String getReportName() {
    return this.reportName;
  }

  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getSelectDisquete() {
    return this.selectDisquete;
  }

  public String getSelectRegion() {
    return this.selectRegion;
  }

  public String getSelectNominaEspecial() {
    return this.selectNominaEspecial;
  }

  public void setSelectDisquete(String string) {
    this.selectDisquete = string;
    try {
      this.disquete = this.definicionesFacade.findDisqueteById(
        Long.valueOf(this.selectDisquete).longValue());
    }
    catch (Exception localException) {
    }
  }

  public void setSelectRegion(String string) {
    this.selectRegion = string;
  }

  public void setSelectNominaEspecial(String string) {
    this.selectNominaEspecial = string;
  }

  public Date getFechaAbono() {
    return this.fechaAbono;
  }

  public Date getFechaProceso() {
    return this.fechaProceso;
  }

  public void setFechaAbono(Date date) {
    this.fechaAbono = date;
  }

  public void setFechaProceso(Date date) {
    this.fechaProceso = date;
  }

  public boolean isGenerado() {
    return this.generado;
  }

  public String getUrlArchivo() {
    return this.urlArchivo;
  }

  public String getNumeroOrdenPago() {
    return this.numeroOrdenPago;
  }

  public void setNumeroOrdenPago(String numeroOrdenPago) {
    this.numeroOrdenPago = numeroOrdenPago;
  }

  public int getAnio() {
    return this.anio;
  }

  public void setAnio(int anio) {
    this.anio = anio;
  }

  public int getMes() {
    return this.mes;
  }

  public void setMes(int mes) {
    this.mes = mes;
  }

  public String getPrestamo() {
    return this.prestamo;
  }

  public void setPrestamo(String prestamo) {
    this.prestamo = prestamo;
  }

  public int getNumero() {
    return this.numero;
  }

  public void setNumero(int numero) {
    this.numero = numero;
  }

  public long getIdUnidadAdministradora() {
    return this.idUnidadAdministradora;
  }

  public void setIdUnidadAdministradora(long l) {
    this.idUnidadAdministradora = l;
  }

  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, 
      "sigefirrhh.base.estructura.UnidadAdministradora");
  }

  public void setSemanaQuincena(String valor) {
    this.semanaQuincena = valor;
  }

  public String getSemanaQuincena()
  {
    return this.semanaQuincena;
  }

  public void setTipoNomina(String valor)
  {
    this.tipoNomina = valor;
  }

  public String getTipoNomina()
  {
    return this.tipoNomina;
  }

  public void setReportId(int valor)
  {
    this.reportId = valor;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setUrlArchivo2(String urlArchivo2)
  {
    this.urlArchivo2 = urlArchivo2;
  }

  public String getUrlArchivo2()
  {
    return this.urlArchivo2;
  }
}