package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class ProcesoNominaFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private ProcesoNominaBusiness procesoNominaBusiness = new ProcesoNominaBusiness();

  public void addFrecuenciaPagar(FrecuenciaPagar frecuenciaPagar)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.procesoNominaBusiness.addFrecuenciaPagar(frecuenciaPagar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateFrecuenciaPagar(FrecuenciaPagar frecuenciaPagar) throws Exception
  {
    try { this.txn.open();
      this.procesoNominaBusiness.updateFrecuenciaPagar(frecuenciaPagar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteFrecuenciaPagar(FrecuenciaPagar frecuenciaPagar) throws Exception
  {
    try { this.txn.open();
      this.procesoNominaBusiness.deleteFrecuenciaPagar(frecuenciaPagar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public FrecuenciaPagar findFrecuenciaPagarById(long frecuenciaPagarId) throws Exception
  {
    try { this.txn.open();
      FrecuenciaPagar frecuenciaPagar = 
        this.procesoNominaBusiness.findFrecuenciaPagarById(frecuenciaPagarId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(frecuenciaPagar);
      return frecuenciaPagar;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllFrecuenciaPagar() throws Exception
  {
    try { this.txn.open();
      return this.procesoNominaBusiness.findAllFrecuenciaPagar();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFrecuenciaPagarByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaBusiness.findFrecuenciaPagarByGrupoNomina(idGrupoNomina);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addNominaEspecial(NominaEspecial nominaEspecial)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.procesoNominaBusiness.addNominaEspecial(nominaEspecial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateNominaEspecial(NominaEspecial nominaEspecial) throws Exception
  {
    try { this.txn.open();
      this.procesoNominaBusiness.updateNominaEspecial(nominaEspecial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteNominaEspecial(NominaEspecial nominaEspecial) throws Exception
  {
    try { this.txn.open();
      this.procesoNominaBusiness.deleteNominaEspecial(nominaEspecial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public NominaEspecial findNominaEspecialById(long nominaEspecialId) throws Exception
  {
    try { this.txn.open();
      NominaEspecial nominaEspecial = 
        this.procesoNominaBusiness.findNominaEspecialById(nominaEspecialId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(nominaEspecial);
      return nominaEspecial;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllNominaEspecial() throws Exception
  {
    try { this.txn.open();
      return this.procesoNominaBusiness.findAllNominaEspecial();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNominaEspecialByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaBusiness.findNominaEspecialByGrupoNomina(idGrupoNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNominaEspecialByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaBusiness.findNominaEspecialByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNominaEspecialByEstatus(String estatus)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaBusiness.findNominaEspecialByEstatus(estatus);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNominaEspecialByFrecuenciaPago(long idFrecuenciaPago)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaBusiness.findNominaEspecialByFrecuenciaPago(idFrecuenciaPago);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSeguridadEspecial(SeguridadEspecial seguridadEspecial)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.procesoNominaBusiness.addSeguridadEspecial(seguridadEspecial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSeguridadEspecial(SeguridadEspecial seguridadEspecial) throws Exception
  {
    try { this.txn.open();
      this.procesoNominaBusiness.updateSeguridadEspecial(seguridadEspecial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSeguridadEspecial(SeguridadEspecial seguridadEspecial) throws Exception
  {
    try { this.txn.open();
      this.procesoNominaBusiness.deleteSeguridadEspecial(seguridadEspecial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SeguridadEspecial findSeguridadEspecialById(long seguridadEspecialId) throws Exception
  {
    try { this.txn.open();
      SeguridadEspecial seguridadEspecial = 
        this.procesoNominaBusiness.findSeguridadEspecialById(seguridadEspecialId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadEspecial);
      return seguridadEspecial;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSeguridadEspecial() throws Exception
  {
    try { this.txn.open();
      return this.procesoNominaBusiness.findAllSeguridadEspecial();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadEspecialByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaBusiness.findSeguridadEspecialByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadEspecialByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaBusiness.findSeguridadEspecialByGrupoNomina(idGrupoNomina);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSeguridadOrdinaria(SeguridadOrdinaria seguridadOrdinaria)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.procesoNominaBusiness.addSeguridadOrdinaria(seguridadOrdinaria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSeguridadOrdinaria(SeguridadOrdinaria seguridadOrdinaria) throws Exception
  {
    try { this.txn.open();
      this.procesoNominaBusiness.updateSeguridadOrdinaria(seguridadOrdinaria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSeguridadOrdinaria(SeguridadOrdinaria seguridadOrdinaria) throws Exception
  {
    try { this.txn.open();
      this.procesoNominaBusiness.deleteSeguridadOrdinaria(seguridadOrdinaria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SeguridadOrdinaria findSeguridadOrdinariaById(long seguridadOrdinariaId) throws Exception
  {
    try { this.txn.open();
      SeguridadOrdinaria seguridadOrdinaria = 
        this.procesoNominaBusiness.findSeguridadOrdinariaById(seguridadOrdinariaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadOrdinaria);
      return seguridadOrdinaria;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSeguridadOrdinaria() throws Exception
  {
    try { this.txn.open();
      return this.procesoNominaBusiness.findAllSeguridadOrdinaria();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadOrdinariaByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaBusiness.findSeguridadOrdinariaByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadOrdinariaByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaBusiness.findSeguridadOrdinariaByGrupoNomina(idGrupoNomina);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addUltimaPrenomina(UltimaPrenomina ultimaPrenomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.procesoNominaBusiness.addUltimaPrenomina(ultimaPrenomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateUltimaPrenomina(UltimaPrenomina ultimaPrenomina) throws Exception
  {
    try { this.txn.open();
      this.procesoNominaBusiness.updateUltimaPrenomina(ultimaPrenomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteUltimaPrenomina(UltimaPrenomina ultimaPrenomina) throws Exception
  {
    try { this.txn.open();
      this.procesoNominaBusiness.deleteUltimaPrenomina(ultimaPrenomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public UltimaPrenomina findUltimaPrenominaById(long ultimaPrenominaId) throws Exception
  {
    try { this.txn.open();
      UltimaPrenomina ultimaPrenomina = 
        this.procesoNominaBusiness.findUltimaPrenominaById(ultimaPrenominaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(ultimaPrenomina);
      return ultimaPrenomina;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllUltimaPrenomina() throws Exception
  {
    try { this.txn.open();
      return this.procesoNominaBusiness.findAllUltimaPrenomina();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}