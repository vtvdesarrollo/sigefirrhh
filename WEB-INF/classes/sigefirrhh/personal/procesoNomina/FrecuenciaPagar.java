package sigefirrhh.personal.procesoNomina;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.GrupoNomina;

public class FrecuenciaPagar
  implements Serializable, PersistenceCapable
{
  private long idFrecuenciaPagar;
  private GrupoNomina grupoNomina;
  private FrecuenciaPago frecuenciaPago;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "frecuenciaPago", "grupoNomina", "idFrecuenciaPagar" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaPago"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 26, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public FrecuenciaPago getFrecuenciaPago()
  {
    return jdoGetfrecuenciaPago(this);
  }

  public GrupoNomina getGrupoNomina()
  {
    return jdoGetgrupoNomina(this);
  }

  public long getIdFrecuenciaPagar()
  {
    return jdoGetidFrecuenciaPagar(this);
  }

  public void setFrecuenciaPago(FrecuenciaPago pago)
  {
    jdoSetfrecuenciaPago(this, pago);
  }

  public void setGrupoNomina(GrupoNomina nomina)
  {
    jdoSetgrupoNomina(this, nomina);
  }

  public void setIdFrecuenciaPagar(long l)
  {
    jdoSetidFrecuenciaPagar(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoNomina.FrecuenciaPagar"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new FrecuenciaPagar());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    FrecuenciaPagar localFrecuenciaPagar = new FrecuenciaPagar();
    localFrecuenciaPagar.jdoFlags = 1;
    localFrecuenciaPagar.jdoStateManager = paramStateManager;
    return localFrecuenciaPagar;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    FrecuenciaPagar localFrecuenciaPagar = new FrecuenciaPagar();
    localFrecuenciaPagar.jdoCopyKeyFieldsFromObjectId(paramObject);
    localFrecuenciaPagar.jdoFlags = 1;
    localFrecuenciaPagar.jdoStateManager = paramStateManager;
    return localFrecuenciaPagar;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaPago);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idFrecuenciaPagar);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaPago = ((FrecuenciaPago)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idFrecuenciaPagar = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(FrecuenciaPagar paramFrecuenciaPagar, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramFrecuenciaPagar == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaPago = paramFrecuenciaPagar.frecuenciaPago;
      return;
    case 1:
      if (paramFrecuenciaPagar == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramFrecuenciaPagar.grupoNomina;
      return;
    case 2:
      if (paramFrecuenciaPagar == null)
        throw new IllegalArgumentException("arg1");
      this.idFrecuenciaPagar = paramFrecuenciaPagar.idFrecuenciaPagar;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof FrecuenciaPagar))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    FrecuenciaPagar localFrecuenciaPagar = (FrecuenciaPagar)paramObject;
    if (localFrecuenciaPagar.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localFrecuenciaPagar, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new FrecuenciaPagarPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new FrecuenciaPagarPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FrecuenciaPagarPK))
      throw new IllegalArgumentException("arg1");
    FrecuenciaPagarPK localFrecuenciaPagarPK = (FrecuenciaPagarPK)paramObject;
    localFrecuenciaPagarPK.idFrecuenciaPagar = this.idFrecuenciaPagar;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FrecuenciaPagarPK))
      throw new IllegalArgumentException("arg1");
    FrecuenciaPagarPK localFrecuenciaPagarPK = (FrecuenciaPagarPK)paramObject;
    this.idFrecuenciaPagar = localFrecuenciaPagarPK.idFrecuenciaPagar;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FrecuenciaPagarPK))
      throw new IllegalArgumentException("arg2");
    FrecuenciaPagarPK localFrecuenciaPagarPK = (FrecuenciaPagarPK)paramObject;
    localFrecuenciaPagarPK.idFrecuenciaPagar = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FrecuenciaPagarPK))
      throw new IllegalArgumentException("arg2");
    FrecuenciaPagarPK localFrecuenciaPagarPK = (FrecuenciaPagarPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localFrecuenciaPagarPK.idFrecuenciaPagar);
  }

  private static final FrecuenciaPago jdoGetfrecuenciaPago(FrecuenciaPagar paramFrecuenciaPagar)
  {
    StateManager localStateManager = paramFrecuenciaPagar.jdoStateManager;
    if (localStateManager == null)
      return paramFrecuenciaPagar.frecuenciaPago;
    if (localStateManager.isLoaded(paramFrecuenciaPagar, jdoInheritedFieldCount + 0))
      return paramFrecuenciaPagar.frecuenciaPago;
    return (FrecuenciaPago)localStateManager.getObjectField(paramFrecuenciaPagar, jdoInheritedFieldCount + 0, paramFrecuenciaPagar.frecuenciaPago);
  }

  private static final void jdoSetfrecuenciaPago(FrecuenciaPagar paramFrecuenciaPagar, FrecuenciaPago paramFrecuenciaPago)
  {
    StateManager localStateManager = paramFrecuenciaPagar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaPagar.frecuenciaPago = paramFrecuenciaPago;
      return;
    }
    localStateManager.setObjectField(paramFrecuenciaPagar, jdoInheritedFieldCount + 0, paramFrecuenciaPagar.frecuenciaPago, paramFrecuenciaPago);
  }

  private static final GrupoNomina jdoGetgrupoNomina(FrecuenciaPagar paramFrecuenciaPagar)
  {
    StateManager localStateManager = paramFrecuenciaPagar.jdoStateManager;
    if (localStateManager == null)
      return paramFrecuenciaPagar.grupoNomina;
    if (localStateManager.isLoaded(paramFrecuenciaPagar, jdoInheritedFieldCount + 1))
      return paramFrecuenciaPagar.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramFrecuenciaPagar, jdoInheritedFieldCount + 1, paramFrecuenciaPagar.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(FrecuenciaPagar paramFrecuenciaPagar, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramFrecuenciaPagar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaPagar.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramFrecuenciaPagar, jdoInheritedFieldCount + 1, paramFrecuenciaPagar.grupoNomina, paramGrupoNomina);
  }

  private static final long jdoGetidFrecuenciaPagar(FrecuenciaPagar paramFrecuenciaPagar)
  {
    return paramFrecuenciaPagar.idFrecuenciaPagar;
  }

  private static final void jdoSetidFrecuenciaPagar(FrecuenciaPagar paramFrecuenciaPagar, long paramLong)
  {
    StateManager localStateManager = paramFrecuenciaPagar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFrecuenciaPagar.idFrecuenciaPagar = paramLong;
      return;
    }
    localStateManager.setLongField(paramFrecuenciaPagar, jdoInheritedFieldCount + 2, paramFrecuenciaPagar.idFrecuenciaPagar, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}