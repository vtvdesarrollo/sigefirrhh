package sigefirrhh.personal.procesoNomina;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class UltimaNomina
  implements Serializable, PersistenceCapable
{
  private long idUltimaNomina;
  private int numeroNomina;
  private double unidades;
  private double montoAsigna;
  private double montoDeduce;
  private String origen;
  private String documentoSoporte;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private Trabajador trabajador;
  private GrupoNomina grupoNomina;
  private TipoPersonal tipoPersonal;
  private NominaEspecial nominaEspecial;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "documentoSoporte", "frecuenciaTipoPersonal", "grupoNomina", "idUltimaNomina", "montoAsigna", "montoDeduce", "nominaEspecial", "numeroNomina", "origen", "tipoPersonal", "trabajador", "unidades" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), Long.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.procesoNomina.NominaEspecial"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), Double.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 21, 26, 26, 24, 21, 21, 26, 21, 21, 26, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal()
  {
    return jdoGetfrecuenciaTipoPersonal(this);
  }

  public GrupoNomina getGrupoNomina()
  {
    return jdoGetgrupoNomina(this);
  }

  public long getIdUltimaNomina()
  {
    return jdoGetidUltimaNomina(this);
  }

  public double getMontoAsigna()
  {
    return jdoGetmontoAsigna(this);
  }

  public double getMontoDeduce()
  {
    return jdoGetmontoDeduce(this);
  }

  public NominaEspecial getNominaEspecial()
  {
    return jdoGetnominaEspecial(this);
  }

  public int getNumeroNomina()
  {
    return jdoGetnumeroNomina(this);
  }

  public String getOrigen()
  {
    return jdoGetorigen(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public double getUnidades()
  {
    return jdoGetunidades(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setFrecuenciaTipoPersonal(FrecuenciaTipoPersonal personal)
  {
    jdoSetfrecuenciaTipoPersonal(this, personal);
  }

  public void setGrupoNomina(GrupoNomina nomina)
  {
    jdoSetgrupoNomina(this, nomina);
  }

  public void setIdUltimaNomina(long l)
  {
    jdoSetidUltimaNomina(this, l);
  }

  public void setMontoAsigna(double d)
  {
    jdoSetmontoAsigna(this, d);
  }

  public void setMontoDeduce(double d)
  {
    jdoSetmontoDeduce(this, d);
  }

  public void setNominaEspecial(NominaEspecial especial)
  {
    jdoSetnominaEspecial(this, especial);
  }

  public void setNumeroNomina(int i)
  {
    jdoSetnumeroNomina(this, i);
  }

  public void setOrigen(String string)
  {
    jdoSetorigen(this, string);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setUnidades(double d)
  {
    jdoSetunidades(this, d);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoNomina.UltimaNomina"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UltimaNomina());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UltimaNomina localUltimaNomina = new UltimaNomina();
    localUltimaNomina.jdoFlags = 1;
    localUltimaNomina.jdoStateManager = paramStateManager;
    return localUltimaNomina;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UltimaNomina localUltimaNomina = new UltimaNomina();
    localUltimaNomina.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUltimaNomina.jdoFlags = 1;
    localUltimaNomina.jdoStateManager = paramStateManager;
    return localUltimaNomina;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaTipoPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUltimaNomina);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAsigna);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoDeduce);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nominaEspecial);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroNomina);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origen);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaTipoPersonal = ((FrecuenciaTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUltimaNomina = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAsigna = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoDeduce = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nominaEspecial = ((NominaEspecial)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origen = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UltimaNomina paramUltimaNomina, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramUltimaNomina.conceptoTipoPersonal;
      return;
    case 1:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramUltimaNomina.documentoSoporte;
      return;
    case 2:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaTipoPersonal = paramUltimaNomina.frecuenciaTipoPersonal;
      return;
    case 3:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramUltimaNomina.grupoNomina;
      return;
    case 4:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.idUltimaNomina = paramUltimaNomina.idUltimaNomina;
      return;
    case 5:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.montoAsigna = paramUltimaNomina.montoAsigna;
      return;
    case 6:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.montoDeduce = paramUltimaNomina.montoDeduce;
      return;
    case 7:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.nominaEspecial = paramUltimaNomina.nominaEspecial;
      return;
    case 8:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.numeroNomina = paramUltimaNomina.numeroNomina;
      return;
    case 9:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.origen = paramUltimaNomina.origen;
      return;
    case 10:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramUltimaNomina.tipoPersonal;
      return;
    case 11:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramUltimaNomina.trabajador;
      return;
    case 12:
      if (paramUltimaNomina == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramUltimaNomina.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UltimaNomina))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UltimaNomina localUltimaNomina = (UltimaNomina)paramObject;
    if (localUltimaNomina.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUltimaNomina, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UltimaNominaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UltimaNominaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UltimaNominaPK))
      throw new IllegalArgumentException("arg1");
    UltimaNominaPK localUltimaNominaPK = (UltimaNominaPK)paramObject;
    localUltimaNominaPK.idUltimaNomina = this.idUltimaNomina;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UltimaNominaPK))
      throw new IllegalArgumentException("arg1");
    UltimaNominaPK localUltimaNominaPK = (UltimaNominaPK)paramObject;
    this.idUltimaNomina = localUltimaNominaPK.idUltimaNomina;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UltimaNominaPK))
      throw new IllegalArgumentException("arg2");
    UltimaNominaPK localUltimaNominaPK = (UltimaNominaPK)paramObject;
    localUltimaNominaPK.idUltimaNomina = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UltimaNominaPK))
      throw new IllegalArgumentException("arg2");
    UltimaNominaPK localUltimaNominaPK = (UltimaNominaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localUltimaNominaPK.idUltimaNomina);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(UltimaNomina paramUltimaNomina)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 0))
      return paramUltimaNomina.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramUltimaNomina, jdoInheritedFieldCount + 0, paramUltimaNomina.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(UltimaNomina paramUltimaNomina, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramUltimaNomina, jdoInheritedFieldCount + 0, paramUltimaNomina.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGetdocumentoSoporte(UltimaNomina paramUltimaNomina)
  {
    if (paramUltimaNomina.jdoFlags <= 0)
      return paramUltimaNomina.documentoSoporte;
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.documentoSoporte;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 1))
      return paramUltimaNomina.documentoSoporte;
    return localStateManager.getStringField(paramUltimaNomina, jdoInheritedFieldCount + 1, paramUltimaNomina.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(UltimaNomina paramUltimaNomina, String paramString)
  {
    if (paramUltimaNomina.jdoFlags == 0)
    {
      paramUltimaNomina.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramUltimaNomina, jdoInheritedFieldCount + 1, paramUltimaNomina.documentoSoporte, paramString);
  }

  private static final FrecuenciaTipoPersonal jdoGetfrecuenciaTipoPersonal(UltimaNomina paramUltimaNomina)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.frecuenciaTipoPersonal;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 2))
      return paramUltimaNomina.frecuenciaTipoPersonal;
    return (FrecuenciaTipoPersonal)localStateManager.getObjectField(paramUltimaNomina, jdoInheritedFieldCount + 2, paramUltimaNomina.frecuenciaTipoPersonal);
  }

  private static final void jdoSetfrecuenciaTipoPersonal(UltimaNomina paramUltimaNomina, FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.frecuenciaTipoPersonal = paramFrecuenciaTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramUltimaNomina, jdoInheritedFieldCount + 2, paramUltimaNomina.frecuenciaTipoPersonal, paramFrecuenciaTipoPersonal);
  }

  private static final GrupoNomina jdoGetgrupoNomina(UltimaNomina paramUltimaNomina)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.grupoNomina;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 3))
      return paramUltimaNomina.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramUltimaNomina, jdoInheritedFieldCount + 3, paramUltimaNomina.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(UltimaNomina paramUltimaNomina, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramUltimaNomina, jdoInheritedFieldCount + 3, paramUltimaNomina.grupoNomina, paramGrupoNomina);
  }

  private static final long jdoGetidUltimaNomina(UltimaNomina paramUltimaNomina)
  {
    return paramUltimaNomina.idUltimaNomina;
  }

  private static final void jdoSetidUltimaNomina(UltimaNomina paramUltimaNomina, long paramLong)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.idUltimaNomina = paramLong;
      return;
    }
    localStateManager.setLongField(paramUltimaNomina, jdoInheritedFieldCount + 4, paramUltimaNomina.idUltimaNomina, paramLong);
  }

  private static final double jdoGetmontoAsigna(UltimaNomina paramUltimaNomina)
  {
    if (paramUltimaNomina.jdoFlags <= 0)
      return paramUltimaNomina.montoAsigna;
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.montoAsigna;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 5))
      return paramUltimaNomina.montoAsigna;
    return localStateManager.getDoubleField(paramUltimaNomina, jdoInheritedFieldCount + 5, paramUltimaNomina.montoAsigna);
  }

  private static final void jdoSetmontoAsigna(UltimaNomina paramUltimaNomina, double paramDouble)
  {
    if (paramUltimaNomina.jdoFlags == 0)
    {
      paramUltimaNomina.montoAsigna = paramDouble;
      return;
    }
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.montoAsigna = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramUltimaNomina, jdoInheritedFieldCount + 5, paramUltimaNomina.montoAsigna, paramDouble);
  }

  private static final double jdoGetmontoDeduce(UltimaNomina paramUltimaNomina)
  {
    if (paramUltimaNomina.jdoFlags <= 0)
      return paramUltimaNomina.montoDeduce;
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.montoDeduce;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 6))
      return paramUltimaNomina.montoDeduce;
    return localStateManager.getDoubleField(paramUltimaNomina, jdoInheritedFieldCount + 6, paramUltimaNomina.montoDeduce);
  }

  private static final void jdoSetmontoDeduce(UltimaNomina paramUltimaNomina, double paramDouble)
  {
    if (paramUltimaNomina.jdoFlags == 0)
    {
      paramUltimaNomina.montoDeduce = paramDouble;
      return;
    }
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.montoDeduce = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramUltimaNomina, jdoInheritedFieldCount + 6, paramUltimaNomina.montoDeduce, paramDouble);
  }

  private static final NominaEspecial jdoGetnominaEspecial(UltimaNomina paramUltimaNomina)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.nominaEspecial;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 7))
      return paramUltimaNomina.nominaEspecial;
    return (NominaEspecial)localStateManager.getObjectField(paramUltimaNomina, jdoInheritedFieldCount + 7, paramUltimaNomina.nominaEspecial);
  }

  private static final void jdoSetnominaEspecial(UltimaNomina paramUltimaNomina, NominaEspecial paramNominaEspecial)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.nominaEspecial = paramNominaEspecial;
      return;
    }
    localStateManager.setObjectField(paramUltimaNomina, jdoInheritedFieldCount + 7, paramUltimaNomina.nominaEspecial, paramNominaEspecial);
  }

  private static final int jdoGetnumeroNomina(UltimaNomina paramUltimaNomina)
  {
    if (paramUltimaNomina.jdoFlags <= 0)
      return paramUltimaNomina.numeroNomina;
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.numeroNomina;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 8))
      return paramUltimaNomina.numeroNomina;
    return localStateManager.getIntField(paramUltimaNomina, jdoInheritedFieldCount + 8, paramUltimaNomina.numeroNomina);
  }

  private static final void jdoSetnumeroNomina(UltimaNomina paramUltimaNomina, int paramInt)
  {
    if (paramUltimaNomina.jdoFlags == 0)
    {
      paramUltimaNomina.numeroNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.numeroNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramUltimaNomina, jdoInheritedFieldCount + 8, paramUltimaNomina.numeroNomina, paramInt);
  }

  private static final String jdoGetorigen(UltimaNomina paramUltimaNomina)
  {
    if (paramUltimaNomina.jdoFlags <= 0)
      return paramUltimaNomina.origen;
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.origen;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 9))
      return paramUltimaNomina.origen;
    return localStateManager.getStringField(paramUltimaNomina, jdoInheritedFieldCount + 9, paramUltimaNomina.origen);
  }

  private static final void jdoSetorigen(UltimaNomina paramUltimaNomina, String paramString)
  {
    if (paramUltimaNomina.jdoFlags == 0)
    {
      paramUltimaNomina.origen = paramString;
      return;
    }
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.origen = paramString;
      return;
    }
    localStateManager.setStringField(paramUltimaNomina, jdoInheritedFieldCount + 9, paramUltimaNomina.origen, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(UltimaNomina paramUltimaNomina)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.tipoPersonal;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 10))
      return paramUltimaNomina.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramUltimaNomina, jdoInheritedFieldCount + 10, paramUltimaNomina.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(UltimaNomina paramUltimaNomina, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramUltimaNomina, jdoInheritedFieldCount + 10, paramUltimaNomina.tipoPersonal, paramTipoPersonal);
  }

  private static final Trabajador jdoGettrabajador(UltimaNomina paramUltimaNomina)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.trabajador;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 11))
      return paramUltimaNomina.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramUltimaNomina, jdoInheritedFieldCount + 11, paramUltimaNomina.trabajador);
  }

  private static final void jdoSettrabajador(UltimaNomina paramUltimaNomina, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramUltimaNomina, jdoInheritedFieldCount + 11, paramUltimaNomina.trabajador, paramTrabajador);
  }

  private static final double jdoGetunidades(UltimaNomina paramUltimaNomina)
  {
    if (paramUltimaNomina.jdoFlags <= 0)
      return paramUltimaNomina.unidades;
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
      return paramUltimaNomina.unidades;
    if (localStateManager.isLoaded(paramUltimaNomina, jdoInheritedFieldCount + 12))
      return paramUltimaNomina.unidades;
    return localStateManager.getDoubleField(paramUltimaNomina, jdoInheritedFieldCount + 12, paramUltimaNomina.unidades);
  }

  private static final void jdoSetunidades(UltimaNomina paramUltimaNomina, double paramDouble)
  {
    if (paramUltimaNomina.jdoFlags == 0)
    {
      paramUltimaNomina.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramUltimaNomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramUltimaNomina.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramUltimaNomina, jdoInheritedFieldCount + 12, paramUltimaNomina.unidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}