package sigefirrhh.personal.procesoNomina;

import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.Mes;
import sigefirrhh.base.definiciones.MesNoGenBeanBusiness;
import sigefirrhh.base.definiciones.Semana;
import sigefirrhh.base.definiciones.SemanaNoGenBeanBusiness;

public class SeguridadOrdinariaNoGenBeanBusiness
{
  Logger log = Logger.getLogger(SeguridadOrdinariaNoGenBeanBusiness.class.getName());

  public SeguridadOrdinaria findNominaMre(long idGrupoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String cierreDiplomatico = "N";
    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina && cierreDiplomatico == pCierreDiplomatico";
    Query query = null;
    try {
      query = pm.newQuery(SeguridadOrdinaria.class, filter);
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
    query.declareParameters("long pIdGrupoNomina, String pCierreDiplomatico");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pCierreDiplomatico", cierreDiplomatico);
    query.setOrdering("fechaFin descending");
    Collection colSeguridadOrdinaria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadOrdinaria);

    Iterator iterator = colSeguridadOrdinaria.iterator();
    SeguridadOrdinaria seguridadOrdinaria = (SeguridadOrdinaria)iterator.next();

    return seguridadOrdinaria;
  }

  public Collection findFechaProximaNomina(long idGrupoNomina)
    throws Exception
  {
    int semanaMes = 0;
    int semanaAnio = 0;
    int lunesPrQuincena = 0;
    int lunesSeQuincena = 0;
    boolean haveSemana5 = false;
    int numeroSemanasMes = 0;
    int mesSemanal = 0;
    int anioSemanal = 0;
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina";
    Query query = null;
    try {
      query = pm.newQuery(SeguridadOrdinaria.class, filter);
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
    query.declareParameters("long pIdGrupoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    query.setOrdering("fechaFin descending");
    Collection colSeguridadOrdinaria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadOrdinaria);

    Iterator iterator = colSeguridadOrdinaria.iterator();
    SeguridadOrdinaria seguridadOrdinaria = (SeguridadOrdinaria)iterator.next();

    Calendar fechaInicio = Calendar.getInstance();
    fechaInicio.setTime(seguridadOrdinaria.getFechaFin());
    fechaInicio.add(5, 1);

    Calendar fechaFin = Calendar.getInstance();

    if (seguridadOrdinaria.getGrupoNomina().getPeriodicidad().equals("M"))
    {
      fechaFin.set(fechaInicio.getTime().getYear() + 1900, fechaInicio.getTime().getMonth(), 1);
      fechaFin.add(2, 1);
      fechaFin.add(5, -1);
      this.log.error(fechaFin.getTime());
    } else if (seguridadOrdinaria.getGrupoNomina().getPeriodicidad().equals("Q"))
    {
      if (fechaInicio.getTime().getDate() == 1)
      {
        fechaFin.set(fechaInicio.getTime().getYear() + 1900, fechaInicio.getTime().getMonth(), fechaInicio.getTime().getDate() + 14);
      }
      else {
        fechaFin.set(fechaInicio.getTime().getYear() + 1900, fechaInicio.getTime().getMonth(), 1);
        fechaFin.add(2, 1);
        fechaFin.add(5, -1);
      }
    }
    else
    {
      SemanaNoGenBeanBusiness semanaNoGenBeanBusiness = new SemanaNoGenBeanBusiness();

      Semana semana = new Semana();
      semana = semanaNoGenBeanBusiness.findByGrupoNominaAndFechaInicio(idGrupoNomina, fechaInicio.getTime());
      fechaFin.setTime(semana.getFechaFin());
      semanaMes = semana.getSemanaMes();
      semanaAnio = semana.getSemanaAnio();
      mesSemanal = Integer.valueOf(semana.getMes()).intValue();
      anioSemanal = semana.getAnio();

      haveSemana5 = semanaNoGenBeanBusiness.tieneSemana5(fechaFin.getTime().getYear() + 1900, String.valueOf(fechaFin.getTime().getMonth() + 1));

      Semana numeroSemanas = new Semana();

      numeroSemanas = semanaNoGenBeanBusiness.findNumeroSemanasMes(idGrupoNomina, fechaFin.getTime().getYear() + 1900, String.valueOf(fechaFin.getTime().getMonth() + 1));
      numeroSemanasMes = numeroSemanas.getSemanaMes();
    }

    if ((seguridadOrdinaria.getGrupoNomina().getPeriodicidad().equals("M")) || (seguridadOrdinaria.getGrupoNomina().getPeriodicidad().equals("Q")))
    {
      MesNoGenBeanBusiness mesNoGenBeanBusiness = new MesNoGenBeanBusiness();
      Mes mes = new Mes();
      mes = mesNoGenBeanBusiness.finMesByAnioAndMes(fechaInicio.getTime().getYear() + 1900, String.valueOf(fechaInicio.getTime().getMonth() + 1));
      lunesPrQuincena = mes.getLunesPrQuincena();
      lunesSeQuincena = mes.getLunesSeQuincena();
    }

    Collection col = new ArrayList();
    col.add(fechaInicio);
    col.add(fechaFin);
    col.add(new Integer(lunesPrQuincena));
    col.add(new Integer(lunesSeQuincena));
    col.add(Boolean.valueOf(haveSemana5));
    col.add(new Integer(semanaMes));
    col.add(new Integer(semanaAnio));
    col.add(seguridadOrdinaria.getGrupoNomina().getPeriodicidad());
    col.add(new Integer(numeroSemanasMes));
    col.add(new Integer(mesSemanal));
    col.add(new Integer(anioSemanal));
    return col;
  }

  public Collection findFechaUltimaNomina(long idGrupoNomina)
    throws Exception
  {
    int semanaMes = 0;
    int semanaAnio = 0;
    int lunesPrQuincena = 0;
    int lunesSeQuincena = 0;
    boolean haveSemana5 = false;
    int numeroSemanasMes = 0;

    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina";
    Query query = null;
    try {
      query = pm.newQuery(SeguridadOrdinaria.class, filter);
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
    query.declareParameters("long pIdGrupoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    query.setOrdering("fechaFin descending");
    Collection colSeguridadOrdinaria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadOrdinaria);

    Iterator iterator = colSeguridadOrdinaria.iterator();
    SeguridadOrdinaria seguridadOrdinaria = (SeguridadOrdinaria)iterator.next();

    Calendar fechaInicio = Calendar.getInstance();
    fechaInicio.setTime(seguridadOrdinaria.getFechaInicio());

    Calendar fechaFin = Calendar.getInstance();
    fechaFin.setTime(seguridadOrdinaria.getFechaFin());

    if (seguridadOrdinaria.getGrupoNomina().getPeriodicidad().equals("S"))
    {
      SemanaNoGenBeanBusiness semanaNoGenBeanBusiness = new SemanaNoGenBeanBusiness();

      Semana semana = new Semana();
      semana = semanaNoGenBeanBusiness.findByGrupoNominaAndFechaInicio(idGrupoNomina, fechaInicio.getTime());
      semanaMes = semana.getSemanaMes();
      semanaAnio = semana.getSemanaAnio();
      haveSemana5 = semanaNoGenBeanBusiness.tieneSemana5(fechaInicio.getTime().getYear() + 1900, String.valueOf(fechaInicio.getTime().getMonth() + 1));

      Semana numeroSemanas = new Semana();

      numeroSemanas = semanaNoGenBeanBusiness.findNumeroSemanasMes(idGrupoNomina, fechaInicio.getTime().getYear() + 1900, String.valueOf(fechaInicio.getTime().getMonth() + 1));
      numeroSemanasMes = numeroSemanas.getSemanaMes();
    }

    if ((seguridadOrdinaria.getGrupoNomina().getPeriodicidad().equals("M")) || (seguridadOrdinaria.getGrupoNomina().getPeriodicidad().equals("Q")))
    {
      MesNoGenBeanBusiness mesNoGenBeanBusiness = new MesNoGenBeanBusiness();
      Mes mes = new Mes();
      mes = mesNoGenBeanBusiness.finMesByAnioAndMes(fechaInicio.getTime().getYear() + 1900, String.valueOf(fechaInicio.getTime().getMonth() + 1));
      lunesPrQuincena = mes.getLunesPrQuincena();
      lunesSeQuincena = mes.getLunesSeQuincena();
    }

    Collection col = new ArrayList();
    col.add(fechaInicio);
    col.add(fechaFin);
    col.add(new Integer(lunesPrQuincena));
    col.add(new Integer(lunesSeQuincena));
    col.add(Boolean.valueOf(haveSemana5));
    col.add(new Integer(semanaMes));
    col.add(new Integer(semanaAnio));
    col.add(seguridadOrdinaria.getGrupoNomina().getPeriodicidad());
    col.add(new Integer(numeroSemanasMes));
    return col;
  }

  public Collection findByGrupoNominaDesc(long idGrupoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina";

    Query query = pm.newQuery(SeguridadOrdinaria.class, filter);

    query.declareParameters("long pIdGrupoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));

    query.setOrdering("anio descending, mes descending, fechaInicio descending");

    Collection colSeguridadOrdinaria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadOrdinaria);

    return colSeguridadOrdinaria;
  }
}