package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FirmasReportes;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportNominaEspecialForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportNominaEspecialForm.class.getName());
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private String selectGrupoNomina;
  private String inicio;
  private String fin;
  private Calendar inicioAux;
  private Calendar finAux;
  private String mensaje = "";
  private long idGrupoNomina;
  private Collection listGrupoNomina;
  private long firma1;
  private Collection listFirma1;
  private long firma2;
  private Collection listFirma2;
  private long firma3;
  private Collection listFirma3;
  private long idUnidadAdministradora;
  private Collection listUnidadAdministradora;
  private String periodicidad;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private Integer semanaMes;
  private Integer lunesPrQuincena;
  private Integer lunesSeQuincena;
  private Boolean tieneSemana5;
  private Integer semanaAnio;
  private boolean show;
  private boolean show2;
  private boolean auxShow;
  private Collection listNominaEspecial;
  private String selectNominaEspecial;
  private NominaEspecial nominaEspecial;

  public void setSelectNominaEspecial(String selectNominaEspecial)
  {
    this.selectNominaEspecial = selectNominaEspecial;
  }
  public ReportNominaEspecialForm() {
    this.reportName = "nominarac";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.inicioAux = Calendar.getInstance();
    this.finAux = Calendar.getInstance();
    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportNominaEspecialForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void cambiarNombreAReporte()
  {
    if (this.tipoReporte == 1)
      this.reportName = "nominarac";
    else if (this.tipoReporte == 2)
      this.reportName = "nominaalf";
    else if (this.tipoReporte == 3)
      this.reportName = "nominauel";
    else if (this.tipoReporte == 4)
      this.reportName = "detconceptonoalf";
    else if (this.tipoReporte == 28)
      this.reportName = "detconceptonocod";
    else if (this.tipoReporte == 29)
      this.reportName = "detconceptonoced";
    else if (this.tipoReporte == 5)
      this.reportName = "resconceptono";
    else if (this.tipoReporte == 6)
      this.reportName = "resconceptonouel";
    else if (this.tipoReporte == 7)
      this.reportName = "depbannoalf";
    else if (this.tipoReporte == 8)
      this.reportName = "chequesno";
    else if (this.tipoReporte == 9)
      this.reportName = "aceppaguadm";
    else if (this.tipoReporte == 10)
      this.reportName = "aceppaglp";
    else if (this.tipoReporte == 15)
      this.reportName = "resaportesno";
    else if (this.tipoReporte == 16)
      this.reportName = "resaportesnouel";
    else if (this.tipoReporte == 20)
      this.reportName = "resconceptonouadm";
    else if (this.tipoReporte == 21)
      this.reportName = "aceppagdep";
    else if (this.tipoReporte == 22)
      this.reportName = "nominareg";
    else if (this.tipoReporte == 23)
      this.reportName = "nominadep";
    else if (this.tipoReporte == 24)
      this.reportName = "nominapr";
    else if (this.tipoReporte == 25)
      this.reportName = "depbannocod";
    else if (this.tipoReporte == 26)
      this.reportName = "depbannoced";
    else if (this.tipoReporte == 27)
      this.reportName = "depbannoua";
    else if (this.tipoReporte == 31)
      this.reportName = "depbannocedtipo";
    else if (this.tipoReporte == 400)
      this.reportName = "netopagar";
    else if (this.tipoReporte == 405)
      this.reportName = "netopagarsc";
    else if (this.tipoReporte == 70)
      this.reportName = "resconceptonocat";
    else if (this.tipoReporte == 71)
      this.reportName = "resconceptonocatuel";
    else if (this.tipoReporte == 100)
      this.reportName = "recibosalf";
    else if (this.tipoReporte == 101)
      this.reportName = "recibos2alfpptc";
    else if (this.tipoReporte == 102)
      this.reportName = "recibos3alfpptc";
    else if (this.tipoReporte == 103)
      this.reportName = "recibos4alfpptc";
    else if (this.tipoReporte == 104)
      this.reportName = "reciboscod";
    else if (this.tipoReporte == 105)
      this.reportName = "recibos2codpptc";
    else if (this.tipoReporte == 106)
      this.reportName = "recibos3codpptc";
    else if (this.tipoReporte == 107)
      this.reportName = "recibos4codpptc";
    else if (this.tipoReporte == 108)
      this.reportName = "recibosced";
    else if (this.tipoReporte == 109)
      this.reportName = "recibos2cedpptc";
    else if (this.tipoReporte == 110)
      this.reportName = "recibos3cedpptc";
    else if (this.tipoReporte == 111)
      this.reportName = "recibos4cedpptc";
    else if (this.tipoReporte == 112)
      this.reportName = "recibos3deppptc";
    else if (this.tipoReporte == 300)
      this.reportName = "aceppaguadm";
    else if (this.tipoReporte == 305)
      this.reportName = "aceppaglp";
    else if (this.tipoReporte == 310)
      this.reportName = "aceppagdep";
    else if (this.tipoReporte == 315)
      this.reportName = "aceppagdep0";
    else if (this.tipoReporte == 320) {
      this.reportName = "aceppaguel0";
    }

    if (this.idUnidadAdministradora != 0L)
      this.reportName += "_ua";
  }

  public void changeGrupoNomina(ValueChangeEvent event)
  {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.listNominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialByGrupoNominaAndEstatus(this.idGrupoNomina, "P", "N");

      this.show = true;
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeNominaEspecial(ValueChangeEvent event)
  {
    long idNominaEspecial = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.show = false;
      this.show2 = false;
      this.nominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialById(idNominaEspecial);
      this.inicioAux.setTime(this.nominaEspecial.getFechaInicio());
      this.finAux.setTime(this.nominaEspecial.getFechaFin());
      this.show2 = true;
      this.show = true;
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      this.listUnidadAdministradora = this.estructuraFacade.findUnidadAdministradoraByOrganismo(this.login.getIdOrganismo());
      this.listFirma1 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());
      this.listFirma2 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());
      this.listFirma3 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());

      log.error("Vacio:" + this.listGrupoNomina.isEmpty());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listGrupoNomina = new ArrayList();
      this.listUnidadAdministradora = new ArrayList();
      this.listFirma1 = new ArrayList();
      this.listFirma2 = new ArrayList();
      this.listFirma3 = new ArrayList();
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      Map parameters = new Hashtable();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("numero_nomina", new Integer(this.nominaEspecial.getNumeroNomina()));

      Date fec_ini = new Date(this.inicioAux.getTime().getYear(), this.inicioAux.getTime().getMonth(), this.inicioAux.getTime().getDate());
      Date fec_fin = new Date(this.finAux.getTime().getYear(), this.finAux.getTime().getMonth(), this.finAux.getTime().getDate());

      parameters.put("fec_ini", fec_ini);
      parameters.put("fec_fin", fec_fin);
      parameters.put("anio", new Integer(fec_ini.getYear()));

      parameters.put("id_grupo_nomina", new Long(this.idGrupoNomina));
      parameters.put("semana_anio", new Integer(0));
      parameters.put("nomina_estatus", this.nominaEspecial.getPersonal());
      parameters.put("nombre_nomina", this.nominaEspecial.getDescripcion());

      if (this.tipoReporte > 99) {
        parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/procesoNomina");
      }

      if ((this.tipoReporte > 99) && (this.mensaje != null)) {
        parameters.put("mensaje", this.mensaje);
      }

      if (this.firma1 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma1);
        parameters.put("nombre_uno", firma.getNombre());
        parameters.put("cargo_uno", firma.getCargo());
        if (firma.getNombramiento() == null)
          parameters.put("nombramiento_uno", "");
        else {
          parameters.put("nombramiento_uno", firma.getNombramiento());
        }
      }
      if (this.firma2 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma2);
        parameters.put("nombre_dos", firma.getNombre());
        parameters.put("cargo_dos", firma.getCargo());
        if (firma.getNombramiento() == null)
          parameters.put("nombramiento_dos", "");
        else {
          parameters.put("nombramiento_dos", firma.getNombramiento());
        }
      }
      if (this.firma3 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma3);
        parameters.put("nombre_tres", firma.getNombre());
        parameters.put("cargo_tres", firma.getCargo());
        if (firma.getNombramiento() == null)
          parameters.put("nombramiento_tres", "");
        else {
          parameters.put("nombramiento_tres", firma.getNombramiento());
        }
      }
      if (this.tipoReporte == 1)
        this.reportName = "nominarac";
      else if (this.tipoReporte == 2)
        this.reportName = "nominaalf";
      else if (this.tipoReporte == 3)
        this.reportName = "nominauel";
      else if (this.tipoReporte == 4)
        this.reportName = "detconceptono";
      else if (this.tipoReporte == 5)
        this.reportName = "resconceptono";
      else if (this.tipoReporte == 6)
        this.reportName = "resconceptonouel";
      else if (this.tipoReporte == 7)
        this.reportName = "depbannoalf";
      else if (this.tipoReporte == 8)
        this.reportName = "chequesno";
      else if (this.tipoReporte == 9)
        this.reportName = "aceppaguadm";
      else if (this.tipoReporte == 10)
        this.reportName = "aceppaglp";
      else if (this.tipoReporte == 15)
        this.reportName = "resaportesno";
      else if (this.tipoReporte == 16)
        this.reportName = "resaportesnouel";
      else if (this.tipoReporte == 20)
        this.reportName = "resconceptonouadm";
      else if (this.tipoReporte == 21)
        this.reportName = "aceppagdep";
      else if (this.tipoReporte == 22)
        this.reportName = "nominareg";
      else if (this.tipoReporte == 23)
        this.reportName = "nominadep";
      else if (this.tipoReporte == 24)
        this.reportName = "nominapr";
      else if (this.tipoReporte == 25)
        this.reportName = "depbannocod";
      else if (this.tipoReporte == 26)
        this.reportName = "depbannoced";
      else if (this.tipoReporte == 27)
        this.reportName = "depbannoua";
      else if (this.tipoReporte == 31)
        this.reportName = "depbannocedtipo";
      else if (this.tipoReporte == 400)
        this.reportName = "netopagar";
      else if (this.tipoReporte == 405)
        this.reportName = "netopagarsc";
      else if (this.tipoReporte == 70)
        this.reportName = "resconceptonocat";
      else if (this.tipoReporte == 71)
        this.reportName = "resconceptonocatuel";
      else if (this.tipoReporte == 100)
        this.reportName = "recibosalf";
      else if (this.tipoReporte == 101)
        this.reportName = "recibos2alfpptc";
      else if (this.tipoReporte == 102)
        this.reportName = "recibos3alfpptc";
      else if (this.tipoReporte == 103)
        this.reportName = "recibos4alfpptc";
      else if (this.tipoReporte == 104)
        this.reportName = "reciboscod";
      else if (this.tipoReporte == 105)
        this.reportName = "recibos2codpptc";
      else if (this.tipoReporte == 106)
        this.reportName = "recibos3codpptc";
      else if (this.tipoReporte == 107)
        this.reportName = "recibos4codpptc";
      else if (this.tipoReporte == 108)
        this.reportName = "recibosced";
      else if (this.tipoReporte == 109)
        this.reportName = "recibos2cedpptc";
      else if (this.tipoReporte == 110)
        this.reportName = "recibos3cedpptc";
      else if (this.tipoReporte == 111)
        this.reportName = "recibos4cedpptc";
      else if (this.tipoReporte == 112)
        this.reportName = "recibos3deppptc";
      else if (this.tipoReporte == 300)
        this.reportName = "aceppaguadm";
      else if (this.tipoReporte == 305)
        this.reportName = "aceppaglp";
      else if (this.tipoReporte == 310)
        this.reportName = "aceppagdep";
      else if (this.tipoReporte == 315)
        this.reportName = "aceppagdep0";
      else if (this.tipoReporte == 320) {
        this.reportName = "aceppaguel0";
      }

      if (this.idUnidadAdministradora != 0L) {
        this.reportName += "_ua";
        parameters.put("id_unidad_administradora", new Long(this.idUnidadAdministradora));
      }
      JasperForWeb report = new JasperForWeb();

      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/procesoNomina");

      report.start();
      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public Collection getListGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getListFirma1() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma1, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public Collection getListFirma2() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma2, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public Collection getListFirma3() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma3, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }
  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String string) {
    this.selectGrupoNomina = string;
  }
  public boolean isShow() {
    return this.show;
  }
  public int getTipoReporte() {
    return this.tipoReporte;
  }
  public void setTipoReporte(int i) {
    this.tipoReporte = i;
  }
  public long getIdGrupoNomina() {
    return this.idGrupoNomina;
  }
  public LoginSession getLogin() {
    return this.login;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setIdGrupoNomina(long l) {
    this.idGrupoNomina = l;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public long getFirma1()
  {
    return this.firma1;
  }

  public long getFirma2()
  {
    return this.firma2;
  }

  public long getFirma3()
  {
    return this.firma3;
  }

  public void setFirma1(long l)
  {
    this.firma1 = l;
  }

  public void setFirma2(long l)
  {
    this.firma2 = l;
  }

  public void setFirma3(long l)
  {
    this.firma3 = l;
  }

  public Collection getListNominaEspecial() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listNominaEspecial, "sigefirrhh.personal.procesoNomina.NominaEspecial");
  }
  public String getSelectNominaEspecial() {
    return this.selectNominaEspecial;
  }

  public String getFin() {
    if (this.finAux == null) {
      return null;
    }
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio() {
    if (this.inicioAux == null) {
      return null;
    }
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }

  public boolean isShow2()
  {
    return this.show2;
  }
  public void setFin(String string) {
    this.fin = string;
  }
  public void setInicio(String string) {
    this.inicio = string;
  }

  public long getIdUnidadAdministradora()
  {
    return this.idUnidadAdministradora;
  }

  public void setIdUnidadAdministradora(long l)
  {
    this.idUnidadAdministradora = l;
  }

  public void setListUnidadAdministradora(Collection collection)
  {
    this.listUnidadAdministradora = collection;
  }

  public String getMensaje() {
    return this.mensaje;
  }
  public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }
}