package sigefirrhh.personal.procesoNomina;

import java.io.Serializable;

public class TrabajadorDisquetePK
  implements Serializable
{
  public long idTrabajadorDisquete;

  public TrabajadorDisquetePK()
  {
  }

  public TrabajadorDisquetePK(long idTrabajadorDisquete)
  {
    this.idTrabajadorDisquete = idTrabajadorDisquete;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TrabajadorDisquetePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TrabajadorDisquetePK thatPK)
  {
    return 
      this.idTrabajadorDisquete == thatPK.idTrabajadorDisquete;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTrabajadorDisquete)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTrabajadorDisquete);
  }
}