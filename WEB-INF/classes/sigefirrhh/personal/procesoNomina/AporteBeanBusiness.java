package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import sigefirrhh.base.definiciones.GrupoNomina;

public class AporteBeanBusiness extends AbstractBeanBusiness
{
  public void actualizarAportesPatronales(GrupoNomina grupoNomina, long anio, long mes)
    throws Exception
  {
    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      StringBuffer sql = new StringBuffer();

      if (grupoNomina.getPeriodicidad().equalsIgnoreCase("S"))
        sql.append("select actualizar_aporte_patronal_historicos(?, ?, ?)");
      else {
        sql.append("select actualizar_aporte_patronal_historicoq(?, ?, ?)"); } st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, grupoNomina.getIdGrupoNomina());
      st.setInt(2, (int)anio);
      st.setInt(3, (int)mes);

      int valor = 0;
      rs = st.executeQuery();
      connection.commit();
      return; } finally { if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}