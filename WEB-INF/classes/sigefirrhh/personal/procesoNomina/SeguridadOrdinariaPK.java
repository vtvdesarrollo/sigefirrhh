package sigefirrhh.personal.procesoNomina;

import java.io.Serializable;

public class SeguridadOrdinariaPK
  implements Serializable
{
  public long idSeguridadOrdinaria;

  public SeguridadOrdinariaPK()
  {
  }

  public SeguridadOrdinariaPK(long idSeguridadOrdinaria)
  {
    this.idSeguridadOrdinaria = idSeguridadOrdinaria;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SeguridadOrdinariaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SeguridadOrdinariaPK thatPK)
  {
    return 
      this.idSeguridadOrdinaria == thatPK.idSeguridadOrdinaria;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSeguridadOrdinaria)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSeguridadOrdinaria);
  }
}