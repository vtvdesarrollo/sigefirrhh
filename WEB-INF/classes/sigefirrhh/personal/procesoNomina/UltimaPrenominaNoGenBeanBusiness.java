package sigefirrhh.personal.procesoNomina;

import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoBeanBusiness;

public class UltimaPrenominaNoGenBeanBusiness
{
  Logger log = Logger.getLogger(ConceptoBeanBusiness.class.getName());

  public Collection UltimaPrenominaNoGenBeanBusiness(long idGrupoNomina, long numeroNomina) throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina && numeroNomina == pNumeroNomina";
    Query query = pm.newQuery(UltimaPrenomina.class, filter);

    query.declareParameters("long pIdGrupoNomina, long pNumeroNomina");

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pNumeroNomina", new Long(numeroNomina));

    Collection colUltimaPrenomina = 
      new ArrayList((Collection)query.executeWithMap(parameters));
    this.log.error("colUltimaPrenomina size" + colUltimaPrenomina.size());
    return colUltimaPrenomina;
  }
}