package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ActualizacionAportePatronalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ActualizacionAportePatronalForm.class.getName());
  private int reportId;
  private String idGrupoNomina;
  private GrupoNomina grupoNomina = null;
  private String reportName;
  private String orden;
  private int mes;
  private int anio;
  private Collection listGrupoNomina;
  private DefinicionesNoGenFacade definicionesFacade = null;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = null;
  private LoginSession login;
  private TipoPersonal tipoPersonal;

  public ActualizacionAportePatronalForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
    this.reportName = "";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
      }

      public PhaseId getPhaseId()
      {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void changeGrupoNomina(ValueChangeEvent event)
  {
    long idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idGrupoNomina > 0L)
        this.grupoNomina = this.definicionesFacade.findGrupoNominaById(idGrupoNomina);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try {
      Date fechaActual = new Date();
      this.anio = (fechaActual.getYear() + 1900);
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listGrupoNomina = new ArrayList();
    }
  }

  public String ejecutar()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.procesoNominaNoGenFacade.actualizarAportesPatronales(this.grupoNomina, this.anio, this.mes);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.grupoNomina);

      context.addMessage("success_add", new FacesMessage("Se Ejecuto con éxito"));
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String orden) {
    this.orden = orden;
  }

  public int getAnio()
  {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }

  public int getMes()
  {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }

  public String getIdGrupoNomina() {
    return this.idGrupoNomina;
  }
  public void setIdGrupoNomina(String idGrupoNomina) {
    this.idGrupoNomina = idGrupoNomina;
  }
}