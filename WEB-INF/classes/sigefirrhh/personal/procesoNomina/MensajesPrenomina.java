package sigefirrhh.personal.procesoNomina;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.GrupoNomina;

public class MensajesPrenomina
  implements Serializable, PersistenceCapable
{
  private long idMensajesPrenomina;
  private GrupoNomina grupoNomina;
  private NominaEspecial nominaEspecial;
  private String mensaje;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "grupoNomina", "idMensajesPrenomina", "mensaje", "nominaEspecial" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.procesoNomina.NominaEspecial") };
  private static final byte[] jdoFieldFlags = { 26, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public long getIdMensajesPreNomina()
  {
    return jdoGetidMensajesPrenomina(this);
  }

  public void setIdMensajesPreNomina(long idMensajesPrenomina) {
    jdoSetidMensajesPrenomina(this, idMensajesPrenomina);
  }

  public GrupoNomina getGrupoNomina() {
    return jdoGetgrupoNomina(this);
  }

  public void setGrupoNomina(GrupoNomina grupoNomina) {
    jdoSetgrupoNomina(this, grupoNomina);
  }

  public NominaEspecial getNominaEspecial() {
    return jdoGetnominaEspecial(this);
  }

  public void setNominaEspecial(NominaEspecial nominaEspecial) {
    jdoSetnominaEspecial(this, nominaEspecial);
  }

  public String getMensaje() {
    return jdoGetmensaje(this);
  }

  public void setMensaje(String mensaje) {
    jdoSetmensaje(this, mensaje);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoNomina.MensajesPrenomina"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new MensajesPrenomina());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    MensajesPrenomina localMensajesPrenomina = new MensajesPrenomina();
    localMensajesPrenomina.jdoFlags = 1;
    localMensajesPrenomina.jdoStateManager = paramStateManager;
    return localMensajesPrenomina;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    MensajesPrenomina localMensajesPrenomina = new MensajesPrenomina();
    localMensajesPrenomina.jdoCopyKeyFieldsFromObjectId(paramObject);
    localMensajesPrenomina.jdoFlags = 1;
    localMensajesPrenomina.jdoStateManager = paramStateManager;
    return localMensajesPrenomina;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idMensajesPrenomina);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mensaje);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nominaEspecial);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idMensajesPrenomina = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mensaje = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nominaEspecial = ((NominaEspecial)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(MensajesPrenomina paramMensajesPrenomina, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramMensajesPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramMensajesPrenomina.grupoNomina;
      return;
    case 1:
      if (paramMensajesPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.idMensajesPrenomina = paramMensajesPrenomina.idMensajesPrenomina;
      return;
    case 2:
      if (paramMensajesPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.mensaje = paramMensajesPrenomina.mensaje;
      return;
    case 3:
      if (paramMensajesPrenomina == null)
        throw new IllegalArgumentException("arg1");
      this.nominaEspecial = paramMensajesPrenomina.nominaEspecial;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof MensajesPrenomina))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    MensajesPrenomina localMensajesPrenomina = (MensajesPrenomina)paramObject;
    if (localMensajesPrenomina.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localMensajesPrenomina, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new MensajesPrenominaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new MensajesPrenominaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MensajesPrenominaPK))
      throw new IllegalArgumentException("arg1");
    MensajesPrenominaPK localMensajesPrenominaPK = (MensajesPrenominaPK)paramObject;
    localMensajesPrenominaPK.idMensajesPrenomina = this.idMensajesPrenomina;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MensajesPrenominaPK))
      throw new IllegalArgumentException("arg1");
    MensajesPrenominaPK localMensajesPrenominaPK = (MensajesPrenominaPK)paramObject;
    this.idMensajesPrenomina = localMensajesPrenominaPK.idMensajesPrenomina;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MensajesPrenominaPK))
      throw new IllegalArgumentException("arg2");
    MensajesPrenominaPK localMensajesPrenominaPK = (MensajesPrenominaPK)paramObject;
    localMensajesPrenominaPK.idMensajesPrenomina = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MensajesPrenominaPK))
      throw new IllegalArgumentException("arg2");
    MensajesPrenominaPK localMensajesPrenominaPK = (MensajesPrenominaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localMensajesPrenominaPK.idMensajesPrenomina);
  }

  private static final GrupoNomina jdoGetgrupoNomina(MensajesPrenomina paramMensajesPrenomina)
  {
    StateManager localStateManager = paramMensajesPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramMensajesPrenomina.grupoNomina;
    if (localStateManager.isLoaded(paramMensajesPrenomina, jdoInheritedFieldCount + 0))
      return paramMensajesPrenomina.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramMensajesPrenomina, jdoInheritedFieldCount + 0, paramMensajesPrenomina.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(MensajesPrenomina paramMensajesPrenomina, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramMensajesPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramMensajesPrenomina.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramMensajesPrenomina, jdoInheritedFieldCount + 0, paramMensajesPrenomina.grupoNomina, paramGrupoNomina);
  }

  private static final long jdoGetidMensajesPrenomina(MensajesPrenomina paramMensajesPrenomina)
  {
    return paramMensajesPrenomina.idMensajesPrenomina;
  }

  private static final void jdoSetidMensajesPrenomina(MensajesPrenomina paramMensajesPrenomina, long paramLong)
  {
    StateManager localStateManager = paramMensajesPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramMensajesPrenomina.idMensajesPrenomina = paramLong;
      return;
    }
    localStateManager.setLongField(paramMensajesPrenomina, jdoInheritedFieldCount + 1, paramMensajesPrenomina.idMensajesPrenomina, paramLong);
  }

  private static final String jdoGetmensaje(MensajesPrenomina paramMensajesPrenomina)
  {
    if (paramMensajesPrenomina.jdoFlags <= 0)
      return paramMensajesPrenomina.mensaje;
    StateManager localStateManager = paramMensajesPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramMensajesPrenomina.mensaje;
    if (localStateManager.isLoaded(paramMensajesPrenomina, jdoInheritedFieldCount + 2))
      return paramMensajesPrenomina.mensaje;
    return localStateManager.getStringField(paramMensajesPrenomina, jdoInheritedFieldCount + 2, paramMensajesPrenomina.mensaje);
  }

  private static final void jdoSetmensaje(MensajesPrenomina paramMensajesPrenomina, String paramString)
  {
    if (paramMensajesPrenomina.jdoFlags == 0)
    {
      paramMensajesPrenomina.mensaje = paramString;
      return;
    }
    StateManager localStateManager = paramMensajesPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramMensajesPrenomina.mensaje = paramString;
      return;
    }
    localStateManager.setStringField(paramMensajesPrenomina, jdoInheritedFieldCount + 2, paramMensajesPrenomina.mensaje, paramString);
  }

  private static final NominaEspecial jdoGetnominaEspecial(MensajesPrenomina paramMensajesPrenomina)
  {
    StateManager localStateManager = paramMensajesPrenomina.jdoStateManager;
    if (localStateManager == null)
      return paramMensajesPrenomina.nominaEspecial;
    if (localStateManager.isLoaded(paramMensajesPrenomina, jdoInheritedFieldCount + 3))
      return paramMensajesPrenomina.nominaEspecial;
    return (NominaEspecial)localStateManager.getObjectField(paramMensajesPrenomina, jdoInheritedFieldCount + 3, paramMensajesPrenomina.nominaEspecial);
  }

  private static final void jdoSetnominaEspecial(MensajesPrenomina paramMensajesPrenomina, NominaEspecial paramNominaEspecial)
  {
    StateManager localStateManager = paramMensajesPrenomina.jdoStateManager;
    if (localStateManager == null)
    {
      paramMensajesPrenomina.nominaEspecial = paramNominaEspecial;
      return;
    }
    localStateManager.setObjectField(paramMensajesPrenomina, jdoInheritedFieldCount + 3, paramMensajesPrenomina.nominaEspecial, paramNominaEspecial);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}