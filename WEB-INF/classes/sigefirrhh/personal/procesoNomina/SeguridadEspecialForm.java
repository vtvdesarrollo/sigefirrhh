package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class SeguridadEspecialForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SeguridadEspecialForm.class.getName());
  private SeguridadEspecial seguridadEspecial;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private ProcesoNominaFacade procesoNominaFacade = new ProcesoNominaFacade();
  private boolean showSeguridadEspecialByAnio;
  private boolean showSeguridadEspecialByGrupoNomina;
  private int findAnio;
  private String findSelectGrupoNomina;
  private Collection findColGrupoNomina;
  private Collection colNominaEspecial;
  private Collection colGrupoNomina;
  private String selectNominaEspecial;
  private String selectGrupoNomina;
  private Object stateResultSeguridadEspecialByAnio = null;

  private Object stateResultSeguridadEspecialByGrupoNomina = null;

  public int getFindAnio()
  {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }
  public String getFindSelectGrupoNomina() {
    return this.findSelectGrupoNomina;
  }
  public void setFindSelectGrupoNomina(String valGrupoNomina) {
    this.findSelectGrupoNomina = valGrupoNomina;
  }

  public Collection getFindColGrupoNomina() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoNomina.getIdGrupoNomina()), 
        grupoNomina.toString()));
    }
    return col;
  }

  public String getSelectNominaEspecial()
  {
    return this.selectNominaEspecial;
  }
  public void setSelectNominaEspecial(String valNominaEspecial) {
    Iterator iterator = this.colNominaEspecial.iterator();
    NominaEspecial nominaEspecial = null;
    this.seguridadEspecial.setNominaEspecial(null);
    while (iterator.hasNext()) {
      nominaEspecial = (NominaEspecial)iterator.next();
      if (String.valueOf(nominaEspecial.getIdNominaEspecial()).equals(
        valNominaEspecial)) {
        this.seguridadEspecial.setNominaEspecial(
          nominaEspecial);
        break;
      }
    }
    this.selectNominaEspecial = valNominaEspecial;
  }
  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String valGrupoNomina) {
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    this.seguridadEspecial.setGrupoNomina(null);
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      if (String.valueOf(grupoNomina.getIdGrupoNomina()).equals(
        valGrupoNomina)) {
        this.seguridadEspecial.setGrupoNomina(
          grupoNomina);
        break;
      }
    }
    this.selectGrupoNomina = valGrupoNomina;
  }
  public Collection getResult() {
    return this.result;
  }

  public SeguridadEspecial getSeguridadEspecial() {
    if (this.seguridadEspecial == null) {
      this.seguridadEspecial = new SeguridadEspecial();
    }
    return this.seguridadEspecial;
  }

  public SeguridadEspecialForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColNominaEspecial()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colNominaEspecial.iterator();
    NominaEspecial nominaEspecial = null;
    while (iterator.hasNext()) {
      nominaEspecial = (NominaEspecial)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nominaEspecial.getIdNominaEspecial()), 
        nominaEspecial.toString()));
    }
    return col;
  }

  public Collection getColGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoNomina.iterator();
    GrupoNomina grupoNomina = null;
    while (iterator.hasNext()) {
      grupoNomina = (GrupoNomina)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoNomina.getIdGrupoNomina()), 
        grupoNomina.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColGrupoNomina = 
        this.definicionesFacade.findGrupoNominaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colNominaEspecial = 
        this.procesoNominaFacade.findAllNominaEspecial();
      this.colGrupoNomina = 
        this.definicionesFacade.findGrupoNominaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSeguridadEspecialByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      log.error("Se dispone a buscar la coleccion de SeguridadEspecial");
      this.result = 
        this.procesoNominaFacade.findSeguridadEspecialByAnio(this.findAnio);
      this.showSeguridadEspecialByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSeguridadEspecialByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findSelectGrupoNomina = null;

    return null;
  }

  public String findSeguridadEspecialByGrupoNomina()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.procesoNominaFacade.findSeguridadEspecialByGrupoNomina(Long.valueOf(this.findSelectGrupoNomina).longValue());
      this.showSeguridadEspecialByGrupoNomina = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSeguridadEspecialByGrupoNomina)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findSelectGrupoNomina = null;

    return null;
  }

  public boolean isShowSeguridadEspecialByAnio() {
    return this.showSeguridadEspecialByAnio;
  }
  public boolean isShowSeguridadEspecialByGrupoNomina() {
    return this.showSeguridadEspecialByGrupoNomina;
  }

  public String selectSeguridadEspecial()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectNominaEspecial = null;
    this.selectGrupoNomina = null;

    long idSeguridadEspecial = 
      Long.parseLong((String)requestParameterMap.get("idSeguridadEspecial"));
    try
    {
      this.seguridadEspecial = 
        this.procesoNominaFacade.findSeguridadEspecialById(
        idSeguridadEspecial);
      if (this.seguridadEspecial.getNominaEspecial() != null) {
        this.selectNominaEspecial = 
          String.valueOf(this.seguridadEspecial.getNominaEspecial().getIdNominaEspecial());
      }
      if (this.seguridadEspecial.getGrupoNomina() != null) {
        this.selectGrupoNomina = 
          String.valueOf(this.seguridadEspecial.getGrupoNomina().getIdGrupoNomina());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.seguridadEspecial = null;
    this.showSeguridadEspecialByAnio = false;
    this.showSeguridadEspecialByGrupoNomina = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.seguridadEspecial.getFechaInicio() != null) && 
      (this.seguridadEspecial.getFechaInicio().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Inicio período no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.seguridadEspecial.getFechaFin() != null) && 
      (this.seguridadEspecial.getFechaFin().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Fin período no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.seguridadEspecial.getFechaProceso() != null) && 
      (this.seguridadEspecial.getFechaProceso().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha de Proceso no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.procesoNominaFacade.addSeguridadEspecial(
          this.seguridadEspecial);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.procesoNominaFacade.updateSeguridadEspecial(
          this.seguridadEspecial);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);

      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.procesoNominaFacade.deleteSeguridadEspecial(
        this.seguridadEspecial);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.seguridadEspecial = new SeguridadEspecial();

    this.selectNominaEspecial = null;

    this.selectGrupoNomina = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.seguridadEspecial.setIdSeguridadEspecial(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.procesoNomina.SeguridadEspecial"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.seguridadEspecial = new SeguridadEspecial();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}