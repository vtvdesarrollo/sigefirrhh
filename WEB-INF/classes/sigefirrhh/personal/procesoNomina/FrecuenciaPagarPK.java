package sigefirrhh.personal.procesoNomina;

import java.io.Serializable;

public class FrecuenciaPagarPK
  implements Serializable
{
  public long idFrecuenciaPagar;

  public FrecuenciaPagarPK()
  {
  }

  public FrecuenciaPagarPK(long idFrecuenciaPagar)
  {
    this.idFrecuenciaPagar = idFrecuenciaPagar;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((FrecuenciaPagarPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(FrecuenciaPagarPK thatPK)
  {
    return 
      this.idFrecuenciaPagar == thatPK.idFrecuenciaPagar;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idFrecuenciaPagar)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idFrecuenciaPagar);
  }
}