package sigefirrhh.personal.procesoNomina;

import java.io.Serializable;

public class UltimaPrenominaPK
  implements Serializable
{
  public long idUltimaPrenomina;

  public UltimaPrenominaPK()
  {
  }

  public UltimaPrenominaPK(long idUltimaPrenomina)
  {
    this.idUltimaPrenomina = idUltimaPrenomina;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UltimaPrenominaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UltimaPrenominaPK thatPK)
  {
    return 
      this.idUltimaPrenomina == thatPK.idUltimaPrenomina;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUltimaPrenomina)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUltimaPrenomina);
  }
}