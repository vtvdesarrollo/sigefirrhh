package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.PrintStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportVariacionesPrenominaForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportVariacionesPrenominaForm.class.getName());
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private String selectGrupoNomina;
  private String inicio;
  private String fin;
  private Calendar inicioAux;
  private Calendar finAux;
  private long idGrupoNomina;
  private Collection listGrupoNomina;
  private String periodicidad = "";
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private Integer semanaMes;
  private Integer lunesPrQuincena;
  private Integer lunesSeQuincena;
  private Boolean tieneSemana5;
  private Integer semanaAnio;
  private Integer numeroSemanasMes;
  private Integer mesSemanal;
  private boolean show;
  private boolean auxShow;
  private long idUnidadAdministradora;
  private Collection listUnidadAdministradora;
  private boolean mensual = false;
  private boolean quincenal = false;
  private boolean semanal = false;
  private String periodoMensual;
  private String periodoQuincenal;
  private String periodoSemanal;

  public ReportVariacionesPrenominaForm()
  {
    this.reportName = "variacionesprsem";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportVariacionesPrenominaForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void cambiarNombreAReporte()
  {
    try {
      if (this.periodicidad.equals("S"))
        this.reportName = "variacionesprsem";
      else
        this.reportName = "variacionesprqui";
    }
    catch (Exception localException)
    {
    }
  }

  public void changeGrupoNomina(ValueChangeEvent event) {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.mensual = false;
      this.quincenal = false;
      this.semanal = false;
      Collection colPeriodo = this.procesoNominaNoGenFacade.findFechaProximaNomina(this.idGrupoNomina);
      Iterator iteratorPeriodo = colPeriodo.iterator();
      if (iteratorPeriodo.hasNext()) {
        this.inicioAux = ((Calendar)iteratorPeriodo.next());
        this.finAux = ((Calendar)iteratorPeriodo.next());
        this.lunesPrQuincena = ((Integer)iteratorPeriodo.next());
        this.lunesSeQuincena = ((Integer)iteratorPeriodo.next());
        this.tieneSemana5 = ((Boolean)iteratorPeriodo.next());
        this.semanaMes = ((Integer)iteratorPeriodo.next());
        this.semanaAnio = ((Integer)iteratorPeriodo.next());
        this.periodicidad = ((String)iteratorPeriodo.next());
        this.numeroSemanasMes = ((Integer)iteratorPeriodo.next());
        this.mesSemanal = ((Integer)iteratorPeriodo.next());

        this.auxShow = true;
        if (this.periodicidad.equals("M"))
          this.mensual = true;
        else if (this.periodicidad.equals("Q"))
          this.quincenal = true;
        else
          this.semanal = true;
      }
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      this.listUnidadAdministradora = this.estructuraFacade.findUnidadAdministradoraByOrganismo(this.login.getIdOrganismo());
      log.error("vacio:" + this.listGrupoNomina.isEmpty());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listGrupoNomina = new ArrayList();
      this.listUnidadAdministradora = new ArrayList();
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    Date fec_ini = new Date(this.inicioAux.getTime().getYear(), this.inicioAux.getTime().getMonth(), this.inicioAux.getTime().getDate());
    Date fec_fin = new Date(this.finAux.getTime().getYear(), this.finAux.getTime().getMonth(), this.finAux.getTime().getDate());
    parameters.put("fec_ini", fec_ini);
    parameters.put("fec_fin", fec_fin);
    parameters.put("id_grupo_nomina", new Long(this.idGrupoNomina));

    Calendar inicio2 = Calendar.getInstance();
    inicio2 = this.inicioAux;

    if (this.periodicidad.equals("M")) {
      log.error("es mensual");
      inicio2.add(2, -1);
      parameters.put("anio", new Integer(inicio2.getTime().getYear() + 1900));
      parameters.put("mes", new Integer(inicio2.getTime().getMonth() + 1));
      parameters.put("periodo", new Integer(2));
      parameters.put("comparado", "MES ANTERIOR");

      log.error("anio " + (inicio2.getTime().getYear() + 1900));
      log.error("mes " + (inicio2.getTime().getMonth() + 1));
      log.error("periodo 2");
      log.error("MES ANTERIOR");
    }
    else if (this.periodicidad.equals("Q"))
    {
      log.error("es quincenal");

      int semanaQuincena = 0;

      if (this.inicioAux.getTime().getDate() == 1)
        semanaQuincena = 1;
      else {
        semanaQuincena = 2;
      }
      if (this.periodoQuincenal.equals("MA"))
      {
        inicio2.add(2, -1);
        parameters.put("anio", new Integer(inicio2.getTime().getYear() + 1900));
        parameters.put("mes", new Integer(inicio2.getTime().getMonth() + 1));
        parameters.put("periodo", new Integer(semanaQuincena));

        log.error("MES ANTERIOR");
        log.error("anio " + (inicio2.getTime().getYear() + 1900));
        log.error("mes " + (inicio2.getTime().getMonth() + 1));
        log.error("periodo " + semanaQuincena);
      }
      else
      {
        if (this.inicioAux.getTime().getDate() == 1) {
          inicio2.add(2, -1);
          parameters.put("periodo", new Integer(2));
          System.out.print("periodo 2");
        } else {
          parameters.put("periodo", new Integer(1));
          System.out.print("periodo 1");
        }
        parameters.put("anio", new Integer(inicio2.getTime().getYear() + 1900));
        parameters.put("mes", new Integer(inicio2.getTime().getMonth() + 1));
        parameters.put("comparado", "QUINCENA ANTERIOR");

        log.error("anio " + (inicio2.getTime().getYear() + 1900));
        log.error("mes " + (inicio2.getTime().getMonth() + 1));
        log.error("QUINCENA ANTERIOR");
      }
    }
    else {
      inicio2.add(2, -1);
      parameters.put("anio", new Integer(inicio2.getTime().getYear() + 1900));
      parameters.put("mes", new Integer(inicio2.getTime().getMonth() + 1));
      parameters.put("periodo", new Integer(this.semanaAnio.intValue() - 1));
      parameters.put("comparado", "MES ANTERIOR");

      log.error("MES ANTERIOR");
      log.error("anio " + (inicio2.getTime().getYear() + 1900));
      log.error("mes " + (inicio2.getTime().getMonth() + 1));
      log.error("periodo " + (this.semanaAnio.intValue() - 1));
    }

    if (this.periodicidad.equals("S"))
      this.reportName = "variacionesprsem";
    else {
      this.reportName = "variacionesprqui";
    }

    JasperForWeb report = new JasperForWeb();
    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/procesoNomina");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public Collection getListGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }

  public String getSelectGrupoNomina()
  {
    return this.selectGrupoNomina;
  }

  public void setSelectGrupoNomina(String string)
  {
    this.selectGrupoNomina = string;
  }

  public String getFin()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }

  public void setFin(String string)
  {
    this.fin = string;
  }

  public void setInicio(String string)
  {
    this.inicio = string;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public long getIdGrupoNomina()
  {
    return this.idGrupoNomina;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setIdGrupoNomina(long l)
  {
    this.idGrupoNomina = l;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public long getIdUnidadAdministradora()
  {
    return this.idUnidadAdministradora;
  }

  public void setIdUnidadAdministradora(long l)
  {
    this.idUnidadAdministradora = l;
  }

  public boolean isMensual() {
    return this.mensual;
  }
  public void setMensual(boolean mensual) {
    this.mensual = mensual;
  }
  public boolean isQuincenal() {
    return this.quincenal;
  }
  public void setQuincenal(boolean quincenal) {
    this.quincenal = quincenal;
  }
  public boolean isSemanal() {
    return this.semanal;
  }
  public void setSemanal(boolean semanal) {
    this.semanal = semanal;
  }
  public String getPeriodoMensual() {
    return this.periodoMensual;
  }
  public void setPeriodoMensual(String periodoMensual) {
    this.periodoMensual = periodoMensual;
  }
  public String getPeriodoQuincenal() {
    return this.periodoQuincenal;
  }
  public void setPeriodoQuincenal(String periodoQuincenal) {
    this.periodoQuincenal = periodoQuincenal;
  }
  public String getPeriodoSemanal() {
    return this.periodoSemanal;
  }
  public void setPeriodoSemanal(String periodoSemanal) {
    this.periodoSemanal = periodoSemanal;
  }
}