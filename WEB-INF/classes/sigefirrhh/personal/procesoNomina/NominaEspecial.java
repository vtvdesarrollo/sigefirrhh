package sigefirrhh.personal.procesoNomina;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.GrupoNomina;

public class NominaEspecial
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_PERSONAL;
  protected static final Map LISTA_SINO;
  private long idNominaEspecial;
  private GrupoNomina grupoNomina;
  private int anio;
  private int numeroNomina;
  private String descripcion;
  private int mes;
  private Date fechaRegistro;
  private Date fechaProceso;
  private Date fechaInicio;
  private Date fechaFin;
  private String estatus;
  private String personal;
  private String pagada;
  private Date fechaPago;
  private String usuario;
  private FrecuenciaPago frecuenciaPago;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "descripcion", "estatus", "fechaFin", "fechaInicio", "fechaPago", "fechaProceso", "fechaRegistro", "frecuenciaPago", "grupoNomina", "idNominaEspecial", "mes", "numeroNomina", "pagada", "personal", "usuario" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaPago"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), Long.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 26, 26, 24, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoNomina.NominaEspecial"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new NominaEspecial());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_PERSONAL = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("A", "POR PROCESAR");
    LISTA_ESTATUS.put("P", "PROCESADA");
    LISTA_PERSONAL.put("A", "ACTIVO");
    LISTA_PERSONAL.put("E", "EGRESADO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public NominaEspecial()
  {
    jdoSetanio(this, 0);

    jdoSetmes(this, 0);

    jdoSetestatus(this, "A");

    jdoSetpersonal(this, "A");

    jdoSetpagada(this, "N");
  }

  public String toString()
  {
    return jdoGetgrupoNomina(this).getNombre() + " " + jdoGetdescripcion(this) + " " + jdoGetnumeroNomina(this);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public GrupoNomina getGrupoNomina()
  {
    return jdoGetgrupoNomina(this);
  }

  public long getIdNominaEspecial()
  {
    return jdoGetidNominaEspecial(this);
  }

  public int getNumeroNomina()
  {
    return jdoGetnumeroNomina(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setGrupoNomina(GrupoNomina nomina)
  {
    jdoSetgrupoNomina(this, nomina);
  }

  public void setIdNominaEspecial(long l)
  {
    jdoSetidNominaEspecial(this, l);
  }

  public void setNumeroNomina(int i)
  {
    jdoSetnumeroNomina(this, i);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public Date getFechaProceso()
  {
    return jdoGetfechaProceso(this);
  }

  public FrecuenciaPago getFrecuenciaPago()
  {
    return jdoGetfrecuenciaPago(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public String getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setFechaProceso(Date date)
  {
    jdoSetfechaProceso(this, date);
  }

  public void setFrecuenciaPago(FrecuenciaPago pago)
  {
    jdoSetfrecuenciaPago(this, pago);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setUsuario(String string)
  {
    jdoSetusuario(this, string);
  }

  public Date getFechaFin()
  {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio()
  {
    return jdoGetfechaInicio(this);
  }

  public void setFechaFin(Date date)
  {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date)
  {
    jdoSetfechaInicio(this, date);
  }

  public String getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(String personal) {
    jdoSetpersonal(this, personal);
  }
  public String getPagada() {
    return jdoGetpagada(this);
  }
  public void setPagada(String pagada) {
    jdoSetpagada(this, pagada);
  }
  public Date getFechaPago() {
    return jdoGetfechaPago(this);
  }
  public void setFechaPago(Date fechaPago) {
    jdoSetfechaPago(this, fechaPago);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 16;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    NominaEspecial localNominaEspecial = new NominaEspecial();
    localNominaEspecial.jdoFlags = 1;
    localNominaEspecial.jdoStateManager = paramStateManager;
    return localNominaEspecial;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    NominaEspecial localNominaEspecial = new NominaEspecial();
    localNominaEspecial.jdoCopyKeyFieldsFromObjectId(paramObject);
    localNominaEspecial.jdoFlags = 1;
    localNominaEspecial.jdoStateManager = paramStateManager;
    return localNominaEspecial;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaPago);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaPago);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idNominaEspecial);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroNomina);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.pagada);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.personal);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaPago = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaPago = ((FrecuenciaPago)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idNominaEspecial = localStateManager.replacingLongField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pagada = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(NominaEspecial paramNominaEspecial, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramNominaEspecial.anio;
      return;
    case 1:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramNominaEspecial.descripcion;
      return;
    case 2:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramNominaEspecial.estatus;
      return;
    case 3:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramNominaEspecial.fechaFin;
      return;
    case 4:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramNominaEspecial.fechaInicio;
      return;
    case 5:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.fechaPago = paramNominaEspecial.fechaPago;
      return;
    case 6:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramNominaEspecial.fechaProceso;
      return;
    case 7:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramNominaEspecial.fechaRegistro;
      return;
    case 8:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaPago = paramNominaEspecial.frecuenciaPago;
      return;
    case 9:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramNominaEspecial.grupoNomina;
      return;
    case 10:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.idNominaEspecial = paramNominaEspecial.idNominaEspecial;
      return;
    case 11:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramNominaEspecial.mes;
      return;
    case 12:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.numeroNomina = paramNominaEspecial.numeroNomina;
      return;
    case 13:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.pagada = paramNominaEspecial.pagada;
      return;
    case 14:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramNominaEspecial.personal;
      return;
    case 15:
      if (paramNominaEspecial == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramNominaEspecial.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof NominaEspecial))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    NominaEspecial localNominaEspecial = (NominaEspecial)paramObject;
    if (localNominaEspecial.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localNominaEspecial, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new NominaEspecialPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new NominaEspecialPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NominaEspecialPK))
      throw new IllegalArgumentException("arg1");
    NominaEspecialPK localNominaEspecialPK = (NominaEspecialPK)paramObject;
    localNominaEspecialPK.idNominaEspecial = this.idNominaEspecial;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NominaEspecialPK))
      throw new IllegalArgumentException("arg1");
    NominaEspecialPK localNominaEspecialPK = (NominaEspecialPK)paramObject;
    this.idNominaEspecial = localNominaEspecialPK.idNominaEspecial;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NominaEspecialPK))
      throw new IllegalArgumentException("arg2");
    NominaEspecialPK localNominaEspecialPK = (NominaEspecialPK)paramObject;
    localNominaEspecialPK.idNominaEspecial = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 10);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NominaEspecialPK))
      throw new IllegalArgumentException("arg2");
    NominaEspecialPK localNominaEspecialPK = (NominaEspecialPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 10, localNominaEspecialPK.idNominaEspecial);
  }

  private static final int jdoGetanio(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.anio;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.anio;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 0))
      return paramNominaEspecial.anio;
    return localStateManager.getIntField(paramNominaEspecial, jdoInheritedFieldCount + 0, paramNominaEspecial.anio);
  }

  private static final void jdoSetanio(NominaEspecial paramNominaEspecial, int paramInt)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramNominaEspecial, jdoInheritedFieldCount + 0, paramNominaEspecial.anio, paramInt);
  }

  private static final String jdoGetdescripcion(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.descripcion;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.descripcion;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 1))
      return paramNominaEspecial.descripcion;
    return localStateManager.getStringField(paramNominaEspecial, jdoInheritedFieldCount + 1, paramNominaEspecial.descripcion);
  }

  private static final void jdoSetdescripcion(NominaEspecial paramNominaEspecial, String paramString)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramNominaEspecial, jdoInheritedFieldCount + 1, paramNominaEspecial.descripcion, paramString);
  }

  private static final String jdoGetestatus(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.estatus;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.estatus;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 2))
      return paramNominaEspecial.estatus;
    return localStateManager.getStringField(paramNominaEspecial, jdoInheritedFieldCount + 2, paramNominaEspecial.estatus);
  }

  private static final void jdoSetestatus(NominaEspecial paramNominaEspecial, String paramString)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramNominaEspecial, jdoInheritedFieldCount + 2, paramNominaEspecial.estatus, paramString);
  }

  private static final Date jdoGetfechaFin(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.fechaFin;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.fechaFin;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 3))
      return paramNominaEspecial.fechaFin;
    return (Date)localStateManager.getObjectField(paramNominaEspecial, jdoInheritedFieldCount + 3, paramNominaEspecial.fechaFin);
  }

  private static final void jdoSetfechaFin(NominaEspecial paramNominaEspecial, Date paramDate)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramNominaEspecial, jdoInheritedFieldCount + 3, paramNominaEspecial.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.fechaInicio;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.fechaInicio;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 4))
      return paramNominaEspecial.fechaInicio;
    return (Date)localStateManager.getObjectField(paramNominaEspecial, jdoInheritedFieldCount + 4, paramNominaEspecial.fechaInicio);
  }

  private static final void jdoSetfechaInicio(NominaEspecial paramNominaEspecial, Date paramDate)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramNominaEspecial, jdoInheritedFieldCount + 4, paramNominaEspecial.fechaInicio, paramDate);
  }

  private static final Date jdoGetfechaPago(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.fechaPago;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.fechaPago;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 5))
      return paramNominaEspecial.fechaPago;
    return (Date)localStateManager.getObjectField(paramNominaEspecial, jdoInheritedFieldCount + 5, paramNominaEspecial.fechaPago);
  }

  private static final void jdoSetfechaPago(NominaEspecial paramNominaEspecial, Date paramDate)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.fechaPago = paramDate;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.fechaPago = paramDate;
      return;
    }
    localStateManager.setObjectField(paramNominaEspecial, jdoInheritedFieldCount + 5, paramNominaEspecial.fechaPago, paramDate);
  }

  private static final Date jdoGetfechaProceso(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.fechaProceso;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.fechaProceso;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 6))
      return paramNominaEspecial.fechaProceso;
    return (Date)localStateManager.getObjectField(paramNominaEspecial, jdoInheritedFieldCount + 6, paramNominaEspecial.fechaProceso);
  }

  private static final void jdoSetfechaProceso(NominaEspecial paramNominaEspecial, Date paramDate)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramNominaEspecial, jdoInheritedFieldCount + 6, paramNominaEspecial.fechaProceso, paramDate);
  }

  private static final Date jdoGetfechaRegistro(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.fechaRegistro;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.fechaRegistro;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 7))
      return paramNominaEspecial.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramNominaEspecial, jdoInheritedFieldCount + 7, paramNominaEspecial.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(NominaEspecial paramNominaEspecial, Date paramDate)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramNominaEspecial, jdoInheritedFieldCount + 7, paramNominaEspecial.fechaRegistro, paramDate);
  }

  private static final FrecuenciaPago jdoGetfrecuenciaPago(NominaEspecial paramNominaEspecial)
  {
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.frecuenciaPago;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 8))
      return paramNominaEspecial.frecuenciaPago;
    return (FrecuenciaPago)localStateManager.getObjectField(paramNominaEspecial, jdoInheritedFieldCount + 8, paramNominaEspecial.frecuenciaPago);
  }

  private static final void jdoSetfrecuenciaPago(NominaEspecial paramNominaEspecial, FrecuenciaPago paramFrecuenciaPago)
  {
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.frecuenciaPago = paramFrecuenciaPago;
      return;
    }
    localStateManager.setObjectField(paramNominaEspecial, jdoInheritedFieldCount + 8, paramNominaEspecial.frecuenciaPago, paramFrecuenciaPago);
  }

  private static final GrupoNomina jdoGetgrupoNomina(NominaEspecial paramNominaEspecial)
  {
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.grupoNomina;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 9))
      return paramNominaEspecial.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramNominaEspecial, jdoInheritedFieldCount + 9, paramNominaEspecial.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(NominaEspecial paramNominaEspecial, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramNominaEspecial, jdoInheritedFieldCount + 9, paramNominaEspecial.grupoNomina, paramGrupoNomina);
  }

  private static final long jdoGetidNominaEspecial(NominaEspecial paramNominaEspecial)
  {
    return paramNominaEspecial.idNominaEspecial;
  }

  private static final void jdoSetidNominaEspecial(NominaEspecial paramNominaEspecial, long paramLong)
  {
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.idNominaEspecial = paramLong;
      return;
    }
    localStateManager.setLongField(paramNominaEspecial, jdoInheritedFieldCount + 10, paramNominaEspecial.idNominaEspecial, paramLong);
  }

  private static final int jdoGetmes(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.mes;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.mes;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 11))
      return paramNominaEspecial.mes;
    return localStateManager.getIntField(paramNominaEspecial, jdoInheritedFieldCount + 11, paramNominaEspecial.mes);
  }

  private static final void jdoSetmes(NominaEspecial paramNominaEspecial, int paramInt)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramNominaEspecial, jdoInheritedFieldCount + 11, paramNominaEspecial.mes, paramInt);
  }

  private static final int jdoGetnumeroNomina(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.numeroNomina;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.numeroNomina;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 12))
      return paramNominaEspecial.numeroNomina;
    return localStateManager.getIntField(paramNominaEspecial, jdoInheritedFieldCount + 12, paramNominaEspecial.numeroNomina);
  }

  private static final void jdoSetnumeroNomina(NominaEspecial paramNominaEspecial, int paramInt)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.numeroNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.numeroNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramNominaEspecial, jdoInheritedFieldCount + 12, paramNominaEspecial.numeroNomina, paramInt);
  }

  private static final String jdoGetpagada(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.pagada;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.pagada;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 13))
      return paramNominaEspecial.pagada;
    return localStateManager.getStringField(paramNominaEspecial, jdoInheritedFieldCount + 13, paramNominaEspecial.pagada);
  }

  private static final void jdoSetpagada(NominaEspecial paramNominaEspecial, String paramString)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.pagada = paramString;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.pagada = paramString;
      return;
    }
    localStateManager.setStringField(paramNominaEspecial, jdoInheritedFieldCount + 13, paramNominaEspecial.pagada, paramString);
  }

  private static final String jdoGetpersonal(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.personal;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.personal;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 14))
      return paramNominaEspecial.personal;
    return localStateManager.getStringField(paramNominaEspecial, jdoInheritedFieldCount + 14, paramNominaEspecial.personal);
  }

  private static final void jdoSetpersonal(NominaEspecial paramNominaEspecial, String paramString)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.personal = paramString;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.personal = paramString;
      return;
    }
    localStateManager.setStringField(paramNominaEspecial, jdoInheritedFieldCount + 14, paramNominaEspecial.personal, paramString);
  }

  private static final String jdoGetusuario(NominaEspecial paramNominaEspecial)
  {
    if (paramNominaEspecial.jdoFlags <= 0)
      return paramNominaEspecial.usuario;
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
      return paramNominaEspecial.usuario;
    if (localStateManager.isLoaded(paramNominaEspecial, jdoInheritedFieldCount + 15))
      return paramNominaEspecial.usuario;
    return localStateManager.getStringField(paramNominaEspecial, jdoInheritedFieldCount + 15, paramNominaEspecial.usuario);
  }

  private static final void jdoSetusuario(NominaEspecial paramNominaEspecial, String paramString)
  {
    if (paramNominaEspecial.jdoFlags == 0)
    {
      paramNominaEspecial.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramNominaEspecial.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaEspecial.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramNominaEspecial, jdoInheritedFieldCount + 15, paramNominaEspecial.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}