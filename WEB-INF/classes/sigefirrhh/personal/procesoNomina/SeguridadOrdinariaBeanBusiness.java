package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;

public class SeguridadOrdinariaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSeguridadOrdinaria(SeguridadOrdinaria seguridadOrdinaria)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SeguridadOrdinaria seguridadOrdinariaNew = 
      (SeguridadOrdinaria)BeanUtils.cloneBean(
      seguridadOrdinaria);

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (seguridadOrdinariaNew.getGrupoNomina() != null) {
      seguridadOrdinariaNew.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        seguridadOrdinariaNew.getGrupoNomina().getIdGrupoNomina()));
    }
    pm.makePersistent(seguridadOrdinariaNew);
  }

  public void updateSeguridadOrdinaria(SeguridadOrdinaria seguridadOrdinaria) throws Exception
  {
    SeguridadOrdinaria seguridadOrdinariaModify = 
      findSeguridadOrdinariaById(seguridadOrdinaria.getIdSeguridadOrdinaria());

    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();

    if (seguridadOrdinaria.getGrupoNomina() != null) {
      seguridadOrdinaria.setGrupoNomina(
        grupoNominaBeanBusiness.findGrupoNominaById(
        seguridadOrdinaria.getGrupoNomina().getIdGrupoNomina()));
    }

    BeanUtils.copyProperties(seguridadOrdinariaModify, seguridadOrdinaria);
  }

  public void deleteSeguridadOrdinaria(SeguridadOrdinaria seguridadOrdinaria) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SeguridadOrdinaria seguridadOrdinariaDelete = 
      findSeguridadOrdinariaById(seguridadOrdinaria.getIdSeguridadOrdinaria());
    pm.deletePersistent(seguridadOrdinariaDelete);
  }

  public SeguridadOrdinaria findSeguridadOrdinariaById(long idSeguridadOrdinaria) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSeguridadOrdinaria == pIdSeguridadOrdinaria";
    Query query = pm.newQuery(SeguridadOrdinaria.class, filter);

    query.declareParameters("long pIdSeguridadOrdinaria");

    parameters.put("pIdSeguridadOrdinaria", new Long(idSeguridadOrdinaria));

    Collection colSeguridadOrdinaria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSeguridadOrdinaria.iterator();
    return (SeguridadOrdinaria)iterator.next();
  }

  public Collection findSeguridadOrdinariaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent seguridadOrdinariaExtent = pm.getExtent(
      SeguridadOrdinaria.class, true);
    Query query = pm.newQuery(seguridadOrdinariaExtent);
    query.setOrdering("anio ascending, mes ascending, fechaInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(SeguridadOrdinaria.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, mes ascending, fechaInicio ascending");

    Collection colSeguridadOrdinaria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadOrdinaria);

    return colSeguridadOrdinaria;
  }

  public Collection findByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoNomina.idGrupoNomina == pIdGrupoNomina";

    Query query = pm.newQuery(SeguridadOrdinaria.class, filter);

    query.declareParameters("long pIdGrupoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));

    query.setOrdering("anio ascending, mes ascending, fechaInicio ascending");

    Collection colSeguridadOrdinaria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadOrdinaria);

    return colSeguridadOrdinaria;
  }
}