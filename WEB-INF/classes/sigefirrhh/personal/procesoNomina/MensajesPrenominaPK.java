package sigefirrhh.personal.procesoNomina;

import java.io.Serializable;

public class MensajesPrenominaPK
  implements Serializable
{
  public long idMensajesPrenomina;

  public MensajesPrenominaPK()
  {
  }

  public MensajesPrenominaPK(long idMensajesPrenomina)
  {
    this.idMensajesPrenomina = idMensajesPrenomina;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((MensajesPrenominaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(MensajesPrenominaPK thatPK)
  {
    return 
      this.idMensajesPrenomina == thatPK.idMensajesPrenomina;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idMensajesPrenomina)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idMensajesPrenomina);
  }
}