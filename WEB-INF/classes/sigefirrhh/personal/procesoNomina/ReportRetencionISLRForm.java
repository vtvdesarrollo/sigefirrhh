package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.Disquete;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.login.LoginSession;

public class ReportRetencionISLRForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportRetencionISLRForm.class.getName());
  private boolean generado;
  private String urlArchivo;
  private String reportName;
  private int reportId;
  private String selectTipoPersonal;
  private String selectGrupoNomina;
  private String selectDisquete;
  private String selectRegion;
  private String selectNominaEspecial;
  private String tipoNomina = "0";
  private String inicio;
  private String fin;
  private Date fechaProceso = new Date();
  private Date fechaAbono;
  private Disquete disquete;
  private Calendar inicioAux;
  private Calendar finAux;
  private String numeroOrdenPago;
  private long idTipoPersonal;
  private long idGrupoNomina;
  private String semanaQuincena;
  private int anio;
  private int mes;
  private int numero;
  private String prestamo;
  private long idRegion;
  private long idNominaEspecial;
  private Collection listTipoPersonal;
  private Collection listGrupoNomina;
  private Collection listDisquete;
  private Collection listRegion;
  private Collection listNominaEspecial;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private ProcesoNominaNoGenFacade nominaEspecialFacade;
  private LoginSession login;
  private boolean show;
  private boolean showEspecial = false;
  private boolean showOrdinaria = false;
  private boolean auxShow;
  private TipoPersonal tipoPersonal;
  private GrupoNomina grupoNomina;
  private NominaEspecial nominaEspecial;
  private long idUnidadAdministradora;
  private Collection listUnidadAdministradora;
  private String ParaQueryPedro = "1";

  public ReportRetencionISLRForm()
  {
    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.nominaEspecialFacade = new ProcesoNominaNoGenFacade();
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.selectTipoPersonal = null;
    this.selectGrupoNomina = null;
    this.reportName = "retencionislr";
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void changeGrupoNomina(ValueChangeEvent event)
  {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.auxShow = false;
    try {
      if (this.idGrupoNomina != 0L)
        this.grupoNomina = this.definicionesFacade.findGrupoNominaById(this.idGrupoNomina);
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeNominaEspecial(ValueChangeEvent event)
  {
    this.idNominaEspecial = Long.valueOf(
      (String)event.getNewValue()).longValue();
  }

  public void changeTipoNomina(ValueChangeEvent event)
  {
    this.showEspecial = false;
    this.showOrdinaria = false;

    if ("1".equals((String)event.getNewValue())) {
      this.showOrdinaria = true;
    } else if ("2".equals((String)event.getNewValue())) {
      this.showEspecial = true;
      try {
        this.listNominaEspecial = 
          this.nominaEspecialFacade.findNominaEspecialByGrupoNominaAndEstatusAndMesAndAnio(
          this.idGrupoNomina, "P", this.mes, this.anio);
        log.error("ListNOminaEspecial: " + this.listNominaEspecial);

        Iterator iteratorPeriodo = this.listNominaEspecial.iterator();
        if (iteratorPeriodo.hasNext()) {
          NominaEspecial ne = (NominaEspecial)iteratorPeriodo.next();
          this.idNominaEspecial = ne.getIdNominaEspecial();
          log.error("Valor de idNominaEspecial :" + this.idNominaEspecial);
        }

      }
      catch (Exception e)
      {
        this.listNominaEspecial = new ArrayList();
        log.error("Excepcion Controlada (No se encuentra Nómina Especial:)", e);
      }
    }
  }

  public String generar()
    throws Exception
  {
    Map parameters = new Hashtable();
    String consulta;
    String consulta;
    if ("2".equals(this.tipoNomina))
    {
      consulta = "select hb.anio, hb.mes, gn.nombre as nombre_grupo, ne.descripcion as descripcion_nomina, 'N/A' as semana_quincena, p.numero_rif, sum(asignaciones)-sum(deducciones) as base, porcentaje from historicobaseislr  hb, trabajador t, personal p, gruponomina gn, nominaespecial ne where hb.id_trabajador = t.id_trabajador and t.id_personal = p.id_personal and hb.id_grupo_nomina = gn.id_grupo_nomina and hb.id_nomina_especial = ne.id_nomina_especial and hb.id_nomina_especial = " + 
        this.idNominaEspecial + 
        " group by hb.anio, hb.mes, gn.nombre, ne.descripcion, p.numero_rif, hb.porcentaje";
    }
    else
    {
      String clausulaEspecial = " ";
      String clausulaSemanaQuincena = " ";
      String semanaQuincena = "Todas";

      if ("1".equals(this.tipoNomina)) {
        clausulaEspecial = " and id_nomina_especial is null ";
        if ((this.semanaQuincena != null) && (!"".equals(this.semanaQuincena))) {
          clausulaSemanaQuincena = " and hb.semana_quincena = " + this.semanaQuincena;
          semanaQuincena = this.semanaQuincena;
        }

      }

      consulta = "select hb.anio, hb.mes, gn.nombre as nombre_grupo, 'N/A' as descripcion_nomina, '69' as semana_quincena, p.numero_rif, sum(asignaciones)-sum(deducciones) as base, porcentaje from historicobaseislr  hb, trabajador t, personal p, gruponomina gn where hb.id_trabajador = t.id_trabajador and t.id_personal = p.id_personal and hb.id_grupo_nomina = gn.id_grupo_nomina and hb.id_grupo_nomina = " + 
        this.idGrupoNomina + 
        " and anio = " + this.anio + 
        " and mes =  " + this.mes + 
        clausulaEspecial + 
        clausulaSemanaQuincena + 
        "group by hb.anio, hb.mes, gn.nombre, p.numero_rif, hb.porcentaje";
    }

    log.error("Consulta: " + consulta);
    try
    {
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("consulta", consulta);
      JasperForWeb report = new JasperForWeb();

      report.setType(3);
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/procesoNomina");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);
      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      log.error("PASO 3");
    }
    return null;
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalByConcepto(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador(), "5003");
      this.listDisquete = this.definicionesFacade.findDisqueteByTipoDisquete("A", this.login.getIdOrganismo());
      this.listRegion = this.estructuraFacade.findAllRegion();

      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaByOrganismo(this.login.getIdOrganismo());

      this.listUnidadAdministradora = this.estructuraFacade.findUnidadAdministradoraByOrganismo(this.login.getIdOrganismo());
      log.error("vacio (listUnidadAdministradora):" + this.listUnidadAdministradora.isEmpty());
    }
    catch (Exception e)
    {
      log.error("Excepción controlada: ", e);
      this.listTipoPersonal = new ArrayList();
      this.listGrupoNomina = new ArrayList();
      this.listNominaEspecial = new ArrayList();
      this.listUnidadAdministradora = new ArrayList();
    }
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListGrupoNomina()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listGrupoNomina, "sigefirrhh.base.definiciones.GrupoNomina");
  }
  public Collection getListDisquete() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listDisquete, "sigefirrhh.base.definiciones.Disquete");
  }
  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }

  public Collection getListNominaEspecial() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listNominaEspecial, "sigefirrhh.personal.procesoNomina.NominaEspecial");
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string)
  {
    this.selectTipoPersonal = string;
  }

  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String string) {
    this.selectGrupoNomina = string;
  }
  public String getFin() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }
  public void setFin(String string) {
    this.fin = string;
  }
  public void setInicio(String string) {
    this.inicio = string;
  }
  public boolean isShow() {
    return this.auxShow;
  }
  public boolean isShowEspecial() {
    return this.showEspecial;
  }
  public boolean isShowOrdinaria() {
    return this.showOrdinaria;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }

  public long getIdGrupoNomina() {
    return this.idGrupoNomina;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public void setIdTipoPersonal(long l) {
    this.idTipoPersonal = l;
  }
  public void setIdGrupoNomina(long l) {
    this.idGrupoNomina = l;
  }

  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }
  public String getSelectDisquete() {
    return this.selectDisquete;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }
  public String getSelectNominaEspecial() {
    return this.selectNominaEspecial;
  }

  public void setSelectDisquete(String string) {
    this.selectDisquete = string;
    try {
      this.disquete = this.definicionesFacade.findDisqueteById(Long.valueOf(this.selectDisquete).longValue());
    } catch (Exception localException) {
    }
  }

  public void setSelectRegion(String string) {
    this.selectRegion = string;
  }

  public void setSelectNominaEspecial(String string) {
    this.selectNominaEspecial = string;
  }

  public Date getFechaAbono() {
    return this.fechaAbono;
  }
  public Date getFechaProceso() {
    return this.fechaProceso;
  }
  public void setFechaAbono(Date date) {
    this.fechaAbono = date;
  }
  public void setFechaProceso(Date date) {
    this.fechaProceso = date;
  }
  public boolean isGenerado() {
    return this.generado;
  }
  public String getUrlArchivo() {
    return this.urlArchivo;
  }

  public String getNumeroOrdenPago()
  {
    return this.numeroOrdenPago;
  }
  public void setNumeroOrdenPago(String numeroOrdenPago) {
    this.numeroOrdenPago = numeroOrdenPago;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
  public String getPrestamo() {
    return this.prestamo;
  }
  public void setPrestamo(String prestamo) {
    this.prestamo = prestamo;
  }
  public int getNumero() {
    return this.numero;
  }
  public void setNumero(int numero) {
    this.numero = numero;
  }
  public long getIdUnidadAdministradora() {
    return this.idUnidadAdministradora;
  }

  public void setIdUnidadAdministradora(long l) {
    this.idUnidadAdministradora = l;
  }

  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }

  public void setSemanaQuincena(String valor) {
    this.semanaQuincena = valor;
  }

  public String getSemanaQuincena() {
    return this.semanaQuincena;
  }

  public void setTipoNomina(String valor) {
    this.tipoNomina = valor;
  }

  public String getTipoNomina() {
    return this.tipoNomina;
  }

  public void setReportId(int valor) {
    this.reportId = valor;
  }

  public int getReportId() {
    return this.reportId;
  }
}