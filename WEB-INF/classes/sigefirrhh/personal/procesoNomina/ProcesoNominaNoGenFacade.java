package sigefirrhh.personal.procesoNomina;

import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.jdo.PersistenceManager;
import sigefirrhh.base.definiciones.GrupoNomina;

public class ProcesoNominaNoGenFacade extends ProcesoNominaFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private ProcesoNominaNoGenBusiness procesoNominaNoGenBusiness = new ProcesoNominaNoGenBusiness();

  public Collection findFechaProximaNomina(long idGrupoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      Collection colPeriodo = 
        this.procesoNominaNoGenBusiness.findFechaProximaNomina(idGrupoNomina);
      PersistenceManager pm = PMThread.getPM();

      return colPeriodo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFechaUltimaNomina(long idGrupoNomina) throws Exception
  {
    try { this.txn.open();
      Collection colPeriodo = 
        this.procesoNominaNoGenBusiness.findFechaUltimaNomina(idGrupoNomina);
      PersistenceManager pm = PMThread.getPM();

      return colPeriodo;
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void generarSueldoPromedioMensual(long idOrganismo, long idGrupoNomina, String periodicidad, String recalculo, Date fechaInicio, Date fechaFin, Integer semanaMes, Boolean tieneSemana5, Integer numeroSemanasMes)
    throws Exception
  {
    this.procesoNominaNoGenBusiness.generarSueldoPromedioMensual(idOrganismo, 
      idGrupoNomina, 
      periodicidad, 
      recalculo, 
      fechaInicio, 
      fechaFin, 
      semanaMes, 
      tieneSemana5, 
      numeroSemanasMes);
  }

  public void generarSueldoPromedioSemanal(long idOrganismo, long idGrupoNomina, String periodicidad, String recalculo, Date fechaInicio, Date fechaFin, Integer semanaMes, Boolean tieneSemana5, Integer numeroSemanasMes)
    throws Exception
  {
    this.procesoNominaNoGenBusiness.generarSueldoPromedioSemanal(idOrganismo, 
      idGrupoNomina, 
      periodicidad, 
      recalculo, 
      fechaInicio, 
      fechaFin, 
      semanaMes, 
      tieneSemana5, 
      numeroSemanasMes);
  }

  public boolean generarPrenomina(long idGrupoNomina, String periodicidad, String recalculo, Date fechaInicio, Date fechaFin, long idOrganismo, Integer lunesPrQuincena, Integer lunesSeQuincena, Boolean tieneSemana5, Integer semanaMes, Integer numeroSemanasMes) throws Exception
  {
    try {
      this.txn.open();
      return this.procesoNominaNoGenBusiness.generarPrenomina(idGrupoNomina, periodicidad, recalculo, fechaInicio, fechaFin, idOrganismo, lunesPrQuincena, lunesSeQuincena, tieneSemana5, semanaMes, numeroSemanasMes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public boolean generarNomina(long idGrupoNomina, String periodicidad, String recalculo, Date fechaInicio, Date fechaFin, long idOrganismo, Integer lunesPrQuincena, Integer lunesSeQuincena, Boolean tieneSemana5, Integer semanaMes, String usuario, Integer semanaAnio, Integer numeroSemanasMes, Integer mesSemanal, Integer anioSemanal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaNoGenBusiness.generarNomina(idGrupoNomina, periodicidad, recalculo, fechaInicio, fechaFin, idOrganismo, lunesPrQuincena, lunesSeQuincena, tieneSemana5, semanaMes, usuario, semanaAnio, numeroSemanasMes, mesSemanal, anioSemanal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public boolean generarPrenominaEspecial(long idGrupoNomina, Date fechaInicio, Date fechaFin, long idOrganismo, long numeroNomina, long idFrecuenciaPago, long idNominaEspecial, String usuario, String estatus)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaNoGenBusiness.generarPrenominaEspecial(idGrupoNomina, fechaInicio, fechaFin, idOrganismo, numeroNomina, idFrecuenciaPago, idNominaEspecial, usuario, estatus);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public boolean generarNominaEspecial(long idGrupoNomina, Date fechaInicio, Date fechaFin, long idOrganismo, long numeroNomina, long idFrecuenciaPago, long idNominaEspecial, String usuario, String estatus, int anio, int mes) throws Exception
  {
    try {
      this.txn.open();
      return this.procesoNominaNoGenBusiness.generarNominaEspecial(idGrupoNomina, fechaInicio, fechaFin, idOrganismo, numeroNomina, idFrecuenciaPago, idNominaEspecial, usuario, estatus, anio, mes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public int findLastNumeroNominaEspecial() throws Exception
  {
    try {
      this.txn.open();
      return this.procesoNominaNoGenBusiness.findLastNumeroNominaEspecial();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public int findLastNumeroNominaEspecialByGrupoNominaAndAnio(long idGrupoNomina, int anio) throws Exception
  {
    try {
      this.txn.open();
      return this.procesoNominaNoGenBusiness.findLastNumeroNominaEspecialByGrupoNominaAndAnio(idGrupoNomina, anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNominaEspecialByGrupoNominaAndEstatus(long idGrupoNomina, String estatus) throws Exception
  {
    try {
      this.txn.open();
      return this.procesoNominaNoGenBusiness.findNominaEspecialByGrupoNominaAndEstatus(idGrupoNomina, estatus);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNominaEspecialByGrupoNominaAndEstatusAndAnio(long idGrupoNomina, String estatus, int anio) throws Exception
  {
    try {
      this.txn.open();
      return this.procesoNominaNoGenBusiness.findNominaEspecialByGrupoNominaAndEstatusAndAnio(idGrupoNomina, estatus, anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNominaEspecialByGrupoNominaAndEstatusAndMesAndAnio(long idGrupoNomina, String estatus, int mes, int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoNominaNoGenBusiness.findNominaEspecialByGrupoNominaAndEstatusAndMesAndAnio(idGrupoNomina, estatus, mes, anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public boolean reversarNomina(long idGrupoNomina, int numeroNomina, Date fechaInicio, String periodicidad, int anio, int mes, int semanaQuincena, boolean nominaEspecial)
    throws Exception
  {
    try
    {
      this.txn.open();
      return 
        this.procesoNominaNoGenBusiness.reversarNomina(
        idGrupoNomina, 
        numeroNomina, 
        fechaInicio, 
        periodicidad, 
        anio, 
        mes, 
        semanaQuincena, 
        nominaEspecial);
    }
    finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void calcularPromedios(long idTipoPersonal, String periodicidad)
    throws Exception
  {
    this.procesoNominaNoGenBusiness.calcularPromedios(idTipoPersonal, periodicidad);
  }

  public Collection findNominaEspecialByGrupoNominaAndEstatus(long idGrupoNomina, String estatus, String pagada) throws Exception {
    try {
      this.txn.open();
      return this.procesoNominaNoGenBusiness.findNominaEspecialByGrupoNominaAndEstatus(idGrupoNomina, estatus, pagada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadOrdinariaByGrupoNominaDesc(long idGrupoNomina) throws Exception
  {
    try {
      this.txn.open();
      return this.procesoNominaNoGenBusiness.findSeguridadOrdinariaByGrupoNominaDesc(idGrupoNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SeguridadOrdinaria findNominaMre(long idGrupoNomina) throws Exception
  {
    try { this.txn.open();
      return 
        this.procesoNominaNoGenBusiness.findNominaMre(idGrupoNomina);
    } finally
    {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void actualizarAportesPatronales(GrupoNomina grupoNomina, long anio, long mes) throws Exception {
    this.procesoNominaNoGenBusiness.actualizarAportesPatronales(grupoNomina, anio, mes);
  }

  public Collection getMensajesUltimaPrenomina(Long idNominaEspecial, Long idGrupoNomina) throws Exception
  {
    try
    {
      this.txn.open();

      return this.procesoNominaNoGenBusiness.getMensajesUltimaPrenomina(idNominaEspecial, idGrupoNomina);
    }
    finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection getMensajesUltimaPrenomina(Long idGrupoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();

      return this.procesoNominaNoGenBusiness.getMensajesUltimaPrenomina(idGrupoNomina);
    }
    finally {
      if (this.txn != null) this.txn.close();
    }
  }
}