package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesBusiness;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaPagoBeanBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;
import sigefirrhh.personal.trabajador.ConceptoFijoNoGenBeanBusiness;
import sigefirrhh.personal.trabajador.ConceptoVariableNoGenBeanBusiness;

public class GenerarPrenominaEspecialBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(GenerarPrenominaEspecialBeanBusiness.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DefinicionesBusiness definicionesBusiness = new DefinicionesBusiness();
  private ConceptoFijoNoGenBeanBusiness conceptoFijoNoGenBeanBusiness = new ConceptoFijoNoGenBeanBusiness();
  private ConceptoVariableNoGenBeanBusiness conceptoVariableNoGenBeanBusiness = new ConceptoVariableNoGenBeanBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public boolean generar(long idGrupoNomina, java.util.Date fechaInicio, java.util.Date fechaFin, long idOrganismo, long numeroNomina, long idFrecuenciaPago, long idNominaEspecial, String usuario, String estatus)
    throws Exception
  {
    int anio = fechaInicio.getYear() + 1900;
    int mes = fechaInicio.getMonth() + 1;

    java.sql.Date fechaSql = new java.sql.Date(new java.util.Date().getYear(), new java.util.Date().getMonth(), new java.util.Date().getDate());

    int semanaQuincena = 0;
    if (fechaInicio.getDate() == 1)
      semanaQuincena = 1;
    else {
      semanaQuincena = 2;
    }

    java.sql.Date fechaInicioSql = new java.sql.Date(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDate());
    java.sql.Date fechaFinSql = new java.sql.Date(fechaFin.getYear(), fechaFin.getMonth(), fechaFin.getDate());

    this.log.error("numero_nomina" + numeroNomina);
    this.log.error("id_frecuencia_pago" + idFrecuenciaPago);
    this.log.error("id_gregupo_nomina" + idGrupoNomina);

    boolean sobregirados = false;

    GrupoNomina grupoNomina = new GrupoNomina();
    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();
    grupoNomina = grupoNominaBeanBusiness.findGrupoNominaById(idGrupoNomina);

    String periodicidad = grupoNomina.getPeriodicidad();

    FrecuenciaPagoBeanBusiness frecuenciaPagoBeanBusiness = new FrecuenciaPagoBeanBusiness();

    FrecuenciaPago frecuenciaPago = frecuenciaPagoBeanBusiness.findFrecuenciaPagoById(idFrecuenciaPago);

    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      StringBuffer sql = new StringBuffer();
      sql.append("select generar_nomina(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setBoolean(1, true);
      st.setBoolean(2, false);
      st.setLong(3, idGrupoNomina);
      st.setString(4, periodicidad);
      st.setString(5, "N");
      st.setDate(6, fechaInicioSql);
      st.setDate(7, fechaFinSql);
      st.setLong(8, idOrganismo);
      st.setInt(9, 0);
      st.setInt(10, 0);
      st.setBoolean(11, false);
      st.setInt(12, 0);
      st.setString(13, usuario);
      st.setInt(14, 0);
      st.setInt(15, 0);
      st.setBoolean(16, false);
      st.setInt(17, semanaQuincena);
      st.setLong(18, numeroNomina);
      st.setDate(19, fechaSql);
      st.setInt(20, mes);
      st.setInt(21, anio);
      st.setDate(22, null);
      st.setDate(23, null);
      st.setDate(24, null);
      st.setDate(25, null);
      st.setLong(26, idNominaEspecial);
      st.setInt(27, frecuenciaPago.getCodFrecuenciaPago());
      st.setString(28, estatus);
      st.setString(29, "N");
      int valor = 0;
      rs = st.executeQuery();
      rs.next();
      valor = rs.getInt(1);
      connection.commit();

      this.log.error("ejecutó la prenomina especial");

      if (valor == 1) {
        this.log.error("HAY SOBREGIRADOS");
        return true;
      }
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5) {  }
 
    }
    return false;
  }

  public Collection getMensajesUltimaPrenomina(Long idNominaEspecial, Long idGrupoNomina)
    throws Exception
  {
    this.log.debug("getMensajesUltimaPrenomina()");
    PersistenceManager pm = PMThread.getPM();

    String filter = "nominaEspecial.idNominaEspecial == pIdNominaEspecial && grupoNomina.idGrupoNomina == pIdGrupoNomina";

    Query query = pm.newQuery(MensajesPrenomina.class, filter);

    query.declareParameters("long pIdNominaEspecial, long pIdGrupoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdNominaEspecial", idNominaEspecial);
    parameters.put("pIdGrupoNomina", idGrupoNomina);

    Collection colMensajesUltimaPrenomina = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    this.log.debug("query:--------------------------" + query);
    this.log.debug("idNominaEspecial:--------------------------" + idNominaEspecial);
    this.log.debug("idGrupoNomina:--------------------------" + idGrupoNomina);
    this.log.debug("colMensajesUltimaPrenomina:--------------------------" + colMensajesUltimaPrenomina);

    pm.makeTransientAll(colMensajesUltimaPrenomina);

    return colMensajesUltimaPrenomina;
  }
}