package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class ProcesoNominaBusiness extends AbstractBusiness
  implements Serializable
{
  private FrecuenciaPagarBeanBusiness frecuenciaPagarBeanBusiness = new FrecuenciaPagarBeanBusiness();

  private NominaEspecialBeanBusiness nominaEspecialBeanBusiness = new NominaEspecialBeanBusiness();

  private SeguridadEspecialBeanBusiness seguridadEspecialBeanBusiness = new SeguridadEspecialBeanBusiness();

  private SeguridadOrdinariaBeanBusiness seguridadOrdinariaBeanBusiness = new SeguridadOrdinariaBeanBusiness();

  private UltimaPrenominaBeanBusiness ultimaPrenominaBeanBusiness = new UltimaPrenominaBeanBusiness();

  public void addFrecuenciaPagar(FrecuenciaPagar frecuenciaPagar)
    throws Exception
  {
    this.frecuenciaPagarBeanBusiness.addFrecuenciaPagar(frecuenciaPagar);
  }

  public void updateFrecuenciaPagar(FrecuenciaPagar frecuenciaPagar) throws Exception {
    this.frecuenciaPagarBeanBusiness.updateFrecuenciaPagar(frecuenciaPagar);
  }

  public void deleteFrecuenciaPagar(FrecuenciaPagar frecuenciaPagar) throws Exception {
    this.frecuenciaPagarBeanBusiness.deleteFrecuenciaPagar(frecuenciaPagar);
  }

  public FrecuenciaPagar findFrecuenciaPagarById(long frecuenciaPagarId) throws Exception {
    return this.frecuenciaPagarBeanBusiness.findFrecuenciaPagarById(frecuenciaPagarId);
  }

  public Collection findAllFrecuenciaPagar() throws Exception {
    return this.frecuenciaPagarBeanBusiness.findFrecuenciaPagarAll();
  }

  public Collection findFrecuenciaPagarByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    return this.frecuenciaPagarBeanBusiness.findByGrupoNomina(idGrupoNomina);
  }

  public void addNominaEspecial(NominaEspecial nominaEspecial)
    throws Exception
  {
    this.nominaEspecialBeanBusiness.addNominaEspecial(nominaEspecial);
  }

  public void updateNominaEspecial(NominaEspecial nominaEspecial) throws Exception {
    this.nominaEspecialBeanBusiness.updateNominaEspecial(nominaEspecial);
  }

  public void deleteNominaEspecial(NominaEspecial nominaEspecial) throws Exception {
    this.nominaEspecialBeanBusiness.deleteNominaEspecial(nominaEspecial);
  }

  public NominaEspecial findNominaEspecialById(long nominaEspecialId) throws Exception {
    return this.nominaEspecialBeanBusiness.findNominaEspecialById(nominaEspecialId);
  }

  public Collection findAllNominaEspecial() throws Exception {
    return this.nominaEspecialBeanBusiness.findNominaEspecialAll();
  }

  public Collection findNominaEspecialByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    return this.nominaEspecialBeanBusiness.findByGrupoNomina(idGrupoNomina);
  }

  public Collection findNominaEspecialByAnio(int anio)
    throws Exception
  {
    return this.nominaEspecialBeanBusiness.findByAnio(anio);
  }

  public Collection findNominaEspecialByEstatus(String estatus)
    throws Exception
  {
    return this.nominaEspecialBeanBusiness.findByEstatus(estatus);
  }

  public Collection findNominaEspecialByFrecuenciaPago(long idFrecuenciaPago)
    throws Exception
  {
    return this.nominaEspecialBeanBusiness.findByFrecuenciaPago(idFrecuenciaPago);
  }

  public void addSeguridadEspecial(SeguridadEspecial seguridadEspecial)
    throws Exception
  {
    this.seguridadEspecialBeanBusiness.addSeguridadEspecial(seguridadEspecial);
  }

  public void updateSeguridadEspecial(SeguridadEspecial seguridadEspecial) throws Exception {
    this.seguridadEspecialBeanBusiness.updateSeguridadEspecial(seguridadEspecial);
  }

  public void deleteSeguridadEspecial(SeguridadEspecial seguridadEspecial) throws Exception {
    this.seguridadEspecialBeanBusiness.deleteSeguridadEspecial(seguridadEspecial);
  }

  public SeguridadEspecial findSeguridadEspecialById(long seguridadEspecialId) throws Exception {
    return this.seguridadEspecialBeanBusiness.findSeguridadEspecialById(seguridadEspecialId);
  }

  public Collection findAllSeguridadEspecial() throws Exception {
    return this.seguridadEspecialBeanBusiness.findSeguridadEspecialAll();
  }

  public Collection findSeguridadEspecialByAnio(int anio)
    throws Exception
  {
    return this.seguridadEspecialBeanBusiness.findByAnio(anio);
  }

  public Collection findSeguridadEspecialByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    return this.seguridadEspecialBeanBusiness.findByGrupoNomina(idGrupoNomina);
  }

  public void addSeguridadOrdinaria(SeguridadOrdinaria seguridadOrdinaria)
    throws Exception
  {
    this.seguridadOrdinariaBeanBusiness.addSeguridadOrdinaria(seguridadOrdinaria);
  }

  public void updateSeguridadOrdinaria(SeguridadOrdinaria seguridadOrdinaria) throws Exception {
    this.seguridadOrdinariaBeanBusiness.updateSeguridadOrdinaria(seguridadOrdinaria);
  }

  public void deleteSeguridadOrdinaria(SeguridadOrdinaria seguridadOrdinaria) throws Exception {
    this.seguridadOrdinariaBeanBusiness.deleteSeguridadOrdinaria(seguridadOrdinaria);
  }

  public SeguridadOrdinaria findSeguridadOrdinariaById(long seguridadOrdinariaId) throws Exception {
    return this.seguridadOrdinariaBeanBusiness.findSeguridadOrdinariaById(seguridadOrdinariaId);
  }

  public Collection findAllSeguridadOrdinaria() throws Exception {
    return this.seguridadOrdinariaBeanBusiness.findSeguridadOrdinariaAll();
  }

  public Collection findSeguridadOrdinariaByAnio(int anio)
    throws Exception
  {
    return this.seguridadOrdinariaBeanBusiness.findByAnio(anio);
  }

  public Collection findSeguridadOrdinariaByGrupoNomina(long idGrupoNomina)
    throws Exception
  {
    return this.seguridadOrdinariaBeanBusiness.findByGrupoNomina(idGrupoNomina);
  }

  public void addUltimaPrenomina(UltimaPrenomina ultimaPrenomina)
    throws Exception
  {
    this.ultimaPrenominaBeanBusiness.addUltimaPrenomina(ultimaPrenomina);
  }

  public void updateUltimaPrenomina(UltimaPrenomina ultimaPrenomina) throws Exception {
    this.ultimaPrenominaBeanBusiness.updateUltimaPrenomina(ultimaPrenomina);
  }

  public void deleteUltimaPrenomina(UltimaPrenomina ultimaPrenomina) throws Exception {
    this.ultimaPrenominaBeanBusiness.deleteUltimaPrenomina(ultimaPrenomina);
  }

  public UltimaPrenomina findUltimaPrenominaById(long ultimaPrenominaId) throws Exception {
    return this.ultimaPrenominaBeanBusiness.findUltimaPrenominaById(ultimaPrenominaId);
  }

  public Collection findAllUltimaPrenomina() throws Exception {
    return this.ultimaPrenominaBeanBusiness.findUltimaPrenominaAll();
  }
}