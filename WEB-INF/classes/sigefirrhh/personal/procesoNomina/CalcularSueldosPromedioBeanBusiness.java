package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.tools.NumberTools;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.ParametroGobierno;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.personal.conceptos.CalcularConceptoBeanBusiness;
import sigefirrhh.personal.docentes.ConceptoDocente;
import sigefirrhh.personal.trabajador.Trabajador;

public class CalcularSueldosPromedioBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(CalcularSueldosPromedioBeanBusiness.class.getName());

  public Collection calcularConceptoDocente(Collection colConceptoDocente, long idCargo, double horasSemanales)
  {
    Statement stActualizarConceptosCalculados = null;
    Connection connection = Resource.getConnection();

    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

    Collection colConceptoCalculado = new ArrayList();
    try
    {
      connection.setAutoCommit(true);

      stActualizarConceptosCalculados = connection.createStatement();

      Iterator iter = colConceptoDocente.iterator();

      while (iter.hasNext()) {
        double montoCalculado = 0.0D;
        ConceptoDocente conceptoDocente = (ConceptoDocente)iter.next();
        if ((conceptoDocente.getConceptoTipoPersonal().getRecalculo().equals("S")) && 
          (conceptoDocente.getConceptoTipoPersonal().getConcepto().getSueldoBasico().equals("N"))) {
          StringBuffer sql = new StringBuffer();

          montoCalculado = calcularConceptoBeanBusiness.calcularDocente(conceptoDocente.getConceptoTipoPersonal().getIdConceptoTipoPersonal(), conceptoDocente.getIdMovimiento(), conceptoDocente.getUnidades(), conceptoDocente.getConceptoTipoPersonal().getTipo(), conceptoDocente.getFrecuenciaTipoPersonal().getCodFrecuenciaPago(), idCargo, conceptoDocente.getConceptoTipoPersonal().getValor(), conceptoDocente.getConceptoTipoPersonal().getTopeMinimo(), conceptoDocente.getConceptoTipoPersonal().getTopeMaximo(), horasSemanales);

          sql.append("update conceptodocente set monto = " + montoCalculado + " where id_concepto_docente = " + conceptoDocente.getIdConceptoDocente());

          conceptoDocente.setMonto(montoCalculado);
          stActualizarConceptosCalculados.execute(sql.toString());
        }
        colConceptoCalculado.add(conceptoDocente);
      }
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }

    return colConceptoCalculado;
  }

  public void calcularUnTrabajador(Trabajador trabajador) {
    Connection connection = null;
    ResultSet rsConceptosCalculados = null;
    PreparedStatement stConceptosCalculados = null;
    Statement stActualizarConceptosCalculados = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    ResultSet rsPromedios = null;
    PreparedStatement stPromedios = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();
      Statement stSueldoPromedio = null;

      long idGrupoNomina = trabajador.getTipoPersonal().getGrupoNomina().getIdGrupoNomina();
      String periodicidad = trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad();

      StringBuffer sql = new StringBuffer();
      sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, fp.cod_frecuencia_pago, cf.unidades, ctp.tipo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
      sql.append("  concepto c, frecuenciapago fp");
      sql.append(" where");
      sql.append(" cf.id_trabajador = ? ");
      sql.append("  and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and c.sueldo_basico = 'N'");
      sql.append(" and ctp.recalculo = 'S'");
      sql.append(" and ctp.cod_concepto not in('5001', '5003', '5004')");
      sql.append(" order by c.cod_concepto");

      stActualizarConceptosCalculados = connection.createStatement();
      stSueldoPromedio = connection.createStatement();

      stConceptosCalculados = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptosCalculados.setLong(1, trabajador.getIdTrabajador());
      rsConceptosCalculados = stConceptosCalculados.executeQuery();

      while (rsConceptosCalculados.next()) {
        double montoCalculado = 0.0D;
        sql = new StringBuffer();

        montoCalculado = calcularConceptoBeanBusiness.calcular(rsConceptosCalculados.getLong("id_concepto_tipo_personal"), trabajador.getIdTrabajador(), rsConceptosCalculados.getDouble("unidades"), rsConceptosCalculados.getString("tipo"), rsConceptosCalculados.getInt("cod_frecuencia_pago"), trabajador.getTurno().getJornadaDiaria(), trabajador.getTurno().getJornadaSemanal(), trabajador.getTipoPersonal().getFormulaIntegral(), trabajador.getTipoPersonal().getFormulaSemanal(), trabajador.getCargo().getIdCargo(), rsConceptosCalculados.getDouble("valor"), rsConceptosCalculados.getDouble("tope_minimo"), rsConceptosCalculados.getDouble("tope_maximo"));

        sql = new StringBuffer();
        sql.append("select update_monto_concepto_fijo(?, ?)");

        st = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        st.setLong(1, rsConceptosCalculados.getLong("id_concepto_fijo"));
        st.setDouble(2, montoCalculado);

        rs = st.executeQuery();
      }

      double montoBase1Si = 0.0D;
      double montoBase2Si = 0.0D;
      double montoBaseTotalSi = 0.0D;

      double montoBase1Sb = 0.0D;
      double montoBase2Sb = 0.0D;
      double montoBaseTotalSb = 0.0D;

      double montoBase1Co = 0.0D;
      double montoBase2Co = 0.0D;
      double montoBaseTotalCo = 0.0D;

      double montoBase1Pt = 0.0D;
      double montoBase2Pt = 0.0D;
      double montoBaseTotalPt = 0.0D;

      double montoBase1Pc = 0.0D;
      double montoBase2Pc = 0.0D;
      double montoBaseTotalPc = 0.0D;

      double montoBase1As = 0.0D;
      double montoBase2As = 0.0D;
      double montoBaseTotalAs = 0.0D;

      sql = new StringBuffer();
      sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, fp.cod_frecuencia_pago, cf.unidades, cf.monto, ");
      sql.append(" c.sueldo_basico, c.sueldo_integral, c.compensacion, c.primas_trabajador, c.primas_cargo, c.ajuste_sueldo");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
      sql.append("  concepto c, frecuenciapago fp");
      sql.append(" where");
      sql.append("  cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and ctp.cod_concepto < '5000'");
      sql.append(" and cf.id_trabajador = ? ");
      sql.append(" and (c.sueldo_basico = 'S' ");
      sql.append(" or c.sueldo_integral = 'S' ");
      sql.append(" or c.compensacion = 'S' ");
      sql.append(" or c.primas_trabajador = 'S' ");
      sql.append(" or c.primas_cargo = 'S' ");
      sql.append(" or c.ajuste_sueldo= 'S') ");

      stPromedios = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stPromedios.setLong(1, trabajador.getIdTrabajador());
      rsPromedios = stPromedios.executeQuery();

      while (rsPromedios.next()) {
        if (!periodicidad.equals("S")) {
          if (rsPromedios.getString("sueldo_integral").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1Si += rsPromedios.getDouble("monto") * 2.0D;
            else {
              montoBase2Si += rsPromedios.getDouble("monto");
            }
          }
          if (rsPromedios.getString("sueldo_basico").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1Sb += rsPromedios.getDouble("monto") * 2.0D;
            else
              montoBase2Sb += rsPromedios.getDouble("monto");
          }
          else if (rsPromedios.getString("compensacion").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1Co += rsPromedios.getDouble("monto") * 2.0D;
            else
              montoBase2Co += rsPromedios.getDouble("monto");
          }
          else if (rsPromedios.getString("primas_trabajador").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1Pt += rsPromedios.getDouble("monto") * 2.0D;
            else
              montoBase2Pt += rsPromedios.getDouble("monto");
          }
          else if (rsPromedios.getString("primas_cargo").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1Pc += rsPromedios.getDouble("monto") * 2.0D;
            else
              montoBase2Pc += rsPromedios.getDouble("monto");
          }
          else if (rsPromedios.getString("ajuste_sueldo").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1As += rsPromedios.getDouble("monto") * 2.0D;
            else {
              montoBase2As += rsPromedios.getDouble("monto");
            }
          }

        }
        else if (rsPromedios.getInt("cod_frecuencia_pago") == 4) {
          if (rsPromedios.getString("sueldo_integral").equals("S")) {
            montoBase1Si += rsPromedios.getDouble("monto");
          }
          if (rsPromedios.getString("sueldo_basico").equals("S"))
            montoBase1Sb += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("compensacion").equals("S"))
            montoBase1Co += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_trabajador").equals("S"))
            montoBase1Pt += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_cargo").equals("S"))
            montoBase1Pc += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("ajuste_sueldo").equals("S"))
            montoBase1As += rsPromedios.getDouble("monto");
        }
        else if (rsPromedios.getInt("cod_frecuencia_pago") != 10) {
          if (rsPromedios.getString("sueldo_integral").equals("S")) {
            montoBase2Si += rsPromedios.getDouble("monto");
          }
          if (rsPromedios.getString("sueldo_basico").equals("S"))
            montoBase2Sb += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("compensacion").equals("S"))
            montoBase2Co += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_trabajador").equals("S"))
            montoBase2Pt += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_cargo").equals("S"))
            montoBase2Pc += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("ajuste_sueldo").equals("S"))
            montoBase2As += rsPromedios.getDouble("monto");
        }
        else
        {
          if (rsPromedios.getString("sueldo_integral").equals("S")) {
            montoBase2Si += rsPromedios.getDouble("monto") * 4.0D;
          }
          if (rsPromedios.getString("sueldo_basico").equals("S"))
            montoBase2Sb += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("compensacion").equals("S"))
            montoBase2Co += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("primas_trabajador").equals("S"))
            montoBase2Pt += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("primas_cargo").equals("S"))
            montoBase2Pc += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("ajuste_sueldo").equals("S")) {
            montoBase2As += rsPromedios.getDouble("monto") * 4.0D;
          }

        }

      }

      if (periodicidad.equals("S")) {
        if (trabajador.getTipoPersonal().getFormulaIntegral().equals("1")) {
          montoBaseTotalSi = (montoBase1Si / 7.0D + montoBase2Si / 30.0D) * 365.0D / 12.0D;
          montoBaseTotalSb = (montoBase1Sb / 7.0D + montoBase2Sb / 30.0D) * 365.0D / 12.0D;
          montoBaseTotalCo = (montoBase1Co / 7.0D + montoBase2Co / 30.0D) * 365.0D / 12.0D;
          montoBaseTotalPt = (montoBase1Pt / 7.0D + montoBase2Pt / 30.0D) * 365.0D / 12.0D;
          montoBaseTotalPc = (montoBase1Pc / 7.0D + montoBase2Pc / 30.0D) * 365.0D / 12.0D;
          montoBaseTotalAs = (montoBase1As / 7.0D + montoBase2As / 30.0D) * 365.0D / 12.0D;
        } else if (trabajador.getTipoPersonal().getFormulaIntegral().equals("2")) {
          montoBaseTotalSi = montoBase1Si / 7.0D * 30.0D + montoBase2Si;
          montoBaseTotalSb = montoBase1Sb / 7.0D * 30.0D + montoBase2Sb;
          montoBaseTotalCo = montoBase1Co / 7.0D * 30.0D + montoBase2Co;
          montoBaseTotalPt = montoBase1Pt / 7.0D * 30.0D + montoBase2Pt;
          montoBaseTotalPc = montoBase1Pc / 7.0D * 30.0D + montoBase2Pc;
          montoBaseTotalAs = montoBase1As / 7.0D * 30.0D + montoBase2As;
        } else if (trabajador.getTipoPersonal().getFormulaIntegral().equals("3")) {
          montoBaseTotalSi = montoBase1Si * 52.0D / 12.0D + montoBase2Si;
          montoBaseTotalSb = montoBase1Sb * 52.0D / 12.0D + montoBase2Sb;
          montoBaseTotalCo = montoBase1Co * 52.0D / 12.0D + montoBase2Co;
          montoBaseTotalPt = montoBase1Pt * 52.0D / 12.0D + montoBase2Pt;
          montoBaseTotalPc = montoBase1Pc * 52.0D / 12.0D + montoBase2Pc;
          montoBaseTotalAs = montoBase1As * 52.0D / 12.0D + montoBase2As;
        }
      } else {
        montoBaseTotalSi = montoBase1Si + montoBase2Si;
        montoBaseTotalSb = montoBase1Sb + montoBase2Sb;
        montoBaseTotalCo = montoBase1Co + montoBase2Co;
        montoBaseTotalPt = montoBase1Pt + montoBase2Pt;
        montoBaseTotalPc = montoBase1Pc + montoBase2Pc;
        montoBaseTotalAs = montoBase1As + montoBase2As;
      }

      sql = new StringBuffer();
      sql.append("UPDATE sueldoPromedio SET ");
      sql.append("promedio_integral = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalSi));
      sql.append(",promedio_sueldo = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalSb));
      sql.append(",promedio_compensacion = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalCo));
      sql.append(",promedio_primast = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalPt));
      sql.append(",promedio_primasc = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalPc));
      sql.append(",promedio_ajustes = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalAs));
      sql.append(" WHERE id_trabajador = ");
      sql.append(trabajador.getIdTrabajador());

      stSueldoPromedio.addBatch(sql.toString());
      stSueldoPromedio.executeBatch();
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
    finally {
      if (rsConceptosCalculados != null) try {
          rsConceptosCalculados.close();
        } catch (Exception localException1) {
        } if (rsPromedios != null) try {
          rsPromedios.close();
        } catch (Exception localException2) {
        } if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (stConceptosCalculados != null) try {
          stConceptosCalculados.close();
        } catch (Exception localException4) {
        } if (stPromedios != null) try {
          stPromedios.close();
        } catch (Exception localException5) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException6) {
        } if (stActualizarConceptosCalculados != null) try {
          stActualizarConceptosCalculados.close();
        } catch (Exception localException7) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException8)
        {
        } 
    }
  }

  public void calcularUnTrabajadorMensual(Trabajador trabajador) { ResultSet rsConceptosCalculados = null;
    PreparedStatement stConceptosCalculados = null;
    Statement stActualizarConceptosCalculados = null;
    Connection connection = null;
    try
    {
      connection = Resource.getConnection();
      CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();
      Statement stSueldoPromedio = null;
      long idGrupoNomina = trabajador.getTipoPersonal().getGrupoNomina().getIdGrupoNomina();
      String periodicidad = trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad();

      StringBuffer sql = new StringBuffer();
      sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, fp.cod_frecuencia_pago, cf.unidades, ctp.tipo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
      sql.append("  concepto c, frecuenciapago fp");
      sql.append(" where");
      sql.append(" cf.id_trabajador = ? ");
      sql.append("  and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and c.sueldo_basico = 'N'");
      sql.append(" and ctp.recalculo = 'S'");
      sql.append(" and ctp.cod_concepto not in('5001', '5003', '5004')");

      stActualizarConceptosCalculados = connection.createStatement();
      stSueldoPromedio = connection.createStatement();

      stConceptosCalculados = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptosCalculados.setLong(1, trabajador.getIdTrabajador());
      rsConceptosCalculados = stConceptosCalculados.executeQuery();

      while (rsConceptosCalculados.next()) {
        double montoCalculado = 0.0D;
        sql = new StringBuffer();
        montoCalculado = calcularConceptoBeanBusiness.calcular(rsConceptosCalculados.getLong("id_concepto_tipo_personal"), trabajador.getIdTrabajador(), rsConceptosCalculados.getDouble("unidades"), rsConceptosCalculados.getString("tipo"), rsConceptosCalculados.getInt("cod_frecuencia_pago"), trabajador.getTurno().getJornadaDiaria(), trabajador.getTurno().getJornadaSemanal(), trabajador.getTipoPersonal().getFormulaIntegral(), trabajador.getTipoPersonal().getFormulaSemanal(), trabajador.getCargo().getIdCargo(), rsConceptosCalculados.getDouble("valor"), rsConceptosCalculados.getDouble("tope_minimo"), rsConceptosCalculados.getDouble("tope_maximo"));

        sql.append("update conceptofijo set monto = " + montoCalculado + " where id_concepto_fijo = " + rsConceptosCalculados.getLong("id_concepto_fijo"));

        stActualizarConceptosCalculados.addBatch(sql.toString());
      }

      stActualizarConceptosCalculados.executeBatch();

      ResultSet rsPromedios = null;
      PreparedStatement stPromedios = null;
      double montoBase1Si = 0.0D;
      double montoBase2Si = 0.0D;
      double montoBaseTotalSi = 0.0D;

      double montoBase1Sb = 0.0D;
      double montoBase2Sb = 0.0D;
      double montoBaseTotalSb = 0.0D;

      double montoBase1Co = 0.0D;
      double montoBase2Co = 0.0D;
      double montoBaseTotalCo = 0.0D;

      double montoBase1Pt = 0.0D;
      double montoBase2Pt = 0.0D;
      double montoBaseTotalPt = 0.0D;

      double montoBase1Pc = 0.0D;
      double montoBase2Pc = 0.0D;
      double montoBaseTotalPc = 0.0D;

      double montoBase1As = 0.0D;
      double montoBase2As = 0.0D;
      double montoBaseTotalAs = 0.0D;

      sql = new StringBuffer();
      sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, fp.cod_frecuencia_pago, cf.unidades, cf.monto, ");
      sql.append(" c.sueldo_basico, c.sueldo_integral, c.compensacion, c.primas_trabajador, c.primas_cargo, c.ajuste_sueldo");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
      sql.append("  concepto c, frecuenciapago fp");
      sql.append(" where");
      sql.append("  cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and ctp.cod_concepto < '5000'");
      sql.append(" and cf.id_trabajador = ? ");
      sql.append(" and (c.sueldo_basico = 'S' ");
      sql.append(" or c.sueldo_integral = 'S' ");
      sql.append(" or c.compensacion = 'S' ");
      sql.append(" or c.primas_trabajador = 'S' ");
      sql.append(" or c.primas_cargo = 'S' ");
      sql.append(" or c.ajuste_sueldo= 'S') ");

      stPromedios = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stPromedios.setLong(1, trabajador.getIdTrabajador());
      rsPromedios = stPromedios.executeQuery();

      while (rsPromedios.next()) {
        if (rsPromedios.getString("sueldo_integral").equals("S")) {
          if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
            montoBase1Si += rsPromedios.getDouble("monto") * 2.0D;
          else {
            montoBase2Si += rsPromedios.getDouble("monto");
          }
        }
        if (rsPromedios.getString("sueldo_basico").equals("S")) {
          if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
            montoBase1Sb += rsPromedios.getDouble("monto") * 2.0D;
          else
            montoBase2Sb += rsPromedios.getDouble("monto");
        }
        else if (rsPromedios.getString("compensacion").equals("S")) {
          if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
            montoBase1Co += rsPromedios.getDouble("monto") * 2.0D;
          else
            montoBase2Co += rsPromedios.getDouble("monto");
        }
        else if (rsPromedios.getString("primas_trabajador").equals("S")) {
          if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
            montoBase1Pt += rsPromedios.getDouble("monto") * 2.0D;
          else
            montoBase2Pt += rsPromedios.getDouble("monto");
        }
        else if (rsPromedios.getString("primas_cargo").equals("S")) {
          if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
            montoBase1Pc += rsPromedios.getDouble("monto") * 2.0D;
          else
            montoBase2Pc += rsPromedios.getDouble("monto");
        }
        else if (rsPromedios.getString("ajuste_sueldo").equals("S")) {
          if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
            montoBase1As += rsPromedios.getDouble("monto") * 2.0D;
          else {
            montoBase2As += rsPromedios.getDouble("monto");
          }
        }

        montoBaseTotalSi = montoBase1Si + montoBase2Si;
        montoBaseTotalSb = montoBase1Sb + montoBase2Sb;
        montoBaseTotalCo = montoBase1Co + montoBase2Co;
        montoBaseTotalPt = montoBase1Pt + montoBase2Pt;
        montoBaseTotalPc = montoBase1Pc + montoBase2Pc;
        montoBaseTotalAs = montoBase1As + montoBase2As;

        sql = new StringBuffer();
        sql.append("UPDATE sueldoPromedio SET ");
        sql.append("promedio_integral = ");
        sql.append(NumberTools.twoDecimal(montoBaseTotalSi));
        sql.append(",promedio_sueldo = ");
        sql.append(NumberTools.twoDecimal(montoBaseTotalSb));
        sql.append(",promedio_compensacion = ");
        sql.append(NumberTools.twoDecimal(montoBaseTotalCo));
        sql.append(",promedio_primast = ");
        sql.append(NumberTools.twoDecimal(montoBaseTotalPt));
        sql.append(",promedio_primasc = ");
        sql.append(NumberTools.twoDecimal(montoBaseTotalPc));
        sql.append(",promedio_ajustes = ");
        sql.append(NumberTools.twoDecimal(montoBaseTotalAs));
        sql.append(" WHERE id_trabajador = ");
        sql.append(trabajador.getIdTrabajador());

        stSueldoPromedio.addBatch(sql.toString());
        stSueldoPromedio.executeBatch();
      }
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
    finally {
      if (rsConceptosCalculados != null) try {
          rsConceptosCalculados.close();
        } catch (Exception localException1) {
        } if (stConceptosCalculados != null) try {
          stConceptosCalculados.close();
        } catch (Exception localException2) {
        } if (stActualizarConceptosCalculados != null) try {
          stActualizarConceptosCalculados.close();
        } catch (Exception localException3) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException4)
        {
        } 
    } } 
  public void calcularUnTrabajadorSemanal(Trabajador trabajador)
  {
    ResultSet rsConceptosCalculados = null;
    PreparedStatement stConceptosCalculados = null;
    Statement stActualizarConceptosCalculados = null;

    ResultSet rsPromedios = null;
    PreparedStatement stPromedios = null;

    Connection connection = null;
    try
    {
      connection = Resource.getConnection();

      CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();
      Statement stSueldoPromedio = null;
      long idGrupoNomina = trabajador.getTipoPersonal().getGrupoNomina().getIdGrupoNomina();
      String periodicidad = trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad();

      StringBuffer sql = new StringBuffer();
      sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, fp.cod_frecuencia_pago, cf.unidades, ctp.tipo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
      sql.append("  concepto c, frecuenciapago fp");
      sql.append(" where");
      sql.append(" cf.id_trabajador = ? ");
      sql.append("  and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and c.sueldo_basico = 'N'");
      sql.append(" and ctp.recalculo = 'S'");
      sql.append(" and ctp.cod_concepto not in('5001', '5003', '5004')");

      stActualizarConceptosCalculados = connection.createStatement();
      stSueldoPromedio = connection.createStatement();

      stConceptosCalculados = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptosCalculados.setLong(1, trabajador.getIdTrabajador());
      rsConceptosCalculados = stConceptosCalculados.executeQuery();

      while (rsConceptosCalculados.next()) {
        double montoCalculado = 0.0D;
        sql = new StringBuffer();
        montoCalculado = calcularConceptoBeanBusiness.calcular(rsConceptosCalculados.getLong("id_concepto_tipo_personal"), trabajador.getIdTrabajador(), rsConceptosCalculados.getDouble("unidades"), rsConceptosCalculados.getString("tipo"), rsConceptosCalculados.getInt("cod_frecuencia_pago"), trabajador.getTurno().getJornadaDiaria(), trabajador.getTurno().getJornadaSemanal(), trabajador.getTipoPersonal().getFormulaIntegral(), trabajador.getTipoPersonal().getFormulaSemanal(), trabajador.getCargo().getIdCargo(), rsConceptosCalculados.getDouble("valor"), rsConceptosCalculados.getDouble("tope_minimo"), rsConceptosCalculados.getDouble("tope_maximo"));
        sql.append("update conceptofijo set monto = " + montoCalculado + " where id_concepto_fijo = " + rsConceptosCalculados.getLong("id_concepto_fijo"));
        stActualizarConceptosCalculados.addBatch(sql.toString());
      }

      stActualizarConceptosCalculados.executeBatch();

      double montoBase1Si = 0.0D;
      double montoBase2Si = 0.0D;
      double montoBaseTotalSi = 0.0D;

      double montoBase1Sb = 0.0D;
      double montoBase2Sb = 0.0D;
      double montoBaseTotalSb = 0.0D;

      double montoBase1Co = 0.0D;
      double montoBase2Co = 0.0D;
      double montoBaseTotalCo = 0.0D;

      double montoBase1Pt = 0.0D;
      double montoBase2Pt = 0.0D;
      double montoBaseTotalPt = 0.0D;

      double montoBase1Pc = 0.0D;
      double montoBase2Pc = 0.0D;
      double montoBaseTotalPc = 0.0D;

      double montoBase1As = 0.0D;
      double montoBase2As = 0.0D;
      double montoBaseTotalAs = 0.0D;

      sql = new StringBuffer();
      sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, fp.cod_frecuencia_pago, cf.unidades, cf.monto, ");
      sql.append(" c.sueldo_basico, c.sueldo_integral, c.compensacion, c.primas_trabajador, c.primas_cargo, c.ajuste_sueldo");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
      sql.append("  concepto c, frecuenciapago fp");
      sql.append(" where");
      sql.append("  cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and ctp.cod_concepto < '5000'");
      sql.append(" and cf.id_trabajador = ? ");
      sql.append(" and (c.sueldo_basico = 'S' ");
      sql.append(" or c.sueldo_integral = 'S' ");
      sql.append(" or c.compensacion = 'S' ");
      sql.append(" or c.primas_trabajador = 'S' ");
      sql.append(" or c.primas_cargo = 'S' ");
      sql.append(" or c.ajuste_sueldo= 'S') ");

      stPromedios = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stPromedios.setLong(1, trabajador.getIdTrabajador());
      rsPromedios = stPromedios.executeQuery();

      while (rsPromedios.next()) {
        if (rsPromedios.getInt("cod_frecuencia_pago") == 4) {
          if (rsPromedios.getString("sueldo_integral").equals("S")) {
            montoBase1Si += rsPromedios.getDouble("monto");
          }
          if (rsPromedios.getString("sueldo_basico").equals("S"))
            montoBase1Sb += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("compensacion").equals("S"))
            montoBase1Co += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_trabajador").equals("S"))
            montoBase1Pt += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_cargo").equals("S"))
            montoBase1Pc += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("ajuste_sueldo").equals("S"))
            montoBase1As += rsPromedios.getDouble("monto");
        }
        else if (rsPromedios.getInt("cod_frecuencia_pago") != 10) {
          if (rsPromedios.getString("sueldo_integral").equals("S")) {
            montoBase2Si += rsPromedios.getDouble("monto");
          }
          if (rsPromedios.getString("sueldo_basico").equals("S"))
            montoBase2Sb += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("compensacion").equals("S"))
            montoBase2Co += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_trabajador").equals("S"))
            montoBase2Pt += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_cargo").equals("S"))
            montoBase2Pc += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("ajuste_sueldo").equals("S"))
            montoBase2As += rsPromedios.getDouble("monto");
        }
        else
        {
          if (rsPromedios.getString("sueldo_integral").equals("S")) {
            montoBase2Si += rsPromedios.getDouble("monto") * 4.0D;
          }
          if (rsPromedios.getString("sueldo_basico").equals("S"))
            montoBase2Sb += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("compensacion").equals("S"))
            montoBase2Co += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("primas_trabajador").equals("S"))
            montoBase2Pt += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("primas_cargo").equals("S"))
            montoBase2Pc += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("ajuste_sueldo").equals("S")) {
            montoBase2As += rsPromedios.getDouble("monto") * 4.0D;
          }
        }
      }

      if (trabajador.getTipoPersonal().getFormulaIntegral().equals("1")) {
        montoBaseTotalSi = (montoBase1Si / 7.0D + montoBase2Si / 30.0D) * 365.0D / 12.0D;
        montoBaseTotalSb = (montoBase1Sb / 7.0D + montoBase2Sb / 30.0D) * 365.0D / 12.0D;
        montoBaseTotalCo = (montoBase1Co / 7.0D + montoBase2Co / 30.0D) * 365.0D / 12.0D;
        montoBaseTotalPt = (montoBase1Pt / 7.0D + montoBase2Pt / 30.0D) * 365.0D / 12.0D;
        montoBaseTotalPc = (montoBase1Pc / 7.0D + montoBase2Pc / 30.0D) * 365.0D / 12.0D;
        montoBaseTotalAs = (montoBase1As / 7.0D + montoBase2As / 30.0D) * 365.0D / 12.0D;
      } else if (trabajador.getTipoPersonal().getFormulaIntegral().equals("2")) {
        montoBaseTotalSi = montoBase1Si / 7.0D * 30.0D + montoBase2Si;
        montoBaseTotalSb = montoBase1Sb / 7.0D * 30.0D + montoBase2Sb;
        montoBaseTotalCo = montoBase1Co / 7.0D * 30.0D + montoBase2Co;
        montoBaseTotalPt = montoBase1Pt / 7.0D * 30.0D + montoBase2Pt;
        montoBaseTotalPc = montoBase1Pc / 7.0D * 30.0D + montoBase2Pc;
        montoBaseTotalAs = montoBase1As / 7.0D * 30.0D + montoBase2As;
      } else if (trabajador.getTipoPersonal().getFormulaIntegral().equals("3")) {
        montoBaseTotalSi = montoBase1Si * 52.0D / 12.0D + montoBase2Si;
        montoBaseTotalSb = montoBase1Sb * 52.0D / 12.0D + montoBase2Sb;
        montoBaseTotalCo = montoBase1Co * 52.0D / 12.0D + montoBase2Co;
        montoBaseTotalPt = montoBase1Pt * 52.0D / 12.0D + montoBase2Pt;
        montoBaseTotalPc = montoBase1Pc * 52.0D / 12.0D + montoBase2Pc;
        montoBaseTotalAs = montoBase1As * 52.0D / 12.0D + montoBase2As;
      }

      sql = new StringBuffer();
      sql.append("UPDATE sueldoPromedio SET ");
      sql.append("promedio_integral = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalSi));
      sql.append(",promedio_sueldo = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalSb));
      sql.append(",promedio_compensacion = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalCo));
      sql.append(",promedio_primast = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalPt));
      sql.append(",promedio_primasc = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalPc));
      sql.append(",promedio_ajustes = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalAs));
      sql.append(" WHERE id_trabajador = ");
      sql.append(trabajador.getIdTrabajador());

      stSueldoPromedio.addBatch(sql.toString());
      stSueldoPromedio.executeBatch();
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
    finally {
      if (rsConceptosCalculados != null) try {
          rsConceptosCalculados.close();
        } catch (Exception localException1) {
        } if (rsPromedios != null) try {
          rsPromedios.close();
        } catch (Exception localException2) {
        } if (stConceptosCalculados != null) try {
          stConceptosCalculados.close();
        } catch (Exception localException3) {
        } if (stPromedios != null) try {
          stPromedios.close();
        } catch (Exception localException4) {
        } if (stActualizarConceptosCalculados != null) try {
          stActualizarConceptosCalculados.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6)
        {
        }
    }
  }

  public void calcularMensual(long idOrganismo, long idGrupoNomina, String periodicidad, String recalculo, Date fechaInicio, Date fechaFin, Integer semanaMes, Boolean tieneSemana5, Integer numeroSemanasMes)
    throws Exception
  {
    boolean primeraQuincena = false;

    if (fechaInicio.getDate() == 1)
    {
      primeraQuincena = true;
    }
    else primeraQuincena = false;

    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      this.log.error("1-idOrganismo " + idOrganismo);
      this.log.error("2-idGrupoNomina " + idGrupoNomina);
      this.log.error("3-periodicidad " + periodicidad);
      this.log.error("4-recalculo " + recalculo);
      this.log.error("5-primeraQincena " + primeraQuincena);
      this.log.error("6-semanaMes " + semanaMes);
      this.log.error("7-tieneSemana5 " + tieneSemana5);
      this.log.error("8-numeroSemanaMes " + numeroSemanasMes);

      StringBuffer sql = new StringBuffer();
      sql.append("select calcular_sueldo_promedio_mensual(?, ?, ?, ?, ?, ?, ?, ?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idOrganismo);
      st.setLong(2, idGrupoNomina);
      st.setString(3, periodicidad);
      st.setString(4, recalculo);
      st.setBoolean(5, primeraQuincena);
      st.setInt(6, semanaMes.intValue());
      st.setBoolean(7, tieneSemana5.booleanValue());
      st.setInt(8, numeroSemanasMes.intValue());

      rs = st.executeQuery();
      connection.commit();

      this.log.error("ejecutó SP sueldo_promedio_mensual");
    }
    finally
    {
      if (rs != null) try {
          rs.close();
        }
        catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
  }

  public double calcularLimiteMensual(double montoBaseTotal, double montoBase2Sso, String parametro, String periodicidad, ParametroGobierno parametroGobierno)
  {
    double limite = 0.0D;
    if (parametro.equals("SSO"))
      limite = parametroGobierno.getLimmenSso();
    else if (parametro.equals("LPH"))
      limite = parametroGobierno.getLimmenLph();
    else if (parametro.equals("FJU")) {
      limite = parametroGobierno.getLimmenFju();
    }
    if (montoBaseTotal > limite) {
      montoBaseTotal = limite;
    }
    return montoBaseTotal;
  }

  public void reCalcularConceptosPortipoPersonal(long idTipoPersonal)
    throws Exception
  {
    double montoCalculado = 0.0D;

    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

    Connection connection = null;
    ResultSet rsConceptosCalculados = null;
    PreparedStatement stConceptosCalculados = null;
    try
    {
      StringBuffer sql = new StringBuffer();
      connection = Resource.getConnection();
      Statement stActualizarConceptosCalculados = connection.createStatement();

      sql = new StringBuffer();
      sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, fp.cod_frecuencia_pago, cf.unidades, ctp.tipo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo,");
      sql.append(" t.id_trabajador, tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, t.id_cargo");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
      sql.append("  concepto c, frecuenciapago fp, trabajador t, turno tu, tipopersonal tp");
      sql.append(" where");
      sql.append(" cf.id_trabajador = t.id_trabajador ");
      sql.append(" and t.id_turno = tu.id_turno");
      sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_tipo_personal = ? ");
      sql.append(" and  cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and ctp.recalculo = 'S'");
      sql.append(" and c.cod_concepto not in('5001', '5003', '5004')");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");

      stConceptosCalculados = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptosCalculados.setLong(1, idTipoPersonal);
      rsConceptosCalculados = stConceptosCalculados.executeQuery();

      while (rsConceptosCalculados.next()) {
        montoCalculado = 0.0D;
        sql = new StringBuffer();
        if (rsConceptosCalculados.getString("tipo").equals("I"))
          montoCalculado = calcularConceptoBeanBusiness.calcular(rsConceptosCalculados.getLong("id_concepto_tipo_personal"), rsConceptosCalculados.getLong("id_trabajador"), rsConceptosCalculados.getDouble("unidades"), rsConceptosCalculados.getString("tipo"), rsConceptosCalculados.getInt("cod_frecuencia_pago"), rsConceptosCalculados.getDouble("jornada_diaria"), rsConceptosCalculados.getDouble("jornada_semanal"), rsConceptosCalculados.getString("formula_integral"), rsConceptosCalculados.getString("formula_semanal"), rsConceptosCalculados.getLong("id_cargo"), rsConceptosCalculados.getDouble("valor"), rsConceptosCalculados.getDouble("tope_minimo"), rsConceptosCalculados.getDouble("tope_maximo"));
        else {
          montoCalculado = calcularConceptoBeanBusiness.calcular(rsConceptosCalculados.getLong("id_concepto_tipo_personal"), rsConceptosCalculados.getLong("id_trabajador"), 1.0D, rsConceptosCalculados.getString("tipo"), rsConceptosCalculados.getInt("cod_frecuencia_pago"), rsConceptosCalculados.getDouble("jornada_diaria"), rsConceptosCalculados.getDouble("jornada_semanal"), rsConceptosCalculados.getString("formula_integral"), rsConceptosCalculados.getString("formula_semanal"), rsConceptosCalculados.getLong("id_cargo"), rsConceptosCalculados.getDouble("valor"), rsConceptosCalculados.getDouble("tope_minimo"), rsConceptosCalculados.getDouble("tope_maximo"));
        }
        sql.append("update conceptofijo set monto = " + montoCalculado + " where id_concepto_fijo = " + rsConceptosCalculados.getLong("id_concepto_fijo"));

        stActualizarConceptosCalculados.addBatch(sql.toString());
      }

      stActualizarConceptosCalculados.executeBatch();
      connection.commit();
    }
    finally {
      if (rsConceptosCalculados != null) try {
          rsConceptosCalculados.close();
        }
        catch (Exception localException) {
        } if (stConceptosCalculados != null) try {
          stConceptosCalculados.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
  }

  public void calcularSemanal(long idOrganismo, long idGrupoNomina, String periodicidad, String recalculo, Date fechaInicio, Date fechaFin, Integer semanaMes, Boolean tieneSemana5, Integer numeroSemanasMes)
    throws Exception
  {
    boolean primeraQuincena = false;

    if (fechaInicio.getDate() == 1)
    {
      primeraQuincena = true;
    }
    else primeraQuincena = false;

    this.log.error("1-idOrganismo " + idOrganismo);
    this.log.error("2-idGrupoNomina " + idGrupoNomina);
    this.log.error("3-periodicidad " + periodicidad);
    this.log.error("4-recalculo " + recalculo);
    this.log.error("5-primeraQincena " + primeraQuincena);
    this.log.error("6-semanaMes " + semanaMes);
    this.log.error("7-tieneSemana5 " + tieneSemana5);
    this.log.error("8-numeroSemanaMes " + numeroSemanasMes);

    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      StringBuffer sql = new StringBuffer();
      sql.append("select calcular_sueldo_promedio_semanal(?, ?, ?, ?, ?, ?, ?, ?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idOrganismo);
      st.setLong(2, idGrupoNomina);
      st.setString(3, periodicidad);
      st.setString(4, recalculo);
      st.setBoolean(5, primeraQuincena);
      st.setInt(6, semanaMes.intValue());
      st.setBoolean(7, tieneSemana5.booleanValue());
      st.setInt(8, numeroSemanasMes.intValue());

      rs = st.executeQuery();

      connection.commit();

      this.log.error("ejecutó SP sueldo_promedio_semanal");
    } finally {
      if (rs != null) try {
          rs.close();
        }
        catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
  }

  public double calcularLimiteSemanal(double montoBaseTotal, double montoBase2Sso, String parametro, String periodicidad, ParametroGobierno parametroGobierno)
  {
    double limite = 0.0D;
    if (parametro.equals("SSO")) {
      if (montoBase2Sso == 0.0D)
        limite = parametroGobierno.getLimsemSso();
      else {
        limite = parametroGobierno.getLimmenSso();
      }
    }
    else if (parametro.equals("LPH"))
      limite = parametroGobierno.getLimmenLph();
    else if (parametro.equals("FJU")) {
      limite = parametroGobierno.getLimmenFju();
    }
    if (montoBaseTotal > limite) {
      montoBaseTotal = limite;
    }
    return montoBaseTotal;
  }

  public void agregarConceptoMovimientoAnterior(long idTrabajador, long numeroMovimiento) {
    ResultSet rsConceptosCalculados = null;
    PreparedStatement stConceptosCalculados = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    Connection connection = null;

    StringBuffer sql = new StringBuffer();
    sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, cf.id_frecuencia_tipo_personal, cf.monto, ");
    sql.append(" fp.cod_frecuencia_pago, cf.unidades, ctp.tipo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo, c.recalculo ");
    sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
    sql.append("  concepto c, frecuenciapago fp");
    sql.append(" where");
    sql.append(" cf.id_trabajador = ? ");
    sql.append("  and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
    sql.append(" and ctp.id_concepto = c.id_concepto");
    sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
    sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
    sql.append(" order by c.cod_concepto");
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      stConceptosCalculados = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptosCalculados.setLong(1, idTrabajador);
      rsConceptosCalculados = stConceptosCalculados.executeQuery();

      int anio = new Date().getYear() + 1900;
      while (rsConceptosCalculados.next())
      {
        sql = new StringBuffer();

        this.log.error("1- id_concepto_fijo " + rsConceptosCalculados.getLong("id_concepto_fijo"));
        this.log.error("2- monto " + rsConceptosCalculados.getDouble("monto"));
        this.log.error("3- numero_movimiento " + numeroMovimiento);
        this.log.error("4- id_concepto_tipo_personal " + rsConceptosCalculados.getLong("id_concepto_tipo_personal"));
        this.log.error("5- id_frecuencia_tipo_personal " + rsConceptosCalculados.getLong("id_frecuencia_tipo_personal"));
        this.log.error("6- id_trabajador " + idTrabajador);
        this.log.error("7- anio " + new Date().getYear() + 1900);

        sql = new StringBuffer();
        sql.append("select actualizar_conceptomovimiento_anterior(?,?,?,?,?,?,?)");

        st = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        st.setLong(1, rsConceptosCalculados.getLong("id_concepto_fijo"));
        st.setDouble(2, rsConceptosCalculados.getDouble("monto"));
        st.setLong(3, numeroMovimiento);
        st.setLong(4, rsConceptosCalculados.getLong("id_concepto_tipo_personal"));
        st.setLong(5, rsConceptosCalculados.getLong("id_frecuencia_tipo_personal"));
        st.setLong(6, idTrabajador);
        st.setInt(7, anio);
        rs = st.executeQuery();
      }
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException1) {
        } if (rsConceptosCalculados != null) try {
          rsConceptosCalculados.close();
        } catch (Exception localException2) {
        } if (stConceptosCalculados != null) try {
          stConceptosCalculados.close();
        } catch (Exception localException3) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException4)
        {
        } 
    }
  }

  public void calcularUnTrabajadorParaMovimientos(Trabajador trabajador, long numeroMovimiento) { CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();
    ResultSet rsConceptosCalculados = null;
    PreparedStatement stConceptosCalculados = null;

    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;

    Statement stSueldoPromedio = null;

    ResultSet rsPromedios = null;
    PreparedStatement stPromedios = null;

    String periodicidad = trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad();

    StringBuffer sql = new StringBuffer();
    sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, cf.id_frecuencia_tipo_personal, cf.monto, ");
    sql.append(" fp.cod_frecuencia_pago, cf.unidades, ctp.tipo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo, ctp.recalculo ");
    sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
    sql.append("  concepto c, frecuenciapago fp");
    sql.append(" where");
    sql.append(" cf.id_trabajador = ? ");
    sql.append("  and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
    sql.append(" and ctp.id_concepto = c.id_concepto");
    sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
    sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
    sql.append(" order by c.cod_concepto");
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      stSueldoPromedio = connection.createStatement();

      stConceptosCalculados = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptosCalculados.setLong(1, trabajador.getIdTrabajador());
      rsConceptosCalculados = stConceptosCalculados.executeQuery();

      int anio = new Date().getYear() + 1900;
      while (rsConceptosCalculados.next()) {
        double montoCalculado = 0.0D;
        sql = new StringBuffer();

        if (rsConceptosCalculados.getString("recalculo").equals("S")) {
          this.log.error("va a calcular ");

          montoCalculado = calcularConceptoBeanBusiness.calcular(rsConceptosCalculados.getLong("id_concepto_tipo_personal"), 
            trabajador.getIdTrabajador(), rsConceptosCalculados.getDouble("unidades"), 
            rsConceptosCalculados.getString("tipo"), 
            rsConceptosCalculados.getInt("cod_frecuencia_pago"), 
            trabajador.getTurno().getJornadaDiaria(), trabajador.getTurno().getJornadaSemanal(), 
            trabajador.getTipoPersonal().getFormulaIntegral(), 
            trabajador.getTipoPersonal().getFormulaSemanal(), trabajador.getCargo().getIdCargo(), 
            rsConceptosCalculados.getDouble("valor"), 
            rsConceptosCalculados.getDouble("tope_minimo"), 
            rsConceptosCalculados.getDouble("tope_maximo"));
        } else {
          montoCalculado = rsConceptosCalculados.getDouble("monto");
        }
        this.log.error("1- id_concepto_fijo " + rsConceptosCalculados.getLong("id_concepto_fijo"));
        this.log.error("2- monto " + montoCalculado);
        this.log.error("3- numero_movimiento " + numeroMovimiento);
        this.log.error("4- id_concepto_tipo_personal " + rsConceptosCalculados.getLong("id_concepto_tipo_personal"));
        this.log.error("5- id_frecuencia_tipo_personal " + rsConceptosCalculados.getLong("id_frecuencia_tipo_personal"));
        this.log.error("6- id_trabajador " + trabajador.getIdTrabajador());
        this.log.error("7- anio " + new Date().getYear() + 1900);

        sql = new StringBuffer();
        sql.append("select actualizar_conceptomovimiento(?,?,?,?,?,?,?)");

        st = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        st.setLong(1, rsConceptosCalculados.getLong("id_concepto_fijo"));
        st.setDouble(2, montoCalculado);
        st.setLong(3, numeroMovimiento);
        st.setLong(4, rsConceptosCalculados.getLong("id_concepto_tipo_personal"));
        st.setLong(5, rsConceptosCalculados.getLong("id_frecuencia_tipo_personal"));
        st.setLong(6, trabajador.getIdTrabajador());
        st.setInt(7, anio);
        rs = st.executeQuery();
      }

      double montoBase1Si = 0.0D;
      double montoBase2Si = 0.0D;
      double montoBaseTotalSi = 0.0D;

      double montoBase1Sb = 0.0D;
      double montoBase2Sb = 0.0D;
      double montoBaseTotalSb = 0.0D;

      double montoBase1Co = 0.0D;
      double montoBase2Co = 0.0D;
      double montoBaseTotalCo = 0.0D;

      double montoBase1Pt = 0.0D;
      double montoBase2Pt = 0.0D;
      double montoBaseTotalPt = 0.0D;

      double montoBase1Pc = 0.0D;
      double montoBase2Pc = 0.0D;
      double montoBaseTotalPc = 0.0D;

      double montoBase1As = 0.0D;
      double montoBase2As = 0.0D;
      double montoBaseTotalAs = 0.0D;

      sql = new StringBuffer();
      sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, fp.cod_frecuencia_pago, cf.unidades, cf.monto, ");
      sql.append(" c.sueldo_basico, c.sueldo_integral, c.compensacion, c.primas_trabajador, c.primas_cargo, c.ajuste_sueldo");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
      sql.append("  concepto c, frecuenciapago fp");
      sql.append(" where");
      sql.append("  cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and ctp.cod_concepto < '5000'");
      sql.append(" and cf.id_trabajador = ? ");
      sql.append(" and (c.sueldo_basico = 'S' ");
      sql.append(" or c.sueldo_integral = 'S' ");
      sql.append(" or c.compensacion = 'S' ");
      sql.append(" or c.primas_trabajador = 'S' ");
      sql.append(" or c.primas_cargo = 'S' ");
      sql.append(" or c.ajuste_sueldo= 'S') ");

      stPromedios = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stPromedios.setLong(1, trabajador.getIdTrabajador());
      rsPromedios = stPromedios.executeQuery();

      while (rsPromedios.next()) {
        if (!periodicidad.equals("S")) {
          if (rsPromedios.getString("sueldo_integral").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1Si += rsPromedios.getDouble("monto") * 2.0D;
            else {
              montoBase2Si += rsPromedios.getDouble("monto");
            }
          }
          if (rsPromedios.getString("sueldo_basico").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1Sb += rsPromedios.getDouble("monto") * 2.0D;
            else
              montoBase2Sb += rsPromedios.getDouble("monto");
          }
          else if (rsPromedios.getString("compensacion").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1Co += rsPromedios.getDouble("monto") * 2.0D;
            else
              montoBase2Co += rsPromedios.getDouble("monto");
          }
          else if (rsPromedios.getString("primas_trabajador").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1Pt += rsPromedios.getDouble("monto") * 2.0D;
            else
              montoBase2Pt += rsPromedios.getDouble("monto");
          }
          else if (rsPromedios.getString("primas_cargo").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1Pc += rsPromedios.getDouble("monto") * 2.0D;
            else
              montoBase2Pc += rsPromedios.getDouble("monto");
          }
          else if (rsPromedios.getString("ajuste_sueldo").equals("S")) {
            if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
              montoBase1As += rsPromedios.getDouble("monto") * 2.0D;
            else {
              montoBase2As += rsPromedios.getDouble("monto");
            }
          }

        }
        else if (rsPromedios.getInt("cod_frecuencia_pago") == 4) {
          if (rsPromedios.getString("sueldo_integral").equals("S")) {
            montoBase1Si += rsPromedios.getDouble("monto");
          }
          if (rsPromedios.getString("sueldo_basico").equals("S"))
            montoBase1Sb += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("compensacion").equals("S"))
            montoBase1Co += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_trabajador").equals("S"))
            montoBase1Pt += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_cargo").equals("S"))
            montoBase1Pc += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("ajuste_sueldo").equals("S"))
            montoBase1As += rsPromedios.getDouble("monto");
        }
        else if (rsPromedios.getInt("cod_frecuencia_pago") != 10) {
          if (rsPromedios.getString("sueldo_integral").equals("S")) {
            montoBase2Si += rsPromedios.getDouble("monto");
          }
          if (rsPromedios.getString("sueldo_basico").equals("S"))
            montoBase2Sb += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("compensacion").equals("S"))
            montoBase2Co += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_trabajador").equals("S"))
            montoBase2Pt += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("primas_cargo").equals("S"))
            montoBase2Pc += rsPromedios.getDouble("monto");
          else if (rsPromedios.getString("ajuste_sueldo").equals("S"))
            montoBase2As += rsPromedios.getDouble("monto");
        }
        else
        {
          if (rsPromedios.getString("sueldo_integral").equals("S")) {
            montoBase2Si += rsPromedios.getDouble("monto") * 4.0D;
          }
          if (rsPromedios.getString("sueldo_basico").equals("S"))
            montoBase2Sb += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("compensacion").equals("S"))
            montoBase2Co += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("primas_trabajador").equals("S"))
            montoBase2Pt += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("primas_cargo").equals("S"))
            montoBase2Pc += rsPromedios.getDouble("monto") * 4.0D;
          else if (rsPromedios.getString("ajuste_sueldo").equals("S")) {
            montoBase2As += rsPromedios.getDouble("monto") * 4.0D;
          }

        }

      }

      if (periodicidad.equals("S")) {
        if (trabajador.getTipoPersonal().getFormulaIntegral().equals("1")) {
          montoBaseTotalSi = (montoBase1Si / 7.0D + montoBase2Si / 30.0D) * 365.0D / 12.0D;
          montoBaseTotalSb = (montoBase1Sb / 7.0D + montoBase2Sb / 30.0D) * 365.0D / 12.0D;
          montoBaseTotalCo = (montoBase1Co / 7.0D + montoBase2Co / 30.0D) * 365.0D / 12.0D;
          montoBaseTotalPt = (montoBase1Pt / 7.0D + montoBase2Pt / 30.0D) * 365.0D / 12.0D;
          montoBaseTotalPc = (montoBase1Pc / 7.0D + montoBase2Pc / 30.0D) * 365.0D / 12.0D;
          montoBaseTotalAs = (montoBase1As / 7.0D + montoBase2As / 30.0D) * 365.0D / 12.0D;
        } else if (trabajador.getTipoPersonal().getFormulaIntegral().equals("2")) {
          montoBaseTotalSi = montoBase1Si / 7.0D * 30.0D + montoBase2Si;
          montoBaseTotalSb = montoBase1Sb / 7.0D * 30.0D + montoBase2Sb;
          montoBaseTotalCo = montoBase1Co / 7.0D * 30.0D + montoBase2Co;
          montoBaseTotalPt = montoBase1Pt / 7.0D * 30.0D + montoBase2Pt;
          montoBaseTotalPc = montoBase1Pc / 7.0D * 30.0D + montoBase2Pc;
          montoBaseTotalAs = montoBase1As / 7.0D * 30.0D + montoBase2As;
        } else if (trabajador.getTipoPersonal().getFormulaIntegral().equals("3")) {
          montoBaseTotalSi = montoBase1Si * 52.0D / 12.0D + montoBase2Si;
          montoBaseTotalSb = montoBase1Sb * 52.0D / 12.0D + montoBase2Sb;
          montoBaseTotalCo = montoBase1Co * 52.0D / 12.0D + montoBase2Co;
          montoBaseTotalPt = montoBase1Pt * 52.0D / 12.0D + montoBase2Pt;
          montoBaseTotalPc = montoBase1Pc * 52.0D / 12.0D + montoBase2Pc;
          montoBaseTotalAs = montoBase1As * 52.0D / 12.0D + montoBase2As;
        }
      } else {
        montoBaseTotalSi = montoBase1Si + montoBase2Si;
        montoBaseTotalSb = montoBase1Sb + montoBase2Sb;
        montoBaseTotalCo = montoBase1Co + montoBase2Co;
        montoBaseTotalPt = montoBase1Pt + montoBase2Pt;
        montoBaseTotalPc = montoBase1Pc + montoBase2Pc;
        montoBaseTotalAs = montoBase1As + montoBase2As;
      }

      sql = new StringBuffer();
      sql.append("UPDATE sueldoPromedio SET ");
      sql.append("promedio_integral = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalSi));
      sql.append(",promedio_sueldo = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalSb));
      sql.append(",promedio_compensacion = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalCo));
      sql.append(",promedio_primast = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalPt));
      sql.append(",promedio_primasc = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalPc));
      sql.append(",promedio_ajustes = ");
      sql.append(NumberTools.twoDecimal(montoBaseTotalAs));
      sql.append(" WHERE id_trabajador = ");
      sql.append(trabajador.getIdTrabajador());

      stSueldoPromedio.addBatch(sql.toString());
      stSueldoPromedio.executeBatch();
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException1) {
        } if (rsPromedios != null) try {
          rsPromedios.close();
        } catch (Exception localException2) {
        } if (rsConceptosCalculados != null) try {
          rsConceptosCalculados.close();
        } catch (Exception localException3) {
        } if (stConceptosCalculados != null) try {
          stConceptosCalculados.close();
        } catch (Exception localException4) {
        } if (stPromedios != null) try {
          stPromedios.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6)
        {
        } 
    } } 
  public void calcularPromedios(long idTipoPersonal, String periodicidad) throws Exception {
    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      this.log.error("1-idTipoPersonal " + idTipoPersonal);
      this.log.error("2-periodicidad " + periodicidad);

      StringBuffer sql = new StringBuffer();
      if (periodicidad.equals("S"))
        sql.append("select calcular_promedios_semanal(?)");
      else {
        sql.append("select calcular_promedios_mensual(?)");
      }
      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);

      rs = st.executeQuery();
      connection.commit();

      this.log.error("ejecutó SP ");
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        } 
    }
  }

  public void calcularConceptosProyectados(Trabajador trabajador)
  {
    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();
    ResultSet rsConceptosCalculados = null;
    PreparedStatement stConceptosCalculados = null;

    Statement stActualizar = null;

    Connection connection = null;

    String periodicidad = trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad();

    StringBuffer sql = new StringBuffer();
    sql.append("select  cp.id_concepto_tipo_personal, cp.id_frecuencia_tipo_personal, cp.monto, ");
    sql.append(" fp.cod_frecuencia_pago, 1 as unidades, ctp.tipo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo, c.recalculo ");
    sql.append(" from  conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
    sql.append("  concepto c, frecuenciapago fp, conceptoproyectado cp");
    sql.append(" where");
    sql.append(" cp.id_trabajador = ? ");
    sql.append("  and cp.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
    sql.append(" and ctp.id_concepto = c.id_concepto");
    sql.append(" and cp.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
    sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
    sql.append(" order by c.cod_concepto");
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      stActualizar = connection.createStatement();

      stConceptosCalculados = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptosCalculados.setLong(1, trabajador.getIdTrabajador());
      rsConceptosCalculados = stConceptosCalculados.executeQuery();

      int anio = new Date().getYear() + 1900;
      while (rsConceptosCalculados.next()) {
        double montoCalculado = 0.0D;
        sql = new StringBuffer();

        if (rsConceptosCalculados.getString("recalculo").equals("S")) {
          this.log.error(" id_concepto_tipo_personal  = " + rsConceptosCalculados.getLong("id_concepto_tipo_personal"));
          this.log.error(" id_trabajador  = " + trabajador.getIdTrabajador());
          this.log.error(" unidades  = " + rsConceptosCalculados.getDouble("unidades"));
          this.log.error(" tipo  = " + rsConceptosCalculados.getString("tipo"));
          this.log.error(" cod_frecuencia_pago  = " + rsConceptosCalculados.getInt("cod_frecuencia_pago"));
          this.log.error(" jornada_diaria  = " + trabajador.getTurno().getJornadaDiaria());
          this.log.error(" jornada_semanal  = " + trabajador.getTurno().getJornadaSemanal());
          this.log.error(" formula Integral  = " + trabajador.getTipoPersonal().getFormulaIntegral());
          this.log.error(" formula_semanal  = " + trabajador.getTipoPersonal().getFormulaSemanal());
          this.log.error(" id_cargo  = " + trabajador.getCargo().getIdCargo());
          this.log.error(" valor  = " + rsConceptosCalculados.getDouble("valor"));
          this.log.error(" tope minimo  = " + rsConceptosCalculados.getDouble("tope_minimo"));
          this.log.error(" tope maximo  = " + rsConceptosCalculados.getDouble("tope_maximo"));

          montoCalculado = calcularConceptoBeanBusiness.calcularProyectado(rsConceptosCalculados.getLong("id_concepto_tipo_personal"), trabajador.getIdTrabajador(), rsConceptosCalculados.getDouble("unidades"), rsConceptosCalculados.getString("tipo"), rsConceptosCalculados.getInt("cod_frecuencia_pago"), trabajador.getTurno().getJornadaDiaria(), trabajador.getTurno().getJornadaSemanal(), trabajador.getTipoPersonal().getFormulaIntegral(), trabajador.getTipoPersonal().getFormulaSemanal(), trabajador.getCargo().getIdCargo(), rsConceptosCalculados.getDouble("valor"), rsConceptosCalculados.getDouble("tope_minimo"), rsConceptosCalculados.getDouble("tope_maximo"));
        } else {
          montoCalculado = rsConceptosCalculados.getDouble("monto");
        }
        this.log.error(" id_concepto_tipo_personal = " + rsConceptosCalculados.getLong("id_concepto_tipo_personal"));
        this.log.error(" monto_calculado = " + montoCalculado);
        sql.append("update conceptoproyectado set monto = " + montoCalculado);
        sql.append(" where id_trabajador = " + trabajador.getIdTrabajador());
        sql.append(" and id_concepto_tipo_personal = " + rsConceptosCalculados.getLong("id_concepto_tipo_personal"));
        sql.append(" and id_frecuencia_tipo_personal = " + rsConceptosCalculados.getLong("id_frecuencia_tipo_personal"));
        stActualizar.execute(sql.toString());
      }
    }
    catch (Exception e)
    {
      this.log.error("Excepcion controlada:", e);
    }
    finally {
      if (rsConceptosCalculados != null) try {
          rsConceptosCalculados.close();
        } catch (Exception localException1) {
        } if (stConceptosCalculados != null) try {
          stConceptosCalculados.close();
        } catch (Exception localException2) {
        } if (stActualizar != null) try {
          stActualizar.close();
        } catch (Exception localException3) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException4)
        {
        }
    }
  }
}