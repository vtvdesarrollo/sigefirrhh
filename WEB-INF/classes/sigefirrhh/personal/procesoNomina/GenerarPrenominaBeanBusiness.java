package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.GrupoNominaBeanBusiness;
import sigefirrhh.personal.trabajador.ConceptoFijoNoGenBeanBusiness;
import sigefirrhh.personal.trabajador.ConceptoVariableNoGenBeanBusiness;
import sigefirrhh.personal.trabajador.PrestamoNoGenBeanBusiness;
import sigefirrhh.personal.trabajador.SueldoPromedioBeanBusinessExtend;

public class GenerarPrenominaBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(GenerarPrenominaBeanBusiness.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DefinicionesNoGenBusiness definicionesBusiness = new DefinicionesNoGenBusiness();
  private ConceptoFijoNoGenBeanBusiness conceptoFijoNoGenBeanBusiness = new ConceptoFijoNoGenBeanBusiness();
  private ConceptoVariableNoGenBeanBusiness conceptoVariableNoGenBeanBusiness = new ConceptoVariableNoGenBeanBusiness();
  private PrestamoNoGenBeanBusiness prestamoNoGenBeanBusiness = new PrestamoNoGenBeanBusiness();
  private SueldoPromedioBeanBusinessExtend sueldoPromedioBeanBusiness = new SueldoPromedioBeanBusinessExtend();

  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public boolean generar(long idGrupoNomina, String periodicidad, String recalculo, java.util.Date fechaInicio, java.util.Date fechaFin, long idOrganismo, Integer lunesPrQuincena, Integer lunesSeQuincena, Boolean tieneSemana5, Integer semanaMes, Integer numeroSemanasMes)
    throws Exception
  {
    CalcularSueldosPromedioBeanBusiness calcularSueldosPromedio = 
      new CalcularSueldosPromedioBeanBusiness();
    this.log.error("PASO POR PRENOMINA NUEVA 0.0");
    if (periodicidad.equals("S"))
      calcularSueldosPromedio.calcularSemanal(
        idOrganismo, idGrupoNomina, periodicidad, recalculo, fechaInicio, fechaFin, semanaMes, tieneSemana5, numeroSemanasMes);
    else {
      calcularSueldosPromedio.calcularMensual(
        idOrganismo, idGrupoNomina, periodicidad, recalculo, fechaInicio, fechaFin, semanaMes, tieneSemana5, numeroSemanasMes);
    }

    this.log.error("PASO POR PRENOMINA NUEVA 0");
    return generarPrenomina(idGrupoNomina, periodicidad, recalculo, fechaInicio, fechaFin, idOrganismo, lunesPrQuincena, lunesSeQuincena, tieneSemana5, semanaMes);
  }

  private boolean generarPrenomina(long idGrupoNomina, String periodicidad, String recalculo, java.util.Date fechaInicio, java.util.Date fechaFin, long idOrganismo, Integer lunesPrQuincena, Integer lunesSeQuincena, Boolean tieneSemana5, Integer semanaMes)
    throws Exception
  {
    boolean sobregirados = false;
    double montoCalculado = 0.0D;
    this.log.error("VA INICIAR LA NUEVA PRENOMINA CON SP");

    GrupoNomina grupoNomina = new GrupoNomina();
    GrupoNominaBeanBusiness grupoNominaBeanBusiness = new GrupoNominaBeanBusiness();
    grupoNomina = grupoNominaBeanBusiness.findGrupoNominaById(idGrupoNomina);

    int anio = fechaInicio.getYear() + 1900;
    int mes = fechaInicio.getMonth() + 1;

    long idTrabajador = 0L;
    long idHistoricoNomina = 0L;

    int semanaQuincena = 0;
    boolean primeraQuincena;
    boolean primeraQuincena;
    if (fechaInicio.getDate() == 1) {
      semanaQuincena = 1;
      primeraQuincena = true;
    } else {
      semanaQuincena = 2;
      primeraQuincena = false;
    }

    java.sql.Date fechaInicioSql = new java.sql.Date(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDate());
    java.sql.Date fechaFinSql = new java.sql.Date(fechaFin.getYear(), fechaFin.getMonth(), fechaFin.getDate());

    java.sql.Date fechaSql = new java.sql.Date(new java.util.Date().getYear(), new java.util.Date().getMonth(), new java.util.Date().getDate());

    this.log.error("1- false");
    this.log.error("2- true");
    this.log.error("3- " + idGrupoNomina);
    this.log.error("4- " + periodicidad);
    this.log.error("5- " + recalculo);
    this.log.error("6- " + fechaInicioSql);
    this.log.error("7- " + fechaFinSql);
    this.log.error("8- " + idOrganismo);
    this.log.error("9- " + lunesPrQuincena.intValue());
    this.log.error("10- " + lunesSeQuincena.intValue());
    this.log.error("11- " + tieneSemana5.booleanValue());
    this.log.error("12- " + semanaMes.intValue());
    this.log.error("13- null");
    this.log.error("14- 0");
    this.log.error("15- 0");
    this.log.error("16- " + primeraQuincena);
    this.log.error("17- " + semanaQuincena);
    this.log.error("18- 0");
    this.log.error("19- " + fechaSql);
    this.log.error("20- " + mes);
    this.log.error("21- " + anio);
    this.log.error("22- null");
    this.log.error("23- null");
    this.log.error("24- null");
    this.log.error("25- null");
    this.log.error("26- 0");
    this.log.error("27- 0");
    this.log.error("28- A");
    this.log.error("29- " + grupoNomina.getPagosNominaEgresados());

    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      StringBuffer sql = new StringBuffer();
      sql.append("select generar_nomina(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setBoolean(1, true);
      st.setBoolean(2, true);
      st.setLong(3, idGrupoNomina);
      st.setString(4, periodicidad);
      st.setString(5, recalculo);
      st.setDate(6, fechaInicioSql);
      st.setDate(7, fechaFinSql);
      st.setLong(8, idOrganismo);
      st.setInt(9, lunesPrQuincena.intValue());
      st.setInt(10, lunesSeQuincena.intValue());
      st.setBoolean(11, tieneSemana5.booleanValue());
      st.setInt(12, semanaMes.intValue());
      st.setString(13, null);
      st.setInt(14, 0);
      st.setInt(15, 0);
      st.setBoolean(16, primeraQuincena);
      st.setInt(17, semanaQuincena);
      st.setLong(18, 0L);
      st.setDate(19, fechaSql);
      st.setInt(20, mes);
      st.setInt(21, anio);
      st.setDate(22, null);
      st.setDate(23, null);
      st.setDate(24, null);
      st.setDate(25, null);
      st.setInt(26, 0);
      st.setInt(27, 0);
      st.setString(28, "A");
      st.setString(29, grupoNomina.getPagosNominaEgresados());

      int valor = 0;
      rs = st.executeQuery();
      rs.next();
      valor = rs.getInt(1);
      connection.commit();

      this.log.error("ejecutó la prenomina");
      if (valor == 1) {
        this.log.error("HAY SOBREGIRADOS");
        return true;
      }
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5) {  }
 
    }
    return false;
  }

  public Collection getMensajesUltimaPrenomina(Long idGrupoNomina)
    throws Exception
  {
    this.log.debug("getMensajesUltimaPrenomina()");
    PersistenceManager pm = PMThread.getPM();

    String filter = "nominaEspecial == null && grupoNomina.idGrupoNomina == pIdGrupoNomina";

    Query query = pm.newQuery(MensajesPrenomina.class, filter);

    query.declareParameters("long pIdGrupoNomina");

    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", idGrupoNomina);

    Collection colMensajesUltimaPrenomina = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    this.log.error("query:--------------------------" + query);
    this.log.error("idGrupoNomina:--------------------------" + idGrupoNomina);
    this.log.error("colMensajesUltimaPrenomina:--------------------------" + colMensajesUltimaPrenomina);

    pm.makeTransientAll(colMensajesUltimaPrenomina);

    return colMensajesUltimaPrenomina;
  }
}