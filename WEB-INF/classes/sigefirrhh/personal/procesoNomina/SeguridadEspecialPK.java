package sigefirrhh.personal.procesoNomina;

import java.io.Serializable;

public class SeguridadEspecialPK
  implements Serializable
{
  public long idSeguridadEspecial;

  public SeguridadEspecialPK()
  {
  }

  public SeguridadEspecialPK(long idSeguridadEspecial)
  {
    this.idSeguridadEspecial = idSeguridadEspecial;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SeguridadEspecialPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SeguridadEspecialPK thatPK)
  {
    return 
      this.idSeguridadEspecial == thatPK.idSeguridadEspecial;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSeguridadEspecial)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSeguridadEspecial);
  }
}