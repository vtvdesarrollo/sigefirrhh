package sigefirrhh.personal.procesoNomina;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ReversarNominaBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(ReversarNominaBeanBusiness.class.getName());

  public boolean reversar(long idGrupoNomina, int numeroNomina, java.util.Date fechaInicio, String periodicidad, int anio, int mes, int semanaQuincena, boolean nominaEspecial)
    throws Exception
  {
    java.sql.Date fechaInicioSql = new java.sql.Date(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDate());

    this.log.error("1- " + idGrupoNomina);
    this.log.error("2-" + numeroNomina);
    this.log.error("3- " + fechaInicioSql);
    this.log.error("4- " + periodicidad);
    this.log.error("5- " + anio);
    this.log.error("6- " + mes);
    this.log.error("7- " + semanaQuincena);
    this.log.error("8- " + nominaEspecial);

    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      StringBuffer sql = new StringBuffer();
      sql.append("select reversar_nomina(?, ?, ?, ?, ?, ?, ?, ?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idGrupoNomina);
      st.setInt(2, numeroNomina);
      st.setDate(3, fechaInicioSql);
      st.setString(4, periodicidad);
      st.setInt(5, anio);
      st.setInt(6, mes);
      st.setInt(7, semanaQuincena);
      st.setBoolean(8, nominaEspecial);

      int valor = 0;
      rs = st.executeQuery();
      rs.next();
      valor = rs.getInt(1);
      connection.commit();

      if (valor == 1)
        return true;
      int valor;
      StringBuffer sql;
      if (valor == 2)
        return false;
      int valor;
      StringBuffer sql;
      if (valor == 100) {
        this.log.error("Hubó un error");
        ErrorSistema error = new ErrorSistema();
        error.setDescription("Hubó un error");
        throw error;
      }
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException6) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException7) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException8)
        {
        }
    }
    if (rs != null) try {
        rs.close();
      } catch (Exception localException9) {
      } if (st != null) try {
        st.close();
      } catch (Exception localException10) {
      } if (connection != null) try {
        connection.close(); connection = null;
      }
      catch (Exception localException11)
      {
      } return false;
  }
}