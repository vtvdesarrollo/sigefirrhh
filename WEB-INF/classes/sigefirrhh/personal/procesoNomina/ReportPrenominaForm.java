package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportPrenominaForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportPrenominaForm.class.getName());
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private String selectGrupoNomina;
  private String inicio;
  private String fin;
  private Calendar inicioAux;
  private Calendar finAux;
  private long idGrupoNomina;
  private Collection listGrupoNomina;
  private String periodicidad;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private Integer semanaMes;
  private Integer lunesPrQuincena;
  private Integer lunesSeQuincena;
  private Boolean tieneSemana5;
  private Integer semanaAnio;
  private boolean show;
  private boolean auxShow;
  private boolean showSemana;
  private long idUnidadAdministradora;
  private Collection listUnidadAdministradora;
  private String conceptoDesde = "0000";
  private String conceptoHasta = "9999";

  public ReportPrenominaForm() {
    this.reportName = "prenominarac";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportPrenominaForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public boolean isShowConcepto() {
    return (this.tipoReporte >= 50) && (this.tipoReporte <= 99);
  }
  private void cambiarNombreAReporte() {
    if (this.tipoReporte == 10)
      this.reportName = "prenominarac";
    else if (this.tipoReporte == 15)
      this.reportName = "prenominaalf";
    else if (this.tipoReporte == 20)
      this.reportName = "prenominapr";
    else if (this.tipoReporte == 25)
      this.reportName = "prenominauel";
    else if (this.tipoReporte == 30)
      this.reportName = "prenominareg";
    else if (this.tipoReporte == 35)
      this.reportName = "prenominadep";
    else if (this.tipoReporte == 50)
      this.reportName = "detconceptopralf";
    else if (this.tipoReporte == 55)
      this.reportName = "detconceptoprcod";
    else if (this.tipoReporte == 60)
      this.reportName = "detconceptoprced";
    else if (this.tipoReporte == 100)
      this.reportName = "resconceptopr";
    else if (this.tipoReporte == 105)
      this.reportName = "resconceptopruel";
    else if (this.tipoReporte == 110)
      this.reportName = "resconceptoprcat";
    else if (this.tipoReporte == 115)
      this.reportName = "resconceptoprcatuel";
    else if (this.tipoReporte == 130)
      this.reportName = "resconceptopruadm";
    else if (this.tipoReporte == 150)
      this.reportName = "depbanpralf";
    else if (this.tipoReporte == 155)
      this.reportName = "depbanprcod";
    else if (this.tipoReporte == 160)
      this.reportName = "depbanprced";
    else if (this.tipoReporte == 165)
      this.reportName = "depbanprua";
    else if (this.tipoReporte == 170)
      this.reportName = "depbanprcedtipo";
    else if (this.tipoReporte == 200)
      this.reportName = "chequespr";
    else if (this.tipoReporte == 207)
      this.reportName = "contratosvencidos";
    else if (this.tipoReporte == 300) {
      this.reportName = "sobregirados";
    }
    if (this.idUnidadAdministradora != 0L)
      this.reportName += "_ua";
  }

  public void changeGrupoNomina(ValueChangeEvent event)
  {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.showSemana = false;
      Collection colPeriodo = this.procesoNominaNoGenFacade.findFechaProximaNomina(this.idGrupoNomina);
      Iterator iteratorPeriodo = colPeriodo.iterator();
      if (iteratorPeriodo.hasNext()) {
        this.inicioAux = ((Calendar)iteratorPeriodo.next());
        this.finAux = ((Calendar)iteratorPeriodo.next());
        this.lunesPrQuincena = ((Integer)iteratorPeriodo.next());
        this.lunesSeQuincena = ((Integer)iteratorPeriodo.next());
        this.tieneSemana5 = ((Boolean)iteratorPeriodo.next());
        this.semanaMes = ((Integer)iteratorPeriodo.next());
        this.semanaAnio = ((Integer)iteratorPeriodo.next());
        this.periodicidad = ((String)iteratorPeriodo.next());
        this.auxShow = true;
        if (this.periodicidad.equals("S"))
          this.showSemana = true;
      }
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      this.listUnidadAdministradora = this.estructuraFacade.findUnidadAdministradoraByOrganismo(this.login.getIdOrganismo());
      log.error("Vacio:" + this.listGrupoNomina.isEmpty());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listGrupoNomina = new ArrayList();
      this.listUnidadAdministradora = new ArrayList();
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("numero_nomina", new Integer(0));
    Date fec_ini = new Date(this.inicioAux.getTime().getYear(), this.inicioAux.getTime().getMonth(), this.inicioAux.getTime().getDate());
    Date fec_fin = new Date(this.finAux.getTime().getYear(), this.finAux.getTime().getMonth(), this.finAux.getTime().getDate());
    parameters.put("fec_ini", fec_ini);
    parameters.put("fec_fin", fec_fin);
    parameters.put("id_grupo_nomina", new Long(this.idGrupoNomina));
    parameters.put("semana_anio", this.semanaAnio);
    parameters.put("nomina_estatus", "A");
    parameters.put("nombre_nomina", "ORDINARIA");
    parameters.put("anio", new Integer(fec_ini.getYear()));
    log.error("anio " + new Integer(fec_ini.getYear()));

    if ((this.tipoReporte >= 50) && (this.tipoReporte <= 99)) {
      parameters.put("cod_concepto_desde", this.conceptoDesde);
      parameters.put("cod_concepto_hasta", this.conceptoHasta);
      log.error("cod_concepto_desde" + this.conceptoDesde);
      log.error("cod_concepto_hasta" + this.conceptoHasta);
    }

    if (this.tipoReporte == 10)
      this.reportName = "prenominarac";
    else if (this.tipoReporte == 15)
      this.reportName = "prenominaalf";
    else if (this.tipoReporte == 20)
      this.reportName = "prenominapr";
    else if (this.tipoReporte == 25)
      this.reportName = "prenominauel";
    else if (this.tipoReporte == 30)
      this.reportName = "prenominareg";
    else if (this.tipoReporte == 35)
      this.reportName = "prenominadep";
    else if (this.tipoReporte == 50)
      this.reportName = "detconceptopralf";
    else if (this.tipoReporte == 55)
      this.reportName = "detconceptoprcod";
    else if (this.tipoReporte == 60)
      this.reportName = "detconceptoprced";
    else if (this.tipoReporte == 100)
      this.reportName = "resconceptopr";
    else if (this.tipoReporte == 105)
      this.reportName = "resconceptopruel";
    else if (this.tipoReporte == 110)
      this.reportName = "resconceptoprcat";
    else if (this.tipoReporte == 115)
      this.reportName = "resconceptoprcatuel";
    else if (this.tipoReporte == 130)
      this.reportName = "resconceptopruadm";
    else if (this.tipoReporte == 150)
      this.reportName = "depbanpralf";
    else if (this.tipoReporte == 155)
      this.reportName = "depbanprcod";
    else if (this.tipoReporte == 160)
      this.reportName = "depbanprced";
    else if (this.tipoReporte == 165)
      this.reportName = "depbanprua";
    else if (this.tipoReporte == 170)
      this.reportName = "depbanprcedtipo";
    else if (this.tipoReporte == 200)
      this.reportName = "chequespr";
    else if (this.tipoReporte == 207)
      this.reportName = "contratosvencidos";
    else if (this.tipoReporte == 300) {
      this.reportName = "sobregirados";
    }
    if (this.idUnidadAdministradora != 0L) {
      this.reportName += "_ua";
      parameters.put("id_unidad_administradora", new Long(this.idUnidadAdministradora));
    }
    JasperForWeb report = new JasperForWeb();

    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/procesoNomina");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public Collection getListGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }

  public String getSelectGrupoNomina()
  {
    return this.selectGrupoNomina;
  }

  public void setSelectGrupoNomina(String string)
  {
    this.selectGrupoNomina = string;
  }

  public String getFin()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }

  public void setFin(String string)
  {
    this.fin = string;
  }

  public void setInicio(String string)
  {
    this.inicio = string;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public long getIdGrupoNomina()
  {
    return this.idGrupoNomina;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setIdGrupoNomina(long l)
  {
    this.idGrupoNomina = l;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public long getIdUnidadAdministradora()
  {
    return this.idUnidadAdministradora;
  }

  public void setIdUnidadAdministradora(long l)
  {
    this.idUnidadAdministradora = l;
  }

  public Integer getSemanaAnio() {
    return this.semanaAnio;
  }
  public void setSemanaAnio(Integer semanaAnio) {
    this.semanaAnio = semanaAnio;
  }
  public boolean isShowSemana() {
    return this.showSemana;
  }
  public String getConceptoHasta() {
    return this.conceptoHasta;
  }
  public void setConceptoHasta(String conceptoHasta) {
    this.conceptoHasta = conceptoHasta;
  }
  public DefinicionesNoGenFacade getDefinicionesFacade() {
    return this.definicionesFacade;
  }
  public void setDefinicionesFacade(DefinicionesNoGenFacade definicionesFacade) {
    this.definicionesFacade = definicionesFacade;
  }
  public String getConceptoDesde() {
    return this.conceptoDesde;
  }
  public void setConceptoDesde(String conceptoDesde) {
    this.conceptoDesde = conceptoDesde;
  }
}