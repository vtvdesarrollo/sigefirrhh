package sigefirrhh.personal.procesoNomina;

import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportPrenominaEspecialForm
  implements Serializable
{
  private static Logger log = Logger.getLogger(ReportPrenominaEspecialForm.class.getName());
  int tipoReporte;
  private int reportId;
  private String reportName;
  private String selectGrupoNomina;
  private String inicio;
  private String fin;
  private Calendar inicioAux;
  private Calendar finAux;
  private long idGrupoNomina;
  private Collection listGrupoNomina;
  private String periodicidad;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private Integer semanaMes;
  private Integer lunesPrQuincena;
  private Integer lunesSeQuincena;
  private Boolean tieneSemana5;
  private Integer semanaAnio;
  private boolean show;
  private boolean show2;
  private Collection listNominaEspecial;
  private String selectNominaEspecial;
  private NominaEspecial nominaEspecial;
  private long idUnidadAdministradora;
  private Collection listUnidadAdministradora;

  public void setSelectNominaEspecial(String selectNominaEspecial)
  {
    this.selectNominaEspecial = selectNominaEspecial;
  }
  public ReportPrenominaEspecialForm() {
    this.reportName = "prenominarac";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.inicio = null;
    this.fin = null;
    this.inicioAux = Calendar.getInstance();
    this.finAux = Calendar.getInstance();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportPrenominaEspecialForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void cambiarNombreAReporte()
  {
    if (this.tipoReporte == 1)
      this.reportName = "prenominarac";
    else if (this.tipoReporte == 2)
      this.reportName = "prenominaalf";
    else if (this.tipoReporte == 3)
      this.reportName = "prenominauel";
    else if (this.tipoReporte == 4)
      this.reportName = "detconceptopralf";
    else if (this.tipoReporte == 28)
      this.reportName = "detconceptoprcod";
    else if (this.tipoReporte == 29)
      this.reportName = "detconceptoprced";
    else if (this.tipoReporte == 5)
      this.reportName = "resconceptopr";
    else if (this.tipoReporte == 6)
      this.reportName = "resconceptopruel";
    else if (this.tipoReporte == 7)
      this.reportName = "depbanpralf";
    else if (this.tipoReporte == 8)
      this.reportName = "chequespr";
    else if (this.tipoReporte == 20)
      this.reportName = "resconceptopruadm";
    else if (this.tipoReporte == 22)
      this.reportName = "prenominareg";
    else if (this.tipoReporte == 23)
      this.reportName = "prenominadep";
    else if (this.tipoReporte == 24)
      this.reportName = "prenominapr";
    else if (this.tipoReporte == 25)
      this.reportName = "depbanprcod";
    else if (this.tipoReporte == 26)
      this.reportName = "depbanprced";
    else if (this.tipoReporte == 27)
      this.reportName = "depbanprua";
    else if (this.tipoReporte == 30)
      this.reportName = "sobregirados";
    else if (this.tipoReporte == 31)
      this.reportName = "depbannocedtipo";
    else if (this.tipoReporte == 70)
      this.reportName = "resconceptoprcat";
    else if (this.tipoReporte == 71) {
      this.reportName = "resconceptoprcatuel";
    }

    if (this.idUnidadAdministradora != 0L)
      this.reportName += "_ua";
  }

  public void changeGrupoNomina(ValueChangeEvent event)
  {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.show = false;
      this.show2 = false;
      this.listNominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialByGrupoNominaAndEstatus(this.idGrupoNomina, "A", "N");

      this.show = true;
    }
    catch (Exception e) {
      this.show = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeNominaEspecial(ValueChangeEvent event)
  {
    long idNominaEspecial = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.show2 = false;
      this.nominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialById(idNominaEspecial);
      this.inicioAux.setTime(this.nominaEspecial.getFechaInicio());
      this.finAux.setTime(this.nominaEspecial.getFechaFin());
      this.show2 = true;
    }
    catch (Exception e)
    {
      this.show = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      this.listUnidadAdministradora = this.estructuraFacade.findUnidadAdministradoraByOrganismo(this.login.getIdOrganismo());
      log.error("Vacio:" + this.listGrupoNomina.isEmpty());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listGrupoNomina = new ArrayList();
      this.listUnidadAdministradora = new ArrayList();
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("numero_nomina", new Integer(this.nominaEspecial.getNumeroNomina()));

    Date fec_ini = new Date(this.inicioAux.getTime().getYear(), this.inicioAux.getTime().getMonth(), this.inicioAux.getTime().getDate());
    Date fec_fin = new Date(this.finAux.getTime().getYear(), this.finAux.getTime().getMonth(), this.finAux.getTime().getDate());

    parameters.put("fec_ini", fec_ini);
    parameters.put("fec_fin", fec_fin);
    parameters.put("anio", new Integer(fec_ini.getYear()));

    parameters.put("id_grupo_nomina", new Long(this.idGrupoNomina));
    parameters.put("semana_anio", new Integer(0));
    parameters.put("nomina_estatus", this.nominaEspecial.getPersonal());
    parameters.put("nombre_nomina", this.nominaEspecial.getDescripcion());

    if (this.tipoReporte == 1)
      this.reportName = "prenominarac";
    else if (this.tipoReporte == 2)
      this.reportName = "prenominaalf";
    else if (this.tipoReporte == 3)
      this.reportName = "prenominauel";
    else if (this.tipoReporte == 4)
      this.reportName = "detconceptopralf";
    else if (this.tipoReporte == 28)
      this.reportName = "detconceptoprcod";
    else if (this.tipoReporte == 29)
      this.reportName = "detconceptoprced";
    else if (this.tipoReporte == 5)
      this.reportName = "resconceptopr";
    else if (this.tipoReporte == 6)
      this.reportName = "resconceptopruel";
    else if (this.tipoReporte == 7)
      this.reportName = "depbanpralf";
    else if (this.tipoReporte == 8)
      this.reportName = "chequespr";
    else if (this.tipoReporte == 20)
      this.reportName = "resconceptopruadm";
    else if (this.tipoReporte == 22)
      this.reportName = "prenominareg";
    else if (this.tipoReporte == 23)
      this.reportName = "prenominadep";
    else if (this.tipoReporte == 24)
      this.reportName = "prenominapr";
    else if (this.tipoReporte == 25)
      this.reportName = "depbanprcod";
    else if (this.tipoReporte == 26)
      this.reportName = "depbanprced";
    else if (this.tipoReporte == 27)
      this.reportName = "depbanprua";
    else if (this.tipoReporte == 30)
      this.reportName = "sobregirados";
    else if (this.tipoReporte == 31)
      this.reportName = "depbannocedtipo";
    else if (this.tipoReporte == 70)
      this.reportName = "resconceptoprcat";
    else if (this.tipoReporte == 71) {
      this.reportName = "resconceptoprcatuel";
    }

    if (this.idUnidadAdministradora != 0L) {
      this.reportName += "_ua";
      parameters.put("id_unidad_administradora", new Long(this.idUnidadAdministradora));
    }
    JasperForWeb report = new JasperForWeb();

    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/procesoNomina");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public Collection getListGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }
  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String string) {
    this.selectGrupoNomina = string;
  }
  public boolean isShow() {
    return this.show;
  }
  public int getTipoReporte() {
    return this.tipoReporte;
  }
  public void setTipoReporte(int i) {
    this.tipoReporte = i;
  }
  public long getIdGrupoNomina() {
    return this.idGrupoNomina;
  }
  public LoginSession getLogin() {
    return this.login;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setIdGrupoNomina(long l) {
    this.idGrupoNomina = l;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public Collection getListNominaEspecial() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listNominaEspecial, "sigefirrhh.personal.procesoNomina.NominaEspecial");
  }
  public String getSelectNominaEspecial() {
    return this.selectNominaEspecial;
  }
  public String getFin() {
    if (this.finAux == null) {
      return null;
    }
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio() {
    if (this.inicioAux == null) {
      return null;
    }
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }

  public boolean isShow2()
  {
    return this.show2;
  }
  public void setFin(String string) {
    this.fin = string;
  }
  public void setInicio(String string) {
    this.inicio = string;
  }

  public long getIdUnidadAdministradora()
  {
    return this.idUnidadAdministradora;
  }

  public void setIdUnidadAdministradora(long l)
  {
    this.idUnidadAdministradora = l;
  }
}