package sigefirrhh.personal.constancias;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class ConstanciasFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private ConstanciasBusiness constanciasBusiness = new ConstanciasBusiness();

  public void addConstancia(Constancia constancia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.constanciasBusiness.addConstancia(constancia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConstancia(Constancia constancia) throws Exception
  {
    try { this.txn.open();
      this.constanciasBusiness.updateConstancia(constancia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConstancia(Constancia constancia) throws Exception
  {
    try { this.txn.open();
      this.constanciasBusiness.deleteConstancia(constancia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Constancia findConstanciaById(long constanciaId) throws Exception
  {
    try { this.txn.open();
      Constancia constancia = 
        this.constanciasBusiness.findConstanciaById(constanciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(constancia);
      return constancia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConstancia() throws Exception
  {
    try { this.txn.open();
      return this.constanciasBusiness.findAllConstancia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConstanciaByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.constanciasBusiness.findConstanciaByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoConstancia(ConceptoConstancia conceptoConstancia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.constanciasBusiness.addConceptoConstancia(conceptoConstancia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoConstancia(ConceptoConstancia conceptoConstancia) throws Exception
  {
    try { this.txn.open();
      this.constanciasBusiness.updateConceptoConstancia(conceptoConstancia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoConstancia(ConceptoConstancia conceptoConstancia) throws Exception
  {
    try { this.txn.open();
      this.constanciasBusiness.deleteConceptoConstancia(conceptoConstancia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoConstancia findConceptoConstanciaById(long conceptoConstanciaId) throws Exception
  {
    try { this.txn.open();
      ConceptoConstancia conceptoConstancia = 
        this.constanciasBusiness.findConceptoConstanciaById(conceptoConstanciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoConstancia);
      return conceptoConstancia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoConstancia() throws Exception
  {
    try { this.txn.open();
      return this.constanciasBusiness.findAllConceptoConstancia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoConstanciaByConstancia(long idConstancia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.constanciasBusiness.findConceptoConstanciaByConstancia(idConstancia);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}