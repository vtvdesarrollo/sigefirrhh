package sigefirrhh.personal.constancias;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class ConstanciasNoGenFacade extends ConstanciasFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private ConstanciasNoGenBusiness constanciasNoGenBusiness = new ConstanciasNoGenBusiness();

  public Collection findConstanciaByTipoPersonalAndTipo(long idTipoPersonal, String tipo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.constanciasNoGenBusiness.findConstanciaByTipoPersonalAndTipo(idTipoPersonal, tipo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void calcularConceptosProyectados(long idTrabajador, long idConstancia, String periodicidad, long idTipoPersonal)
    throws Exception
  {
    this.constanciasNoGenBusiness.calcularConceptosProyectado(idTrabajador, idConstancia, periodicidad, idTipoPersonal);
  }
}