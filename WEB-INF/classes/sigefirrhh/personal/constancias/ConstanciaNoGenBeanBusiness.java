package sigefirrhh.personal.constancias;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import eforserver.tools.NumberTools;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;
import sigefirrhh.personal.conceptos.CalcularConceptoBeanBusiness;

public class ConstanciaNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(ConstanciaNoGenBeanBusiness.class.getName());

  public Collection findByTipoPersonalAndTipo(long idTipoPersonal, String tipo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && tipo == pTipo";

    Query query = pm.newQuery(Constancia.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pTipo");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pTipo", tipo);

    Collection colConstancia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConstancia);

    return colConstancia;
  }

  public void calcularConceptosProyectados(long idTrabajador, long idConstancia, String periodicidad, long idTipoPersonal)
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    ResultSet rsFrecuencia = null;
    PreparedStatement stFrecuencia = null;

    Statement stActualizar = null;
    try
    {
      CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

      StringBuffer sql = new StringBuffer();
      double monto = 0.0D;

      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      stActualizar = connection.createStatement();

      stActualizar = connection.createStatement();

      sql = new StringBuffer();
      sql.append("select fp.cod_frecuencia_pago, ftp.id_frecuencia_tipo_personal");
      sql.append(" from frecuenciatipopersonal ftp, frecuenciapago fp");
      sql.append(" where fp.cod_frecuencia_pago = ?");
      sql.append(" and ftp.id_tipo_personal = ?");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      stFrecuencia = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      if (periodicidad.equals("S"))
        stFrecuencia.setInt(1, 5);
      else {
        stFrecuencia.setInt(1, 2);
      }
      stFrecuencia.setLong(2, idTipoPersonal);
      rsFrecuencia = stFrecuencia.executeQuery();

      if (!rsFrecuencia.next())
        return;
      double monto;
      CalcularConceptoBeanBusiness calcularConceptoBeanBusiness;
      StringBuffer sql = new StringBuffer();
      sql.append("select t.id_trabajador, ctp.id_concepto_tipo_personal,");
      sql.append(" tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral,");
      sql.append(" tp.formula_semanal, ctp.unidades, ctp.tipo,");
      sql.append(" t.id_cargo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo");
      sql.append(" from conceptoconstancia cc, conceptotipopersonal ctp, constancia c,");
      sql.append(" trabajador t, tipopersonal tp, turno tu");
      sql.append(" where ctp.id_concepto_tipo_personal in (");
      sql.append(" select cc.id_concepto_tipo_personal");
      sql.append(" from conceptoconstancia cc, constancia c");
      sql.append(" where cc.id_constancia = c.id_constancia");
      sql.append(" and c.id_constancia = ?");
      sql.append(" and cc.id_concepto_tipo_personal not in(");
      sql.append(" select cf.id_concepto_tipo_personal");
      sql.append(" from conceptofijo cf");
      sql.append(" where cf.id_trabajador = ?))");
      sql.append(" and cc.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and t.id_tipo_personal = ctp.id_tipo_personal");
      sql.append(" and tp.id_tipo_personal = t.id_tipo_personal");
      sql.append(" and t.id_turno = tu.id_turno");
      sql.append(" and id_trabajador = ?");
      sql.append(" and cc.id_constancia = c.id_constancia");
      sql.append(" and c.id_constancia = ?");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConstancia);
      stRegistros.setLong(2, idTrabajador);
      stRegistros.setLong(3, idTrabajador);
      stRegistros.setLong(4, idConstancia);

      rsRegistros = stRegistros.executeQuery();

      sql = new StringBuffer();
      sql.append("delete from conceptoproyectado where id_trabajador = " + idTrabajador);
      this.log.error("elimino conceptos");
      stActualizar.execute(sql.toString());

      while (rsRegistros.next()) {
        sql = new StringBuffer();

        monto = calcularConceptoBeanBusiness.calcular_constancia(rsRegistros.getLong("id_concepto_tipo_personal"), 
          rsRegistros.getLong("id_trabajador"), rsRegistros.getDouble("unidades"), 
          rsRegistros.getString("tipo"), rsFrecuencia.getInt("cod_frecuencia_pago"), 
          rsRegistros.getDouble("jornada_diaria"), rsRegistros.getDouble("jornada_semanal"), 
          rsRegistros.getString("formula_integral"), rsRegistros.getString("formula_semanal"), 
          rsRegistros.getLong("id_cargo"), rsRegistros.getDouble("valor"), 
          rsRegistros.getDouble("tope_minimo"), rsRegistros.getDouble("tope_maximo"));

        sql.append("insert into conceptoproyectado (id_trabajador, id_concepto_tipo_personal,");
        sql.append(" id_frecuencia_tipo_personal, monto) values( " + idTrabajador + ",");
        sql.append(rsRegistros.getLong("id_concepto_tipo_personal") + ",");
        sql.append(rsFrecuencia.getLong("id_frecuencia_tipo_personal") + ",");
        sql.append(NumberTools.twoDecimal(monto) + ")");

        stActualizar.addBatch(sql.toString());
      }

      stActualizar.executeBatch();
      rsRegistros.close();
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException6) {
        } if (rsFrecuencia != null) try {
          rsFrecuencia.close();
        } catch (Exception localException7) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException8) {
        } if (stFrecuencia != null) try {
          stFrecuencia.close();
        } catch (Exception localException9) {
        } if (stActualizar != null) try {
          stActualizar.close();
        } catch (Exception localException10) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException11)
        {
        }
    }
    if (rsRegistros != null) try {
        rsRegistros.close();
      } catch (Exception localException12) {
      } if (rsFrecuencia != null) try {
        rsFrecuencia.close();
      } catch (Exception localException13) {
      } if (stRegistros != null) try {
        stRegistros.close();
      } catch (Exception localException14) {
      } if (stFrecuencia != null) try {
        stFrecuencia.close();
      } catch (Exception localException15) {
      } if (stActualizar != null) try {
        stActualizar.close();
      } catch (Exception localException16) {
      } if (connection != null) try {
        connection.close(); connection = null;
      }
      catch (Exception localException17)
      {
      }
  }
}