package sigefirrhh.personal.constancias;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class ConstanciasBusiness extends AbstractBusiness
  implements Serializable
{
  private ConstanciaBeanBusiness constanciaBeanBusiness = new ConstanciaBeanBusiness();

  private ConceptoConstanciaBeanBusiness conceptoConstanciaBeanBusiness = new ConceptoConstanciaBeanBusiness();

  public void addConstancia(Constancia constancia)
    throws Exception
  {
    this.constanciaBeanBusiness.addConstancia(constancia);
  }

  public void updateConstancia(Constancia constancia) throws Exception {
    this.constanciaBeanBusiness.updateConstancia(constancia);
  }

  public void deleteConstancia(Constancia constancia) throws Exception {
    this.constanciaBeanBusiness.deleteConstancia(constancia);
  }

  public Constancia findConstanciaById(long constanciaId) throws Exception {
    return this.constanciaBeanBusiness.findConstanciaById(constanciaId);
  }

  public Collection findAllConstancia() throws Exception {
    return this.constanciaBeanBusiness.findConstanciaAll();
  }

  public Collection findConstanciaByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.constanciaBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addConceptoConstancia(ConceptoConstancia conceptoConstancia)
    throws Exception
  {
    this.conceptoConstanciaBeanBusiness.addConceptoConstancia(conceptoConstancia);
  }

  public void updateConceptoConstancia(ConceptoConstancia conceptoConstancia) throws Exception {
    this.conceptoConstanciaBeanBusiness.updateConceptoConstancia(conceptoConstancia);
  }

  public void deleteConceptoConstancia(ConceptoConstancia conceptoConstancia) throws Exception {
    this.conceptoConstanciaBeanBusiness.deleteConceptoConstancia(conceptoConstancia);
  }

  public ConceptoConstancia findConceptoConstanciaById(long conceptoConstanciaId) throws Exception {
    return this.conceptoConstanciaBeanBusiness.findConceptoConstanciaById(conceptoConstanciaId);
  }

  public Collection findAllConceptoConstancia() throws Exception {
    return this.conceptoConstanciaBeanBusiness.findConceptoConstanciaAll();
  }

  public Collection findConceptoConstanciaByConstancia(long idConstancia)
    throws Exception
  {
    return this.conceptoConstanciaBeanBusiness.findByConstancia(idConstancia);
  }
}