package sigefirrhh.personal.constancias;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ConceptoConstanciaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoConstanciaForm.class.getName());
  private ConceptoConstancia conceptoConstancia;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ConstanciasFacade constanciasFacade = new ConstanciasFacade();
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private DefinicionesFacadeExtend definicionesFacadeExtend = new DefinicionesFacadeExtend();
  private boolean showConceptoConstanciaByConstancia;
  private String findSelectTipoPersonalForConstancia;
  private String findSelectConstancia;
  private Collection findColTipoPersonalForConstancia;
  private Collection findColConstancia;
  private Collection colTipoPersonal;
  private Collection colConstancia;
  private Collection colConceptoTipoPersonal;
  private String selectTipoPersonal;
  private String selectConstancia;
  private String selectConceptoTipoPersonal;
  private Object stateResultConceptoConstanciaByConstancia = null;

  public Collection getFindColTipoPersonalForConstancia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonalForConstancia.iterator();
    TipoPersonal tipoPersonalForConstancia = null;
    while (iterator.hasNext()) {
      tipoPersonalForConstancia = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConstancia.getIdTipoPersonal()), 
        tipoPersonalForConstancia.toString()));
    }
    return col;
  }
  public String getFindSelectTipoPersonalForConstancia() {
    return this.findSelectTipoPersonalForConstancia;
  }
  public void setFindSelectTipoPersonalForConstancia(String valTipoPersonalForConstancia) {
    this.findSelectTipoPersonalForConstancia = valTipoPersonalForConstancia;
  }
  public void findChangeTipoPersonalForConstancia(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColConstancia = null;
      if (idTipoPersonal > 0L)
        this.findColConstancia = 
          this.constanciasFacade.findConstanciaByTipoPersonal(
          idTipoPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowTipoPersonalForConstancia() { return this.findColTipoPersonalForConstancia != null; }

  public String getFindSelectConstancia() {
    return this.findSelectConstancia;
  }
  public void setFindSelectConstancia(String valConstancia) {
    this.findSelectConstancia = valConstancia;
  }

  public Collection getFindColConstancia() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConstancia.iterator();
    Constancia constancia = null;
    while (iterator.hasNext()) {
      constancia = (Constancia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(constancia.getIdConstancia()), 
        constancia.toString()));
    }
    return col;
  }
  public boolean isFindShowConstancia() {
    return this.findColConstancia != null;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    this.selectTipoPersonal = valTipoPersonal;
  }
  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colConstancia = null;
      this.colConceptoTipoPersonal = null;
      if (idTipoPersonal > 0L) {
        this.colConstancia = 
          this.constanciasFacade.findConstanciaByTipoPersonal(
          idTipoPersonal);
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonalAsignaciones(
          idTipoPersonal);
      } else {
        this.selectConstancia = null;
        this.conceptoConstancia.setConstancia(null);
        this.selectConceptoTipoPersonal = null;
        this.conceptoConstancia.setConceptoTipoPersonal(null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectConstancia = null;
      this.conceptoConstancia.setConstancia(null);
      this.selectConceptoTipoPersonal = null;
      this.conceptoConstancia.setConceptoTipoPersonal(null);
    }
  }

  public String getSelectConstancia() {
    return this.selectConstancia;
  }
  public void setSelectConstancia(String valConstancia) {
    Iterator iterator = this.colConstancia.iterator();
    Constancia constancia = null;
    this.conceptoConstancia.setConstancia(null);
    while (iterator.hasNext()) {
      constancia = (Constancia)iterator.next();
      if (String.valueOf(constancia.getIdConstancia()).equals(
        valConstancia)) {
        this.conceptoConstancia.setConstancia(
          constancia);
        break;
      }
    }
    this.selectConstancia = valConstancia;
  }
  public boolean isShowConstancia() {
    return this.colConstancia != null;
  }

  public String getSelectConceptoTipoPersonal()
  {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    this.conceptoConstancia.setConceptoTipoPersonal(null);
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()).equals(
        valConceptoTipoPersonal)) {
        this.conceptoConstancia.setConceptoTipoPersonal(
          conceptoTipoPersonal);
        break;
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public boolean isShowConceptoTipoPersonal() {
    return this.colConceptoTipoPersonal != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoConstancia getConceptoConstancia() {
    if (this.conceptoConstancia == null) {
      this.conceptoConstancia = new ConceptoConstancia();
    }
    return this.conceptoConstancia;
  }

  public ConceptoConstanciaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConstancia = null;
    while (iterator.hasNext()) {
      tipoPersonalForConstancia = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConstancia.getIdTipoPersonal()), 
        tipoPersonalForConstancia.toString()));
    }
    return col;
  }

  public Collection getColConstancia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConstancia.iterator();
    Constancia constancia = null;
    while (iterator.hasNext()) {
      constancia = (Constancia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(constancia.getIdConstancia()), 
        constancia.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getListOrigen() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoConstancia.LISTA_ORIGEN.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonalForConstancia = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConceptoConstanciaByConstancia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.constanciasFacade.findConceptoConstanciaByConstancia(Long.valueOf(this.findSelectConstancia).longValue());
      this.showConceptoConstanciaByConstancia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoConstanciaByConstancia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonalForConstancia = null;
    this.findSelectConstancia = null;

    return null;
  }

  public boolean isShowConceptoConstanciaByConstancia() {
    return this.showConceptoConstanciaByConstancia;
  }

  public String selectConceptoConstancia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConstancia = null;
    this.selectTipoPersonal = null;
    this.selectConceptoTipoPersonal = null;

    long idConceptoConstancia = 
      Long.parseLong((String)requestParameterMap.get("idConceptoConstancia"));
    try
    {
      this.conceptoConstancia = 
        this.constanciasFacade.findConceptoConstanciaById(
        idConceptoConstancia);
      if (this.conceptoConstancia.getConstancia() != null) {
        this.selectConstancia = 
          String.valueOf(this.conceptoConstancia.getConstancia().getIdConstancia());
      }
      if (this.conceptoConstancia.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.conceptoConstancia.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }

      Constancia constancia = null;
      TipoPersonal tipoPersonalForConstancia = null;
      ConceptoTipoPersonal conceptoTipoPersonal = null;
      TipoPersonal tipoPersonalForConceptoTipoPersonal = null;

      if (this.conceptoConstancia.getConstancia() != null) {
        long idConstancia = 
          this.conceptoConstancia.getConstancia().getIdConstancia();
        this.selectConstancia = String.valueOf(idConstancia);
        constancia = this.constanciasFacade.findConstanciaById(
          idConstancia);
        this.colConstancia = this.constanciasFacade.findConstanciaByTipoPersonal(
          constancia.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonal = 
          this.conceptoConstancia.getConstancia().getTipoPersonal().getIdTipoPersonal();
        this.selectTipoPersonal = String.valueOf(idTipoPersonal);
        tipoPersonalForConstancia = 
          this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
        this.colTipoPersonal = 
          this.definicionesFacade.findAllTipoPersonal();
      }
      if (this.conceptoConstancia.getConceptoTipoPersonal() != null) {
        long idConceptoTipoPersonal = 
          this.conceptoConstancia.getConceptoTipoPersonal().getIdConceptoTipoPersonal();
        this.selectConceptoTipoPersonal = String.valueOf(idConceptoTipoPersonal);
        conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(
          idConceptoTipoPersonal);
        this.colConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          conceptoTipoPersonal.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonalForConceptoTipoPersonal = 
          this.conceptoConstancia.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal();

        tipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findTipoPersonalById(
          idTipoPersonalForConceptoTipoPersonal);
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.conceptoConstancia = null;
    this.showConceptoConstanciaByConstancia = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.constanciasFacade.addConceptoConstancia(
          this.conceptoConstancia);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.constanciasFacade.updateConceptoConstancia(
          this.conceptoConstancia);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.constanciasFacade.deleteConceptoConstancia(
        this.conceptoConstancia);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.conceptoConstancia = new ConceptoConstancia();

    this.selectConstancia = null;

    this.selectTipoPersonal = null;

    this.selectConceptoTipoPersonal = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoConstancia.setIdConceptoConstancia(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.constancias.ConceptoConstancia"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.conceptoConstancia = new ConceptoConstancia();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}