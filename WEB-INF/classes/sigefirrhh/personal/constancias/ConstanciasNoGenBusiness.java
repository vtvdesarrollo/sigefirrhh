package sigefirrhh.personal.constancias;

import java.io.Serializable;
import java.util.Collection;

public class ConstanciasNoGenBusiness extends ConstanciasBusiness
  implements Serializable
{
  private ConstanciaNoGenBeanBusiness constanciaNoGenBeanBusiness = new ConstanciaNoGenBeanBusiness();

  public Collection findConstanciaByTipoPersonalAndTipo(long idTipoPersonal, String tipo) throws Exception
  {
    return this.constanciaNoGenBeanBusiness.findByTipoPersonalAndTipo(idTipoPersonal, tipo);
  }

  public void calcularConceptosProyectado(long idTrabajador, long idConstancia, String periodicidad, long idTipoPersonal) throws Exception
  {
    this.constanciaNoGenBeanBusiness.calcularConceptosProyectados(idTrabajador, idConstancia, periodicidad, idTipoPersonal);
  }
}