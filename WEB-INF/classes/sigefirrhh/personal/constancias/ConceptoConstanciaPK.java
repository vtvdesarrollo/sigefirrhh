package sigefirrhh.personal.constancias;

import java.io.Serializable;

public class ConceptoConstanciaPK
  implements Serializable
{
  public long idConceptoConstancia;

  public ConceptoConstanciaPK()
  {
  }

  public ConceptoConstanciaPK(long idConceptoConstancia)
  {
    this.idConceptoConstancia = idConceptoConstancia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoConstanciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoConstanciaPK thatPK)
  {
    return 
      this.idConceptoConstancia == thatPK.idConceptoConstancia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoConstancia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoConstancia);
  }
}