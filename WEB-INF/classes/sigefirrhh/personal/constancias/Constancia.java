package sigefirrhh.personal.constancias;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class Constancia
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  private long idConstancia;
  private TipoPersonal tipoPersonal;
  private String tipo;
  private String oficina;
  private String firmante;
  private String cargo;
  private String nombramiento;
  private String piePaginaUno;
  private String piePaginaDos;
  private String piePaginaTres;
  private String piePaginaCuatro;
  private String iniciales;
  private String observacion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cargo", "firmante", "idConstancia", "iniciales", "nombramiento", "observacion", "oficina", "piePaginaCuatro", "piePaginaDos", "piePaginaTres", "piePaginaUno", "tipo", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.constancias.Constancia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Constancia());

    LISTA_TIPO = 
      new LinkedHashMap();
    LISTA_TIPO.put("1", "ANUAL");
    LISTA_TIPO.put("2", "MENSUAL");
    LISTA_TIPO.put("3", "EGRESADO");
    LISTA_TIPO.put("4", "LPH");
    LISTA_TIPO.put("5", "SIN SUELDO");
  }

  public Constancia()
  {
    jdoSettipo(this, "1");
  }

  public String toString()
  {
    return jdoGettipoPersonal(this).getNombre() + " - " + 
      LISTA_TIPO.get(jdoGettipo(this));
  }
  public String getCargo() {
    return jdoGetcargo(this);
  }
  public void setCargo(String cargo) {
    jdoSetcargo(this, cargo);
  }
  public String getFirmante() {
    return jdoGetfirmante(this);
  }
  public void setFirmante(String firmante) {
    jdoSetfirmante(this, firmante);
  }
  public long getIdConstancia() {
    return jdoGetidConstancia(this);
  }
  public void setIdConstancia(long idConstancia) {
    jdoSetidConstancia(this, idConstancia);
  }
  public String getNombramiento() {
    return jdoGetnombramiento(this);
  }
  public void setNombramiento(String nombramiento) {
    jdoSetnombramiento(this, nombramiento);
  }
  public String getObservacion() {
    return jdoGetobservacion(this);
  }
  public void setObservacion(String observacion) {
    jdoSetobservacion(this, observacion);
  }
  public String getOficina() {
    return jdoGetoficina(this);
  }
  public void setOficina(String oficina) {
    jdoSetoficina(this, oficina);
  }
  public String getPiePaginaDos() {
    return jdoGetpiePaginaDos(this);
  }
  public void setPiePaginaDos(String piePaginaDos) {
    jdoSetpiePaginaDos(this, piePaginaDos);
  }
  public String getPiePaginaTres() {
    return jdoGetpiePaginaTres(this);
  }
  public void setPiePaginaTres(String piePaginaTres) {
    jdoSetpiePaginaTres(this, piePaginaTres);
  }
  public String getPiePaginaUno() {
    return jdoGetpiePaginaUno(this);
  }
  public void setPiePaginaUno(String piePaginaUno) {
    jdoSetpiePaginaUno(this, piePaginaUno);
  }
  public String getPiePaginaCuatro() {
    return jdoGetpiePaginaCuatro(this);
  }
  public void setPiePaginaCuatro(String piePaginaCuatro) {
    jdoSetpiePaginaCuatro(this, piePaginaCuatro);
  }
  public String getIniciales() {
    return jdoGetiniciales(this);
  }
  public void setIniciales(String iniciales) {
    jdoSetiniciales(this, iniciales);
  }
  public String getTipo() {
    return jdoGettipo(this);
  }
  public void setTipo(String tipo) {
    jdoSettipo(this, tipo);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Constancia localConstancia = new Constancia();
    localConstancia.jdoFlags = 1;
    localConstancia.jdoStateManager = paramStateManager;
    return localConstancia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Constancia localConstancia = new Constancia();
    localConstancia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConstancia.jdoFlags = 1;
    localConstancia.jdoStateManager = paramStateManager;
    return localConstancia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.firmante);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConstancia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.iniciales);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombramiento);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observacion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.oficina);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.piePaginaCuatro);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.piePaginaDos);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.piePaginaTres);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.piePaginaUno);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.firmante = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConstancia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.iniciales = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombramiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.oficina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.piePaginaCuatro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.piePaginaDos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.piePaginaTres = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.piePaginaUno = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Constancia paramConstancia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramConstancia.cargo;
      return;
    case 1:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.firmante = paramConstancia.firmante;
      return;
    case 2:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.idConstancia = paramConstancia.idConstancia;
      return;
    case 3:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.iniciales = paramConstancia.iniciales;
      return;
    case 4:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.nombramiento = paramConstancia.nombramiento;
      return;
    case 5:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.observacion = paramConstancia.observacion;
      return;
    case 6:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.oficina = paramConstancia.oficina;
      return;
    case 7:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.piePaginaCuatro = paramConstancia.piePaginaCuatro;
      return;
    case 8:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.piePaginaDos = paramConstancia.piePaginaDos;
      return;
    case 9:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.piePaginaTres = paramConstancia.piePaginaTres;
      return;
    case 10:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.piePaginaUno = paramConstancia.piePaginaUno;
      return;
    case 11:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramConstancia.tipo;
      return;
    case 12:
      if (paramConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramConstancia.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Constancia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Constancia localConstancia = (Constancia)paramObject;
    if (localConstancia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConstancia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConstanciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConstanciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConstanciaPK))
      throw new IllegalArgumentException("arg1");
    ConstanciaPK localConstanciaPK = (ConstanciaPK)paramObject;
    localConstanciaPK.idConstancia = this.idConstancia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConstanciaPK))
      throw new IllegalArgumentException("arg1");
    ConstanciaPK localConstanciaPK = (ConstanciaPK)paramObject;
    this.idConstancia = localConstanciaPK.idConstancia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConstanciaPK))
      throw new IllegalArgumentException("arg2");
    ConstanciaPK localConstanciaPK = (ConstanciaPK)paramObject;
    localConstanciaPK.idConstancia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConstanciaPK))
      throw new IllegalArgumentException("arg2");
    ConstanciaPK localConstanciaPK = (ConstanciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localConstanciaPK.idConstancia);
  }

  private static final String jdoGetcargo(Constancia paramConstancia)
  {
    if (paramConstancia.jdoFlags <= 0)
      return paramConstancia.cargo;
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.cargo;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 0))
      return paramConstancia.cargo;
    return localStateManager.getStringField(paramConstancia, jdoInheritedFieldCount + 0, paramConstancia.cargo);
  }

  private static final void jdoSetcargo(Constancia paramConstancia, String paramString)
  {
    if (paramConstancia.jdoFlags == 0)
    {
      paramConstancia.cargo = paramString;
      return;
    }
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.cargo = paramString;
      return;
    }
    localStateManager.setStringField(paramConstancia, jdoInheritedFieldCount + 0, paramConstancia.cargo, paramString);
  }

  private static final String jdoGetfirmante(Constancia paramConstancia)
  {
    if (paramConstancia.jdoFlags <= 0)
      return paramConstancia.firmante;
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.firmante;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 1))
      return paramConstancia.firmante;
    return localStateManager.getStringField(paramConstancia, jdoInheritedFieldCount + 1, paramConstancia.firmante);
  }

  private static final void jdoSetfirmante(Constancia paramConstancia, String paramString)
  {
    if (paramConstancia.jdoFlags == 0)
    {
      paramConstancia.firmante = paramString;
      return;
    }
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.firmante = paramString;
      return;
    }
    localStateManager.setStringField(paramConstancia, jdoInheritedFieldCount + 1, paramConstancia.firmante, paramString);
  }

  private static final long jdoGetidConstancia(Constancia paramConstancia)
  {
    return paramConstancia.idConstancia;
  }

  private static final void jdoSetidConstancia(Constancia paramConstancia, long paramLong)
  {
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.idConstancia = paramLong;
      return;
    }
    localStateManager.setLongField(paramConstancia, jdoInheritedFieldCount + 2, paramConstancia.idConstancia, paramLong);
  }

  private static final String jdoGetiniciales(Constancia paramConstancia)
  {
    if (paramConstancia.jdoFlags <= 0)
      return paramConstancia.iniciales;
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.iniciales;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 3))
      return paramConstancia.iniciales;
    return localStateManager.getStringField(paramConstancia, jdoInheritedFieldCount + 3, paramConstancia.iniciales);
  }

  private static final void jdoSetiniciales(Constancia paramConstancia, String paramString)
  {
    if (paramConstancia.jdoFlags == 0)
    {
      paramConstancia.iniciales = paramString;
      return;
    }
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.iniciales = paramString;
      return;
    }
    localStateManager.setStringField(paramConstancia, jdoInheritedFieldCount + 3, paramConstancia.iniciales, paramString);
  }

  private static final String jdoGetnombramiento(Constancia paramConstancia)
  {
    if (paramConstancia.jdoFlags <= 0)
      return paramConstancia.nombramiento;
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.nombramiento;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 4))
      return paramConstancia.nombramiento;
    return localStateManager.getStringField(paramConstancia, jdoInheritedFieldCount + 4, paramConstancia.nombramiento);
  }

  private static final void jdoSetnombramiento(Constancia paramConstancia, String paramString)
  {
    if (paramConstancia.jdoFlags == 0)
    {
      paramConstancia.nombramiento = paramString;
      return;
    }
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.nombramiento = paramString;
      return;
    }
    localStateManager.setStringField(paramConstancia, jdoInheritedFieldCount + 4, paramConstancia.nombramiento, paramString);
  }

  private static final String jdoGetobservacion(Constancia paramConstancia)
  {
    if (paramConstancia.jdoFlags <= 0)
      return paramConstancia.observacion;
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.observacion;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 5))
      return paramConstancia.observacion;
    return localStateManager.getStringField(paramConstancia, jdoInheritedFieldCount + 5, paramConstancia.observacion);
  }

  private static final void jdoSetobservacion(Constancia paramConstancia, String paramString)
  {
    if (paramConstancia.jdoFlags == 0)
    {
      paramConstancia.observacion = paramString;
      return;
    }
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.observacion = paramString;
      return;
    }
    localStateManager.setStringField(paramConstancia, jdoInheritedFieldCount + 5, paramConstancia.observacion, paramString);
  }

  private static final String jdoGetoficina(Constancia paramConstancia)
  {
    if (paramConstancia.jdoFlags <= 0)
      return paramConstancia.oficina;
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.oficina;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 6))
      return paramConstancia.oficina;
    return localStateManager.getStringField(paramConstancia, jdoInheritedFieldCount + 6, paramConstancia.oficina);
  }

  private static final void jdoSetoficina(Constancia paramConstancia, String paramString)
  {
    if (paramConstancia.jdoFlags == 0)
    {
      paramConstancia.oficina = paramString;
      return;
    }
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.oficina = paramString;
      return;
    }
    localStateManager.setStringField(paramConstancia, jdoInheritedFieldCount + 6, paramConstancia.oficina, paramString);
  }

  private static final String jdoGetpiePaginaCuatro(Constancia paramConstancia)
  {
    if (paramConstancia.jdoFlags <= 0)
      return paramConstancia.piePaginaCuatro;
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.piePaginaCuatro;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 7))
      return paramConstancia.piePaginaCuatro;
    return localStateManager.getStringField(paramConstancia, jdoInheritedFieldCount + 7, paramConstancia.piePaginaCuatro);
  }

  private static final void jdoSetpiePaginaCuatro(Constancia paramConstancia, String paramString)
  {
    if (paramConstancia.jdoFlags == 0)
    {
      paramConstancia.piePaginaCuatro = paramString;
      return;
    }
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.piePaginaCuatro = paramString;
      return;
    }
    localStateManager.setStringField(paramConstancia, jdoInheritedFieldCount + 7, paramConstancia.piePaginaCuatro, paramString);
  }

  private static final String jdoGetpiePaginaDos(Constancia paramConstancia)
  {
    if (paramConstancia.jdoFlags <= 0)
      return paramConstancia.piePaginaDos;
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.piePaginaDos;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 8))
      return paramConstancia.piePaginaDos;
    return localStateManager.getStringField(paramConstancia, jdoInheritedFieldCount + 8, paramConstancia.piePaginaDos);
  }

  private static final void jdoSetpiePaginaDos(Constancia paramConstancia, String paramString)
  {
    if (paramConstancia.jdoFlags == 0)
    {
      paramConstancia.piePaginaDos = paramString;
      return;
    }
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.piePaginaDos = paramString;
      return;
    }
    localStateManager.setStringField(paramConstancia, jdoInheritedFieldCount + 8, paramConstancia.piePaginaDos, paramString);
  }

  private static final String jdoGetpiePaginaTres(Constancia paramConstancia)
  {
    if (paramConstancia.jdoFlags <= 0)
      return paramConstancia.piePaginaTres;
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.piePaginaTres;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 9))
      return paramConstancia.piePaginaTres;
    return localStateManager.getStringField(paramConstancia, jdoInheritedFieldCount + 9, paramConstancia.piePaginaTres);
  }

  private static final void jdoSetpiePaginaTres(Constancia paramConstancia, String paramString)
  {
    if (paramConstancia.jdoFlags == 0)
    {
      paramConstancia.piePaginaTres = paramString;
      return;
    }
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.piePaginaTres = paramString;
      return;
    }
    localStateManager.setStringField(paramConstancia, jdoInheritedFieldCount + 9, paramConstancia.piePaginaTres, paramString);
  }

  private static final String jdoGetpiePaginaUno(Constancia paramConstancia)
  {
    if (paramConstancia.jdoFlags <= 0)
      return paramConstancia.piePaginaUno;
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.piePaginaUno;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 10))
      return paramConstancia.piePaginaUno;
    return localStateManager.getStringField(paramConstancia, jdoInheritedFieldCount + 10, paramConstancia.piePaginaUno);
  }

  private static final void jdoSetpiePaginaUno(Constancia paramConstancia, String paramString)
  {
    if (paramConstancia.jdoFlags == 0)
    {
      paramConstancia.piePaginaUno = paramString;
      return;
    }
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.piePaginaUno = paramString;
      return;
    }
    localStateManager.setStringField(paramConstancia, jdoInheritedFieldCount + 10, paramConstancia.piePaginaUno, paramString);
  }

  private static final String jdoGettipo(Constancia paramConstancia)
  {
    if (paramConstancia.jdoFlags <= 0)
      return paramConstancia.tipo;
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.tipo;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 11))
      return paramConstancia.tipo;
    return localStateManager.getStringField(paramConstancia, jdoInheritedFieldCount + 11, paramConstancia.tipo);
  }

  private static final void jdoSettipo(Constancia paramConstancia, String paramString)
  {
    if (paramConstancia.jdoFlags == 0)
    {
      paramConstancia.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramConstancia, jdoInheritedFieldCount + 11, paramConstancia.tipo, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(Constancia paramConstancia)
  {
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConstancia.tipoPersonal;
    if (localStateManager.isLoaded(paramConstancia, jdoInheritedFieldCount + 12))
      return paramConstancia.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramConstancia, jdoInheritedFieldCount + 12, paramConstancia.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(Constancia paramConstancia, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConstancia.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConstancia, jdoInheritedFieldCount + 12, paramConstancia.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}