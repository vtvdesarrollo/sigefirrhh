package sigefirrhh.personal.constancias;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;

public class ConceptoConstancia
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ORIGEN;
  private long idConceptoConstancia;
  private Constancia constancia;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private String origen;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "constancia", "idConceptoConstancia", "origen" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.constancias.Constancia"), Long.TYPE, sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 26, 26, 24, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.constancias.ConceptoConstancia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoConstancia());

    LISTA_ORIGEN = 
      new LinkedHashMap();
    LISTA_ORIGEN.put("F", "FIJO");
    LISTA_ORIGEN.put("P", "PROYECTADO");
  }

  public ConceptoConstancia()
  {
    jdoSetorigen(this, "F");
  }
  public String toString() {
    return jdoGetconstancia(this).getTipo() + " - " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion();
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal)
  {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }

  public Constancia getConstancia()
  {
    return jdoGetconstancia(this);
  }

  public void setConstancia(Constancia constancia)
  {
    jdoSetconstancia(this, constancia);
  }

  public String getOrigen() {
    return jdoGetorigen(this);
  }

  public void setOrigen(String origen)
  {
    jdoSetorigen(this, origen);
  }

  public long getIdConceptoConstancia()
  {
    return jdoGetidConceptoConstancia(this);
  }

  public void setIdConceptoConstancia(long idConceptoConstancia)
  {
    jdoSetidConceptoConstancia(this, idConceptoConstancia);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoConstancia localConceptoConstancia = new ConceptoConstancia();
    localConceptoConstancia.jdoFlags = 1;
    localConceptoConstancia.jdoStateManager = paramStateManager;
    return localConceptoConstancia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoConstancia localConceptoConstancia = new ConceptoConstancia();
    localConceptoConstancia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoConstancia.jdoFlags = 1;
    localConceptoConstancia.jdoStateManager = paramStateManager;
    return localConceptoConstancia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.constancia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoConstancia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origen);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.constancia = ((Constancia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoConstancia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origen = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoConstancia paramConceptoConstancia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoConstancia.conceptoTipoPersonal;
      return;
    case 1:
      if (paramConceptoConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.constancia = paramConceptoConstancia.constancia;
      return;
    case 2:
      if (paramConceptoConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoConstancia = paramConceptoConstancia.idConceptoConstancia;
      return;
    case 3:
      if (paramConceptoConstancia == null)
        throw new IllegalArgumentException("arg1");
      this.origen = paramConceptoConstancia.origen;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoConstancia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoConstancia localConceptoConstancia = (ConceptoConstancia)paramObject;
    if (localConceptoConstancia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoConstancia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoConstanciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoConstanciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoConstanciaPK))
      throw new IllegalArgumentException("arg1");
    ConceptoConstanciaPK localConceptoConstanciaPK = (ConceptoConstanciaPK)paramObject;
    localConceptoConstanciaPK.idConceptoConstancia = this.idConceptoConstancia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoConstanciaPK))
      throw new IllegalArgumentException("arg1");
    ConceptoConstanciaPK localConceptoConstanciaPK = (ConceptoConstanciaPK)paramObject;
    this.idConceptoConstancia = localConceptoConstanciaPK.idConceptoConstancia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoConstanciaPK))
      throw new IllegalArgumentException("arg2");
    ConceptoConstanciaPK localConceptoConstanciaPK = (ConceptoConstanciaPK)paramObject;
    localConceptoConstanciaPK.idConceptoConstancia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoConstanciaPK))
      throw new IllegalArgumentException("arg2");
    ConceptoConstanciaPK localConceptoConstanciaPK = (ConceptoConstanciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localConceptoConstanciaPK.idConceptoConstancia);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoConstancia paramConceptoConstancia)
  {
    StateManager localStateManager = paramConceptoConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoConstancia.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoConstancia, jdoInheritedFieldCount + 0))
      return paramConceptoConstancia.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoConstancia, jdoInheritedFieldCount + 0, paramConceptoConstancia.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoConstancia paramConceptoConstancia, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoConstancia.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoConstancia, jdoInheritedFieldCount + 0, paramConceptoConstancia.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final Constancia jdoGetconstancia(ConceptoConstancia paramConceptoConstancia)
  {
    StateManager localStateManager = paramConceptoConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoConstancia.constancia;
    if (localStateManager.isLoaded(paramConceptoConstancia, jdoInheritedFieldCount + 1))
      return paramConceptoConstancia.constancia;
    return (Constancia)localStateManager.getObjectField(paramConceptoConstancia, jdoInheritedFieldCount + 1, paramConceptoConstancia.constancia);
  }

  private static final void jdoSetconstancia(ConceptoConstancia paramConceptoConstancia, Constancia paramConstancia)
  {
    StateManager localStateManager = paramConceptoConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoConstancia.constancia = paramConstancia;
      return;
    }
    localStateManager.setObjectField(paramConceptoConstancia, jdoInheritedFieldCount + 1, paramConceptoConstancia.constancia, paramConstancia);
  }

  private static final long jdoGetidConceptoConstancia(ConceptoConstancia paramConceptoConstancia)
  {
    return paramConceptoConstancia.idConceptoConstancia;
  }

  private static final void jdoSetidConceptoConstancia(ConceptoConstancia paramConceptoConstancia, long paramLong)
  {
    StateManager localStateManager = paramConceptoConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoConstancia.idConceptoConstancia = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoConstancia, jdoInheritedFieldCount + 2, paramConceptoConstancia.idConceptoConstancia, paramLong);
  }

  private static final String jdoGetorigen(ConceptoConstancia paramConceptoConstancia)
  {
    if (paramConceptoConstancia.jdoFlags <= 0)
      return paramConceptoConstancia.origen;
    StateManager localStateManager = paramConceptoConstancia.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoConstancia.origen;
    if (localStateManager.isLoaded(paramConceptoConstancia, jdoInheritedFieldCount + 3))
      return paramConceptoConstancia.origen;
    return localStateManager.getStringField(paramConceptoConstancia, jdoInheritedFieldCount + 3, paramConceptoConstancia.origen);
  }

  private static final void jdoSetorigen(ConceptoConstancia paramConceptoConstancia, String paramString)
  {
    if (paramConceptoConstancia.jdoFlags == 0)
    {
      paramConceptoConstancia.origen = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoConstancia.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoConstancia.origen = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoConstancia, jdoInheritedFieldCount + 3, paramConceptoConstancia.origen, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}