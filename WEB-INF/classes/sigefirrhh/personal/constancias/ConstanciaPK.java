package sigefirrhh.personal.constancias;

import java.io.Serializable;

public class ConstanciaPK
  implements Serializable
{
  public long idConstancia;

  public ConstanciaPK()
  {
  }

  public ConstanciaPK(long idConstancia)
  {
    this.idConstancia = idConstancia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConstanciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConstanciaPK thatPK)
  {
    return 
      this.idConstancia == thatPK.idConstancia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConstancia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConstancia);
  }
}