package sigefirrhh.personal.constancias;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ConstanciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConstancia(Constancia constancia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Constancia constanciaNew = 
      (Constancia)BeanUtils.cloneBean(
      constancia);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (constanciaNew.getTipoPersonal() != null) {
      constanciaNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        constanciaNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(constanciaNew);
  }

  public void updateConstancia(Constancia constancia) throws Exception
  {
    Constancia constanciaModify = 
      findConstanciaById(constancia.getIdConstancia());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (constancia.getTipoPersonal() != null) {
      constancia.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        constancia.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(constanciaModify, constancia);
  }

  public void deleteConstancia(Constancia constancia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Constancia constanciaDelete = 
      findConstanciaById(constancia.getIdConstancia());
    pm.deletePersistent(constanciaDelete);
  }

  public Constancia findConstanciaById(long idConstancia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConstancia == pIdConstancia";
    Query query = pm.newQuery(Constancia.class, filter);

    query.declareParameters("long pIdConstancia");

    parameters.put("pIdConstancia", new Long(idConstancia));

    Collection colConstancia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConstancia.iterator();
    return (Constancia)iterator.next();
  }

  public Collection findConstanciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent constanciaExtent = pm.getExtent(
      Constancia.class, true);
    Query query = pm.newQuery(constanciaExtent);
    query.setOrdering("tipoPersonal.codTipoPersonal ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(Constancia.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("tipoPersonal.codTipoPersonal ascending");

    Collection colConstancia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConstancia);

    return colConstancia;
  }
}