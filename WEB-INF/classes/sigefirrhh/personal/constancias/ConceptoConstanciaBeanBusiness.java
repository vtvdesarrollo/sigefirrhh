package sigefirrhh.personal.constancias;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;

public class ConceptoConstanciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoConstancia(ConceptoConstancia conceptoConstancia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoConstancia conceptoConstanciaNew = 
      (ConceptoConstancia)BeanUtils.cloneBean(
      conceptoConstancia);

    ConstanciaBeanBusiness constanciaBeanBusiness = new ConstanciaBeanBusiness();

    if (conceptoConstanciaNew.getConstancia() != null) {
      conceptoConstanciaNew.setConstancia(
        constanciaBeanBusiness.findConstanciaById(
        conceptoConstanciaNew.getConstancia().getIdConstancia()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoConstanciaNew.getConceptoTipoPersonal() != null) {
      conceptoConstanciaNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoConstanciaNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }
    pm.makePersistent(conceptoConstanciaNew);
  }

  public void updateConceptoConstancia(ConceptoConstancia conceptoConstancia) throws Exception
  {
    ConceptoConstancia conceptoConstanciaModify = 
      findConceptoConstanciaById(conceptoConstancia.getIdConceptoConstancia());

    ConstanciaBeanBusiness constanciaBeanBusiness = new ConstanciaBeanBusiness();

    if (conceptoConstancia.getConstancia() != null) {
      conceptoConstancia.setConstancia(
        constanciaBeanBusiness.findConstanciaById(
        conceptoConstancia.getConstancia().getIdConstancia()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoConstancia.getConceptoTipoPersonal() != null) {
      conceptoConstancia.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoConstancia.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    BeanUtils.copyProperties(conceptoConstanciaModify, conceptoConstancia);
  }

  public void deleteConceptoConstancia(ConceptoConstancia conceptoConstancia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoConstancia conceptoConstanciaDelete = 
      findConceptoConstanciaById(conceptoConstancia.getIdConceptoConstancia());
    pm.deletePersistent(conceptoConstanciaDelete);
  }

  public ConceptoConstancia findConceptoConstanciaById(long idConceptoConstancia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoConstancia == pIdConceptoConstancia";
    Query query = pm.newQuery(ConceptoConstancia.class, filter);

    query.declareParameters("long pIdConceptoConstancia");

    parameters.put("pIdConceptoConstancia", new Long(idConceptoConstancia));

    Collection colConceptoConstancia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoConstancia.iterator();
    return (ConceptoConstancia)iterator.next();
  }

  public Collection findConceptoConstanciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoConstanciaExtent = pm.getExtent(
      ConceptoConstancia.class, true);
    Query query = pm.newQuery(conceptoConstanciaExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByConstancia(long idConstancia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "constancia.idConstancia == pIdConstancia";

    Query query = pm.newQuery(ConceptoConstancia.class, filter);

    query.declareParameters("long pIdConstancia");
    HashMap parameters = new HashMap();

    parameters.put("pIdConstancia", new Long(idConstancia));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoConstancia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoConstancia);

    return colConceptoConstancia;
  }
}