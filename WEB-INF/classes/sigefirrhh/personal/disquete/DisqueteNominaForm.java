package sigefirrhh.personal.disquete;

import eforserver.common.Resource;
import eforserver.presentation.ListUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.Disquete;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ReportRelacionCargosForm;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.procesoNomina.ProcesoNominaNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;

public class DisqueteNominaForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportRelacionCargosForm.class.getName());
  private boolean showNominaEspecial;
  private String tipoNomina = "O";
  private boolean generado;
  private String urlArchivo;
  private NominaEspecial nominaEspecial;
  private long idNominaEspecial;
  private Collection listNominaEspecial;
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private String selectNominaEspecial;
  private String selectGrupoNomina;
  private String selectDisquete;
  private String selectRegion;
  private String selectUnidadAdministradora;
  private String inicio;
  private String fin;
  private Date fechaProceso = new Date();
  private Date fechaAbono = new Date();
  private Disquete disquete;
  private Calendar inicioAux;
  private Calendar finAux;
  private String numeroOrdenPago;
  private long idGrupoNomina;
  private long idUnidadAdministradora;
  private long idRegion;
  private int numeroNomina;
  private Collection listGrupoNomina;
  private Collection listDisquete;
  private Collection listRegion;
  private Collection listUnidadAdministradora;
  private String periodicidad;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private Integer semanaMes;
  private Integer lunesPrQuincena;
  private Integer lunesSeQuincena;
  private Boolean tieneSemana5;
  private Integer semanaAnio;
  private boolean show;
  private boolean auxShow = true;

  public DisqueteNominaForm() {
    this.reportName = "nominarac";

    this.inicio = null;
    this.fin = null;

    this.auxShow = true;
    this.inicioAux = Calendar.getInstance();
    this.finAux = Calendar.getInstance();
    this.lunesPrQuincena = new Integer(0);
    this.lunesSeQuincena = new Integer(0);
    this.tieneSemana5 = new Boolean(false);
    this.semanaMes = new Integer(0);
    this.semanaAnio = new Integer(0);
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void changeGrupoNomina(ValueChangeEvent event) {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    this.showNominaEspecial = false;
    this.auxShow = true;
    try {
      if (this.idGrupoNomina != 0L)
        if (this.tipoNomina.equals("O"))
          buscarNomina(this.idGrupoNomina);
        else
          llenarNominaEspecial(this.idGrupoNomina);
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  private void buscarNomina(long idGrupoNomina) {
    try {
      Collection colPeriodo = this.procesoNominaNoGenFacade.findFechaUltimaNomina(idGrupoNomina);
      Iterator iteratorPeriodo = colPeriodo.iterator();
      if (iteratorPeriodo.hasNext()) {
        this.inicioAux = ((Calendar)iteratorPeriodo.next());
        this.finAux = ((Calendar)iteratorPeriodo.next());
        this.lunesPrQuincena = ((Integer)iteratorPeriodo.next());
        this.lunesSeQuincena = ((Integer)iteratorPeriodo.next());
        this.tieneSemana5 = ((Boolean)iteratorPeriodo.next());
        this.semanaMes = ((Integer)iteratorPeriodo.next());
        this.semanaAnio = ((Integer)iteratorPeriodo.next());
      }
      this.auxShow = true;
    } catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  private void llenarNominaEspecial(long idGrupoNomina) {
    try { this.listNominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialByGrupoNominaAndEstatus(idGrupoNomina, "P");
      this.showNominaEspecial = true;
    } catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeNominaEspecial(ValueChangeEvent event)
  {
    this.idNominaEspecial = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if (this.idGrupoNomina != 0L) {
        this.nominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialById(this.idNominaEspecial);

        this.inicioAux.setTime(this.nominaEspecial.getFechaInicio());
        this.finAux.setTime(this.nominaEspecial.getFechaFin());
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoNomina(ValueChangeEvent event)
  {
    String tipoNomina = (String)event.getNewValue();
    try {
      this.auxShow = true;
      this.showNominaEspecial = false;

      if (this.idGrupoNomina != 0L) {
        if (tipoNomina.equals("O"))
          buscarNomina(this.idGrupoNomina);
        else {
          llenarNominaEspecial(this.idGrupoNomina);
        }

      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void prueba()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
    try
    {
      File file = new File(request.getRealPath("temp") + "/disquetnomina.txt");
      file.createNewFile();

      FileWriter writer = new FileWriter(file);

      writer.write("prueba1   111 \n");
      writer.write("prueba2");
      writer.flush();
      writer.close();
    }
    catch (Exception localException)
    {
    }
  }

  public void generar() {
    this.numeroNomina = 0;
    this.idUnidadAdministradora = Long.valueOf(String.valueOf(this.selectUnidadAdministradora)).longValue();
    this.idRegion = Long.valueOf(String.valueOf(this.selectRegion)).longValue();
    long idBanco = this.disquete.getBanco().getIdBanco();
    if (this.tipoNomina.equals("E")) {
      this.numeroNomina = this.nominaEspecial.getNumeroNomina();
    }
    this.generado = false;
    int contador = 0;
    int tope = 0;

    log.warn("numero nomina" + this.numeroNomina);
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();

    ResultSet rsEncabezado = null;
    PreparedStatement stEncabezado = null;

    ResultSet rsEncabezado2 = null;
    PreparedStatement stEncabezado2 = null;

    ResultSet rsEncabezado3 = null;
    PreparedStatement stEncabezado3 = null;

    ResultSet rsDetalle = null;
    PreparedStatement stDetalle = null;

    ResultSet rsTotal = null;
    PreparedStatement stTotal = null;

    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;

    ResultSet rsTotalNominaTrabajadores = null;
    PreparedStatement stTotalNominaTrabajadores = null;

    StringBuffer sqlTotalNominaTrab = new StringBuffer();

    StringBuffer valorBuffer = new StringBuffer();

    Connection connection = Resource.getConnection();
    try {
      Random rmd = new Random();
      rmd.nextDouble();

      boolean success = new File(request.getRealPath("disquetes")).mkdir();

      File file = new File(request.getRealPath("disquetes") + "/" + rmd.nextDouble() + ".txt");
      file.createNewFile();

      OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), Charset.forName("ISO-8859-1"));

      Statement stActualizar = null;
      stActualizar = connection.createStatement();
      StringBuffer sql = new StringBuffer();
      sql.append("update banco set correlativo = correlativo + 1 where id_banco = " + idBanco);
      stActualizar.addBatch(sql.toString());
      stActualizar.executeBatch();

      if ((this.idRegion == 0L) && (this.idUnidadAdministradora == 0L))
      {
        sqlTotalNominaTrab.append("select count(distinct un.id_trabajador) as total, (sum(monto_asigna) - sum(monto_deduce)) as neto, min(correlativo) as correlativo ");
        sqlTotalNominaTrab.append(" from ultimanomina un, trabajador t, banco b ");
        sqlTotalNominaTrab.append(" where un.id_trabajador = t.id_trabajador");
        sqlTotalNominaTrab.append(" and t.id_banco_nomina = b.id_banco");
        sqlTotalNominaTrab.append(" and un.forma_pago = '1'");
        if (!this.selectGrupoNomina.equals("0")) {
          sqlTotalNominaTrab.append(" and id_grupo_nomina = ? ");
        }
        sqlTotalNominaTrab.append(" and t.id_banco_nomina = ? ");
        sqlTotalNominaTrab.append(" and numero_nomina = ?");
        stTotalNominaTrabajadores = connection.prepareStatement(
          sqlTotalNominaTrab.toString(), 
          1000, 
          1007);
        if (!this.selectGrupoNomina.equals("0")) {
          stTotalNominaTrabajadores.setLong(1, this.idGrupoNomina);
          stTotalNominaTrabajadores.setLong(2, idBanco);
          stTotalNominaTrabajadores.setLong(3, this.numeroNomina);
          rsTotalNominaTrabajadores = stTotalNominaTrabajadores.executeQuery();
        } else {
          stTotalNominaTrabajadores.setLong(1, idBanco);
          stTotalNominaTrabajadores.setLong(2, this.numeroNomina);
          rsTotalNominaTrabajadores = stTotalNominaTrabajadores.executeQuery();
        }
      }
      else if ((this.idRegion != 0L) && (this.idUnidadAdministradora != 0L))
      {
        sqlTotalNominaTrab.append("select count(distinct un.id_trabajador) as total, (sum(monto_asigna) - sum(monto_deduce)) as neto, min(correlativo) as correlativo ");
        sqlTotalNominaTrab.append(" from ultimanomina un, trabajador t, banco b, ");
        sqlTotalNominaTrab.append(" dependencia d, sede s, administradorauel au ");
        sqlTotalNominaTrab.append(" where un.id_trabajador = t.id_trabajador");
        sqlTotalNominaTrab.append(" and t.id_banco_nomina = b.id_banco");
        sqlTotalNominaTrab.append(" and t.id_dependencia = d.id_dependencia");
        sqlTotalNominaTrab.append(" and d.id_sede = s.id_sede");
        sqlTotalNominaTrab.append(" and d.id_administradora_uel = au.id_administradora_uel");
        sqlTotalNominaTrab.append(" and un.forma_pago = '1'");
        if (!this.selectGrupoNomina.equals("0")) {
          sqlTotalNominaTrab.append(" and id_grupo_nomina = ? ");
        }
        sqlTotalNominaTrab.append(" and s.id_region = ?");
        sqlTotalNominaTrab.append(" and au.id_unidad_administradora = ?");
        sqlTotalNominaTrab.append(" and t.id_banco_nomina = ? ");
        sqlTotalNominaTrab.append(" and numero_nomina = ?");
        stTotalNominaTrabajadores = connection.prepareStatement(
          sqlTotalNominaTrab.toString(), 
          1000, 
          1007);
        if (!this.selectGrupoNomina.equals("0")) {
          stTotalNominaTrabajadores.setLong(1, this.idGrupoNomina);
          stTotalNominaTrabajadores.setLong(2, this.idRegion);
          stTotalNominaTrabajadores.setLong(3, this.idUnidadAdministradora);
          stTotalNominaTrabajadores.setLong(4, idBanco);
          stTotalNominaTrabajadores.setLong(5, this.numeroNomina);
        } else {
          stTotalNominaTrabajadores.setLong(1, this.idRegion);
          stTotalNominaTrabajadores.setLong(2, this.idUnidadAdministradora);
          stTotalNominaTrabajadores.setLong(3, idBanco);
          stTotalNominaTrabajadores.setLong(4, this.numeroNomina);
        }
        rsTotalNominaTrabajadores = stTotalNominaTrabajadores.executeQuery();
      }
      else if ((this.idRegion == 0L) && (this.idUnidadAdministradora != 0L))
      {
        sqlTotalNominaTrab.append("select count(distinct un.id_trabajador) as total, (sum(monto_asigna) - sum(monto_deduce)) as neto, min(correlativo) as correlativo ");
        sqlTotalNominaTrab.append(" from ultimanomina un, trabajador t, banco b, ");
        sqlTotalNominaTrab.append(" dependencia d, administradorauel au ");
        sqlTotalNominaTrab.append(" where un.id_trabajador = t.id_trabajador");
        sqlTotalNominaTrab.append(" and t.id_banco_nomina = b.id_banco");
        sqlTotalNominaTrab.append(" and t.id_dependencia = d.id_dependencia");
        sqlTotalNominaTrab.append(" and d.id_administradora_uel = au.id_administradora_uel");
        sqlTotalNominaTrab.append(" and un.forma_pago = '1'");
        if (!this.selectGrupoNomina.equals("0")) {
          sqlTotalNominaTrab.append(" and id_grupo_nomina = ? ");
        }
        sqlTotalNominaTrab.append(" and au.id_unidad_administradora = ?");
        sqlTotalNominaTrab.append(" and t.id_banco_nomina = ? ");
        sqlTotalNominaTrab.append(" and numero_nomina = ?");
        stTotalNominaTrabajadores = connection.prepareStatement(
          sqlTotalNominaTrab.toString(), 
          1000, 
          1007);
        if (!this.selectGrupoNomina.equals("0")) {
          stTotalNominaTrabajadores.setLong(1, this.idGrupoNomina);
          stTotalNominaTrabajadores.setLong(2, this.idUnidadAdministradora);
          stTotalNominaTrabajadores.setLong(3, idBanco);
          stTotalNominaTrabajadores.setLong(4, this.numeroNomina);
          rsTotalNominaTrabajadores = stTotalNominaTrabajadores.executeQuery();
        } else {
          stTotalNominaTrabajadores.setLong(1, this.idUnidadAdministradora);
          stTotalNominaTrabajadores.setLong(2, idBanco);
          stTotalNominaTrabajadores.setLong(3, this.numeroNomina);
          rsTotalNominaTrabajadores = stTotalNominaTrabajadores.executeQuery();
        }
      }
      else if ((this.idRegion != 0L) && (this.idUnidadAdministradora == 0L))
      {
        sqlTotalNominaTrab.append("select count(distinct un.id_trabajador) as total, (sum(monto_asigna) - sum(monto_deduce)) as neto, min(correlativo) as correlativo ");
        sqlTotalNominaTrab.append(" from ultimanomina un, trabajador t, banco b, ");
        sqlTotalNominaTrab.append(" dependencia d, sede s ");
        sqlTotalNominaTrab.append(" where un.id_trabajador = t.id_trabajador");
        sqlTotalNominaTrab.append(" and t.id_banco_nomina = b.id_banco");
        sqlTotalNominaTrab.append(" and t.id_dependencia = d.id_dependencia");
        sqlTotalNominaTrab.append(" and d.id_sede = s.id_sede");
        sqlTotalNominaTrab.append(" and un.forma_pago = '1'");
        if (!this.selectGrupoNomina.equals("0")) {
          sqlTotalNominaTrab.append(" and id_grupo_nomina = ? ");
        }
        sqlTotalNominaTrab.append(" and s.id_region = ?");
        sqlTotalNominaTrab.append(" and t.id_banco_nomina = ? ");
        sqlTotalNominaTrab.append(" and numero_nomina = ?");
        stTotalNominaTrabajadores = connection.prepareStatement(
          sqlTotalNominaTrab.toString(), 
          1000, 
          1007);
        if (!this.selectGrupoNomina.equals("0")) {
          stTotalNominaTrabajadores.setLong(1, this.idGrupoNomina);
          stTotalNominaTrabajadores.setLong(2, this.idRegion);
          stTotalNominaTrabajadores.setLong(3, idBanco);
          stTotalNominaTrabajadores.setLong(4, this.numeroNomina);
          rsTotalNominaTrabajadores = stTotalNominaTrabajadores.executeQuery();
        } else {
          stTotalNominaTrabajadores.setLong(1, this.idRegion);
          stTotalNominaTrabajadores.setLong(2, idBanco);
          stTotalNominaTrabajadores.setLong(3, this.numeroNomina);
          rsTotalNominaTrabajadores = stTotalNominaTrabajadores.executeQuery();
        }
      }

      if ((this.idRegion == 0L) && (this.idUnidadAdministradora == 0L)) {
        sql = new StringBuffer();
        sql.append("select sum(monto_asigna)-sum(monto_deduce) as neto, ");
        sql.append(" max(t.cedula) as cedula , ");
        sql.append(" max(p.primer_nombre) as primer_nombre,");
        sql.append(" max(p.segundo_nombre) as segundo_nombre,");
        sql.append(" max(p.primer_apellido) as primer_apellido,");
        sql.append(" max(p.segundo_apellido) as segundo_apellido,");
        sql.append(" max(p.nacionalidad) as nacionalidad,");
        sql.append(" max(p.email) as e_mail,");
        sql.append(" max(b.identificador_ahorro) as ahorro,");
        sql.append(" max(b.identificador_corriente) as corriente,");
        sql.append(" max(t.tipo_cta_nomina) as tipo_cuenta,");
        sql.append(" max(t.sueldo_basico) as sueldo_basico,");
        sql.append(" max(t.cuenta_nomina) as cuenta, max(t.codigo_nomina) as codigo_nomina");
        sql.append(" from ultimanomina un, trabajador t, personal p, banco b");
        sql.append(" where un.id_trabajador = t.id_trabajador");
        sql.append(" and t.id_banco_nomina = b.id_banco");
        sql.append(" and t.id_personal = p.id_personal");
        sql.append(" and un.forma_pago = '1'");
        if (!this.selectGrupoNomina.equals("0")) {
          sql.append(" and un.id_grupo_nomina = ?");
        }
        sql.append(" and t.id_banco_nomina = ?");
        sql.append(" and un.numero_nomina = ?");
        sql.append(" group by t.id_trabajador");
        sql.append(" order by max(t.cedula)");

        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        if (!this.selectGrupoNomina.equals("0")) {
          stTrabajadores.setLong(1, this.idGrupoNomina);
          stTrabajadores.setLong(2, idBanco);
          stTrabajadores.setLong(3, this.numeroNomina);
        } else {
          stTrabajadores.setLong(1, idBanco);
          stTrabajadores.setLong(2, this.numeroNomina);
        }
        rsTrabajadores = stTrabajadores.executeQuery();
      }
      else if ((this.idRegion != 0L) && (this.idUnidadAdministradora != 0L)) {
        sql = new StringBuffer();
        sql.append("select sum(monto_asigna)-sum(monto_deduce) as neto, ");
        sql.append(" max(t.cedula) as cedula , ");
        sql.append(" max(p.primer_nombre) as primer_nombre,");
        sql.append(" max(p.segundo_nombre) as segundo_nombre,");
        sql.append(" max(p.primer_apellido) as primer_apellido,");
        sql.append(" max(p.segundo_apellido) as segundo_apellido,");
        sql.append(" max(p.nacionalidad) as nacionalidad,");
        sql.append(" max(p.email) as e_mail,");
        sql.append(" max(b.identificador_ahorro) as ahorro,");
        sql.append(" max(b.identificador_corriente) as corriente,");
        sql.append(" max(t.tipo_cta_nomina) as tipo_cuenta,");
        sql.append(" max(t.sueldo_basico) as sueldo_basico,");
        sql.append(" max(t.cuenta_nomina) as cuenta, max(t.codigo_nomina) as codigo_nomina");
        sql.append(" from ultimanomina un, trabajador t, personal p, banco b, ");
        sql.append(" dependencia d, sede s, administradorauel au ");
        sql.append(" where un.id_trabajador = t.id_trabajador");
        sql.append(" and t.id_banco_nomina = b.id_banco");
        sql.append(" and t.id_personal = p.id_personal");
        sql.append(" and t.id_dependencia = d.id_dependencia");
        sql.append(" and d.id_sede = s.id_sede");
        sql.append(" and d.id_administradora_uel = au.id_administradora_uel");
        sql.append(" and un.forma_pago = '1'");
        if (!this.selectGrupoNomina.equals("0")) {
          sql.append(" and un.id_grupo_nomina = ?");
        }
        sql.append(" and s.id_region = ?");
        sql.append(" and au.id_unidad_administradora = ?");
        sql.append(" and t.id_banco_nomina = ?");
        sql.append(" and un.numero_nomina = ?");
        sql.append(" group by t.id_trabajador");
        sql.append(" order by max(t.cedula)");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        if (!this.selectGrupoNomina.equals("0")) {
          stTrabajadores.setLong(1, this.idGrupoNomina);
          stTrabajadores.setLong(2, this.idRegion);
          stTrabajadores.setLong(3, this.idUnidadAdministradora);
          stTrabajadores.setLong(4, idBanco);
          stTrabajadores.setLong(5, this.numeroNomina);
        } else {
          stTrabajadores.setLong(1, this.idRegion);
          stTrabajadores.setLong(2, this.idUnidadAdministradora);
          stTrabajadores.setLong(3, idBanco);
          stTrabajadores.setLong(4, this.numeroNomina);
        }
        rsTrabajadores = stTrabajadores.executeQuery();
      }
      else if ((this.idRegion == 0L) && (this.idUnidadAdministradora != 0L)) {
        sql = new StringBuffer();
        sql.append("select sum(monto_asigna)-sum(monto_deduce) as neto, ");
        sql.append(" max(t.cedula) as cedula , ");
        sql.append(" max(p.primer_nombre) as primer_nombre,");
        sql.append(" max(p.segundo_nombre) as segundo_nombre,");
        sql.append(" max(p.primer_apellido) as primer_apellido,");
        sql.append(" max(p.segundo_apellido) as segundo_apellido,");
        sql.append(" max(p.nacionalidad) as nacionalidad,");
        sql.append(" max(p.email) as e_mail,");
        sql.append(" max(b.identificador_ahorro) as ahorro,");
        sql.append(" max(b.identificador_corriente) as corriente,");
        sql.append(" max(t.tipo_cta_nomina) as tipo_cuenta,");
        sql.append(" max(t.sueldo_basico) as sueldo_basico,");
        sql.append(" max(t.cuenta_nomina) as cuenta, max(t.codigo_nomina) as codigo_nomina");
        sql.append(" from ultimanomina un, trabajador t, personal p, banco b, ");
        sql.append(" dependencia d, administradorauel au ");
        sql.append(" where un.id_trabajador = t.id_trabajador");
        sql.append(" and t.id_banco_nomina = b.id_banco");
        sql.append(" and t.id_personal = p.id_personal");
        sql.append(" and t.id_dependencia = d.id_dependencia");
        sql.append(" and d.id_administradora_uel = au.id_administradora_uel");
        sql.append(" and un.forma_pago = '1'");
        if (!this.selectGrupoNomina.equals("0")) {
          sql.append(" and un.id_grupo_nomina = ?");
        }
        sql.append(" and au.id_unidad_administradora = ?");
        sql.append(" and t.id_banco_nomina = ?");
        sql.append(" and un.numero_nomina = ?");
        sql.append(" group by t.id_trabajador");
        sql.append(" order by max(t.cedula)");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        if (!this.selectGrupoNomina.equals("0")) {
          stTrabajadores.setLong(1, this.idGrupoNomina);
          stTrabajadores.setLong(2, this.idUnidadAdministradora);
          stTrabajadores.setLong(3, idBanco);
          stTrabajadores.setLong(4, this.numeroNomina);
        } else {
          stTrabajadores.setLong(1, this.idUnidadAdministradora);
          stTrabajadores.setLong(2, idBanco);
          stTrabajadores.setLong(3, this.numeroNomina);
        }
        rsTrabajadores = stTrabajadores.executeQuery();
      }
      else if ((this.idRegion != 0L) && (this.idUnidadAdministradora == 0L)) {
        sql = new StringBuffer();
        sql.append("select sum(monto_asigna)-sum(monto_deduce) as neto, ");
        sql.append(" max(t.cedula) as cedula , ");
        sql.append(" max(p.primer_nombre) as primer_nombre,");
        sql.append(" max(p.segundo_nombre) as segundo_nombre,");
        sql.append(" max(p.primer_apellido) as primer_apellido,");
        sql.append(" max(p.segundo_apellido) as segundo_apellido,");
        sql.append(" max(p.nacionalidad) as nacionalidad,");
        sql.append(" max(p.email) as e_mail,");
        sql.append(" max(b.identificador_ahorro) as ahorro,");
        sql.append(" max(b.identificador_corriente) as corriente,");
        sql.append(" max(t.tipo_cta_nomina) as tipo_cuenta,");
        sql.append(" max(t.sueldo_basico) as sueldo_basico,");
        sql.append(" max(t.cuenta_nomina) as cuenta, max(t.codigo_nomina) as codigo_nomina");
        sql.append(" from ultimanomina un, trabajador t, personal p, banco b, ");
        sql.append(" dependencia d, sede s ");
        sql.append(" where un.id_trabajador = t.id_trabajador");
        sql.append(" and t.id_banco_nomina = b.id_banco");
        sql.append(" and t.id_personal = p.id_personal");
        sql.append(" and t.id_dependencia = d.id_dependencia");
        sql.append(" and d.id_sede = s.id_sede");
        sql.append(" and un.forma_pago = '1'");
        if (!this.selectGrupoNomina.equals("0")) {
          sql.append(" and un.id_grupo_nomina = ?");
        }
        sql.append(" and s.id_region = ?");
        sql.append(" and t.id_banco_nomina = ?");
        sql.append(" and un.numero_nomina = ?");
        sql.append(" group by t.id_trabajador");
        sql.append(" order by max(t.cedula)");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        if (!this.selectGrupoNomina.equals("0")) {
          stTrabajadores.setLong(1, this.idGrupoNomina);
          stTrabajadores.setLong(2, this.idRegion);
          stTrabajadores.setLong(3, idBanco);
          stTrabajadores.setLong(4, this.numeroNomina);
        } else {
          stTrabajadores.setLong(1, this.idRegion);
          stTrabajadores.setLong(2, idBanco);
          stTrabajadores.setLong(3, this.numeroNomina);
        }
        rsTrabajadores = stTrabajadores.executeQuery();
      }

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '1' order by numero_campo");
      stEncabezado = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stEncabezado.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsEncabezado = stEncabezado.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '4' order by numero_campo");
      stEncabezado2 = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stEncabezado2.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsEncabezado2 = stEncabezado2.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '5' order by numero_campo");
      stEncabezado3 = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stEncabezado3.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsEncabezado3 = stEncabezado3.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '2' order by numero_campo");
      stDetalle = connection.prepareStatement(
        sql.toString(), 
        1000, 
        1007);
      stDetalle.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsDetalle = stDetalle.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '3' order by numero_campo");
      stTotal = connection.prepareStatement(
        sql.toString(), 
        1000, 
        1007);
      stTotal.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsTotal = stTotal.executeQuery();

      boolean hayEncabezado = false;

      hayEncabezado = ToolsDisquete.armaEncabezado(rsEncabezado, rsTotalNominaTrabajadores, writer, 1, hayEncabezado, this.fechaProceso, this.fechaAbono, this.numeroOrdenPago, 0, 0);

      hayEncabezado = ToolsDisquete.armaEncabezado(rsEncabezado2, rsTotalNominaTrabajadores, writer, 2, hayEncabezado, this.fechaProceso, this.fechaAbono, this.numeroOrdenPago, 0, 0);

      hayEncabezado = ToolsDisquete.armaEncabezado(rsEncabezado3, rsTotalNominaTrabajadores, writer, 3, hayEncabezado, this.fechaProceso, this.fechaAbono, this.numeroOrdenPago, 0, 0);

      hayEncabezado = ToolsDisquete.armaDetalle(null, rsDetalle, rsTrabajadores, writer, 1, hayEncabezado, this.fechaProceso, this.fechaAbono, this.numeroOrdenPago, 0, 0);

      hayEncabezado = ToolsDisquete.armaTotal(rsTotal, rsTotalNominaTrabajadores, writer, hayEncabezado, this.fechaProceso, this.fechaAbono, this.numeroOrdenPago, 0, 0);

      writer.flush();
      writer.close();
      this.generado = true;

      this.urlArchivo = ("/sigefirrhh/disquetes/" + file.getName());
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      this.listDisquete = this.definicionesFacade.findDisqueteByTipoDisquete("N", this.login.getIdOrganismo());
      this.listRegion = this.estructuraFacade.findAllRegion();
      this.listUnidadAdministradora = this.estructuraFacade.findAllUnidadAdministradora();
      log.error("vacio: " + this.listGrupoNomina.isEmpty());
    } catch (Exception e) {
      this.listGrupoNomina = new ArrayList();
    }
  }

  public Collection getListGrupoNomina() {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getListDisquete() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listDisquete, "sigefirrhh.base.definiciones.Disquete");
  }
  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }
  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }
  public Collection getListNominaEspecial() {
    if ((this.listNominaEspecial != null) && (!this.listNominaEspecial.isEmpty())) {
      return ListUtil.convertCollectionToSelectItemsWithId(
        this.listNominaEspecial, "sigefirrhh.personal.procesoNomina.NominaEspecial");
    }
    return null;
  }

  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String string) {
    this.selectGrupoNomina = string;
  }
  public String getFin() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }
  public void setFin(String string) {
    this.fin = string;
  }
  public void setInicio(String string) {
    this.inicio = string;
  }
  public boolean isShow() {
    return this.auxShow;
  }
  public int getTipoReporte() {
    return this.tipoReporte;
  }
  public void setTipoReporte(int i) {
    this.tipoReporte = i;
  }
  public long getIdGrupoNomina() {
    return this.idGrupoNomina;
  }
  public LoginSession getLogin() {
    return this.login;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setIdGrupoNomina(long l) {
    this.idGrupoNomina = l;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }
  public String getSelectDisquete() {
    return this.selectDisquete;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }
  public String getSelectUnidadAdministradora() {
    return this.selectUnidadAdministradora;
  }
  public void setSelectDisquete(String string) {
    this.selectDisquete = string;
    try {
      this.disquete = this.definicionesFacade.findDisqueteById(Long.valueOf(this.selectDisquete).longValue());
    } catch (Exception localException) {
    }
  }

  public void setSelectRegion(String string) {
    this.selectRegion = string;
  }
  public void setSelectUnidadAdministradora(String string) {
    this.selectUnidadAdministradora = string;
  }
  public Date getFechaAbono() {
    return this.fechaAbono;
  }
  public Date getFechaProceso() {
    return this.fechaProceso;
  }
  public void setFechaAbono(Date date) {
    this.fechaAbono = date;
  }
  public void setFechaProceso(Date date) {
    this.fechaProceso = date;
  }
  public boolean isGenerado() {
    return this.generado;
  }
  public String getUrlArchivo() {
    return this.urlArchivo;
  }

  public String getTipoNomina() {
    return this.tipoNomina;
  }
  public void setTipoNomina(String string) {
    this.tipoNomina = string;
  }
  public boolean isShowNominaEspecial() {
    return this.showNominaEspecial;
  }

  public String getSelectNominaEspecial() {
    return this.selectNominaEspecial;
  }

  public void setSelectNominaEspecial(String string) {
    this.selectNominaEspecial = string;
  }

  public int getNumeroNomina() {
    return this.numeroNomina;
  }
  public void setNumeroNomina(int numeroNomina) {
    this.numeroNomina = numeroNomina;
  }
  public String getNumeroOrdenPago() {
    return this.numeroOrdenPago;
  }
  public void setNumeroOrdenPago(String numeroOrdenPago) {
    this.numeroOrdenPago = numeroOrdenPago;
  }
}