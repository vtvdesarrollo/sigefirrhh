package sigefirrhh.personal.disquete;

import eforserver.common.Resource;
import eforserver.presentation.ListUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.Disquete;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.bienestar.ticketAlimentacion.SeguridadTicket;
import sigefirrhh.bienestar.ticketAlimentacion.TicketAlimentacionFacadeExtend;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class DisqueteTicketForm
  implements Serializable
{
  static Logger log = Logger.getLogger(DisqueteTicketForm.class.getName());
  private boolean showEspecial;
  private boolean generado;
  private String urlArchivo;
  private int reportId;
  private String tipoProceso = "N";
  private String selectGrupoTicket;
  private String selectDisquete;
  private String selectRegion;
  private String selectUnidadAdministradora;
  private java.util.Date fechaProceso = new java.util.Date();
  private java.util.Date fechaAbono = new java.util.Date();
  private Disquete disquete;
  private String numeroOrdenPago;
  private long idGrupoTicket;
  private long idUnidadAdministradora;
  private long idRegion;
  private Collection listGrupoTicket;
  private Collection listDisquete;
  private Collection listRegion;
  private Collection listUnidadAdministradora;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private TicketAlimentacionFacadeExtend ticketAlimentacionFacade = new TicketAlimentacionFacadeExtend();
  private int mes;
  private int anio;
  private boolean auxShow;

  public DisqueteTicketForm()
  {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectGrupoTicket = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void changeGrupoTicket(ValueChangeEvent event) {
    this.idGrupoTicket = Long.valueOf(
      (String)event.getNewValue()).longValue();
    this.showEspecial = false;
    this.auxShow = false;
    try {
      if (this.idGrupoTicket != 0L)
        if (this.tipoProceso.equals("N"))
          buscarProceso();
        else
          this.showEspecial = true;
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  private void buscarProceso() {
    try {
      SeguridadTicket seguridadTicket = this.ticketAlimentacionFacade.findLastSeguridadTicket(this.idGrupoTicket);
      this.mes = seguridadTicket.getMes();
      this.anio = seguridadTicket.getAnio();
      this.auxShow = true;
    } catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoProceso(ValueChangeEvent event)
  {
    String tipoProceso = (String)event.getNewValue();
    try {
      this.auxShow = false;
      if (tipoProceso.equals("N"))
        buscarProceso();
      else
        this.showEspecial = true;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void generar()
  {
    this.generado = false;
    int contador = 0;
    int tope = 0;

    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();

    ResultSet rsEncabezado = null;
    PreparedStatement stEncabezado = null;

    ResultSet rsEncabezado2 = null;
    PreparedStatement stEncabezado2 = null;

    ResultSet rsEncabezado3 = null;
    PreparedStatement stEncabezado3 = null;

    ResultSet rsDetalle = null;
    PreparedStatement stDetalle = null;

    ResultSet rsTotal = null;
    PreparedStatement stTotal = null;

    ResultSet rsTotal_registros = null;
    PreparedStatement stTotal_registros = null;

    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;

    ResultSet rsTotalNominaTrabajadores = null;
    PreparedStatement stTotalNominaTrabajadores = null;

    StringBuffer sqlTotalNominaTrab = new StringBuffer();

    StringBuffer valorBuffer = new StringBuffer();

    Connection connection = Resource.getConnection();
    try {
      Random rmd = new Random();
      rmd.nextDouble();

      boolean success = new File(request.getRealPath("disquetes")).mkdir();

      File file = new File(request.getRealPath("disquetes") + "/" + rmd.nextDouble() + ".txt");
      file.createNewFile();

      OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), Charset.forName("ISO-8859-1"));

      StringBuffer sql = new StringBuffer();
      sql.append("select sum(ht.neto_cantidad_ticket) as neto_cantidad_ticket, sum(ht.neto_monto_cobrar) as neto_monto_cobrar,  ");
      sql.append(" max(ht.cod_ticket) as cod_ticket, max(ht.nombre_dependencia) as nombre_dependencia, ht.denominacion_ticket, ");
      sql.append(" t.cedula as cedula, max(t.codigo_nomina) as codigo_nomina, ");
      sql.append(" max(lp.cod_lugar_pago) as lugar, max(lp.nombre) as nombre_lugar, max(p.primer_nombre) as primer_nombre,");
      sql.append(" max(p.segundo_nombre) as segundo_nombre,");
      sql.append(" max(p.primer_apellido) as primer_apellido,");
      sql.append(" max(p.segundo_apellido) as segundo_apellido,");
      sql.append(" max(p.nacionalidad) as nacionalidad,");
      sql.append(" max(p.fecha_nacimiento) as fecha_nacimiento,");
      sql.append(" max(lp.cod_cesta) as cod_cesta");
      sql.append(" from historicoticket ht, trabajador t, personal p, tipopersonal tp, lugarpago lp ");
      sql.append(" where ht.id_trabajador = t.id_trabajador");
      sql.append(" and t.id_personal = p.id_personal");
      sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_lugar_pago = lp.id_lugar_pago");
      sql.append(" and ht.neto_cantidad_ticket > 0");
      sql.append(" and tp.id_grupo_ticket = ?");
      sql.append(" and ht.mes_proceso  = ?");
      sql.append(" and ht.anio_proceso  = ?");
      sql.append(" and ht.especial  = ?");
      sql.append(" group by t.cedula, ht.denominacion_ticket");
      sql.append(" order by t.cedula");

      stTrabajadores = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTrabajadores.setLong(1, this.idGrupoTicket);
      stTrabajadores.setInt(2, this.mes);
      stTrabajadores.setInt(3, this.anio);
      if (this.tipoProceso.equals("E"))
        stTrabajadores.setString(4, "S");
      else {
        stTrabajadores.setString(4, "N");
      }
      rsTrabajadores = stTrabajadores.executeQuery();
      log.warn(sql.toString());

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '2' order by numero_campo");
      stDetalle = connection.prepareStatement(
        sql.toString(), 
        1000, 
        1007);
      stDetalle.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsDetalle = stDetalle.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '1' order by numero_campo");
      stEncabezado = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stEncabezado.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsEncabezado = stEncabezado.executeQuery();
      log.warn("........................encabezado");

      sql = new StringBuffer();
      sql.append("select count(distinct t.id_trabajador) as total_registros");
      sql.append(" from historicoticket ht, trabajador t, personal p, tipopersonal tp, lugarpago lp ");
      sql.append(" where ht.id_trabajador = t.id_trabajador");
      sql.append(" and t.id_personal = p.id_personal");
      sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_lugar_pago = lp.id_lugar_pago");
      sql.append(" and ht.neto_cantidad_ticket > 0");
      sql.append(" and tp.id_grupo_ticket = ?");
      sql.append(" and ht.mes_proceso  = ?");
      sql.append(" and ht.anio_proceso  = ?");
      sql.append(" and ht.especial  = ?");

      stTotal_registros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTotal_registros.setLong(1, this.idGrupoTicket);
      stTotal_registros.setInt(2, this.mes);
      stTotal_registros.setInt(3, this.anio);
      if (this.tipoProceso.equals("E"))
        stTotal_registros.setString(4, "S");
      else {
        stTotal_registros.setString(4, "N");
      }
      rsTotal_registros = stTotal_registros.executeQuery();

      sql = new StringBuffer();
      sql.append("select count(distinct t.id_trabajador) as total, sum(ht.neto_monto_cobrar) as TotalTicket  ");
      sql.append(" from historicoticket ht, trabajador t, personal p, tipopersonal tp, lugarpago lp ");
      sql.append(" where ht.id_trabajador = t.id_trabajador");
      sql.append(" and t.id_personal = p.id_personal");
      sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_lugar_pago = lp.id_lugar_pago");
      sql.append(" and ht.neto_cantidad_ticket > 0");
      sql.append(" and tp.id_grupo_ticket = ?");
      sql.append(" and ht.mes_proceso  = ?");
      sql.append(" and ht.anio_proceso  = ?");
      sql.append(" and ht.especial  = ?");

      stTotal = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTotal.setLong(1, this.idGrupoTicket);
      stTotal.setInt(2, this.mes);
      stTotal.setInt(3, this.anio);
      if (this.tipoProceso.equals("E"))
        stTotal.setString(4, "S");
      else {
        stTotal.setString(4, "N");
      }
      rsTotal = stTotal.executeQuery();

      log.warn(sql.toString());

      String campoTrabajo = "ninguno";
      boolean hayEncabezado = false;
      while (rsEncabezado.next()) {
        hayEncabezado = true;
        rsTotal.beforeFirst();
        rsTotal_registros.beforeFirst();
        if (rsEncabezado.getString("tipo_campo").equals("F"))
          writer.write(ToolsDisquete.convertirLetras(rsEncabezado.getString("campo_usuario"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), " "));
        else if (rsEncabezado.getString("tipo_campo").equals("C"))
        {
          if (rsTotal_registros.next()) {
            writer.write(ToolsDisquete.convertirNumero(rsTotal_registros.getDouble("total_registros"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          }
        }
        else if (rsEncabezado.getString("tipo_campo").equals("E"))
          switch (Integer.valueOf(rsEncabezado.getString("campo_entrada")).intValue())
          {
          case 1:
            String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
            valor = valor.substring(2);
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
            break;
          case 2:
            String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));

            break;
          case 3:
            String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
            valor = valor.substring(2);
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
            break;
          case 4:
            String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));

            break;
          case 5:
            String valor = String.valueOf(this.fechaAbono.getDate());
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
            break;
          case 6:
            String valor = String.valueOf(this.fechaProceso.getDate());
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
            break;
          case 7:
            valorBuffer.append(String.valueOf(this.fechaAbono.getHours() + 1));
            valorBuffer.append(String.valueOf(this.fechaAbono.getMinutes() + 1));
            valorBuffer.append(String.valueOf(this.fechaAbono.getSeconds() + 1));
            writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
            break;
          case 8:
            valorBuffer.append(String.valueOf(this.fechaProceso.getHours() + 1));
            valorBuffer.append(String.valueOf(this.fechaProceso.getMinutes() + 1));
            valorBuffer.append(String.valueOf(this.fechaProceso.getSeconds() + 1));
            writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
            break;
          case 9:
            String valor = String.valueOf(this.fechaAbono.getMonth() + 1);
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
            break;
          case 10:
            String valor = String.valueOf(this.fechaProceso.getMonth() + 1);
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
            break;
          case 11:
            String valor = String.valueOf(this.numeroOrdenPago);
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
            break;
          case 12:
            String valor = String.valueOf(this.anio);
            valor = valor.substring(2);
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
            break;
          case 13:
            String valor = String.valueOf(this.anio);
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));

            break;
          case 14:
            String valor = String.valueOf(this.mes);
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          default:
            break;
          }
        else if (rsEncabezado.getString("tipo_campo").equals("V")) {
          switch (Integer.valueOf(rsEncabezado.getString("campo_totales")).intValue())
          {
          case 1:
            log.warn("encabezado 1");
            if (rsTotal.next()) {
              writer.write(ToolsDisquete.convertirNumero(rsTrabajadores.getDouble("aporte"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
            }
            break;
          case 2:
            log.warn("encabezado 2");
            if (rsTotal.next())
            {
              writer.write(ToolsDisquete.convertirNumero(rsTotal.getDouble("TotalTicket"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
            }

            break;
          case 3:
            log.warn("encabezado 3");
            if (rsTotal.next())
            {
              writer.write(ToolsDisquete.convertirNumero(rsTrabajadores.getDouble("retencion"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
            }

            break;
          case 4:
            log.warn("encabezado 4");
            if (rsTotal.next()) {
              writer.write(ToolsDisquete.convertirNumero(rsTotal.getDouble("total"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
            }
            break;
          case 6:
            log.warn("encabezado 6");
            if (rsTotal.next())
            {
              writer.write(ToolsDisquete.convertirNumero(rsTrabajadores.getDouble("retencion") + rsTrabajadores.getDouble("aporte"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
            }

            break;
          case 5:
            log.warn("encabezado 5");
            if (rsTotal.next())
            {
              writer.write(ToolsDisquete.convertirNumero(rsTrabajadores.getDouble("correlativo"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
            }

            break;
          }

        }

      }

      if (hayEncabezado) {
        writer.write("\n");
      }
      hayEncabezado = false;

      while (rsTrabajadores.next()) {
        contador++;

        tope++;
        rsDetalle.beforeFirst();
        while (rsDetalle.next()) {
          if (rsDetalle.getString("tipo_campo").equals("F")) {
            log.warn("detalle Fijo");
            writer.write(ToolsDisquete.convertirLetras(rsDetalle.getString("campo_usuario"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
          } else if (rsDetalle.getString("tipo_campo").equals("C")) {
            log.warn("detalle contador");
            writer.write(ToolsDisquete.convertirNumero(contador, 1.0D, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
          }
          else if (rsDetalle.getString("tipo_campo").equals("E")) {
            switch (Integer.valueOf(rsDetalle.getString("campo_entrada")).intValue())
            {
            case 1:
              String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
              valor = valor.substring(2);
              writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 2:
              String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
              writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));

              break;
            case 3:
              String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
              valor = valor.substring(2);
              writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 4:
              String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
              writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));

              break;
            case 5:
              String valor = String.valueOf(this.fechaAbono.getDate());
              writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 6:
              String valor = String.valueOf(this.fechaProceso.getDate());
              writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 7:
              valorBuffer.append(String.valueOf(this.fechaAbono.getHours() + 1));
              valorBuffer.append(String.valueOf(this.fechaAbono.getMinutes() + 1));
              valorBuffer.append(String.valueOf(this.fechaAbono.getSeconds() + 1));
              writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 8:
              valorBuffer.append(String.valueOf(this.fechaProceso.getHours() + 1));
              valorBuffer.append(String.valueOf(this.fechaProceso.getMinutes() + 1));
              valorBuffer.append(String.valueOf(this.fechaProceso.getSeconds() + 1));
              writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 9:
              String valor = String.valueOf(this.fechaAbono.getMonth() + 1);
              writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 10:
              String valor = String.valueOf(this.fechaProceso.getMonth() + 1);
              writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            default:
              break;
            }
          } else if (rsDetalle.getString("tipo_campo").equals("V")) {
            switch (Integer.valueOf(rsDetalle.getString("campo_base_datos")).intValue())
            {
            case 9:
              try
              {
                campoTrabajo = "Año Nacimiento (AA)";
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_nacimiento").getYear() + 1900);
                valor = valor.substring(2);
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              } catch (Exception e) {
                log.error("Error en la seccion de Detalle N.  en el campo " + campoTrabajo);
                log.error(e);
              }

            case 10:
              try
              {
                campoTrabajo = "Año Nacimiento (AAAA)";
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_nacimiento").getYear() + 1900);
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              } catch (Exception e) {
                log.error("Error en la seccion de Detalle N. en el campo " + campoTrabajo);
                log.error(e);
              }

            case 34:
              try
              {
                campoTrabajo = "Dia Nacimiento";
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_nacimiento").getDate());
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              } catch (Exception e) {
                log.error("Error en la seccion de Detalle N.  en el campo " + campoTrabajo);
                log.error(e);
              }

            case 42:
              try
              {
                campoTrabajo = "Mes Nacimiento";
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_nacimiento").getMonth() + 1);
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              } catch (Exception e) {
                log.error("Error en la seccion de Detalle N. en el campo " + campoTrabajo);
                log.error(e);
              }

            case 13:
              log.warn("detalle 13");
              String nombresApellidos = rsTrabajadores.getString("primer_apellido") + " " + rsTrabajadores.getString("primer_nombre");
              writer.write(ToolsDisquete.convertirLetras(nombresApellidos, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 14:
              log.warn("detalle 14");

              writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("cedula"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 18:
              log.warn("detalle 18");

              writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("codigo_nomina"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 27:
              log.warn("detalle 27");
              writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("nombre_dependencia"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 52:
              log.warn("detalle 52");
              writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("nacionalidad"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 54:
              log.warn("detalle 54");
              writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("primer_apellido"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 55:
              log.warn("detalle 55");
              writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("primer_nombre"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 60:
              log.warn("detalle 60");
              writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("segundo_apellido"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 61:
              log.warn("detalle 61");
              writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("segundo_nombre"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 76:
              log.warn("detalle 50");
              writer.write(ToolsDisquete.convertirNumero(rsTrabajadores.getDouble("neto_monto_cobrar"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 77:
              log.warn("detalle 77");
              writer.write(ToolsDisquete.convertirNumero(rsTrabajadores.getInt("neto_cantidad_ticket"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 78:
              log.warn("detalle 78");
              writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("cod_ticket"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 83:
              log.warn("detalle 83");
              writer.write(ToolsDisquete.convertirNumero(rsTrabajadores.getDouble("denominacion_ticket"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 85:
              log.warn("detalle 85");
              writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("lugar"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              break;
            case 110:
              log.warn("detalle 110");
              writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("nombre_lugar"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), ToolsDisquete.caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            }

          }

        }

        if (!rsTrabajadores.isLast()) {
          writer.write("\n");
        }
        if (tope == 5000) {
          writer.flush();
        }

      }

      writer.flush();
      writer.close();
      this.generado = true;

      this.urlArchivo = ("/sigefirrhh/disquetes/" + file.getName());
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoTicket = this.ticketAlimentacionFacade.findGrupoTicketWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());

      this.listDisquete = this.definicionesFacade.findDisqueteByTipoDisquete("T", this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      this.listGrupoTicket = new ArrayList();
    }
  }

  public Collection getListGrupoTicket() {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoTicket.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getListDisquete() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listDisquete, "sigefirrhh.base.definiciones.Disquete");
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public LoginSession getLogin() {
    return this.login;
  }
  public int getReportId() {
    return this.reportId;
  }

  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getSelectDisquete() {
    return this.selectDisquete;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }
  public String getSelectUnidadAdministradora() {
    return this.selectUnidadAdministradora;
  }
  public void setSelectDisquete(String string) {
    this.selectDisquete = string;
    try {
      this.disquete = this.definicionesFacade.findDisqueteById(Long.valueOf(this.selectDisquete).longValue());
    } catch (Exception localException) {
    }
  }

  public void setSelectRegion(String string) {
    this.selectRegion = string;
  }
  public void setSelectUnidadAdministradora(String string) {
    this.selectUnidadAdministradora = string;
  }
  public java.util.Date getFechaAbono() {
    return this.fechaAbono;
  }
  public java.util.Date getFechaProceso() {
    return this.fechaProceso;
  }
  public void setFechaAbono(java.util.Date date) {
    this.fechaAbono = date;
  }
  public void setFechaProceso(java.util.Date date) {
    this.fechaProceso = date;
  }
  public boolean isGenerado() {
    return this.generado;
  }
  public String getUrlArchivo() {
    return this.urlArchivo;
  }

  public String getNumeroOrdenPago() {
    return this.numeroOrdenPago;
  }
  public void setNumeroOrdenPago(String numeroOrdenPago) {
    this.numeroOrdenPago = numeroOrdenPago;
  }
  public String getselectGrupoTicket() {
    return this.selectGrupoTicket;
  }
  public void setselectGrupoTicket(String selectGrupoTicket) {
    this.selectGrupoTicket = selectGrupoTicket;
  }
  public String getTipoProceso() {
    return this.tipoProceso;
  }
  public void setTipoProceso(String tipoProceso) {
    this.tipoProceso = tipoProceso;
  }
  public boolean isShowEspecial() {
    return this.showEspecial;
  }
  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
}