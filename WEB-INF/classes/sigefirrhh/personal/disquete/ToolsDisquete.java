package sigefirrhh.personal.disquete;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import org.apache.log4j.Logger;

public class ToolsDisquete
{
  static Logger log = Logger.getLogger(ToolsDisquete.class.getName());

  public static String convertirNumero(double monto, double multiplicador, int longitud, boolean alineacionDerecha, String relleno, String separador, String separador_miles)
  {
    StringBuffer valor = new StringBuffer();

    log.error("convertirNumero / monto : " + monto);
    log.error("LUIS MORA: ");

    DecimalFormat df = new DecimalFormat();
    DecimalFormatSymbols dfs = new DecimalFormatSymbols();

    if ((separador.equals("N")) && (separador_miles.equals("N")))
    {
      monto *= multiplicador;
      df.applyPattern("###");
      valor.append(df.format(monto));
    }
    else if ((separador.equals("N")) && (separador_miles.equals("P")))
    {
      dfs.setDecimalSeparator(',');
      dfs.setGroupingSeparator('.');
      df.setDecimalFormatSymbols(dfs);
      valor.append(df.format(monto));
      valor = new StringBuffer(valor.toString().replaceAll(",", ""));
    }
    else if ((separador.equals("N")) && (separador_miles.equals("C")))
    {
      dfs.setDecimalSeparator('#');
      dfs.setGroupingSeparator(',');
      df.setDecimalFormatSymbols(dfs);
      valor.append(df.format(monto));

      StringBuffer valor_D = new StringBuffer(valor.toString().replaceAll("#", ""));

      valor = valor_D;
    }
    else if ((separador.equals("C")) && (separador_miles.equals("C")))
    {
      dfs.setDecimalSeparator(',');
      dfs.setGroupingSeparator(',');
      df.setDecimalFormatSymbols(dfs);

      valor.append(df.format(monto));
    } else if ((separador.equals("C")) && (separador_miles.equals("P")))
    {
      dfs.setDecimalSeparator(',');
      dfs.setGroupingSeparator('.');
      df.setDecimalFormatSymbols(dfs);

      valor.append(df.format(monto));
    } else if ((separador.equals("C")) && (separador_miles.equals("N")))
    {
      dfs.setDecimalSeparator(',');
      dfs.setGroupingSeparator('#');
      df.setDecimalFormatSymbols(dfs);
      valor.append(df.format(monto));

      valor = new StringBuffer(valor.toString().replaceAll("#", ""));
    }
    else if ((separador.equals("P")) && (separador_miles.equals("P")))
    {
      dfs.setDecimalSeparator('.');
      dfs.setGroupingSeparator('.');
      df.setDecimalFormatSymbols(dfs);

      valor.append(df.format(monto));
    } else if ((separador.equals("P")) && (separador_miles.equals("C")))
    {
      dfs.setDecimalSeparator('.');
      dfs.setGroupingSeparator(',');
      df.setDecimalFormatSymbols(dfs);

      valor.append(df.format(monto));
    } else if ((separador.equals("P")) && (separador_miles.equals("N")))
    {
      dfs.setDecimalSeparator('.');
      dfs.setGroupingSeparator(',');
      df.setDecimalFormatSymbols(dfs);
      valor.append(df.format(monto));
      valor = new StringBuffer(valor.toString().replaceAll(",", ""));
    }

    log.error("******* convertirNumero / FINALLLLLL valor:" + valor);
    log.error("******* convertirNumero / FINALLLLLL longitud:" + longitud);
    longitud -= valor.length();

    StringBuffer valorRelleno = new StringBuffer();

    int i = 0;
    if (longitud > 0) {
      for (i = 0; i < longitud; i++) {
        valorRelleno.append(relleno);
      }
      if (alineacionDerecha) {
        return valorRelleno.toString() + valor.toString();
      }
      return valor.toString() + valorRelleno.toString();
    }

    if (longitud == 0) {
      return valor.toString();
    }
    return "";
  }

  public static String convertirNumero(double monto, double multiplicador, int longitud, boolean alineacionDerecha, String relleno, String separador)
  {
    StringBuffer valor = new StringBuffer();
    monto *= multiplicador;

    DecimalFormat df = new DecimalFormat();
    DecimalFormatSymbols dfs = new DecimalFormatSymbols();

    if (!separador.equals("P"))
    {
      if (separador.equals("C")) {
        dfs.setDecimalSeparator(',');
        df.setDecimalFormatSymbols(dfs);
        df.applyPattern("###.00");
      } else if (separador.equals("N")) {
        df.applyPattern("###");
      }
      valor.append(df.format(monto));
    } else {
      dfs.setDecimalSeparator('.');
      df.setDecimalFormatSymbols(dfs);
      df.applyPattern("###.0#");
      valor.append(monto);
    }

    longitud -= valor.length();

    StringBuffer valorRelleno = new StringBuffer();

    int i = 0;
    if (longitud > 0)
    {
      for (i = 0; i < longitud; i++) {
        valorRelleno.append(relleno);
      }
      if (alineacionDerecha) {
        return valorRelleno.toString() + valor.toString();
      }
      return valor.toString() + valorRelleno.toString();
    }

    if (longitud == 0) {
      return valor.toString();
    }
    return "";
  }

  public static String convertirNumero(double monto, double multiplicador, int longitud, boolean alineacionDerecha, String relleno)
  {
    StringBuffer valor = new StringBuffer();
    monto *= multiplicador;

    DecimalFormat df = new DecimalFormat();

    df.applyPattern("####");

    valor.append(df.format(monto));

    longitud -= valor.length();

    StringBuffer valorRelleno = new StringBuffer();

    int i = 0;
    if (longitud > 0)
    {
      for (i = 0; i < longitud; i++) {
        valorRelleno.append(relleno);
      }
      if (alineacionDerecha) {
        return valorRelleno.toString() + valor.toString();
      }
      return valor.toString() + valorRelleno.toString();
    }

    if (longitud == 0) {
      return valor.toString();
    }
    return "";
  }

  public static String convertirLetras(String valor, int longitud, boolean alineacionDerecha, String relleno) {
    StringBuffer valorRelleno = new StringBuffer();

    if (valor == null) {
      valor = " ";
    }

    int longitudInicial = longitud;

    longitud -= valor.length();

    int i = 0;
    if (longitud > 0) {
      for (i = 0; i < longitud; i++) {
        valorRelleno.append(relleno);
      }

      if (alineacionDerecha) {
        return valorRelleno.toString() + valor.toString();
      }
      return valor.toString() + valorRelleno.toString();
    }

    if (longitud == 0) {
      return valor;
    }
    return valor.substring(0, longitudInicial);
  }

  public static String caracterRelleno(String caracter)
  {
    String respuesta = null;

    if (caracter.equals("C"))
      respuesta = "0";
    else if (caracter.equals("B"))
      respuesta = " ";
    else if (caracter.equals("N")) {
      respuesta = "";
    }

    return respuesta;
  }

  public static String quitarCharEspecial(String valor)
  {
    String palabra = valor;

    palabra = palabra.replaceAll("Á", "A");
    palabra = palabra.replaceAll("É", "E");
    palabra = palabra.replaceAll("Í", "I");
    palabra = palabra.replaceAll("Ó", "O");
    palabra = palabra.replaceAll("Ú", "U");
    palabra = palabra.replaceAll("Ñ", "N");
    palabra = palabra.replaceAll("á", "a");
    palabra = palabra.replaceAll("é", "e");
    palabra = palabra.replaceAll("í", "i");
    palabra = palabra.replaceAll("ó", "o");
    palabra = palabra.replaceAll("ú", "u");
    palabra = palabra.replaceAll("ñ", "n");

    palabra = palabra.replaceAll("\\?", " ");

    palabra = palabra.replaceAll("¿", " ");
    palabra = palabra.replaceAll("¡", " ");
    palabra = palabra.replaceAll("!", " ");

    return palabra;
  }

  public static boolean armaEncabezado(ResultSet rsEncabezado, ResultSet rsTotalNominaTrabajadores, OutputStreamWriter writer, int posicion, boolean hayEncabezado, java.util.Date fechaProceso, java.util.Date fechaAbono, String numeroOrdenPago, int anio, int mes)
    throws NumberFormatException, SQLException, IOException
  {
    StringBuffer valorBuffer = new StringBuffer();
    String campoTrabajo = "ninguno";
    boolean yoEscribi = false;
    int contador = 0;
    log.error("Comienza generación del encabezado N° " + posicion);

    while (rsEncabezado.next())
    {
      if ((hayEncabezado) && (!yoEscribi)) {
        writer.write("\n");
      }
      contador++;
      hayEncabezado = true;
      yoEscribi = true;

      rsTotalNominaTrabajadores.beforeFirst();
      if (rsEncabezado.getString("tipo_campo").equals("F")) {
        writer.write(convertirLetras(rsEncabezado.getString("campo_usuario"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
      }
      else if (rsEncabezado.getString("tipo_campo").equals("C")) {
        writer.write(convertirNumero(contador, 1.0D, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
      }
      else if (rsEncabezado.getString("tipo_campo").equals("E"))
        switch (Integer.valueOf(rsEncabezado.getString("campo_entrada")).intValue())
        {
        case 1:
          try {
            campoTrabajo = "Año Abono (AA)";
            String valor = String.valueOf(fechaAbono.getYear() + 1900);
            valor = valor.substring(2);
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 2:
          try
          {
            campoTrabajo = "Año Abono (AAAA)";
            String valor = String.valueOf(fechaAbono.getYear() + 1900);
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 3:
          try
          {
            campoTrabajo = "Año Proceso (AA)";
            String valor = String.valueOf(fechaProceso.getYear() + 1900);
            valor = valor.substring(2);
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 4:
          try
          {
            campoTrabajo = "Año Proceso (AAAA)";
            String valor = String.valueOf(fechaProceso.getYear() + 1900);
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 5:
          try
          {
            campoTrabajo = "Dia Abono";
            String valor = String.valueOf(fechaAbono.getDate());
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 6:
          try
          {
            campoTrabajo = "Dia Proceso";
            String valor = String.valueOf(fechaProceso.getDate());
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 7:
          try
          {
            campoTrabajo = "Hora Abono (HHMMSS)";
            valorBuffer.append(String.valueOf(fechaAbono.getHours() + 1));
            valorBuffer.append(String.valueOf(fechaAbono.getMinutes() + 1));
            valorBuffer.append(String.valueOf(fechaAbono.getSeconds() + 1));
            writer.write(convertirLetras(valorBuffer.toString(), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }
        case 8:
          try
          {
            campoTrabajo = "HORA EJECUCION (HHMMSS)";
            valorBuffer.append(String.valueOf(fechaProceso.getHours() + 1));
            valorBuffer.append(String.valueOf(fechaProceso.getMinutes() + 1));
            valorBuffer.append(String.valueOf(fechaProceso.getSeconds() + 1));
            writer.write(convertirLetras(valorBuffer.toString(), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 9:
          try
          {
            campoTrabajo = "Mes Abono";
            String valor = String.valueOf(fechaAbono.getMonth() + 1);
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 10:
          try
          {
            campoTrabajo = "Mes Proceso";
            String valor = String.valueOf(fechaProceso.getMonth() + 1);
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 11:
          try
          {
            campoTrabajo = "Numero Orden de Pago";
            String valor = String.valueOf(numeroOrdenPago);
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }
        case 12:
          try
          {
            campoTrabajo = "AÑO PROCESO (AA)";
            String valor = String.valueOf(anio);
            valor = valor.substring(2);
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 13:
          try
          {
            campoTrabajo = "AÑO PROCESO (AAAA)";
            String valor = String.valueOf(anio);
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 14:
          try
          {
            campoTrabajo = "Mes Proceso";
            String valor = String.valueOf(mes);
            writer.write(convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        default:
          break;
        }
      else if (rsEncabezado.getString("tipo_campo").equals("V")) {
        switch (Integer.valueOf(rsEncabezado.getString("campo_totales")).intValue())
        {
        case 1:
          try
          {
            campoTrabajo = "aporte";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("aporte"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 2:
          try
          {
            campoTrabajo = "Total Neto Nomina";
            log.error(" Paso " + campoTrabajo);
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("neto"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
          }
          catch (Exception e)
          {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 3:
          try
          {
            campoTrabajo = "retencion";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("retencion"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 4:
          try
          {
            campoTrabajo = "Total Trabajadores";
            log.error(" Paso " + campoTrabajo);
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("total"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 5:
          try
          {
            campoTrabajo = "Correlativo";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("correlativo"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 6:
          try
          {
            campoTrabajo = "Total Retencion Aporte Lph";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("retencion") + rsTotalNominaTrabajadores.getDouble("aporte"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 7:
          try
          {
            campoTrabajo = "Total Abono Mensual";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("prestaciones"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 8:
          try
          {
            campoTrabajo = "TOTAL DIAS ADICIONALES PRESTACIONES";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("adicional"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        case 9:
          try
          {
            campoTrabajo = "Total abono mas adicional";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("prestaciones") + rsTotalNominaTrabajadores.getDouble("adicional"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), caracterRelleno(rsEncabezado.getString("rellenar_cero")), rsEncabezado.getString("separador_decimal"), rsEncabezado.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Encabezado N. " + posicion + " en el campo " + campoTrabajo);
            log.error(e);
          }

        }

      }

    }

    return hayEncabezado;
  }

  public static boolean armaDetalle(ResultSet rsParametroGobierno, ResultSet rsDetalle, ResultSet rsTrabajadores, OutputStreamWriter writer, int posicion, boolean hayEncabezado, java.util.Date fechaProceso, java.util.Date fechaAbono, String numeroOrdenPago, int anio, int mes)
    throws NumberFormatException, SQLException, IOException
  {
    StringBuffer valorBuffer = new StringBuffer();
    int contador = 0;
    int tope = 0;
    String campoTrabajo = "ninguno";

    log.error("Comienza generación del detalle N° " + posicion);

    while (rsTrabajadores.next())
    {
      if (hayEncabezado)
        writer.write("\n");
      else {
        hayEncabezado = true;
      }
      contador++;

      tope++;
      rsDetalle.beforeFirst();
      while (rsDetalle.next())
      {
        hayEncabezado = true;

        if (rsDetalle.getString("tipo_campo").equals("F"))
          writer.write(convertirLetras(rsDetalle.getString("campo_usuario"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
        else if (rsDetalle.getString("tipo_campo").equals("C"))
          writer.write(convertirNumero(contador, 1.0D, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
        else if (rsDetalle.getString("tipo_campo").equals("E"))
          switch (Integer.valueOf(rsDetalle.getString("campo_entrada")).intValue())
          {
          case 1:
            try {
              campoTrabajo = "Año Abono (AA)";
              String valor = String.valueOf(fechaAbono.getYear() + 1900);
              valor = valor.substring(2);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 2:
            try
            {
              campoTrabajo = "Año Abono (AAAA)";
              String valor = String.valueOf(fechaAbono.getYear() + 1900);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 3:
            try
            {
              campoTrabajo = "Año Proceso (AA)";
              String valor = String.valueOf(fechaProceso.getYear() + 1900);
              valor = valor.substring(2);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 4:
            try
            {
              campoTrabajo = "Año Abono (AAAA)";
              String valor = String.valueOf(fechaProceso.getYear() + 1900);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 5:
            try
            {
              campoTrabajo = "Dia Abono";
              String valor = String.valueOf(fechaAbono.getDate());
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 6:
            try
            {
              campoTrabajo = "Dia Proceso";
              String valor = String.valueOf(fechaProceso.getDate());
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 7:
            try
            {
              campoTrabajo = "Hora Abono (HHMMSS)";
              valorBuffer.append(String.valueOf(fechaAbono.getHours() + 1));
              valorBuffer.append(String.valueOf(fechaAbono.getMinutes() + 1));
              valorBuffer.append(String.valueOf(fechaAbono.getSeconds() + 1));
              writer.write(convertirLetras(valorBuffer.toString(), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }
          case 8:
            try
            {
              campoTrabajo = "HORA EJECUCION (HHMMSS)";
              valorBuffer.append(String.valueOf(fechaProceso.getHours() + 1));
              valorBuffer.append(String.valueOf(fechaProceso.getMinutes() + 1));
              valorBuffer.append(String.valueOf(fechaProceso.getSeconds() + 1));
              writer.write(convertirLetras(valorBuffer.toString(), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 9:
            try
            {
              campoTrabajo = "Mes Abono";
              String valor = String.valueOf(fechaAbono.getMonth() + 1);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 10:
            try
            {
              campoTrabajo = "Mes Proceso";
              String valor = String.valueOf(fechaProceso.getMonth() + 1);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 11:
            try
            {
              campoTrabajo = "Numero Orden de Pago";
              String valor = String.valueOf(numeroOrdenPago);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 12:
            try
            {
              campoTrabajo = "Año Proceso (AA)";
              String valor = String.valueOf(anio);
              valor = valor.substring(2);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 13:
            try
            {
              campoTrabajo = "Año Proceso (AAAA)";
              String valor = String.valueOf(anio);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 14:
            try
            {
              campoTrabajo = "Mes Proceso";
              String valor = String.valueOf(mes);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          default:
            break;
          }
        else if (rsDetalle.getString("tipo_campo").equals("V"))
        {
          switch (Integer.valueOf(rsDetalle.getString("campo_base_datos")).intValue()) {
          case 2:
            try {
              campoTrabajo = "AÑO EGRESO (AA)";
              String valor;
              String valor;
              if (rsTrabajadores.getDate("fecha_egreso") == null) {
                valor = "";
              } else {
                valor = String.valueOf(rsTrabajadores.getDate("fecha_egreso").getYear() + 1900);
                valor = valor.substring(2);
              }
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 3:
            try
            {
              campoTrabajo = "Año Egreso Organismo (AAAA)";
              String valor;
              String valor;
              if (rsTrabajadores.getDate("fecha_egreso") == null)
                valor = "";
              else {
                valor = String.valueOf(rsTrabajadores.getDate("fecha_egreso").getYear() + 1900);
              }
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 7:
            try
            {
              campoTrabajo = "Año Ingreso Organismo (AA)";
              String valor = String.valueOf(rsTrabajadores.getDate("fecha_ingreso").getYear() + 1900);
              valor = valor.substring(2);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 8:
            try
            {
              campoTrabajo = "Año Ingreso Organismo (AAAA)";
              String valor = String.valueOf(rsTrabajadores.getDate("fecha_ingreso").getYear() + 1900);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 9:
            try
            {
              campoTrabajo = "Año Nacimiento (AA)";
              String valor = String.valueOf(rsTrabajadores.getDate("fecha_nacimiento").getYear() + 1900);
              valor = valor.substring(2);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 10:
            try
            {
              campoTrabajo = "Año Nacimiento (AAAA)";
              String valor = String.valueOf(rsTrabajadores.getDate("fecha_nacimiento").getYear() + 1900);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 13:
            try
            {
              campoTrabajo = "Apellidos y nombres";

              String nombresApellidos = quitarCharEspecial(rsTrabajadores.getString("primer_apellido")) + " " + quitarCharEspecial(rsTrabajadores.getString("primer_nombre"));
              writer.write(convertirLetras(nombresApellidos, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 14:
            try
            {
              campoTrabajo = "Cedula";
              writer.write(convertirLetras(rsTrabajadores.getString("cedula"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 15:
            try
            {
              campoTrabajo = "Codigo Cargo";
              writer.write(convertirLetras(rsTrabajadores.getString("codcargo"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 16:
            try
            {
              campoTrabajo = "Codigo Concepto";
              writer.write(convertirLetras(rsTrabajadores.getString("concepto"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 17:
            try
            {
              campoTrabajo = "Codigo Dependencia";
              writer.write(convertirLetras(rsTrabajadores.getString("dependencia"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 18:
            try
            {
              campoTrabajo = "Codigo Nomina";
              writer.write(convertirLetras(rsTrabajadores.getString("codigo_nomina"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 24:
            try
            {
              campoTrabajo = "Cuenta Nomina";
              writer.write(convertirLetras(rsTrabajadores.getString("cuenta"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 25:
            try
            {
              campoTrabajo = "Descripcion Cargo";
              writer.write(convertirLetras(rsTrabajadores.getString("cargo"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 26:
            try
            {
              campoTrabajo = "Descripcion Concepto";

              writer.write(convertirLetras(rsTrabajadores.getString("concepto_descripcion"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 27:
            try
            {
              campoTrabajo = "Nombre Dependencia";
              writer.write(convertirLetras(rsTrabajadores.getString("nombre_dependencia"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 31:
            try
            {
              campoTrabajo = "Dia Egreso";
              String valor;
              String valor;
              if (rsTrabajadores.getDate("fecha_egreso") == null)
                valor = "";
              else {
                valor = String.valueOf(rsTrabajadores.getDate("fecha_egreso").getDate());
              }
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 33:
            try
            {
              campoTrabajo = "Dia Ingreso";
              String valor = String.valueOf(rsTrabajadores.getDate("fecha_ingreso").getDate());
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 34:
            try
            {
              campoTrabajo = "Dia Nacimiento";
              String valor = String.valueOf(rsTrabajadores.getDate("fecha_nacimiento").getDate());
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 35:
            try
            {
              campoTrabajo = "Estado Civil";
              log.error("detalle 35-EstadoCivil");
              writer.write(convertirLetras(rsTrabajadores.getString("estado_civil"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 38:
            try
            {
              campoTrabajo = "Mes Egreso";
              String valor;
              String valor;
              if (rsTrabajadores.getDate("fecha_egreso") == null)
                valor = "";
              else {
                valor = String.valueOf(rsTrabajadores.getDate("fecha_egreso").getMonth() + 1);
              }
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 41:
            try
            {
              campoTrabajo = "Mes Ingreso";
              String valor = String.valueOf(rsTrabajadores.getDate("fecha_ingreso").getMonth() + 1);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 42:
            try
            {
              campoTrabajo = "Mes Nacimiento";
              String valor = String.valueOf(rsTrabajadores.getDate("fecha_nacimiento").getMonth() + 1);
              writer.write(convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 44:
            try
            {
              campoTrabajo = "Monto aporte Patronal";
              writer.write(convertirNumero(rsTrabajadores.getDouble("aporte"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 50:
            try
            {
              campoTrabajo = "Neto Nómina";
              writer.write(convertirNumero(rsTrabajadores.getDouble("neto"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 51:
            try
            {
              campoTrabajo = "Monto Retencion";
              writer.write(convertirNumero(rsTrabajadores.getDouble("retencion"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 52:
            try
            {
              campoTrabajo = "Nacionalidad";
              writer.write(convertirLetras(rsTrabajadores.getString("nacionalidad"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 54:
            try
            {
              campoTrabajo = "Primer Apellido";

              writer.write(convertirLetras(quitarCharEspecial(rsTrabajadores.getString("primer_apellido")), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 55:
            try
            {
              campoTrabajo = "Primer Nombre";

              writer.write(convertirLetras(quitarCharEspecial(rsTrabajadores.getString("primer_nombre")), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 56:
            try
            {
              campoTrabajo = "Tipo Cuenta";
              if (rsTrabajadores.getString("tipo_cuenta").equals("A"))
                writer.write(convertirLetras(rsTrabajadores.getString("ahorro"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
              else
                writer.write(convertirLetras(rsTrabajadores.getString("corriente"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            }
            catch (Exception e)
            {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 57:
            try
            {
              campoTrabajo = "Cod_tipo_personal";
              writer.write(convertirLetras(rsTrabajadores.getString("cod_tipo_personal"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 60:
            try
            {
              campoTrabajo = "Segundo Apellido";

              writer.write(convertirLetras(quitarCharEspecial(rsTrabajadores.getString("segundo_apellido")), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 61:
            try
            {
              campoTrabajo = "Segundo Nombre";
              writer.write(convertirLetras(rsTrabajadores.getString("segundo_nombre"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 62:
            try
            {
              campoTrabajo = "Sexo";
              writer.write(convertirLetras(rsTrabajadores.getString("sexo"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 63:
            try
            {
              campoTrabajo = "Sueldo Basico";
              writer.write(convertirNumero(rsTrabajadores.getDouble("sueldo_basico"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 64:
            try
            {
              campoTrabajo = "Sueldo Integral";
              writer.write(convertirNumero(rsTrabajadores.getDouble("promedio_integral"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 75:
            try
            {
              campoTrabajo = "Cuenta LPH Trabajador";
              writer.write(convertirLetras(rsTrabajadores.getString("cuenta_lph"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 76:
            try
            {
              campoTrabajo = "Monto total tickets";
              writer.write(convertirNumero(rsTrabajadores.getDouble("neto_monto_cobrar"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 77:
            try
            {
              campoTrabajo = "Numero de Tickets";
              writer.write(convertirNumero(rsTrabajadores.getInt("neto_cantidad_ticket"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 78:
            try
            {
              campoTrabajo = "Codigo Cesta";
              writer.write(convertirLetras(rsTrabajadores.getString("cod_ticket"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 80:
            try
            {
              campoTrabajo = "Sexo";
              String sexo;
              String sexo;
              if (rsTrabajadores.getString("sexo").equals("F"))
                sexo = "2";
              else {
                sexo = "1";
              }
              writer.write(convertirLetras(sexo, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 81:
            try
            {
              campoTrabajo = "Estatus (0,1,2)";
              String estatus = "0";
              if (rsTrabajadores.getString("estatus").equals("E")) {
                if ((rsTrabajadores.getDate("fecha_egreso").getMonth() + 1 == mes) && 
                  (rsTrabajadores.getDate("fecha_egreso").getYear() + 1900 == anio))
                  estatus = "1";
              }
              else if ((rsTrabajadores.getString("estatus").equals("A")) && 
                (rsTrabajadores.getDate("fecha_ingreso").getMonth() + 1 == mes) && 
                (rsTrabajadores.getDate("fecha_ingreso").getYear() + 1900 == anio)) {
                estatus = "2";
              }

              writer.write(convertirLetras(estatus, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 82:
            try
            {
              campoTrabajo = "Monto Retencion + Monto Aporte";
              writer.write(convertirNumero(rsTrabajadores.getDouble("retencion") + rsTrabajadores.getDouble("aporte"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 83:
            try
            {
              campoTrabajo = "Denominación ticket";
              writer.write(convertirNumero(rsTrabajadores.getDouble("denominacion_ticket"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 85:
            try
            {
              campoTrabajo = "Codigo Lugar Pago";
              writer.write(convertirLetras(rsTrabajadores.getString("lugar"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 86:
            try
            {
              campoTrabajo = "email de trabajador";

              writer.write(convertirLetras(rsTrabajadores.getString("e_mail"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 90:
            try
            {
              campoTrabajo = "Forma de Pago  (1=Deposito,2=Cheque)";
              writer.write(convertirLetras(rsTrabajadores.getString("forma_pago"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 91:
            try
            {
              campoTrabajo = "Forma de Pago D=Deposito/C=Cheque";
              String formapago;
              String formapago;
              if (rsTrabajadores.getString("forma_pago").equals("1"))
                formapago = "D";
              else {
                formapago = "C";
              }
              writer.write(convertirLetras(formapago, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 92:
            try
            {
              campoTrabajo = "Promedio SSO";
              writer.write(convertirNumero(rsTrabajadores.getDouble("promedio_sso"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 93:
            try
            {
              campoTrabajo = "Promedio SPF";
              writer.write(convertirNumero(rsTrabajadores.getDouble("promedio_spf"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 94:
            try
            {
              campoTrabajo = "Promedio LPH";
              writer.write(convertirNumero(rsTrabajadores.getDouble("promedio_lph"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 95:
            try
            {
              campoTrabajo = "Promedio FJU";
              writer.write(convertirNumero(rsTrabajadores.getDouble("promedio_fju"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 96:
            try
            {
              campoTrabajo = "Tipo acción LPH 1=ACtivo,2=Retirar,3=Ingresa";
              String estatus = "1";

              if (rsTrabajadores.getString("estatus").equals("E")) {
                if ((rsTrabajadores.getDate("fecha_egreso").getMonth() + 1 == mes) && 
                  (rsTrabajadores.getDate("fecha_egreso").getYear() + 1900 == anio))
                  estatus = "2";
              }
              else if ((rsTrabajadores.getString("estatus").equals("A")) && 
                (rsTrabajadores.getDate("fecha_ingreso").getMonth() + 1 == mes) && 
                (rsTrabajadores.getDate("fecha_ingreso").getYear() + 1900 == anio)) {
                estatus = "3";
              }

              writer.write(convertirLetras(estatus, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 97:
            try
            {
              campoTrabajo = "Porcentaje trabajador LPH";
              writer.write(convertirNumero(rsParametroGobierno.getDouble("porctrab_lph"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 98:
            try
            {
              campoTrabajo = "Porcentaje patrono  LPH";
              writer.write(convertirNumero(rsParametroGobierno.getDouble("porcpat_lph"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 99:
            try
            {
              campoTrabajo = "Porcentaje patrono LPH + porcentaje trabajador  LPH";
              writer.write(convertirNumero(rsParametroGobierno.getDouble("porcpat_lph") + rsParametroGobierno.getDouble("porctrab_lph"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal")));
            }
            catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 110:
            try
            {
              campoTrabajo = "Nombre Lugar Pago";
              writer.write(convertirLetras(rsTrabajadores.getString("nombre_lugar"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero"))));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e);
            }

          case 111:
            try
            {
              campoTrabajo = "Mes Proceso";
              writer.write(convertirNumero(rsTrabajadores.getDouble("porcentaje_isrl"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), caracterRelleno(rsDetalle.getString("rellenar_cero")), rsDetalle.getString("separador_decimal"), rsDetalle.getString("separador_miles")));
            } catch (Exception e) {
              log.error("Error en la seccion de Detalle N. " + posicion + " en el campo " + campoTrabajo);
              log.error(e); } case 4:
          case 5:
          case 6:
          case 11:
          case 12:
          case 19:
          case 20:
          case 21:
          case 22:
          case 23:
          case 28:
          case 29:
          case 30:
          case 32:
          case 36:
          case 37:
          case 39:
          case 40:
          case 43:
          case 45:
          case 46:
          case 47:
          case 48:
          case 49:
          case 53:
          case 58:
          case 59:
          case 65:
          case 66:
          case 67:
          case 68:
          case 69:
          case 70:
          case 71:
          case 72:
          case 73:
          case 74:
          case 79:
          case 84:
          case 87:
          case 88:
          case 89:
          case 100:
          case 101:
          case 102:
          case 103:
          case 104:
          case 105:
          case 106:
          case 107:
          case 108:
          case 109: }  }  } if (tope == 5000) {
        writer.flush();
      }

    }

    return hayEncabezado;
  }

  public static boolean armaTotal(ResultSet rsTotal, ResultSet rsTotalNominaTrabajadores, OutputStreamWriter writer, boolean hayEncabezado, java.util.Date fechaProceso, java.util.Date fechaAbono, String numeroOrdenPago, int anio, int mes)
    throws NumberFormatException, SQLException, IOException
  {
    StringBuffer valorBuffer = new StringBuffer();
    boolean yoEscribi = false;

    String campoTrabajo = "ninguno";

    log.error("Comienza generación del Total");

    while (rsTotal.next())
    {
      if ((hayEncabezado) && (!yoEscribi))
        writer.write("\n");
      else {
        hayEncabezado = true;
      }

      hayEncabezado = true;
      yoEscribi = true;

      rsTotalNominaTrabajadores.beforeFirst();
      if (rsTotal.getString("tipo_campo").equals("F"))
      {
        writer.write(convertirLetras(rsTotal.getString("campo_usuario"), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
      } else if (rsTotal.getString("tipo_campo").equals("E"))
      {
        switch (Integer.valueOf(rsTotal.getString("campo_entrada")).intValue())
        {
        case 1:
          try {
            campoTrabajo = "Año Abono (AA)";
            String valor = String.valueOf(fechaAbono.getYear() + 1900);
            valor = valor.substring(2);
            writer.write(convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }

        case 2:
          try
          {
            campoTrabajo = "Año Abono (AAAA)";
            String valor = String.valueOf(fechaAbono.getYear() + 1900);
            writer.write(convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }

        case 3:
          try
          {
            campoTrabajo = "Año Proceso (AA)";
            String valor = String.valueOf(fechaProceso.getYear() + 1900);
            valor = valor.substring(2);
            writer.write(convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }

        case 4:
          try
          {
            campoTrabajo = "Año Abono (AAAA)";
            String valor = String.valueOf(fechaProceso.getYear() + 1900);
            writer.write(convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }

        case 5:
          try
          {
            campoTrabajo = "Dia Abono";
            String valor = String.valueOf(fechaAbono.getDate());
            writer.write(convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }

        case 6:
          try
          {
            campoTrabajo = "Dia Proceso";
            String valor = String.valueOf(fechaProceso.getDate());
            writer.write(convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }

        case 7:
          try
          {
            campoTrabajo = "Hora Abono (HHMMSS)";
            valorBuffer.append(String.valueOf(fechaAbono.getHours() + 1));
            valorBuffer.append(String.valueOf(fechaAbono.getMinutes() + 1));
            valorBuffer.append(String.valueOf(fechaAbono.getSeconds() + 1));
            writer.write(convertirLetras(valorBuffer.toString(), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }
        case 8:
          try
          {
            campoTrabajo = "AÑO INGRESO ORGANISMO (AAAA)";
            valorBuffer.append(String.valueOf(fechaProceso.getHours() + 1));
            valorBuffer.append(String.valueOf(fechaProceso.getMinutes() + 1));
            valorBuffer.append(String.valueOf(fechaProceso.getSeconds() + 1));
            writer.write(convertirLetras(valorBuffer.toString(), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }

        case 9:
          try
          {
            campoTrabajo = "AÑO NACIMIENTO (AA)";
            String valor = String.valueOf(fechaAbono.getMonth() + 1);
            writer.write(convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }

        case 10:
          try
          {
            campoTrabajo = "Mes Proceso";
            String valor = String.valueOf(fechaProceso.getMonth() + 1);
            writer.write(convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }

        case 11:
          try
          {
            campoTrabajo = "Numero Orden de Pago";
            String valor = String.valueOf(numeroOrdenPago);
            writer.write(convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          } catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }
        default:
          break;
        }
      }
      else if (rsTotal.getString("tipo_campo").equals("V"))
      {
        switch (Integer.valueOf(rsTotal.getString("campo_totales")).intValue()) {
        case 1:
          try {
            campoTrabajo = "TOTAL APORTE ORGANISMO";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("aporte"), rsTotal.getDouble("multiplicador"), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero")), rsTotal.getString("separador_decimal"), rsTotal.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }
        case 2:
          try {
            campoTrabajo = "TOTAL NOMINA A PAGAR";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("neto"), rsTotal.getDouble("multiplicador"), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero")), rsTotal.getString("separador_decimal"), rsTotal.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }
        case 3:
          try {
            campoTrabajo = "TOTAL RETENCION";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("retencion"), rsTotal.getDouble("multiplicador"), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero")), rsTotal.getString("separador_decimal"), rsTotal.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }
        case 4:
          try {
            campoTrabajo = "TOTAL TRABAJADORES";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("total"), rsTotal.getDouble("multiplicador"), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero")), rsTotal.getString("separador_decimal"), rsTotal.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo);
            log.error(e);
          }
        case 5:
          try {
            campoTrabajo = "CORRELATIVO";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("correlativo"), rsTotal.getDouble("multiplicador"), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero"))));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo, e);
          }
        case 6:
          try {
            campoTrabajo = "TOTAL RETENCION + APORTE ORGANISMO";
            if (rsTotalNominaTrabajadores.next())
              writer.write(convertirNumero(rsTotalNominaTrabajadores.getDouble("retencion") + rsTotalNominaTrabajadores.getDouble("aporte"), rsTotal.getDouble("multiplicador"), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), caracterRelleno(rsTotal.getString("rellenar_cero")), rsTotal.getString("separador_decimal"), rsTotal.getString("separador_miles")));
          }
          catch (Exception e) {
            log.error("Error en la seccion de Totales en el campo " + campoTrabajo, e);
          }

        }

      }

    }

    return hayEncabezado;
  }
}