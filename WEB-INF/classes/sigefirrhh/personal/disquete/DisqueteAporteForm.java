package sigefirrhh.personal.disquete;

import eforserver.common.Resource;
import eforserver.presentation.ListUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.Disquete;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class DisqueteAporteForm
  implements Serializable
{
  static Logger log = Logger.getLogger(DisqueteAporteForm.class.getName());
  private boolean generado;
  private String urlArchivo;
  private String reportName;
  private String selectTipoPersonal;
  private String selectDisquete;
  private String selectRegion;
  private String inicio;
  private String fin;
  private Date fechaProceso = new Date();
  private Date fechaAbono;
  private Disquete disquete;
  private Calendar inicioAux;
  private Calendar finAux;
  private String numeroOrdenPago;
  private long idTipoPersonal;
  private int anio;
  private int mes;
  private int numero;
  private String prestamo;
  private long idRegion;
  private Collection listTipoPersonal;
  private Collection listDisquete;
  private Collection listRegion;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private boolean show;
  private boolean auxShow;
  private TipoPersonal tipoPersonal;
  private long idUnidadAdministradora;
  private Collection listUnidadAdministradora;
  private String ParaQueryPedro = "1";

  public DisqueteAporteForm()
  {
    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.auxShow = false;
    try {
      if (this.idTipoPersonal != 0L)
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public String generar()
  {
    this.idRegion = Long.valueOf(String.valueOf(this.selectRegion)).longValue();
    long idBanco = this.disquete.getBanco().getIdBanco();

    this.generado = false;
    int contador = 0;
    int tope = 0;

    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();

    ResultSet rsEncabezado = null;
    PreparedStatement stEncabezado = null;

    ResultSet rsEncabezado2 = null;
    PreparedStatement stEncabezado2 = null;

    ResultSet rsEncabezado3 = null;
    PreparedStatement stEncabezado3 = null;

    ResultSet rsDetalle = null;
    PreparedStatement stDetalle = null;

    ResultSet rsDetalle2 = null;
    PreparedStatement stDetalle2 = null;

    ResultSet rsTotal = null;
    PreparedStatement stTotal = null;

    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;

    ResultSet rsParametroGobierno = null;
    PreparedStatement stParametroGobierno = null;

    ResultSet rsTotalLph = null;
    PreparedStatement stTotalLph = null;

    StringBuffer sqlTotalLph = new StringBuffer();

    StringBuffer valorBuffer = new StringBuffer();

    Connection connection = Resource.getConnection();
    try
    {
      String conceptoRetencion = this.disquete.getConcepto().getCodConcepto();
      String conceptoRetroactivo = this.disquete.getConcepto().getConceptoRetroactivo().getCodConcepto();

      Random rmd = new Random();
      rmd.nextDouble();

      boolean success = new File(request.getRealPath("disquetes")).mkdir();
      File file = new File(request.getRealPath("disquetes") + "/" + rmd.nextDouble() + ".txt");
      file.createNewFile();

      OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), Charset.forName("ISO-8859-1"));

      Iterator iterTipoPersonal = this.listTipoPersonal.iterator();

      sqlTotalLph.append("select count(distinct t.id_trabajador) as total, sum(monto_deduce) as retencion, sum(monto_aporte)  as aporte");
      sqlTotalLph.append(" from trabajador t, conceptotipopersonal ctp, concepto c,v_historico h,dependencia d,sede s , banco b ,");
      sqlTotalLph.append(" personal p, sueldopromedio sp, administradorauel z, unidadadministradora x ");
      sqlTotalLph.append(" where h.id_trabajador = t.id_trabajador ");
      sqlTotalLph.append("   and t.id_trabajador = sp.id_trabajador ");
      sqlTotalLph.append("   and t.id_personal = p.id_personal ");
      sqlTotalLph.append(" and t.id_dependencia = d.id_dependencia");
      sqlTotalLph.append(" and d.id_administradora_uel = z.id_administradora_uel");
      sqlTotalLph.append(" and z.id_unidad_administradora = x.id_unidad_administradora");
      sqlTotalLph.append(" and d.id_sede = s.id_sede");
      sqlTotalLph.append(" and t.id_banco_lph = b.id_banco");
      sqlTotalLph.append(" and h.mes = ? ");
      sqlTotalLph.append(" and h.anio = ? ");
      sqlTotalLph.append(" and h.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal ");
      sqlTotalLph.append(" and ctp.id_concepto = c.id_concepto ");
      sqlTotalLph.append(" and t.id_dependencia = d.id_dependencia");
      sqlTotalLph.append(" and c.cod_concepto in(?,?) ");

      if ((this.idRegion == 0L) && (this.idTipoPersonal == 0L)) {
        sqlTotalLph.append(" and h.id_tipo_personal <> ? ");
        sqlTotalLph.append(" and s.id_region <> ?");
      }

      if ((this.idRegion != 0L) && (this.idTipoPersonal != 0L)) {
        sqlTotalLph.append(" and h.id_tipo_personal = ? ");
        sqlTotalLph.append(" and s.id_region = ?");
      }

      if ((this.idRegion != 0L) && (this.idTipoPersonal == 0L)) {
        sqlTotalLph.append(" and h.id_tipo_personal <> ? ");
        sqlTotalLph.append(" and s.id_region = ?");
      }

      if ((this.idRegion == 0L) && (this.idTipoPersonal != 0L)) {
        sqlTotalLph.append(" and h.id_tipo_personal = ? ");
        sqlTotalLph.append(" and s.id_region <> ?");
      }

      if (this.numero != 0) {
        sqlTotalLph.append(" and h.semana_quincena = ? ");
      }
      if (this.numero == 0) {
        sqlTotalLph.append(" and h.semana_quincena <> ? ");
      }
      sqlTotalLph.append(" and t.id_banco_lph =  ?");

      if ((this.disquete.getIngresos().equals("S")) && (this.disquete.getEgresos().equals("N"))) {
        sqlTotalLph.append(" and ((t.mes_ingreso =? and t.anio_ingreso = ?) or (t.mes_entrada =? and t.anio_entrada=?))");
      }
      if ((this.disquete.getIngresos().equals("N")) && (this.disquete.getEgresos().equals("N"))) {
        sqlTotalLph.append(" and not ((t.mes_ingreso =? and t.anio_ingreso = ?) or (t.mes_entrada =? and t.anio_entrada=?))");
      }

      if ((this.disquete.getEgresos().equals("S")) && (this.disquete.getIngresos().equals("N"))) {
        sqlTotalLph.append(" and ((t.mes_egreso =? and t.anio_egreso = ?) or (t.mes_salida =? and t.anio_salida=?))");
        sqlTotalLph.append(" and t.estatus = 'E' and fecha_egreso is not null ");
      }

      if (this.idUnidadAdministradora != 0L)
      {
        sqlTotalLph.append(" and x.id_unidad_administradora = ? ");
      }

      stTotalLph = connection.prepareStatement(
        sqlTotalLph.toString(), 
        1000, 
        1007);
      stTotalLph.setLong(1, this.mes);
      stTotalLph.setLong(2, this.anio);
      stTotalLph.setString(3, conceptoRetencion);
      stTotalLph.setString(4, conceptoRetroactivo);
      stTotalLph.setLong(5, this.idTipoPersonal);
      stTotalLph.setLong(6, this.idRegion);
      stTotalLph.setInt(7, this.numero);
      stTotalLph.setLong(8, idBanco);
      if ((this.disquete.getIngresos().equals("S")) || (this.disquete.getIngresos().equals("N")) || (this.disquete.getEgresos().equals("S"))) {
        stTotalLph.setLong(9, this.mes);
        stTotalLph.setLong(10, this.anio);
        stTotalLph.setLong(11, this.mes);
        stTotalLph.setLong(12, this.anio);
        this.ParaQueryPedro = "2";
      }

      if (this.ParaQueryPedro.equals("1"))
      {
        if (this.idUnidadAdministradora != 0L) {
          stTotalLph.setLong(9, this.idUnidadAdministradora);
        }

      }
      else if (this.idUnidadAdministradora != 0L) {
        stTotalLph.setLong(13, this.idUnidadAdministradora);
      }

      rsTotalLph = stTotalLph.executeQuery();

      log.error("........................pasa5");
      StringBuffer sql = new StringBuffer();
      sql.append("select sum(monto_deduce) as retencion, sum (monto_aporte) as aporte, ");
      sql.append(" max(t.cedula) as cedula , ");
      sql.append(" max(p.primer_nombre) as primer_nombre,");
      sql.append(" max(p.segundo_nombre) as segundo_nombre,");
      sql.append(" max(p.primer_apellido) as primer_apellido,");
      sql.append(" max(p.segundo_apellido) as segundo_apellido,");
      sql.append(" max(p.nacionalidad) as nacionalidad,");
      sql.append(" max(t.cuenta_lph) as cuenta_lph, ");
      sql.append(" max(t.cuenta_nomina) as cuenta_nomina, ");
      sql.append(" max(p.sexo) as sexo,");
      sql.append(" max(t.estatus) as estatus,");
      sql.append(" max(t.cod_tipo_personal) as cod_tipo_personal,");
      sql.append(" max(t.forma_pago) as forma_pago,");
      sql.append(" max(p.estado_civil) as estado_civil,");
      sql.append(" max(p.fecha_nacimiento) as fecha_nacimiento,");
      sql.append(" max(t.fecha_ingreso) as fecha_ingreso,");
      sql.append(" max(t.fecha_egreso) as fecha_egreso,");
      sql.append(" max(d.cod_dependencia) as dependencia,");
      sql.append(" max(c.cod_concepto) as concepto,");
      sql.append(" max(c.descripcion) as concepto_descripcion,");
      sql.append(" max(t.codigo_nomina) as codigo_nomina,");
      sql.append(" max(t.sueldo_basico) as sueldo_basico,");
      sql.append(" max(sp.promedio_integral) as promedio_integral,");
      sql.append(" max(sp.promedio_sso) as promedio_sso,");
      sql.append(" max(sp.promedio_spf) as promedio_spf,");
      sql.append(" max(sp.promedio_lph) as promedio_lph,");
      sql.append(" max(sp.promedio_fju) as promedio_fju,");
      sql.append(" max(t.porcentaje_islr) as porcentaje_islr");
      sql.append(" from trabajador t, personal p,  dependencia d, conceptotipopersonal ctp, concepto c,v_historico h, sueldopromedio sp, sede s , banco b, administradorauel z, unidadadministradora x ");
      sql.append(" where h.id_trabajador = t.id_trabajador");
      sql.append(" and t.id_personal = p.id_personal");
      sql.append(" and t.id_trabajador = sp.id_trabajador");
      sql.append(" and t.id_dependencia = d.id_dependencia");
      sql.append(" and d.id_administradora_uel = z.id_administradora_uel");
      sql.append(" and z.id_unidad_administradora = x.id_unidad_administradora");
      sql.append(" and t.id_banco_lph = b.id_banco");
      sql.append(" and d.id_sede = s.id_sede");
      sql.append(" and h.mes = ? ");
      sql.append(" and h.anio = ? ");
      sql.append(" and h.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal ");
      sql.append(" and ctp.id_concepto = c.id_concepto ");
      sql.append(" and c.cod_concepto in(?,?)  ");

      if ((this.idRegion == 0L) && (this.idTipoPersonal == 0L)) {
        sql.append(" and h.id_tipo_personal <> ? ");
        sql.append(" and s.id_region <> ?");
      }
      if ((this.idRegion != 0L) && (this.idTipoPersonal != 0L)) {
        sql.append(" and h.id_tipo_personal = ? ");
        sql.append(" and s.id_region = ?");
      }
      if ((this.idRegion != 0L) && (this.idTipoPersonal == 0L)) {
        sql.append(" and h.id_tipo_personal <> ? ");
        sql.append(" and s.id_region = ?");
      }
      if ((this.idRegion == 0L) && (this.idTipoPersonal != 0L)) {
        sql.append(" and h.id_tipo_personal = ? ");
        sql.append(" and s.id_region <> ?");
      }
      if (this.numero != 0) {
        sql.append(" and h.semana_quincena = ? ");
      }
      if (this.numero == 0) {
        sql.append(" and h.semana_quincena <> ? ");
      }
      sql.append(" and t.id_banco_lph =  ?");

      if ((this.disquete.getIngresos().equals("S")) && (this.disquete.getEgresos().equals("N"))) {
        sql.append(" and ((t.mes_ingreso =? and t.anio_ingreso = ?) or (t.mes_entrada =? and t.anio_entrada=?))");
      }
      if ((this.disquete.getIngresos().equals("N")) && (this.disquete.getEgresos().equals("N"))) {
        sql.append(" and not ((t.mes_ingreso =? and t.anio_ingreso = ?) or (t.mes_entrada =? and t.anio_entrada=?))");
      }
      if ((this.disquete.getEgresos().equals("S")) && (this.disquete.getIngresos().equals("N"))) {
        sql.append(" and ((t.mes_egreso =? and t.anio_egreso = ?) or (t.mes_salida =? and t.anio_salida=?))");
        sql.append(" and t.estatus = 'E' and fecha_egreso is not null ");
      }

      if (this.idUnidadAdministradora != 0L) {
        sql.append(" and x.id_unidad_administradora = ? ");
      }

      sql.append(" group by t.id_trabajador ");
      sql.append(" order by max(t.cedula)");

      stTrabajadores = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTrabajadores.setLong(1, this.mes);
      stTrabajadores.setLong(2, this.anio);
      stTrabajadores.setString(3, conceptoRetencion);
      stTrabajadores.setString(4, conceptoRetroactivo);
      stTrabajadores.setLong(5, this.idTipoPersonal);
      stTrabajadores.setLong(6, this.idRegion);
      stTrabajadores.setInt(7, this.numero);
      stTrabajadores.setLong(8, idBanco);

      if ((this.disquete.getIngresos().equals("S")) || (this.disquete.getIngresos().equals("N")) || (this.disquete.getEgresos().equals("S"))) {
        stTrabajadores.setLong(9, this.mes);
        stTrabajadores.setLong(10, this.anio);
        stTrabajadores.setLong(11, this.mes);
        stTrabajadores.setLong(12, this.anio);
        this.ParaQueryPedro = "2";
      }

      if (this.ParaQueryPedro.equals("1")) {
        if (this.idUnidadAdministradora != 0L) {
          stTrabajadores.setLong(9, this.idUnidadAdministradora);
        }
      }
      else if (this.idUnidadAdministradora != 0L) {
        stTrabajadores.setLong(13, this.idUnidadAdministradora);
      }

      rsTrabajadores = stTrabajadores.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from parametrogobierno ");

      stParametroGobierno = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      rsParametroGobierno = stParametroGobierno.executeQuery();

      rsParametroGobierno.first();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '1' order by numero_campo");
      stEncabezado = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stEncabezado.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsEncabezado = stEncabezado.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '4' order by numero_campo");
      stEncabezado2 = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stEncabezado2.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsEncabezado2 = stEncabezado2.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '5' order by numero_campo");
      stEncabezado3 = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stEncabezado3.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsEncabezado3 = stEncabezado3.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '2' order by numero_campo");
      stDetalle = connection.prepareStatement(
        sql.toString(), 
        1000, 
        1007);
      stDetalle.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsDetalle = stDetalle.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '6' order by numero_campo");
      stDetalle2 = connection.prepareStatement(
        sql.toString(), 
        1000, 
        1007);
      stDetalle2.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsDetalle2 = stDetalle2.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '3' order by numero_campo");
      stTotal = connection.prepareStatement(
        sql.toString(), 
        1000, 
        1007);
      stTotal.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsTotal = stTotal.executeQuery();

      boolean hayEncabezado = false;

      hayEncabezado = ToolsDisquete.armaEncabezado(rsEncabezado, rsTotalLph, writer, 1, hayEncabezado, this.fechaProceso, this.fechaAbono, this.numeroOrdenPago, this.anio, this.mes);

      hayEncabezado = ToolsDisquete.armaEncabezado(rsEncabezado2, rsTotalLph, writer, 2, hayEncabezado, this.fechaProceso, this.fechaAbono, this.numeroOrdenPago, this.anio, this.mes);

      hayEncabezado = ToolsDisquete.armaEncabezado(rsEncabezado3, rsTotalLph, writer, 3, hayEncabezado, this.fechaProceso, this.fechaAbono, this.numeroOrdenPago, this.anio, this.mes);

      hayEncabezado = ToolsDisquete.armaDetalle(rsParametroGobierno, rsDetalle, rsTrabajadores, writer, 1, hayEncabezado, this.fechaProceso, this.fechaAbono, this.numeroOrdenPago, this.anio, this.mes);

      hayEncabezado = ToolsDisquete.armaDetalle(rsParametroGobierno, rsDetalle2, rsTrabajadores, writer, 1, hayEncabezado, this.fechaProceso, this.fechaAbono, this.numeroOrdenPago, this.anio, this.mes);

      hayEncabezado = ToolsDisquete.armaTotal(rsTotal, rsTotalLph, writer, hayEncabezado, this.fechaProceso, this.fechaAbono, this.numeroOrdenPago, this.anio, this.mes);

      writer.flush();
      writer.close();
      this.generado = true;

      this.urlArchivo = ("/sigefirrhh/disquetes/" + file.getName());

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalByConcepto(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador(), "5003");
      this.listDisquete = this.definicionesFacade.findDisqueteByTipoDisquete("A", this.login.getIdOrganismo());
      this.listRegion = this.estructuraFacade.findAllRegion();

      this.listUnidadAdministradora = this.estructuraFacade.findUnidadAdministradoraByOrganismo(this.login.getIdOrganismo());
      log.error("vacio:" + this.listUnidadAdministradora.isEmpty());
    }
    catch (Exception e)
    {
      log.error("Excepción controlada: ", e);
      this.listTipoPersonal = new ArrayList();

      this.listUnidadAdministradora = new ArrayList();
    }
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getListDisquete() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listDisquete, "sigefirrhh.base.definiciones.Disquete");
  }
  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }

  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
  }
  public String getFin() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }
  public void setFin(String string) {
    this.fin = string;
  }
  public void setInicio(String string) {
    this.inicio = string;
  }
  public boolean isShow() {
    return this.auxShow;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public void setIdTipoPersonal(long l) {
    this.idTipoPersonal = l;
  }

  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }
  public String getSelectDisquete() {
    return this.selectDisquete;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }

  public void setSelectDisquete(String string) {
    this.selectDisquete = string;
    try {
      this.disquete = this.definicionesFacade.findDisqueteById(Long.valueOf(this.selectDisquete).longValue());
    } catch (Exception localException) {
    }
  }

  public void setSelectRegion(String string) {
    this.selectRegion = string;
  }

  public Date getFechaAbono() {
    return this.fechaAbono;
  }
  public Date getFechaProceso() {
    return this.fechaProceso;
  }
  public void setFechaAbono(Date date) {
    this.fechaAbono = date;
  }
  public void setFechaProceso(Date date) {
    this.fechaProceso = date;
  }
  public boolean isGenerado() {
    return this.generado;
  }
  public String getUrlArchivo() {
    return this.urlArchivo;
  }

  public String getNumeroOrdenPago()
  {
    return this.numeroOrdenPago;
  }
  public void setNumeroOrdenPago(String numeroOrdenPago) {
    this.numeroOrdenPago = numeroOrdenPago;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
  public String getPrestamo() {
    return this.prestamo;
  }
  public void setPrestamo(String prestamo) {
    this.prestamo = prestamo;
  }
  public int getNumero() {
    return this.numero;
  }
  public void setNumero(int numero) {
    this.numero = numero;
  }
  public long getIdUnidadAdministradora() {
    return this.idUnidadAdministradora;
  }

  public void setIdUnidadAdministradora(long l) {
    this.idUnidadAdministradora = l;
  }

  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }
}