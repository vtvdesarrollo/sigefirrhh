package sigefirrhh.personal.disquete;

import eforserver.common.Resource;
import eforserver.presentation.ListUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.Disquete;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class DisqueteFideicomisoForm
  implements Serializable
{
  static Logger log = Logger.getLogger(DisqueteFideicomisoForm.class.getName());
  private boolean generado;
  private String urlArchivo;
  private String reportName;
  private String selectTipoPersonal;
  private String selectDisquete;
  private String selectRegion;
  private String inicio;
  private String fin;
  private java.util.Date fechaProceso = new java.util.Date();
  private java.util.Date fechaAbono;
  private Disquete disquete;
  private Calendar inicioAux;
  private Calendar finAux;
  private String numeroOrdenPago;
  private long idTipoPersonal;
  private int anio;
  private int mes;
  private long idRegion;
  private Collection listTipoPersonal;
  private Collection listDisquete;
  private Collection listRegion;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private boolean show;
  private boolean auxShow;
  private String diaAdicional = "N";

  public DisqueteFideicomisoForm()
  {
    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    this.fechaAbono = new java.util.Date();

    refresh();
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    this.idTipoPersonal = Long.valueOf((String)event.getNewValue()).longValue();
    this.auxShow = false;
    try {
      if (this.idTipoPersonal != 0L)
        this.auxShow = true;
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void generar()
  {
    this.idRegion = Long.valueOf(String.valueOf(this.selectRegion)).longValue();
    long idBanco = this.disquete.getBanco().getIdBanco();

    this.generado = false;
    int contador = 0;
    int tope = 0;

    StringBuffer sqlVerificar = new StringBuffer();
    ResultSet rsVerificar = null;

    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();

    ResultSet rsEncabezado = null;
    PreparedStatement stEncabezado = null;

    ResultSet rsEncabezado2 = null;
    PreparedStatement stEncabezado2 = null;

    ResultSet rsEncabezado3 = null;
    PreparedStatement stEncabezado3 = null;

    ResultSet rsDetalle = null;
    PreparedStatement stDetalle = null;

    ResultSet rsTotal = null;
    PreparedStatement stTotal = null;

    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;

    ResultSet rsTotalFideicomiso = null;
    PreparedStatement stTotalFideicomiso = null;

    StringBuffer sqlTotalFideicomiso = new StringBuffer();

    StringBuffer valorBuffer = new StringBuffer();

    Connection connection = Resource.getConnection();
    try {
      Random rmd = new Random();
      rmd.nextDouble();

      if ((this.mes <= 0) || (this.mes > 12) || (this.anio <= 1900)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede generar disquete. Mes y Año deben tener valores válidos.", ""));
        return;
      }

      if (this.idTipoPersonal == 0L) {
        try {
          PreparedStatement stVerificar = null;
          sqlVerificar = new StringBuffer();
          sqlVerificar.append("select count(*) as contador from tipopersonal tp where tp.calcula_prestaciones  = 'S' ");
          sqlVerificar.append("and tp.id_tipo_personal not in ");
          sqlVerificar.append("(select pm.id_tipo_personal from prestacionesmensuales pm where ");
          sqlVerificar.append(" pm.mes = ?  and ");
          sqlVerificar.append(" pm.anio = ? )");
          sqlVerificar.append(" and tp.id_tipo_personal  in (select pm1.id_tipo_personal from prestacionesmensuales pm1 )");
          stVerificar = connection.prepareStatement(
            sqlVerificar.toString(), 
            1000, 
            1007);
          stVerificar.setLong(1, this.mes);
          stVerificar.setLong(2, this.anio);
          rsVerificar = stVerificar.executeQuery();
          while (rsVerificar.next()) {
            long contTP = 0L;
            contTP = rsVerificar.getLong(1);
            if (contTP != 0L) {
              context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: No se puede generar disquete. Existen tipos de personal para los que no se ha corrido el cálculo de prestaciones", ""));
              return;
            }
          }
        } catch (Exception e) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un Error: " + e, ""));
          log.error("Excepcion controlada:", e);
          connection = null;
          return;
        }
      }

      boolean success = new File(request.getRealPath("disquetes")).mkdir();

      File file = new File(request.getRealPath("disquetes") + "/" + rmd.nextDouble() + ".txt");
      file.createNewFile();

      OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), Charset.forName("ISO-8859-1"));

      Statement stActualizar = null;
      stActualizar = connection.createStatement();
      StringBuffer sql = new StringBuffer();
      sql.append("update banco set correlativo = correlativo + 1 where id_banco = " + idBanco);
      stActualizar.addBatch(sql.toString());
      stActualizar.executeBatch();

      if ((this.idRegion == 0L) && (this.idTipoPersonal == 0L))
      {
        sqlTotalFideicomiso.append("select count(distinct pm.id_trabajador) as total, sum(monto_prestaciones) as prestaciones ,  sum(monto_adicional) as adicional ");
        sqlTotalFideicomiso.append(" from prestacionesmensuales pm, trabajador t, banco b ");
        sqlTotalFideicomiso.append(" where pm.id_trabajador = t.id_trabajador");
        sqlTotalFideicomiso.append(" and t.id_banco_fid = b.id_banco");
        sqlTotalFideicomiso.append(" and t.id_banco_fid = ? ");
        sqlTotalFideicomiso.append(" and pm.mes = ? ");
        sqlTotalFideicomiso.append(" and pm.anio = ? ");
        if (this.disquete.getIngresos().equals("S"))
          sqlTotalFideicomiso.append(" and t.id_personal in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        else if (this.disquete.getIngresos().equals("N")) {
          sqlTotalFideicomiso.append(" and t.id_personal not in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        }

        stTotalFideicomiso = connection.prepareStatement(
          sqlTotalFideicomiso.toString(), 
          1000, 
          1007);
        stTotalFideicomiso.setLong(1, idBanco);
        stTotalFideicomiso.setLong(2, this.mes);
        stTotalFideicomiso.setLong(3, this.anio);
        rsTotalFideicomiso = stTotalFideicomiso.executeQuery();
      }
      else if ((this.idRegion != 0L) && (this.idTipoPersonal != 0L))
      {
        sqlTotalFideicomiso.append("select count(distinct pm.id_trabajador) as total, sum(monto_prestaciones) as prestaciones ,  sum(monto_adicional) as adicional ");
        sqlTotalFideicomiso.append(" from prestacionesmensuales pm, trabajador t, banco b, ");
        sqlTotalFideicomiso.append(" dependencia d, sede s");
        sqlTotalFideicomiso.append(" where un.id_trabajador = t.id_trabajador");
        sqlTotalFideicomiso.append(" and t.id_banco_fid = b.id_banco");
        sqlTotalFideicomiso.append(" and t.id_dependencia = d.id_dependencia");
        sqlTotalFideicomiso.append(" and d.id_sede = s.id_sede");
        sqlTotalFideicomiso.append(" and t.id_tipo_personal = ? ");
        sqlTotalFideicomiso.append(" and s.id_region = ?");
        sqlTotalFideicomiso.append(" and t.id_banco_fid = ? ");
        sqlTotalFideicomiso.append(" and pm.mes = ? ");
        sqlTotalFideicomiso.append(" and pm.anio = ? ");
        if (this.disquete.getIngresos().equals("S"))
          sqlTotalFideicomiso.append(" and t.id_personal in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        else if (this.disquete.getIngresos().equals("N")) {
          sqlTotalFideicomiso.append(" and t.id_personal not in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        }
        stTotalFideicomiso = connection.prepareStatement(
          sqlTotalFideicomiso.toString(), 
          1000, 
          1007);
        stTotalFideicomiso.setLong(1, this.idTipoPersonal);
        stTotalFideicomiso.setLong(2, this.idRegion);
        stTotalFideicomiso.setLong(3, idBanco);
        stTotalFideicomiso.setLong(4, this.mes);
        stTotalFideicomiso.setLong(5, this.anio);
        rsTotalFideicomiso = stTotalFideicomiso.executeQuery();
      }
      else if ((this.idRegion == 0L) && (this.idTipoPersonal != 0L))
      {
        sqlTotalFideicomiso.append("select count(distinct pm.id_trabajador) as total, sum(monto_prestaciones) as prestaciones ,  sum(monto_adicional) as adicional ");
        sqlTotalFideicomiso.append(" from prestacionesmensuales pm, trabajador t, banco b, ");
        sqlTotalFideicomiso.append(" dependencia d ");
        sqlTotalFideicomiso.append(" where pm.id_trabajador = t.id_trabajador");
        sqlTotalFideicomiso.append(" and t.id_banco_fid = b.id_banco");
        sqlTotalFideicomiso.append(" and t.id_dependencia = d.id_dependencia");
        sqlTotalFideicomiso.append(" and t.id_tipo_personal = ? ");
        sqlTotalFideicomiso.append(" and t.id_banco_fid = ? ");
        sqlTotalFideicomiso.append(" and pm.mes = ? ");
        sqlTotalFideicomiso.append(" and pm.anio = ? ");
        if (this.disquete.getIngresos().equals("S"))
          sqlTotalFideicomiso.append(" and t.id_personal in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        else if (this.disquete.getIngresos().equals("N")) {
          sqlTotalFideicomiso.append(" and t.id_personal not in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        }
        stTotalFideicomiso = connection.prepareStatement(
          sqlTotalFideicomiso.toString(), 
          1000, 
          1007);
        stTotalFideicomiso.setLong(1, this.idTipoPersonal);
        stTotalFideicomiso.setLong(2, idBanco);
        stTotalFideicomiso.setLong(3, this.mes);
        stTotalFideicomiso.setLong(4, this.anio);
        rsTotalFideicomiso = stTotalFideicomiso.executeQuery();
      }
      else if ((this.idRegion != 0L) && (this.idTipoPersonal == 0L))
      {
        sqlTotalFideicomiso.append("select count(distinct pm.id_trabajador) as total, sum(monto_prestaciones) as prestaciones ,  sum(monto_adicional) as adicional ");
        sqlTotalFideicomiso.append(" from prestacionesmensuales pm, trabajador t, banco b, ");
        sqlTotalFideicomiso.append(" dependencia d, sede s ");
        sqlTotalFideicomiso.append(" where pm.id_trabajador = t.id_trabajador");
        sqlTotalFideicomiso.append(" and t.id_banco_fid = b.id_banco");
        sqlTotalFideicomiso.append(" and t.id_dependencia = d.id_dependencia");
        sqlTotalFideicomiso.append(" and d.id_sede = s.id_sede");
        sqlTotalFideicomiso.append(" and t.id_tipo_personal = ? ");
        sqlTotalFideicomiso.append(" and s.id_region = ?");
        sqlTotalFideicomiso.append(" and t.id_banco_fid = ? ");
        sqlTotalFideicomiso.append(" and pm.mes = ? ");
        sqlTotalFideicomiso.append(" and pm.anio = ? ");
        if (this.disquete.getIngresos().equals("S"))
          sqlTotalFideicomiso.append(" and t.id_personal in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        else if (this.disquete.getIngresos().equals("N")) {
          sqlTotalFideicomiso.append(" and t.id_personal not in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        }
        stTotalFideicomiso = connection.prepareStatement(
          sqlTotalFideicomiso.toString(), 
          1000, 
          1007);
        stTotalFideicomiso.setLong(1, this.idTipoPersonal);
        stTotalFideicomiso.setLong(2, this.idRegion);
        stTotalFideicomiso.setLong(3, idBanco);
        stTotalFideicomiso.setLong(4, this.mes);
        stTotalFideicomiso.setLong(5, this.anio);
        rsTotalFideicomiso = stTotalFideicomiso.executeQuery();
      }

      if ((this.idRegion == 0L) && (this.idTipoPersonal == 0L)) {
        sql = new StringBuffer();
        sql.append("select sum(monto_prestaciones) as prestaciones, sum(monto_adicional) as adicional, sum(monto_prestaciones+monto_adicional) as total_prestaciones, ");
        sql.append(" max(t.cedula) as cedula , ");
        sql.append(" max(p.primer_nombre) as primer_nombre,");
        sql.append(" max(p.segundo_nombre) as segundo_nombre,");
        sql.append(" max(p.primer_apellido) as primer_apellido,");
        sql.append(" max(p.segundo_apellido) as segundo_apellido,");
        sql.append(" max(p.nacionalidad) as nacionalidad,");
        sql.append(" max(p.direccion_residencia) as direccion,");
        sql.append(" max(p.telefono_residencia) as telefono,");
        sql.append(" max(p.estado_civil) as estado_civil,");
        sql.append(" max(t.cuenta_fid) as cuenta, ");
        sql.append(" max(t.fecha_ingreso) as fecha_ingreso, ");
        sql.append(" max(t.anio_ingreso) as anio_ingreso, ");
        sql.append(" max(t.mes_ingreso) as mes_ingreso, ");
        sql.append(" max(t.dia_ingreso) as dia_ingreso, ");
        sql.append(" max(d.cod_dependencia) as dependencia,");
        sql.append(" max(tp.cod_tipo_personal) as tipopersonal,");
        sql.append(" max(pv.fecha_apertura_fideicomiso) as fecha_apertura_fideicomiso");
        sql.append(" from prestacionesmensuales pm, trabajador t, personal p, banco b, dependencia d, parametrovarios pv , tipopersonal tp");
        sql.append(" where pm.id_trabajador = t.id_trabajador");
        sql.append(" and t.id_banco_fid = b.id_banco");
        sql.append(" and t.id_personal = p.id_personal");
        sql.append(" and t.id_dependencia = d.id_dependencia");
        sql.append(" and t.id_tipo_personal = pv.id_tipo_personal");
        sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and t.id_banco_fid = ?");
        sql.append(" and pm.mes = ? ");
        sql.append(" and pm.anio = ? ");
        if (this.disquete.getIngresos().equals("S"))
          sql.append(" and t.id_personal in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        else if (this.disquete.getIngresos().equals("N")) {
          sql.append(" and t.id_personal not in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        }
        sql.append(" group by t.id_trabajador");
        sql.append(" order by max(t.cedula)");

        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stTrabajadores.setLong(1, idBanco);
        stTrabajadores.setInt(2, this.mes);
        stTrabajadores.setInt(3, this.anio);
        rsTrabajadores = stTrabajadores.executeQuery();
      }
      else if ((this.idRegion != 0L) && (this.idTipoPersonal != 0L)) {
        sql = new StringBuffer();
        sql.append("select sum(monto_prestaciones) as prestaciones, sum(monto_adicional) as adicional, sum(monto_prestaciones+monto_adicional) as total_prestaciones,");
        sql.append(" max(t.cedula) as cedula , ");
        sql.append(" max(p.primer_nombre) as primer_nombre,");
        sql.append(" max(p.segundo_nombre) as segundo_nombre,");
        sql.append(" max(p.primer_apellido) as primer_apellido,");
        sql.append(" max(p.segundo_apellido) as segundo_apellido,");
        sql.append(" max(p.nacionalidad) as nacionalidad,");
        sql.append(" max(p.direccion_residencia) as direccion,");
        sql.append(" max(p.telefono_residencia) as telefono,");
        sql.append(" max(p.estado_civil) as estado_civil,");
        sql.append(" max(t.cuenta_fid) as cuenta, ");
        sql.append(" max(t.fecha_ingreso) as fecha_ingreso, ");
        sql.append(" max(t.anio_ingreso) as anio_ingreso, ");
        sql.append(" max(t.mes_ingreso) as mes_ingreso, ");
        sql.append(" max(t.dia_ingreso) as dia_ingreso, ");
        sql.append(" max(d.cod_dependencia) as dependencia,");
        sql.append(" max(tp.cod_tipo_personal) as tipopersonal,");
        sql.append(" max(pv.fecha_apertura_fideicomiso) as fecha_apertura_fideicomiso");
        sql.append(" from prestacionesmensuales pm, trabajador t, personal p, banco b, ");
        sql.append(" dependencia d, sede s, parametrovarios pv  , tipopersonal tp");
        sql.append(" where pm.id_trabajador = t.id_trabajador");
        sql.append(" and t.id_banco_fid = b.id_banco");
        sql.append(" and t.id_personal = p.id_personal");
        sql.append(" and t.id_dependencia = d.id_dependencia");
        sql.append(" and d.id_sede = s.id_sede");
        sql.append(" and t.id_tipo_personal = pv.id_tipo_personal");
        sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and t.id_tipo_personal = ?");
        sql.append(" and s.id_region = ?");
        sql.append(" and t.id_banco_fid = ?");
        sql.append(" and pm.mes = ? ");
        sql.append(" and pm.anio = ? ");
        if (this.disquete.getIngresos().equals("S"))
          sql.append(" and t.id_personal in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        else if (this.disquete.getIngresos().equals("N")) {
          sql.append(" and t.id_personal not in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        }
        sql.append(" group by t.id_trabajador");
        sql.append(" order by max(t.cedula)");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stTrabajadores.setLong(1, this.idTipoPersonal);
        stTrabajadores.setLong(2, this.idRegion);
        stTrabajadores.setLong(3, idBanco);
        stTrabajadores.setInt(4, this.mes);
        stTrabajadores.setInt(5, this.anio);
        rsTrabajadores = stTrabajadores.executeQuery();
      }
      else if ((this.idRegion == 0L) && (this.idTipoPersonal != 0L)) {
        sql = new StringBuffer();
        sql.append("select sum(monto_prestaciones) as prestaciones, sum(monto_adicional) as adicional, sum(monto_prestaciones+monto_adicional) as total_prestaciones,");
        sql.append(" max(t.cedula) as cedula , ");
        sql.append(" max(p.primer_nombre) as primer_nombre,");
        sql.append(" max(p.segundo_nombre) as segundo_nombre,");
        sql.append(" max(p.primer_apellido) as primer_apellido,");
        sql.append(" max(p.segundo_apellido) as segundo_apellido,");
        sql.append(" max(p.nacionalidad) as nacionalidad,");
        sql.append(" max(p.direccion_residencia) as direccion,");
        sql.append(" max(p.telefono_residencia) as telefono,");
        sql.append(" max(p.estado_civil) as estado_civil,");
        sql.append(" max(t.cuenta_fid) as cuenta, ");
        sql.append(" max(t.fecha_ingreso) as fecha_ingreso, ");
        sql.append(" max(t.anio_ingreso) as anio_ingreso, ");
        sql.append(" max(t.mes_ingreso) as mes_ingreso, ");
        sql.append(" max(t.dia_ingreso) as dia_ingreso, ");
        sql.append(" max(d.cod_dependencia) as dependencia,");
        sql.append(" max(pv.fecha_apertura_fideicomiso) as fecha_apertura_fideicomiso, ");
        sql.append(" max(tp.cod_tipo_personal) as tipopersonal");
        sql.append(" from prestacionesmensuales pm, trabajador t, personal p, banco b, dependencia d, parametrovarios pv , tipopersonal tp");
        sql.append(" where pm.id_trabajador = t.id_trabajador");
        sql.append(" and t.id_banco_fid = b.id_banco");
        sql.append(" and t.id_personal = p.id_personal");
        sql.append(" and t.id_dependencia = d.id_dependencia");
        sql.append(" and t.id_tipo_personal = pv.id_tipo_personal");
        sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and t.id_tipo_personal = ?");
        sql.append(" and t.id_banco_fid = ?");
        sql.append(" and pm.mes = ? ");
        sql.append(" and pm.anio = ? ");
        if (this.disquete.getIngresos().equals("S"))
          sql.append(" and t.id_personal in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        else if (this.disquete.getIngresos().equals("N")) {
          sql.append(" and t.id_personal not in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        }
        sql.append(" group by t.id_trabajador");
        sql.append(" order by max(t.cedula)");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stTrabajadores.setLong(1, this.idTipoPersonal);
        stTrabajadores.setLong(2, idBanco);
        stTrabajadores.setInt(3, this.mes);
        stTrabajadores.setInt(4, this.anio);
        rsTrabajadores = stTrabajadores.executeQuery();
        log.error(sql.toString());
      } else if ((this.idRegion != 0L) && (this.idTipoPersonal == 0L)) {
        sql = new StringBuffer();
        sql.append("select sum(monto_prestaciones) as prestaciones, sum(monto_adicional) as adicional, sum(monto_prestaciones+monto_adicional) as total_prestaciones,");
        sql.append(" max(t.cedula) as cedula , ");
        sql.append(" max(p.primer_nombre) as primer_nombre,");
        sql.append(" max(p.segundo_nombre) as segundo_nombre,");
        sql.append(" max(p.primer_apellido) as primer_apellido,");
        sql.append(" max(p.segundo_apellido) as segundo_apellido,");
        sql.append(" max(p.nacionalidad) as nacionalidad,");
        sql.append(" max(p.direccion_residencia) as direccion,");
        sql.append(" max(p.telefono_residencia) as telefono,");
        sql.append(" max(p.estado_civil) as estado_civil,");
        sql.append(" max(t.cuenta_fid) as cuenta, ");
        sql.append(" max(t.fecha_ingreso) as fecha_ingreso, ");
        sql.append(" max(t.anio_ingreso) as anio_ingreso, ");
        sql.append(" max(t.mes_ingreso) as mes_ingreso, ");
        sql.append(" max(t.dia_ingreso) as dia_ingreso, ");
        sql.append(" max(d.cod_dependencia) as dependencia,");
        sql.append(" max(pv.fecha_apertura_fideicomiso) as fecha_apertura_fideicomiso,");
        sql.append(" max(tp.cod_tipo_personal) as tipopersonal");
        sql.append(" from prestacionesmensuales pm, trabajador t, personal p, banco b, ");
        sql.append(" dependencia d, sede s, parametrovarios pv  , tipopersonal tp");
        sql.append(" where pm.id_trabajador = t.id_trabajador");
        sql.append(" and t.id_banco_fid = b.id_banco");
        sql.append(" and t.id_personal = p.id_personal");
        sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and t.id_dependencia = d.id_dependencia");
        sql.append(" and d.id_sede = s.id_sede");
        sql.append(" and t.id_tipo_personal = pv.id_tipo_personal");
        sql.append(" and s.id_region = ?");
        sql.append(" and t.id_banco_fid = ?");
        sql.append(" and pm.mes = ? ");
        sql.append(" and pm.anio = ? ");
        if (this.disquete.getIngresos().equals("S"))
          sql.append(" and t.id_personal in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        else if (this.disquete.getIngresos().equals("N")) {
          sql.append(" and t.id_personal not in(select id_personal from prestacionesmensuales group by id_personal having count(id_personal)=1) ");
        }
        sql.append(" group by t.id_trabajador");
        sql.append(" order by max(t.cedula)");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stTrabajadores.setLong(1, this.idRegion);
        stTrabajadores.setLong(2, idBanco);
        stTrabajadores.setInt(3, this.mes);
        stTrabajadores.setInt(4, this.anio);
        rsTrabajadores = stTrabajadores.executeQuery();
      }

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '1' order by numero_campo");
      stEncabezado = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stEncabezado.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsEncabezado = stEncabezado.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '4' order by numero_campo");
      stEncabezado2 = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stEncabezado2.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsEncabezado2 = stEncabezado2.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '5' order by numero_campo");
      stEncabezado3 = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stEncabezado3.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsEncabezado3 = stEncabezado3.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '2' order by numero_campo");
      stDetalle = connection.prepareStatement(
        sql.toString(), 
        1000, 
        1007);
      stDetalle.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsDetalle = stDetalle.executeQuery();

      sql = new StringBuffer();
      sql.append("select * from detalledisquete where id_disquete = ? ");
      sql.append(" and tipo_registro = '3' order by numero_campo");
      stTotal = connection.prepareStatement(
        sql.toString(), 
        1000, 
        1007);
      stTotal.setLong(1, Long.valueOf(this.selectDisquete).longValue());
      rsTotal = stTotal.executeQuery();

      boolean hayEncabezado = false;
      while (rsEncabezado.next()) {
        hayEncabezado = true;
        rsTotalFideicomiso.beforeFirst();
        if (rsEncabezado.getString("tipo_campo").equals("F"))
          writer.write(ToolsDisquete.convertirLetras(rsEncabezado.getString("campo_usuario"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), " "));
        else if (rsEncabezado.getString("tipo_campo").equals("E"))
          switch (Integer.valueOf(rsEncabezado.getString("campo_entrada")).intValue())
          {
          case 1:
            String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
            valor = valor.substring(2);
            String relleno;
            String relleno;
            if (rsEncabezado.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 2:
            String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
            String relleno;
            String relleno;
            if (rsEncabezado.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));

            break;
          case 3:
            String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
            valor = valor.substring(2);
            String relleno;
            String relleno;
            if (rsEncabezado.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 4:
            String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
            String relleno;
            String relleno;
            if (rsEncabezado.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));

            break;
          case 5:
            String valor = String.valueOf(this.fechaAbono.getDate());
            String relleno;
            String relleno;
            if (rsEncabezado.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 6:
            String valor = String.valueOf(this.fechaProceso.getDate());
            String relleno;
            String relleno;
            if (rsEncabezado.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 7:
            valorBuffer.append(String.valueOf(this.fechaAbono.getHours() + 1));
            valorBuffer.append(String.valueOf(this.fechaAbono.getMinutes() + 1));
            valorBuffer.append(String.valueOf(this.fechaAbono.getSeconds() + 1));
            String relleno;
            String relleno;
            if (rsEncabezado.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 8:
            valorBuffer.append(String.valueOf(this.fechaProceso.getHours() + 1));
            valorBuffer.append(String.valueOf(this.fechaProceso.getMinutes() + 1));
            valorBuffer.append(String.valueOf(this.fechaProceso.getSeconds() + 1));
            String relleno;
            String relleno;
            if (rsEncabezado.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 9:
            String valor = String.valueOf(this.fechaAbono.getMonth() + 1);
            String relleno;
            String relleno;
            if (rsEncabezado.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 10:
            String valor = String.valueOf(this.fechaProceso.getMonth() + 1);
            String relleno;
            String relleno;
            if (rsEncabezado.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 11:
            String valor = String.valueOf(this.numeroOrdenPago);
            String relleno;
            String relleno;
            if (rsEncabezado.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
          default:
            break;
          }
        else if (rsEncabezado.getString("tipo_campo").equals("V")) {
          switch (Integer.valueOf(rsEncabezado.getString("campo_totales")).intValue())
          {
          case 4:
            log.error("encabezado 4");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsEncabezado.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("total"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            }

            break;
          case 5:
            log.error("encabezado 5");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsEncabezado.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("correlativo"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            }

            break;
          case 7:
            log.error("encabezado 2");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsEncabezado.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("prestaciones"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            }

            break;
          case 8:
            log.error("encabezado 2");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsEncabezado.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("adicional"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            }

            break;
          case 9:
            log.warn("encabezado 2");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsEncabezado.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("prestaciones") + rsTotalFideicomiso.getDouble("adicional"), rsEncabezado.getDouble("multiplicador"), rsEncabezado.getInt("longitud_campo"), rsEncabezado.getString("alineacion_campo").equals("D"), relleno));
            }

            break;
          case 6:
          }

        }

      }

      if (hayEncabezado) {
        writer.write("\n");
      }
      hayEncabezado = false;

      while (rsEncabezado2.next()) {
        hayEncabezado = true;
        rsTotalFideicomiso.beforeFirst();
        if (rsEncabezado2.getString("tipo_campo").equals("F")) {
          log.warn("encabezado fijo");
          writer.write(ToolsDisquete.convertirLetras(rsEncabezado2.getString("campo_usuario"), rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), " "));
        } else if (rsEncabezado2.getString("tipo_campo").equals("E")) {
          switch (Integer.valueOf(rsEncabezado2.getString("campo_entrada")).intValue())
          {
          case 1:
            String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
            valor = valor.substring(2);
            String relleno;
            String relleno;
            if (rsEncabezado2.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 2:
            String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
            String relleno;
            String relleno;
            if (rsEncabezado2.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));

            break;
          case 3:
            String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
            valor = valor.substring(2);
            String relleno;
            String relleno;
            if (rsEncabezado2.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 4:
            String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
            String relleno;
            String relleno;
            if (rsEncabezado2.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));

            break;
          case 5:
            String valor = String.valueOf(this.fechaAbono.getDate());
            String relleno;
            String relleno;
            if (rsEncabezado2.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 6:
            String valor = String.valueOf(this.fechaProceso.getDate());
            String relleno;
            String relleno;
            if (rsEncabezado2.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 7:
            valorBuffer.append(String.valueOf(this.fechaAbono.getHours() + 1));
            valorBuffer.append(String.valueOf(this.fechaAbono.getMinutes() + 1));
            valorBuffer.append(String.valueOf(this.fechaAbono.getSeconds() + 1));
            String relleno;
            String relleno;
            if (rsEncabezado2.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 8:
            valorBuffer.append(String.valueOf(this.fechaProceso.getHours() + 1));
            valorBuffer.append(String.valueOf(this.fechaProceso.getMinutes() + 1));
            valorBuffer.append(String.valueOf(this.fechaProceso.getSeconds() + 1));
            String relleno;
            String relleno;
            if (rsEncabezado2.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 9:
            String valor = String.valueOf(this.fechaAbono.getMonth() + 1);
            String relleno;
            String relleno;
            if (rsEncabezado2.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 10:
            String valor = String.valueOf(this.fechaProceso.getMonth() + 1);
            String relleno;
            String relleno;
            if (rsEncabezado2.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 11:
            String valor = String.valueOf(this.numeroOrdenPago);
            String relleno;
            String relleno;
            if (rsEncabezado2.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
          default:
            break;
          } } else if (rsEncabezado2.getString("tipo_campo").equals("V")) {
          switch (Integer.valueOf(rsEncabezado2.getString("campo_totales")).intValue())
          {
          case 2:
            log.warn("encabezado 2");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsEncabezado2.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("neto"), rsEncabezado2.getDouble("multiplicador"), rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
            }

            break;
          case 4:
            log.warn("encabezado 4");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsEncabezado2.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("total"), rsEncabezado2.getDouble("multiplicador"), rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
            }

          case 5:
            log.warn("encabezado 5");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsEncabezado2.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("correlativo"), rsEncabezado2.getDouble("multiplicador"), rsEncabezado2.getInt("longitud_campo"), rsEncabezado2.getString("alineacion_campo").equals("D"), relleno));
            }
            break;
          case 3:
          }
        }

      }

      if (hayEncabezado) {
        writer.write("\n");
      }
      hayEncabezado = false;

      while (rsEncabezado3.next()) {
        hayEncabezado = true;
        rsTotalFideicomiso.beforeFirst();
        if (rsEncabezado3.getString("tipo_campo").equals("F")) {
          log.warn("encabezado fijo");
          writer.write(ToolsDisquete.convertirLetras(rsEncabezado3.getString("campo_usuario"), rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), " "));
        } else if (rsEncabezado3.getString("tipo_campo").equals("E")) {
          switch (Integer.valueOf(rsEncabezado3.getString("campo_entrada")).intValue())
          {
          case 1:
            String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
            valor = valor.substring(2);
            String relleno;
            String relleno;
            if (rsEncabezado3.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 2:
            String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
            String relleno;
            String relleno;
            if (rsEncabezado3.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));

            break;
          case 3:
            String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
            valor = valor.substring(2);
            String relleno;
            String relleno;
            if (rsEncabezado3.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 4:
            String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
            String relleno;
            String relleno;
            if (rsEncabezado3.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));

            break;
          case 5:
            String valor = String.valueOf(this.fechaAbono.getDate());
            String relleno;
            String relleno;
            if (rsEncabezado3.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 6:
            String valor = String.valueOf(this.fechaProceso.getDate());
            String relleno;
            String relleno;
            if (rsEncabezado3.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 7:
            valorBuffer.append(String.valueOf(this.fechaAbono.getHours() + 1));
            valorBuffer.append(String.valueOf(this.fechaAbono.getMinutes() + 1));
            valorBuffer.append(String.valueOf(this.fechaAbono.getSeconds() + 1));
            String relleno;
            String relleno;
            if (rsEncabezado3.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 8:
            valorBuffer.append(String.valueOf(this.fechaProceso.getHours() + 1));
            valorBuffer.append(String.valueOf(this.fechaProceso.getMinutes() + 1));
            valorBuffer.append(String.valueOf(this.fechaProceso.getSeconds() + 1));
            String relleno;
            String relleno;
            if (rsEncabezado3.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 9:
            String valor = String.valueOf(this.fechaAbono.getMonth() + 1);
            String relleno;
            String relleno;
            if (rsEncabezado3.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 10:
            String valor = String.valueOf(this.fechaProceso.getMonth() + 1);
            String relleno;
            String relleno;
            if (rsEncabezado3.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 11:
            String valor = String.valueOf(this.numeroOrdenPago);
            String relleno;
            String relleno;
            if (rsEncabezado3.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
          default:
            break;
          } } else if (rsEncabezado3.getString("tipo_campo").equals("V")) {
          switch (Integer.valueOf(rsEncabezado3.getString("campo_totales")).intValue())
          {
          case 2:
            log.warn("encabezado 2");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsEncabezado3.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("neto"), rsEncabezado3.getDouble("multiplicador"), rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
            }

            break;
          case 4:
            log.warn("encabezado 4");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsEncabezado3.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("total"), rsEncabezado3.getDouble("multiplicador"), rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
            }

          case 5:
            log.warn("encabezado 5");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsEncabezado3.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("correlativo"), rsEncabezado3.getDouble("multiplicador"), rsEncabezado3.getInt("longitud_campo"), rsEncabezado3.getString("alineacion_campo").equals("D"), relleno));
            }
            break;
          case 3:
          }
        }

      }

      if (hayEncabezado) {
        writer.write("\n");
      }
      hayEncabezado = false;

      while (rsTrabajadores.next()) {
        if ((!this.diaAdicional.equals("S")) || (rsTrabajadores.getDouble("adicional") != 0.0D))
        {
          contador++;

          tope++;
          rsDetalle.beforeFirst();
          while (rsDetalle.next()) {
            if (rsDetalle.getString("tipo_campo").equals("F")) {
              log.warn("detalle Fijo");
              writer.write(ToolsDisquete.convertirLetras(rsDetalle.getString("campo_usuario"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), " "));
            }
            else if (rsDetalle.getString("tipo_campo").equals("C")) {
              log.warn("detalle contador");
              String relleno;
              String relleno;
              if (rsDetalle.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(contador, 1.0D, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
            }
            else if (rsDetalle.getString("tipo_campo").equals("V")) {
              log.warn("--------------numero " + rsDetalle.getString("campo_base_datos"));
              switch (Integer.valueOf(rsDetalle.getString("campo_base_datos")).intValue())
              {
              case 1:
                log.warn("detalle 1");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirNumero(rsTrabajadores.getDouble("prestaciones"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));

                break;
              case 7:
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_ingreso").getYear() + 1900);
                valor = valor.substring(2);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 8:
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_ingreso").getYear() + 1900);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 13:
                log.warn("detalle 13");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                String nombresApellidos = rsTrabajadores.getString("primer_apellido") + " " + rsTrabajadores.getString("primer_nombre");
                writer.write(ToolsDisquete.convertirLetras(nombresApellidos, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 14:
                log.warn("detalle 14");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }

                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("cedula"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 17:
                log.warn("detalle 17");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }

                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("dependencia"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 47:
                log.warn("detalle 47");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirNumero(rsTrabajadores.getDouble("adicional"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));

                break;
              case 33:
                log.warn("ID " + rsTrabajadores.getInt("cedula"));
                log.warn("fecha " + rsTrabajadores.getDate("fecha_ingreso"));
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_ingreso").getDate());
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 35:
                log.error("detalle 35");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("estado_civil"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 41:
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_ingreso").getMonth() + 1);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 52:
                log.warn("detalle 52");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("nacionalidad"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 54:
                log.warn("detalle 54");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("primer_apellido"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 55:
                log.warn("detalle 55");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("primer_nombre"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 57:
                log.warn("detalle 57. cod tipo personal");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("tipopersonal"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 60:
                log.warn("detalle 60");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("segundo_apellido"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 61:
                log.warn("detalle 61");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("segundo_nombre"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 67:
                log.warn("detalle 67");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("cuenta"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 68:
                log.warn("detalle 68");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("direccion"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 69:
                log.warn("detalle 69");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(rsTrabajadores.getString("telefono"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 70:
                log.warn("detalle 70");
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_apertura_fideicomiso").getDate());
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 71:
                log.warn("detalle 71");
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_apertura_fideicomiso").getMonth() + 1);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 72:
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_apertura_fideicomiso").getYear() + 1900);
                valor = valor.substring(2);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 73:
                String valor = String.valueOf(rsTrabajadores.getDate("fecha_apertura_fideicomiso").getYear() + 1900);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));

                break;
              case 74:
                log.warn("detalle 74");
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirNumero(rsTrabajadores.getDouble("total_prestaciones"), rsDetalle.getDouble("multiplicador"), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
              default:
                break;
              } } else if (rsDetalle.getString("tipo_campo").equals("E")) {
              switch (Integer.valueOf(rsDetalle.getString("campo_entrada")).intValue())
              {
              case 1:
                String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
                valor = valor.substring(2);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 2:
                String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));

                break;
              case 3:
                String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
                valor = valor.substring(2);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 4:
                String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));

                break;
              case 5:
                String valor = String.valueOf(this.fechaAbono.getDate());
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 6:
                String valor = String.valueOf(this.fechaProceso.getDate());
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 7:
                valorBuffer.append(String.valueOf(this.fechaAbono.getHours() + 1));
                valorBuffer.append(String.valueOf(this.fechaAbono.getMinutes() + 1));
                valorBuffer.append(String.valueOf(this.fechaAbono.getSeconds() + 1));
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 8:
                valorBuffer.append(String.valueOf(this.fechaProceso.getHours() + 1));
                valorBuffer.append(String.valueOf(this.fechaProceso.getMinutes() + 1));
                valorBuffer.append(String.valueOf(this.fechaProceso.getSeconds() + 1));
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 9:
                String valor = String.valueOf(this.fechaAbono.getMonth() + 1);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 10:
                String valor = String.valueOf(this.fechaProceso.getMonth() + 1);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
                break;
              case 11:
                String valor = String.valueOf(this.numeroOrdenPago);
                String relleno;
                String relleno;
                if (rsDetalle.getString("rellenar_cero").equals("C"))
                  relleno = "0";
                else {
                  relleno = " ";
                }
                writer.write(ToolsDisquete.convertirLetras(valor, rsDetalle.getInt("longitud_campo"), rsDetalle.getString("alineacion_campo").equals("D"), relleno));
              }

            }

          }

          if (!rsTrabajadores.isLast()) {
            writer.write("\n");
          }
          if (tope == 5000) {
            writer.flush();
          }

        }

      }

      while (rsTotal.next()) {
        if (rsTotal.isFirst()) {
          writer.write("\n");
        }
        hayEncabezado = true;
        rsTotalFideicomiso.beforeFirst();
        if (rsTotal.getString("tipo_campo").equals("F")) {
          log.warn("Total Fijo");
          writer.write(ToolsDisquete.convertirLetras(rsTotal.getString("campo_usuario"), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), " "));
        } else if (rsTotal.getString("tipo_campo").equals("E")) {
          switch (Integer.valueOf(rsTotal.getString("campo_entrada")).intValue())
          {
          case 1:
            String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
            valor = valor.substring(2);
            String relleno;
            String relleno;
            if (rsDetalle.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 2:
            String valor = String.valueOf(this.fechaAbono.getYear() + 1900);
            String relleno;
            String relleno;
            if (rsTotal.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 3:
            String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
            valor = valor.substring(2);
            String relleno;
            String relleno;
            if (rsTotal.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 4:
            String valor = String.valueOf(this.fechaProceso.getYear() + 1900);
            String relleno;
            String relleno;
            if (rsTotal.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 5:
            String valor = String.valueOf(this.fechaAbono.getDate());
            String relleno;
            String relleno;
            if (rsTotal.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 6:
            String valor = String.valueOf(this.fechaProceso.getDate());
            String relleno;
            String relleno;
            if (rsTotal.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 7:
            valorBuffer.append(String.valueOf(this.fechaAbono.getHours() + 1));
            valorBuffer.append(String.valueOf(this.fechaAbono.getMinutes() + 1));
            valorBuffer.append(String.valueOf(this.fechaAbono.getSeconds() + 1));
            String relleno;
            String relleno;
            if (rsTotal.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 8:
            valorBuffer.append(String.valueOf(this.fechaProceso.getHours() + 1));
            valorBuffer.append(String.valueOf(this.fechaProceso.getMinutes() + 1));
            valorBuffer.append(String.valueOf(this.fechaProceso.getSeconds() + 1));
            String relleno;
            String relleno;
            if (rsTotal.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valorBuffer.toString(), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 9:
            String valor = String.valueOf(this.fechaAbono.getMonth() + 1);
            String relleno;
            String relleno;
            if (rsTotal.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 10:
            String valor = String.valueOf(this.fechaProceso.getMonth() + 1);
            String relleno;
            String relleno;
            if (rsTotal.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            break;
          case 11:
            String valor = String.valueOf(this.numeroOrdenPago);
            String relleno;
            String relleno;
            if (rsTotal.getString("rellenar_cero").equals("C"))
              relleno = "0";
            else {
              relleno = " ";
            }
            writer.write(ToolsDisquete.convertirLetras(valor, rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
          default:
            break;
          } } else if (rsTotal.getString("tipo_campo").equals("V"))
        {
          switch (Integer.valueOf(rsTotal.getString("campo_totales")).intValue()) {
          case 2:
            log.warn("total 2");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsTotal.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("neto"), rsTotal.getDouble("multiplicador"), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            }

          case 4:
            log.warn("total 4");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsTotal.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("total"), rsTotal.getDouble("multiplicador"), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            }

          case 5:
            log.warn("total 4");
            if (rsTotalFideicomiso.next())
            {
              String relleno;
              String relleno;
              if (rsTotal.getString("rellenar_cero").equals("C"))
                relleno = "0";
              else {
                relleno = " ";
              }
              writer.write(ToolsDisquete.convertirNumero(rsTotalFideicomiso.getDouble("correlativo"), rsTotal.getDouble("multiplicador"), rsTotal.getInt("longitud_campo"), rsTotal.getString("alineacion_campo").equals("D"), relleno));
            }

            break;
          case 3:
          }

        }

      }

      writer.flush();
      writer.close();
      this.generado = true;

      this.urlArchivo = ("/sigefirrhh/disquetes/" + file.getName());
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalByPrestaciones(this.login.getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());
      this.listDisquete = this.definicionesFacade.findDisqueteByTipoDisquete("F", this.login.getIdOrganismo());
      this.listRegion = this.estructuraFacade.findAllRegion();
    }
    catch (Exception e)
    {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public Collection getListTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getListDisquete() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listDisquete, "sigefirrhh.base.definiciones.Disquete");
  }
  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }

  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
  }
  public String getFin() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getInicio() {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }
  public void setFin(String string) {
    this.fin = string;
  }
  public void setInicio(String string) {
    this.inicio = string;
  }
  public boolean isShow() {
    return this.auxShow;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public void setIdTipoPersonal(long l) {
    this.idTipoPersonal = l;
  }

  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }
  public String getSelectDisquete() {
    return this.selectDisquete;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }

  public void setSelectDisquete(String string) {
    this.selectDisquete = string;
    try {
      this.disquete = this.definicionesFacade.findDisqueteById(Long.valueOf(this.selectDisquete).longValue());
    } catch (Exception localException) {
    }
  }

  public void setSelectRegion(String string) {
    this.selectRegion = string;
  }

  public java.util.Date getFechaAbono() {
    return this.fechaAbono;
  }
  public java.util.Date getFechaProceso() {
    return this.fechaProceso;
  }
  public void setFechaAbono(java.util.Date date) {
    this.fechaAbono = date;
  }
  public void setFechaProceso(java.util.Date date) {
    this.fechaProceso = date;
  }
  public boolean isGenerado() {
    return this.generado;
  }
  public String getUrlArchivo() {
    return this.urlArchivo;
  }

  public String getNumeroOrdenPago()
  {
    return this.numeroOrdenPago;
  }
  public void setNumeroOrdenPago(String numeroOrdenPago) {
    this.numeroOrdenPago = numeroOrdenPago;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }

  public String getDiaAdicional() {
    return this.diaAdicional;
  }
  public void setDiaAdicional(String diaAdicional) {
    this.diaAdicional = diaAdicional;
  }
}