package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class EmbargoConceptoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEmbargoConcepto(EmbargoConcepto embargoConcepto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    EmbargoConcepto embargoConceptoNew = 
      (EmbargoConcepto)BeanUtils.cloneBean(
      embargoConcepto);

    EmbargoBeanBusiness embargoBeanBusiness = new EmbargoBeanBusiness();

    if (embargoConceptoNew.getEmbargo() != null) {
      embargoConceptoNew.setEmbargo(
        embargoBeanBusiness.findEmbargoById(
        embargoConceptoNew.getEmbargo().getIdEmbargo()));
    }

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (embargoConceptoNew.getConcepto() != null) {
      embargoConceptoNew.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        embargoConceptoNew.getConcepto().getIdConcepto()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (embargoConceptoNew.getPersonal() != null) {
      embargoConceptoNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        embargoConceptoNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(embargoConceptoNew);
  }

  public void updateEmbargoConcepto(EmbargoConcepto embargoConcepto) throws Exception
  {
    EmbargoConcepto embargoConceptoModify = 
      findEmbargoConceptoById(embargoConcepto.getIdEmbargoConcepto());

    EmbargoBeanBusiness embargoBeanBusiness = new EmbargoBeanBusiness();

    if (embargoConcepto.getEmbargo() != null) {
      embargoConcepto.setEmbargo(
        embargoBeanBusiness.findEmbargoById(
        embargoConcepto.getEmbargo().getIdEmbargo()));
    }

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (embargoConcepto.getConcepto() != null) {
      embargoConcepto.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        embargoConcepto.getConcepto().getIdConcepto()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (embargoConcepto.getPersonal() != null) {
      embargoConcepto.setPersonal(
        personalBeanBusiness.findPersonalById(
        embargoConcepto.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(embargoConceptoModify, embargoConcepto);
  }

  public void deleteEmbargoConcepto(EmbargoConcepto embargoConcepto) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    EmbargoConcepto embargoConceptoDelete = 
      findEmbargoConceptoById(embargoConcepto.getIdEmbargoConcepto());
    pm.deletePersistent(embargoConceptoDelete);
  }

  public EmbargoConcepto findEmbargoConceptoById(long idEmbargoConcepto) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEmbargoConcepto == pIdEmbargoConcepto";
    Query query = pm.newQuery(EmbargoConcepto.class, filter);

    query.declareParameters("long pIdEmbargoConcepto");

    parameters.put("pIdEmbargoConcepto", new Long(idEmbargoConcepto));

    Collection colEmbargoConcepto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEmbargoConcepto.iterator();
    return (EmbargoConcepto)iterator.next();
  }

  public Collection findEmbargoConceptoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent embargoConceptoExtent = pm.getExtent(
      EmbargoConcepto.class, true);
    Query query = pm.newQuery(embargoConceptoExtent);
    query.setOrdering("concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByEmbargo(long idEmbargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "embargo.idEmbargo == pIdEmbargo";

    Query query = pm.newQuery(EmbargoConcepto.class, filter);

    query.declareParameters("long pIdEmbargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdEmbargo", new Long(idEmbargo));

    query.setOrdering("concepto.codConcepto ascending");

    Collection colEmbargoConcepto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEmbargoConcepto);

    return colEmbargoConcepto;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(EmbargoConcepto.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("concepto.codConcepto ascending");

    Collection colEmbargoConcepto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEmbargoConcepto);

    return colEmbargoConcepto;
  }
}