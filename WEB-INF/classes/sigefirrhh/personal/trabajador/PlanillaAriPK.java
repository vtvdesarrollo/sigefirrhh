package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class PlanillaAriPK
  implements Serializable
{
  public long idPlanillaAri;

  public PlanillaAriPK()
  {
  }

  public PlanillaAriPK(long idPlanillaAri)
  {
    this.idPlanillaAri = idPlanillaAri;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PlanillaAriPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PlanillaAriPK thatPK)
  {
    return 
      this.idPlanillaAri == thatPK.idPlanillaAri;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPlanillaAri)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPlanillaAri);
  }
}