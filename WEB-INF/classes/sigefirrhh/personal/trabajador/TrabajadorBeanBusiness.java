package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.BancoBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.definiciones.TurnoBeanBusiness;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.DependenciaBeanBusiness;
import sigefirrhh.base.estructura.LugarPago;
import sigefirrhh.base.estructura.LugarPagoBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaMovimientoBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBeanBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.UsuarioTipoPersonal;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class TrabajadorBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();

  public void addTrabajador(Trabajador trabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Trabajador trabajadorNew = 
      (Trabajador)BeanUtils.cloneBean(
      trabajador);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (trabajadorNew.getPersonal() != null) {
      trabajadorNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        trabajadorNew.getPersonal().getIdPersonal()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (trabajadorNew.getTipoPersonal() != null) {
      trabajadorNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        trabajadorNew.getTipoPersonal().getIdTipoPersonal()));
    }

    RegistroCargosBeanBusiness registroCargosBeanBusiness = new RegistroCargosBeanBusiness();

    if (trabajadorNew.getRegistroCargos() != null) {
      trabajadorNew.setRegistroCargos(
        registroCargosBeanBusiness.findRegistroCargosById(
        trabajadorNew.getRegistroCargos().getIdRegistroCargos()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (trabajadorNew.getCargo() != null) {
      trabajadorNew.setCargo(
        cargoBeanBusiness.findCargoById(
        trabajadorNew.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (trabajadorNew.getDependencia() != null) {
      trabajadorNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        trabajadorNew.getDependencia().getIdDependencia()));
    }

    LugarPagoBeanBusiness lugarPagoBeanBusiness = new LugarPagoBeanBusiness();

    if (trabajadorNew.getLugarPago() != null) {
      trabajadorNew.setLugarPago(
        lugarPagoBeanBusiness.findLugarPagoById(
        trabajadorNew.getLugarPago().getIdLugarPago()));
    }

    TurnoBeanBusiness turnoBeanBusiness = new TurnoBeanBusiness();

    if (trabajadorNew.getTurno() != null) {
      trabajadorNew.setTurno(
        turnoBeanBusiness.findTurnoById(
        trabajadorNew.getTurno().getIdTurno()));
    }

    BancoBeanBusiness bancoNominaBeanBusiness = new BancoBeanBusiness();

    if (trabajadorNew.getBancoNomina() != null) {
      trabajadorNew.setBancoNomina(
        bancoNominaBeanBusiness.findBancoById(
        trabajadorNew.getBancoNomina().getIdBanco()));
    }

    BancoBeanBusiness bancoLphBeanBusiness = new BancoBeanBusiness();

    if (trabajadorNew.getBancoLph() != null) {
      trabajadorNew.setBancoLph(
        bancoLphBeanBusiness.findBancoById(
        trabajadorNew.getBancoLph().getIdBanco()));
    }

    BancoBeanBusiness bancoFidBeanBusiness = new BancoBeanBusiness();

    if (trabajadorNew.getBancoFid() != null) {
      trabajadorNew.setBancoFid(
        bancoFidBeanBusiness.findBancoById(
        trabajadorNew.getBancoFid().getIdBanco()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (trabajadorNew.getCausaMovimiento() != null) {
      trabajadorNew.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        trabajadorNew.getCausaMovimiento().getIdCausaMovimiento()));
    }

    DependenciaBeanBusiness dependenciaRealBeanBusiness = new DependenciaBeanBusiness();

    if (trabajadorNew.getDependenciaReal() != null) {
      trabajadorNew.setDependenciaReal(
        dependenciaRealBeanBusiness.findDependenciaById(
        trabajadorNew.getDependenciaReal().getIdDependencia()));
    }

    CargoBeanBusiness cargoRealBeanBusiness = new CargoBeanBusiness();

    if (trabajadorNew.getCargoReal() != null) {
      trabajadorNew.setCargoReal(
        cargoRealBeanBusiness.findCargoById(
        trabajadorNew.getCargoReal().getIdCargo()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (trabajadorNew.getOrganismo() != null) {
      trabajadorNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        trabajadorNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(trabajadorNew);
  }

  public void updateTrabajador(Trabajador trabajador) throws Exception
  {
    TrabajadorNoGenBusiness trabajadorBusiness = new TrabajadorNoGenBusiness();
    Trabajador trabajadorModify = 
      findTrabajadorById(trabajador.getIdTrabajador());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (trabajador.getPersonal() != null) {
      trabajador.setPersonal(
        personalBeanBusiness.findPersonalById(
        trabajador.getPersonal().getIdPersonal()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (trabajador.getTipoPersonal() != null) {
      trabajador.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        trabajador.getTipoPersonal().getIdTipoPersonal()));
    }

    RegistroCargosBeanBusiness registroCargosBeanBusiness = new RegistroCargosBeanBusiness();

    if (trabajador.getRegistroCargos() != null) {
      trabajador.setRegistroCargos(
        registroCargosBeanBusiness.findRegistroCargosById(
        trabajador.getRegistroCargos().getIdRegistroCargos()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (trabajador.getCargo() != null) {
      trabajador.setCargo(
        cargoBeanBusiness.findCargoById(
        trabajador.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (trabajador.getDependencia() != null) {
      trabajador.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        trabajador.getDependencia().getIdDependencia()));
    }

    LugarPagoBeanBusiness lugarPagoBeanBusiness = new LugarPagoBeanBusiness();

    if (trabajador.getLugarPago() != null) {
      trabajador.setLugarPago(
        lugarPagoBeanBusiness.findLugarPagoById(
        trabajador.getLugarPago().getIdLugarPago()));
    }

    TurnoBeanBusiness turnoBeanBusiness = new TurnoBeanBusiness();

    if (trabajador.getTurno() != null) {
      trabajador.setTurno(
        turnoBeanBusiness.findTurnoById(
        trabajador.getTurno().getIdTurno()));
    }

    BancoBeanBusiness bancoNominaBeanBusiness = new BancoBeanBusiness();

    if (trabajador.getBancoNomina() != null) {
      trabajador.setBancoNomina(
        bancoNominaBeanBusiness.findBancoById(
        trabajador.getBancoNomina().getIdBanco()));
    }

    BancoBeanBusiness bancoLphBeanBusiness = new BancoBeanBusiness();

    if (trabajador.getBancoLph() != null) {
      trabajador.setBancoLph(
        bancoLphBeanBusiness.findBancoById(
        trabajador.getBancoLph().getIdBanco()));
    }

    BancoBeanBusiness bancoFidBeanBusiness = new BancoBeanBusiness();

    if (trabajador.getBancoFid() != null) {
      trabajador.setBancoFid(
        bancoFidBeanBusiness.findBancoById(
        trabajador.getBancoFid().getIdBanco()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (trabajador.getCausaMovimiento() != null) {
      trabajador.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        trabajador.getCausaMovimiento().getIdCausaMovimiento()));
    }

    DependenciaBeanBusiness dependenciaRealBeanBusiness = new DependenciaBeanBusiness();

    if (trabajador.getDependenciaReal() != null) {
      trabajador.setDependenciaReal(
        dependenciaRealBeanBusiness.findDependenciaById(
        trabajador.getDependenciaReal().getIdDependencia()));
    }

    CargoBeanBusiness cargoRealBeanBusiness = new CargoBeanBusiness();

    if (trabajador.getCargoReal() != null) {
      trabajador.setCargoReal(
        cargoRealBeanBusiness.findCargoById(
        trabajador.getCargoReal().getIdCargo()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (trabajador.getOrganismo() != null) {
      trabajador.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        trabajador.getOrganismo().getIdOrganismo()));
    }

    if ((trabajador.getFormaPago().equals("1")) && ((trabajador.getBancoNomina() == null) || (trabajador.getCuentaNomina() == null) || (trabajador.getCuentaNomina() == ""))) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Si registra el tipo de pago Depósito, debe asociar un banco e introducir un número de cuenta");
      throw error;
    }
    if (trabajadorBusiness.existeCuenta(trabajador.getCedula(), trabajador.getCuentaNomina(), "N")) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La cuenta nomina ya se encuentra registrada en otro trabajador");
      throw error;
    }
    if ((trabajador.getCuentaLph() != null) && (!trabajador.getCuentaLph().equals("")) && (trabajadorBusiness.existeCuenta(trabajador.getCedula(), trabajador.getCuentaLph(), "L"))) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La cuenta LPH ya se encuentra registrada en otro trabajador");
      throw error;
    }

    BeanUtils.copyProperties(trabajadorModify, trabajador);
  }

  public void deleteTrabajador(Trabajador trabajador) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Trabajador trabajadorDelete = 
      findTrabajadorById(trabajador.getIdTrabajador());
    pm.deletePersistent(trabajadorDelete);
  }

  public Trabajador findTrabajadorById(long idTrabajador) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTrabajador == pIdTrabajador";
    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdTrabajador");

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTrabajador.iterator();
    return (Trabajador)iterator.next();
  }

  public Collection findTrabajadorAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent trabajadorExtent = pm.getExtent(
      Trabajador.class, true);
    Query query = pm.newQuery(trabajadorExtent);
    query.setOrdering("personal.primerApellido ascending, personal.primerNombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("personal.primerApellido ascending, personal.primerNombre ascending");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByTipoPersonal(long idTipoPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdTipoPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("personal.primerApellido ascending, personal.primerNombre ascending");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByNombresApellidos(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    StringBuffer filter = new StringBuffer();
    if (primerNombre != null) {
      filter.append("personal.primerNombre.startsWith(pPrimerNombre) && ");
    }
    if (segundoNombre != null) {
      filter.append("personal.segundoNombre.startsWith(pSegundoNombre) && ");
    }
    if (primerApellido != null) {
      filter.append("personal.primerApellido.startsWith(pPrimerApellido) && ");
    }
    if (segundoApellido != null) {
      filter.append("personal.segundoApellido.startsWith(pSegundoApellido) && ");
    }
    filter.append("organismo.idOrganismo==pIdOrganismo");

    Query query = pm.newQuery(Trabajador.class, filter.toString());

    StringBuffer declare = new StringBuffer();

    if (primerNombre != null) {
      declare.append("String pPrimerNombre, ");
    }
    if (segundoNombre != null) {
      declare.append("String pSegundoNombre, ");
    }
    if (primerApellido != null) {
      declare.append("String pPrimerApellido, ");
    }
    if (segundoApellido != null) {
      declare.append("String pSegundoApellido, ");
    }
    declare.append("long pIdOrganismo");

    query.declareParameters(declare.toString());
    query.setOrdering("personal.primerApellido ascending, personal.primerNombre ascending");
    HashMap parameters = new HashMap();

    if (primerNombre != null) {
      parameters.put("pPrimerNombre", new String(primerNombre));
    }
    if (segundoNombre != null) {
      parameters.put("pSegundoNombre", new String(segundoNombre));
    }
    if (primerApellido != null) {
      parameters.put("pPrimerApellido", new String(primerApellido));
    }
    if (segundoApellido != null) {
      parameters.put("pSegundoApellido", new String(segundoApellido));
    }
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPersonal);

    return colPersonal;
  }

  public Collection findByNombresApellidosAndClasificacion(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo, long idClasificacionPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    StringBuffer filter = new StringBuffer();
    if (primerNombre != null) {
      filter.append("personal.primerNombre.startsWith(pPrimerNombre) && ");
    }
    if (segundoNombre != null) {
      filter.append("personal.segundoNombre.startsWith(pSegundoNombre) && ");
    }
    if (primerApellido != null) {
      filter.append("personal.primerApellido.startsWith(pPrimerApellido) && ");
    }
    if (segundoApellido != null) {
      filter.append("personal.segundoApellido.startsWith(pSegundoApellido) && ");
    }
    filter.append("organismo.idOrganismo==pIdOrganismo && tipoPersonal.clasificacionPersonal.idClasificacionPersonal == pIdClasificacionPersonal");

    Query query = pm.newQuery(Trabajador.class, filter.toString());

    StringBuffer declare = new StringBuffer();

    if (primerNombre != null) {
      declare.append("String pPrimerNombre, ");
    }
    if (segundoNombre != null) {
      declare.append("String pSegundoNombre, ");
    }
    if (primerApellido != null) {
      declare.append("String pPrimerApellido, ");
    }
    if (segundoApellido != null) {
      declare.append("String pSegundoApellido, ");
    }
    declare.append("long pIdOrganismo, long pIdClasificacionPersonal");

    query.declareParameters(declare.toString());
    query.setOrdering("personal.primerApellido ascending, personal.primerNombre ascending, fechaIngreso ascending");
    HashMap parameters = new HashMap();

    if (primerNombre != null) {
      parameters.put("pPrimerNombre", new String(primerNombre));
    }
    if (segundoNombre != null) {
      parameters.put("pSegundoNombre", new String(segundoNombre));
    }
    if (primerApellido != null) {
      parameters.put("pPrimerApellido", new String(primerApellido));
    }
    if (segundoApellido != null) {
      parameters.put("pSegundoApellido", new String(segundoApellido));
    }
    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pIdClasificacionPersonal", new Long(idClasificacionPersonal));

    Collection colPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPersonal);

    return colPersonal;
  }

  public Collection findByCedula(int cedula, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.cedula == pCedula && organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("int pCedula, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("personal.primerApellido ascending");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByCedulaAndClasificacion(int cedula, long idOrganismo, long idClasificacionPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.cedula == pCedula && organismo.idOrganismo == pIdOrganismo && tipoPersonal.clasificacionPersonal.idClasificacionPersonal == pIdClasificacionPersonal";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("int pCedula, long pIdOrganismo, long pIdClasificacionPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));
    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pIdClasificacionPersonal", new Long(idClasificacionPersonal));

    query.setOrdering("personal.primerApellido ascending, fechaIngresoOrganismo ascending ");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByCedula(int cedula, long idOrganismo, long idUsuario, String administrador)
    throws Exception
  {
    Collection colUsuarioTipoPersonal = this.sistemaBusiness.findUsuarioTipoPersonalByUsuario(idUsuario);
    Iterator iterUsuarioTipoPersonal = colUsuarioTipoPersonal.iterator();

    PersistenceManager pm = PMThread.getPM();
    Collection colTrabajador = new ArrayList();

    StringBuffer filter = new StringBuffer();

    filter.append("personal.cedula == pCedula && organismo.idOrganismo == pIdOrganismo");
    if (!administrador.equals("S")) {
      filter.append(" && tipoPersonal.idTipoPersonal == pIdTipoPersonal");
    }

    Query query = pm.newQuery(Trabajador.class, filter.toString());

    query.declareParameters("int pCedula, long pIdOrganismo");
    if (!administrador.equals("S")) {
      while (iterUsuarioTipoPersonal.hasNext()) {
        UsuarioTipoPersonal usuarioTipoPersonal = (UsuarioTipoPersonal)iterUsuarioTipoPersonal.next();

        HashMap parameters = new HashMap();

        parameters.put("pCedula", new Integer(cedula));
        parameters.put("pIdOrganismo", new Long(idOrganismo));
        parameters.put("pIdTipoPersonal", new Long(usuarioTipoPersonal.getTipoPersonal().getIdTipoPersonal()));
        query.setOrdering("personal.primerApellido ascending");

        colTrabajador.addAll((Collection)query.executeWithMap(parameters));
      }
    } else {
      HashMap parameters = new HashMap();

      parameters.put("pCedula", new Integer(cedula));
      parameters.put("pIdOrganismo", new Long(idOrganismo));

      query.setOrdering("personal.primerApellido ascending");

      colTrabajador.addAll((Collection)query.executeWithMap(parameters));
    }
    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByCedula(int cedula, long idOrganismo, long idUsuario, String administrador, String estatus)
    throws Exception
  {
    Collection colUsuarioTipoPersonal = this.sistemaBusiness.findUsuarioTipoPersonalByUsuario(idUsuario);
    Iterator iterUsuarioTipoPersonal = colUsuarioTipoPersonal.iterator();

    PersistenceManager pm = PMThread.getPM();
    Collection colTrabajador = new ArrayList();

    StringBuffer filter = new StringBuffer();

    filter.append("personal.cedula == pCedula && organismo.idOrganismo == pIdOrganismo");
    if (!administrador.equals("S")) {
      filter.append(" && tipoPersonal.idTipoPersonal == pIdTipoPersonal");
    }

    Query query = pm.newQuery(Trabajador.class, filter.toString());

    query.declareParameters("int pCedula, long pIdOrganismo, ");

    if (!administrador.equals("S")) {
      while (iterUsuarioTipoPersonal.hasNext()) {
        UsuarioTipoPersonal usuarioTipoPersonal = (UsuarioTipoPersonal)iterUsuarioTipoPersonal.next();

        HashMap parameters = new HashMap();

        parameters.put("pCedula", new Integer(cedula));
        parameters.put("pIdOrganismo", new Long(idOrganismo));
        parameters.put("pIdTipoPersonal", new Long(usuarioTipoPersonal.getTipoPersonal().getIdTipoPersonal()));
        parameters.put("pEstatus", new String(estatus));

        query.setOrdering("personal.primerApellido ascending");

        colTrabajador.addAll((Collection)query.executeWithMap(parameters));
      }
    } else {
      HashMap parameters = new HashMap();

      parameters.put("pCedula", new Integer(cedula));
      parameters.put("pIdOrganismo", new Long(idOrganismo));
      parameters.put("pEstatus", new String(estatus));

      query.setOrdering("personal.primerApellido ascending");

      colTrabajador.addAll((Collection)query.executeWithMap(parameters));
    }
    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByCodigoNomina(long idTipoPersonal, int codigoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && codigoNomina == pCodigoNomina";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdTipoPersonal, int pCodigoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pCodigoNomina", new Integer(codigoNomina));

    query.setOrdering("tipoPersonal.nombre ascending, personal.primerApellido ascending");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.cedula == pCedula && organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("int pCedula, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("personal.primerApellido ascending");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByCodigoNominaAndTipoPersonalAndEstatus(long idTipoPersonal, int codigoNomina, String estatus)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && codigoNomina == pCodigoNomina && estatus == pEstatus";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdTipoPersonal, int pCodigoNomina, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pCodigoNomina", new Integer(codigoNomina));
    parameters.put("pEstatus", estatus);

    query.setOrdering("tipoPersonal.nombre ascending, personal.primerApellido ascending");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByCedulaAndTipoPersonalAndEstatus(int cedula, long idTipoPersonal, String estatus)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.cedula == pCedula && tipoPersonal.idTipoPersonal == pIdTipoPersonal && estatus == pEstatus";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("int pCedula, long pIdTipoPersonal, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pEstatus", estatus);

    query.setOrdering("personal.primerApellido ascending");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByCedulaAndTipoPersonal(int cedula, long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.cedula == pCedula && tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("int pCedula, long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("personal.primerApellido ascending");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByCodigoNominaAndTipoPersonal(int codigoNomina, long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "codigoNomina == pCodigoNomina && tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("int pCodigoNomina, long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pCodigoNomina", new Integer(codigoNomina));
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("personal.primerApellido ascending");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findByNombresApellidosAndTipoPersonal(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    StringBuffer filter = new StringBuffer();
    if (primerNombre != null) {
      filter.append("personal.primerNombre.startsWith(pPrimerNombre) && ");
    }
    if (segundoNombre != null) {
      filter.append("personal.segundoNombre.startsWith(pSegundoNombre) && ");
    }
    if (primerApellido != null) {
      filter.append("personal.primerApellido.startsWith(pPrimerApellido) && ");
    }
    if (segundoApellido != null) {
      filter.append("personal.segundoApellido.startsWith(pSegundoApellido) && ");
    }
    filter.append("tipoPersonal.idTipoPersonal == pIdTipoPersonal ");

    Query query = pm.newQuery(Trabajador.class, filter.toString());

    StringBuffer declare = new StringBuffer();

    if (primerNombre != null) {
      declare.append("String pPrimerNombre, ");
    }
    if (segundoNombre != null) {
      declare.append("String pSegundoNombre, ");
    }
    if (primerApellido != null) {
      declare.append("String pPrimerApellido, ");
    }
    if (segundoApellido != null) {
      declare.append("String pSegundoApellido, ");
    }
    declare.append("long pIdTipoPersonal");

    query.declareParameters(declare.toString());
    query.setOrdering("personal.primerApellido ascending, personal.primerNombre ascending");
    HashMap parameters = new HashMap();

    if (primerNombre != null) {
      parameters.put("pPrimerNombre", new String(primerNombre));
    }
    if (segundoNombre != null) {
      parameters.put("pSegundoNombre", new String(segundoNombre));
    }
    if (primerApellido != null) {
      parameters.put("pPrimerApellido", new String(primerApellido));
    }
    if (segundoApellido != null) {
      parameters.put("pSegundoApellido", new String(segundoApellido));
    }
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPersonal);

    return colPersonal;
  }

  public Collection findByCedulaAndIdTipoPersonal(int cedula, long idTipoPersonal)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsTrabajador = null;
    PreparedStatement stTrabajador = null;
    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select t.id_trabajador, t.cedula, p.primer_nombre, ");
      sql.append(" p.segundo_nombre, p.primer_apellido, p.segundo_apellido, ");
      sql.append(" t.codigo_nomina, t.estatus, tp.nombre ");
      sql.append(" from trabajador t, personal p, tipopersonal tp");
      sql.append(" where t.id_personal = p.id_personal");
      sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_tipo_personal = ?");
      sql.append(" and t.cedula = ?");

      stTrabajador = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTrabajador.setLong(1, idTipoPersonal);
      stTrabajador.setInt(2, cedula);
      rsTrabajador = stTrabajador.executeQuery();

      while (rsTrabajador.next()) {
        TrabajadorAux trabajador = new TrabajadorAux();
        trabajador.setCedula(rsTrabajador.getInt("cedula"));
        trabajador.setIdTrabajador(rsTrabajador.getLong("id_trabajador"));
        trabajador.setCodigoNomina(rsTrabajador.getInt("codigo_nomina"));
        trabajador.setTipoPersonal(rsTrabajador.getString("nombre"));
        trabajador.setPrimerNombre(rsTrabajador.getString("primer_nombre"));
        trabajador.setSegundoNombre(rsTrabajador.getString("segundo_nombre"));
        trabajador.setPrimerApellido(rsTrabajador.getString("primer_apellido"));
        trabajador.setSegundoApellido(rsTrabajador.getString("segundo_apellido"));
        trabajador.setEstatus(rsTrabajador.getString("estatus"));

        col.add(trabajador);
      }

      return col;
    } finally {
      if (rsTrabajador != null) try {
          rsTrabajador.close();
        } catch (Exception localException3) {
        } if (stTrabajador != null) try {
          stTrabajador.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findByNombresApellidosAndIdTipoPersonal(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idTipoPersonal)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsTrabajador = null;
    PreparedStatement stTrabajador = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select t.id_trabajador, t.cedula, p.primer_nombre, ");
      sql.append(" p.segundo_nombre, p.primer_apellido, p.segundo_apellido, ");
      sql.append(" t.codigo_nomina, t.estatus, tp.nombre ");
      sql.append(" from trabajador t, personal p, tipopersonal tp");
      sql.append(" where t.id_personal = p.id_personal");
      sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_tipo_personal = ?");
      if ((primerNombre != null) && (!primerNombre.equals(""))) {
        sql.append(" and p.primer_nombre like ?");
      }
      if ((segundoNombre != null) && (!segundoNombre.equals(""))) {
        sql.append(" and p.segundo_nombre like ?");
      }
      if ((primerApellido != null) && (!primerApellido.equals(""))) {
        sql.append(" and p.primer_apellido like ?");
      }
      if ((segundoApellido != null) && (!segundoApellido.equals(""))) {
        sql.append(" and p.segundo_apellido like ?");
      }
      sql.append(" order by cedula");

      stTrabajador = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTrabajador.setLong(1, idTipoPersonal);
      int contador = 1;
      if ((primerNombre != null) && (!primerNombre.equals(""))) {
        contador++;
        stTrabajador.setString(contador, "%" + primerNombre + "%");
      }
      if ((segundoNombre != null) && (!segundoNombre.equals(""))) {
        contador++;
        stTrabajador.setString(contador, "%" + segundoNombre + "%");
      }
      if ((primerApellido != null) && (!primerApellido.equals(""))) {
        contador++;
        stTrabajador.setString(contador, "%" + primerApellido + "%");
      }
      if ((segundoApellido != null) && (!segundoApellido.equals(""))) {
        contador++;
        stTrabajador.setString(contador, "%" + segundoApellido + "%");
      }

      rsTrabajador = stTrabajador.executeQuery();

      while (rsTrabajador.next()) {
        TrabajadorAux trabajador = new TrabajadorAux();
        trabajador.setCedula(rsTrabajador.getInt("cedula"));
        trabajador.setIdTrabajador(rsTrabajador.getLong("id_trabajador"));
        trabajador.setCodigoNomina(rsTrabajador.getInt("codigo_nomina"));
        trabajador.setTipoPersonal(rsTrabajador.getString("nombre"));
        trabajador.setPrimerNombre(rsTrabajador.getString("primer_nombre"));
        trabajador.setSegundoNombre(rsTrabajador.getString("segundo_nombre"));
        trabajador.setPrimerApellido(rsTrabajador.getString("primer_apellido"));
        trabajador.setSegundoApellido(rsTrabajador.getString("segundo_apellido"));
        trabajador.setEstatus(rsTrabajador.getString("estatus"));

        col.add(trabajador);
      }

      return col;
    } finally {
      if (rsTrabajador != null) try {
          rsTrabajador.close();
        } catch (Exception localException3) {
        } if (stTrabajador != null) try {
          stTrabajador.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}