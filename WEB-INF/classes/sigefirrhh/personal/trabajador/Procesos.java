package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.expediente.Personal;

public class Procesos
  implements Serializable, PersistenceCapable
{
  private long idProcesos;
  private String estatus;
  private String parcialProximaNomina;
  private int diasProximaNomina;
  private int lunesProximaNomina;
  private int semanasAdelantadas;
  private int semanasCierreAdelantadas;
  private String procesoUno;
  private String procesodos;
  private String procesotres;
  private String procesocuatro;
  private Date fechaProcesoUno;
  private Date fechaProcesoDos;
  private Date fechaProcesoTres;
  private Date fechaProcesocuatro;
  private Date fechaReactivacion;
  private Personal personal;
  private TipoPersonal tipoPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "diasProximaNomina", "estatus", "fechaProcesoDos", "fechaProcesoTres", "fechaProcesoUno", "fechaProcesocuatro", "fechaReactivacion", "idProcesos", "lunesProximaNomina", "parcialProximaNomina", "personal", "procesoUno", "procesocuatro", "procesodos", "procesotres", "semanasAdelantadas", "semanasCierreAdelantadas", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 26, 21, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getDiasProximaNomina()
  {
    return jdoGetdiasProximaNomina(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public Date getFechaProcesocuatro()
  {
    return jdoGetfechaProcesocuatro(this);
  }

  public Date getFechaProcesoDos()
  {
    return jdoGetfechaProcesoDos(this);
  }

  public Date getFechaProcesoTres()
  {
    return jdoGetfechaProcesoTres(this);
  }

  public Date getFechaProcesoUno()
  {
    return jdoGetfechaProcesoUno(this);
  }

  public Date getFechaReactivacion()
  {
    return jdoGetfechaReactivacion(this);
  }

  public long getIdProcesos()
  {
    return jdoGetidProcesos(this);
  }

  public int getLunesProximaNomina()
  {
    return jdoGetlunesProximaNomina(this);
  }

  public String getParcialProximaNomina()
  {
    return jdoGetparcialProximaNomina(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public String getProcesocuatro()
  {
    return jdoGetprocesocuatro(this);
  }

  public String getProcesodos()
  {
    return jdoGetprocesodos(this);
  }

  public String getProcesotres()
  {
    return jdoGetprocesotres(this);
  }

  public String getProcesoUno()
  {
    return jdoGetprocesoUno(this);
  }

  public int getSemanasAdelantadas()
  {
    return jdoGetsemanasAdelantadas(this);
  }

  public int getSemanasCierreAdelantadas()
  {
    return jdoGetsemanasCierreAdelantadas(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setDiasProximaNomina(int i)
  {
    jdoSetdiasProximaNomina(this, i);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setFechaProcesocuatro(Date date)
  {
    jdoSetfechaProcesocuatro(this, date);
  }

  public void setFechaProcesoDos(Date date)
  {
    jdoSetfechaProcesoDos(this, date);
  }

  public void setFechaProcesoTres(Date date)
  {
    jdoSetfechaProcesoTres(this, date);
  }

  public void setFechaProcesoUno(Date date)
  {
    jdoSetfechaProcesoUno(this, date);
  }

  public void setFechaReactivacion(Date date)
  {
    jdoSetfechaReactivacion(this, date);
  }

  public void setIdProcesos(long l)
  {
    jdoSetidProcesos(this, l);
  }

  public void setLunesProximaNomina(int i)
  {
    jdoSetlunesProximaNomina(this, i);
  }

  public void setParcialProximaNomina(String string)
  {
    jdoSetparcialProximaNomina(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setProcesocuatro(String string)
  {
    jdoSetprocesocuatro(this, string);
  }

  public void setProcesodos(String string)
  {
    jdoSetprocesodos(this, string);
  }

  public void setProcesotres(String string)
  {
    jdoSetprocesotres(this, string);
  }

  public void setProcesoUno(String string)
  {
    jdoSetprocesoUno(this, string);
  }

  public void setSemanasAdelantadas(int i)
  {
    jdoSetsemanasAdelantadas(this, i);
  }

  public void setSemanasCierreAdelantadas(int i)
  {
    jdoSetsemanasCierreAdelantadas(this, i);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 18;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.Procesos"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Procesos());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Procesos localProcesos = new Procesos();
    localProcesos.jdoFlags = 1;
    localProcesos.jdoStateManager = paramStateManager;
    return localProcesos;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Procesos localProcesos = new Procesos();
    localProcesos.jdoCopyKeyFieldsFromObjectId(paramObject);
    localProcesos.jdoFlags = 1;
    localProcesos.jdoStateManager = paramStateManager;
    return localProcesos;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasProximaNomina);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProcesoDos);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProcesoTres);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProcesoUno);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProcesocuatro);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaReactivacion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idProcesos);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.lunesProximaNomina);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.parcialProximaNomina);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.procesoUno);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.procesocuatro);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.procesodos);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.procesotres);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanasAdelantadas);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanasCierreAdelantadas);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasProximaNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProcesoDos = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProcesoTres = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProcesoUno = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProcesocuatro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaReactivacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idProcesos = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lunesProximaNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.parcialProximaNomina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.procesoUno = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.procesocuatro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.procesodos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.procesotres = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanasAdelantadas = localStateManager.replacingIntField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanasCierreAdelantadas = localStateManager.replacingIntField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Procesos paramProcesos, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.diasProximaNomina = paramProcesos.diasProximaNomina;
      return;
    case 1:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramProcesos.estatus;
      return;
    case 2:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProcesoDos = paramProcesos.fechaProcesoDos;
      return;
    case 3:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProcesoTres = paramProcesos.fechaProcesoTres;
      return;
    case 4:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProcesoUno = paramProcesos.fechaProcesoUno;
      return;
    case 5:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProcesocuatro = paramProcesos.fechaProcesocuatro;
      return;
    case 6:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.fechaReactivacion = paramProcesos.fechaReactivacion;
      return;
    case 7:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.idProcesos = paramProcesos.idProcesos;
      return;
    case 8:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.lunesProximaNomina = paramProcesos.lunesProximaNomina;
      return;
    case 9:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.parcialProximaNomina = paramProcesos.parcialProximaNomina;
      return;
    case 10:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramProcesos.personal;
      return;
    case 11:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.procesoUno = paramProcesos.procesoUno;
      return;
    case 12:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.procesocuatro = paramProcesos.procesocuatro;
      return;
    case 13:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.procesodos = paramProcesos.procesodos;
      return;
    case 14:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.procesotres = paramProcesos.procesotres;
      return;
    case 15:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.semanasAdelantadas = paramProcesos.semanasAdelantadas;
      return;
    case 16:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.semanasCierreAdelantadas = paramProcesos.semanasCierreAdelantadas;
      return;
    case 17:
      if (paramProcesos == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramProcesos.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Procesos))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Procesos localProcesos = (Procesos)paramObject;
    if (localProcesos.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localProcesos, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ProcesosPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ProcesosPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProcesosPK))
      throw new IllegalArgumentException("arg1");
    ProcesosPK localProcesosPK = (ProcesosPK)paramObject;
    localProcesosPK.idProcesos = this.idProcesos;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProcesosPK))
      throw new IllegalArgumentException("arg1");
    ProcesosPK localProcesosPK = (ProcesosPK)paramObject;
    this.idProcesos = localProcesosPK.idProcesos;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProcesosPK))
      throw new IllegalArgumentException("arg2");
    ProcesosPK localProcesosPK = (ProcesosPK)paramObject;
    localProcesosPK.idProcesos = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProcesosPK))
      throw new IllegalArgumentException("arg2");
    ProcesosPK localProcesosPK = (ProcesosPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localProcesosPK.idProcesos);
  }

  private static final int jdoGetdiasProximaNomina(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.diasProximaNomina;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.diasProximaNomina;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 0))
      return paramProcesos.diasProximaNomina;
    return localStateManager.getIntField(paramProcesos, jdoInheritedFieldCount + 0, paramProcesos.diasProximaNomina);
  }

  private static final void jdoSetdiasProximaNomina(Procesos paramProcesos, int paramInt)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.diasProximaNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.diasProximaNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramProcesos, jdoInheritedFieldCount + 0, paramProcesos.diasProximaNomina, paramInt);
  }

  private static final String jdoGetestatus(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.estatus;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.estatus;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 1))
      return paramProcesos.estatus;
    return localStateManager.getStringField(paramProcesos, jdoInheritedFieldCount + 1, paramProcesos.estatus);
  }

  private static final void jdoSetestatus(Procesos paramProcesos, String paramString)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramProcesos, jdoInheritedFieldCount + 1, paramProcesos.estatus, paramString);
  }

  private static final Date jdoGetfechaProcesoDos(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.fechaProcesoDos;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.fechaProcesoDos;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 2))
      return paramProcesos.fechaProcesoDos;
    return (Date)localStateManager.getObjectField(paramProcesos, jdoInheritedFieldCount + 2, paramProcesos.fechaProcesoDos);
  }

  private static final void jdoSetfechaProcesoDos(Procesos paramProcesos, Date paramDate)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.fechaProcesoDos = paramDate;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.fechaProcesoDos = paramDate;
      return;
    }
    localStateManager.setObjectField(paramProcesos, jdoInheritedFieldCount + 2, paramProcesos.fechaProcesoDos, paramDate);
  }

  private static final Date jdoGetfechaProcesoTres(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.fechaProcesoTres;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.fechaProcesoTres;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 3))
      return paramProcesos.fechaProcesoTres;
    return (Date)localStateManager.getObjectField(paramProcesos, jdoInheritedFieldCount + 3, paramProcesos.fechaProcesoTres);
  }

  private static final void jdoSetfechaProcesoTres(Procesos paramProcesos, Date paramDate)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.fechaProcesoTres = paramDate;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.fechaProcesoTres = paramDate;
      return;
    }
    localStateManager.setObjectField(paramProcesos, jdoInheritedFieldCount + 3, paramProcesos.fechaProcesoTres, paramDate);
  }

  private static final Date jdoGetfechaProcesoUno(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.fechaProcesoUno;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.fechaProcesoUno;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 4))
      return paramProcesos.fechaProcesoUno;
    return (Date)localStateManager.getObjectField(paramProcesos, jdoInheritedFieldCount + 4, paramProcesos.fechaProcesoUno);
  }

  private static final void jdoSetfechaProcesoUno(Procesos paramProcesos, Date paramDate)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.fechaProcesoUno = paramDate;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.fechaProcesoUno = paramDate;
      return;
    }
    localStateManager.setObjectField(paramProcesos, jdoInheritedFieldCount + 4, paramProcesos.fechaProcesoUno, paramDate);
  }

  private static final Date jdoGetfechaProcesocuatro(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.fechaProcesocuatro;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.fechaProcesocuatro;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 5))
      return paramProcesos.fechaProcesocuatro;
    return (Date)localStateManager.getObjectField(paramProcesos, jdoInheritedFieldCount + 5, paramProcesos.fechaProcesocuatro);
  }

  private static final void jdoSetfechaProcesocuatro(Procesos paramProcesos, Date paramDate)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.fechaProcesocuatro = paramDate;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.fechaProcesocuatro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramProcesos, jdoInheritedFieldCount + 5, paramProcesos.fechaProcesocuatro, paramDate);
  }

  private static final Date jdoGetfechaReactivacion(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.fechaReactivacion;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.fechaReactivacion;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 6))
      return paramProcesos.fechaReactivacion;
    return (Date)localStateManager.getObjectField(paramProcesos, jdoInheritedFieldCount + 6, paramProcesos.fechaReactivacion);
  }

  private static final void jdoSetfechaReactivacion(Procesos paramProcesos, Date paramDate)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.fechaReactivacion = paramDate;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.fechaReactivacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramProcesos, jdoInheritedFieldCount + 6, paramProcesos.fechaReactivacion, paramDate);
  }

  private static final long jdoGetidProcesos(Procesos paramProcesos)
  {
    return paramProcesos.idProcesos;
  }

  private static final void jdoSetidProcesos(Procesos paramProcesos, long paramLong)
  {
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.idProcesos = paramLong;
      return;
    }
    localStateManager.setLongField(paramProcesos, jdoInheritedFieldCount + 7, paramProcesos.idProcesos, paramLong);
  }

  private static final int jdoGetlunesProximaNomina(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.lunesProximaNomina;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.lunesProximaNomina;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 8))
      return paramProcesos.lunesProximaNomina;
    return localStateManager.getIntField(paramProcesos, jdoInheritedFieldCount + 8, paramProcesos.lunesProximaNomina);
  }

  private static final void jdoSetlunesProximaNomina(Procesos paramProcesos, int paramInt)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.lunesProximaNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.lunesProximaNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramProcesos, jdoInheritedFieldCount + 8, paramProcesos.lunesProximaNomina, paramInt);
  }

  private static final String jdoGetparcialProximaNomina(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.parcialProximaNomina;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.parcialProximaNomina;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 9))
      return paramProcesos.parcialProximaNomina;
    return localStateManager.getStringField(paramProcesos, jdoInheritedFieldCount + 9, paramProcesos.parcialProximaNomina);
  }

  private static final void jdoSetparcialProximaNomina(Procesos paramProcesos, String paramString)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.parcialProximaNomina = paramString;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.parcialProximaNomina = paramString;
      return;
    }
    localStateManager.setStringField(paramProcesos, jdoInheritedFieldCount + 9, paramProcesos.parcialProximaNomina, paramString);
  }

  private static final Personal jdoGetpersonal(Procesos paramProcesos)
  {
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.personal;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 10))
      return paramProcesos.personal;
    return (Personal)localStateManager.getObjectField(paramProcesos, jdoInheritedFieldCount + 10, paramProcesos.personal);
  }

  private static final void jdoSetpersonal(Procesos paramProcesos, Personal paramPersonal)
  {
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramProcesos, jdoInheritedFieldCount + 10, paramProcesos.personal, paramPersonal);
  }

  private static final String jdoGetprocesoUno(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.procesoUno;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.procesoUno;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 11))
      return paramProcesos.procesoUno;
    return localStateManager.getStringField(paramProcesos, jdoInheritedFieldCount + 11, paramProcesos.procesoUno);
  }

  private static final void jdoSetprocesoUno(Procesos paramProcesos, String paramString)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.procesoUno = paramString;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.procesoUno = paramString;
      return;
    }
    localStateManager.setStringField(paramProcesos, jdoInheritedFieldCount + 11, paramProcesos.procesoUno, paramString);
  }

  private static final String jdoGetprocesocuatro(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.procesocuatro;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.procesocuatro;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 12))
      return paramProcesos.procesocuatro;
    return localStateManager.getStringField(paramProcesos, jdoInheritedFieldCount + 12, paramProcesos.procesocuatro);
  }

  private static final void jdoSetprocesocuatro(Procesos paramProcesos, String paramString)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.procesocuatro = paramString;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.procesocuatro = paramString;
      return;
    }
    localStateManager.setStringField(paramProcesos, jdoInheritedFieldCount + 12, paramProcesos.procesocuatro, paramString);
  }

  private static final String jdoGetprocesodos(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.procesodos;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.procesodos;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 13))
      return paramProcesos.procesodos;
    return localStateManager.getStringField(paramProcesos, jdoInheritedFieldCount + 13, paramProcesos.procesodos);
  }

  private static final void jdoSetprocesodos(Procesos paramProcesos, String paramString)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.procesodos = paramString;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.procesodos = paramString;
      return;
    }
    localStateManager.setStringField(paramProcesos, jdoInheritedFieldCount + 13, paramProcesos.procesodos, paramString);
  }

  private static final String jdoGetprocesotres(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.procesotres;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.procesotres;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 14))
      return paramProcesos.procesotres;
    return localStateManager.getStringField(paramProcesos, jdoInheritedFieldCount + 14, paramProcesos.procesotres);
  }

  private static final void jdoSetprocesotres(Procesos paramProcesos, String paramString)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.procesotres = paramString;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.procesotres = paramString;
      return;
    }
    localStateManager.setStringField(paramProcesos, jdoInheritedFieldCount + 14, paramProcesos.procesotres, paramString);
  }

  private static final int jdoGetsemanasAdelantadas(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.semanasAdelantadas;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.semanasAdelantadas;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 15))
      return paramProcesos.semanasAdelantadas;
    return localStateManager.getIntField(paramProcesos, jdoInheritedFieldCount + 15, paramProcesos.semanasAdelantadas);
  }

  private static final void jdoSetsemanasAdelantadas(Procesos paramProcesos, int paramInt)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.semanasAdelantadas = paramInt;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.semanasAdelantadas = paramInt;
      return;
    }
    localStateManager.setIntField(paramProcesos, jdoInheritedFieldCount + 15, paramProcesos.semanasAdelantadas, paramInt);
  }

  private static final int jdoGetsemanasCierreAdelantadas(Procesos paramProcesos)
  {
    if (paramProcesos.jdoFlags <= 0)
      return paramProcesos.semanasCierreAdelantadas;
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.semanasCierreAdelantadas;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 16))
      return paramProcesos.semanasCierreAdelantadas;
    return localStateManager.getIntField(paramProcesos, jdoInheritedFieldCount + 16, paramProcesos.semanasCierreAdelantadas);
  }

  private static final void jdoSetsemanasCierreAdelantadas(Procesos paramProcesos, int paramInt)
  {
    if (paramProcesos.jdoFlags == 0)
    {
      paramProcesos.semanasCierreAdelantadas = paramInt;
      return;
    }
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.semanasCierreAdelantadas = paramInt;
      return;
    }
    localStateManager.setIntField(paramProcesos, jdoInheritedFieldCount + 16, paramProcesos.semanasCierreAdelantadas, paramInt);
  }

  private static final TipoPersonal jdoGettipoPersonal(Procesos paramProcesos)
  {
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
      return paramProcesos.tipoPersonal;
    if (localStateManager.isLoaded(paramProcesos, jdoInheritedFieldCount + 17))
      return paramProcesos.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramProcesos, jdoInheritedFieldCount + 17, paramProcesos.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(Procesos paramProcesos, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramProcesos.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesos.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramProcesos, jdoInheritedFieldCount + 17, paramProcesos.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}