package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.ManualPersonal;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.LugarPago;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;

public class UbicarTrabajadorForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(UbicarTrabajadorForm.class.getName());
  private Trabajador trabajador;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private CargoNoGenFacade cargoFacade = new CargoNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private Collection resultTrabajador;
  private Personal personal;
  private int findTrabajadorCedula;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private int findTrabajadorCodigoNomina;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private boolean showResultTrabajador;
  private String findSelectPersonal;
  private Collection colPersonal;
  private Collection findColTipoPersonal;
  private Collection colCargo;
  private Collection colLugarPago;
  private Collection colTurno;
  private Collection colBancoNomina;
  private Collection colBancoLph;
  private Collection colBancoFid;
  private Collection colDependenciaReal;
  private Collection colManualCargoForCargoReal = new ArrayList();
  private Collection colCargoReal;
  private String selectLugarPago;
  private String selectTurno;
  private String selectBancoNomina;
  private String selectBancoLph;
  private String selectBancoFid;
  private String selectCausaMovimiento;
  private String selectDependenciaReal;
  private String selectManualCargoForCargoReal;
  private String selectCargoReal;
  private boolean showData;
  private String findSelectTrabajadorIdTipoPersonal;
  private Object stateResultPersonal = null;

  private Object stateResultTrabajadorByPersonal = null;

  private void resetResultTrabajador()
  {
    this.resultTrabajador = null;

    this.trabajador = null;

    this.showResultTrabajador = false;
  }
  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedula(this.findTrabajadorCedula, 
        this.login.getIdOrganismo());

      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
      log.error("showResult" + this.showResultTrabajador);
      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidos(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, this.login.getIdOrganismo());
        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public void setFindSelectTrabajadorIdTipoPersonal(String string)
  {
    this.findSelectTrabajadorIdTipoPersonal = string;
  }

  public String getSelectLugarPago() {
    return this.selectLugarPago;
  }
  public void setSelectLugarPago(String valLugarPago) {
    Iterator iterator = this.colLugarPago.iterator();
    LugarPago lugarPago = null;
    this.trabajador.setLugarPago(null);
    while (iterator.hasNext()) {
      lugarPago = (LugarPago)iterator.next();
      if (String.valueOf(lugarPago.getIdLugarPago()).equals(
        valLugarPago)) {
        this.trabajador.setLugarPago(
          lugarPago);
        break;
      }
    }
    this.selectLugarPago = valLugarPago;
  }
  public String getSelectTurno() {
    return this.selectTurno;
  }
  public void setSelectTurno(String valTurno) {
    Iterator iterator = this.colTurno.iterator();
    Turno turno = null;
    this.trabajador.setTurno(null);
    while (iterator.hasNext()) {
      turno = (Turno)iterator.next();
      if (String.valueOf(turno.getIdTurno()).equals(
        valTurno)) {
        this.trabajador.setTurno(
          turno);
        break;
      }
    }
    this.selectTurno = valTurno;
  }
  public String getSelectBancoNomina() {
    return this.selectBancoNomina;
  }
  public void setSelectBancoNomina(String valBancoNomina) {
    Iterator iterator = this.colBancoNomina.iterator();
    Banco bancoNomina = null;
    this.trabajador.setBancoNomina(null);
    while (iterator.hasNext()) {
      bancoNomina = (Banco)iterator.next();
      if (String.valueOf(bancoNomina.getIdBanco()).equals(
        valBancoNomina)) {
        this.trabajador.setBancoNomina(
          bancoNomina);
        break;
      }
    }
    this.selectBancoNomina = valBancoNomina;
  }
  public String getSelectBancoLph() {
    return this.selectBancoLph;
  }
  public void setSelectBancoLph(String valBancoLph) {
    Iterator iterator = this.colBancoLph.iterator();
    Banco bancoLph = null;
    this.trabajador.setBancoLph(null);
    while (iterator.hasNext()) {
      bancoLph = (Banco)iterator.next();
      if (String.valueOf(bancoLph.getIdBanco()).equals(
        valBancoLph)) {
        this.trabajador.setBancoLph(
          bancoLph);
        break;
      }
    }
    this.selectBancoLph = valBancoLph;
  }
  public String getSelectBancoFid() {
    return this.selectBancoFid;
  }
  public void setSelectBancoFid(String valBancoFid) {
    Iterator iterator = this.colBancoFid.iterator();
    Banco bancoFid = null;
    this.trabajador.setBancoFid(null);
    while (iterator.hasNext()) {
      bancoFid = (Banco)iterator.next();
      if (String.valueOf(bancoFid.getIdBanco()).equals(
        valBancoFid)) {
        this.trabajador.setBancoFid(
          bancoFid);
        break;
      }
    }
    this.selectBancoFid = valBancoFid;
  }
  public String getSelectCausaMovimiento() {
    return this.selectCausaMovimiento;
  }

  public String getSelectDependenciaReal() {
    return this.selectDependenciaReal;
  }
  public void setSelectDependenciaReal(String valDependenciaReal) {
    Iterator iterator = this.colDependenciaReal.iterator();
    Dependencia dependenciaReal = null;
    this.trabajador.setDependenciaReal(null);
    while (iterator.hasNext()) {
      dependenciaReal = (Dependencia)iterator.next();
      if (String.valueOf(dependenciaReal.getIdDependencia()).equals(
        valDependenciaReal)) {
        this.trabajador.setDependenciaReal(
          dependenciaReal);
        break;
      }
    }
    this.selectDependenciaReal = valDependenciaReal;
  }
  public String getSelectManualCargoForCargoReal() {
    return this.selectManualCargoForCargoReal;
  }
  public void setSelectManualCargoForCargoReal(String valManualCargoForCargoReal) {
    this.selectManualCargoForCargoReal = valManualCargoForCargoReal;
  }
  public void changeManualCargoForCargoReal(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargoReal = null;
      if (idManualCargo > 0L)
        this.colCargoReal = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowManualCargoForCargoReal() { return this.colManualCargoForCargoReal != null; }

  public String getSelectCargoReal() {
    return this.selectCargoReal;
  }
  public void setSelectCargoReal(String valCargoReal) {
    Iterator iterator = this.colCargoReal.iterator();
    Cargo cargoReal = null;
    this.trabajador.setCargoReal(null);
    while (iterator.hasNext()) {
      cargoReal = (Cargo)iterator.next();
      if (String.valueOf(cargoReal.getIdCargo()).equals(
        valCargoReal)) {
        this.trabajador.setCargoReal(
          cargoReal);
        break;
      }
    }
    this.selectCargoReal = valCargoReal;
  }
  public boolean isShowCargoReal() {
    return this.colCargoReal != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public Trabajador getTrabajador() {
    if (this.trabajador == null) {
      this.trabajador = new Trabajador();
    }
    return this.trabajador;
  }

  public UbicarTrabajadorForm()
    throws Exception
  {
    if (this.login == null) {
      FacesContext context = FacesContext.getCurrentInstance();
      this.login = 
        ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
        context, 
        "loginSession"));
    }
  }

  public Collection getFindColTipoPersonal()
  {
    if (this.findColTipoPersonal == null) {
      try {
        log.error("paso por fctp 1");
        this.findColTipoPersonal = new ArrayList();
        this.findColTipoPersonal = 
          this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
        log.error("paso por fctp 2");
      } catch (Exception e) {
        log.error("Excepcion controlada:", e);
      }
    }

    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getColLugarPago()
  {
    if (this.colLugarPago == null) {
      try {
        this.colLugarPago = 
          this.estructuraFacade.findAllLugarPago();
      } catch (Exception e) {
        log.error("Excepcion controlada:", e);
      }

    }

    Collection col = new ArrayList();
    Iterator iterator = this.colLugarPago.iterator();
    LugarPago lugarPago = null;
    while (iterator.hasNext()) {
      lugarPago = (LugarPago)iterator.next();
      col.add(new SelectItem(
        String.valueOf(lugarPago.getIdLugarPago()), 
        lugarPago.toString()));
    }
    return col;
  }

  public Collection getColTurno() {
    if (this.colTurno == null) {
      try {
        this.colTurno = 
          this.definicionesFacade.findTurnoByOrganismo(
          this.login.getOrganismo().getIdOrganismo());
      }
      catch (Exception localException)
      {
      }
    }
    Collection col = new ArrayList();
    Iterator iterator = this.colTurno.iterator();
    Turno turno = null;
    while (iterator.hasNext()) {
      turno = (Turno)iterator.next();
      col.add(new SelectItem(
        String.valueOf(turno.getIdTurno()), 
        turno.toString()));
    }
    return col;
  }
  public Collection getListRegimen() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_REGIMEN.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListFeVida() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListRiesgo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_RIESGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListFormaPago()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_PAGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColBancoNomina()
  {
    if (this.colBancoNomina == null) {
      try {
        this.colBancoNomina = 
          this.definicionesFacade.findAllBanco();
      }
      catch (Exception localException)
      {
      }
    }
    Collection col = new ArrayList();
    Iterator iterator = this.colBancoNomina.iterator();
    Banco bancoNomina = null;
    while (iterator.hasNext()) {
      bancoNomina = (Banco)iterator.next();
      col.add(new SelectItem(
        String.valueOf(bancoNomina.getIdBanco()), 
        bancoNomina.toString()));
    }
    return col;
  }

  public Collection getListTipoCtaNomina() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_CUENTA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColBancoLph()
  {
    if (this.colBancoLph == null) {
      try {
        this.colBancoLph = 
          this.definicionesFacade.findAllBanco();
      }
      catch (Exception localException)
      {
      }
    }
    Collection col = new ArrayList();
    Iterator iterator = this.colBancoLph.iterator();
    Banco bancoLph = null;
    while (iterator.hasNext()) {
      bancoLph = (Banco)iterator.next();
      col.add(new SelectItem(
        String.valueOf(bancoLph.getIdBanco()), 
        bancoLph.toString()));
    }
    return col;
  }

  public Collection getColBancoFid()
  {
    if (this.colBancoFid == null)
      try {
        this.colBancoFid = 
          this.definicionesFacade.findAllBanco();
      }
      catch (Exception localException)
      {
      }
    Collection col = new ArrayList();
    Iterator iterator = this.colBancoFid.iterator();
    Banco bancoFid = null;
    while (iterator.hasNext()) {
      bancoFid = (Banco)iterator.next();
      col.add(new SelectItem(
        String.valueOf(bancoFid.getIdBanco()), 
        bancoFid.toString()));
    }
    return col;
  }

  public Collection getColDependenciaReal()
  {
    if (this.colDependenciaReal == null)
      try {
        this.colDependenciaReal = 
          this.estructuraFacade.findDependenciaByOrganismo(
          getLogin().getOrganismo().getIdOrganismo());
      }
      catch (Exception localException)
      {
      }
    Collection col = new ArrayList();
    Iterator iterator = this.colDependenciaReal.iterator();
    Dependencia dependenciaReal = null;
    while (iterator.hasNext()) {
      dependenciaReal = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependenciaReal.getIdDependencia()), 
        dependenciaReal.toString()));
    }
    return col;
  }

  public Collection getColManualCargoForCargoReal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargoReal.iterator();
    ManualCargo manualCargoForCargoReal = null;
    while (iterator.hasNext()) {
      manualCargoForCargoReal = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargoReal.getIdManualCargo()), 
        manualCargoForCargoReal.toString()));
    }
    return col;
  }

  public Collection getColCargoReal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargoReal.iterator();
    Cargo cargoReal = null;
    while (iterator.hasNext()) {
      cargoReal = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargoReal.getIdCargo()), 
        cargoReal.toString()));
    }
    return col;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectLugarPago = null;
    this.selectTurno = null;
    this.selectBancoNomina = null;
    this.selectBancoLph = null;
    this.selectBancoFid = null;
    this.selectCausaMovimiento = null;
    this.selectDependenciaReal = null;
    this.selectCargoReal = null;
    this.selectManualCargoForCargoReal = null;

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = null;
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);

      if (this.trabajador.getLugarPago() != null) {
        this.selectLugarPago = 
          String.valueOf(this.trabajador.getLugarPago().getIdLugarPago());
      }
      if (this.trabajador.getTurno() != null) {
        this.selectTurno = 
          String.valueOf(this.trabajador.getTurno().getIdTurno());
      }
      if (this.trabajador.getBancoNomina() != null) {
        this.selectBancoNomina = 
          String.valueOf(this.trabajador.getBancoNomina().getIdBanco());
      }
      if (this.trabajador.getBancoLph() != null) {
        this.selectBancoLph = 
          String.valueOf(this.trabajador.getBancoLph().getIdBanco());
      }
      if (this.trabajador.getBancoFid() != null) {
        this.selectBancoFid = 
          String.valueOf(this.trabajador.getBancoFid().getIdBanco());
      }

      if (this.trabajador.getDependenciaReal() != null) {
        this.selectDependenciaReal = 
          String.valueOf(this.trabajador.getDependenciaReal().getIdDependencia());
      }
      if (this.trabajador.getCargoReal() != null) {
        this.selectCargoReal = 
          String.valueOf(this.trabajador.getCargoReal().getIdCargo());
      }

      Cargo cargoReal = null;
      ManualCargo manualCargoForCargoReal = null;

      if (this.trabajador.getCargoReal() != null) {
        long idCargoReal = 
          this.trabajador.getCargoReal().getIdCargo();
        this.selectCargoReal = String.valueOf(idCargoReal);
        cargoReal = this.cargoFacade.findCargoById(
          idCargoReal);
        this.colCargoReal = this.cargoFacade.findCargoByManualCargo(
          cargoReal.getManualCargo().getIdManualCargo());

        long idManualCargoForCargoReal = 
          this.trabajador.getCargoReal().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargoReal = String.valueOf(idManualCargoForCargoReal);
        manualCargoForCargoReal = 
          this.cargoFacade.findManualCargoById(
          idManualCargoForCargoReal);
      }

      Collection colManualPersonal = this.cargoFacade.findManualPersonalByTipoPersonal(this.trabajador.getTipoPersonal().getIdTipoPersonal());
      Iterator iterManualPersonal = colManualPersonal.iterator();
      while (iterManualPersonal.hasNext()) {
        ManualPersonal manualPersonal = (ManualPersonal)iterManualPersonal.next();

        ManualCargo manualCargo = this.cargoFacade.findManualCargoById(manualPersonal.getManualCargo().getIdManualCargo());
        this.colManualCargoForCargoReal.add(manualCargo);
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    this.selected = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.trabajador.getFechaIngreso() != null) && 
      (this.trabajador.getFechaIngreso().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Ingreso Organismo no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaIngresoApn() != null) && 
      (this.trabajador.getFechaIngresoApn().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Ingreso APN no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaVacaciones() != null) && 
      (this.trabajador.getFechaVacaciones().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Vacaciones no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaPrestaciones() != null) && 
      (this.trabajador.getFechaPrestaciones().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Prestaciones no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaAntiguedad() != null) && 
      (this.trabajador.getFechaAntiguedad().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Antiguedad no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaEgreso() != null) && 
      (this.trabajador.getFechaEgreso().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Egreso no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaJubilacion() != null) && 
      (this.trabajador.getFechaJubilacion().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Jubilacion no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaSalidaSig() != null) && 
      (this.trabajador.getFechaSalidaSig().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Salida Sig no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaEntradaSig() != null) && 
      (this.trabajador.getFechaEntradaSig().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Entrada Sig no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.trabajador.setPersonal(
          this.personal);
        this.trabajadorFacade.addTrabajador(
          this.trabajador);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.trabajadorFacade.updateTrabajador(
          this.trabajador);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.trabajadorFacade.deleteTrabajador(
        this.trabajador);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public boolean isShowBancoNominaAux() {
    try {
      return this.trabajador.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowTipoCtaNominaAux() {
    try {
      return this.trabajador.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowCuentaNominaAux() {
    try {
      return this.trabajador.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.trabajador = new Trabajador();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.trabajador = new Trabajador();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return this.selected;
  }

  public boolean isShowResultPersonal()
  {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return this.adding;
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getFindTrabajadorCedula()
  {
    return this.findTrabajadorCedula;
  }

  public int getFindTrabajadorCodigoNomina()
  {
    return this.findTrabajadorCodigoNomina;
  }

  public String getFindTrabajadorPrimerApellido()
  {
    return this.findTrabajadorPrimerApellido;
  }

  public String getFindTrabajadorPrimerNombre()
  {
    return this.findTrabajadorPrimerNombre;
  }

  public String getFindTrabajadorSegundoApellido()
  {
    return this.findTrabajadorSegundoApellido;
  }

  public String getFindTrabajadorSegundoNombre()
  {
    return this.findTrabajadorSegundoNombre;
  }

  public void setFindTrabajadorCedula(int i)
  {
    this.findTrabajadorCedula = i;
  }

  public void setFindTrabajadorCodigoNomina(int i)
  {
    this.findTrabajadorCodigoNomina = i;
  }

  public void setFindTrabajadorPrimerApellido(String string)
  {
    this.findTrabajadorPrimerApellido = string;
  }

  public void setFindTrabajadorPrimerNombre(String string)
  {
    this.findTrabajadorPrimerNombre = string;
  }

  public void setFindTrabajadorSegundoApellido(String string)
  {
    this.findTrabajadorSegundoApellido = string;
  }

  public void setFindTrabajadorSegundoNombre(String string)
  {
    this.findTrabajadorSegundoNombre = string;
  }

  public String getFindSelectTrabajadorIdTipoPersonal()
  {
    return this.findSelectTrabajadorIdTipoPersonal;
  }

  public boolean isShowResultTrabajador()
  {
    return this.showResultTrabajador;
  }

  public Collection getResultTrabajador()
  {
    return this.resultTrabajador;
  }

  public void setShowData(boolean b)
  {
    this.showData = b;
  }
}