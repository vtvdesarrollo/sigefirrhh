package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.Ausencia;

public class ReportRepososAusenciasPermisosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportRepososAusenciasPermisosForm.class.getName());
  private static final long serialVersionUID = 1L;
  private int reportId;
  private String formato = "1";
  private long idTipoPersonal;
  private long idDependencia = 0L;
  private String idClase = "0";
  private String reportName;
  private String orden = "A";
  private Date fechaTope;
  private Date fechaTopeFinal;
  private int edadMinima;
  private int edadMaxima;
  private int servicioMinimo;
  private int servicioMaximo;
  private Calendar fechaInicialEdad;
  private Calendar fechaFinalEdad;
  private Calendar fechaInicialServicio;
  private Calendar fechaFinalServicio;
  private Collection listTipoPersonal;
  private Collection listClase;
  private Collection listDependencia;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private EstructuraFacade estructuraFacade;

  public ReportRepososAusenciasPermisosForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();

    this.reportName = "ReportAusenciasPermisosForm";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      private static final long serialVersionUID = 1L;

      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportRepososAusenciasPermisosForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listDependencia = this.estructuraFacade.findAllDependencia();
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e)
    {
      this.listDependencia = new ArrayList();
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      String archivoReporte = "";

      if (this.formato.equals("2")) {
        archivoReporte = "a_";
      }
      if (this.orden.equals("A")) {
        archivoReporte = archivoReporte + "Reposogneral1alf";
      }
      else if (this.orden.equals("C"))
        archivoReporte = archivoReporte + "Reposogneral1ced";
      else if (this.orden.equals("N")) {
        archivoReporte = archivoReporte + "Reposogneral1cod";
      }

      if (this.idDependencia == 0L)
        archivoReporte = archivoReporte + "dep";
      else if (this.idDependencia != 0L) {
        archivoReporte = archivoReporte + "dep1";
      }

      if (this.idClase.equals("R"))
        archivoReporte = archivoReporte + "Rep";
      else if (this.idClase.equals("P"))
        archivoReporte = archivoReporte + "Per";
      else if (this.idClase.equals("A")) {
        archivoReporte = archivoReporte + "Aus";
      }

      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      if (this.idDependencia != 0L) {
        parameters.put("dependencia", new Long(this.idDependencia));
      }

      this.fechaInicialServicio = Calendar.getInstance();
      this.fechaInicialServicio.setTime(this.fechaTope);
      this.fechaFinalServicio = Calendar.getInstance();
      this.fechaFinalServicio.setTime(this.fechaTopeFinal);

      log.error("FechafinalServicio " + this.fechaFinalServicio.getTime());
      log.error("FechainicialServicio " + this.fechaInicialServicio.getTime());

      parameters.put("fecha_ini", this.fechaInicialServicio.getTime());
      parameters.put("fecha_fin", this.fechaFinalServicio.getTime());

      log.error("Clase " + this.idClase);

      if (this.idClase.equals("R"))
        parameters.put("clase", this.idClase);
      else if (this.idClase.equals("P"))
        parameters.put("clase", this.idClase);
      else if (this.idClase.equals("A")) {
        parameters.put("clase", this.idClase);
      }

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(archivoReporte);
      report.setParameters(parameters);
      log.error("Parametros " + parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/trabajador");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error(e);
    }
    return null;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l)
  {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getListClase() {
    Collection col = new ArrayList();

    Iterator iterEntry = Ausencia.LISTA_CLASE.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListDependencia()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listDependencia, "sigefirrhh.base.estructura.Dependencia");
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public String getOrden()
  {
    return this.orden;
  }

  public void setOrden(String string)
  {
    this.orden = string;
  }

  public long getIdDependencia()
  {
    return this.idDependencia;
  }

  public void setIdDependencia(long l)
  {
    this.idDependencia = l;
  }

  public String getidClase() {
    return this.idClase;
  }

  public void setIdClase(String l)
  {
    this.idClase = l;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String string)
  {
    this.formato = string;
  }

  public int getServicioMaximo() {
    return this.servicioMaximo;
  }
  public void setServicioMaximo(int servicioMaximo) {
    this.servicioMaximo = servicioMaximo;
  }
  public int getServicioMinimo() {
    return this.servicioMinimo;
  }
  public void setServicioMinimo(int servicioMinimo) {
    this.servicioMinimo = servicioMinimo;
  }
  public Date getFechaTope() {
    return this.fechaTope;
  }
  public void setFechaTope(Date fechaTope) {
    this.fechaTope = fechaTope;
  }
  public Date getFechaTopeFinal() {
    return this.fechaTopeFinal;
  }
  public void setFechaTopeFinal(Date fechaTopeFinal) {
    this.fechaTopeFinal = fechaTopeFinal;
  }
  public int getEdadMaxima() {
    return this.edadMaxima;
  }
  public void setEdadMaxima(int edadMaxima) {
    this.edadMaxima = edadMaxima;
  }
  public int getEdadMinima() {
    return this.edadMinima;
  }
  public void setEdadMinima(int edadMinima) {
    this.edadMinima = edadMinima;
  }
}