package sigefirrhh.personal.trabajador;

import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ConceptoFijoNoGenBeanBusiness
{
  public Collection findForNomina(long idTrabajador, Long idFrecuenciaPago, String estatus, String criterio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && estatus == pEstatus && frecuenciaTipoPersonal.frecuenciaPago.idFrecuenciaPago == pIdFrecuenciaPago && conceptoTipoPersonal.concepto.codConcepto" + criterio;

    Query query = pm.newQuery(ConceptoFijo.class, filter);

    query.declareParameters("long pIdTrabajador, long pIdFrecuenciaPago, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pIdFrecuenciaPago", idFrecuenciaPago);
    parameters.put("pEstatus", estatus);

    Collection colConceptoFijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoFijo);

    return colConceptoFijo;
  }

  public Collection findByTrabajadorAndConceptoTipoPersonal(long idTrabajador, long idConceptoTipoPersonal) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal";

    Query query = pm.newQuery(ConceptoFijo.class, filter);

    query.declareParameters("long pIdTrabajador, long pIdConceptoTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    Collection colConceptoFijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoFijo;
  }

  public Collection findForRegistroCargos(long idTrabajador, int codFrecuenciaPago, String metodo) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String parametro = "S";

    String filter = "trabajador.idTrabajador == pIdTrabajador && conceptoTipoPersonal.concepto." + metodo + " ==pParametro && frecuenciaTipoPersonal.frecuenciaPago.codFrecuencia == pCodFrecuenciaPago";

    Query query = pm.newQuery(ConceptoFijo.class, filter);

    query.declareParameters("long pIdTrabajador, int pCodFrecuenciaPago");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pCodFrecuenciaPago", new Integer(codFrecuenciaPago));

    Collection colConceptoFijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoFijo;
  }

  public Collection findFromConceptoAsociado(long idTrabajador, long idConceptoTipoPersonal)
    throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = null;
    String estatus = "A";

    filter = "trabajador.idTrabajador == pIdTrabajador && estatus == pEstatus && conceptoTipoPersonal.idConceptoTipoPersonal == ca.conceptoAsociar.idConceptoTipoPersonal ";

    Query query = pm.newQuery(ConceptoFijo.class, filter);
    query.declareParameters("long pIdTrabajador, long pIdConceptoTipoPersonal, String pEstatus");
    query.declareImports("import sigefirrhh.base.definiciones.ConceptoAsociado");
    query.declareVariables("ConceptoAsociado ca");

    parameters.put("pEstatus", estatus);
    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    Collection colConceptoFijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoFijo;
  }

  public Collection findByTrabajadorConceptoTipoPersonalFrecuenciaTipoPersonal(long idTrabajador, long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal && frecuenciaTipoPersonal.idFrecuenciaTipoPersonal == pIdFrecuenciaTipoPersonal";

    Query query = pm.newQuery(ConceptoFijo.class, filter);

    query.declareParameters("long pIdTrabajador, long pIdConceptoTipoPersonal, long pIdFrecuenciaTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));
    parameters.put("pIdFrecuenciaTipoPersonal", new Long(idFrecuenciaTipoPersonal));

    Collection colConceptoFijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoFijo;
  }

  public Collection findByTrabajadorNoPersistente(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(ConceptoFijo.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoFijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoFijo;
  }

  public Collection findByTrabajadorAndCodConcepto(long idTrabajador, String codConcepto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && conceptoTipoPersonal.concepto.codConcepto == pCodConcepto";

    Query query = pm.newQuery(ConceptoFijo.class, filter);

    query.declareParameters("long pIdTrabajador, String pCodConcepto");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pCodConcepto", codConcepto);

    Collection colConceptoFijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoFijo;
  }

  public double findMontoByTrabajadorQuincenal(long idTrabajador, int criterio)
    throws Exception
  {
    double monto = 0.0D;

    Connection connection = null;
    StringBuffer sql = new StringBuffer();
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select cf.monto,  fp.cod_frecuencia_pago");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, concepto c, ");
      sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp");
      sql.append(" where cf.id_trabajador = ?");
      sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and ctp.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      if (criterio == 1) {
        sql.append(" and c.sueldo_basico = 'S'");
      } else if (criterio == 2) {
        sql.append(" and c.compensacion = 'S'");
      } else if (criterio == 3) {
        sql.append(" and c.compensacion = 'N'");
        sql.append(" and c.sueldo_basico = 'N'");
        sql.append(" and c.cod_concepto < '5000'");
      } else if (criterio == 4) {
        sql.append(" and c.cod_concepto < '5000'");
      }
      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stRegistros.setLong(1, idTrabajador);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next()) {
        if (rsRegistros.getInt("cod_frecuencia_pago") == 3)
          monto += rsRegistros.getDouble("monto") * 2.0D;
        else
          monto += rsRegistros.getDouble("monto");
      }
    }
    finally
    {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return monto;
  }

  public double findMontoProyectadoByTrabajadorQuincenal(long idTrabajador, int criterio)
    throws Exception
  {
    double monto = 0.0D;

    Connection connection = null;

    StringBuffer sql = new StringBuffer();

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select cp.monto,  fp.cod_frecuencia_pago");
      sql.append(" from conceptoproyectado cp, conceptotipopersonal ctp, concepto c, ");
      sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp");
      sql.append(" where cp.id_trabajador = ?");
      sql.append(" and cp.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and ctp.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      if (criterio == 1) {
        sql.append(" and c.sueldo_basico = 'S'");
      } else if (criterio == 2) {
        sql.append(" and c.compensacion = 'S'");
      } else if (criterio == 3) {
        sql.append(" and c.compensacion = 'N'");
        sql.append(" and c.sueldo_basico = 'N'");
        sql.append(" and c.cod_concepto < '5000'");
      } else if (criterio == 4) {
        sql.append(" and c.cod_concepto < '5000'");
      }
      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stRegistros.setLong(1, idTrabajador);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next()) {
        if (rsRegistros.getInt("cod_frecuencia_pago") == 3)
          monto += rsRegistros.getDouble("monto") * 2.0D;
        else {
          monto += rsRegistros.getDouble("monto");
        }
      }
    }
    finally
    {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return monto;
  }

  public double findMontoByTrabajadorQuincenal(long idTrabajador, int criterio, String reflejaMovimiento)
    throws Exception
  {
    double monto = 0.0D;

    Connection connection = null;
    StringBuffer sql = new StringBuffer();
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select cf.monto,  fp.cod_frecuencia_pago");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, concepto c, ");
      sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp");
      sql.append(" where cf.id_trabajador = ?");
      sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and ctp.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and ctp.refleja_movimiento = 'S'");
      if (criterio == 1) {
        sql.append(" and c.sueldo_basico = 'S'");
      } else if (criterio == 2) {
        sql.append(" and c.compensacion = 'S'");
      } else if (criterio == 3) {
        sql.append(" and c.compensacion = 'N'");
        sql.append(" and c.sueldo_basico = 'N'");
        sql.append(" and c.cod_concepto < '5000'");
      } else if (criterio == 4) {
        sql.append(" and c.cod_concepto < '5000'");
      }
      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stRegistros.setLong(1, idTrabajador);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next()) {
        if (rsRegistros.getInt("cod_frecuencia_pago") == 3)
          monto += rsRegistros.getDouble("monto") * 2.0D;
        else
          monto += rsRegistros.getDouble("monto");
      }
    }
    finally
    {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return monto;
  }

  public double findMontoProyectadoByTrabajadorQuincenal(long idTrabajador, int criterio, String reflejaMovimiento)
    throws Exception
  {
    double monto = 0.0D;

    Connection connection = null;

    StringBuffer sql = new StringBuffer();

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select cp.monto,  fp.cod_frecuencia_pago");
      sql.append(" from conceptoproyectado cp, conceptotipopersonal ctp, concepto c, ");
      sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp");
      sql.append(" where cp.id_trabajador = ?");
      sql.append(" and cp.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and ctp.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and ctp.refleja_movimiento = 'S'");
      if (criterio == 1) {
        sql.append(" and c.sueldo_basico = 'S'");
      } else if (criterio == 2) {
        sql.append(" and c.compensacion = 'S'");
      } else if (criterio == 3) {
        sql.append(" and c.compensacion = 'N'");
        sql.append(" and c.sueldo_basico = 'N'");
        sql.append(" and c.cod_concepto < '5000'");
      } else if (criterio == 4) {
        sql.append(" and c.cod_concepto < '5000'");
      }
      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stRegistros.setLong(1, idTrabajador);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next()) {
        if (rsRegistros.getInt("cod_frecuencia_pago") == 3)
          monto += rsRegistros.getDouble("monto") * 2.0D;
        else {
          monto += rsRegistros.getDouble("monto");
        }
      }
    }
    finally
    {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return monto;
  }
}