package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class TrabajadorAuxPK
  implements Serializable
{
  public long idTrabajador;

  public TrabajadorAuxPK()
  {
  }

  public TrabajadorAuxPK(long idTrabajador)
  {
    this.idTrabajador = idTrabajador;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TrabajadorAuxPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TrabajadorAuxPK thatPK)
  {
    return 
      this.idTrabajador == thatPK.idTrabajador;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTrabajador)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTrabajador);
  }
}