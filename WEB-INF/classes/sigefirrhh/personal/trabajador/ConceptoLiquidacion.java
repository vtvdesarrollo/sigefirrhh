package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;

public class ConceptoLiquidacion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idConceptoLiquidacion;
  private double unidades;
  private double monto;
  private Date fechaRegistro;
  private String documentoSoporte;
  private String estatus;
  private int mesSobretiempo;
  private int anioSobretiempo;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anioSobretiempo", "conceptoTipoPersonal", "documentoSoporte", "estatus", "fechaRegistro", "frecuenciaTipoPersonal", "idConceptoLiquidacion", "mesSobretiempo", "monto", "trabajador", "unidades" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 26, 21, 21, 21, 26, 24, 21, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.ConceptoLiquidacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoLiquidacion());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("A", "ACTIVO");
    LISTA_ESTATUS.put("S", "SUSPENDIDO");
  }

  public ConceptoLiquidacion()
  {
    jdoSetestatus(this, "A");
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmonto(this));

    return jdoGetconceptoTipoPersonal(this).getConcepto().getCodConcepto() + " - " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + 
      a + " - " + 
      jdoGetfrecuenciaTipoPersonal(this).getFrecuenciaPago().getCodFrecuenciaPago() + " - " + jdoGetestatus(this);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal()
  {
    return jdoGetfrecuenciaTipoPersonal(this);
  }

  public long getIdConceptoLiquidacion()
  {
    return jdoGetidConceptoLiquidacion(this);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public double getUnidades()
  {
    return jdoGetunidades(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setFrecuenciaTipoPersonal(FrecuenciaTipoPersonal personal)
  {
    jdoSetfrecuenciaTipoPersonal(this, personal);
  }

  public void setIdConceptoLiquidacion(long l)
  {
    jdoSetidConceptoLiquidacion(this, l);
  }

  public void setMonto(double d)
  {
    jdoSetmonto(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setUnidades(double d)
  {
    jdoSetunidades(this, d);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public int getAnioSobretiempo() {
    return jdoGetanioSobretiempo(this);
  }
  public void setAnioSobretiempo(int anioSobretiempo) {
    jdoSetanioSobretiempo(this, anioSobretiempo);
  }
  public int getMesSobretiempo() {
    return jdoGetmesSobretiempo(this);
  }
  public void setMesSobretiempo(int mesSobretiempo) {
    jdoSetmesSobretiempo(this, mesSobretiempo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoLiquidacion localConceptoLiquidacion = new ConceptoLiquidacion();
    localConceptoLiquidacion.jdoFlags = 1;
    localConceptoLiquidacion.jdoStateManager = paramStateManager;
    return localConceptoLiquidacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoLiquidacion localConceptoLiquidacion = new ConceptoLiquidacion();
    localConceptoLiquidacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoLiquidacion.jdoFlags = 1;
    localConceptoLiquidacion.jdoStateManager = paramStateManager;
    return localConceptoLiquidacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioSobretiempo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaTipoPersonal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoLiquidacion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesSobretiempo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioSobretiempo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaTipoPersonal = ((FrecuenciaTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoLiquidacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesSobretiempo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoLiquidacion paramConceptoLiquidacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoLiquidacion == null)
        throw new IllegalArgumentException("arg1");
      this.anioSobretiempo = paramConceptoLiquidacion.anioSobretiempo;
      return;
    case 1:
      if (paramConceptoLiquidacion == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoLiquidacion.conceptoTipoPersonal;
      return;
    case 2:
      if (paramConceptoLiquidacion == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramConceptoLiquidacion.documentoSoporte;
      return;
    case 3:
      if (paramConceptoLiquidacion == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramConceptoLiquidacion.estatus;
      return;
    case 4:
      if (paramConceptoLiquidacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramConceptoLiquidacion.fechaRegistro;
      return;
    case 5:
      if (paramConceptoLiquidacion == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaTipoPersonal = paramConceptoLiquidacion.frecuenciaTipoPersonal;
      return;
    case 6:
      if (paramConceptoLiquidacion == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoLiquidacion = paramConceptoLiquidacion.idConceptoLiquidacion;
      return;
    case 7:
      if (paramConceptoLiquidacion == null)
        throw new IllegalArgumentException("arg1");
      this.mesSobretiempo = paramConceptoLiquidacion.mesSobretiempo;
      return;
    case 8:
      if (paramConceptoLiquidacion == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramConceptoLiquidacion.monto;
      return;
    case 9:
      if (paramConceptoLiquidacion == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramConceptoLiquidacion.trabajador;
      return;
    case 10:
      if (paramConceptoLiquidacion == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramConceptoLiquidacion.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoLiquidacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoLiquidacion localConceptoLiquidacion = (ConceptoLiquidacion)paramObject;
    if (localConceptoLiquidacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoLiquidacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoLiquidacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoLiquidacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoLiquidacionPK))
      throw new IllegalArgumentException("arg1");
    ConceptoLiquidacionPK localConceptoLiquidacionPK = (ConceptoLiquidacionPK)paramObject;
    localConceptoLiquidacionPK.idConceptoLiquidacion = this.idConceptoLiquidacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoLiquidacionPK))
      throw new IllegalArgumentException("arg1");
    ConceptoLiquidacionPK localConceptoLiquidacionPK = (ConceptoLiquidacionPK)paramObject;
    this.idConceptoLiquidacion = localConceptoLiquidacionPK.idConceptoLiquidacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoLiquidacionPK))
      throw new IllegalArgumentException("arg2");
    ConceptoLiquidacionPK localConceptoLiquidacionPK = (ConceptoLiquidacionPK)paramObject;
    localConceptoLiquidacionPK.idConceptoLiquidacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoLiquidacionPK))
      throw new IllegalArgumentException("arg2");
    ConceptoLiquidacionPK localConceptoLiquidacionPK = (ConceptoLiquidacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localConceptoLiquidacionPK.idConceptoLiquidacion);
  }

  private static final int jdoGetanioSobretiempo(ConceptoLiquidacion paramConceptoLiquidacion)
  {
    if (paramConceptoLiquidacion.jdoFlags <= 0)
      return paramConceptoLiquidacion.anioSobretiempo;
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoLiquidacion.anioSobretiempo;
    if (localStateManager.isLoaded(paramConceptoLiquidacion, jdoInheritedFieldCount + 0))
      return paramConceptoLiquidacion.anioSobretiempo;
    return localStateManager.getIntField(paramConceptoLiquidacion, jdoInheritedFieldCount + 0, paramConceptoLiquidacion.anioSobretiempo);
  }

  private static final void jdoSetanioSobretiempo(ConceptoLiquidacion paramConceptoLiquidacion, int paramInt)
  {
    if (paramConceptoLiquidacion.jdoFlags == 0)
    {
      paramConceptoLiquidacion.anioSobretiempo = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoLiquidacion.anioSobretiempo = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoLiquidacion, jdoInheritedFieldCount + 0, paramConceptoLiquidacion.anioSobretiempo, paramInt);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoLiquidacion paramConceptoLiquidacion)
  {
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoLiquidacion.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoLiquidacion, jdoInheritedFieldCount + 1))
      return paramConceptoLiquidacion.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoLiquidacion, jdoInheritedFieldCount + 1, paramConceptoLiquidacion.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoLiquidacion paramConceptoLiquidacion, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoLiquidacion.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoLiquidacion, jdoInheritedFieldCount + 1, paramConceptoLiquidacion.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGetdocumentoSoporte(ConceptoLiquidacion paramConceptoLiquidacion)
  {
    if (paramConceptoLiquidacion.jdoFlags <= 0)
      return paramConceptoLiquidacion.documentoSoporte;
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoLiquidacion.documentoSoporte;
    if (localStateManager.isLoaded(paramConceptoLiquidacion, jdoInheritedFieldCount + 2))
      return paramConceptoLiquidacion.documentoSoporte;
    return localStateManager.getStringField(paramConceptoLiquidacion, jdoInheritedFieldCount + 2, paramConceptoLiquidacion.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(ConceptoLiquidacion paramConceptoLiquidacion, String paramString)
  {
    if (paramConceptoLiquidacion.jdoFlags == 0)
    {
      paramConceptoLiquidacion.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoLiquidacion.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoLiquidacion, jdoInheritedFieldCount + 2, paramConceptoLiquidacion.documentoSoporte, paramString);
  }

  private static final String jdoGetestatus(ConceptoLiquidacion paramConceptoLiquidacion)
  {
    if (paramConceptoLiquidacion.jdoFlags <= 0)
      return paramConceptoLiquidacion.estatus;
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoLiquidacion.estatus;
    if (localStateManager.isLoaded(paramConceptoLiquidacion, jdoInheritedFieldCount + 3))
      return paramConceptoLiquidacion.estatus;
    return localStateManager.getStringField(paramConceptoLiquidacion, jdoInheritedFieldCount + 3, paramConceptoLiquidacion.estatus);
  }

  private static final void jdoSetestatus(ConceptoLiquidacion paramConceptoLiquidacion, String paramString)
  {
    if (paramConceptoLiquidacion.jdoFlags == 0)
    {
      paramConceptoLiquidacion.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoLiquidacion.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoLiquidacion, jdoInheritedFieldCount + 3, paramConceptoLiquidacion.estatus, paramString);
  }

  private static final Date jdoGetfechaRegistro(ConceptoLiquidacion paramConceptoLiquidacion)
  {
    if (paramConceptoLiquidacion.jdoFlags <= 0)
      return paramConceptoLiquidacion.fechaRegistro;
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoLiquidacion.fechaRegistro;
    if (localStateManager.isLoaded(paramConceptoLiquidacion, jdoInheritedFieldCount + 4))
      return paramConceptoLiquidacion.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramConceptoLiquidacion, jdoInheritedFieldCount + 4, paramConceptoLiquidacion.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(ConceptoLiquidacion paramConceptoLiquidacion, Date paramDate)
  {
    if (paramConceptoLiquidacion.jdoFlags == 0)
    {
      paramConceptoLiquidacion.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoLiquidacion.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoLiquidacion, jdoInheritedFieldCount + 4, paramConceptoLiquidacion.fechaRegistro, paramDate);
  }

  private static final FrecuenciaTipoPersonal jdoGetfrecuenciaTipoPersonal(ConceptoLiquidacion paramConceptoLiquidacion)
  {
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoLiquidacion.frecuenciaTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoLiquidacion, jdoInheritedFieldCount + 5))
      return paramConceptoLiquidacion.frecuenciaTipoPersonal;
    return (FrecuenciaTipoPersonal)localStateManager.getObjectField(paramConceptoLiquidacion, jdoInheritedFieldCount + 5, paramConceptoLiquidacion.frecuenciaTipoPersonal);
  }

  private static final void jdoSetfrecuenciaTipoPersonal(ConceptoLiquidacion paramConceptoLiquidacion, FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoLiquidacion.frecuenciaTipoPersonal = paramFrecuenciaTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoLiquidacion, jdoInheritedFieldCount + 5, paramConceptoLiquidacion.frecuenciaTipoPersonal, paramFrecuenciaTipoPersonal);
  }

  private static final long jdoGetidConceptoLiquidacion(ConceptoLiquidacion paramConceptoLiquidacion)
  {
    return paramConceptoLiquidacion.idConceptoLiquidacion;
  }

  private static final void jdoSetidConceptoLiquidacion(ConceptoLiquidacion paramConceptoLiquidacion, long paramLong)
  {
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoLiquidacion.idConceptoLiquidacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoLiquidacion, jdoInheritedFieldCount + 6, paramConceptoLiquidacion.idConceptoLiquidacion, paramLong);
  }

  private static final int jdoGetmesSobretiempo(ConceptoLiquidacion paramConceptoLiquidacion)
  {
    if (paramConceptoLiquidacion.jdoFlags <= 0)
      return paramConceptoLiquidacion.mesSobretiempo;
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoLiquidacion.mesSobretiempo;
    if (localStateManager.isLoaded(paramConceptoLiquidacion, jdoInheritedFieldCount + 7))
      return paramConceptoLiquidacion.mesSobretiempo;
    return localStateManager.getIntField(paramConceptoLiquidacion, jdoInheritedFieldCount + 7, paramConceptoLiquidacion.mesSobretiempo);
  }

  private static final void jdoSetmesSobretiempo(ConceptoLiquidacion paramConceptoLiquidacion, int paramInt)
  {
    if (paramConceptoLiquidacion.jdoFlags == 0)
    {
      paramConceptoLiquidacion.mesSobretiempo = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoLiquidacion.mesSobretiempo = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoLiquidacion, jdoInheritedFieldCount + 7, paramConceptoLiquidacion.mesSobretiempo, paramInt);
  }

  private static final double jdoGetmonto(ConceptoLiquidacion paramConceptoLiquidacion)
  {
    if (paramConceptoLiquidacion.jdoFlags <= 0)
      return paramConceptoLiquidacion.monto;
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoLiquidacion.monto;
    if (localStateManager.isLoaded(paramConceptoLiquidacion, jdoInheritedFieldCount + 8))
      return paramConceptoLiquidacion.monto;
    return localStateManager.getDoubleField(paramConceptoLiquidacion, jdoInheritedFieldCount + 8, paramConceptoLiquidacion.monto);
  }

  private static final void jdoSetmonto(ConceptoLiquidacion paramConceptoLiquidacion, double paramDouble)
  {
    if (paramConceptoLiquidacion.jdoFlags == 0)
    {
      paramConceptoLiquidacion.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoLiquidacion.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoLiquidacion, jdoInheritedFieldCount + 8, paramConceptoLiquidacion.monto, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(ConceptoLiquidacion paramConceptoLiquidacion)
  {
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoLiquidacion.trabajador;
    if (localStateManager.isLoaded(paramConceptoLiquidacion, jdoInheritedFieldCount + 9))
      return paramConceptoLiquidacion.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramConceptoLiquidacion, jdoInheritedFieldCount + 9, paramConceptoLiquidacion.trabajador);
  }

  private static final void jdoSettrabajador(ConceptoLiquidacion paramConceptoLiquidacion, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoLiquidacion.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramConceptoLiquidacion, jdoInheritedFieldCount + 9, paramConceptoLiquidacion.trabajador, paramTrabajador);
  }

  private static final double jdoGetunidades(ConceptoLiquidacion paramConceptoLiquidacion)
  {
    if (paramConceptoLiquidacion.jdoFlags <= 0)
      return paramConceptoLiquidacion.unidades;
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoLiquidacion.unidades;
    if (localStateManager.isLoaded(paramConceptoLiquidacion, jdoInheritedFieldCount + 10))
      return paramConceptoLiquidacion.unidades;
    return localStateManager.getDoubleField(paramConceptoLiquidacion, jdoInheritedFieldCount + 10, paramConceptoLiquidacion.unidades);
  }

  private static final void jdoSetunidades(ConceptoLiquidacion paramConceptoLiquidacion, double paramDouble)
  {
    if (paramConceptoLiquidacion.jdoFlags == 0)
    {
      paramConceptoLiquidacion.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoLiquidacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoLiquidacion.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoLiquidacion, jdoInheritedFieldCount + 10, paramConceptoLiquidacion.unidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}