package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class AnticipoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAnticipo(Anticipo anticipo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Anticipo anticipoNew = 
      (Anticipo)BeanUtils.cloneBean(
      anticipo);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (anticipoNew.getTipoPersonal() != null) {
      anticipoNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        anticipoNew.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (anticipoNew.getPersonal() != null) {
      anticipoNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        anticipoNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(anticipoNew);
  }

  public void updateAnticipo(Anticipo anticipo) throws Exception
  {
    Anticipo anticipoModify = 
      findAnticipoById(anticipo.getIdAnticipo());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (anticipo.getTipoPersonal() != null) {
      anticipo.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        anticipo.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (anticipo.getPersonal() != null) {
      anticipo.setPersonal(
        personalBeanBusiness.findPersonalById(
        anticipo.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(anticipoModify, anticipo);
  }

  public void deleteAnticipo(Anticipo anticipo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Anticipo anticipoDelete = 
      findAnticipoById(anticipo.getIdAnticipo());
    pm.deletePersistent(anticipoDelete);
  }

  public Anticipo findAnticipoById(long idAnticipo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAnticipo == pIdAnticipo";
    Query query = pm.newQuery(Anticipo.class, filter);

    query.declareParameters("long pIdAnticipo");

    parameters.put("pIdAnticipo", new Long(idAnticipo));

    Collection colAnticipo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAnticipo.iterator();
    return (Anticipo)iterator.next();
  }

  public Collection findAnticipoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent anticipoExtent = pm.getExtent(
      Anticipo.class, true);
    Query query = pm.newQuery(anticipoExtent);
    query.setOrdering("fechaAnticipo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Anticipo.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaAnticipo ascending");

    Collection colAnticipo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAnticipo);

    return colAnticipo;
  }
}