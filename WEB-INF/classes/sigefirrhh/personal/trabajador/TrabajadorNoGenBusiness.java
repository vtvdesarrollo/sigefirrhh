package sigefirrhh.personal.trabajador;

import java.util.Collection;
import java.util.Date;
import sigefirrhh.personal.conceptos.CalcularConceptoBeanBusiness;

public class TrabajadorNoGenBusiness extends TrabajadorBusiness
{
  private TrabajadorNoGenBeanBusiness trabajadorNoGenBeanBusiness = new TrabajadorNoGenBeanBusiness();

  private ConceptoFijoNoGenBeanBusiness conceptoFijoNoGenBeanBusiness = new ConceptoFijoNoGenBeanBusiness();

  private ConceptoVariableNoGenBeanBusiness conceptoVariableNoGenBeanBusiness = new ConceptoVariableNoGenBeanBusiness();

  private ConceptoLiquidacionNoGenBeanBusiness conceptoLiquidacionNoGenBeanBusiness = new ConceptoLiquidacionNoGenBeanBusiness();

  private CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

  private PlanillaAriNoGenBeanBusiness planillaAriNoGenBeanBusiness = new PlanillaAriNoGenBeanBusiness();

  private GenerarPlanillaArcBeanBusiness generarPlanillaArcBeanBusiness = new GenerarPlanillaArcBeanBusiness();

  public Collection findTrabajadorByPassword(int cedula, String password)
    throws Exception
  {
    return this.trabajadorNoGenBeanBusiness.findByPassword(cedula, password);
  }

  public Collection findTrabajadorForRegistro(int cedula, int anioInicio, Date fechaNacimiento) throws Exception {
    return this.trabajadorNoGenBeanBusiness.findForRegistro(cedula, anioInicio, fechaNacimiento);
  }

  public Collection findTrabajadorForPrimaAntiguedad(Date fechaInicio, Date fechaFin, long idTipoPersonal) throws Exception {
    return this.trabajadorNoGenBeanBusiness.findForPrimaAntiguedad(fechaInicio, fechaFin, idTipoPersonal);
  }

  public Collection findConceptoFijoByTrabajadorAndConceptoTipoPersonal(long idTrabajador, long idConceptoTipoPersonal) throws Exception {
    return this.conceptoFijoNoGenBeanBusiness.findByTrabajadorAndConceptoTipoPersonal(idTrabajador, idConceptoTipoPersonal);
  }

  public Collection findConceptoVariableByTrabajadorAndConceptoTipoPersonal(long idTrabajador, long idConceptoTipoPersonal) throws Exception {
    return this.conceptoVariableNoGenBeanBusiness.findByTrabajadorAndConceptoTipoPersonal(idTrabajador, idConceptoTipoPersonal);
  }

  public Collection findConceptoLiquidacionByTrabajadorAndConceptoTipoPersonal(long idTrabajador, long idConceptoTipoPersonal) throws Exception {
    return this.conceptoLiquidacionNoGenBeanBusiness.findByTrabajadorAndConceptoTipoPersonal(idTrabajador, idConceptoTipoPersonal);
  }

  public Collection findTrabajadorForBonoVacacional(Date fechaInicio, Date fechaFin, long idTipoPersonal) throws Exception
  {
    return this.trabajadorNoGenBeanBusiness.findForBonoVacacional(fechaInicio, fechaFin, idTipoPersonal);
  }

  public Collection findConceptoFijoForRegistroCargos(long idTrabajador, int codFrecuenciaPago, String metodo) throws Exception {
    return this.conceptoFijoNoGenBeanBusiness.findForRegistroCargos(idTrabajador, codFrecuenciaPago, metodo);
  }

  public Collection findConceptoFijoFromConceptoAsociado(long idTrabajador, long idConceptoTipoPersonal) throws Exception {
    return this.conceptoFijoNoGenBeanBusiness.findFromConceptoAsociado(idTrabajador, idConceptoTipoPersonal);
  }

  public double calcularConcepto(long idConceptoTipoPersonal, long idTrabajador, double unidades, String tipo, int codFrecuenciaPago, double jornadaDiaria, double jornadaSemanal, String formulaIntegral, String formulaSemanal, long idCargo, double valor, double topeMinimo, double topeMaximo) throws Exception {
    return this.calcularConceptoBeanBusiness.calcular(idConceptoTipoPersonal, idTrabajador, unidades, tipo, codFrecuenciaPago, jornadaDiaria, jornadaSemanal, formulaIntegral, formulaSemanal, idCargo, valor, topeMinimo, topeMaximo);
  }

  public Collection findTrabajadorByCedulaAndTipoPersonal(int cedula, long idOrganismo, long idTipoPersonal) throws Exception
  {
    return this.trabajadorNoGenBeanBusiness.findByCedulaAndTipoPersonal(
      cedula, idOrganismo, idTipoPersonal);
  }

  public Collection findConceptoFijoByTrabajadorConceptoTipoPersonalFrecuenciaTipoPersonal(long idTrabajador, long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal) throws Exception {
    return this.conceptoFijoNoGenBeanBusiness.findByTrabajadorConceptoTipoPersonalFrecuenciaTipoPersonal(idTrabajador, idConceptoTipoPersonal, idFrecuenciaTipoPersonal);
  }

  public int verificarSiTrabajadorPuedeIngresar(long idPersonal, long idOrganismo, long idTipoPersonal) throws Exception {
    return this.trabajadorNoGenBeanBusiness.verificarSiTrabajadorPuedeIngresar(idPersonal, idOrganismo, idTipoPersonal);
  }

  public int validarFechaIngreso(long idPersonal, long idOrganismo, Date fechaIngreso) throws Exception
  {
    return this.trabajadorNoGenBeanBusiness.validarFechaIngreso(idPersonal, idOrganismo, fechaIngreso);
  }

  public int verificarSiTrabajadorPuedeReingresar(long idPersonal, long idOrganismo, long idTipoPersonal)
    throws Exception
  {
    return this.trabajadorNoGenBeanBusiness.verificarSiTrabajadorPuedeReingresar(idPersonal, idOrganismo, idTipoPersonal);
  }

  public int buscarUltimoCodigoNomina(long idTipoPersonal) throws Exception {
    return this.trabajadorNoGenBeanBusiness.buscarUltimoCodigoNomina(idTipoPersonal);
  }

  public Collection findConceptoFijoByTrabajadorNoPersistente(long idTrabajador) throws Exception {
    return this.conceptoFijoNoGenBeanBusiness.findByTrabajadorNoPersistente(idTrabajador);
  }
  public boolean existeCuenta(int cedula, String cuentaNomina, String tipoCuenta) throws Exception {
    return this.trabajadorNoGenBeanBusiness.existeCuenta(cedula, cuentaNomina, tipoCuenta);
  }
  public void generarPlanillaArc(long idTipoPersonal, String periodicidad, int anio) throws Exception {
    this.generarPlanillaArcBeanBusiness.generar(idTipoPersonal, periodicidad, anio);
  }

  public Collection findTrabajadorByPersonalAndEstatus(long idPersonal, String estatus, long idOrganismo) throws Exception
  {
    return this.trabajadorNoGenBeanBusiness.findByPersonalAndEstatus(
      idPersonal, estatus, idOrganismo);
  }

  public Collection findConceptoFijoByTrabajadorAndCodConcepto(long idTrabajador, String codConcepto) throws Exception {
    return this.conceptoFijoNoGenBeanBusiness.findByTrabajadorAndCodConcepto(idTrabajador, codConcepto);
  }

  public double findMontoConceptoFijoByTrabajadorQuincenal(long idTrabajador, int criterio) throws Exception {
    return this.conceptoFijoNoGenBeanBusiness.findMontoByTrabajadorQuincenal(idTrabajador, criterio);
  }

  public double findMontoConceptoProyectadoByTrabajadorQuincenal(long idTrabajador, int criterio) throws Exception {
    return this.conceptoFijoNoGenBeanBusiness.findMontoProyectadoByTrabajadorQuincenal(idTrabajador, criterio);
  }

  public double findMontoConceptoFijoByTrabajadorQuincenal(long idTrabajador, int criterio, String reflejaMovimiento) throws Exception {
    return this.conceptoFijoNoGenBeanBusiness.findMontoByTrabajadorQuincenal(idTrabajador, criterio, reflejaMovimiento);
  }

  public double findMontoConceptoProyectadoByTrabajadorQuincenal(long idTrabajador, int criterio, String reflejaMovimiento) throws Exception {
    return this.conceptoFijoNoGenBeanBusiness.findMontoProyectadoByTrabajadorQuincenal(idTrabajador, criterio, reflejaMovimiento);
  }
}