package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;

public class ConceptoFijo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idConceptoFijo;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private double unidades;
  private double monto;
  private double unidadesRestituir;
  private double montoRestituir;
  private Date fechaRegistro;
  private Date fechaEliminar;
  private Date fechaComienzo;
  private String documentoSoporte;
  private String estatus;
  private String restituir;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private Trabajador trabajador;
  private double montoAnterior;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "documentoSoporte", "estatus", "fechaComienzo", "fechaEliminar", "fechaRegistro", "frecuenciaTipoPersonal", "idConceptoFijo", "idSitp", "monto", "montoAnterior", "montoRestituir", "restituir", "tiempoSitp", "trabajador", "unidades", "unidadesRestituir" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), Double.TYPE, Double.TYPE }; private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 21, 21, 26, 24, 21, 21, 21, 21, 21, 21, 26, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.ConceptoFijo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoFijo());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("A", "ACTIVO");
    LISTA_ESTATUS.put("S", "SUSPENDIDO");
    LISTA_ESTATUS.put("Y", "PROYECTADO");
  }

  public ConceptoFijo()
  {
    jdoSetunidadesRestituir(this, 0.0D);

    jdoSetmontoRestituir(this, 0.0D);

    jdoSetestatus(this, "A");

    jdoSetrestituir(this, "N");
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmonto(this));

    return jdoGetconceptoTipoPersonal(this).getConcepto().getCodConcepto() + " - " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + 
      a + " - " + 
      jdoGetfrecuenciaTipoPersonal(this).getFrecuenciaPago().getCodFrecuenciaPago() + " - " + jdoGetestatus(this);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public Date getFechaComienzo()
  {
    return jdoGetfechaComienzo(this);
  }

  public Date getFechaEliminar()
  {
    return jdoGetfechaEliminar(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal()
  {
    return jdoGetfrecuenciaTipoPersonal(this);
  }

  public long getIdConceptoFijo()
  {
    return jdoGetidConceptoFijo(this);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public double getUnidades()
  {
    return jdoGetunidades(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setFechaComienzo(Date date)
  {
    jdoSetfechaComienzo(this, date);
  }

  public void setFechaEliminar(Date date)
  {
    jdoSetfechaEliminar(this, date);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setFrecuenciaTipoPersonal(FrecuenciaTipoPersonal personal)
  {
    jdoSetfrecuenciaTipoPersonal(this, personal);
  }

  public void setIdConceptoFijo(long l)
  {
    jdoSetidConceptoFijo(this, l);
  }

  public void setMonto(double d)
  {
    jdoSetmonto(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setUnidades(double d)
  {
    jdoSetunidades(this, d);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public String getRestituir()
  {
    return jdoGetrestituir(this);
  }

  public void setRestituir(String string)
  {
    jdoSetrestituir(this, string);
  }

  public double getMontoRestituir()
  {
    return jdoGetmontoRestituir(this);
  }

  public double getUnidadesRestituir()
  {
    return jdoGetunidadesRestituir(this);
  }

  public void setMontoRestituir(double d)
  {
    jdoSetmontoRestituir(this, d);
  }

  public void setUnidadesRestituir(double d)
  {
    jdoSetunidadesRestituir(this, d);
  }

  public double getMontoAnterior() {
    return jdoGetmontoAnterior(this);
  }
  public void setMontoAnterior(double montoAnterior) {
    jdoSetmontoAnterior(this, montoAnterior);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 17;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoFijo localConceptoFijo = new ConceptoFijo();
    localConceptoFijo.jdoFlags = 1;
    localConceptoFijo.jdoStateManager = paramStateManager;
    return localConceptoFijo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoFijo localConceptoFijo = new ConceptoFijo();
    localConceptoFijo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoFijo.jdoFlags = 1;
    localConceptoFijo.jdoStateManager = paramStateManager;
    return localConceptoFijo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaComienzo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEliminar);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaTipoPersonal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoFijo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAnterior);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoRestituir);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.restituir);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidadesRestituir);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaComienzo = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEliminar = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaTipoPersonal = ((FrecuenciaTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoFijo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAnterior = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoRestituir = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.restituir = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadesRestituir = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoFijo paramConceptoFijo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoFijo.conceptoTipoPersonal;
      return;
    case 1:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramConceptoFijo.documentoSoporte;
      return;
    case 2:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramConceptoFijo.estatus;
      return;
    case 3:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaComienzo = paramConceptoFijo.fechaComienzo;
      return;
    case 4:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEliminar = paramConceptoFijo.fechaEliminar;
      return;
    case 5:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramConceptoFijo.fechaRegistro;
      return;
    case 6:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaTipoPersonal = paramConceptoFijo.frecuenciaTipoPersonal;
      return;
    case 7:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoFijo = paramConceptoFijo.idConceptoFijo;
      return;
    case 8:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramConceptoFijo.idSitp;
      return;
    case 9:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramConceptoFijo.monto;
      return;
    case 10:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.montoAnterior = paramConceptoFijo.montoAnterior;
      return;
    case 11:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.montoRestituir = paramConceptoFijo.montoRestituir;
      return;
    case 12:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.restituir = paramConceptoFijo.restituir;
      return;
    case 13:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramConceptoFijo.tiempoSitp;
      return;
    case 14:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramConceptoFijo.trabajador;
      return;
    case 15:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramConceptoFijo.unidades;
      return;
    case 16:
      if (paramConceptoFijo == null)
        throw new IllegalArgumentException("arg1");
      this.unidadesRestituir = paramConceptoFijo.unidadesRestituir;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoFijo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoFijo localConceptoFijo = (ConceptoFijo)paramObject;
    if (localConceptoFijo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoFijo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoFijoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoFijoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoFijoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoFijoPK localConceptoFijoPK = (ConceptoFijoPK)paramObject;
    localConceptoFijoPK.idConceptoFijo = this.idConceptoFijo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoFijoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoFijoPK localConceptoFijoPK = (ConceptoFijoPK)paramObject;
    this.idConceptoFijo = localConceptoFijoPK.idConceptoFijo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoFijoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoFijoPK localConceptoFijoPK = (ConceptoFijoPK)paramObject;
    localConceptoFijoPK.idConceptoFijo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoFijoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoFijoPK localConceptoFijoPK = (ConceptoFijoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localConceptoFijoPK.idConceptoFijo);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoFijo paramConceptoFijo)
  {
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 0))
      return paramConceptoFijo.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoFijo, jdoInheritedFieldCount + 0, paramConceptoFijo.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoFijo paramConceptoFijo, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoFijo, jdoInheritedFieldCount + 0, paramConceptoFijo.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGetdocumentoSoporte(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.documentoSoporte;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.documentoSoporte;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 1))
      return paramConceptoFijo.documentoSoporte;
    return localStateManager.getStringField(paramConceptoFijo, jdoInheritedFieldCount + 1, paramConceptoFijo.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(ConceptoFijo paramConceptoFijo, String paramString)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoFijo, jdoInheritedFieldCount + 1, paramConceptoFijo.documentoSoporte, paramString);
  }

  private static final String jdoGetestatus(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.estatus;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.estatus;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 2))
      return paramConceptoFijo.estatus;
    return localStateManager.getStringField(paramConceptoFijo, jdoInheritedFieldCount + 2, paramConceptoFijo.estatus);
  }

  private static final void jdoSetestatus(ConceptoFijo paramConceptoFijo, String paramString)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoFijo, jdoInheritedFieldCount + 2, paramConceptoFijo.estatus, paramString);
  }

  private static final Date jdoGetfechaComienzo(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.fechaComienzo;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.fechaComienzo;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 3))
      return paramConceptoFijo.fechaComienzo;
    return (Date)localStateManager.getObjectField(paramConceptoFijo, jdoInheritedFieldCount + 3, paramConceptoFijo.fechaComienzo);
  }

  private static final void jdoSetfechaComienzo(ConceptoFijo paramConceptoFijo, Date paramDate)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.fechaComienzo = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.fechaComienzo = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoFijo, jdoInheritedFieldCount + 3, paramConceptoFijo.fechaComienzo, paramDate);
  }

  private static final Date jdoGetfechaEliminar(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.fechaEliminar;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.fechaEliminar;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 4))
      return paramConceptoFijo.fechaEliminar;
    return (Date)localStateManager.getObjectField(paramConceptoFijo, jdoInheritedFieldCount + 4, paramConceptoFijo.fechaEliminar);
  }

  private static final void jdoSetfechaEliminar(ConceptoFijo paramConceptoFijo, Date paramDate)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.fechaEliminar = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.fechaEliminar = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoFijo, jdoInheritedFieldCount + 4, paramConceptoFijo.fechaEliminar, paramDate);
  }

  private static final Date jdoGetfechaRegistro(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.fechaRegistro;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.fechaRegistro;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 5))
      return paramConceptoFijo.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramConceptoFijo, jdoInheritedFieldCount + 5, paramConceptoFijo.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(ConceptoFijo paramConceptoFijo, Date paramDate)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoFijo, jdoInheritedFieldCount + 5, paramConceptoFijo.fechaRegistro, paramDate);
  }

  private static final FrecuenciaTipoPersonal jdoGetfrecuenciaTipoPersonal(ConceptoFijo paramConceptoFijo)
  {
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.frecuenciaTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 6))
      return paramConceptoFijo.frecuenciaTipoPersonal;
    return (FrecuenciaTipoPersonal)localStateManager.getObjectField(paramConceptoFijo, jdoInheritedFieldCount + 6, paramConceptoFijo.frecuenciaTipoPersonal);
  }

  private static final void jdoSetfrecuenciaTipoPersonal(ConceptoFijo paramConceptoFijo, FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.frecuenciaTipoPersonal = paramFrecuenciaTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoFijo, jdoInheritedFieldCount + 6, paramConceptoFijo.frecuenciaTipoPersonal, paramFrecuenciaTipoPersonal);
  }

  private static final long jdoGetidConceptoFijo(ConceptoFijo paramConceptoFijo)
  {
    return paramConceptoFijo.idConceptoFijo;
  }

  private static final void jdoSetidConceptoFijo(ConceptoFijo paramConceptoFijo, long paramLong)
  {
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.idConceptoFijo = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoFijo, jdoInheritedFieldCount + 7, paramConceptoFijo.idConceptoFijo, paramLong);
  }

  private static final int jdoGetidSitp(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.idSitp;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.idSitp;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 8))
      return paramConceptoFijo.idSitp;
    return localStateManager.getIntField(paramConceptoFijo, jdoInheritedFieldCount + 8, paramConceptoFijo.idSitp);
  }

  private static final void jdoSetidSitp(ConceptoFijo paramConceptoFijo, int paramInt)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoFijo, jdoInheritedFieldCount + 8, paramConceptoFijo.idSitp, paramInt);
  }

  private static final double jdoGetmonto(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.monto;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.monto;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 9))
      return paramConceptoFijo.monto;
    return localStateManager.getDoubleField(paramConceptoFijo, jdoInheritedFieldCount + 9, paramConceptoFijo.monto);
  }

  private static final void jdoSetmonto(ConceptoFijo paramConceptoFijo, double paramDouble)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoFijo, jdoInheritedFieldCount + 9, paramConceptoFijo.monto, paramDouble);
  }

  private static final double jdoGetmontoAnterior(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.montoAnterior;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.montoAnterior;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 10))
      return paramConceptoFijo.montoAnterior;
    return localStateManager.getDoubleField(paramConceptoFijo, jdoInheritedFieldCount + 10, paramConceptoFijo.montoAnterior);
  }

  private static final void jdoSetmontoAnterior(ConceptoFijo paramConceptoFijo, double paramDouble)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.montoAnterior = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.montoAnterior = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoFijo, jdoInheritedFieldCount + 10, paramConceptoFijo.montoAnterior, paramDouble);
  }

  private static final double jdoGetmontoRestituir(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.montoRestituir;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.montoRestituir;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 11))
      return paramConceptoFijo.montoRestituir;
    return localStateManager.getDoubleField(paramConceptoFijo, jdoInheritedFieldCount + 11, paramConceptoFijo.montoRestituir);
  }

  private static final void jdoSetmontoRestituir(ConceptoFijo paramConceptoFijo, double paramDouble)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.montoRestituir = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.montoRestituir = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoFijo, jdoInheritedFieldCount + 11, paramConceptoFijo.montoRestituir, paramDouble);
  }

  private static final String jdoGetrestituir(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.restituir;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.restituir;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 12))
      return paramConceptoFijo.restituir;
    return localStateManager.getStringField(paramConceptoFijo, jdoInheritedFieldCount + 12, paramConceptoFijo.restituir);
  }

  private static final void jdoSetrestituir(ConceptoFijo paramConceptoFijo, String paramString)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.restituir = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.restituir = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoFijo, jdoInheritedFieldCount + 12, paramConceptoFijo.restituir, paramString);
  }

  private static final Date jdoGettiempoSitp(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.tiempoSitp;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.tiempoSitp;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 13))
      return paramConceptoFijo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramConceptoFijo, jdoInheritedFieldCount + 13, paramConceptoFijo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ConceptoFijo paramConceptoFijo, Date paramDate)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoFijo, jdoInheritedFieldCount + 13, paramConceptoFijo.tiempoSitp, paramDate);
  }

  private static final Trabajador jdoGettrabajador(ConceptoFijo paramConceptoFijo)
  {
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.trabajador;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 14))
      return paramConceptoFijo.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramConceptoFijo, jdoInheritedFieldCount + 14, paramConceptoFijo.trabajador);
  }

  private static final void jdoSettrabajador(ConceptoFijo paramConceptoFijo, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramConceptoFijo, jdoInheritedFieldCount + 14, paramConceptoFijo.trabajador, paramTrabajador);
  }

  private static final double jdoGetunidades(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.unidades;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.unidades;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 15))
      return paramConceptoFijo.unidades;
    return localStateManager.getDoubleField(paramConceptoFijo, jdoInheritedFieldCount + 15, paramConceptoFijo.unidades);
  }

  private static final void jdoSetunidades(ConceptoFijo paramConceptoFijo, double paramDouble)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoFijo, jdoInheritedFieldCount + 15, paramConceptoFijo.unidades, paramDouble);
  }

  private static final double jdoGetunidadesRestituir(ConceptoFijo paramConceptoFijo)
  {
    if (paramConceptoFijo.jdoFlags <= 0)
      return paramConceptoFijo.unidadesRestituir;
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoFijo.unidadesRestituir;
    if (localStateManager.isLoaded(paramConceptoFijo, jdoInheritedFieldCount + 16))
      return paramConceptoFijo.unidadesRestituir;
    return localStateManager.getDoubleField(paramConceptoFijo, jdoInheritedFieldCount + 16, paramConceptoFijo.unidadesRestituir);
  }

  private static final void jdoSetunidadesRestituir(ConceptoFijo paramConceptoFijo, double paramDouble)
  {
    if (paramConceptoFijo.jdoFlags == 0)
    {
      paramConceptoFijo.unidadesRestituir = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoFijo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoFijo.unidadesRestituir = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoFijo, jdoInheritedFieldCount + 16, paramConceptoFijo.unidadesRestituir, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}