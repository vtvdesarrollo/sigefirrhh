package sigefirrhh.personal.trabajador;

import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ConceptoFijoBeanBusinessExtend extends ConceptoFijoBeanBusiness
{
  public static final byte FRECUENCIA_IGUAL_3 = 1;
  public static final byte FRECUENCIA_DIFERENTE_3 = 2;
  public static final byte FRECUENCIA_IGUAL_4 = 3;
  public static final byte FRECUENCIA_DIFERENTE_4 = 4;

  public Collection findFromConceptoAsociado(long idTrabajador, long idConceptoTipoPersonal, byte frecuenciaMensual)
    throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = null;
    long idFrecuenciaPago = 0L;
    String estatus = "A";

    switch (frecuenciaMensual) {
    case 1:
      idFrecuenciaPago = 3L;
      filter = "trabajador.idTrabajador == pIdTrabajador && estatus == pEstatus && conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal && frecuenciaTipoPersonal.frecuenciaPago.idFrecuenciaPago == pIdFrecuenciaPago";
      break;
    case 2:
      idFrecuenciaPago = 3L;
      filter = "trabajador.idTrabajador == pIdTrabajador && estatus == pEstatus && conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal && frecuenciaTipoPersonal.frecuenciaPago.idFrecuenciaPago != pIdFrecuenciaPago";
      break;
    case 3:
      idFrecuenciaPago = 4L;
      filter = "trabajador.idTrabajador == pIdTrabajador && estatus == pEstatus && conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal && frecuenciaTipoPersonal.frecuenciaPago.idFrecuenciaPago == pIdFrecuenciaPago";
      break;
    case 4:
      idFrecuenciaPago = 4L;
      filter = "trabajador.idTrabajador == pIdTrabajador && estatus == pEstatus && conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal && frecuenciaTipoPersonal.frecuenciaPago.idFrecuenciaPago != pIdFrecuenciaPago";
    }
    Query query = pm.newQuery(ConceptoFijo.class, filter);

    query.declareParameters("long pIdTrabajador, long pIdConceptoTipoPersonal, long pIdFrecuenciaPago, String pEstatus");

    parameters.put("pEstatus", estatus);
    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));
    parameters.put("pIdFrecuenciaPago", new Long(idFrecuenciaPago));

    Collection colConceptoFijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoFijo;
  }

  public ConceptoFijo findByTrabajador(long idFrecuenciaTipoPersonal, long idConceptoTipoPersonal, long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "frecuenciaTipoPersonal.idFrecuenciaTipoPersonal == pIdFrecuenciaTipoPersonal && conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal && trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(ConceptoFijo.class, filter);

    query.declareParameters("long pIdFrecuenciaTipoPersonal, long pIdConceptoTipoPersonal, long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdFrecuenciaTipoPersonal", new Long(idFrecuenciaTipoPersonal));
    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));
    parameters.put("pIdTrabajador", new Long(idTrabajador));

    Collection colConceptoFijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    ConceptoFijo conceptoFijo = (ConceptoFijo)colConceptoFijo.iterator().next();

    return conceptoFijo;
  }
}