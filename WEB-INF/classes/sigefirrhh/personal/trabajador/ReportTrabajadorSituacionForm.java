package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportTrabajadorSituacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportTrabajadorSituacionForm.class.getName());
  private int reportId;
  private long idTipoPersonal;
  private String reportName;
  private String situacion = "1";
  private String orden = "A";
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;

  public ReportTrabajadorSituacionForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "trasbencuaalf";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportTrabajadorSituacionForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try {
      if (this.situacion != null) {
        if (this.situacion.equals("1"))
          this.reportName = "traencua";
        else if (this.situacion.equals("2"))
          this.reportName = "tracomua";
        else if (this.situacion.equals("3"))
          this.reportName = "traliccsua";
        else if (this.situacion.equals("4"))
          this.reportName = "tralicssua";
        else if (this.situacion.equals("5")) {
          this.reportName = "trasusssua";
        }
        if (this.orden.equals("A"))
          this.reportName += "alf";
        else if (this.orden.equals("C"))
          this.reportName += "ced";
        else
          this.reportName += "cod";
      }
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport() {
    this.reportName = "";
    Map parameters = new Hashtable();
    try
    {
      if (this.situacion != null) {
        if (this.situacion.equals("1"))
          this.reportName = "traencua";
        else if (this.situacion.equals("2"))
          this.reportName = "tracomua";
        else if (this.situacion.equals("3"))
          this.reportName = "traliccsua";
        else if (this.situacion.equals("4"))
          this.reportName = "tralicssua";
        else if (this.situacion.equals("5")) {
          this.reportName = "trasusssua";
        }
        if (this.orden.equals("A"))
          this.reportName += "alf";
        else if (this.orden.equals("C"))
          this.reportName += "ced";
        else {
          this.reportName += "cod";
        }
      }
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));

      JasperForWeb report = new JasperForWeb();

      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/trabajador");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String getIdTipoPersonal() {
    return String.valueOf(this.idTipoPersonal);
  }
  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }
  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }
  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String string) {
    this.orden = string;
  }

  public String getSituacion()
  {
    return this.situacion;
  }

  public void setSituacion(String situacion)
  {
    this.situacion = situacion;
  }
}