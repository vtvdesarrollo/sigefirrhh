package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;

public class Prestamo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idPrestamo;
  private double montoPrestamo;
  private int numeroCuotas;
  private int cuotasPagadas;
  private double montoCuota;
  private double montoPagado;
  private Date fechaOtorgo;
  private Date fechaComienzoPago;
  private Date fechaRegistro;
  private String documentoSoporte;
  private String estatus;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "cuotasPagadas", "documentoSoporte", "estatus", "fechaComienzoPago", "fechaOtorgo", "fechaRegistro", "frecuenciaTipoPersonal", "idPrestamo", "montoCuota", "montoPagado", "montoPrestamo", "numeroCuotas", "trabajador" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), Long.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") }; private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 21, 21, 21, 26, 24, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.Prestamo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Prestamo());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("A", "ACTIVO");
    LISTA_ESTATUS.put("S", "SUSPENDIDO");
    LISTA_ESTATUS.put("P", "PAGADO");
  }

  public Prestamo()
  {
    jdoSetestatus(this, "A");
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmontoPrestamo(this));
    return jdoGetconceptoTipoPersonal(this).getConcepto().getCodConcepto() + " - " + getConceptoTipoPersonal().getConcepto().getDescripcion() + " - " + a + " - " + jdoGetestatus(this);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public int getCuotasPagadas()
  {
    return jdoGetcuotasPagadas(this);
  }

  public Date getFechaComienzoPago()
  {
    return jdoGetfechaComienzoPago(this);
  }

  public Date getFechaOtorgo()
  {
    return jdoGetfechaOtorgo(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal()
  {
    return jdoGetfrecuenciaTipoPersonal(this);
  }

  public long getIdPrestamo()
  {
    return jdoGetidPrestamo(this);
  }

  public double getMontoCuota()
  {
    return jdoGetmontoCuota(this);
  }

  public double getMontoPagado()
  {
    return jdoGetmontoPagado(this);
  }

  public double getMontoPrestamo()
  {
    return jdoGetmontoPrestamo(this);
  }

  public int getNumeroCuotas()
  {
    return jdoGetnumeroCuotas(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setCuotasPagadas(int i)
  {
    jdoSetcuotasPagadas(this, i);
  }

  public void setFechaComienzoPago(Date date)
  {
    jdoSetfechaComienzoPago(this, date);
  }

  public void setFechaOtorgo(Date date)
  {
    jdoSetfechaOtorgo(this, date);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setFrecuenciaTipoPersonal(FrecuenciaTipoPersonal personal)
  {
    jdoSetfrecuenciaTipoPersonal(this, personal);
  }

  public void setIdPrestamo(long l)
  {
    jdoSetidPrestamo(this, l);
  }

  public void setMontoCuota(double d)
  {
    jdoSetmontoCuota(this, d);
  }

  public void setMontoPagado(double d)
  {
    jdoSetmontoPagado(this, d);
  }

  public void setMontoPrestamo(double d)
  {
    jdoSetmontoPrestamo(this, d);
  }

  public void setNumeroCuotas(int i)
  {
    jdoSetnumeroCuotas(this, i);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Prestamo localPrestamo = new Prestamo();
    localPrestamo.jdoFlags = 1;
    localPrestamo.jdoStateManager = paramStateManager;
    return localPrestamo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Prestamo localPrestamo = new Prestamo();
    localPrestamo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPrestamo.jdoFlags = 1;
    localPrestamo.jdoStateManager = paramStateManager;
    return localPrestamo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cuotasPagadas);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaComienzoPago);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaOtorgo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaTipoPersonal);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPrestamo);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoCuota);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPagado);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPrestamo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroCuotas);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuotasPagadas = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaComienzoPago = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaOtorgo = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaTipoPersonal = ((FrecuenciaTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPrestamo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoCuota = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPagado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPrestamo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroCuotas = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Prestamo paramPrestamo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramPrestamo.conceptoTipoPersonal;
      return;
    case 1:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.cuotasPagadas = paramPrestamo.cuotasPagadas;
      return;
    case 2:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramPrestamo.documentoSoporte;
      return;
    case 3:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramPrestamo.estatus;
      return;
    case 4:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaComienzoPago = paramPrestamo.fechaComienzoPago;
      return;
    case 5:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaOtorgo = paramPrestamo.fechaOtorgo;
      return;
    case 6:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramPrestamo.fechaRegistro;
      return;
    case 7:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaTipoPersonal = paramPrestamo.frecuenciaTipoPersonal;
      return;
    case 8:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.idPrestamo = paramPrestamo.idPrestamo;
      return;
    case 9:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.montoCuota = paramPrestamo.montoCuota;
      return;
    case 10:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.montoPagado = paramPrestamo.montoPagado;
      return;
    case 11:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.montoPrestamo = paramPrestamo.montoPrestamo;
      return;
    case 12:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.numeroCuotas = paramPrestamo.numeroCuotas;
      return;
    case 13:
      if (paramPrestamo == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramPrestamo.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Prestamo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Prestamo localPrestamo = (Prestamo)paramObject;
    if (localPrestamo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPrestamo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PrestamoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PrestamoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrestamoPK))
      throw new IllegalArgumentException("arg1");
    PrestamoPK localPrestamoPK = (PrestamoPK)paramObject;
    localPrestamoPK.idPrestamo = this.idPrestamo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrestamoPK))
      throw new IllegalArgumentException("arg1");
    PrestamoPK localPrestamoPK = (PrestamoPK)paramObject;
    this.idPrestamo = localPrestamoPK.idPrestamo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrestamoPK))
      throw new IllegalArgumentException("arg2");
    PrestamoPK localPrestamoPK = (PrestamoPK)paramObject;
    localPrestamoPK.idPrestamo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 8);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrestamoPK))
      throw new IllegalArgumentException("arg2");
    PrestamoPK localPrestamoPK = (PrestamoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 8, localPrestamoPK.idPrestamo);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(Prestamo paramPrestamo)
  {
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 0))
      return paramPrestamo.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramPrestamo, jdoInheritedFieldCount + 0, paramPrestamo.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(Prestamo paramPrestamo, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramPrestamo, jdoInheritedFieldCount + 0, paramPrestamo.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final int jdoGetcuotasPagadas(Prestamo paramPrestamo)
  {
    if (paramPrestamo.jdoFlags <= 0)
      return paramPrestamo.cuotasPagadas;
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.cuotasPagadas;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 1))
      return paramPrestamo.cuotasPagadas;
    return localStateManager.getIntField(paramPrestamo, jdoInheritedFieldCount + 1, paramPrestamo.cuotasPagadas);
  }

  private static final void jdoSetcuotasPagadas(Prestamo paramPrestamo, int paramInt)
  {
    if (paramPrestamo.jdoFlags == 0)
    {
      paramPrestamo.cuotasPagadas = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.cuotasPagadas = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestamo, jdoInheritedFieldCount + 1, paramPrestamo.cuotasPagadas, paramInt);
  }

  private static final String jdoGetdocumentoSoporte(Prestamo paramPrestamo)
  {
    if (paramPrestamo.jdoFlags <= 0)
      return paramPrestamo.documentoSoporte;
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.documentoSoporte;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 2))
      return paramPrestamo.documentoSoporte;
    return localStateManager.getStringField(paramPrestamo, jdoInheritedFieldCount + 2, paramPrestamo.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(Prestamo paramPrestamo, String paramString)
  {
    if (paramPrestamo.jdoFlags == 0)
    {
      paramPrestamo.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramPrestamo, jdoInheritedFieldCount + 2, paramPrestamo.documentoSoporte, paramString);
  }

  private static final String jdoGetestatus(Prestamo paramPrestamo)
  {
    if (paramPrestamo.jdoFlags <= 0)
      return paramPrestamo.estatus;
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.estatus;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 3))
      return paramPrestamo.estatus;
    return localStateManager.getStringField(paramPrestamo, jdoInheritedFieldCount + 3, paramPrestamo.estatus);
  }

  private static final void jdoSetestatus(Prestamo paramPrestamo, String paramString)
  {
    if (paramPrestamo.jdoFlags == 0)
    {
      paramPrestamo.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramPrestamo, jdoInheritedFieldCount + 3, paramPrestamo.estatus, paramString);
  }

  private static final Date jdoGetfechaComienzoPago(Prestamo paramPrestamo)
  {
    if (paramPrestamo.jdoFlags <= 0)
      return paramPrestamo.fechaComienzoPago;
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.fechaComienzoPago;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 4))
      return paramPrestamo.fechaComienzoPago;
    return (Date)localStateManager.getObjectField(paramPrestamo, jdoInheritedFieldCount + 4, paramPrestamo.fechaComienzoPago);
  }

  private static final void jdoSetfechaComienzoPago(Prestamo paramPrestamo, Date paramDate)
  {
    if (paramPrestamo.jdoFlags == 0)
    {
      paramPrestamo.fechaComienzoPago = paramDate;
      return;
    }
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.fechaComienzoPago = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPrestamo, jdoInheritedFieldCount + 4, paramPrestamo.fechaComienzoPago, paramDate);
  }

  private static final Date jdoGetfechaOtorgo(Prestamo paramPrestamo)
  {
    if (paramPrestamo.jdoFlags <= 0)
      return paramPrestamo.fechaOtorgo;
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.fechaOtorgo;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 5))
      return paramPrestamo.fechaOtorgo;
    return (Date)localStateManager.getObjectField(paramPrestamo, jdoInheritedFieldCount + 5, paramPrestamo.fechaOtorgo);
  }

  private static final void jdoSetfechaOtorgo(Prestamo paramPrestamo, Date paramDate)
  {
    if (paramPrestamo.jdoFlags == 0)
    {
      paramPrestamo.fechaOtorgo = paramDate;
      return;
    }
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.fechaOtorgo = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPrestamo, jdoInheritedFieldCount + 5, paramPrestamo.fechaOtorgo, paramDate);
  }

  private static final Date jdoGetfechaRegistro(Prestamo paramPrestamo)
  {
    if (paramPrestamo.jdoFlags <= 0)
      return paramPrestamo.fechaRegistro;
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.fechaRegistro;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 6))
      return paramPrestamo.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramPrestamo, jdoInheritedFieldCount + 6, paramPrestamo.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(Prestamo paramPrestamo, Date paramDate)
  {
    if (paramPrestamo.jdoFlags == 0)
    {
      paramPrestamo.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPrestamo, jdoInheritedFieldCount + 6, paramPrestamo.fechaRegistro, paramDate);
  }

  private static final FrecuenciaTipoPersonal jdoGetfrecuenciaTipoPersonal(Prestamo paramPrestamo)
  {
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.frecuenciaTipoPersonal;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 7))
      return paramPrestamo.frecuenciaTipoPersonal;
    return (FrecuenciaTipoPersonal)localStateManager.getObjectField(paramPrestamo, jdoInheritedFieldCount + 7, paramPrestamo.frecuenciaTipoPersonal);
  }

  private static final void jdoSetfrecuenciaTipoPersonal(Prestamo paramPrestamo, FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.frecuenciaTipoPersonal = paramFrecuenciaTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramPrestamo, jdoInheritedFieldCount + 7, paramPrestamo.frecuenciaTipoPersonal, paramFrecuenciaTipoPersonal);
  }

  private static final long jdoGetidPrestamo(Prestamo paramPrestamo)
  {
    return paramPrestamo.idPrestamo;
  }

  private static final void jdoSetidPrestamo(Prestamo paramPrestamo, long paramLong)
  {
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.idPrestamo = paramLong;
      return;
    }
    localStateManager.setLongField(paramPrestamo, jdoInheritedFieldCount + 8, paramPrestamo.idPrestamo, paramLong);
  }

  private static final double jdoGetmontoCuota(Prestamo paramPrestamo)
  {
    if (paramPrestamo.jdoFlags <= 0)
      return paramPrestamo.montoCuota;
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.montoCuota;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 9))
      return paramPrestamo.montoCuota;
    return localStateManager.getDoubleField(paramPrestamo, jdoInheritedFieldCount + 9, paramPrestamo.montoCuota);
  }

  private static final void jdoSetmontoCuota(Prestamo paramPrestamo, double paramDouble)
  {
    if (paramPrestamo.jdoFlags == 0)
    {
      paramPrestamo.montoCuota = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.montoCuota = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestamo, jdoInheritedFieldCount + 9, paramPrestamo.montoCuota, paramDouble);
  }

  private static final double jdoGetmontoPagado(Prestamo paramPrestamo)
  {
    if (paramPrestamo.jdoFlags <= 0)
      return paramPrestamo.montoPagado;
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.montoPagado;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 10))
      return paramPrestamo.montoPagado;
    return localStateManager.getDoubleField(paramPrestamo, jdoInheritedFieldCount + 10, paramPrestamo.montoPagado);
  }

  private static final void jdoSetmontoPagado(Prestamo paramPrestamo, double paramDouble)
  {
    if (paramPrestamo.jdoFlags == 0)
    {
      paramPrestamo.montoPagado = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.montoPagado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestamo, jdoInheritedFieldCount + 10, paramPrestamo.montoPagado, paramDouble);
  }

  private static final double jdoGetmontoPrestamo(Prestamo paramPrestamo)
  {
    if (paramPrestamo.jdoFlags <= 0)
      return paramPrestamo.montoPrestamo;
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.montoPrestamo;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 11))
      return paramPrestamo.montoPrestamo;
    return localStateManager.getDoubleField(paramPrestamo, jdoInheritedFieldCount + 11, paramPrestamo.montoPrestamo);
  }

  private static final void jdoSetmontoPrestamo(Prestamo paramPrestamo, double paramDouble)
  {
    if (paramPrestamo.jdoFlags == 0)
    {
      paramPrestamo.montoPrestamo = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.montoPrestamo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestamo, jdoInheritedFieldCount + 11, paramPrestamo.montoPrestamo, paramDouble);
  }

  private static final int jdoGetnumeroCuotas(Prestamo paramPrestamo)
  {
    if (paramPrestamo.jdoFlags <= 0)
      return paramPrestamo.numeroCuotas;
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.numeroCuotas;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 12))
      return paramPrestamo.numeroCuotas;
    return localStateManager.getIntField(paramPrestamo, jdoInheritedFieldCount + 12, paramPrestamo.numeroCuotas);
  }

  private static final void jdoSetnumeroCuotas(Prestamo paramPrestamo, int paramInt)
  {
    if (paramPrestamo.jdoFlags == 0)
    {
      paramPrestamo.numeroCuotas = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.numeroCuotas = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestamo, jdoInheritedFieldCount + 12, paramPrestamo.numeroCuotas, paramInt);
  }

  private static final Trabajador jdoGettrabajador(Prestamo paramPrestamo)
  {
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
      return paramPrestamo.trabajador;
    if (localStateManager.isLoaded(paramPrestamo, jdoInheritedFieldCount + 13))
      return paramPrestamo.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramPrestamo, jdoInheritedFieldCount + 13, paramPrestamo.trabajador);
  }

  private static final void jdoSettrabajador(Prestamo paramPrestamo, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramPrestamo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestamo.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramPrestamo, jdoInheritedFieldCount + 13, paramPrestamo.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}