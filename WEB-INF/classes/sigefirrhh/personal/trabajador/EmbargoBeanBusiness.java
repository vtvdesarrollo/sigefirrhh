package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class EmbargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEmbargo(Embargo embargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Embargo embargoNew = 
      (Embargo)BeanUtils.cloneBean(
      embargo);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (embargoNew.getPersonal() != null) {
      embargoNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        embargoNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(embargoNew);
  }

  public void updateEmbargo(Embargo embargo) throws Exception
  {
    Embargo embargoModify = 
      findEmbargoById(embargo.getIdEmbargo());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (embargo.getPersonal() != null) {
      embargo.setPersonal(
        personalBeanBusiness.findPersonalById(
        embargo.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(embargoModify, embargo);
  }

  public void deleteEmbargo(Embargo embargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Embargo embargoDelete = 
      findEmbargoById(embargo.getIdEmbargo());
    pm.deletePersistent(embargoDelete);
  }

  public Embargo findEmbargoById(long idEmbargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEmbargo == pIdEmbargo";
    Query query = pm.newQuery(Embargo.class, filter);

    query.declareParameters("long pIdEmbargo");

    parameters.put("pIdEmbargo", new Long(idEmbargo));

    Collection colEmbargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEmbargo.iterator();
    return (Embargo)iterator.next();
  }

  public Collection findEmbargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent embargoExtent = pm.getExtent(
      Embargo.class, true);
    Query query = pm.newQuery(embargoExtent);
    query.setOrdering("fechaVigencia ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Embargo.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaVigencia ascending");

    Collection colEmbargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEmbargo);

    return colEmbargo;
  }
}