package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SueldoPromedioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSueldoPromedio(SueldoPromedio sueldoPromedio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SueldoPromedio sueldoPromedioNew = 
      (SueldoPromedio)BeanUtils.cloneBean(
      sueldoPromedio);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (sueldoPromedioNew.getTrabajador() != null) {
      sueldoPromedioNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        sueldoPromedioNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(sueldoPromedioNew);
  }

  public void updateSueldoPromedio(SueldoPromedio sueldoPromedio) throws Exception
  {
    SueldoPromedio sueldoPromedioModify = 
      findSueldoPromedioById(sueldoPromedio.getIdSueldoPromedio());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (sueldoPromedio.getTrabajador() != null) {
      sueldoPromedio.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        sueldoPromedio.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(sueldoPromedioModify, sueldoPromedio);
  }

  public void deleteSueldoPromedio(SueldoPromedio sueldoPromedio) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SueldoPromedio sueldoPromedioDelete = 
      findSueldoPromedioById(sueldoPromedio.getIdSueldoPromedio());
    pm.deletePersistent(sueldoPromedioDelete);
  }

  public SueldoPromedio findSueldoPromedioById(long idSueldoPromedio) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSueldoPromedio == pIdSueldoPromedio";
    Query query = pm.newQuery(SueldoPromedio.class, filter);

    query.declareParameters("long pIdSueldoPromedio");

    parameters.put("pIdSueldoPromedio", new Long(idSueldoPromedio));

    Collection colSueldoPromedio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSueldoPromedio.iterator();
    return (SueldoPromedio)iterator.next();
  }

  public Collection findSueldoPromedioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent sueldoPromedioExtent = pm.getExtent(
      SueldoPromedio.class, true);
    Query query = pm.newQuery(sueldoPromedioExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(SueldoPromedio.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    Collection colSueldoPromedio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSueldoPromedio);

    return colSueldoPromedio;
  }

  public Collection findByTiempoSitp(Date tiempoSitp)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tiempoSitp == pTiempoSitp";

    Query query = pm.newQuery(SueldoPromedio.class, filter);

    query.declareParameters("java.util.Date pTiempoSitp");
    HashMap parameters = new HashMap();

    parameters.put("pTiempoSitp", tiempoSitp);

    Collection colSueldoPromedio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSueldoPromedio);

    return colSueldoPromedio;
  }
}