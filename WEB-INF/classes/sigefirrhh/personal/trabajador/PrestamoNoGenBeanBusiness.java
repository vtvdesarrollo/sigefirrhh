package sigefirrhh.personal.trabajador;

import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class PrestamoNoGenBeanBusiness
{
  public Collection findForNomina(long idTrabajador, Long idFrecuenciaPago, String estatus, String criterio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && estatus == pEstatus && frecuenciaTipoPersonal.frecuenciaPago.idFrecuenciaPago == pIdFrecuenciaPago && conceptoTipoPersonal.concepto.codConcepto" + criterio;

    Query query = pm.newQuery(Prestamo.class, filter);

    query.declareParameters("long pIdTrabajador, long pIdFrecuenciaPago, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pIdFrecuenciaPago", idFrecuenciaPago);
    parameters.put("pEstatus", estatus);

    Collection colPrestamo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrestamo);

    return colPrestamo;
  }
}