package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class SueldoPromedio
  implements Serializable, PersistenceCapable
{
  private long idSueldoPromedio;
  private double promedioIntegral;
  private double promedioSueldo;
  private double promedioAjustes;
  private double promedioCompensacion;
  private double promedioPrimasc;
  private double promedioPrimast;
  private double sueldoAnual;
  private double promedioSso;
  private double retencionSso;
  private double aporteSso;
  private double promedioSpf;
  private double retencionSpf;
  private double aporteSpf;
  private double promedioLph;
  private double retencionLph;
  private double aporteLph;
  private double promedioFju;
  private double retencionFju;
  private double aporteFju;
  private double promedioVacaciones;
  private double promedioUtilidad;
  private double montoVariableSso;
  private double montoVariableFju;
  private double montoVariableLph;
  private Trabajador trabajador;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aporteFju", "aporteLph", "aporteSpf", "aporteSso", "idSitp", "idSueldoPromedio", "montoVariableFju", "montoVariableLph", "montoVariableSso", "promedioAjustes", "promedioCompensacion", "promedioFju", "promedioIntegral", "promedioLph", "promedioPrimasc", "promedioPrimast", "promedioSpf", "promedioSso", "promedioSueldo", "promedioUtilidad", "promedioVacaciones", "retencionFju", "retencionLph", "retencionSpf", "retencionSso", "sueldoAnual", "tiempoSitp", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, Long.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public SueldoPromedio()
  {
    jdoSetpromedioIntegral(this, 0.0D);

    jdoSetpromedioSueldo(this, 0.0D);

    jdoSetpromedioAjustes(this, 0.0D);

    jdoSetpromedioCompensacion(this, 0.0D);

    jdoSetpromedioPrimasc(this, 0.0D);

    jdoSetpromedioPrimast(this, 0.0D);

    jdoSetsueldoAnual(this, 0.0D);

    jdoSetpromedioSso(this, 0.0D);

    jdoSetretencionSso(this, 0.0D);

    jdoSetaporteSso(this, 0.0D);

    jdoSetpromedioSpf(this, 0.0D);

    jdoSetretencionSpf(this, 0.0D);

    jdoSetpromedioLph(this, 0.0D);

    jdoSetretencionLph(this, 0.0D);

    jdoSetpromedioFju(this, 0.0D);

    jdoSetretencionFju(this, 0.0D);

    jdoSetpromedioVacaciones(this, 0.0D);

    jdoSetpromedioUtilidad(this, 0.0D);

    jdoSetmontoVariableSso(this, 0.0D);

    jdoSetmontoVariableFju(this, 0.0D);

    jdoSetmontoVariableLph(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat a = new DecimalFormat();
    a.applyPattern("##,###,###.00");
    String promedioIntegral = a.format(jdoGetpromedioIntegral(this));
    String promedioSueldo = a.format(jdoGetpromedioSueldo(this));
    String promedioSso = a.format(jdoGetpromedioSso(this));
    String promedioLph = a.format(jdoGetpromedioLph(this));

    return "Integral: " + promedioIntegral + "Sueldo: " + promedioSueldo + "SSO: " + promedioSso + "LPH: " + promedioLph;
  }

  public long getIdSueldoPromedio()
  {
    return jdoGetidSueldoPromedio(this);
  }

  public double getPromedioFju()
  {
    return jdoGetpromedioFju(this);
  }

  public double getPromedioIntegral()
  {
    return jdoGetpromedioIntegral(this);
  }

  public double getPromedioLph()
  {
    return jdoGetpromedioLph(this);
  }

  public double getPromedioSso()
  {
    return jdoGetpromedioSso(this);
  }

  public double getPromedioSueldo()
  {
    return jdoGetpromedioSueldo(this);
  }

  public double getPromedioUtilidad()
  {
    return jdoGetpromedioUtilidad(this);
  }

  public double getPromedioVacaciones()
  {
    return jdoGetpromedioVacaciones(this);
  }

  public double getSueldoAnual()
  {
    return jdoGetsueldoAnual(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setIdSueldoPromedio(long l)
  {
    jdoSetidSueldoPromedio(this, l);
  }

  public void setPromedioFju(double d)
  {
    jdoSetpromedioFju(this, d);
  }

  public void setPromedioIntegral(double d)
  {
    jdoSetpromedioIntegral(this, d);
  }

  public void setPromedioLph(double d)
  {
    jdoSetpromedioLph(this, d);
  }

  public void setPromedioSso(double d)
  {
    jdoSetpromedioSso(this, d);
  }

  public void setPromedioSueldo(double d)
  {
    jdoSetpromedioSueldo(this, d);
  }

  public void setPromedioUtilidad(double d)
  {
    jdoSetpromedioUtilidad(this, d);
  }

  public void setPromedioVacaciones(double d)
  {
    jdoSetpromedioVacaciones(this, d);
  }

  public void setSueldoAnual(double d)
  {
    jdoSetsueldoAnual(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public double getRetencionFju()
  {
    return jdoGetretencionFju(this);
  }

  public double getRetencionLph()
  {
    return jdoGetretencionLph(this);
  }

  public double getRetencionSpf()
  {
    return jdoGetretencionSpf(this);
  }

  public double getRetencionSso()
  {
    return jdoGetretencionSso(this);
  }

  public void setRetencionFju(double d)
  {
    jdoSetretencionFju(this, d);
  }

  public void setRetencionLph(double d)
  {
    jdoSetretencionLph(this, d);
  }

  public void setRetencionSpf(double d)
  {
    jdoSetretencionSpf(this, d);
  }

  public void setRetencionSso(double d)
  {
    jdoSetretencionSso(this, d);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public double getAporteFju()
  {
    return jdoGetaporteFju(this);
  }

  public void setAporteFju(double aporteFju)
  {
    jdoSetaporteFju(this, aporteFju);
  }

  public double getAporteLph()
  {
    return jdoGetaporteLph(this);
  }

  public void setAporteLph(double aporteLph)
  {
    jdoSetaporteLph(this, aporteLph);
  }

  public double getAporteSpf()
  {
    return jdoGetaporteSpf(this);
  }

  public void setAporteSpf(double aporteSpf)
  {
    jdoSetaporteSpf(this, aporteSpf);
  }

  public double getAporteSso()
  {
    return jdoGetaporteSso(this);
  }

  public void setAporteSso(double aporteSso)
  {
    jdoSetaporteSso(this, aporteSso);
  }

  public double getPromedioSpf()
  {
    return jdoGetpromedioSpf(this);
  }

  public void setPromedioSpf(double promedioSpf)
  {
    jdoSetpromedioSpf(this, promedioSpf);
  }

  public double getPromedioAjustes()
  {
    return jdoGetpromedioAjustes(this);
  }

  public void setPromedioAjustes(double promedioAjustes)
  {
    jdoSetpromedioAjustes(this, promedioAjustes);
  }

  public double getPromedioCompensacion()
  {
    return jdoGetpromedioCompensacion(this);
  }

  public void setPromedioCompensacion(double promedioCompensacion)
  {
    jdoSetpromedioCompensacion(this, promedioCompensacion);
  }

  public double getPromedioPrimasc()
  {
    return jdoGetpromedioPrimasc(this);
  }

  public void setPromedioPrimasc(double promedioPrimasc)
  {
    jdoSetpromedioPrimasc(this, promedioPrimasc);
  }

  public double getPromedioPrimast()
  {
    return jdoGetpromedioPrimast(this);
  }

  public void setPromedioPrimast(double promedioPrimast)
  {
    jdoSetpromedioPrimast(this, promedioPrimast);
  }

  public double getMontoVariableFju()
  {
    return jdoGetmontoVariableFju(this);
  }

  public double getMontoVariableLph()
  {
    return jdoGetmontoVariableLph(this);
  }

  public double getMontoVariableSso()
  {
    return jdoGetmontoVariableSso(this);
  }

  public void setMontoVariableFju(double d)
  {
    jdoSetmontoVariableFju(this, d);
  }

  public void setMontoVariableLph(double d)
  {
    jdoSetmontoVariableLph(this, d);
  }

  public void setMontoVariableSso(double d)
  {
    jdoSetmontoVariableSso(this, d);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 28;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.SueldoPromedio"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SueldoPromedio());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SueldoPromedio localSueldoPromedio = new SueldoPromedio();
    localSueldoPromedio.jdoFlags = 1;
    localSueldoPromedio.jdoStateManager = paramStateManager;
    return localSueldoPromedio;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SueldoPromedio localSueldoPromedio = new SueldoPromedio();
    localSueldoPromedio.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSueldoPromedio.jdoFlags = 1;
    localSueldoPromedio.jdoStateManager = paramStateManager;
    return localSueldoPromedio;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.aporteFju);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.aporteLph);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.aporteSpf);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.aporteSso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSueldoPromedio);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoVariableFju);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoVariableLph);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoVariableSso);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioAjustes);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioCompensacion);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioFju);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioIntegral);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioLph);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioPrimasc);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioPrimast);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioSpf);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioSso);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioSueldo);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioUtilidad);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.promedioVacaciones);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionFju);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionLph);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionSpf);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionSso);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoAnual);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aporteFju = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aporteLph = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aporteSpf = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aporteSso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSueldoPromedio = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoVariableFju = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoVariableLph = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoVariableSso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioAjustes = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioCompensacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioFju = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioIntegral = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioLph = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioPrimasc = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioPrimast = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioSpf = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioSso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioSueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioUtilidad = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioVacaciones = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionFju = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionLph = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionSpf = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionSso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoAnual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SueldoPromedio paramSueldoPromedio, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.aporteFju = paramSueldoPromedio.aporteFju;
      return;
    case 1:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.aporteLph = paramSueldoPromedio.aporteLph;
      return;
    case 2:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.aporteSpf = paramSueldoPromedio.aporteSpf;
      return;
    case 3:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.aporteSso = paramSueldoPromedio.aporteSso;
      return;
    case 4:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramSueldoPromedio.idSitp;
      return;
    case 5:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.idSueldoPromedio = paramSueldoPromedio.idSueldoPromedio;
      return;
    case 6:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.montoVariableFju = paramSueldoPromedio.montoVariableFju;
      return;
    case 7:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.montoVariableLph = paramSueldoPromedio.montoVariableLph;
      return;
    case 8:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.montoVariableSso = paramSueldoPromedio.montoVariableSso;
      return;
    case 9:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioAjustes = paramSueldoPromedio.promedioAjustes;
      return;
    case 10:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioCompensacion = paramSueldoPromedio.promedioCompensacion;
      return;
    case 11:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioFju = paramSueldoPromedio.promedioFju;
      return;
    case 12:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioIntegral = paramSueldoPromedio.promedioIntegral;
      return;
    case 13:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioLph = paramSueldoPromedio.promedioLph;
      return;
    case 14:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioPrimasc = paramSueldoPromedio.promedioPrimasc;
      return;
    case 15:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioPrimast = paramSueldoPromedio.promedioPrimast;
      return;
    case 16:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioSpf = paramSueldoPromedio.promedioSpf;
      return;
    case 17:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioSso = paramSueldoPromedio.promedioSso;
      return;
    case 18:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioSueldo = paramSueldoPromedio.promedioSueldo;
      return;
    case 19:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioUtilidad = paramSueldoPromedio.promedioUtilidad;
      return;
    case 20:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.promedioVacaciones = paramSueldoPromedio.promedioVacaciones;
      return;
    case 21:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.retencionFju = paramSueldoPromedio.retencionFju;
      return;
    case 22:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.retencionLph = paramSueldoPromedio.retencionLph;
      return;
    case 23:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.retencionSpf = paramSueldoPromedio.retencionSpf;
      return;
    case 24:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.retencionSso = paramSueldoPromedio.retencionSso;
      return;
    case 25:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoAnual = paramSueldoPromedio.sueldoAnual;
      return;
    case 26:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramSueldoPromedio.tiempoSitp;
      return;
    case 27:
      if (paramSueldoPromedio == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramSueldoPromedio.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SueldoPromedio))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SueldoPromedio localSueldoPromedio = (SueldoPromedio)paramObject;
    if (localSueldoPromedio.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSueldoPromedio, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SueldoPromedioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SueldoPromedioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SueldoPromedioPK))
      throw new IllegalArgumentException("arg1");
    SueldoPromedioPK localSueldoPromedioPK = (SueldoPromedioPK)paramObject;
    localSueldoPromedioPK.idSueldoPromedio = this.idSueldoPromedio;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SueldoPromedioPK))
      throw new IllegalArgumentException("arg1");
    SueldoPromedioPK localSueldoPromedioPK = (SueldoPromedioPK)paramObject;
    this.idSueldoPromedio = localSueldoPromedioPK.idSueldoPromedio;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SueldoPromedioPK))
      throw new IllegalArgumentException("arg2");
    SueldoPromedioPK localSueldoPromedioPK = (SueldoPromedioPK)paramObject;
    localSueldoPromedioPK.idSueldoPromedio = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SueldoPromedioPK))
      throw new IllegalArgumentException("arg2");
    SueldoPromedioPK localSueldoPromedioPK = (SueldoPromedioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localSueldoPromedioPK.idSueldoPromedio);
  }

  private static final double jdoGetaporteFju(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.aporteFju;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.aporteFju;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 0))
      return paramSueldoPromedio.aporteFju;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 0, paramSueldoPromedio.aporteFju);
  }

  private static final void jdoSetaporteFju(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.aporteFju = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.aporteFju = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 0, paramSueldoPromedio.aporteFju, paramDouble);
  }

  private static final double jdoGetaporteLph(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.aporteLph;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.aporteLph;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 1))
      return paramSueldoPromedio.aporteLph;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 1, paramSueldoPromedio.aporteLph);
  }

  private static final void jdoSetaporteLph(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.aporteLph = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.aporteLph = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 1, paramSueldoPromedio.aporteLph, paramDouble);
  }

  private static final double jdoGetaporteSpf(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.aporteSpf;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.aporteSpf;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 2))
      return paramSueldoPromedio.aporteSpf;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 2, paramSueldoPromedio.aporteSpf);
  }

  private static final void jdoSetaporteSpf(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.aporteSpf = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.aporteSpf = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 2, paramSueldoPromedio.aporteSpf, paramDouble);
  }

  private static final double jdoGetaporteSso(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.aporteSso;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.aporteSso;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 3))
      return paramSueldoPromedio.aporteSso;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 3, paramSueldoPromedio.aporteSso);
  }

  private static final void jdoSetaporteSso(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.aporteSso = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.aporteSso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 3, paramSueldoPromedio.aporteSso, paramDouble);
  }

  private static final int jdoGetidSitp(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.idSitp;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.idSitp;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 4))
      return paramSueldoPromedio.idSitp;
    return localStateManager.getIntField(paramSueldoPromedio, jdoInheritedFieldCount + 4, paramSueldoPromedio.idSitp);
  }

  private static final void jdoSetidSitp(SueldoPromedio paramSueldoPromedio, int paramInt)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramSueldoPromedio, jdoInheritedFieldCount + 4, paramSueldoPromedio.idSitp, paramInt);
  }

  private static final long jdoGetidSueldoPromedio(SueldoPromedio paramSueldoPromedio)
  {
    return paramSueldoPromedio.idSueldoPromedio;
  }

  private static final void jdoSetidSueldoPromedio(SueldoPromedio paramSueldoPromedio, long paramLong)
  {
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.idSueldoPromedio = paramLong;
      return;
    }
    localStateManager.setLongField(paramSueldoPromedio, jdoInheritedFieldCount + 5, paramSueldoPromedio.idSueldoPromedio, paramLong);
  }

  private static final double jdoGetmontoVariableFju(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.montoVariableFju;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.montoVariableFju;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 6))
      return paramSueldoPromedio.montoVariableFju;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 6, paramSueldoPromedio.montoVariableFju);
  }

  private static final void jdoSetmontoVariableFju(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.montoVariableFju = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.montoVariableFju = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 6, paramSueldoPromedio.montoVariableFju, paramDouble);
  }

  private static final double jdoGetmontoVariableLph(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.montoVariableLph;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.montoVariableLph;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 7))
      return paramSueldoPromedio.montoVariableLph;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 7, paramSueldoPromedio.montoVariableLph);
  }

  private static final void jdoSetmontoVariableLph(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.montoVariableLph = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.montoVariableLph = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 7, paramSueldoPromedio.montoVariableLph, paramDouble);
  }

  private static final double jdoGetmontoVariableSso(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.montoVariableSso;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.montoVariableSso;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 8))
      return paramSueldoPromedio.montoVariableSso;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 8, paramSueldoPromedio.montoVariableSso);
  }

  private static final void jdoSetmontoVariableSso(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.montoVariableSso = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.montoVariableSso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 8, paramSueldoPromedio.montoVariableSso, paramDouble);
  }

  private static final double jdoGetpromedioAjustes(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioAjustes;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioAjustes;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 9))
      return paramSueldoPromedio.promedioAjustes;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 9, paramSueldoPromedio.promedioAjustes);
  }

  private static final void jdoSetpromedioAjustes(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioAjustes = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioAjustes = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 9, paramSueldoPromedio.promedioAjustes, paramDouble);
  }

  private static final double jdoGetpromedioCompensacion(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioCompensacion;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioCompensacion;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 10))
      return paramSueldoPromedio.promedioCompensacion;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 10, paramSueldoPromedio.promedioCompensacion);
  }

  private static final void jdoSetpromedioCompensacion(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioCompensacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioCompensacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 10, paramSueldoPromedio.promedioCompensacion, paramDouble);
  }

  private static final double jdoGetpromedioFju(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioFju;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioFju;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 11))
      return paramSueldoPromedio.promedioFju;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 11, paramSueldoPromedio.promedioFju);
  }

  private static final void jdoSetpromedioFju(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioFju = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioFju = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 11, paramSueldoPromedio.promedioFju, paramDouble);
  }

  private static final double jdoGetpromedioIntegral(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioIntegral;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioIntegral;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 12))
      return paramSueldoPromedio.promedioIntegral;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 12, paramSueldoPromedio.promedioIntegral);
  }

  private static final void jdoSetpromedioIntegral(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioIntegral = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioIntegral = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 12, paramSueldoPromedio.promedioIntegral, paramDouble);
  }

  private static final double jdoGetpromedioLph(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioLph;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioLph;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 13))
      return paramSueldoPromedio.promedioLph;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 13, paramSueldoPromedio.promedioLph);
  }

  private static final void jdoSetpromedioLph(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioLph = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioLph = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 13, paramSueldoPromedio.promedioLph, paramDouble);
  }

  private static final double jdoGetpromedioPrimasc(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioPrimasc;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioPrimasc;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 14))
      return paramSueldoPromedio.promedioPrimasc;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 14, paramSueldoPromedio.promedioPrimasc);
  }

  private static final void jdoSetpromedioPrimasc(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioPrimasc = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioPrimasc = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 14, paramSueldoPromedio.promedioPrimasc, paramDouble);
  }

  private static final double jdoGetpromedioPrimast(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioPrimast;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioPrimast;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 15))
      return paramSueldoPromedio.promedioPrimast;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 15, paramSueldoPromedio.promedioPrimast);
  }

  private static final void jdoSetpromedioPrimast(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioPrimast = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioPrimast = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 15, paramSueldoPromedio.promedioPrimast, paramDouble);
  }

  private static final double jdoGetpromedioSpf(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioSpf;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioSpf;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 16))
      return paramSueldoPromedio.promedioSpf;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 16, paramSueldoPromedio.promedioSpf);
  }

  private static final void jdoSetpromedioSpf(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioSpf = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioSpf = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 16, paramSueldoPromedio.promedioSpf, paramDouble);
  }

  private static final double jdoGetpromedioSso(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioSso;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioSso;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 17))
      return paramSueldoPromedio.promedioSso;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 17, paramSueldoPromedio.promedioSso);
  }

  private static final void jdoSetpromedioSso(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioSso = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioSso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 17, paramSueldoPromedio.promedioSso, paramDouble);
  }

  private static final double jdoGetpromedioSueldo(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioSueldo;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioSueldo;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 18))
      return paramSueldoPromedio.promedioSueldo;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 18, paramSueldoPromedio.promedioSueldo);
  }

  private static final void jdoSetpromedioSueldo(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioSueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioSueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 18, paramSueldoPromedio.promedioSueldo, paramDouble);
  }

  private static final double jdoGetpromedioUtilidad(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioUtilidad;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioUtilidad;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 19))
      return paramSueldoPromedio.promedioUtilidad;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 19, paramSueldoPromedio.promedioUtilidad);
  }

  private static final void jdoSetpromedioUtilidad(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioUtilidad = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioUtilidad = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 19, paramSueldoPromedio.promedioUtilidad, paramDouble);
  }

  private static final double jdoGetpromedioVacaciones(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.promedioVacaciones;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.promedioVacaciones;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 20))
      return paramSueldoPromedio.promedioVacaciones;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 20, paramSueldoPromedio.promedioVacaciones);
  }

  private static final void jdoSetpromedioVacaciones(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.promedioVacaciones = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.promedioVacaciones = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 20, paramSueldoPromedio.promedioVacaciones, paramDouble);
  }

  private static final double jdoGetretencionFju(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.retencionFju;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.retencionFju;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 21))
      return paramSueldoPromedio.retencionFju;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 21, paramSueldoPromedio.retencionFju);
  }

  private static final void jdoSetretencionFju(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.retencionFju = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.retencionFju = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 21, paramSueldoPromedio.retencionFju, paramDouble);
  }

  private static final double jdoGetretencionLph(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.retencionLph;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.retencionLph;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 22))
      return paramSueldoPromedio.retencionLph;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 22, paramSueldoPromedio.retencionLph);
  }

  private static final void jdoSetretencionLph(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.retencionLph = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.retencionLph = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 22, paramSueldoPromedio.retencionLph, paramDouble);
  }

  private static final double jdoGetretencionSpf(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.retencionSpf;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.retencionSpf;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 23))
      return paramSueldoPromedio.retencionSpf;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 23, paramSueldoPromedio.retencionSpf);
  }

  private static final void jdoSetretencionSpf(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.retencionSpf = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.retencionSpf = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 23, paramSueldoPromedio.retencionSpf, paramDouble);
  }

  private static final double jdoGetretencionSso(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.retencionSso;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.retencionSso;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 24))
      return paramSueldoPromedio.retencionSso;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 24, paramSueldoPromedio.retencionSso);
  }

  private static final void jdoSetretencionSso(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.retencionSso = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.retencionSso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 24, paramSueldoPromedio.retencionSso, paramDouble);
  }

  private static final double jdoGetsueldoAnual(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.sueldoAnual;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.sueldoAnual;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 25))
      return paramSueldoPromedio.sueldoAnual;
    return localStateManager.getDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 25, paramSueldoPromedio.sueldoAnual);
  }

  private static final void jdoSetsueldoAnual(SueldoPromedio paramSueldoPromedio, double paramDouble)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.sueldoAnual = paramDouble;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.sueldoAnual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSueldoPromedio, jdoInheritedFieldCount + 25, paramSueldoPromedio.sueldoAnual, paramDouble);
  }

  private static final Date jdoGettiempoSitp(SueldoPromedio paramSueldoPromedio)
  {
    if (paramSueldoPromedio.jdoFlags <= 0)
      return paramSueldoPromedio.tiempoSitp;
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.tiempoSitp;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 26))
      return paramSueldoPromedio.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramSueldoPromedio, jdoInheritedFieldCount + 26, paramSueldoPromedio.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(SueldoPromedio paramSueldoPromedio, Date paramDate)
  {
    if (paramSueldoPromedio.jdoFlags == 0)
    {
      paramSueldoPromedio.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSueldoPromedio, jdoInheritedFieldCount + 26, paramSueldoPromedio.tiempoSitp, paramDate);
  }

  private static final Trabajador jdoGettrabajador(SueldoPromedio paramSueldoPromedio)
  {
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
      return paramSueldoPromedio.trabajador;
    if (localStateManager.isLoaded(paramSueldoPromedio, jdoInheritedFieldCount + 27))
      return paramSueldoPromedio.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramSueldoPromedio, jdoInheritedFieldCount + 27, paramSueldoPromedio.trabajador);
  }

  private static final void jdoSettrabajador(SueldoPromedio paramSueldoPromedio, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramSueldoPromedio.jdoStateManager;
    if (localStateManager == null)
    {
      paramSueldoPromedio.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramSueldoPromedio, jdoInheritedFieldCount + 27, paramSueldoPromedio.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}