package sigefirrhh.personal.trabajador;

import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ConceptoLiquidacionNoGenBeanBusiness
{
  public Collection findForNomina(long idTrabajador, Long idFrecuenciaPago, String estatus, String criterio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && estatus == pEstatus && frecuenciaTipoPersonal.frecuenciaPago.idFrecuenciaPago == pIdFrecuenciaPago && conceptoTipoPersonal.concepto.codConcepto" + criterio;

    Query query = pm.newQuery(ConceptoLiquidacion.class, filter);

    query.declareParameters("long pIdTrabajador, long pIdFrecuenciaPago, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pIdFrecuenciaPago", idFrecuenciaPago);
    parameters.put("pEstatus", estatus);

    Collection colConceptoLiquidacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoLiquidacion);

    return colConceptoLiquidacion;
  }

  public Collection findByTrabajadorAndConceptoTipoPersonal(long idTrabajador, long idConceptoTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal";

    Query query = pm.newQuery(ConceptoLiquidacion.class, filter);

    query.declareParameters("long pIdTrabajador, long pIdConceptoTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    Collection colConceptoLiquidacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colConceptoLiquidacion;
  }
}