package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class PlanillaArcPK
  implements Serializable
{
  public long idPlanillaArc;

  public PlanillaArcPK()
  {
  }

  public PlanillaArcPK(long idPlanillaArc)
  {
    this.idPlanillaArc = idPlanillaArc;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PlanillaArcPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PlanillaArcPK thatPK)
  {
    return 
      this.idPlanillaArc == thatPK.idPlanillaArc;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPlanillaArc)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPlanillaArc);
  }
}