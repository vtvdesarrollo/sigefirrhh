package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class ProcesosPK
  implements Serializable
{
  public long idProcesos;

  public ProcesosPK()
  {
  }

  public ProcesosPK(long idProcesos)
  {
    this.idProcesos = idProcesos;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ProcesosPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ProcesosPK thatPK)
  {
    return 
      this.idProcesos == thatPK.idProcesos;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idProcesos)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idProcesos);
  }
}