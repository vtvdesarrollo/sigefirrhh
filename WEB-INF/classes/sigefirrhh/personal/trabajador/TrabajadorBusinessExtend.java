package sigefirrhh.personal.trabajador;

import eforserver.sequence.IdentityGenerator;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesBusinessExtend;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;

public class TrabajadorBusinessExtend extends TrabajadorBusiness
{
  Logger log = Logger.getLogger(TrabajadorBusinessExtend.class.getName());

  TrabajadorBeanBusinessExtend trabajadorBeanBusiness = new TrabajadorBeanBusinessExtend();

  ConceptoFijoBeanBusinessExtend conceptoFijoBeanBusiness = new ConceptoFijoBeanBusinessExtend();

  ConceptoVariableBeanBusiness conceptoVariableBeanBusiness = new ConceptoVariableBeanBusiness();

  ConceptoLiquidacionBeanBusiness conceptoLiquidacioneBeanBusiness = new ConceptoLiquidacionBeanBusiness();

  SueldoPromedioBeanBusinessExtend sueldoPromedioBeanBusiness = new SueldoPromedioBeanBusinessExtend();

  DefinicionesBusinessExtend definicionesBusiness = new DefinicionesBusinessExtend();

  public Collection findTrabajadorActivoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.trabajadorBeanBusiness.findActivoByTipoPersonal(
      idTipoPersonal);
  }

  public Trabajador findTrabajadorActivoByTipoPersonal(int cedula, long idTipoPersonal)
    throws Exception
  {
    return this.trabajadorBeanBusiness.findActivoByTipoPersonal(
      cedula, 
      idTipoPersonal);
  }

  public Collection findTrabajadorActivoByTipoPersonal(long idTipoPersonal, long idOrganismo) throws Exception
  {
    return this.trabajadorBeanBusiness.findActivoByTipoPersonal(
      idTipoPersonal, idOrganismo);
  }

  public Collection findConceptoFijoFromConceptoAsociado(long idTrabajador, long idConceptoTipoPersonal, byte frecuenciaMensual)
    throws Exception
  {
    return this.conceptoFijoBeanBusiness.findFromConceptoAsociado(
      idTrabajador, idConceptoTipoPersonal, frecuenciaMensual);
  }

  public ConceptoFijo findConceptoFijoByTrabajador(long idFrecuenciaTipoPersonal, long idConceptoTipoPersonal, long idTrabajador)
    throws Exception
  {
    return this.conceptoFijoBeanBusiness.findByTrabajador(
      idFrecuenciaTipoPersonal, idConceptoTipoPersonal, idTrabajador);
  }

  public ConceptoVariable findConceptoVariableByTrabajador(long idFrecuenciaTipoPersonal, long idConceptoTipoPersonal, long idTrabajador)
    throws Exception
  {
    return this.conceptoVariableBeanBusiness.findByTrabajador(
      idFrecuenciaTipoPersonal, idConceptoTipoPersonal, idTrabajador);
  }

  public ConceptoLiquidacion findConceptoLiquidacionByTrabajador(long idFrecuenciaTipoPersonal, long idConceptoTipoPersonal, long idTrabajador)
    throws Exception
  {
    return this.conceptoLiquidacionBeanBusiness.findByTrabajador(
      idFrecuenciaTipoPersonal, idConceptoTipoPersonal, idTrabajador);
  }

  public Collection findSueldoPromedioByTrabajador(long idTrabajador) throws Exception
  {
    return this.sueldoPromedioBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void cargarConceptos(Collection temporalConceptos, String accion, String tipoConcepto) throws Exception
  {
    Iterator iterator = temporalConceptos.iterator();
    TemporalConcepto temporalConcepto = null;
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
    Trabajador trabajador = null;
    ConceptoFijo conceptoFijo = null;
    ConceptoVariable conceptoVariable = null;

    while (iterator.hasNext()) {
      temporalConcepto = (TemporalConcepto)iterator.next();

      conceptoTipoPersonal = 
        (ConceptoTipoPersonal)
        this.definicionesBusiness.findConceptoTipoPersonalByCodConcepto(
        temporalConcepto.getIdTipoPersonal(), 
        temporalConcepto.getCodigoConcepto()).iterator().next();
      frecuenciaTipoPersonal = 
        this.definicionesBusiness.findFrecuenciaTipoPersonalByTipoPersonal(
        temporalConcepto.getIdFrecuenciaPago(), 
        temporalConcepto.getIdTipoPersonal());
      trabajador = 
        (Trabajador)
        findTrabajadorByCedula(
        temporalConcepto.getCedula(), 
        temporalConcepto.getIdOrganismo()).iterator().next();

      if (tipoConcepto.equals("F"))
        try {
          conceptoFijo = findConceptoFijoByTrabajador(
            frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal(), 
            conceptoTipoPersonal.getIdConceptoTipoPersonal(), 
            trabajador.getIdTrabajador());
          if (accion.equals("M"))
            conceptoFijo.setMonto(temporalConcepto.getMonto());
          else if (accion.equals("S"))
            conceptoFijo.setMonto(conceptoFijo.getMonto() + temporalConcepto.getMonto());
        }
        catch (Exception e) {
          this.log.error("Excepcion controlada:", e);
          addConceptoFijoAux(temporalConcepto, frecuenciaTipoPersonal, conceptoTipoPersonal, trabajador);
        }
      else if (tipoConcepto.equals("V"))
        try {
          conceptoVariable = findConceptoVariableByTrabajador(
            frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal(), 
            conceptoTipoPersonal.getIdConceptoTipoPersonal(), 
            trabajador.getIdTrabajador());
          if (accion.equals("M"))
            conceptoVariable.setMonto(temporalConcepto.getMonto());
          else if (accion.equals("S"))
            conceptoVariable.setMonto(conceptoVariable.getMonto() + temporalConcepto.getMonto());
        }
        catch (Exception e) {
          addConceptoVariableAux(temporalConcepto, frecuenciaTipoPersonal, conceptoTipoPersonal, trabajador);
        }
    }
  }

  private void addConceptoFijoAux(TemporalConcepto temporalConcepto, FrecuenciaTipoPersonal frecuenciaTipoPersonal, ConceptoTipoPersonal conceptoTipoPersonal, Trabajador trabajador)
    throws Exception
  {
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

    ConceptoFijo conceptoFijo = new ConceptoFijo();
    conceptoFijo.setIdConceptoFijo(
      identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo"));
    conceptoFijo.setConceptoTipoPersonal(conceptoTipoPersonal);
    conceptoFijo.setTrabajador(trabajador);
    conceptoFijo.setMonto(temporalConcepto.getMonto());
    conceptoFijo.setFrecuenciaTipoPersonal(frecuenciaTipoPersonal);
    conceptoFijo.setFechaRegistro(new Date());
    addConceptoFijo(conceptoFijo);
  }

  private void addConceptoVariableAux(TemporalConcepto temporalConcepto, FrecuenciaTipoPersonal frecuenciaTipoPersonal, ConceptoTipoPersonal conceptoTipoPersonal, Trabajador trabajador)
    throws Exception
  {
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

    ConceptoVariable conceptoVariable = new ConceptoVariable();
    conceptoVariable.setIdConceptoVariable(
      identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable"));
    conceptoVariable.setConceptoTipoPersonal(conceptoTipoPersonal);
    conceptoVariable.setTrabajador(trabajador);
    conceptoVariable.setMonto(temporalConcepto.getMonto());
    conceptoVariable.setFrecuenciaTipoPersonal(frecuenciaTipoPersonal);
    conceptoVariable.setFechaRegistro(new Date());
    addConceptoVariable(conceptoVariable);
  }

  private void addConceptoLiquidacionAux(TemporalConcepto temporalConcepto, FrecuenciaTipoPersonal frecuenciaTipoPersonal, ConceptoTipoPersonal conceptoTipoPersonal, Trabajador trabajador)
    throws Exception
  {
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

    ConceptoLiquidacion conceptoLiquidacion = new ConceptoLiquidacion();
    conceptoLiquidacion.setIdConceptoLiquidacion(
      identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoLiquidacion"));
    conceptoLiquidacion.setConceptoTipoPersonal(conceptoTipoPersonal);
    conceptoLiquidacion.setTrabajador(trabajador);
    conceptoLiquidacion.setMonto(temporalConcepto.getMonto());
    conceptoLiquidacion.setFrecuenciaTipoPersonal(frecuenciaTipoPersonal);
    conceptoLiquidacion.setFechaRegistro(new Date());
    addConceptoLiquidacion(conceptoLiquidacion);
  }
}