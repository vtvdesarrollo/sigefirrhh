package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.jdo.PersistenceManager;

public class TrabajadorFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();

  public void addTrabajadorAsignatura(TrabajadorAsignatura trabajadorAsignatura)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addTrabajadorAsignatura(trabajadorAsignatura);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTrabajadorAsignatura(TrabajadorAsignatura trabajadorAsignatura) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updateTrabajadorAsignatura(trabajadorAsignatura);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTrabajadorAsignatura(TrabajadorAsignatura trabajadorAsignatura) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deleteTrabajadorAsignatura(trabajadorAsignatura);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TrabajadorAsignatura findTrabajadorAsignaturaById(long trabajadorAsignaturaId) throws Exception
  {
    try { this.txn.open();
      TrabajadorAsignatura trabajadorAsignatura = 
        this.trabajadorBusiness.findTrabajadorAsignaturaById(trabajadorAsignaturaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(trabajadorAsignatura);
      return trabajadorAsignatura;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTrabajadorAsignatura() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllTrabajadorAsignatura();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorAsignaturaByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorAsignaturaByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAnticipo(Anticipo anticipo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addAnticipo(anticipo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAnticipo(Anticipo anticipo) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updateAnticipo(anticipo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAnticipo(Anticipo anticipo) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deleteAnticipo(anticipo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Anticipo findAnticipoById(long anticipoId) throws Exception
  {
    try { this.txn.open();
      Anticipo anticipo = 
        this.trabajadorBusiness.findAnticipoById(anticipoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(anticipo);
      return anticipo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAnticipo() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllAnticipo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAnticipoByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findAnticipoByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCajaAhorro(CajaAhorro cajaAhorro)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addCajaAhorro(cajaAhorro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCajaAhorro(CajaAhorro cajaAhorro) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updateCajaAhorro(cajaAhorro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCajaAhorro(CajaAhorro cajaAhorro) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deleteCajaAhorro(cajaAhorro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CajaAhorro findCajaAhorroById(long cajaAhorroId) throws Exception
  {
    try { this.txn.open();
      CajaAhorro cajaAhorro = 
        this.trabajadorBusiness.findCajaAhorroById(cajaAhorroId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(cajaAhorro);
      return cajaAhorro;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCajaAhorroByTrabajador(long idTrabajador) throws Exception
  {
    try {
      this.txn.open();
      return this.trabajadorBusiness.findCajaAhorroByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCajaAhorro()
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findAllCajaAhorro();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoFijo(ConceptoFijo conceptoFijo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addConceptoFijo(conceptoFijo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoFijo(ConceptoFijo conceptoFijo) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updateConceptoFijo(conceptoFijo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoFijo(ConceptoFijo conceptoFijo) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deleteConceptoFijo(conceptoFijo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoFijo findConceptoFijoById(long conceptoFijoId) throws Exception
  {
    try { this.txn.open();
      ConceptoFijo conceptoFijo = 
        this.trabajadorBusiness.findConceptoFijoById(conceptoFijoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoFijo);
      return conceptoFijo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoFijo() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllConceptoFijo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoFijoByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findConceptoFijoByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoVariable(ConceptoVariable conceptoVariable)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addConceptoVariable(conceptoVariable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoVariable(ConceptoVariable conceptoVariable) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updateConceptoVariable(conceptoVariable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoVariable(ConceptoVariable conceptoVariable) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deleteConceptoVariable(conceptoVariable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoVariable findConceptoVariableById(long conceptoVariableId) throws Exception
  {
    try { this.txn.open();
      ConceptoVariable conceptoVariable = 
        this.trabajadorBusiness.findConceptoVariableById(conceptoVariableId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoVariable);
      return conceptoVariable;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoVariable() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllConceptoVariable();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoVariableByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findConceptoVariableByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoLiquidacion(ConceptoLiquidacion conceptoLiquidacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addConceptoLiquidacion(conceptoLiquidacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoLiquidacion(ConceptoLiquidacion conceptoLiquidacion) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updateConceptoLiquidacion(conceptoLiquidacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoLiquidacion(ConceptoLiquidacion conceptoLiquidacion) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deleteConceptoLiquidacion(conceptoLiquidacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoLiquidacion findConceptoLiquidacionById(long conceptoLiquidacionId) throws Exception
  {
    try { this.txn.open();
      ConceptoLiquidacion conceptoLiquidacion = 
        this.trabajadorBusiness.findConceptoLiquidacionById(conceptoLiquidacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoLiquidacion);
      return conceptoLiquidacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoLiquidacion() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllConceptoLiquidacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoLiquidacionByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findConceptoLiquidacionByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEmbargo(Embargo embargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addEmbargo(embargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEmbargo(Embargo embargo) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updateEmbargo(embargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEmbargo(Embargo embargo) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deleteEmbargo(embargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Embargo findEmbargoById(long embargoId) throws Exception
  {
    try { this.txn.open();
      Embargo embargo = 
        this.trabajadorBusiness.findEmbargoById(embargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(embargo);
      return embargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEmbargo() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllEmbargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEmbargoByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findEmbargoByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEmbargoConcepto(EmbargoConcepto embargoConcepto)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addEmbargoConcepto(embargoConcepto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEmbargoConcepto(EmbargoConcepto embargoConcepto) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updateEmbargoConcepto(embargoConcepto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEmbargoConcepto(EmbargoConcepto embargoConcepto) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deleteEmbargoConcepto(embargoConcepto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public EmbargoConcepto findEmbargoConceptoById(long embargoConceptoId) throws Exception
  {
    try { this.txn.open();
      EmbargoConcepto embargoConcepto = 
        this.trabajadorBusiness.findEmbargoConceptoById(embargoConceptoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(embargoConcepto);
      return embargoConcepto;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEmbargoConcepto() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllEmbargoConcepto();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEmbargoConceptoByEmbargo(long idEmbargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findEmbargoConceptoByEmbargo(idEmbargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEmbargoConceptoByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findEmbargoConceptoByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPlanillaArc(PlanillaArc planillaArc)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addPlanillaArc(planillaArc);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePlanillaArc(PlanillaArc planillaArc) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updatePlanillaArc(planillaArc);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePlanillaArc(PlanillaArc planillaArc) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deletePlanillaArc(planillaArc);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PlanillaArc findPlanillaArcById(long planillaArcId) throws Exception
  {
    try { this.txn.open();
      PlanillaArc planillaArc = 
        this.trabajadorBusiness.findPlanillaArcById(planillaArcId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(planillaArc);
      return planillaArc;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPlanillaArc() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllPlanillaArc();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPlanillaArcByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findPlanillaArcByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPlanillaAri(PlanillaAri planillaAri)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addPlanillaAri(planillaAri);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePlanillaAri(PlanillaAri planillaAri) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updatePlanillaAri(planillaAri);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePlanillaAri(PlanillaAri planillaAri) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deletePlanillaAri(planillaAri);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PlanillaAri findPlanillaAriById(long planillaAriId) throws Exception
  {
    try { this.txn.open();
      PlanillaAri planillaAri = 
        this.trabajadorBusiness.findPlanillaAriById(planillaAriId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(planillaAri);
      return planillaAri;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPlanillaAri() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllPlanillaAri();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPlanillaAriByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findPlanillaAriByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPrestamo(Prestamo prestamo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addPrestamo(prestamo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePrestamo(Prestamo prestamo) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updatePrestamo(prestamo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePrestamo(Prestamo prestamo) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deletePrestamo(prestamo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Prestamo findPrestamoById(long prestamoId) throws Exception
  {
    try { this.txn.open();
      Prestamo prestamo = 
        this.trabajadorBusiness.findPrestamoById(prestamoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(prestamo);
      return prestamo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPrestamo() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllPrestamo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPrestamoByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findPrestamoByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSueldoPromedio(SueldoPromedio sueldoPromedio)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addSueldoPromedio(sueldoPromedio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSueldoPromedio(SueldoPromedio sueldoPromedio) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updateSueldoPromedio(sueldoPromedio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSueldoPromedio(SueldoPromedio sueldoPromedio) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deleteSueldoPromedio(sueldoPromedio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SueldoPromedio findSueldoPromedioById(long sueldoPromedioId) throws Exception
  {
    try { this.txn.open();
      SueldoPromedio sueldoPromedio = 
        this.trabajadorBusiness.findSueldoPromedioById(sueldoPromedioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(sueldoPromedio);
      return sueldoPromedio;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSueldoPromedio() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllSueldoPromedio();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSueldoPromedioByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findSueldoPromedioByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSueldoPromedioByTiempoSitp(Date tiempoSitp)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findSueldoPromedioByTiempoSitp(tiempoSitp);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTrabajador(Trabajador trabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.trabajadorBusiness.addTrabajador(trabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTrabajador(Trabajador trabajador) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.updateTrabajador(trabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTrabajador(Trabajador trabajador) throws Exception
  {
    try { this.txn.open();
      this.trabajadorBusiness.deleteTrabajador(trabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Trabajador findTrabajadorById(long trabajadorId) throws Exception
  {
    try { this.txn.open();
      Trabajador trabajador = 
        this.trabajadorBusiness.findTrabajadorById(trabajadorId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(trabajador);
      return trabajador;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTrabajador() throws Exception
  {
    try { this.txn.open();
      return this.trabajadorBusiness.findAllTrabajador();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByPersonal(idPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByTipoPersonal(long idTipoPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByTipoPersonal(idTipoPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findTrabajadorByNombresApellidos(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByNombresApellidos(
        primerNombre, segundoNombre, primerApellido, segundoApellido, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByCedula(int cedula, long idOrganismo) throws Exception
  {
    try {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByCedula(
        cedula, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByCodigoNomina(long idTipoPersonal, int codigoNomina) throws Exception
  {
    try {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByCodigoNomina(
        idTipoPersonal, codigoNomina);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findTrabajadorByCedula(int cedula, long idOrganismo, long idUsuario, String administrador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByCedula(
        cedula, idOrganismo, idUsuario, administrador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByCedula(int cedula, long idOrganismo, long idUsuario, String administrador, String estatus) throws Exception
  {
    try {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByCedula(
        cedula, idOrganismo, idUsuario, administrador, estatus);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(long idTipoPersonal, int codigoNomina, String estatus) throws Exception
  {
    try {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(
        idTipoPersonal, codigoNomina, estatus);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByCedulaAndTipoPersonalAndEstatus(int cedula, long idTipoPersonal, String estatus) throws Exception
  {
    try {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByCedulaAndTipoPersonalAndEstatus(
        cedula, idTipoPersonal, estatus);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findTrabajadorByCedulaAndTipoPersonal(int cedula, long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByCedulaAndTipoPersonal(
        cedula, idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByNombresApellidosAndTipoPersonal(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByNombresApellidosAndTipoPersonal(
        primerNombre, segundoNombre, primerApellido, segundoApellido, idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByCedulaAndIdTipoPersonal(int cedula, long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByCedulaAndIdTipoPersonal(
        cedula, idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByNombresApellidosAndIdTipoPersonal(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByNombresApellidosAndIdTipoPersonal(
        primerNombre, segundoNombre, primerApellido, segundoApellido, idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByNombresApellidosAndClasificacion(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo, long idClasificacionPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByNombresApellidosAndClasificacion(
        primerNombre, segundoNombre, primerApellido, segundoApellido, idOrganismo, idClasificacionPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByCedulaAndClasificacion(int cedula, long idOrganismo, long idClasificacionPersonal) throws Exception
  {
    try {
      this.txn.open();
      return this.trabajadorBusiness.findTrabajadorByCedulaAndClasificacion(
        cedula, idOrganismo, idClasificacionPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}