package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.expediente.Personal;

public class Anticipo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  protected static final Map LISTA_PAGO;
  protected static final Map LISTA_ESTATUS;
  private long idAnticipo;
  private TipoPersonal tipoPersonal;
  private int anio;
  private int mes;
  private Date fechaAnticipo;
  private double montoAnticipo;
  private String tipoAnticipo;
  private String formaAnticipo;
  private String estatus;
  private String observaciones;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "estatus", "fechaAnticipo", "formaAnticipo", "idAnticipo", "idSitp", "mes", "montoAnticipo", "observaciones", "personal", "tiempoSitp", "tipoAnticipo", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 21, 21, 21, 26, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.Anticipo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Anticipo());

    LISTA_TIPO = 
      new LinkedHashMap();
    LISTA_PAGO = 
      new LinkedHashMap();
    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_TIPO.put("N", "NUEVO REGIMEN");
    LISTA_TIPO.put("V", "VIEJO REGIMEN");
    LISTA_TIPO.put("I", "INTERESES NUEVO REGIMEN");
    LISTA_TIPO.put("F", "INTERESES VIEJO REGIMEN");
    LISTA_TIPO.put("B", "BONO VACACIONAL");
    LISTA_TIPO.put("U", "BONO FIN DE AÑO");
    LISTA_PAGO.put("P", "PAGO DIRECTO");
    LISTA_PAGO.put("B", "BONO DEUDA PUBLICA");
    LISTA_PAGO.put("O", "OTRO INSTRUMENTO");
    LISTA_ESTATUS.put("S", "SOLICITADO");
    LISTA_ESTATUS.put("A", "APROBADO");
    LISTA_ESTATUS.put("R", "RECHAZADO");
  }

  public Anticipo()
  {
    jdoSetanio(this, 0);

    jdoSetmes(this, 0);

    jdoSetmontoAnticipo(this, 0.0D);

    jdoSettipoAnticipo(this, "N");
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmontoAnticipo(this));

    return new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaAnticipo(this)) + " - " + 
      LISTA_TIPO.get(jdoGettipoAnticipo(this)) + " - " + a;
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public void setAnio(int anio)
  {
    jdoSetanio(this, anio);
  }

  public Date getFechaAnticipo()
  {
    return jdoGetfechaAnticipo(this);
  }

  public void setFechaAnticipo(Date fechaAnticipo)
  {
    jdoSetfechaAnticipo(this, fechaAnticipo);
  }

  public String getFormaAnticipo()
  {
    return jdoGetformaAnticipo(this);
  }

  public void setFormaAnticipo(String formaAnticipo)
  {
    jdoSetformaAnticipo(this, formaAnticipo);
  }

  public long getIdAnticipo()
  {
    return jdoGetidAnticipo(this);
  }

  public void setIdAnticipo(long idAnticipo)
  {
    jdoSetidAnticipo(this, idAnticipo);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public void setMes(int mes)
  {
    jdoSetmes(this, mes);
  }

  public double getMontoAnticipo()
  {
    return jdoGetmontoAnticipo(this);
  }

  public void setMontoAnticipo(double montoAnticipo)
  {
    jdoSetmontoAnticipo(this, montoAnticipo);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public String getTipoAnticipo()
  {
    return jdoGettipoAnticipo(this);
  }

  public void setTipoAnticipo(String tipoAnticipo)
  {
    jdoSettipoAnticipo(this, tipoAnticipo);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones)
  {
    jdoSetobservaciones(this, observaciones);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Anticipo localAnticipo = new Anticipo();
    localAnticipo.jdoFlags = 1;
    localAnticipo.jdoStateManager = paramStateManager;
    return localAnticipo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Anticipo localAnticipo = new Anticipo();
    localAnticipo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAnticipo.jdoFlags = 1;
    localAnticipo.jdoStateManager = paramStateManager;
    return localAnticipo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaAnticipo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.formaAnticipo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAnticipo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAnticipo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoAnticipo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaAnticipo = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.formaAnticipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAnticipo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAnticipo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoAnticipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Anticipo paramAnticipo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramAnticipo.anio;
      return;
    case 1:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramAnticipo.estatus;
      return;
    case 2:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaAnticipo = paramAnticipo.fechaAnticipo;
      return;
    case 3:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.formaAnticipo = paramAnticipo.formaAnticipo;
      return;
    case 4:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.idAnticipo = paramAnticipo.idAnticipo;
      return;
    case 5:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramAnticipo.idSitp;
      return;
    case 6:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramAnticipo.mes;
      return;
    case 7:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.montoAnticipo = paramAnticipo.montoAnticipo;
      return;
    case 8:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramAnticipo.observaciones;
      return;
    case 9:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramAnticipo.personal;
      return;
    case 10:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramAnticipo.tiempoSitp;
      return;
    case 11:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.tipoAnticipo = paramAnticipo.tipoAnticipo;
      return;
    case 12:
      if (paramAnticipo == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramAnticipo.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Anticipo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Anticipo localAnticipo = (Anticipo)paramObject;
    if (localAnticipo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAnticipo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AnticipoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AnticipoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AnticipoPK))
      throw new IllegalArgumentException("arg1");
    AnticipoPK localAnticipoPK = (AnticipoPK)paramObject;
    localAnticipoPK.idAnticipo = this.idAnticipo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AnticipoPK))
      throw new IllegalArgumentException("arg1");
    AnticipoPK localAnticipoPK = (AnticipoPK)paramObject;
    this.idAnticipo = localAnticipoPK.idAnticipo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AnticipoPK))
      throw new IllegalArgumentException("arg2");
    AnticipoPK localAnticipoPK = (AnticipoPK)paramObject;
    localAnticipoPK.idAnticipo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AnticipoPK))
      throw new IllegalArgumentException("arg2");
    AnticipoPK localAnticipoPK = (AnticipoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localAnticipoPK.idAnticipo);
  }

  private static final int jdoGetanio(Anticipo paramAnticipo)
  {
    if (paramAnticipo.jdoFlags <= 0)
      return paramAnticipo.anio;
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.anio;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 0))
      return paramAnticipo.anio;
    return localStateManager.getIntField(paramAnticipo, jdoInheritedFieldCount + 0, paramAnticipo.anio);
  }

  private static final void jdoSetanio(Anticipo paramAnticipo, int paramInt)
  {
    if (paramAnticipo.jdoFlags == 0)
    {
      paramAnticipo.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramAnticipo, jdoInheritedFieldCount + 0, paramAnticipo.anio, paramInt);
  }

  private static final String jdoGetestatus(Anticipo paramAnticipo)
  {
    if (paramAnticipo.jdoFlags <= 0)
      return paramAnticipo.estatus;
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.estatus;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 1))
      return paramAnticipo.estatus;
    return localStateManager.getStringField(paramAnticipo, jdoInheritedFieldCount + 1, paramAnticipo.estatus);
  }

  private static final void jdoSetestatus(Anticipo paramAnticipo, String paramString)
  {
    if (paramAnticipo.jdoFlags == 0)
    {
      paramAnticipo.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramAnticipo, jdoInheritedFieldCount + 1, paramAnticipo.estatus, paramString);
  }

  private static final Date jdoGetfechaAnticipo(Anticipo paramAnticipo)
  {
    if (paramAnticipo.jdoFlags <= 0)
      return paramAnticipo.fechaAnticipo;
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.fechaAnticipo;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 2))
      return paramAnticipo.fechaAnticipo;
    return (Date)localStateManager.getObjectField(paramAnticipo, jdoInheritedFieldCount + 2, paramAnticipo.fechaAnticipo);
  }

  private static final void jdoSetfechaAnticipo(Anticipo paramAnticipo, Date paramDate)
  {
    if (paramAnticipo.jdoFlags == 0)
    {
      paramAnticipo.fechaAnticipo = paramDate;
      return;
    }
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.fechaAnticipo = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAnticipo, jdoInheritedFieldCount + 2, paramAnticipo.fechaAnticipo, paramDate);
  }

  private static final String jdoGetformaAnticipo(Anticipo paramAnticipo)
  {
    if (paramAnticipo.jdoFlags <= 0)
      return paramAnticipo.formaAnticipo;
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.formaAnticipo;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 3))
      return paramAnticipo.formaAnticipo;
    return localStateManager.getStringField(paramAnticipo, jdoInheritedFieldCount + 3, paramAnticipo.formaAnticipo);
  }

  private static final void jdoSetformaAnticipo(Anticipo paramAnticipo, String paramString)
  {
    if (paramAnticipo.jdoFlags == 0)
    {
      paramAnticipo.formaAnticipo = paramString;
      return;
    }
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.formaAnticipo = paramString;
      return;
    }
    localStateManager.setStringField(paramAnticipo, jdoInheritedFieldCount + 3, paramAnticipo.formaAnticipo, paramString);
  }

  private static final long jdoGetidAnticipo(Anticipo paramAnticipo)
  {
    return paramAnticipo.idAnticipo;
  }

  private static final void jdoSetidAnticipo(Anticipo paramAnticipo, long paramLong)
  {
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.idAnticipo = paramLong;
      return;
    }
    localStateManager.setLongField(paramAnticipo, jdoInheritedFieldCount + 4, paramAnticipo.idAnticipo, paramLong);
  }

  private static final int jdoGetidSitp(Anticipo paramAnticipo)
  {
    if (paramAnticipo.jdoFlags <= 0)
      return paramAnticipo.idSitp;
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.idSitp;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 5))
      return paramAnticipo.idSitp;
    return localStateManager.getIntField(paramAnticipo, jdoInheritedFieldCount + 5, paramAnticipo.idSitp);
  }

  private static final void jdoSetidSitp(Anticipo paramAnticipo, int paramInt)
  {
    if (paramAnticipo.jdoFlags == 0)
    {
      paramAnticipo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramAnticipo, jdoInheritedFieldCount + 5, paramAnticipo.idSitp, paramInt);
  }

  private static final int jdoGetmes(Anticipo paramAnticipo)
  {
    if (paramAnticipo.jdoFlags <= 0)
      return paramAnticipo.mes;
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.mes;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 6))
      return paramAnticipo.mes;
    return localStateManager.getIntField(paramAnticipo, jdoInheritedFieldCount + 6, paramAnticipo.mes);
  }

  private static final void jdoSetmes(Anticipo paramAnticipo, int paramInt)
  {
    if (paramAnticipo.jdoFlags == 0)
    {
      paramAnticipo.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramAnticipo, jdoInheritedFieldCount + 6, paramAnticipo.mes, paramInt);
  }

  private static final double jdoGetmontoAnticipo(Anticipo paramAnticipo)
  {
    if (paramAnticipo.jdoFlags <= 0)
      return paramAnticipo.montoAnticipo;
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.montoAnticipo;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 7))
      return paramAnticipo.montoAnticipo;
    return localStateManager.getDoubleField(paramAnticipo, jdoInheritedFieldCount + 7, paramAnticipo.montoAnticipo);
  }

  private static final void jdoSetmontoAnticipo(Anticipo paramAnticipo, double paramDouble)
  {
    if (paramAnticipo.jdoFlags == 0)
    {
      paramAnticipo.montoAnticipo = paramDouble;
      return;
    }
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.montoAnticipo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAnticipo, jdoInheritedFieldCount + 7, paramAnticipo.montoAnticipo, paramDouble);
  }

  private static final String jdoGetobservaciones(Anticipo paramAnticipo)
  {
    if (paramAnticipo.jdoFlags <= 0)
      return paramAnticipo.observaciones;
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.observaciones;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 8))
      return paramAnticipo.observaciones;
    return localStateManager.getStringField(paramAnticipo, jdoInheritedFieldCount + 8, paramAnticipo.observaciones);
  }

  private static final void jdoSetobservaciones(Anticipo paramAnticipo, String paramString)
  {
    if (paramAnticipo.jdoFlags == 0)
    {
      paramAnticipo.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramAnticipo, jdoInheritedFieldCount + 8, paramAnticipo.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Anticipo paramAnticipo)
  {
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.personal;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 9))
      return paramAnticipo.personal;
    return (Personal)localStateManager.getObjectField(paramAnticipo, jdoInheritedFieldCount + 9, paramAnticipo.personal);
  }

  private static final void jdoSetpersonal(Anticipo paramAnticipo, Personal paramPersonal)
  {
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramAnticipo, jdoInheritedFieldCount + 9, paramAnticipo.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(Anticipo paramAnticipo)
  {
    if (paramAnticipo.jdoFlags <= 0)
      return paramAnticipo.tiempoSitp;
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.tiempoSitp;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 10))
      return paramAnticipo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramAnticipo, jdoInheritedFieldCount + 10, paramAnticipo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Anticipo paramAnticipo, Date paramDate)
  {
    if (paramAnticipo.jdoFlags == 0)
    {
      paramAnticipo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAnticipo, jdoInheritedFieldCount + 10, paramAnticipo.tiempoSitp, paramDate);
  }

  private static final String jdoGettipoAnticipo(Anticipo paramAnticipo)
  {
    if (paramAnticipo.jdoFlags <= 0)
      return paramAnticipo.tipoAnticipo;
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.tipoAnticipo;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 11))
      return paramAnticipo.tipoAnticipo;
    return localStateManager.getStringField(paramAnticipo, jdoInheritedFieldCount + 11, paramAnticipo.tipoAnticipo);
  }

  private static final void jdoSettipoAnticipo(Anticipo paramAnticipo, String paramString)
  {
    if (paramAnticipo.jdoFlags == 0)
    {
      paramAnticipo.tipoAnticipo = paramString;
      return;
    }
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.tipoAnticipo = paramString;
      return;
    }
    localStateManager.setStringField(paramAnticipo, jdoInheritedFieldCount + 11, paramAnticipo.tipoAnticipo, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(Anticipo paramAnticipo)
  {
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
      return paramAnticipo.tipoPersonal;
    if (localStateManager.isLoaded(paramAnticipo, jdoInheritedFieldCount + 12))
      return paramAnticipo.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramAnticipo, jdoInheritedFieldCount + 12, paramAnticipo.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(Anticipo paramAnticipo, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramAnticipo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAnticipo.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramAnticipo, jdoInheritedFieldCount + 12, paramAnticipo.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}