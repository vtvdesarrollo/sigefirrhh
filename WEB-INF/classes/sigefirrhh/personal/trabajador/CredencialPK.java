package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class CredencialPK
  implements Serializable
{
  public long idCredencial;

  public CredencialPK()
  {
  }

  public CredencialPK(long idCredencial)
  {
    this.idCredencial = idCredencial;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CredencialPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CredencialPK thatPK)
  {
    return 
      this.idCredencial == thatPK.idCredencial;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCredencial)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCredencial);
  }
}