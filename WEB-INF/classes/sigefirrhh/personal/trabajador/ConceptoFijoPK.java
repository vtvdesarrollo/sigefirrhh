package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class ConceptoFijoPK
  implements Serializable
{
  public long idConceptoFijo;

  public ConceptoFijoPK()
  {
  }

  public ConceptoFijoPK(long idConceptoFijo)
  {
    this.idConceptoFijo = idConceptoFijo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoFijoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoFijoPK thatPK)
  {
    return 
      this.idConceptoFijo == thatPK.idConceptoFijo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoFijo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoFijo);
  }
}