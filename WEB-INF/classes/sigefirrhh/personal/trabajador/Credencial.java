package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.SubtipoCredencial;
import sigefirrhh.personal.expediente.Personal;

public class Credencial
  implements Serializable, PersistenceCapable
{
  private long idCredencial;
  private String numero;
  private SubtipoCredencial subTipoCredencial;
  private Date fechaEntrega;
  private Date fechaRetiro;
  private String color;
  private String observaciones;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "color", "fechaEntrega", "fechaRetiro", "idCredencial", "numero", "observaciones", "personal", "subTipoCredencial" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.bienestar.SubtipoCredencial") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetsubTipoCredencial(this).getNombre() + " - " + "Nro. " + jdoGetnumero(this) + "--Entregado: " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaEntrega(this));
  }

  public String getColor()
  {
    return jdoGetcolor(this);
  }
  public void setColor(String color) {
    jdoSetcolor(this, color);
  }
  public Date getFechaEntrega() {
    return jdoGetfechaEntrega(this);
  }
  public void setFechaEntrega(Date fechaEntrega) {
    jdoSetfechaEntrega(this, fechaEntrega);
  }
  public Date getFechaRetiro() {
    return jdoGetfechaRetiro(this);
  }
  public void setFechaRetiro(Date fechaRetiro) {
    jdoSetfechaRetiro(this, fechaRetiro);
  }
  public long getIdCredencial() {
    return jdoGetidCredencial(this);
  }
  public void setIdCredencial(long idCredencial) {
    jdoSetidCredencial(this, idCredencial);
  }
  public String getNumero() {
    return jdoGetnumero(this);
  }
  public void setNumero(String numero) {
    jdoSetnumero(this, numero);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }
  public SubtipoCredencial getSubTipoCredencial() {
    return jdoGetsubTipoCredencial(this);
  }
  public void setSubTipoCredencial(SubtipoCredencial subTipoCredencial) {
    jdoSetsubTipoCredencial(this, subTipoCredencial);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.Credencial"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Credencial());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Credencial localCredencial = new Credencial();
    localCredencial.jdoFlags = 1;
    localCredencial.jdoStateManager = paramStateManager;
    return localCredencial;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Credencial localCredencial = new Credencial();
    localCredencial.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCredencial.jdoFlags = 1;
    localCredencial.jdoStateManager = paramStateManager;
    return localCredencial;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.color);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEntrega);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRetiro);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCredencial);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numero);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.subTipoCredencial);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.color = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEntrega = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRetiro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCredencial = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numero = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.subTipoCredencial = ((SubtipoCredencial)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Credencial paramCredencial, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.color = paramCredencial.color;
      return;
    case 1:
      if (paramCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEntrega = paramCredencial.fechaEntrega;
      return;
    case 2:
      if (paramCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRetiro = paramCredencial.fechaRetiro;
      return;
    case 3:
      if (paramCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.idCredencial = paramCredencial.idCredencial;
      return;
    case 4:
      if (paramCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.numero = paramCredencial.numero;
      return;
    case 5:
      if (paramCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramCredencial.observaciones;
      return;
    case 6:
      if (paramCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramCredencial.personal;
      return;
    case 7:
      if (paramCredencial == null)
        throw new IllegalArgumentException("arg1");
      this.subTipoCredencial = paramCredencial.subTipoCredencial;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Credencial))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Credencial localCredencial = (Credencial)paramObject;
    if (localCredencial.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCredencial, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CredencialPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CredencialPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CredencialPK))
      throw new IllegalArgumentException("arg1");
    CredencialPK localCredencialPK = (CredencialPK)paramObject;
    localCredencialPK.idCredencial = this.idCredencial;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CredencialPK))
      throw new IllegalArgumentException("arg1");
    CredencialPK localCredencialPK = (CredencialPK)paramObject;
    this.idCredencial = localCredencialPK.idCredencial;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CredencialPK))
      throw new IllegalArgumentException("arg2");
    CredencialPK localCredencialPK = (CredencialPK)paramObject;
    localCredencialPK.idCredencial = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CredencialPK))
      throw new IllegalArgumentException("arg2");
    CredencialPK localCredencialPK = (CredencialPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localCredencialPK.idCredencial);
  }

  private static final String jdoGetcolor(Credencial paramCredencial)
  {
    if (paramCredencial.jdoFlags <= 0)
      return paramCredencial.color;
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramCredencial.color;
    if (localStateManager.isLoaded(paramCredencial, jdoInheritedFieldCount + 0))
      return paramCredencial.color;
    return localStateManager.getStringField(paramCredencial, jdoInheritedFieldCount + 0, paramCredencial.color);
  }

  private static final void jdoSetcolor(Credencial paramCredencial, String paramString)
  {
    if (paramCredencial.jdoFlags == 0)
    {
      paramCredencial.color = paramString;
      return;
    }
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramCredencial.color = paramString;
      return;
    }
    localStateManager.setStringField(paramCredencial, jdoInheritedFieldCount + 0, paramCredencial.color, paramString);
  }

  private static final Date jdoGetfechaEntrega(Credencial paramCredencial)
  {
    if (paramCredencial.jdoFlags <= 0)
      return paramCredencial.fechaEntrega;
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramCredencial.fechaEntrega;
    if (localStateManager.isLoaded(paramCredencial, jdoInheritedFieldCount + 1))
      return paramCredencial.fechaEntrega;
    return (Date)localStateManager.getObjectField(paramCredencial, jdoInheritedFieldCount + 1, paramCredencial.fechaEntrega);
  }

  private static final void jdoSetfechaEntrega(Credencial paramCredencial, Date paramDate)
  {
    if (paramCredencial.jdoFlags == 0)
    {
      paramCredencial.fechaEntrega = paramDate;
      return;
    }
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramCredencial.fechaEntrega = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCredencial, jdoInheritedFieldCount + 1, paramCredencial.fechaEntrega, paramDate);
  }

  private static final Date jdoGetfechaRetiro(Credencial paramCredencial)
  {
    if (paramCredencial.jdoFlags <= 0)
      return paramCredencial.fechaRetiro;
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramCredencial.fechaRetiro;
    if (localStateManager.isLoaded(paramCredencial, jdoInheritedFieldCount + 2))
      return paramCredencial.fechaRetiro;
    return (Date)localStateManager.getObjectField(paramCredencial, jdoInheritedFieldCount + 2, paramCredencial.fechaRetiro);
  }

  private static final void jdoSetfechaRetiro(Credencial paramCredencial, Date paramDate)
  {
    if (paramCredencial.jdoFlags == 0)
    {
      paramCredencial.fechaRetiro = paramDate;
      return;
    }
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramCredencial.fechaRetiro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCredencial, jdoInheritedFieldCount + 2, paramCredencial.fechaRetiro, paramDate);
  }

  private static final long jdoGetidCredencial(Credencial paramCredencial)
  {
    return paramCredencial.idCredencial;
  }

  private static final void jdoSetidCredencial(Credencial paramCredencial, long paramLong)
  {
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramCredencial.idCredencial = paramLong;
      return;
    }
    localStateManager.setLongField(paramCredencial, jdoInheritedFieldCount + 3, paramCredencial.idCredencial, paramLong);
  }

  private static final String jdoGetnumero(Credencial paramCredencial)
  {
    if (paramCredencial.jdoFlags <= 0)
      return paramCredencial.numero;
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramCredencial.numero;
    if (localStateManager.isLoaded(paramCredencial, jdoInheritedFieldCount + 4))
      return paramCredencial.numero;
    return localStateManager.getStringField(paramCredencial, jdoInheritedFieldCount + 4, paramCredencial.numero);
  }

  private static final void jdoSetnumero(Credencial paramCredencial, String paramString)
  {
    if (paramCredencial.jdoFlags == 0)
    {
      paramCredencial.numero = paramString;
      return;
    }
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramCredencial.numero = paramString;
      return;
    }
    localStateManager.setStringField(paramCredencial, jdoInheritedFieldCount + 4, paramCredencial.numero, paramString);
  }

  private static final String jdoGetobservaciones(Credencial paramCredencial)
  {
    if (paramCredencial.jdoFlags <= 0)
      return paramCredencial.observaciones;
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramCredencial.observaciones;
    if (localStateManager.isLoaded(paramCredencial, jdoInheritedFieldCount + 5))
      return paramCredencial.observaciones;
    return localStateManager.getStringField(paramCredencial, jdoInheritedFieldCount + 5, paramCredencial.observaciones);
  }

  private static final void jdoSetobservaciones(Credencial paramCredencial, String paramString)
  {
    if (paramCredencial.jdoFlags == 0)
    {
      paramCredencial.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramCredencial.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramCredencial, jdoInheritedFieldCount + 5, paramCredencial.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Credencial paramCredencial)
  {
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramCredencial.personal;
    if (localStateManager.isLoaded(paramCredencial, jdoInheritedFieldCount + 6))
      return paramCredencial.personal;
    return (Personal)localStateManager.getObjectField(paramCredencial, jdoInheritedFieldCount + 6, paramCredencial.personal);
  }

  private static final void jdoSetpersonal(Credencial paramCredencial, Personal paramPersonal)
  {
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramCredencial.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramCredencial, jdoInheritedFieldCount + 6, paramCredencial.personal, paramPersonal);
  }

  private static final SubtipoCredencial jdoGetsubTipoCredencial(Credencial paramCredencial)
  {
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
      return paramCredencial.subTipoCredencial;
    if (localStateManager.isLoaded(paramCredencial, jdoInheritedFieldCount + 7))
      return paramCredencial.subTipoCredencial;
    return (SubtipoCredencial)localStateManager.getObjectField(paramCredencial, jdoInheritedFieldCount + 7, paramCredencial.subTipoCredencial);
  }

  private static final void jdoSetsubTipoCredencial(Credencial paramCredencial, SubtipoCredencial paramSubtipoCredencial)
  {
    StateManager localStateManager = paramCredencial.jdoStateManager;
    if (localStateManager == null)
    {
      paramCredencial.subTipoCredencial = paramSubtipoCredencial;
      return;
    }
    localStateManager.setObjectField(paramCredencial, jdoInheritedFieldCount + 7, paramCredencial.subTipoCredencial, paramSubtipoCredencial);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}