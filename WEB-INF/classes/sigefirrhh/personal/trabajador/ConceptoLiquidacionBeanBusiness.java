package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonalBeanBusiness;

public class ConceptoLiquidacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoLiquidacion(ConceptoLiquidacion conceptoLiquidacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoLiquidacion conceptoLiquidacionNew = 
      (ConceptoLiquidacion)BeanUtils.cloneBean(
      conceptoLiquidacion);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoLiquidacionNew.getConceptoTipoPersonal() != null) {
      conceptoLiquidacionNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoLiquidacionNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (conceptoLiquidacionNew.getFrecuenciaTipoPersonal() != null) {
      conceptoLiquidacionNew.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        conceptoLiquidacionNew.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (conceptoLiquidacionNew.getTrabajador() != null) {
      conceptoLiquidacionNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        conceptoLiquidacionNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(conceptoLiquidacionNew);
  }

  public void updateConceptoLiquidacion(ConceptoLiquidacion conceptoLiquidacion) throws Exception
  {
    ConceptoLiquidacion conceptoLiquidacionModify = 
      findConceptoLiquidacionById(conceptoLiquidacion.getIdConceptoLiquidacion());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoLiquidacion.getConceptoTipoPersonal() != null) {
      conceptoLiquidacion.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoLiquidacion.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (conceptoLiquidacion.getFrecuenciaTipoPersonal() != null) {
      conceptoLiquidacion.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        conceptoLiquidacion.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (conceptoLiquidacion.getTrabajador() != null) {
      conceptoLiquidacion.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        conceptoLiquidacion.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(conceptoLiquidacionModify, conceptoLiquidacion);
  }

  public void deleteConceptoLiquidacion(ConceptoLiquidacion conceptoLiquidacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoLiquidacion conceptoLiquidacionDelete = 
      findConceptoLiquidacionById(conceptoLiquidacion.getIdConceptoLiquidacion());
    pm.deletePersistent(conceptoLiquidacionDelete);
  }

  public ConceptoLiquidacion findConceptoLiquidacionById(long idConceptoLiquidacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoLiquidacion == pIdConceptoLiquidacion";
    Query query = pm.newQuery(ConceptoLiquidacion.class, filter);

    query.declareParameters("long pIdConceptoLiquidacion");

    parameters.put("pIdConceptoLiquidacion", new Long(idConceptoLiquidacion));

    Collection colConceptoLiquidacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoLiquidacion.iterator();
    return (ConceptoLiquidacion)iterator.next();
  }

  public Collection findConceptoLiquidacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoLiquidacionExtent = pm.getExtent(
      ConceptoLiquidacion.class, true);
    Query query = pm.newQuery(conceptoLiquidacionExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(ConceptoLiquidacion.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoLiquidacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoLiquidacion);

    return colConceptoLiquidacion;
  }

  public ConceptoLiquidacion findByTrabajador(long idFrecuenciaTipoPersonal, long idConceptoTipoPersonal, long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    ConceptoLiquidacion conLiq = null;

    String filter = "frecuenciatipopersonal.idFrecuenciaTipoPersonal == pIdFrecuenciaTipoPersonal && conceptotipopersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal && trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(ConceptoLiquidacion.class, filter);

    query.declareParameters("long pIdFrecuenciaTipoPersonal, long pIdConceptoTipoPersonal, long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdFrecuenciaTipoPersonal", new Long(idFrecuenciaTipoPersonal));
    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));
    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoLiquidacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));
    conLiq = (ConceptoLiquidacion)colConceptoLiquidacion.iterator().next();

    pm.makeTransientAll(colConceptoLiquidacion);

    return conLiq;
  }
}