package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class ConceptoLiquidacionPK
  implements Serializable
{
  public long idConceptoLiquidacion;

  public ConceptoLiquidacionPK()
  {
  }

  public ConceptoLiquidacionPK(long idConceptoLiquidacion)
  {
    this.idConceptoLiquidacion = idConceptoLiquidacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoLiquidacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoLiquidacionPK thatPK)
  {
    return 
      this.idConceptoLiquidacion == thatPK.idConceptoLiquidacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoLiquidacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoLiquidacion);
  }
}