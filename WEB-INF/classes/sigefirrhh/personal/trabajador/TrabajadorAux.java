package sigefirrhh.personal.trabajador;

import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TrabajadorAux
  implements PersistenceCapable
{
  private long idTrabajador;
  private String tipoPersonal;
  private int cedula;
  private String estatus;
  private int codigoNomina;
  private String primerNombre;
  private String segundoNombre;
  private String primerApellido;
  private String segundoApellido;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cedula", "codigoNomina", "estatus", "idTrabajador", "primerApellido", "primerNombre", "segundoApellido", "segundoNombre", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return 
      jdoGetcedula(this) + "  -  " + 
      jdoGetprimerApellido(this) + " " + (
      jdoGetsegundoApellido(this) == null ? "" : jdoGetsegundoApellido(this)) + ", " + 
      jdoGetprimerNombre(this) + " " + (
      jdoGetsegundoNombre(this) == null ? "" : jdoGetsegundoNombre(this)) + " - " + 
      jdoGettipoPersonal(this) + " - " + 
      jdoGetcodigoNomina(this) + " - " + 
      jdoGetestatus(this);
  }

  public int getCedula()
  {
    return jdoGetcedula(this);
  }
  public void setCedula(int cedula) {
    jdoSetcedula(this, cedula);
  }
  public int getCodigoNomina() {
    return jdoGetcodigoNomina(this);
  }
  public void setCodigoNomina(int codigoNomina) {
    jdoSetcodigoNomina(this, codigoNomina);
  }
  public String getEstatus() {
    return jdoGetestatus(this);
  }
  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }
  public long getIdTrabajador() {
    return jdoGetidTrabajador(this);
  }
  public void setIdTrabajador(long idTrabajador) {
    jdoSetidTrabajador(this, idTrabajador);
  }
  public String getPrimerApellido() {
    return jdoGetprimerApellido(this);
  }
  public void setPrimerApellido(String primerApellido) {
    jdoSetprimerApellido(this, primerApellido);
  }
  public String getPrimerNombre() {
    return jdoGetprimerNombre(this);
  }
  public void setPrimerNombre(String primerNombre) {
    jdoSetprimerNombre(this, primerNombre);
  }
  public String getSegundoApellido() {
    return jdoGetsegundoApellido(this);
  }
  public void setSegundoApellido(String segundoApellido) {
    jdoSetsegundoApellido(this, segundoApellido);
  }
  public String getSegundoNombre() {
    return jdoGetsegundoNombre(this);
  }
  public void setSegundoNombre(String segundoNombre) {
    jdoSetsegundoNombre(this, segundoNombre);
  }
  public String getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(String tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.TrabajadorAux"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TrabajadorAux());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TrabajadorAux localTrabajadorAux = new TrabajadorAux();
    localTrabajadorAux.jdoFlags = 1;
    localTrabajadorAux.jdoStateManager = paramStateManager;
    return localTrabajadorAux;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TrabajadorAux localTrabajadorAux = new TrabajadorAux();
    localTrabajadorAux.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTrabajadorAux.jdoFlags = 1;
    localTrabajadorAux.jdoStateManager = paramStateManager;
    return localTrabajadorAux;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNomina);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTrabajador);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerApellido);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerNombre);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoApellido);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoNombre);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTrabajador = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TrabajadorAux paramTrabajadorAux, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTrabajadorAux == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramTrabajadorAux.cedula;
      return;
    case 1:
      if (paramTrabajadorAux == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNomina = paramTrabajadorAux.codigoNomina;
      return;
    case 2:
      if (paramTrabajadorAux == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramTrabajadorAux.estatus;
      return;
    case 3:
      if (paramTrabajadorAux == null)
        throw new IllegalArgumentException("arg1");
      this.idTrabajador = paramTrabajadorAux.idTrabajador;
      return;
    case 4:
      if (paramTrabajadorAux == null)
        throw new IllegalArgumentException("arg1");
      this.primerApellido = paramTrabajadorAux.primerApellido;
      return;
    case 5:
      if (paramTrabajadorAux == null)
        throw new IllegalArgumentException("arg1");
      this.primerNombre = paramTrabajadorAux.primerNombre;
      return;
    case 6:
      if (paramTrabajadorAux == null)
        throw new IllegalArgumentException("arg1");
      this.segundoApellido = paramTrabajadorAux.segundoApellido;
      return;
    case 7:
      if (paramTrabajadorAux == null)
        throw new IllegalArgumentException("arg1");
      this.segundoNombre = paramTrabajadorAux.segundoNombre;
      return;
    case 8:
      if (paramTrabajadorAux == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramTrabajadorAux.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TrabajadorAux))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TrabajadorAux localTrabajadorAux = (TrabajadorAux)paramObject;
    if (localTrabajadorAux.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTrabajadorAux, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TrabajadorAuxPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TrabajadorAuxPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrabajadorAuxPK))
      throw new IllegalArgumentException("arg1");
    TrabajadorAuxPK localTrabajadorAuxPK = (TrabajadorAuxPK)paramObject;
    localTrabajadorAuxPK.idTrabajador = this.idTrabajador;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrabajadorAuxPK))
      throw new IllegalArgumentException("arg1");
    TrabajadorAuxPK localTrabajadorAuxPK = (TrabajadorAuxPK)paramObject;
    this.idTrabajador = localTrabajadorAuxPK.idTrabajador;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrabajadorAuxPK))
      throw new IllegalArgumentException("arg2");
    TrabajadorAuxPK localTrabajadorAuxPK = (TrabajadorAuxPK)paramObject;
    localTrabajadorAuxPK.idTrabajador = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrabajadorAuxPK))
      throw new IllegalArgumentException("arg2");
    TrabajadorAuxPK localTrabajadorAuxPK = (TrabajadorAuxPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localTrabajadorAuxPK.idTrabajador);
  }

  private static final int jdoGetcedula(TrabajadorAux paramTrabajadorAux)
  {
    if (paramTrabajadorAux.jdoFlags <= 0)
      return paramTrabajadorAux.cedula;
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorAux.cedula;
    if (localStateManager.isLoaded(paramTrabajadorAux, jdoInheritedFieldCount + 0))
      return paramTrabajadorAux.cedula;
    return localStateManager.getIntField(paramTrabajadorAux, jdoInheritedFieldCount + 0, paramTrabajadorAux.cedula);
  }

  private static final void jdoSetcedula(TrabajadorAux paramTrabajadorAux, int paramInt)
  {
    if (paramTrabajadorAux.jdoFlags == 0)
    {
      paramTrabajadorAux.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAux.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajadorAux, jdoInheritedFieldCount + 0, paramTrabajadorAux.cedula, paramInt);
  }

  private static final int jdoGetcodigoNomina(TrabajadorAux paramTrabajadorAux)
  {
    if (paramTrabajadorAux.jdoFlags <= 0)
      return paramTrabajadorAux.codigoNomina;
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorAux.codigoNomina;
    if (localStateManager.isLoaded(paramTrabajadorAux, jdoInheritedFieldCount + 1))
      return paramTrabajadorAux.codigoNomina;
    return localStateManager.getIntField(paramTrabajadorAux, jdoInheritedFieldCount + 1, paramTrabajadorAux.codigoNomina);
  }

  private static final void jdoSetcodigoNomina(TrabajadorAux paramTrabajadorAux, int paramInt)
  {
    if (paramTrabajadorAux.jdoFlags == 0)
    {
      paramTrabajadorAux.codigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAux.codigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajadorAux, jdoInheritedFieldCount + 1, paramTrabajadorAux.codigoNomina, paramInt);
  }

  private static final String jdoGetestatus(TrabajadorAux paramTrabajadorAux)
  {
    if (paramTrabajadorAux.jdoFlags <= 0)
      return paramTrabajadorAux.estatus;
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorAux.estatus;
    if (localStateManager.isLoaded(paramTrabajadorAux, jdoInheritedFieldCount + 2))
      return paramTrabajadorAux.estatus;
    return localStateManager.getStringField(paramTrabajadorAux, jdoInheritedFieldCount + 2, paramTrabajadorAux.estatus);
  }

  private static final void jdoSetestatus(TrabajadorAux paramTrabajadorAux, String paramString)
  {
    if (paramTrabajadorAux.jdoFlags == 0)
    {
      paramTrabajadorAux.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAux.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajadorAux, jdoInheritedFieldCount + 2, paramTrabajadorAux.estatus, paramString);
  }

  private static final long jdoGetidTrabajador(TrabajadorAux paramTrabajadorAux)
  {
    return paramTrabajadorAux.idTrabajador;
  }

  private static final void jdoSetidTrabajador(TrabajadorAux paramTrabajadorAux, long paramLong)
  {
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAux.idTrabajador = paramLong;
      return;
    }
    localStateManager.setLongField(paramTrabajadorAux, jdoInheritedFieldCount + 3, paramTrabajadorAux.idTrabajador, paramLong);
  }

  private static final String jdoGetprimerApellido(TrabajadorAux paramTrabajadorAux)
  {
    if (paramTrabajadorAux.jdoFlags <= 0)
      return paramTrabajadorAux.primerApellido;
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorAux.primerApellido;
    if (localStateManager.isLoaded(paramTrabajadorAux, jdoInheritedFieldCount + 4))
      return paramTrabajadorAux.primerApellido;
    return localStateManager.getStringField(paramTrabajadorAux, jdoInheritedFieldCount + 4, paramTrabajadorAux.primerApellido);
  }

  private static final void jdoSetprimerApellido(TrabajadorAux paramTrabajadorAux, String paramString)
  {
    if (paramTrabajadorAux.jdoFlags == 0)
    {
      paramTrabajadorAux.primerApellido = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAux.primerApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajadorAux, jdoInheritedFieldCount + 4, paramTrabajadorAux.primerApellido, paramString);
  }

  private static final String jdoGetprimerNombre(TrabajadorAux paramTrabajadorAux)
  {
    if (paramTrabajadorAux.jdoFlags <= 0)
      return paramTrabajadorAux.primerNombre;
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorAux.primerNombre;
    if (localStateManager.isLoaded(paramTrabajadorAux, jdoInheritedFieldCount + 5))
      return paramTrabajadorAux.primerNombre;
    return localStateManager.getStringField(paramTrabajadorAux, jdoInheritedFieldCount + 5, paramTrabajadorAux.primerNombre);
  }

  private static final void jdoSetprimerNombre(TrabajadorAux paramTrabajadorAux, String paramString)
  {
    if (paramTrabajadorAux.jdoFlags == 0)
    {
      paramTrabajadorAux.primerNombre = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAux.primerNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajadorAux, jdoInheritedFieldCount + 5, paramTrabajadorAux.primerNombre, paramString);
  }

  private static final String jdoGetsegundoApellido(TrabajadorAux paramTrabajadorAux)
  {
    if (paramTrabajadorAux.jdoFlags <= 0)
      return paramTrabajadorAux.segundoApellido;
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorAux.segundoApellido;
    if (localStateManager.isLoaded(paramTrabajadorAux, jdoInheritedFieldCount + 6))
      return paramTrabajadorAux.segundoApellido;
    return localStateManager.getStringField(paramTrabajadorAux, jdoInheritedFieldCount + 6, paramTrabajadorAux.segundoApellido);
  }

  private static final void jdoSetsegundoApellido(TrabajadorAux paramTrabajadorAux, String paramString)
  {
    if (paramTrabajadorAux.jdoFlags == 0)
    {
      paramTrabajadorAux.segundoApellido = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAux.segundoApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajadorAux, jdoInheritedFieldCount + 6, paramTrabajadorAux.segundoApellido, paramString);
  }

  private static final String jdoGetsegundoNombre(TrabajadorAux paramTrabajadorAux)
  {
    if (paramTrabajadorAux.jdoFlags <= 0)
      return paramTrabajadorAux.segundoNombre;
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorAux.segundoNombre;
    if (localStateManager.isLoaded(paramTrabajadorAux, jdoInheritedFieldCount + 7))
      return paramTrabajadorAux.segundoNombre;
    return localStateManager.getStringField(paramTrabajadorAux, jdoInheritedFieldCount + 7, paramTrabajadorAux.segundoNombre);
  }

  private static final void jdoSetsegundoNombre(TrabajadorAux paramTrabajadorAux, String paramString)
  {
    if (paramTrabajadorAux.jdoFlags == 0)
    {
      paramTrabajadorAux.segundoNombre = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAux.segundoNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajadorAux, jdoInheritedFieldCount + 7, paramTrabajadorAux.segundoNombre, paramString);
  }

  private static final String jdoGettipoPersonal(TrabajadorAux paramTrabajadorAux)
  {
    if (paramTrabajadorAux.jdoFlags <= 0)
      return paramTrabajadorAux.tipoPersonal;
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorAux.tipoPersonal;
    if (localStateManager.isLoaded(paramTrabajadorAux, jdoInheritedFieldCount + 8))
      return paramTrabajadorAux.tipoPersonal;
    return localStateManager.getStringField(paramTrabajadorAux, jdoInheritedFieldCount + 8, paramTrabajadorAux.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(TrabajadorAux paramTrabajadorAux, String paramString)
  {
    if (paramTrabajadorAux.jdoFlags == 0)
    {
      paramTrabajadorAux.tipoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajadorAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAux.tipoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajadorAux, jdoInheritedFieldCount + 8, paramTrabajadorAux.tipoPersonal, paramString);
  }
}