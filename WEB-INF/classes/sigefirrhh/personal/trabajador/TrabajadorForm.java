package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.registroCargos.RegistroCargosFacade;

public class TrabajadorForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TrabajadorForm.class.getName());
  private Trabajador trabajador;
  private Collection result;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private RegistroCargosFacade registroCargosFacade = new RegistroCargosFacade();
  private CargoFacade cargoFacade = new CargoFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private Collection resultTrabajador;
  private Personal personal;
  private int findPersonalCedula;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private int findTrabajadorCedula;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection findColTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private Collection colFrecuenciaTipoPersonal;
  private boolean selectedTrabajador;
  private Collection colPersonal;
  private Collection colTipoPersonal;
  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  private Object stateScrollPersonal = null;
  private Object stateResultPersonal = null;

  private Object stateScrollTrabajadorByPersonal = null;
  private Object stateResultTrabajadorByPersonal = null;

  public Collection getFindColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public boolean isShowData()
  {
    return this.selected;
  }

  public Collection getResult() {
    return this.result;
  }

  public Trabajador getTrabajador() {
    if (this.trabajador == null) {
      this.trabajador = new Trabajador();
    }
    return this.trabajador;
  }

  public TrabajadorForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListFeVida() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListEstatus() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListMovimiento() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_MOVIMIENTO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListSituacion() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_SITUACION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListRiesgo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_RIESGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListRegimen() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_REGIMEN.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListFormaPago() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_PAGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTipoCtaNomina()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_CUENTA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());

      if (this.selectedTrabajador) {
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(this.trabajador.getTipoPersonal().getIdTipoPersonal());
        this.colFrecuenciaTipoPersonal = 
          this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(this.trabajador.getTipoPersonal().getIdTipoPersonal());
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndIdTipoPersonal(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidosAndIdTipoPersonal(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);

      Cargo cargo = null;
      manualCargoForCargo = null;
    }
    catch (Exception e)
    {
      ManualCargo manualCargoForCargo;
      log.error("Excepcion controlada:", e);
    }

    this.selected = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  public boolean isShowBancoNominaAux()
  {
    try
    {
      return this.trabajador.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowTipoCtaNominaAux() {
    try {
      return this.trabajador.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowCuentaNominaAux() {
    try {
      return this.trabajador.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  private void resetResultTrabajador()
  {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String abort()
  {
    this.selected = false;
    resetResult();
    this.trabajador = new Trabajador();
    return "cancel";
  }

  public String abortUpdate()
  {
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.trabajador = new Trabajador();
    return "cancel";
  }

  public Personal getPersonal()
  {
    return this.personal;
  }

  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }

  public boolean isShowResult() {
    return this.showResult;
  }

  public int getScrollx()
  {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getFindTrabajadorCedula()
  {
    return this.findTrabajadorCedula;
  }

  public void setFindTrabajadorCedula(int i)
  {
    this.findTrabajadorCedula = i;
  }

  public boolean isShowResultTrabajador()
  {
    return this.showResultTrabajador;
  }

  public void setShowResultTrabajador(boolean b)
  {
    this.showResultTrabajador = b;
  }

  public Collection getResultTrabajador()
  {
    return this.resultTrabajador;
  }

  public int getFindTrabajadorCodigoNomina()
  {
    return this.findTrabajadorCodigoNomina;
  }

  public String getFindTrabajadorPrimerApellido()
  {
    return this.findTrabajadorPrimerApellido;
  }

  public String getFindTrabajadorPrimerNombre()
  {
    return this.findTrabajadorPrimerNombre;
  }

  public String getFindTrabajadorSegundoApellido()
  {
    return this.findTrabajadorSegundoApellido;
  }

  public String getFindTrabajadorSegundoNombre()
  {
    return this.findTrabajadorSegundoNombre;
  }

  public void setFindTrabajadorCodigoNomina(int i)
  {
    this.findTrabajadorCodigoNomina = i;
  }

  public void setFindTrabajadorPrimerApellido(String string)
  {
    this.findTrabajadorPrimerApellido = string;
  }

  public void setFindTrabajadorPrimerNombre(String string)
  {
    this.findTrabajadorPrimerNombre = string;
  }

  public void setFindTrabajadorSegundoApellido(String string)
  {
    this.findTrabajadorSegundoApellido = string;
  }

  public void setFindTrabajadorSegundoNombre(String string)
  {
    this.findTrabajadorSegundoNombre = string;
  }

  public String getFindSelectTrabajadorIdTipoPersonal()
  {
    return this.findSelectTrabajadorIdTipoPersonal;
  }

  public void setFindSelectTrabajadorIdTipoPersonal(String string)
  {
    this.findSelectTrabajadorIdTipoPersonal = string;
  }

  public String getFindSelectTrabajador()
  {
    return this.findSelectTrabajador;
  }

  public void setFindSelectTrabajador(String string)
  {
    this.findSelectTrabajador = string;
  }
}