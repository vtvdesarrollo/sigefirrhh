package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.expediente.Familiar;

public class EmbargoFamiliar
  implements Serializable, PersistenceCapable
{
  private long idEmbargoFamiliar;
  private Embargo embargo;
  private Familiar familiar;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "embargo", "familiar", "idEmbargoFamiliar" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.personal.trabajador.Embargo"), sunjdo$classForName$("sigefirrhh.personal.expediente.Familiar"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 26, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Embargo getEmbargo()
  {
    return jdoGetembargo(this);
  }

  public Familiar getFamiliar()
  {
    return jdoGetfamiliar(this);
  }

  public long getIdEmbargoFamiliar()
  {
    return jdoGetidEmbargoFamiliar(this);
  }

  public void setEmbargo(Embargo embargo)
  {
    jdoSetembargo(this, embargo);
  }

  public void setFamiliar(Familiar familiar)
  {
    jdoSetfamiliar(this, familiar);
  }

  public void setIdEmbargoFamiliar(long l)
  {
    jdoSetidEmbargoFamiliar(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.EmbargoFamiliar"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new EmbargoFamiliar());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    EmbargoFamiliar localEmbargoFamiliar = new EmbargoFamiliar();
    localEmbargoFamiliar.jdoFlags = 1;
    localEmbargoFamiliar.jdoStateManager = paramStateManager;
    return localEmbargoFamiliar;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    EmbargoFamiliar localEmbargoFamiliar = new EmbargoFamiliar();
    localEmbargoFamiliar.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEmbargoFamiliar.jdoFlags = 1;
    localEmbargoFamiliar.jdoStateManager = paramStateManager;
    return localEmbargoFamiliar;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.embargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.familiar);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEmbargoFamiliar);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.embargo = ((Embargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.familiar = ((Familiar)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEmbargoFamiliar = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(EmbargoFamiliar paramEmbargoFamiliar, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEmbargoFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.embargo = paramEmbargoFamiliar.embargo;
      return;
    case 1:
      if (paramEmbargoFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.familiar = paramEmbargoFamiliar.familiar;
      return;
    case 2:
      if (paramEmbargoFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.idEmbargoFamiliar = paramEmbargoFamiliar.idEmbargoFamiliar;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof EmbargoFamiliar))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    EmbargoFamiliar localEmbargoFamiliar = (EmbargoFamiliar)paramObject;
    if (localEmbargoFamiliar.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEmbargoFamiliar, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EmbargoFamiliarPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EmbargoFamiliarPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EmbargoFamiliarPK))
      throw new IllegalArgumentException("arg1");
    EmbargoFamiliarPK localEmbargoFamiliarPK = (EmbargoFamiliarPK)paramObject;
    localEmbargoFamiliarPK.idEmbargoFamiliar = this.idEmbargoFamiliar;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EmbargoFamiliarPK))
      throw new IllegalArgumentException("arg1");
    EmbargoFamiliarPK localEmbargoFamiliarPK = (EmbargoFamiliarPK)paramObject;
    this.idEmbargoFamiliar = localEmbargoFamiliarPK.idEmbargoFamiliar;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EmbargoFamiliarPK))
      throw new IllegalArgumentException("arg2");
    EmbargoFamiliarPK localEmbargoFamiliarPK = (EmbargoFamiliarPK)paramObject;
    localEmbargoFamiliarPK.idEmbargoFamiliar = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EmbargoFamiliarPK))
      throw new IllegalArgumentException("arg2");
    EmbargoFamiliarPK localEmbargoFamiliarPK = (EmbargoFamiliarPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localEmbargoFamiliarPK.idEmbargoFamiliar);
  }

  private static final Embargo jdoGetembargo(EmbargoFamiliar paramEmbargoFamiliar)
  {
    StateManager localStateManager = paramEmbargoFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoFamiliar.embargo;
    if (localStateManager.isLoaded(paramEmbargoFamiliar, jdoInheritedFieldCount + 0))
      return paramEmbargoFamiliar.embargo;
    return (Embargo)localStateManager.getObjectField(paramEmbargoFamiliar, jdoInheritedFieldCount + 0, paramEmbargoFamiliar.embargo);
  }

  private static final void jdoSetembargo(EmbargoFamiliar paramEmbargoFamiliar, Embargo paramEmbargo)
  {
    StateManager localStateManager = paramEmbargoFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoFamiliar.embargo = paramEmbargo;
      return;
    }
    localStateManager.setObjectField(paramEmbargoFamiliar, jdoInheritedFieldCount + 0, paramEmbargoFamiliar.embargo, paramEmbargo);
  }

  private static final Familiar jdoGetfamiliar(EmbargoFamiliar paramEmbargoFamiliar)
  {
    StateManager localStateManager = paramEmbargoFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoFamiliar.familiar;
    if (localStateManager.isLoaded(paramEmbargoFamiliar, jdoInheritedFieldCount + 1))
      return paramEmbargoFamiliar.familiar;
    return (Familiar)localStateManager.getObjectField(paramEmbargoFamiliar, jdoInheritedFieldCount + 1, paramEmbargoFamiliar.familiar);
  }

  private static final void jdoSetfamiliar(EmbargoFamiliar paramEmbargoFamiliar, Familiar paramFamiliar)
  {
    StateManager localStateManager = paramEmbargoFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoFamiliar.familiar = paramFamiliar;
      return;
    }
    localStateManager.setObjectField(paramEmbargoFamiliar, jdoInheritedFieldCount + 1, paramEmbargoFamiliar.familiar, paramFamiliar);
  }

  private static final long jdoGetidEmbargoFamiliar(EmbargoFamiliar paramEmbargoFamiliar)
  {
    return paramEmbargoFamiliar.idEmbargoFamiliar;
  }

  private static final void jdoSetidEmbargoFamiliar(EmbargoFamiliar paramEmbargoFamiliar, long paramLong)
  {
    StateManager localStateManager = paramEmbargoFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoFamiliar.idEmbargoFamiliar = paramLong;
      return;
    }
    localStateManager.setLongField(paramEmbargoFamiliar, jdoInheritedFieldCount + 2, paramEmbargoFamiliar.idEmbargoFamiliar, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}