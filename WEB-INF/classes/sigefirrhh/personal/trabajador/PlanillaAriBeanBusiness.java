package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PlanillaAriBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPlanillaAri(PlanillaAri planillaAri)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PlanillaAri planillaAriNew = 
      (PlanillaAri)BeanUtils.cloneBean(
      planillaAri);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (planillaAriNew.getTrabajador() != null) {
      planillaAriNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        planillaAriNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(planillaAriNew);
  }

  public void updatePlanillaAri(PlanillaAri planillaAri) throws Exception
  {
    PlanillaAri planillaAriModify = 
      findPlanillaAriById(planillaAri.getIdPlanillaAri());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (planillaAri.getTrabajador() != null) {
      planillaAri.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        planillaAri.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(planillaAriModify, planillaAri);
  }

  public void deletePlanillaAri(PlanillaAri planillaAri) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PlanillaAri planillaAriDelete = 
      findPlanillaAriById(planillaAri.getIdPlanillaAri());
    pm.deletePersistent(planillaAriDelete);
  }

  public PlanillaAri findPlanillaAriById(long idPlanillaAri) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPlanillaAri == pIdPlanillaAri";
    Query query = pm.newQuery(PlanillaAri.class, filter);

    query.declareParameters("long pIdPlanillaAri");

    parameters.put("pIdPlanillaAri", new Long(idPlanillaAri));

    Collection colPlanillaAri = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPlanillaAri.iterator();
    return (PlanillaAri)iterator.next();
  }

  public Collection findPlanillaAriAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent planillaAriExtent = pm.getExtent(
      PlanillaAri.class, true);
    Query query = pm.newQuery(planillaAriExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(PlanillaAri.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    Collection colPlanillaAri = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPlanillaAri);

    return colPlanillaAri;
  }
}