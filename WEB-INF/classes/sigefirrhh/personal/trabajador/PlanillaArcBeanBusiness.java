package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PlanillaArcBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPlanillaArc(PlanillaArc planillaArc)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PlanillaArc planillaArcNew = 
      (PlanillaArc)BeanUtils.cloneBean(
      planillaArc);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (planillaArcNew.getTrabajador() != null) {
      planillaArcNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        planillaArcNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(planillaArcNew);
  }

  public void updatePlanillaArc(PlanillaArc planillaArc) throws Exception
  {
    PlanillaArc planillaArcModify = 
      findPlanillaArcById(planillaArc.getIdPlanillaArc());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (planillaArc.getTrabajador() != null) {
      planillaArc.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        planillaArc.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(planillaArcModify, planillaArc);
  }

  public void deletePlanillaArc(PlanillaArc planillaArc) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PlanillaArc planillaArcDelete = 
      findPlanillaArcById(planillaArc.getIdPlanillaArc());
    pm.deletePersistent(planillaArcDelete);
  }

  public PlanillaArc findPlanillaArcById(long idPlanillaArc) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPlanillaArc == pIdPlanillaArc";
    Query query = pm.newQuery(PlanillaArc.class, filter);

    query.declareParameters("long pIdPlanillaArc");

    parameters.put("pIdPlanillaArc", new Long(idPlanillaArc));

    Collection colPlanillaArc = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPlanillaArc.iterator();
    return (PlanillaArc)iterator.next();
  }

  public Collection findPlanillaArcAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent planillaArcExtent = pm.getExtent(
      PlanillaArc.class, true);
    Query query = pm.newQuery(planillaArcExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(PlanillaArc.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    Collection colPlanillaArc = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPlanillaArc);

    return colPlanillaArc;
  }
}