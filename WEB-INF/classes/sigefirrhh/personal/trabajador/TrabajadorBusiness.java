package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class TrabajadorBusiness extends AbstractBusiness
  implements Serializable
{
  private TrabajadorAsignaturaBeanBusiness trabajadorAsignaturaBeanBusiness = new TrabajadorAsignaturaBeanBusiness();

  private AnticipoBeanBusiness anticipoBeanBusiness = new AnticipoBeanBusiness();

  private CajaAhorroBeanBusiness cajaAhorroBeanBusiness = new CajaAhorroBeanBusiness();

  private ConceptoFijoBeanBusiness conceptoFijoBeanBusiness = new ConceptoFijoBeanBusiness();

  private ConceptoVariableBeanBusiness conceptoVariableBeanBusiness = new ConceptoVariableBeanBusiness();

  public ConceptoLiquidacionBeanBusiness conceptoLiquidacionBeanBusiness = new ConceptoLiquidacionBeanBusiness();

  private EmbargoBeanBusiness embargoBeanBusiness = new EmbargoBeanBusiness();

  private EmbargoConceptoBeanBusiness embargoConceptoBeanBusiness = new EmbargoConceptoBeanBusiness();

  private PlanillaArcBeanBusiness planillaArcBeanBusiness = new PlanillaArcBeanBusiness();

  private PlanillaAriBeanBusiness planillaAriBeanBusiness = new PlanillaAriBeanBusiness();

  private PrestamoBeanBusiness prestamoBeanBusiness = new PrestamoBeanBusiness();

  private SueldoPromedioBeanBusiness sueldoPromedioBeanBusiness = new SueldoPromedioBeanBusiness();

  private TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

  public void addTrabajadorAsignatura(TrabajadorAsignatura trabajadorAsignatura)
    throws Exception
  {
    this.trabajadorAsignaturaBeanBusiness.addTrabajadorAsignatura(trabajadorAsignatura);
  }

  public void updateTrabajadorAsignatura(TrabajadorAsignatura trabajadorAsignatura) throws Exception {
    this.trabajadorAsignaturaBeanBusiness.updateTrabajadorAsignatura(trabajadorAsignatura);
  }

  public void deleteTrabajadorAsignatura(TrabajadorAsignatura trabajadorAsignatura) throws Exception {
    this.trabajadorAsignaturaBeanBusiness.deleteTrabajadorAsignatura(trabajadorAsignatura);
  }

  public TrabajadorAsignatura findTrabajadorAsignaturaById(long trabajadorAsignaturaId) throws Exception {
    return this.trabajadorAsignaturaBeanBusiness.findTrabajadorAsignaturaById(trabajadorAsignaturaId);
  }

  public Collection findAllTrabajadorAsignatura() throws Exception {
    return this.trabajadorAsignaturaBeanBusiness.findTrabajadorAsignaturaAll();
  }

  public Collection findTrabajadorAsignaturaByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.trabajadorAsignaturaBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addAnticipo(Anticipo anticipo)
    throws Exception
  {
    this.anticipoBeanBusiness.addAnticipo(anticipo);
  }

  public void updateAnticipo(Anticipo anticipo) throws Exception {
    this.anticipoBeanBusiness.updateAnticipo(anticipo);
  }

  public void deleteAnticipo(Anticipo anticipo) throws Exception {
    this.anticipoBeanBusiness.deleteAnticipo(anticipo);
  }

  public Anticipo findAnticipoById(long anticipoId) throws Exception {
    return this.anticipoBeanBusiness.findAnticipoById(anticipoId);
  }

  public Collection findAllAnticipo() throws Exception {
    return this.anticipoBeanBusiness.findAnticipoAll();
  }

  public Collection findAnticipoByPersonal(long idPersonal)
    throws Exception
  {
    return this.anticipoBeanBusiness.findByPersonal(idPersonal);
  }

  public void addCajaAhorro(CajaAhorro cajaAhorro)
    throws Exception
  {
    this.cajaAhorroBeanBusiness.addCajaAhorro(cajaAhorro);
  }

  public void updateCajaAhorro(CajaAhorro cajaAhorro) throws Exception {
    this.cajaAhorroBeanBusiness.updateCajaAhorro(cajaAhorro);
  }

  public void deleteCajaAhorro(CajaAhorro cajaAhorro) throws Exception {
    this.cajaAhorroBeanBusiness.deleteCajaAhorro(cajaAhorro);
  }

  public CajaAhorro findCajaAhorroById(long cajaAhorroId) throws Exception {
    return this.cajaAhorroBeanBusiness.findCajaAhorroById(cajaAhorroId);
  }

  public Collection findAllCajaAhorro() throws Exception {
    return this.cajaAhorroBeanBusiness.findCajaAhorroAll();
  }

  public Collection findCajaAhorroByTrabajador(long idTrabajador) throws Exception
  {
    return this.cajaAhorroBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addConceptoFijo(ConceptoFijo conceptoFijo)
    throws Exception
  {
    this.conceptoFijoBeanBusiness.addConceptoFijo(conceptoFijo);
  }

  public void updateConceptoFijo(ConceptoFijo conceptoFijo) throws Exception {
    this.conceptoFijoBeanBusiness.updateConceptoFijo(conceptoFijo);
  }

  public void deleteConceptoFijo(ConceptoFijo conceptoFijo) throws Exception {
    this.conceptoFijoBeanBusiness.deleteConceptoFijo(conceptoFijo);
  }

  public ConceptoFijo findConceptoFijoById(long conceptoFijoId) throws Exception {
    return this.conceptoFijoBeanBusiness.findConceptoFijoById(conceptoFijoId);
  }

  public Collection findAllConceptoFijo() throws Exception {
    return this.conceptoFijoBeanBusiness.findConceptoFijoAll();
  }

  public Collection findConceptoFijoByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.conceptoFijoBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addConceptoVariable(ConceptoVariable conceptoVariable)
    throws Exception
  {
    this.conceptoVariableBeanBusiness.addConceptoVariable(conceptoVariable);
  }

  public void updateConceptoVariable(ConceptoVariable conceptoVariable) throws Exception {
    this.conceptoVariableBeanBusiness.updateConceptoVariable(conceptoVariable);
  }

  public void deleteConceptoVariable(ConceptoVariable conceptoVariable) throws Exception {
    this.conceptoVariableBeanBusiness.deleteConceptoVariable(conceptoVariable);
  }

  public ConceptoVariable findConceptoVariableById(long conceptoVariableId) throws Exception {
    return this.conceptoVariableBeanBusiness.findConceptoVariableById(conceptoVariableId);
  }

  public Collection findAllConceptoVariable() throws Exception {
    return this.conceptoVariableBeanBusiness.findConceptoVariableAll();
  }

  public Collection findConceptoVariableByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.conceptoVariableBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addConceptoLiquidacion(ConceptoLiquidacion conceptoLiquidacion)
    throws Exception
  {
    this.conceptoLiquidacionBeanBusiness.addConceptoLiquidacion(conceptoLiquidacion);
  }

  public void updateConceptoLiquidacion(ConceptoLiquidacion conceptoLiquidacion) throws Exception {
    this.conceptoLiquidacionBeanBusiness.updateConceptoLiquidacion(conceptoLiquidacion);
  }

  public void deleteConceptoLiquidacion(ConceptoLiquidacion conceptoLiquidacion) throws Exception {
    this.conceptoLiquidacionBeanBusiness.deleteConceptoLiquidacion(conceptoLiquidacion);
  }

  public ConceptoLiquidacion findConceptoLiquidacionById(long conceptoLiquidacionId) throws Exception {
    return this.conceptoLiquidacionBeanBusiness.findConceptoLiquidacionById(conceptoLiquidacionId);
  }

  public Collection findAllConceptoLiquidacion() throws Exception {
    return this.conceptoLiquidacionBeanBusiness.findConceptoLiquidacionAll();
  }

  public Collection findConceptoLiquidacionByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.conceptoLiquidacionBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addEmbargo(Embargo embargo)
    throws Exception
  {
    this.embargoBeanBusiness.addEmbargo(embargo);
  }

  public void updateEmbargo(Embargo embargo) throws Exception {
    this.embargoBeanBusiness.updateEmbargo(embargo);
  }

  public void deleteEmbargo(Embargo embargo) throws Exception {
    this.embargoBeanBusiness.deleteEmbargo(embargo);
  }

  public Embargo findEmbargoById(long embargoId) throws Exception {
    return this.embargoBeanBusiness.findEmbargoById(embargoId);
  }

  public Collection findAllEmbargo() throws Exception {
    return this.embargoBeanBusiness.findEmbargoAll();
  }

  public Collection findEmbargoByPersonal(long idPersonal)
    throws Exception
  {
    return this.embargoBeanBusiness.findByPersonal(idPersonal);
  }

  public void addEmbargoConcepto(EmbargoConcepto embargoConcepto)
    throws Exception
  {
    this.embargoConceptoBeanBusiness.addEmbargoConcepto(embargoConcepto);
  }

  public void updateEmbargoConcepto(EmbargoConcepto embargoConcepto) throws Exception {
    this.embargoConceptoBeanBusiness.updateEmbargoConcepto(embargoConcepto);
  }

  public void deleteEmbargoConcepto(EmbargoConcepto embargoConcepto) throws Exception {
    this.embargoConceptoBeanBusiness.deleteEmbargoConcepto(embargoConcepto);
  }

  public EmbargoConcepto findEmbargoConceptoById(long embargoConceptoId) throws Exception {
    return this.embargoConceptoBeanBusiness.findEmbargoConceptoById(embargoConceptoId);
  }

  public Collection findAllEmbargoConcepto() throws Exception {
    return this.embargoConceptoBeanBusiness.findEmbargoConceptoAll();
  }

  public Collection findEmbargoConceptoByEmbargo(long idEmbargo)
    throws Exception
  {
    return this.embargoConceptoBeanBusiness.findByEmbargo(idEmbargo);
  }

  public Collection findEmbargoConceptoByPersonal(long idPersonal)
    throws Exception
  {
    return this.embargoConceptoBeanBusiness.findByPersonal(idPersonal);
  }

  public void addPlanillaArc(PlanillaArc planillaArc)
    throws Exception
  {
    this.planillaArcBeanBusiness.addPlanillaArc(planillaArc);
  }

  public void updatePlanillaArc(PlanillaArc planillaArc) throws Exception {
    this.planillaArcBeanBusiness.updatePlanillaArc(planillaArc);
  }

  public void deletePlanillaArc(PlanillaArc planillaArc) throws Exception {
    this.planillaArcBeanBusiness.deletePlanillaArc(planillaArc);
  }

  public PlanillaArc findPlanillaArcById(long planillaArcId) throws Exception {
    return this.planillaArcBeanBusiness.findPlanillaArcById(planillaArcId);
  }

  public Collection findAllPlanillaArc() throws Exception {
    return this.planillaArcBeanBusiness.findPlanillaArcAll();
  }

  public Collection findPlanillaArcByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.planillaArcBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addPlanillaAri(PlanillaAri planillaAri)
    throws Exception
  {
    this.planillaAriBeanBusiness.addPlanillaAri(planillaAri);
  }

  public void updatePlanillaAri(PlanillaAri planillaAri) throws Exception {
    this.planillaAriBeanBusiness.updatePlanillaAri(planillaAri);
  }

  public void deletePlanillaAri(PlanillaAri planillaAri) throws Exception {
    this.planillaAriBeanBusiness.deletePlanillaAri(planillaAri);
  }

  public PlanillaAri findPlanillaAriById(long planillaAriId) throws Exception {
    return this.planillaAriBeanBusiness.findPlanillaAriById(planillaAriId);
  }

  public Collection findAllPlanillaAri() throws Exception {
    return this.planillaAriBeanBusiness.findPlanillaAriAll();
  }

  public Collection findPlanillaAriByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.planillaAriBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addPrestamo(Prestamo prestamo)
    throws Exception
  {
    this.prestamoBeanBusiness.addPrestamo(prestamo);
  }

  public void updatePrestamo(Prestamo prestamo) throws Exception {
    this.prestamoBeanBusiness.updatePrestamo(prestamo);
  }

  public void deletePrestamo(Prestamo prestamo) throws Exception {
    this.prestamoBeanBusiness.deletePrestamo(prestamo);
  }

  public Prestamo findPrestamoById(long prestamoId) throws Exception {
    return this.prestamoBeanBusiness.findPrestamoById(prestamoId);
  }

  public Collection findAllPrestamo() throws Exception {
    return this.prestamoBeanBusiness.findPrestamoAll();
  }

  public Collection findPrestamoByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.prestamoBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addSueldoPromedio(SueldoPromedio sueldoPromedio)
    throws Exception
  {
    this.sueldoPromedioBeanBusiness.addSueldoPromedio(sueldoPromedio);
  }

  public void updateSueldoPromedio(SueldoPromedio sueldoPromedio) throws Exception {
    this.sueldoPromedioBeanBusiness.updateSueldoPromedio(sueldoPromedio);
  }

  public void deleteSueldoPromedio(SueldoPromedio sueldoPromedio) throws Exception {
    this.sueldoPromedioBeanBusiness.deleteSueldoPromedio(sueldoPromedio);
  }

  public SueldoPromedio findSueldoPromedioById(long sueldoPromedioId) throws Exception {
    return this.sueldoPromedioBeanBusiness.findSueldoPromedioById(sueldoPromedioId);
  }

  public Collection findAllSueldoPromedio() throws Exception {
    return this.sueldoPromedioBeanBusiness.findSueldoPromedioAll();
  }

  public Collection findSueldoPromedioByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.sueldoPromedioBeanBusiness.findByTrabajador(idTrabajador);
  }

  public Collection findSueldoPromedioByTiempoSitp(Date tiempoSitp) throws Exception {
    return this.sueldoPromedioBeanBusiness.findByTiempoSitp(tiempoSitp);
  }

  public void addTrabajador(Trabajador trabajador) throws Exception {
    this.trabajadorBeanBusiness.addTrabajador(trabajador);
  }

  public void updateTrabajador(Trabajador trabajador) throws Exception {
    this.trabajadorBeanBusiness.updateTrabajador(trabajador);
  }

  public void deleteTrabajador(Trabajador trabajador) throws Exception {
    this.trabajadorBeanBusiness.deleteTrabajador(trabajador);
  }

  public Trabajador findTrabajadorById(long trabajadorId) throws Exception {
    return this.trabajadorBeanBusiness.findTrabajadorById(trabajadorId);
  }

  public Collection findAllTrabajador() throws Exception {
    return this.trabajadorBeanBusiness.findTrabajadorAll();
  }

  public Collection findTrabajadorByPersonal(long idPersonal, long idOrganismo) throws Exception {
    return this.trabajadorBeanBusiness.findByPersonal(idPersonal, idOrganismo);
  }

  public Collection findTrabajadorByTipoPersonal(long idTipoPersonal, long idOrganismo) throws Exception {
    return this.trabajadorBeanBusiness.findByTipoPersonal(idTipoPersonal, idOrganismo);
  }

  public Collection findTrabajadorByNombresApellidos(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo) throws Exception
  {
    return this.trabajadorBeanBusiness.findByNombresApellidos(
      primerNombre, segundoNombre, primerApellido, segundoApellido, idOrganismo);
  }

  public Collection findTrabajadorByCedula(int cedula, long idOrganismo) throws Exception
  {
    return this.trabajadorBeanBusiness.findByCedula(
      cedula, idOrganismo);
  }

  public Collection findTrabajadorByCodigoNomina(long idTipoPersonal, int codigoNomina) throws Exception
  {
    return this.trabajadorBeanBusiness.findByCodigoNomina(
      idTipoPersonal, codigoNomina);
  }

  public Collection findTrabajadorByCedula(int cedula, long idOrganismo, long idUsuario, String administrador, String estatus)
    throws Exception
  {
    return this.trabajadorBeanBusiness.findByCedula(
      cedula, idOrganismo, idUsuario, administrador, estatus);
  }

  public Collection findTrabajadorByCedula(int cedula, long idOrganismo, long idUsuario, String administrador) throws Exception
  {
    return this.trabajadorBeanBusiness.findByCedula(
      cedula, idOrganismo, idUsuario, administrador);
  }

  public Collection findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(long idTipoPersonal, int codigoNomina, String estatus) throws Exception
  {
    return this.trabajadorBeanBusiness.findByCodigoNominaAndTipoPersonalAndEstatus(
      idTipoPersonal, codigoNomina, estatus);
  }

  public Collection findTrabajadorByCedulaAndTipoPersonalAndEstatus(int cedula, long idTipoPersonal, String estatus) throws Exception
  {
    return this.trabajadorBeanBusiness.findByCedulaAndTipoPersonalAndEstatus(
      cedula, idTipoPersonal, estatus);
  }

  public Collection findTrabajadorByCedulaAndTipoPersonal(int cedula, long idTipoPersonal)
    throws Exception
  {
    return this.trabajadorBeanBusiness.findByCedulaAndTipoPersonal(
      cedula, idTipoPersonal);
  }

  public Collection findTrabajadorByNombresApellidosAndTipoPersonal(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idTipoPersonal)
    throws Exception
  {
    return this.trabajadorBeanBusiness.findByNombresApellidosAndTipoPersonal(
      primerNombre, segundoNombre, primerApellido, segundoApellido, idTipoPersonal);
  }

  public Collection findTrabajadorByCedulaAndIdTipoPersonal(int cedula, long idTipoPersonal)
    throws Exception
  {
    return this.trabajadorBeanBusiness.findByCedulaAndIdTipoPersonal(
      cedula, idTipoPersonal);
  }

  public Collection findTrabajadorByNombresApellidosAndIdTipoPersonal(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idTipoPersonal)
    throws Exception
  {
    return this.trabajadorBeanBusiness.findByNombresApellidosAndIdTipoPersonal(
      primerNombre, segundoNombre, primerApellido, segundoApellido, idTipoPersonal);
  }

  public Collection findTrabajadorByCedulaAndClasificacion(int cedula, long idOrganismo, long idClasificacionPersonal) throws Exception
  {
    return this.trabajadorBeanBusiness.findByCedulaAndClasificacion(
      cedula, idOrganismo, idClasificacionPersonal);
  }

  public Collection findTrabajadorByNombresApellidosAndClasificacion(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo, long idClasificacionPersonal) throws Exception
  {
    return this.trabajadorBeanBusiness.findByNombresApellidosAndClasificacion(
      primerNombre, segundoNombre, primerApellido, segundoApellido, idOrganismo, idClasificacionPersonal);
  }
}