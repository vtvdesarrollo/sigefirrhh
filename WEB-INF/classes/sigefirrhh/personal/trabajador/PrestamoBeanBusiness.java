package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonalBeanBusiness;

public class PrestamoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPrestamo(Prestamo prestamo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Prestamo prestamoNew = 
      (Prestamo)BeanUtils.cloneBean(
      prestamo);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (prestamoNew.getConceptoTipoPersonal() != null) {
      prestamoNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        prestamoNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (prestamoNew.getFrecuenciaTipoPersonal() != null) {
      prestamoNew.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        prestamoNew.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (prestamoNew.getTrabajador() != null) {
      prestamoNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        prestamoNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(prestamoNew);
  }

  public void updatePrestamo(Prestamo prestamo) throws Exception
  {
    Prestamo prestamoModify = 
      findPrestamoById(prestamo.getIdPrestamo());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (prestamo.getConceptoTipoPersonal() != null) {
      prestamo.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        prestamo.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (prestamo.getFrecuenciaTipoPersonal() != null) {
      prestamo.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        prestamo.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (prestamo.getTrabajador() != null) {
      prestamo.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        prestamo.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(prestamoModify, prestamo);
  }

  public void deletePrestamo(Prestamo prestamo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Prestamo prestamoDelete = 
      findPrestamoById(prestamo.getIdPrestamo());
    pm.deletePersistent(prestamoDelete);
  }

  public Prestamo findPrestamoById(long idPrestamo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPrestamo == pIdPrestamo";
    Query query = pm.newQuery(Prestamo.class, filter);

    query.declareParameters("long pIdPrestamo");

    parameters.put("pIdPrestamo", new Long(idPrestamo));

    Collection colPrestamo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPrestamo.iterator();
    return (Prestamo)iterator.next();
  }

  public Collection findPrestamoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent prestamoExtent = pm.getExtent(
      Prestamo.class, true);
    Query query = pm.newQuery(prestamoExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(Prestamo.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colPrestamo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrestamo);

    return colPrestamo;
  }
}