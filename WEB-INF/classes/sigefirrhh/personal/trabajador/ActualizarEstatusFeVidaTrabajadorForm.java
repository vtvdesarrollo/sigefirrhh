package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ActualizarEstatusFeVidaTrabajadorForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ActualizarEstatusFeVidaTrabajadorForm.class.getName());
  private boolean trabajadorEgresado;
  private Trabajador trabajador;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private DefinicionesFacadeExtend definicionesFacadeExtend = new DefinicionesFacadeExtend();
  private TrabajadorNoGenFacade trabajadorFacade = new TrabajadorNoGenFacade();
  private Collection resultTrabajador;
  private Personal personal;
  private int findTrabajadorCedula;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private int findTrabajadorCodigoNomina;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private boolean showResultTrabajador;
  private String findSelectPersonal;
  private Collection colPersonal;
  private Collection findColTipoPersonal;
  private boolean showData;
  private String findSelectTrabajadorIdTipoPersonal;
  private boolean retroactivo;
  private boolean activo;
  private boolean showConceptoFrecuencia;
  private Collection listConceptoTipoPersonal;
  private Collection listFrecuenciaTipoPersonal;
  private long idConceptoTipoPersonal;
  private long idFrecuenciaTipoPersonal;
  private double dias;
  private String pagarRetroactivo = "N";
  private double montoConcepto = 0.0D;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private Object stateResultPersonal = null;

  private Object stateResultTrabajadorByPersonal = null;

  public boolean isShowRetroactivo()
  {
    return (this.retroactivo) && (this.activo);
  }

  public boolean isShowConceptoFrecuencia() {
    return this.showConceptoFrecuencia;
  }
  private void resetResultTrabajador() {
    this.resultTrabajador = null;

    this.trabajador = null;

    this.showResultTrabajador = false;
  }
  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndIdTipoPersonal(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());

      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
      log.error("showResult" + this.showResultTrabajador);
      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidosAndIdTipoPersonal(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public void setFindSelectTrabajadorIdTipoPersonal(String string)
  {
    this.findSelectTrabajadorIdTipoPersonal = string;
  }
  public Collection getListConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }
  public Collection getListFrecuenciaTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()), 
        frecuenciaTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Trabajador getTrabajador() {
    if (this.trabajador == null) {
      this.trabajador = new Trabajador();
    }
    return this.trabajador;
  }

  public ActualizarEstatusFeVidaTrabajadorForm()
    throws Exception
  {
    if (this.login == null) {
      FacesContext context = FacesContext.getCurrentInstance();
      this.login = 
        ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
        context, 
        "loginSession"));
    }
  }

  public Collection getFindColTipoPersonal()
  {
    if (this.findColTipoPersonal == null) {
      try {
        log.error("paso por fctp 1");
        this.findColTipoPersonal = new ArrayList();
        this.findColTipoPersonal = 
          this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
        log.error("paso por fctp 2");
      } catch (Exception e) {
        log.error("Excepcion controlada:", e);
      }
    }

    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListFeVida()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListEstatus() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void changeEstatus(ValueChangeEvent event)
  {
    String estatus = 
      (String)event.getNewValue();

    this.trabajador.setEstatus(estatus);
    try
    {
      this.activo = false;
      if ((this.retroactivo) && (estatus.equals("A")))
        this.activo = true;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changePagarRetroactivo(ValueChangeEvent event)
  {
    String pagarRetroactivo = 
      (String)event.getNewValue();
    try
    {
      this.showConceptoFrecuencia = false;
      if (pagarRetroactivo.equals("S"))
        this.showConceptoFrecuencia = true;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = null;
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
      if (!this.trabajador.getEstatus().equals("A"))
        this.retroactivo = true;
      else {
        this.retroactivo = false;
      }
      this.listConceptoTipoPersonal = this.definicionesFacadeExtend.findConceptoTipoPersonalAsignacionesByTipoPersonalAndTipoPrestamo(this.trabajador.getTipoPersonal().getIdTipoPersonal(), "R");
      this.listFrecuenciaTipoPersonal = this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(this.trabajador.getTipoPersonal().getIdTipoPersonal());

      this.trabajadorEgresado = this.trabajador.getEstatus().equals("E");
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    this.selected = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    log.error("pagar_retroactivo " + this.pagarRetroactivo);
    if (this.pagarRetroactivo.equals("S")) {
      try {
        log.error("pasa 1 ");
        this.conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(this.idConceptoTipoPersonal);
        log.error("pasa 2 ");
        this.frecuenciaTipoPersonal = this.definicionesFacade.findFrecuenciaTipoPersonalById(this.idFrecuenciaTipoPersonal);
        log.error("pasa 3 ");
      }
      catch (Exception e) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No ha seleccionado concepto o frecuencia", ""));
        return null;
      }

    }

    try
    {
      this.trabajadorFacade.updateTrabajador(
        this.trabajador);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.trabajador, this.trabajador.getPersonal());

      if ((this.montoConcepto > 0.0D) && (this.pagarRetroactivo.equals("S")) && (this.trabajador.getEstatus().equals("A"))) {
        ConceptoVariable conceptoVariable = new ConceptoVariable();
        conceptoVariable.setAnioSobretiempo(0);
        conceptoVariable.setConceptoTipoPersonal(this.conceptoTipoPersonal);
        conceptoVariable.setDocumentoSoporte("");
        conceptoVariable.setEstatus("A");
        conceptoVariable.setFechaRegistro(new Date());
        conceptoVariable.setFrecuenciaTipoPersonal(this.frecuenciaTipoPersonal);
        conceptoVariable.setMesSobretiempo(0);
        conceptoVariable.setMonto(this.montoConcepto);
        conceptoVariable.setTrabajador(this.trabajador);
        conceptoVariable.setUnidades(this.dias);

        IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
        conceptoVariable.setIdConceptoVariable(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable"));
        this.trabajadorFacade.addConceptoVariable(conceptoVariable);
      }

      context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));

      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (ErrorSistema a) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
      else
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.trabajadorFacade.deleteTrabajador(
        this.trabajador);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.trabajador, this.trabajador.getPersonal());

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.trabajador = new Trabajador();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.trabajador = new Trabajador();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return this.selected;
  }

  public boolean isShowResultPersonal()
  {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return this.adding;
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getFindTrabajadorCedula()
  {
    return this.findTrabajadorCedula;
  }

  public int getFindTrabajadorCodigoNomina()
  {
    return this.findTrabajadorCodigoNomina;
  }

  public String getFindTrabajadorPrimerApellido()
  {
    return this.findTrabajadorPrimerApellido;
  }

  public String getFindTrabajadorPrimerNombre()
  {
    return this.findTrabajadorPrimerNombre;
  }

  public String getFindTrabajadorSegundoApellido()
  {
    return this.findTrabajadorSegundoApellido;
  }

  public String getFindTrabajadorSegundoNombre()
  {
    return this.findTrabajadorSegundoNombre;
  }

  public void setFindTrabajadorCedula(int i)
  {
    this.findTrabajadorCedula = i;
  }

  public void setFindTrabajadorCodigoNomina(int i)
  {
    this.findTrabajadorCodigoNomina = i;
  }

  public void setFindTrabajadorPrimerApellido(String string)
  {
    this.findTrabajadorPrimerApellido = string;
  }

  public void setFindTrabajadorPrimerNombre(String string)
  {
    this.findTrabajadorPrimerNombre = string;
  }

  public void setFindTrabajadorSegundoApellido(String string)
  {
    this.findTrabajadorSegundoApellido = string;
  }

  public void setFindTrabajadorSegundoNombre(String string)
  {
    this.findTrabajadorSegundoNombre = string;
  }

  public String getFindSelectTrabajadorIdTipoPersonal()
  {
    return this.findSelectTrabajadorIdTipoPersonal;
  }

  public boolean isShowResultTrabajador()
  {
    return this.showResultTrabajador;
  }

  public Collection getResultTrabajador()
  {
    return this.resultTrabajador;
  }

  public void setShowData(boolean b)
  {
    this.showData = b;
  }

  public boolean isTrabajadorEgresado() {
    return this.trabajadorEgresado;
  }
  public long getIdConceptoTipoPersonal() {
    return this.idConceptoTipoPersonal;
  }
  public void setIdConceptoTipoPersonal(long idConceptoTipoPersonal) {
    this.idConceptoTipoPersonal = idConceptoTipoPersonal;
  }
  public long getIdFrecuenciaTipoPersonal() {
    return this.idFrecuenciaTipoPersonal;
  }
  public void setIdFrecuenciaTipoPersonal(long idFrecuenciaTipoPersonal) {
    this.idFrecuenciaTipoPersonal = idFrecuenciaTipoPersonal;
  }
  public double getDias() {
    return this.dias;
  }
  public void setDias(double dias) {
    this.dias = dias;
  }
  public String getPagarRetroactivo() {
    return this.pagarRetroactivo;
  }
  public void setPagarRetroactivo(String pagarRetroactivo) {
    this.pagarRetroactivo = pagarRetroactivo;
  }
  public double getMontoConcepto() {
    return this.montoConcepto;
  }
  public void setMontoConcepto(double montoConcepto) {
    this.montoConcepto = montoConcepto;
  }
}