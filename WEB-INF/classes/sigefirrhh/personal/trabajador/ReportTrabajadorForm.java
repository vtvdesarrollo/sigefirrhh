package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.adiestramiento.AdiestramientoFacade;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.personal.AreaCarrera;
import sigefirrhh.base.personal.Carrera;
import sigefirrhh.base.personal.CarreraArea;
import sigefirrhh.base.personal.GrupoProfesion;
import sigefirrhh.base.personal.NivelEducativo;
import sigefirrhh.base.personal.PersonalFacade;
import sigefirrhh.base.personal.PersonalNoGenFacade;
import sigefirrhh.base.personal.Profesion;
import sigefirrhh.base.personal.TipoAmonestacion;
import sigefirrhh.base.personal.TipoIdioma;
import sigefirrhh.base.personal.TipoReconocimiento;
import sigefirrhh.base.personal.Titulo;
import sigefirrhh.login.LoginSession;

public class ReportTrabajadorForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportTrabajadorForm.class.getName());
  private int reportId;
  private String estatus;
  private long idDependencia;
  private long idTipoPersonal;
  private String reportName;
  private Collection listDependencia;
  private Collection listTipoPersonal;
  private EstructuraFacade estructuraFacade;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private boolean showAreaConocimiento;
  private boolean auxShowAreaConocimiento;
  private long idAreaConocimiento;
  private Collection colAreaConocimiento;
  private boolean showManualCargo;
  private boolean auxShowManualCargo;
  private long idManualCargo;
  private Collection colManualCargo;
  private boolean showCargo;
  private boolean auxShowCargo;
  private long idCargo;
  private Collection colCargo;
  private boolean showProfesion;
  private boolean auxShowProfesion;
  private long idProfesion;
  private Collection colProfesion;
  private boolean showGrupoProfesion;
  private boolean auxShowGrupoProfesion;
  private long idGrupoProfesion;
  private Collection colGrupoProfesion;
  private boolean showTipoAmonestacion;
  private boolean auxShowTipoAmonestacion;
  private long idTipoAmonestacion;
  private Collection colTipoAmonestacion;
  private boolean showTipoReconocimiento;
  private boolean auxShowTipoReconocimiento;
  private long idTipoReconocimiento;
  private Collection colTipoReconocimiento;
  private boolean showTipoIdioma;
  private boolean auxShowTipoIdioma;
  private long idTipoIdioma;
  private Collection colTipoIdioma;
  private boolean showAreaCarrera;
  private boolean auxShowAreaCarrera;
  private long idAreaCarrera;
  private Collection colAreaCarrera;
  private boolean showCarreraArea;
  private boolean auxShowCarreraArea;
  private long idCarreraArea;
  private Collection colCarreraArea;
  private boolean showNivelEducativoForTitulo;
  private boolean auxShowNivelEducativoForTitulo;
  private long idNivelEducativoForTitulo;
  private Collection colNivelEducativoForTitulo;
  private boolean showTitulo;
  private boolean auxShowTitulo;
  private long idTitulo;
  private Collection colTitulo;
  private boolean showNivelEducativo;
  private boolean auxShowNivelEducativo;
  private long idNivelEducativo;
  private Collection colNivelEducativo;
  private AdiestramientoFacade adiestramientoFacade = new AdiestramientoFacade();
  private CargoFacade cargoFacade = new CargoFacade();
  private PersonalFacade personalFacade = new PersonalFacade();
  private PersonalNoGenFacade personalNoGenFacade = new PersonalNoGenFacade();
  private long tipoReporte;

  public ReportTrabajadorForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "GrupoProfesionTrabajador_p1";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportTrabajadorForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listDependencia = this.estructuraFacade.findAllDependencia();
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listDependencia = new ArrayList();
    }
  }

  public void changeReport(ValueChangeEvent event) { long idDependencia = Long.valueOf(
      (String)event.getNewValue()).longValue();
  }

  public void changeReportTrabajadorForCarrera(ValueChangeEvent event)
  {
    Long aux = (Long)event.getNewValue();
    long idAreaCarrera = aux.intValue();
    try {
      this.colCarreraArea = null;
      if (idAreaCarrera > 0L) {
        this.colCarreraArea = 
          this.personalFacade.findCarreraAreaByAreaCarrera(idAreaCarrera);
        if (this.tipoReporte == 11L) {
          this.auxShowCarreraArea = true;
        }
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeReportTrabajadorForTitulo(ValueChangeEvent event)
  {
    Long aux = (Long)event.getNewValue();
    long idNivelEducativo = aux.intValue();
    try {
      this.colTitulo = null;
      if (idNivelEducativo > 0L) {
        this.colTitulo = 
          this.personalFacade.findTituloByNivelEducativo(idNivelEducativo);
        this.auxShowTitulo = true;
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeReportTrabajadorForTipoReporte(ValueChangeEvent event)
  {
    Long auxTipoReporte = (Long)event.getNewValue();
    this.tipoReporte = auxTipoReporte.intValue();
    try
    {
      this.auxShowAreaConocimiento = false;
      this.auxShowManualCargo = false;
      this.auxShowProfesion = false;
      this.auxShowGrupoProfesion = false;
      this.auxShowTipoAmonestacion = false;
      this.auxShowTipoReconocimiento = false;
      this.auxShowTipoIdioma = false;
      this.auxShowAreaCarrera = false;
      this.auxShowCarreraArea = false;
      this.auxShowTitulo = false;
      this.auxShowNivelEducativo = false;
      this.auxShowNivelEducativoForTitulo = false;
      if (this.tipoReporte == 1L) {
        if (this.colAreaConocimiento == null) {
          this.colAreaConocimiento = 
            this.adiestramientoFacade.findAllAreaConocimiento();
          this.auxShowAreaConocimiento = true;
        } else {
          this.auxShowAreaConocimiento = true;
        }
      } else if (this.tipoReporte == 2L) {
        if (this.colManualCargo == null) {
          this.colManualCargo = 
            this.cargoFacade.findAllManualCargo();
          this.auxShowManualCargo = true;
        } else {
          this.auxShowManualCargo = true;
        }
      } else if (this.tipoReporte == 3L) {
        if (this.colProfesion == null) {
          this.colProfesion = 
            this.personalFacade.findAllProfesion();
          this.auxShowProfesion = true;
        } else {
          this.auxShowProfesion = true;
        }
      } else if (this.tipoReporte == 4L) {
        if (this.colGrupoProfesion == null) {
          this.colGrupoProfesion = 
            this.personalFacade.findAllGrupoProfesion();
          this.auxShowGrupoProfesion = true;
        } else {
          this.auxShowGrupoProfesion = true;
        }
      } else if (this.tipoReporte == 5L) {
        if (this.colTipoAmonestacion == null) {
          this.colTipoAmonestacion = 
            this.personalFacade.findAllTipoAmonestacion();
          this.auxShowTipoAmonestacion = true;
        } else {
          this.auxShowTipoAmonestacion = true;
        }
      } else if (this.tipoReporte == 6L) {
        if (this.colTipoReconocimiento == null) {
          this.colTipoReconocimiento = 
            this.personalFacade.findAllTipoReconocimiento();
          this.auxShowTipoReconocimiento = true;
        } else {
          this.auxShowTipoReconocimiento = true;
        }
      } else if (this.tipoReporte == 7L) {
        if (this.colTipoIdioma == null) {
          this.colTipoIdioma = 
            this.personalFacade.findAllTipoIdioma();
          this.auxShowTipoIdioma = true;
        } else {
          this.auxShowTipoIdioma = true;
        }
      } else if (this.tipoReporte == 8L) {
        if (this.colAreaCarrera == null) {
          this.colAreaCarrera = 
            this.personalFacade.findAllAreaCarrera();
          this.auxShowAreaCarrera = true;
        } else {
          this.auxShowAreaCarrera = true;
        }
      } else if (this.tipoReporte == 9L) {
        if (this.colNivelEducativoForTitulo == null) {
          this.colNivelEducativoForTitulo = 
            this.personalNoGenFacade.findNivelEducativoForTitulo();
          this.auxShowNivelEducativoForTitulo = true;
        } else {
          this.auxShowNivelEducativoForTitulo = true;
        }
      } else if (this.tipoReporte == 10L) {
        if (this.colNivelEducativo == null) {
          this.colNivelEducativo = 
            this.personalFacade.findAllNivelEducativo();
          this.auxShowNivelEducativo = true;
        } else {
          this.auxShowNivelEducativo = true;
        }
      } else if (this.tipoReporte == 11L) {
        if (this.colAreaCarrera == null) {
          this.colAreaCarrera = 
            this.personalFacade.findAllAreaCarrera();
          this.auxShowAreaCarrera = true;
        } else {
          this.auxShowAreaCarrera = true;
        }
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void cambiarNombreAReporte() {
    if (this.tipoReporte == 2L)
      this.reportName = "EstudioTrabajador";
    else if (this.tipoReporte == 3L) {
      this.reportName = "ProfesionTrabajador";
    }
    else if (this.tipoReporte == 4L) {
      this.reportName = "GrupoProfesionTrabajador";
    }
    else if (this.tipoReporte == 5L) {
      this.reportName = "SancionTrabajador";
    }
    else if (this.tipoReporte == 6L) {
      this.reportName = "ReconocimientoTrabajador";
    }
    else if (this.tipoReporte == 7L) {
      this.reportName = "IdiomaTrabajador";
    }
    else if (this.tipoReporte == 8L) {
      this.reportName = "AreaCarreraTrabajador";
    }
    else if (this.tipoReporte == 9L) {
      this.reportName = "TituloTrabajador";
    }
    else if (this.tipoReporte == 10L) {
      this.reportName = "NivelEducativoTrabajador";
    }
    else if (this.tipoReporte == 11L) {
      this.reportName = "CarreraTrabajador";
    }

    if (this.idDependencia != 0L) {
      this.reportName += "_p1";
    }
    else {
      this.reportName += "_p2";
    }

    log.error("PASO POR REPORTE" + this.reportName);
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    int anioActual = new Date().getYear();
    try {
      if (this.tipoReporte >= 8L) {
        parameters.put("anio_actual", new Integer(anioActual));
      }
      if (this.tipoReporte == 2L) {
        this.reportName = "EstudioTrabajador";
        parameters.put("id_area_conocimiento", new Long(this.idAreaConocimiento));
        String nombreAreaConocimiento = this.adiestramientoFacade.findAreaConocimientoById(this.idAreaConocimiento).getDescripcion();
        parameters.put("nombre_area_conocimiento", nombreAreaConocimiento);
      } else if (this.tipoReporte == 3L) {
        this.reportName = "ProfesionTrabajador";
        parameters.put("id_profesion", new Long(this.idProfesion));
        String nombreProfesion = this.personalFacade.findProfesionById(this.idProfesion).getNombre();
        parameters.put("nombre_profesion", nombreProfesion);
      } else if (this.tipoReporte == 4L) {
        this.reportName = "GrupoProfesionTrabajador";
        parameters.put("id_grupo_profesion", new Long(this.idGrupoProfesion));
        String nombreGrupoProfesion = this.personalFacade.findGrupoProfesionById(this.idGrupoProfesion).getNombre();
        parameters.put("nombre_grupo_profesion", nombreGrupoProfesion);
      } else if (this.tipoReporte == 5L) {
        this.reportName = "SancionTrabajador";
        parameters.put("id_tipo_amonestacion", new Long(this.idTipoAmonestacion));
        String nombreSancion = this.personalFacade.findTipoAmonestacionById(this.idTipoAmonestacion).getDescripcion();
        parameters.put("nombre_tipo_amonestacion", nombreSancion);
      } else if (this.tipoReporte == 6L) {
        this.reportName = "ReconocimientoTrabajador";
        parameters.put("id_tipo_reconocimiento", new Long(this.idTipoReconocimiento));
        String nombreReconocimiento = this.personalFacade.findTipoReconocimientoById(this.idTipoReconocimiento).getDescripcion();
        parameters.put("nombre_reconocimiento", nombreReconocimiento);
      } else if (this.tipoReporte == 7L) {
        this.reportName = "IdiomaTrabajador";
        parameters.put("id_tipo_idioma", new Long(this.idTipoIdioma));
        String nombreIdioma = this.personalFacade.findTipoIdiomaById(this.idTipoIdioma).getDescripcion();
        parameters.put("nombre_tipo_idioma", nombreIdioma);
      } else if (this.tipoReporte == 8L) {
        this.reportName = "AreaCarreraTrabajador";
        parameters.put("id_area_carrera", new Long(this.idAreaCarrera));
        String nombreAreaCarrera = this.personalFacade.findAreaCarreraById(this.idAreaCarrera).getDescripcion();
        parameters.put("nombre_area_carrera", nombreAreaCarrera);
      } else if (this.tipoReporte == 9L) {
        this.reportName = "TituloTrabajador";
        parameters.put("id_titulo", new Long(this.idTitulo));
        String nombreTitulo = this.personalFacade.findTituloById(this.idTitulo).getDescripcion();
        parameters.put("nombre_titulo", nombreTitulo);
      } else if (this.tipoReporte == 10L) {
        this.reportName = "NivelEducativoTrabajador";
        parameters.put("id_nivel_educativo", new Long(this.idNivelEducativo));
        String nombreNivelEducativo = this.personalFacade.findNivelEducativoById(this.idNivelEducativo).getDescripcion();
        parameters.put("nombre_nivel_educativo", nombreNivelEducativo);
      } else if (this.tipoReporte == 11L) {
        this.reportName = "CarreraTrabajador";
        CarreraArea carreraArea = this.personalFacade.findCarreraAreaById(this.idCarreraArea);
        Carrera carrera = this.personalFacade.findCarreraById(carreraArea.getCarrera().getIdCarrera());
        long idCarrera = carrera.getIdCarrera();
        String nombreCarrera = carrera.getNombre();
        parameters.put("id_carrera", new Long(idCarrera));
        parameters.put("nombre_carrera", nombreCarrera);
      }

      if (this.idDependencia != 0L) {
        this.reportName += "_p1";
        String nombreDependencia = this.estructuraFacade.findDependenciaById(this.idDependencia).getNombre();
        parameters.put("nombre_dependencia", nombreDependencia);
        parameters.put("id_dependencia", new Long(this.idDependencia));
      } else {
        this.reportName += "_p2";
      }
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      String nombreTipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal).getNombre();
      parameters.put("nombre_tipo_personal", nombreTipoPersonal);

      log.error("reportId" + this.reportId);
      log.error("reportName:" + this.reportName);
      log.error("nombre_organismo" + this.login.getOrganismo().getNombreOrganismo());
      log.error("logo" + ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      log.error("id_tipo_personal" + this.idTipoPersonal);

      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/trabajador");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String getEstatus()
  {
    return this.estatus;
  }

  public String getIdDependencia()
  {
    return String.valueOf(this.idDependencia);
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setEstatus(String string)
  {
    this.estatus = string;
  }

  public void setIdDependencia(String l)
  {
    this.idDependencia = Long.parseLong(l);
  }

  public void setIdTipoPersonal(String l)
  {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public Collection getListDependencia()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listDependencia, "sigefirrhh.base.estructura.Dependencia");
  }

  public Collection getListEstatus()
  {
    return ListUtil.convertMapToSelectItems(Trabajador.LISTA_ESTATUS);
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public boolean isShowAreaConocimiento()
  {
    return this.auxShowAreaConocimiento;
  }

  public void setShowAreaConocimiento(boolean b)
  {
    this.showAreaConocimiento = b;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public long getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(long l)
  {
    this.tipoReporte = l;
  }

  public Collection getColAreaConocimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(areaConocimiento.getIdAreaConocimiento()), 
        areaConocimiento.toString()));
    }
    return col;
  }

  public long getIdAreaConocimiento()
  {
    return this.idAreaConocimiento;
  }

  public void setIdAreaConocimiento(long l)
  {
    this.idAreaConocimiento = l;
  }

  public Collection getColManualCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargo.iterator();
    ManualCargo manualCargo = null;
    while (iterator.hasNext()) {
      manualCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargo.getIdManualCargo()), 
        manualCargo.toString()));
    }
    return col;
  }

  public long getIdManualCargo()
  {
    return this.idManualCargo;
  }

  public boolean isShowManualCargo()
  {
    return this.auxShowManualCargo;
  }

  public void setIdManualCargo(long l)
  {
    this.idManualCargo = l;
  }

  public void setShowManualCargo(boolean b)
  {
    this.showManualCargo = b;
  }

  public boolean isShowGrupoProfesion()
  {
    return this.auxShowGrupoProfesion;
  }

  public boolean isShowProfesion()
  {
    return this.auxShowProfesion;
  }

  public boolean isShowTipoAmonestacion()
  {
    return this.auxShowTipoAmonestacion;
  }

  public void setShowGrupoProfesion(boolean b)
  {
    this.showGrupoProfesion = b;
  }

  public void setShowProfesion(boolean b)
  {
    this.showProfesion = b;
  }

  public void setShowTipoAmonestacion(boolean b)
  {
    this.showTipoAmonestacion = b;
  }

  public Collection getColGrupoProfesion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoProfesion.iterator();
    GrupoProfesion grupoProfesion = null;
    while (iterator.hasNext()) {
      grupoProfesion = (GrupoProfesion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoProfesion.getIdGrupoProfesion()), 
        grupoProfesion.toString()));
    }
    return col;
  }

  public Collection getColProfesion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colProfesion.iterator();
    Profesion profesion = null;
    while (iterator.hasNext()) {
      profesion = (Profesion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(profesion.getIdProfesion()), 
        profesion.toString()));
    }
    return col;
  }

  public Collection getColTipoAmonestacion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoAmonestacion.iterator();
    TipoAmonestacion tipoAmonestacion = null;
    while (iterator.hasNext()) {
      tipoAmonestacion = (TipoAmonestacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoAmonestacion.getIdTipoAmonestacion()), 
        tipoAmonestacion.toString()));
    }
    return col;
  }

  public long getIdGrupoProfesion()
  {
    return this.idGrupoProfesion;
  }

  public long getIdProfesion()
  {
    return this.idProfesion;
  }

  public long getIdTipoAmonestacion()
  {
    return this.idTipoAmonestacion;
  }

  public void setIdGrupoProfesion(long l)
  {
    this.idGrupoProfesion = l;
  }

  public void setIdProfesion(long l)
  {
    this.idProfesion = l;
  }

  public void setIdTipoAmonestacion(long l)
  {
    this.idTipoAmonestacion = l;
  }

  public Collection getColAreaCarrera()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAreaCarrera.iterator();
    AreaCarrera areaCarrera = null;
    while (iterator.hasNext()) {
      areaCarrera = (AreaCarrera)iterator.next();
      col.add(new SelectItem(
        String.valueOf(areaCarrera.getIdAreaCarrera()), 
        areaCarrera.toString()));
    }
    return col;
  }

  public Collection getColCarreraArea()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCarreraArea.iterator();
    CarreraArea carreraArea = null;
    while (iterator.hasNext()) {
      carreraArea = (CarreraArea)iterator.next();
      col.add(new SelectItem(
        String.valueOf(carreraArea.getIdCarreraArea()), 
        carreraArea.toString()));
    }
    return col;
  }

  public Collection getColNivelEducativo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colNivelEducativo.iterator();
    NivelEducativo nivelEducativo = null;
    while (iterator.hasNext()) {
      nivelEducativo = (NivelEducativo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nivelEducativo.getIdNivelEducativo()), 
        nivelEducativo.toString()));
    }
    return col;
  }

  public Collection getColTipoIdioma()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoIdioma.iterator();
    TipoIdioma tipoIdioma = null;
    while (iterator.hasNext()) {
      tipoIdioma = (TipoIdioma)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoIdioma.getIdTipoIdioma()), 
        tipoIdioma.toString()));
    }
    return col;
  }

  public Collection getColTipoReconocimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoReconocimiento.iterator();
    TipoReconocimiento tipoReconocimiento = null;
    while (iterator.hasNext()) {
      tipoReconocimiento = (TipoReconocimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoReconocimiento.getIdTipoReconocimiento()), 
        tipoReconocimiento.toString()));
    }
    return col;
  }

  public long getIdAreaCarrera()
  {
    return this.idAreaCarrera;
  }

  public long getIdNivelEducativo()
  {
    return this.idNivelEducativo;
  }

  public long getIdTipoIdioma()
  {
    return this.idTipoIdioma;
  }

  public long getIdTipoReconocimiento()
  {
    return this.idTipoReconocimiento;
  }

  public boolean isShowAreaCarrera()
  {
    return this.auxShowAreaCarrera;
  }

  public boolean isShowNivelEducativo()
  {
    return this.auxShowNivelEducativo;
  }

  public boolean isShowTipoIdioma()
  {
    return this.auxShowTipoIdioma;
  }

  public boolean isShowTipoReconocimiento()
  {
    return this.auxShowTipoReconocimiento;
  }

  public void setIdAreaCarrera(long l)
  {
    this.idAreaCarrera = l;
  }

  public void setIdNivelEducativo(long l)
  {
    this.idNivelEducativo = l;
  }

  public void setIdTipoIdioma(long l)
  {
    this.idTipoIdioma = l;
  }

  public void setIdTipoReconocimiento(long l)
  {
    this.idTipoReconocimiento = l;
  }

  public void setShowAreaCarrera(boolean b)
  {
    this.showAreaCarrera = b;
  }

  public void setShowNivelEducativo(boolean b)
  {
    this.showNivelEducativo = b;
  }

  public void setShowTipoIdioma(boolean b)
  {
    this.showTipoIdioma = b;
  }

  public void setShowTipoReconocimiento(boolean b)
  {
    this.showTipoReconocimiento = b;
  }

  public boolean isAuxShowCargo()
  {
    return this.auxShowCargo;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getColTitulo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTitulo.iterator();
    Titulo titulo = null;
    while (iterator.hasNext()) {
      titulo = (Titulo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(titulo.getIdTitulo()), 
        titulo.toString()));
    }
    return col;
  }

  public long getIdCargo()
  {
    return this.idCargo;
  }

  public long getIdTitulo()
  {
    return this.idTitulo;
  }

  public boolean isShowCargo()
  {
    return this.auxShowCargo;
  }

  public boolean isShowTitulo()
  {
    return this.auxShowTitulo;
  }

  public void setAuxShowCargo(boolean b)
  {
    this.auxShowCargo = b;
  }

  public void setIdCargo(long l)
  {
    this.idCargo = l;
  }

  public void setIdTitulo(long l)
  {
    this.idTitulo = l;
  }

  public Collection getColNivelEducativoForTitulo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colNivelEducativoForTitulo.iterator();
    NivelEducativo nivelEducativo = null;
    while (iterator.hasNext()) {
      nivelEducativo = (NivelEducativo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nivelEducativo.getIdNivelEducativo()), 
        nivelEducativo.toString()));
    }
    return col;
  }

  public long getIdNivelEducativoForTitulo()
  {
    return this.idNivelEducativoForTitulo;
  }

  public boolean isShowNivelEducativoForTitulo()
  {
    return this.auxShowNivelEducativoForTitulo;
  }

  public void setIdNivelEducativoForTitulo(long l)
  {
    this.idNivelEducativoForTitulo = l;
  }

  public long getIdCarreraArea()
  {
    return this.idCarreraArea;
  }

  public boolean isShowCarreraArea()
  {
    return this.auxShowCarreraArea;
  }

  public void setIdCarreraArea(long l)
  {
    this.idCarreraArea = l;
  }
}