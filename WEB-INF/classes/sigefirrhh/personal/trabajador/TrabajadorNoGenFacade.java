package sigefirrhh.personal.trabajador;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import org.apache.log4j.Logger;

public class TrabajadorNoGenFacade extends TrabajadorFacade
  implements Serializable
{
  static Logger log = Logger.getLogger(TrabajadorNoGenFacade.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private TrabajadorNoGenBusiness trabajadorNoGenBusiness = new TrabajadorNoGenBusiness();

  public TrabajadorNoGenFacade()
  {
    log.error("TrabajadorFacade");
  }

  public Collection findTrabajadorByPassword(int cedula, String password) throws Exception
  {
    try {
      this.txn.open();
      return this.trabajadorNoGenBusiness.findTrabajadorByPassword(cedula, password);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorForRegistro(int cedula, int anioInicio, Date fechaNacimiento) throws Exception
  {
    try { this.txn.open();
      return this.trabajadorNoGenBusiness.findTrabajadorForRegistro(cedula, anioInicio, fechaNacimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoFijoByTrabajadorAndConceptoTipoPersonal(long idTrabajador, long idConceptoTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.trabajadorNoGenBusiness.findConceptoFijoByTrabajadorAndConceptoTipoPersonal(idTrabajador, idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoVariableByTrabajadorAndConceptoTipoPersonal(long idTrabajador, long idConceptoTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.trabajadorNoGenBusiness.findConceptoVariableByTrabajadorAndConceptoTipoPersonal(idTrabajador, idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoLiquidcionByTrabajadorAndConceptoTipoPersonal(long idTrabajador, long idConceptoTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorNoGenBusiness.findConceptoLiquidacionByTrabajadorAndConceptoTipoPersonal(idTrabajador, idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public double calcularConcepto(long idConceptoTipoPersonal, long idTrabajador, double unidades, String tipo, int codFrecuenciaPago, double jornadaDiaria, double jornadaSemanal, String formulaIntegral, String formulaSemanal, long idCargo, double valor, double topeMinimo, double topeMaximo) throws Exception
  {
    try {
      this.txn.open();
      return this.trabajadorNoGenBusiness.calcularConcepto(idConceptoTipoPersonal, idTrabajador, unidades, tipo, codFrecuenciaPago, jornadaDiaria, jornadaSemanal, formulaIntegral, formulaSemanal, idCargo, valor, topeMinimo, topeMaximo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByCedulaAndTipoPersonal(int cedula, long idOrganismo, long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorNoGenBusiness.findTrabajadorByCedulaAndTipoPersonal(
        cedula, idOrganismo, idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoFijoByTrabajadorConceptoTipoPersonalFrecuenciaTipoPersonal(long idTrabajador, long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.trabajadorNoGenBusiness.findConceptoFijoByTrabajadorConceptoTipoPersonalFrecuenciaTipoPersonal(idTrabajador, idConceptoTipoPersonal, idFrecuenciaTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public int verificarSiTrabajadorPuedeIngresar(long idPersonal, long idOrganismo, long idTipoPersonal)
    throws Exception
  {
    return this.trabajadorNoGenBusiness.verificarSiTrabajadorPuedeIngresar(idPersonal, idOrganismo, idTipoPersonal);
  }

  public int validarFechaIngreso(long idPersonal, long idOrganismo, Date fechaIngreso)
    throws Exception
  {
    return this.trabajadorNoGenBusiness.validarFechaIngreso(idPersonal, idOrganismo, fechaIngreso);
  }

  public int verificarSiTrabajadorPuedeReingresar(long idPersonal, long idOrganismo, long idTipoPersonal)
    throws Exception
  {
    return this.trabajadorNoGenBusiness.verificarSiTrabajadorPuedeReingresar(idPersonal, idOrganismo, idTipoPersonal);
  }

  public int buscarUltimoCodigoNomina(long idTipoPersonal)
    throws Exception
  {
    return this.trabajadorNoGenBusiness.buscarUltimoCodigoNomina(idTipoPersonal);
  }

  public boolean existeCuenta(int cedula, String cuentaNomina, String tipoCuenta)
    throws Exception
  {
    return this.trabajadorNoGenBusiness.existeCuenta(cedula, cuentaNomina, tipoCuenta);
  }

  public void generarPlanillaArc(long idTipoPersonal, String periodicidad, int anio)
    throws Exception
  {
    this.trabajadorNoGenBusiness.generarPlanillaArc(idTipoPersonal, periodicidad, anio);
  }

  public Collection findTrabajadorByPersonalAndEstatus(long idPersonal, String estatus, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorNoGenBusiness.findTrabajadorByPersonalAndEstatus(
        idPersonal, estatus, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoFijoByTrabajadorAndCodConcepto(long idTrabajador, String codConcepto) throws Exception
  {
    try { this.txn.open();
      return this.trabajadorNoGenBusiness.findConceptoFijoByTrabajadorAndCodConcepto(idTrabajador, codConcepto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public double findMontoConceptoFijoByTrabajadorQuincenal(long idTrabajador, int criterio)
    throws Exception
  {
    return this.trabajadorNoGenBusiness.findMontoConceptoFijoByTrabajadorQuincenal(idTrabajador, criterio);
  }

  public double findMontoConceptoProyectadoByTrabajadorQuincenal(long idTrabajador, int criterio) throws Exception
  {
    return this.trabajadorNoGenBusiness.findMontoConceptoProyectadoByTrabajadorQuincenal(idTrabajador, criterio);
  }

  public double findMontoConceptoFijoByTrabajadorQuincenal(long idTrabajador, int criterio, String reflejaMovimiento) throws Exception
  {
    return this.trabajadorNoGenBusiness.findMontoConceptoFijoByTrabajadorQuincenal(idTrabajador, criterio, reflejaMovimiento);
  }

  public double findMontoConceptoProyectadoByTrabajadorQuincenal(long idTrabajador, int criterio, String reflejaMovimiento) throws Exception
  {
    return this.trabajadorNoGenBusiness.findMontoConceptoProyectadoByTrabajadorQuincenal(idTrabajador, criterio, reflejaMovimiento);
  }
}