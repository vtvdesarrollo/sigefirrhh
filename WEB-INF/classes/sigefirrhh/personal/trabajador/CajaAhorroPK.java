package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class CajaAhorroPK
  implements Serializable
{
  public long idCajaAhorro;

  public CajaAhorroPK()
  {
  }

  public CajaAhorroPK(long idCajaAhorro)
  {
    this.idCajaAhorro = idCajaAhorro;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CajaAhorroPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CajaAhorroPK thatPK)
  {
    return 
      this.idCajaAhorro == thatPK.idCajaAhorro;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCajaAhorro)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCajaAhorro);
  }
}