package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class TrabajadorPK
  implements Serializable
{
  public long idTrabajador;

  public TrabajadorPK()
  {
  }

  public TrabajadorPK(long idTrabajador)
  {
    this.idTrabajador = idTrabajador;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TrabajadorPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TrabajadorPK thatPK)
  {
    return 
      this.idTrabajador == thatPK.idTrabajador;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTrabajador)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTrabajador);
  }
}