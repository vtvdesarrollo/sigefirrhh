package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class EmbargoPK
  implements Serializable
{
  public long idEmbargo;

  public EmbargoPK()
  {
  }

  public EmbargoPK(long idEmbargo)
  {
    this.idEmbargo = idEmbargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EmbargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EmbargoPK thatPK)
  {
    return 
      this.idEmbargo == thatPK.idEmbargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEmbargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEmbargo);
  }
}