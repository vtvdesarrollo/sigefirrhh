package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.LugarPago;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.registroCargos.RegistroCargos;

public class Trabajador
  implements Serializable, PersistenceCapable
{
  public static final Map LISTA_ESTATUS;
  public static final Map LISTA_MOVIMIENTO;
  public static final Map LISTA_SITUACION;
  public static final Map LISTA_SINO;
  public static final Map LISTA_PAGO;
  public static final Map LISTA_CUENTA;
  public static final Map LISTA_RIESGO;
  public static final Map LISTA_REGIMEN;
  private long idTrabajador;
  private Personal personal;
  private TipoPersonal tipoPersonal;
  private int cedula;
  private String codTipoPersonal;
  private String estatus;
  private String movimiento;
  private String situacion;
  private RegistroCargos registroCargos;
  private Cargo cargo;
  private Dependencia dependencia;
  private LugarPago lugarPago;
  private int codigoNomina;
  private String codCargo;
  private double sueldoBasico;
  private int paso;
  private Turno turno;
  private String riesgo;
  private String regimen;
  private Date fechaIngreso;
  private Date fechaTipoPersonal;
  private Date fechaIngresoApn;
  private Date fechaVacaciones;
  private Date fechaPrestaciones;
  private Date fechaAntiguedad;
  private Date fechaEgreso;
  private Date fechaJubilacion;
  private Date fechaSalidaSig;
  private Date fechaEntradaSig;
  private Date fechaIngresoCargo;
  private Date fechaEncargaduria;
  private Date fechaComisionServicio;
  private String organismoComisionServicio;
  private String formaPago;
  private Banco bancoNomina;
  private String tipoCtaNomina;
  private String cuentaNomina;
  private Banco bancoLph;
  private String cuentaLph;
  private Banco bancoFid;
  private String cuentaFid;
  private double porcentajeIslr;
  private String cotizaSso;
  private String cotizaSpf;
  private String cotizaLph;
  private String cotizaFju;
  private String dedProxNomina;
  private String parProxNomina;
  private double porcentajeJubilacion;
  private double baseJubilacion;
  private Date fechaFeVida;
  private String feVida;
  private double horasSemanales;
  private String codigoPatronal;
  private CausaMovimiento causaMovimiento;
  private Date fechaUltimoMovimiento;
  private int codigoNominaReal;
  private Dependencia dependenciaReal;
  private Cargo cargoReal;
  private Organismo organismo;
  private int mesIngreso;
  private int diaIngreso;
  private int anioIngreso;
  private int mesIngresoApn;
  private int diaIngresoApn;
  private int anioIngresoApn;
  private int mesVacaciones;
  private int diaVacaciones;
  private int anioVacaciones;
  private int mesPrestaciones;
  private int diaPrestaciones;
  private int anioPrestaciones;
  private int mesAntiguedad;
  private int diaAntiguedad;
  private int anioAntiguedad;
  private int mesJubilacion;
  private int diaJubilacion;
  private int anioJubilacion;
  private int mesEgreso;
  private int diaEgreso;
  private int anioEgreso;
  private int mesIngresoCargo;
  private int diaIngresoCargo;
  private int anioEntrada;
  private int mesEntrada;
  private int diaEntrada;
  private int anioIngresoCargo;
  private int lunesPrimera;
  private int lunesSegunda;
  private int lunesRetroactivo;
  private String hayRetroactivo;
  private String jubilacionPlanificada;
  private int diasTrabajados;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anioAntiguedad", "anioEgreso", "anioEntrada", "anioIngreso", "anioIngresoApn", "anioIngresoCargo", "anioJubilacion", "anioPrestaciones", "anioVacaciones", "bancoFid", "bancoLph", "bancoNomina", "baseJubilacion", "cargo", "cargoReal", "causaMovimiento", "cedula", "codCargo", "codTipoPersonal", "codigoNomina", "codigoNominaReal", "codigoPatronal", "cotizaFju", "cotizaLph", "cotizaSpf", "cotizaSso", "cuentaFid", "cuentaLph", "cuentaNomina", "dedProxNomina", "dependencia", "dependenciaReal", "diaAntiguedad", "diaEgreso", "diaEntrada", "diaIngreso", "diaIngresoApn", "diaIngresoCargo", "diaJubilacion", "diaPrestaciones", "diaVacaciones", "diasTrabajados", "estatus", "feVida", "fechaAntiguedad", "fechaComisionServicio", "fechaEgreso", "fechaEncargaduria", "fechaEntradaSig", "fechaFeVida", "fechaIngreso", "fechaIngresoApn", "fechaIngresoCargo", "fechaJubilacion", "fechaPrestaciones", "fechaSalidaSig", "fechaTipoPersonal", "fechaUltimoMovimiento", "fechaVacaciones", "formaPago", "hayRetroactivo", "horasSemanales", "idTrabajador", "jubilacionPlanificada", "lugarPago", "lunesPrimera", "lunesRetroactivo", "lunesSegunda", "mesAntiguedad", "mesEgreso", "mesEntrada", "mesIngreso", "mesIngresoApn", "mesIngresoCargo", "mesJubilacion", "mesPrestaciones", "mesVacaciones", "movimiento", "organismo", "organismoComisionServicio", "parProxNomina", "paso", "personal", "porcentajeIslr", "porcentajeJubilacion", "regimen", "registroCargos", "riesgo", "situacion", "sueldoBasico", "tipoCtaNomina", "tipoPersonal", "turno" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), Double.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), sunjdo$classForName$("sigefirrhh.base.registro.CausaMovimiento"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.LugarPago"), Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.registroCargos.RegistroCargos"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.base.definiciones.Turno") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 26, 26, 21, 26, 26, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 21, 26, 21, 21, 21, 26, 21, 21, 21, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Trabajador());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_MOVIMIENTO = 
      new LinkedHashMap();
    LISTA_SITUACION = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_PAGO = 
      new LinkedHashMap();
    LISTA_CUENTA = 
      new LinkedHashMap();
    LISTA_RIESGO = 
      new LinkedHashMap();
    LISTA_REGIMEN = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("A", "ACTIVO");
    LISTA_ESTATUS.put("E", "EGRESADO");
    LISTA_ESTATUS.put("S", "SUSPENDIDO");
    LISTA_ESTATUS.put("P", "PERMISO/LICENCIA SIN SUELDO");
    LISTA_MOVIMIENTO.put("A", "APROBADO");
    LISTA_MOVIMIENTO.put("T", "EN TRAMITE");
    LISTA_SITUACION.put("1", "NORMAL");
    LISTA_SITUACION.put("2", "ENCARGADO");
    LISTA_SITUACION.put("3", "COMISION SERVICIO");
    LISTA_SITUACION.put("4", "VACACIONES");
    LISTA_SITUACION.put("5", "PROCESO JUBILACION");
    LISTA_SITUACION.put("6", "SUPLENTE");
    LISTA_SITUACION.put("7", "EN DISPONIBILIDAD");
    LISTA_SITUACION.put("8", "LICENCIA CON SUELDO");
    LISTA_SITUACION.put("9", "LICENCIA SIN SUELDO");
    LISTA_SITUACION.put("10", "SERVICIO EXTERIOR");
    LISTA_SITUACION.put("11", "SERVICIO INTERNO");
    LISTA_SITUACION.put("12", "SUSPENDIDO");
    LISTA_SITUACION.put("13", "SUSPENDIDO CON SUELDO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
    LISTA_PAGO.put("1", "DEPOSITO");
    LISTA_PAGO.put("2", "CHEQUE");
    LISTA_PAGO.put("3", "EFECTIVO");
    LISTA_PAGO.put("T", "TRANSFERENCIA");
    LISTA_CUENTA.put("A", "AHORRO");
    LISTA_CUENTA.put("C", "CORRIENTE");
    LISTA_CUENTA.put("O", "OTRO");
    LISTA_CUENTA.put("N", "NO APLICA");
    LISTA_RIESGO.put("0", "NO APLICA");
    LISTA_RIESGO.put("1", "BAJO");
    LISTA_RIESGO.put("2", "MEDIO");
    LISTA_RIESGO.put("3", "ALTO");
    LISTA_REGIMEN.put("I", "INTEGRAL");
    LISTA_REGIMEN.put("P", "PARCIAL");
  }

  public Trabajador()
  {
    jdoSetestatus(this, "A");

    jdoSetmovimiento(this, "A");

    jdoSetsituacion(this, "1");

    jdoSetcodigoNomina(this, 1);

    jdoSetsueldoBasico(this, 0.0D);

    jdoSetpaso(this, 1);

    jdoSetriesgo(this, "1");

    jdoSetregimen(this, "I");

    jdoSetformaPago(this, "2");

    jdoSetporcentajeIslr(this, 0.0D);

    jdoSetcotizaSso(this, "S");

    jdoSetcotizaSpf(this, "S");

    jdoSetcotizaLph(this, "S");

    jdoSetcotizaFju(this, "S");

    jdoSetdedProxNomina(this, "S");

    jdoSetparProxNomina(this, "N");

    jdoSetfeVida(this, "S");

    jdoSetcodigoNominaReal(this, 1);

    jdoSetanioEntrada(this, 0);

    jdoSetmesEntrada(this, 0);

    jdoSetdiaEntrada(this, 0);

    jdoSetlunesPrimera(this, 0);

    jdoSetlunesSegunda(this, 0);

    jdoSetlunesRetroactivo(this, 0);

    jdoSethayRetroactivo(this, "N");

    jdoSetjubilacionPlanificada(this, "N");
  }

  public String toString()
  {
    String resultado = jdoGetestatus(this) + " - " + jdoGetpersonal(this).getCedula() + " " + jdoGetpersonal(this).getPrimerApellido() + " " + 
      jdoGetpersonal(this).getPrimerNombre() + " - " + 
      jdoGettipoPersonal(this).getNombre() + " - " + 
      jdoGetcodigoNomina(this) + " - " + 
      jdoGetcargo(this).getCodCargo() + " " + 
      jdoGetcargo(this).getDescripcionCargo();

    if (jdoGethorasSemanales(this) != 0.0D) {
      DecimalFormat b = new DecimalFormat();
      b.applyPattern("##,###,###.00");
      String a = b.format(jdoGethorasSemanales(this));
      resultado = resultado + " - " + a + " H - " + jdoGetdependencia(this).getCodDependencia() + " " + jdoGetdependencia(this).getNombre();
    }
    if (jdoGetpersonal(this).getCredencial() != null) {
      resultado = resultado + " - Cred: " + jdoGetpersonal(this).getCredencial();
    }
    return resultado;
  }

  public Banco getBancoFid()
  {
    return jdoGetbancoFid(this);
  }

  public Banco getBancoLph()
  {
    return jdoGetbancoLph(this);
  }

  public Banco getBancoNomina()
  {
    return jdoGetbancoNomina(this);
  }

  public double getBaseJubilacion()
  {
    return jdoGetbaseJubilacion(this);
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public Cargo getCargoReal()
  {
    return jdoGetcargoReal(this);
  }

  public CausaMovimiento getCausaMovimiento()
  {
    return jdoGetcausaMovimiento(this);
  }

  public int getCedula()
  {
    return jdoGetcedula(this);
  }

  public String getCodCargo()
  {
    return jdoGetcodCargo(this);
  }

  public int getCodigoNomina()
  {
    return jdoGetcodigoNomina(this);
  }

  public String getCodTipoPersonal()
  {
    return jdoGetcodTipoPersonal(this);
  }

  public String getCotizaFju()
  {
    return jdoGetcotizaFju(this);
  }

  public String getCotizaLph()
  {
    return jdoGetcotizaLph(this);
  }

  public String getCotizaSpf()
  {
    return jdoGetcotizaSpf(this);
  }

  public String getCotizaSso()
  {
    return jdoGetcotizaSso(this);
  }

  public String getCuentaFid()
  {
    return jdoGetcuentaFid(this);
  }

  public String getCuentaNomina()
  {
    return jdoGetcuentaNomina(this);
  }

  public String getDedProxNomina()
  {
    return jdoGetdedProxNomina(this);
  }

  public Dependencia getDependencia()
  {
    return jdoGetdependencia(this);
  }

  public Dependencia getDependenciaReal()
  {
    return jdoGetdependenciaReal(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public Date getFechaAntiguedad()
  {
    return jdoGetfechaAntiguedad(this);
  }

  public Date getFechaEgreso()
  {
    return jdoGetfechaEgreso(this);
  }

  public Date getFechaIngreso()
  {
    return jdoGetfechaIngreso(this);
  }

  public Date getFechaIngresoApn()
  {
    return jdoGetfechaIngresoApn(this);
  }

  public Date getFechaJubilacion()
  {
    return jdoGetfechaJubilacion(this);
  }

  public Date getFechaPrestaciones()
  {
    return jdoGetfechaPrestaciones(this);
  }

  public Date getFechaVacaciones()
  {
    return jdoGetfechaVacaciones(this);
  }

  public String getFeVida()
  {
    return jdoGetfeVida(this);
  }

  public String getFormaPago()
  {
    return jdoGetformaPago(this);
  }

  public long getIdTrabajador()
  {
    return jdoGetidTrabajador(this);
  }

  public String getMovimiento()
  {
    return jdoGetmovimiento(this);
  }

  public String getParProxNomina()
  {
    return jdoGetparProxNomina(this);
  }

  public int getPaso()
  {
    return jdoGetpaso(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public double getPorcentajeIslr()
  {
    return jdoGetporcentajeIslr(this);
  }

  public double getPorcentajeJubilacion()
  {
    return jdoGetporcentajeJubilacion(this);
  }

  public RegistroCargos getRegistroCargos()
  {
    return jdoGetregistroCargos(this);
  }

  public String getRiesgo()
  {
    return jdoGetriesgo(this);
  }

  public String getSituacion()
  {
    return jdoGetsituacion(this);
  }

  public double getSueldoBasico()
  {
    return jdoGetsueldoBasico(this);
  }

  public String getTipoCtaNomina()
  {
    return jdoGettipoCtaNomina(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public Turno getTurno()
  {
    return jdoGetturno(this);
  }

  public void setBancoFid(Banco banco)
  {
    jdoSetbancoFid(this, banco);
  }

  public void setBancoLph(Banco banco)
  {
    jdoSetbancoLph(this, banco);
  }

  public void setBancoNomina(Banco banco)
  {
    jdoSetbancoNomina(this, banco);
  }

  public void setBaseJubilacion(double d)
  {
    jdoSetbaseJubilacion(this, d);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public void setCargoReal(Cargo cargo)
  {
    jdoSetcargoReal(this, cargo);
  }

  public void setCausaMovimiento(CausaMovimiento movimiento)
  {
    jdoSetcausaMovimiento(this, movimiento);
  }

  public void setCedula(int i)
  {
    jdoSetcedula(this, i);
  }

  public void setCodCargo(String string)
  {
    jdoSetcodCargo(this, string);
  }

  public void setCodigoNomina(int i)
  {
    jdoSetcodigoNomina(this, i);
  }

  public void setCodTipoPersonal(String string)
  {
    jdoSetcodTipoPersonal(this, string);
  }

  public void setCotizaFju(String string)
  {
    jdoSetcotizaFju(this, string);
  }

  public void setCotizaLph(String string)
  {
    jdoSetcotizaLph(this, string);
  }

  public void setCotizaSpf(String string)
  {
    jdoSetcotizaSpf(this, string);
  }

  public void setCotizaSso(String string)
  {
    jdoSetcotizaSso(this, string);
  }

  public void setCuentaFid(String string)
  {
    jdoSetcuentaFid(this, string);
  }

  public void setCuentaNomina(String string)
  {
    jdoSetcuentaNomina(this, string);
  }

  public void setDedProxNomina(String string)
  {
    jdoSetdedProxNomina(this, string);
  }

  public void setDependencia(Dependencia dependencia)
  {
    jdoSetdependencia(this, dependencia);
  }

  public void setDependenciaReal(Dependencia dependencia)
  {
    jdoSetdependenciaReal(this, dependencia);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setFechaAntiguedad(Date date)
  {
    jdoSetfechaAntiguedad(this, date);
  }

  public void setFechaEgreso(Date date)
  {
    jdoSetfechaEgreso(this, date);
  }

  public void setFechaIngreso(Date date)
  {
    jdoSetfechaIngreso(this, date);
  }

  public void setFechaIngresoApn(Date date)
  {
    jdoSetfechaIngresoApn(this, date);
  }

  public void setFechaJubilacion(Date date)
  {
    jdoSetfechaJubilacion(this, date);
  }

  public void setFechaPrestaciones(Date date)
  {
    jdoSetfechaPrestaciones(this, date);
  }

  public void setFechaVacaciones(Date date)
  {
    jdoSetfechaVacaciones(this, date);
  }

  public void setFeVida(String string)
  {
    jdoSetfeVida(this, string);
  }

  public void setFormaPago(String string)
  {
    jdoSetformaPago(this, string);
  }

  public void setIdTrabajador(long l)
  {
    jdoSetidTrabajador(this, l);
  }

  public void setMovimiento(String string)
  {
    jdoSetmovimiento(this, string);
  }

  public void setParProxNomina(String string)
  {
    jdoSetparProxNomina(this, string);
  }

  public void setPaso(int i)
  {
    jdoSetpaso(this, i);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setPorcentajeIslr(double d)
  {
    jdoSetporcentajeIslr(this, d);
  }

  public void setPorcentajeJubilacion(double d)
  {
    jdoSetporcentajeJubilacion(this, d);
  }

  public void setRegistroCargos(RegistroCargos cargos)
  {
    jdoSetregistroCargos(this, cargos);
  }

  public void setRiesgo(String string)
  {
    jdoSetriesgo(this, string);
  }

  public void setSituacion(String string)
  {
    jdoSetsituacion(this, string);
  }

  public void setSueldoBasico(double d)
  {
    jdoSetsueldoBasico(this, d);
  }

  public void setTipoCtaNomina(String string)
  {
    jdoSettipoCtaNomina(this, string);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public void setTurno(Turno turno)
  {
    jdoSetturno(this, turno);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public LugarPago getLugarPago()
  {
    return jdoGetlugarPago(this);
  }

  public void setLugarPago(LugarPago pago)
  {
    jdoSetlugarPago(this, pago);
  }

  public String getCuentaLph()
  {
    return jdoGetcuentaLph(this);
  }

  public void setCuentaLph(String string)
  {
    jdoSetcuentaLph(this, string);
  }

  public String getRegimen()
  {
    return jdoGetregimen(this);
  }

  public void setRegimen(String string)
  {
    jdoSetregimen(this, string);
  }

  public int getAnioAntiguedad()
  {
    return jdoGetanioAntiguedad(this);
  }

  public int getAnioEgreso()
  {
    return jdoGetanioEgreso(this);
  }

  public int getAnioIngreso()
  {
    return jdoGetanioIngreso(this);
  }

  public int getAnioIngresoApn()
  {
    return jdoGetanioIngresoApn(this);
  }

  public int getAnioJubilacion()
  {
    return jdoGetanioJubilacion(this);
  }

  public int getAnioPrestaciones()
  {
    return jdoGetanioPrestaciones(this);
  }

  public int getAnioVacaciones()
  {
    return jdoGetanioVacaciones(this);
  }

  public int getDiaAntiguedad()
  {
    return jdoGetdiaAntiguedad(this);
  }

  public int getDiaEgreso()
  {
    return jdoGetdiaEgreso(this);
  }

  public int getDiaIngreso()
  {
    return jdoGetdiaIngreso(this);
  }

  public int getDiaIngresoApn()
  {
    return jdoGetdiaIngresoApn(this);
  }

  public int getDiaJubilacion()
  {
    return jdoGetdiaJubilacion(this);
  }

  public int getDiaPrestaciones()
  {
    return jdoGetdiaPrestaciones(this);
  }

  public int getDiaVacaciones()
  {
    return jdoGetdiaVacaciones(this);
  }

  public int getMesAntiguedad()
  {
    return jdoGetmesAntiguedad(this);
  }

  public int getMesEgreso()
  {
    return jdoGetmesEgreso(this);
  }

  public int getMesIngreso()
  {
    return jdoGetmesIngreso(this);
  }

  public int getMesIngresoApn()
  {
    return jdoGetmesIngresoApn(this);
  }

  public int getMesJubilacion()
  {
    return jdoGetmesJubilacion(this);
  }

  public int getMesPrestaciones()
  {
    return jdoGetmesPrestaciones(this);
  }

  public int getMesVacaciones()
  {
    return jdoGetmesVacaciones(this);
  }

  public void setAnioAntiguedad(int i)
  {
    jdoSetanioAntiguedad(this, i);
  }

  public void setAnioEgreso(int i)
  {
    jdoSetanioEgreso(this, i);
  }

  public void setAnioIngreso(int i)
  {
    jdoSetanioIngreso(this, i);
  }

  public void setAnioIngresoApn(int i)
  {
    jdoSetanioIngresoApn(this, i);
  }

  public void setAnioJubilacion(int i)
  {
    jdoSetanioJubilacion(this, i);
  }

  public void setAnioPrestaciones(int i)
  {
    jdoSetanioPrestaciones(this, i);
  }

  public void setAnioVacaciones(int i)
  {
    jdoSetanioVacaciones(this, i);
  }

  public void setDiaAntiguedad(int i)
  {
    jdoSetdiaAntiguedad(this, i);
  }

  public void setDiaEgreso(int i)
  {
    jdoSetdiaEgreso(this, i);
  }

  public void setDiaIngreso(int i)
  {
    jdoSetdiaIngreso(this, i);
  }

  public void setDiaIngresoApn(int i)
  {
    jdoSetdiaIngresoApn(this, i);
  }

  public void setDiaJubilacion(int i)
  {
    jdoSetdiaJubilacion(this, i);
  }

  public void setDiaPrestaciones(int i)
  {
    jdoSetdiaPrestaciones(this, i);
  }

  public void setDiaVacaciones(int i)
  {
    jdoSetdiaVacaciones(this, i);
  }

  public void setMesAntiguedad(int i)
  {
    jdoSetmesAntiguedad(this, i);
  }

  public void setMesEgreso(int i)
  {
    jdoSetmesEgreso(this, i);
  }

  public void setMesIngreso(int i)
  {
    jdoSetmesIngreso(this, i);
  }

  public void setMesIngresoApn(int i)
  {
    jdoSetmesIngresoApn(this, i);
  }

  public void setMesJubilacion(int i)
  {
    jdoSetmesJubilacion(this, i);
  }

  public void setMesPrestaciones(int i)
  {
    jdoSetmesPrestaciones(this, i);
  }

  public void setMesVacaciones(int i)
  {
    jdoSetmesVacaciones(this, i);
  }

  public Date getFechaEntradaSig()
  {
    return jdoGetfechaEntradaSig(this);
  }

  public Date getFechaSalidaSig()
  {
    return jdoGetfechaSalidaSig(this);
  }

  public void setFechaEntradaSig(Date date)
  {
    jdoSetfechaEntradaSig(this, date);
  }

  public void setFechaSalidaSig(Date date)
  {
    jdoSetfechaSalidaSig(this, date);
  }

  public int getCodigoNominaReal()
  {
    return jdoGetcodigoNominaReal(this);
  }

  public void setCodigoNominaReal(int i)
  {
    jdoSetcodigoNominaReal(this, i);
  }

  public int getLunesPrimera()
  {
    return jdoGetlunesPrimera(this);
  }

  public int getLunesRetroactivo()
  {
    return jdoGetlunesRetroactivo(this);
  }

  public int getLunesSegunda()
  {
    return jdoGetlunesSegunda(this);
  }

  public void setLunesPrimera(int i)
  {
    jdoSetlunesPrimera(this, i);
  }

  public void setLunesRetroactivo(int i)
  {
    jdoSetlunesRetroactivo(this, i);
  }

  public void setLunesSegunda(int i)
  {
    jdoSetlunesSegunda(this, i);
  }

  public String getHayRetroactivo() {
    return jdoGethayRetroactivo(this);
  }
  public void setHayRetroactivo(String hayRetroactivo) {
    jdoSethayRetroactivo(this, hayRetroactivo);
  }
  public int getDiasTrabajados() {
    return jdoGetdiasTrabajados(this);
  }
  public void setDiasTrabajados(int diasTrabajados) {
    jdoSetdiasTrabajados(this, diasTrabajados);
  }
  public double getHorasSemanales() {
    return jdoGethorasSemanales(this);
  }
  public void setHorasSemanales(double horasSemanales) {
    jdoSethorasSemanales(this, horasSemanales);
  }
  public int getAnioIngresoCargo() {
    return jdoGetanioIngresoCargo(this);
  }
  public void setAnioIngresoCargo(int anioIngresoCargo) {
    jdoSetanioIngresoCargo(this, anioIngresoCargo);
  }
  public int getDiaIngresoCargo() {
    return jdoGetdiaIngresoCargo(this);
  }
  public void setDiaIngresoCargo(int diaIngresoCargo) {
    jdoSetdiaIngresoCargo(this, diaIngresoCargo);
  }
  public Date getFechaIngresoCargo() {
    return jdoGetfechaIngresoCargo(this);
  }
  public void setFechaIngresoCargo(Date fechaIngresoCargo) {
    jdoSetfechaIngresoCargo(this, fechaIngresoCargo);
  }
  public int getMesIngresoCargo() {
    return jdoGetmesIngresoCargo(this);
  }
  public void setMesIngresoCargo(int mesIngresoCargo) {
    jdoSetmesIngresoCargo(this, mesIngresoCargo);
  }
  public Date getFechaFeVida() {
    return jdoGetfechaFeVida(this);
  }
  public void setFechaFeVida(Date fechaFeVida) {
    jdoSetfechaFeVida(this, fechaFeVida);
  }

  public Date getFechaEncargaduria() {
    return jdoGetfechaEncargaduria(this);
  }

  public void setFechaEncargaduria(Date fechaEncargaduria) {
    jdoSetfechaEncargaduria(this, fechaEncargaduria);
  }

  public Date getFechaComisionServicio() {
    return jdoGetfechaComisionServicio(this);
  }

  public void setFechaComisionServicio(Date fechaComisionServicio) {
    jdoSetfechaComisionServicio(this, fechaComisionServicio);
  }

  public String getOrganismoComisionServicio() {
    return jdoGetorganismoComisionServicio(this);
  }

  public void setOrganismoComisionServicio(String organismoComisionServicio) {
    jdoSetorganismoComisionServicio(this, organismoComisionServicio);
  }

  public Date getFechaTipoPersonal() {
    return jdoGetfechaTipoPersonal(this);
  }

  public void setFechaTipoPersonal(Date fechaTipoPersonal) {
    jdoSetfechaTipoPersonal(this, fechaTipoPersonal);
  }

  public Date getFechaUltimoMovimiento() {
    return jdoGetfechaUltimoMovimiento(this);
  }

  public void setFechaUltimoMovimiento(Date fechaUltimoMovimiento) {
    jdoSetfechaUltimoMovimiento(this, fechaUltimoMovimiento);
  }

  public String getCodigoPatronal() {
    return jdoGetcodigoPatronal(this);
  }

  public void setCodigoPatronal(String codigoPatronal) {
    jdoSetcodigoPatronal(this, codigoPatronal);
  }

  public int getAnioEntrada() {
    return jdoGetanioEntrada(this);
  }
  public void setAnioEntrada(int anioEntrada) {
    jdoSetanioEntrada(this, anioEntrada);
  }
  public int getDiaEntrada() {
    return jdoGetdiaEntrada(this);
  }
  public void setDiaEntrada(int diaEntrada) {
    jdoSetdiaEntrada(this, diaEntrada);
  }
  public int getMesEntrada() {
    return jdoGetmesEntrada(this);
  }
  public void setMesEntrada(int mesEntrada) {
    jdoSetmesEntrada(this, mesEntrada);
  }
  public String getJubilacionPlanificada() {
    return jdoGetjubilacionPlanificada(this);
  }
  public void setJubilacionPlanificada(String jubilacionPlanificada) {
    jdoSetjubilacionPlanificada(this, jubilacionPlanificada);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 93;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Trabajador localTrabajador = new Trabajador();
    localTrabajador.jdoFlags = 1;
    localTrabajador.jdoStateManager = paramStateManager;
    return localTrabajador;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Trabajador localTrabajador = new Trabajador();
    localTrabajador.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTrabajador.jdoFlags = 1;
    localTrabajador.jdoStateManager = paramStateManager;
    return localTrabajador;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioAntiguedad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioEgreso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioEntrada);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioIngreso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioIngresoApn);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioIngresoCargo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioJubilacion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioPrestaciones);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioVacaciones);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.bancoFid);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.bancoLph);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.bancoNomina);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseJubilacion);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargoReal);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.causaMovimiento);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargo);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTipoPersonal);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNomina);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNominaReal);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoPatronal);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cotizaFju);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cotizaLph);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cotizaSpf);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cotizaSso);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cuentaFid);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cuentaLph);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cuentaNomina);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.dedProxNomina);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependenciaReal);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diaAntiguedad);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diaEgreso);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diaEntrada);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diaIngreso);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diaIngresoApn);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diaIngresoCargo);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diaJubilacion);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diaPrestaciones);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diaVacaciones);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasTrabajados);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.feVida);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaAntiguedad);
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaComisionServicio);
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEgreso);
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEncargaduria);
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEntradaSig);
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFeVida);
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaIngreso);
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaIngresoApn);
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaIngresoCargo);
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaJubilacion);
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaPrestaciones);
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaSalidaSig);
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaTipoPersonal);
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaUltimoMovimiento);
      return;
    case 58:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVacaciones);
      return;
    case 59:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.formaPago);
      return;
    case 60:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.hayRetroactivo);
      return;
    case 61:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horasSemanales);
      return;
    case 62:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTrabajador);
      return;
    case 63:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.jubilacionPlanificada);
      return;
    case 64:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.lugarPago);
      return;
    case 65:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.lunesPrimera);
      return;
    case 66:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.lunesRetroactivo);
      return;
    case 67:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.lunesSegunda);
      return;
    case 68:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesAntiguedad);
      return;
    case 69:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesEgreso);
      return;
    case 70:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesEntrada);
      return;
    case 71:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesIngreso);
      return;
    case 72:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesIngresoApn);
      return;
    case 73:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesIngresoCargo);
      return;
    case 74:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesJubilacion);
      return;
    case 75:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesPrestaciones);
      return;
    case 76:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesVacaciones);
      return;
    case 77:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.movimiento);
      return;
    case 78:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 79:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.organismoComisionServicio);
      return;
    case 80:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.parProxNomina);
      return;
    case 81:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.paso);
      return;
    case 82:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 83:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeIslr);
      return;
    case 84:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeJubilacion);
      return;
    case 85:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.regimen);
      return;
    case 86:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registroCargos);
      return;
    case 87:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.riesgo);
      return;
    case 88:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.situacion);
      return;
    case 89:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoBasico);
      return;
    case 90:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoCtaNomina);
      return;
    case 91:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 92:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.turno);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioAntiguedad = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioEgreso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioEntrada = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioIngreso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioIngresoApn = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioIngresoCargo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioJubilacion = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioPrestaciones = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioVacaciones = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bancoFid = ((Banco)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bancoLph = ((Banco)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bancoNomina = ((Banco)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseJubilacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoReal = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaMovimiento = ((CausaMovimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTipoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNominaReal = localStateManager.replacingIntField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoPatronal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cotizaFju = localStateManager.replacingStringField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cotizaLph = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cotizaSpf = localStateManager.replacingStringField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cotizaSso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuentaFid = localStateManager.replacingStringField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuentaLph = localStateManager.replacingStringField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuentaNomina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dedProxNomina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependenciaReal = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diaAntiguedad = localStateManager.replacingIntField(this, paramInt);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diaEgreso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diaEntrada = localStateManager.replacingIntField(this, paramInt);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diaIngreso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diaIngresoApn = localStateManager.replacingIntField(this, paramInt);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diaIngresoCargo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diaJubilacion = localStateManager.replacingIntField(this, paramInt);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diaPrestaciones = localStateManager.replacingIntField(this, paramInt);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diaVacaciones = localStateManager.replacingIntField(this, paramInt);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasTrabajados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.feVida = localStateManager.replacingStringField(this, paramInt);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaAntiguedad = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaComisionServicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEgreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEncargaduria = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEntradaSig = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFeVida = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaIngreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaIngresoApn = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaIngresoCargo = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaJubilacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaPrestaciones = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaSalidaSig = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaTipoPersonal = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaUltimoMovimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 58:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVacaciones = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 59:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.formaPago = localStateManager.replacingStringField(this, paramInt);
      return;
    case 60:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.hayRetroactivo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 61:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horasSemanales = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 62:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTrabajador = localStateManager.replacingLongField(this, paramInt);
      return;
    case 63:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.jubilacionPlanificada = localStateManager.replacingStringField(this, paramInt);
      return;
    case 64:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lugarPago = ((LugarPago)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 65:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lunesPrimera = localStateManager.replacingIntField(this, paramInt);
      return;
    case 66:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lunesRetroactivo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 67:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lunesSegunda = localStateManager.replacingIntField(this, paramInt);
      return;
    case 68:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesAntiguedad = localStateManager.replacingIntField(this, paramInt);
      return;
    case 69:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesEgreso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 70:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesEntrada = localStateManager.replacingIntField(this, paramInt);
      return;
    case 71:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesIngreso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 72:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesIngresoApn = localStateManager.replacingIntField(this, paramInt);
      return;
    case 73:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesIngresoCargo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 74:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesJubilacion = localStateManager.replacingIntField(this, paramInt);
      return;
    case 75:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesPrestaciones = localStateManager.replacingIntField(this, paramInt);
      return;
    case 76:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesVacaciones = localStateManager.replacingIntField(this, paramInt);
      return;
    case 77:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.movimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 78:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 79:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismoComisionServicio = localStateManager.replacingStringField(this, paramInt);
      return;
    case 80:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.parProxNomina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 81:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.paso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 82:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 83:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeIslr = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 84:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeJubilacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 85:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.regimen = localStateManager.replacingStringField(this, paramInt);
      return;
    case 86:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registroCargos = ((RegistroCargos)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 87:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.riesgo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 88:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.situacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 89:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoBasico = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 90:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCtaNomina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 91:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 92:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.turno = ((Turno)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Trabajador paramTrabajador, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.anioAntiguedad = paramTrabajador.anioAntiguedad;
      return;
    case 1:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.anioEgreso = paramTrabajador.anioEgreso;
      return;
    case 2:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.anioEntrada = paramTrabajador.anioEntrada;
      return;
    case 3:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.anioIngreso = paramTrabajador.anioIngreso;
      return;
    case 4:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.anioIngresoApn = paramTrabajador.anioIngresoApn;
      return;
    case 5:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.anioIngresoCargo = paramTrabajador.anioIngresoCargo;
      return;
    case 6:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.anioJubilacion = paramTrabajador.anioJubilacion;
      return;
    case 7:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.anioPrestaciones = paramTrabajador.anioPrestaciones;
      return;
    case 8:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.anioVacaciones = paramTrabajador.anioVacaciones;
      return;
    case 9:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.bancoFid = paramTrabajador.bancoFid;
      return;
    case 10:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.bancoLph = paramTrabajador.bancoLph;
      return;
    case 11:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.bancoNomina = paramTrabajador.bancoNomina;
      return;
    case 12:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.baseJubilacion = paramTrabajador.baseJubilacion;
      return;
    case 13:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramTrabajador.cargo;
      return;
    case 14:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.cargoReal = paramTrabajador.cargoReal;
      return;
    case 15:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.causaMovimiento = paramTrabajador.causaMovimiento;
      return;
    case 16:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramTrabajador.cedula;
      return;
    case 17:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.codCargo = paramTrabajador.codCargo;
      return;
    case 18:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.codTipoPersonal = paramTrabajador.codTipoPersonal;
      return;
    case 19:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNomina = paramTrabajador.codigoNomina;
      return;
    case 20:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNominaReal = paramTrabajador.codigoNominaReal;
      return;
    case 21:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.codigoPatronal = paramTrabajador.codigoPatronal;
      return;
    case 22:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.cotizaFju = paramTrabajador.cotizaFju;
      return;
    case 23:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.cotizaLph = paramTrabajador.cotizaLph;
      return;
    case 24:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.cotizaSpf = paramTrabajador.cotizaSpf;
      return;
    case 25:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.cotizaSso = paramTrabajador.cotizaSso;
      return;
    case 26:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.cuentaFid = paramTrabajador.cuentaFid;
      return;
    case 27:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.cuentaLph = paramTrabajador.cuentaLph;
      return;
    case 28:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.cuentaNomina = paramTrabajador.cuentaNomina;
      return;
    case 29:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.dedProxNomina = paramTrabajador.dedProxNomina;
      return;
    case 30:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramTrabajador.dependencia;
      return;
    case 31:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.dependenciaReal = paramTrabajador.dependenciaReal;
      return;
    case 32:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.diaAntiguedad = paramTrabajador.diaAntiguedad;
      return;
    case 33:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.diaEgreso = paramTrabajador.diaEgreso;
      return;
    case 34:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.diaEntrada = paramTrabajador.diaEntrada;
      return;
    case 35:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.diaIngreso = paramTrabajador.diaIngreso;
      return;
    case 36:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.diaIngresoApn = paramTrabajador.diaIngresoApn;
      return;
    case 37:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.diaIngresoCargo = paramTrabajador.diaIngresoCargo;
      return;
    case 38:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.diaJubilacion = paramTrabajador.diaJubilacion;
      return;
    case 39:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.diaPrestaciones = paramTrabajador.diaPrestaciones;
      return;
    case 40:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.diaVacaciones = paramTrabajador.diaVacaciones;
      return;
    case 41:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.diasTrabajados = paramTrabajador.diasTrabajados;
      return;
    case 42:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramTrabajador.estatus;
      return;
    case 43:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.feVida = paramTrabajador.feVida;
      return;
    case 44:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaAntiguedad = paramTrabajador.fechaAntiguedad;
      return;
    case 45:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaComisionServicio = paramTrabajador.fechaComisionServicio;
      return;
    case 46:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEgreso = paramTrabajador.fechaEgreso;
      return;
    case 47:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEncargaduria = paramTrabajador.fechaEncargaduria;
      return;
    case 48:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEntradaSig = paramTrabajador.fechaEntradaSig;
      return;
    case 49:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFeVida = paramTrabajador.fechaFeVida;
      return;
    case 50:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaIngreso = paramTrabajador.fechaIngreso;
      return;
    case 51:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaIngresoApn = paramTrabajador.fechaIngresoApn;
      return;
    case 52:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaIngresoCargo = paramTrabajador.fechaIngresoCargo;
      return;
    case 53:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaJubilacion = paramTrabajador.fechaJubilacion;
      return;
    case 54:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaPrestaciones = paramTrabajador.fechaPrestaciones;
      return;
    case 55:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaSalidaSig = paramTrabajador.fechaSalidaSig;
      return;
    case 56:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaTipoPersonal = paramTrabajador.fechaTipoPersonal;
      return;
    case 57:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaUltimoMovimiento = paramTrabajador.fechaUltimoMovimiento;
      return;
    case 58:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVacaciones = paramTrabajador.fechaVacaciones;
      return;
    case 59:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.formaPago = paramTrabajador.formaPago;
      return;
    case 60:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.hayRetroactivo = paramTrabajador.hayRetroactivo;
      return;
    case 61:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.horasSemanales = paramTrabajador.horasSemanales;
      return;
    case 62:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.idTrabajador = paramTrabajador.idTrabajador;
      return;
    case 63:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.jubilacionPlanificada = paramTrabajador.jubilacionPlanificada;
      return;
    case 64:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.lugarPago = paramTrabajador.lugarPago;
      return;
    case 65:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.lunesPrimera = paramTrabajador.lunesPrimera;
      return;
    case 66:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.lunesRetroactivo = paramTrabajador.lunesRetroactivo;
      return;
    case 67:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.lunesSegunda = paramTrabajador.lunesSegunda;
      return;
    case 68:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.mesAntiguedad = paramTrabajador.mesAntiguedad;
      return;
    case 69:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.mesEgreso = paramTrabajador.mesEgreso;
      return;
    case 70:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.mesEntrada = paramTrabajador.mesEntrada;
      return;
    case 71:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.mesIngreso = paramTrabajador.mesIngreso;
      return;
    case 72:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.mesIngresoApn = paramTrabajador.mesIngresoApn;
      return;
    case 73:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.mesIngresoCargo = paramTrabajador.mesIngresoCargo;
      return;
    case 74:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.mesJubilacion = paramTrabajador.mesJubilacion;
      return;
    case 75:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.mesPrestaciones = paramTrabajador.mesPrestaciones;
      return;
    case 76:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.mesVacaciones = paramTrabajador.mesVacaciones;
      return;
    case 77:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.movimiento = paramTrabajador.movimiento;
      return;
    case 78:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramTrabajador.organismo;
      return;
    case 79:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.organismoComisionServicio = paramTrabajador.organismoComisionServicio;
      return;
    case 80:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.parProxNomina = paramTrabajador.parProxNomina;
      return;
    case 81:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.paso = paramTrabajador.paso;
      return;
    case 82:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramTrabajador.personal;
      return;
    case 83:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeIslr = paramTrabajador.porcentajeIslr;
      return;
    case 84:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeJubilacion = paramTrabajador.porcentajeJubilacion;
      return;
    case 85:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.regimen = paramTrabajador.regimen;
      return;
    case 86:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.registroCargos = paramTrabajador.registroCargos;
      return;
    case 87:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.riesgo = paramTrabajador.riesgo;
      return;
    case 88:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.situacion = paramTrabajador.situacion;
      return;
    case 89:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoBasico = paramTrabajador.sueldoBasico;
      return;
    case 90:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCtaNomina = paramTrabajador.tipoCtaNomina;
      return;
    case 91:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramTrabajador.tipoPersonal;
      return;
    case 92:
      if (paramTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.turno = paramTrabajador.turno;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Trabajador))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Trabajador localTrabajador = (Trabajador)paramObject;
    if (localTrabajador.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTrabajador, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TrabajadorPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TrabajadorPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrabajadorPK))
      throw new IllegalArgumentException("arg1");
    TrabajadorPK localTrabajadorPK = (TrabajadorPK)paramObject;
    localTrabajadorPK.idTrabajador = this.idTrabajador;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrabajadorPK))
      throw new IllegalArgumentException("arg1");
    TrabajadorPK localTrabajadorPK = (TrabajadorPK)paramObject;
    this.idTrabajador = localTrabajadorPK.idTrabajador;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrabajadorPK))
      throw new IllegalArgumentException("arg2");
    TrabajadorPK localTrabajadorPK = (TrabajadorPK)paramObject;
    localTrabajadorPK.idTrabajador = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 62);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrabajadorPK))
      throw new IllegalArgumentException("arg2");
    TrabajadorPK localTrabajadorPK = (TrabajadorPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 62, localTrabajadorPK.idTrabajador);
  }

  private static final int jdoGetanioAntiguedad(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.anioAntiguedad;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.anioAntiguedad;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 0))
      return paramTrabajador.anioAntiguedad;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 0, paramTrabajador.anioAntiguedad);
  }

  private static final void jdoSetanioAntiguedad(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.anioAntiguedad = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.anioAntiguedad = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 0, paramTrabajador.anioAntiguedad, paramInt);
  }

  private static final int jdoGetanioEgreso(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.anioEgreso;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.anioEgreso;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 1))
      return paramTrabajador.anioEgreso;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 1, paramTrabajador.anioEgreso);
  }

  private static final void jdoSetanioEgreso(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.anioEgreso = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.anioEgreso = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 1, paramTrabajador.anioEgreso, paramInt);
  }

  private static final int jdoGetanioEntrada(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.anioEntrada;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.anioEntrada;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 2))
      return paramTrabajador.anioEntrada;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 2, paramTrabajador.anioEntrada);
  }

  private static final void jdoSetanioEntrada(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.anioEntrada = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.anioEntrada = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 2, paramTrabajador.anioEntrada, paramInt);
  }

  private static final int jdoGetanioIngreso(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.anioIngreso;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.anioIngreso;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 3))
      return paramTrabajador.anioIngreso;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 3, paramTrabajador.anioIngreso);
  }

  private static final void jdoSetanioIngreso(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.anioIngreso = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.anioIngreso = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 3, paramTrabajador.anioIngreso, paramInt);
  }

  private static final int jdoGetanioIngresoApn(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.anioIngresoApn;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.anioIngresoApn;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 4))
      return paramTrabajador.anioIngresoApn;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 4, paramTrabajador.anioIngresoApn);
  }

  private static final void jdoSetanioIngresoApn(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.anioIngresoApn = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.anioIngresoApn = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 4, paramTrabajador.anioIngresoApn, paramInt);
  }

  private static final int jdoGetanioIngresoCargo(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.anioIngresoCargo;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.anioIngresoCargo;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 5))
      return paramTrabajador.anioIngresoCargo;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 5, paramTrabajador.anioIngresoCargo);
  }

  private static final void jdoSetanioIngresoCargo(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.anioIngresoCargo = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.anioIngresoCargo = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 5, paramTrabajador.anioIngresoCargo, paramInt);
  }

  private static final int jdoGetanioJubilacion(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.anioJubilacion;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.anioJubilacion;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 6))
      return paramTrabajador.anioJubilacion;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 6, paramTrabajador.anioJubilacion);
  }

  private static final void jdoSetanioJubilacion(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.anioJubilacion = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.anioJubilacion = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 6, paramTrabajador.anioJubilacion, paramInt);
  }

  private static final int jdoGetanioPrestaciones(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.anioPrestaciones;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.anioPrestaciones;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 7))
      return paramTrabajador.anioPrestaciones;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 7, paramTrabajador.anioPrestaciones);
  }

  private static final void jdoSetanioPrestaciones(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.anioPrestaciones = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.anioPrestaciones = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 7, paramTrabajador.anioPrestaciones, paramInt);
  }

  private static final int jdoGetanioVacaciones(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.anioVacaciones;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.anioVacaciones;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 8))
      return paramTrabajador.anioVacaciones;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 8, paramTrabajador.anioVacaciones);
  }

  private static final void jdoSetanioVacaciones(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.anioVacaciones = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.anioVacaciones = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 8, paramTrabajador.anioVacaciones, paramInt);
  }

  private static final Banco jdoGetbancoFid(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.bancoFid;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 9))
      return paramTrabajador.bancoFid;
    return (Banco)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 9, paramTrabajador.bancoFid);
  }

  private static final void jdoSetbancoFid(Trabajador paramTrabajador, Banco paramBanco)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.bancoFid = paramBanco;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 9, paramTrabajador.bancoFid, paramBanco);
  }

  private static final Banco jdoGetbancoLph(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.bancoLph;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 10))
      return paramTrabajador.bancoLph;
    return (Banco)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 10, paramTrabajador.bancoLph);
  }

  private static final void jdoSetbancoLph(Trabajador paramTrabajador, Banco paramBanco)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.bancoLph = paramBanco;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 10, paramTrabajador.bancoLph, paramBanco);
  }

  private static final Banco jdoGetbancoNomina(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.bancoNomina;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 11))
      return paramTrabajador.bancoNomina;
    return (Banco)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 11, paramTrabajador.bancoNomina);
  }

  private static final void jdoSetbancoNomina(Trabajador paramTrabajador, Banco paramBanco)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.bancoNomina = paramBanco;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 11, paramTrabajador.bancoNomina, paramBanco);
  }

  private static final double jdoGetbaseJubilacion(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.baseJubilacion;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.baseJubilacion;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 12))
      return paramTrabajador.baseJubilacion;
    return localStateManager.getDoubleField(paramTrabajador, jdoInheritedFieldCount + 12, paramTrabajador.baseJubilacion);
  }

  private static final void jdoSetbaseJubilacion(Trabajador paramTrabajador, double paramDouble)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.baseJubilacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.baseJubilacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrabajador, jdoInheritedFieldCount + 12, paramTrabajador.baseJubilacion, paramDouble);
  }

  private static final Cargo jdoGetcargo(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.cargo;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 13))
      return paramTrabajador.cargo;
    return (Cargo)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 13, paramTrabajador.cargo);
  }

  private static final void jdoSetcargo(Trabajador paramTrabajador, Cargo paramCargo)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 13, paramTrabajador.cargo, paramCargo);
  }

  private static final Cargo jdoGetcargoReal(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.cargoReal;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 14))
      return paramTrabajador.cargoReal;
    return (Cargo)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 14, paramTrabajador.cargoReal);
  }

  private static final void jdoSetcargoReal(Trabajador paramTrabajador, Cargo paramCargo)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.cargoReal = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 14, paramTrabajador.cargoReal, paramCargo);
  }

  private static final CausaMovimiento jdoGetcausaMovimiento(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.causaMovimiento;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 15))
      return paramTrabajador.causaMovimiento;
    return (CausaMovimiento)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 15, paramTrabajador.causaMovimiento);
  }

  private static final void jdoSetcausaMovimiento(Trabajador paramTrabajador, CausaMovimiento paramCausaMovimiento)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.causaMovimiento = paramCausaMovimiento;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 15, paramTrabajador.causaMovimiento, paramCausaMovimiento);
  }

  private static final int jdoGetcedula(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.cedula;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.cedula;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 16))
      return paramTrabajador.cedula;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 16, paramTrabajador.cedula);
  }

  private static final void jdoSetcedula(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 16, paramTrabajador.cedula, paramInt);
  }

  private static final String jdoGetcodCargo(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.codCargo;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.codCargo;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 17))
      return paramTrabajador.codCargo;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 17, paramTrabajador.codCargo);
  }

  private static final void jdoSetcodCargo(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.codCargo = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.codCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 17, paramTrabajador.codCargo, paramString);
  }

  private static final String jdoGetcodTipoPersonal(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.codTipoPersonal;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.codTipoPersonal;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 18))
      return paramTrabajador.codTipoPersonal;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 18, paramTrabajador.codTipoPersonal);
  }

  private static final void jdoSetcodTipoPersonal(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.codTipoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.codTipoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 18, paramTrabajador.codTipoPersonal, paramString);
  }

  private static final int jdoGetcodigoNomina(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.codigoNomina;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.codigoNomina;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 19))
      return paramTrabajador.codigoNomina;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 19, paramTrabajador.codigoNomina);
  }

  private static final void jdoSetcodigoNomina(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.codigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.codigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 19, paramTrabajador.codigoNomina, paramInt);
  }

  private static final int jdoGetcodigoNominaReal(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.codigoNominaReal;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.codigoNominaReal;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 20))
      return paramTrabajador.codigoNominaReal;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 20, paramTrabajador.codigoNominaReal);
  }

  private static final void jdoSetcodigoNominaReal(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.codigoNominaReal = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.codigoNominaReal = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 20, paramTrabajador.codigoNominaReal, paramInt);
  }

  private static final String jdoGetcodigoPatronal(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.codigoPatronal;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.codigoPatronal;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 21))
      return paramTrabajador.codigoPatronal;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 21, paramTrabajador.codigoPatronal);
  }

  private static final void jdoSetcodigoPatronal(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.codigoPatronal = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.codigoPatronal = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 21, paramTrabajador.codigoPatronal, paramString);
  }

  private static final String jdoGetcotizaFju(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.cotizaFju;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.cotizaFju;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 22))
      return paramTrabajador.cotizaFju;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 22, paramTrabajador.cotizaFju);
  }

  private static final void jdoSetcotizaFju(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.cotizaFju = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.cotizaFju = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 22, paramTrabajador.cotizaFju, paramString);
  }

  private static final String jdoGetcotizaLph(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.cotizaLph;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.cotizaLph;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 23))
      return paramTrabajador.cotizaLph;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 23, paramTrabajador.cotizaLph);
  }

  private static final void jdoSetcotizaLph(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.cotizaLph = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.cotizaLph = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 23, paramTrabajador.cotizaLph, paramString);
  }

  private static final String jdoGetcotizaSpf(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.cotizaSpf;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.cotizaSpf;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 24))
      return paramTrabajador.cotizaSpf;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 24, paramTrabajador.cotizaSpf);
  }

  private static final void jdoSetcotizaSpf(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.cotizaSpf = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.cotizaSpf = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 24, paramTrabajador.cotizaSpf, paramString);
  }

  private static final String jdoGetcotizaSso(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.cotizaSso;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.cotizaSso;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 25))
      return paramTrabajador.cotizaSso;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 25, paramTrabajador.cotizaSso);
  }

  private static final void jdoSetcotizaSso(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.cotizaSso = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.cotizaSso = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 25, paramTrabajador.cotizaSso, paramString);
  }

  private static final String jdoGetcuentaFid(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.cuentaFid;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.cuentaFid;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 26))
      return paramTrabajador.cuentaFid;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 26, paramTrabajador.cuentaFid);
  }

  private static final void jdoSetcuentaFid(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.cuentaFid = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.cuentaFid = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 26, paramTrabajador.cuentaFid, paramString);
  }

  private static final String jdoGetcuentaLph(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.cuentaLph;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.cuentaLph;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 27))
      return paramTrabajador.cuentaLph;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 27, paramTrabajador.cuentaLph);
  }

  private static final void jdoSetcuentaLph(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.cuentaLph = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.cuentaLph = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 27, paramTrabajador.cuentaLph, paramString);
  }

  private static final String jdoGetcuentaNomina(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.cuentaNomina;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.cuentaNomina;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 28))
      return paramTrabajador.cuentaNomina;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 28, paramTrabajador.cuentaNomina);
  }

  private static final void jdoSetcuentaNomina(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.cuentaNomina = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.cuentaNomina = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 28, paramTrabajador.cuentaNomina, paramString);
  }

  private static final String jdoGetdedProxNomina(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.dedProxNomina;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.dedProxNomina;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 29))
      return paramTrabajador.dedProxNomina;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 29, paramTrabajador.dedProxNomina);
  }

  private static final void jdoSetdedProxNomina(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.dedProxNomina = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.dedProxNomina = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 29, paramTrabajador.dedProxNomina, paramString);
  }

  private static final Dependencia jdoGetdependencia(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.dependencia;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 30))
      return paramTrabajador.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 30, paramTrabajador.dependencia);
  }

  private static final void jdoSetdependencia(Trabajador paramTrabajador, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 30, paramTrabajador.dependencia, paramDependencia);
  }

  private static final Dependencia jdoGetdependenciaReal(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.dependenciaReal;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 31))
      return paramTrabajador.dependenciaReal;
    return (Dependencia)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 31, paramTrabajador.dependenciaReal);
  }

  private static final void jdoSetdependenciaReal(Trabajador paramTrabajador, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.dependenciaReal = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 31, paramTrabajador.dependenciaReal, paramDependencia);
  }

  private static final int jdoGetdiaAntiguedad(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.diaAntiguedad;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.diaAntiguedad;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 32))
      return paramTrabajador.diaAntiguedad;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 32, paramTrabajador.diaAntiguedad);
  }

  private static final void jdoSetdiaAntiguedad(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.diaAntiguedad = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.diaAntiguedad = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 32, paramTrabajador.diaAntiguedad, paramInt);
  }

  private static final int jdoGetdiaEgreso(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.diaEgreso;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.diaEgreso;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 33))
      return paramTrabajador.diaEgreso;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 33, paramTrabajador.diaEgreso);
  }

  private static final void jdoSetdiaEgreso(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.diaEgreso = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.diaEgreso = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 33, paramTrabajador.diaEgreso, paramInt);
  }

  private static final int jdoGetdiaEntrada(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.diaEntrada;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.diaEntrada;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 34))
      return paramTrabajador.diaEntrada;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 34, paramTrabajador.diaEntrada);
  }

  private static final void jdoSetdiaEntrada(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.diaEntrada = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.diaEntrada = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 34, paramTrabajador.diaEntrada, paramInt);
  }

  private static final int jdoGetdiaIngreso(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.diaIngreso;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.diaIngreso;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 35))
      return paramTrabajador.diaIngreso;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 35, paramTrabajador.diaIngreso);
  }

  private static final void jdoSetdiaIngreso(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.diaIngreso = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.diaIngreso = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 35, paramTrabajador.diaIngreso, paramInt);
  }

  private static final int jdoGetdiaIngresoApn(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.diaIngresoApn;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.diaIngresoApn;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 36))
      return paramTrabajador.diaIngresoApn;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 36, paramTrabajador.diaIngresoApn);
  }

  private static final void jdoSetdiaIngresoApn(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.diaIngresoApn = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.diaIngresoApn = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 36, paramTrabajador.diaIngresoApn, paramInt);
  }

  private static final int jdoGetdiaIngresoCargo(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.diaIngresoCargo;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.diaIngresoCargo;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 37))
      return paramTrabajador.diaIngresoCargo;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 37, paramTrabajador.diaIngresoCargo);
  }

  private static final void jdoSetdiaIngresoCargo(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.diaIngresoCargo = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.diaIngresoCargo = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 37, paramTrabajador.diaIngresoCargo, paramInt);
  }

  private static final int jdoGetdiaJubilacion(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.diaJubilacion;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.diaJubilacion;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 38))
      return paramTrabajador.diaJubilacion;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 38, paramTrabajador.diaJubilacion);
  }

  private static final void jdoSetdiaJubilacion(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.diaJubilacion = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.diaJubilacion = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 38, paramTrabajador.diaJubilacion, paramInt);
  }

  private static final int jdoGetdiaPrestaciones(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.diaPrestaciones;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.diaPrestaciones;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 39))
      return paramTrabajador.diaPrestaciones;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 39, paramTrabajador.diaPrestaciones);
  }

  private static final void jdoSetdiaPrestaciones(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.diaPrestaciones = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.diaPrestaciones = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 39, paramTrabajador.diaPrestaciones, paramInt);
  }

  private static final int jdoGetdiaVacaciones(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.diaVacaciones;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.diaVacaciones;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 40))
      return paramTrabajador.diaVacaciones;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 40, paramTrabajador.diaVacaciones);
  }

  private static final void jdoSetdiaVacaciones(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.diaVacaciones = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.diaVacaciones = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 40, paramTrabajador.diaVacaciones, paramInt);
  }

  private static final int jdoGetdiasTrabajados(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.diasTrabajados;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.diasTrabajados;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 41))
      return paramTrabajador.diasTrabajados;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 41, paramTrabajador.diasTrabajados);
  }

  private static final void jdoSetdiasTrabajados(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.diasTrabajados = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.diasTrabajados = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 41, paramTrabajador.diasTrabajados, paramInt);
  }

  private static final String jdoGetestatus(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.estatus;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.estatus;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 42))
      return paramTrabajador.estatus;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 42, paramTrabajador.estatus);
  }

  private static final void jdoSetestatus(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 42, paramTrabajador.estatus, paramString);
  }

  private static final String jdoGetfeVida(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.feVida;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.feVida;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 43))
      return paramTrabajador.feVida;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 43, paramTrabajador.feVida);
  }

  private static final void jdoSetfeVida(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.feVida = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.feVida = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 43, paramTrabajador.feVida, paramString);
  }

  private static final Date jdoGetfechaAntiguedad(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaAntiguedad;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaAntiguedad;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 44))
      return paramTrabajador.fechaAntiguedad;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 44, paramTrabajador.fechaAntiguedad);
  }

  private static final void jdoSetfechaAntiguedad(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaAntiguedad = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaAntiguedad = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 44, paramTrabajador.fechaAntiguedad, paramDate);
  }

  private static final Date jdoGetfechaComisionServicio(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaComisionServicio;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaComisionServicio;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 45))
      return paramTrabajador.fechaComisionServicio;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 45, paramTrabajador.fechaComisionServicio);
  }

  private static final void jdoSetfechaComisionServicio(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaComisionServicio = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaComisionServicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 45, paramTrabajador.fechaComisionServicio, paramDate);
  }

  private static final Date jdoGetfechaEgreso(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaEgreso;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaEgreso;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 46))
      return paramTrabajador.fechaEgreso;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 46, paramTrabajador.fechaEgreso);
  }

  private static final void jdoSetfechaEgreso(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaEgreso = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaEgreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 46, paramTrabajador.fechaEgreso, paramDate);
  }

  private static final Date jdoGetfechaEncargaduria(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaEncargaduria;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaEncargaduria;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 47))
      return paramTrabajador.fechaEncargaduria;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 47, paramTrabajador.fechaEncargaduria);
  }

  private static final void jdoSetfechaEncargaduria(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaEncargaduria = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaEncargaduria = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 47, paramTrabajador.fechaEncargaduria, paramDate);
  }

  private static final Date jdoGetfechaEntradaSig(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaEntradaSig;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaEntradaSig;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 48))
      return paramTrabajador.fechaEntradaSig;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 48, paramTrabajador.fechaEntradaSig);
  }

  private static final void jdoSetfechaEntradaSig(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaEntradaSig = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaEntradaSig = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 48, paramTrabajador.fechaEntradaSig, paramDate);
  }

  private static final Date jdoGetfechaFeVida(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaFeVida;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaFeVida;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 49))
      return paramTrabajador.fechaFeVida;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 49, paramTrabajador.fechaFeVida);
  }

  private static final void jdoSetfechaFeVida(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaFeVida = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaFeVida = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 49, paramTrabajador.fechaFeVida, paramDate);
  }

  private static final Date jdoGetfechaIngreso(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaIngreso;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaIngreso;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 50))
      return paramTrabajador.fechaIngreso;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 50, paramTrabajador.fechaIngreso);
  }

  private static final void jdoSetfechaIngreso(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaIngreso = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaIngreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 50, paramTrabajador.fechaIngreso, paramDate);
  }

  private static final Date jdoGetfechaIngresoApn(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaIngresoApn;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaIngresoApn;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 51))
      return paramTrabajador.fechaIngresoApn;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 51, paramTrabajador.fechaIngresoApn);
  }

  private static final void jdoSetfechaIngresoApn(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaIngresoApn = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaIngresoApn = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 51, paramTrabajador.fechaIngresoApn, paramDate);
  }

  private static final Date jdoGetfechaIngresoCargo(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaIngresoCargo;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaIngresoCargo;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 52))
      return paramTrabajador.fechaIngresoCargo;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 52, paramTrabajador.fechaIngresoCargo);
  }

  private static final void jdoSetfechaIngresoCargo(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaIngresoCargo = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaIngresoCargo = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 52, paramTrabajador.fechaIngresoCargo, paramDate);
  }

  private static final Date jdoGetfechaJubilacion(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaJubilacion;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaJubilacion;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 53))
      return paramTrabajador.fechaJubilacion;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 53, paramTrabajador.fechaJubilacion);
  }

  private static final void jdoSetfechaJubilacion(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaJubilacion = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaJubilacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 53, paramTrabajador.fechaJubilacion, paramDate);
  }

  private static final Date jdoGetfechaPrestaciones(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaPrestaciones;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaPrestaciones;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 54))
      return paramTrabajador.fechaPrestaciones;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 54, paramTrabajador.fechaPrestaciones);
  }

  private static final void jdoSetfechaPrestaciones(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaPrestaciones = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaPrestaciones = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 54, paramTrabajador.fechaPrestaciones, paramDate);
  }

  private static final Date jdoGetfechaSalidaSig(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaSalidaSig;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaSalidaSig;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 55))
      return paramTrabajador.fechaSalidaSig;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 55, paramTrabajador.fechaSalidaSig);
  }

  private static final void jdoSetfechaSalidaSig(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaSalidaSig = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaSalidaSig = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 55, paramTrabajador.fechaSalidaSig, paramDate);
  }

  private static final Date jdoGetfechaTipoPersonal(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaTipoPersonal;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaTipoPersonal;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 56))
      return paramTrabajador.fechaTipoPersonal;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 56, paramTrabajador.fechaTipoPersonal);
  }

  private static final void jdoSetfechaTipoPersonal(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaTipoPersonal = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaTipoPersonal = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 56, paramTrabajador.fechaTipoPersonal, paramDate);
  }

  private static final Date jdoGetfechaUltimoMovimiento(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaUltimoMovimiento;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaUltimoMovimiento;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 57))
      return paramTrabajador.fechaUltimoMovimiento;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 57, paramTrabajador.fechaUltimoMovimiento);
  }

  private static final void jdoSetfechaUltimoMovimiento(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaUltimoMovimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaUltimoMovimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 57, paramTrabajador.fechaUltimoMovimiento, paramDate);
  }

  private static final Date jdoGetfechaVacaciones(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.fechaVacaciones;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.fechaVacaciones;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 58))
      return paramTrabajador.fechaVacaciones;
    return (Date)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 58, paramTrabajador.fechaVacaciones);
  }

  private static final void jdoSetfechaVacaciones(Trabajador paramTrabajador, Date paramDate)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.fechaVacaciones = paramDate;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.fechaVacaciones = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 58, paramTrabajador.fechaVacaciones, paramDate);
  }

  private static final String jdoGetformaPago(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.formaPago;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.formaPago;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 59))
      return paramTrabajador.formaPago;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 59, paramTrabajador.formaPago);
  }

  private static final void jdoSetformaPago(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.formaPago = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.formaPago = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 59, paramTrabajador.formaPago, paramString);
  }

  private static final String jdoGethayRetroactivo(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.hayRetroactivo;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.hayRetroactivo;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 60))
      return paramTrabajador.hayRetroactivo;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 60, paramTrabajador.hayRetroactivo);
  }

  private static final void jdoSethayRetroactivo(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.hayRetroactivo = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.hayRetroactivo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 60, paramTrabajador.hayRetroactivo, paramString);
  }

  private static final double jdoGethorasSemanales(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.horasSemanales;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.horasSemanales;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 61))
      return paramTrabajador.horasSemanales;
    return localStateManager.getDoubleField(paramTrabajador, jdoInheritedFieldCount + 61, paramTrabajador.horasSemanales);
  }

  private static final void jdoSethorasSemanales(Trabajador paramTrabajador, double paramDouble)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.horasSemanales = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.horasSemanales = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrabajador, jdoInheritedFieldCount + 61, paramTrabajador.horasSemanales, paramDouble);
  }

  private static final long jdoGetidTrabajador(Trabajador paramTrabajador)
  {
    return paramTrabajador.idTrabajador;
  }

  private static final void jdoSetidTrabajador(Trabajador paramTrabajador, long paramLong)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.idTrabajador = paramLong;
      return;
    }
    localStateManager.setLongField(paramTrabajador, jdoInheritedFieldCount + 62, paramTrabajador.idTrabajador, paramLong);
  }

  private static final String jdoGetjubilacionPlanificada(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.jubilacionPlanificada;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.jubilacionPlanificada;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 63))
      return paramTrabajador.jubilacionPlanificada;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 63, paramTrabajador.jubilacionPlanificada);
  }

  private static final void jdoSetjubilacionPlanificada(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.jubilacionPlanificada = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.jubilacionPlanificada = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 63, paramTrabajador.jubilacionPlanificada, paramString);
  }

  private static final LugarPago jdoGetlugarPago(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.lugarPago;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 64))
      return paramTrabajador.lugarPago;
    return (LugarPago)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 64, paramTrabajador.lugarPago);
  }

  private static final void jdoSetlugarPago(Trabajador paramTrabajador, LugarPago paramLugarPago)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.lugarPago = paramLugarPago;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 64, paramTrabajador.lugarPago, paramLugarPago);
  }

  private static final int jdoGetlunesPrimera(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.lunesPrimera;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.lunesPrimera;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 65))
      return paramTrabajador.lunesPrimera;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 65, paramTrabajador.lunesPrimera);
  }

  private static final void jdoSetlunesPrimera(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.lunesPrimera = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.lunesPrimera = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 65, paramTrabajador.lunesPrimera, paramInt);
  }

  private static final int jdoGetlunesRetroactivo(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.lunesRetroactivo;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.lunesRetroactivo;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 66))
      return paramTrabajador.lunesRetroactivo;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 66, paramTrabajador.lunesRetroactivo);
  }

  private static final void jdoSetlunesRetroactivo(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.lunesRetroactivo = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.lunesRetroactivo = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 66, paramTrabajador.lunesRetroactivo, paramInt);
  }

  private static final int jdoGetlunesSegunda(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.lunesSegunda;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.lunesSegunda;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 67))
      return paramTrabajador.lunesSegunda;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 67, paramTrabajador.lunesSegunda);
  }

  private static final void jdoSetlunesSegunda(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.lunesSegunda = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.lunesSegunda = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 67, paramTrabajador.lunesSegunda, paramInt);
  }

  private static final int jdoGetmesAntiguedad(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.mesAntiguedad;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.mesAntiguedad;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 68))
      return paramTrabajador.mesAntiguedad;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 68, paramTrabajador.mesAntiguedad);
  }

  private static final void jdoSetmesAntiguedad(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.mesAntiguedad = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.mesAntiguedad = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 68, paramTrabajador.mesAntiguedad, paramInt);
  }

  private static final int jdoGetmesEgreso(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.mesEgreso;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.mesEgreso;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 69))
      return paramTrabajador.mesEgreso;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 69, paramTrabajador.mesEgreso);
  }

  private static final void jdoSetmesEgreso(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.mesEgreso = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.mesEgreso = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 69, paramTrabajador.mesEgreso, paramInt);
  }

  private static final int jdoGetmesEntrada(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.mesEntrada;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.mesEntrada;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 70))
      return paramTrabajador.mesEntrada;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 70, paramTrabajador.mesEntrada);
  }

  private static final void jdoSetmesEntrada(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.mesEntrada = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.mesEntrada = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 70, paramTrabajador.mesEntrada, paramInt);
  }

  private static final int jdoGetmesIngreso(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.mesIngreso;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.mesIngreso;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 71))
      return paramTrabajador.mesIngreso;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 71, paramTrabajador.mesIngreso);
  }

  private static final void jdoSetmesIngreso(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.mesIngreso = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.mesIngreso = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 71, paramTrabajador.mesIngreso, paramInt);
  }

  private static final int jdoGetmesIngresoApn(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.mesIngresoApn;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.mesIngresoApn;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 72))
      return paramTrabajador.mesIngresoApn;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 72, paramTrabajador.mesIngresoApn);
  }

  private static final void jdoSetmesIngresoApn(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.mesIngresoApn = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.mesIngresoApn = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 72, paramTrabajador.mesIngresoApn, paramInt);
  }

  private static final int jdoGetmesIngresoCargo(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.mesIngresoCargo;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.mesIngresoCargo;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 73))
      return paramTrabajador.mesIngresoCargo;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 73, paramTrabajador.mesIngresoCargo);
  }

  private static final void jdoSetmesIngresoCargo(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.mesIngresoCargo = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.mesIngresoCargo = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 73, paramTrabajador.mesIngresoCargo, paramInt);
  }

  private static final int jdoGetmesJubilacion(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.mesJubilacion;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.mesJubilacion;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 74))
      return paramTrabajador.mesJubilacion;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 74, paramTrabajador.mesJubilacion);
  }

  private static final void jdoSetmesJubilacion(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.mesJubilacion = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.mesJubilacion = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 74, paramTrabajador.mesJubilacion, paramInt);
  }

  private static final int jdoGetmesPrestaciones(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.mesPrestaciones;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.mesPrestaciones;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 75))
      return paramTrabajador.mesPrestaciones;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 75, paramTrabajador.mesPrestaciones);
  }

  private static final void jdoSetmesPrestaciones(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.mesPrestaciones = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.mesPrestaciones = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 75, paramTrabajador.mesPrestaciones, paramInt);
  }

  private static final int jdoGetmesVacaciones(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.mesVacaciones;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.mesVacaciones;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 76))
      return paramTrabajador.mesVacaciones;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 76, paramTrabajador.mesVacaciones);
  }

  private static final void jdoSetmesVacaciones(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.mesVacaciones = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.mesVacaciones = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 76, paramTrabajador.mesVacaciones, paramInt);
  }

  private static final String jdoGetmovimiento(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.movimiento;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.movimiento;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 77))
      return paramTrabajador.movimiento;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 77, paramTrabajador.movimiento);
  }

  private static final void jdoSetmovimiento(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.movimiento = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.movimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 77, paramTrabajador.movimiento, paramString);
  }

  private static final Organismo jdoGetorganismo(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.organismo;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 78))
      return paramTrabajador.organismo;
    return (Organismo)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 78, paramTrabajador.organismo);
  }

  private static final void jdoSetorganismo(Trabajador paramTrabajador, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 78, paramTrabajador.organismo, paramOrganismo);
  }

  private static final String jdoGetorganismoComisionServicio(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.organismoComisionServicio;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.organismoComisionServicio;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 79))
      return paramTrabajador.organismoComisionServicio;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 79, paramTrabajador.organismoComisionServicio);
  }

  private static final void jdoSetorganismoComisionServicio(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.organismoComisionServicio = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.organismoComisionServicio = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 79, paramTrabajador.organismoComisionServicio, paramString);
  }

  private static final String jdoGetparProxNomina(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.parProxNomina;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.parProxNomina;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 80))
      return paramTrabajador.parProxNomina;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 80, paramTrabajador.parProxNomina);
  }

  private static final void jdoSetparProxNomina(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.parProxNomina = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.parProxNomina = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 80, paramTrabajador.parProxNomina, paramString);
  }

  private static final int jdoGetpaso(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.paso;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.paso;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 81))
      return paramTrabajador.paso;
    return localStateManager.getIntField(paramTrabajador, jdoInheritedFieldCount + 81, paramTrabajador.paso);
  }

  private static final void jdoSetpaso(Trabajador paramTrabajador, int paramInt)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.paso = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.paso = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajador, jdoInheritedFieldCount + 81, paramTrabajador.paso, paramInt);
  }

  private static final Personal jdoGetpersonal(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.personal;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 82))
      return paramTrabajador.personal;
    return (Personal)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 82, paramTrabajador.personal);
  }

  private static final void jdoSetpersonal(Trabajador paramTrabajador, Personal paramPersonal)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 82, paramTrabajador.personal, paramPersonal);
  }

  private static final double jdoGetporcentajeIslr(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.porcentajeIslr;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.porcentajeIslr;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 83))
      return paramTrabajador.porcentajeIslr;
    return localStateManager.getDoubleField(paramTrabajador, jdoInheritedFieldCount + 83, paramTrabajador.porcentajeIslr);
  }

  private static final void jdoSetporcentajeIslr(Trabajador paramTrabajador, double paramDouble)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.porcentajeIslr = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.porcentajeIslr = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrabajador, jdoInheritedFieldCount + 83, paramTrabajador.porcentajeIslr, paramDouble);
  }

  private static final double jdoGetporcentajeJubilacion(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.porcentajeJubilacion;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.porcentajeJubilacion;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 84))
      return paramTrabajador.porcentajeJubilacion;
    return localStateManager.getDoubleField(paramTrabajador, jdoInheritedFieldCount + 84, paramTrabajador.porcentajeJubilacion);
  }

  private static final void jdoSetporcentajeJubilacion(Trabajador paramTrabajador, double paramDouble)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.porcentajeJubilacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.porcentajeJubilacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrabajador, jdoInheritedFieldCount + 84, paramTrabajador.porcentajeJubilacion, paramDouble);
  }

  private static final String jdoGetregimen(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.regimen;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.regimen;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 85))
      return paramTrabajador.regimen;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 85, paramTrabajador.regimen);
  }

  private static final void jdoSetregimen(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.regimen = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.regimen = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 85, paramTrabajador.regimen, paramString);
  }

  private static final RegistroCargos jdoGetregistroCargos(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.registroCargos;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 86))
      return paramTrabajador.registroCargos;
    return (RegistroCargos)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 86, paramTrabajador.registroCargos);
  }

  private static final void jdoSetregistroCargos(Trabajador paramTrabajador, RegistroCargos paramRegistroCargos)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.registroCargos = paramRegistroCargos;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 86, paramTrabajador.registroCargos, paramRegistroCargos);
  }

  private static final String jdoGetriesgo(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.riesgo;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.riesgo;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 87))
      return paramTrabajador.riesgo;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 87, paramTrabajador.riesgo);
  }

  private static final void jdoSetriesgo(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.riesgo = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.riesgo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 87, paramTrabajador.riesgo, paramString);
  }

  private static final String jdoGetsituacion(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.situacion;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.situacion;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 88))
      return paramTrabajador.situacion;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 88, paramTrabajador.situacion);
  }

  private static final void jdoSetsituacion(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.situacion = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.situacion = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 88, paramTrabajador.situacion, paramString);
  }

  private static final double jdoGetsueldoBasico(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.sueldoBasico;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.sueldoBasico;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 89))
      return paramTrabajador.sueldoBasico;
    return localStateManager.getDoubleField(paramTrabajador, jdoInheritedFieldCount + 89, paramTrabajador.sueldoBasico);
  }

  private static final void jdoSetsueldoBasico(Trabajador paramTrabajador, double paramDouble)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.sueldoBasico = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.sueldoBasico = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrabajador, jdoInheritedFieldCount + 89, paramTrabajador.sueldoBasico, paramDouble);
  }

  private static final String jdoGettipoCtaNomina(Trabajador paramTrabajador)
  {
    if (paramTrabajador.jdoFlags <= 0)
      return paramTrabajador.tipoCtaNomina;
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.tipoCtaNomina;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 90))
      return paramTrabajador.tipoCtaNomina;
    return localStateManager.getStringField(paramTrabajador, jdoInheritedFieldCount + 90, paramTrabajador.tipoCtaNomina);
  }

  private static final void jdoSettipoCtaNomina(Trabajador paramTrabajador, String paramString)
  {
    if (paramTrabajador.jdoFlags == 0)
    {
      paramTrabajador.tipoCtaNomina = paramString;
      return;
    }
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.tipoCtaNomina = paramString;
      return;
    }
    localStateManager.setStringField(paramTrabajador, jdoInheritedFieldCount + 90, paramTrabajador.tipoCtaNomina, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.tipoPersonal;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 91))
      return paramTrabajador.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 91, paramTrabajador.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(Trabajador paramTrabajador, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 91, paramTrabajador.tipoPersonal, paramTipoPersonal);
  }

  private static final Turno jdoGetturno(Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajador.turno;
    if (localStateManager.isLoaded(paramTrabajador, jdoInheritedFieldCount + 92))
      return paramTrabajador.turno;
    return (Turno)localStateManager.getObjectField(paramTrabajador, jdoInheritedFieldCount + 92, paramTrabajador.turno);
  }

  private static final void jdoSetturno(Trabajador paramTrabajador, Turno paramTurno)
  {
    StateManager localStateManager = paramTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajador.turno = paramTurno;
      return;
    }
    localStateManager.setObjectField(paramTrabajador, jdoInheritedFieldCount + 92, paramTrabajador.turno, paramTurno);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}