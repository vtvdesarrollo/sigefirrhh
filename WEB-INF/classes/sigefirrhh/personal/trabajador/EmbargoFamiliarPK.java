package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class EmbargoFamiliarPK
  implements Serializable
{
  public long idEmbargoFamiliar;

  public EmbargoFamiliarPK()
  {
  }

  public EmbargoFamiliarPK(long idEmbargoFamiliar)
  {
    this.idEmbargoFamiliar = idEmbargoFamiliar;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EmbargoFamiliarPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EmbargoFamiliarPK thatPK)
  {
    return 
      this.idEmbargoFamiliar == thatPK.idEmbargoFamiliar;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEmbargoFamiliar)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEmbargoFamiliar);
  }
}