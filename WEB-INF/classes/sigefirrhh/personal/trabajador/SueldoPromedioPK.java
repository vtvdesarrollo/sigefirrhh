package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class SueldoPromedioPK
  implements Serializable
{
  public long idSueldoPromedio;

  public SueldoPromedioPK()
  {
  }

  public SueldoPromedioPK(long idSueldoPromedio)
  {
    this.idSueldoPromedio = idSueldoPromedio;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SueldoPromedioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SueldoPromedioPK thatPK)
  {
    return 
      this.idSueldoPromedio == thatPK.idSueldoPromedio;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSueldoPromedio)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSueldoPromedio);
  }
}