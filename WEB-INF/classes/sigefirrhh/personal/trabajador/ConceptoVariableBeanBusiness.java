package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonalBeanBusiness;

public class ConceptoVariableBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoVariable(ConceptoVariable conceptoVariable)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoVariable conceptoVariableNew = 
      (ConceptoVariable)BeanUtils.cloneBean(
      conceptoVariable);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoVariableNew.getConceptoTipoPersonal() != null) {
      conceptoVariableNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoVariableNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (conceptoVariableNew.getFrecuenciaTipoPersonal() != null) {
      conceptoVariableNew.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        conceptoVariableNew.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (conceptoVariableNew.getTrabajador() != null) {
      conceptoVariableNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        conceptoVariableNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(conceptoVariableNew);
  }

  public void updateConceptoVariable(ConceptoVariable conceptoVariable) throws Exception
  {
    ConceptoVariable conceptoVariableModify = 
      findConceptoVariableById(conceptoVariable.getIdConceptoVariable());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoVariable.getConceptoTipoPersonal() != null) {
      conceptoVariable.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoVariable.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (conceptoVariable.getFrecuenciaTipoPersonal() != null) {
      conceptoVariable.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        conceptoVariable.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (conceptoVariable.getTrabajador() != null) {
      conceptoVariable.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        conceptoVariable.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(conceptoVariableModify, conceptoVariable);
  }

  public void deleteConceptoVariable(ConceptoVariable conceptoVariable) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoVariable conceptoVariableDelete = 
      findConceptoVariableById(conceptoVariable.getIdConceptoVariable());
    pm.deletePersistent(conceptoVariableDelete);
  }

  public ConceptoVariable findConceptoVariableById(long idConceptoVariable) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoVariable == pIdConceptoVariable";
    Query query = pm.newQuery(ConceptoVariable.class, filter);

    query.declareParameters("long pIdConceptoVariable");

    parameters.put("pIdConceptoVariable", new Long(idConceptoVariable));

    Collection colConceptoVariable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoVariable.iterator();
    return (ConceptoVariable)iterator.next();
  }

  public Collection findConceptoVariableAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoVariableExtent = pm.getExtent(
      ConceptoVariable.class, true);
    Query query = pm.newQuery(conceptoVariableExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(ConceptoVariable.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoVariable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoVariable);

    return colConceptoVariable;
  }

  public ConceptoVariable findByTrabajador(long idFrecuenciaTipoPersonal, long idConceptoTipoPersonal, long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    ConceptoVariable conVar = null;

    String filter = "frecuenciatipopersonal.idFrecuenciaTipoPersonal == pIdFrecuenciaTipoPersonal && conceptotipopersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal && trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(ConceptoVariable.class, filter);

    query.declareParameters("long pIdFrecuenciaTipoPersonal, long pIdConceptoTipoPersonal, long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdFrecuenciaTipoPersonal", new Long(idFrecuenciaTipoPersonal));
    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));
    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoVariable = 
      new ArrayList((Collection)query.executeWithMap(parameters));
    conVar = (ConceptoVariable)colConceptoVariable.iterator().next();

    pm.makeTransientAll(colConceptoVariable);

    return conVar;
  }
}