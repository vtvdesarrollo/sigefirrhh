package sigefirrhh.personal.trabajador;

import eforserver.common.Resource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;

public class GenerarPlanillaArcBeanBusiness
{
  Logger log = Logger.getLogger(GenerarPlanillaArcBeanBusiness.class.getName());

  public void generar(long idTipoPersonal, String periodicidad, int anio)
    throws Exception
  {
    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      StringBuffer sql = new StringBuffer();
      sql.append("select generar_planillaarc(?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setString(2, periodicidad);
      st.setInt(3, anio);

      rs = st.executeQuery();
      connection.commit();

      this.log.error("ejecutó el calculo de la planilla arc con existo");
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
  }
}