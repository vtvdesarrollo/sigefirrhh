package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.procesoNomina.ProcesoNominaNoGenFacade;

public class ReportTrabajadorSedeForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportTrabajadorSedeForm.class.getName());
  private int reportId;
  private String formato = "1";
  private long idTipoPersonal;
  private long idSede;
  private String reportName;
  private String orden;
  private String mostrar = "S";
  private Collection listTipoPersonal;
  private Collection listSede;
  private DefinicionesNoGenFacade definicionesFacade;
  private ProcesoNominaNoGenFacade procesoNominaFacade;
  private LoginSession login;
  private EstructuraFacade estructuraFacade;
  private String sexo;
  private double sueldoDesde = 0.0D;
  private double sueldoHasta = 99000000.0D;
  private String sueldoEvaluar = "B";

  public boolean isMostrarBotonCalcular() { return this.mostrar.equals("S"); }

  public void calcularPromedios() {
    FacesContext context = FacesContext.getCurrentInstance();
    TipoPersonal tipoPersonal = null;
    try {
      tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No seleccionó el Tipo de  Personal", ""));
      return;
    }
    try {
      this.procesoNominaFacade.calcularPromedios(tipoPersonal.getIdTipoPersonal(), tipoPersonal.getGrupoNomina().getPeriodicidad());
      context.addMessage("success_add", new FacesMessage("Se actualizaron los promedios con éxito"));
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error, no se actualizaron los promedios", ""));
      return;
    }
  }

  public ReportTrabajadorSedeForm() { FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.procesoNominaFacade = new ProcesoNominaNoGenFacade();
    this.reportName = "trasbalfsuesed";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportTrabajadorSedeForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listSede = this.estructuraFacade.findAllSede();
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listSede = new ArrayList();
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    if (this.orden != null) {
      this.reportName = "";
      if (this.formato.equals("2")) {
        this.reportName = "a_";
      }
      this.reportName += "tra";
      if (this.sueldoEvaluar.equals("B")) {
        this.reportName += "sb";
      }
      if ((this.orden.equals("A")) && (this.mostrar.equals("S")) && (this.idSede == 0L))
        this.reportName += "alfsuesed";
      else if ((this.orden.equals("A")) && (this.mostrar.equals("N")) && (this.idSede == 0L))
        this.reportName += "alfabsed";
      else if ((this.orden.equals("C")) && (this.mostrar.equals("S")) && (this.idSede == 0L))
        this.reportName += "cedsuesed";
      else if ((this.orden.equals("C")) && (this.mostrar.equals("N")) && (this.idSede == 0L))
        this.reportName += "cedsed";
      else if ((this.orden.equals("N")) && (this.mostrar.equals("S")) && (this.idSede == 0L))
        this.reportName += "codsuesed";
      else if ((this.orden.equals("N")) && (this.mostrar.equals("N")) && (this.idSede == 0L))
        this.reportName += "codsed";
      else if ((this.orden.equals("A")) && (this.mostrar.equals("S")) && (this.idSede != 0L))
        this.reportName += "alfsuesed1";
      else if ((this.orden.equals("A")) && (this.mostrar.equals("N")) && (this.idSede != 0L))
        this.reportName += "alfabsed1";
      else if ((this.orden.equals("C")) && (this.mostrar.equals("S")) && (this.idSede != 0L))
        this.reportName += "cedsuesed1";
      else if ((this.orden.equals("C")) && (this.mostrar.equals("N")) && (this.idSede != 0L))
        this.reportName += "cedsed1";
      else if ((this.orden.equals("N")) && (this.mostrar.equals("S")) && (this.idSede != 0L))
        this.reportName += "codsuesed1";
      else if ((this.orden.equals("N")) && (this.mostrar.equals("N")) && (this.idSede != 0L)) {
        this.reportName += "codsed1";
      }
      if (!this.sexo.equals("T")) {
        this.reportName += "sx";
      }
    }
    log.error("reporte........." + this.reportName);
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "";
      if (this.formato.equals("2")) {
        this.reportName = "a_";
      }
      this.reportName += "tra";
      if (this.sueldoEvaluar.equals("B")) {
        this.reportName += "sb";
      }
      if ((this.orden.equals("A")) && (this.mostrar.equals("S")) && (this.idSede == 0L))
        this.reportName += "alfsuesed";
      else if ((this.orden.equals("A")) && (this.mostrar.equals("N")) && (this.idSede == 0L))
        this.reportName += "alfabsed";
      else if ((this.orden.equals("C")) && (this.mostrar.equals("S")) && (this.idSede == 0L))
        this.reportName += "cedsuesed";
      else if ((this.orden.equals("C")) && (this.mostrar.equals("N")) && (this.idSede == 0L))
        this.reportName += "cedsed";
      else if ((this.orden.equals("N")) && (this.mostrar.equals("S")) && (this.idSede == 0L))
        this.reportName += "codsuesed";
      else if ((this.orden.equals("N")) && (this.mostrar.equals("N")) && (this.idSede == 0L))
        this.reportName += "codsed";
      else if ((this.orden.equals("A")) && (this.mostrar.equals("S")) && (this.idSede != 0L))
        this.reportName += "alfsuesed1";
      else if ((this.orden.equals("A")) && (this.mostrar.equals("N")) && (this.idSede != 0L))
        this.reportName += "alfabsed1";
      else if ((this.orden.equals("C")) && (this.mostrar.equals("S")) && (this.idSede != 0L))
        this.reportName += "cedsuesed1";
      else if ((this.orden.equals("C")) && (this.mostrar.equals("N")) && (this.idSede != 0L))
        this.reportName += "cedsed1";
      else if ((this.orden.equals("N")) && (this.mostrar.equals("S")) && (this.idSede != 0L))
        this.reportName += "codsuesed1";
      else if ((this.orden.equals("N")) && (this.mostrar.equals("N")) && (this.idSede != 0L)) {
        this.reportName += "codsed1";
      }
      if (!this.sexo.equals("T")) {
        this.reportName += "sx";
      }
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      parameters.put("sueldodesde", new Double(this.sueldoDesde));
      parameters.put("sueldohasta", new Double(this.sueldoHasta));
      if (this.idSede != 0L) {
        parameters.put("id_sede", new Long(this.idSede));
      }
      if (!this.sexo.equals("T")) {
        parameters.put("sexo", this.sexo);
      }

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }

      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/trabajador");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l)
  {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListSede()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listSede, "sigefirrhh.base.estructura.Sede");
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public String getMostrar()
  {
    return this.mostrar;
  }

  public String getOrden()
  {
    return this.orden;
  }

  public void setMostrar(String string)
  {
    this.mostrar = string;
  }

  public void setOrden(String string)
  {
    this.orden = string;
  }

  public long getIdSede()
  {
    return this.idSede;
  }

  public void setIdSede(long l)
  {
    this.idSede = l;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String string)
  {
    this.formato = string;
  }

  public String getSexo()
  {
    return this.sexo;
  }

  public void setSexo(String sexo)
  {
    this.sexo = sexo;
  }
  public double getSueldoDesde() {
    return this.sueldoDesde;
  }
  public void setSueldoDesde(double sueldoDesde) {
    this.sueldoDesde = sueldoDesde;
  }
  public double getSueldoHasta() {
    return this.sueldoHasta;
  }
  public void setSueldoHasta(double sueldoHasta) {
    this.sueldoHasta = sueldoHasta;
  }

  public String getSueldoEvaluar()
  {
    return this.sueldoEvaluar;
  }

  public void setSueldoEvaluar(String sueldoEvaluar)
  {
    this.sueldoEvaluar = sueldoEvaluar;
  }
}