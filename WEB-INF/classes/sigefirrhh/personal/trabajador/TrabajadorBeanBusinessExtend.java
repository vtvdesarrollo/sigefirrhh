package sigefirrhh.personal.trabajador;

import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class TrabajadorBeanBusinessExtend extends TrabajadorBeanBusiness
{
  public Collection findActivoByTipoPersonal(long idTipoPersonal)
  {
    String estatus = "A";
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && estatus == pEstatus";

    Query query = pm.newQuery(Trabajador.class, filter);
    query.declareParameters("String pEstatus, long pIdTipoPersonal");

    HashMap parameters = new HashMap();

    parameters.put("pEstatus", estatus);
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colTrabajador;
  }

  public Collection findActivoByTipoPersonal(long idTipoPersonal, long idOrganismo)
  {
    String estatus = "A";
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && estatus == pEstatus && organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Trabajador.class, filter);
    query.declareParameters("String pEstatus, long pIdTipoPersonal, long pIdOrganismo");

    HashMap parameters = new HashMap();

    parameters.put("pEstatus", estatus);
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colTrabajador;
  }

  public Trabajador findActivoByTipoPersonal(int cedula, long idTipoPersonal)
    throws Exception
  {
    String estatus = "A";

    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.cedula == pCedula && tipoPersonal.idTipoPersonal == pIdTipoPersonal && estatus == pEstatus";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("int pCedula, long pIdTipoPersonal, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pEstatus", estatus);

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);
    Iterator iterator = colTrabajador.iterator();

    return (Trabajador)iterator.next();
  }
}