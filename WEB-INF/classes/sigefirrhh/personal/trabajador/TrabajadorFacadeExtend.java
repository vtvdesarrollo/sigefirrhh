package sigefirrhh.personal.trabajador;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class TrabajadorFacadeExtend extends TrabajadorFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private TrabajadorBusinessExtend trabajadorBusinessExtend = new TrabajadorBusinessExtend();

  public Collection findTrabajadorActivoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusinessExtend.findTrabajadorActivoByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Trabajador findTrabajadorActivoByTipoPersonal(int cedula, long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusinessExtend.findTrabajadorActivoByTipoPersonal(cedula, idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findConceptoFijoFromConceptoAsociado(long idTrabajador, long idConceptoTipoPersonal, byte frecuenciaMensual)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.trabajadorBusinessExtend.findConceptoFijoFromConceptoAsociado(
        idTrabajador, 
        idConceptoTipoPersonal, 
        frecuenciaMensual);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSueldoPromedioByTrabajador(long idTrabajador) throws Exception
  {
    try {
      this.txn.open();
      return this.trabajadorBusinessExtend.findSueldoPromedioByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void cargarConceptos(Collection temporalConceptos, String accion, String tipoConcepto) throws Exception
  {
    try {
      this.txn.open();
      this.trabajadorBusinessExtend.cargarConceptos(temporalConceptos, accion, tipoConcepto);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}