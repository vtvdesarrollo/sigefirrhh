package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class PasantiaPK
  implements Serializable
{
  public long idPasantia;

  public PasantiaPK()
  {
  }

  public PasantiaPK(long idPasantia)
  {
    this.idPasantia = idPasantia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PasantiaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PasantiaPK thatPK)
  {
    return 
      this.idPasantia == thatPK.idPasantia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPasantia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPasantia);
  }
}