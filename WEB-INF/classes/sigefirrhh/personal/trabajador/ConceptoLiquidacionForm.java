package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ConceptoLiquidacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoLiquidacionForm.class.getName());
  private ConceptoLiquidacion conceptoLiquidacion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private TrabajadorNoGenFacade trabajadorNoGenFacade = new TrabajadorNoGenFacade();
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private Collection colFrecuenciaTipoPersonal;
  private Collection colTrabajador;
  private String selectConceptoTipoPersonal;
  private String selectFrecuenciaTipoPersonal;
  private String selectTrabajador;
  private Collection findColTipoPersonal;
  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  private Object stateScrollConceptoLiquidacionByTrabajador = null;
  private Object stateResultConceptoLiquidacionByTrabajador = null;

  public String calcular()
    throws Exception
  {
    log.error("Paso");
    log.error("idConceptoTipoPersonal en form" + this.conceptoTipoPersonal.getIdConceptoTipoPersonal());
    this.conceptoLiquidacion.setMonto(this.trabajadorNoGenFacade.calcularConcepto(this.conceptoTipoPersonal.getIdConceptoTipoPersonal(), this.conceptoLiquidacion.getTrabajador().getIdTrabajador(), this.conceptoLiquidacion.getUnidades(), this.conceptoTipoPersonal.getTipo(), this.conceptoLiquidacion.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago(), this.conceptoLiquidacion.getTrabajador().getTurno().getJornadaDiaria(), this.conceptoLiquidacion.getTrabajador().getTurno().getJornadaSemanal(), this.conceptoLiquidacion.getTrabajador().getTipoPersonal().getFormulaIntegral(), this.conceptoLiquidacion.getTrabajador().getTipoPersonal().getFormulaSemanal(), this.conceptoLiquidacion.getTrabajador().getCargo().getIdCargo(), this.conceptoTipoPersonal.getValor(), this.conceptoTipoPersonal.getTopeMinimo(), this.conceptoTipoPersonal.getTopeMaximo()));
    return null;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getSelectConceptoTipoPersonal()
  {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();

    this.conceptoLiquidacion.setConceptoTipoPersonal(null);

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      iterator.next();
      if (String.valueOf(id).equals(
        valConceptoTipoPersonal)) {
        try {
          this.conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(id.longValue());
          this.conceptoLiquidacion.setConceptoTipoPersonal(this.definicionesFacade.findConceptoTipoPersonalById(id.longValue()));
        } catch (Exception e) {
          log.error("Excepcion controlada:", e);
        }
      }
    }

    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public String getSelectFrecuenciaTipoPersonal() {
    return this.selectFrecuenciaTipoPersonal;
  }
  public void setSelectFrecuenciaTipoPersonal(String valFrecuenciaTipoPersonal) {
    Iterator iterator = this.colFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
    this.conceptoLiquidacion.setFrecuenciaTipoPersonal(null);
    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      if (String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()).equals(
        valFrecuenciaTipoPersonal)) {
        this.conceptoLiquidacion.setFrecuenciaTipoPersonal(
          frecuenciaTipoPersonal);
      }
    }
    this.selectFrecuenciaTipoPersonal = valFrecuenciaTipoPersonal;
  }
  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    this.conceptoLiquidacion.setTrabajador(null);
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      if (String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador)) {
        this.conceptoLiquidacion.setTrabajador(
          trabajador);
      }
    }
    this.selectTrabajador = valTrabajador;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoLiquidacion getConceptoLiquidacion() {
    if (this.conceptoLiquidacion == null) {
      this.conceptoLiquidacion = new ConceptoLiquidacion();
    }
    return this.conceptoLiquidacion;
  }

  public ConceptoLiquidacionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoLiquidacion.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getColFrecuenciaTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()), 
        frecuenciaTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColTrabajador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndIdTipoPersonal(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidosAndIdTipoPersonal(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findConceptoLiquidacionByTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectTrabajador();
      if (!this.adding) {
        this.result = 
          this.trabajadorFacade.findConceptoLiquidacionByTrabajador(
          this.trabajador.getIdTrabajador());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectConceptoLiquidacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConceptoTipoPersonal = null;
    this.selectFrecuenciaTipoPersonal = null;
    this.selectTrabajador = null;

    long idConceptoLiquidacion = 
      Long.parseLong((String)requestParameterMap.get("idConceptoLiquidacion"));
    try
    {
      this.conceptoLiquidacion = 
        this.trabajadorFacade.findConceptoLiquidacionById(
        idConceptoLiquidacion);

      if (this.conceptoLiquidacion.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.conceptoLiquidacion.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }
      if (this.conceptoLiquidacion.getFrecuenciaTipoPersonal() != null) {
        this.selectFrecuenciaTipoPersonal = 
          String.valueOf(this.conceptoLiquidacion.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal());
      }
      if (this.conceptoLiquidacion.getTrabajador() != null) {
        this.selectTrabajador = 
          String.valueOf(this.conceptoLiquidacion.getTrabajador().getIdTrabajador());
      }

      this.colConceptoTipoPersonal = 
        this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(this.trabajador.getTipoPersonal().getIdTipoPersonal());
      this.colFrecuenciaTipoPersonal = 
        this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(this.trabajador.getTipoPersonal().getIdTipoPersonal());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.conceptoLiquidacion.getFechaRegistro() != null) && 
      (this.conceptoLiquidacion.getFechaRegistro().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Registro no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.conceptoLiquidacion.setTrabajador(
          this.trabajador);
        this.conceptoLiquidacion.setFechaRegistro(new Date());
        this.trabajadorFacade.addConceptoLiquidacion(
          this.conceptoLiquidacion);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.conceptoLiquidacion, this.trabajador.getPersonal(), this.conceptoLiquidacion.getConceptoTipoPersonal().getConcepto(), this.conceptoLiquidacion.getMonto(), this.conceptoLiquidacion.getFrecuenciaTipoPersonal().getFrecuenciaPago());

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.trabajadorFacade.updateConceptoLiquidacion(
          this.conceptoLiquidacion);

        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.conceptoLiquidacion, this.trabajador.getPersonal(), this.conceptoLiquidacion.getConceptoTipoPersonal().getConcepto(), this.conceptoLiquidacion.getMonto(), this.conceptoLiquidacion.getFrecuenciaTipoPersonal().getFrecuenciaPago());

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e)
    {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.trabajadorFacade.deleteConceptoLiquidacion(
        this.conceptoLiquidacion);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.conceptoLiquidacion, this.trabajador.getPersonal(), this.conceptoLiquidacion.getConceptoTipoPersonal().getConcepto(), this.conceptoLiquidacion.getMonto(), this.conceptoLiquidacion.getFrecuenciaTipoPersonal().getFrecuenciaPago());

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;

      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedTrabajador = true;

    this.selectConceptoTipoPersonal = null;

    this.selectFrecuenciaTipoPersonal = null;

    this.selectTrabajador = null;
    try
    {
      this.colConceptoTipoPersonal = 
        this.definicionesFacade.findConceptoTipoPersonalByIdTipoPersonal(this.trabajador.getTipoPersonal().getIdTipoPersonal());
      this.colFrecuenciaTipoPersonal = 
        this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(this.trabajador.getTipoPersonal().getIdTipoPersonal());
    }
    catch (Exception localException)
    {
    }
    this.conceptoLiquidacion = new ConceptoLiquidacion();

    this.conceptoLiquidacion.setTrabajador(this.trabajador);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoLiquidacion.setIdConceptoLiquidacion(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoLiquidacion"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.conceptoLiquidacion = new ConceptoLiquidacion();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;

    this.conceptoLiquidacion = new ConceptoLiquidacion();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedTrabajador));
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedTrabajador);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }
}