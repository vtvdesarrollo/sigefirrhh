package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class EmbargoConceptoPK
  implements Serializable
{
  public long idEmbargoConcepto;

  public EmbargoConceptoPK()
  {
  }

  public EmbargoConceptoPK(long idEmbargoConcepto)
  {
    this.idEmbargoConcepto = idEmbargoConcepto;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EmbargoConceptoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EmbargoConceptoPK thatPK)
  {
    return 
      this.idEmbargoConcepto == thatPK.idEmbargoConcepto;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEmbargoConcepto)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEmbargoConcepto);
  }
}