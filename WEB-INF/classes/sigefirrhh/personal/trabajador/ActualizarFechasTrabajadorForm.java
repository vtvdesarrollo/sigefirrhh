package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ActualizarFechasTrabajadorForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ActualizarFechasTrabajadorForm.class.getName());
  private boolean trabajadorEgresado;
  private Trabajador trabajador;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private CargoNoGenFacade cargoFacade = new CargoNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private Collection resultTrabajador;
  private Personal personal;
  private int findTrabajadorCedula;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private int findTrabajadorCodigoNomina;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private boolean showResultTrabajador;
  private String findSelectPersonal;
  private Collection colPersonal;
  private Collection findColTipoPersonal;
  private boolean showData;
  private String findSelectTrabajadorIdTipoPersonal;
  private Object stateResultPersonal = null;

  private Object stateResultTrabajadorByPersonal = null;

  private void resetResultTrabajador()
  {
    this.resultTrabajador = null;

    this.trabajador = null;

    this.showResultTrabajador = false;
  }
  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndIdTipoPersonal(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());

      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
      log.error("showResult" + this.showResultTrabajador);
      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidosAndIdTipoPersonal(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public void setFindSelectTrabajadorIdTipoPersonal(String string)
  {
    this.findSelectTrabajadorIdTipoPersonal = string;
  }

  public Trabajador getTrabajador()
  {
    if (this.trabajador == null) {
      this.trabajador = new Trabajador();
    }
    return this.trabajador;
  }

  public ActualizarFechasTrabajadorForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getFindColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);

      this.trabajadorEgresado = false;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    this.selected = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.trabajador.getFechaIngreso() != null) && 
      (this.trabajador.getFechaIngreso().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La  Fecha Ingreso Organismo no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaIngresoApn() != null) && 
      (this.trabajador.getFechaIngresoApn().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La  Fecha Ingreso APN no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaVacaciones() != null) && 
      (this.trabajador.getFechaVacaciones().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La  Fecha Vacaciones no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaPrestaciones() != null) && 
      (this.trabajador.getFechaPrestaciones().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La  Fecha Prestaciones no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaAntiguedad() != null) && 
      (this.trabajador.getFechaAntiguedad().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La  Fecha Antiguedad no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trabajador.getFechaJubilacion() != null) && 
      (this.trabajador.getFechaJubilacion().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La  Fecha Jubilacion no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.trabajador.setPersonal(
          this.personal);
        this.trabajadorFacade.addTrabajador(
          this.trabajador);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.trabajador, this.trabajador.getPersonal());

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        if (this.trabajador.getFechaAntiguedad() != null) {
          this.trabajador.setDiaAntiguedad(this.trabajador.getFechaAntiguedad().getDate());
          this.trabajador.setMesAntiguedad(this.trabajador.getFechaAntiguedad().getMonth() + 1);
          this.trabajador.setAnioAntiguedad(this.trabajador.getFechaAntiguedad().getYear() + 1900);
        }
        if (this.trabajador.getFechaIngreso() != null) {
          this.trabajador.setDiaIngreso(this.trabajador.getFechaIngreso().getDate());
          this.trabajador.setMesIngreso(this.trabajador.getFechaIngreso().getMonth() + 1);
          this.trabajador.setAnioIngreso(this.trabajador.getFechaIngreso().getYear() + 1900);
        }
        if (this.trabajador.getFechaIngresoApn() != null) {
          this.trabajador.setDiaIngresoApn(this.trabajador.getFechaIngresoApn().getDate());
          this.trabajador.setMesIngresoApn(this.trabajador.getFechaIngresoApn().getMonth() + 1);
          this.trabajador.setAnioIngresoApn(this.trabajador.getFechaIngresoApn().getYear() + 1900);
        }
        if (this.trabajador.getFechaJubilacion() != null) {
          this.trabajador.setDiaJubilacion(this.trabajador.getFechaJubilacion().getDate());
          this.trabajador.setMesJubilacion(this.trabajador.getFechaJubilacion().getMonth() + 1);
          this.trabajador.setAnioJubilacion(this.trabajador.getFechaJubilacion().getYear() + 1900);
        }
        if (this.trabajador.getFechaPrestaciones() != null) {
          this.trabajador.setDiaPrestaciones(this.trabajador.getFechaPrestaciones().getDate());
          this.trabajador.setMesPrestaciones(this.trabajador.getFechaPrestaciones().getMonth() + 1);
          this.trabajador.setAnioPrestaciones(this.trabajador.getFechaPrestaciones().getYear() + 1900);
        }
        if (this.trabajador.getFechaVacaciones() != null) {
          this.trabajador.setDiaVacaciones(this.trabajador.getFechaVacaciones().getDate());
          this.trabajador.setMesVacaciones(this.trabajador.getFechaVacaciones().getMonth() + 1);
          this.trabajador.setAnioVacaciones(this.trabajador.getFechaVacaciones().getYear() + 1900);
        }
        if (this.trabajador.getFechaEgreso() != null) {
          this.trabajador.setDiaEgreso(this.trabajador.getFechaEgreso().getDate());
          this.trabajador.setMesEgreso(this.trabajador.getFechaEgreso().getMonth() + 1);
          this.trabajador.setAnioEgreso(this.trabajador.getFechaEgreso().getYear() + 1900);
        }

        this.trabajadorFacade.updateTrabajador(
          this.trabajador);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.trabajador, this.trabajador.getPersonal());

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    } catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    } catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.trabajadorFacade.deleteTrabajador(
        this.trabajador);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.trabajador, this.trabajador.getPersonal());

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public boolean isShowBancoNominaAux() {
    try {
      return this.trabajador.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowTipoCtaNominaAux() {
    try {
      return this.trabajador.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowCuentaNominaAux() {
    try {
      return this.trabajador.getFormaPago().equals("1"); } catch (Exception e) {
    }
    return false;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.trabajador = new Trabajador();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.trabajador = new Trabajador();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return this.selected;
  }

  public boolean isShowResultPersonal()
  {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return this.adding;
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getFindTrabajadorCedula()
  {
    return this.findTrabajadorCedula;
  }

  public int getFindTrabajadorCodigoNomina()
  {
    return this.findTrabajadorCodigoNomina;
  }

  public String getFindTrabajadorPrimerApellido()
  {
    return this.findTrabajadorPrimerApellido;
  }

  public String getFindTrabajadorPrimerNombre()
  {
    return this.findTrabajadorPrimerNombre;
  }

  public String getFindTrabajadorSegundoApellido()
  {
    return this.findTrabajadorSegundoApellido;
  }

  public String getFindTrabajadorSegundoNombre()
  {
    return this.findTrabajadorSegundoNombre;
  }

  public void setFindTrabajadorCedula(int i)
  {
    this.findTrabajadorCedula = i;
  }

  public void setFindTrabajadorCodigoNomina(int i)
  {
    this.findTrabajadorCodigoNomina = i;
  }

  public void setFindTrabajadorPrimerApellido(String string)
  {
    this.findTrabajadorPrimerApellido = string;
  }

  public void setFindTrabajadorPrimerNombre(String string)
  {
    this.findTrabajadorPrimerNombre = string;
  }

  public void setFindTrabajadorSegundoApellido(String string)
  {
    this.findTrabajadorSegundoApellido = string;
  }

  public void setFindTrabajadorSegundoNombre(String string)
  {
    this.findTrabajadorSegundoNombre = string;
  }

  public String getFindSelectTrabajadorIdTipoPersonal()
  {
    return this.findSelectTrabajadorIdTipoPersonal;
  }

  public boolean isShowResultTrabajador()
  {
    return this.showResultTrabajador;
  }

  public Collection getResultTrabajador()
  {
    return this.resultTrabajador;
  }

  public void setShowData(boolean b)
  {
    this.showData = b;
  }

  public boolean isTrabajadorEgresado() {
    return this.trabajadorEgresado;
  }
}