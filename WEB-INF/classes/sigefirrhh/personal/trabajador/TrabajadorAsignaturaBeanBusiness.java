package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.docente.Asignatura;
import sigefirrhh.base.docente.AsignaturaBeanBusiness;

public class TrabajadorAsignaturaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTrabajadorAsignatura(TrabajadorAsignatura trabajadorAsignatura)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    TrabajadorAsignatura trabajadorAsignaturaNew = 
      (TrabajadorAsignatura)BeanUtils.cloneBean(
      trabajadorAsignatura);

    AsignaturaBeanBusiness asignaturaBeanBusiness = new AsignaturaBeanBusiness();

    if (trabajadorAsignaturaNew.getAsignatura() != null) {
      trabajadorAsignaturaNew.setAsignatura(
        asignaturaBeanBusiness.findAsignaturaById(
        trabajadorAsignaturaNew.getAsignatura().getIdAsignatura()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (trabajadorAsignaturaNew.getTrabajador() != null) {
      trabajadorAsignaturaNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        trabajadorAsignaturaNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(trabajadorAsignaturaNew);
  }

  public void updateTrabajadorAsignatura(TrabajadorAsignatura trabajadorAsignatura) throws Exception
  {
    TrabajadorAsignatura trabajadorAsignaturaModify = 
      findTrabajadorAsignaturaById(trabajadorAsignatura.getIdTrabajadorAsignatura());

    AsignaturaBeanBusiness asignaturaBeanBusiness = new AsignaturaBeanBusiness();

    if (trabajadorAsignatura.getAsignatura() != null) {
      trabajadorAsignatura.setAsignatura(
        asignaturaBeanBusiness.findAsignaturaById(
        trabajadorAsignatura.getAsignatura().getIdAsignatura()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (trabajadorAsignatura.getTrabajador() != null) {
      trabajadorAsignatura.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        trabajadorAsignatura.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(trabajadorAsignaturaModify, trabajadorAsignatura);
  }

  public void deleteTrabajadorAsignatura(TrabajadorAsignatura trabajadorAsignatura) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TrabajadorAsignatura trabajadorAsignaturaDelete = 
      findTrabajadorAsignaturaById(trabajadorAsignatura.getIdTrabajadorAsignatura());
    pm.deletePersistent(trabajadorAsignaturaDelete);
  }

  public TrabajadorAsignatura findTrabajadorAsignaturaById(long idTrabajadorAsignatura) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTrabajadorAsignatura == pIdTrabajadorAsignatura";
    Query query = pm.newQuery(TrabajadorAsignatura.class, filter);

    query.declareParameters("long pIdTrabajadorAsignatura");

    parameters.put("pIdTrabajadorAsignatura", new Long(idTrabajadorAsignatura));

    Collection colTrabajadorAsignatura = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTrabajadorAsignatura.iterator();
    return (TrabajadorAsignatura)iterator.next();
  }

  public Collection findTrabajadorAsignaturaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent trabajadorAsignaturaExtent = pm.getExtent(
      TrabajadorAsignatura.class, true);
    Query query = pm.newQuery(trabajadorAsignaturaExtent);
    query.setOrdering("asignatura.nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(TrabajadorAsignatura.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("asignatura.nombre ascending");

    Collection colTrabajadorAsignatura = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajadorAsignatura);

    return colTrabajadorAsignatura;
  }
}