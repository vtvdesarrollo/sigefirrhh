package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.personal.expediente.Personal;

public class CajaAhorro
  implements Serializable, PersistenceCapable
{
  private long idCajaAhorro;
  private double porcentajeTrabajador;
  private double porcentajePatron;
  private double aporteTrabajador;
  private double aportePatron;
  private double acumuladoTrabajador;
  private double acumuladoPatron;
  private double acumuladoRetiros;
  private double acumuladoHaberes;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "acumuladoHaberes", "acumuladoPatron", "acumuladoRetiros", "acumuladoTrabajador", "aportePatron", "aporteTrabajador", "conceptoTipoPersonal", "idCajaAhorro", "porcentajePatron", "porcentajeTrabajador", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Long.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 26, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public CajaAhorro()
  {
    jdoSetporcentajeTrabajador(this, 0.0D);

    jdoSetporcentajePatron(this, 0.0D);

    jdoSetaporteTrabajador(this, 0.0D);

    jdoSetaportePatron(this, 0.0D);

    jdoSetacumuladoTrabajador(this, 0.0D);

    jdoSetacumuladoPatron(this, 0.0D);

    jdoSetacumuladoRetiros(this, 0.0D);

    jdoSetacumuladoHaberes(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetaporteTrabajador(this));

    return jdoGettrabajador(this).getCedula() + "  -  " + 
      jdoGettrabajador(this).getPersonal().getPrimerApellido() + " " + 
      jdoGettrabajador(this).getPersonal().getPrimerNombre() + " % --" + a;
  }

  public double getAportePatron()
  {
    return jdoGetaportePatron(this);
  }
  public void setAportePatron(double aportePatron) {
    jdoSetaportePatron(this, aportePatron);
  }
  public double getAporteTrabajador() {
    return jdoGetaporteTrabajador(this);
  }
  public void setAporteTrabajador(double aporteTrabajador) {
    jdoSetaporteTrabajador(this, aporteTrabajador);
  }
  public double getAcumuladoHaberes() {
    return jdoGetacumuladoHaberes(this);
  }
  public void setAcumuladoHaberes(double acumuladoHaberes) {
    jdoSetacumuladoHaberes(this, acumuladoHaberes);
  }

  public double getAcumuladoPatron()
  {
    return jdoGetacumuladoPatron(this);
  }

  public double getAcumuladoRetiros()
  {
    return jdoGetacumuladoRetiros(this);
  }

  public double getAcumuladoTrabajador()
  {
    return jdoGetacumuladoTrabajador(this);
  }

  public long getIdCajaAhorro()
  {
    return jdoGetidCajaAhorro(this);
  }

  public double getPorcentajeTrabajador()
  {
    return jdoGetporcentajeTrabajador(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAcumuladoPatron(double d)
  {
    jdoSetacumuladoPatron(this, d);
  }

  public void setAcumuladoRetiros(double d)
  {
    jdoSetacumuladoRetiros(this, d);
  }

  public void setAcumuladoTrabajador(double d)
  {
    jdoSetacumuladoTrabajador(this, d);
  }

  public void setIdCajaAhorro(long l)
  {
    jdoSetidCajaAhorro(this, l);
  }

  public void setPorcentajeTrabajador(double d)
  {
    jdoSetporcentajeTrabajador(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public double getPorcentajePatron() {
    return jdoGetporcentajePatron(this);
  }
  public void setPorcentajePatron(double porcentajePatron) {
    jdoSetporcentajePatron(this, porcentajePatron);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.CajaAhorro"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CajaAhorro());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CajaAhorro localCajaAhorro = new CajaAhorro();
    localCajaAhorro.jdoFlags = 1;
    localCajaAhorro.jdoStateManager = paramStateManager;
    return localCajaAhorro;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CajaAhorro localCajaAhorro = new CajaAhorro();
    localCajaAhorro.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCajaAhorro.jdoFlags = 1;
    localCajaAhorro.jdoStateManager = paramStateManager;
    return localCajaAhorro;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.acumuladoHaberes);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.acumuladoPatron);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.acumuladoRetiros);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.acumuladoTrabajador);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.aportePatron);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.aporteTrabajador);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCajaAhorro);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajePatron);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeTrabajador);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.acumuladoHaberes = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.acumuladoPatron = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.acumuladoRetiros = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.acumuladoTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aportePatron = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aporteTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCajaAhorro = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajePatron = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CajaAhorro paramCajaAhorro, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCajaAhorro == null)
        throw new IllegalArgumentException("arg1");
      this.acumuladoHaberes = paramCajaAhorro.acumuladoHaberes;
      return;
    case 1:
      if (paramCajaAhorro == null)
        throw new IllegalArgumentException("arg1");
      this.acumuladoPatron = paramCajaAhorro.acumuladoPatron;
      return;
    case 2:
      if (paramCajaAhorro == null)
        throw new IllegalArgumentException("arg1");
      this.acumuladoRetiros = paramCajaAhorro.acumuladoRetiros;
      return;
    case 3:
      if (paramCajaAhorro == null)
        throw new IllegalArgumentException("arg1");
      this.acumuladoTrabajador = paramCajaAhorro.acumuladoTrabajador;
      return;
    case 4:
      if (paramCajaAhorro == null)
        throw new IllegalArgumentException("arg1");
      this.aportePatron = paramCajaAhorro.aportePatron;
      return;
    case 5:
      if (paramCajaAhorro == null)
        throw new IllegalArgumentException("arg1");
      this.aporteTrabajador = paramCajaAhorro.aporteTrabajador;
      return;
    case 6:
      if (paramCajaAhorro == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramCajaAhorro.conceptoTipoPersonal;
      return;
    case 7:
      if (paramCajaAhorro == null)
        throw new IllegalArgumentException("arg1");
      this.idCajaAhorro = paramCajaAhorro.idCajaAhorro;
      return;
    case 8:
      if (paramCajaAhorro == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajePatron = paramCajaAhorro.porcentajePatron;
      return;
    case 9:
      if (paramCajaAhorro == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeTrabajador = paramCajaAhorro.porcentajeTrabajador;
      return;
    case 10:
      if (paramCajaAhorro == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramCajaAhorro.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CajaAhorro))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CajaAhorro localCajaAhorro = (CajaAhorro)paramObject;
    if (localCajaAhorro.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCajaAhorro, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CajaAhorroPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CajaAhorroPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CajaAhorroPK))
      throw new IllegalArgumentException("arg1");
    CajaAhorroPK localCajaAhorroPK = (CajaAhorroPK)paramObject;
    localCajaAhorroPK.idCajaAhorro = this.idCajaAhorro;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CajaAhorroPK))
      throw new IllegalArgumentException("arg1");
    CajaAhorroPK localCajaAhorroPK = (CajaAhorroPK)paramObject;
    this.idCajaAhorro = localCajaAhorroPK.idCajaAhorro;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CajaAhorroPK))
      throw new IllegalArgumentException("arg2");
    CajaAhorroPK localCajaAhorroPK = (CajaAhorroPK)paramObject;
    localCajaAhorroPK.idCajaAhorro = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CajaAhorroPK))
      throw new IllegalArgumentException("arg2");
    CajaAhorroPK localCajaAhorroPK = (CajaAhorroPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localCajaAhorroPK.idCajaAhorro);
  }

  private static final double jdoGetacumuladoHaberes(CajaAhorro paramCajaAhorro)
  {
    if (paramCajaAhorro.jdoFlags <= 0)
      return paramCajaAhorro.acumuladoHaberes;
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
      return paramCajaAhorro.acumuladoHaberes;
    if (localStateManager.isLoaded(paramCajaAhorro, jdoInheritedFieldCount + 0))
      return paramCajaAhorro.acumuladoHaberes;
    return localStateManager.getDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 0, paramCajaAhorro.acumuladoHaberes);
  }

  private static final void jdoSetacumuladoHaberes(CajaAhorro paramCajaAhorro, double paramDouble)
  {
    if (paramCajaAhorro.jdoFlags == 0)
    {
      paramCajaAhorro.acumuladoHaberes = paramDouble;
      return;
    }
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
    {
      paramCajaAhorro.acumuladoHaberes = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 0, paramCajaAhorro.acumuladoHaberes, paramDouble);
  }

  private static final double jdoGetacumuladoPatron(CajaAhorro paramCajaAhorro)
  {
    if (paramCajaAhorro.jdoFlags <= 0)
      return paramCajaAhorro.acumuladoPatron;
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
      return paramCajaAhorro.acumuladoPatron;
    if (localStateManager.isLoaded(paramCajaAhorro, jdoInheritedFieldCount + 1))
      return paramCajaAhorro.acumuladoPatron;
    return localStateManager.getDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 1, paramCajaAhorro.acumuladoPatron);
  }

  private static final void jdoSetacumuladoPatron(CajaAhorro paramCajaAhorro, double paramDouble)
  {
    if (paramCajaAhorro.jdoFlags == 0)
    {
      paramCajaAhorro.acumuladoPatron = paramDouble;
      return;
    }
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
    {
      paramCajaAhorro.acumuladoPatron = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 1, paramCajaAhorro.acumuladoPatron, paramDouble);
  }

  private static final double jdoGetacumuladoRetiros(CajaAhorro paramCajaAhorro)
  {
    if (paramCajaAhorro.jdoFlags <= 0)
      return paramCajaAhorro.acumuladoRetiros;
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
      return paramCajaAhorro.acumuladoRetiros;
    if (localStateManager.isLoaded(paramCajaAhorro, jdoInheritedFieldCount + 2))
      return paramCajaAhorro.acumuladoRetiros;
    return localStateManager.getDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 2, paramCajaAhorro.acumuladoRetiros);
  }

  private static final void jdoSetacumuladoRetiros(CajaAhorro paramCajaAhorro, double paramDouble)
  {
    if (paramCajaAhorro.jdoFlags == 0)
    {
      paramCajaAhorro.acumuladoRetiros = paramDouble;
      return;
    }
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
    {
      paramCajaAhorro.acumuladoRetiros = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 2, paramCajaAhorro.acumuladoRetiros, paramDouble);
  }

  private static final double jdoGetacumuladoTrabajador(CajaAhorro paramCajaAhorro)
  {
    if (paramCajaAhorro.jdoFlags <= 0)
      return paramCajaAhorro.acumuladoTrabajador;
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
      return paramCajaAhorro.acumuladoTrabajador;
    if (localStateManager.isLoaded(paramCajaAhorro, jdoInheritedFieldCount + 3))
      return paramCajaAhorro.acumuladoTrabajador;
    return localStateManager.getDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 3, paramCajaAhorro.acumuladoTrabajador);
  }

  private static final void jdoSetacumuladoTrabajador(CajaAhorro paramCajaAhorro, double paramDouble)
  {
    if (paramCajaAhorro.jdoFlags == 0)
    {
      paramCajaAhorro.acumuladoTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
    {
      paramCajaAhorro.acumuladoTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 3, paramCajaAhorro.acumuladoTrabajador, paramDouble);
  }

  private static final double jdoGetaportePatron(CajaAhorro paramCajaAhorro)
  {
    if (paramCajaAhorro.jdoFlags <= 0)
      return paramCajaAhorro.aportePatron;
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
      return paramCajaAhorro.aportePatron;
    if (localStateManager.isLoaded(paramCajaAhorro, jdoInheritedFieldCount + 4))
      return paramCajaAhorro.aportePatron;
    return localStateManager.getDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 4, paramCajaAhorro.aportePatron);
  }

  private static final void jdoSetaportePatron(CajaAhorro paramCajaAhorro, double paramDouble)
  {
    if (paramCajaAhorro.jdoFlags == 0)
    {
      paramCajaAhorro.aportePatron = paramDouble;
      return;
    }
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
    {
      paramCajaAhorro.aportePatron = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 4, paramCajaAhorro.aportePatron, paramDouble);
  }

  private static final double jdoGetaporteTrabajador(CajaAhorro paramCajaAhorro)
  {
    if (paramCajaAhorro.jdoFlags <= 0)
      return paramCajaAhorro.aporteTrabajador;
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
      return paramCajaAhorro.aporteTrabajador;
    if (localStateManager.isLoaded(paramCajaAhorro, jdoInheritedFieldCount + 5))
      return paramCajaAhorro.aporteTrabajador;
    return localStateManager.getDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 5, paramCajaAhorro.aporteTrabajador);
  }

  private static final void jdoSetaporteTrabajador(CajaAhorro paramCajaAhorro, double paramDouble)
  {
    if (paramCajaAhorro.jdoFlags == 0)
    {
      paramCajaAhorro.aporteTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
    {
      paramCajaAhorro.aporteTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 5, paramCajaAhorro.aporteTrabajador, paramDouble);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(CajaAhorro paramCajaAhorro)
  {
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
      return paramCajaAhorro.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramCajaAhorro, jdoInheritedFieldCount + 6))
      return paramCajaAhorro.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramCajaAhorro, jdoInheritedFieldCount + 6, paramCajaAhorro.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(CajaAhorro paramCajaAhorro, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
    {
      paramCajaAhorro.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramCajaAhorro, jdoInheritedFieldCount + 6, paramCajaAhorro.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final long jdoGetidCajaAhorro(CajaAhorro paramCajaAhorro)
  {
    return paramCajaAhorro.idCajaAhorro;
  }

  private static final void jdoSetidCajaAhorro(CajaAhorro paramCajaAhorro, long paramLong)
  {
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
    {
      paramCajaAhorro.idCajaAhorro = paramLong;
      return;
    }
    localStateManager.setLongField(paramCajaAhorro, jdoInheritedFieldCount + 7, paramCajaAhorro.idCajaAhorro, paramLong);
  }

  private static final double jdoGetporcentajePatron(CajaAhorro paramCajaAhorro)
  {
    if (paramCajaAhorro.jdoFlags <= 0)
      return paramCajaAhorro.porcentajePatron;
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
      return paramCajaAhorro.porcentajePatron;
    if (localStateManager.isLoaded(paramCajaAhorro, jdoInheritedFieldCount + 8))
      return paramCajaAhorro.porcentajePatron;
    return localStateManager.getDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 8, paramCajaAhorro.porcentajePatron);
  }

  private static final void jdoSetporcentajePatron(CajaAhorro paramCajaAhorro, double paramDouble)
  {
    if (paramCajaAhorro.jdoFlags == 0)
    {
      paramCajaAhorro.porcentajePatron = paramDouble;
      return;
    }
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
    {
      paramCajaAhorro.porcentajePatron = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 8, paramCajaAhorro.porcentajePatron, paramDouble);
  }

  private static final double jdoGetporcentajeTrabajador(CajaAhorro paramCajaAhorro)
  {
    if (paramCajaAhorro.jdoFlags <= 0)
      return paramCajaAhorro.porcentajeTrabajador;
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
      return paramCajaAhorro.porcentajeTrabajador;
    if (localStateManager.isLoaded(paramCajaAhorro, jdoInheritedFieldCount + 9))
      return paramCajaAhorro.porcentajeTrabajador;
    return localStateManager.getDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 9, paramCajaAhorro.porcentajeTrabajador);
  }

  private static final void jdoSetporcentajeTrabajador(CajaAhorro paramCajaAhorro, double paramDouble)
  {
    if (paramCajaAhorro.jdoFlags == 0)
    {
      paramCajaAhorro.porcentajeTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
    {
      paramCajaAhorro.porcentajeTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCajaAhorro, jdoInheritedFieldCount + 9, paramCajaAhorro.porcentajeTrabajador, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(CajaAhorro paramCajaAhorro)
  {
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
      return paramCajaAhorro.trabajador;
    if (localStateManager.isLoaded(paramCajaAhorro, jdoInheritedFieldCount + 10))
      return paramCajaAhorro.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramCajaAhorro, jdoInheritedFieldCount + 10, paramCajaAhorro.trabajador);
  }

  private static final void jdoSettrabajador(CajaAhorro paramCajaAhorro, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramCajaAhorro.jdoStateManager;
    if (localStateManager == null)
    {
      paramCajaAhorro.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramCajaAhorro, jdoInheritedFieldCount + 10, paramCajaAhorro.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}