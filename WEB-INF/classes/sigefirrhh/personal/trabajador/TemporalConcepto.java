package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class TemporalConcepto
  implements Serializable
{
  private long idOrganismo;
  private long idTipoPersonal;
  private long idFrecuenciaPago;
  private String codigoConcepto;
  private int cedula;
  private double monto;

  public int getCedula()
  {
    return this.cedula;
  }

  public String getCodigoConcepto() {
    return this.codigoConcepto;
  }

  public long getIdOrganismo() {
    return this.idOrganismo;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }

  public double getMonto() {
    return this.monto;
  }

  public void setCedula(int i) {
    this.cedula = i;
  }

  public void setCodigoConcepto(String string) {
    this.codigoConcepto = string;
  }

  public void setIdOrganismo(long l) {
    this.idOrganismo = l;
  }

  public void setIdTipoPersonal(long l) {
    this.idTipoPersonal = l;
  }

  public void setMonto(double d) {
    this.monto = d;
  }

  public long getIdFrecuenciaPago() {
    return this.idFrecuenciaPago;
  }

  public void setIdFrecuenciaPago(long l) {
    this.idFrecuenciaPago = l;
  }
}