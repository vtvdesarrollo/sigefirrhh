package sigefirrhh.personal.trabajador;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class CargaMasivaPrestamosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CargaMasivaPrestamosForm.class.getName());
  private boolean showEliminar;
  private String accion = "A";
  private Collection listFrecuenciaTipoPersonal;
  private Collection listTipoPersonal;
  private Collection listConcepto;
  private LoginSession login;
  private DefinicionesFacadeExtend definicionesFacade;
  private TipoPersonal tipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private ConceptoTipoPersonal concepto;
  private long idFrecuenciaTipoPersonal;
  private long idConcepto;
  private long idTipoPersonal;
  private int cedula;
  private double montoPrestamo;
  private double montoCuota;
  private int numeroCuotas;
  private int cedulaFinal;
  private double montoPrestamoFinal;
  private double montoCuotaFinal;
  private String nombre;
  private String apellido;
  private java.util.Date fechaOtorgo = new java.util.Date();
  private java.util.Date fechaComienzo = new java.util.Date();
  private ConceptoFijo conceptoFijo;
  private ConceptoVariable conceptoVariable;
  private Collection colConceptoFijo;
  private Collection colConceptoVariable;
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  private java.util.Date fechaActual = new java.util.Date();
  java.sql.Date fechaActualSql = new java.sql.Date(this.fechaActual.getYear(), this.fechaActual.getMonth(), this.fechaActual.getDate());

  private Connection connection = Resource.getConnection();
  Statement stExecute = null;
  StringBuffer sql = new StringBuffer();

  private ResultSet rsRegistroTrabajador = null;
  private PreparedStatement stRegistroTrabajador = null;
  private ResultSet rsRegistroConceptoFijo = null;
  private PreparedStatement stRegistroConceptoFijo = null;
  private long idTrabajador = 0L;
  private boolean conceptoFijoExiste = false;
  private boolean conceptoVariableExiste = false;

  public CargaMasivaPrestamosForm() { FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesFacade = new DefinicionesFacadeExtend();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    try
    {
      this.connection.setAutoCommit(true);
      this.connection = Resource.getConnection();

      this.sql = new StringBuffer();
      this.sql.append("select t.id_trabajador, p.primer_nombre, p.primer_apellido, tu.jornada_diaria, tu.jornada_semanal ");
      this.sql.append(" from trabajador t, personal p, turno tu");
      this.sql.append(" where t.id_personal = p.id_personal");
      this.sql.append(" and tu.id_turno = t.id_turno");
      this.sql.append(" and t.estatus = 'A'");
      this.sql.append(" and  t.cedula = ? and t.id_tipo_personal = ?");

      this.stRegistroTrabajador = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      return;
    }

    refresh(); }

  public void refresh()
  {
    try {
      this.listTipoPersonal = new DefinicionesNoGenFacade().findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.frecuenciaTipoPersonal = null;
    this.idFrecuenciaTipoPersonal = 0L;
    this.listFrecuenciaTipoPersonal = null;

    this.concepto = null;
    this.idConcepto = 0L;
    this.listConcepto = null;
    try
    {
      if (idTipoPersonal > 0L) {
        this.listFrecuenciaTipoPersonal = 
          this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(idTipoPersonal);

        this.listConcepto = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonalAndTipoPrestamo(
          idTipoPersonal, "S");
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    this.nombre = "";
    this.apellido = "";
    this.montoPrestamo = 0.0D;
    this.montoCuota = 0.0D;
    this.montoPrestamoFinal = 0.0D;
    this.montoCuotaFinal = 0.0D;
    this.cedulaFinal = 0;
  }

  public String agregar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.stRegistroTrabajador.setInt(1, this.cedula);
      this.stRegistroTrabajador.setLong(2, this.idTipoPersonal);
      this.rsRegistroTrabajador = this.stRegistroTrabajador.executeQuery();

      if (this.rsRegistroTrabajador.next()) {
        this.idTrabajador = this.rsRegistroTrabajador.getLong("id_trabajador");
        this.nombre = this.rsRegistroTrabajador.getString("primer_nombre");
        this.apellido = this.rsRegistroTrabajador.getString("primer_apellido");
      }

      log.error("ID_TRABAJADOR  = " + this.idTrabajador);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador no existe", ""));
      return null;
    }
    this.sql = new StringBuffer();
    try
    {
      java.sql.Date fechaOtorgoSql = new java.sql.Date(this.fechaOtorgo.getYear(), this.fechaOtorgo.getMonth(), this.fechaOtorgo.getDate());
      java.sql.Date fechaComienzoSql = new java.sql.Date(this.fechaComienzo.getYear(), this.fechaComienzo.getMonth(), this.fechaComienzo.getDate());

      this.numeroCuotas = ((int)Math.ceil(this.montoPrestamo / this.montoCuota));
      this.stExecute = this.connection.createStatement();
      log.error("PASO POR AQUI");
      log.error("fechaOtorgo" + fechaOtorgoSql);
      log.error("fechaComienzo" + fechaComienzoSql);
      log.error("fechaActual" + this.fechaActualSql);

      this.sql.append("insert into prestamo (id_trabajador, id_concepto_tipo_personal, ");
      this.sql.append("id_frecuencia_tipo_personal, monto_prestamo, monto_cuota, ");
      this.sql.append("monto_pagado, numero_cuotas, cuotas_pagadas, fecha_otorgo, ");
      this.sql.append("fecha_comienzo_pago, fecha_registro, documento_soporte, estatus, id_prestamo) values(");
      this.sql.append(this.idTrabajador + ", " + this.concepto.getIdConceptoTipoPersonal() + ", ");
      this.sql.append(this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ", ");
      this.sql.append(this.montoPrestamo + ", " + this.montoCuota + ", 0, " + this.numeroCuotas + ", 0, '");
      this.sql.append(fechaOtorgoSql + "', '" + fechaComienzoSql + "', '" + this.fechaActualSql + "', ");
      this.sql.append("null, 'A', " + this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.Prestamo") + ")");

      this.stExecute.execute(this.sql.toString());
      this.stExecute.close();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
      return null;
    }

    this.cedulaFinal = this.cedula;
    this.montoPrestamoFinal = this.montoPrestamo;
    this.montoCuotaFinal = this.montoCuota;

    this.cedula = 0;
    this.montoCuota = 0.0D;
    this.montoPrestamo = 0.0D;
    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    return null;
  }

  public Collection getListFrecuenciaTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFrecuenciaTipoPersonal, "sigefirrhh.base.definiciones.FrecuenciaTipoPersonal");
  }

  public Collection getListConcepto() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConcepto, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public String getIdFrecuenciaTipoPersonal() {
    return String.valueOf(this.idFrecuenciaTipoPersonal);
  }

  public void setIdFrecuenciaTipoPersonal(String l) {
    this.idFrecuenciaTipoPersonal = Long.parseLong(l);

    Iterator iterator = this.listFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;

    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      if (String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()).equals(
        l)) {
        this.frecuenciaTipoPersonal = frecuenciaTipoPersonal;
        break;
      }
    }
  }

  public String getIdConcepto()
  {
    return String.valueOf(this.idConcepto);
  }

  public void setIdConcepto(String l) {
    this.idConcepto = Long.parseLong(l);
    Iterator iterator = this.listConcepto.iterator();
    ConceptoTipoPersonal concepto = null;

    while (iterator.hasNext()) {
      concepto = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(concepto.getIdConceptoTipoPersonal()).equals(
        l)) {
        this.concepto = concepto;
        break;
      }
    }
  }

  public int getCedula() {
    return this.cedula;
  }

  public void setCedula(int cedula) {
    this.cedula = cedula;
  }

  public int getCedulaFinal() {
    return this.cedulaFinal;
  }

  public void setCedulaFinal(int cedulaFinal) {
    this.cedulaFinal = cedulaFinal;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public boolean isShowFrecuenciaTipoPersonal() {
    return (this.listFrecuenciaTipoPersonal != null) && 
      (!this.listFrecuenciaTipoPersonal.isEmpty());
  }

  public boolean isShowConcepto()
  {
    return (this.listConcepto != null) && 
      (!this.listConcepto.isEmpty());
  }

  public String getApellido()
  {
    return this.apellido;
  }

  public void setApellido(String apellido)
  {
    this.apellido = apellido;
  }

  public String getNombre()
  {
    return this.nombre;
  }

  public void setNombre(String nombre)
  {
    this.nombre = nombre;
  }

  public String getAccion()
  {
    return this.accion;
  }

  public void setAccion(String string)
  {
    this.accion = string;
  }

  public boolean isShowEliminar()
  {
    return this.accion.equals("E");
  }

  public double getMontoCuota()
  {
    return this.montoCuota;
  }

  public double getMontoCuotaFinal()
  {
    return this.montoCuotaFinal;
  }

  public double getMontoPrestamo()
  {
    return this.montoPrestamo;
  }

  public double getMontoPrestamoFinal()
  {
    return this.montoPrestamoFinal;
  }

  public int getNumeroCuotas()
  {
    return this.numeroCuotas;
  }

  public void setMontoCuota(double d)
  {
    this.montoCuota = d;
  }

  public void setMontoCuotaFinal(double d)
  {
    this.montoCuotaFinal = d;
  }

  public void setMontoPrestamo(double d)
  {
    this.montoPrestamo = d;
  }

  public void setMontoPrestamoFinal(double d)
  {
    this.montoPrestamoFinal = d;
  }

  public void setNumeroCuotas(int i)
  {
    this.numeroCuotas = i;
  }

  public java.util.Date getFechaComienzo()
  {
    return this.fechaComienzo;
  }

  public java.util.Date getFechaOtorgo()
  {
    return this.fechaOtorgo;
  }

  public void setFechaComienzo(java.util.Date date)
  {
    this.fechaComienzo = date;
  }

  public void setFechaOtorgo(java.util.Date date)
  {
    this.fechaOtorgo = date;
  }
}