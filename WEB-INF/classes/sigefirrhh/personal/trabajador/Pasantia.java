package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.adiestramiento.EntidadEducativa;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.personal.Carrera;

public class Pasantia
  implements Serializable, PersistenceCapable
{
  private long idPasantia;
  private int aniosPasantia;
  private int mesesPasantia;
  private int diasPasantia;
  private String estatusPasantia;
  private String modalidad;
  private String prorroga;
  private String resultado;
  private double montoSemanal;
  private double montoMensual;
  private Date fechaRegistro;
  private Date fechaInicio;
  private Date fechaFin;
  private String objetoPasantia;
  private String tutorAcademico;
  private String telefTutorAcad;
  private String tutorEmpresarial;
  private String telefTutorEmp;
  private String tareasPasantia;
  private Dependencia dependencia;
  private Carrera carrera;
  private EntidadEducativa entidadEducativa;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aniosPasantia", "carrera", "dependencia", "diasPasantia", "entidadEducativa", "estatusPasantia", "fechaFin", "fechaInicio", "fechaRegistro", "idPasantia", "mesesPasantia", "modalidad", "montoMensual", "montoSemanal", "objetoPasantia", "prorroga", "resultado", "tareasPasantia", "telefTutorAcad", "telefTutorEmp", "trabajador", "tutorAcademico", "tutorEmpresarial" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.personal.Carrera"), sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.adiestramiento.EntidadEducativa"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 26, 26, 21, 26, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAniosPasantia()
  {
    return jdoGetaniosPasantia(this);
  }

  public Carrera getCarrera()
  {
    return jdoGetcarrera(this);
  }

  public Dependencia getDependencia()
  {
    return jdoGetdependencia(this);
  }

  public int getDiasPasantia()
  {
    return jdoGetdiasPasantia(this);
  }

  public EntidadEducativa getEntidadEducativa()
  {
    return jdoGetentidadEducativa(this);
  }

  public String getEstatusPasantia()
  {
    return jdoGetestatusPasantia(this);
  }

  public Date getFechaFin()
  {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio()
  {
    return jdoGetfechaInicio(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public long getIdPasantia()
  {
    return jdoGetidPasantia(this);
  }

  public int getMesesPasantia()
  {
    return jdoGetmesesPasantia(this);
  }

  public String getModalidad()
  {
    return jdoGetmodalidad(this);
  }

  public double getMontoMensual()
  {
    return jdoGetmontoMensual(this);
  }

  public double getMontoSemanal()
  {
    return jdoGetmontoSemanal(this);
  }

  public String getObjetoPasantia()
  {
    return jdoGetobjetoPasantia(this);
  }

  public String getProrroga()
  {
    return jdoGetprorroga(this);
  }

  public String getResultado()
  {
    return jdoGetresultado(this);
  }

  public String getTareasPasantia()
  {
    return jdoGettareasPasantia(this);
  }

  public String getTelefTutorAcad()
  {
    return jdoGettelefTutorAcad(this);
  }

  public String getTelefTutorEmp()
  {
    return jdoGettelefTutorEmp(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public String getTutorAcademico()
  {
    return jdoGettutorAcademico(this);
  }

  public String getTutorEmpresarial()
  {
    return jdoGettutorEmpresarial(this);
  }

  public void setAniosPasantia(int i)
  {
    jdoSetaniosPasantia(this, i);
  }

  public void setCarrera(Carrera carrera)
  {
    jdoSetcarrera(this, carrera);
  }

  public void setDependencia(Dependencia dependencia)
  {
    jdoSetdependencia(this, dependencia);
  }

  public void setDiasPasantia(int i)
  {
    jdoSetdiasPasantia(this, i);
  }

  public void setEntidadEducativa(EntidadEducativa educativa)
  {
    jdoSetentidadEducativa(this, educativa);
  }

  public void setEstatusPasantia(String string)
  {
    jdoSetestatusPasantia(this, string);
  }

  public void setFechaFin(Date date)
  {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date)
  {
    jdoSetfechaInicio(this, date);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setIdPasantia(long l)
  {
    jdoSetidPasantia(this, l);
  }

  public void setMesesPasantia(int i)
  {
    jdoSetmesesPasantia(this, i);
  }

  public void setModalidad(String string)
  {
    jdoSetmodalidad(this, string);
  }

  public void setMontoMensual(double d)
  {
    jdoSetmontoMensual(this, d);
  }

  public void setMontoSemanal(double d)
  {
    jdoSetmontoSemanal(this, d);
  }

  public void setObjetoPasantia(String string)
  {
    jdoSetobjetoPasantia(this, string);
  }

  public void setProrroga(String string)
  {
    jdoSetprorroga(this, string);
  }

  public void setResultado(String string)
  {
    jdoSetresultado(this, string);
  }

  public void setTareasPasantia(String string)
  {
    jdoSettareasPasantia(this, string);
  }

  public void setTelefTutorAcad(String string)
  {
    jdoSettelefTutorAcad(this, string);
  }

  public void setTelefTutorEmp(String string)
  {
    jdoSettelefTutorEmp(this, string);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setTutorAcademico(String string)
  {
    jdoSettutorAcademico(this, string);
  }

  public void setTutorEmpresarial(String string)
  {
    jdoSettutorEmpresarial(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 23;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.Pasantia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Pasantia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Pasantia localPasantia = new Pasantia();
    localPasantia.jdoFlags = 1;
    localPasantia.jdoStateManager = paramStateManager;
    return localPasantia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Pasantia localPasantia = new Pasantia();
    localPasantia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPasantia.jdoFlags = 1;
    localPasantia.jdoStateManager = paramStateManager;
    return localPasantia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosPasantia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.carrera);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasPasantia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.entidadEducativa);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatusPasantia);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPasantia);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesesPasantia);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.modalidad);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoMensual);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoSemanal);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.objetoPasantia);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.prorroga);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.resultado);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tareasPasantia);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefTutorAcad);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefTutorEmp);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tutorAcademico);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tutorEmpresarial);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosPasantia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.carrera = ((Carrera)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasPasantia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entidadEducativa = ((EntidadEducativa)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatusPasantia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPasantia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesesPasantia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.modalidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoMensual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoSemanal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.objetoPasantia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.prorroga = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resultado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tareasPasantia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefTutorAcad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefTutorEmp = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tutorAcademico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tutorEmpresarial = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Pasantia paramPasantia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.aniosPasantia = paramPasantia.aniosPasantia;
      return;
    case 1:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.carrera = paramPasantia.carrera;
      return;
    case 2:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramPasantia.dependencia;
      return;
    case 3:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.diasPasantia = paramPasantia.diasPasantia;
      return;
    case 4:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.entidadEducativa = paramPasantia.entidadEducativa;
      return;
    case 5:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.estatusPasantia = paramPasantia.estatusPasantia;
      return;
    case 6:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramPasantia.fechaFin;
      return;
    case 7:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramPasantia.fechaInicio;
      return;
    case 8:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramPasantia.fechaRegistro;
      return;
    case 9:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.idPasantia = paramPasantia.idPasantia;
      return;
    case 10:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.mesesPasantia = paramPasantia.mesesPasantia;
      return;
    case 11:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.modalidad = paramPasantia.modalidad;
      return;
    case 12:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.montoMensual = paramPasantia.montoMensual;
      return;
    case 13:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.montoSemanal = paramPasantia.montoSemanal;
      return;
    case 14:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.objetoPasantia = paramPasantia.objetoPasantia;
      return;
    case 15:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.prorroga = paramPasantia.prorroga;
      return;
    case 16:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.resultado = paramPasantia.resultado;
      return;
    case 17:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.tareasPasantia = paramPasantia.tareasPasantia;
      return;
    case 18:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.telefTutorAcad = paramPasantia.telefTutorAcad;
      return;
    case 19:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.telefTutorEmp = paramPasantia.telefTutorEmp;
      return;
    case 20:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramPasantia.trabajador;
      return;
    case 21:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.tutorAcademico = paramPasantia.tutorAcademico;
      return;
    case 22:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.tutorEmpresarial = paramPasantia.tutorEmpresarial;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Pasantia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Pasantia localPasantia = (Pasantia)paramObject;
    if (localPasantia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPasantia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PasantiaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PasantiaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PasantiaPK))
      throw new IllegalArgumentException("arg1");
    PasantiaPK localPasantiaPK = (PasantiaPK)paramObject;
    localPasantiaPK.idPasantia = this.idPasantia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PasantiaPK))
      throw new IllegalArgumentException("arg1");
    PasantiaPK localPasantiaPK = (PasantiaPK)paramObject;
    this.idPasantia = localPasantiaPK.idPasantia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PasantiaPK))
      throw new IllegalArgumentException("arg2");
    PasantiaPK localPasantiaPK = (PasantiaPK)paramObject;
    localPasantiaPK.idPasantia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 9);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PasantiaPK))
      throw new IllegalArgumentException("arg2");
    PasantiaPK localPasantiaPK = (PasantiaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 9, localPasantiaPK.idPasantia);
  }

  private static final int jdoGetaniosPasantia(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.aniosPasantia;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.aniosPasantia;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 0))
      return paramPasantia.aniosPasantia;
    return localStateManager.getIntField(paramPasantia, jdoInheritedFieldCount + 0, paramPasantia.aniosPasantia);
  }

  private static final void jdoSetaniosPasantia(Pasantia paramPasantia, int paramInt)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.aniosPasantia = paramInt;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.aniosPasantia = paramInt;
      return;
    }
    localStateManager.setIntField(paramPasantia, jdoInheritedFieldCount + 0, paramPasantia.aniosPasantia, paramInt);
  }

  private static final Carrera jdoGetcarrera(Pasantia paramPasantia)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.carrera;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 1))
      return paramPasantia.carrera;
    return (Carrera)localStateManager.getObjectField(paramPasantia, jdoInheritedFieldCount + 1, paramPasantia.carrera);
  }

  private static final void jdoSetcarrera(Pasantia paramPasantia, Carrera paramCarrera)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.carrera = paramCarrera;
      return;
    }
    localStateManager.setObjectField(paramPasantia, jdoInheritedFieldCount + 1, paramPasantia.carrera, paramCarrera);
  }

  private static final Dependencia jdoGetdependencia(Pasantia paramPasantia)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.dependencia;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 2))
      return paramPasantia.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramPasantia, jdoInheritedFieldCount + 2, paramPasantia.dependencia);
  }

  private static final void jdoSetdependencia(Pasantia paramPasantia, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramPasantia, jdoInheritedFieldCount + 2, paramPasantia.dependencia, paramDependencia);
  }

  private static final int jdoGetdiasPasantia(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.diasPasantia;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.diasPasantia;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 3))
      return paramPasantia.diasPasantia;
    return localStateManager.getIntField(paramPasantia, jdoInheritedFieldCount + 3, paramPasantia.diasPasantia);
  }

  private static final void jdoSetdiasPasantia(Pasantia paramPasantia, int paramInt)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.diasPasantia = paramInt;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.diasPasantia = paramInt;
      return;
    }
    localStateManager.setIntField(paramPasantia, jdoInheritedFieldCount + 3, paramPasantia.diasPasantia, paramInt);
  }

  private static final EntidadEducativa jdoGetentidadEducativa(Pasantia paramPasantia)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.entidadEducativa;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 4))
      return paramPasantia.entidadEducativa;
    return (EntidadEducativa)localStateManager.getObjectField(paramPasantia, jdoInheritedFieldCount + 4, paramPasantia.entidadEducativa);
  }

  private static final void jdoSetentidadEducativa(Pasantia paramPasantia, EntidadEducativa paramEntidadEducativa)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.entidadEducativa = paramEntidadEducativa;
      return;
    }
    localStateManager.setObjectField(paramPasantia, jdoInheritedFieldCount + 4, paramPasantia.entidadEducativa, paramEntidadEducativa);
  }

  private static final String jdoGetestatusPasantia(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.estatusPasantia;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.estatusPasantia;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 5))
      return paramPasantia.estatusPasantia;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 5, paramPasantia.estatusPasantia);
  }

  private static final void jdoSetestatusPasantia(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.estatusPasantia = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.estatusPasantia = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 5, paramPasantia.estatusPasantia, paramString);
  }

  private static final Date jdoGetfechaFin(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.fechaFin;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.fechaFin;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 6))
      return paramPasantia.fechaFin;
    return (Date)localStateManager.getObjectField(paramPasantia, jdoInheritedFieldCount + 6, paramPasantia.fechaFin);
  }

  private static final void jdoSetfechaFin(Pasantia paramPasantia, Date paramDate)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPasantia, jdoInheritedFieldCount + 6, paramPasantia.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.fechaInicio;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.fechaInicio;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 7))
      return paramPasantia.fechaInicio;
    return (Date)localStateManager.getObjectField(paramPasantia, jdoInheritedFieldCount + 7, paramPasantia.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Pasantia paramPasantia, Date paramDate)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPasantia, jdoInheritedFieldCount + 7, paramPasantia.fechaInicio, paramDate);
  }

  private static final Date jdoGetfechaRegistro(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.fechaRegistro;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.fechaRegistro;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 8))
      return paramPasantia.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramPasantia, jdoInheritedFieldCount + 8, paramPasantia.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(Pasantia paramPasantia, Date paramDate)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPasantia, jdoInheritedFieldCount + 8, paramPasantia.fechaRegistro, paramDate);
  }

  private static final long jdoGetidPasantia(Pasantia paramPasantia)
  {
    return paramPasantia.idPasantia;
  }

  private static final void jdoSetidPasantia(Pasantia paramPasantia, long paramLong)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.idPasantia = paramLong;
      return;
    }
    localStateManager.setLongField(paramPasantia, jdoInheritedFieldCount + 9, paramPasantia.idPasantia, paramLong);
  }

  private static final int jdoGetmesesPasantia(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.mesesPasantia;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.mesesPasantia;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 10))
      return paramPasantia.mesesPasantia;
    return localStateManager.getIntField(paramPasantia, jdoInheritedFieldCount + 10, paramPasantia.mesesPasantia);
  }

  private static final void jdoSetmesesPasantia(Pasantia paramPasantia, int paramInt)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.mesesPasantia = paramInt;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.mesesPasantia = paramInt;
      return;
    }
    localStateManager.setIntField(paramPasantia, jdoInheritedFieldCount + 10, paramPasantia.mesesPasantia, paramInt);
  }

  private static final String jdoGetmodalidad(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.modalidad;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.modalidad;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 11))
      return paramPasantia.modalidad;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 11, paramPasantia.modalidad);
  }

  private static final void jdoSetmodalidad(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.modalidad = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.modalidad = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 11, paramPasantia.modalidad, paramString);
  }

  private static final double jdoGetmontoMensual(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.montoMensual;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.montoMensual;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 12))
      return paramPasantia.montoMensual;
    return localStateManager.getDoubleField(paramPasantia, jdoInheritedFieldCount + 12, paramPasantia.montoMensual);
  }

  private static final void jdoSetmontoMensual(Pasantia paramPasantia, double paramDouble)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.montoMensual = paramDouble;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.montoMensual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPasantia, jdoInheritedFieldCount + 12, paramPasantia.montoMensual, paramDouble);
  }

  private static final double jdoGetmontoSemanal(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.montoSemanal;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.montoSemanal;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 13))
      return paramPasantia.montoSemanal;
    return localStateManager.getDoubleField(paramPasantia, jdoInheritedFieldCount + 13, paramPasantia.montoSemanal);
  }

  private static final void jdoSetmontoSemanal(Pasantia paramPasantia, double paramDouble)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.montoSemanal = paramDouble;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.montoSemanal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPasantia, jdoInheritedFieldCount + 13, paramPasantia.montoSemanal, paramDouble);
  }

  private static final String jdoGetobjetoPasantia(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.objetoPasantia;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.objetoPasantia;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 14))
      return paramPasantia.objetoPasantia;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 14, paramPasantia.objetoPasantia);
  }

  private static final void jdoSetobjetoPasantia(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.objetoPasantia = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.objetoPasantia = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 14, paramPasantia.objetoPasantia, paramString);
  }

  private static final String jdoGetprorroga(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.prorroga;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.prorroga;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 15))
      return paramPasantia.prorroga;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 15, paramPasantia.prorroga);
  }

  private static final void jdoSetprorroga(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.prorroga = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.prorroga = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 15, paramPasantia.prorroga, paramString);
  }

  private static final String jdoGetresultado(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.resultado;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.resultado;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 16))
      return paramPasantia.resultado;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 16, paramPasantia.resultado);
  }

  private static final void jdoSetresultado(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.resultado = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.resultado = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 16, paramPasantia.resultado, paramString);
  }

  private static final String jdoGettareasPasantia(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.tareasPasantia;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.tareasPasantia;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 17))
      return paramPasantia.tareasPasantia;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 17, paramPasantia.tareasPasantia);
  }

  private static final void jdoSettareasPasantia(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.tareasPasantia = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.tareasPasantia = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 17, paramPasantia.tareasPasantia, paramString);
  }

  private static final String jdoGettelefTutorAcad(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.telefTutorAcad;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.telefTutorAcad;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 18))
      return paramPasantia.telefTutorAcad;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 18, paramPasantia.telefTutorAcad);
  }

  private static final void jdoSettelefTutorAcad(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.telefTutorAcad = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.telefTutorAcad = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 18, paramPasantia.telefTutorAcad, paramString);
  }

  private static final String jdoGettelefTutorEmp(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.telefTutorEmp;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.telefTutorEmp;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 19))
      return paramPasantia.telefTutorEmp;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 19, paramPasantia.telefTutorEmp);
  }

  private static final void jdoSettelefTutorEmp(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.telefTutorEmp = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.telefTutorEmp = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 19, paramPasantia.telefTutorEmp, paramString);
  }

  private static final Trabajador jdoGettrabajador(Pasantia paramPasantia)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.trabajador;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 20))
      return paramPasantia.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramPasantia, jdoInheritedFieldCount + 20, paramPasantia.trabajador);
  }

  private static final void jdoSettrabajador(Pasantia paramPasantia, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramPasantia, jdoInheritedFieldCount + 20, paramPasantia.trabajador, paramTrabajador);
  }

  private static final String jdoGettutorAcademico(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.tutorAcademico;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.tutorAcademico;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 21))
      return paramPasantia.tutorAcademico;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 21, paramPasantia.tutorAcademico);
  }

  private static final void jdoSettutorAcademico(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.tutorAcademico = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.tutorAcademico = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 21, paramPasantia.tutorAcademico, paramString);
  }

  private static final String jdoGettutorEmpresarial(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.tutorEmpresarial;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.tutorEmpresarial;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 22))
      return paramPasantia.tutorEmpresarial;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 22, paramPasantia.tutorEmpresarial);
  }

  private static final void jdoSettutorEmpresarial(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.tutorEmpresarial = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.tutorEmpresarial = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 22, paramPasantia.tutorEmpresarial, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}