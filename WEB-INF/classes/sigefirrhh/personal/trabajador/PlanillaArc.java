package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PlanillaArc
  implements Serializable, PersistenceCapable
{
  private long idPlanillaArc;
  private int anio;
  private double devengadoEnero;
  private double porcentajeEnero;
  private double retencionEnero;
  private double devengadoFebrero;
  private double porcentajeFebrero;
  private double retencionFebrero;
  private double devengadoMarzo;
  private double porcentajeMarzo;
  private double retencionMarzo;
  private double devengadoAbril;
  private double porcentajeAbril;
  private double retencionAbril;
  private double devengadoMayo;
  private double porcentajeMayo;
  private double retencionMayo;
  private double devengadoJunio;
  private double porcentajeJunio;
  private double retencionJunio;
  private double devengadoJulio;
  private double porcentajeJulio;
  private double retencionJulio;
  private double devengadoAgosto;
  private double porcentajeAgosto;
  private double retencionAgosto;
  private double devengadoSeptiembre;
  private double porcentajeSeptiembre;
  private double retencionSeptiembre;
  private double devengadoOctubre;
  private double porcentajeOctubre;
  private double retencionOctubre;
  private double devengadoNoviembre;
  private double porcentajeNoviembre;
  private double retencionNoviembre;
  private double devengadoDiciembre;
  private double porcentajeDiciembre;
  private double retencionDiciembre;
  private double acumuladoSso;
  private double acumuladoSpf;
  private double acumuladoLph;
  private double acumuladoCaja;
  private double acumuladoHcm;
  private double acumuladoOtros;
  private Trabajador trabajador;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "acumuladoCaja", "acumuladoHcm", "acumuladoLph", "acumuladoOtros", "acumuladoSpf", "acumuladoSso", "anio", "devengadoAbril", "devengadoAgosto", "devengadoDiciembre", "devengadoEnero", "devengadoFebrero", "devengadoJulio", "devengadoJunio", "devengadoMarzo", "devengadoMayo", "devengadoNoviembre", "devengadoOctubre", "devengadoSeptiembre", "idPlanillaArc", "idSitp", "porcentajeAbril", "porcentajeAgosto", "porcentajeDiciembre", "porcentajeEnero", "porcentajeFebrero", "porcentajeJulio", "porcentajeJunio", "porcentajeMarzo", "porcentajeMayo", "porcentajeNoviembre", "porcentajeOctubre", "porcentajeSeptiembre", "retencionAbril", "retencionAgosto", "retencionDiciembre", "retencionEnero", "retencionFebrero", "retencionJulio", "retencionJunio", "retencionMarzo", "retencionMayo", "retencionNoviembre", "retencionOctubre", "retencionSeptiembre", "tiempoSitp", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return "Año: " + jdoGetanio(this);
  }

  public double getAcumuladoCaja()
  {
    return jdoGetacumuladoCaja(this);
  }

  public double getAcumuladoLph()
  {
    return jdoGetacumuladoLph(this);
  }

  public double getAcumuladoOtros()
  {
    return jdoGetacumuladoOtros(this);
  }

  public double getAcumuladoSpf()
  {
    return jdoGetacumuladoSpf(this);
  }

  public double getAcumuladoSso()
  {
    return jdoGetacumuladoSso(this);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public double getDevengadoAbril()
  {
    return jdoGetdevengadoAbril(this);
  }

  public double getDevengadoAgosto()
  {
    return jdoGetdevengadoAgosto(this);
  }

  public double getDevengadoDiciembre()
  {
    return jdoGetdevengadoDiciembre(this);
  }

  public double getDevengadoEnero()
  {
    return jdoGetdevengadoEnero(this);
  }

  public double getDevengadoFebrero()
  {
    return jdoGetdevengadoFebrero(this);
  }

  public double getDevengadoJulio()
  {
    return jdoGetdevengadoJulio(this);
  }

  public double getDevengadoJunio()
  {
    return jdoGetdevengadoJunio(this);
  }

  public double getDevengadoMarzo()
  {
    return jdoGetdevengadoMarzo(this);
  }

  public double getDevengadoMayo()
  {
    return jdoGetdevengadoMayo(this);
  }

  public double getDevengadoNoviembre()
  {
    return jdoGetdevengadoNoviembre(this);
  }

  public double getDevengadoOctubre()
  {
    return jdoGetdevengadoOctubre(this);
  }

  public double getDevengadoSeptiembre()
  {
    return jdoGetdevengadoSeptiembre(this);
  }

  public double getPorcentajeAbril()
  {
    return jdoGetporcentajeAbril(this);
  }

  public double getPorcentajeAgosto()
  {
    return jdoGetporcentajeAgosto(this);
  }

  public double getPorcentajeDiciembre()
  {
    return jdoGetporcentajeDiciembre(this);
  }

  public double getPorcentajeEnero()
  {
    return jdoGetporcentajeEnero(this);
  }

  public double getPorcentajeFebrero()
  {
    return jdoGetporcentajeFebrero(this);
  }

  public double getPorcentajeJulio()
  {
    return jdoGetporcentajeJulio(this);
  }

  public double getPorcentajeJunio()
  {
    return jdoGetporcentajeJunio(this);
  }

  public double getPorcentajeMarzo()
  {
    return jdoGetporcentajeMarzo(this);
  }

  public double getPorcentajeMayo()
  {
    return jdoGetporcentajeMayo(this);
  }

  public double getPorcentajeNoviembre()
  {
    return jdoGetporcentajeNoviembre(this);
  }

  public double getPorcentajeOctubre()
  {
    return jdoGetporcentajeOctubre(this);
  }

  public double getPorcentajeSeptiembre()
  {
    return jdoGetporcentajeSeptiembre(this);
  }

  public double getRetencionAbril()
  {
    return jdoGetretencionAbril(this);
  }

  public double getRetencionAgosto()
  {
    return jdoGetretencionAgosto(this);
  }

  public double getRetencionDiciembre()
  {
    return jdoGetretencionDiciembre(this);
  }

  public double getRetencionEnero()
  {
    return jdoGetretencionEnero(this);
  }

  public double getRetencionFebrero()
  {
    return jdoGetretencionFebrero(this);
  }

  public double getRetencionJulio()
  {
    return jdoGetretencionJulio(this);
  }

  public double getRetencionJunio()
  {
    return jdoGetretencionJunio(this);
  }

  public double getRetencionMarzo()
  {
    return jdoGetretencionMarzo(this);
  }

  public double getRetencionMayo()
  {
    return jdoGetretencionMayo(this);
  }

  public double getRetencionNoviembre()
  {
    return jdoGetretencionNoviembre(this);
  }

  public double getRetencionOctubre()
  {
    return jdoGetretencionOctubre(this);
  }

  public double getRetencionSeptiembre()
  {
    return jdoGetretencionSeptiembre(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAcumuladoCaja(double d)
  {
    jdoSetacumuladoCaja(this, d);
  }

  public void setAcumuladoLph(double d)
  {
    jdoSetacumuladoLph(this, d);
  }

  public void setAcumuladoOtros(double d)
  {
    jdoSetacumuladoOtros(this, d);
  }

  public void setAcumuladoSpf(double d)
  {
    jdoSetacumuladoSpf(this, d);
  }

  public void setAcumuladoSso(double d)
  {
    jdoSetacumuladoSso(this, d);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setDevengadoAbril(double d)
  {
    jdoSetdevengadoAbril(this, d);
  }

  public void setDevengadoAgosto(double d)
  {
    jdoSetdevengadoAgosto(this, d);
  }

  public void setDevengadoDiciembre(double d)
  {
    jdoSetdevengadoDiciembre(this, d);
  }

  public void setDevengadoEnero(double d)
  {
    jdoSetdevengadoEnero(this, d);
  }

  public void setDevengadoFebrero(double d)
  {
    jdoSetdevengadoFebrero(this, d);
  }

  public void setDevengadoJulio(double d)
  {
    jdoSetdevengadoJulio(this, d);
  }

  public void setDevengadoJunio(double d)
  {
    jdoSetdevengadoJunio(this, d);
  }

  public void setDevengadoMarzo(double d)
  {
    jdoSetdevengadoMarzo(this, d);
  }

  public void setDevengadoMayo(double d)
  {
    jdoSetdevengadoMayo(this, d);
  }

  public void setDevengadoNoviembre(double d)
  {
    jdoSetdevengadoNoviembre(this, d);
  }

  public void setDevengadoOctubre(double d)
  {
    jdoSetdevengadoOctubre(this, d);
  }

  public void setDevengadoSeptiembre(double d)
  {
    jdoSetdevengadoSeptiembre(this, d);
  }

  public void setPorcentajeAbril(double d)
  {
    jdoSetporcentajeAbril(this, d);
  }

  public void setPorcentajeAgosto(double d)
  {
    jdoSetporcentajeAgosto(this, d);
  }

  public void setPorcentajeDiciembre(double d)
  {
    jdoSetporcentajeDiciembre(this, d);
  }

  public void setPorcentajeEnero(double d)
  {
    jdoSetporcentajeEnero(this, d);
  }

  public void setPorcentajeFebrero(double d)
  {
    jdoSetporcentajeFebrero(this, d);
  }

  public void setPorcentajeJulio(double d)
  {
    jdoSetporcentajeJulio(this, d);
  }

  public void setPorcentajeJunio(double d)
  {
    jdoSetporcentajeJunio(this, d);
  }

  public void setPorcentajeMarzo(double d)
  {
    jdoSetporcentajeMarzo(this, d);
  }

  public void setPorcentajeMayo(double d)
  {
    jdoSetporcentajeMayo(this, d);
  }

  public void setPorcentajeNoviembre(double d)
  {
    jdoSetporcentajeNoviembre(this, d);
  }

  public void setPorcentajeOctubre(double d)
  {
    jdoSetporcentajeOctubre(this, d);
  }

  public void setPorcentajeSeptiembre(double d)
  {
    jdoSetporcentajeSeptiembre(this, d);
  }

  public void setRetencionAbril(double d)
  {
    jdoSetretencionAbril(this, d);
  }

  public void setRetencionAgosto(double d)
  {
    jdoSetretencionAgosto(this, d);
  }

  public void setRetencionDiciembre(double d)
  {
    jdoSetretencionDiciembre(this, d);
  }

  public void setRetencionEnero(double d)
  {
    jdoSetretencionEnero(this, d);
  }

  public void setRetencionFebrero(double d)
  {
    jdoSetretencionFebrero(this, d);
  }

  public void setRetencionJulio(double d)
  {
    jdoSetretencionJulio(this, d);
  }

  public void setRetencionJunio(double d)
  {
    jdoSetretencionJunio(this, d);
  }

  public void setRetencionMarzo(double d)
  {
    jdoSetretencionMarzo(this, d);
  }

  public void setRetencionMayo(double d)
  {
    jdoSetretencionMayo(this, d);
  }

  public void setRetencionNoviembre(double d)
  {
    jdoSetretencionNoviembre(this, d);
  }

  public void setRetencionOctubre(double d)
  {
    jdoSetretencionOctubre(this, d);
  }

  public void setRetencionSeptiembre(double d)
  {
    jdoSetretencionSeptiembre(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public long getIdPlanillaArc()
  {
    return jdoGetidPlanillaArc(this);
  }

  public void setIdPlanillaArc(long l)
  {
    jdoSetidPlanillaArc(this, l);
  }

  public double getAcumuladoHcm()
  {
    return jdoGetacumuladoHcm(this);
  }

  public void setAcumuladoHcm(double d)
  {
    jdoSetacumuladoHcm(this, d);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 47;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.PlanillaArc"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PlanillaArc());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PlanillaArc localPlanillaArc = new PlanillaArc();
    localPlanillaArc.jdoFlags = 1;
    localPlanillaArc.jdoStateManager = paramStateManager;
    return localPlanillaArc;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PlanillaArc localPlanillaArc = new PlanillaArc();
    localPlanillaArc.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPlanillaArc.jdoFlags = 1;
    localPlanillaArc.jdoStateManager = paramStateManager;
    return localPlanillaArc;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.acumuladoCaja);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.acumuladoHcm);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.acumuladoLph);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.acumuladoOtros);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.acumuladoSpf);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.acumuladoSso);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoAbril);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoAgosto);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoDiciembre);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoEnero);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoFebrero);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoJulio);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoJunio);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoMarzo);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoMayo);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoNoviembre);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoOctubre);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.devengadoSeptiembre);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPlanillaArc);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeAbril);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeAgosto);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeDiciembre);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeEnero);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeFebrero);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeJulio);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeJunio);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeMarzo);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeMayo);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeNoviembre);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeOctubre);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeSeptiembre);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionAbril);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionAgosto);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionDiciembre);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionEnero);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionFebrero);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionJulio);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionJunio);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionMarzo);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionMayo);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionNoviembre);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionOctubre);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.retencionSeptiembre);
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.acumuladoCaja = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.acumuladoHcm = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.acumuladoLph = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.acumuladoOtros = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.acumuladoSpf = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.acumuladoSso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoAbril = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoAgosto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoDiciembre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoEnero = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoFebrero = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoJulio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoJunio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoMarzo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoMayo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoNoviembre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoOctubre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.devengadoSeptiembre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPlanillaArc = localStateManager.replacingLongField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeAbril = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeAgosto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeDiciembre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeEnero = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeFebrero = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeJulio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeJunio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeMarzo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeMayo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeNoviembre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeOctubre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeSeptiembre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionAbril = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionAgosto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionDiciembre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionEnero = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionFebrero = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionJulio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionJunio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionMarzo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionMayo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionNoviembre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionOctubre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.retencionSeptiembre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PlanillaArc paramPlanillaArc, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.acumuladoCaja = paramPlanillaArc.acumuladoCaja;
      return;
    case 1:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.acumuladoHcm = paramPlanillaArc.acumuladoHcm;
      return;
    case 2:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.acumuladoLph = paramPlanillaArc.acumuladoLph;
      return;
    case 3:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.acumuladoOtros = paramPlanillaArc.acumuladoOtros;
      return;
    case 4:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.acumuladoSpf = paramPlanillaArc.acumuladoSpf;
      return;
    case 5:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.acumuladoSso = paramPlanillaArc.acumuladoSso;
      return;
    case 6:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPlanillaArc.anio;
      return;
    case 7:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoAbril = paramPlanillaArc.devengadoAbril;
      return;
    case 8:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoAgosto = paramPlanillaArc.devengadoAgosto;
      return;
    case 9:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoDiciembre = paramPlanillaArc.devengadoDiciembre;
      return;
    case 10:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoEnero = paramPlanillaArc.devengadoEnero;
      return;
    case 11:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoFebrero = paramPlanillaArc.devengadoFebrero;
      return;
    case 12:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoJulio = paramPlanillaArc.devengadoJulio;
      return;
    case 13:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoJunio = paramPlanillaArc.devengadoJunio;
      return;
    case 14:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoMarzo = paramPlanillaArc.devengadoMarzo;
      return;
    case 15:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoMayo = paramPlanillaArc.devengadoMayo;
      return;
    case 16:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoNoviembre = paramPlanillaArc.devengadoNoviembre;
      return;
    case 17:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoOctubre = paramPlanillaArc.devengadoOctubre;
      return;
    case 18:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.devengadoSeptiembre = paramPlanillaArc.devengadoSeptiembre;
      return;
    case 19:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.idPlanillaArc = paramPlanillaArc.idPlanillaArc;
      return;
    case 20:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramPlanillaArc.idSitp;
      return;
    case 21:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeAbril = paramPlanillaArc.porcentajeAbril;
      return;
    case 22:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeAgosto = paramPlanillaArc.porcentajeAgosto;
      return;
    case 23:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeDiciembre = paramPlanillaArc.porcentajeDiciembre;
      return;
    case 24:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeEnero = paramPlanillaArc.porcentajeEnero;
      return;
    case 25:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeFebrero = paramPlanillaArc.porcentajeFebrero;
      return;
    case 26:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeJulio = paramPlanillaArc.porcentajeJulio;
      return;
    case 27:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeJunio = paramPlanillaArc.porcentajeJunio;
      return;
    case 28:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeMarzo = paramPlanillaArc.porcentajeMarzo;
      return;
    case 29:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeMayo = paramPlanillaArc.porcentajeMayo;
      return;
    case 30:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeNoviembre = paramPlanillaArc.porcentajeNoviembre;
      return;
    case 31:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeOctubre = paramPlanillaArc.porcentajeOctubre;
      return;
    case 32:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeSeptiembre = paramPlanillaArc.porcentajeSeptiembre;
      return;
    case 33:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionAbril = paramPlanillaArc.retencionAbril;
      return;
    case 34:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionAgosto = paramPlanillaArc.retencionAgosto;
      return;
    case 35:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionDiciembre = paramPlanillaArc.retencionDiciembre;
      return;
    case 36:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionEnero = paramPlanillaArc.retencionEnero;
      return;
    case 37:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionFebrero = paramPlanillaArc.retencionFebrero;
      return;
    case 38:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionJulio = paramPlanillaArc.retencionJulio;
      return;
    case 39:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionJunio = paramPlanillaArc.retencionJunio;
      return;
    case 40:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionMarzo = paramPlanillaArc.retencionMarzo;
      return;
    case 41:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionMayo = paramPlanillaArc.retencionMayo;
      return;
    case 42:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionNoviembre = paramPlanillaArc.retencionNoviembre;
      return;
    case 43:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionOctubre = paramPlanillaArc.retencionOctubre;
      return;
    case 44:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.retencionSeptiembre = paramPlanillaArc.retencionSeptiembre;
      return;
    case 45:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramPlanillaArc.tiempoSitp;
      return;
    case 46:
      if (paramPlanillaArc == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramPlanillaArc.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PlanillaArc))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PlanillaArc localPlanillaArc = (PlanillaArc)paramObject;
    if (localPlanillaArc.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPlanillaArc, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PlanillaArcPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PlanillaArcPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanillaArcPK))
      throw new IllegalArgumentException("arg1");
    PlanillaArcPK localPlanillaArcPK = (PlanillaArcPK)paramObject;
    localPlanillaArcPK.idPlanillaArc = this.idPlanillaArc;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanillaArcPK))
      throw new IllegalArgumentException("arg1");
    PlanillaArcPK localPlanillaArcPK = (PlanillaArcPK)paramObject;
    this.idPlanillaArc = localPlanillaArcPK.idPlanillaArc;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanillaArcPK))
      throw new IllegalArgumentException("arg2");
    PlanillaArcPK localPlanillaArcPK = (PlanillaArcPK)paramObject;
    localPlanillaArcPK.idPlanillaArc = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 19);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanillaArcPK))
      throw new IllegalArgumentException("arg2");
    PlanillaArcPK localPlanillaArcPK = (PlanillaArcPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 19, localPlanillaArcPK.idPlanillaArc);
  }

  private static final double jdoGetacumuladoCaja(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.acumuladoCaja;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.acumuladoCaja;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 0))
      return paramPlanillaArc.acumuladoCaja;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 0, paramPlanillaArc.acumuladoCaja);
  }

  private static final void jdoSetacumuladoCaja(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.acumuladoCaja = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.acumuladoCaja = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 0, paramPlanillaArc.acumuladoCaja, paramDouble);
  }

  private static final double jdoGetacumuladoHcm(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.acumuladoHcm;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.acumuladoHcm;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 1))
      return paramPlanillaArc.acumuladoHcm;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 1, paramPlanillaArc.acumuladoHcm);
  }

  private static final void jdoSetacumuladoHcm(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.acumuladoHcm = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.acumuladoHcm = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 1, paramPlanillaArc.acumuladoHcm, paramDouble);
  }

  private static final double jdoGetacumuladoLph(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.acumuladoLph;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.acumuladoLph;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 2))
      return paramPlanillaArc.acumuladoLph;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 2, paramPlanillaArc.acumuladoLph);
  }

  private static final void jdoSetacumuladoLph(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.acumuladoLph = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.acumuladoLph = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 2, paramPlanillaArc.acumuladoLph, paramDouble);
  }

  private static final double jdoGetacumuladoOtros(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.acumuladoOtros;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.acumuladoOtros;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 3))
      return paramPlanillaArc.acumuladoOtros;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 3, paramPlanillaArc.acumuladoOtros);
  }

  private static final void jdoSetacumuladoOtros(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.acumuladoOtros = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.acumuladoOtros = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 3, paramPlanillaArc.acumuladoOtros, paramDouble);
  }

  private static final double jdoGetacumuladoSpf(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.acumuladoSpf;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.acumuladoSpf;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 4))
      return paramPlanillaArc.acumuladoSpf;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 4, paramPlanillaArc.acumuladoSpf);
  }

  private static final void jdoSetacumuladoSpf(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.acumuladoSpf = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.acumuladoSpf = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 4, paramPlanillaArc.acumuladoSpf, paramDouble);
  }

  private static final double jdoGetacumuladoSso(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.acumuladoSso;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.acumuladoSso;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 5))
      return paramPlanillaArc.acumuladoSso;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 5, paramPlanillaArc.acumuladoSso);
  }

  private static final void jdoSetacumuladoSso(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.acumuladoSso = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.acumuladoSso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 5, paramPlanillaArc.acumuladoSso, paramDouble);
  }

  private static final int jdoGetanio(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.anio;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.anio;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 6))
      return paramPlanillaArc.anio;
    return localStateManager.getIntField(paramPlanillaArc, jdoInheritedFieldCount + 6, paramPlanillaArc.anio);
  }

  private static final void jdoSetanio(PlanillaArc paramPlanillaArc, int paramInt)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanillaArc, jdoInheritedFieldCount + 6, paramPlanillaArc.anio, paramInt);
  }

  private static final double jdoGetdevengadoAbril(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoAbril;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoAbril;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 7))
      return paramPlanillaArc.devengadoAbril;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 7, paramPlanillaArc.devengadoAbril);
  }

  private static final void jdoSetdevengadoAbril(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoAbril = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoAbril = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 7, paramPlanillaArc.devengadoAbril, paramDouble);
  }

  private static final double jdoGetdevengadoAgosto(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoAgosto;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoAgosto;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 8))
      return paramPlanillaArc.devengadoAgosto;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 8, paramPlanillaArc.devengadoAgosto);
  }

  private static final void jdoSetdevengadoAgosto(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoAgosto = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoAgosto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 8, paramPlanillaArc.devengadoAgosto, paramDouble);
  }

  private static final double jdoGetdevengadoDiciembre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoDiciembre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoDiciembre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 9))
      return paramPlanillaArc.devengadoDiciembre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 9, paramPlanillaArc.devengadoDiciembre);
  }

  private static final void jdoSetdevengadoDiciembre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoDiciembre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoDiciembre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 9, paramPlanillaArc.devengadoDiciembre, paramDouble);
  }

  private static final double jdoGetdevengadoEnero(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoEnero;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoEnero;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 10))
      return paramPlanillaArc.devengadoEnero;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 10, paramPlanillaArc.devengadoEnero);
  }

  private static final void jdoSetdevengadoEnero(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoEnero = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoEnero = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 10, paramPlanillaArc.devengadoEnero, paramDouble);
  }

  private static final double jdoGetdevengadoFebrero(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoFebrero;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoFebrero;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 11))
      return paramPlanillaArc.devengadoFebrero;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 11, paramPlanillaArc.devengadoFebrero);
  }

  private static final void jdoSetdevengadoFebrero(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoFebrero = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoFebrero = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 11, paramPlanillaArc.devengadoFebrero, paramDouble);
  }

  private static final double jdoGetdevengadoJulio(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoJulio;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoJulio;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 12))
      return paramPlanillaArc.devengadoJulio;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 12, paramPlanillaArc.devengadoJulio);
  }

  private static final void jdoSetdevengadoJulio(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoJulio = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoJulio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 12, paramPlanillaArc.devengadoJulio, paramDouble);
  }

  private static final double jdoGetdevengadoJunio(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoJunio;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoJunio;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 13))
      return paramPlanillaArc.devengadoJunio;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 13, paramPlanillaArc.devengadoJunio);
  }

  private static final void jdoSetdevengadoJunio(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoJunio = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoJunio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 13, paramPlanillaArc.devengadoJunio, paramDouble);
  }

  private static final double jdoGetdevengadoMarzo(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoMarzo;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoMarzo;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 14))
      return paramPlanillaArc.devengadoMarzo;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 14, paramPlanillaArc.devengadoMarzo);
  }

  private static final void jdoSetdevengadoMarzo(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoMarzo = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoMarzo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 14, paramPlanillaArc.devengadoMarzo, paramDouble);
  }

  private static final double jdoGetdevengadoMayo(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoMayo;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoMayo;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 15))
      return paramPlanillaArc.devengadoMayo;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 15, paramPlanillaArc.devengadoMayo);
  }

  private static final void jdoSetdevengadoMayo(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoMayo = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoMayo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 15, paramPlanillaArc.devengadoMayo, paramDouble);
  }

  private static final double jdoGetdevengadoNoviembre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoNoviembre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoNoviembre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 16))
      return paramPlanillaArc.devengadoNoviembre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 16, paramPlanillaArc.devengadoNoviembre);
  }

  private static final void jdoSetdevengadoNoviembre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoNoviembre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoNoviembre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 16, paramPlanillaArc.devengadoNoviembre, paramDouble);
  }

  private static final double jdoGetdevengadoOctubre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoOctubre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoOctubre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 17))
      return paramPlanillaArc.devengadoOctubre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 17, paramPlanillaArc.devengadoOctubre);
  }

  private static final void jdoSetdevengadoOctubre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoOctubre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoOctubre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 17, paramPlanillaArc.devengadoOctubre, paramDouble);
  }

  private static final double jdoGetdevengadoSeptiembre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.devengadoSeptiembre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.devengadoSeptiembre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 18))
      return paramPlanillaArc.devengadoSeptiembre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 18, paramPlanillaArc.devengadoSeptiembre);
  }

  private static final void jdoSetdevengadoSeptiembre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.devengadoSeptiembre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.devengadoSeptiembre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 18, paramPlanillaArc.devengadoSeptiembre, paramDouble);
  }

  private static final long jdoGetidPlanillaArc(PlanillaArc paramPlanillaArc)
  {
    return paramPlanillaArc.idPlanillaArc;
  }

  private static final void jdoSetidPlanillaArc(PlanillaArc paramPlanillaArc, long paramLong)
  {
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.idPlanillaArc = paramLong;
      return;
    }
    localStateManager.setLongField(paramPlanillaArc, jdoInheritedFieldCount + 19, paramPlanillaArc.idPlanillaArc, paramLong);
  }

  private static final int jdoGetidSitp(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.idSitp;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.idSitp;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 20))
      return paramPlanillaArc.idSitp;
    return localStateManager.getIntField(paramPlanillaArc, jdoInheritedFieldCount + 20, paramPlanillaArc.idSitp);
  }

  private static final void jdoSetidSitp(PlanillaArc paramPlanillaArc, int paramInt)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanillaArc, jdoInheritedFieldCount + 20, paramPlanillaArc.idSitp, paramInt);
  }

  private static final double jdoGetporcentajeAbril(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeAbril;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeAbril;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 21))
      return paramPlanillaArc.porcentajeAbril;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 21, paramPlanillaArc.porcentajeAbril);
  }

  private static final void jdoSetporcentajeAbril(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeAbril = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeAbril = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 21, paramPlanillaArc.porcentajeAbril, paramDouble);
  }

  private static final double jdoGetporcentajeAgosto(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeAgosto;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeAgosto;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 22))
      return paramPlanillaArc.porcentajeAgosto;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 22, paramPlanillaArc.porcentajeAgosto);
  }

  private static final void jdoSetporcentajeAgosto(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeAgosto = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeAgosto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 22, paramPlanillaArc.porcentajeAgosto, paramDouble);
  }

  private static final double jdoGetporcentajeDiciembre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeDiciembre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeDiciembre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 23))
      return paramPlanillaArc.porcentajeDiciembre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 23, paramPlanillaArc.porcentajeDiciembre);
  }

  private static final void jdoSetporcentajeDiciembre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeDiciembre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeDiciembre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 23, paramPlanillaArc.porcentajeDiciembre, paramDouble);
  }

  private static final double jdoGetporcentajeEnero(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeEnero;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeEnero;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 24))
      return paramPlanillaArc.porcentajeEnero;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 24, paramPlanillaArc.porcentajeEnero);
  }

  private static final void jdoSetporcentajeEnero(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeEnero = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeEnero = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 24, paramPlanillaArc.porcentajeEnero, paramDouble);
  }

  private static final double jdoGetporcentajeFebrero(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeFebrero;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeFebrero;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 25))
      return paramPlanillaArc.porcentajeFebrero;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 25, paramPlanillaArc.porcentajeFebrero);
  }

  private static final void jdoSetporcentajeFebrero(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeFebrero = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeFebrero = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 25, paramPlanillaArc.porcentajeFebrero, paramDouble);
  }

  private static final double jdoGetporcentajeJulio(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeJulio;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeJulio;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 26))
      return paramPlanillaArc.porcentajeJulio;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 26, paramPlanillaArc.porcentajeJulio);
  }

  private static final void jdoSetporcentajeJulio(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeJulio = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeJulio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 26, paramPlanillaArc.porcentajeJulio, paramDouble);
  }

  private static final double jdoGetporcentajeJunio(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeJunio;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeJunio;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 27))
      return paramPlanillaArc.porcentajeJunio;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 27, paramPlanillaArc.porcentajeJunio);
  }

  private static final void jdoSetporcentajeJunio(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeJunio = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeJunio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 27, paramPlanillaArc.porcentajeJunio, paramDouble);
  }

  private static final double jdoGetporcentajeMarzo(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeMarzo;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeMarzo;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 28))
      return paramPlanillaArc.porcentajeMarzo;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 28, paramPlanillaArc.porcentajeMarzo);
  }

  private static final void jdoSetporcentajeMarzo(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeMarzo = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeMarzo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 28, paramPlanillaArc.porcentajeMarzo, paramDouble);
  }

  private static final double jdoGetporcentajeMayo(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeMayo;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeMayo;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 29))
      return paramPlanillaArc.porcentajeMayo;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 29, paramPlanillaArc.porcentajeMayo);
  }

  private static final void jdoSetporcentajeMayo(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeMayo = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeMayo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 29, paramPlanillaArc.porcentajeMayo, paramDouble);
  }

  private static final double jdoGetporcentajeNoviembre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeNoviembre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeNoviembre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 30))
      return paramPlanillaArc.porcentajeNoviembre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 30, paramPlanillaArc.porcentajeNoviembre);
  }

  private static final void jdoSetporcentajeNoviembre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeNoviembre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeNoviembre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 30, paramPlanillaArc.porcentajeNoviembre, paramDouble);
  }

  private static final double jdoGetporcentajeOctubre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeOctubre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeOctubre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 31))
      return paramPlanillaArc.porcentajeOctubre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 31, paramPlanillaArc.porcentajeOctubre);
  }

  private static final void jdoSetporcentajeOctubre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeOctubre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeOctubre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 31, paramPlanillaArc.porcentajeOctubre, paramDouble);
  }

  private static final double jdoGetporcentajeSeptiembre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.porcentajeSeptiembre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.porcentajeSeptiembre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 32))
      return paramPlanillaArc.porcentajeSeptiembre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 32, paramPlanillaArc.porcentajeSeptiembre);
  }

  private static final void jdoSetporcentajeSeptiembre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.porcentajeSeptiembre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.porcentajeSeptiembre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 32, paramPlanillaArc.porcentajeSeptiembre, paramDouble);
  }

  private static final double jdoGetretencionAbril(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionAbril;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionAbril;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 33))
      return paramPlanillaArc.retencionAbril;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 33, paramPlanillaArc.retencionAbril);
  }

  private static final void jdoSetretencionAbril(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionAbril = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionAbril = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 33, paramPlanillaArc.retencionAbril, paramDouble);
  }

  private static final double jdoGetretencionAgosto(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionAgosto;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionAgosto;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 34))
      return paramPlanillaArc.retencionAgosto;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 34, paramPlanillaArc.retencionAgosto);
  }

  private static final void jdoSetretencionAgosto(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionAgosto = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionAgosto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 34, paramPlanillaArc.retencionAgosto, paramDouble);
  }

  private static final double jdoGetretencionDiciembre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionDiciembre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionDiciembre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 35))
      return paramPlanillaArc.retencionDiciembre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 35, paramPlanillaArc.retencionDiciembre);
  }

  private static final void jdoSetretencionDiciembre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionDiciembre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionDiciembre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 35, paramPlanillaArc.retencionDiciembre, paramDouble);
  }

  private static final double jdoGetretencionEnero(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionEnero;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionEnero;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 36))
      return paramPlanillaArc.retencionEnero;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 36, paramPlanillaArc.retencionEnero);
  }

  private static final void jdoSetretencionEnero(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionEnero = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionEnero = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 36, paramPlanillaArc.retencionEnero, paramDouble);
  }

  private static final double jdoGetretencionFebrero(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionFebrero;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionFebrero;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 37))
      return paramPlanillaArc.retencionFebrero;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 37, paramPlanillaArc.retencionFebrero);
  }

  private static final void jdoSetretencionFebrero(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionFebrero = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionFebrero = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 37, paramPlanillaArc.retencionFebrero, paramDouble);
  }

  private static final double jdoGetretencionJulio(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionJulio;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionJulio;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 38))
      return paramPlanillaArc.retencionJulio;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 38, paramPlanillaArc.retencionJulio);
  }

  private static final void jdoSetretencionJulio(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionJulio = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionJulio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 38, paramPlanillaArc.retencionJulio, paramDouble);
  }

  private static final double jdoGetretencionJunio(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionJunio;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionJunio;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 39))
      return paramPlanillaArc.retencionJunio;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 39, paramPlanillaArc.retencionJunio);
  }

  private static final void jdoSetretencionJunio(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionJunio = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionJunio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 39, paramPlanillaArc.retencionJunio, paramDouble);
  }

  private static final double jdoGetretencionMarzo(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionMarzo;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionMarzo;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 40))
      return paramPlanillaArc.retencionMarzo;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 40, paramPlanillaArc.retencionMarzo);
  }

  private static final void jdoSetretencionMarzo(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionMarzo = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionMarzo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 40, paramPlanillaArc.retencionMarzo, paramDouble);
  }

  private static final double jdoGetretencionMayo(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionMayo;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionMayo;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 41))
      return paramPlanillaArc.retencionMayo;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 41, paramPlanillaArc.retencionMayo);
  }

  private static final void jdoSetretencionMayo(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionMayo = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionMayo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 41, paramPlanillaArc.retencionMayo, paramDouble);
  }

  private static final double jdoGetretencionNoviembre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionNoviembre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionNoviembre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 42))
      return paramPlanillaArc.retencionNoviembre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 42, paramPlanillaArc.retencionNoviembre);
  }

  private static final void jdoSetretencionNoviembre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionNoviembre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionNoviembre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 42, paramPlanillaArc.retencionNoviembre, paramDouble);
  }

  private static final double jdoGetretencionOctubre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionOctubre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionOctubre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 43))
      return paramPlanillaArc.retencionOctubre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 43, paramPlanillaArc.retencionOctubre);
  }

  private static final void jdoSetretencionOctubre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionOctubre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionOctubre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 43, paramPlanillaArc.retencionOctubre, paramDouble);
  }

  private static final double jdoGetretencionSeptiembre(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.retencionSeptiembre;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.retencionSeptiembre;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 44))
      return paramPlanillaArc.retencionSeptiembre;
    return localStateManager.getDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 44, paramPlanillaArc.retencionSeptiembre);
  }

  private static final void jdoSetretencionSeptiembre(PlanillaArc paramPlanillaArc, double paramDouble)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.retencionSeptiembre = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.retencionSeptiembre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaArc, jdoInheritedFieldCount + 44, paramPlanillaArc.retencionSeptiembre, paramDouble);
  }

  private static final Date jdoGettiempoSitp(PlanillaArc paramPlanillaArc)
  {
    if (paramPlanillaArc.jdoFlags <= 0)
      return paramPlanillaArc.tiempoSitp;
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.tiempoSitp;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 45))
      return paramPlanillaArc.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramPlanillaArc, jdoInheritedFieldCount + 45, paramPlanillaArc.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(PlanillaArc paramPlanillaArc, Date paramDate)
  {
    if (paramPlanillaArc.jdoFlags == 0)
    {
      paramPlanillaArc.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPlanillaArc, jdoInheritedFieldCount + 45, paramPlanillaArc.tiempoSitp, paramDate);
  }

  private static final Trabajador jdoGettrabajador(PlanillaArc paramPlanillaArc)
  {
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaArc.trabajador;
    if (localStateManager.isLoaded(paramPlanillaArc, jdoInheritedFieldCount + 46))
      return paramPlanillaArc.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramPlanillaArc, jdoInheritedFieldCount + 46, paramPlanillaArc.trabajador);
  }

  private static final void jdoSettrabajador(PlanillaArc paramPlanillaArc, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramPlanillaArc.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaArc.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramPlanillaArc, jdoInheritedFieldCount + 46, paramPlanillaArc.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}