package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportPrestamosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportPrestamosForm.class.getName());
  private int reportId;
  private long idTipoPersonal;
  private String selectTipoPersonal;
  private long idConceptoTipoPersonal;
  private String selectConceptoTipoPersonal;
  private String formato = "1";
  private String reportName;
  private Collection listTipoPersonal;
  private Collection listConceptoTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;

  public ReportPrestamosForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "prestamosvigentes";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportPrestamosForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    this.selectTipoPersonal = String.valueOf(this.idTipoPersonal);
    try {
      if (this.idTipoPersonal != 0L)
        this.listConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(this.idTipoPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listConceptoTipoPersonal = null;
    }
  }

  public void changeConcepto(ValueChangeEvent event) {
    this.idConceptoTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    this.selectConceptoTipoPersonal = String.valueOf(this.idConceptoTipoPersonal);
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try {
      this.reportName = "prestamosvigentes";
      if (this.idConceptoTipoPersonal != 0L) {
        this.reportName += "1";
      }
      if (this.formato.equals("2"))
        this.reportName = ("a_" + this.reportName);
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport() {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "prestamosvigentes";
      if (this.idConceptoTipoPersonal != 0L) {
        this.reportName += "1";
      }
      if (this.formato.equals("2")) {
        this.reportName = ("a_" + this.reportName);
      }

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      if (this.idConceptoTipoPersonal != 0L) {
        parameters.put("id_concepto_tipo_personal", new Long(this.idConceptoTipoPersonal));
      }
      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/trabajador");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String getIdTipoPersonal() {
    return String.valueOf(this.idTipoPersonal);
  }
  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }
  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public boolean isShowConcepto() {
    return (this.listConceptoTipoPersonal != null) && (!this.listConceptoTipoPersonal.isEmpty());
  }
  public Collection getListConceptoTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConceptoTipoPersonal, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String selectConceptoTipoPersonal) {
    this.selectConceptoTipoPersonal = selectConceptoTipoPersonal;
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String selectTipoPersonal) {
    this.selectTipoPersonal = selectTipoPersonal;
  }
}