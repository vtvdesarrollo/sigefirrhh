package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonalBeanBusiness;

public class ConceptoFijoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoFijo(ConceptoFijo conceptoFijo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoFijo conceptoFijoNew = 
      (ConceptoFijo)BeanUtils.cloneBean(
      conceptoFijo);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoFijoNew.getConceptoTipoPersonal() != null) {
      conceptoFijoNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoFijoNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (conceptoFijoNew.getFrecuenciaTipoPersonal() != null) {
      conceptoFijoNew.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        conceptoFijoNew.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (conceptoFijoNew.getTrabajador() != null) {
      conceptoFijoNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        conceptoFijoNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(conceptoFijoNew);
  }

  public void updateConceptoFijo(ConceptoFijo conceptoFijo) throws Exception
  {
    ConceptoFijo conceptoFijoModify = 
      findConceptoFijoById(conceptoFijo.getIdConceptoFijo());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoFijo.getConceptoTipoPersonal() != null) {
      conceptoFijo.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoFijo.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (conceptoFijo.getFrecuenciaTipoPersonal() != null) {
      conceptoFijo.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        conceptoFijo.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (conceptoFijo.getTrabajador() != null) {
      conceptoFijo.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        conceptoFijo.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(conceptoFijoModify, conceptoFijo);
  }

  public void deleteConceptoFijo(ConceptoFijo conceptoFijo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoFijo conceptoFijoDelete = 
      findConceptoFijoById(conceptoFijo.getIdConceptoFijo());
    pm.deletePersistent(conceptoFijoDelete);
  }

  public ConceptoFijo findConceptoFijoById(long idConceptoFijo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoFijo == pIdConceptoFijo";
    Query query = pm.newQuery(ConceptoFijo.class, filter);

    query.declareParameters("long pIdConceptoFijo");

    parameters.put("pIdConceptoFijo", new Long(idConceptoFijo));

    Collection colConceptoFijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoFijo.iterator();
    return (ConceptoFijo)iterator.next();
  }

  public Collection findConceptoFijoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoFijoExtent = pm.getExtent(
      ConceptoFijo.class, true);
    Query query = pm.newQuery(conceptoFijoExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(ConceptoFijo.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoFijo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoFijo);

    return colConceptoFijo;
  }
}