package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class GenerarPlanillaArcForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarPlanillaArcForm.class.getName());
  private String selectProceso;
  private String selectTipoPersonal;
  private int mes;
  private int anio;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private TrabajadorNoGenFacade trabajadorNoGenFacade;
  private LoginSession login;
  private boolean show;
  private boolean auxShow;
  private TipoPersonal tipoPersonal;

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.auxShow = false;
    try {
      if (this.idTipoPersonal != 0L) {
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
        this.auxShow = true;
      }

    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public GenerarPlanillaArcForm() {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.trabajadorNoGenFacade = new TrabajadorNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByArc(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.trabajadorNoGenFacade.generarPlanillaArc(this.idTipoPersonal, this.tipoPersonal.getGrupoNomina().getPeriodicidad(), this.anio);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      context.addMessage("success_add", new FacesMessage("Se calculó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
  }

  public boolean isShow() {
    return this.auxShow;
  }

  public String getSelectProceso() {
    return this.selectProceso;
  }

  public void setSelectProceso(String string) {
    this.selectProceso = string;
  }

  public int getAnio()
  {
    return this.anio;
  }

  public int getMes()
  {
    return this.mes;
  }

  public void setAnio(int i)
  {
    this.anio = i;
  }

  public void setMes(int i)
  {
    this.mes = i;
  }
}