package sigefirrhh.personal.trabajador;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;

public class CajaAhorroBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCajaAhorro(CajaAhorro cajaAhorro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CajaAhorro cajaAhorroNew = (CajaAhorro)BeanUtils.cloneBean(cajaAhorro);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (cajaAhorroNew.getConceptoTipoPersonal() != null) {
      cajaAhorroNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        cajaAhorroNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (cajaAhorroNew.getTrabajador() != null) {
      cajaAhorroNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        cajaAhorroNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(cajaAhorroNew);
  }

  public void updateCajaAhorro(CajaAhorro cajaAhorro) throws Exception
  {
    CajaAhorro cajaAhorroModify = 
      findCajaAhorroById(cajaAhorro.getIdCajaAhorro());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (cajaAhorro.getConceptoTipoPersonal() != null) {
      cajaAhorro.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        cajaAhorro.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (cajaAhorro.getTrabajador() != null) {
      cajaAhorro.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        cajaAhorro.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(cajaAhorroModify, cajaAhorro);
  }

  public void deleteCajaAhorro(CajaAhorro cajaAhorro) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CajaAhorro cajaAhorroDelete = 
      findCajaAhorroById(cajaAhorro.getIdCajaAhorro());
    pm.deletePersistent(cajaAhorroDelete);
  }

  public CajaAhorro findCajaAhorroById(long idCajaAhorro) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCajaAhorro == pIdCajaAhorro";
    Query query = pm.newQuery(CajaAhorro.class, filter);

    query.declareParameters("long pIdCajaAhorro");

    parameters.put("pIdCajaAhorro", new Long(idCajaAhorro));

    Collection colCajaAhorro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCajaAhorro.iterator();
    return (CajaAhorro)iterator.next();
  }

  public Collection findCajaAhorroAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent cajaAhorroExtent = pm.getExtent(
      CajaAhorro.class, true);
    Query query = pm.newQuery(cajaAhorroExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(CajaAhorro.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    Collection colCajaAhorro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCajaAhorro);

    return colCajaAhorro;
  }
}