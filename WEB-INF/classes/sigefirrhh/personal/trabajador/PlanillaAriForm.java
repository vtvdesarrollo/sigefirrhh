package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.ParametroAri;
import sigefirrhh.base.definiciones.TarifaAri;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class PlanillaAriForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PlanillaAriForm.class.getName());
  private PlanillaAri planillaAri;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private ParametroAri parametroAri;
  private Collection colTrabajador;
  private String selectTrabajador;
  private Collection findColTipoPersonal;
  private Object stateResultPlanillaAriByTrabajador = null;

  public Collection getFindColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectTrabajador()
  {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    this.planillaAri.setTrabajador(null);
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      if (String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador)) {
        this.planillaAri.setTrabajador(
          trabajador);
        break;
      }
    }
    this.selectTrabajador = valTrabajador;
  }
  public Collection getResult() {
    return this.result;
  }

  public PlanillaAri getPlanillaAri() {
    if (this.planillaAri == null) {
      this.planillaAri = new PlanillaAri();
    }
    return this.planillaAri;
  }

  public PlanillaAriForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    log.error("a.inicio ");

    this.parametroAri = ((ParametroAri)this.definicionesFacade.findAllParametroAri().iterator().next());

    refresh();
  }

  public String calcular()
  {
    log.error("inicio ");

    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if (this.planillaAri.getDesgravamenes() > this.planillaAri.getRemuneraciones()) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El monto Desgravamenes no puede ser mayor al monto Remuneraciones", ""));
        return null;
      }

      double a = this.planillaAri.getRemuneraciones() / this.parametroAri.getUnidadTributaria();
      log.error("a = " + a);
      double b = this.planillaAri.getDesgravamenes() / this.parametroAri.getUnidadTributaria();
      log.error("b = " + b);
      double c = (this.planillaAri.getTotalCargas() + 1) * this.parametroAri.getUnCarga();
      log.error("c = " + c);
      double d = a - b;
      log.error("d = " + d);
      TarifaAri tarifaAri = this.definicionesFacade.findTarifaAriByTarifaAproximada(d);

      log.error("tarifaARI " + tarifaAri);
      double y = d * tarifaAri.getPorcentaje() / 100.0D - tarifaAri.getSustraendo();

      log.error("y = " + y);

      y -= c;

      double x = y * 100.0D / a;
      if (x < 0.0D) {
        x = 0.0D;
      }
      log.error("x = " + x);
      this.planillaAri.setPorcentaje(x);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }
  public Collection getColTrabajador() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        new DefinicionesFacade().findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedula(this.findTrabajadorCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidos(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findPlanillaAriByTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectTrabajador();
      if (!this.adding) {
        this.result = 
          this.trabajadorFacade.findPlanillaAriByTrabajador(
          this.trabajador.getIdTrabajador());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectPlanillaAri()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTrabajador = null;

    long idPlanillaAri = 
      Long.parseLong((String)requestParameterMap.get("idPlanillaAri"));
    try
    {
      this.planillaAri = 
        this.trabajadorFacade.findPlanillaAriById(
        idPlanillaAri);

      if (this.planillaAri.getTrabajador() != null) {
        this.selectTrabajador = 
          String.valueOf(this.planillaAri.getTrabajador().getIdTrabajador());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.planillaAri.setTrabajador(
          this.trabajador);
        Trabajador trabajador = this.trabajadorFacade.findTrabajadorById(this.planillaAri.getTrabajador().getIdTrabajador());
        trabajador.setPorcentajeIslr(this.planillaAri.getPorcentaje());
        this.trabajadorFacade.updateTrabajador(trabajador);

        this.trabajadorFacade.addPlanillaAri(
          this.planillaAri);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.planillaAri, this.trabajador.getPersonal());

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.trabajadorFacade.updatePlanillaAri(
          this.planillaAri);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.planillaAri, this.trabajador.getPersonal());

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e)
    {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.trabajadorFacade.deletePlanillaAri(
        this.planillaAri);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.planillaAri, this.trabajador.getPersonal());

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;

      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedTrabajador = true;

    this.selectTrabajador = null;

    this.planillaAri = new PlanillaAri();

    this.planillaAri.setTrabajador(this.trabajador);
    try
    {
      log.error("parametroari " + this.parametroAri);

      this.planillaAri.setDesgravamenes(this.parametroAri.getUnDesgravamenes() * this.parametroAri.getUnidadTributaria());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.planillaAri.setIdPlanillaAri(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.PlanillaAri"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.planillaAri = new PlanillaAri();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.planillaAri = new PlanillaAri();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedTrabajador));
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedTrabajador);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public LoginSession getLogin() {
    return this.login;
  }
}