package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class ConceptoVariablePK
  implements Serializable
{
  public long idConceptoVariable;

  public ConceptoVariablePK()
  {
  }

  public ConceptoVariablePK(long idConceptoVariable)
  {
    this.idConceptoVariable = idConceptoVariable;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoVariablePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoVariablePK thatPK)
  {
    return 
      this.idConceptoVariable == thatPK.idConceptoVariable;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoVariable)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoVariable);
  }
}