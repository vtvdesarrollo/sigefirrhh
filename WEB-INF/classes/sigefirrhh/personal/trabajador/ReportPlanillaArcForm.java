package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportPlanillaArcForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportPlanillaArcForm.class.getName());
  private int reportId;
  private String formato = "1";
  private long idTipoPersonal;
  private long idRegion;
  private String reportName;
  private String orden;
  private String mostrar;
  private int anio;
  private Collection listTipoPersonal;
  private Collection listRegion;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private EstructuraFacade estructuraFacade;
  private TipoPersonal tipoPersonal;

  public ReportPlanillaArcForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "arcqui";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportPlanillaArcForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listRegion = this.estructuraFacade.findAllRegion();

      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalByArc(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listRegion = new ArrayList();
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      if (this.idRegion != 0L) {
        if (this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"))
          this.reportName = "arcsemreg";
        else {
          this.reportName = "arcquireg";
        }
      }
      else if (this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"))
        this.reportName = "arcsem";
      else
        this.reportName = "arcqui";
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "";

      if (this.idRegion != 0L) {
        if (this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"))
          this.reportName = "arcsemreg";
        else {
          this.reportName = "arcquireg";
        }
      }
      else if (this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"))
        this.reportName = "arcsem";
      else {
        this.reportName = "arcqui";
      }

      FacesContext context = FacesContext.getCurrentInstance();

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      log.error("PARM 1 " + this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      log.error("PARM 2 " + this.idTipoPersonal);
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      log.error("PARM 3 " + this.login.getOrganismo().getNombreAgenteRetencion());
      parameters.put("nombre_agente_retencion", this.login.getOrganismo().getNombreAgenteRetencion());
      log.error("PARM 4 " + this.login.getOrganismo().getDireccion());
      parameters.put("direccion", this.login.getOrganismo().getDireccion());
      log.error("PARM 5 " + this.login.getOrganismo().getCedulaAgenteRetencion());
      parameters.put("cedula_agente_retencion", this.login.getOrganismo().getCedulaAgenteRetencion());
      log.error("PARM 6 " + this.login.getOrganismo().getRif());
      parameters.put("rif", this.login.getOrganismo().getRif());
      log.error("PARM 7 " + this.login.getOrganismo().getRifAgenteRetencion());
      parameters.put("rif_agente_retencion", this.login.getOrganismo().getRifAgenteRetencion());

      log.error("PARM 8 " + this.anio);

      log.error("1 ");
      Date fechaInicio = new Date(this.anio - 1900, 0, 1);
      log.error("2 ");
      Date fechaFin = new Date(this.anio - 1900, 11, 31);
      log.error("3 ");

      if (this.idRegion != 0L) {
        parameters.put("id_region", new Long(this.idRegion));
      }
      parameters.put("anio", new Integer(this.anio));
      parameters.put("fec_ini", fechaInicio);
      parameters.put("fec_fin", fechaFin);

      log.error("id_tp " + this.idTipoPersonal);
      log.error("anio " + this.anio);

      parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/trabajador");

      JasperForWeb report = new JasperForWeb();

      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/trabajador");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if (this.idTipoPersonal != 0L) {
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l)
  {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public Collection getListRegion()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public String getMostrar()
  {
    return this.mostrar;
  }

  public String getOrden()
  {
    return this.orden;
  }

  public void setMostrar(String string)
  {
    this.mostrar = string;
  }

  public void setOrden(String string)
  {
    this.orden = string;
  }

  public long getIdRegion()
  {
    return this.idRegion;
  }

  public void setIdRegion(long l)
  {
    this.idRegion = l;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String string)
  {
    this.formato = string;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
}