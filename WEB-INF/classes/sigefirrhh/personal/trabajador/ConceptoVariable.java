package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;

public class ConceptoVariable
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idConceptoVariable;
  private double unidades;
  private double monto;
  private Date fechaRegistro;
  private String documentoSoporte;
  private String estatus;
  private int mesSobretiempo;
  private int anioSobretiempo;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anioSobretiempo", "conceptoTipoPersonal", "documentoSoporte", "estatus", "fechaRegistro", "frecuenciaTipoPersonal", "idConceptoVariable", "mesSobretiempo", "monto", "trabajador", "unidades" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 26, 21, 21, 21, 26, 24, 21, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.ConceptoVariable"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoVariable());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("A", "ACTIVO");
    LISTA_ESTATUS.put("S", "SUSPENDIDO");
  }

  public ConceptoVariable()
  {
    jdoSetestatus(this, "A");
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmonto(this));

    return jdoGetconceptoTipoPersonal(this).getConcepto().getCodConcepto() + " - " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + 
      a + " - " + 
      jdoGetfrecuenciaTipoPersonal(this).getFrecuenciaPago().getCodFrecuenciaPago() + " - " + jdoGetestatus(this);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal()
  {
    return jdoGetfrecuenciaTipoPersonal(this);
  }

  public long getIdConceptoVariable()
  {
    return jdoGetidConceptoVariable(this);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public double getUnidades()
  {
    return jdoGetunidades(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setFrecuenciaTipoPersonal(FrecuenciaTipoPersonal personal)
  {
    jdoSetfrecuenciaTipoPersonal(this, personal);
  }

  public void setIdConceptoVariable(long l)
  {
    jdoSetidConceptoVariable(this, l);
  }

  public void setMonto(double d)
  {
    jdoSetmonto(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setUnidades(double d)
  {
    jdoSetunidades(this, d);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public int getAnioSobretiempo() {
    return jdoGetanioSobretiempo(this);
  }
  public void setAnioSobretiempo(int anioSobretiempo) {
    jdoSetanioSobretiempo(this, anioSobretiempo);
  }
  public int getMesSobretiempo() {
    return jdoGetmesSobretiempo(this);
  }
  public void setMesSobretiempo(int mesSobretiempo) {
    jdoSetmesSobretiempo(this, mesSobretiempo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoVariable localConceptoVariable = new ConceptoVariable();
    localConceptoVariable.jdoFlags = 1;
    localConceptoVariable.jdoStateManager = paramStateManager;
    return localConceptoVariable;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoVariable localConceptoVariable = new ConceptoVariable();
    localConceptoVariable.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoVariable.jdoFlags = 1;
    localConceptoVariable.jdoStateManager = paramStateManager;
    return localConceptoVariable;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioSobretiempo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaTipoPersonal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoVariable);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesSobretiempo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioSobretiempo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaTipoPersonal = ((FrecuenciaTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoVariable = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesSobretiempo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoVariable paramConceptoVariable, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoVariable == null)
        throw new IllegalArgumentException("arg1");
      this.anioSobretiempo = paramConceptoVariable.anioSobretiempo;
      return;
    case 1:
      if (paramConceptoVariable == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoVariable.conceptoTipoPersonal;
      return;
    case 2:
      if (paramConceptoVariable == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramConceptoVariable.documentoSoporte;
      return;
    case 3:
      if (paramConceptoVariable == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramConceptoVariable.estatus;
      return;
    case 4:
      if (paramConceptoVariable == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramConceptoVariable.fechaRegistro;
      return;
    case 5:
      if (paramConceptoVariable == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaTipoPersonal = paramConceptoVariable.frecuenciaTipoPersonal;
      return;
    case 6:
      if (paramConceptoVariable == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoVariable = paramConceptoVariable.idConceptoVariable;
      return;
    case 7:
      if (paramConceptoVariable == null)
        throw new IllegalArgumentException("arg1");
      this.mesSobretiempo = paramConceptoVariable.mesSobretiempo;
      return;
    case 8:
      if (paramConceptoVariable == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramConceptoVariable.monto;
      return;
    case 9:
      if (paramConceptoVariable == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramConceptoVariable.trabajador;
      return;
    case 10:
      if (paramConceptoVariable == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramConceptoVariable.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoVariable))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoVariable localConceptoVariable = (ConceptoVariable)paramObject;
    if (localConceptoVariable.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoVariable, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoVariablePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoVariablePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoVariablePK))
      throw new IllegalArgumentException("arg1");
    ConceptoVariablePK localConceptoVariablePK = (ConceptoVariablePK)paramObject;
    localConceptoVariablePK.idConceptoVariable = this.idConceptoVariable;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoVariablePK))
      throw new IllegalArgumentException("arg1");
    ConceptoVariablePK localConceptoVariablePK = (ConceptoVariablePK)paramObject;
    this.idConceptoVariable = localConceptoVariablePK.idConceptoVariable;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoVariablePK))
      throw new IllegalArgumentException("arg2");
    ConceptoVariablePK localConceptoVariablePK = (ConceptoVariablePK)paramObject;
    localConceptoVariablePK.idConceptoVariable = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoVariablePK))
      throw new IllegalArgumentException("arg2");
    ConceptoVariablePK localConceptoVariablePK = (ConceptoVariablePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localConceptoVariablePK.idConceptoVariable);
  }

  private static final int jdoGetanioSobretiempo(ConceptoVariable paramConceptoVariable)
  {
    if (paramConceptoVariable.jdoFlags <= 0)
      return paramConceptoVariable.anioSobretiempo;
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVariable.anioSobretiempo;
    if (localStateManager.isLoaded(paramConceptoVariable, jdoInheritedFieldCount + 0))
      return paramConceptoVariable.anioSobretiempo;
    return localStateManager.getIntField(paramConceptoVariable, jdoInheritedFieldCount + 0, paramConceptoVariable.anioSobretiempo);
  }

  private static final void jdoSetanioSobretiempo(ConceptoVariable paramConceptoVariable, int paramInt)
  {
    if (paramConceptoVariable.jdoFlags == 0)
    {
      paramConceptoVariable.anioSobretiempo = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVariable.anioSobretiempo = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoVariable, jdoInheritedFieldCount + 0, paramConceptoVariable.anioSobretiempo, paramInt);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoVariable paramConceptoVariable)
  {
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVariable.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoVariable, jdoInheritedFieldCount + 1))
      return paramConceptoVariable.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoVariable, jdoInheritedFieldCount + 1, paramConceptoVariable.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoVariable paramConceptoVariable, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVariable.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoVariable, jdoInheritedFieldCount + 1, paramConceptoVariable.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGetdocumentoSoporte(ConceptoVariable paramConceptoVariable)
  {
    if (paramConceptoVariable.jdoFlags <= 0)
      return paramConceptoVariable.documentoSoporte;
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVariable.documentoSoporte;
    if (localStateManager.isLoaded(paramConceptoVariable, jdoInheritedFieldCount + 2))
      return paramConceptoVariable.documentoSoporte;
    return localStateManager.getStringField(paramConceptoVariable, jdoInheritedFieldCount + 2, paramConceptoVariable.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(ConceptoVariable paramConceptoVariable, String paramString)
  {
    if (paramConceptoVariable.jdoFlags == 0)
    {
      paramConceptoVariable.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVariable.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoVariable, jdoInheritedFieldCount + 2, paramConceptoVariable.documentoSoporte, paramString);
  }

  private static final String jdoGetestatus(ConceptoVariable paramConceptoVariable)
  {
    if (paramConceptoVariable.jdoFlags <= 0)
      return paramConceptoVariable.estatus;
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVariable.estatus;
    if (localStateManager.isLoaded(paramConceptoVariable, jdoInheritedFieldCount + 3))
      return paramConceptoVariable.estatus;
    return localStateManager.getStringField(paramConceptoVariable, jdoInheritedFieldCount + 3, paramConceptoVariable.estatus);
  }

  private static final void jdoSetestatus(ConceptoVariable paramConceptoVariable, String paramString)
  {
    if (paramConceptoVariable.jdoFlags == 0)
    {
      paramConceptoVariable.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVariable.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoVariable, jdoInheritedFieldCount + 3, paramConceptoVariable.estatus, paramString);
  }

  private static final Date jdoGetfechaRegistro(ConceptoVariable paramConceptoVariable)
  {
    if (paramConceptoVariable.jdoFlags <= 0)
      return paramConceptoVariable.fechaRegistro;
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVariable.fechaRegistro;
    if (localStateManager.isLoaded(paramConceptoVariable, jdoInheritedFieldCount + 4))
      return paramConceptoVariable.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramConceptoVariable, jdoInheritedFieldCount + 4, paramConceptoVariable.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(ConceptoVariable paramConceptoVariable, Date paramDate)
  {
    if (paramConceptoVariable.jdoFlags == 0)
    {
      paramConceptoVariable.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVariable.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoVariable, jdoInheritedFieldCount + 4, paramConceptoVariable.fechaRegistro, paramDate);
  }

  private static final FrecuenciaTipoPersonal jdoGetfrecuenciaTipoPersonal(ConceptoVariable paramConceptoVariable)
  {
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVariable.frecuenciaTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoVariable, jdoInheritedFieldCount + 5))
      return paramConceptoVariable.frecuenciaTipoPersonal;
    return (FrecuenciaTipoPersonal)localStateManager.getObjectField(paramConceptoVariable, jdoInheritedFieldCount + 5, paramConceptoVariable.frecuenciaTipoPersonal);
  }

  private static final void jdoSetfrecuenciaTipoPersonal(ConceptoVariable paramConceptoVariable, FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVariable.frecuenciaTipoPersonal = paramFrecuenciaTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoVariable, jdoInheritedFieldCount + 5, paramConceptoVariable.frecuenciaTipoPersonal, paramFrecuenciaTipoPersonal);
  }

  private static final long jdoGetidConceptoVariable(ConceptoVariable paramConceptoVariable)
  {
    return paramConceptoVariable.idConceptoVariable;
  }

  private static final void jdoSetidConceptoVariable(ConceptoVariable paramConceptoVariable, long paramLong)
  {
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVariable.idConceptoVariable = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoVariable, jdoInheritedFieldCount + 6, paramConceptoVariable.idConceptoVariable, paramLong);
  }

  private static final int jdoGetmesSobretiempo(ConceptoVariable paramConceptoVariable)
  {
    if (paramConceptoVariable.jdoFlags <= 0)
      return paramConceptoVariable.mesSobretiempo;
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVariable.mesSobretiempo;
    if (localStateManager.isLoaded(paramConceptoVariable, jdoInheritedFieldCount + 7))
      return paramConceptoVariable.mesSobretiempo;
    return localStateManager.getIntField(paramConceptoVariable, jdoInheritedFieldCount + 7, paramConceptoVariable.mesSobretiempo);
  }

  private static final void jdoSetmesSobretiempo(ConceptoVariable paramConceptoVariable, int paramInt)
  {
    if (paramConceptoVariable.jdoFlags == 0)
    {
      paramConceptoVariable.mesSobretiempo = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVariable.mesSobretiempo = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoVariable, jdoInheritedFieldCount + 7, paramConceptoVariable.mesSobretiempo, paramInt);
  }

  private static final double jdoGetmonto(ConceptoVariable paramConceptoVariable)
  {
    if (paramConceptoVariable.jdoFlags <= 0)
      return paramConceptoVariable.monto;
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVariable.monto;
    if (localStateManager.isLoaded(paramConceptoVariable, jdoInheritedFieldCount + 8))
      return paramConceptoVariable.monto;
    return localStateManager.getDoubleField(paramConceptoVariable, jdoInheritedFieldCount + 8, paramConceptoVariable.monto);
  }

  private static final void jdoSetmonto(ConceptoVariable paramConceptoVariable, double paramDouble)
  {
    if (paramConceptoVariable.jdoFlags == 0)
    {
      paramConceptoVariable.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVariable.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoVariable, jdoInheritedFieldCount + 8, paramConceptoVariable.monto, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(ConceptoVariable paramConceptoVariable)
  {
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVariable.trabajador;
    if (localStateManager.isLoaded(paramConceptoVariable, jdoInheritedFieldCount + 9))
      return paramConceptoVariable.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramConceptoVariable, jdoInheritedFieldCount + 9, paramConceptoVariable.trabajador);
  }

  private static final void jdoSettrabajador(ConceptoVariable paramConceptoVariable, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVariable.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramConceptoVariable, jdoInheritedFieldCount + 9, paramConceptoVariable.trabajador, paramTrabajador);
  }

  private static final double jdoGetunidades(ConceptoVariable paramConceptoVariable)
  {
    if (paramConceptoVariable.jdoFlags <= 0)
      return paramConceptoVariable.unidades;
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoVariable.unidades;
    if (localStateManager.isLoaded(paramConceptoVariable, jdoInheritedFieldCount + 10))
      return paramConceptoVariable.unidades;
    return localStateManager.getDoubleField(paramConceptoVariable, jdoInheritedFieldCount + 10, paramConceptoVariable.unidades);
  }

  private static final void jdoSetunidades(ConceptoVariable paramConceptoVariable, double paramDouble)
  {
    if (paramConceptoVariable.jdoFlags == 0)
    {
      paramConceptoVariable.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoVariable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoVariable.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoVariable, jdoInheritedFieldCount + 10, paramConceptoVariable.unidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}