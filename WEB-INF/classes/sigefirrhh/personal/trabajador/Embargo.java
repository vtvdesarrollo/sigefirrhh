package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.expediente.Personal;

public class Embargo
  implements Serializable, PersistenceCapable
{
  private long idEmbargo;
  private String oficio;
  private String expediente;
  private Date fechaVigencia;
  private int numeroMenores;
  private int numeroTickets;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "expediente", "fechaVigencia", "idEmbargo", "numeroMenores", "numeroTickets", "oficio", "personal" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Embargo()
  {
    jdoSetnumeroMenores(this, 0);

    jdoSetnumeroTickets(this, 0);
  }

  public String toString()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaVigencia(this));
  }

  public String getExpediente()
  {
    return jdoGetexpediente(this);
  }
  public void setExpediente(String expediente) {
    jdoSetexpediente(this, expediente);
  }
  public Date getFechaVigencia() {
    return jdoGetfechaVigencia(this);
  }
  public void setFechaVigencia(Date fechaVigencia) {
    jdoSetfechaVigencia(this, fechaVigencia);
  }
  public long getIdEmbargo() {
    return jdoGetidEmbargo(this);
  }
  public void setIdEmbargo(long idEmbargo) {
    jdoSetidEmbargo(this, idEmbargo);
  }
  public int getNumeroMenores() {
    return jdoGetnumeroMenores(this);
  }
  public void setNumeroMenores(int numeroMenores) {
    jdoSetnumeroMenores(this, numeroMenores);
  }
  public int getNumeroTickets() {
    return jdoGetnumeroTickets(this);
  }
  public void setNumeroTickets(int numeroTickets) {
    jdoSetnumeroTickets(this, numeroTickets);
  }
  public String getOficio() {
    return jdoGetoficio(this);
  }
  public void setOficio(String oficio) {
    jdoSetoficio(this, oficio);
  }
  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.Embargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Embargo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Embargo localEmbargo = new Embargo();
    localEmbargo.jdoFlags = 1;
    localEmbargo.jdoStateManager = paramStateManager;
    return localEmbargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Embargo localEmbargo = new Embargo();
    localEmbargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEmbargo.jdoFlags = 1;
    localEmbargo.jdoStateManager = paramStateManager;
    return localEmbargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.expediente);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEmbargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroMenores);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroTickets);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.oficio);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.expediente = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEmbargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMenores = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroTickets = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.oficio = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Embargo paramEmbargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEmbargo == null)
        throw new IllegalArgumentException("arg1");
      this.expediente = paramEmbargo.expediente;
      return;
    case 1:
      if (paramEmbargo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramEmbargo.fechaVigencia;
      return;
    case 2:
      if (paramEmbargo == null)
        throw new IllegalArgumentException("arg1");
      this.idEmbargo = paramEmbargo.idEmbargo;
      return;
    case 3:
      if (paramEmbargo == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMenores = paramEmbargo.numeroMenores;
      return;
    case 4:
      if (paramEmbargo == null)
        throw new IllegalArgumentException("arg1");
      this.numeroTickets = paramEmbargo.numeroTickets;
      return;
    case 5:
      if (paramEmbargo == null)
        throw new IllegalArgumentException("arg1");
      this.oficio = paramEmbargo.oficio;
      return;
    case 6:
      if (paramEmbargo == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramEmbargo.personal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Embargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Embargo localEmbargo = (Embargo)paramObject;
    if (localEmbargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEmbargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EmbargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EmbargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EmbargoPK))
      throw new IllegalArgumentException("arg1");
    EmbargoPK localEmbargoPK = (EmbargoPK)paramObject;
    localEmbargoPK.idEmbargo = this.idEmbargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EmbargoPK))
      throw new IllegalArgumentException("arg1");
    EmbargoPK localEmbargoPK = (EmbargoPK)paramObject;
    this.idEmbargo = localEmbargoPK.idEmbargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EmbargoPK))
      throw new IllegalArgumentException("arg2");
    EmbargoPK localEmbargoPK = (EmbargoPK)paramObject;
    localEmbargoPK.idEmbargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EmbargoPK))
      throw new IllegalArgumentException("arg2");
    EmbargoPK localEmbargoPK = (EmbargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localEmbargoPK.idEmbargo);
  }

  private static final String jdoGetexpediente(Embargo paramEmbargo)
  {
    if (paramEmbargo.jdoFlags <= 0)
      return paramEmbargo.expediente;
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargo.expediente;
    if (localStateManager.isLoaded(paramEmbargo, jdoInheritedFieldCount + 0))
      return paramEmbargo.expediente;
    return localStateManager.getStringField(paramEmbargo, jdoInheritedFieldCount + 0, paramEmbargo.expediente);
  }

  private static final void jdoSetexpediente(Embargo paramEmbargo, String paramString)
  {
    if (paramEmbargo.jdoFlags == 0)
    {
      paramEmbargo.expediente = paramString;
      return;
    }
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargo.expediente = paramString;
      return;
    }
    localStateManager.setStringField(paramEmbargo, jdoInheritedFieldCount + 0, paramEmbargo.expediente, paramString);
  }

  private static final Date jdoGetfechaVigencia(Embargo paramEmbargo)
  {
    if (paramEmbargo.jdoFlags <= 0)
      return paramEmbargo.fechaVigencia;
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargo.fechaVigencia;
    if (localStateManager.isLoaded(paramEmbargo, jdoInheritedFieldCount + 1))
      return paramEmbargo.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramEmbargo, jdoInheritedFieldCount + 1, paramEmbargo.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(Embargo paramEmbargo, Date paramDate)
  {
    if (paramEmbargo.jdoFlags == 0)
    {
      paramEmbargo.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargo.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramEmbargo, jdoInheritedFieldCount + 1, paramEmbargo.fechaVigencia, paramDate);
  }

  private static final long jdoGetidEmbargo(Embargo paramEmbargo)
  {
    return paramEmbargo.idEmbargo;
  }

  private static final void jdoSetidEmbargo(Embargo paramEmbargo, long paramLong)
  {
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargo.idEmbargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramEmbargo, jdoInheritedFieldCount + 2, paramEmbargo.idEmbargo, paramLong);
  }

  private static final int jdoGetnumeroMenores(Embargo paramEmbargo)
  {
    if (paramEmbargo.jdoFlags <= 0)
      return paramEmbargo.numeroMenores;
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargo.numeroMenores;
    if (localStateManager.isLoaded(paramEmbargo, jdoInheritedFieldCount + 3))
      return paramEmbargo.numeroMenores;
    return localStateManager.getIntField(paramEmbargo, jdoInheritedFieldCount + 3, paramEmbargo.numeroMenores);
  }

  private static final void jdoSetnumeroMenores(Embargo paramEmbargo, int paramInt)
  {
    if (paramEmbargo.jdoFlags == 0)
    {
      paramEmbargo.numeroMenores = paramInt;
      return;
    }
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargo.numeroMenores = paramInt;
      return;
    }
    localStateManager.setIntField(paramEmbargo, jdoInheritedFieldCount + 3, paramEmbargo.numeroMenores, paramInt);
  }

  private static final int jdoGetnumeroTickets(Embargo paramEmbargo)
  {
    if (paramEmbargo.jdoFlags <= 0)
      return paramEmbargo.numeroTickets;
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargo.numeroTickets;
    if (localStateManager.isLoaded(paramEmbargo, jdoInheritedFieldCount + 4))
      return paramEmbargo.numeroTickets;
    return localStateManager.getIntField(paramEmbargo, jdoInheritedFieldCount + 4, paramEmbargo.numeroTickets);
  }

  private static final void jdoSetnumeroTickets(Embargo paramEmbargo, int paramInt)
  {
    if (paramEmbargo.jdoFlags == 0)
    {
      paramEmbargo.numeroTickets = paramInt;
      return;
    }
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargo.numeroTickets = paramInt;
      return;
    }
    localStateManager.setIntField(paramEmbargo, jdoInheritedFieldCount + 4, paramEmbargo.numeroTickets, paramInt);
  }

  private static final String jdoGetoficio(Embargo paramEmbargo)
  {
    if (paramEmbargo.jdoFlags <= 0)
      return paramEmbargo.oficio;
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargo.oficio;
    if (localStateManager.isLoaded(paramEmbargo, jdoInheritedFieldCount + 5))
      return paramEmbargo.oficio;
    return localStateManager.getStringField(paramEmbargo, jdoInheritedFieldCount + 5, paramEmbargo.oficio);
  }

  private static final void jdoSetoficio(Embargo paramEmbargo, String paramString)
  {
    if (paramEmbargo.jdoFlags == 0)
    {
      paramEmbargo.oficio = paramString;
      return;
    }
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargo.oficio = paramString;
      return;
    }
    localStateManager.setStringField(paramEmbargo, jdoInheritedFieldCount + 5, paramEmbargo.oficio, paramString);
  }

  private static final Personal jdoGetpersonal(Embargo paramEmbargo)
  {
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargo.personal;
    if (localStateManager.isLoaded(paramEmbargo, jdoInheritedFieldCount + 6))
      return paramEmbargo.personal;
    return (Personal)localStateManager.getObjectField(paramEmbargo, jdoInheritedFieldCount + 6, paramEmbargo.personal);
  }

  private static final void jdoSetpersonal(Embargo paramEmbargo, Personal paramPersonal)
  {
    StateManager localStateManager = paramEmbargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargo.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramEmbargo, jdoInheritedFieldCount + 6, paramEmbargo.personal, paramPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}