package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.docente.Asignatura;

public class TrabajadorAsignatura
  implements Serializable, PersistenceCapable
{
  private long idTrabajadorAsignatura;
  private Asignatura asignatura;
  private double horas;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "asignatura", "horas", "idTrabajadorAsignatura", "trabajador" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.docente.Asignatura"), Double.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 26, 21, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGethoras(this));

    return jdoGetasignatura(this) + " - " + a;
  }

  public Asignatura getAsignatura()
  {
    return jdoGetasignatura(this);
  }
  public void setAsignatura(Asignatura asignatura) {
    jdoSetasignatura(this, asignatura);
  }
  public double getHoras() {
    return jdoGethoras(this);
  }
  public void setHoras(double horas) {
    jdoSethoras(this, horas);
  }
  public long getIdTrabajadorAsignatura() {
    return jdoGetidTrabajadorAsignatura(this);
  }
  public void setIdTrabajadorAsignatura(long idTrabajadorAsignatura) {
    jdoSetidTrabajadorAsignatura(this, idTrabajadorAsignatura);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.TrabajadorAsignatura"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TrabajadorAsignatura());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TrabajadorAsignatura localTrabajadorAsignatura = new TrabajadorAsignatura();
    localTrabajadorAsignatura.jdoFlags = 1;
    localTrabajadorAsignatura.jdoStateManager = paramStateManager;
    return localTrabajadorAsignatura;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TrabajadorAsignatura localTrabajadorAsignatura = new TrabajadorAsignatura();
    localTrabajadorAsignatura.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTrabajadorAsignatura.jdoFlags = 1;
    localTrabajadorAsignatura.jdoStateManager = paramStateManager;
    return localTrabajadorAsignatura;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.asignatura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horas);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTrabajadorAsignatura);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asignatura = ((Asignatura)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horas = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTrabajadorAsignatura = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TrabajadorAsignatura paramTrabajadorAsignatura, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTrabajadorAsignatura == null)
        throw new IllegalArgumentException("arg1");
      this.asignatura = paramTrabajadorAsignatura.asignatura;
      return;
    case 1:
      if (paramTrabajadorAsignatura == null)
        throw new IllegalArgumentException("arg1");
      this.horas = paramTrabajadorAsignatura.horas;
      return;
    case 2:
      if (paramTrabajadorAsignatura == null)
        throw new IllegalArgumentException("arg1");
      this.idTrabajadorAsignatura = paramTrabajadorAsignatura.idTrabajadorAsignatura;
      return;
    case 3:
      if (paramTrabajadorAsignatura == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramTrabajadorAsignatura.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TrabajadorAsignatura))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TrabajadorAsignatura localTrabajadorAsignatura = (TrabajadorAsignatura)paramObject;
    if (localTrabajadorAsignatura.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTrabajadorAsignatura, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TrabajadorAsignaturaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TrabajadorAsignaturaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrabajadorAsignaturaPK))
      throw new IllegalArgumentException("arg1");
    TrabajadorAsignaturaPK localTrabajadorAsignaturaPK = (TrabajadorAsignaturaPK)paramObject;
    localTrabajadorAsignaturaPK.idTrabajadorAsignatura = this.idTrabajadorAsignatura;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrabajadorAsignaturaPK))
      throw new IllegalArgumentException("arg1");
    TrabajadorAsignaturaPK localTrabajadorAsignaturaPK = (TrabajadorAsignaturaPK)paramObject;
    this.idTrabajadorAsignatura = localTrabajadorAsignaturaPK.idTrabajadorAsignatura;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrabajadorAsignaturaPK))
      throw new IllegalArgumentException("arg2");
    TrabajadorAsignaturaPK localTrabajadorAsignaturaPK = (TrabajadorAsignaturaPK)paramObject;
    localTrabajadorAsignaturaPK.idTrabajadorAsignatura = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrabajadorAsignaturaPK))
      throw new IllegalArgumentException("arg2");
    TrabajadorAsignaturaPK localTrabajadorAsignaturaPK = (TrabajadorAsignaturaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localTrabajadorAsignaturaPK.idTrabajadorAsignatura);
  }

  private static final Asignatura jdoGetasignatura(TrabajadorAsignatura paramTrabajadorAsignatura)
  {
    StateManager localStateManager = paramTrabajadorAsignatura.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorAsignatura.asignatura;
    if (localStateManager.isLoaded(paramTrabajadorAsignatura, jdoInheritedFieldCount + 0))
      return paramTrabajadorAsignatura.asignatura;
    return (Asignatura)localStateManager.getObjectField(paramTrabajadorAsignatura, jdoInheritedFieldCount + 0, paramTrabajadorAsignatura.asignatura);
  }

  private static final void jdoSetasignatura(TrabajadorAsignatura paramTrabajadorAsignatura, Asignatura paramAsignatura)
  {
    StateManager localStateManager = paramTrabajadorAsignatura.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAsignatura.asignatura = paramAsignatura;
      return;
    }
    localStateManager.setObjectField(paramTrabajadorAsignatura, jdoInheritedFieldCount + 0, paramTrabajadorAsignatura.asignatura, paramAsignatura);
  }

  private static final double jdoGethoras(TrabajadorAsignatura paramTrabajadorAsignatura)
  {
    if (paramTrabajadorAsignatura.jdoFlags <= 0)
      return paramTrabajadorAsignatura.horas;
    StateManager localStateManager = paramTrabajadorAsignatura.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorAsignatura.horas;
    if (localStateManager.isLoaded(paramTrabajadorAsignatura, jdoInheritedFieldCount + 1))
      return paramTrabajadorAsignatura.horas;
    return localStateManager.getDoubleField(paramTrabajadorAsignatura, jdoInheritedFieldCount + 1, paramTrabajadorAsignatura.horas);
  }

  private static final void jdoSethoras(TrabajadorAsignatura paramTrabajadorAsignatura, double paramDouble)
  {
    if (paramTrabajadorAsignatura.jdoFlags == 0)
    {
      paramTrabajadorAsignatura.horas = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrabajadorAsignatura.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAsignatura.horas = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrabajadorAsignatura, jdoInheritedFieldCount + 1, paramTrabajadorAsignatura.horas, paramDouble);
  }

  private static final long jdoGetidTrabajadorAsignatura(TrabajadorAsignatura paramTrabajadorAsignatura)
  {
    return paramTrabajadorAsignatura.idTrabajadorAsignatura;
  }

  private static final void jdoSetidTrabajadorAsignatura(TrabajadorAsignatura paramTrabajadorAsignatura, long paramLong)
  {
    StateManager localStateManager = paramTrabajadorAsignatura.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAsignatura.idTrabajadorAsignatura = paramLong;
      return;
    }
    localStateManager.setLongField(paramTrabajadorAsignatura, jdoInheritedFieldCount + 2, paramTrabajadorAsignatura.idTrabajadorAsignatura, paramLong);
  }

  private static final Trabajador jdoGettrabajador(TrabajadorAsignatura paramTrabajadorAsignatura)
  {
    StateManager localStateManager = paramTrabajadorAsignatura.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorAsignatura.trabajador;
    if (localStateManager.isLoaded(paramTrabajadorAsignatura, jdoInheritedFieldCount + 3))
      return paramTrabajadorAsignatura.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramTrabajadorAsignatura, jdoInheritedFieldCount + 3, paramTrabajadorAsignatura.trabajador);
  }

  private static final void jdoSettrabajador(TrabajadorAsignatura paramTrabajadorAsignatura, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajadorAsignatura.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorAsignatura.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramTrabajadorAsignatura, jdoInheritedFieldCount + 3, paramTrabajadorAsignatura.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}