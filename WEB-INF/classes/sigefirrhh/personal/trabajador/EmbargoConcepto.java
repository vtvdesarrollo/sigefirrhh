package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.personal.expediente.Personal;

public class EmbargoConcepto
  implements Serializable, PersistenceCapable
{
  private long idEmbargoConcepto;
  private Embargo embargo;
  private Concepto concepto;
  private String nombre;
  private int cedulaBeneficiario;
  private String rifBeneficiario;
  private double porcentaje;
  private double monto;
  private String cuenta;
  private String banco;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "banco", "cedulaBeneficiario", "concepto", "cuenta", "embargo", "idEmbargoConcepto", "monto", "nombre", "personal", "porcentaje", "rifBeneficiario" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Embargo"), Long.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Double.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 26, 21, 26, 24, 21, 21, 26, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public EmbargoConcepto()
  {
    jdoSetcedulaBeneficiario(this, 0);

    jdoSetporcentaje(this, 0.0D);

    jdoSetmonto(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmonto(this));
    String c = b.format(jdoGetporcentaje(this));
    return jdoGetconcepto(this).getCodConcepto() + 
      " " + jdoGetconcepto(this).getDescripcion() + " - Monto " + 
      a + " -  % " + c;
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }
  public String getBanco() {
    return jdoGetbanco(this);
  }
  public void setBanco(String banco) {
    jdoSetbanco(this, banco);
  }
  public int getCedulaBeneficiario() {
    return jdoGetcedulaBeneficiario(this);
  }
  public void setCedulaBeneficiario(int cedulaBeneficiario) {
    jdoSetcedulaBeneficiario(this, cedulaBeneficiario);
  }
  public Concepto getConcepto() {
    return jdoGetconcepto(this);
  }
  public void setConcepto(Concepto concepto) {
    jdoSetconcepto(this, concepto);
  }
  public String getCuenta() {
    return jdoGetcuenta(this);
  }
  public void setCuenta(String cuenta) {
    jdoSetcuenta(this, cuenta);
  }
  public Embargo getEmbargo() {
    return jdoGetembargo(this);
  }
  public void setEmbargo(Embargo embargo) {
    jdoSetembargo(this, embargo);
  }
  public long getIdEmbargoConcepto() {
    return jdoGetidEmbargoConcepto(this);
  }
  public void setIdEmbargoConcepto(long idEmbargoConcepto) {
    jdoSetidEmbargoConcepto(this, idEmbargoConcepto);
  }
  public double getMonto() {
    return jdoGetmonto(this);
  }
  public void setMonto(double monto) {
    jdoSetmonto(this, monto);
  }
  public String getNombre() {
    return jdoGetnombre(this);
  }
  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }
  public double getPorcentaje() {
    return jdoGetporcentaje(this);
  }
  public void setPorcentaje(double porcentaje) {
    jdoSetporcentaje(this, porcentaje);
  }
  public String getRifBeneficiario() {
    return jdoGetrifBeneficiario(this);
  }
  public void setRifBeneficiario(String rifBeneficiario) {
    jdoSetrifBeneficiario(this, rifBeneficiario);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.EmbargoConcepto"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new EmbargoConcepto());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    EmbargoConcepto localEmbargoConcepto = new EmbargoConcepto();
    localEmbargoConcepto.jdoFlags = 1;
    localEmbargoConcepto.jdoStateManager = paramStateManager;
    return localEmbargoConcepto;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    EmbargoConcepto localEmbargoConcepto = new EmbargoConcepto();
    localEmbargoConcepto.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEmbargoConcepto.jdoFlags = 1;
    localEmbargoConcepto.jdoStateManager = paramStateManager;
    return localEmbargoConcepto;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.banco);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaBeneficiario);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.concepto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cuenta);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.embargo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEmbargoConcepto);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.rifBeneficiario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.banco = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaBeneficiario = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.concepto = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuenta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.embargo = ((Embargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEmbargoConcepto = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.rifBeneficiario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(EmbargoConcepto paramEmbargoConcepto, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEmbargoConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.banco = paramEmbargoConcepto.banco;
      return;
    case 1:
      if (paramEmbargoConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaBeneficiario = paramEmbargoConcepto.cedulaBeneficiario;
      return;
    case 2:
      if (paramEmbargoConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.concepto = paramEmbargoConcepto.concepto;
      return;
    case 3:
      if (paramEmbargoConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.cuenta = paramEmbargoConcepto.cuenta;
      return;
    case 4:
      if (paramEmbargoConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.embargo = paramEmbargoConcepto.embargo;
      return;
    case 5:
      if (paramEmbargoConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.idEmbargoConcepto = paramEmbargoConcepto.idEmbargoConcepto;
      return;
    case 6:
      if (paramEmbargoConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramEmbargoConcepto.monto;
      return;
    case 7:
      if (paramEmbargoConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramEmbargoConcepto.nombre;
      return;
    case 8:
      if (paramEmbargoConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramEmbargoConcepto.personal;
      return;
    case 9:
      if (paramEmbargoConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramEmbargoConcepto.porcentaje;
      return;
    case 10:
      if (paramEmbargoConcepto == null)
        throw new IllegalArgumentException("arg1");
      this.rifBeneficiario = paramEmbargoConcepto.rifBeneficiario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof EmbargoConcepto))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    EmbargoConcepto localEmbargoConcepto = (EmbargoConcepto)paramObject;
    if (localEmbargoConcepto.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEmbargoConcepto, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EmbargoConceptoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EmbargoConceptoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EmbargoConceptoPK))
      throw new IllegalArgumentException("arg1");
    EmbargoConceptoPK localEmbargoConceptoPK = (EmbargoConceptoPK)paramObject;
    localEmbargoConceptoPK.idEmbargoConcepto = this.idEmbargoConcepto;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EmbargoConceptoPK))
      throw new IllegalArgumentException("arg1");
    EmbargoConceptoPK localEmbargoConceptoPK = (EmbargoConceptoPK)paramObject;
    this.idEmbargoConcepto = localEmbargoConceptoPK.idEmbargoConcepto;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EmbargoConceptoPK))
      throw new IllegalArgumentException("arg2");
    EmbargoConceptoPK localEmbargoConceptoPK = (EmbargoConceptoPK)paramObject;
    localEmbargoConceptoPK.idEmbargoConcepto = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EmbargoConceptoPK))
      throw new IllegalArgumentException("arg2");
    EmbargoConceptoPK localEmbargoConceptoPK = (EmbargoConceptoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localEmbargoConceptoPK.idEmbargoConcepto);
  }

  private static final String jdoGetbanco(EmbargoConcepto paramEmbargoConcepto)
  {
    if (paramEmbargoConcepto.jdoFlags <= 0)
      return paramEmbargoConcepto.banco;
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoConcepto.banco;
    if (localStateManager.isLoaded(paramEmbargoConcepto, jdoInheritedFieldCount + 0))
      return paramEmbargoConcepto.banco;
    return localStateManager.getStringField(paramEmbargoConcepto, jdoInheritedFieldCount + 0, paramEmbargoConcepto.banco);
  }

  private static final void jdoSetbanco(EmbargoConcepto paramEmbargoConcepto, String paramString)
  {
    if (paramEmbargoConcepto.jdoFlags == 0)
    {
      paramEmbargoConcepto.banco = paramString;
      return;
    }
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoConcepto.banco = paramString;
      return;
    }
    localStateManager.setStringField(paramEmbargoConcepto, jdoInheritedFieldCount + 0, paramEmbargoConcepto.banco, paramString);
  }

  private static final int jdoGetcedulaBeneficiario(EmbargoConcepto paramEmbargoConcepto)
  {
    if (paramEmbargoConcepto.jdoFlags <= 0)
      return paramEmbargoConcepto.cedulaBeneficiario;
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoConcepto.cedulaBeneficiario;
    if (localStateManager.isLoaded(paramEmbargoConcepto, jdoInheritedFieldCount + 1))
      return paramEmbargoConcepto.cedulaBeneficiario;
    return localStateManager.getIntField(paramEmbargoConcepto, jdoInheritedFieldCount + 1, paramEmbargoConcepto.cedulaBeneficiario);
  }

  private static final void jdoSetcedulaBeneficiario(EmbargoConcepto paramEmbargoConcepto, int paramInt)
  {
    if (paramEmbargoConcepto.jdoFlags == 0)
    {
      paramEmbargoConcepto.cedulaBeneficiario = paramInt;
      return;
    }
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoConcepto.cedulaBeneficiario = paramInt;
      return;
    }
    localStateManager.setIntField(paramEmbargoConcepto, jdoInheritedFieldCount + 1, paramEmbargoConcepto.cedulaBeneficiario, paramInt);
  }

  private static final Concepto jdoGetconcepto(EmbargoConcepto paramEmbargoConcepto)
  {
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoConcepto.concepto;
    if (localStateManager.isLoaded(paramEmbargoConcepto, jdoInheritedFieldCount + 2))
      return paramEmbargoConcepto.concepto;
    return (Concepto)localStateManager.getObjectField(paramEmbargoConcepto, jdoInheritedFieldCount + 2, paramEmbargoConcepto.concepto);
  }

  private static final void jdoSetconcepto(EmbargoConcepto paramEmbargoConcepto, Concepto paramConcepto)
  {
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoConcepto.concepto = paramConcepto;
      return;
    }
    localStateManager.setObjectField(paramEmbargoConcepto, jdoInheritedFieldCount + 2, paramEmbargoConcepto.concepto, paramConcepto);
  }

  private static final String jdoGetcuenta(EmbargoConcepto paramEmbargoConcepto)
  {
    if (paramEmbargoConcepto.jdoFlags <= 0)
      return paramEmbargoConcepto.cuenta;
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoConcepto.cuenta;
    if (localStateManager.isLoaded(paramEmbargoConcepto, jdoInheritedFieldCount + 3))
      return paramEmbargoConcepto.cuenta;
    return localStateManager.getStringField(paramEmbargoConcepto, jdoInheritedFieldCount + 3, paramEmbargoConcepto.cuenta);
  }

  private static final void jdoSetcuenta(EmbargoConcepto paramEmbargoConcepto, String paramString)
  {
    if (paramEmbargoConcepto.jdoFlags == 0)
    {
      paramEmbargoConcepto.cuenta = paramString;
      return;
    }
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoConcepto.cuenta = paramString;
      return;
    }
    localStateManager.setStringField(paramEmbargoConcepto, jdoInheritedFieldCount + 3, paramEmbargoConcepto.cuenta, paramString);
  }

  private static final Embargo jdoGetembargo(EmbargoConcepto paramEmbargoConcepto)
  {
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoConcepto.embargo;
    if (localStateManager.isLoaded(paramEmbargoConcepto, jdoInheritedFieldCount + 4))
      return paramEmbargoConcepto.embargo;
    return (Embargo)localStateManager.getObjectField(paramEmbargoConcepto, jdoInheritedFieldCount + 4, paramEmbargoConcepto.embargo);
  }

  private static final void jdoSetembargo(EmbargoConcepto paramEmbargoConcepto, Embargo paramEmbargo)
  {
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoConcepto.embargo = paramEmbargo;
      return;
    }
    localStateManager.setObjectField(paramEmbargoConcepto, jdoInheritedFieldCount + 4, paramEmbargoConcepto.embargo, paramEmbargo);
  }

  private static final long jdoGetidEmbargoConcepto(EmbargoConcepto paramEmbargoConcepto)
  {
    return paramEmbargoConcepto.idEmbargoConcepto;
  }

  private static final void jdoSetidEmbargoConcepto(EmbargoConcepto paramEmbargoConcepto, long paramLong)
  {
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoConcepto.idEmbargoConcepto = paramLong;
      return;
    }
    localStateManager.setLongField(paramEmbargoConcepto, jdoInheritedFieldCount + 5, paramEmbargoConcepto.idEmbargoConcepto, paramLong);
  }

  private static final double jdoGetmonto(EmbargoConcepto paramEmbargoConcepto)
  {
    if (paramEmbargoConcepto.jdoFlags <= 0)
      return paramEmbargoConcepto.monto;
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoConcepto.monto;
    if (localStateManager.isLoaded(paramEmbargoConcepto, jdoInheritedFieldCount + 6))
      return paramEmbargoConcepto.monto;
    return localStateManager.getDoubleField(paramEmbargoConcepto, jdoInheritedFieldCount + 6, paramEmbargoConcepto.monto);
  }

  private static final void jdoSetmonto(EmbargoConcepto paramEmbargoConcepto, double paramDouble)
  {
    if (paramEmbargoConcepto.jdoFlags == 0)
    {
      paramEmbargoConcepto.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoConcepto.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramEmbargoConcepto, jdoInheritedFieldCount + 6, paramEmbargoConcepto.monto, paramDouble);
  }

  private static final String jdoGetnombre(EmbargoConcepto paramEmbargoConcepto)
  {
    if (paramEmbargoConcepto.jdoFlags <= 0)
      return paramEmbargoConcepto.nombre;
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoConcepto.nombre;
    if (localStateManager.isLoaded(paramEmbargoConcepto, jdoInheritedFieldCount + 7))
      return paramEmbargoConcepto.nombre;
    return localStateManager.getStringField(paramEmbargoConcepto, jdoInheritedFieldCount + 7, paramEmbargoConcepto.nombre);
  }

  private static final void jdoSetnombre(EmbargoConcepto paramEmbargoConcepto, String paramString)
  {
    if (paramEmbargoConcepto.jdoFlags == 0)
    {
      paramEmbargoConcepto.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoConcepto.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramEmbargoConcepto, jdoInheritedFieldCount + 7, paramEmbargoConcepto.nombre, paramString);
  }

  private static final Personal jdoGetpersonal(EmbargoConcepto paramEmbargoConcepto)
  {
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoConcepto.personal;
    if (localStateManager.isLoaded(paramEmbargoConcepto, jdoInheritedFieldCount + 8))
      return paramEmbargoConcepto.personal;
    return (Personal)localStateManager.getObjectField(paramEmbargoConcepto, jdoInheritedFieldCount + 8, paramEmbargoConcepto.personal);
  }

  private static final void jdoSetpersonal(EmbargoConcepto paramEmbargoConcepto, Personal paramPersonal)
  {
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoConcepto.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramEmbargoConcepto, jdoInheritedFieldCount + 8, paramEmbargoConcepto.personal, paramPersonal);
  }

  private static final double jdoGetporcentaje(EmbargoConcepto paramEmbargoConcepto)
  {
    if (paramEmbargoConcepto.jdoFlags <= 0)
      return paramEmbargoConcepto.porcentaje;
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoConcepto.porcentaje;
    if (localStateManager.isLoaded(paramEmbargoConcepto, jdoInheritedFieldCount + 9))
      return paramEmbargoConcepto.porcentaje;
    return localStateManager.getDoubleField(paramEmbargoConcepto, jdoInheritedFieldCount + 9, paramEmbargoConcepto.porcentaje);
  }

  private static final void jdoSetporcentaje(EmbargoConcepto paramEmbargoConcepto, double paramDouble)
  {
    if (paramEmbargoConcepto.jdoFlags == 0)
    {
      paramEmbargoConcepto.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoConcepto.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramEmbargoConcepto, jdoInheritedFieldCount + 9, paramEmbargoConcepto.porcentaje, paramDouble);
  }

  private static final String jdoGetrifBeneficiario(EmbargoConcepto paramEmbargoConcepto)
  {
    if (paramEmbargoConcepto.jdoFlags <= 0)
      return paramEmbargoConcepto.rifBeneficiario;
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
      return paramEmbargoConcepto.rifBeneficiario;
    if (localStateManager.isLoaded(paramEmbargoConcepto, jdoInheritedFieldCount + 10))
      return paramEmbargoConcepto.rifBeneficiario;
    return localStateManager.getStringField(paramEmbargoConcepto, jdoInheritedFieldCount + 10, paramEmbargoConcepto.rifBeneficiario);
  }

  private static final void jdoSetrifBeneficiario(EmbargoConcepto paramEmbargoConcepto, String paramString)
  {
    if (paramEmbargoConcepto.jdoFlags == 0)
    {
      paramEmbargoConcepto.rifBeneficiario = paramString;
      return;
    }
    StateManager localStateManager = paramEmbargoConcepto.jdoStateManager;
    if (localStateManager == null)
    {
      paramEmbargoConcepto.rifBeneficiario = paramString;
      return;
    }
    localStateManager.setStringField(paramEmbargoConcepto, jdoInheritedFieldCount + 10, paramEmbargoConcepto.rifBeneficiario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}