package sigefirrhh.personal.trabajador;

import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.RestringidoNoGenBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;

public class TrabajadorNoGenBeanBusiness extends TrabajadorBusiness
{
  Logger log = Logger.getLogger(TrabajadorNoGenBeanBusiness.class.getName());

  public Collection findForNomina(String estatus, long idGrupoNomina, long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && tipoPersonal.grupoNomina.idGrupoNomina == pIdGrupoNomina && estatus == pEstatus";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("String pEstatus, long pIdGrupoNomina, long pIdTipoPersonal");

    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoNomina", new Long(idGrupoNomina));
    parameters.put("pEstatus", estatus);
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colTrabajador;
  }

  public Collection findByPassword(int cedula, String password)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.cedula == pCedula &&  personal.password == pPassword";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("int pCedula, String pPassword");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));
    parameters.put("pPassword", password);

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findForRegistro(int cedula, int anioInicio, Date fechaNacimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Date inicio = new Date(anioInicio - 1900, 0, 1);
    Date fin = new Date(anioInicio + 1 - 1900, 0, 1);

    String filter = "personal.cedula == pCedula &&  fechaIngreso >= pInicio && fechaIngreso < pFin && personal.fechaNacimiento == pFechaNacimiento";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("int pCedula, Date pInicio, Date pFin, Date pFechaNacimiento");
    query.declareImports("import java.util.Date");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));
    parameters.put("pInicio", inicio);
    parameters.put("pFin", fin);
    parameters.put("pFechaNacimiento", fechaNacimiento);

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public Collection findForPrimaAntiguedad(Date fechaInicio, Date fechaFin, long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    int diaInicio = fechaInicio.getDate();
    int mesInicio = fechaInicio.getMonth() + 1;
    int diaFin = fechaFin.getDate();
    int mesFin = fechaFin.getMonth() + 1;
    String estatus = "A";
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && estatus == pEstatus && diaAntiguedad >= pDiaInicio && mesAntiguedad >= pMesInicio && diaAntiguedad <= pDiaFin && mesAntiguedad <= pMesFin";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdTipoPersonal, int pDiaInicio, int pMesInicio, int pDiaFin, int pMesFin, String pEstatus");

    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pDiaInicio", new Integer(diaInicio));
    parameters.put("pMesInicio", new Integer(mesInicio));
    parameters.put("pDiaFin", new Integer(diaFin));
    parameters.put("pMesFin", new Integer(mesFin));
    parameters.put("pEstatus", estatus);

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colTrabajador;
  }

  public Collection findForBonoVacacional(Date fechaInicio, Date fechaFin, long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    int diaInicio = fechaInicio.getDate();
    int mesInicio = fechaInicio.getMonth() + 1;
    int diaFin = fechaFin.getDate();
    int mesFin = fechaFin.getMonth() + 1;
    String estatus = "A";
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && estatus == pEstatus && diaVacaciones >= pDiaInicio && mesVacaciones >= pMesInicio && diaVacaciones <= pDiaFin && mesVacaciones <= pMesFin";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdTipoPersonal, int pDiaInicio, int pMesInicio, int pDiaFin, int pMesFin, String pEstatus");

    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pDiaInicio", new Integer(diaInicio));
    parameters.put("pMesInicio", new Integer(mesInicio));
    parameters.put("pDiaFin", new Integer(diaFin));
    parameters.put("pMesFin", new Integer(mesFin));
    parameters.put("pEstatus", estatus);

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    return colTrabajador;
  }

  public Collection findByCedulaAndTipoPersonal(int cedula, long idOrganismo, long idTipoPersonal)
    throws Exception
  {
    String estatus = "A";
    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.cedula == pCedula && organismo.idOrganismo == pIdOrganismo && tipoPersonal.idTipoPersonal == pIdTipoPersonal && estatus == pEstatus";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("int pCedula, long pIdOrganismo, long pIdTipoPersonal, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));
    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pEstatus", estatus);

    query.setOrdering("personal.primerApellido ascending");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }

  public int verificarSiTrabajadorPuedeIngresar(long idPersonal, long idOrganismo, long idTipoPersonal)
    throws Exception
  {
    TxnManager txn = TxnManagerFactory.makeTransactionManager();
    txn.open();
    RestringidoNoGenBeanBusiness restringidoNoGenBeanBusiness = new RestringidoNoGenBeanBusiness();

    String estatus = "A";
    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.idPersonal == pIdPersonal && organismo.idOrganismo == pIdOrganismo && estatus == pEstatus";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pEstatus", estatus);

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);
    txn.close();

    Iterator iterTrabajador = colTrabajador.iterator();

    if (iterTrabajador.hasNext()) {
      Trabajador trabajador = (Trabajador)iterTrabajador.next();
      boolean existe = restringidoNoGenBeanBusiness.estanRestringidos(trabajador.getTipoPersonal().getIdTipoPersonal(), idTipoPersonal);
      if (existe)
      {
        return 1;
      }
      existe = restringidoNoGenBeanBusiness.estanRestringidos(idTipoPersonal, trabajador.getTipoPersonal().getIdTipoPersonal());

      if (existe)
      {
        return 1;
      }
      return 5;
    }

    txn.open();
    estatus = "E";
    filter = "personal.idPersonal == pIdPersonal && organismo.idOrganismo == pIdOrganismo && estatus == pEstatus && tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo, String pEstatus, long pIdTipoPersonal");
    parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pEstatus", estatus);
    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);
    txn.close();
    if (colTrabajador.size() != 0) {
      return 2;
    }

    txn.open();
    estatus = "E";
    filter = "personal.idPersonal == pIdPersonal && organismo.idOrganismo == pIdOrganismo && estatus == pEstatus ";

    query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo, String pEstatus");
    parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pEstatus", estatus);

    colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);
    txn.close();
    if (colTrabajador.size() != 0) {
      return 3;
    }

    return 0;
  }

  public int validarFechaIngreso(long idPersonal, long idOrganismo, Date fechaIngreso)
    throws Exception
  {
    TxnManager txn = TxnManagerFactory.makeTransactionManager();
    txn.open();
    RestringidoNoGenBeanBusiness restringidoNoGenBeanBusiness = new RestringidoNoGenBeanBusiness();

    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.idPersonal == pIdPersonal && organismo.idOrganismo == pIdOrganismo  && fechaEgreso >= pFechaIngreso ";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo, Date pFechaIngreso");
    query.declareImports("import java.util.Date");

    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pFechaIngreso", fechaIngreso);

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);
    txn.close();

    Iterator iterTrabajador = colTrabajador.iterator();

    if (iterTrabajador.hasNext()) {
      return 1;
    }

    return 0;
  }

  public int verificarSiTrabajadorPuedeReingresar(long idPersonal, long idOrganismo, long idTipoPersonal)
    throws Exception
  {
    TxnManager txn = TxnManagerFactory.makeTransactionManager();
    txn.open();
    RestringidoNoGenBeanBusiness restringidoNoGenBeanBusiness = new RestringidoNoGenBeanBusiness();

    String estatus = "A";
    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.idPersonal == pIdPersonal && organismo.idOrganismo == pIdOrganismo && estatus == pEstatus";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pEstatus", estatus);

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);
    txn.close();

    Iterator iterTrabajador = colTrabajador.iterator();

    if (iterTrabajador.hasNext())
    {
      Trabajador trabajador = (Trabajador)iterTrabajador.next();

      boolean existe = restringidoNoGenBeanBusiness.estanRestringidos(trabajador.getTipoPersonal().getIdTipoPersonal(), idTipoPersonal);
      if (existe) {
        this.log.error("paso no1.1");
        return 1;
      }
      existe = restringidoNoGenBeanBusiness.estanRestringidos(idTipoPersonal, trabajador.getTipoPersonal().getIdTipoPersonal());

      if (existe) {
        this.log.error("paso no1.2");
        return 1;
      }
      return 5;
    }

    return 0;
  }

  public int buscarUltimoCodigoNomina(long idTipoPersonal)
  {
    Connection connection = null;
    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;
    int max = 0;

    StringBuffer sql = new StringBuffer();
    sql.append("select max(codigo_nomina) as max_codigo_nomina from trabajador");
    sql.append(" where");
    sql.append("  trabajador.id_tipo_personal = ?");
    try
    {
      connection = Resource.getConnection();

      stTrabajadores = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stTrabajadores.setLong(1, idTipoPersonal);
      rsTrabajadores = stTrabajadores.executeQuery();

      if (rsTrabajadores.next()) {
        max = rsTrabajadores.getInt("max_codigo_nomina");
      }

      return max;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return 0;
    }
    finally {
      if (rsTrabajadores != null) try {
          rsTrabajadores.close();
        } catch (Exception localException7) {
        } if (stTrabajadores != null) try {
          stTrabajadores.close();
        } catch (Exception localException8) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException9) {
        } 
    }
  }

  public boolean existeCuenta(int cedula, String cuentaNomina, String tipoCuenta) throws Exception { boolean existe = false;
    Connection connection = null;
    ResultSet rsTrabajador = null;
    PreparedStatement stTrabajador = null;

    StringBuffer sql = new StringBuffer();
    sql.append("select cuenta_nomina from trabajador");
    sql.append(" where");
    sql.append(" trabajador.cedula <> ?");
    if (tipoCuenta.equals("N")) {
      sql.append(" and trabajador.cuenta_nomina = ?");
      sql.append(" and trabajador.forma_pago = '1'");
    } else if (tipoCuenta.equals("L")) {
      sql.append(" and trabajador.cuenta_lph = ?");
    } else {
      sql.append(" and trabajador.cuenta_fid = ?");
    }

    try
    {
      connection = Resource.getConnection();

      stTrabajador = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stTrabajador.setInt(1, cedula);
      stTrabajador.setString(2, cuentaNomina);
      rsTrabajador = stTrabajador.executeQuery();

      if (rsTrabajador.next()) {
        existe = true;
      }

      return existe;
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    }
    finally {
      if (rsTrabajador != null) try {
          rsTrabajador.close();
        } catch (Exception localException7) {
        } if (stTrabajador != null) try {
          stTrabajador.close();
        } catch (Exception localException8) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException9)
        {
        } 
    } } 
  public Collection findByPersonalAndEstatus(long idPersonal, String estatus, long idOrganismo) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal &&  organismo.idOrganismo == pIdOrganismo && estatus == pEstatus";

    Query query = pm.newQuery(Trabajador.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pEstatus", estatus);

    query.setOrdering("personal.primerApellido ascending, personal.primerNombre ascending");

    Collection colTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajador);

    return colTrabajador;
  }
}