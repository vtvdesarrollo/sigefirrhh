package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class AnticipoPK
  implements Serializable
{
  public long idAnticipo;

  public AnticipoPK()
  {
  }

  public AnticipoPK(long idAnticipo)
  {
    this.idAnticipo = idAnticipo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AnticipoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AnticipoPK thatPK)
  {
    return 
      this.idAnticipo == thatPK.idAnticipo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAnticipo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAnticipo);
  }
}