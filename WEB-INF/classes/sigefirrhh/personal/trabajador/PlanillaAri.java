package sigefirrhh.personal.trabajador;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PlanillaAri
  implements Serializable, PersistenceCapable
{
  private long idPlanillaAri;
  private int anio;
  private double remuneraciones;
  private double desgravamenes;
  private int totalCargas;
  private double porcentaje;
  private double remuneracionesVariacion1;
  private double desgravamenesVariacion1;
  private int totalCargasVariacion1;
  private double porcentajeVariacion1;
  private double remuneracionesVariacion2;
  private double desgravamenesVariacion2;
  private int totalCargasVariacion2;
  private double porcentajeVariacion2;
  private double remuneracionesVariacion3;
  private double desgravamenesVariacion3;
  private int totalCargasVariacion3;
  private double porcentajeVariacion3;
  private double remuneracionesVariacion4;
  private double desgravamenesVariacion4;
  private int totalCargasVariacion4;
  private double porcentajeVariacion4;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "desgravamenes", "desgravamenesVariacion1", "desgravamenesVariacion2", "desgravamenesVariacion3", "desgravamenesVariacion4", "idPlanillaAri", "porcentaje", "porcentajeVariacion1", "porcentajeVariacion2", "porcentajeVariacion3", "porcentajeVariacion4", "remuneraciones", "remuneracionesVariacion1", "remuneracionesVariacion2", "remuneracionesVariacion3", "remuneracionesVariacion4", "totalCargas", "totalCargasVariacion1", "totalCargasVariacion2", "totalCargasVariacion3", "totalCargasVariacion4", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Long.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetremuneraciones(this));

    DecimalFormat c = new DecimalFormat();
    c.applyPattern("##,###,###.00");
    String d = c.format(jdoGetdesgravamenes(this));

    return "Remuneraciones : " + a + " Desgravamenes : " + d + "  - % " + jdoGetporcentaje(this);
  }

  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public double getDesgravamenes() {
    return jdoGetdesgravamenes(this);
  }
  public void setDesgravamenes(double desgravamenes) {
    jdoSetdesgravamenes(this, desgravamenes);
  }
  public double getDesgravamenesVariacion1() {
    return jdoGetdesgravamenesVariacion1(this);
  }
  public void setDesgravamenesVariacion1(double desgravamenesVariacion1) {
    jdoSetdesgravamenesVariacion1(this, desgravamenesVariacion1);
  }
  public double getDesgravamenesVariacion2() {
    return jdoGetdesgravamenesVariacion2(this);
  }
  public void setDesgravamenesVariacion2(double desgravamenesVariacion2) {
    jdoSetdesgravamenesVariacion2(this, desgravamenesVariacion2);
  }
  public double getDesgravamenesVariacion3() {
    return jdoGetdesgravamenesVariacion3(this);
  }
  public void setDesgravamenesVariacion3(double desgravamenesVariacion3) {
    jdoSetdesgravamenesVariacion3(this, desgravamenesVariacion3);
  }
  public double getDesgravamenesVariacion4() {
    return jdoGetdesgravamenesVariacion4(this);
  }
  public void setDesgravamenesVariacion4(double desgravamenesVariacion4) {
    jdoSetdesgravamenesVariacion4(this, desgravamenesVariacion4);
  }
  public long getIdPlanillaAri() {
    return jdoGetidPlanillaAri(this);
  }
  public void setIdPlanillaAri(long idPlanillaAri) {
    jdoSetidPlanillaAri(this, idPlanillaAri);
  }
  public double getPorcentaje() {
    return jdoGetporcentaje(this);
  }
  public void setPorcentaje(double porcentaje) {
    jdoSetporcentaje(this, porcentaje);
  }
  public double getPorcentajeVariacion1() {
    return jdoGetporcentajeVariacion1(this);
  }
  public void setPorcentajeVariacion1(double porcentajeVariacion1) {
    jdoSetporcentajeVariacion1(this, porcentajeVariacion1);
  }
  public double getPorcentajeVariacion2() {
    return jdoGetporcentajeVariacion2(this);
  }
  public void setPorcentajeVariacion2(double porcentajeVariacion2) {
    jdoSetporcentajeVariacion2(this, porcentajeVariacion2);
  }
  public double getPorcentajeVariacion3() {
    return jdoGetporcentajeVariacion3(this);
  }
  public void setPorcentajeVariacion3(double porcentajeVariacion3) {
    jdoSetporcentajeVariacion3(this, porcentajeVariacion3);
  }
  public double getPorcentajeVariacion4() {
    return jdoGetporcentajeVariacion4(this);
  }
  public void setPorcentajeVariacion4(double porcentajeVariacion4) {
    jdoSetporcentajeVariacion4(this, porcentajeVariacion4);
  }
  public double getRemuneraciones() {
    return jdoGetremuneraciones(this);
  }
  public void setRemuneraciones(double remuneraciones) {
    jdoSetremuneraciones(this, remuneraciones);
  }
  public double getRemuneracionesVariacion1() {
    return jdoGetremuneracionesVariacion1(this);
  }
  public void setRemuneracionesVariacion1(double remuneracionesVariacion1) {
    jdoSetremuneracionesVariacion1(this, remuneracionesVariacion1);
  }
  public double getRemuneracionesVariacion2() {
    return jdoGetremuneracionesVariacion2(this);
  }
  public void setRemuneracionesVariacion2(double remuneracionesVariacion2) {
    jdoSetremuneracionesVariacion2(this, remuneracionesVariacion2);
  }
  public double getRemuneracionesVariacion3() {
    return jdoGetremuneracionesVariacion3(this);
  }
  public void setRemuneracionesVariacion3(double remuneracionesVariacion3) {
    jdoSetremuneracionesVariacion3(this, remuneracionesVariacion3);
  }
  public double getRemuneracionesVariacion4() {
    return jdoGetremuneracionesVariacion4(this);
  }
  public void setRemuneracionesVariacion4(double remuneracionesVariacion4) {
    jdoSetremuneracionesVariacion4(this, remuneracionesVariacion4);
  }
  public int getTotalCargas() {
    return jdoGettotalCargas(this);
  }
  public void setTotalCargas(int totalCargas) {
    jdoSettotalCargas(this, totalCargas);
  }
  public int getTotalCargasVariacion1() {
    return jdoGettotalCargasVariacion1(this);
  }
  public void setTotalCargasVariacion1(int totalCargasVariacion1) {
    jdoSettotalCargasVariacion1(this, totalCargasVariacion1);
  }
  public int getTotalCargasVariacion2() {
    return jdoGettotalCargasVariacion2(this);
  }
  public void setTotalCargasVariacion2(int totalCargasVariacion2) {
    jdoSettotalCargasVariacion2(this, totalCargasVariacion2);
  }
  public int getTotalCargasVariacion3() {
    return jdoGettotalCargasVariacion3(this);
  }
  public void setTotalCargasVariacion3(int totalCargasVariacion3) {
    jdoSettotalCargasVariacion3(this, totalCargasVariacion3);
  }
  public int getTotalCargasVariacion4() {
    return jdoGettotalCargasVariacion4(this);
  }
  public void setTotalCargasVariacion4(int totalCargasVariacion4) {
    jdoSettotalCargasVariacion4(this, totalCargasVariacion4);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 23;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.trabajador.PlanillaAri"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PlanillaAri());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PlanillaAri localPlanillaAri = new PlanillaAri();
    localPlanillaAri.jdoFlags = 1;
    localPlanillaAri.jdoStateManager = paramStateManager;
    return localPlanillaAri;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PlanillaAri localPlanillaAri = new PlanillaAri();
    localPlanillaAri.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPlanillaAri.jdoFlags = 1;
    localPlanillaAri.jdoStateManager = paramStateManager;
    return localPlanillaAri;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.desgravamenes);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.desgravamenesVariacion1);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.desgravamenesVariacion2);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.desgravamenesVariacion3);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.desgravamenesVariacion4);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPlanillaAri);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeVariacion1);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeVariacion2);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeVariacion3);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeVariacion4);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.remuneraciones);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.remuneracionesVariacion1);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.remuneracionesVariacion2);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.remuneracionesVariacion3);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.remuneracionesVariacion4);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.totalCargas);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.totalCargasVariacion1);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.totalCargasVariacion2);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.totalCargasVariacion3);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.totalCargasVariacion4);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.desgravamenes = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.desgravamenesVariacion1 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.desgravamenesVariacion2 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.desgravamenesVariacion3 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.desgravamenesVariacion4 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPlanillaAri = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeVariacion1 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeVariacion2 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeVariacion3 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeVariacion4 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.remuneraciones = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.remuneracionesVariacion1 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.remuneracionesVariacion2 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.remuneracionesVariacion3 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.remuneracionesVariacion4 = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalCargas = localStateManager.replacingIntField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalCargasVariacion1 = localStateManager.replacingIntField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalCargasVariacion2 = localStateManager.replacingIntField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalCargasVariacion3 = localStateManager.replacingIntField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalCargasVariacion4 = localStateManager.replacingIntField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PlanillaAri paramPlanillaAri, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPlanillaAri.anio;
      return;
    case 1:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.desgravamenes = paramPlanillaAri.desgravamenes;
      return;
    case 2:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.desgravamenesVariacion1 = paramPlanillaAri.desgravamenesVariacion1;
      return;
    case 3:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.desgravamenesVariacion2 = paramPlanillaAri.desgravamenesVariacion2;
      return;
    case 4:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.desgravamenesVariacion3 = paramPlanillaAri.desgravamenesVariacion3;
      return;
    case 5:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.desgravamenesVariacion4 = paramPlanillaAri.desgravamenesVariacion4;
      return;
    case 6:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.idPlanillaAri = paramPlanillaAri.idPlanillaAri;
      return;
    case 7:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramPlanillaAri.porcentaje;
      return;
    case 8:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeVariacion1 = paramPlanillaAri.porcentajeVariacion1;
      return;
    case 9:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeVariacion2 = paramPlanillaAri.porcentajeVariacion2;
      return;
    case 10:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeVariacion3 = paramPlanillaAri.porcentajeVariacion3;
      return;
    case 11:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeVariacion4 = paramPlanillaAri.porcentajeVariacion4;
      return;
    case 12:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.remuneraciones = paramPlanillaAri.remuneraciones;
      return;
    case 13:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.remuneracionesVariacion1 = paramPlanillaAri.remuneracionesVariacion1;
      return;
    case 14:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.remuneracionesVariacion2 = paramPlanillaAri.remuneracionesVariacion2;
      return;
    case 15:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.remuneracionesVariacion3 = paramPlanillaAri.remuneracionesVariacion3;
      return;
    case 16:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.remuneracionesVariacion4 = paramPlanillaAri.remuneracionesVariacion4;
      return;
    case 17:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.totalCargas = paramPlanillaAri.totalCargas;
      return;
    case 18:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.totalCargasVariacion1 = paramPlanillaAri.totalCargasVariacion1;
      return;
    case 19:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.totalCargasVariacion2 = paramPlanillaAri.totalCargasVariacion2;
      return;
    case 20:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.totalCargasVariacion3 = paramPlanillaAri.totalCargasVariacion3;
      return;
    case 21:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.totalCargasVariacion4 = paramPlanillaAri.totalCargasVariacion4;
      return;
    case 22:
      if (paramPlanillaAri == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramPlanillaAri.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PlanillaAri))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PlanillaAri localPlanillaAri = (PlanillaAri)paramObject;
    if (localPlanillaAri.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPlanillaAri, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PlanillaAriPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PlanillaAriPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanillaAriPK))
      throw new IllegalArgumentException("arg1");
    PlanillaAriPK localPlanillaAriPK = (PlanillaAriPK)paramObject;
    localPlanillaAriPK.idPlanillaAri = this.idPlanillaAri;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanillaAriPK))
      throw new IllegalArgumentException("arg1");
    PlanillaAriPK localPlanillaAriPK = (PlanillaAriPK)paramObject;
    this.idPlanillaAri = localPlanillaAriPK.idPlanillaAri;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanillaAriPK))
      throw new IllegalArgumentException("arg2");
    PlanillaAriPK localPlanillaAriPK = (PlanillaAriPK)paramObject;
    localPlanillaAriPK.idPlanillaAri = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanillaAriPK))
      throw new IllegalArgumentException("arg2");
    PlanillaAriPK localPlanillaAriPK = (PlanillaAriPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localPlanillaAriPK.idPlanillaAri);
  }

  private static final int jdoGetanio(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.anio;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.anio;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 0))
      return paramPlanillaAri.anio;
    return localStateManager.getIntField(paramPlanillaAri, jdoInheritedFieldCount + 0, paramPlanillaAri.anio);
  }

  private static final void jdoSetanio(PlanillaAri paramPlanillaAri, int paramInt)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanillaAri, jdoInheritedFieldCount + 0, paramPlanillaAri.anio, paramInt);
  }

  private static final double jdoGetdesgravamenes(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.desgravamenes;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.desgravamenes;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 1))
      return paramPlanillaAri.desgravamenes;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 1, paramPlanillaAri.desgravamenes);
  }

  private static final void jdoSetdesgravamenes(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.desgravamenes = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.desgravamenes = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 1, paramPlanillaAri.desgravamenes, paramDouble);
  }

  private static final double jdoGetdesgravamenesVariacion1(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.desgravamenesVariacion1;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.desgravamenesVariacion1;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 2))
      return paramPlanillaAri.desgravamenesVariacion1;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 2, paramPlanillaAri.desgravamenesVariacion1);
  }

  private static final void jdoSetdesgravamenesVariacion1(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.desgravamenesVariacion1 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.desgravamenesVariacion1 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 2, paramPlanillaAri.desgravamenesVariacion1, paramDouble);
  }

  private static final double jdoGetdesgravamenesVariacion2(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.desgravamenesVariacion2;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.desgravamenesVariacion2;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 3))
      return paramPlanillaAri.desgravamenesVariacion2;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 3, paramPlanillaAri.desgravamenesVariacion2);
  }

  private static final void jdoSetdesgravamenesVariacion2(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.desgravamenesVariacion2 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.desgravamenesVariacion2 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 3, paramPlanillaAri.desgravamenesVariacion2, paramDouble);
  }

  private static final double jdoGetdesgravamenesVariacion3(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.desgravamenesVariacion3;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.desgravamenesVariacion3;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 4))
      return paramPlanillaAri.desgravamenesVariacion3;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 4, paramPlanillaAri.desgravamenesVariacion3);
  }

  private static final void jdoSetdesgravamenesVariacion3(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.desgravamenesVariacion3 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.desgravamenesVariacion3 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 4, paramPlanillaAri.desgravamenesVariacion3, paramDouble);
  }

  private static final double jdoGetdesgravamenesVariacion4(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.desgravamenesVariacion4;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.desgravamenesVariacion4;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 5))
      return paramPlanillaAri.desgravamenesVariacion4;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 5, paramPlanillaAri.desgravamenesVariacion4);
  }

  private static final void jdoSetdesgravamenesVariacion4(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.desgravamenesVariacion4 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.desgravamenesVariacion4 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 5, paramPlanillaAri.desgravamenesVariacion4, paramDouble);
  }

  private static final long jdoGetidPlanillaAri(PlanillaAri paramPlanillaAri)
  {
    return paramPlanillaAri.idPlanillaAri;
  }

  private static final void jdoSetidPlanillaAri(PlanillaAri paramPlanillaAri, long paramLong)
  {
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.idPlanillaAri = paramLong;
      return;
    }
    localStateManager.setLongField(paramPlanillaAri, jdoInheritedFieldCount + 6, paramPlanillaAri.idPlanillaAri, paramLong);
  }

  private static final double jdoGetporcentaje(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.porcentaje;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.porcentaje;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 7))
      return paramPlanillaAri.porcentaje;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 7, paramPlanillaAri.porcentaje);
  }

  private static final void jdoSetporcentaje(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 7, paramPlanillaAri.porcentaje, paramDouble);
  }

  private static final double jdoGetporcentajeVariacion1(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.porcentajeVariacion1;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.porcentajeVariacion1;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 8))
      return paramPlanillaAri.porcentajeVariacion1;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 8, paramPlanillaAri.porcentajeVariacion1);
  }

  private static final void jdoSetporcentajeVariacion1(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.porcentajeVariacion1 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.porcentajeVariacion1 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 8, paramPlanillaAri.porcentajeVariacion1, paramDouble);
  }

  private static final double jdoGetporcentajeVariacion2(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.porcentajeVariacion2;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.porcentajeVariacion2;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 9))
      return paramPlanillaAri.porcentajeVariacion2;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 9, paramPlanillaAri.porcentajeVariacion2);
  }

  private static final void jdoSetporcentajeVariacion2(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.porcentajeVariacion2 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.porcentajeVariacion2 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 9, paramPlanillaAri.porcentajeVariacion2, paramDouble);
  }

  private static final double jdoGetporcentajeVariacion3(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.porcentajeVariacion3;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.porcentajeVariacion3;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 10))
      return paramPlanillaAri.porcentajeVariacion3;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 10, paramPlanillaAri.porcentajeVariacion3);
  }

  private static final void jdoSetporcentajeVariacion3(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.porcentajeVariacion3 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.porcentajeVariacion3 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 10, paramPlanillaAri.porcentajeVariacion3, paramDouble);
  }

  private static final double jdoGetporcentajeVariacion4(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.porcentajeVariacion4;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.porcentajeVariacion4;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 11))
      return paramPlanillaAri.porcentajeVariacion4;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 11, paramPlanillaAri.porcentajeVariacion4);
  }

  private static final void jdoSetporcentajeVariacion4(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.porcentajeVariacion4 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.porcentajeVariacion4 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 11, paramPlanillaAri.porcentajeVariacion4, paramDouble);
  }

  private static final double jdoGetremuneraciones(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.remuneraciones;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.remuneraciones;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 12))
      return paramPlanillaAri.remuneraciones;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 12, paramPlanillaAri.remuneraciones);
  }

  private static final void jdoSetremuneraciones(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.remuneraciones = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.remuneraciones = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 12, paramPlanillaAri.remuneraciones, paramDouble);
  }

  private static final double jdoGetremuneracionesVariacion1(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.remuneracionesVariacion1;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.remuneracionesVariacion1;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 13))
      return paramPlanillaAri.remuneracionesVariacion1;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 13, paramPlanillaAri.remuneracionesVariacion1);
  }

  private static final void jdoSetremuneracionesVariacion1(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.remuneracionesVariacion1 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.remuneracionesVariacion1 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 13, paramPlanillaAri.remuneracionesVariacion1, paramDouble);
  }

  private static final double jdoGetremuneracionesVariacion2(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.remuneracionesVariacion2;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.remuneracionesVariacion2;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 14))
      return paramPlanillaAri.remuneracionesVariacion2;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 14, paramPlanillaAri.remuneracionesVariacion2);
  }

  private static final void jdoSetremuneracionesVariacion2(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.remuneracionesVariacion2 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.remuneracionesVariacion2 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 14, paramPlanillaAri.remuneracionesVariacion2, paramDouble);
  }

  private static final double jdoGetremuneracionesVariacion3(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.remuneracionesVariacion3;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.remuneracionesVariacion3;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 15))
      return paramPlanillaAri.remuneracionesVariacion3;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 15, paramPlanillaAri.remuneracionesVariacion3);
  }

  private static final void jdoSetremuneracionesVariacion3(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.remuneracionesVariacion3 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.remuneracionesVariacion3 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 15, paramPlanillaAri.remuneracionesVariacion3, paramDouble);
  }

  private static final double jdoGetremuneracionesVariacion4(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.remuneracionesVariacion4;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.remuneracionesVariacion4;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 16))
      return paramPlanillaAri.remuneracionesVariacion4;
    return localStateManager.getDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 16, paramPlanillaAri.remuneracionesVariacion4);
  }

  private static final void jdoSetremuneracionesVariacion4(PlanillaAri paramPlanillaAri, double paramDouble)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.remuneracionesVariacion4 = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.remuneracionesVariacion4 = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanillaAri, jdoInheritedFieldCount + 16, paramPlanillaAri.remuneracionesVariacion4, paramDouble);
  }

  private static final int jdoGettotalCargas(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.totalCargas;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.totalCargas;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 17))
      return paramPlanillaAri.totalCargas;
    return localStateManager.getIntField(paramPlanillaAri, jdoInheritedFieldCount + 17, paramPlanillaAri.totalCargas);
  }

  private static final void jdoSettotalCargas(PlanillaAri paramPlanillaAri, int paramInt)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.totalCargas = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.totalCargas = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanillaAri, jdoInheritedFieldCount + 17, paramPlanillaAri.totalCargas, paramInt);
  }

  private static final int jdoGettotalCargasVariacion1(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.totalCargasVariacion1;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.totalCargasVariacion1;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 18))
      return paramPlanillaAri.totalCargasVariacion1;
    return localStateManager.getIntField(paramPlanillaAri, jdoInheritedFieldCount + 18, paramPlanillaAri.totalCargasVariacion1);
  }

  private static final void jdoSettotalCargasVariacion1(PlanillaAri paramPlanillaAri, int paramInt)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.totalCargasVariacion1 = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.totalCargasVariacion1 = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanillaAri, jdoInheritedFieldCount + 18, paramPlanillaAri.totalCargasVariacion1, paramInt);
  }

  private static final int jdoGettotalCargasVariacion2(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.totalCargasVariacion2;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.totalCargasVariacion2;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 19))
      return paramPlanillaAri.totalCargasVariacion2;
    return localStateManager.getIntField(paramPlanillaAri, jdoInheritedFieldCount + 19, paramPlanillaAri.totalCargasVariacion2);
  }

  private static final void jdoSettotalCargasVariacion2(PlanillaAri paramPlanillaAri, int paramInt)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.totalCargasVariacion2 = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.totalCargasVariacion2 = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanillaAri, jdoInheritedFieldCount + 19, paramPlanillaAri.totalCargasVariacion2, paramInt);
  }

  private static final int jdoGettotalCargasVariacion3(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.totalCargasVariacion3;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.totalCargasVariacion3;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 20))
      return paramPlanillaAri.totalCargasVariacion3;
    return localStateManager.getIntField(paramPlanillaAri, jdoInheritedFieldCount + 20, paramPlanillaAri.totalCargasVariacion3);
  }

  private static final void jdoSettotalCargasVariacion3(PlanillaAri paramPlanillaAri, int paramInt)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.totalCargasVariacion3 = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.totalCargasVariacion3 = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanillaAri, jdoInheritedFieldCount + 20, paramPlanillaAri.totalCargasVariacion3, paramInt);
  }

  private static final int jdoGettotalCargasVariacion4(PlanillaAri paramPlanillaAri)
  {
    if (paramPlanillaAri.jdoFlags <= 0)
      return paramPlanillaAri.totalCargasVariacion4;
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.totalCargasVariacion4;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 21))
      return paramPlanillaAri.totalCargasVariacion4;
    return localStateManager.getIntField(paramPlanillaAri, jdoInheritedFieldCount + 21, paramPlanillaAri.totalCargasVariacion4);
  }

  private static final void jdoSettotalCargasVariacion4(PlanillaAri paramPlanillaAri, int paramInt)
  {
    if (paramPlanillaAri.jdoFlags == 0)
    {
      paramPlanillaAri.totalCargasVariacion4 = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.totalCargasVariacion4 = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanillaAri, jdoInheritedFieldCount + 21, paramPlanillaAri.totalCargasVariacion4, paramInt);
  }

  private static final Trabajador jdoGettrabajador(PlanillaAri paramPlanillaAri)
  {
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
      return paramPlanillaAri.trabajador;
    if (localStateManager.isLoaded(paramPlanillaAri, jdoInheritedFieldCount + 22))
      return paramPlanillaAri.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramPlanillaAri, jdoInheritedFieldCount + 22, paramPlanillaAri.trabajador);
  }

  private static final void jdoSettrabajador(PlanillaAri paramPlanillaAri, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramPlanillaAri.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanillaAri.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramPlanillaAri, jdoInheritedFieldCount + 22, paramPlanillaAri.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}