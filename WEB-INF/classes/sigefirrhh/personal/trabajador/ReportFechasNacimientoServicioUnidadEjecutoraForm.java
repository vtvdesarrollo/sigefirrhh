package sigefirrhh.personal.trabajador;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportFechasNacimientoServicioUnidadEjecutoraForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportFechasNacimientoServicioUnidadEjecutoraForm.class.getName());
  private int reportId;
  private String formato = "1";
  private long idTipoPersonal;
  private long idUnidadEjecutora;
  private String reportName;
  private String orden;
  private String sexo = "T";
  private Date fechaTope;
  private int edadMinima;
  private int edadMaxima;
  private int servicioMinimo;
  private int servicioMaximo;
  private Calendar fechaInicialEdad;
  private Calendar fechaFinalEdad;
  private Calendar fechaInicialServicio;
  private Calendar fechaFinalServicio;
  private Collection listTipoPersonal;
  private Collection listUnidadEjecutora;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private EstructuraFacade estructuraFacade;

  public ReportFechasNacimientoServicioUnidadEjecutoraForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "tranacseralfuel";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportFechasNacimientoServicioUnidadEjecutoraForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listUnidadEjecutora = this.estructuraFacade.findAllUnidadEjecutora();
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listUnidadEjecutora = new ArrayList();
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    if (this.orden != null) {
      this.reportName = "";

      if (this.formato.equals("2")) {
        this.reportName = "a_";
      }

      if (this.orden.equals("A"))
        this.reportName = "tranacseralf";
      else if (this.orden.equals("C"))
        this.reportName = "tranacserced";
      else if (this.orden.equals("N")) {
        this.reportName = "tranacsercod";
      }
      if (this.idUnidadEjecutora == 0L)
        this.reportName += "uel";
      else if (this.idUnidadEjecutora != 0L) {
        this.reportName += "uel1";
      }

      if (!this.sexo.equals("T"))
        this.reportName += "sex";
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "";
      if (this.formato.equals("2")) {
        this.reportName = "a_";
      }
      if (this.idUnidadEjecutora == 0L) {
        if (this.orden.equals("A"))
          this.reportName = "tranacseralfuel";
        else if (this.orden.equals("C"))
          this.reportName = "tranacserceduel";
        else if (this.orden.equals("N"))
          this.reportName = "tranacsercoduel";
      }
      else if (this.idUnidadEjecutora != 0L) {
        if (this.orden.equals("A"))
          this.reportName = "tranacseralfuel1";
        else if (this.orden.equals("C"))
          this.reportName = "tranacserceduel1";
        else if (this.orden.equals("N")) {
          this.reportName = "tranacsercoduel1";
        }
      }
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      if (this.idUnidadEjecutora != 0L) {
        parameters.put("id_unidad_ejecutora", new Long(this.idUnidadEjecutora));
      }
      if (!this.sexo.equals("T")) {
        parameters.put("sexo", this.sexo);
      }
      this.fechaInicialEdad = Calendar.getInstance();
      this.fechaInicialEdad.setTime(this.fechaTope);
      this.fechaInicialEdad.add(1, -this.edadMaxima);

      this.fechaInicialEdad.add(5, 1);
      this.fechaFinalEdad = Calendar.getInstance();
      this.fechaFinalEdad.setTime(this.fechaTope);
      this.fechaFinalEdad.add(1, -this.edadMinima);

      this.fechaFinalEdad.add(5, 1);

      log.error("FechafinalEdad " + this.fechaFinalEdad.getTime());
      log.error("FechainicialEdad " + this.fechaInicialEdad.getTime());
      log.error("edadminima " + this.edadMinima);
      log.error("edadmaxima " + this.edadMaxima);

      parameters.put("fecnac_ini", this.fechaInicialEdad.getTime());
      parameters.put("fecnac_fin", this.fechaFinalEdad.getTime());
      parameters.put("anionac_ini", new Integer(this.edadMinima));
      parameters.put("anionac_fin", new Integer(this.edadMaxima));

      this.fechaInicialServicio = Calendar.getInstance();
      this.fechaInicialServicio.setTime(this.fechaTope);
      this.fechaInicialServicio.add(1, -this.servicioMaximo);

      this.fechaInicialServicio.add(5, 1);
      this.fechaFinalServicio = Calendar.getInstance();
      this.fechaFinalServicio.setTime(this.fechaTope);
      this.fechaFinalServicio.add(1, -this.servicioMinimo);

      this.fechaFinalServicio.add(5, 1);

      log.error("FechafinalServicio " + this.fechaFinalServicio.getTime());
      log.error("FechainicialServicio " + this.fechaInicialServicio.getTime());
      log.error("serviciominimo " + this.servicioMinimo);
      log.error("serviciomaximo " + this.servicioMaximo);

      parameters.put("fecing_ini", this.fechaInicialServicio.getTime());
      parameters.put("fecing_fin", this.fechaFinalServicio.getTime());
      parameters.put("anioser_ini", new Integer(this.servicioMinimo));
      parameters.put("anioser_fin", new Integer(this.servicioMaximo));

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/trabajador");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l)
  {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListUnidadEjecutora()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadEjecutora, "sigefirrhh.base.estructura.UnidadEjecutora");
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public String getOrden()
  {
    return this.orden;
  }

  public void setOrden(String string)
  {
    this.orden = string;
  }

  public long getIdUnidadEjecutora()
  {
    return this.idUnidadEjecutora;
  }

  public void setIdUnidadEjecutora(long l)
  {
    this.idUnidadEjecutora = l;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String string)
  {
    this.formato = string;
  }

  public int getServicioMaximo() {
    return this.servicioMaximo;
  }
  public void setServicioMaximo(int servicioMaximo) {
    this.servicioMaximo = servicioMaximo;
  }
  public int getServicioMinimo() {
    return this.servicioMinimo;
  }
  public void setServicioMinimo(int servicioMinimo) {
    this.servicioMinimo = servicioMinimo;
  }
  public Date getFechaTope() {
    return this.fechaTope;
  }
  public void setFechaTope(Date fechaTope) {
    this.fechaTope = fechaTope;
  }
  public int getEdadMaxima() {
    return this.edadMaxima;
  }
  public void setEdadMaxima(int edadMaxima) {
    this.edadMaxima = edadMaxima;
  }
  public int getEdadMinima() {
    return this.edadMinima;
  }
  public void setEdadMinima(int edadMinima) {
    this.edadMinima = edadMinima;
  }

  public String getSexo()
  {
    return this.sexo;
  }

  public void setSexo(String sexo)
  {
    this.sexo = sexo;
  }
}