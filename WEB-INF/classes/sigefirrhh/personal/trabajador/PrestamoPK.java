package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class PrestamoPK
  implements Serializable
{
  public long idPrestamo;

  public PrestamoPK()
  {
  }

  public PrestamoPK(long idPrestamo)
  {
    this.idPrestamo = idPrestamo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PrestamoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PrestamoPK thatPK)
  {
    return 
      this.idPrestamo == thatPK.idPrestamo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPrestamo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPrestamo);
  }
}