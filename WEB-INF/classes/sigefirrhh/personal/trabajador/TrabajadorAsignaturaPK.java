package sigefirrhh.personal.trabajador;

import java.io.Serializable;

public class TrabajadorAsignaturaPK
  implements Serializable
{
  public long idTrabajadorAsignatura;

  public TrabajadorAsignaturaPK()
  {
  }

  public TrabajadorAsignaturaPK(long idTrabajadorAsignatura)
  {
    this.idTrabajadorAsignatura = idTrabajadorAsignatura;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TrabajadorAsignaturaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TrabajadorAsignaturaPK thatPK)
  {
    return 
      this.idTrabajadorAsignatura == thatPK.idTrabajadorAsignatura;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTrabajadorAsignatura)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTrabajadorAsignatura);
  }
}