package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.TipoOtraActividad;

public class OtraActividad
  implements Serializable, PersistenceCapable
{
  private long idOtraActividad;
  private TipoOtraActividad tipoOtraActividad;
  private String observaciones;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idOtraActividad", "observaciones", "personal", "tipoOtraActividad" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.personal.TipoOtraActividad") };
  private static final byte[] jdoFieldFlags = { 24, 21, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGettipoOtraActividad(this).getDescripcion();
  }

  public long getIdOtraActividad()
  {
    return jdoGetidOtraActividad(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public TipoOtraActividad getTipoOtraActividad()
  {
    return jdoGettipoOtraActividad(this);
  }

  public void setIdOtraActividad(long l)
  {
    jdoSetidOtraActividad(this, l);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setTipoOtraActividad(TipoOtraActividad actividad)
  {
    jdoSettipoOtraActividad(this, actividad);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.OtraActividad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new OtraActividad());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    OtraActividad localOtraActividad = new OtraActividad();
    localOtraActividad.jdoFlags = 1;
    localOtraActividad.jdoStateManager = paramStateManager;
    return localOtraActividad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    OtraActividad localOtraActividad = new OtraActividad();
    localOtraActividad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localOtraActividad.jdoFlags = 1;
    localOtraActividad.jdoStateManager = paramStateManager;
    return localOtraActividad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idOtraActividad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoOtraActividad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idOtraActividad = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoOtraActividad = ((TipoOtraActividad)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(OtraActividad paramOtraActividad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.idOtraActividad = paramOtraActividad.idOtraActividad;
      return;
    case 1:
      if (paramOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramOtraActividad.observaciones;
      return;
    case 2:
      if (paramOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramOtraActividad.personal;
      return;
    case 3:
      if (paramOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.tipoOtraActividad = paramOtraActividad.tipoOtraActividad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof OtraActividad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    OtraActividad localOtraActividad = (OtraActividad)paramObject;
    if (localOtraActividad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localOtraActividad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new OtraActividadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new OtraActividadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof OtraActividadPK))
      throw new IllegalArgumentException("arg1");
    OtraActividadPK localOtraActividadPK = (OtraActividadPK)paramObject;
    localOtraActividadPK.idOtraActividad = this.idOtraActividad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof OtraActividadPK))
      throw new IllegalArgumentException("arg1");
    OtraActividadPK localOtraActividadPK = (OtraActividadPK)paramObject;
    this.idOtraActividad = localOtraActividadPK.idOtraActividad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof OtraActividadPK))
      throw new IllegalArgumentException("arg2");
    OtraActividadPK localOtraActividadPK = (OtraActividadPK)paramObject;
    localOtraActividadPK.idOtraActividad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof OtraActividadPK))
      throw new IllegalArgumentException("arg2");
    OtraActividadPK localOtraActividadPK = (OtraActividadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localOtraActividadPK.idOtraActividad);
  }

  private static final long jdoGetidOtraActividad(OtraActividad paramOtraActividad)
  {
    return paramOtraActividad.idOtraActividad;
  }

  private static final void jdoSetidOtraActividad(OtraActividad paramOtraActividad, long paramLong)
  {
    StateManager localStateManager = paramOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramOtraActividad.idOtraActividad = paramLong;
      return;
    }
    localStateManager.setLongField(paramOtraActividad, jdoInheritedFieldCount + 0, paramOtraActividad.idOtraActividad, paramLong);
  }

  private static final String jdoGetobservaciones(OtraActividad paramOtraActividad)
  {
    if (paramOtraActividad.jdoFlags <= 0)
      return paramOtraActividad.observaciones;
    StateManager localStateManager = paramOtraActividad.jdoStateManager;
    if (localStateManager == null)
      return paramOtraActividad.observaciones;
    if (localStateManager.isLoaded(paramOtraActividad, jdoInheritedFieldCount + 1))
      return paramOtraActividad.observaciones;
    return localStateManager.getStringField(paramOtraActividad, jdoInheritedFieldCount + 1, paramOtraActividad.observaciones);
  }

  private static final void jdoSetobservaciones(OtraActividad paramOtraActividad, String paramString)
  {
    if (paramOtraActividad.jdoFlags == 0)
    {
      paramOtraActividad.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramOtraActividad.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramOtraActividad, jdoInheritedFieldCount + 1, paramOtraActividad.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(OtraActividad paramOtraActividad)
  {
    StateManager localStateManager = paramOtraActividad.jdoStateManager;
    if (localStateManager == null)
      return paramOtraActividad.personal;
    if (localStateManager.isLoaded(paramOtraActividad, jdoInheritedFieldCount + 2))
      return paramOtraActividad.personal;
    return (Personal)localStateManager.getObjectField(paramOtraActividad, jdoInheritedFieldCount + 2, paramOtraActividad.personal);
  }

  private static final void jdoSetpersonal(OtraActividad paramOtraActividad, Personal paramPersonal)
  {
    StateManager localStateManager = paramOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramOtraActividad.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramOtraActividad, jdoInheritedFieldCount + 2, paramOtraActividad.personal, paramPersonal);
  }

  private static final TipoOtraActividad jdoGettipoOtraActividad(OtraActividad paramOtraActividad)
  {
    StateManager localStateManager = paramOtraActividad.jdoStateManager;
    if (localStateManager == null)
      return paramOtraActividad.tipoOtraActividad;
    if (localStateManager.isLoaded(paramOtraActividad, jdoInheritedFieldCount + 3))
      return paramOtraActividad.tipoOtraActividad;
    return (TipoOtraActividad)localStateManager.getObjectField(paramOtraActividad, jdoInheritedFieldCount + 3, paramOtraActividad.tipoOtraActividad);
  }

  private static final void jdoSettipoOtraActividad(OtraActividad paramOtraActividad, TipoOtraActividad paramTipoOtraActividad)
  {
    StateManager localStateManager = paramOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramOtraActividad.tipoOtraActividad = paramTipoOtraActividad;
      return;
    }
    localStateManager.setObjectField(paramOtraActividad, jdoInheritedFieldCount + 3, paramOtraActividad.tipoOtraActividad, paramTipoOtraActividad);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}