package sigefirrhh.personal.expediente;

import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class TrayectoriaNoGenBeanBusiness extends TrayectoriaBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(TrayectoriaNoGenBeanBusiness.class.getName());

  public String verificarTrayectoria(int cedula, String codCausaMovimiento, java.util.Date fecha)
    throws Exception
  {
    this.log.error("cod_causa_movimiento.................." + codCausaMovimiento);
    StringBuffer sql = new StringBuffer();
    String mensaje = null;

    if (fecha == null) {
      mensaje = "Debe especificar la fecha de vigencia del movimiento ";
      return mensaje;
    }
    java.sql.Date fechaSql = new java.sql.Date(fecha.getYear(), fecha.getMonth(), fecha.getDate());

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    Connection connection = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select * ");
      sql.append(" from trayectoria ");
      sql.append(" where cedula = ? ");
      sql.append(" and fecha_vigencia > ? ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stRegistros.setLong(1, cedula);
      stRegistros.setDate(2, fechaSql);
      rsRegistros = stRegistros.executeQuery();
      String str1;
      if (rsRegistros.next()) {
        mensaje = "Existe un movimiento de " + rsRegistros.getString("descripcion_movimiento") + 
          " con fecha de vigencia " + rsRegistros.getDate("fecha_vigencia") + " aprobado en remesa No." + 
          rsRegistros.getString("numero_remesa") + ", FP-020 No." + rsRegistros.getLong("numero_movimiento") + 
          " y año de preparación " + rsRegistros.getInt("anio_preparacion");
        return mensaje;
      }

      sql = new StringBuffer();
      sql.append("select * ");
      sql.append(" from trayectoria ");
      sql.append(" where cedula = ? ");
      sql.append(" and fecha_vigencia = ? ");
      sql.append(" and cod_causa_movimiento = ? ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stRegistros.setLong(1, cedula);
      stRegistros.setDate(2, fechaSql);
      stRegistros.setString(3, codCausaMovimiento);
      rsRegistros = stRegistros.executeQuery();

      if (rsRegistros.next()) {
        mensaje = "Existe un movimiento en identicas condiciones aprobado en remesa No." + 
          rsRegistros.getString("numero_remesa") + ", FP-020 No." + rsRegistros.getLong("numero_movimiento") + 
          " y año de preparación No." + rsRegistros.getInt("anio_preparacion");
        return mensaje;
      }

      if ((codCausaMovimiento.equals("0101")) || (codCausaMovimiento.equals("0201")))
      {
        sql = new StringBuffer();
        sql.append("select * ");
        sql.append(" from trayectoria ");
        sql.append(" where cedula = ? ");
        sql.append(" and cod_causa_movmiento = 504 ");

        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);

        stRegistros.setLong(1, cedula);
        stRegistros.setString(2, codCausaMovimiento);
        rsRegistros = stRegistros.executeQuery();

        if (rsRegistros.next()) {
          Calendar fechaCal = Calendar.getInstance();
          fechaCal.setTime(fecha);
          fechaCal.add(1, -1);

          if (rsRegistros.getDate("fecha_vigencia").compareTo(fechaCal.getTime()) < 0) {
            mensaje = "Improcedente la aprobación del movimiento por cuanto el funcionario destituido del " + 
              rsRegistros.getString("nombre_oganismo") + " el " + rsRegistros.getDate("fecha_vigencia");
            return mensaje;
          }
          Calendar fechaCal;
          mensaje = mensaje + ", extinguiéndose la condición de funcionario de carrera, debe " + 
            " tramitarse un movimiento de ingreso";
        }

        sql = new StringBuffer();
        sql.append("select * ");
        sql.append(" from trayectoria ");
        sql.append(" where cedula = ? ");
        sql.append(" and cod_causa_movmiento = 501 ");

        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);

        stRegistros.setLong(1, cedula);
        stRegistros.setString(2, codCausaMovimiento);
        rsRegistros = stRegistros.executeQuery();

        if (rsRegistros.next()) {
          Calendar fechaCal = Calendar.getInstance();
          fechaCal.setTime(fecha);
          fechaCal.add(2, -6);

          if (rsRegistros.getDate("fecha_vigencia").compareTo(fechaCal.getTime()) < 0) {
            mensaje = "Improcedente la aprobación del movimiento por cuanto el funcionario renunció al " + 
              rsRegistros.getString("nombre_oganismo") + " el " + rsRegistros.getDate("fecha_vigencia");
            return mensaje;
          }
        }
      }

      if (codCausaMovimiento.equals("310"))
      {
        sql = new StringBuffer();
        sql.append("select cod_causa_movimiento, fecha_vigencia ");
        sql.append(" from trayectoria ");
        sql.append(" where cedula = ? ");
        sql.append(" order by fecha_vigencia desc limit 1 ");

        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);

        stRegistros.setLong(1, cedula);
        rsRegistros = stRegistros.executeQuery();

        if (rsRegistros.next())
        {
          if (!rsRegistros.getString("cod_causa_movimiento").equals("507")) {
            mensaje = "Improcedente la aprobación del movimiento por cuanto el funcionario no posee en su trayectoria el movimiento de remoción";
            return mensaje;
          }

          Calendar cal = Calendar.getInstance();

          cal.setTime(fecha);
          cal.add(2, -1);

          if (rsRegistros.getDate("fecha_vigencia").compareTo(cal.getTime()) < 0) {
            mensaje = "Improcedente la aprobación del movimiento por cuanto el funcionario posee en su trayectoria el movimiento de remoción con fecha superior a un mes";
            this.log.error("fecha vigencia:" + rsRegistros.getDate("fecha_vigencia"));
            this.log.error("fecha disminuida (fecha-1 mes):" + fecha);
            return mensaje;
          }
        }
        else {
          mensaje = "Improcedente la aprobación del movimiento por cuanto el funcionario no posee en su trayectoria el movimiento de remoción";
          return mensaje;
        }
      }

      if (codCausaMovimiento.equals("312"))
      {
        sql = new StringBuffer();
        sql.append("select cod_causa_movimiento, fecha_vigencia ");
        sql.append(" from trayectoria ");
        sql.append(" where cedula = ? ");
        sql.append(" order by fecha_vigencia desc limit 1 ");

        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);

        stRegistros.setLong(1, cedula);
        rsRegistros = stRegistros.executeQuery();

        if (rsRegistros.next())
        {
          if ((!rsRegistros.getString("cod_causa_movimiento").equals("503")) && (!rsRegistros.getString("cod_causa_movimiento").equals("504")) && (!rsRegistros.getString("cod_causa_movimiento").equals("508"))) {
            mensaje = "Improcedente la aprobación del movimiento por cuanto el funcionario no posee en su trayectoria el movimiento de egreso por Destitución, Retiro por Reducción de Personal o Retiro Posterior a la Remoción";
            return mensaje;
          }
        }
        else {
          mensaje = "Improcedente la aprobación del movimiento por cuanto el funcionario no posee en su trayectoria el movimiento de egreso por Destitución, Retiro por Reducción de Personal o Retiro Posterior a la Remoción";
          return mensaje;
        }

      }

      if (codCausaMovimiento.equals("319"))
      {
        sql = new StringBuffer();
        sql.append("select cod_causa_movimiento ");
        sql.append(" from trayectoria ");
        sql.append(" where cedula = ? ");
        sql.append(" order by fecha_vigencia desc limit 1 ");

        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);

        stRegistros.setLong(1, cedula);
        rsRegistros = stRegistros.executeQuery();

        if (rsRegistros.next())
        {
          if (!rsRegistros.getString("cod_causa_movimiento").equals("510")) {
            mensaje = "Improcedente la aprobación del movimiento por cuanto el funcionario no posee en su trayectoria el movimiento de egreso por transferencia de competencia";
            return mensaje;
          }
        } else {
          mensaje = "Improcedente la aprobación del movimiento por cuanto el funcionario no posee en su trayectoria el movimiento de egreso por transferencia de competencia";
          return mensaje;
        }
      }
    }
    finally
    {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException33) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException34) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException35)
        {
        }
    }
    if (rsRegistros != null) try {
        rsRegistros.close();
      } catch (Exception localException36) {
      } if (stRegistros != null) try {
        stRegistros.close();
      } catch (Exception localException37) {
      } if (connection != null) try {
        connection.close(); connection = null;
      }
      catch (Exception localException38)
      {
      } return mensaje;
  }

  public String verificarReingreso(int cedula, String codCausaMovimiento, java.util.Date fechaVigencia) throws Exception
  {
    Connection connection = null;

    StringBuffer sql = new StringBuffer();
    String mensaje = null;
    this.log.error("fecha_egreso_business....." + fechaVigencia);
    if (fechaVigencia == null) {
      mensaje = "Debe especificar la fecha de vigencia del movimiento ";
      return mensaje;
    }

    Calendar calFechaEgreso = Calendar.getInstance();
    Calendar calFechaVigencia = Calendar.getInstance();

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select * ");
      sql.append(" from trayectoria ");
      sql.append(" where cedula = ? ");
      sql.append(" order by id_trayectoria desc limit 1");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stRegistros.setLong(1, cedula);
      rsRegistros = stRegistros.executeQuery();

      if (rsRegistros.next())
      {
        if (rsRegistros.getString("cod_causa_movimiento").equals("502")) {
          mensaje = "true";
        } else if (rsRegistros.getString("cod_causa_movimiento").equals("504"))
        {
          calFechaEgreso.setTime(rsRegistros.getDate("fecha_vigencia"));
          calFechaVigencia.setTime(fechaVigencia);
          calFechaVigencia.add(1, -1);
          if (calFechaEgreso.after(calFechaVigencia))
            mensaje = "El trabajador posee un movimiento de destitución debe esperar por lo menos un año para poder reingresar";
          else
            mensaje = "true";
        }
        else {
          mensaje = "continuar";
        }
      }
      else mensaje = "El trabajador no posee un movimiento anterior de Egreso"; 
    }
    finally
    {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return mensaje;
  }

  public Collection findByPersonal(long idPersonal, String estatus, String origen)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal && estatus == pEstatus && origen == pOrigen";

    Query query = pm.newQuery(Trayectoria.class, filter);

    query.declareParameters("long pIdPersonal, String pEstatus, String pOrigen");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pEstatus", estatus);
    parameters.put("pOrigen", origen);

    query.setOrdering("fechaVigencia ascending");

    Collection colTrayectoria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrayectoria);

    return colTrayectoria;
  }

  public Collection findByPersonal(long idPersonal, String estatus) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal && estatus == pEstatus";

    Query query = pm.newQuery(Trayectoria.class, filter);

    query.declareParameters("long pIdPersonal, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pEstatus", estatus);

    query.setOrdering("fechaVigencia ascending");

    Collection colTrayectoria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrayectoria);

    return colTrayectoria;
  }

  public Collection findAprobadoByPersonal(long idPersonal) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String estatus1 = "4";
    String estatus2 = "5";

    String filter = "personal.idPersonal == pIdPersonal && (estatus == pEstatus1 || estatus == pEstatus2)";

    Query query = pm.newQuery(Trayectoria.class, filter);

    query.declareParameters("long pIdPersonal, String pEstatus1, String pEstatus2");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pEstatus1", estatus1);
    parameters.put("pEstatus2", estatus2);

    query.setOrdering("fechaVigencia ascending");

    Collection colTrayectoria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrayectoria);

    return colTrayectoria;
  }

  public Collection findDevueltoByPersonal(long idPersonal) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String estatus1 = "1";
    String estatus2 = "3";

    String filter = "personal.idPersonal == pIdPersonal && (estatus == pEstatus1 || estatus == pEstatus2)";

    Query query = pm.newQuery(Trayectoria.class, filter);

    query.declareParameters("long pIdPersonal, String pEstatus1, String pEstatus2");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pEstatus1", estatus1);
    parameters.put("pEstatus2", estatus2);

    query.setOrdering("fechaVigencia ascending");

    Collection colTrayectoria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrayectoria);

    return colTrayectoria;
  }

  public Collection findByAnioAndNumeroMovimiento(int anio, int numeroMovimiento) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anioPreparacion == pAnio && numeroMovimiento == pNumeroMovimiento";

    Query query = pm.newQuery(Trayectoria.class, filter);

    query.declareParameters("int pAnio, int pNumeroMovimiento");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));
    parameters.put("pNumeroMovimiento", new Integer(numeroMovimiento));

    Collection colTrayectoria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrayectoria);

    return colTrayectoria;
  }
}