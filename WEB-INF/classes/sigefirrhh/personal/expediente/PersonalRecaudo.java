package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.RelacionRecaudo;

public class PersonalRecaudo
  implements Serializable, PersistenceCapable
{
  private long idPersonalRecaudo;
  private Date fechaRecaudo;
  private RelacionRecaudo relacionRecaudo;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "fechaRecaudo", "idPersonalRecaudo", "personal", "relacionRecaudo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.personal.RelacionRecaudo") };
  private static final byte[] jdoFieldFlags = { 21, 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetrelacionRecaudo(this).getRecaudo() + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaRecaudo(this));
  }

  public Date getFechaRecaudo()
  {
    return jdoGetfechaRecaudo(this);
  }

  public long getIdPersonalRecaudo()
  {
    return jdoGetidPersonalRecaudo(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public RelacionRecaudo getRelacionRecaudo()
  {
    return jdoGetrelacionRecaudo(this);
  }

  public void setFechaRecaudo(Date date)
  {
    jdoSetfechaRecaudo(this, date);
  }

  public void setIdPersonalRecaudo(long l)
  {
    jdoSetidPersonalRecaudo(this, l);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setRelacionRecaudo(RelacionRecaudo recaudo)
  {
    jdoSetrelacionRecaudo(this, recaudo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.PersonalRecaudo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PersonalRecaudo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PersonalRecaudo localPersonalRecaudo = new PersonalRecaudo();
    localPersonalRecaudo.jdoFlags = 1;
    localPersonalRecaudo.jdoStateManager = paramStateManager;
    return localPersonalRecaudo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PersonalRecaudo localPersonalRecaudo = new PersonalRecaudo();
    localPersonalRecaudo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPersonalRecaudo.jdoFlags = 1;
    localPersonalRecaudo.jdoStateManager = paramStateManager;
    return localPersonalRecaudo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRecaudo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPersonalRecaudo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.relacionRecaudo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRecaudo = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPersonalRecaudo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.relacionRecaudo = ((RelacionRecaudo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PersonalRecaudo paramPersonalRecaudo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPersonalRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRecaudo = paramPersonalRecaudo.fechaRecaudo;
      return;
    case 1:
      if (paramPersonalRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.idPersonalRecaudo = paramPersonalRecaudo.idPersonalRecaudo;
      return;
    case 2:
      if (paramPersonalRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramPersonalRecaudo.personal;
      return;
    case 3:
      if (paramPersonalRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.relacionRecaudo = paramPersonalRecaudo.relacionRecaudo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PersonalRecaudo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PersonalRecaudo localPersonalRecaudo = (PersonalRecaudo)paramObject;
    if (localPersonalRecaudo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPersonalRecaudo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PersonalRecaudoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PersonalRecaudoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PersonalRecaudoPK))
      throw new IllegalArgumentException("arg1");
    PersonalRecaudoPK localPersonalRecaudoPK = (PersonalRecaudoPK)paramObject;
    localPersonalRecaudoPK.idPersonalRecaudo = this.idPersonalRecaudo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PersonalRecaudoPK))
      throw new IllegalArgumentException("arg1");
    PersonalRecaudoPK localPersonalRecaudoPK = (PersonalRecaudoPK)paramObject;
    this.idPersonalRecaudo = localPersonalRecaudoPK.idPersonalRecaudo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PersonalRecaudoPK))
      throw new IllegalArgumentException("arg2");
    PersonalRecaudoPK localPersonalRecaudoPK = (PersonalRecaudoPK)paramObject;
    localPersonalRecaudoPK.idPersonalRecaudo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PersonalRecaudoPK))
      throw new IllegalArgumentException("arg2");
    PersonalRecaudoPK localPersonalRecaudoPK = (PersonalRecaudoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localPersonalRecaudoPK.idPersonalRecaudo);
  }

  private static final Date jdoGetfechaRecaudo(PersonalRecaudo paramPersonalRecaudo)
  {
    if (paramPersonalRecaudo.jdoFlags <= 0)
      return paramPersonalRecaudo.fechaRecaudo;
    StateManager localStateManager = paramPersonalRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramPersonalRecaudo.fechaRecaudo;
    if (localStateManager.isLoaded(paramPersonalRecaudo, jdoInheritedFieldCount + 0))
      return paramPersonalRecaudo.fechaRecaudo;
    return (Date)localStateManager.getObjectField(paramPersonalRecaudo, jdoInheritedFieldCount + 0, paramPersonalRecaudo.fechaRecaudo);
  }

  private static final void jdoSetfechaRecaudo(PersonalRecaudo paramPersonalRecaudo, Date paramDate)
  {
    if (paramPersonalRecaudo.jdoFlags == 0)
    {
      paramPersonalRecaudo.fechaRecaudo = paramDate;
      return;
    }
    StateManager localStateManager = paramPersonalRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonalRecaudo.fechaRecaudo = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPersonalRecaudo, jdoInheritedFieldCount + 0, paramPersonalRecaudo.fechaRecaudo, paramDate);
  }

  private static final long jdoGetidPersonalRecaudo(PersonalRecaudo paramPersonalRecaudo)
  {
    return paramPersonalRecaudo.idPersonalRecaudo;
  }

  private static final void jdoSetidPersonalRecaudo(PersonalRecaudo paramPersonalRecaudo, long paramLong)
  {
    StateManager localStateManager = paramPersonalRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonalRecaudo.idPersonalRecaudo = paramLong;
      return;
    }
    localStateManager.setLongField(paramPersonalRecaudo, jdoInheritedFieldCount + 1, paramPersonalRecaudo.idPersonalRecaudo, paramLong);
  }

  private static final Personal jdoGetpersonal(PersonalRecaudo paramPersonalRecaudo)
  {
    StateManager localStateManager = paramPersonalRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramPersonalRecaudo.personal;
    if (localStateManager.isLoaded(paramPersonalRecaudo, jdoInheritedFieldCount + 2))
      return paramPersonalRecaudo.personal;
    return (Personal)localStateManager.getObjectField(paramPersonalRecaudo, jdoInheritedFieldCount + 2, paramPersonalRecaudo.personal);
  }

  private static final void jdoSetpersonal(PersonalRecaudo paramPersonalRecaudo, Personal paramPersonal)
  {
    StateManager localStateManager = paramPersonalRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonalRecaudo.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramPersonalRecaudo, jdoInheritedFieldCount + 2, paramPersonalRecaudo.personal, paramPersonal);
  }

  private static final RelacionRecaudo jdoGetrelacionRecaudo(PersonalRecaudo paramPersonalRecaudo)
  {
    StateManager localStateManager = paramPersonalRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramPersonalRecaudo.relacionRecaudo;
    if (localStateManager.isLoaded(paramPersonalRecaudo, jdoInheritedFieldCount + 3))
      return paramPersonalRecaudo.relacionRecaudo;
    return (RelacionRecaudo)localStateManager.getObjectField(paramPersonalRecaudo, jdoInheritedFieldCount + 3, paramPersonalRecaudo.relacionRecaudo);
  }

  private static final void jdoSetrelacionRecaudo(PersonalRecaudo paramPersonalRecaudo, RelacionRecaudo paramRelacionRecaudo)
  {
    StateManager localStateManager = paramPersonalRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonalRecaudo.relacionRecaudo = paramRelacionRecaudo;
      return;
    }
    localStateManager.setObjectField(paramPersonalRecaudo, jdoInheritedFieldCount + 3, paramPersonalRecaudo.relacionRecaudo, paramRelacionRecaudo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}