package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class AntecedenteAuxPK
  implements Serializable
{
  public long id;

  public AntecedenteAuxPK()
  {
  }

  public AntecedenteAuxPK(long id)
  {
    this.id = id;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AntecedenteAuxPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AntecedenteAuxPK thatPK)
  {
    return 
      this.id == thatPK.id;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.id)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.id);
  }
}