package sigefirrhh.personal.expediente;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class ExpedienteNoGenFacade extends ExpedienteFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private ExpedienteNoGenBusiness expedienteNoGenBusiness = new ExpedienteNoGenBusiness();

  public Collection findTrayectoriaByPersonal(long idPersonal, String estatus, String origen)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteNoGenBusiness.findTrayectoriaByPersonal(idPersonal, estatus, origen);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrayectoriaByPersonal(long idPersonal, String estatus) throws Exception
  {
    try { this.txn.open();
      return this.expedienteNoGenBusiness.findTrayectoriaByPersonal(idPersonal, estatus);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrayectoriaAprobadoByPersonal(long idPersonal) throws Exception
  {
    try { this.txn.open();
      return this.expedienteNoGenBusiness.findTrayectoriaAprobadoByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrayectoriaDevueltoByPersonal(long idPersonal) throws Exception
  {
    try { this.txn.open();
      return this.expedienteNoGenBusiness.findTrayectoriaDevueltoByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrayectoriaByAnioAndNumeroMovimiento(int anio, int numeroMovimiento) throws Exception
  {
    try { this.txn.open();
      return this.expedienteNoGenBusiness.findTrayectoriaByAnioAndNumeroMovimiento(anio, numeroMovimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFamiliarByPersonalAndParentesco(long idPersonal, String parentesco) throws Exception
  {
    try { this.txn.open();
      return this.expedienteNoGenBusiness.findFamiliarByPersonalAndParentesco(idPersonal, parentesco);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFamiliarByPersonalParentescoEdadMaxima(long idPersonal, String parentesco, int edadMaxima) throws Exception
  {
    try { this.txn.open();
      return this.expedienteNoGenBusiness.findFamiliarByPersonalParentescoEdadMaxima(idPersonal, parentesco, edadMaxima);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFamiliarByPersonalParentescoPensionado(long idPersonal, String parentescoH, String parentescoC, int edadMaximaH, int edadMaximaF, int edadMaximaM) throws Exception
  {
    try {
      this.txn.open();
      return this.expedienteNoGenBusiness.findFamiliarByPersonalParentescoPensionado(idPersonal, parentescoH, parentescoC, edadMaximaH, edadMaximaF, edadMaximaM);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEstudioByPersonalAndTipoCurso(long idPersonal, String tipoCurso)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteNoGenBusiness.findEstudioByPersonalAndTipoCurso(idPersonal, tipoCurso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEducacionByClasificacionDocentePostgrado(long idPersonal) throws Exception
  {
    try { this.txn.open();
      return this.expedienteNoGenBusiness.findEducacionByClasificacionDocentePostgrado(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEducacionByClasificacionDocenteDoctorado(long idPersonal) throws Exception
  {
    try { this.txn.open();
      return this.expedienteNoGenBusiness.findEducacionByClasificacionDocenteDoctorado(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAntecedentesApn(long idPersonal)
    throws Exception
  {
    return this.expedienteNoGenBusiness.findAntecedentesApn(idPersonal);
  }

  public AntecedenteAux calcularTiempoApn(Collection colAntecedente)
    throws Exception
  {
    return this.expedienteNoGenBusiness.calcularTiempoApn(colAntecedente);
  }
}