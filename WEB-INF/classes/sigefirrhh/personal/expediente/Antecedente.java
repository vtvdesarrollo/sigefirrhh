package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Antecedente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_VAC_PENDIENTES;
  protected static final Map LISTA_PRE_PENDIENTES;
  private long idAntecedente;
  private String nombreInstitucion;
  private String personalIngreso;
  private Date fechaIngreso;
  private String cargoIngreso;
  private String descargoIngreso;
  private int codracIngreso;
  private double sueldoIngreso;
  private double compensacionIngreso;
  private double primasIngreso;
  private String personalEgreso;
  private Date fechaEgreso;
  private String cargoEgreso;
  private String descargoEgreso;
  private String causaEgreso;
  private int codracEgreso;
  private double sueldoEgreso;
  private double compensacionEgreso;
  private double primasEgreso;
  private String prestacionesPendientes;
  private String vacacionesPendientes;
  private int diasVacaciones;
  private String observaciones;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cargoEgreso", "cargoIngreso", "causaEgreso", "codracEgreso", "codracIngreso", "compensacionEgreso", "compensacionIngreso", "descargoEgreso", "descargoIngreso", "diasVacaciones", "fechaEgreso", "fechaIngreso", "idAntecedente", "idSitp", "nombreInstitucion", "observaciones", "personal", "personalEgreso", "personalIngreso", "prestacionesPendientes", "primasEgreso", "primasIngreso", "sueldoEgreso", "sueldoIngreso", "tiempoSitp", "vacacionesPendientes" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Antecedente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Antecedente());

    LISTA_VAC_PENDIENTES = 
      new LinkedHashMap();
    LISTA_PRE_PENDIENTES = 
      new LinkedHashMap();

    LISTA_VAC_PENDIENTES.put("S", "SI");
    LISTA_VAC_PENDIENTES.put("N", "NO");
    LISTA_PRE_PENDIENTES.put("S", "SI");
    LISTA_PRE_PENDIENTES.put("N", "NO");
  }

  public Antecedente()
  {
    jdoSetcodracIngreso(this, 0);

    jdoSetsueldoIngreso(this, 0.0D);

    jdoSetcompensacionIngreso(this, 0.0D);

    jdoSetprimasIngreso(this, 0.0D);

    jdoSetcodracEgreso(this, 0);

    jdoSetsueldoEgreso(this, 0.0D);

    jdoSetcompensacionEgreso(this, 0.0D);

    jdoSetprimasEgreso(this, 0.0D);

    jdoSetprestacionesPendientes(this, "N");

    jdoSetvacacionesPendientes(this, "N");

    jdoSetdiasVacaciones(this, 0);
  }

  public String toString()
  {
    return jdoGetnombreInstitucion(this) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaIngreso(this)) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaEgreso(this));
  }

  public String getCargoEgreso()
  {
    return jdoGetcargoEgreso(this);
  }

  public String getCargoIngreso()
  {
    return jdoGetcargoIngreso(this);
  }

  public String getCausaEgreso()
  {
    return jdoGetcausaEgreso(this);
  }

  public int getCodracEgreso()
  {
    return jdoGetcodracEgreso(this);
  }

  public int getCodracIngreso()
  {
    return jdoGetcodracIngreso(this);
  }

  public double getCompensacionEgreso()
  {
    return jdoGetcompensacionEgreso(this);
  }

  public double getCompensacionIngreso()
  {
    return jdoGetcompensacionIngreso(this);
  }

  public String getDescargoEgreso()
  {
    return jdoGetdescargoEgreso(this);
  }

  public String getDescargoIngreso()
  {
    return jdoGetdescargoIngreso(this);
  }

  public int getDiasVacaciones()
  {
    return jdoGetdiasVacaciones(this);
  }

  public Date getFechaEgreso()
  {
    return jdoGetfechaEgreso(this);
  }

  public Date getFechaIngreso()
  {
    return jdoGetfechaIngreso(this);
  }

  public long getIdAntecedente()
  {
    return jdoGetidAntecedente(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public String getPersonalEgreso()
  {
    return jdoGetpersonalEgreso(this);
  }

  public String getPersonalIngreso()
  {
    return jdoGetpersonalIngreso(this);
  }

  public String getPrestacionesPendientes()
  {
    return jdoGetprestacionesPendientes(this);
  }

  public double getPrimasEgreso()
  {
    return jdoGetprimasEgreso(this);
  }

  public double getPrimasIngreso()
  {
    return jdoGetprimasIngreso(this);
  }

  public double getSueldoEgreso()
  {
    return jdoGetsueldoEgreso(this);
  }

  public double getSueldoIngreso()
  {
    return jdoGetsueldoIngreso(this);
  }

  public String getVacacionesPendientes()
  {
    return jdoGetvacacionesPendientes(this);
  }

  public void setCargoEgreso(String string)
  {
    jdoSetcargoEgreso(this, string);
  }

  public void setCargoIngreso(String string)
  {
    jdoSetcargoIngreso(this, string);
  }

  public void setCausaEgreso(String string)
  {
    jdoSetcausaEgreso(this, string);
  }

  public void setCodracEgreso(int i)
  {
    jdoSetcodracEgreso(this, i);
  }

  public void setCodracIngreso(int i)
  {
    jdoSetcodracIngreso(this, i);
  }

  public void setCompensacionEgreso(double d)
  {
    jdoSetcompensacionEgreso(this, d);
  }

  public void setCompensacionIngreso(double d)
  {
    jdoSetcompensacionIngreso(this, d);
  }

  public void setDescargoEgreso(String string)
  {
    jdoSetdescargoEgreso(this, string);
  }

  public void setDescargoIngreso(String string)
  {
    jdoSetdescargoIngreso(this, string);
  }

  public void setDiasVacaciones(int i)
  {
    jdoSetdiasVacaciones(this, i);
  }

  public void setFechaEgreso(Date date)
  {
    jdoSetfechaEgreso(this, date);
  }

  public void setFechaIngreso(Date date)
  {
    jdoSetfechaIngreso(this, date);
  }

  public void setIdAntecedente(long l)
  {
    jdoSetidAntecedente(this, l);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setPersonalEgreso(String string)
  {
    jdoSetpersonalEgreso(this, string);
  }

  public void setPersonalIngreso(String string)
  {
    jdoSetpersonalIngreso(this, string);
  }

  public void setPrestacionesPendientes(String string)
  {
    jdoSetprestacionesPendientes(this, string);
  }

  public void setPrimasEgreso(double d)
  {
    jdoSetprimasEgreso(this, d);
  }

  public void setPrimasIngreso(double d)
  {
    jdoSetprimasIngreso(this, d);
  }

  public void setSueldoEgreso(double d)
  {
    jdoSetsueldoEgreso(this, d);
  }

  public void setSueldoIngreso(double d)
  {
    jdoSetsueldoIngreso(this, d);
  }

  public void setVacacionesPendientes(String string)
  {
    jdoSetvacacionesPendientes(this, string);
  }

  public String getNombreInstitucion()
  {
    return jdoGetnombreInstitucion(this);
  }

  public void setNombreInstitucion(String string)
  {
    jdoSetnombreInstitucion(this, string);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 26;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Antecedente localAntecedente = new Antecedente();
    localAntecedente.jdoFlags = 1;
    localAntecedente.jdoStateManager = paramStateManager;
    return localAntecedente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Antecedente localAntecedente = new Antecedente();
    localAntecedente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAntecedente.jdoFlags = 1;
    localAntecedente.jdoStateManager = paramStateManager;
    return localAntecedente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoEgreso);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoIngreso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.causaEgreso);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codracEgreso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codracIngreso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacionEgreso);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacionIngreso);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descargoEgreso);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descargoIngreso);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasVacaciones);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEgreso);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaIngreso);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAntecedente);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreInstitucion);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.personalEgreso);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.personalIngreso);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.prestacionesPendientes);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasEgreso);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasIngreso);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoEgreso);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoIngreso);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vacacionesPendientes);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoEgreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaEgreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codracEgreso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codracIngreso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacionEgreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacionIngreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descargoEgreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descargoIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasVacaciones = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEgreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaIngreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAntecedente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreInstitucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personalEgreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personalIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.prestacionesPendientes = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasEgreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasIngreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoEgreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoIngreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vacacionesPendientes = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Antecedente paramAntecedente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.cargoEgreso = paramAntecedente.cargoEgreso;
      return;
    case 1:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.cargoIngreso = paramAntecedente.cargoIngreso;
      return;
    case 2:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.causaEgreso = paramAntecedente.causaEgreso;
      return;
    case 3:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.codracEgreso = paramAntecedente.codracEgreso;
      return;
    case 4:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.codracIngreso = paramAntecedente.codracIngreso;
      return;
    case 5:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.compensacionEgreso = paramAntecedente.compensacionEgreso;
      return;
    case 6:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.compensacionIngreso = paramAntecedente.compensacionIngreso;
      return;
    case 7:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.descargoEgreso = paramAntecedente.descargoEgreso;
      return;
    case 8:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.descargoIngreso = paramAntecedente.descargoIngreso;
      return;
    case 9:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.diasVacaciones = paramAntecedente.diasVacaciones;
      return;
    case 10:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEgreso = paramAntecedente.fechaEgreso;
      return;
    case 11:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.fechaIngreso = paramAntecedente.fechaIngreso;
      return;
    case 12:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.idAntecedente = paramAntecedente.idAntecedente;
      return;
    case 13:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramAntecedente.idSitp;
      return;
    case 14:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.nombreInstitucion = paramAntecedente.nombreInstitucion;
      return;
    case 15:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramAntecedente.observaciones;
      return;
    case 16:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramAntecedente.personal;
      return;
    case 17:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.personalEgreso = paramAntecedente.personalEgreso;
      return;
    case 18:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.personalIngreso = paramAntecedente.personalIngreso;
      return;
    case 19:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.prestacionesPendientes = paramAntecedente.prestacionesPendientes;
      return;
    case 20:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.primasEgreso = paramAntecedente.primasEgreso;
      return;
    case 21:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.primasIngreso = paramAntecedente.primasIngreso;
      return;
    case 22:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoEgreso = paramAntecedente.sueldoEgreso;
      return;
    case 23:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoIngreso = paramAntecedente.sueldoIngreso;
      return;
    case 24:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramAntecedente.tiempoSitp;
      return;
    case 25:
      if (paramAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.vacacionesPendientes = paramAntecedente.vacacionesPendientes;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Antecedente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Antecedente localAntecedente = (Antecedente)paramObject;
    if (localAntecedente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAntecedente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AntecedentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AntecedentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AntecedentePK))
      throw new IllegalArgumentException("arg1");
    AntecedentePK localAntecedentePK = (AntecedentePK)paramObject;
    localAntecedentePK.idAntecedente = this.idAntecedente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AntecedentePK))
      throw new IllegalArgumentException("arg1");
    AntecedentePK localAntecedentePK = (AntecedentePK)paramObject;
    this.idAntecedente = localAntecedentePK.idAntecedente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AntecedentePK))
      throw new IllegalArgumentException("arg2");
    AntecedentePK localAntecedentePK = (AntecedentePK)paramObject;
    localAntecedentePK.idAntecedente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 12);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AntecedentePK))
      throw new IllegalArgumentException("arg2");
    AntecedentePK localAntecedentePK = (AntecedentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 12, localAntecedentePK.idAntecedente);
  }

  private static final String jdoGetcargoEgreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.cargoEgreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.cargoEgreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 0))
      return paramAntecedente.cargoEgreso;
    return localStateManager.getStringField(paramAntecedente, jdoInheritedFieldCount + 0, paramAntecedente.cargoEgreso);
  }

  private static final void jdoSetcargoEgreso(Antecedente paramAntecedente, String paramString)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.cargoEgreso = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.cargoEgreso = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedente, jdoInheritedFieldCount + 0, paramAntecedente.cargoEgreso, paramString);
  }

  private static final String jdoGetcargoIngreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.cargoIngreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.cargoIngreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 1))
      return paramAntecedente.cargoIngreso;
    return localStateManager.getStringField(paramAntecedente, jdoInheritedFieldCount + 1, paramAntecedente.cargoIngreso);
  }

  private static final void jdoSetcargoIngreso(Antecedente paramAntecedente, String paramString)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.cargoIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.cargoIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedente, jdoInheritedFieldCount + 1, paramAntecedente.cargoIngreso, paramString);
  }

  private static final String jdoGetcausaEgreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.causaEgreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.causaEgreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 2))
      return paramAntecedente.causaEgreso;
    return localStateManager.getStringField(paramAntecedente, jdoInheritedFieldCount + 2, paramAntecedente.causaEgreso);
  }

  private static final void jdoSetcausaEgreso(Antecedente paramAntecedente, String paramString)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.causaEgreso = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.causaEgreso = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedente, jdoInheritedFieldCount + 2, paramAntecedente.causaEgreso, paramString);
  }

  private static final int jdoGetcodracEgreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.codracEgreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.codracEgreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 3))
      return paramAntecedente.codracEgreso;
    return localStateManager.getIntField(paramAntecedente, jdoInheritedFieldCount + 3, paramAntecedente.codracEgreso);
  }

  private static final void jdoSetcodracEgreso(Antecedente paramAntecedente, int paramInt)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.codracEgreso = paramInt;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.codracEgreso = paramInt;
      return;
    }
    localStateManager.setIntField(paramAntecedente, jdoInheritedFieldCount + 3, paramAntecedente.codracEgreso, paramInt);
  }

  private static final int jdoGetcodracIngreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.codracIngreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.codracIngreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 4))
      return paramAntecedente.codracIngreso;
    return localStateManager.getIntField(paramAntecedente, jdoInheritedFieldCount + 4, paramAntecedente.codracIngreso);
  }

  private static final void jdoSetcodracIngreso(Antecedente paramAntecedente, int paramInt)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.codracIngreso = paramInt;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.codracIngreso = paramInt;
      return;
    }
    localStateManager.setIntField(paramAntecedente, jdoInheritedFieldCount + 4, paramAntecedente.codracIngreso, paramInt);
  }

  private static final double jdoGetcompensacionEgreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.compensacionEgreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.compensacionEgreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 5))
      return paramAntecedente.compensacionEgreso;
    return localStateManager.getDoubleField(paramAntecedente, jdoInheritedFieldCount + 5, paramAntecedente.compensacionEgreso);
  }

  private static final void jdoSetcompensacionEgreso(Antecedente paramAntecedente, double paramDouble)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.compensacionEgreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.compensacionEgreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAntecedente, jdoInheritedFieldCount + 5, paramAntecedente.compensacionEgreso, paramDouble);
  }

  private static final double jdoGetcompensacionIngreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.compensacionIngreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.compensacionIngreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 6))
      return paramAntecedente.compensacionIngreso;
    return localStateManager.getDoubleField(paramAntecedente, jdoInheritedFieldCount + 6, paramAntecedente.compensacionIngreso);
  }

  private static final void jdoSetcompensacionIngreso(Antecedente paramAntecedente, double paramDouble)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.compensacionIngreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.compensacionIngreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAntecedente, jdoInheritedFieldCount + 6, paramAntecedente.compensacionIngreso, paramDouble);
  }

  private static final String jdoGetdescargoEgreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.descargoEgreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.descargoEgreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 7))
      return paramAntecedente.descargoEgreso;
    return localStateManager.getStringField(paramAntecedente, jdoInheritedFieldCount + 7, paramAntecedente.descargoEgreso);
  }

  private static final void jdoSetdescargoEgreso(Antecedente paramAntecedente, String paramString)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.descargoEgreso = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.descargoEgreso = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedente, jdoInheritedFieldCount + 7, paramAntecedente.descargoEgreso, paramString);
  }

  private static final String jdoGetdescargoIngreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.descargoIngreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.descargoIngreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 8))
      return paramAntecedente.descargoIngreso;
    return localStateManager.getStringField(paramAntecedente, jdoInheritedFieldCount + 8, paramAntecedente.descargoIngreso);
  }

  private static final void jdoSetdescargoIngreso(Antecedente paramAntecedente, String paramString)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.descargoIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.descargoIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedente, jdoInheritedFieldCount + 8, paramAntecedente.descargoIngreso, paramString);
  }

  private static final int jdoGetdiasVacaciones(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.diasVacaciones;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.diasVacaciones;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 9))
      return paramAntecedente.diasVacaciones;
    return localStateManager.getIntField(paramAntecedente, jdoInheritedFieldCount + 9, paramAntecedente.diasVacaciones);
  }

  private static final void jdoSetdiasVacaciones(Antecedente paramAntecedente, int paramInt)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.diasVacaciones = paramInt;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.diasVacaciones = paramInt;
      return;
    }
    localStateManager.setIntField(paramAntecedente, jdoInheritedFieldCount + 9, paramAntecedente.diasVacaciones, paramInt);
  }

  private static final Date jdoGetfechaEgreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.fechaEgreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.fechaEgreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 10))
      return paramAntecedente.fechaEgreso;
    return (Date)localStateManager.getObjectField(paramAntecedente, jdoInheritedFieldCount + 10, paramAntecedente.fechaEgreso);
  }

  private static final void jdoSetfechaEgreso(Antecedente paramAntecedente, Date paramDate)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.fechaEgreso = paramDate;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.fechaEgreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAntecedente, jdoInheritedFieldCount + 10, paramAntecedente.fechaEgreso, paramDate);
  }

  private static final Date jdoGetfechaIngreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.fechaIngreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.fechaIngreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 11))
      return paramAntecedente.fechaIngreso;
    return (Date)localStateManager.getObjectField(paramAntecedente, jdoInheritedFieldCount + 11, paramAntecedente.fechaIngreso);
  }

  private static final void jdoSetfechaIngreso(Antecedente paramAntecedente, Date paramDate)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.fechaIngreso = paramDate;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.fechaIngreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAntecedente, jdoInheritedFieldCount + 11, paramAntecedente.fechaIngreso, paramDate);
  }

  private static final long jdoGetidAntecedente(Antecedente paramAntecedente)
  {
    return paramAntecedente.idAntecedente;
  }

  private static final void jdoSetidAntecedente(Antecedente paramAntecedente, long paramLong)
  {
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.idAntecedente = paramLong;
      return;
    }
    localStateManager.setLongField(paramAntecedente, jdoInheritedFieldCount + 12, paramAntecedente.idAntecedente, paramLong);
  }

  private static final int jdoGetidSitp(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.idSitp;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.idSitp;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 13))
      return paramAntecedente.idSitp;
    return localStateManager.getIntField(paramAntecedente, jdoInheritedFieldCount + 13, paramAntecedente.idSitp);
  }

  private static final void jdoSetidSitp(Antecedente paramAntecedente, int paramInt)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramAntecedente, jdoInheritedFieldCount + 13, paramAntecedente.idSitp, paramInt);
  }

  private static final String jdoGetnombreInstitucion(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.nombreInstitucion;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.nombreInstitucion;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 14))
      return paramAntecedente.nombreInstitucion;
    return localStateManager.getStringField(paramAntecedente, jdoInheritedFieldCount + 14, paramAntecedente.nombreInstitucion);
  }

  private static final void jdoSetnombreInstitucion(Antecedente paramAntecedente, String paramString)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.nombreInstitucion = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.nombreInstitucion = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedente, jdoInheritedFieldCount + 14, paramAntecedente.nombreInstitucion, paramString);
  }

  private static final String jdoGetobservaciones(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.observaciones;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.observaciones;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 15))
      return paramAntecedente.observaciones;
    return localStateManager.getStringField(paramAntecedente, jdoInheritedFieldCount + 15, paramAntecedente.observaciones);
  }

  private static final void jdoSetobservaciones(Antecedente paramAntecedente, String paramString)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedente, jdoInheritedFieldCount + 15, paramAntecedente.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Antecedente paramAntecedente)
  {
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.personal;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 16))
      return paramAntecedente.personal;
    return (Personal)localStateManager.getObjectField(paramAntecedente, jdoInheritedFieldCount + 16, paramAntecedente.personal);
  }

  private static final void jdoSetpersonal(Antecedente paramAntecedente, Personal paramPersonal)
  {
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramAntecedente, jdoInheritedFieldCount + 16, paramAntecedente.personal, paramPersonal);
  }

  private static final String jdoGetpersonalEgreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.personalEgreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.personalEgreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 17))
      return paramAntecedente.personalEgreso;
    return localStateManager.getStringField(paramAntecedente, jdoInheritedFieldCount + 17, paramAntecedente.personalEgreso);
  }

  private static final void jdoSetpersonalEgreso(Antecedente paramAntecedente, String paramString)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.personalEgreso = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.personalEgreso = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedente, jdoInheritedFieldCount + 17, paramAntecedente.personalEgreso, paramString);
  }

  private static final String jdoGetpersonalIngreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.personalIngreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.personalIngreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 18))
      return paramAntecedente.personalIngreso;
    return localStateManager.getStringField(paramAntecedente, jdoInheritedFieldCount + 18, paramAntecedente.personalIngreso);
  }

  private static final void jdoSetpersonalIngreso(Antecedente paramAntecedente, String paramString)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.personalIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.personalIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedente, jdoInheritedFieldCount + 18, paramAntecedente.personalIngreso, paramString);
  }

  private static final String jdoGetprestacionesPendientes(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.prestacionesPendientes;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.prestacionesPendientes;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 19))
      return paramAntecedente.prestacionesPendientes;
    return localStateManager.getStringField(paramAntecedente, jdoInheritedFieldCount + 19, paramAntecedente.prestacionesPendientes);
  }

  private static final void jdoSetprestacionesPendientes(Antecedente paramAntecedente, String paramString)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.prestacionesPendientes = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.prestacionesPendientes = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedente, jdoInheritedFieldCount + 19, paramAntecedente.prestacionesPendientes, paramString);
  }

  private static final double jdoGetprimasEgreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.primasEgreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.primasEgreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 20))
      return paramAntecedente.primasEgreso;
    return localStateManager.getDoubleField(paramAntecedente, jdoInheritedFieldCount + 20, paramAntecedente.primasEgreso);
  }

  private static final void jdoSetprimasEgreso(Antecedente paramAntecedente, double paramDouble)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.primasEgreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.primasEgreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAntecedente, jdoInheritedFieldCount + 20, paramAntecedente.primasEgreso, paramDouble);
  }

  private static final double jdoGetprimasIngreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.primasIngreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.primasIngreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 21))
      return paramAntecedente.primasIngreso;
    return localStateManager.getDoubleField(paramAntecedente, jdoInheritedFieldCount + 21, paramAntecedente.primasIngreso);
  }

  private static final void jdoSetprimasIngreso(Antecedente paramAntecedente, double paramDouble)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.primasIngreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.primasIngreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAntecedente, jdoInheritedFieldCount + 21, paramAntecedente.primasIngreso, paramDouble);
  }

  private static final double jdoGetsueldoEgreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.sueldoEgreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.sueldoEgreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 22))
      return paramAntecedente.sueldoEgreso;
    return localStateManager.getDoubleField(paramAntecedente, jdoInheritedFieldCount + 22, paramAntecedente.sueldoEgreso);
  }

  private static final void jdoSetsueldoEgreso(Antecedente paramAntecedente, double paramDouble)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.sueldoEgreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.sueldoEgreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAntecedente, jdoInheritedFieldCount + 22, paramAntecedente.sueldoEgreso, paramDouble);
  }

  private static final double jdoGetsueldoIngreso(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.sueldoIngreso;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.sueldoIngreso;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 23))
      return paramAntecedente.sueldoIngreso;
    return localStateManager.getDoubleField(paramAntecedente, jdoInheritedFieldCount + 23, paramAntecedente.sueldoIngreso);
  }

  private static final void jdoSetsueldoIngreso(Antecedente paramAntecedente, double paramDouble)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.sueldoIngreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.sueldoIngreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAntecedente, jdoInheritedFieldCount + 23, paramAntecedente.sueldoIngreso, paramDouble);
  }

  private static final Date jdoGettiempoSitp(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.tiempoSitp;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.tiempoSitp;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 24))
      return paramAntecedente.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramAntecedente, jdoInheritedFieldCount + 24, paramAntecedente.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Antecedente paramAntecedente, Date paramDate)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAntecedente, jdoInheritedFieldCount + 24, paramAntecedente.tiempoSitp, paramDate);
  }

  private static final String jdoGetvacacionesPendientes(Antecedente paramAntecedente)
  {
    if (paramAntecedente.jdoFlags <= 0)
      return paramAntecedente.vacacionesPendientes;
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedente.vacacionesPendientes;
    if (localStateManager.isLoaded(paramAntecedente, jdoInheritedFieldCount + 25))
      return paramAntecedente.vacacionesPendientes;
    return localStateManager.getStringField(paramAntecedente, jdoInheritedFieldCount + 25, paramAntecedente.vacacionesPendientes);
  }

  private static final void jdoSetvacacionesPendientes(Antecedente paramAntecedente, String paramString)
  {
    if (paramAntecedente.jdoFlags == 0)
    {
      paramAntecedente.vacacionesPendientes = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedente.vacacionesPendientes = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedente, jdoInheritedFieldCount + 25, paramAntecedente.vacacionesPendientes, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}