package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class VacacionProgramadaPK
  implements Serializable
{
  public long idVacacionProgramada;

  public VacacionProgramadaPK()
  {
  }

  public VacacionProgramadaPK(long idVacacionProgramada)
  {
    this.idVacacionProgramada = idVacacionProgramada;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((VacacionProgramadaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(VacacionProgramadaPK thatPK)
  {
    return 
      this.idVacacionProgramada == thatPK.idVacacionProgramada;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idVacacionProgramada)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idVacacionProgramada);
  }
}