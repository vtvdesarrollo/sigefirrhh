package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class Declaracion
  implements Serializable, PersistenceCapable
{
  private long idDeclaracion;
  private Date fechaRegistro;
  private String descargo;
  private String observaciones;
  private Personal personal;
  private Organismo organismo;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "descargo", "fechaRegistro", "idDeclaracion", "idSitp", "observaciones", "organismo", "personal", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 26, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescargo(this) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaRegistro(this));
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public long getIdDeclaracion()
  {
    return jdoGetidDeclaracion(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setIdDeclaracion(long l)
  {
    jdoSetidDeclaracion(this, l);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public String getDescargo()
  {
    return jdoGetdescargo(this);
  }

  public void setDescargo(String string)
  {
    jdoSetdescargo(this, string);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Declaracion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Declaracion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Declaracion localDeclaracion = new Declaracion();
    localDeclaracion.jdoFlags = 1;
    localDeclaracion.jdoStateManager = paramStateManager;
    return localDeclaracion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Declaracion localDeclaracion = new Declaracion();
    localDeclaracion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDeclaracion.jdoFlags = 1;
    localDeclaracion.jdoStateManager = paramStateManager;
    return localDeclaracion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDeclaracion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDeclaracion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Declaracion paramDeclaracion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDeclaracion == null)
        throw new IllegalArgumentException("arg1");
      this.descargo = paramDeclaracion.descargo;
      return;
    case 1:
      if (paramDeclaracion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramDeclaracion.fechaRegistro;
      return;
    case 2:
      if (paramDeclaracion == null)
        throw new IllegalArgumentException("arg1");
      this.idDeclaracion = paramDeclaracion.idDeclaracion;
      return;
    case 3:
      if (paramDeclaracion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramDeclaracion.idSitp;
      return;
    case 4:
      if (paramDeclaracion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramDeclaracion.observaciones;
      return;
    case 5:
      if (paramDeclaracion == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramDeclaracion.organismo;
      return;
    case 6:
      if (paramDeclaracion == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramDeclaracion.personal;
      return;
    case 7:
      if (paramDeclaracion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramDeclaracion.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Declaracion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Declaracion localDeclaracion = (Declaracion)paramObject;
    if (localDeclaracion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDeclaracion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DeclaracionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DeclaracionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DeclaracionPK))
      throw new IllegalArgumentException("arg1");
    DeclaracionPK localDeclaracionPK = (DeclaracionPK)paramObject;
    localDeclaracionPK.idDeclaracion = this.idDeclaracion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DeclaracionPK))
      throw new IllegalArgumentException("arg1");
    DeclaracionPK localDeclaracionPK = (DeclaracionPK)paramObject;
    this.idDeclaracion = localDeclaracionPK.idDeclaracion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DeclaracionPK))
      throw new IllegalArgumentException("arg2");
    DeclaracionPK localDeclaracionPK = (DeclaracionPK)paramObject;
    localDeclaracionPK.idDeclaracion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DeclaracionPK))
      throw new IllegalArgumentException("arg2");
    DeclaracionPK localDeclaracionPK = (DeclaracionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localDeclaracionPK.idDeclaracion);
  }

  private static final String jdoGetdescargo(Declaracion paramDeclaracion)
  {
    if (paramDeclaracion.jdoFlags <= 0)
      return paramDeclaracion.descargo;
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
      return paramDeclaracion.descargo;
    if (localStateManager.isLoaded(paramDeclaracion, jdoInheritedFieldCount + 0))
      return paramDeclaracion.descargo;
    return localStateManager.getStringField(paramDeclaracion, jdoInheritedFieldCount + 0, paramDeclaracion.descargo);
  }

  private static final void jdoSetdescargo(Declaracion paramDeclaracion, String paramString)
  {
    if (paramDeclaracion.jdoFlags == 0)
    {
      paramDeclaracion.descargo = paramString;
      return;
    }
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
    {
      paramDeclaracion.descargo = paramString;
      return;
    }
    localStateManager.setStringField(paramDeclaracion, jdoInheritedFieldCount + 0, paramDeclaracion.descargo, paramString);
  }

  private static final Date jdoGetfechaRegistro(Declaracion paramDeclaracion)
  {
    if (paramDeclaracion.jdoFlags <= 0)
      return paramDeclaracion.fechaRegistro;
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
      return paramDeclaracion.fechaRegistro;
    if (localStateManager.isLoaded(paramDeclaracion, jdoInheritedFieldCount + 1))
      return paramDeclaracion.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramDeclaracion, jdoInheritedFieldCount + 1, paramDeclaracion.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(Declaracion paramDeclaracion, Date paramDate)
  {
    if (paramDeclaracion.jdoFlags == 0)
    {
      paramDeclaracion.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
    {
      paramDeclaracion.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramDeclaracion, jdoInheritedFieldCount + 1, paramDeclaracion.fechaRegistro, paramDate);
  }

  private static final long jdoGetidDeclaracion(Declaracion paramDeclaracion)
  {
    return paramDeclaracion.idDeclaracion;
  }

  private static final void jdoSetidDeclaracion(Declaracion paramDeclaracion, long paramLong)
  {
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
    {
      paramDeclaracion.idDeclaracion = paramLong;
      return;
    }
    localStateManager.setLongField(paramDeclaracion, jdoInheritedFieldCount + 2, paramDeclaracion.idDeclaracion, paramLong);
  }

  private static final int jdoGetidSitp(Declaracion paramDeclaracion)
  {
    if (paramDeclaracion.jdoFlags <= 0)
      return paramDeclaracion.idSitp;
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
      return paramDeclaracion.idSitp;
    if (localStateManager.isLoaded(paramDeclaracion, jdoInheritedFieldCount + 3))
      return paramDeclaracion.idSitp;
    return localStateManager.getIntField(paramDeclaracion, jdoInheritedFieldCount + 3, paramDeclaracion.idSitp);
  }

  private static final void jdoSetidSitp(Declaracion paramDeclaracion, int paramInt)
  {
    if (paramDeclaracion.jdoFlags == 0)
    {
      paramDeclaracion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
    {
      paramDeclaracion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramDeclaracion, jdoInheritedFieldCount + 3, paramDeclaracion.idSitp, paramInt);
  }

  private static final String jdoGetobservaciones(Declaracion paramDeclaracion)
  {
    if (paramDeclaracion.jdoFlags <= 0)
      return paramDeclaracion.observaciones;
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
      return paramDeclaracion.observaciones;
    if (localStateManager.isLoaded(paramDeclaracion, jdoInheritedFieldCount + 4))
      return paramDeclaracion.observaciones;
    return localStateManager.getStringField(paramDeclaracion, jdoInheritedFieldCount + 4, paramDeclaracion.observaciones);
  }

  private static final void jdoSetobservaciones(Declaracion paramDeclaracion, String paramString)
  {
    if (paramDeclaracion.jdoFlags == 0)
    {
      paramDeclaracion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
    {
      paramDeclaracion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramDeclaracion, jdoInheritedFieldCount + 4, paramDeclaracion.observaciones, paramString);
  }

  private static final Organismo jdoGetorganismo(Declaracion paramDeclaracion)
  {
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
      return paramDeclaracion.organismo;
    if (localStateManager.isLoaded(paramDeclaracion, jdoInheritedFieldCount + 5))
      return paramDeclaracion.organismo;
    return (Organismo)localStateManager.getObjectField(paramDeclaracion, jdoInheritedFieldCount + 5, paramDeclaracion.organismo);
  }

  private static final void jdoSetorganismo(Declaracion paramDeclaracion, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
    {
      paramDeclaracion.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramDeclaracion, jdoInheritedFieldCount + 5, paramDeclaracion.organismo, paramOrganismo);
  }

  private static final Personal jdoGetpersonal(Declaracion paramDeclaracion)
  {
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
      return paramDeclaracion.personal;
    if (localStateManager.isLoaded(paramDeclaracion, jdoInheritedFieldCount + 6))
      return paramDeclaracion.personal;
    return (Personal)localStateManager.getObjectField(paramDeclaracion, jdoInheritedFieldCount + 6, paramDeclaracion.personal);
  }

  private static final void jdoSetpersonal(Declaracion paramDeclaracion, Personal paramPersonal)
  {
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
    {
      paramDeclaracion.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramDeclaracion, jdoInheritedFieldCount + 6, paramDeclaracion.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(Declaracion paramDeclaracion)
  {
    if (paramDeclaracion.jdoFlags <= 0)
      return paramDeclaracion.tiempoSitp;
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
      return paramDeclaracion.tiempoSitp;
    if (localStateManager.isLoaded(paramDeclaracion, jdoInheritedFieldCount + 7))
      return paramDeclaracion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramDeclaracion, jdoInheritedFieldCount + 7, paramDeclaracion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Declaracion paramDeclaracion, Date paramDate)
  {
    if (paramDeclaracion.jdoFlags == 0)
    {
      paramDeclaracion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramDeclaracion.jdoStateManager;
    if (localStateManager == null)
    {
      paramDeclaracion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramDeclaracion, jdoInheritedFieldCount + 7, paramDeclaracion.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}