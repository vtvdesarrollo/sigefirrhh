package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class VacacionDisfrutada
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idVacacionDisfrutada;
  private TipoPersonal tipoPersonal;
  private int anio;
  private Date fechaInicio;
  private Date fechaFin;
  private Date fechaReintegro;
  private int semanaAnio;
  private int diasDisfrute;
  private String observaciones;
  private Personal personal;
  private Vacacion vacacion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "diasDisfrute", "fechaFin", "fechaInicio", "fechaReintegro", "idVacacionDisfrutada", "observaciones", "personal", "semanaAnio", "tipoPersonal", "vacacion" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.expediente.Vacacion") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 26, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.VacacionDisfrutada"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new VacacionDisfrutada());

    LISTA_SI_NO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public VacacionDisfrutada()
  {
    jdoSetanio(this, 0);

    jdoSetsemanaAnio(this, 0);

    jdoSetdiasDisfrute(this, 0);
  }

  public String toString()
  {
    return jdoGetanio(this) + " desde " + new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this)) + " hasta " + new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaFin(this));
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public int getDiasDisfrute() {
    return jdoGetdiasDisfrute(this);
  }
  public void setDiasDisfrute(int diasDisfrute) {
    jdoSetdiasDisfrute(this, diasDisfrute);
  }
  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }
  public void setFechaFin(Date fechaFin) {
    jdoSetfechaFin(this, fechaFin);
  }
  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }
  public void setFechaInicio(Date fechaInicio) {
    jdoSetfechaInicio(this, fechaInicio);
  }
  public Date getFechaReintegro() {
    return jdoGetfechaReintegro(this);
  }
  public void setFechaReintegro(Date fechaReintegro) {
    jdoSetfechaReintegro(this, fechaReintegro);
  }
  public long getIdVacacionDisfrutada() {
    return jdoGetidVacacionDisfrutada(this);
  }
  public void setIdVacacionDisfrutada(long idVacacionDisfrutada) {
    jdoSetidVacacionDisfrutada(this, idVacacionDisfrutada);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }
  public Vacacion getVacacion() {
    return jdoGetvacacion(this);
  }
  public void setVacacion(Vacacion vacacion) {
    jdoSetvacacion(this, vacacion);
  }

  public int getSemanaAnio() {
    return jdoGetsemanaAnio(this);
  }
  public void setSemanaAnio(int semanaAnio) {
    jdoSetsemanaAnio(this, semanaAnio);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    VacacionDisfrutada localVacacionDisfrutada = new VacacionDisfrutada();
    localVacacionDisfrutada.jdoFlags = 1;
    localVacacionDisfrutada.jdoStateManager = paramStateManager;
    return localVacacionDisfrutada;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    VacacionDisfrutada localVacacionDisfrutada = new VacacionDisfrutada();
    localVacacionDisfrutada.jdoCopyKeyFieldsFromObjectId(paramObject);
    localVacacionDisfrutada.jdoFlags = 1;
    localVacacionDisfrutada.jdoStateManager = paramStateManager;
    return localVacacionDisfrutada;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasDisfrute);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaReintegro);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idVacacionDisfrutada);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanaAnio);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.vacacion);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasDisfrute = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaReintegro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idVacacionDisfrutada = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanaAnio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vacacion = ((Vacacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(VacacionDisfrutada paramVacacionDisfrutada, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramVacacionDisfrutada == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramVacacionDisfrutada.anio;
      return;
    case 1:
      if (paramVacacionDisfrutada == null)
        throw new IllegalArgumentException("arg1");
      this.diasDisfrute = paramVacacionDisfrutada.diasDisfrute;
      return;
    case 2:
      if (paramVacacionDisfrutada == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramVacacionDisfrutada.fechaFin;
      return;
    case 3:
      if (paramVacacionDisfrutada == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramVacacionDisfrutada.fechaInicio;
      return;
    case 4:
      if (paramVacacionDisfrutada == null)
        throw new IllegalArgumentException("arg1");
      this.fechaReintegro = paramVacacionDisfrutada.fechaReintegro;
      return;
    case 5:
      if (paramVacacionDisfrutada == null)
        throw new IllegalArgumentException("arg1");
      this.idVacacionDisfrutada = paramVacacionDisfrutada.idVacacionDisfrutada;
      return;
    case 6:
      if (paramVacacionDisfrutada == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramVacacionDisfrutada.observaciones;
      return;
    case 7:
      if (paramVacacionDisfrutada == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramVacacionDisfrutada.personal;
      return;
    case 8:
      if (paramVacacionDisfrutada == null)
        throw new IllegalArgumentException("arg1");
      this.semanaAnio = paramVacacionDisfrutada.semanaAnio;
      return;
    case 9:
      if (paramVacacionDisfrutada == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramVacacionDisfrutada.tipoPersonal;
      return;
    case 10:
      if (paramVacacionDisfrutada == null)
        throw new IllegalArgumentException("arg1");
      this.vacacion = paramVacacionDisfrutada.vacacion;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof VacacionDisfrutada))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    VacacionDisfrutada localVacacionDisfrutada = (VacacionDisfrutada)paramObject;
    if (localVacacionDisfrutada.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localVacacionDisfrutada, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new VacacionDisfrutadaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new VacacionDisfrutadaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VacacionDisfrutadaPK))
      throw new IllegalArgumentException("arg1");
    VacacionDisfrutadaPK localVacacionDisfrutadaPK = (VacacionDisfrutadaPK)paramObject;
    localVacacionDisfrutadaPK.idVacacionDisfrutada = this.idVacacionDisfrutada;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VacacionDisfrutadaPK))
      throw new IllegalArgumentException("arg1");
    VacacionDisfrutadaPK localVacacionDisfrutadaPK = (VacacionDisfrutadaPK)paramObject;
    this.idVacacionDisfrutada = localVacacionDisfrutadaPK.idVacacionDisfrutada;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VacacionDisfrutadaPK))
      throw new IllegalArgumentException("arg2");
    VacacionDisfrutadaPK localVacacionDisfrutadaPK = (VacacionDisfrutadaPK)paramObject;
    localVacacionDisfrutadaPK.idVacacionDisfrutada = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VacacionDisfrutadaPK))
      throw new IllegalArgumentException("arg2");
    VacacionDisfrutadaPK localVacacionDisfrutadaPK = (VacacionDisfrutadaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localVacacionDisfrutadaPK.idVacacionDisfrutada);
  }

  private static final int jdoGetanio(VacacionDisfrutada paramVacacionDisfrutada)
  {
    if (paramVacacionDisfrutada.jdoFlags <= 0)
      return paramVacacionDisfrutada.anio;
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionDisfrutada.anio;
    if (localStateManager.isLoaded(paramVacacionDisfrutada, jdoInheritedFieldCount + 0))
      return paramVacacionDisfrutada.anio;
    return localStateManager.getIntField(paramVacacionDisfrutada, jdoInheritedFieldCount + 0, paramVacacionDisfrutada.anio);
  }

  private static final void jdoSetanio(VacacionDisfrutada paramVacacionDisfrutada, int paramInt)
  {
    if (paramVacacionDisfrutada.jdoFlags == 0)
    {
      paramVacacionDisfrutada.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionDisfrutada.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacionDisfrutada, jdoInheritedFieldCount + 0, paramVacacionDisfrutada.anio, paramInt);
  }

  private static final int jdoGetdiasDisfrute(VacacionDisfrutada paramVacacionDisfrutada)
  {
    if (paramVacacionDisfrutada.jdoFlags <= 0)
      return paramVacacionDisfrutada.diasDisfrute;
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionDisfrutada.diasDisfrute;
    if (localStateManager.isLoaded(paramVacacionDisfrutada, jdoInheritedFieldCount + 1))
      return paramVacacionDisfrutada.diasDisfrute;
    return localStateManager.getIntField(paramVacacionDisfrutada, jdoInheritedFieldCount + 1, paramVacacionDisfrutada.diasDisfrute);
  }

  private static final void jdoSetdiasDisfrute(VacacionDisfrutada paramVacacionDisfrutada, int paramInt)
  {
    if (paramVacacionDisfrutada.jdoFlags == 0)
    {
      paramVacacionDisfrutada.diasDisfrute = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionDisfrutada.diasDisfrute = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacionDisfrutada, jdoInheritedFieldCount + 1, paramVacacionDisfrutada.diasDisfrute, paramInt);
  }

  private static final Date jdoGetfechaFin(VacacionDisfrutada paramVacacionDisfrutada)
  {
    if (paramVacacionDisfrutada.jdoFlags <= 0)
      return paramVacacionDisfrutada.fechaFin;
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionDisfrutada.fechaFin;
    if (localStateManager.isLoaded(paramVacacionDisfrutada, jdoInheritedFieldCount + 2))
      return paramVacacionDisfrutada.fechaFin;
    return (Date)localStateManager.getObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 2, paramVacacionDisfrutada.fechaFin);
  }

  private static final void jdoSetfechaFin(VacacionDisfrutada paramVacacionDisfrutada, Date paramDate)
  {
    if (paramVacacionDisfrutada.jdoFlags == 0)
    {
      paramVacacionDisfrutada.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionDisfrutada.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 2, paramVacacionDisfrutada.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(VacacionDisfrutada paramVacacionDisfrutada)
  {
    if (paramVacacionDisfrutada.jdoFlags <= 0)
      return paramVacacionDisfrutada.fechaInicio;
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionDisfrutada.fechaInicio;
    if (localStateManager.isLoaded(paramVacacionDisfrutada, jdoInheritedFieldCount + 3))
      return paramVacacionDisfrutada.fechaInicio;
    return (Date)localStateManager.getObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 3, paramVacacionDisfrutada.fechaInicio);
  }

  private static final void jdoSetfechaInicio(VacacionDisfrutada paramVacacionDisfrutada, Date paramDate)
  {
    if (paramVacacionDisfrutada.jdoFlags == 0)
    {
      paramVacacionDisfrutada.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionDisfrutada.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 3, paramVacacionDisfrutada.fechaInicio, paramDate);
  }

  private static final Date jdoGetfechaReintegro(VacacionDisfrutada paramVacacionDisfrutada)
  {
    if (paramVacacionDisfrutada.jdoFlags <= 0)
      return paramVacacionDisfrutada.fechaReintegro;
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionDisfrutada.fechaReintegro;
    if (localStateManager.isLoaded(paramVacacionDisfrutada, jdoInheritedFieldCount + 4))
      return paramVacacionDisfrutada.fechaReintegro;
    return (Date)localStateManager.getObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 4, paramVacacionDisfrutada.fechaReintegro);
  }

  private static final void jdoSetfechaReintegro(VacacionDisfrutada paramVacacionDisfrutada, Date paramDate)
  {
    if (paramVacacionDisfrutada.jdoFlags == 0)
    {
      paramVacacionDisfrutada.fechaReintegro = paramDate;
      return;
    }
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionDisfrutada.fechaReintegro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 4, paramVacacionDisfrutada.fechaReintegro, paramDate);
  }

  private static final long jdoGetidVacacionDisfrutada(VacacionDisfrutada paramVacacionDisfrutada)
  {
    return paramVacacionDisfrutada.idVacacionDisfrutada;
  }

  private static final void jdoSetidVacacionDisfrutada(VacacionDisfrutada paramVacacionDisfrutada, long paramLong)
  {
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionDisfrutada.idVacacionDisfrutada = paramLong;
      return;
    }
    localStateManager.setLongField(paramVacacionDisfrutada, jdoInheritedFieldCount + 5, paramVacacionDisfrutada.idVacacionDisfrutada, paramLong);
  }

  private static final String jdoGetobservaciones(VacacionDisfrutada paramVacacionDisfrutada)
  {
    if (paramVacacionDisfrutada.jdoFlags <= 0)
      return paramVacacionDisfrutada.observaciones;
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionDisfrutada.observaciones;
    if (localStateManager.isLoaded(paramVacacionDisfrutada, jdoInheritedFieldCount + 6))
      return paramVacacionDisfrutada.observaciones;
    return localStateManager.getStringField(paramVacacionDisfrutada, jdoInheritedFieldCount + 6, paramVacacionDisfrutada.observaciones);
  }

  private static final void jdoSetobservaciones(VacacionDisfrutada paramVacacionDisfrutada, String paramString)
  {
    if (paramVacacionDisfrutada.jdoFlags == 0)
    {
      paramVacacionDisfrutada.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionDisfrutada.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramVacacionDisfrutada, jdoInheritedFieldCount + 6, paramVacacionDisfrutada.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(VacacionDisfrutada paramVacacionDisfrutada)
  {
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionDisfrutada.personal;
    if (localStateManager.isLoaded(paramVacacionDisfrutada, jdoInheritedFieldCount + 7))
      return paramVacacionDisfrutada.personal;
    return (Personal)localStateManager.getObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 7, paramVacacionDisfrutada.personal);
  }

  private static final void jdoSetpersonal(VacacionDisfrutada paramVacacionDisfrutada, Personal paramPersonal)
  {
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionDisfrutada.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 7, paramVacacionDisfrutada.personal, paramPersonal);
  }

  private static final int jdoGetsemanaAnio(VacacionDisfrutada paramVacacionDisfrutada)
  {
    if (paramVacacionDisfrutada.jdoFlags <= 0)
      return paramVacacionDisfrutada.semanaAnio;
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionDisfrutada.semanaAnio;
    if (localStateManager.isLoaded(paramVacacionDisfrutada, jdoInheritedFieldCount + 8))
      return paramVacacionDisfrutada.semanaAnio;
    return localStateManager.getIntField(paramVacacionDisfrutada, jdoInheritedFieldCount + 8, paramVacacionDisfrutada.semanaAnio);
  }

  private static final void jdoSetsemanaAnio(VacacionDisfrutada paramVacacionDisfrutada, int paramInt)
  {
    if (paramVacacionDisfrutada.jdoFlags == 0)
    {
      paramVacacionDisfrutada.semanaAnio = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionDisfrutada.semanaAnio = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacionDisfrutada, jdoInheritedFieldCount + 8, paramVacacionDisfrutada.semanaAnio, paramInt);
  }

  private static final TipoPersonal jdoGettipoPersonal(VacacionDisfrutada paramVacacionDisfrutada)
  {
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionDisfrutada.tipoPersonal;
    if (localStateManager.isLoaded(paramVacacionDisfrutada, jdoInheritedFieldCount + 9))
      return paramVacacionDisfrutada.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 9, paramVacacionDisfrutada.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(VacacionDisfrutada paramVacacionDisfrutada, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionDisfrutada.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 9, paramVacacionDisfrutada.tipoPersonal, paramTipoPersonal);
  }

  private static final Vacacion jdoGetvacacion(VacacionDisfrutada paramVacacionDisfrutada)
  {
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionDisfrutada.vacacion;
    if (localStateManager.isLoaded(paramVacacionDisfrutada, jdoInheritedFieldCount + 10))
      return paramVacacionDisfrutada.vacacion;
    return (Vacacion)localStateManager.getObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 10, paramVacacionDisfrutada.vacacion);
  }

  private static final void jdoSetvacacion(VacacionDisfrutada paramVacacionDisfrutada, Vacacion paramVacacion)
  {
    StateManager localStateManager = paramVacacionDisfrutada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionDisfrutada.vacacion = paramVacacion;
      return;
    }
    localStateManager.setObjectField(paramVacacionDisfrutada, jdoInheritedFieldCount + 10, paramVacacionDisfrutada.vacacion, paramVacacion);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}