package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.Carrera;
import sigefirrhh.base.personal.CarreraBeanBusiness;

public class ActividadDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addActividadDocente(ActividadDocente actividadDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ActividadDocente actividadDocenteNew = 
      (ActividadDocente)BeanUtils.cloneBean(
      actividadDocente);

    CarreraBeanBusiness carreraBeanBusiness = new CarreraBeanBusiness();

    if (actividadDocenteNew.getCarrera() != null) {
      actividadDocenteNew.setCarrera(
        carreraBeanBusiness.findCarreraById(
        actividadDocenteNew.getCarrera().getIdCarrera()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (actividadDocenteNew.getPersonal() != null) {
      actividadDocenteNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        actividadDocenteNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(actividadDocenteNew);
  }

  public void updateActividadDocente(ActividadDocente actividadDocente) throws Exception
  {
    ActividadDocente actividadDocenteModify = 
      findActividadDocenteById(actividadDocente.getIdActividadDocente());

    CarreraBeanBusiness carreraBeanBusiness = new CarreraBeanBusiness();

    if (actividadDocente.getCarrera() != null) {
      actividadDocente.setCarrera(
        carreraBeanBusiness.findCarreraById(
        actividadDocente.getCarrera().getIdCarrera()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (actividadDocente.getPersonal() != null) {
      actividadDocente.setPersonal(
        personalBeanBusiness.findPersonalById(
        actividadDocente.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(actividadDocenteModify, actividadDocente);
  }

  public void deleteActividadDocente(ActividadDocente actividadDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ActividadDocente actividadDocenteDelete = 
      findActividadDocenteById(actividadDocente.getIdActividadDocente());
    pm.deletePersistent(actividadDocenteDelete);
  }

  public ActividadDocente findActividadDocenteById(long idActividadDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idActividadDocente == pIdActividadDocente";
    Query query = pm.newQuery(ActividadDocente.class, filter);

    query.declareParameters("long pIdActividadDocente");

    parameters.put("pIdActividadDocente", new Long(idActividadDocente));

    Collection colActividadDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colActividadDocente.iterator();
    return (ActividadDocente)iterator.next();
  }

  public Collection findActividadDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent actividadDocenteExtent = pm.getExtent(
      ActividadDocente.class, true);
    Query query = pm.newQuery(actividadDocenteExtent);
    query.setOrdering("anioInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(ActividadDocente.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anioInicio ascending");

    Collection colActividadDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colActividadDocente);

    return colActividadDocente;
  }
}