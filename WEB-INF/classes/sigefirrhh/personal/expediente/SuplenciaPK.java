package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class SuplenciaPK
  implements Serializable
{
  public long idSuplencia;

  public SuplenciaPK()
  {
  }

  public SuplenciaPK(long idSuplencia)
  {
    this.idSuplencia = idSuplencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SuplenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SuplenciaPK thatPK)
  {
    return 
      this.idSuplencia == thatPK.idSuplencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSuplencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSuplencia);
  }
}