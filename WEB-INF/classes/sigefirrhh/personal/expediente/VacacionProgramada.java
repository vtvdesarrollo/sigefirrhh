package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class VacacionProgramada
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idVacacionProgramada;
  private TipoPersonal tipoPersonal;
  private int anio;
  private Date fechaInicio;
  private Date fechaFin;
  private Date fechaReintegro;
  private int semanaAnio;
  private int diasDisfrute;
  private String observaciones;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "diasDisfrute", "fechaFin", "fechaInicio", "fechaReintegro", "idVacacionProgramada", "observaciones", "personal", "semanaAnio", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 26, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.VacacionProgramada"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new VacacionProgramada());

    LISTA_SI_NO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public VacacionProgramada()
  {
    jdoSetanio(this, 0);

    jdoSetsemanaAnio(this, 0);

    jdoSetdiasDisfrute(this, 0);
  }

  public String toString()
  {
    return jdoGetanio(this) + " desde" + new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this)) + " hasta " + new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaFin(this));
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public int getDiasDisfrute() {
    return jdoGetdiasDisfrute(this);
  }
  public void setDiasDisfrute(int diasDisfrute) {
    jdoSetdiasDisfrute(this, diasDisfrute);
  }
  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }
  public void setFechaFin(Date fechaFin) {
    jdoSetfechaFin(this, fechaFin);
  }
  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }
  public void setFechaInicio(Date fechaInicio) {
    jdoSetfechaInicio(this, fechaInicio);
  }
  public Date getFechaReintegro() {
    return jdoGetfechaReintegro(this);
  }
  public void setFechaReintegro(Date fechaReintegro) {
    jdoSetfechaReintegro(this, fechaReintegro);
  }
  public long getIdVacacionProgramada() {
    return jdoGetidVacacionProgramada(this);
  }
  public void setIdVacacionProgramada(long idVacacionProgramada) {
    jdoSetidVacacionProgramada(this, idVacacionProgramada);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }
  public int getSemanaAnio() {
    return jdoGetsemanaAnio(this);
  }
  public void setSemanaAnio(int semanaAnio) {
    jdoSetsemanaAnio(this, semanaAnio);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    VacacionProgramada localVacacionProgramada = new VacacionProgramada();
    localVacacionProgramada.jdoFlags = 1;
    localVacacionProgramada.jdoStateManager = paramStateManager;
    return localVacacionProgramada;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    VacacionProgramada localVacacionProgramada = new VacacionProgramada();
    localVacacionProgramada.jdoCopyKeyFieldsFromObjectId(paramObject);
    localVacacionProgramada.jdoFlags = 1;
    localVacacionProgramada.jdoStateManager = paramStateManager;
    return localVacacionProgramada;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasDisfrute);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaReintegro);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idVacacionProgramada);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanaAnio);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasDisfrute = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaReintegro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idVacacionProgramada = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanaAnio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(VacacionProgramada paramVacacionProgramada, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramVacacionProgramada == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramVacacionProgramada.anio;
      return;
    case 1:
      if (paramVacacionProgramada == null)
        throw new IllegalArgumentException("arg1");
      this.diasDisfrute = paramVacacionProgramada.diasDisfrute;
      return;
    case 2:
      if (paramVacacionProgramada == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramVacacionProgramada.fechaFin;
      return;
    case 3:
      if (paramVacacionProgramada == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramVacacionProgramada.fechaInicio;
      return;
    case 4:
      if (paramVacacionProgramada == null)
        throw new IllegalArgumentException("arg1");
      this.fechaReintegro = paramVacacionProgramada.fechaReintegro;
      return;
    case 5:
      if (paramVacacionProgramada == null)
        throw new IllegalArgumentException("arg1");
      this.idVacacionProgramada = paramVacacionProgramada.idVacacionProgramada;
      return;
    case 6:
      if (paramVacacionProgramada == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramVacacionProgramada.observaciones;
      return;
    case 7:
      if (paramVacacionProgramada == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramVacacionProgramada.personal;
      return;
    case 8:
      if (paramVacacionProgramada == null)
        throw new IllegalArgumentException("arg1");
      this.semanaAnio = paramVacacionProgramada.semanaAnio;
      return;
    case 9:
      if (paramVacacionProgramada == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramVacacionProgramada.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof VacacionProgramada))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    VacacionProgramada localVacacionProgramada = (VacacionProgramada)paramObject;
    if (localVacacionProgramada.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localVacacionProgramada, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new VacacionProgramadaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new VacacionProgramadaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VacacionProgramadaPK))
      throw new IllegalArgumentException("arg1");
    VacacionProgramadaPK localVacacionProgramadaPK = (VacacionProgramadaPK)paramObject;
    localVacacionProgramadaPK.idVacacionProgramada = this.idVacacionProgramada;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VacacionProgramadaPK))
      throw new IllegalArgumentException("arg1");
    VacacionProgramadaPK localVacacionProgramadaPK = (VacacionProgramadaPK)paramObject;
    this.idVacacionProgramada = localVacacionProgramadaPK.idVacacionProgramada;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VacacionProgramadaPK))
      throw new IllegalArgumentException("arg2");
    VacacionProgramadaPK localVacacionProgramadaPK = (VacacionProgramadaPK)paramObject;
    localVacacionProgramadaPK.idVacacionProgramada = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VacacionProgramadaPK))
      throw new IllegalArgumentException("arg2");
    VacacionProgramadaPK localVacacionProgramadaPK = (VacacionProgramadaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localVacacionProgramadaPK.idVacacionProgramada);
  }

  private static final int jdoGetanio(VacacionProgramada paramVacacionProgramada)
  {
    if (paramVacacionProgramada.jdoFlags <= 0)
      return paramVacacionProgramada.anio;
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionProgramada.anio;
    if (localStateManager.isLoaded(paramVacacionProgramada, jdoInheritedFieldCount + 0))
      return paramVacacionProgramada.anio;
    return localStateManager.getIntField(paramVacacionProgramada, jdoInheritedFieldCount + 0, paramVacacionProgramada.anio);
  }

  private static final void jdoSetanio(VacacionProgramada paramVacacionProgramada, int paramInt)
  {
    if (paramVacacionProgramada.jdoFlags == 0)
    {
      paramVacacionProgramada.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionProgramada.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacionProgramada, jdoInheritedFieldCount + 0, paramVacacionProgramada.anio, paramInt);
  }

  private static final int jdoGetdiasDisfrute(VacacionProgramada paramVacacionProgramada)
  {
    if (paramVacacionProgramada.jdoFlags <= 0)
      return paramVacacionProgramada.diasDisfrute;
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionProgramada.diasDisfrute;
    if (localStateManager.isLoaded(paramVacacionProgramada, jdoInheritedFieldCount + 1))
      return paramVacacionProgramada.diasDisfrute;
    return localStateManager.getIntField(paramVacacionProgramada, jdoInheritedFieldCount + 1, paramVacacionProgramada.diasDisfrute);
  }

  private static final void jdoSetdiasDisfrute(VacacionProgramada paramVacacionProgramada, int paramInt)
  {
    if (paramVacacionProgramada.jdoFlags == 0)
    {
      paramVacacionProgramada.diasDisfrute = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionProgramada.diasDisfrute = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacionProgramada, jdoInheritedFieldCount + 1, paramVacacionProgramada.diasDisfrute, paramInt);
  }

  private static final Date jdoGetfechaFin(VacacionProgramada paramVacacionProgramada)
  {
    if (paramVacacionProgramada.jdoFlags <= 0)
      return paramVacacionProgramada.fechaFin;
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionProgramada.fechaFin;
    if (localStateManager.isLoaded(paramVacacionProgramada, jdoInheritedFieldCount + 2))
      return paramVacacionProgramada.fechaFin;
    return (Date)localStateManager.getObjectField(paramVacacionProgramada, jdoInheritedFieldCount + 2, paramVacacionProgramada.fechaFin);
  }

  private static final void jdoSetfechaFin(VacacionProgramada paramVacacionProgramada, Date paramDate)
  {
    if (paramVacacionProgramada.jdoFlags == 0)
    {
      paramVacacionProgramada.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionProgramada.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramVacacionProgramada, jdoInheritedFieldCount + 2, paramVacacionProgramada.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(VacacionProgramada paramVacacionProgramada)
  {
    if (paramVacacionProgramada.jdoFlags <= 0)
      return paramVacacionProgramada.fechaInicio;
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionProgramada.fechaInicio;
    if (localStateManager.isLoaded(paramVacacionProgramada, jdoInheritedFieldCount + 3))
      return paramVacacionProgramada.fechaInicio;
    return (Date)localStateManager.getObjectField(paramVacacionProgramada, jdoInheritedFieldCount + 3, paramVacacionProgramada.fechaInicio);
  }

  private static final void jdoSetfechaInicio(VacacionProgramada paramVacacionProgramada, Date paramDate)
  {
    if (paramVacacionProgramada.jdoFlags == 0)
    {
      paramVacacionProgramada.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionProgramada.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramVacacionProgramada, jdoInheritedFieldCount + 3, paramVacacionProgramada.fechaInicio, paramDate);
  }

  private static final Date jdoGetfechaReintegro(VacacionProgramada paramVacacionProgramada)
  {
    if (paramVacacionProgramada.jdoFlags <= 0)
      return paramVacacionProgramada.fechaReintegro;
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionProgramada.fechaReintegro;
    if (localStateManager.isLoaded(paramVacacionProgramada, jdoInheritedFieldCount + 4))
      return paramVacacionProgramada.fechaReintegro;
    return (Date)localStateManager.getObjectField(paramVacacionProgramada, jdoInheritedFieldCount + 4, paramVacacionProgramada.fechaReintegro);
  }

  private static final void jdoSetfechaReintegro(VacacionProgramada paramVacacionProgramada, Date paramDate)
  {
    if (paramVacacionProgramada.jdoFlags == 0)
    {
      paramVacacionProgramada.fechaReintegro = paramDate;
      return;
    }
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionProgramada.fechaReintegro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramVacacionProgramada, jdoInheritedFieldCount + 4, paramVacacionProgramada.fechaReintegro, paramDate);
  }

  private static final long jdoGetidVacacionProgramada(VacacionProgramada paramVacacionProgramada)
  {
    return paramVacacionProgramada.idVacacionProgramada;
  }

  private static final void jdoSetidVacacionProgramada(VacacionProgramada paramVacacionProgramada, long paramLong)
  {
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionProgramada.idVacacionProgramada = paramLong;
      return;
    }
    localStateManager.setLongField(paramVacacionProgramada, jdoInheritedFieldCount + 5, paramVacacionProgramada.idVacacionProgramada, paramLong);
  }

  private static final String jdoGetobservaciones(VacacionProgramada paramVacacionProgramada)
  {
    if (paramVacacionProgramada.jdoFlags <= 0)
      return paramVacacionProgramada.observaciones;
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionProgramada.observaciones;
    if (localStateManager.isLoaded(paramVacacionProgramada, jdoInheritedFieldCount + 6))
      return paramVacacionProgramada.observaciones;
    return localStateManager.getStringField(paramVacacionProgramada, jdoInheritedFieldCount + 6, paramVacacionProgramada.observaciones);
  }

  private static final void jdoSetobservaciones(VacacionProgramada paramVacacionProgramada, String paramString)
  {
    if (paramVacacionProgramada.jdoFlags == 0)
    {
      paramVacacionProgramada.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionProgramada.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramVacacionProgramada, jdoInheritedFieldCount + 6, paramVacacionProgramada.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(VacacionProgramada paramVacacionProgramada)
  {
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionProgramada.personal;
    if (localStateManager.isLoaded(paramVacacionProgramada, jdoInheritedFieldCount + 7))
      return paramVacacionProgramada.personal;
    return (Personal)localStateManager.getObjectField(paramVacacionProgramada, jdoInheritedFieldCount + 7, paramVacacionProgramada.personal);
  }

  private static final void jdoSetpersonal(VacacionProgramada paramVacacionProgramada, Personal paramPersonal)
  {
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionProgramada.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramVacacionProgramada, jdoInheritedFieldCount + 7, paramVacacionProgramada.personal, paramPersonal);
  }

  private static final int jdoGetsemanaAnio(VacacionProgramada paramVacacionProgramada)
  {
    if (paramVacacionProgramada.jdoFlags <= 0)
      return paramVacacionProgramada.semanaAnio;
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionProgramada.semanaAnio;
    if (localStateManager.isLoaded(paramVacacionProgramada, jdoInheritedFieldCount + 8))
      return paramVacacionProgramada.semanaAnio;
    return localStateManager.getIntField(paramVacacionProgramada, jdoInheritedFieldCount + 8, paramVacacionProgramada.semanaAnio);
  }

  private static final void jdoSetsemanaAnio(VacacionProgramada paramVacacionProgramada, int paramInt)
  {
    if (paramVacacionProgramada.jdoFlags == 0)
    {
      paramVacacionProgramada.semanaAnio = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionProgramada.semanaAnio = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacionProgramada, jdoInheritedFieldCount + 8, paramVacacionProgramada.semanaAnio, paramInt);
  }

  private static final TipoPersonal jdoGettipoPersonal(VacacionProgramada paramVacacionProgramada)
  {
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
      return paramVacacionProgramada.tipoPersonal;
    if (localStateManager.isLoaded(paramVacacionProgramada, jdoInheritedFieldCount + 9))
      return paramVacacionProgramada.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramVacacionProgramada, jdoInheritedFieldCount + 9, paramVacacionProgramada.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(VacacionProgramada paramVacacionProgramada, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramVacacionProgramada.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacionProgramada.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramVacacionProgramada, jdoInheritedFieldCount + 9, paramVacacionProgramada.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}