package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Certificado
  implements Serializable, PersistenceCapable
{
  private long idCertificado;
  private Date fechaEmision;
  private String numero;
  private String libro;
  private String folio;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "fechaEmision", "folio", "idCertificado", "idSitp", "libro", "numero", "personal", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnumero(this) + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaEmision(this));
  }

  public Date getFechaEmision()
  {
    return jdoGetfechaEmision(this);
  }

  public String getFolio()
  {
    return jdoGetfolio(this);
  }

  public long getIdCertificado()
  {
    return jdoGetidCertificado(this);
  }

  public String getLibro()
  {
    return jdoGetlibro(this);
  }

  public String getNumero()
  {
    return jdoGetnumero(this);
  }

  public void setFechaEmision(Date date)
  {
    jdoSetfechaEmision(this, date);
  }

  public void setFolio(String string)
  {
    jdoSetfolio(this, string);
  }

  public void setIdCertificado(long l)
  {
    jdoSetidCertificado(this, l);
  }

  public void setLibro(String string)
  {
    jdoSetlibro(this, string);
  }

  public void setNumero(String string)
  {
    jdoSetnumero(this, string);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Certificado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Certificado());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Certificado localCertificado = new Certificado();
    localCertificado.jdoFlags = 1;
    localCertificado.jdoStateManager = paramStateManager;
    return localCertificado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Certificado localCertificado = new Certificado();
    localCertificado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCertificado.jdoFlags = 1;
    localCertificado.jdoStateManager = paramStateManager;
    return localCertificado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEmision);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.folio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCertificado);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.libro);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numero);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEmision = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.folio = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCertificado = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.libro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numero = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Certificado paramCertificado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCertificado == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEmision = paramCertificado.fechaEmision;
      return;
    case 1:
      if (paramCertificado == null)
        throw new IllegalArgumentException("arg1");
      this.folio = paramCertificado.folio;
      return;
    case 2:
      if (paramCertificado == null)
        throw new IllegalArgumentException("arg1");
      this.idCertificado = paramCertificado.idCertificado;
      return;
    case 3:
      if (paramCertificado == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramCertificado.idSitp;
      return;
    case 4:
      if (paramCertificado == null)
        throw new IllegalArgumentException("arg1");
      this.libro = paramCertificado.libro;
      return;
    case 5:
      if (paramCertificado == null)
        throw new IllegalArgumentException("arg1");
      this.numero = paramCertificado.numero;
      return;
    case 6:
      if (paramCertificado == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramCertificado.personal;
      return;
    case 7:
      if (paramCertificado == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramCertificado.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Certificado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Certificado localCertificado = (Certificado)paramObject;
    if (localCertificado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCertificado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CertificadoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CertificadoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CertificadoPK))
      throw new IllegalArgumentException("arg1");
    CertificadoPK localCertificadoPK = (CertificadoPK)paramObject;
    localCertificadoPK.idCertificado = this.idCertificado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CertificadoPK))
      throw new IllegalArgumentException("arg1");
    CertificadoPK localCertificadoPK = (CertificadoPK)paramObject;
    this.idCertificado = localCertificadoPK.idCertificado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CertificadoPK))
      throw new IllegalArgumentException("arg2");
    CertificadoPK localCertificadoPK = (CertificadoPK)paramObject;
    localCertificadoPK.idCertificado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CertificadoPK))
      throw new IllegalArgumentException("arg2");
    CertificadoPK localCertificadoPK = (CertificadoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localCertificadoPK.idCertificado);
  }

  private static final Date jdoGetfechaEmision(Certificado paramCertificado)
  {
    if (paramCertificado.jdoFlags <= 0)
      return paramCertificado.fechaEmision;
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
      return paramCertificado.fechaEmision;
    if (localStateManager.isLoaded(paramCertificado, jdoInheritedFieldCount + 0))
      return paramCertificado.fechaEmision;
    return (Date)localStateManager.getObjectField(paramCertificado, jdoInheritedFieldCount + 0, paramCertificado.fechaEmision);
  }

  private static final void jdoSetfechaEmision(Certificado paramCertificado, Date paramDate)
  {
    if (paramCertificado.jdoFlags == 0)
    {
      paramCertificado.fechaEmision = paramDate;
      return;
    }
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificado.fechaEmision = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCertificado, jdoInheritedFieldCount + 0, paramCertificado.fechaEmision, paramDate);
  }

  private static final String jdoGetfolio(Certificado paramCertificado)
  {
    if (paramCertificado.jdoFlags <= 0)
      return paramCertificado.folio;
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
      return paramCertificado.folio;
    if (localStateManager.isLoaded(paramCertificado, jdoInheritedFieldCount + 1))
      return paramCertificado.folio;
    return localStateManager.getStringField(paramCertificado, jdoInheritedFieldCount + 1, paramCertificado.folio);
  }

  private static final void jdoSetfolio(Certificado paramCertificado, String paramString)
  {
    if (paramCertificado.jdoFlags == 0)
    {
      paramCertificado.folio = paramString;
      return;
    }
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificado.folio = paramString;
      return;
    }
    localStateManager.setStringField(paramCertificado, jdoInheritedFieldCount + 1, paramCertificado.folio, paramString);
  }

  private static final long jdoGetidCertificado(Certificado paramCertificado)
  {
    return paramCertificado.idCertificado;
  }

  private static final void jdoSetidCertificado(Certificado paramCertificado, long paramLong)
  {
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificado.idCertificado = paramLong;
      return;
    }
    localStateManager.setLongField(paramCertificado, jdoInheritedFieldCount + 2, paramCertificado.idCertificado, paramLong);
  }

  private static final int jdoGetidSitp(Certificado paramCertificado)
  {
    if (paramCertificado.jdoFlags <= 0)
      return paramCertificado.idSitp;
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
      return paramCertificado.idSitp;
    if (localStateManager.isLoaded(paramCertificado, jdoInheritedFieldCount + 3))
      return paramCertificado.idSitp;
    return localStateManager.getIntField(paramCertificado, jdoInheritedFieldCount + 3, paramCertificado.idSitp);
  }

  private static final void jdoSetidSitp(Certificado paramCertificado, int paramInt)
  {
    if (paramCertificado.jdoFlags == 0)
    {
      paramCertificado.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificado.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramCertificado, jdoInheritedFieldCount + 3, paramCertificado.idSitp, paramInt);
  }

  private static final String jdoGetlibro(Certificado paramCertificado)
  {
    if (paramCertificado.jdoFlags <= 0)
      return paramCertificado.libro;
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
      return paramCertificado.libro;
    if (localStateManager.isLoaded(paramCertificado, jdoInheritedFieldCount + 4))
      return paramCertificado.libro;
    return localStateManager.getStringField(paramCertificado, jdoInheritedFieldCount + 4, paramCertificado.libro);
  }

  private static final void jdoSetlibro(Certificado paramCertificado, String paramString)
  {
    if (paramCertificado.jdoFlags == 0)
    {
      paramCertificado.libro = paramString;
      return;
    }
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificado.libro = paramString;
      return;
    }
    localStateManager.setStringField(paramCertificado, jdoInheritedFieldCount + 4, paramCertificado.libro, paramString);
  }

  private static final String jdoGetnumero(Certificado paramCertificado)
  {
    if (paramCertificado.jdoFlags <= 0)
      return paramCertificado.numero;
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
      return paramCertificado.numero;
    if (localStateManager.isLoaded(paramCertificado, jdoInheritedFieldCount + 5))
      return paramCertificado.numero;
    return localStateManager.getStringField(paramCertificado, jdoInheritedFieldCount + 5, paramCertificado.numero);
  }

  private static final void jdoSetnumero(Certificado paramCertificado, String paramString)
  {
    if (paramCertificado.jdoFlags == 0)
    {
      paramCertificado.numero = paramString;
      return;
    }
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificado.numero = paramString;
      return;
    }
    localStateManager.setStringField(paramCertificado, jdoInheritedFieldCount + 5, paramCertificado.numero, paramString);
  }

  private static final Personal jdoGetpersonal(Certificado paramCertificado)
  {
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
      return paramCertificado.personal;
    if (localStateManager.isLoaded(paramCertificado, jdoInheritedFieldCount + 6))
      return paramCertificado.personal;
    return (Personal)localStateManager.getObjectField(paramCertificado, jdoInheritedFieldCount + 6, paramCertificado.personal);
  }

  private static final void jdoSetpersonal(Certificado paramCertificado, Personal paramPersonal)
  {
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificado.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramCertificado, jdoInheritedFieldCount + 6, paramCertificado.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(Certificado paramCertificado)
  {
    if (paramCertificado.jdoFlags <= 0)
      return paramCertificado.tiempoSitp;
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
      return paramCertificado.tiempoSitp;
    if (localStateManager.isLoaded(paramCertificado, jdoInheritedFieldCount + 7))
      return paramCertificado.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramCertificado, jdoInheritedFieldCount + 7, paramCertificado.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Certificado paramCertificado, Date paramDate)
  {
    if (paramCertificado.jdoFlags == 0)
    {
      paramCertificado.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramCertificado.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificado.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCertificado, jdoInheritedFieldCount + 7, paramCertificado.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}