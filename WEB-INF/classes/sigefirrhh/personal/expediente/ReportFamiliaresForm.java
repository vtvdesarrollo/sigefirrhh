package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportFamiliaresForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportFamiliaresForm.class.getName());
  private int reportId;
  private long idRegion;
  private long idTipoPersonal;
  private String reportName;
  private String orden;
  private Date fechaTope;
  private int edadMinima;
  private int edadMaxima;
  private String parentesco;
  private Calendar fechaInicial;
  private Calendar fechaFinal;
  private String formato = "1";
  private Collection listRegion;
  private EstructuraFacade estructuraFacade;
  private DefinicionesFacade definicionesFacade;
  private LoginSession login;
  private Collection listTipoPersonal;
  private String agrupado = "G";

  public ReportFamiliaresForm() { FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesFacade();

    this.fechaTope = Calendar.getInstance().getTime();

    this.reportName = "familiaresalf";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportFamiliaresForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listRegion = this.estructuraFacade.findAllRegion();
      this.listTipoPersonal = this.definicionesFacade.findAllTipoPersonal();
    }
    catch (Exception e) {
      this.listRegion = new ArrayList();
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try {
      if ((this.parentesco != null) && (this.orden != null)) {
        this.reportName = "";
        if (this.formato.equals("2")) {
          this.reportName = "a_";
        }
        if (this.parentesco.equals("0")) {
          if ((this.orden.equals("A")) && (this.idRegion == 0L))
            this.reportName += "familiaresalf";
          else if ((this.orden.equals("C")) && (this.idRegion == 0L))
            this.reportName += "familiaresced";
          else if ((this.orden.equals("O")) && (this.idRegion == 0L)) {
            this.reportName += "familiarescod";
          }
          if ((this.orden.equals("A")) && (this.idRegion != 0L))
            this.reportName += "familiaresalfreg";
          else if ((this.orden.equals("C")) && (this.idRegion != 0L))
            this.reportName += "familiarescedreg";
          else if ((this.orden.equals("O")) && (this.idRegion != 0L))
            this.reportName += "familiarescodreg";
        }
        else {
          if ((this.orden.equals("A")) && (this.idRegion == 0L))
            this.reportName += "familiaresalfp";
          else if ((this.orden.equals("C")) && (this.idRegion == 0L))
            this.reportName += "familiarescedp";
          else if ((this.orden.equals("O")) && (this.idRegion == 0L)) {
            this.reportName += "familiarescodp";
          }
          if ((this.orden.equals("A")) && (this.idRegion != 0L))
            this.reportName += "familiaresalfregp";
          else if ((this.orden.equals("C")) && (this.idRegion != 0L))
            this.reportName += "familiarescedregp";
          else if ((this.orden.equals("O")) && (this.idRegion != 0L)) {
            this.reportName += "familiarescodregp";
          }
        }
        if (this.agrupado.equals("D"))
          this.reportName += "dep";
      }
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "";
      if (this.formato.equals("2")) {
        this.reportName = "a_";
      }
      if (this.parentesco.equals("0")) {
        if ((this.orden.equals("A")) && (this.idRegion == 0L))
          this.reportName += "familiaresalf";
        else if ((this.orden.equals("C")) && (this.idRegion == 0L))
          this.reportName += "familiaresced";
        else if ((this.orden.equals("O")) && (this.idRegion == 0L)) {
          this.reportName += "familiarescod";
        }
        if ((this.orden.equals("A")) && (this.idRegion != 0L))
          this.reportName += "familiaresalfreg";
        else if ((this.orden.equals("C")) && (this.idRegion != 0L))
          this.reportName += "familiarescedreg";
        else if ((this.orden.equals("O")) && (this.idRegion != 0L))
          this.reportName += "familiarescodreg";
      }
      else
      {
        if ((this.orden.equals("A")) && (this.idRegion == 0L))
          this.reportName += "familiaresalfp";
        else if ((this.orden.equals("C")) && (this.idRegion == 0L))
          this.reportName += "familiarescedp";
        else if ((this.orden.equals("O")) && (this.idRegion == 0L)) {
          this.reportName += "familiarescodp";
        }
        if ((this.orden.equals("A")) && (this.idRegion != 0L))
          this.reportName += "familiaresalfregp";
        else if ((this.orden.equals("C")) && (this.idRegion != 0L))
          this.reportName += "familiarescedregp";
        else if ((this.orden.equals("O")) && (this.idRegion != 0L)) {
          this.reportName += "familiarescodregp";
        }
      }
      if (this.agrupado.equals("D")) {
        this.reportName += "dep";
      }
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      if (this.idRegion != 0L) {
        parameters.put("id_region", new Long(this.idRegion));
      }
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      if (!this.parentesco.equals("0"))
      {
        this.fechaInicial = Calendar.getInstance();
        this.fechaInicial.setTime(this.fechaTope);
        this.fechaInicial.add(1, -this.edadMaxima);

        this.fechaInicial.add(5, 1);
        this.fechaFinal = Calendar.getInstance();
        this.fechaFinal.setTime(this.fechaTope);
        this.fechaFinal.add(1, -this.edadMinima);

        this.fechaFinal.add(5, 1);

        log.error("Fechafinal" + this.fechaFinal.getTime());
        log.error("Fechainicial" + this.fechaInicial.getTime());
        log.error("edadminima" + this.edadMinima);
        log.error("edadmaxima" + this.edadMaxima);

        if (this.edadMaxima == 0) {
          this.edadMaxima = 999;
        }

        parameters.put("fec_ini", this.fechaInicial.getTime());
        parameters.put("fec_fin", this.fechaFinal.getTime());
        parameters.put("edad_ini", new Integer(this.edadMinima));
        parameters.put("edad_fin", new Integer(this.edadMaxima));
        parameters.put("parentesco", new String(this.parentesco));
      }

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/expediente");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }
  public Collection getListTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }
  public String getEdadMaxima() {
    return String.valueOf(this.edadMaxima);
  }
  public void setEdadMaxima(String edadMaxima) {
    this.edadMaxima = Integer.parseInt(edadMaxima);
  }
  public String getEdadMinima() {
    return String.valueOf(this.edadMinima);
  }
  public void setEdadMinima(String edadMinima) {
    this.edadMinima = Integer.parseInt(edadMinima);
  }
  public Date getFechaTope() {
    return this.fechaTope;
  }
  public void setFechaTope(Date fechaTope) {
    this.fechaTope = fechaTope;
  }
  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String orden) {
    this.orden = orden;
  }

  public String getParentesco() {
    return this.parentesco;
  }
  public void setParentesco(String parentesco) {
    this.parentesco = parentesco;
  }
  public long getIdRegion() {
    return this.idRegion;
  }
  public void setEdadMaxima(int i) {
    this.edadMaxima = i;
  }
  public void setEdadMinima(int i) {
    this.edadMinima = i;
  }
  public void setIdRegion(long l) {
    this.idRegion = l;
  }

  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }

  public String getAgrupado() {
    return this.agrupado;
  }

  public void setAgrupado(String agrupado) {
    this.agrupado = agrupado;
  }
}