package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.Carrera;
import sigefirrhh.base.personal.NivelEducativo;
import sigefirrhh.base.personal.Titulo;
import sigefirrhh.base.ubicacion.Ciudad;

public class Educacion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_REGISTRO_TITULO;
  protected static final Map LISTA_BECADO;
  protected static final Map LISTA_SECTOR;
  protected static final Map LISTA_REEMBOLSO;
  private long idEducacion;
  private NivelEducativo nivelEducativo;
  private int anioInicio;
  private int anioFin;
  private String estatus;
  private Carrera carrera;
  private Titulo titulo;
  private String registroTitulo;
  private Date fechaRegistro;
  private String nombreEntidad;
  private String nombrePostgrado;
  private Ciudad ciudad;
  private String sector;
  private String escala;
  private String calificacion;
  private String mencion;
  private String becado;
  private String organizacionBecaria;
  private String reembolso;
  private String observaciones;
  private int aniosExperiencia;
  private int mesesExperiencia;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anioFin", "anioInicio", "aniosExperiencia", "becado", "calificacion", "carrera", "ciudad", "escala", "estatus", "fechaRegistro", "idEducacion", "idSitp", "mencion", "mesesExperiencia", "nivelEducativo", "nombreEntidad", "nombrePostgrado", "observaciones", "organizacionBecaria", "personal", "reembolso", "registroTitulo", "sector", "tiempoSitp", "titulo" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.personal.Carrera"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.personal.NivelEducativo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.Titulo") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 26, 26, 21, 21, 21, 24, 21, 21, 21, 26, 21, 21, 21, 21, 26, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Educacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Educacion());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_REGISTRO_TITULO = 
      new LinkedHashMap();
    LISTA_BECADO = 
      new LinkedHashMap();
    LISTA_SECTOR = 
      new LinkedHashMap();
    LISTA_REEMBOLSO = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("E", "ESTUDIANDO");
    LISTA_ESTATUS.put("F", "FINALIZO");
    LISTA_ESTATUS.put("N", "NO TERMINO");
    LISTA_REGISTRO_TITULO.put("S", "SI");
    LISTA_REGISTRO_TITULO.put("N", "NO");
    LISTA_BECADO.put("N", "NO");
    LISTA_BECADO.put("S", "SI");
    LISTA_SECTOR.put("P", "PRIVADO");
    LISTA_SECTOR.put("U", "PUBLICO");
    LISTA_SECTOR.put("E", "EXTERIOR");
    LISTA_REEMBOLSO.put("S", "SI");
    LISTA_REEMBOLSO.put("N", "NO");
  }

  public Educacion()
  {
    jdoSetestatus(this, "F");

    jdoSetregistroTitulo(this, "S");

    jdoSetsector(this, "P");

    jdoSetbecado(this, "N");

    jdoSetreembolso(this, "N");

    jdoSetaniosExperiencia(this, 0);

    jdoSetmesesExperiencia(this, 0);
  }

  public String toString()
  {
    if (jdoGetcarrera(this) != null) {
      return jdoGetnivelEducativo(this).getDescripcion() + " - " + 
        LISTA_ESTATUS.get(jdoGetestatus(this)) + " - " + 
        jdoGetanioInicio(this) + " - " + 
        jdoGetanioFin(this) + " - " + 
        jdoGetcarrera(this).getNombre();
    }
    return jdoGetnivelEducativo(this).getDescripcion() + " - " + 
      LISTA_ESTATUS.get(jdoGetestatus(this)) + " - " + 
      jdoGetanioInicio(this) + " - " + 
      jdoGetanioFin(this);
  }

  public int getAnioFin()
  {
    return jdoGetanioFin(this);
  }

  public int getAnioInicio() {
    return jdoGetanioInicio(this);
  }

  public int getAniosExperiencia() {
    return jdoGetaniosExperiencia(this);
  }

  public String getBecado() {
    return jdoGetbecado(this);
  }

  public Carrera getCarrera() {
    return jdoGetcarrera(this);
  }

  public Ciudad getCiudad() {
    return jdoGetciudad(this);
  }

  public String getEstatus() {
    return jdoGetestatus(this);
  }

  public long getIdEducacion() {
    return jdoGetidEducacion(this);
  }

  public int getMesesExperiencia() {
    return jdoGetmesesExperiencia(this);
  }

  public NivelEducativo getNivelEducativo() {
    return jdoGetnivelEducativo(this);
  }

  public String getNombreEntidad() {
    return jdoGetnombreEntidad(this);
  }

  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }

  public String getOrganizacionBecaria() {
    return jdoGetorganizacionBecaria(this);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public String getRegistroTitulo() {
    return jdoGetregistroTitulo(this);
  }

  public String getSector() {
    return jdoGetsector(this);
  }

  public Titulo getTitulo() {
    return jdoGettitulo(this);
  }

  public void setAnioFin(int i) {
    jdoSetanioFin(this, i);
  }

  public void setAnioInicio(int i) {
    jdoSetanioInicio(this, i);
  }

  public void setAniosExperiencia(int i) {
    jdoSetaniosExperiencia(this, i);
  }

  public void setBecado(String string) {
    jdoSetbecado(this, string);
  }

  public void setCarrera(Carrera carrera) {
    jdoSetcarrera(this, carrera);
  }

  public void setCiudad(Ciudad ciudad) {
    jdoSetciudad(this, ciudad);
  }

  public void setEstatus(String string) {
    jdoSetestatus(this, string);
  }

  public void setIdEducacion(long l) {
    jdoSetidEducacion(this, l);
  }

  public void setMesesExperiencia(int i) {
    jdoSetmesesExperiencia(this, i);
  }

  public void setNivelEducativo(NivelEducativo educativo) {
    jdoSetnivelEducativo(this, educativo);
  }

  public void setNombreEntidad(String string) {
    jdoSetnombreEntidad(this, string);
  }

  public void setObservaciones(String string) {
    jdoSetobservaciones(this, string);
  }

  public void setOrganizacionBecaria(String string) {
    jdoSetorganizacionBecaria(this, string);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public void setRegistroTitulo(String string) {
    jdoSetregistroTitulo(this, string);
  }

  public void setSector(String string) {
    jdoSetsector(this, string);
  }

  public void setTitulo(Titulo titulo) {
    jdoSettitulo(this, titulo);
  }

  public Date getFechaRegistro() {
    return jdoGetfechaRegistro(this);
  }

  public void setFechaRegistro(Date date) {
    jdoSetfechaRegistro(this, date);
  }

  public String getReembolso() {
    return jdoGetreembolso(this);
  }

  public void setReembolso(String string) {
    jdoSetreembolso(this, string);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public String getNombrePostgrado()
  {
    return jdoGetnombrePostgrado(this);
  }

  public void setNombrePostgrado(String string)
  {
    jdoSetnombrePostgrado(this, string);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public String getCalificacion()
  {
    return jdoGetcalificacion(this);
  }

  public String getEscala()
  {
    return jdoGetescala(this);
  }

  public void setCalificacion(String string)
  {
    jdoSetcalificacion(this, string);
  }

  public void setEscala(String string)
  {
    jdoSetescala(this, string);
  }

  public String getMencion()
  {
    return jdoGetmencion(this);
  }

  public void setMencion(String string)
  {
    jdoSetmencion(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 25;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Educacion localEducacion = new Educacion();
    localEducacion.jdoFlags = 1;
    localEducacion.jdoStateManager = paramStateManager;
    return localEducacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Educacion localEducacion = new Educacion();
    localEducacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEducacion.jdoFlags = 1;
    localEducacion.jdoStateManager = paramStateManager;
    return localEducacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioFin);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioInicio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosExperiencia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.becado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.calificacion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.carrera);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.escala);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEducacion);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mencion);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesesExperiencia);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nivelEducativo);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreEntidad);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombrePostgrado);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.organizacionBecaria);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.reembolso);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.registroTitulo);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sector);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.titulo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioFin = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioInicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosExperiencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.becado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.calificacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.carrera = ((Carrera)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.escala = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEducacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mencion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesesExperiencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = ((NivelEducativo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombrePostgrado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organizacionBecaria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.reembolso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registroTitulo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sector = localStateManager.replacingStringField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.titulo = ((Titulo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Educacion paramEducacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.anioFin = paramEducacion.anioFin;
      return;
    case 1:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.anioInicio = paramEducacion.anioInicio;
      return;
    case 2:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.aniosExperiencia = paramEducacion.aniosExperiencia;
      return;
    case 3:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.becado = paramEducacion.becado;
      return;
    case 4:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.calificacion = paramEducacion.calificacion;
      return;
    case 5:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.carrera = paramEducacion.carrera;
      return;
    case 6:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramEducacion.ciudad;
      return;
    case 7:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.escala = paramEducacion.escala;
      return;
    case 8:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramEducacion.estatus;
      return;
    case 9:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramEducacion.fechaRegistro;
      return;
    case 10:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.idEducacion = paramEducacion.idEducacion;
      return;
    case 11:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramEducacion.idSitp;
      return;
    case 12:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.mencion = paramEducacion.mencion;
      return;
    case 13:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.mesesExperiencia = paramEducacion.mesesExperiencia;
      return;
    case 14:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramEducacion.nivelEducativo;
      return;
    case 15:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreEntidad = paramEducacion.nombreEntidad;
      return;
    case 16:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombrePostgrado = paramEducacion.nombrePostgrado;
      return;
    case 17:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramEducacion.observaciones;
      return;
    case 18:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.organizacionBecaria = paramEducacion.organizacionBecaria;
      return;
    case 19:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramEducacion.personal;
      return;
    case 20:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.reembolso = paramEducacion.reembolso;
      return;
    case 21:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.registroTitulo = paramEducacion.registroTitulo;
      return;
    case 22:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.sector = paramEducacion.sector;
      return;
    case 23:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramEducacion.tiempoSitp;
      return;
    case 24:
      if (paramEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.titulo = paramEducacion.titulo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Educacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Educacion localEducacion = (Educacion)paramObject;
    if (localEducacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEducacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EducacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EducacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EducacionPK))
      throw new IllegalArgumentException("arg1");
    EducacionPK localEducacionPK = (EducacionPK)paramObject;
    localEducacionPK.idEducacion = this.idEducacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EducacionPK))
      throw new IllegalArgumentException("arg1");
    EducacionPK localEducacionPK = (EducacionPK)paramObject;
    this.idEducacion = localEducacionPK.idEducacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EducacionPK))
      throw new IllegalArgumentException("arg2");
    EducacionPK localEducacionPK = (EducacionPK)paramObject;
    localEducacionPK.idEducacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 10);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EducacionPK))
      throw new IllegalArgumentException("arg2");
    EducacionPK localEducacionPK = (EducacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 10, localEducacionPK.idEducacion);
  }

  private static final int jdoGetanioFin(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.anioFin;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.anioFin;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 0))
      return paramEducacion.anioFin;
    return localStateManager.getIntField(paramEducacion, jdoInheritedFieldCount + 0, paramEducacion.anioFin);
  }

  private static final void jdoSetanioFin(Educacion paramEducacion, int paramInt)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.anioFin = paramInt;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.anioFin = paramInt;
      return;
    }
    localStateManager.setIntField(paramEducacion, jdoInheritedFieldCount + 0, paramEducacion.anioFin, paramInt);
  }

  private static final int jdoGetanioInicio(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.anioInicio;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.anioInicio;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 1))
      return paramEducacion.anioInicio;
    return localStateManager.getIntField(paramEducacion, jdoInheritedFieldCount + 1, paramEducacion.anioInicio);
  }

  private static final void jdoSetanioInicio(Educacion paramEducacion, int paramInt)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.anioInicio = paramInt;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.anioInicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramEducacion, jdoInheritedFieldCount + 1, paramEducacion.anioInicio, paramInt);
  }

  private static final int jdoGetaniosExperiencia(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.aniosExperiencia;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.aniosExperiencia;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 2))
      return paramEducacion.aniosExperiencia;
    return localStateManager.getIntField(paramEducacion, jdoInheritedFieldCount + 2, paramEducacion.aniosExperiencia);
  }

  private static final void jdoSetaniosExperiencia(Educacion paramEducacion, int paramInt)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.aniosExperiencia = paramInt;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.aniosExperiencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramEducacion, jdoInheritedFieldCount + 2, paramEducacion.aniosExperiencia, paramInt);
  }

  private static final String jdoGetbecado(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.becado;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.becado;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 3))
      return paramEducacion.becado;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 3, paramEducacion.becado);
  }

  private static final void jdoSetbecado(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.becado = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.becado = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 3, paramEducacion.becado, paramString);
  }

  private static final String jdoGetcalificacion(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.calificacion;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.calificacion;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 4))
      return paramEducacion.calificacion;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 4, paramEducacion.calificacion);
  }

  private static final void jdoSetcalificacion(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.calificacion = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.calificacion = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 4, paramEducacion.calificacion, paramString);
  }

  private static final Carrera jdoGetcarrera(Educacion paramEducacion)
  {
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.carrera;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 5))
      return paramEducacion.carrera;
    return (Carrera)localStateManager.getObjectField(paramEducacion, jdoInheritedFieldCount + 5, paramEducacion.carrera);
  }

  private static final void jdoSetcarrera(Educacion paramEducacion, Carrera paramCarrera)
  {
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.carrera = paramCarrera;
      return;
    }
    localStateManager.setObjectField(paramEducacion, jdoInheritedFieldCount + 5, paramEducacion.carrera, paramCarrera);
  }

  private static final Ciudad jdoGetciudad(Educacion paramEducacion)
  {
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.ciudad;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 6))
      return paramEducacion.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramEducacion, jdoInheritedFieldCount + 6, paramEducacion.ciudad);
  }

  private static final void jdoSetciudad(Educacion paramEducacion, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramEducacion, jdoInheritedFieldCount + 6, paramEducacion.ciudad, paramCiudad);
  }

  private static final String jdoGetescala(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.escala;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.escala;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 7))
      return paramEducacion.escala;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 7, paramEducacion.escala);
  }

  private static final void jdoSetescala(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.escala = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.escala = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 7, paramEducacion.escala, paramString);
  }

  private static final String jdoGetestatus(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.estatus;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.estatus;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 8))
      return paramEducacion.estatus;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 8, paramEducacion.estatus);
  }

  private static final void jdoSetestatus(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 8, paramEducacion.estatus, paramString);
  }

  private static final Date jdoGetfechaRegistro(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.fechaRegistro;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.fechaRegistro;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 9))
      return paramEducacion.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramEducacion, jdoInheritedFieldCount + 9, paramEducacion.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(Educacion paramEducacion, Date paramDate)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramEducacion, jdoInheritedFieldCount + 9, paramEducacion.fechaRegistro, paramDate);
  }

  private static final long jdoGetidEducacion(Educacion paramEducacion)
  {
    return paramEducacion.idEducacion;
  }

  private static final void jdoSetidEducacion(Educacion paramEducacion, long paramLong)
  {
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.idEducacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramEducacion, jdoInheritedFieldCount + 10, paramEducacion.idEducacion, paramLong);
  }

  private static final int jdoGetidSitp(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.idSitp;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.idSitp;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 11))
      return paramEducacion.idSitp;
    return localStateManager.getIntField(paramEducacion, jdoInheritedFieldCount + 11, paramEducacion.idSitp);
  }

  private static final void jdoSetidSitp(Educacion paramEducacion, int paramInt)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramEducacion, jdoInheritedFieldCount + 11, paramEducacion.idSitp, paramInt);
  }

  private static final String jdoGetmencion(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.mencion;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.mencion;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 12))
      return paramEducacion.mencion;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 12, paramEducacion.mencion);
  }

  private static final void jdoSetmencion(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.mencion = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.mencion = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 12, paramEducacion.mencion, paramString);
  }

  private static final int jdoGetmesesExperiencia(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.mesesExperiencia;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.mesesExperiencia;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 13))
      return paramEducacion.mesesExperiencia;
    return localStateManager.getIntField(paramEducacion, jdoInheritedFieldCount + 13, paramEducacion.mesesExperiencia);
  }

  private static final void jdoSetmesesExperiencia(Educacion paramEducacion, int paramInt)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.mesesExperiencia = paramInt;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.mesesExperiencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramEducacion, jdoInheritedFieldCount + 13, paramEducacion.mesesExperiencia, paramInt);
  }

  private static final NivelEducativo jdoGetnivelEducativo(Educacion paramEducacion)
  {
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.nivelEducativo;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 14))
      return paramEducacion.nivelEducativo;
    return (NivelEducativo)localStateManager.getObjectField(paramEducacion, jdoInheritedFieldCount + 14, paramEducacion.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(Educacion paramEducacion, NivelEducativo paramNivelEducativo)
  {
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.nivelEducativo = paramNivelEducativo;
      return;
    }
    localStateManager.setObjectField(paramEducacion, jdoInheritedFieldCount + 14, paramEducacion.nivelEducativo, paramNivelEducativo);
  }

  private static final String jdoGetnombreEntidad(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.nombreEntidad;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.nombreEntidad;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 15))
      return paramEducacion.nombreEntidad;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 15, paramEducacion.nombreEntidad);
  }

  private static final void jdoSetnombreEntidad(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.nombreEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.nombreEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 15, paramEducacion.nombreEntidad, paramString);
  }

  private static final String jdoGetnombrePostgrado(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.nombrePostgrado;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.nombrePostgrado;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 16))
      return paramEducacion.nombrePostgrado;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 16, paramEducacion.nombrePostgrado);
  }

  private static final void jdoSetnombrePostgrado(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.nombrePostgrado = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.nombrePostgrado = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 16, paramEducacion.nombrePostgrado, paramString);
  }

  private static final String jdoGetobservaciones(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.observaciones;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.observaciones;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 17))
      return paramEducacion.observaciones;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 17, paramEducacion.observaciones);
  }

  private static final void jdoSetobservaciones(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 17, paramEducacion.observaciones, paramString);
  }

  private static final String jdoGetorganizacionBecaria(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.organizacionBecaria;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.organizacionBecaria;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 18))
      return paramEducacion.organizacionBecaria;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 18, paramEducacion.organizacionBecaria);
  }

  private static final void jdoSetorganizacionBecaria(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.organizacionBecaria = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.organizacionBecaria = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 18, paramEducacion.organizacionBecaria, paramString);
  }

  private static final Personal jdoGetpersonal(Educacion paramEducacion)
  {
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.personal;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 19))
      return paramEducacion.personal;
    return (Personal)localStateManager.getObjectField(paramEducacion, jdoInheritedFieldCount + 19, paramEducacion.personal);
  }

  private static final void jdoSetpersonal(Educacion paramEducacion, Personal paramPersonal)
  {
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramEducacion, jdoInheritedFieldCount + 19, paramEducacion.personal, paramPersonal);
  }

  private static final String jdoGetreembolso(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.reembolso;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.reembolso;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 20))
      return paramEducacion.reembolso;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 20, paramEducacion.reembolso);
  }

  private static final void jdoSetreembolso(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.reembolso = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.reembolso = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 20, paramEducacion.reembolso, paramString);
  }

  private static final String jdoGetregistroTitulo(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.registroTitulo;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.registroTitulo;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 21))
      return paramEducacion.registroTitulo;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 21, paramEducacion.registroTitulo);
  }

  private static final void jdoSetregistroTitulo(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.registroTitulo = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.registroTitulo = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 21, paramEducacion.registroTitulo, paramString);
  }

  private static final String jdoGetsector(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.sector;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.sector;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 22))
      return paramEducacion.sector;
    return localStateManager.getStringField(paramEducacion, jdoInheritedFieldCount + 22, paramEducacion.sector);
  }

  private static final void jdoSetsector(Educacion paramEducacion, String paramString)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.sector = paramString;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.sector = paramString;
      return;
    }
    localStateManager.setStringField(paramEducacion, jdoInheritedFieldCount + 22, paramEducacion.sector, paramString);
  }

  private static final Date jdoGettiempoSitp(Educacion paramEducacion)
  {
    if (paramEducacion.jdoFlags <= 0)
      return paramEducacion.tiempoSitp;
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.tiempoSitp;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 23))
      return paramEducacion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramEducacion, jdoInheritedFieldCount + 23, paramEducacion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Educacion paramEducacion, Date paramDate)
  {
    if (paramEducacion.jdoFlags == 0)
    {
      paramEducacion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramEducacion, jdoInheritedFieldCount + 23, paramEducacion.tiempoSitp, paramDate);
  }

  private static final Titulo jdoGettitulo(Educacion paramEducacion)
  {
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramEducacion.titulo;
    if (localStateManager.isLoaded(paramEducacion, jdoInheritedFieldCount + 24))
      return paramEducacion.titulo;
    return (Titulo)localStateManager.getObjectField(paramEducacion, jdoInheritedFieldCount + 24, paramEducacion.titulo);
  }

  private static final void jdoSettitulo(Educacion paramEducacion, Titulo paramTitulo)
  {
    StateManager localStateManager = paramEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEducacion.titulo = paramTitulo;
      return;
    }
    localStateManager.setObjectField(paramEducacion, jdoInheritedFieldCount + 24, paramEducacion.titulo, paramTitulo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}