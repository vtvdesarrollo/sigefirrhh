package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.personal.PersonalFacade;
import sigefirrhh.base.personal.RelacionRecaudo;
import sigefirrhh.login.LoginSession;

public class AusenciaRecaudoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(AusenciaRecaudoForm.class.getName());
  private AusenciaRecaudo ausenciaRecaudo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private PersonalFacade personalFacade = new PersonalFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private Collection colRelacionRecaudo;
  private Collection colAusencia;
  private String selectRelacionRecaudo;
  private String selectAusencia;

  public String getSelectRelacionRecaudo()
  {
    return this.selectRelacionRecaudo;
  }
  public void setSelectRelacionRecaudo(String valRelacionRecaudo) {
    Iterator iterator = this.colRelacionRecaudo.iterator();
    RelacionRecaudo relacionRecaudo = null;
    this.ausenciaRecaudo.setRelacionRecaudo(null);
    while (iterator.hasNext()) {
      relacionRecaudo = (RelacionRecaudo)iterator.next();
      if (String.valueOf(relacionRecaudo.getIdRelacionRecaudo()).equals(
        valRelacionRecaudo)) {
        this.ausenciaRecaudo.setRelacionRecaudo(
          relacionRecaudo);
        break;
      }
    }
    this.selectRelacionRecaudo = valRelacionRecaudo;
  }
  public String getSelectAusencia() {
    return this.selectAusencia;
  }
  public void setSelectAusencia(String valAusencia) {
    Iterator iterator = this.colAusencia.iterator();
    Ausencia ausencia = null;
    this.ausenciaRecaudo.setAusencia(null);
    while (iterator.hasNext()) {
      ausencia = (Ausencia)iterator.next();
      if (String.valueOf(ausencia.getIdAusencia()).equals(
        valAusencia)) {
        this.ausenciaRecaudo.setAusencia(
          ausencia);
        break;
      }
    }
    this.selectAusencia = valAusencia;
  }
  public Collection getResult() {
    return this.result;
  }

  public AusenciaRecaudo getAusenciaRecaudo() {
    if (this.ausenciaRecaudo == null) {
      this.ausenciaRecaudo = new AusenciaRecaudo();
    }
    return this.ausenciaRecaudo;
  }

  public AusenciaRecaudoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRelacionRecaudo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRelacionRecaudo.iterator();
    RelacionRecaudo relacionRecaudo = null;
    while (iterator.hasNext()) {
      relacionRecaudo = (RelacionRecaudo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(relacionRecaudo.getIdRelacionRecaudo()), 
        relacionRecaudo.toString()));
    }
    return col;
  }

  public Collection getColAusencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAusencia.iterator();
    Ausencia ausencia = null;
    while (iterator.hasNext()) {
      ausencia = (Ausencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ausencia.getIdAusencia()), 
        ausencia.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colRelacionRecaudo = 
        this.personalFacade.findAllRelacionRecaudo();
      this.colAusencia = 
        this.expedienteFacade.findAllAusencia();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String selectAusenciaRecaudo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRelacionRecaudo = null;
    this.selectAusencia = null;

    long idAusenciaRecaudo = 
      Long.parseLong((String)requestParameterMap.get("idAusenciaRecaudo"));
    try
    {
      this.ausenciaRecaudo = 
        this.expedienteFacade.findAusenciaRecaudoById(
        idAusenciaRecaudo);
      if (this.ausenciaRecaudo.getRelacionRecaudo() != null) {
        this.selectRelacionRecaudo = 
          String.valueOf(this.ausenciaRecaudo.getRelacionRecaudo().getIdRelacionRecaudo());
      }
      if (this.ausenciaRecaudo.getAusencia() != null) {
        this.selectAusencia = 
          String.valueOf(this.ausenciaRecaudo.getAusencia().getIdAusencia());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.ausenciaRecaudo = null;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.ausenciaRecaudo.getFechaRecaudo() != null) && 
      (this.ausenciaRecaudo.getFechaRecaudo().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Recaudo no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.expedienteFacade.addAusenciaRecaudo(
          this.ausenciaRecaudo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.expedienteFacade.updateAusenciaRecaudo(
          this.ausenciaRecaudo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.expedienteFacade.deleteAusenciaRecaudo(
        this.ausenciaRecaudo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.ausenciaRecaudo = new AusenciaRecaudo();

    this.selectRelacionRecaudo = null;

    this.selectAusencia = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.ausenciaRecaudo.setIdAusenciaRecaudo(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.AusenciaRecaudo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.ausenciaRecaudo = new AusenciaRecaudo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}