package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.CalendarTools;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.mre.MreFacade;
import sigefirrhh.base.mre.SedeDiplomatica;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ComisionServicioForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ComisionServicioForm.class.getName());
  private ComisionServicio comisionServicio;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private MreFacade mreFacade = new MreFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private String diferencia;
  private Collection colSedeDiplomatica;
  private Collection colPersonal;
  private String selectSedeDiplomatica;
  private String selectPersonal;
  private Object stateScrollPersonal = null;
  private Object stateResultPersonal = null;

  private Object stateScrollComisionServicioByPersonal = null;
  private Object stateResultComisionServicioByPersonal = null;

  public String getSelectSedeDiplomatica()
  {
    return this.selectSedeDiplomatica;
  }
  public void setSelectSedeDiplomatica(String valSedeDiplomatica) {
    Iterator iterator = this.colSedeDiplomatica.iterator();
    SedeDiplomatica sedeDiplomatica = null;
    this.comisionServicio.setSedeDiplomatica(null);
    while (iterator.hasNext()) {
      sedeDiplomatica = (SedeDiplomatica)iterator.next();
      if (String.valueOf(sedeDiplomatica.getIdSedeDiplomatica()).equals(
        valSedeDiplomatica)) {
        this.comisionServicio.setSedeDiplomatica(
          sedeDiplomatica);
      }
    }
    this.selectSedeDiplomatica = valSedeDiplomatica;
  }
  public String getSelectPersonal() {
    return this.selectPersonal;
  }
  public void setSelectPersonal(String valPersonal) {
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    this.comisionServicio.setPersonal(null);
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      if (String.valueOf(personal.getIdPersonal()).equals(
        valPersonal)) {
        this.comisionServicio.setPersonal(
          personal);
      }
    }
    this.selectPersonal = valPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public ComisionServicio getComisionServicio() {
    if (this.comisionServicio == null) {
      this.comisionServicio = new ComisionServicio();
    }
    return this.comisionServicio;
  }

  public ComisionServicioForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColSedeDiplomatica()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colSedeDiplomatica.iterator();
    SedeDiplomatica sedeDiplomatica = null;
    while (iterator.hasNext()) {
      sedeDiplomatica = (SedeDiplomatica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(sedeDiplomatica.getIdSedeDiplomatica()), 
        sedeDiplomatica.toString()));
    }
    return col;
  }

  public Collection getColPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(personal.getIdPersonal()), 
        personal.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colSedeDiplomatica = 
        this.mreFacade.findAllSedeDiplomatica();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findComisionServicioByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.expedienteFacade.findComisionServicioByPersonal(
          this.personal.getIdPersonal());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectComisionServicio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectSedeDiplomatica = null;
    this.selectPersonal = null;

    long idComisionServicio = 
      Long.parseLong((String)requestParameterMap.get("idComisionServicio"));
    try
    {
      this.comisionServicio = 
        this.expedienteFacade.findComisionServicioById(
        idComisionServicio);

      if (this.comisionServicio.getSedeDiplomatica() != null) {
        this.selectSedeDiplomatica = 
          String.valueOf(this.comisionServicio.getSedeDiplomatica().getIdSedeDiplomatica());
      }
      if (this.comisionServicio.getPersonal() != null) {
        this.selectPersonal = 
          String.valueOf(this.comisionServicio.getPersonal().getIdPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.comisionServicio.getFechaInicio() != null) && 
      (this.comisionServicio.getFechaInicio().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Inicio no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.comisionServicio.getTiempoSitp() != null) && 
      (this.comisionServicio.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }
    try
    {
      if ((this.comisionServicio.getFechaFin() != null) && (this.comisionServicio.getFechaInicio().compareTo(this.comisionServicio.getFechaFin()) >= 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Fecha Fin no puede ser menor a la Fecha Inicio", ""));
        error = true;
      }
    }
    catch (Exception localException1) {
    }
    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.comisionServicio.setPersonal(
          this.personal);
        this.expedienteFacade.addComisionServicio(
          this.comisionServicio);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.comisionServicio, this.personal);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.expedienteFacade.updateComisionServicio(
          this.comisionServicio);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.comisionServicio, this.personal);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.expedienteFacade.deleteComisionServicio(
        this.comisionServicio);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.comisionServicio, this.personal);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;

    this.selectSedeDiplomatica = null;

    this.selectPersonal = null;

    this.comisionServicio = new ComisionServicio();

    this.comisionServicio.setPersonal(this.personal);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.comisionServicio.setIdComisionServicio(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.ComisionServicio"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.comisionServicio = new ComisionServicio();
    return "cancel";
  }
  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.comisionServicio = new ComisionServicio();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }

  public String getDiferencia()
  {
    if ((this.comisionServicio.getFechaInicio() != null) && (this.comisionServicio.getFechaFin() != null)) {
      CalendarTools caldendarTools = new CalendarTools();
      return caldendarTools.calculateDifTwoDates(this.comisionServicio.getFechaInicio(), this.comisionServicio.getFechaFin());
    }
    return null;
  }

  public void setDiferencia(String string)
  {
    this.diferencia = string;
  }

  public LoginSession getLogin() {
    return this.login;
  }
}