package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class TrayectoriaPK
  implements Serializable
{
  public long idTrayectoria;

  public TrayectoriaPK()
  {
  }

  public TrayectoriaPK(long idTrayectoria)
  {
    this.idTrayectoria = idTrayectoria;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TrayectoriaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TrayectoriaPK thatPK)
  {
    return 
      this.idTrayectoria == thatPK.idTrayectoria;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTrayectoria)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTrayectoria);
  }
}