package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.Carrera;
import sigefirrhh.base.personal.CarreraBeanBusiness;
import sigefirrhh.base.personal.NivelEducativo;
import sigefirrhh.base.personal.NivelEducativoBeanBusiness;
import sigefirrhh.base.personal.Titulo;
import sigefirrhh.base.personal.TituloBeanBusiness;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;

public class EducacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEducacion(Educacion educacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Educacion educacionNew = 
      (Educacion)BeanUtils.cloneBean(
      educacion);

    NivelEducativoBeanBusiness nivelEducativoBeanBusiness = new NivelEducativoBeanBusiness();

    if (educacionNew.getNivelEducativo() != null) {
      educacionNew.setNivelEducativo(
        nivelEducativoBeanBusiness.findNivelEducativoById(
        educacionNew.getNivelEducativo().getIdNivelEducativo()));
    }

    CarreraBeanBusiness carreraBeanBusiness = new CarreraBeanBusiness();

    if (educacionNew.getCarrera() != null) {
      educacionNew.setCarrera(
        carreraBeanBusiness.findCarreraById(
        educacionNew.getCarrera().getIdCarrera()));
    }

    TituloBeanBusiness tituloBeanBusiness = new TituloBeanBusiness();

    if (educacionNew.getTitulo() != null) {
      educacionNew.setTitulo(
        tituloBeanBusiness.findTituloById(
        educacionNew.getTitulo().getIdTitulo()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (educacionNew.getCiudad() != null) {
      educacionNew.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        educacionNew.getCiudad().getIdCiudad()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (educacionNew.getPersonal() != null) {
      educacionNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        educacionNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(educacionNew);
  }

  public void updateEducacion(Educacion educacion) throws Exception
  {
    Educacion educacionModify = 
      findEducacionById(educacion.getIdEducacion());

    NivelEducativoBeanBusiness nivelEducativoBeanBusiness = new NivelEducativoBeanBusiness();

    if (educacion.getNivelEducativo() != null) {
      educacion.setNivelEducativo(
        nivelEducativoBeanBusiness.findNivelEducativoById(
        educacion.getNivelEducativo().getIdNivelEducativo()));
    }

    CarreraBeanBusiness carreraBeanBusiness = new CarreraBeanBusiness();

    if (educacion.getCarrera() != null) {
      educacion.setCarrera(
        carreraBeanBusiness.findCarreraById(
        educacion.getCarrera().getIdCarrera()));
    }

    TituloBeanBusiness tituloBeanBusiness = new TituloBeanBusiness();

    if (educacion.getTitulo() != null) {
      educacion.setTitulo(
        tituloBeanBusiness.findTituloById(
        educacion.getTitulo().getIdTitulo()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (educacion.getCiudad() != null) {
      educacion.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        educacion.getCiudad().getIdCiudad()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (educacion.getPersonal() != null) {
      educacion.setPersonal(
        personalBeanBusiness.findPersonalById(
        educacion.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(educacionModify, educacion);
  }

  public void deleteEducacion(Educacion educacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Educacion educacionDelete = 
      findEducacionById(educacion.getIdEducacion());
    pm.deletePersistent(educacionDelete);
  }

  public Educacion findEducacionById(long idEducacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEducacion == pIdEducacion";
    Query query = pm.newQuery(Educacion.class, filter);

    query.declareParameters("long pIdEducacion");

    parameters.put("pIdEducacion", new Long(idEducacion));

    Collection colEducacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEducacion.iterator();
    return (Educacion)iterator.next();
  }

  public Collection findEducacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent educacionExtent = pm.getExtent(
      Educacion.class, true);
    Query query = pm.newQuery(educacionExtent);
    query.setOrdering("anioInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Educacion.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anioInicio ascending");

    Collection colEducacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEducacion);

    return colEducacion;
  }
}