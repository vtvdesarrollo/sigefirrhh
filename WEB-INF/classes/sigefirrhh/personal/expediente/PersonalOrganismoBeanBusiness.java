package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class PersonalOrganismoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPersonalOrganismo(PersonalOrganismo personalOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PersonalOrganismo personalOrganismoNew = 
      (PersonalOrganismo)BeanUtils.cloneBean(
      personalOrganismo);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (personalOrganismoNew.getPersonal() != null) {
      personalOrganismoNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        personalOrganismoNew.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (personalOrganismoNew.getOrganismo() != null) {
      personalOrganismoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        personalOrganismoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(personalOrganismoNew);
  }

  public void updatePersonalOrganismo(PersonalOrganismo personalOrganismo) throws Exception
  {
    PersonalOrganismo personalOrganismoModify = 
      findPersonalOrganismoById(personalOrganismo.getIdPersonalOrganismo());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (personalOrganismo.getPersonal() != null) {
      personalOrganismo.setPersonal(
        personalBeanBusiness.findPersonalById(
        personalOrganismo.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (personalOrganismo.getOrganismo() != null) {
      personalOrganismo.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        personalOrganismo.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(personalOrganismoModify, personalOrganismo);
  }

  public void deletePersonalOrganismo(PersonalOrganismo personalOrganismo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PersonalOrganismo personalOrganismoDelete = 
      findPersonalOrganismoById(personalOrganismo.getIdPersonalOrganismo());
    pm.deletePersistent(personalOrganismoDelete);
  }

  public PersonalOrganismo findPersonalOrganismoById(long idPersonalOrganismo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPersonalOrganismo == pIdPersonalOrganismo";
    Query query = pm.newQuery(PersonalOrganismo.class, filter);

    query.declareParameters("long pIdPersonalOrganismo");

    parameters.put("pIdPersonalOrganismo", new Long(idPersonalOrganismo));

    Collection colPersonalOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPersonalOrganismo.iterator();
    return (PersonalOrganismo)iterator.next();
  }

  public Collection findPersonalOrganismoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent personalOrganismoExtent = pm.getExtent(
      PersonalOrganismo.class, true);
    Query query = pm.newQuery(personalOrganismoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}